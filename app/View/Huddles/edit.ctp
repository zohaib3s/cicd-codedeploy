<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php
$user_permissions = $this->Session->read('user_permissions');
//$huddle_permission = $this->Session->read('user_huddle_level_permissions');
//echo "permissions " . $huddle_permission;
$user_current_account = $this->Session->read('user_current_account');
$users = $this->Session->read('user_current_account');
$huddle_permission = $this->Custom->get_huddle_permissions($huddle_id, $user_id);
$account_id = $users['accounts']['account_id'];
?>
<div id="wrapper" style="display:none;" >

    <style type="text/css">
        #HudDesc {
            min-height: 20px;
        }
        #HudTitle {
            min-height: 20px;
        }
    </style>
    <div class="container box newCoachingHuddle">
        <h1 id="wiz-heading1">
            <?php
            if ($huddle_type == '1') {
                echo 'Edit Collaboration Huddle';
            } else if ($huddle_type == "3") {
                echo 'Edit Assessment Huddle';
            } else {
                echo 'Edit Coaching / Mentoring Huddle';
            }
            ?></h1>
        <script type="text/javascript">

            (function ($) {
                jQuery.expr[':'].Contains = function (a, i, m) {
                    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
                };

                function listFilter(header, list) {
                    var huddle_type = <?php echo $huddle_type; ?>;
                    var form = $("<div>").attr({"class": "filterform"});
                    input = $("<input id='input-filter' placeholder='Search people or group'><div  id='cancel-btn' type='text' style=' width: 10px;  position: absolute; right: 93px; margin-top: 7px; display:none;cursor: pointer; '>X</div>").attr({"class": "filterinput", "type": "text"});
                    $(form).append(input).appendTo(header);
                    $('.filterform input').css({'width': '100%', 'float': 'right'});
                    $(input).change(function () {
                        var filter = $(this).val();
                        $('#liNodataFound').remove();
                        if (filter) {
                            $search_count = $(list).find("a:Contains(" + filter + ")").length;
                            if ($search_count > 4) {
                                $('div.thumb').css('top', '0px');
                                $('div.overview').css('top', '0px');
                                $('.scrollbar').css('display', 'block');
                            } else {
                                $('div.thumb').css('top', '0px');
                                $('div.overview').css('top', '0px');
                                $('.scrollbar').css('display', 'none');
                            }
                            $(list).find("a:not(:Contains(" + filter + "))").parent().parent().slideUp(400, function () {
                                $('.widget-scrollable').tinyscrollbar_update();
                            });
                            $(list).find("a:Contains(" + filter + ")").parent().parent().slideDown(400, function () {
                                $('.widget-scrollable').tinyscrollbar_update();
                            });
                            jQuery('#cancel-btn').css('display', 'block');
                            if ($(list).find("a:Contains(" + filter + ")").length == 0) {
                                $('#liNodataFound').remove();
                                if (huddle_type == 2) {
                                    $("#list-containers").append('<li id="liNodataFound">No people match this search. Please try again.</li>');
                                } else {
                                    $("#list-containers").append('<li id="liNodataFound">No people or groups match this search. Please try again.</li>');
                                }

                            } else {
                                $('#liNodataFound').remove();
                            }

                        } else {
                            jQuery('#cancel-btn').css('display', 'none');
                            $('.scrollbar').css('display', 'block');
                            $(list).find("li").slideDown(400, function () {
                                $('.widget-scrollable').tinyscrollbar_update();
                            });

                        }
                        return false;
                    }).keyup(function () {
                        $(this).change();
                    });

                    $('#cancel-btn').click(function (e) {
                        jQuery('#cancel-btn').css('display', 'none');
                        $('div.thumb').css('top', '0px');
                        $('div.overview').css('top', '0px');
                        $('.scrollbar').css('display', 'block');
                        jQuery('#input-filter').val('');
                        $(list).find("li").slideDown(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });
                    })
                }

                /*$(function () {
                 listFilter($("#header-container"), $("#list-containers"));
                 });*/
            }(jQuery));
            $(document).ready(function (e) {
                $('#caoch-checkbox').click(function (e) {
                    if ($(this).is(':checked') == true) {
                        $(".caoch-checkbox input[type='checkbox']").prop('checked', true);
                    } else {
                        $(".caoch-checkbox input[type='checkbox']").prop('checked', false);
                    }
                });
                /*$('#mentee-checkbox').click(function (e) {
                 if ($(this).is(':checked') == true) {
                 $(".mentee-checkbox input[type='checkbox']").prop('checked', true);
                 $("#list-containers li").each(function (value) {
                 if ($(this).find('label [type="checkbox"]').is(":checked")) {
                 if ($(this).find("span .chk_is_coachee").is(':checked')) {
                 $("#chk_is_coachee").val("1");
                 }
                 }
                 });
                 } else {
                 $(".mentee-checkbox input[type='checkbox']").prop('checked', false);
                 $("#chk_is_coachee").val("0");
                 }
                 })*/
<?php if ($huddle_type == 2): ?>
                    $("span.mentee-checkbox input[type=radio]").click(function () {

                        var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                        coachee_check_count = 0;
                        $('.huddle_edit_cls [type="checkbox"]:checked').each(function () {
                            var $radioName = $(this).attr('id');
                            var strCoach = $radioName.replace('new_chkbox_', '');
                            if ($('input[name=is_mentor_' + strCoach + ']').is(':checked')) {
                                var $radios = $('input:radio[name=user_role_' + this.value + ']');
                                $radios.filter('[value=210]').prop('checked', true);
                                coachee_check_count++;
                            }
                        });
                        if (coachee_check_count > 0) {
                            $(this).prop("checked", false);
                            var $radioName = $(this).attr('id');
                            var strCoach = $radioName.replace('mentee', 'caoch');
                            $('#' + strCoach).prop("checked", true);
                            //$(this).parent().parent().find('label [type="checkbox"]').prop("checked", false);
                            alert('You can only select one Coachee. If you want this to be your Coachee, please unselect the Coachee you have already selected.');
                        } else {
                            $(this).parent().parent().find('label [type="checkbox"]').prop("checked", true);
                        }
                        if (huddle_row_selector.length > 0 && !huddle_row_selector.is(":checked")) {
                            //huddle_row_selector.trigger('click');
                            //$("#chk_is_coachee").val("1");

                        }

                    });
<?php elseif ($huddle_type == 3): ?>
                    $("span.mentee-checkbox input[type=radio]").click(function () {
                        var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                        coachee_check_count = 0;
                        //                        $('.huddle_edit_cls [type="checkbox"]:checked').each(function () {
                        //                            var $radioName = $(this).attr('id');
                        //                            var strCoach = $radioName.replace('new_chkbox_', '');
                        //                            if ($('input[name=is_mentor_' + strCoach + ']').is(':checked')) {
                        //                                var $radios = $('input:radio[name=user_role_' + this.value + ']');
                        //                                $radios.filter('[value=210]').prop('checked', true);
                        //                                coachee_check_count++;
                        //                            }
                        //                        });
                        if (coachee_check_count > 0) {
                            $(this).prop("checked", false);
                            var $radioName = $(this).attr('id');
                            var strCoach = $radioName.replace('mentee', 'caoch');
                            $('#' + strCoach).prop("checked", true);
                            //$(this).parent().parent().find('label [type="checkbox"]').prop("checked", false);
                            alert('You can only select one Coachee. If you want this to be your Coachee, please unselect the Coachee you have already selected.');
                        } else {
                            $(this).parent().parent().find('label [type="checkbox"]').prop("checked", true);
                        }
                        if (huddle_row_selector.length > 0 && !huddle_row_selector.is(":checked")) {
                            //huddle_row_selector.trigger('click');
                            //$("#chk_is_coachee").val("1");

                        }

                    });
<?php endif ?>

                $("span.caoch-checkbox input[type=radio]").click(function () {


                    var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                    if (huddle_row_selector.length > 0 && !huddle_row_selector.is(":checked")) {
                        //huddle_row_selector.trigger('click');
                    }


                });

                $('#select-all-none').click(function (e) {
                    if ($('#select-all').is(':checked')) {
                        $('#select-all-label').html('Select None');
                        $('#select-all').prop('checked', true);
                        $(".member-user").prop('checked', true);
                        $(".super-user").prop('checked', true);
                        $(".viewer-user").prop('checked', true);
                        $(".huddle_edit_cls_chkbx").prop('checked', true);
                    } else {
                        $('#select-all-label').html('Select All');
                        $('#select-all').prop('checked', false);
                        $(".member-user").prop('checked', false);
                        $(".super-user").prop('checked', false);
                        $(".viewer-user").prop('checked', false);
                        $(".huddle_edit_cls_chkbx").prop('checked', false);
                    }

                });
            });

        </script>

        <script>
            $(document).ready(function () {

                $('#collab-huddle').click(function () {
                    $('#wiz-heading').text('Edit Collaboration Huddle');
                    $('#step2-label').text("Invite other people in your account to participate with you in the huddle. You can always do this later. You're defaulted to be an Admin in the Huddle.");
                    $('#video_huddle_name').prop("disabled", false);
                    $('#video_huddle_description').prop("disabled", false);
                    $('#video_huddle_name').css("background-color", 'white');
                    $('#video_huddle_description').css("background-color", 'white');
                    $('.coach_mentee').css('display', 'none');
                    $('.grpClass').css('display', 'block');
                    $('.coach_perms').css('display', 'block');
                    $('#select-all-none-cnt').css('display', 'block');
                    $('.groups-table-header').css('padding-bottom', '0px');
                    $("#Coaches_info").css("display", "block");
                    $("#Coachee_info").css("display", "none");
                    $(".grpUsrs").css("display", "block");

                    if ($('.coach_mentee_hidden').length > 0) {
                        $('.coach_mentee_hidden').remove();
                    }

                    if ($('.coach_coach_hidden').length > 0) {
                        $('.coach_coach_hidden').remove();
                    }

                });
                $('#coach-huddle').click(function () {
                    $("#tabs").tabs({disabled: [2]});
                    $('#wiz-heading').text('Edit Coaching / Mentoring Huddle');
                    $('#step2-label').text('Coaching Huddle creator is defaulted to be a coach in the Huddle. You may add another coach to the huddle, but you must select a participant to be coached.');
                    $('#video_huddle_name').prop("disabled", true);
                    $('#video_huddle_description').prop("disabled", true);
                    $('#video_huddle_name').css("background-color", '#F0EEE3');
                    $('#video_huddle_description').css("background-color", '#F0EEE3');
                    $('.coach_mentee').css('display', 'block');
                    $('.grpClass').css('display', 'none');

                    $('.coach_perms').css('display', 'none');
                    $('.coach_mentee').css('float', 'none');
                    $('.coach_mentee').attr('style', 'float: right !important');
                    $('#select_all_coaches_panel').css('display', 'none');
                    $('#select-all-none-cnt').css('display', 'none');
                    $('.groups-table-header').css('padding-bottom', '10px');
                    $("#Coaches_info").css("display", "none");
                    $("#Coachee_info").css("display", "block");
                    $(".grpUsrs").css("display", "none");

                    $("span.mentee-checkbox input[type=radio]").each(function () {

                        if ($(this).is(":checked")) {
                            var radio_id = $(this).attr('id').split('_');
                            //$('#super_admin_ids_'+radio_id[1]).prop('checked', true);
                            //$('#new_checkbox_'+radio_id[1]).prop('checked', true);
                            var html = '<input type="checkbox" class="coach_mentee_hidden" value="1" id="is_mentor_' + radio_id[1] + '" name="is_mentor_' + radio_id[1] + '" style="display:none" checked="checked" />';
                            $(this).parent().parent().append(html);
                        }

                    });

                    $("span.caoch-checkbox input[type=radio]").each(function () {

                        if ($(this).is(":checked")) {
                            var radio_id = $(this).attr('id').split('_');
                            $('#super_admin_ids_' + radio_id[1]).prop('checked', true);
                            $('#new_checkbox_' + radio_id[1]).prop('checked', true);

                            var html = '<input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_' + radio_id[1] + '" name="is_coach_' + radio_id[1] + '" style="display:none"  checked="checked">';
                            $(this).parent().parent().append(html);
                        }

                    });
                });
                $('.step2').click(function () {

                    $("#tabs").tabs("option", "active", 1);

                    setTimeout(function () {
                        $('#input-filter').trigger('change');
                    }, 200);

                    $('#HudTitle').css("display", "block");
                    $('#HudDesc').css("display", "block");
                    if ($('#collab-huddle').is(':checked')) {
                        if ($('#video_huddle_name').val() != '') {
                            $("#huddle-name-last").text($('#video_huddle_name').val());
                            $("#huddle-name-last").css('width', '250px');
                            $("#name-id").val($('#video_huddle_name').val());
                            $("#ajaxInputName").val($('#video_huddle_name').val());
                            $(".editArea").css("display", "none");
                        } else {
                            $(".editArea").css("display", "block");
                        }
                        if ($('#video_huddle_description').val() != '') {
                            $("#huddle-desc-last").text($('#video_huddle_description').val());
                            $("#huddle-desc-last").css('width', '250px');
                            $("#desc-id").val($('#video_huddle_description').val());
                            $("#ajaxInputDesc").val($('#video_huddle_description').val());
                            $(".editAread").css("display", "none");
                        } else {
                            $(".editAread").css("display", "block");
                        }
                        $(".grpUsrs").css("display", "block");
                        $(".grpClass").css("display", "block");
                    }
                    if ($('#eval-huddle').is(':checked')) {
                        if ($('#video_huddle_name').val() != '') {
                            $("#huddle-name-last").text($('#video_huddle_name').val());
                            $("#huddle-name-last").css('width', '250px');
                            $("#name-id").val($('#video_huddle_name').val());
                            $("#ajaxInputName").val($('#video_huddle_name').val());
                            $(".editArea").css("display", "none");
                        } else {
                            $(".editArea").css("display", "block");
                        }
                        if ($('#video_huddle_description').val() != '') {
                            $("#huddle-desc-last").text($('#video_huddle_description').val());
                            $("#huddle-desc-last").css('width', '250px');
                            $("#desc-id").val($('#video_huddle_description').val());
                            $("#ajaxInputDesc").val($('#video_huddle_description').val());
                            $(".editAread").css("display", "none");
                        } else {
                            $(".editAread").css("display", "block");
                        }
                        $('#eval_required_deadline').css('display', 'block');
                        $('#eval-submission-date').val($('#date').val());
                        $('#eval-submission-time').val($('#time').val());
                        $(".grpUsrs").css("display", "block");
                        $(".grpClass").css("display", "block");
                    }

                    if ($('#coach-huddle').is(':checked')) {

                        $(".editArea").css("display", "none");
                        $(".editAread").css("display", "none");
                        $(".grpUsrs").css("display", "none");
                        $(".grpClass").css("display", "none");
                    }

                });
                $(document).on('click', '.step3', function () {
                    if ($('#coach-huddle').is(':checked')) {
                        var h_type = 2
                    } else if ($('#eval-huddle').is(':checked')) {
                        var h_type = 3
                    } else {
                        var h_type = 1
                    }

<?php
if ($isCreator):
    ?>
                        var participants = ['<?php echo $users['User']['id']; ?>||200||' + h_type];
<?php else: ?>
                        var participants = [];
<?php endif; ?>
                    var mentor_name = '';
                    var huddle_type = 1;
                    $(".editArea").css("display", "none");
                    $(".editAread").css("display", "none");
                    $('#HudTitle').css("display", "block");
                    $('#HudDesc').css("display", "block");
                    var coachee_count = 0;
                    $('input[name="super_admin_ids[]"]:checked').each(function () {
                        var leave_user = 0;
                        if ($('#coach-huddle').is(':checked')) {
                            $("#list-containers li").each(function (value) {
                                if ($(this).find('label [type="checkbox"]').is(":checked")) {
                                    if ($(this).find("span .chk_is_coachee").is(':checked')) {
                                        $("#chk_is_coachee").val("1");
                                    }
                                }
                            });
                            if ($('input[name=is_coach_' + this.value + ']').is(':checked')) {
                                var $radios = $('input:radio[name=user_role_' + this.value + ']');
                                $radios.filter('[value=200]').prop('checked', true);
                            } else {
                                if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                                    var $radios = $('input:radio[name=user_role_' + this.value + ']');
                                    $radios.filter('[value=210]').prop('checked', true);
                                    coachee_count++;
                                }
                            }

                        } else if ($('#eval-huddle').is(':checked')) {
                            console.log("assessment huddle");
                            $("#list-containers li").each(function (value) {
                                if ($(this).find('label [type="checkbox"]').is(":checked")) {
                                    if ($(this).find("span .chk_is_coachee").is(':checked')) {
                                        $("#chk_is_coachee").val("1");
                                    }
                                }
                            });
                            if ($('input[name=is_coach_' + this.value + ']').is(':checked')) {
                                var $radios = $('input:radio[name=user_role_' + this.value + ']');
                                $radios.filter('[value=200]').prop('checked', true);
                            } else {
                                if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                                    var $radios = $('input:radio[name=user_role_' + this.value + ']');
                                    $radios.filter('[value=210]').prop('checked', true);
                                    coachee_count++;
                                }
                            }
                        }

                        var role = $('input[name=user_role_' + this.value + ']:checked').val();

                        if ($("#coach-huddle").is(':checked') && ($('input[name=is_coach_' + this.value + ']').is(':checked') || $('input[name=is_mentor_' + this.value + ']').is(':checked'))) {
                            role = role + '||2';
                        } else if ($("#eval-huddle").is(':checked') && ($('input[name=is_coach_' + this.value + ']').is(':checked') || $('input[name=is_mentor_' + this.value + ']').is(':checked'))) {

                            role = role + '||3';
                        } else {
                            if ($("#collab-huddle").is(':checked')) {
                                role = role + '||1';
                            }
                            else {
                                role = role + '||2';
                            }
                        }
                        if (this.value < 0) {
                            role = role + '||' + $("#super_admin_fullname_" + this.value).val();
                        }

                        if (leave_user == 0) {
                            participants.push(this.value + '||' + role);
                            leave_user = 0;
                        }
                        if ($('input[name="is_mentor_' + this.value + '"]').is(':checked')) {
                            if (mentor_name == '')
                                mentor_name = $('#lblsuper_admin_ids_' + this.value).html();
                        }
                    });
                    if (h_type == 2 && coachee_count == 0) {
                        alert('You must select a coachee to be coached. Please select the Huddle participant to add a coachee.');
                        return false;
                    }
                    if (h_type == 3 && coachee_count == 0) {
                        alert('You must select a assessed participant to be assessed. Please select the Huddle participant to add a assessed participant.');
                        return;
                    }
                    if (h_type == 2 && coachee_count > 1) {
                        alert('You can only select one Coachee. If you want this to be your Coachee, please unselect the Coachee you have already selected.');
                        $("#tabs").tabs({disabled: [2]});
                        return false;
                    }
                    else {
                        $("#tabs").tabs("enable", 2);
                    }

                    $('.step3 a').attr('href', '#step-3');
                    $('.thirdone').addClass('ui-tabs-active');
                    $('.thirdone').addClass('ui-state-active');
                    $('.secondone').removeClass('ui-tabs-active');
                    $('.secondone').removeClass('ui-state-active');
                    $('.firstone').removeClass('ui-tabs-active');
                    $('.firstone').removeClass('ui-state-active');
                    $("#tabs").tabs("option", "active", 2);

                    $("#step-1").css("display", "none");
                    $("#step-2").css("display", "none");
                    $("#step-3").css("display", "block");
                    $('input[name="group_ids[]"]:checked').each(function () {

                        if ($('#coach-huddle').is(':checked')) {

                            if ($('input[name=is_coach_' + this.value + ']').is(':checked')) {
                                var $radios = $('input:radio[name=group_role_' + this.value + ']');
                                $radios.filter('[value=200]').prop('checked', true);
                            } else {
                                if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                                    var $radios = $('input:radio[name=group_role_' + this.value + ']');
                                    $radios.filter('[value=210]').prop('checked', true);
                                }
                            }
                        }
                        if ($('#eval-huddle').is(':checked')) {

                            if ($('input[name=is_coach_' + this.value + ']').is(':checked')) {
                                var $radios = $('input:radio[name=group_role_' + this.value + ']');
                                $radios.filter('[value=200]').prop('checked', true);
                            } else {
                                if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                                    var $radios = $('input:radio[name=group_role_' + this.value + ']');
                                    $radios.filter('[value=210]').prop('checked', true);
                                }
                            }
                        }
                        var role = $('input[name=group_role_' + this.value + ']:checked').val();
                        if ($("#coach-huddle").is(':checked') && ($('input[name=is_coach_' + this.value + ']').is(':checked') || $('input[name=is_mentor_' + this.value + ']').is(':checked'))) {
                            role = role + '||2';
                        } else if ($("#eval-huddle").is(':checked') && ($('input[name=is_coach_' + this.value + ']').is(':checked') || $('input[name=is_mentor_' + this.value + ']').is(':checked'))) {
                            role = role + '||3';
                        } else {
                            role = role + '||1';
                        }

                        participants.push(this.value + '||' + role);
                        if ($('input[name="is_mentor_' + this.value + '"]').is(':checked')) {
                            if (mentor_name == '')
                                mentor_name = $('#lblsuper_admin_ids_' + this.value).html();

                        }
                    });

                    if ($('#coach-huddle').is(':checked')) {
                        huddle_type = 2;
                        if (mentor_name == '')
                            mentor_name = $("#HudTitle").text().trim();
                        console.log(mentor_name);
                        //console.log(mentor_name);
                        $("#huddle-name-last").text(mentor_name);
                        $("#name-id").val(mentor_name);
                        $("#ajaxInputName").val(mentor_name);
                        $("#huddle-desc-last").val($('#video_huddle_description').val());
                        $("#desc-id").val(' ');
                        $("#ajaxInputDesc").val($('#video_huddle_description').val());
                    }
                    if ($('#eval-huddle').is(':checked')) {
                        huddle_type = 3;
                        if (mentor_name == '')
                            mentor_name = $("#HudTitle").text().trim();

                    }
                    $.ajax({
                        type: 'POST',
                        data: {
                            participants: participants,
                            type: huddle_type,
                        },
                        url: home_url + '/Huddles/get_people_ajax/',
                        success: function (res) {
                            $('#participants').html(res);
                        }
                    });

                });
            });
            $(function () {

                $('.coach_mentee').css('display', 'none');
                $('.grpClass').css('display', 'block');
                $("#tabs").tabs();
                $(document).on("change", '.huddle_edit_cls [type="checkbox"]', function () {
                    if ($(this).is(":checked")) {
                        if ($('#coach-huddle').is(':checked')) {
                            coachee_check_count = 0;
                            $('.huddle_edit_cls [type="checkbox"]:checked').each(function () {
                                var new_id = this.id;
                                var arr = new_id.split('_');
                                if ($('input[name=is_mentor_' + arr[2] + ']').is(':checked')) {
                                    var $radios = $('input:radio[name=user_role_' + arr[2] + ']');
                                    $radios.filter('[value=210]').prop('checked', true);
                                    coachee_check_count++;
                                }
                            });
                            if (coachee_check_count > 1) {
                                $(this).parent().parent().find('label [type="checkbox"]').prop("checked", false);
                                $(this).prop("checked", false);
                                alert('You can only select one Coachee. If you want this to be your Coachee, please unselect the Coachee you have already selected.');
                            } else {
                                $(this).parent().parent().find('label [type="checkbox"]').prop("checked", true);

                                $("#huddle-name-last").text($(this).parent().parent().find('label a').text());
                                $("#name-id").val($(this).parent().parent().find('label a').text());
                            }
                        } else {
                            $(this).parent().parent().find('label [type="checkbox"]').prop("checked", true);
                        }
                    }
                    else {
                        $(this).parent().parent().find('label [type="checkbox"]').prop("checked", false);
                    }
                });

                $("#coaching_trig_2").click(function () {
                    $(".cls_coaching_trig_2").trigger("click");
                });
                $("#cls_coaching_trig").click(function () {
                    $(".cls_coaching_trig").trigger("click");
                });
                $(document).on("change", '.coach_mentee [type="radio"]', function () {
                    var radio_val = $(this).val();
                    var radio_name = $(this).attr('name');
                    var get_radio_id = radio_name.split("_");
                    var radio_id = get_radio_id[1];
                    var html = '';
                    $(this).parent().parent().find('[type="checkbox"]').remove();
                    if (radio_val == "1") {
                        html = '<input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_' + radio_id + '" name="is_coach_' + radio_id + '" style="display:none" checked="checked" />';
                        $(this).parent().parent().append(html);
                    }
                    else {
                        html = '<input type="checkbox" value="1" id="is_mentor_' + radio_id + '" name="is_mentor_' + radio_id + '" style="display:none" checked="checked" />';
                        $(this).parent().parent().append(html);
                    }

                });
            });
            $(document).on("click", '#ui-id-1', function () {
                $(".appendix-content").hide();
            });

            $(document).on('click', '.admin-btn-v', function (e) {
                alert("Viewers are unable to participate as Admin or Members in Collaboration Huddles.");
                return false;
            });

        </script>
        <style>
            input[type="search"]{
                border: 1px solid #c5c5c5;
                border-radius: 3px;
                box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;
                font-size: 13px;
                font-weight: bold;
                color: #222;
                padding: 8px 4px;
            }
            .scrolable_box {
                height:200px;
                overflow-y:auto;
                padding-left: 12px;
            }
        </style>
        <?php $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $huddles['AccountFolder']['created_by']); ?>

        <form method="post" id="new_video_huddle" enctype="multipart/form-data" class="new_video_huddle"
              action="<?php echo $this->base . '/Huddles/edit/' . $huddle_id; ?>" accept-charset="UTF-8">
            <div id="tabs">
                <ul class="wizard-steps">
                    <li class="firstone"><a href="#step-1"><span>1</span> Huddle Types</a></li>
                    <li class="secondone step2"><a href="#step-2"><span>2</span> Participants</a></li>
                    <li class="thirdone step3"><a href="#step-3"><span>3</span> Review</a></li>
                    <div style="clear: both;"></div>
                </ul>
                <div class="wiz-container">
                    <div id="step-1">
                        <div style="margin:0;padding:0;display:inline"><input type="hidden" value="âœ“" name="utf8"></div>

                        <a class="close-reveal-modal"></a>
                        <div class="span5">
                            <div class="row"  style="margin-bottom: 20px;">
                                <?php if ($isCreator || $user_permissions['UserAccount']['manage_coach_huddles'] == '1' || $huddle_permission == 200): ?>
                                    <?php if ($huddle_type == '2'): ?>
                                        <span class="wiz-step1-radio"><input type="radio" id="coach-huddle" class="cls_coaching_trig" <?php
                                            if ($huddle_type == '2') {
                                                echo 'checked="checked"';
                                            }
                                            ?> name="type" value="2"><label  id="cls_coaching_trig"> Coaching / Mentoring Huddle</label></span>
                                        <a onMouseOut="hide_sidebar()" class="appendix appendix2" href="#">?</a>
                                        <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                            <p>Coaching/Mentoring Huddles enhance and personalize 1:1 coaching and mentoring.  These Huddles are private and secure and help establish trust between professionals.</p>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>

                                <div style="clear: both;height:10px;"></div>
                                <?php if ($isCreator || $user_permissions['UserAccount']['manage_collab_huddles'] == '1' || $huddle_permission == 200): ?>
                                    <?php if ($huddle_type == '1'): ?>
                                        <span class="wiz-step1-radio"><input type="radio" id="collab-huddle" name="type" class="cls_coaching_trig_2" <?php
                                            if ($huddle_type == '1') {
                                                echo 'checked="checked"';
                                            }
                                            ?> value="1"><label id="coaching_trig_2"> Collaboration Huddle</label></span>
                                        <a onMouseOut="hide_sidebar()" class="appendix appendix2" href="#">?</a>
                                        <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                            <p>A Collaboration Huddle is a great way to conduct blended professional development workshops or trainings, professional learning communities, peer observations, or any other learning environment where videos and related resources can be shared, annotated, and discussed.</p>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if ($isCreator || $huddle_permission == 200): ?>
                                    <?php if ($huddle_type == '3'): ?>
                                        <span class="wiz-step1-radio"><input type="radio" id="eval-huddle" name="type" class="cls_coaching_trig_2" <?php
                                            if ($huddle_type == '3') {
                                                echo 'checked="checked"';
                                            }
                                            ?> value="3"><label id="coaching_trig_2"> Assessment Huddle</label></span>
                                        <a onMouseOut="hide_sidebar()" class="appendix appendix2" href="#">?</a>
                                        <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                            <p>Assessment Huddles are designed specifically for not only giving feedback to performance task, but also adding customizable ratings in reference to standards or competencies on a rubric or framework.</p>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <div style="clear: both;height:10px;"></div>
                            </div>
                            <?php if ($huddle_type != '2' && $huddle_type != '3') { ?>
                                <div class="row" style="margin-bottom: 20px;">
                                    <input name="data[name]" id="video_huddle_name" class="size-big huddle-name "  placeholder="Huddle Name" size="30" type="text" <?php
                                    if ($huddle_type == '2') {
                                        echo 'disabled';
                                    }
                                    ?> value="<?php echo $huddles['AccountFolder']['name']; ?>"  required/>
                                    <label for="video_huddle_name" class="error" style="display:none;background-color: none !important;">This field is required.</label>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="row">
                                    <textarea rows="4" placeholder="Huddle Description (Optional)" id="video_huddle_description"  cols="40" class="size-big" name="description" <?php
                                    if ($huddle_type == '2') {
                                        echo 'disabled';
                                    }
                                    ?>><?php echo $huddles['AccountFolder']['desc']; ?></textarea>
                                </div>
                            <?php } elseif ($huddle_type == '3') { ?>
                                <div class="row" style="margin-bottom: 20px;">
                                    <input name="data[name]" id="video_huddle_name" class="size-big huddle-name " placeholder="Huddle Name(e.g. Formal Assessment/Assignment/Exam)" size="30" type="text"
                                    <?php
                                    if ($huddle_type == '3') {
                                        echo '';
                                    }
                                    ?> value="<?php echo $huddles['AccountFolder']['name']; ?>"  required/>
                                    <label for="video_huddle_name" class="error" style="display:none;background-color: none !important;">This field is required.</label>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="row">
                                    <textarea rows="4" placeholder="Huddle Description (Optional)" id="video_huddle_description"  cols="40" class="size-big" name="description" <?php
                                    if ($huddle_type == '3') {
                                        echo '';
                                    }
                                    ?>><?php echo $huddles['AccountFolder']['desc']; ?></textarea>
                                </div>

                                <?php if ($huddle_type == '3'): ?>
                                    <div id="submission_allowed" class="row" style="margin-top: 15px;">
                                        <label>Video submissions allowed per assessed participants</label>
                                        <select name="submission_allowed" id="video-submissin-allowed" style="width: 200px;">
                                            <option value="1" <?php
                                            if (empty($submission_allowed) || $submission_allowed == 1): echo 'selected="selected"';
                                            endif;
                                            ?> >1 </option>
                                            <option value="2" <?php echo $submission_allowed == 2 ? 'selected="selected"' : '' ?>> 2 </option>
                                            <option value="3" <?php echo $submission_allowed == 3 ? 'selected="selected"' : '' ?>> 3 </option>
                                            <option value="4" <?php echo $submission_allowed == 4 ? 'selected="selected"' : '' ?>> 4 </option>
                                            <option value="5" <?php echo $submission_allowed == 5 ? 'selected="selected"' : '' ?>> 5 </option>
                                        </select>
                                        <br/><br/>
                                    </div>
                                    <div id="eval_submission_deadline" class="row" style="display:block;">
                                        <label class="" style="display: block;">Submission Deadline:</label>
                                        <input type="text" id="date" value="<?php echo $submission_deadline_date; ?>"  class="size-small"  placeholder="Date" required>
                                        <input type="text" id="time"  value="<?php echo $submission_deadline_time; ?>"   class="size-small"  placeholder="Time" required>
                                    </div>
                                    <script type="text/javascript">
                                        function select_all_evaluator(radio_val) {

                                            $('.coach_evaluation [type="radio"]').each(function () {

                                                var radio_name = $(this).attr('name');
                                                var get_radio_id = radio_name.split("_");
                                                var radio_id = get_radio_id[1];

                                                var html = '';

                                                if (radio_val == "1") {
                                                    $('#is_coach_' + radio_id).prop('checked', true);
                                                    $('#is_mentor_' + radio_id).prop('checked', false);

                                                }
                                                else {
                                                    $('#is_coach_' + radio_id).prop('checked', false);
                                                    $('#is_mentor_' + radio_id).prop('checked', true);

                                                }
                                            });
                                        }
                                        $(document).ready(function (e) {

                                            $("#all_evaluator").click(function () {

                                                if ($(this).is(':checked')) {
                                                    $(".participant-check").removeAttr("checked");
                                                    $("#all_particapant").removeAttr("checked");
                                                    $(".evaluator-check").prop('checked', true);
                                                    select_all_evaluator(1);
                                                } else {
                                                    $(".evaluator-check").removeAttr("checked");
                                                    $(".eval_coach_hidden").removeAttr("checked");

                                                }

                                            });
                                            $("#all_particapant").click(function () {
                                                if ($(this).is(':checked')) {
                                                    $(".evaluator-check").removeAttr("checked");
                                                    $("#all_evaluator").removeAttr("checked");
                                                    $(".participant-check").prop('checked', true);
                                                    select_all_evaluator(2);
                                                } else {
                                                    $(".participant-check").removeAttr("checked");
                                                    $(".eval_participant_hidden").removeAttr("checked");
                                                }

                                            });

                                            $('#date').datetimepicker({
                                                minDate: 0,
                                                timepicker: false,
                                                format: 'm-d-Y'
                                            });
                                            $('#date').attr('disabled', false);
                                            $('#time').attr('disabled', false);
                                            $('#time').datetimepicker({
                                                datepicker: false,
                                                format: 'g:i A',
                                                formatTime: 'g:i A',
                                                ampm: true
                                            });
                                            $('#eval-submission-date').datetimepicker({
                                                minDate: 0,
                                                timepicker: false,
                                                format: 'm-d-Y'
                                            });
                                            $('#eval-submission-time').datetimepicker({
                                                datepicker: false,
                                                format: 'g:i A',
                                                formatTime: 'g:i A',
                                                ampm: true
                                            });

                                        });

                                    </script>
                                <?php endif; ?>
                            <?php } ?>
                            <?php echo $this->Form->input('created_by', array('value' => $user_id, 'type' => 'hidden', 'id' => 'video_huddle_created_by')); ?>
                        </div>

                        <div class="row-fluid">
                        </div> <?php if ($huddle_type != '2') { ?><br/><br/><?php } ?>
                        <?php if ($this->Custom->is_enabled_framework_and_standards($account_id) && !$this->Custom->check_trial($account_id)): ?>
                            <input type="checkbox" id="chkenableframework" name="chkenableframework" value="1" <?php echo $chk_frameworks == '1' ? 'checked="checked"' : ''; ?>><label for="chkenableframework" style="padding-left: 7px;">Enable Framework/Rubric in this Huddle</label>
                            <br/><br/>
                            <input id="frameworkchngwarn" type="hidden" value="">
                            <?php if (!empty($frameworks) && count($frameworks) > 0): ?>
                                <div id="frmDiv">
                                    <select name="frameworks" id="frameworks">
                                        <option value="">Select Framework</option>
                                        <?php foreach ($frameworks as $framework) { ?>
                                            <option value="<?php echo $framework['AccountTag']['account_tag_id'] ?>" <?php if (isset($framework_id) && $framework['AccountTag']['account_tag_id'] == $framework_id) echo ' selected="selected"'; ?>><?php echo $framework['AccountTag']['tag_title'] ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                    <br/><br/>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if ($this->Custom->is_enable_tags($account_id) && !$this->Custom->check_trial($account_id)): ?>
                            <input type="checkbox" id="chkenabletags" name="chkenabletags" value="1" <?php echo $chk_tags == '1' ? 'checked="checked"' : ''; ?>><label for="chkenabletags" style="padding-left: 7px;">Enable custom Video Markers in this Huddle</label>
                        <?php endif; ?>
                        <br/><br/>
                        <?php if ($huddle_type == '2') { ?>  <input type="checkbox" id="chkcoacheepermissions" name="chkcoacheepermissions" value="1" <?php echo $coachee_permission == '1' ? 'checked="checked"' : ''; ?>><label for="chkcoacheepermissions" style="padding-left: 7px;">Enable the Coachee permission to download, copy, edit, and delete all videos in this Huddle.</label>
                            <br/><br/>
                            <input type="checkbox" id="coach_hud_feedback" name="coach_hud_feedback" value="1" <?php echo $coach_hud_feedback == '1' ? 'checked="checked"' : ''; ?>><label for="coach_hud_feedback" style="padding-left: 7px;">Publish comments for coachee after completing observation</label>


                        <?php } ?>
                        <div class="form-actions" style="text-align:left;">
                            <a href="#" class="btn btn-green step2" id="next-step2">Next</a>
                            <a class="btn btn-transparent" href="<?php echo $this->base . '/Huddles' ?>">Cancel</a>
                        </div>
                    </div>
                    <div id="step-2">
                        <h3 id="step2-label" style="float: left;margin-top: 5px;margin-bottom: 10px;font-size: 14px;">Invite other people in your account to participate with you in the huddle. You can always do this later. You're defaulted to be an Admin in the Huddle.</h3>

                        <div class="row-fluid">
                            <div class="groups-table span12">

                                <div class="span4 huddle-span4" style="margin-left:0px;">
                                    <div class="groups-table-header groups-table-header-create">
                                        <div style="width: 200px; float: left; margin-left: 8px;top: 10px;position: relative;padding-bottom: 18px;">
                                            <?php if ($user_permissions['UserAccount']['permission_maintain_folders'] == 1 && $user_permissions['UserAccount']['permission_administrator_user_new_role'] == 1): ?>
                                                <a style="margin-left: -8px;" id="pop-up-btn" data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="btn btn-small btn-green" href="#"><span class="plus">+</span>
                                                </a>
                                            <?php else: ?>
                                                <a style="margin-left: -8px;"><span class="">&nbsp;&nbsp;</span>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                        <div style="width: 248px; padding: 0px;float: left;margin-left: 426px;" class="search-box">
                                            <div  id="header-container" class="filterform">
                                            </div>
                                        </div>
                                        <?php if ($huddle_type == '2'): ?>
                                            <a onMouseOut="hide_sidebar()" class="appendix right" href="#" id="Coachee_info" style="display:none;">?</a>
                                            <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                                <h3>Info</h3>
                                                <p>Coach(es):  Can edit coaching Huddle, by adding or removing other coaches in the Huddle; upload/download/copy all videos and supporting resources; clip/edit videos; add/edit/delete all video annotations; enable/disable tags/account framework; create/participate/edit all discussions; delete the coaching Huddle.</p>
                                                <p>Coachee: Can upload/download/copy/delete videos and resources they add to the Huddle; clip/edit their own videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                            </div>
                                        <?php elseif ($huddle_type == '3'): ?>
                                            <a onMouseOut="hide_sidebar()" class="appendix right" href="#" id="evaluation_info" style="display:none;">?</a>
                                            <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                                <h3>Info</h3>
                                                <p>Assessor(s): Can add or remove Huddle participants in the Huddle; upload/download/copy all videos and supporting resources; edit videos; add/edit video comments; enable/disable custom video markers and framework/rubric; delete the assessment Huddle</p>
                                                <p>Assessed participant(s): Can upload/download/copy/delete videos and attachments they add to the Huddle; edit their own videos; reply to evaluator(s) feedback</p>
                                            </div>
                                        <?php else: ?>
                                            <a onMouseOut="hide_sidebar()" class="appendix right" href="#" id="Coaches_info" >?</a>
                                            <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                                <h3>Info</h3>
                                                <p>Admin: Can add/remove users; upload/download/copy/delete all videos and documents; clip all videos; add/edit/delete all video annotations and comments; create/participate/edit all Huddle discussions; delete Huddle.</p>
                                                <p>Members: Can upload/download/copy/delete videos and documents they add to the Huddle; clip their videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                                <p>Viewers: View videos and documents only.</p>
                                            </div>
                                        <?php endif; ?>


                                        <div style="clear:both"></div>
                                        <div id="searchdiv" class="filterform" style="top: -20px;position: relative;margin-bottom: -1px;">
                                            <fieldset>
                                                <input type="search" name="search" value="" id="id_search" placeholder='Search people or group' class="filterinput" style="margin-right: 48px;width: 238px; float: right; margin-top: -32px;">
                                                <div id='cancel-srch' style='width:10px;position:absolute; right:60px; margin-top:-27px;cursor:pointer;display:none;'>X</div>
                                            </fieldset>

                                        </div>
                                        <?php if (count($super_users) > 1 || (isset($huddleUsers) && count($huddleUsers) > 1) || (isset($users_groups) && count($users_groups) > 0)): ?>
                                            <?php
                                            $style = '';
                                            if ($huddle_type == 2) {
                                                $style = 'style="float: left;margin-left: 465px;min-width:162px;position: relative; top: 16px;"';
                                            } elseif ($huddle_type == 3) {
                                                $style = 'style="float: left;margin-left:438px;min-width:162px;position: relative; top: 5px;"';
                                            }
                                            ?>

                                            <div id="select-all-none-cnt" class="select-all-none selectnone"  <?php echo $style; ?>>
                                                <label id="select-all-none" for="select-all" style="padding-left:0px;"><input type="checkbox" name="select-all" id="select-all" style="display:inline-block;" /> <span id="select-all-label">Select All</span></label>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($huddle_type == 2): ?>
                                            <div id="select_all_coaches_panel" class="huddles-select-all-block coach_mentee">
                                                <span class="caoch-checkbox" style="display:block;" >
                                                    <input type="checkbox" id="caoch-checkbox" name="is_coach"> Coach</span>
                                                <span class="mentee-checkbox" style="display:none;" ><input type="checkbox" id="mentee-checkbox" name="is_mentor" >Coachee</span>
                                            </div>
                                        <?php elseif ($huddle_type == 3): ?>
                                            <div id="select_all_coaches_panel" class="huddles-select-all-block eval_coach_mentee">
                                                <span class="caoch-checkbox"  style="margin-right: 17px;display: block;float: left;    margin-left: 9px;">
                                                    <input type="checkbox"  id="all_evaluator" name="is_coach"> Assessor
                                                </span>
                                                <span class="mentee-checkbox participant-checkbox"  style="display:block;float: left;" >
                                                    <input type="checkbox" class="participant-check" id="all_particapant" name="is_mentor" >Assessed Participant
                                                </span>
                                            </div>
                                            <style>
                                                .huddles-select-all-block{
                                                    text-align: right;
                                                    margin: 5px 58px 0 0;
                                                    width: 267px;
                                                }
                                            </style>
                                        <?php else: ?>
                                            <div class="huddles-select-all-block coach_perms" style="margin-top: -9px;margin-right: 52px;">
                                                <span class="admin-radio" style="margin-right: 5px;"><input type="radio" id="admin-radio" name="select-all"> Admin</span>
                                                <span class="member-radio"><input type="radio" id="member-radio" name="select-all" style="margin-left: -8px;"> Member</span>
                                                <span class="viewer-radio" style="margin-right: 3px;margin-left: 4px;"><input type="radio" id="viewers-radio" name="select-all"> Viewer</span>
                                            </div>
                                        <?php endif; ?>
                                        <div style="clear:both"></div>
                                    </div>
                                    <div class="groups-table-content">
                                        <div class="scrolable_box">
                                            <div>
                                                <div style="top: 0px;">
                                                    <div  id="people-lists">
                                                        <ul id="list-containers">
                                                            <?php if ($huddle_type == 2): ?>
                                                                <input type="hidden" id="chk_is_coachee" value="1" name="chk_is_coachee"  />
                                                            <?php elseif ($huddle_type == 3): ?>
                                                                <input type="hidden" id="chk_is_coachee" value="1" name="chk_is_coachee"  />
                                                            <?php else: ?>
                                                                <input type="hidden" id="chk_is_coachee"  value="0" name="chk_is_coachee"  />
                                                            <?php endif; ?>
                                                            <?php
                                                            if (count($users_record) > 0):
                                                                $user_ids = array();
                                                                $role_admin = array();
                                                                $role_user = array();
                                                                $role_viewer = array();

                                                                foreach ($supperUsers as $supperUser) {

                                                                    $user_ids[] = $supperUser['AccountFolderUser']['user_id'];

                                                                    if ($supperUser['AccountFolderUser']['role_id'] == '200') {
                                                                        $role_admin[] = $supperUser['AccountFolderUser']['user_id'];
                                                                    }
                                                                    if ($supperUser['AccountFolderUser']['role_id'] == '210') {
                                                                        $role_user[] = $supperUser['AccountFolderUser']['user_id'];
                                                                    }
                                                                    if ($supperUser['AccountFolderUser']['role_id'] == '220') {
                                                                        $role_viewer[] = $supperUser['AccountFolderUser']['user_id'];
                                                                    }
                                                                }

                                                                foreach ($huddleUsers as $huddleUser) {

                                                                    $user_ids[] = $huddleUser['AccountFolderUser']['user_id'];

                                                                    if ($huddleUser['AccountFolderUser']['role_id'] == '200') {
                                                                        $role_admin[] = $huddleUser['AccountFolderUser']['user_id'];
                                                                    }
                                                                    if ($huddleUser['AccountFolderUser']['role_id'] == '210') {
                                                                        $role_user[] = $huddleUser['AccountFolderUser']['user_id'];
                                                                    }
                                                                    if ($huddleUser['AccountFolderUser']['role_id'] == '220') {
                                                                        $role_viewer[] = $huddleUser['AccountFolderUser']['user_id'];
                                                                    }
                                                                }

                                                                foreach ($huddleViewers as $huddleViewer) {
                                                                    $user_ids[] = $huddleViewer['AccountFolderUser']['user_id'];

                                                                    if ($huddleViewer['AccountFolderUser']['role_id'] == '200') {
                                                                        $role_admin[] = $huddleViewer['AccountFolderUser']['user_id'];
                                                                    }
                                                                    if ($huddleViewer['AccountFolderUser']['role_id'] == '210') {
                                                                        $role_user[] = $huddleViewer['AccountFolderUser']['user_id'];
                                                                    }
                                                                    if ($huddleViewer['AccountFolderUser']['role_id'] == '220') {
                                                                        $role_viewer[] = $huddleViewer['AccountFolderUser']['user_id'];
                                                                    }
                                                                }
                                                                ?>
                                                                <?php
                                                                if ($users_record):
                                                                    ?>
                                                                    <?php foreach ($users_record as $row): ?>
                                                                        <?php
                                                                        //  $display = '';
                                                                        $is_coach = '';
                                                                        $is_mentor = '';


                                                                        if ($row['is_coach'] == '1') {
                                                                            $is_coach = 'checked="checked"';
                                                                        } else {
                                                                            $is_mentor = 'checked="checked"';
                                                                        }

                                                                        if ($row['is_mentee'] == '1') {
                                                                            $is_mentor = 'checked="checked"';
                                                                        } /* else {
                                                                          $is_mentor = '';
                                                                          } */

                                                                        if (!in_array($row['id'], $role_admin) && !in_array($row['id'], $role_user) && !in_array($row['id'], $role_viewer)) {
                                                                            $role_user[] = $row['id'];
                                                                        }
                                                                        if ($huddles['AccountFolder']['created_by'] == $row['id']):
                                                                            ?>
                                                                            <li style="display:none;margin: 0;border: none;padding: 0;" >
                                                                                <div style="display:none;">
                                                                                    <label class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class='super-user' style="display:none;"  type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) ) ? 'checked="checked"' : '') ?>> <a id="lblsuper_admin_ids_<?php echo $row['id'] ?>" style="color: #757575; font-weight: normal;"> <?php echo $row['first_name'] . " " . $row['last_name'] ?></a></label>
                                                                                    <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">


                                                                                    <?php if ($huddle_type == 2): ?>
                                                                                        <div class="permissions coach_mentee">
                                                                                            <span class="caoch-checkbox" ><input type="radio" value="1" id="caoch-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" <?php echo $is_coach; ?>><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>" >Coach</label></span>
                                                                                            <span class="mentee-checkbox"><input value="2" type="radio" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" <?php echo $is_mentor; ?>  class="chk_is_coachee"> <span id="mentee-chk"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span></span>
                                                                                            <input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_<?php echo $row['id'] ?>" name="is_coach_<?php echo $row['id'] ?>" style="display:none" <?php
                                                                                            echo $is_coach;
                                                                                            ?> />
                                                                                            <input type="checkbox" value="1" id="is_mentor_<?php echo $row['id'] ?>" name="is_mentor_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_mentor; ?>  />
                                                                                        </div>
                                                                                        <div class="permissions coach_perms" style="display: none">
                                                                                            <label for="user_role_<?php echo $row['id'] ?>_200">
                                                                                                <input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                            </label>
                                                                                            <label for="user_role_<?php echo $row['id'] ?>_210">
                                                                                                <input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                            </label>
                                                                                            <label for="user_role_<?php echo $row['id'] ?>_220">
                                                                                                <input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                            </label>
                                                                                        </div>
                                                                                    <?php elseif ($huddle_type == 3): ?>

                                                                                        <div class="permissions coach_evaluation coach_mentee">
                                                                                            <span class="caoch-checkbox" ><input type="radio" value="1" id="caoch-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" <?php echo $is_coach; ?> class="evaluator-check"><label class="cls_sp_label " for="caoch-checkbox_<?php echo $row['id'] ?>" >Assessor</label></span>
                                                                                            <span class="mentee-checkbox"><input value="2" type="radio" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" <?php echo $is_mentor; ?>  class="chk_is_coachee participant-check"> <span id="mentee-chk"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Assessed Participant</label></span></span>
                                                                                            <input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_<?php echo $row['id'] ?>" name="is_coach_<?php echo $row['id'] ?>" style="display:none" <?php
                                                                                            echo $is_coach;
                                                                                            ?> />
                                                                                            <input type="checkbox" value="1" id="is_mentor_<?php echo $row['id'] ?>" name="is_mentor_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_mentor; ?>  />
                                                                                        </div>
                                                                                        <div class="permissions coach_perms" style="display: none">
                                                                                            <label for="user_role_<?php echo $row['id'] ?>_200">
                                                                                                <input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                            </label>
                                                                                            <label for="user_role_<?php echo $row['id'] ?>_210">
                                                                                                <input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                            </label>
                                                                                            <label for="user_role_<?php echo $row['id'] ?>_220">
                                                                                                <input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                            </label>
                                                                                        </div>
                                                                                    <?php else: ?>

                                                                                        <div class="permissions coach_perms" >
                                                                                            <label for="user_role_<?php echo $row['id'] ?>_200">
                                                                                                <input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                            </label>
                                                                                            <label for="user_role_<?php echo $row['id'] ?>_210">
                                                                                                <input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                            </label>
                                                                                            <label for="user_role_<?php echo $row['id'] ?>_220">
                                                                                                <input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                            </label>
                                                                                        </div>
                                                                                    <?php endif; ?>
                                                                                    <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="1" class="huddle_edit_cls_chkbx"  <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) ) ? 'checked="checked"' : '') ?> id="new_chkbox_<?php echo $row['id'] ?>"> <label for="new_chkbox_<?php echo $row['id'] ?>" class="cls_sp_label">Huddle Participant</label></span>
                                                                                </div>
                                                                            </li>
                                                                        <?php elseif ($row['is_user'] == 'admin'): ?>
                                                                            <li >
                                                                                <label class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class='super-user' style="display:none;"  type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) ) ? 'checked="checked"' : '') ?>> <a id="lblsuper_admin_ids_<?php echo $row['id'] ?>" style="color: #757575; font-weight: normal;"> <?php echo $row['first_name'] . " " . $row['last_name'] ?></a></label>
                                                                                <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">

                                                                                <?php if ($huddle_type == 2): ?>
                                                                                    <div class="permissions coach_mentee">
                                                                                        <span class="caoch-checkbox" ><input type="radio" value="1" id="caoch-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" <?php echo $is_coach; ?>><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>" >Coach</label></span>
                                                                                        <span class="mentee-checkbox"><input value="2" type="radio" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" <?php echo $is_mentor; ?>  class="chk_is_coachee "> <span id="mentee-chk"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span></span>
                                                                                        <input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_<?php echo $row['id'] ?>" name="is_coach_<?php echo $row['id'] ?>" style="display:none" <?php
                                                                                        echo $is_coach;
                                                                                        ?> />
                                                                                        <input type="checkbox" value="1" id="is_mentor_<?php echo $row['id'] ?>" name="is_mentor_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_mentor; ?>  />
                                                                                    </div>
                                                                                    <div class="permissions coach_perms" style="display: none">
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200">
                                                                                            <input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210">
                                                                                            <input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220">
                                                                                            <input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                        </label>
                                                                                    </div>
                                                                                <?php elseif ($huddle_type == 3): ?>
                                                                                    <div class="permissions coach_evaluation coach_mentee">
                                                                                        <span class="caoch-checkbox" ><input type="radio" value="1" id="caoch-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" <?php echo $is_coach; ?> class="evaluator-check"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>" >Assessor</label></span>
                                                                                        <span class="mentee-checkbox"><input value="2" type="radio" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" <?php echo $is_mentor; ?>  class="chk_is_coachee participant-check"> <span id="mentee-chk"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Assessed Participant</label></span></span>
                                                                                        <input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_<?php echo $row['id'] ?>" name="is_coach_<?php echo $row['id'] ?>" style="display:none" <?php
                                                                                        echo $is_coach;
                                                                                        ?> />
                                                                                        <input type="checkbox" value="1" id="is_mentor_<?php echo $row['id'] ?>" name="is_mentor_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_mentor; ?>  />
                                                                                    </div>
                                                                                    <div class="permissions coach_perms" style="display: none">
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200">
                                                                                            <input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210">
                                                                                            <input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220">
                                                                                            <input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                        </label>
                                                                                    </div>
                                                                                <?php else: ?>
                                                                                    <div class="permissions coach_perms">
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200">
                                                                                            <input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210">
                                                                                            <input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                        </label>
                                                                                    </div>
                                                                                <?php endif; ?>
                                                                                <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="1" class="huddle_edit_cls_chkbx"  <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) ) ? 'checked="checked"' : '') ?> id="new_chkbox_<?php echo $row['id'] ?>"> <label for="new_chkbox_<?php echo $row['id'] ?>" class="cls_sp_label">Huddle Participant</label></span>
                                                                            </li>

                                                                        <?php elseif ($row['is_user'] == 'member'): ?>
                                                                            <li>
                                                                                <label class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class='member-user' style="display:none;"  type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) == true) ? 'checked="checked"' : '') ?>> <a id="lblsuper_admin_ids_<?php echo $row['id'] ?>" style="color: #757575; font-weight: normal;"><?php echo $row['first_name'] . " " . $row['last_name'] ?></a></label>
                                                                                <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">

                                                                                <?php if ($huddle_type == 2): ?>
                                                                                    <div class="permissions coach_mentee">
                                                                                        <span class="caoch-checkbox" >
                                                                                            <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>" <?php echo $is_coach; ?>><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Coach</label></span>
                                                                                        <span class="mentee-checkbox"><input type="radio" value="2"   id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" <?php echo $is_mentor; ?>><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span>
                                                                                        <input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_<?php echo $row['id'] ?>" name="is_coach_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_coach; ?> />
                                                                                        <input type="checkbox" value="1" id="is_mentor_<?php echo $row['id'] ?>" name="is_mentor_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_mentor; ?>  />
                                                                                    </div>
                                                                                    <div class="permissions coach_perms" style="display: none">
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200">
                                                                                            <input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210">
                                                                                            <input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220">
                                                                                            <input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                        </label>
                                                                                    </div>
                                                                                <?php elseif ($huddle_type == 3): ?>
                                                                                    <div class="permissions coach_evaluation coach_mentee">
                                                                                        <span class="caoch-checkbox" >
                                                                                            <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>" <?php echo $is_coach; ?> class="evaluator-check"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Assessor</label></span>
                                                                                        <span class="mentee-checkbox"><input type="radio" value="2"   id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee participant-check" <?php echo $is_mentor; ?> ><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>" >Assessed Participant</label></span>
                                                                                        <input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_<?php echo $row['id'] ?>" name="is_coach_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_coach; ?> />
                                                                                        <input type="checkbox" value="1" id="is_mentor_<?php echo $row['id'] ?>" name="is_mentor_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_mentor; ?>  />
                                                                                    </div>
                                                                                    <div class="permissions coach_perms" style="display: none">
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200">
                                                                                            <input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210">
                                                                                            <input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220">
                                                                                            <input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                        </label>
                                                                                    </div>
                                                                                <?php else: ?>
                                                                                    <div class="permissions coach_perms">
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200"><input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                        </label>
                                                                                    </div>
                                                                                <?php endif; ?>
                                                                                <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="1"  class="huddle_edit_cls_chkbx" <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) == true) ? 'checked="checked"' : '') ?> id="new_chkbox_<?php echo $row['id'] ?>"><label for="new_chkbox_<?php echo $row['id'] ?>"  class="cls_sp_label">Huddle Participant</label></span>
                                                                            </li>
                                                                        <?php elseif ($row['is_user'] == 'viewer'): ?>
                                                                            <?php if ($huddle_type == 1): ?>
                                                                                <li>
                                                                                    <label class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class='member-user' style="display:none;"  type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) == true) ? 'checked="checked"' : '') ?>> <a id="lblsuper_admin_ids_<?php echo $row['id'] ?>" style="color: #757575; font-weight: normal;"><?php echo $row['first_name'] . " " . $row['last_name'] ?></a></label>
                                                                                    <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">
                                                                                    <div class="permissions coach_perms">
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200"><input style="margin-left: -20px;" class="admin-btn-v" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200"   value="200" />Admin
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="admin-btn-v" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210"  value="210"/>Member
                                                                                        </label>
                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn1" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" checked="checked" />Viewer
                                                                                        </label>
                                                                                    </div>

                                                                                    <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="1"  class="huddle_edit_cls_chkbx" <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) == true) ? 'checked="checked"' : '') ?> id="new_chkbox_<?php echo $row['id'] ?>"><label for="new_chkbox_<?php echo $row['id'] ?>"  class="cls_sp_label">Huddle Participant</label></span>
                                                                                </li>
                                                                            <?php endif; ?>
                                                                        <?php elseif ($row['is_user'] == 'group'): ?>
                                                                            <?php
                                                                            $group_permission_assigned = false;

                                                                            foreach ($groups as $group) {
                                                                                $grupIds[] = $group['AccountFolderGroup']['group_id'];

                                                                                if ($group['AccountFolderGroup']['role_id'] == '200') {
                                                                                    $role_admin[] = $group['AccountFolderGroup']['group_id'] . "_grp";
                                                                                    $group_permission_assigned = true;
                                                                                }
                                                                                if ($group['AccountFolderGroup']['role_id'] == '210') {
                                                                                    $role_user[] = $group['AccountFolderGroup']['group_id'] . "_grp";
                                                                                    $group_permission_assigned = true;
                                                                                }
                                                                                if ($group['AccountFolderGroup']['role_id'] == '220') {
                                                                                    $role_viewer[] = $group['AccountFolderGroup']['group_id'] . "_grp";
                                                                                    $group_permission_assigned = true;
                                                                                }
                                                                            }

                                                                            if ($group_permission_assigned == false) {

                                                                                $role_viewer[] = $row['id'] . "_grp";
                                                                            }
                                                                            ?>
                                                                            <?php if ($huddle_type != 3 && $huddle_type != 2): ?>
                                                                                <li style="margin: 0;border: none;padding: 0;">
                                                                                    <div class="grpClass" style="border-bottom: 1px solid #ededed;float: left;width: 100%;padding-bottom: 10px;padding-top: 6px;margin-bottom: 15px;">
                                                                                        <label class="huddle_permission_editor_row"><input class='viewer-user' type="checkbox" value="<?php echo $row['id']; ?>" name="group_ids[]" id="group_ids_" style="display: none;" <?php echo ((isset($grupIds) && is_array($grupIds) && count($grupIds) > 0 && in_array($row['id'], $grupIds) == true) ? 'checked="checked"' : '') ?>> <a id="lblsuper_admin_ids_<?php echo $row['id'] ?>" style="color: #757575; font-weight: normal;"><?php echo $row['name']; ?></a></label>

                                                                                        <div class="permissions coach_mentee">
                                                                                            <span class="caoch-checkbox">
                                                                                                <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>" <?php echo $is_coach; ?>><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Coach</label></span>
                                                                                            <span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" <?php echo $is_mentor; ?>><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span>
                                                                                            <input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_<?php echo $row['id'] ?>" name="is_coach_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_coach; ?> />
                                                                                            <input type="checkbox" value="1" id="is_mentor_<?php echo $row['id'] ?>" name="is_mentor_<?php echo $row['id'] ?>" style="display:none" <?php echo $is_mentor; ?>  />
                                                                                        </div>
                                                                                        <div class="permissions coach_perms" style="display: none;">
                                                                                            <label for="group_role_<?php echo $row['id'] ?>_200"><input style="margin-left: -20px;" class="admin-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'] . "_grp", $role_admin) == true)) ? 'checked="checked"' : '') ?>/>Admin
                                                                                            </label>
                                                                                            <label for="group_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'] . "_grp", $role_user) == true)) ? 'checked="checked"' : '') ?>/>Member
                                                                                            </label>
                                                                                            <label for="group_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'] . "_grp", $role_viewer) == true)) ? 'checked="checked"' : '') ?>/>Viewer
                                                                                            </label>
                                                                                        </div>
                                                                                        <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="1"  class="huddle_edit_cls_chkbx"  name="chk_huddle" <?php echo ((isset($grupIds) && is_array($grupIds) && count($grupIds) > 0 && in_array($row['id'], $grupIds) == true) ? 'checked="checked"' : '') ?> id="new_chkbox_<?php echo $row['id']; ?>" ><label for="new_chkbox_<?php echo $row['id']; ?>" class="cls_sp_label" >Huddle Participant</label></span>

                                                                                    </div>
                                                                                </li>
                                                                            <?php endif; ?>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
                                                            <?php else: ?>
                                                                <li>
                                                                    To Invite users into the account <a data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="" href="#"><span class="">click here</span></a>
                                                                </li>
                                                            <?php endif; ?>
                                                            <?php if ($huddle_type == 2): ?>
                                                                <li id="noresults">No people match this search. Please try again.</li>
                                                            <?php else: ?>
                                                                <li id="noresults">No people or groups match this search. Please try again.</li>
                                                            <?php endif; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div><br/><br/>
                        <div class="form-actions" style="text-align:left;">
                            <a href="#" class="btn btn-green step3" id="next-step3">Next</a>
                            <a class="btn btn-transparent" href="<?php echo $this->base . '/Huddles' ?>">Cancel</a>
                        </div>
                    </div>
                    <div id="step-3">
                        <div>
                            <label style="font-weight: bold;">Huddle Name:</label>
                            <div id="HudTitle" class="mmargin-top" name="click to edit title" style="font-weight: bold; width: 410px">
                                <h4 id="huddle-name-last" class="last-step-labels" style="cursor:pointer;"><?php echo $huddles['AccountFolder']['name']; ?></h4>
                            </div>
                            <div class="editArea">
                                <input id="ajaxInputName" type="text" name="name"  val="<?php echo $huddles['AccountFolder']['name']; ?>"  style="width: 250px;">
                                <input id="name-id" type="hidden"/>
                            </div>
                            <label style="font-weight: bold;">Description:</label>
                            <div id="HudDesc" class="mmargin-top" name="click to edit desc" style="font-weight: bold; width: 410px">
                                <h4 id="huddle-desc-last" class="last-step-labels" style="cursor:pointer;"><?php echo $huddles['AccountFolder']['desc']; ?></h4>
                            </div>
                            <div class="editAread">
                                <!--<input id="ajaxInputDesc" type="text" name="hdescription" style="width: 250px;">-->
                                <textarea rows="3" id="ajaxInputDesc" name="description" style="width: 250px;"><?php echo $huddles['AccountFolder']['desc']; ?></textarea>
                                <input id="desc-id" type="hidden"/>
                            </div>
                            <?php if ($huddle_type == '3'): ?>

                                <div id="eval_required_deadline" class="editAread1" style="display: block;">
                                    <label class="" style="display: block;">Submission Deadline:</label>
                                    <input type="text" name="submission_deadline_date" value="<?php echo $submission_deadline_date ?>" id="eval-submission-date" class="size-small" placeholder="Date" required="">
                                    <input type="text" name="submission_deadline_time" value="<?php echo $submission_deadline_time ?>" id="eval-submission-time" class="size-small" placeholder="Time" required="">
                                </div>
                            <?php endif; ?>
                        </div>
                        <div id='participants'>
                        </div>
                        <div class="input-group mmargin-top">
                            <div id="editor2-toolbar" class="editor-toolbar" style="display: none;">
                                <a data-wysihtml5-command="bold">bold</a>
                                <a data-wysihtml5-command="italic">italic</a>
                                <a data-wysihtml5-command="insertOrderedList">ol</a>
                                <a data-wysihtml5-command="insertUnorderedList">ul</a>
                                <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                            </div>
                            <?php
                            echo $this->Form->textarea('message', array('id' => 'editor-2', 'class' => 'editor-textarea', 'value' => (isset($huddle_message) ) ? $huddle_message['AccountFolderMetaData']['meta_data_value'] : '',
                                'style' => 'color: #696565 !important;width:470px;text-indent:5px;padding-right:0px;padding-left:0px;height:150px;',
                                'placeholder' => 'Message to User(s) participating in Huddle...(Optional)'));
                            ?>
                            <?php echo $this->Form->error('videoHuddle.message'); ?>
                        </div>
                        <div class="form-actions" style="text-align:left;">
                            <input type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="btnEditSaveHuddle" value="Edit Huddle" name="commit" onclick="
                                    beforeHuddleEdit();" class="btn btn-green"  >
                            <a class="btn btn-transparent" href="<?php echo $this->base . '/Huddles' ?>">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div id="addSuperAdminModal" class="modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="header">
                        <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/add-user.png'); ?>" /> New User</h4>
                        <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                    </div>
                    <form accept-charset="UTF-8" action="<?php echo $this->base . '/Huddles/addUsers' ?>" enctype="multipart/form-data" method="post" name="admin_form" id="inviteFrm" onsubmit="return false;" style="padding-top:0px;">
                        <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                        <div id="flashMessage2" class="message error" style="display:none;"></div>
                        <div class="way-form">
                            <h3>Add name and email</h3>
                            <ol class="autoadd autoadd-sfont fmargin-list-left">
                                <li>
                                    <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" required placeholder="Full Name" type="text" value="" /></label>
                                    <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" required placeholder="Email Address" type="email" value="" /></label>
                                    <a href="#" class="close">x</a>
                                </li>
                                <li>
                                    <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" placeholder="Full Name" type="text" value="" /></label>
                                    <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" placeholder="Email Address" type="email" value="" /></label>
                                    <a href="#" class="close">x</a>
                                </li>
                            </ol>
                            <input id="controller_source" name="controller_source" type="hidden" value="video_huddles" />
                            <input id="action_source" name="action" type="hidden" value="add" />
                            <input id="action_source" name="user_type" type="hidden" value="110" />
                            <button style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="btnAddToAccount_addHuddle" class="btn btn-green fmargin-left" type="button">Add to Account</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <script type="text/javascript">
            $(document).ready(function (e) {
                $('#frameworks').change(function () {
                    if ($('#frameworkchngwarn').val() !== '1')
                    {
                        alert('Warning!!! Changing framework will delete all your current framework Tags');
                    }
                    $('#frameworkchngwarn').val('1');
                });
                $('#chkenableframework').click(function () {
                    if ($("#chkenableframework").is(':checked') ? 1 : 0 === 1)
                    {
                        $('#frmDiv').show();
                    }
                    else {
                        $('#frmDiv').hide();
                    }
                });
                if ($("#chkenableframework").is(':checked') ? 1 : 0 === 0)
                {
                    $('#frmDiv').hide();
                }
                if ($("#chkenableframework").is(':checked') ? 1 : 0 === 1)
                {
                    $('#frmDiv').show();
                }

                $("#id_search").quicksearch("#list-containers li", {
                    noResults: '#noresults',
                    stripeRows: ['odd', 'even'],
                    loader: 'span.loading',
                    minValLength: 2
                });
                $('#cancel-srch').click(function (e) {
                    $("#searchdiv").triggerHandler("focus");
                    $("#id_search").val('').trigger('keyup');
                });
                $("#id_search").keyup(function () {
                    if ($('#id_search').val().length > 0)
                        $('#cancel-srch').css('display', 'block');
                    else
                        $('#cancel-srch').css('display', 'none');
                });

                $("#HudTitle").mouseover(function () {
                    $(this).css("backgroundColor", "#FAFABE")
                });
                $("#HudTitle").mouseout(function () {
                    $(this).css("backgroundColor", "#ffffff")
                });
                $("#HudTitle").click(function () {
                    if ($("#coach-huddle").is(':checked')) {
                        $("#ajaxInputName").focus();
                    } else {
                        $(this).css("display", "none");
                        $(".editArea").css("display", "block");
                        $("#ajaxInputName").val($(this).text().trim());
                        $("#ajaxInputName").focus();
                    }
                });

                $("#HudDesc").mouseover(function () {
                    $(this).css("backgroundColor", "#FAFABE")
                });
                $("#HudDesc").mouseout(function () {
                    $(this).css("backgroundColor", "#ffffff")
                });
                $("#HudDesc").click(function () {
                    //         if($('#collab-huddle').is(':checked')) {
                    $(this).css("display", "none");
                    $(".editAread").css("display", "block");
                    $("#ajaxInputDesc").val($(this).text().trim());
                    $("#ajaxInputDesc").focus();
                    //         }
                });

                $("#admin-radio").click(function () {
                    if ($(this).is(':checked')) {
                        $(".member-btn").removeAttr("checked");
                        $(".viewer-btn").removeAttr("checked");
                        $(".admin-btn").prop('checked', true);
                    }

                });
                $("#member-radio").click(function () {
                    if ($(this).is(':checked')) {
                        $(".admin-btn").removeAttr("checked");
                        $(".viewer-btn").removeAttr("checked");
                        $(".member-btn").prop('checked', true);

                    }
                });
                $("#viewers-radio").click(function () {
                    if ($(this).is(':checked')) {
                        $(".admin-btn").removeAttr("checked");
                        $(".member-btn").removeAttr("checked");
                        $(".viewer-btn").prop('checked', true);
                    }
                });

                var foo = <?php echo $huddle_type; ?>;

                if (foo == 2)
                {
                    $("#tabs").tabs("option", "active", 1);

                    setTimeout(function () {
                        $('#input-filter').trigger('change');
                    }, 200);

                    $('#HudTitle').css("display", "block");
                    $('#HudDesc').css("display", "block");
                    if ($('#collab-huddle').is(':checked')) {
                        if ($('#video_huddle_name').val() != '') {
                            $("#huddle-name-last").text($('#video_huddle_name').val());
                            $("#huddle-name-last").css('width', '250px');
                            $("#name-id").val($('#video_huddle_name').val());
                            $("#ajaxInputName").val($('#video_huddle_name').val());
                            $(".editArea").css("display", "none");
                        } else {
                            $(".editArea").css("display", "block");
                        }
                        if ($('#video_huddle_description').val() != '') {
                            $("#huddle-desc-last").text($('#video_huddle_description').val());
                            $("#huddle-desc-last").css('width', '250px');
                            $("#desc-id").val($('#video_huddle_description').val());
                            $("#ajaxInputDesc").val($('#video_huddle_description').val());
                            $(".editAread").css("display", "none");
                        } else {
                            $(".editAread").css("display", "block");
                        }
                        $(".grpUsrs").css("display", "block");
                        $(".grpClass").css("display", "block");
                    }
                    if ($('#coach-huddle').is(':checked')) {

                        $(".editArea").css("display", "none");
                        $(".editAread").css("display", "none");
                        $(".grpUsrs").css("display", "none");
                        $(".grpClass").css("display", "none");
                    }

                } else if (foo == 3) {
                    $("#tabs").tabs("option", "active", 0);

                    setTimeout(function () {
                        $('#input-filter').trigger('change');
                    }, 200);

                    $('#HudTitle').css("display", "block");
                    $('#HudDesc').css("display", "block");
                    if ($('#eval-huddle').is(':checked')) {
                        if ($('#video_huddle_name').val() != '') {
                            $("#huddle-name-last").text($('#video_huddle_name').val());
                            $("#huddle-name-last").css('width', '250px');
                            $("#name-id").val($('#video_huddle_name').val());
                            $("#ajaxInputName").val($('#video_huddle_name').val());
                            $(".editArea").css("display", "none");
                        } else {
                            $(".editArea").css("display", "block");
                        }
                        if ($('#video_huddle_description').val() != '') {
                            $("#huddle-desc-last").text($('#video_huddle_description').val());
                            $("#huddle-desc-last").css('width', '250px');
                            $("#desc-id").val($('#video_huddle_description').val());
                            $("#ajaxInputDesc").val($('#video_huddle_description').val());
                            $(".editAread").css("display", "none");
                        } else {
                            $(".editAread").css("display", "block");
                        }
                        $(".grpUsrs").css("display", "block");
                        $(".grpClass").css("display", "block");
                    }
                    if ($('#eval-huddle').is(':checked')) {
                        $(".editArea").css("display", "none");
                        $(".editAread").css("display", "none");
                        $(".grpUsrs").css("display", "none");
                        $(".grpClass").css("display", "none");
                    }
                }
                $("#wrapper").show();
            });

            var posted_data = [];
            var new_ids = 0;

            $(document).ready(function () {
                $('#btnAddToAccount_addHuddle').click(function () {
                    addToInviteList();
                });
                $('.close-reveal-modal').click(function (e) {
                    $("#inviteFrm").trigger("reset");
                });
                $('#pop-up-btn').click(function () {
                    $('.fmargin-left').attr('disabled', false);
                });

                $('#video_huddle_name').keypress(function (e) {
                    if (e.keyCode == 13) {
                        $(this).closest('form').find('textarea').eq(0).focus();
                        return false;
                    }
                    $(this).removeClass('error').next().hide();
                });

            });

            function beforeHuddleEdit() {


                $.each($(".huddle_edit_cls_chkbx"), function (index, value) {

                    if ($(this).prop('checked') == true) {

                    } else {

                        var unselected_id = $(this).attr('id');
                        var unselected_id_array = $(this).attr('id').split('_');
                        var unselected_user_id = unselected_id_array[2];

                        $('[name="super_admin_email_' + unselected_user_id + '"]').remove();
                        $('[name="coach_' + unselected_user_id + '"]').remove();
                        $('[name="is_mentor_' + unselected_user_id + '"]').remove();
                        $('[name="iuser_role_' + unselected_user_id + '"]').remove();
                    }

                });



                if ($("#ajaxInputName").val().trim().length > 0) {
                    var video_huddle_name = $("#ajaxInputName");
                } else {
                    if ($("#ajaxInputName").val().trim().length > 0) {
                        $("#ajaxInputName").val($("#name-id").val());
                        var video_huddle_name = $("#name-id");
                    } else {
                        var video_huddle_name = $("#ajaxInputName");
                    }
                }


                if (video_huddle_name.val().trim().length > 0) {

                    if ($("#coach-huddle").is(':checked')) {

                        $("#chk_is_coachee").val('0');
                        $("span.mentee-checkbox input[type=radio]").each(function () {

                            var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                            if (huddle_row_selector.length > 0 && huddle_row_selector.is(":checked") && $(this).is(":checked")) {
                                $("#chk_is_coachee").val('1');
                            }
                        });

                        var get_chk = $("#chk_is_coachee").val();
                        if (get_chk != '1') {
                            alert("You must select a participant to be coached before creating a coaching/mentoring Huddle.");
                            return false;
                        } else {
                            $('#new_video_huddle').submit();
                        }
                    }
                    else {
                        $('#new_video_huddle').submit();
                    }
                } else {
                    $("#ajaxInputName").addClass('error');
                    $('#btnSaveHuddle').attr('disabled', false);
                    $('#HudTitle').hide();
                    $('.editArea').css('display', 'block');


                }

                return false;
            }

            function is_email_exists(email) {
                for (var i = 0; i < posted_data.length; i++) {
                    var posted_user = posted_data[i];
                    if (posted_user[1] == email)
                        return true;
                }
                return false;
            }
            function ValidateEmail(mail)
            {
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
                {
                    return (true);
                }
                return (false);
            }
            function addToInviteList() {
                var lposted_data = [];
                var userNames = document.getElementsByName('users[][name]');
                var userEmails = document.getElementsByName('users[][email]');
                var i = 0;
                for (i = 0; i < userNames.length; i++) {
                    var userEmail = $(userEmails[i]);
                    if (is_email_exists(userEmail.val())) {
                        alert('A new user with this email is already added.');
                        return false;
                    }
                }

                for (i = 0; i < userNames.length; i++) {
                    var userName = $(userNames[i]);
                    var userEmail = $(userEmails[i]);
                    if (userName.val() != '' || userEmail.val() != '') {
                        if (userName.val() == '') {
                            alert('Please enter a valid Full Name.');
                            userName.focus();
                            return false;
                        }
                        if (userEmail.val() == '' || !ValidateEmail(userEmail.val())) {
                            alert('Please enter a valid Email.');
                            userEmail.focus();
                            return false;
                        }
                        if (is_email_exists(userEmail.val())) {
                            alert('A new user with this email is already added.');
                            return false;
                        }
                        var newUser = [];
                        newUser[newUser.length] = '';
                        newUser[newUser.length] = userName.val();
                        newUser[newUser.length] = userEmail.val();
                        lposted_data[lposted_data.length] = newUser;
                    }
                }

                if (lposted_data.length == 0) {
                    alert('Please enter atleast one Full name or email.');
                    return;
                }
                $("#inviteFrm").trigger("reset");
                $.ajax({
                    type: 'POST',
                    data: {
                        user_data: lposted_data,
                    },
                    url: '<?php echo $this->base; ?>/Huddles/verifyNewUsers',
                    success: function (response) {
                        if (response != '') {
                            $('#flashMessage2').css('display', 'block');
                            $('#flashMessage2').html(response);
                        } else {

                            $('#flashMessage2').css('display', 'none');

                            new_ids -= 1;

                            for (var i = 0; i < lposted_data.length; i++) {

                                var data = lposted_data[i];
                                var html = '';

                                html += '<li>';
                                html += '<label class="huddle_permission_editor_row" for="super_admin_ids_' + new_ids + '"><input checked="checked" type="checkbox" value="' + new_ids + '" name="super_admin_ids[]" id="super_admin_ids_' + new_ids + '" style="display:none;"><a id="lblsuper_admin_ids_' + new_ids + '" style="color: #757575; font-weight: normal;">' + data[1] + '</a></label>';
                                html += '<input type="hidden" value="' + data[1] + '" name="super_admin_fullname_' + new_ids + '" id="super_admin_fullname_' + new_ids + '">';
                                html += '<input type="hidden" value="' + data[2] + '" name="super_admin_email_' + new_ids + '" id="super_admin_email_' + new_ids + '">';
                                if ($('#coach-huddle').is(':checked')) {
                                    html += '<div class="permissions coach_mentee" style="float:right !important;">';
                                } else if ($('#eval-huddle').is(':checked')) {
                                    html += '<div class="permissions coach_mentee" style="float:right !important;">';
                                } else {
                                    html += '<div class="permissions coach_mentee" style="display:none;float:right !important;">';
                                }
                                html += '<span class="caoch-checkbox" style="margin-right: 5px;">';
                                if ($('#coach-huddle').is(':checked')) {
                                    html += '<input type="radio" id="caoch-checkbox_' + new_ids + '" value="1" name="coach_' + new_ids + '"><label for="caoch-checkbox_' + new_ids + '" class="cls_sp_label">Coach</label></span>';
                                }
                                else if ($('#eval-huddle').is(':checked'))
                                {
                                    html += '<input type="radio" id="caoch-checkbox_' + new_ids + '" value="1" name="coach_' + new_ids + '"><label for="caoch-checkbox_' + new_ids + '" class="cls_sp_label">Assessor</label></span>';
                                }

                                if ($('#coach-huddle').is(':checked')) {
                                    html += '<input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_' + new_ids + '" name="is_coach_' + new_ids + '" style="display:none">';
                                } else if ($('#eval-huddle').is(':checked')) {
                                    html += '<input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_' + new_ids + '" name="is_coach_' + new_ids + '" style="display:none">';
                                }

                                if ($('#coach-huddle').is(':checked')) {
                                    html += '<span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_' + new_ids + '" name="coach_' + new_ids + '" class="chk_is_coachee" checked="checked"><label for="mentee-checkbox_' + new_ids + '" class="cls_sp_label">Coachee</label></span>';
                                    $("#chk_is_coachee").val("1");

                                } else if ($('#eval-huddle').is(':checked')) {
                                    html += '<span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_' + new_ids + '" name="coach_' + new_ids + '" class="chk_is_coachee" checked="checked"><label for="mentee-checkbox_' + new_ids + '" class="cls_sp_label">Assessed Participant</label></span>';
                                    $("#chk_is_coachee").val("1");
                                } else {

                                    html += '<span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_' + new_ids + '" name="coach_' + new_ids + '" class="chk_is_coachee" ><label for="mentee-checkbox_' + new_ids + '" class="cls_sp_label">Coachee</label></span>';
                                }

                                if ($('#coach-huddle').is(':checked')) {
                                    html += '<input type="checkbox" value="1" class="coach_mentee_hidden" id="is_mentor_' + new_ids + '" name="is_mentor_' + new_ids + '" style="display:none" checked="checked">';
                                } else if ($('#eval-huddle').is(':checked')) {
                                    html += '<input type="checkbox" value="1" class="coach_mentee_hidden" id="is_mentor_' + new_ids + '" name="is_mentor_' + new_ids + '" style="display:none" checked="checked">';
                                }

                                html += '</div>';

                                if ($('#collab-huddle').is(':checked')) {
                                    html += '<div class="permissions coach_perms">';
                                } else {
                                    html += '<div class="permissions coach_perms" style="display:none;">';
                                }
                                html += '<label for="user_role_' + new_ids + '_200"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_200" value="200" class="admin-btn" style="margin-left: -20px;">Admin</label>';
                                html += '<label for="user_role_' + new_ids + '_210"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_210" value="210" class="member-btn" checked="checked">Member</label>';
                                html += '<label for="user_role_' + new_ids + '_220"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_220" value="220" class="viewer-btn">Viewer</label>';
                                html += '</div>';
                                html += '<span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" checked="checked" value="' + new_ids + '" id="new_checkbox_' + new_ids + '"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_' + new_ids + '">Huddle Participant</label></span>';
                                html += '</li>';
                                $('.groups-table .huddle-span4 .groups-table-content ul').prepend(html);
                                new_ids -= 1;
                                var newUser = [];
                                newUser[newUser.length] = data[0];
                                newUser[newUser.length] = data[1];
                                posted_data[posted_data.length] = newUser;
                            }

                            var $overview = $('.widget-scrollable.horizontal .overview');
                            $.each($overview, function () {

                                var width = $.map($(this).children(), function (child) {
                                    return $(child).outerWidth() +
                                            $(child).pixels('margin-left') + $(child).pixels('margin-right');
                                });
                                $(this).width($.sum(width));

                            });

                            $('.widget-scrollable').tinyscrollbar();
                            $('#addSuperAdminModal').modal('hide');


                        }
                    },
                    errors: function (response) {
                        alert(response.contents);
                    }

                });

            }


            function hide_sidebar() {
                $(".appendix-content").delay(500).fadeOut(500);
                // $("#collab_help").delay(500).fadeOut(500);


            }

        </script>

        <?php if ($huddle_type == "2") {
            ?>
            <script type="text/javascript">
                $(document).ready(function (e) {
                    $('#wiz-heading').text('Edit Coaching / Mentoring Huddle');
                    $('#step2-label').text('Coaching Huddle creator is defaulted to be a coach in the Huddle. You may add another coach to the huddle, but you must select a participant to be coached.');
                    $('#video_huddle_name').prop("disabled", true);
                    $('#video_huddle_description').prop("disabled", true);
                    $('#video_huddle_name').css("background-color", '#F0EEE3');
                    $('#video_huddle_description').css("background-color", '#F0EEE3');
                    $('.coach_mentee').css('display', 'block');
                    $('.grpClass').css('display', 'none');
                    $('.coach_perms').css('display', 'none');
                    $('.coach_mentee').css('float', 'none');
                    $('.eval_coach_mentee').css('float', 'none');
                    $('.coach_mentee').attr('style', 'float: right !important');
                    $('.eval_coach_mentee').attr('style', 'float: right !important');
                    $('#select_all_coaches_panel').css('display', 'none');
                    $('#select-all-none-cnt').css('display', 'none');
                    $('.groups-table-header').css('padding-bottom', '10px');
                    $("#Coaches_info").css("display", "none");
                    $("#Coachee_info").css("display", "block");
                    $("#wrapper").show();
                });
                $(function () {
                    $('.grpClass').css('display', 'none');
                });
            </script>
        <?php } elseif ($huddle_type == 3) {
            ?>
            <script type="text/javascript">
                $(document).ready(function (e) {
                    $('#wiz-heading').text('Edit Assessment Huddle');
                    $('#step2-label').text("You're defaulted to be the assessor in the Huddle. You may add another participant to help assess, but you must select at least one participant to be assessed.");
                    $('.coach_mentee').css('display', 'block');
                    $('.coach_mentee').css('float', 'none');
                    $('.eval_coach_mentee').css('float', 'none');
                    $('.coach_mentee').attr('style', 'float: right !important');
                    $('.eval_coach_mentee').attr('style', 'float: right !important');
                    $('#select-all-none-cnt').css('display', 'block');
                    $('#huddles-select-all-block').css('display', 'none');
                    $("#Coachee_info").css("display", "none;");
                    $("#Coaches_info").css("display", "none;");
                    $("#evaluation_info").css("display", "block;");

                });

            </script>

        <?php } else {
            ?>
            <script type="text/javascript">
                $(document).ready(function (e) {
                    $('#wiz-heading').text('Edit Collaboration Huddle');
                    $('#step2-label').text("Invite other people in your account to participate with you in the huddle. You can always do this later. You're defaulted to be an Admin in the Huddle.");
                    $('#video_huddle_name').prop("disabled", false);
                    $('#video_huddle_description').prop("disabled", false);
                    $('#video_huddle_name').css("background-color", 'white');
                    $('#video_huddle_description').css("background-color", 'white');
                    $('.coach_mentee').css('display', 'none');
                    $('.grpClass').css('display', 'block');
                    $('.coach_perms').css('display', 'block');
                    $('#select-all-none-cnt').css('display', 'block');
                    $('.groups-table-header').css('padding-bottom', '0px');
                    $("#Coaches_info").css("display", "block");
                    $("#Coachee_info").css("display", "none");

                });
                $(function () {
                    $('.grpClass').css('display', 'block');
                });


            </script>
        </div>
    <?php }
    ?>
    <script>

        $(document).ready(function (e) {
<?php if (isset($parent_folder_name)): ?>
                var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/huddles"><?php echo $breadcrumb_language_based_content['huddles_breadcrumb']; ?></a><?php echo $breadcrumb; ?><a href="/Folder/<?php echo addslashes($parent_folder_name['AccountFolder']['account_folder_id']); ?>"><?php echo addslashes($parent_folder_name['AccountFolder']['name']); ?> </a><span><?php echo addslashes($current_huddle_info['AccountFolder']['name']); ?></span></div>';

                $('.breadCrum').html(bread_crumb_data);

<?php else : ?>
                var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/huddles"><?php echo $breadcrumb_language_based_content['huddles_breadcrumb']; ?></a> <span><?php echo addslashes($current_huddle_info['AccountFolder']['name']); ?></span></div>';

                $('.breadCrum').html(bread_crumb_data);

<?php endif; ?>

        });

    </script>