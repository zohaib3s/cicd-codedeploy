<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$account_id = $user_current_account['accounts']['account_id'];
$user_id = $user_current_account['User']['id'];
?>
<style type="text/css">
    .observation_left{
        float:left;
        width:50%;
        padding:10px;
        box-sizing:border-box;
        border-right:solid 1px #dadada;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_left p{
        margin:0px;
        font-size:13px;
    }
    .observation_left h2{
        text-align:center;
        font-size:20px;
        font-weight:normal;
        margin: 12px 0px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_left h3{
        color:#707070;
        font-size:13px;
        text-align:center;
        font-weight:normal;
        margin:0px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_right{
        float:right;
        width:50%;
        padding:10px;
        box-sizing:border-box;
        margin-top: -20px;
    }
    .observation_btn_box{
        text-align:center;
        margin:15px 0px;
    }
    .observation_btn{
        width:235px;
        background:url(/img/observation_bg.png) repeat-x;
        font-weight:bold;
        font-size:18px;
        color:#fff;
        text-shadow:1px 1px 0px #205f15;
        border:solid 2px #2a8f17;
        border-radius:5px;
        outline:none;
        text-align:center;
        padding:15px 0px;
        cursor:pointer;
    }
    .timer_box{
        padding:0px 0px;
        text-align:center;
        color:#5daf46;
        font-weight:300 !important;
        font-size:4em ;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
    }
    .timer_box span{
        font-weight:300 !important;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
    }
    .stop_watch_sec{
        color:#707070;
        font-size:20px;
        font-weight:400;
    }
    .start_btn_box{
        text-align:center;
    }
    .start_time_btn{
        width:235px;
        background:url(/img/observation_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #8e631f;
        border:solid 1px #2a8f17;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .start_time_sml_btn{
        width:85px;
        background:url(/img/observation_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #8e631f;
        border:solid 1px #2a8f17;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .pause_time_btn{
        width:85px;
        background: url(/img/pause_btn_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #832920;
        border:solid 1px #f5ab35;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .stop_time_btn{
        width:85px;
        background: url(/img/stop_btn_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:1px 1px 0px #205f15;
        border:solid 1px #d23727;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .note_box{
        border:solid 1px #cccccc;
        margin:20px 0px;
    }
    .note_box textarea{
        border:0px;
        padding:10px;
        resize:none;
        outline:none;
        height:95px;
        color:#707070;
    }
    .tags_standard_box{
        border-top:solid 1px #cccccc;
        border-bottom:solid 1px #cccccc;
        padding:11px 15px;
        color:#898686;
        font-size:13px;
        height:42px;
        box-sizing:border-box;
    }
    .tags_box{
        height:42px;
        padding:9px 15px;
        box-sizing:border-box;
    }
    .tags_box img{
        vertical-align:middle;
    }
    .tags_box input[type="text"]{
        border:0px;
        outline:none;
    }
    .call_notes_data_box{
        margin:5px 0px;
    }
    .call_notes_data_body{
        font-size:13px;
        padding:10px;
        background:#f2f2f2;
        color:#454545;
    }
    .call_notes_data_header{
        padding:10px;
        color:#858585;
    }
    .call_notes_data_header img{
        vertical-align:middle;
    }
    .callNotes_actionBox{
        float:right;
        margin-top: 3px;
    }
    .optioncls a {
        width: 20px;
        height: 20px;
        border-radius: 50%;
        border: solid 2px #1297e0;
        display: inline-block;
        position: relative;
        text-align: center;
        line-height: 12px;
        text-align:center;
    }
    .call_notes_data_box .tags_standard_box{
        border-top:0px;
    }
    .call_notes_data_box .note_box{
        margin:0px;
    }
    .associate_video{
        width:482px;
        height:auto;
        background:#000;
        margin:20px auto;
        text-align:center;
        position: relative;
        min-height: 200px;
    }
    .associate_video .videos-list__item-thumb{
        width: 100%;
        margin: 0 auto;
        position: relative;
    }
    .associate_video .videos-list__item-thumb video{
        width: 95%;
    }
    .associate_video .videos-list__item-thumb .play-icon {
        top: 135px !important;
    }
    .associate_video img{
        margin-top:22px;
        cursor:pointer;
    }
    .video_upload_dialog{
        padding:40px;
        text-align:center;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        /*width:600px;*/
        padding-top: 20px;
    }
    .Associate_Video_btn{
        display:inline-block;
        margin:0px 10px;
        border:solid 1px #add_fromHuddle;
        padding:15px;
        color:#b4b1aa;
        border:solid 1px #cccccc;
    }
    .Associate_Video_btn a{
        color:#b4b1aa;
        text-decoration:none;
        font-weight: normal;
    }
    .Associate_Video_btn img{
        vertical-align:middle;
        position:relative;
        margin-right:5px;
    }
    .video_box_workSpace{
        padding:10px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        width:600px;
    }
    .video_box_workSpace img{
        vertical-align:middle;
        position:relative;
    }
    .workspace_header{
        color:#979797;
    }
    .back_btn_dialog{
        float:right;
        font-size:13px;
        margin-top:7px;
        cursor: pointer;
    }
    .find_video{
        width:100%;
        box-sizing:border-box;
        padding:10px;
        border:solid 1px #ececec;
        color:#b4b1aa;
        margin:6px 0px;
        -webkit-box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
        -moz-box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
        box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
    }
    .videos_list_box{
        height:270px;
        overflow-y:scroll;
    }
    .video_detail{
        padding:6px;
    }
    .video_detail_left{
        float:left;
        clear: both;
    }
    .video_detail_left img{
        vertical-align: top;
        width: 74px !important;
        height: 55px !important;
        min-width: 74px !important;
        min-height: 60px !important;
    }
    .video_detail_text{
        float:left;
        color:#616160;
        padding:0px 0px 0px 20px;
        font-size:14px;
    }
    .video_detail_text a{
        color:#5a80a0;
        text-decoration:none;
    }
    #observations-1 .observation_left{
        float: left;
        width: 100% !important;
        padding: 10px;
        box-sizing: border-box;
        border-right: solid 0px #dadada;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .videos-list__item {
        margin: 0 0 40px 30px !important;
    }
    .video_viewercls{
        width:auto;
    }
    .observation_mainContainer{
        margin: -10px -20px;
    }
    .coach_cochee_container{
        width: 100%;
        margin: 10px 0px;
    }
    .doted_container{
        border-radius:5px;
        border:dashed 1px #bfbfbf;
        padding: 10px;
        color: #959595;
        font-size: 15px;
        text-align:center;
        margin-bottom: 10px;
    }
    .timer_box2{
        background: #ed1c24;
        font-size: 9px;
        color: #fff;
        padding: 2px 5px;
        position: absolute;
        right: 0px;
        top: -18px;
    }
    .note_box{
        position: relative;
    }
    #tab-area .tab-content{
        padding-top:-10px;
        margin-top: -10px;
    }
    .standard-search{
        position: relative;
        float: none;
        margin: 0 auto;
        width: 390px;
    }
    .standard-search .text-input{
        width: 340px !important;
    }
    .observation_search_bg{
        width: 94% !important;
        background:  url(/img/observation_search_bg.jpg) no-repeat right;
    }
    .printable_icons{
        text-align: right;
        padding-right: 10px;
    }
    #observatons-added-notes{
        // position: relative;
    }
    .videos_list_box .videos-list__item-thumb{
        min-height: 56px;
        margin-left: 23px;
    }
    .videos_list_box .video-unpublished{
        overflow:hidden;
        position: relative;
    }
    .videos_list_box .video-unpublished img{
        min-height: 55px !important;
    }
    .videos_list_box .video-unpublished .play-icon {
        top: 74px !important;
        width: 25px;
        height: 25px;
        background-size: 25px;
        left: 48px;
    }
    #scrollbar1 {
        width: 100%;
    }
</style>
<div class="header header-huddle">
    <div class="action-buttons btn-group right mmargin-top">
        <a href="<?php echo $this->base . '/Huddles'; ?>" class="btn" style="">Back to all Huddles</a>
        <a href="<?php echo $this->base . '/Huddles/delete/' . $huddle_des['AccountFolder']['account_folder_id']; ?>"
           data-confirm="Are you sure you want to delete this Huddle?" data-method="delete" class="btn icon2-trash" rel="tooltip" title="delete"></a>
    </div>
    <h2 class="title" style="width: 445px;"><?php echo $huddle_des['AccountFolder']['name']; ?></h2>
    <p class="title-desc" style="width: 720px;"><?php echo $huddle_des['AccountFolder']['desc']; ?></p>
</div>


<div class="tab-box" style="overflow: visible;">
    <div id="tab-area">

        <div class="table-container observation_mainContainer">
<?php
echo $details_html;
?>
            <div class="clear"></div>
        </div>
        <input id="txtUploadedFilePath" type="hidden" value="" />
        <input id="huddle_id" type="hidden" value="<?php echo $huddle_id; ?>" />
        <input id="video_id" type="hidden" value="<?php echo $video_id; ?>" />
        <input id="txtUploadedFileName" type="hidden" value="" />
        <input id="txtUploadedFileMimeType" type="hidden" value="" />
        <input id="txtUploadedFileSize" type="hidden" value="" />
        <input id="txtUploadedDocType" type="hidden" value="" />
        <input id="uploadPath" type="hidden" value="<?php echo "/tempupload/$account_id/" . date('Y/m/d/'); ?>" />
        <input id="account_id" type="hidden" value="<?php echo $account_id; ?>" />
        <input id="user_id" type="hidden" value="<?php echo $user_id; ?>" />
    </div>
</div>
</div>
<script>

    $(document).ready(function () {


        $(document).on('change', '.select-radio', function () {
            $('.select-radio').not(this).prop('checked', false);
        });


        function load_huddles_videos() {
            $.ajax({
                type: "POST",
                url: home_url + "/huddles/get_huddle_document",
                dataType: 'json',
                data: {
                    huddle_id: $('#huddle_id').val(),
                    video_id: $('#video_id').val()
                },
                success: function (response) {
                    $('#observations-7').html(response.model);
                    $('.pause_time_btn').hide();
                    $('#sw_start').show();
                    $('#observations-2').slideUp();
                    $('#observations-7').slideDown();
                    $('#stop-call-notes').html($('#observation-comments').html());
                    $('#ObservationTitle1').text($('#ObservationTitle').text());
                    $('.printable_icons').remove();
                },
                error: function (e) {

                }
            });
        }

        $('#observations-comments').keypress(function (e) {
            $hours = $('#sw_h').text();
            $min = $('#sw_m').text();
            $sec = $('#sw_s').text();
            $time = $hours + ':' + $min + ':' + $sec;
            if ($('.timer_box2').text() == '00:00:00') {
                $('.timer_box2').text($time);
                $('.timer_box2').show();

            }
            $(this).css('border', '0');
            $(this).css('border-radius', '0');
        });
        var fieldWidth = parseInt($("#ObservationTitle").css("width"));
        $("#ajaxInput").css("width", (fieldWidth - 50) + "px");
        $(".editArea").css("width", (fieldWidth) + "px");
        $("#ObservationTitle").mouseover(function () {
            $(this).css("backgroundColor", "#FAFABE")
        });
        $("#ObservationTitle").mouseout(function () {
            $(this).css("backgroundColor", "#ffffff");
        });
        $("#ObservationTitle").click(function () {
            $(this).css("display", "none");
            $(".editArea").css("display", "inline-block");
            $("#ajaxInput").val($(this).text().trim());
            $("#ajaxInput").focus();
        });
        $("#ajaxInput").blur(function () {
            $(".editArea").css("display", "none");
            $("#ObservationTitle").css("display", "inline-block");
            $("#ObservationTitle").text($(this).val());
        });
        $(document).on('click', '#select-from-workspace', function (e) {
            e.preventDefault();
            $('.video_upload_dialog').slideUp();
            $('.select-from-workspace').slideDown();
            $('.select-from-huddle').slideUp();
            $('#add_video').show();
        });
        $(document).on('click', '#select-from-huddle', function (e) {
            e.preventDefault();
            $('.video_upload_dialog').slideUp();
            $('.select-from-huddle').slideDown();
            $('.select-from-workspace').slideUp();
            $('#add_video').show();
        });
        $(document).on('click', '#back-to-select-container', function (e) {
            e.preventDefault();
            $('.select-from-workspace').slideUp();
            $('.select-from-huddle').slideUp();
            $('#add_video').hide();
            $('.video_upload_dialog').slideDown();
        });
        $(document).on('click', '.select-video-btn', function (e) {
            $('#movefolderto').modal('hide');
            $('#name-container').hide();
            $('#publish-button-container').show();
        });
        $('.pause_time_btn').hide();
        $('.stop_time_btn').hide();
        function create_empty_record() {
            $.ajax({
                type: "POST",
                url: home_url + "/Huddles/add_call_notes",
                dataType: 'json',
                data: {
                    observation_title: $('#ObservationTitle').text(),
                    observations_comments: $('#observations-comments').val(),
                    observations_standards: $('#txtVideostandard1').val(),
                    observations_tags: $('#txtVideoTags1').val(),
                    observations_time: $('.timer_box2').text(),
                    account_id: $('#account_id').val(),
                    user_id: $('#user_id').val(),
                    huddle_id: $('#huddle_id').val(),
                    video_id: ''
                },
                success: function (response) {
                    $("ul#expList1").find('input:checkbox').removeAttr('checked');
                    $('#observations-comments').val('');
                    $('#txtVideoTags1_tagsinput span').remove();
                    $('#txtVideostandard1_tagsinput span').remove();
                    $('#txtVideostandard1_tag').show();
                    $('.timer_box2').text('00:00:00');
                    $('.timer_box2').hide();
                    $('#observation-comments').html(response.comments);
                    $('#video_id').val(response.document_id);
                    $('#txtVideoTags1').removeAllTag();
                    $('#txtVideostandard1').removeAllTag();
                    $("#expList1").find('input:checkbox').removeAttr('checked');
                    $('#observations-comments').attr("placeholder", "Add a comment...");
                },
                error: function (e) {

                }
            });
        }
        (function ($) {
            $.extend({
                APP: {
                    formatTimer: function (a) {
                        if (a < 10) {
                            a = '0' + a;
                        }
                        return a;
                    },
                    startTimer: function (dir) {

                        var a;
// save type
                        $.APP.dir = dir;
// get current date
                        $.APP.d1 = new Date();
                        switch ($.APP.state) {

                            case 'pause' :

// resume timer
// get current timestamp (for calculations) and
// substract time difference between pause and now
                                $.APP.t1 = $.APP.d1.getTime() - $.APP.td;
                                break;
                            default :

// get current timestamp (for calculations)
                                $.APP.t1 = $.APP.d1.getTime();
// if countdown add ms based on seconds in textfield
                                if ($.APP.dir === 'cd') {
                                    $.APP.t1 += parseInt($('#cd_seconds').val()) * 1000;
                                }

                                break;
                        }

// reset state
                        $.APP.state = 'alive';
                        $('#' + $.APP.dir + '_status').html('Running');
// start loop
                        $.APP.loopTimer();
                    },
                    pauseTimer: function () {

// save timestamp of pause
                        $.APP.dp = new Date();
                        $.APP.tp = $.APP.dp.getTime();
// save elapsed time (until pause)
                        $.APP.td = $.APP.tp - $.APP.t1;
// change button value
                        $('#' + $.APP.dir + '_start').val('Resume');
// set state
                        $.APP.state = 'pause';
                        $('#' + $.APP.dir + '_status').html('Paused');
                    },
                    stopTimer: function () {

// change button value
                        $('#' + $.APP.dir + '_start').val('Restart');
// set state
                        $.APP.state = 'stop';
                        $('#' + $.APP.dir + '_status').html('Stopped');
                    },
                    resetTimer: function () {

// reset display
                        $('#' + $.APP.dir + '_ms,#' + $.APP.dir + '_s,#' + $.APP.dir + '_m,#' + $.APP.dir + '_h').html('00');
// change button value
                        $('#' + $.APP.dir + '_start').val('Start');
// set state
                        $.APP.state = 'reset';
                        $('#' + $.APP.dir + '_status').html('Reset & Idle again');
                    },
                    endTimer: function (callback) {

// change button value
                        $('#' + $.APP.dir + '_start').val('Restart');
// set state
                        $.APP.state = 'end';
// invoke callback
                        if (typeof callback === 'function') {
                            callback();
                        }

                    },
                    loopTimer: function () {

                        var td;
                        var d2, t2;
                        var ms = 0;
                        var s = 0;
                        var m = 0;
                        var h = 0;
                        if ($.APP.state === 'alive') {

// get current date and convert it into
// timestamp for calculations
                            d2 = new Date();
                            t2 = d2.getTime();
// calculate time difference between
// initial and current timestamp
                            if ($.APP.dir === 'sw') {
                                td = t2 - $.APP.t1;
// reversed if countdown
                            } else {
                                td = $.APP.t1 - t2;
                                if (td <= 0) {
// if time difference is 0 end countdown
                                    $.APP.endTimer(function () {
                                        $.APP.resetTimer();
                                        $('#' + $.APP.dir + '_status').html('Ended & Reset');
                                    });
                                }
                            }

// calculate milliseconds
                            ms = td % 1000;
                            if (ms < 1) {
                                ms = 0;
                            } else {
// calculate seconds
                                s = (td - ms) / 1000;
                                if (s < 1) {
                                    s = 0;
                                } else {
// calculate minutes
                                    var m = (s - (s % 60)) / 60;
                                    if (m < 1) {
                                        m = 0;
                                    } else {
// calculate hours
                                        var h = (m - (m % 60)) / 60;
                                        if (h < 1) {
                                            h = 0;
                                        }
                                    }
                                }
                            }

// substract elapsed minutes & hours
                            ms = Math.round(ms / 100);
                            s = s - (m * 60);
                            m = m - (h * 60);
// update display
                            $('#' + $.APP.dir + '_ms').html($.APP.formatTimer(ms));
                            $('#' + $.APP.dir + '_s').html($.APP.formatTimer(s));
                            $('#' + $.APP.dir + '_m').html($.APP.formatTimer(m));
                            $('#' + $.APP.dir + '_h').html($.APP.formatTimer(h));
// loop
                            $.APP.t = setTimeout($.APP.loopTimer, 1);
                        } else {

// kill loop
                            clearTimeout($.APP.t);
                            return true;
                        }

                    }

                }

            });
            $(document).on('click', '#sw_start', function () {
                $.APP.startTimer('sw');
                $(this).hide();
                //create_empty_record();
                $('.pause_time_btn').show();
                $('.stop_time_btn').show();
                $('#txtVideoTags1_tag').removeAttr('disabled');
                $('#txtVideostandard1_tag').removeAttr('disabled');
                $('#observations-comments').removeAttr('disabled');
            });
            $(document).on('click', '#cd_start', function () {
                $.APP.startTimer('cd');

            });
            $(document).on('click', '#sw_stop,#cd_stop', function () {
                if (confirm("Are you sure to want stop observation?")) {
                    $.APP.stopTimer();
                    $.APP.resetTimer();
                    $(this).hide();
                    load_huddles_videos();
                }
                else {
                    return false;
                }
            });
            $(document).on('click', '#sw_reset,#cd_reset', function () {
                $.APP.resetTimer();
            });
            $(document).on('click', '#sw_pause,#cd_pause', function () {
                $('#sw_start').removeClass('start_time_btn');
                $('#sw_start').addClass('start_time_sml_btn');
                $('#sw_start').show();
                $(this).hide();
                $.APP.pauseTimer();
            });
        })(jQuery);

        $(document).on('click', '#gObservationStart', function (e) {
            $.APP.resetTimer();
            $('#sw_start').show();
            $('#sw_pause').hide();
            $('#sw_stop').hide();
            e.preventDefault();
            $('.tabset').hide();
            $('#video_id').val('');
            $('.action-buttons').hide();
            $('.btn-box').hide();
            $('#observations-1').slideUp();
            $('#observations-2').slideDown();
            $('#txtVideoTags1_tag').attr('disabled', 'disabled');
            $('#txtVideostandard1_tag').attr('disabled', 'disabled');
            $('#observations-comments').attr('disabled', 'disabled');
            $('#observatons-added-notes').hide();

        });
        $(document).on('click', '.ob-details-view', function (e) {
            e.preventDefault();
            document_id = $(this).attr('data-document-id');
            load_details(document_id);

        });

    });

    function update_observation_tab() {
        $.ajax({
            type: "POST",
            url: home_url + "/huddles/load_observation_tab",
            dataType: 'json',
            data: {
                account_id: $('#account_id').val(),
                user_id: $('#user_id').val(),
                huddle_id: $('#huddle_id').val(),
                video_id: $('#video_id').val()
            },
            success: function (response) {
                $('#videos-list').html(response.html);
            },
            error: function (e) {

            }
        });
    }
    $(document).on('click', '#publish-observations', function (e) {
        e.preventDefault();
        var document_id = $(this).attr('data-document-id');
        huddle_id = $('#huddle_id').val();
        $.ajax({
            type: "POST",
            url: home_url + "/huddles/publish_observation",
            dataType: 'json',
            data: {
                account_id: $('#account_id').val(),
                user_id: $('#user_id').val(),
                huddle_id: huddle_id,
                video_id: document_id
            },
            success: function (response) {
                window.location.href = home_url + '/Huddles/observation_details/' + huddle_id + '/' + document_id
            },
            error: function (e) {

            }
        });
    });
    $(document).on('click', '#add_video', function (e) {
        e.preventDefault();
        if ($('#sw_s').val() == '00') {
            alert('Please start observation before add comments.');
            return false;
        }
        var huddle_id = $('#huddle_id').val();
        var video_id = $('#video_id').val();
        $.ajax({
            type: "POST",
            url: home_url + "/huddles/add_observation_video",
            dataType: 'json',
            data: {
                loaded_video_id: $("#add-huddle-video input[type='checkbox']:checked").val(),
                observation_title: $('#ObservationTitle').text(),
                account_id: $('#account_id').val(),
                user_id: $('#user_id').val(),
                huddle_id: $('#huddle_id').val(),
                video_id: $('#video_id').val()
            },
            success: function (response) {
                window.location.href = home_url + '/Huddles/observation_details/' + huddle_id + '/' + video_id
                $("ul#expList1").find('input:checkbox').removeAttr('checked');
                $('#observations-comments').val('');
                $('#txtVideoTags1_tagsinput span').remove();
                $('#txtVideostandard1_tagsinput span').remove();
                $('#txtVideostandard1_tag').show();
                $('.timer_box2').text('00:00:00');
                $('#observation-comments').html(response.comments);
                $('#video_id').val(response.document_id);
                $('#txtVideoTags1').removeAllTag();
                $('#txtVideostandard1').removeAllTag();
                $("#expList1").find('input:checkbox').removeAttr('checked');
                $('#observations-comments').attr("placeholder", "Add a comment...");
                $('#observations-7').html(response.html);
                $('#observations-7').slideDown();
                $('#associate-videos-model').modal('hide');
                $('.modal-backdrop').remove();
                $("body").removeClass("wysihtml5-supported modal-open");
            },
            error: function (e) {

            }
        });
    });

    $(document).on('click', '#add-notes', function (e) {
        e.preventDefault();
        if ($('#observations-comments').val() == '') {
            $('#observations-comments').css('border', '1px solid red');
            $('#observations-comments').css('border-radius', '0');
            return true;
        }
        $.ajax({
            type: "POST",
            url: home_url + "/Huddles/add_call_notes",
            dataType: 'json',
            data: {
                observation_title: $('#ObservationTitle').text(),
                observations_comments: $('#observations-comments').val(),
                observations_standards: $('#txtVideostandard1').val(),
                observations_tags: $('#txtVideoTags1').val(),
                observations_time: $('.timer_box2').text(),
                account_id: $('#account_id').val(),
                user_id: $('#user_id').val(),
                huddle_id: $('#huddle_id').val(),
                video_id: $('#video_id').val()
            },
            success: function (response) {
                $("ul#expList1").find('input:checkbox').removeAttr('checked');
                $('#observations-comments').val('');
                $('#txtVideoTags1_tagsinput span').remove();
                $('#txtVideostandard1_tagsinput span').remove();
                $('#txtVideostandard1_tag').show();
                $('.timer_box2').text('00:00:00');
                $('.timer_box2').hide();
                $('#observation-comments').html(response.comments);
                $('.call').html(response.comments);
                $('#video_id').val(response.document_id);
                $('#txtVideoTags1').removeAllTag();
                $('#txtVideostandard1').removeAllTag();
                $("#expList1").find('input:checkbox').removeAttr('checked');
                $('#observations-comments').attr("placeholder", "Add a comment...");
            },
            error: function (e) {

            }
        });

//$('#observatons-added-notes').slideDown();
    });
</script>
