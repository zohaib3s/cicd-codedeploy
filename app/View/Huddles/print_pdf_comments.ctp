<?php

$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');

$sibme_base_url = Configure::read('sibme_base_url');

function formatSeconds($seconds) {
    $hours = 0;
    $milliseconds = str_replace("0.", '', $seconds - floor($seconds));

    if ($seconds > 3600) {
        $hours = floor($seconds / 3600);
    }
    $seconds = $seconds % 3600;


    return str_pad($hours, 2, '0', STR_PAD_LEFT)
            . gmdate(':i:s', $seconds)
            . ($milliseconds ? ".$milliseconds" : '');
}
?>
<ul class="comments" id="normal_comment">   

    <?php if (isset($videoComments) && $videoComments != ''): ?>
        <?php
        $timeS = '';
        foreach ($videoComments as $vidComments):
            ?>
            <?php
            $commentDate = $vidComments['Comment']['created_date'];
            ?>

            <li class="comment thread">
                <?php echo ($vidComments['Comment']['time'] == 0 ? "" : formatSeconds($vidComments['Comment']['time']) ); ?>
                <strong><?php echo $vidComments['User']['first_name'] . " " . $vidComments['User']['last_name']; ?></strong>&nbsp;<i>Posted on: &nbsp;<?php echo date('m/d/Y', strtotime($commentDate)); ?></i><br/>                    
                <p style="bottom: 0in;">
                    <?php echo nl2br($vidComments['Comment']['comment']); ?>
                </p>               
                <?php  if (!empty($vidComments['tags'])) { ?>                
                    <p style="word-wrap: break-word;bottom: 0in;">
                        <?php
                        $tags = '';
                        $get_value='';
                        foreach ($vidComments['tags'] as $tag) {                            
                            if ($tag['AccountCommentTag']['ref_type'] == '0') {
                               $tags=$tag['account_tags']['tag_code'] . '-' . $tag['account_tags']['tag_title'];
                            } else {
                                $tags=empty($tag['account_tags']['tag_title']) ? $tag['AccountCommentTag']['tag_title'] : $tag['account_tags']['tag_title'];
                            }
                            $get_value.="#".$tags.", ";
                        }
                        echo trim($get_value, ", ");                                                
                        ?>
                    </p>
                <?php } ?>
                <?php
                //level1 
                echo $this->element('ajax/vidCommentsRepliesPdf', array(
                    "vidComments" => $vidComments,
                    "huddle_permission" => $huddle_permission,
                    "user_current_account" => $user_current_account,
                    "huddle" => $huddle,
                    "user_permissions" => $user_permissions,
                    "counter" => 0
                ));
                ?>
            </li>

            <?php
        endforeach;
        ?>
    <?php endif; ?>


</ul>