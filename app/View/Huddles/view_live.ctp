<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$account_id = $user_current_account['accounts']['account_id'];
$user_id = $user_current_account['User']['id'];
$video_data["huddle"] = $video_data["huddle"][0];

foreach ($video_data['huddle_users'] as $hudd_user) {
    if ($hudd_user['huddle_users']['role_id'] == 210) {
        $video_detail_coachee = $hudd_user['huddle_users']['user_id'];
    }
}
?>
<style>
    [class*="icon-"]::before {
        background: none !important;

    }
</style>
<script src="<?php echo $this->webroot . 'crossdomain.xml' ?>"></script>
<script src="<?php echo $this->webroot . 'cross-domain-policy.dtd' ?>"></script>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="http://fonts.googleapis.com/css?family=Dosis" rel="stylesheet" type="text/css"/>
<!-- bitdash player -->
<script src="https://content.jwplatform.com/libraries/gXmBKmOv.js"></script>
<div id="flashMessage2" class="message error" style="display:none;"></div>
<input id="tab_hidden" type="hidden" value="<?php echo $tab; ?>">
<input id="latest_video_created_date" type="hidden" value="<?php echo $latest_video_created_date; ?>">
<input id="video_envo" type="hidden" value="1">

<style>
    .action-buttons a {
        position: relative;
    }

    .share_div {
        position: absolute;
        z-index: 20;
        background: #fff;
        padding: 10px;
        width: 308px;
        left: -255px;
        top: 40px;
        border-radius: 5px;
        box-shadow: 0px 0px 12px rgba(0, 0, 0, 0.1);
    }

    .share_div:after {
        bottom: 100%;
        left: 88%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-color: rgba(255, 255, 255, 0);
        border-bottom-color: #fff;
        border-width: 10px;
        margin-left: -10px;
    }

    .share_border {
        border-radius: 5px;
        border: dashed 1px #bfbfbf;
    }

    .share_border input[type="text"] {
        background: transparent;
        border: 0px;
        box-shadow: none;
        border-radius: 0px;
        font-weight: normal;
        width: 239px;
        border-radius: 5px;
    }

    .share_copy_btn {
        background: url(/app/img/tab-bg.gif) repeat-x left top;
        background-size: contain;
        border: 0px;
        padding: 8px 8px;
        color: #333;
        font-family: "Segoe UI", Frutiger, "Frutiger Linotype";
        font-size: 13px;
        font-weight: 600;
        outline: none;
    }

    /*    .header.header-huddle .action-buttons.btn-group a {
            padding: 8px 15px;
            line-height: 1.3;
        }*/

    .observation_mainContainer .tab-content {
        margin-top: 0px !important;
    }
</style>

<div class="header header-huddle">
    <div class="action-buttons btn-group right mmargin-top" style="margin-right: 15px;">
        <?php if ($isVideoPage != 0): ?>
            <?php if ($user_huddle_level_permissions == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $video_data["huddle"]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>

                <?php if ($video_data['huddle_type'] == 2): ?>
                    <?php if ($enable_tracker == 1 && !empty($video_detail_coachee)): ?>
                        <a href="<?php echo $this->base . '/dashboard/video_detail/' . $video_data['videoDetail']['Document']['id'] . '/' . $user_current_account['User']['id'] . '/' . $video_detail_coachee; ?>/1"
                           rel="tooltip" data-toggle="modal" data-target="#coach_modal" data-remote="false" class="btn">Coaching
                            Summary </a>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ($video_data['huddle_type'] == 3): ?>
                    <?php if ($this->Custom->is_enable_assessment($account_id) && !empty($video_detail_coachee)): ?>
                        <?php if ($this->Custom->check_if_evaluated_participant($video_data["huddle_des"]['AccountFolder']['account_folder_id'], $video_data['videoDetail']['Document']['created_by'])) { ?>
                            <a href="<?php echo $this->base . '/dashboard/assessment_detail/' . $video_data['videoDetail']['Document']['id'] . '/' . $user_current_account['User']['id'] . '/' . $video_data['videoDetail']['Document']['created_by']; ?>/1"
                               rel="tooltip" data-toggle="modal" data-target="#assessment_modal" data-remote="false"
                               class="btn">Assessment Summary </a>
                        <?php } ?>

                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>

        <?php endif; ?>

        <a href="<?php echo $this->base . '/Huddles'; ?>" class="btn" style="">Back to all Huddles</a>

        <?php if ($user_huddle_level_permissions == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $video_data["huddle"]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
            <a href="<?php echo $this->base . '/Huddles/edit/' . $video_data["huddle_des"]['AccountFolder']['account_folder_id']; ?>"
               class="btn icon2-pen" rel="tooltip" title="edit"></a>
            <a href="<?php echo $this->base . '/Huddles/delete/' . $video_data["huddle_des"]['AccountFolder']['account_folder_id']; ?>"
               data-confirm="Are you sure you want to delete this Huddle?" data-method="delete" class="btn icon2-trash"
               rel="tooltip" title="delete"></a>

            <!--<a class="btn" id="huddle_share"><img alt="share" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/share.png'); ?>" />
                                                                                                                                                                                                                                                                                                                                     <div id="share-div" class="share_div" style="display: none;">
                                                                                                                                                                                                                                                                                                                                           <div class="share_border">
            <?php $url_info = base64_encode($account_id); ?>                                                                                                                                                                                                                                                               </a>-->
        <?php endif; ?>
    </div>
    <!--
        <script type="text/javascript">
            $(document).ready(function () {
                $('#huddle_share').click(function () {
                    $('#share-div').show();
                });
            });
            var copyTextBtn = document.querySelector('#copyBtnShare');
            copyTextBtn.addEventListener('click', function (event) {
                var copyText = document.querySelector('.js-copytext');
                copyText.select();

                try {
                    var successful = document.execCommand('copy');
                    //var msg = successful ? 'successful' : 'unsuccessful';
                    //console.log('Copying text command was ' + msg);
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
            });
        </script>
    -->
    <div style="width:400px; float:left; padding-top: 11px;">
        <h2 class="title"><?php echo $video_data["huddle_des"]['AccountFolder']['name']; ?></h2>
    </div>
    <div style="clear: both;"></div>
    <?php
    $desc_width = 'style="width: 780px;"';
    if ($video_data['huddle_type'] == 2):
        $desc_width = 'style="width: 780px;"';
    elseif ($video_data['huddle_type'] == 3):
        $desc_width = 'style="width: 780px;"';
    endif;

    $is_avaluator = $this->Custom->check_if_evalutor($huddle_id, $user_id);
    ?>
    <p class="title-desc" <?php echo $desc_width; ?> ><?php echo $video_data["huddle_des"]['AccountFolder']['desc']; ?></p>
    <?php if (($video_data['huddle_type'] == '3' || ($video_data['huddle_type'] == '2' && $this->Custom->is_enabled_coach_feedback($huddle_id))) && $detail_id != '' && $is_avaluator): ?><?php //coaching huddle feedback ?>
        <p id="publish-feeback" class="title-desc"
           style="float:right;padding-right: 53px; <?php echo $total_incative_comments == 0 ? 'display: none;' : 'display: block;' ?>">
            <a href="<?php echo $this->base . '/Huddles/feedback_publish/' . $huddle_id . '/' . $detail_id ?>"
               class="btn btn-green right">Publish Feedback</a></p>
    <?php endif ?>
    <div style="clear: both;"></div>
</div>
<div class="tab-box" style="overflow: visible;">
    <div id="tab-area">
        <?php
        $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $video_data["huddle"]['AccountFolder']['created_by']) &&
            $user_permissions['UserAccount']['permission_maintain_folders'] == '1';
        $canUploadDocument = in_array($user_huddle_level_permissions, array(200, 210)) || $isCreator;
        $canViewDiscussion = in_array($user_huddle_level_permissions, array(200, 210)) || $isCreator;
        $canViewPeople = in_array($user_huddle_level_permissions, array(200, 210, 220)) || $isCreator;
        $canAddObservation = $user_current_account['roles']['role_id'] != '120' || $isCreator || @$user_permissions['UserAccount']['permission_administrator_observation_new_role'];
        ?>


        <div class="table-container">
            <?php echo $videosTab; ?>

        </div>
        <input id="txtUploadedFilePath" type="hidden" value=""/>
        <input id="txtUploadedFileName" type="hidden" value=""/>
        <input id="txtUploadedFileMimeType" type="hidden" value=""/>
        <input id="txtUploadedUrl" type="hidden" value=""/>
        <input id="txtUploadedFileSize" type="hidden" value=""/>
        <input id="txtUploadedDocType" type="hidden" value=""/>
        <input id="uploadPath" type="hidden" value="<?php echo "/tempupload/$account_id/" . date('Y/m/d/'); ?>"/>
        <input id="tabIndex" type="hidden" value="<?php echo $tab; ?>"/>
        <input id="account_id" type="hidden" value="<?php echo $account_id; ?>"/>
        <input id="user_id" type="hidden" value="<?php echo $user_id; ?>"/>
    </div>
</div>

<div id="addPermissionModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin"
                                                                               src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/admin.png'); ?>"/>
                    Permission Request</h4>
                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
            </div>
            <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/permission_request' ?>"
                  enctype="multipart/form-data"
                  method="post" name="admin_form" class="people-form">
                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;"/></div>
                <div class="way-form">
                    <h3 class="way-form-divider"><span>Step 1:</span> Send message to Account Owner</h3>
                    <textarea class="lmargin-bottom fmargin-left" cols="50" id="message" name="message"
                              placeholder="Message(Optional)" rows="4" style="width:450px;">I would like permission to create New Huddles.  Can you please activate this privilege on my permissions and privileges page (lock icon next to my name) and click save changes.</textarea>
                    <input id="controller_source" name="account_id" type="hidden" value="<?php echo $account_id; ?>"/>
                    <input id="controller_source" name="user_id" type="hidden" value="<?php echo $user_id; ?>"/>
                    <button class="btn btn-green fmargin-left" type="submit">Send Request</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="coach_modal" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 810px;">
        <div class="modal-content">
        </div>
    </div>
</div>
<div id="assessment_modal" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 810px;">
        <div class="modal-content">
        </div>
    </div>
</div>
<!--<script>
    is_unpublish = true;
    if (typeof ($('#publish-feeback').css('display')) != 'undefined' && $('#publish-feeback').css('display') == 'block') {
        is_unpublish = false;
    }
    if (!is_unpublish) {

        window.onbeforeunload = confirmExit;
    }
    function confirmExit(evt)
    {
        var message = 'You have not published your feedback for evaluated participant to view. Do you want to publish now?';
        return message;
    }
</script>-->
<script>
    $("#assessment_modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });</script>
<script>
    $("#coach_modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $(document).on("click", ".remove_tracker_button", function () {
        var r = confirm('Are you sure want to Delete this video from tracker?');
        if (r === true) {
            $('#remove_tracking_form').submit();
        }
    });</script>
<script type="text/javascript">
    $('#publish-feeback').click(function (e) {
        e.preventDefault();
        $video_id = $('#video_id').val();
        $.ajax({
            url: '<?php echo $this->base . '/huddles/feedback_publish/' . $huddle_id ?>/' + $video_id,
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status == true) {
                    $('#publish-feeback').css('display', 'none');

                    <?php if(IS_QA): ?>
                    var metadata = {
                        comment_published: 'Comments Published',

                    };
                    Intercom('trackEvent', 'comments-published', metadata);
                    <?php endif; ?>
                    alert(response.message);
                } else {
                    alert(response.message);
                }
            }
        });
    });

    <?php if ($video_data['huddle_type'] == 3) { ?>
    $('#gVideoUpload').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: '<?php echo $this->base . '/Huddles/check_videos_count' ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                huddle_id: '<?php echo $huddle_id ?>',
                user_id: '<?php echo $user_id; ?>'
            },
            success: function (response) {
                if (response.can_upload == 'yes') {
                    $.ajax({
                        url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
                        type: 'POST',
                        success: function (response) {
                            if (response != '') {
                                $('#flashMessage2').css('display', 'block');
                                $('#flashMessage2').html(response);

                            } else {
                                OpenFilePicker('video');
                            }
                        }
                    });
                } else {
                    alert(response.message);
                }
            }
        });
    });
    <?php } else { ?>
    $('#gVideoUpload').click(function (e) {
        $.ajax({
            url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
            type: 'POST',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);

                } else {
                    OpenFilePicker('video');
                }
            }
        });
    });
    <?php } ?>


    $('#gDocumentUpload').click(function (e) {
        $.ajax({
            url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
            type: 'POST',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);

                } else {
                    OpenFilePicker('doc');
                }
            }
        });
    });

    $('#flashMessage2').click(function (e) {

        $('#flashMessage2').fadeOut();
    });


</script>

<style type="text/css">
    .analytics_new {
        background-color: #FFF;
        border: 1px solid #DAD6CE;
        border-radius: 3px 3px 3px 3px;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        padding-bottom: 25px;
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
    }

    .analytics_new img {
        max-width: 100%;
    }

    .analytics_top {
        background: #fff;
        border-bottom: 2px solid #e8e8e8;
        padding: 8px 10px 15px 8px;
    }

    .analytic_img_cls {
        float: left;
        width: 98px;
    }

    .analytic_detail_cls {
        float: left;
        padding: 7px 17px 9px 16px;
        width: 532px;
    }

    .analytic_detail_cls h3 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-weight: 400;
        font-size: 23px;
    }

    .analytic_detail_cls h4 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-weight: 400;
        font-size: 15px;
    }

    .analytic_detail_cls label {
        color: #668dab;
    }

    .analytic_detail_cls span {
        display: inline-block;
        margin-right: 7px;
    }

    .analytic_topnav {
        float: right;
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-weight: 600;
        width: 365px;
        text-align: right;
    }

    .analytic_topnav a {
        color: #668dab;
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-weight: 600;
    }

    .analytic_topnav img {
        margin: 0 4px;
    }

    .analytic_overview {
        background: #fcfcfc;
        padding: 12px;
        border-bottom: 2px solid #e8e8e8;
    }

    .analytic_overview h3 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-weight: 400;
        font-size: 23px;
    }

    .over_datecls {
        color: #668dab;
        font-size: 14px;
        padding: 4px 0 5px 0;
    }

    .over_datecls img {
        margin-right: 5px;
        position: relative;
        top: -1px;
    }

    .analytic_countsbox {
        border: 1px solid #DAD6CE;
        border-radius: 2px;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        float: left;
        width: 23%;
        background: #fff;
        margin: 12px 1%;
    }

    .analytic_countsbox .anbox1 {
        background: #fcfcfc;
        text-align: center;
        padding: 21px 14px;
        width: 25%;
        float: left;
    }

    .analytic_countsbox .anbox2 {
        padding: 10px 8px;
        width: 75%;
        float: right;
    }

    .analytic_countsbox .anbox2 h4 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-weight: 600;
        font-size: 18px;
        color: #000000;
    }

    .analytic_countsbox .anbox2 h5 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-weight: 400;
        font-size: 13px;
        color: #878787;
    }

    .analytic_overview2 {
        background: #fff;
        padding: 12px;
        border-bottom: 2px solid #e8e8e8;
        position: relative;
    }

    .over_select {
        position: absolute;
        top: 12px;
        right: 15px;
    }

    .over_select select {
        background: #668dab;
        width: 170px;
        height: 38px;
        border-radius: 36px;
        color: #fff;
        padding: 6px 6px 6px 10px;
    }

    .am_chart1 {
        border: 2px solid #e8e8e8;
        padding: 10px;
        float: left;
        width: 47%;
        margin: 20px 1%;
    }

    .am_chart2 {
        padding: 20px 0;
    }

    .analytic_filter {
        float: left;
        width: 100%;
        padding: 0 15px 15px 20px;
    }

    .analytic_filter h3 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-weight: 400;
        font-size: 23px;
        margin: -6px 0 5px 0;
    }

    .analytic_filter select {
        width: 100px;
        border: 0;
        border-bottom: 2px solid #ddd;
        margin: 0 6px 0 4px;
    }

    .analytic_filter input[type=text] {
        width: 70px;
        border: 1px solid #ddd;
        margin: 0 6px 0 4px;
    }

    .analytic_filter .btn {
        margin-left: 10px;
    }

    .analytic_custombtn {
        float: right;
        color: #668dab;
        cursor: pointer;
    }

    .analytic_custombtn a {
        color: #668dab;
        text-decoration: none;
    }

    .analytic_custom2 {
        display: none;
    }

    .framework_filter {
        background: #fcfcfc;
        padding: 12px;
        border-bottom: 2px solid #e8e8e8;
    }

    .flter_leftcls {
        float: left;
        width: 45%;
    }

    .flter_leftcls select {
        width: 150px;
        border: 0;
        border-bottom: 2px solid #ddd;
        margin: 0 6px 0 4px;
        background: #fcfcfc;;
    }

    .flter_rightcls {
        float: left;
        width: 55%;
    }

    .flter_rightcls input[type=text] {
        border: 0;
        outline: 0 !important;
        box-shadow: 0 0px 0px rgba(0, 0, 0, 0.09) inset;
        background: #fcfcfc;
        color: #668dab;
        width: 119px;
    }

    .flter_rightcls ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #668dab;
    }

    .flter_rightcls ::-moz-placeholder { /* Firefox 19+ */
        color: #668dab;
    }

    .flter_rightcls :-ms-input-placeholder { /* IE 10+ */
        color: #668dab;
    }

    .flter_rightcls :-moz-placeholder { /* Firefox 18- */
        color: #668dab;
    }

    .flter_rightcls .token_filter {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        display: inline-block;
        border: 1px solid #b9b9b9;
        background-color: #fff;
        white-space: nowrap;

        vertical-align: top;
        cursor: default;
        border-radius: 5px;
    }

    .flter_rightcls .token-label {
        display: inline-block;
        overflow: hidden;
        text-overflow: ellipsis;
        padding-left: 4px;
        vertical-align: top;
        max-width: 100px;
        font-size: 12px;
    }

    .flter_rightcls .close {
        font-family: Arial;
        display: inline-block;
        line-height: 100%;
        font-size: 1.1em;
        line-height: 1.49em;
        margin-left: 5px;
        float: none;
        height: 100%;
        vertical-align: top;
        padding-right: 4px;
        position: absolute;
        right: 15px;
        top: 6px;
        font-weight: 400;
        color: #998c8c;
    }

    .flter_rightcls .token {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        display: inline-block;
        border: 1px solid #d9d9d9;
        background-color: #fff;
        white-space: nowrap;
        margin: 4px 5px 5px 0;

        vertical-align: top;
        cursor: default;
        position: relative;
        border-radius: 34px;
        padding: 3px 20px 0px 4px;
    }

    .performance_left {
        float: left;
        width: 49%;
        border-right: 2px solid #ddd;
        padding: 2%;
        max-height: 350px;
        overflow: auto;
    }

    .performance_left h3 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-size: 17px;
    }

    .performance_left h5 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        font-size: 13px;
        margin: 16px 0 13px 0;
    }

    .performance_right {
        float: left;
        width: 49%;
        padding: 2%;
        max-height: 350px;
        overflow: auto;
    }

    .performance_outer {
        padding: 20px;
    }

    .per_r1 {
        width: 30%;
        float: left;
    }

    .per_r2 {
        width: 11%;
        float: left;
        text-align: center;
    }

    .per_r3 {
        width: 59%;
        float: left;
    }

    .per_row {
        margin: 5px 0;
    }

    .per_r1 select {
        width: 100%;
        border-radius: 4px;
        border: 1px solid #ddd;
        height: 28px;
    }

    .per_r2 span {
        width: 28px;
        height: 28px;
        border-radius: 50%;
        display: inline-block;
        line-height: 26px;
        text-align: center;
        color: #fff;
        font-size: 12px;
    }

    .per_green {
        background: #2cb742;
    }

    .per_blu {
        background: #3498db;
    }

    .per_r3 {
    }

    .performance_before_select h3 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        margin: 15px 0;
        font-size: 15px;
        color: #4f4f4f;
    }

    .performance_before_select {
        text-align: center;
        padding-top: 73px;
    }

    .performance_right h4 {
        font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        margin: 10px 0;
        font-size: 14px;
        color: #4f4f4f;
    }

    #performance_poup .modal-content form {
        padding: 0;
    }


</style>
