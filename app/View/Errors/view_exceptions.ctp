<div>
    <?php if (isset($view_exceptions['AuditErrors']) && $view_exceptions['AuditErrors'] != ''): ?>
        <div style="padding-bottom:10px; "><strong>Created By : </strong> <?php echo $view_exceptions['User']['first_name'] . ' ' . $view_exceptions['User']['last_name']; ?></div>
        <div  style="padding-bottom:10px; "><strong>Error Page : </strong> <?php echo $view_exceptions['AuditErrors']['path']; ?></div>
        <div  style="padding-bottom:10px; "><strong>Error Number : </strong> <?php echo $view_exceptions['AuditErrors']['error_id']; ?></div>
        <div><h2>Error Details</h2>
            <?php
            echo $view_exceptions['AuditErrors']['error_detail'];
            ?>
        </div>
        <?php if ($view_exceptions['AuditErrors']['form_fields']): ?>
            <div><h2>Form Fields</h2></div>
            <pre>
                <?php
                echo $view_exceptions['AuditErrors']['form_fields'];
                ?>
            <?php endif; ?>
        </pre>
    <?php else: ?>
        <div class='message'>No Errors Found.</div>
    <?php endif; ?>
</div>