<div>
    <div class='message error-message'>
        We have encountered an unexpected problem, your issue has been reported to the support team with incident number: <strong><?php echo $this->Session->read('error_id');
    ?></strong>
    </div>
</div>