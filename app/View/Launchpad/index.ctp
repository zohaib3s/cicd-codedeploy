
<?php
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
if ($allAccounts):
    ?>

    <style>
        #flashMessage
        {
            /* display: none; */
        }
        
        .clear{
            clear:both;
        }
        .launchpad_container{
            width:745px;
            margin:0 auto;
            margin-top:20px;
        }
        .logo_box{
            text-align:center;
            margin:10px 0px;
        }
        .launchpad_container h1 {
            color: #0d3244;
            font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
            font-size: 23px;
            font-weight: 500;
            margin: 0px;
            text-align:center;
        }
        .search_box{
            margin:1px;
            text-align:center;
        }
        .logo_container{
            background:#fff;
            border:solid 1px #c9c8c8;
            margin:15px 0px;
            box-sizing:border-box;
            border-bottom: 0;
        }
        .launchpad_logos{
            border-right: solid 1px #c9c8c8;
            float: left;
            width: 247.5px;
            padding: 12px 10px;
            border-bottom: solid 1px #c9c8c8;
        }

        .logo_container a:nth-child(3n) .launchpad_logos{
            border-right:0px;
        }
        .logo_container {
            background: none;
            border: 0px;
            margin: 15px 0px;
            box-sizing: border-box;
            border-bottom: 0;
        }
        .client_logo{
            text-align:center;
            display:table;
            width:100%;
            min-height: 100px;
        }
        .client_logo_inner{
            display:table-cell;
            vertical-align:middle;
            text-align:center;

        }
        .client_logo_inner {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
            padding: 30px 0px;
            padding-bottom: 48px;
        }
        .client_logo_inner img{  max-width: 85%; height: 45px; width: 140px; max-width: 100px;}
        .launchpad_logos {
            float: left;
            width: 230px;
            padding: 20px 10px;
            border: solid 2px #e6e9ec;
            margin: 9px;
            border-radius: 8px;
            background: #fff !important;
        }
        .launchpad_logos h4{
            text-align: center;
            color: #848688;
            font-size: 17px;
        }
        .launchpad_logos h1{
            color: #0d3244;
            font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
            font-size: 22px;
            font-weight: 500;
            margin: 0px;
            text-align:center;
        }
        .launchpad_logos p{
            margin:1px 0px;
            text-align:center;
            color:#9c9c9c;
            font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
            font-size: 14px;
            font-weight: 500;
        }
        .launchpad_logos p {
            margin: 1px 0px;
            text-align: center;
            color: #747474;
            font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
            font-size: 15px;
            font-weight: 600;
        }
        .content {
            width: 70px;
            background-color: #fffaa5;
        }
        .diagonal {
            border-bottom: 38px solid #ccf2fd;
            border-left: 0px solid rgba(0, 0, 0, 0);
            display: block;
            width: 50%;
            float: right;
        }
        .clear {
            clear: both
        }
    </style>


    <!--<div class="content">
      <div class="diagonal"></div>
      <div class="clear"></div>
    </div>-->
    <?php
    $logo_config = $this->Custom->get_site_settings('launchpad_uper_logo_config');
    $lunch_pad_border = $this->Custom->get_site_settings('lunchpad_border');
    ?>

    <div class="launchpad_container">
        <div class="logo_box">
            <img src="<?php echo $this->Custom->get_site_settings('launchpad_header_logo'); ?>" width="<?php echo $logo_config['width'] ?>"  alt=""/>
        </div>
        <h1><?php echo $language_based_content['choose_account']; ?></h1>
        <br>
        <div class="search_box" style="margin-left:222px;">
            <div class="search">
                <input type="hidden" id="search-mode" action="" class="btn-search" value="<?php //echo $view_mode;?>"/>
                <input type="text" id="txtSearchDashboard" name="txtSearchDashboard" class="round" value="<?php echo isset($search_text) ? $search_text : ''; ?>" placeholder="<?php echo $language_based_content['search_placeholder_lp']; ?>">
                <input type="submit" id="search_dashboard" class="corner" value="">
                <span id="clearSearchDashboard" class="cross clear-video-input-box cross-search">X</span>
            </div>
        </div>
        <style>
            .search_box .round {
                width: 100%;
                border-radius: 15px;
                border: 1px #b2b2b2 solid;
                padding: 5px 25px 5px 25px;
                position: absolute;
                top: 0;
                left: 0;
                z-index: 5;
                color: #b2b2b2 !important;
                font-weight: normal !important;
            }

            .search_box .corner {
                position: absolute;
                top: 3px;
                left: 5px;
                height: 20px;
                width: 20px;
                z-index: 10;
                border-radius: 10px;
                border: none;
                background-image: url(<?php echo $this->webroot . 'img/search_btn.png' ?>);
                background-color: transparent;
            }

            .search_box .search {
                position: relative;
                width: 300px;
                height: 30px;
            }

            span.cross {
                cursor: pointer;
                display: none;
                position: absolute;
                top: 3px;
                right: 5px;
                height: 20px;
                width: 20px;
                z-index: 10;
            }
        </style>

<?php /* ?>
        <div class="search_box" style="margin-left:222px;">
            <!--Search field here-->
            <!--    <form method="post" id="new_video_huddle" enctype="multipart/form-data" class="new_video_huddle"
                      action="" accept-charset="UTF-8"> -->
            <input type="hidden" id="search-mode" action="" class="btn-search" value="<?php //echo $view_mode;                                       ?>"/>
            <input  id="search_dashboard" action="" class="btn-search" value="" style="width:27px;margin-top: -10px;"/>
            <input class="text-input" name="txtSearchDashboard" id="txtSearchDashboard" type="text" value="<?php
            if (isset($search_text)) {
                echo $search_text;
            }
            ?>" placeholder="Search ..." style="margin-right: 25px;margin-top: -10px;"/>
            <span id="clearSearchDashboard"  style="display: none;margin-right: 322px;margin-top:135px;" class="clear-video-input-box cross-search search-x-huddles">X</span>
            <!--    </form>-->
        </div>
<?php */ ?>
        <br>

        <div class="logo_container">
            <?php
            $count = 1;
            ?>
            <?php foreach ($allAccounts as $accounts): ?>
                <?php
                if ($accounts['accounts']['is_suspended'] == true)
                    continue;

                $role = '';
                if (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '100') {
                    $role = $language_based_content['account_owner'];
                } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '110') {
                    $role = $language_based_content['super_admin'];
                } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '120') {
                    $role = $language_based_content['user'];
                } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '115') {
                    $role = $language_based_content['admin'];
                } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '125') {
                    $role = $language_based_content['viewer'];
                } else {
                    $role = $language_based_content['user'];
                }
                $div = '';
                $style = '';
                $lastClass = '';
                if ($count % 5 == 0) {
                    $style = 'border-right: none;';
                    $div = '</div><div class="box  span3 " style="padding: 0px 0px 0px;">';
                }
                //$logo_image = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/sibme_logo2.png');

                $logo_image = $this->Custom->get_site_settings('launchpad_logo');
                $logo_path = $this->Custom->getSecureSibmecdnImageUrl("static/companies/" . $accounts['accounts']['account_id'] . "/" . $accounts['accounts']['image_logo'], $accounts['accounts']['image_logo']);
                if (!empty($accounts['accounts']['image_logo'])) {
                    $logo_image = $logo_path;
                }
                ?>
                <a href="<?php echo $this->base . '/launchpad/account/' . $accounts['accounts']['account_id'] ?>" title=" <?php echo isset($accounts['accounts']['company_name']) ? $accounts['accounts']['company_name'] : ''; ?>">
                    <div class="launchpad_logos" style="<?php echo ($accounts['accounts']['account_id'] == $account_id ? "background-color: #f5f5f5; border:solid 2px $lunch_pad_border" : ""); ?>">
                        <div class="client_logo">
                            <div class="client_logo_inner">
                                <?php
                                echo $this->Html->image($logo_image, array('height' => '45', 'width' => '140', 'style="width:140px;height:45px;"'));
                                ?>
                            <!--<img src="/app/img/sibme_logo_3.png" width="158" height="51" alt=""/> -->
                            </div>

                        </div>
                        <h4 style=" text-align: center;" class="nomargin-vertical compact wrap">
                            <?php echo isset($accounts['accounts']['company_name']) ? $accounts['accounts']['company_name'] : ''; ?>
                        </h4>
                        <p><?php echo $role; ?></p>
                        <?php if (isset($accounts['accounts']['parent_account_id']) && $accounts['accounts']['parent_account_id'] == $account_id): ?>
                            <div class="text-center">
                                <div class="action-buttons btn-group">
                                    <a data-original-title="Edit" href="<?php echo $this->base . '/accounts/edit/' . $accounts['accounts']['account_id'] ?>" class="btn icon2-edit" rel="tooltip"></a>
                                    <a href="<?php echo $this->base . '/accounts/delete/' . $accounts['accounts']['account_id'] ?>"  onclick="return confirm('Are you sure you want to permanently delete this account?')" class="btn icon2-trash" data-method="delete" rel="tooltip nofollow" title="Remove"></a>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </a>
                <?php $count++; ?>
            <?php endforeach; ?>
            <div class="clear"></div>
        </div>

    </div>

    <!--<div class="row lmargin-bottom" style="margin-top: 30px; ">

        <div class="box launchpad span3 " style="padding: 0px 0px 0px;" >
            <h5 class="smargin-vertical"style="text-align: center;color: rgb(131,128,124)">Choose an account</h5>
            <hr>
    <?php
    $count = 1;
    ?>
    <?php foreach ($allAccounts as $accounts): ?>
        <?php
        if ($accounts['accounts']['is_suspended'] == true)
            continue;

        $role = '';
        if (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '100') {
            $role = 'Account Owner';
        } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '110') {
            $role = 'Super Admin';
        } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '120') {
            $role = 'User';
        } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '115') {
            $role = 'Admin';
        } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '125') {
            $role = 'Viewer';
        } else {
            $role = 'User';
        }
        $div = '';
        $style = '';
        $lastClass = '';
        if ($count % 5 == 0) {
            $style = 'border-right: none;';
            $div = '</div><div class="box  span3 " style="padding: 0px 0px 0px;">';
        }
        ?>
                                                                                                                                                                        <a href="<?php echo $this->base . '/launchpad/account/' . $accounts['accounts']['account_id'] ?>" title=" <?php echo isset($accounts['accounts']['company_name']) ? $accounts['accounts']['company_name'] : ''; ?>  " style="display:block;float:left;">
                                                                                                                                                                            <div class="lanchpad-box <?php echo ($accounts['accounts']['account_id'] == $account_id ? "lanchpad-box-selected" : ""); ?>" style="">
                                                                                                                                                                                <div style="text-align: center;margin-top:30px;">
        <?php
        $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/sibme.png');
        echo $this->Html->image($chimg);
        ?>
                                                                                                                                                                                </div>
                                                                                                                                                                                <div style="margin-top: 20px;">
                                                                                                                                                                                    <h4 style=" text-align: center;" class="nomargin-vertical compact wrap">

        <?php echo isset($accounts['accounts']['company_name']) ? $accounts['accounts']['company_name'] : ''; ?>
                                                                                                                                                                                    </h4>
                                                                                                                                                                                </div>
                                                                                                                                                                                <div style="text-align: center;padding-bottom: 5px;">
                                                                                                                                                                                    <span style="font-weight: normal;text-transform: none;" class="user-status inline">
        <?php echo $role; ?>
                                                                                                                                                                                    </span>
                                                                                                                                                                                </div>
        <?php if (isset($accounts['accounts']['parent_account_id']) && $accounts['accounts']['parent_account_id'] == $account_id): ?>
                                                                                                                                                                                                                    <div class="text-center">
                                                                                                                                                                                                                    <div class="action-buttons btn-group">
                                                                                                                                                                                                                    <a data-original-title="Edit" href="<?php echo $this->base . '/accounts/edit/' . $accounts['accounts']['account_id'] ?>" class="btn icon2-edit" rel="tooltip"></a>
                                                                                                                                                                                                                    <a href="<?php echo $this->base . '/accounts/delete/' . $accounts['accounts']['account_id'] ?>"  onclick="return confirm('Are you sure you want to permanently delete this account?')" class="btn icon2-trash" data-method="delete" rel="tooltip nofollow" title="Remove"></a>
                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                    </div>
        <?php endif; ?>
                                                                                                                                                                            </div>
                                                                                                                                                                        </a>
        <?php //echo $div;     ?>
        <?php $count++; ?>
    <?php endforeach; ?>
        </div>
        <div class='clearfix'></div>
    </div>

    <style>
        .lanchpad-box:hover{
            background-color: rgb(251,251,251);

        }
    </style>-->
<?php else: ?>
    <div class="message info" style="margin-top: 50px;">
        No Account Found.
    </div>
<?php endif; ?>

<script>

    $(document).on('click', '#search_dashboard', function (e) {
        var search_text = $('#txtSearchDashboard').val();

        $.ajax({
            type: 'post',
            url: home_url + '/Launchpad/search',
            data: {txtSearchDashboard: search_text},
            success: function (data)
            {

                $('.logo_container').html(data);

            }
        });


    });


    $("#txtSearchDashboard").on("keypress", function (e) {
        $("#clearSearchDashboard").show();
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {

            var search_text = $('#txtSearchDashboard').val();

            $.ajax({
                type: 'post',
                url: home_url + '/Launchpad/search',
                data: {txtSearchDashboard: search_text},
                success: function (data)
                {

                    $('.logo_container').html(data);

                }
            });
        }
    });

    $(document).on('click', '#clearSearchDashboard', function (e) {
        $('#txtSearchDashboard').val('');
        $('#clearSearchDashboard').hide();

        var search_text = $('#txtSearchDashboard').val();

        $.ajax({
            type: 'post',
            url: home_url + '/Launchpad/search',
            data: {txtSearchDashboard: search_text},
            success: function (data)
            {

                $('.logo_container').html(data);

            }
        });


    });


</script>
