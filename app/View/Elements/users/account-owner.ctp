<?php
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$role = $users['roles']['role_id'];
?>
<div class="rel" rel="account-owner">
    <hr class="full">
    <h2 class="inline"><?php echo $language_based_content['account_owner_people_section']; ?></h2>
    <a onMouseOut="hide_sidebar()" href="#" class="appendix appendix--info"></a>

    <div class="appendix-content down">
        <h3 style="margin-bottom: 0"><?php echo $language_based_content['info_people_section']; ?></h3>
        <p>
           <?php echo $language_based_content['account_owner_info_people_section'].$language_based_content['account_owner_info_people_section_1']; ?> 
            <!--The account owner is responsible for managing the account. They are the only member of the account who can manage account settings. They can change the colors and logos of the account, manage plans and billing, and cancel the account.-->
        </p>
    </div>
    <ul class="users row-fluid glow">
        <?php if ($accountOwner): ?>
            <?php
            $pictureUrl = '';
            if ($accountOwner['User']['is_active'] == '1') {
                $pictureUrl = $this->base . '/Users/editUser/' . $accountOwner['User']['id'] . '/1';
            } else {
                $pictureUrl = $this->base . '/permissions/assign_user/' . $accountOwner['users_accounts']['account_id'] . '/' . $accountOwner['User']['id'];
            }
            ?>
            <li class="span4">
                <?php if (isset($accountOwner['User']['image']) && $accountOwner['User']['image'] != ''): ?>
                    <?php
                    $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $accountOwner['User']['id'] . "/" . $accountOwner['User']['image'], $accountOwner['User']['image']);
                    ?>
                    <a href="#">
                        <?php echo $this->Html->image($avatar_path, array('alt' => $accountOwner['User']['first_name'] . ' ' . $accountOwner['User']['last_name'], 'class' => 'photo inline', 'data-original-title' => $accountOwner['User']['first_name'] . ' ' . $accountOwner['User']['last_name'], 'height' => '53', 'rel' => 'tooltip', 'width' => '53', 'align' => 'left')); ?>
                    </a>
                <?php else: ?>
                    <a href="#">
                        <img width="53" height="53" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" rel="tooltip" class="photo" alt="<?php echo $accountOwner['User']['first_name'] . ' ' . $accountOwner['User']['last_name']; ?>" data-original-title="<?php echo $accountOwner['User']['first_name'] . ' ' . $accountOwner['User']['last_name']; ?>">
                    </a>
                <?php endif; ?>
                <span class="user-name"><?php echo $accountOwner['User']['first_name'] . " " . $accountOwner['User']['last_name']; ?></span>
                <p class="user-job">
                    <?php echo $accountOwner['User']['title'] ?>
                </p>
                <p class="user-job">
                    <?php if ($currentRole == 110 || ($user_permissions['UserAccount']['permission_administrator_user_new_role'] == 1 && $currentRole != 100)): ?>
                        <a rel="tooltip" class="icon-email blue-link" data-toggle="modal" data-target="#messageParticipant<?php echo $accountOwner['User']['id']; ?>" href="#" data-original-title="<?php echo $accountOwner['User']['email']; ?>"></a>
                    <?php endif; ?>
                    <?php if ($currentRole == 100) { // || $currentRole == 110) { ?>
                        <a rel="tooltip" class="icon-locked" href="<?php echo $this->base . '/permissions/assign_user/' . $accountOwner['users_accounts']['account_id'] . '/' . $accountOwner['User']['id'] ?>" data-original-title="<?php echo $language_based_content['permission_privileges_people_section']; ?>"></a>
                        <?php if ($role == 100 && ($accountOwner['User']['is_active'] == 1 && $accountOwner['User']['type'] == 'Active' )) { ?>
                            <a rel="tooltip" class="icon-password"  data-toggle="modal" data-target="#changePassword<?php echo $accountOwner['User']['id']; ?>" href="#" data-original-title="<?php echo $language_based_content['change_password_people_section']; ?>"></a>
                        <?php } ?>
                    <?php } ?>
                </p>

                <div id="changePassword<?php echo $accountOwner['User']['id']; ?>" class="modal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="header">
                                <h4 class="header-title nomargin-vertical smargin-bottom"><?php echo $language_based_content['change_password_of_account_owner_people']; ?></h4>
                                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                            </div>
                            <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/send_user_messages' ?>" class="new_message" enctype="multipart/form-data" id="new_message_1" method="post">
                                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                                <div class="input-group mmargin-top">

                                    <div class="row mrgn-0"><div class="span4"><strong><?php echo $language_based_content['name_people_section']; ?></strong> </div> <div class="span8"><strong><?php echo $accountOwner['User']['first_name'] . " " . $accountOwner['User']['last_name']; ?></strong> </div></div>
                                    <div class="row mrgn-0"><div class="span4"><strong><?php echo $language_based_content['email_people_section']; ?></strong> </div> <div class="span4"><strong><?php echo $accountOwner['User']['email']; ?></strong></div></div>

                                    <div class="input-group">
                                        <input autocomplete="off" class="size-big" id="user_password<?php echo $accountOwner['User']['id']; ?>" name="password"  placeholder=" <?php echo $language_based_content['new_password_placeholder_people_section']; ?>" size="30" type="password">
                                        <label><?php echo $language_based_content['password_question_people_section']; ?></label>
                                    </div>

                                    <div class="input-group">
                                        <input class="size-big" id="user_password_confirmation<?php echo $accountOwner['User']['id']; ?>" name="password_confirmation"  placeholder="<?php echo $language_based_content['confirm_password_placeholder_people_section']; ?>" size="30" type="password">
                                        <label><?php echo $language_based_content['password_question_people_section']; ?></label>
                                    </div>
                                    <p id="msg_err<?php echo $accountOwner['User']['id']; ?>" style="color:red;"></p>
                                    <div id="msg_succ<?php echo $accountOwner['User']['id']; ?>" class="message success pass_success" style="cursor: pointer; display:none;"></div>
                                </div>
                                <?php echo $this->Form->input('sender_id', array('id' => 'sender_id_ao', 'type' => 'hidden', 'value' => $user_id)); ?>
                                <?php echo $this->Form->input('recipient_id', array('id' => 'recipient_id_ao', 'type' => 'hidden', 'value' => $suppAdmin['User']['id'])); ?>
                                <?php echo $this->Form->input('message_type', array('id' => 'message_type_ao', 'type' => 'hidden', 'value' => '3')); ?>
                                <p>
                                    <button class="btn btn-blue inline" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" id="<?php echo $accountOwner['User']['id']; ?>" onclick="changePassword(this);"><?php echo $language_based_content['submit_people_section_button']; ?></button>
                                    <a id="changePassword_cancel<?php echo $accountOwner['User']['id']; ?>"  class="btn btn-transparent" data-dismiss="modal"><?php echo $language_based_content['cancel_people_section_button']; ?></a>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>

                <div id="messageParticipant<?php echo $accountOwner['User']['id']; ?>" class="modal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="header">
                                <h4 class="header-title nomargin-vertical smargin-bottom"><?php echo $language_based_content['message_to_account_owner_people_section']; ?></h4>
                                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                            </div>
                            <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/send_user_messages' ?>" class="new_message" enctype="multipart/form-data" id="new_message_2" method="post">
                                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                                <div class="input-group mmargin-top">
                                    <div id="editor<?php echo $accountOwner['User']['id']; ?>-toolbar" class="editor-toolbar">
                                        <a data-wysihtml5-command="bold"><?php echo $language_based_content['bold_people_section']; ?></a>
                                        <a data-wysihtml5-command="italic"><?php echo $language_based_content['italic_people_section']; ?></a>
                                        <a data-wysihtml5-command="insertOrderedList">ol</a>
                                        <a data-wysihtml5-command="insertUnorderedList">ul</a>
                                        <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote"><?php echo $language_based_content['quote_people_section']; ?></a>
                                    </div>
                                    <?php echo $this->Form->textarea('message', array('id' => 'editor-' . $suppAdmin['User']['id'], 'class' => 'editor-textarea', 'style' => 'width:530px', 'cols' => '65', 'required' => 'required', 'placeholder' => $language_based_content['message_people_section'].' ...')); ?>

                                </div>
                                <?php echo $this->Form->input('sender_id', array('id' => 'sender_id_su', 'type' => 'hidden', 'value' => $accountOwner['User']['id'])); ?>
                                <?php echo $this->Form->input('recipient_id', array('id' => 'recipient_id_su', 'type' => 'hidden', 'value' => $accountOwner['User']['id'])); ?>
                                <?php echo $this->Form->input('message_type', array('id' => 'message_type_su', 'type' => 'hidden', 'value' => '3')); ?>

                                <p>
                                    <button class="btn btn-blue inline" type="submit">Send</button>
                                    <a id="attachment_cancel<?php echo $accountOwner['User']['id']; ?>"  class="btn btn-transparent" style="display: block;"><?php echo $language_based_content['cancel_people_section_button']; ?></a>
                                </p>
                            </form>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $("#message_attachment<?php echo $accountOwner['User']['id']; ?>").hide();
                                    $("#attachment_cancel<?php echo $accountOwner['User']['id']; ?>").hide();
                                    $("#attachment-file<?php echo $accountOwner['User']['id']; ?>").click(function () {
                                        $("#message_attachment<?php echo $accountOwner['User']['id']; ?>").show("slow");
                                        $("#attachment_cancel<?php echo $accountOwner['User']['id']; ?>").show("slow");
                                        $(this).hide();
                                    });

                                    $("#attachment_cancel<?php echo $accountOwner['User']['id']; ?>").click(function () {
                                        //For IE
                                        $("#message_attachment<?php echo $accountOwner['User']['id']; ?>").replaceWith($("#message_attachment<?php echo $accountOwner['User']['id']; ?>").clone(true));
                                        //For other browsers
                                        $("#message_attachment<?php echo $accountOwner['User']['id']; ?>").val("");

                                        $("#message_attachment<?php echo $accountOwner['User']['id']; ?>").hide();
                                        $("#attachment-file<?php echo $accountOwner['User']['id']; ?>").show("slow");
                                        $(this).hide();
                                    });
                                });
                                function hide_sidebar() {
                                    $(".appendix-content").delay(500).fadeOut(500);
                                    // $("#collab_help").delay(500).fadeOut(500);


                                }
                            </script>
                        </div>
                    </div>
                </div>
            </li>
        <?php endif; ?>
    </ul>
    <hr class="full--slim dashed">
</div>
