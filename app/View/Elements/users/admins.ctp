<?php $users = $this->Session->read('user_current_account'); ?>

<div class="rel" rel="admins">
    <?php if ($users['roles']['role_id'] == '100' || $users['roles']['role_id'] == '110' || $users['roles']['role_id'] == '120'): ?>
        <a href="#" data-toggle="modal" data-target="#addAdminModal" class="btn btn-blue right"><span class="plus">+</span> Add User</a>
    <?php endif; ?>
    <h2 class="inline">Users</h2>
    <a href="#" class="appendix appendix--info"></a>

    <div class="appendix-content down">
        <h3>Info</h3>
        <p>Users can only access their private Workspace, participate in Huddles they have been invited to join, and view videos in the account library; however, the Account Owner or Super Users in the account can grant users special privileges by clicking the lock icon next to their name. </p>
    </div>
    <div style="padding: 0px; float: right; width: 244px; margin-right: 20px;" class="search-box">
        <div  id="admin-user-container" class="filterform"> </div>
    </div>
    <ul class="row-fluid users" id="admin-user-list-containers">
        <?php if ($admins): $count = 0; ?>
            <?php
            foreach ($admins as $adminsUsers):
                $class = '';
                if ($count % 2 == 0) {
                    $class = 'last';
                }
                $glow = '';
                if ($adminsUsers['User']['type'] == 'Active')
                    $glow = 'glow2';
                else
                    $glow = 'glow3';

                $pictureUrl = '';
                if ($users['accounts']['in_trial'] != 1) {
                    if ($adminsUsers['User']['type'] == 'Active') {
                        $pictureUrl = $this->base . '/Users/editUser/' . $adminsUsers['User']['id'] . '/1';
                    } else {
                        $pictureUrl = $this->base . '/permissions/assign_user/' . $adminsUsers['users_accounts']['account_id'] . '/' . $adminsUsers['User']['id'];
                    }
                }
                ?>
                <li class="span4 last  <?php echo $glow; ?> " >

                    <?php if (isset($adminsUsers['User']['image']) && $adminsUsers['User']['image'] != ''): ?>
                        <?php
                        $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $adminsUsers['User']['id'] . "/" . $adminsUsers['User']['image'], $adminsUsers['User']['image']);
                        ?>
                        <a href="#">
                            <?php echo $this->Html->image($avatar_path, array('alt' => $adminsUsers['User']['first_name'] . " " . $adminsUsers['User']['last_name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53', 'align' => 'left')); ?>
                        </a>
                    <?php else: ?>
                        <a href="#">
                            <img width="53" height="53" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo" alt="Photo-default">
                        </a>
                    <?php endif; ?>
                    <?php
                    $full_name = $adminsUsers['User']['first_name'] . ' ' . $adminsUsers['User']['last_name'];
                    $user_job = $adminsUsers['User']['title'];
                    ?>
                    <span class="user-name"><?php echo strlen($full_name) > 20 ? '<span rel="tooltip" data-original-title="' . $full_name . '">' . mb_substr($full_name, 0, 20) . "...</span>" : $full_name; ?></span>

                    <p class="user-job"><?php echo strlen($user_job) > 20 ? '<span rel="tooltip" data-original-title="' . $user_job . '">' . mb_substr($user_job, 0, 20) . "...</span>" : $user_job; ?></p>


                    <p class="actions">
                        <a rel="tooltip" class="icon-email blue-link" data-toggle="modal" data-target="#messageParticipant<?php echo $adminsUsers['User']['id']; ?>" href="#" data-original-title="<?php echo $adminsUsers['User']['email']; ?>"></a>
                        <a rel="tooltip" class="icon-locked" href="<?php echo $this->base . '/permissions/assign_user/' . $adminsUsers['users_accounts']['account_id'] . '/' . $adminsUsers['User']['id'] ?>" data-original-title="permission and privileges"></a>
                        <a rel="tooltip" href="<?php echo $this->base . '/Users/deleteAccount/' . $adminsUsers['User']['id'] . '/' . $adminsUsers['users_accounts']['account_id']; ?>" class="icon-trash" data-confirm="Are you sure you want to delete this user? If yes, the huddles they created will be transferred to the account owner." data-method="delete" rel="tooltip nofollow" data-original-title="Delete User" style="margin-top: 2px;"></a>
                    </p>
                </li>
                <div id="messageParticipant<?php echo $adminsUsers['User']['id']; ?>"  class="modal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="header">
                                <h4 class="header-title nomargin-vertical smargin-bottom">Message To User</h4>
                                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                            </div>
                            <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/send_user_messages' ?>" class="new_message" enctype="multipart/form-data" id="new_message_<?php echo $adminsUsers['User']['id']?>" method="post">
                                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                                <div class="input-group mmargin-top">
                                    <div id="editor<?php echo $adminsUsers['User']['id']; ?>-toolbar" class="editor-toolbar" style="display: none;">
                                        <a data-wysihtml5-command="bold">bold</a>
                                        <a data-wysihtml5-command="italic">italic</a>
                                        <a data-wysihtml5-command="insertOrderedList">ol</a>
                                        <a data-wysihtml5-command="insertUnorderedList">ul</a>
                                        <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                                    </div>
                                    <?php echo $this->Form->textarea('message', array('id' => 'editor-' . $adminsUsers['User']['id'], 'required' => 'required', 'class' => 'editor-textarea', 'style' => 'width:530px', 'cols' => '45', 'placeholder' => 'Message ...')); ?>
                                </div>

                                <?php echo $this->Form->input('sender_id', array('id'=>'sender_id_ad_'.$adminsUsers['User']['id'], 'type' => 'hidden', 'value' => $user_id)); ?>
                                <?php echo $this->Form->input('recipient_id', array('id'=>'recipient_id_ad_'.$adminsUsers['User']['id'],'type' => 'hidden', 'value' => $adminsUsers['User']['id'])); ?>
                                <?php echo $this->Form->input('message_type', array('id'=>'message_type_ad_'.$adminsUsers['User']['id'],'type' => 'hidden', 'value' => '4')); ?>
                                <p>
                                    <button class="btn btn-blue inline" type="submit">Send</button>
                                    <a id="attachment_cancel<?php echo $adminsUsers['User']['id']; ?>"  class="btn btn-transparent">Cancel</a>
                                </p>
                            </form>
                            <script type="text/javascript">

                                $(document).ready(function () {
                                    $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").hide();
                                    $("#attachment_cancel<?php echo $adminsUsers['User']['id']; ?>").hide();
                                    $("#attachment-file<?php echo $adminsUsers['User']['id']; ?>").click(function () {
                                        $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").show("slow");
                                        $("#attachment_cancel<?php echo $adminsUsers['User']['id']; ?>").show("slow");
                                        $(this).hide();
                                    });

                                    $("#attachment_cancel<?php echo $adminsUsers['User']['id']; ?>").click(function () {
                                        //For IE
                                        $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").replaceWith($("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").clone(true));
                                        //For other browsers
                                        $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").val("");
                                        $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").hide();
                                        $("#attachment-file<?php echo $adminsUsers['User']['id']; ?>").show("slow");
                                        $(this).hide();
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <?php
                $count++;
            endforeach;
            ?>
        <?php endif; ?>
    </ul>

    <hr class="full--slim dashed">
</div>
