<?php
$btn_1_color = $this->Custom->get_site_settings('primary_bg_color');
$btn_2_color = $this->Custom->get_site_settings('secondary_bg_color');
?>
<div class="rel" rel="groups">
    <a href="#" style="background-color:<?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color; ?>;" class="btn btn-green right" data-toggle="modal" data-target="#addGroupModal" data-dismiss-modal-class="close-reveal-modal, .close-reveal"><span class="plus">+</span>
        <?php echo $language_based_content['add_group_people_section']; ?></a>
    <h2 class="inline"><?php echo $language_based_content['groups_people_section']; ?></h2>
    <a onMouseOut="hide_sidebar()" href="#" class="appendix appendix--info"></a>
    <div class="appendix-content down">
        <h3><?php echo $language_based_content['info_people_section']; ?></h3>
        <p><?php echo $language_based_content['organize_your_etc_people_section']; ?>
        </p>
    </div>
    <div style="float: right; width: 300px;" class="permissions-search-box">
        <input type="button" value="" class="permissions-btn-search" id="btnGroupSearch">
        <input type="text" style="" placeholder="<?php echo $language_based_content['search_groups_people_section']; ?>" value="" id="txtSearchGroups" class="permissions-text-input">
        <span class="clear-video-input-box" style="display: none; position: relative; width: 20px; right: -270px; top: -26px; cursor: pointer" id="clearGroups">X</span>
    </div>
    <div class="row" id="grous-list">
        <?php if ($userGroups): ?>
            <?php foreach ($userGroups as $group): ?>
                <div class="span5 groups_panel">
                    <div class="right">
                        <a href="<?php echo $this->base . '/users/editGroups/' . $group['group_id']; ?>" class="icon-pen" rel="tooltip" title="<?php echo $language_based_content['edit_people_section']; ?>"></a>
                        <a href="<?php echo $this->base . '/users/deleteGroups/' . $group['group_id']; ?>" class="icon-trash" data-confirm="<?php echo $language_based_content['are_you_sure_want_to_delete_group_people_section']; ?>" data-method="delete" rel="tooltip nofollow" title="<?php echo $language_based_content['remove_people_section']; ?>"></a>
                    </div>
                    <h3 class="nomargin-top" style="min-height: 21px;"><?php echo $group['department_name']; ?></h3>

                    <style>
                        .users_table{
                            margin-bottom: 20px;
                            margin-top: 10px;
                            border: 1px solid #cecece;
                            border-radius: 3px;
                        }
                        .users_table tbody, .users_table thead
                        {
                            display: block;
                        }

                        .users_table tbody
                        {
                            overflow: auto;
                            height: 200px;
                        }
                        .permissions-search-box{
                            padding:0px;
                        }
                        #grous-list{
                            margin-top: 22px;
                        }
                    </style>

                    <div class="users_table">
                        <table class="center-last">
                            <thead>
                                <tr>
                                    <th style="width:285px;"><?php echo $language_based_content['name_simple_people_section']; ?></th>
                                    <th  style="width:50px;" style="text-align: center;;"><?php echo $language_based_content['remove_people_section']; ?></th>
                                </tr>
                            </thead>
                            <?php if (isset($group['users']) && count($group['users']) > 0): ?>
                                <tbody>
                                    <?php foreach ($group['users'] as $user): ?>
                                        <?php
                                        $role = '';
                                        if ($user['role'] == '120') {
                                            $role = '(U)';
                                        } elseif ($user['role'] == '110') {
                                            $role = '(SU)';
                                        } elseif ($user['role'] == '100') {
                                            $role = '(AO)';
                                        } elseif ($user['role'] == '115') {
                                            $role = '(A)';
                                        }
                                        ?>

                                        <tr>
                                            <td style="width:285px;">
                                                <b><?php echo $user['first_name'] . " " . $user['last_name']; ?> </b>
                                                <span class="smaller"> <?php echo $role; ?></span>
                                            </td>
                                            <td  style="width:78px;">
                                                <a href="<?php echo $this->base . '/users/deleteGroupUsers/' . $user['user_group_id'] ?>" class="icon-remove" data-confirm="<?php echo $language_based_content['are_you_delete_remove_admin_people_section']; ?>" data-method="delete" rel="nofollow"></a>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                </tbody>
                            <?php else: ?>
                                <tbody>
                                    <tr>
                                        <td style="width:378px;"><strong><?php echo $language_based_content['no_user_found_people_section']; ?></strong></td>
                                    </tr>
                                </tbody>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
                <?php
            endforeach;
        else:
            ?>
            <div class="row" style="margin-left: 0px;">
                <div style="margin-left: 20px;"><?php echo $language_based_content['no_groups_added_people_section']; ?></div>
            </div>
        <?php endif; ?>
    </div>
</div>

<script>
    function hide_sidebar() {
        $(".appendix-content").delay(500).fadeOut(500);
        // $("#collab_help").delay(500).fadeOut(500);


    }
</script>
