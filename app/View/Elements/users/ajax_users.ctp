<?php $users = $this->Session->read('user_current_account'); ?>
<ul class="row-fluid users">
    <?php if ($admins): $count = 0; ?>
        <?php
        foreach ($admins as $adminsUsers):

            $class = '';
            if ($count % 2 == 0) {
                $class = 'last';
            }
            $glow = '';
            if ($adminsUsers['User']['type'] == 'Active')
                $glow = 'glow2';
            else
                $glow = '';

            $pictureUrl = '';
            if ($users['accounts']['in_trial'] != 1) {
                if ($adminsUsers['User']['type'] == 'Active') {
                    $pictureUrl = $this->base . '/Users/editUser/' . $adminsUsers['User']['id'] . '/1';
                } else {
                    $pictureUrl = $this->base . '/permissions/assign_user/' . $adminsUsers['users_accounts']['account_id'] . '/' . $adminsUsers['User']['id'];
                }
            }
            ?>
            <li class="span4 last  <?php echo $glow; ?> " >
                <?php if (isset($adminsUsers['User']['image']) && $adminsUsers['User']['image'] != ''): ?>
                <?php
                    $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $adminsUsers['User']['id'] . "/" . $adminsUsers['User']['image'], $adminsUsers['User']['image']);
                ?>
                    <a href="<?php echo $pictureUrl; ?>">
                        <?php echo $this->Html->image($avatar_path, array('alt' => $adminsUsers['User']['first_name'] . " " . $adminsUsers['User']['last_name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53', 'align' => 'left')); ?>
                    </a>
                <?php else: ?>
                    <a href="<?php echo $pictureUrl; ?>">
                        <img width="53" height="53" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo" alt="Photo-default">
                    </a>
                <?php endif; ?>
                <span class="user-name"><?php echo $adminsUsers['User']['first_name'] . ' ' . $adminsUsers['User']['last_name']; ?></span>

                <p class="user-job"><?php echo $adminsUsers['User']['title']; ?></p>

                <p class="actions">
                    <a rel="tooltip" class="icon-email blue-link" data-toggle="modal" data-target="#messageParticipant<?php echo $adminsUsers['User']['id']; ?>" href="#" data-original-title="<?php echo $adminsUsers['User']['email']; ?>"></a>
                    <a rel="tooltip" class="icon-locked" href="<?php echo $this->base . '/permissions/assign_user/' . $adminsUsers['users_accounts']['account_id'] . '/' . $adminsUsers['User']['id'] ?>" data-original-title="permission and privileges"></a>
                </p>
            </li>
            <div id="messageParticipant<?php echo $adminsUsers['User']['id']; ?>"  class="modal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="header">
                            <h4 class="header-title nomargin-vertical smargin-bottom">Message To User</h4>
                            <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                        </div>
                        <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/send_user_messages' ?>" class="new_message" enctype="multipart/form-data" id="new_message_<?php echo $adminsUsers['User']['id']; ?>" method="post">
                            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                            <div class="input-group mmargin-top">
                                <div id="editor<?php echo $adminsUsers['User']['id']; ?>-toolbar" class="editor-toolbar" style="display: none;">
                                    <a data-wysihtml5-command="bold">bold</a>
                                    <a data-wysihtml5-command="italic">italic</a>
                                    <a data-wysihtml5-command="insertOrderedList">ol</a>
                                    <a data-wysihtml5-command="insertUnorderedList">ul</a>
                                    <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                                </div>
                                <?php echo $this->Form->textarea('message', array('id' => 'editor-' . $adminsUsers['User']['id'], 'required' => 'required', 'class' => 'editor-textarea', 'style'=> 'width:530px', 'cols' => '45', 'placeholder' => 'Message ...')); ?>
                            </div>

                            <?php echo $this->Form->input('sender_id', array('id'=>'sender_id_au_'.$adminsUsers['User']['id'], 'type' => 'hidden', 'value' => $user_id)); ?>
                            <?php echo $this->Form->input('recipient_id', array('id'=>'recipient_id_au_'.$adminsUsers['User']['id'],'type' => 'hidden', 'value' => $adminsUsers['User']['id'])); ?>
                            <?php //echo $this->Form->input('attachment', array('id' => 'message_attachment' . $adminsUsers['User']['id'], 'type' => 'file', 'value' => $adminsUsers['User']['id'], 'placeholder' => 'attach file')); ?>
                            <?php echo $this->Form->input('message_type', array('id'=>'message_type_au_'.$adminsUsers['User']['id'],'type' => 'hidden', 'value' => '4')); ?>
                            <p>
                <!--                            <a id="attachment-file<?php echo $adminsUsers['User']['id']; ?>" class="btn btn-white btn-light btn-gray-text icon2-clip inline"> Attach File</a>-->
                                <button class="btn btn-blue inline" type="submit">Send</button>
                                <a id="attachment_cancel<?php echo $adminsUsers['User']['id']; ?>"  class="btn btn-transparent">Cancel</a>
                            </p>
                        </form>
                        <script type="text/javascript">

                            $(document).ready(function() {

                                $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").hide();
                                $("#attachment_cancel<?php echo $adminsUsers['User']['id']; ?>").hide();
                                $("#attachment-file<?php echo $adminsUsers['User']['id']; ?>").click(function() {
                                    $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").show("slow");
                                    $("#attachment_cancel<?php echo $adminsUsers['User']['id']; ?>").show("slow");
                                    $(this).hide();
                                });

                                $("#attachment_cancel<?php echo $adminsUsers['User']['id']; ?>").click(function() {
                                    //For IE
                                    $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").replaceWith($("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").clone(true));
                                    //For other browsers
                                    $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").val("");
                                    $("#message_attachment<?php echo $adminsUsers['User']['id']; ?>").hide();
                                    $("#attachment-file<?php echo $adminsUsers['User']['id']; ?>").show("slow");
                                    $(this).hide();

                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
            <?php
            $count++;
        endforeach;
        ?>
    <?php else: ?>
        <li>No User matched this search. Try another search.</li>
    <?php endif; ?>
</ul>