<?php
$users = $this->Session->read('user_current_account');
$role = $users['roles']['role_id'];
?>

<script type="text/javascript">
    $('#viewer-count').text("(<?php echo $count ? $count : 0; ?>)");
    $('#viewer-container [rel="tooltip"]').tooltip({container: 'body'});
</script>

<ul class="users row-fluid" id="viewer-list-containers">
    <?php if ($supperAdmins): $count = 1; ?>
        <?php
        foreach ($supperAdmins as $suppAdmin):
            $class = '';
            if ($count % 2 == 0) {
                $class = 'last';
            }
            $glow = '';
            if ($suppAdmin['User']['type'] == 'Active')
                $glow = 'glow2';
            else
                $glow = '';

            $pictureUrl = '';
            if ($users['accounts']['in_trial'] != 1) {
                if ($suppAdmin['User']['type'] == 'Active') {
                    $pictureUrl = $this->base . '/Users/editUser/' . $suppAdmin['User']['id'] . '/1';
                } else {
                    $pictureUrl = $this->base . '/permissions/assign_user/' . $suppAdmin['users_accounts']['account_id'] . '/' . $suppAdmin['User']['id'];
                }
            }
            ?>
            <li class="span4 last <?php echo $glow; ?>">
                <?php if (isset($suppAdmin['User']['image']) && $suppAdmin['User']['image'] != ''): ?>
                    <?php
                    $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $suppAdmin['User']['id'] . "/" . $suppAdmin['User']['image'], $suppAdmin['User']['image']);
                    ?>
                    <a href="#">
                        <?php echo $this->Html->image($avatar_path, array('alt' => $suppAdmin['User']['first_name'] . " " . $suppAdmin['User']['last_name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53', 'align' => 'left')); ?>
                    </a>
                <?php else: ?>
                    <a href="#">
                        <img width="53" height="53" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo" alt="Photo-default">
                    </a>
                <?php endif; ?>
                <?php
                $full_name = $suppAdmin['User']['first_name'] . ' ' . $suppAdmin['User']['last_name'];
                $user_job = $suppAdmin['User']['title'];
                ?>
                <span class="user-name"><?php echo strlen($full_name) > 20 ? '<span rel="tooltip" data-original-title="' . $full_name . '">' . mb_substr($full_name, 0, 20) . "...</span>" : $full_name; ?></span>

                <p class="user-job"><?php echo strlen($user_job) > 20 ? '<span rel="tooltip" data-original-title="' . $user_job . '">' . mb_substr($user_job, 0, 20) . "...</span>" : $user_job; ?></p>

        <p class="actions">
            <?php if ($suppAdmin['User']['id'] != $users['User']['id']): ?>
            <a rel="tooltip" class="icon-email blue-link" data-toggle="modal" data-target="#messageParticipant<?php echo $suppAdmin['User']['id']; ?>" href="#" data-original-title="<?php echo $suppAdmin['User']['email']; ?>"></a>
            <?php endif ?>
            <?php if( in_array($role, [100,110]) || ( in_array($role, [115,120]) && $manage_users_permission == 1 ) ) {?>
            <a rel="tooltip" class="icon-locked" href="<?php echo $this->base . '/permissions/assign_user/' . $suppAdmin['users_accounts']['account_id'] . '/' . $suppAdmin['User']['id'] ?>" data-original-title="<?php echo $language_based_content['permission_privileges_people_section']; ?>"></a>
            <a rel="tooltip" href="<?php echo $this->base . '/Users/deleteAccount/' . $suppAdmin['User']['id'] . '/' . $suppAdmin['users_accounts']['account_id'];?>" class="icon-trash" data-confirm="<?php echo $language_based_content['delete_user_huddle_transfered_people_section']; ?>" data-method="delete" rel="tooltip nofollow" data-original-title="<?php echo $language_based_content['delete_user_people_section']; ?>" style="margin-top: 2px;"></a>
            <?php if( $suppAdmin['User']['is_active'] == 1 && $suppAdmin['User']['type'] == 'Active' ){ ?>
                <a rel="tooltip" class="icon-password"  data-toggle="modal" data-target="#changePassword<?php echo $suppAdmin['User']['id']; ?>" href="#" data-original-title="<?php echo $language_based_content['change_password_people_section']; ?>"></a>
            <?php } ?>
            <?php } ?>
        </p>
    </li>

            <div id="changePassword<?php echo $suppAdmin['User']['id']; ?>" class="modal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="header">
                            <h4 class="header-title nomargin-vertical smargin-bottom"><?php echo $language_based_content['change_password_viewer_people_section']; ?></h4>
                            <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                        </div>
                        <form accept-charset="UTF-8" action="#" class="new_message" enctype="multipart/form-data"  method="post">
                            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                            <div class="input-group mmargin-top">

                                <div class="row mrgn-0"><div class="span4"><strong><?php echo $language_based_content['name_people_section']; ?></strong> </div> <div class="span8"><strong><?php echo $suppAdmin['User']['first_name'] . " " . $suppAdmin['User']['last_name']; ?></strong> </div></div>
                                <div class="row mrgn-0"><div class="span4"><strong><?php echo $language_based_content['email_people_section']; ?></strong> </div> <div class="span4"><strong><?php echo $suppAdmin['User']['email']; ?> </strong></div></div>

                                <div class="input-group">
                                    <input autocomplete="off" class="size-big" id="user_password<?php echo $suppAdmin['User']['id']; ?>" name="password"  placeholder="<?php echo $language_based_content['new_password_placeholder_people_section']; ?>" size="30" type="password">
                                    <label><?php echo $language_based_content['password_question_people_section']; ?></label>
                                </div>

                                <div class="input-group">
                                    <input class="size-big" id="user_password_confirmation<?php echo $suppAdmin['User']['id']; ?>" name="user[password_confirmation]"  placeholder="<?php echo $language_based_content['confirm_password_placeholder_people_section']; ?>" size="30" type="password">
                                    <label><?php echo $language_based_content['password_question_people_section']; ?></label>
                                </div>
                                <p id="msg_err<?php echo $suppAdmin['User']['id']; ?>" style="color:red;"></p>
                                <div id="msg_succ<?php echo $suppAdmin['User']['id']; ?>" class="message success pass_success" style="cursor: pointer; display:none;"></div>
                            </div>

                            <p>
                                <button class="btn btn-blue inline" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" id="<?php echo $suppAdmin['User']['id']; ?>" onclick="changePassword(this);"><?php echo $language_based_content['submit_people_section_button']; ?></button>
                                <a id="changePassword_cancel<?php echo $suppAdmin['User']['id']; ?>"  class="btn btn-transparent" data-dismiss="modal"><?php echo $language_based_content['cancel_people_section_button']; ?></a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>

            <div id="messageParticipant<?php echo $suppAdmin['User']['id']; ?>" class="modal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="header">
                            <h4 class="header-title nomargin-vertical smargin-bottom"><?php echo $language_based_content['message_to_viewer_people_section']; ?></h4>
                            <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                        </div>
                        <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/send_user_messages' ?>" class="new_message" enctype="multipart/form-data" id="new_message_<?php echo $suppAdmin['User']['id']; ?>" method="post">
                            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                            <div class="input-group mmargin-top">
                                <div id="editor<?php echo $suppAdmin['User']['id']; ?>-toolbar" class="editor-toolbar">
                                    <a data-wysihtml5-command="bold"><?php echo $language_based_content['bold_people_section']; ?></a>
                                    <a data-wysihtml5-command="italic"><?php echo $language_based_content['italic_people_section']; ?></a>
                                    <a data-wysihtml5-command="insertOrderedList">ol</a>
                                    <a data-wysihtml5-command="insertUnorderedList">ul</a>
                                    <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote"><?php echo $language_based_content['quote_people_section']; ?></a>
                                </div>
                                <?php echo $this->Form->textarea('message', array('id' => 'editor-' . $suppAdmin['User']['id'], 'class' => 'editor-textarea', 'style' => 'width:530px', 'cols' => '65', 'required' => 'required', 'placeholder' => $language_based_content['message_people_section'].' ...')); ?>
                            </div>
                            <?php echo $this->Form->input('sender_id', array('id' => 'sender_id_ajax_viewer_' . $suppAdmin['User']['id'], 'type' => 'hidden', 'value' => $user_id)); ?>
                            <?php echo $this->Form->input('recipient_id', array('id' => 'recipient_id_ajax_viewer_' . $suppAdmin['User']['id'], 'type' => 'hidden', 'value' => $suppAdmin['User']['id'])); ?>
                            <?php echo $this->Form->input('message_type', array('id' => 'message_type_ajax_viewer_' . $suppAdmin['User']['id'], 'type' => 'hidden', 'value' => '3')); ?>
                            <p>
                                <button class="btn btn-blue inline" type="submit"><?php echo $language_based_content['submit_people_section_button']; ?></button>
                                <a id="attachment_cancel<?php echo $suppAdmin['User']['id']; ?>"  class="btn btn-transparent" data-dismiss="modal"><?php echo $language_based_content['cancel_people_section_button']; ?></a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
            <?php
            $count++;
        endforeach;
    else:
        if ($request_type == '1'):
            ?>
            <li><?php echo $language_based_content['no_viewers_added_people_section']; ?></li>
        <?php else: ?>
            <li><?php echo $language_based_content['no_viewers_matched_people_section']; ?></li>
        <?php endif;
    endif;
    ?>
</ul>


