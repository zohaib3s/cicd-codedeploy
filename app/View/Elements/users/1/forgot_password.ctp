<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo $this->Custom->get_site_settings('site_title') ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style>
            img { max-width:100%;}
        </style>
    </head>
    <?php
    if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'];
    } else {
        $redirect = 'http://' . $_SERVER['HTTP_HOST'];
    }
    ?>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';">
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                        <tr>
                            <td style="text-align:center; padding:20px 0;">
                                <img src="<?php echo $redirect . '/img/logo-dark.png'; ?>" width="180" height="87"/>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; font-size:16px; padding:20px 0;">
                                <b> <?php echo $data['User']['first_name'] . ' ' . $data['User']['last_name'] ?></b><br />
                                your username is <br/> <b style="color:#222 !important; text-decoration: none;"><?php echo $data['User']['username'] ?></b> <br />

                            </td>

                        </tr>

                        <tr>
                            <td style="text-align:center; padding:20px 0;">
                                <a href="<?php echo $reset_pass_url ?>"><img src="<?php echo $redirect . '/img/reset_pass.png' ?>" width="330" height="50" /></a>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>

                        <tr>
                            <td style="font-size: 12px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif;">
                                <b style="font-size: 13px;">Didn't ask to reset your password? </b><br /><br />
                                <?php echo str_replace("th at's","that's","If you didn't ask to reset your password, it's likely that another user entered your username or email address by mistake while trying to reset their password. If th at's the case, you don't need to take any other action and can safely disregard this email.");?>
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>