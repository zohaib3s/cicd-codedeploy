<div class="row" style="margin-left: 0px;">
    <?php if ($userGroups): ?>
        <?php $i = 1; ?>
        <?php foreach ($userGroups as $group): ?>               
            <div class="span5 groups_panel">
                <div class="right">
                    <a href="<?php echo $this->base . '/users/editGroups/' . $group['group_id']; ?>" class="icon-pen" rel="tooltip" title="Edit"></a>
                    <a href="<?php echo $this->base . '/users/deleteGroups/' . $group['group_id']; ?>" class="icon-trash" data-confirm="Are you sure you want to delete this group?" data-method="delete" rel="tooltip nofollow" title="Remove"></a>
                </div>
                <h3 class="nomargin-top" style="min-height: 21px;"><?php echo $group['department_name']; ?></h3>
                
                <style>
                    .users_table{
                        margin-bottom: 20px;
                        margin-top: 10px;
                        border: 1px solid #cecece;
                        border-radius: 3px;
                    }
                    .users_table tbody, .users_table thead
                    {
                        display: block;
                    }

                    .users_table tbody 
                    {
                       overflow: auto;
                       height: 200px;
                    }
                    .permissions-search-box{
                        padding:0px;
                    }
                    #grous-list{
                        margin-top: 22px;
                    }
                
                </style>
                
                <div class="users_table">

                    <table class="center-last">
                        <thead>
                            <tr>
                                <th style="width:285px;">Name</th>
                                <th style="width:50px;text-align: center;">Remove</th>
                            </tr>
                        </thead>
                        <?php if (isset($group['users']) && count($group['users']) > 0): ?>
                        <tbody>
                            <?php foreach ($group['users'] as $user): ?>
                                <?php
                                $role = '';
                                if ($user['role'] == '120') {
                                    $role = '(U)';
                                } elseif ($user['role'] == '110') {
                                    $role = '(SU)';
                                }
                                ?>
                                
                                    <tr>
                                        <td style="width:285px;">
                                            <b><?php echo $user['first_name'] . " " . $user['last_name']; ?> </b> 
                                            <span class="smaller"> <?php echo $role; ?></span>
                                        </td>
                                        <td style="width:78px;">
                                            <a href="<?php echo $this->base . '/users/deleteGroupUsers/' . $user['user_group_id'] ?>" class="icon-remove" data-confirm="Are you sure you want to remove this admin from group?" data-method="delete" rel="nofollow"></a>
                                        </td>
                                    </tr>                       
                            <?php endforeach; ?>
                             </tbody>
                        <?php else: ?>
                            <tbody>
                                <tr>
                                    <td style="width:378px;"><strong>No User found.</strong></td>
                                </tr>
                            </tbody>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
            <?php $i++; ?>
        <?php endforeach; ?>
    <?php else: ?>
        <div style="margin-left: 20px;">No Group matched this search. Try another search.</div>
    <?php endif; ?>

</div>