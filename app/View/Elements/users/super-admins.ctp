<?php $users = $this->Session->read('user_current_account'); ?>
<div rel="super-admins" class="rel">
    <?php if ($users['roles']['role_id'] == '100' || $users['roles']['role_id'] == '110'): ?>
        <a class="btn btn-green right" data-toggle="modal" data-target="#addSuperAdminModal" href="#"><span class="plus">+</span> Add Super Admin</a>
    <?php endif; ?>
    <h2 class="inline">Super Admins</h2>
    <a class="appendix appendix--info" href="#"></a>

    <div class="appendix-content down">
        <h3>Info</h3>
        <p>Super Admins can do just about everything the Account Owner can aside from accessing and changing the account settings. This means they can create, edit and delete huddles and upload videos related documents to the account video library. They can even add and remove Users and Super Users from the account. We recommend giving Super User privileges to people you really trust.</p>
    </div>

    <div style="padding: 0px; float: right; width: 244px; margin-right: 20px;" class="search-box">
        <div  id="super-user-container" class="filterform"> </div>
    </div>

    <ul class="users row-fluid" id="super-user-list-containers">
        <?php if ($supperAdmins): $count = 1; ?>
            <?php
            foreach ($supperAdmins as $suppAdmin):

                $class = '';
                if ($count % 2 == 0) {
                    $class = 'last';
                }
                $glow = '';
                if ($suppAdmin['User']['type'] == 'Active')
                    $glow = 'glow2';
                else
                    $glow = 'glow3';

                $pictureUrl = '';
                if ($users['accounts']['in_trial'] != 1) {
                    if ($suppAdmin['User']['type'] == 'Active') {
                        $pictureUrl = $this->base . '/Users/editUser/' . $suppAdmin['User']['id'] . '/1';
                    } else {
                        $pictureUrl = $this->base . '/permissions/assign_user/' . $suppAdmin['users_accounts']['account_id'] . '/' . $suppAdmin['User']['id'];
                    }
                }
                ?>
                <li class="span4 last <?php echo $glow; ?>">
                    <?php if (isset($suppAdmin['User']['image']) && $suppAdmin['User']['image'] != ''): ?>
                        <?php
                        $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $suppAdmin['User']['id'] . "/" . $suppAdmin['User']['image'], $suppAdmin['User']['image']);
                        ?>
                        <a href="#">
                            <?php echo $this->Html->image($avatar_path, array('alt' => $suppAdmin['User']['first_name'] . " " . $suppAdmin['User']['last_name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53', 'align' => 'left')); ?>
                        </a>
                    <?php else: ?>
                        <a href="#">
                            <img width="53" height="53" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo" alt="Photo-default">
                        </a>
                    <?php endif; ?>
                    <?php
                    $full_name = $suppAdmin['User']['first_name'] . ' ' . $suppAdmin['User']['last_name'];
                    $user_job = $suppAdmin['User']['title'];
                    ?>
                    <span class="user-name"><?php echo strlen($full_name) > 20 ? '<span rel="tooltip" data-original-title="' . $full_name . '">' . mb_substr($full_name, 0, 20) . "...</span>" : $full_name; ?></span>

                    <p class="user-job"><?php echo strlen($user_job) > 20 ? '<span rel="tooltip" data-original-title="' . $user_job . '">' . mb_substr($user_job, 0, 20) . "...</span>" : $user_job; ?></p>

                    <p class="actions">
                        <?php if ($suppAdmin['User']['id'] != $users['User']['id']): ?>
                            <a rel="tooltip" class="icon-email blue-link" data-toggle="modal" data-target="#messageParticipant<?php echo $suppAdmin['User']['id']; ?>" href="#" data-original-title="<?php echo $suppAdmin['User']['email']; ?>"></a>
                        <?php endif ?>
                        <a rel="tooltip" class="icon-locked" href="<?php echo $this->base . '/permissions/assign_user/' . $suppAdmin['users_accounts']['account_id'] . '/' . $suppAdmin['User']['id'] ?>" data-original-title="permission and privileges"></a>
                        <a rel="tooltip" href="<?php echo $this->base . '/Users/deleteAccount/' . $adminsUsers['User']['id'] . '/' . $adminsUsers['users_accounts']['account_id']; ?>" class="icon-trash" data-confirm="Are you sure you want to delete this user? If yes, the huddles they created will be transferred to the account owner." data-method="delete" rel="tooltip nofollow" data-original-title="Delete User" style="margin-top: 2px;"></a>
                    </p>
                </li>
                <div id="messageParticipant<?php echo $suppAdmin['User']['id']; ?>" class="modal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="header">
                                <h4 class="header-title nomargin-vertical smargin-bottom">Message To Super User</h4>
                                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                            </div>
                            <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/send_user_messages' ?>" class="new_message" enctype="multipart/form-data" id="new_message_5" method="post">
                                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                                <div class="input-group mmargin-top">
                                    <div id="editor<?php echo $suppAdmin['User']['id']; ?>-toolbar" class="editor-toolbar">
                                        <a data-wysihtml5-command="bold">bold</a>
                                        <a data-wysihtml5-command="italic">italic</a>
                                        <a data-wysihtml5-command="insertOrderedList">ol</a>
                                        <a data-wysihtml5-command="insertUnorderedList">ul</a>
                                        <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                                    </div>
                                    <?php echo $this->Form->textarea('message', array('id' => 'editor-' . $suppAdmin['User']['id'], 'class' => 'editor-textarea', 'style' => 'width:530px', 'cols' => '65', 'required' => 'required', 'placeholder' => 'Message ...')); ?>
                                </div>
                                <?php echo $this->Form->input('sender_id', array('id'=>'sender_id_sa_'.$suppAdmin['User']['id'],'type' => 'hidden', 'value' => $user_id)); ?>
                                <?php echo $this->Form->input('recipient_id', array('id'=>'recipient_id_sa_'.$suppAdmin['User']['id'], 'type' => 'hidden', 'value' => $suppAdmin['User']['id'])); ?>
                                <?php //echo $this->Form->input('attachment', array('id' => 'message_attachment' . $suppAdmin['User']['id'], 'type' => 'file', 'value' => $suppAdmin['User']['id'], 'placeholder' => 'attach file')); ?>
                                <?php echo $this->Form->input('message_type', array(''=>'message_type_sa_'.$suppAdmin['User']['id'],'type' => 'hidden', 'value' => '3')); ?>

                                <p>
                                    <!---<a id="attachment-file<?php echo $suppAdmin['User']['id']; ?>" class="btn btn-white btn-light btn-gray-text icon2-clip inline"> Attach File</a>-->
                                    <button class="btn btn-blue inline" type="submit">Send</button>
                                    <a id="attachment_cancel<?php echo $suppAdmin['User']['id']; ?>"  class="btn btn-transparent">Cancel</a>
                                </p>
                            </form>
                            <script type="text/javascript">
                                $(document).ready(function () {

                                    $("#message_attachment<?php echo $suppAdmin['User']['id']; ?>").hide();
                                    $("#attachment_cancel<?php echo $suppAdmin['User']['id']; ?>").hide();
                                    $("#attachment-file<?php echo $suppAdmin['User']['id']; ?>").click(function () {
                                        $("#message_attachment<?php echo $suppAdmin['User']['id']; ?>").show("slow");
                                        $("#attachment_cancel<?php echo $suppAdmin['User']['id']; ?>").show("slow");
                                        $(this).hide();
                                    });

                                    $("#attachment_cancel<?php echo $suppAdmin['User']['id']; ?>").click(function () {
                                        /*For IE*/
                                        $("#message_attachment<?php echo $suppAdmin['User']['id']; ?>").replaceWith($("#message_attachment<?php echo $suppAdmin['User']['id']; ?>").clone(true));
                                        /*For other browsers*/
                                        $("#message_attachment<?php echo $suppAdmin['User']['id']; ?>").val("");

                                        $("#message_attachment<?php echo $suppAdmin['User']['id']; ?>").hide();
                                        $("#attachment-file<?php echo $suppAdmin['User']['id']; ?>").show("slow");
                                        $(this).hide();

                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
                <?php
                $count++;
            endforeach;
        else:
            ?>
            <li> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
        <?php endif; ?>
    </ul>
    <hr class="full--slim dashed">
</div>
