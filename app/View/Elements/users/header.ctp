<div rel="super-admins" class="rel">
    <a class="btn btn-green right" data-toggle="modal" data-target="#addSuperAdminModal" href="#"><span class="plus">+</span> Add Super User</a>

    <h2 class="inline">Super Users</h2>
    <a class="appendix appendix--info" href="#"></a>

    <div class="appendix-content down">
        <h3>Info</h3>

        <p>Super Users can do just about everything the account owner can aside from accessing and changing the account
            settings. This means they can create, edit, and delete Huddles. They can
            even add and delete other Users and Super Users from the account. We recommend only granting Super Users to
            people you really trust (e.g. leadership team or teacher leaders). </p>
    </div>
    <ul class="users row-fluid glow">
        <?php if ($supperAdmins): $count = 1; ?>
            <?php
            foreach ($supperAdmins as $suppAdmin):
                $class = '';
                if ($count % 2 == 0) {
                    $class = 'last';
                }
                ?>
                <li class="span4 last <?php //echo $class;    ?>">
                    <a href="#">
                        <img width="53" height="53" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo" alt="Photo-default">
                    </a>

                    <span class="user-name"><?php echo $suppAdmin['Users']['first_name'] . ' ' . $suppAdmin['Users']['last_name']; ?></span>

                    <p class="user-job"></p>

                    <p class="actions">
                        <a rel="tooltip" class="icon-email blue-link" data-reveal-id="messageParticipant<?php echo $suppAdmin['Users']['id']; ?>" href="#" data-original-title="<?php $suppAdmin['Users']['email']; ?>"></a>
                        <a rel="tooltip" class="icon-locked" href="#" data-original-title="permission and privileges"></a>
                    </p>
                </li>
                <div id="messageParticipant<?php echo $suppAdmin['Users']['id']; ?>" class="modal">
                    <a class="close-reveal-modal close style2" data-dismiss="modal">&#215;</a>
                    <h2 class="nomargin-top">Message User</h2>
                    <form accept-charset="UTF-8" action="<?php echo $this->base . '/messages/send' ?>" class="new_message" enctype="multipart/form-data" id="new_message_3" method="post">
                        <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                        <div class="input-group mmargin-top">
                            <div id="editor2-toolbar" class="editor-toolbar">
                                <a data-wysihtml5-command="bold">bold</a>
                                <a data-wysihtml5-command="italic">italic</a>
                                <a data-wysihtml5-command="insertOrderedList">ol</a>
                                <a data-wysihtml5-command="insertUnorderedList">ul</a>
                                <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                            </div>
                            <?php echo $this->FORM->textarea('message', array('id' => 'editor-2', 'class' => 'editor-textarea', 'cols' => '45', 'placeholder' => 'Message to User(s) participating in Huddle...(Optional)')); ?>
                        </div>
                     <?php echo $this->FORM->input('sender_id', array('type' => 'hidden', 'value' => $user_id)); ?>
                        <?php echo $this->FORM->input('recipient_id', array('type' => 'hidden', 'value' => $suppAdmin['Users']['id'])); ?>
                        <?php echo $this->FORM->input('attachment', array('type' => 'file', 'value' => $suppAdmin['Users']['id'], 'placeholder' => 'attach file')); ?>
                        <?php echo $this->FORM->input('message_type', array('id'=>'message_type_h','type' => 'hidden', 'value' => '3')); ?>

                        <p>
                            <a id="attachment-file<?php echo $suppAdmin['Users']['id']; ?>" class="btn btn-white btn-light btn-gray-text icon2-clip inline"> Attach File</a>
                            <button class="btn btn-blue inline" type="submit">Send</button>
                            <a id="attachment_cancel<?php echo $suppAdmin['Users']['id']; ?>"  class="btn btn-transparent">Cancel</a>
                        </p>
                    </form>
                    <script type="text/javascript">

                        $(document).ready(function() {

                            $("#message_attachment<?php echo $suppAdmin['Users']['id']; ?>").hide();
                            $("#attachment_cancel<?php echo $suppAdmin['Users']['id']; ?>").hide();
                            $("#attachment-file<?php echo $suppAdmin['Users']['id']; ?>").click(function() {
                                $("#message_attachment<?php echo $suppAdmin['Users']['id']; ?>").show("slow");
                                $("#attachment_cancel<?php echo $suppAdmin['Users']['id']; ?>").show("slow");
                                $(this).hide();
                            });

                            $("#attachment_cancel<?php echo $suppAdmin['Users']['id']; ?>").click(function() {
                                /*For IE*/
                                $("#message_attachment<?php echo $suppAdmin['Users']['id']; ?>").replaceWith($("#message_attachment<?php echo $suppAdmin['Users']['id']; ?>").clone(true));
                                /*For other browsers*/
                                $("#message_attachment<?php echo $suppAdmin['Users']['id']; ?>").val("");
                                $("#message_attachment<?php echo $suppAdmin['Users']['id']; ?>").hide();
                                $("#attachment-file<?php echo $suppAdmin['Users']['id']; ?>").show("slow");
                                $(this).hide();

                            });
                        });
                    </script>

                </div>
                <?php
                $count++;
            endforeach;
            ?>

        <?php endif; ?>
    </ul>
    <hr class="full--slim dashed">
</div>
