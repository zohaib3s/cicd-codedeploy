<?php
$amazon_base_url = Configure::read('amazon_base_url');
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$user_id = $users['User']['id'];
$account_id = $users['accounts']['account_id'];
$isFirefoxBrowser = $this->Browser->isFirefox();
?>
<style>
    .observationCls1 { padding-left: 0; margin-left: 0px;}
    .observationCls1 li{ margin-bottom: 5px;    display: inline-block;}
    .observationCls1 li .tab-3-box-left{    margin-bottom: 15px;}
     button.publish_obser1{
        background: #2dcc70;
    padding: 4px 12px;
    color: #fff !important;
    text-decoration: none;
    position: relative;
    top: -10px;
    margin-left: 8px;
    border:0px;
    }
</style>
<div id="ob-videos-list">
    <p id="notification" style="display: none;padding-left: 29px;padding-right: 9px;padding-bottom: 0px;"></p>
    <ul id="ulDiscussions" class="observationCls1 observation_detail" id="ob-videos-list">
        <?php if (is_array($videos) && count($videos) > 0): ?>
            <?php foreach ($videos as $row): ?>

                <?php
                if ($row['Document']['current_duration'] > 0 && $row['Document']['is_associated'] == 0 && $huddle_permission == '210' && $row['Document']['is_processed'] > 0) {
                    $bool = 1;
                } elseif ($huddle_permission == '210' && $row['Document']['current_duration'] == 0 && $row['Document']['is_associated'] == 0) {
                    $bool = 1;
                } else {
                    $bool = 0;
                }
                ?>

                <?php if ($bool == 0): ?>



                    <li>
                        <div class="tab-3-box">
                            <div class="tab-3-box-left">
                                <h2><a class="wrap" id="vide-title-<?php echo $row['Document']['id']; ?>"
                                    <?php
                                    if ($row['Document']['current_duration'] > 0 && $row['Document']['is_processed'] == 4 && $row['Document']['is_associated'] > 0) {
                                        echo 'href="' . $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['is_associated'] . '"';
                                    } elseif ($row['Document']['current_duration'] > 0) {
                                        echo 'href="' . $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $row['Document']['id'] . '"';
                                    } else {
                                        echo 'href="' . $this->base . '/video_details/scripted_observations/' . $huddle_id . '/' . $row['Document']['id'] . '"';
                                    }
                                    ?>
                                       title="<?php echo $row['afd']['title'] ?>" ><?php echo (strlen($row['afd']['title']) > 52 ? mb_substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title']) ?> </a></h2>
                                <div class="frame"><?php if (isset($users['User']['image']) && $users['User']['image'] != ''): ?>
                                        <?php
                                        $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
                                        echo $this->Html->image($chimg, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '42', 'width' => '42', 'align' => 'left'));
                                        ?>
                                    <?php else: ?>
                                        <img width="42" height="42"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo photo inline" rel="image-uploader" alt="Photo-default">
                                    <?php endif; ?></div>
                                <div class="text"> <strong class="title"><?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?></strong>

                                    <div style="float: right" class="docs-box">

                                    </div>
                                    <div style="clear: both;"></div>
                                    <div>
                                        <strong class="posted">Posted <?php echo date('M d, Y', strtotime($row['Document']['created_date'])) ?></strong>
                                        <?php if ($huddle_permission == '200') { ?>
                                            <a rel="nofollow" data-confirm="Are you sure you want to delete this Observation?" id="delete-main-observation" class="icon2-trash right smargin-right" href="javascript:deleteObservation('<?php echo $huddle_id; ?>', '<?php echo $row['Document']['id']; ?>');" style="border: none !important;background: transparent !important;color: #808080;font-weight: normal;text-decoration: initial;float: left;margin-top: -15px;"></a>
                                        <?php } ?>
                                        <?php if ($row['Document']['current_duration'] > 0 && $row['Document']['is_processed'] == 4 && $row['Document']['is_associated'] == 0 && $row['Document']['upload_status'] != 'cancelled' && $row['Document']['published'] == 1) : ?>
                                            <button id ="publish-observations_<?php echo $row['Document']['id'] ?>" class="publish_obser1" href="javascript:void(0)" ><?php echo $language_based_content['publish_observation_for_coachee']; ?></button>
                                        <?php elseif ($row['Document']['current_duration'] == 0 && $row['Document']['is_associated'] == 0 && $row['Document']['published'] == 0) : ?>
                                            <a class="publish_obser1" href="<?php echo $this->base . '/Huddles/publish_scripted_observation/' . $huddle_id . '/' . $row['Document']['id'] ?>" ><?php echo $language_based_content['publish_observation_for_coachee']; ?></a>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php if ($row['Document']['current_duration'] > 0 || $row['Document']['scripted_current_duration'] === NULL) {
                            ?>
                            <div class="video_icon"> <img src="/app/img/video_color.png"></div>
                            <?php
                        } else {
                            ?> <div class="video_icon"> <img src="/app/img/scripted_observation.png"></div>
                            <?php }
                            ?> <div  style="position: absolute;right: 15px;top: 93px;">
                            <?php if ($row['Document']['current_duration'] > 0 && $row['Document']['is_processed'] == 4 && $row['Document']['is_associated'] > 0): ?>
                                <?php echo $language_based_content['published_view']; ?>
                            <?php elseif ($row['Document']['current_duration'] == 0 && $row['Document']['is_associated'] == 1): ?>
                                <?php echo $language_based_content['published_view']; ?>
                            <?php endif; ?>
                        </div>

                        <?php
                        if ($row['Document']['current_duration'] > 0 && $row['Document']['is_processed'] != 4 && $row['Document']['is_associated'] < 1) {
                            ?>

                            <div class="live_record" style="position: absolute;right: 15px;top: 93px;">
                                <img src="/app/webroot/img/recording_img.png" /> <?php echo $language_based_content['live_recording_view']; ?>
                            </div>
                        <?php } ?>
                    </li>

                    <?php
                    $videoID = $row['Document']['id'];

                    $document_files_array = $this->Custom->get_document_url($row['Document']);

                    if (empty($document_files_array['url'])) {
                        $row['Document']['published'] = 0;
                        $document_files_array['url'] = $row['Document']['original_file_name'];
                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                    } else {
                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        @$row['Document']['duration'] = $document_files_array['duration'];
                    }

                    $videoFilePath = pathinfo($document_files_array['url']);
                    $videoFileName = $videoFilePath['filename'];
                    ?>
                    <li style="display:none;" class="videos-list__item">
                        <?php
                        $isEditable = ($huddle_permission == 200) ||
                                ($huddle_permission == 210 && $user_current_account['User']['id'] == $row['Document']['created_by']) ||
                                ($user_current_account['User']['id'] == $row['Document']['created_by'] && $user_permissions['UserAccount']['permission_maintain_folders'] == '1');
                        ?>
                        <?php if ($isEditable): ?>

                        <?php endif; ?>
                        <div style="display:none;" class="clearfix"></div>
                        <div style="display:none;" class="videos-list__item-thumb">
                            <?php
                            if ($row['Document']['published'] == '1'):

                                $seconds = $row['Document']['duration'] % 60;
                                $minutes = ($row['Document']['duration'] / 60) % 60;
                                $hours = gmdate("H", $row['Document']['duration']);
                                ?>
                                <a href="<?php echo $this->base . '/Huddles/observation_details/' . $huddle_id . '/' . $row['Document']['id'] ?>"  data-document-id="<?php echo $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                    <?php
                                    $thumbnail_image_path = $document_files_array['thumbnail'];

                                    echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                    ?>
                                    <div class="play-icon"></div>
                                    <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                                </a>
                            <?php else: ?>
                                <a href="<?php echo $this->base . '/Huddles/observation_details/' . $huddle_id . '/' . $row['Document']['id'] ?>"  data-document-id="<?php echo $row['Document']['id']; ?>" id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                    <div  class="video-unpublished ">
                                        <span class="huddles-unpublished" style="color:#fff;">
                                            <?php if ($row['Document']['encoder_status'] == 'Error'): ?>
                                                Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                            <?php else : ?>
                                                Video is not published.
                                            <?php endif; ?>
                                        </span>
                                    </div>
                                </a>
                            <?php endif; ?>
                        </div>
                    </li>

                <?php endif; ?>

                <script>

                   $('#publish-observations_<?php echo $row['Document']['id'] ?>').click(function(e){
                        $('#count_ob').val($('#count_ob_temp').val());
                        $(this).attr('disabled',true);
                        e.preventDefault();
                        video_id = '<?php echo $row['Document']['id']; ?>';
                        huddle_id = '<?php echo $huddle_id; ?>';
                        user_id = '<?php echo $user_id ?>';
                        account_id = '<?php echo $account_id; ?>';

                        $.ajax({
                            type: "POST",
                            url: home_url + "/Huddles/publish_observation",
                            dataType: 'json',
                            data: {
                                account_id: account_id,
                                user_id: user_id,
                                huddle_id: huddle_id,
                                video_id: video_id
                            },
                            success: function (response) {
                                // console.log(response.document_id);
                                //json_decode(response);
                                var new_id = response.document_id;
                                // alert(new_id);
                                window.location.href = home_url + '/Huddles/view/' + huddle_id + '/' + '1/' + new_id;
                            },
                            async: false,
                            
                            error: function (e) {

                            }
                        });
                    });
                </script>


            <?php endforeach; ?>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            $('.regen-thumbnail-image').click(function () {

                var r = confirm('Do you want to Regenerate Thumbnail?');
                if (r == true) {

                    var document_id = $(this).attr('document_id');
                    var huddle_id = $(this).attr('huddle_id');
                    $.ajax({
                        type: 'POST',
                        url: home_url + '/Huddles/regenerate_thumbnail/' + document_id + '/' + huddle_id,
                        success: function (res) {

                            alert('Thumbnail generated successfully.');
                            d = new Date();
                            var src_image = $("#img_" + document_id).attr("src");
                            $("#img_" + document_id).attr("src", src_image + "&dd=" + d.getTime());
                        },
                        errors: function (response) {
                            alert('Unable to generate Thumbnail, please try again later.');
                        }
                    });
                }

            });
        });
    </script>

<?php else: ?>
    <li class="videos-list__item_noitem">
        <div style="margin-left: -15px;"><?php echo $language_based_content['none_of_bservation_match']?></div>
    </li>
<?php endif; ?>
<script>
    $('[rel~="tooltip"]').tooltip({container: 'body'});
</script>
