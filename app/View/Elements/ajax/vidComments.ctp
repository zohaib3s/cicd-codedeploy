<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
$account_folder_id = $huddle[0]['AccountFolder']['account_folder_id'];
$video_details = $this->Custom->get_video_details($video_id);

function formatSeconds($seconds) {
    $hours = 0;
    $milliseconds = str_replace("0.", '', $seconds - floor($seconds));

    if ($seconds > 3600) {
        $hours = floor($seconds / 3600);
    }
    $seconds = $seconds % 3600;


    return str_pad($hours, 2, '0', STR_PAD_LEFT)
            . gmdate(':i:s', $seconds)
            . ($milliseconds ? ".$milliseconds" : '');
}

if (!function_exists('_make_url_clickable_cb')) {

    function _make_url_clickable_cb($matches) {
        $ret = '';
        $url = $matches[2];

        if (empty($url))
            return $matches[0];
// removed trailing [.,;:] from URL
        if (in_array(substr($url, -1), array('.', ',', ';', ':')) === true) {
            $ret = substr($url, -1);
            $url = substr($url, 0, strlen($url) - 1);
        }
        return $matches[1] . "<a href=\"$url\" target=\"_blank\" rel=\"nofollow\">$url</a>" . $ret;
    }

}

if (!function_exists('_make_web_ftp_clickable_cb')) {

    function _make_web_ftp_clickable_cb($matches) {
        $ret = '';
        $dest = $matches[2];
        $dest = 'http://' . $dest;

        if (empty($dest))
            return $matches[0];
// removed trailing [,;:] from URL
        if (in_array(substr($dest, -1), array('.', ',', ';', ':')) === true) {
            $ret = substr($dest, -1);
            $dest = substr($dest, 0, strlen($dest) - 1);
        }
        return $matches[1] . "<a href=\"$dest\" target=\"_blank\" rel=\"nofollow\">$dest</a>" . $ret;
    }

}

if (!function_exists('_make_email_clickable_cb')) {

    function _make_email_clickable_cb($matches) {
        $email = $matches[2] . '@' . $matches[3];
        return $matches[1] . "<a href=\"mailto:$email\" target=\"_blank\">$email</a>";
    }

}

if (!function_exists('make_clickable')) {

    function make_clickable($ret) {

        $ret = str_replace("&nbsp;", " ", $ret);
        $ret = ' ' . $ret;
// in testing, using arrays here was found to be faster
        $ret = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_url_clickable_cb', $ret);
        $ret = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_web_ftp_clickable_cb', $ret);
        $ret = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', '_make_email_clickable_cb', $ret);

// this one is not in an array because we need it to run last, for cleanup of accidental links within links
        $ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
        $ret = trim($ret);
        return $ret;
    }

}

function defaulttagsclasses($posid) {
    $class_name = 'tags_qucls';
    if ($posid == 1) {
        $class_name = 'tags_qucls';
    } else if ($posid == 2) {
        $class_name = 'tags_sugcls';
    } else if ($posid == 3) {
        $class_name = 'tags_notescls';
    } else if ($posid == 4) {
        $class_name = 'tags_strangthcls';
    } else if ($posid == 5) {
        $class_name = 'tags_strangthcls';
    }
    return $class_name;
}

function defaulttagsclasses2($posid) {
    $class_name = 'tags_qucls';
    if ($posid == 1) {
        $class_name = 'tags_quclsbg';
    } else if ($posid == 2) {
        $class_name = 'tags_sugclsbg';
    } else if ($posid == 3) {
        $class_name = 'tags_notesclsbg';
    } else if ($posid == 4) {
        $class_name = 'tags_strangthclsbg';
    } else if ($posid == 5) {
        $class_name = 'tags_strangthclsbg';
    }
    return $class_name;
}

function _gettagclass($comment_tags, $default_tags) {
    $return = -1;
    foreach ($comment_tags as $comment_tag) {
        foreach ($default_tags as $key => $default_tag_value) {
            if (!empty($comment_tag['account_tags']['account_tag_id'])) {
                if ($default_tag_value['AccountTag']['account_tag_id'] == $comment_tag['AccountCommentTag']['account_tag_id']) {
                    $return = $key;
                    break;
                }
            }
        }
    }
    $return = $return + 1;
    return $return;
}
?>
<div id="vidCommentsNew" style="height: 615px;overflow-y: auto;">

    <script type="text/javascript">
<?php
$timeS = array();
if ($videoComments) {
    foreach ($videoComments as $cmt) {
        if (!empty($cmt['Comment']['time']))
            $timeS[] = $cmt['Comment']['time'];
    }
}
?>
        $('#example_video_<?php echo $video_id ?>_html5_api').attr('data-markers', '[<?php echo implode(',', $timeS); ?>]');</script>
    <?php $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']); ?>
    <?php if ($huddle_permission == '200' || $huddle_permission == '210' || ($isCreator && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')) : ?>
        <?php if ($type != 'get_video_comments_obs') { ?>
            <?php
            $comment_box_display = 'style="display:block;"';
            $comment_box_add_btn = 'style="display:none;"';
            ?>
            <div id="comment_form" <?php echo $comment_box_display; ?>>
                <form accept-charset="UTF-8" action="<?php echo $this->base; ?>/Huddles/addComments" class="new_comment" id="comments-form1" method="post" style="display: none;">
                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <input id="tokentag" type="hidden" name="authenticity_token" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" />
                    <div class="input-group">
                        <textarea cols="50" id="comment_comment" required name="comment[comment]" placeholder="Add a comment..." rows="4"></textarea>
                    </div>
                    <input id="comment_access_level" name="comment[access_level]" type="hidden" value="" />
                    <input id="synchro_time" name="synchro_time" type="hidden" value="{:value=&gt;&quot;&quot;}" />
                    <input id="synchro_time_class" name="synchro_time_class" type="hidden" value="{:value=&gt;&quot;&quot;}" />
                    <input type="hidden" id="videoId" name="videoId" value="<?php echo $video_id ?>">
                    <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id ?>">

                    <div id="comment-for" class="input-group">
                        For:
                        <label for="for_synchro_time">
                            <input id="for_synchro_time"  name="for" type="radio" value="synchro_time" /> Time-specific (<span id="right-now-time">0:00</span>)
                        </label>

                        <label for="for_entire_video">
                            <input id="for_entire_video"  name="for" type="radio" value="entire_video" checked/> Whole video
                        </label>
                    </div>

                    <div class="input-group">
                        <input type="submit" name="submit" value="Add Comment" class="btn btn-green">
                        <input type="hidden" name="type" value="add_video_comments"/>
                        <a id="close_comment_form" class="btn btn-transparent js-close-comments-form">Cancel</a>
                    </div>
                </form>
                <div id="indicator" style="display:none;"><img alt="Indicator" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/indicator.gif'); ?>" /></div>
            </div>
        <?php } ?>
    <?php endif; ?>

    <a id="reload_button" style="display: none;"></a>
    <form accept-charset="UTF-8" action="<?php echo $this->base; ?>/Huddles/load_timecomment"
          data-remote="true" enctype="multipart/form-data" id="time_comment_form"
          method="post" name="time_comment_form"
          >

        <div style="margin:0;padding:0;display:inline">
            <input name="utf8" type="hidden" value="&#x2713;" />
        </div>
        <input id="synchro_time" name="synchro_time" type="hidden" value="{:value=&gt;&quot;&quot;}" />
        <input id="commentable_id" name="commentable_id" type="hidden" value="<?php echo $user_id ?>" />
        <input id="commentable_id" name="user_id" type="hidden" value="<?php echo $user_id ?>" />
        <input id="commentable_type" name="commentable_type" type="hidden" value="Video" />
        <input id="time_submit_button" name="commit" style="display: none" type="submit" value="Show Time Comment" />

    </form>
    <div id="indicator" style="display:none;"><img alt="Indicator" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/indicator.gif'); ?>" /></div>

    <?php if (isset($videoComments) && $videoComments != ''): ?>
        <?php
        $timeS = '';
        $video_comment_counter = 0;
        if (count($videoComments) < 1 && $searchCmt != '')
            echo '<li style="text-align:center;">None of your comments or tags matched this search.<br>Try another search.</li>';
        else
//            print_r($videoComments);
//            exit();
            foreach ($videoComments as $vidComments):

                $commentDate = $vidComments['Comment']['created_date'];
                $commentsDate = $this->Custom->formateDate($commentDate);
                $syn_class = _gettagclass($vidComments['default_tags'], $defaulttags);
                if ($syn_class == 0 || $vidComments['Comment']['time'] == 0) {
                    $syn_class = 'synchro';
                } else {
                    $syn_class = 'synchro_' . $syn_class;
                }
                ?>
                <ul class="comments comments_huddles" id="normal_comment_<?php echo $vidComments['Comment']['time'] ?>" style="overflow-x: hidden;">
                    <div class="comment_box_<?php echo $vidComments['Comment']['time'] ?>" rel="<?php echo $vidComments['Comment']['id'] ?>" id="comment_box_<?php echo $vidComments['Comment']['id'] ?>" style="margin-top: 12px;">
                        <li class="<?php echo $syn_class; ?>">
                            <div class="synchro-inner">
                                <i></i><span data-time="<?php echo $vidComments['Comment']['time']; ?>" class="synchro-time"><?php echo ($vidComments['Comment']['time'] == 0 ? "All" : formatSeconds($vidComments['Comment']['time']) ); ?></span>
                            </div>
                        </li>

                        <li class="comment thread">
                            <a href="#<?php //echo $this->base . '/users/editUser/' . $vidComments['Comment']['user_id'];                                                                                                                                                   ?>">
                                <?php if (isset($vidComments['User']['image']) && $vidComments['User']['image'] != ''): ?>
                                    <?php
                                    $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $vidComments['User']['user_id'] . "/" . $vidComments['User']['image']);
                                    echo $this->Html->image($chimg, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '21', 'width' => '21', 'align' => 'left'));
                                    ?>
                                <?php else: ?>
                                    <img width="21" height="21"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo photo inline" rel="image-uploader" alt="Photo-default">
                                <?php endif; ?>
                            </a>
                            <div class="comment-header"><?php echo $vidComments['User']['first_name'] . " " . $vidComments['User']['last_name']; ?></div>

                            <?php
                            if ($vidComments['Comment']['ref_type'] == '6') {
                                if (Configure::read('use_cloudfront') == true) {
                                    $url = $vidComments['Comment']['comment'];
                                    $videoFilePath = pathinfo($url);
                                    $videoFileName = $videoFilePath['filename'];
                                    $ext = '.' . $videoFilePath['extension'];
                                    if ($videoFilePath['dirname'] == ".") {

                                        $video_path = $this->Custom->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . $ext);
                                        $relative_video_path = $videoFileName . $ext;
                                    } else {

                                        $video_path = $this->Custom->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . $ext);
                                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . $ext;
                                    }
                                } else {
                                    $url = $vidComments['Comment']['comment'];
                                    $videoFilePath = pathinfo($url);
                                    $videoFileName = $videoFilePath['filename'];
                                    $ext = '.' . $videoFilePath['extension'];
                                    if ($videoFilePath['dirname'] == ".") {

                                        $video_path = $this->Custom->getSecureAmazonUrl($videoFileName . $ext);
                                        $relative_video_path = $videoFileName . $ext;
                                    } else {

                                        $video_path = $this->Custom->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName . $ext);
                                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . $ext;
                                    }
                                }
                                ?>
                                <div class="comment-body comment">
                                    <audio controls>
                                        <source src="<?php echo $video_path; ?>">

                                        Your browser does not support the audio element.
                                    </audio>

                                <?php } else {
                                    ?>
                                    <div class="comment-body comment more">  <?php
                                        echo nl2br($vidComments['Comment']['comment']);
                                    }
                                    ?>

                                </div>

                                <div class="comment-footer">
                                    <span class="comment-date">
                                        <?php echo $commentsDate; ?>
                                    </span>
                                    <?php if (!isset($comments_action)): ?>
                                        <div class="comment-actions" style="width: 150px;">
                                            <form id="delete-main-comments-frm<?php echo $vidComments['Comment']['id']; ?>" action="<?php echo $this->base . '/Huddles/deleteVideoComments/' . $vidComments['Comment']['id']; ?>" accept-charset="UTF-8">
                                                <?php if ($huddle_type == 3): ?>

                                                    <?php if (($huddle_permission == '200') || ($this->Custom->is_creator($user_current_account['User']['id'], $video_details['Document']['created_by']))): ?>
                                                        <?php if ($user_current_account['User']['id'] != $vidComments['Comment']['created_by']): ?>
                                                            <a class="comment-reply" style="cursor: pointer;" id="reply_button_<?php echo $vidComments['Comment']['id'] ?>">Reply</a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <?php if (($huddle_permission == '200' || $huddle_permission == '210') || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                        <?php if ($user_current_account['User']['id'] != $vidComments['Comment']['created_by']): ?>
                                                            <a class="comment-reply" style="cursor: pointer;" id="reply_button_<?php echo $vidComments['Comment']['id'] ?>">Reply</a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                    <!--                                    <a rel="nofollow"  id="edit-main-comments_<?php echo $vidComments['Comment']['id'] ?>" class="comment-edit"  style="cursor: pointer;">Edit</a>-->
                                                    <a   data-toggle="modal" data-target="#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>"  class="comment-edit"  style="cursor: pointer;">Edit</a>
                                                    <a rel="nofollow" data-confirm="Are you sure you want to delete this comment?" id="delete-main-comments" class="comment-delete" href="javascript:deleteComment('<?php echo $vidComments['Comment']['id']; ?>');">Delete</a>
                                                    <?php if ($this->Custom->match_timestamp_count_comment($vidComments['Comment']['id'], $video_id) != 0): ?>    <span class="timestamp_attachment_no" style="color: #5a80a0;cursor:pointer;"><img style="width:12px;" src = "<?php echo $this->webroot . 'img/comment_box_attachment.jpg'; ?>"> <?php echo $this->Custom->match_timestamp_count_comment($vidComments['Comment']['id'], $video_id); ?></span> <?php endif; ?>
                                                <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $vidComments['Comment']['created_by'])): ?>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--                                    <a rel="nofollow"  id="edit-main-comments_<?php echo $vidComments['Comment']['id'] ?>" class="comment-edit"  style="cursor: pointer;">Edit</a>-->
                                                    <a   data-toggle="modal" data-target="#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>"  class="comment-edit"  style="cursor: pointer;">Edit</a>
                                                    <a rel="nofollow" data-confirm="Are you sure you want to delete this comment?" id="delete-main-comments" class="comment-delete" href="javascript:deleteComment('<?php echo $vidComments['Comment']['id']; ?>');">Delete</a>
                                                    <?php if ($this->Custom->match_timestamp_count_comment($vidComments['Comment']['id'], $video_id) != 0): ?>     <span class="timestamp_attachment_no" style="color: #5a80a0;cursor:pointer;"><img style="width:12px;" src = "<?php echo $this->webroot . 'img/comment_box_attachment.jpg'; ?>"> <?php echo $this->Custom->match_timestamp_count_comment($vidComments['Comment']['id'], $video_id); ?></span>    <?php endif; ?>
                                                <?php endif; ?>
                                            </form>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <div id="docs-standards" style="margin-bottom: 20px;">
                                    <?php
                                    $show_all_button = false;
                                    $selected_tags = array();
                                    $total_tags = count($vidComments['default_tags']) + count($vidComments['account_tags']) + count($vidComments['standard']);
                                    if (isset($vidComments['default_tags']) && count($vidComments['default_tags']) > 0) {
                                        $count = 0;
                                        foreach ($vidComments['default_tags'] as $default_tag) {

                                            $show_all_button = true;
                                            if ($count > 0 && (count($vidComments['default_tags']) > 1 || count($vidComments['account_tags']) > 1)) {
                                                continue;
                                            }

                                            $selected_tags[] = $default_tag['AccountCommentTag']['account_comment_tag_id'];
                                            ?>
                                            <div id="tagFilterList">
                                                <div id="tagFilterContainer" class="btnwraper" style="margin-right:5px;">
                                                    <button class="btn btn-lilghtgray"># <?php echo empty($default_tag['account_tags']['tag_title']) ? $default_tag['AccountCommentTag']['tag_title'] : $default_tag['account_tags']['tag_title']; ?></button>

                                                    <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                        <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag1('<?php echo $default_tag['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                                    <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $vidComments['Comment']['created_by'])): ?>
                                                        <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag1('<?php echo $default_tag['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                                    <?php endif; ?>

                                                </div>
                                            </div>

                                            <?php if ($count == 0 && $show_all_button): ?>

                                                <div id="view-more-standar-<?php echo $vidComments['Comment']['id']; ?>" show-block-data="#standard-box-<?php echo $vidComments['Comment']['id']; ?>" style="    color: #0152a4; font-weight:400;"><?php echo ($total_tags - 1); ?> More <a  style="text-decoration: underline; color: #0152a4; font-weight:400;"  href="#">View All</a></div>
                                                <script type="text/javascript">
                                                    var $standard_view_more_btn = '<?php echo '#view-more-standar-' . $vidComments['Comment']['id'] ?>';
                                                    var $standar_view_box = '<?php echo '#standard-box-' . $vidComments['Comment']['id'] ?>';

                                                    $($standard_view_more_btn).on('click', function (e) {
                                                        show_id = $(this).attr('show-block-data');

                                                        $(show_id).show();
                                                        $(this).hide();
                                                        e.preventDefault();
                                                    });
                                                    $(document).on('click', '.standard-close-btn', function (e) {
                                                        show_id = $(this).attr('show-block-data_2');
                                                        show_all_button = $(this).attr('view-all-button');
                                                        $(show_id).hide();
                                                        $(show_all_button).show();
                                                        e.preventDefault();
                                                    })
                                                </script>
                                            <?php endif; ?>
                                            <?php
                                            $count++;
                                        }
                                    }
                                    ?>
                                    <?php
                                    $selected_standard = array();
                                    if (isset($vidComments['standard']) && count($vidComments['standard']) > 0) {
                                        $count = 0;

                                        foreach ($vidComments['standard'] as $standard) {
                                            if ($show_all_button == true) {
                                                continue;
                                            }
                                            if ($count > 0 && count($vidComments['standard']) > 1) {
                                                continue;
                                            }
                                            $selected_standard[] = $standard['account_tags']['account_tag_id'];
                                            ?>

                                            <div id="tagFilterList">
                                                <div id="tagFilterContainer" class="btnwraper" style="margin-right:5px;">
                                                    <button class="btn btn-lilghtgray"># <?php echo $standard['account_tags']['tag_code'] . '-' . $standard['account_tags']['tag_title']; ?></button>
                                                    <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                        <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag1('<?php echo $standard['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                                    <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $vidComments['Comment']['created_by'])): ?>
                                                        <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag1('<?php echo $standard['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <?php if ($count == 0 && count($vidComments['standard']) > 1): ?>
                                                <div id="view-more-standar-<?php echo $vidComments['Comment']['id']; ?>" show-block-data="#standard-box-<?php echo $vidComments['Comment']['id']; ?>" style="    color: #0152a4; font-weight:400;"><?php echo (count($vidComments['standard']) - 1); ?> More <a  style="text-decoration: underline; color: #0152a4; font-weight:400;"  href="#">View All</a></div>
                                                <script type="text/javascript">
                                                    var $standard_view_more_btn = '<?php echo '#view-more-standar-' . $vidComments['Comment']['id'] ?>';
                                                    var $standar_view_box = '<?php echo '#standard-box-' . $vidComments['Comment']['id'] ?>';

                                                    $($standard_view_more_btn).on('click', function (e) {
                                                        show_id = $(this).attr('show-block-data');

                                                        $(show_id).show();
                                                        $(this).hide();
                                                        e.preventDefault();
                                                    });
                                                    $(document).on('click', '.standard-close-btn', function (e) {
                                                        show_id = $(this).attr('show-block-data_2');
                                                        show_all_button = $(this).attr('view-all-button');
                                                        $(show_id).hide();
                                                        $(show_all_button).show();
                                                        e.preventDefault();
                                                    })
                                                </script>
                                            <?php endif; ?>
                                            <?php
                                            //'# ' + tag_code + '-' + tag_array[0] + '...'
                                            $stand_title = explode(" ", $standard['AccountCommentTag']['tag_title']);
                                            ?>
                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $("#txtVideostandard1<?php echo $vidComments['Comment']['id']; ?>").addTag("# <?php echo $standard['account_tags']['tag_code'] . '-' . $stand_title[0] . '...'; ?>");
                                                });</script>

                                            <?php
                                            if (isset($standard['AccountCommentTag']) && $standard['AccountCommentTag'] != '') {
                                                $count++;
                                            }
                                        }
                                    }


                                    if (count($vidComments['standard']) >= 1 || count($vidComments['default_tags']) >= 1 || count($vidComments['account_tags']) >= 1) {
                                        ?>
                                        <br/>
                                        <div class="tags_viewalls" id="standard-box-<?php echo $vidComments['Comment']['id']; ?>">
                                            <div class="tags_grybg">
                                                <span style="float:left">Tags</span>
                                                <a style="float: right;font-size: 15px;text-decoration: underline; cursor: pointer;font-weight: 200;
                                                   text-decoration: none;" show-block-data_2="#standard-box-<?php echo $vidComments['Comment']['id']; ?>" view-all-button="#view-more-standar-<?php echo $vidComments['Comment']['id']; ?>" class="standard-close-btn" href="#"> x</a>
                                                <div class="clear"></div>
                                            </div>
                                            <?php
                                            if (isset($vidComments['default_tags']) && count($vidComments['default_tags']) > 0) {

                                                foreach ($vidComments['default_tags'] as $default_tag) {

                                                    if (isset($selected_tags) && is_array($selected_tags) && in_array($default_tag['AccountCommentTag']['account_comment_tag_id'], $selected_tags)) {
                                                        continue;
                                                    }
                                                    ?>
                                                    <div id="tagFilterList">
                                                        <div id="tagFilterContainer" class="btnwraper" style="margin-right:5px;">
                                                            <button class="btn btn-lilghtgray"># <?php echo empty($default_tag['account_tags']['tag_title']) ? $default_tag['AccountCommentTag']['tag_title'] : $default_tag['account_tags']['tag_title']; ?></button>

                                                            <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                                <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag1('<?php echo $default_tag['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                                            <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $vidComments['Comment']['created_by'])): ?>
                                                                <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag1('<?php echo $default_tag['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                                            <?php endif; ?>

                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <?php
                                            foreach ($vidComments['standard'] as $standard) {
                                                if (isset($selected_standard) && is_array($selected_standard) && in_array($standard['account_tags']['account_tag_id'], $selected_standard)) {
                                                    continue;
                                                }
                                                $selected_standard[] = $standard['account_tags']['account_tag_id'];
                                                ?>
                                                <div id="tagFilterList">
                                                    <div id="tagFilterContainer" class="btnwraper" style="margin-right:5px;">
                                                        <button class="btn btn-lilghtgray"># <?php echo $standard['account_tags']['tag_code'] . '-' . $standard['account_tags']['tag_title']; ?></button>
                                                        <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                            <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag1('<?php echo $standard['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                                        <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $vidComments['Comment']['created_by'])): ?>
                                                            <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag1('<?php echo $standard['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div style="clear: both;"> </div>
                                                </div>
                                                <?php
                                                //'# ' + tag_code + '-' + tag_array[0] + '...'
                                                $stand_title = explode(" ", $standard['AccountCommentTag']['tag_title']);
                                                ?>
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $("#txtVideostandard1<?php echo $vidComments['Comment']['id']; ?>").addTag("# <?php echo $standard['account_tags']['tag_code'] . '-' . $stand_title[0] . '...'; ?>");
                                                    });</script>
                                            <?php }
                                            ?>
                                        </div>

                                        <?php
                                    }
                                    ?>

                                </div>
                                <?php
                                echo '<div style="clear:both;"></div>';
                                ?>

                                <div id="reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>" style="display: none;">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            var isSubmitting = false;
                                            $("#reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide();
                                            $("#reply_button_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                                $("#reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>").show("slow", function () {
                                                    $('#comment_comment_<?php echo $vidComments['Comment']['id'] ?>').focus();
                                                });
                                            });
                                            $("#close_reply_form_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                                $("#reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide("slow");
                                            });
                                            function generate_reply_html(comment_id, postData) {

                                                var html = '<div id="comment_box_7302">';
                                                html += '<li class="comment">';
                                                html += '<a href="#">';
                                                html += ' <img width="21" height="21" src="/img/home/photo-default.png" class="photo photo inline" rel="image-uploader" alt="Photo-default">';
                                                html += ' </a>';
                                                html += ' <div class="comment-header comment-reply">' + postData.comment_from + '<span class="reply-to-icon">&nbsp;</span> <span style="color: #9c9c9c; font-size: 11px;">' + postData.comment_to + '</span></div>';
                                                html += '<div class="comment-body comment more">' + nl2br(postData.comment_current) + '</div>';
                                                html += '<div class="comment-footer">';
                                                html += '<span class="comment-date">Saving Reply...</span>';
                                                html += '</div>';
                                                html += '<ul class="comment-replies" id="nested_comment_7302" style="">';
                                                html += '</ul>';
                                                html += '</li>';
                                                html += '</div>';
                                                $('#nested_comment_' + comment_id).prepend(html);
                                            }


                                            $('#comments_form_<?php echo $vidComments['Comment']['id'] ?>').on('submit', function (event) {
                                                event.preventDefault();
                                                //console.log($('.form_reply'));
                                                // var audio_path = $('#add_audio_comments_<?php echo $vidComments['Comment']['id'] ?>').val();
                                                $('.input-group-reply').hide();
                                                var comment_from = '<?php echo str_replace("'", "\\'", $users['User']['first_name']) . " " . str_replace("'", "\\'", $users['User']['last_name']); ?>';
                                                var comment_to = '<?php echo str_replace("'", "\\'", $vidComments['User']['first_name']) . " " . str_replace("'", "\\'", $vidComments['User']['last_name']); ?>';
                                                var comment_current = $('#comment_comment_<?php echo $vidComments['Comment']['id'] ?>').val();
                                                var postData = {
                                                    comment_from: comment_from,
                                                    comment_to: comment_to,
                                                    comment_current: comment_current
                                                };
                                                generate_reply_html("<?php echo $vidComments['Comment']['id'] ?>", postData);
                                                if (isSubmitting)
                                                    return false;
                                                isSubmitting = true;
                                                var $form = $(this);
                                                $.ajax({
                                                    type: $form.attr('method'),
                                                    url: $form.attr('action'),
                                                    data: $form.serialize(),
                                                    success: function () {
                                                        getVideoComments();
                                                        isSubmitting = false;
                                                    }
                                                });
                                                $('#add_audio_comments_<?php echo $vidComments['Comment']['id'] ?>').val('');
                                                return false;
                                            });
                                            $('#delete-main-comments-frm<?php echo $vidComments['Comment']['id']; ?>').on('submit', function (event) {

                                                var $form = $(this);
                                                $.ajax({
                                                    type: $form.attr('method'),
                                                    url: $form.attr('action'),
                                                    data: $form.serialize(),
                                                    success: function (data, status) {
                                                        getVideoComments();
                                                        ob_getVideoComments();
                                                    }
                                                });
                                                event.preventDefault();
                                                return false;
                                            });
                                            $("#comment_comment_<?php echo $vidComments['Comment']['id'] ?>").keyup(function (e) {
                                                var str_length = $("#comment_comment_<?php echo $vidComments['Comment']['id'] ?>").val().trim();
                                                if (str_length.length > 0 && e.which == 13) {
                                                    $("#add-replies_<?php echo $vidComments['Comment']['id'] ?>").trigger("click");
                                                }
                                            });
                                        });</script>

                                    <form method="post" id="comments_form_<?php echo $vidComments['Comment']['id'] ?>"
                                          class="new_comment"
                                          action="<?php echo $this->base . '/Huddles/addReply/' . $vidComments['Comment']['id']; ?>"
                                          accept-charset="UTF-8">
                                        <div style="margin:0;padding:0;display:inline">
                                            <input type="hidden" value="?" name="utf8">
                                        </div>
                                        <input type="hidden" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" name="authenticity_token" id="tokentag">
                                        <div class="input-group input-group-reply">
                                            <textarea rows="3" placeholder="Add Reply..." name="comment[comment]" id="comment_comment_<?php echo $vidComments['Comment']['id'] ?>" ></textarea>
                                        </div>
                                        <input type="hidden" value="nested" name="comment[access_level]" id="comment_access_level">

                                        <div class="input-group input-group-reply">
                                            <input id="add-replies_<?php echo $vidComments['Comment']['id'] ?>" type="submit" class="btn btn-green" value="Add Reply" name="submit">
                                            <a class="btn btn-transparent" id="close_reply_form_<?php echo $vidComments['Comment']['id'] ?>">Cancel</a>
            <!--                                <input name="comment[audio_comments]" id="add_audio_comments_<?php //echo $vidComments['Comment']['id']                                                                                                                                                    ?>" type="hidden" value=""/>
                                            <img alt="Indicator" id="record_<?php //echo $vidComments['Comment']['id']                                                                                                                                                    ?>" class="show_recorder" src="<?php //echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/recorder.jpg');                                                                                                                                                    ?>" />
                                                <img alt="Indicator" id="play_<?php //echo $vidComments['Comment']['id']                                                                                                                                                    ?>" class="show_player" style="display:inline;" src="<?php //echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/icons/total_videos.png');                                                                                                                                                    ?>" />
                                                <a class="button disabled one" id="save_<?php //echo $vidComments['Comment']['id']                                                                                                                                                    ?>">Upload</a>-->
                                        </div>
                                    </form>
                                </div>
                                <div id="edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>" class="modal" role="dialog" style="display: none;">
                                    <div class="modal-dialog" style="width: 900px;">
                                        <div class="modal-content">
                                            <div class="header">
                                                <h4 class="header-title nomargin-vertical smargin-bottom">Edit Comment</h4>
                                                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                                            </div>

                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    var tagIDsd = $('[name=txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                                                    var tagID = $('[name=txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                                                    $('#' + tagIDsd + '_tag').css('width', '110px');
                                                    var isSubmitting = false;
                                                    $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide();
                                                    $("#edit-main-comments_<?php echo $vidComments['Comment']['id'] ?>").click(function () {

                                                        var $comments_text = $('#comments_text_<?php echo $vidComments['Comment']['id'] ?>').val();
                                                        var comment_comment_edit = '<?php echo preg_replace("/\r|\n/", "", html_entity_decode((addslashes($vidComments['Comment']['comment'])))); ?>';
                                                        $('#comment_comments_<?php echo $vidComments['Comment']['id'] ?>').val(comment_comment_edit);
                                                        $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").show("slow", function () {
                                                            $('#comment_comment_<?php echo $vidComments['Comment']['id'] ?>').focus();
                                                        });
                                                    });
                                                    $("#close_edit_form_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                                        $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide("slow");
                                                    });
                                                    $('#comment_form_<?php echo $vidComments['Comment']['id'] ?>').on('submit', function (event) {
                                                        event.preventDefault();
                                                        if (isSubmitting)
                                                            return false;
                                                        isSubmitting = true;
                                                        var $form = $(this);
                                                        $.ajax({
                                                            type: $form.attr('method'),
                                                            url: $form.attr('action'),
                                                            data: $form.serialize(),
                                                            success: function (data, status) {
            <?php if ($type != 'get_video_comments_obs'): ?>
                                                                    getVideoComments();
            <?php else: ?>
                                                                    ob_getVideoComments();
            <?php endif; ?>
                                                                isSubmitting = false;
                                                            }
                                                        });
                                                        return false;
                                                    });
                                                    //                                                $(document).on("click", "#save_<?php //echo $vidComments['Comment']['id']                                                                                                                                                    ?>", function(){
                                                    //                                                    Fr.voice.export(function(blob){
                                                    //                                                      var formData = new FormData();
                                                    //                                                      formData.append('file', blob);
                                                    //
                                                    //                                                      $.ajax({
                                                    //                                                        url: home_url + "/huddles/upload_audio_comment",
                                                    //                                                        type: 'POST',
                                                    //                                                        data: formData,
                                                    //                                                        contentType: false,
                                                    //                                                        processData: false,
                                                    //                                                        success: function(url) {
                                                    //                                                          $("#audio").attr("src", url);
                                                    //                                                          $("#audio")[0].play();
                                                    //                                                          $('#add_audio_comments_<?php //echo $vidComments['Comment']['id']                                                                                                                                                    ?>').val(url);
                                                    //                                                          alert("Saved In Server. See audio element's src for URL");
                                                    //                                                        }
                                                    //                                                      });
                                                    //                                                    }, "blob");
                                                    //                                                  restore();
                                                    //                                                  });
                                                    //
                                                    //                                                   $(document).on("click", "#record_<?php //echo $vidComments['Comment']['id']                                                                                                                                                    ?>", function(){
                                                    //                                                    elem = $(this);
                                                    //                                                    Fr.voice.record($("#live").is(":checked"), function(){
                                                    //                                                      elem.addClass("disabled");
                                                    //                                                      $("#live").addClass("disabled");
                                                    //                                                      $(".one").removeClass("disabled");
                                                    //
                                                    //                                                      /**
                                                    //                                                       * The Waveform canvas
                                                    //                                                       */
                                                    //                                                      analyser = Fr.voice.context.createAnalyser();
                                                    //                                                      analyser.fftSize = 2048;
                                                    //                                                      analyser.minDecibels = -90;
                                                    //                                                      analyser.maxDecibels = -10;
                                                    //                                                      analyser.smoothingTimeConstant = 0.85;
                                                    //                                                      Fr.voice.input.connect(analyser);
                                                    //
                                                    //                                                      var bufferLength = analyser.frequencyBinCount;
                                                    //                                                      var dataArray = new Uint8Array(bufferLength);
                                                    //
                                                    //                                                      WIDTH = 500, HEIGHT = 200;
                                                    //                                                      canvasCtx = $("#level")[0].getContext("2d");
                                                    //                                                      canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);
                                                    //
                                                    //                                                      function draw() {
                                                    //                                                        drawVisual = requestAnimationFrame(draw);
                                                    //                                                        analyser.getByteTimeDomainData(dataArray);
                                                    //                                                        canvasCtx.fillStyle = 'rgb(200, 200, 200)';
                                                    //                                                        canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
                                                    //                                                        canvasCtx.lineWidth = 2;
                                                    //                                                        canvasCtx.strokeStyle = 'rgb(0, 0, 0)';
                                                    //
                                                    //                                                        canvasCtx.beginPath();
                                                    //                                                        var sliceWidth = WIDTH * 1.0 / bufferLength;
                                                    //                                                        var x = 0;
                                                    //                                                        for(var i = 0; i < bufferLength; i++) {
                                                    //                                                          var v = dataArray[i] / 128.0;
                                                    //                                                          var y = v * HEIGHT/2;
                                                    //
                                                    //                                                          if(i === 0) {
                                                    //                                                            canvasCtx.moveTo(x, y);
                                                    //                                                          } else {
                                                    //                                                            canvasCtx.lineTo(x, y);
                                                    //                                                          }
                                                    //
                                                    //                                                          x += sliceWidth;
                                                    //                                                        }
                                                    //                                                        canvasCtx.lineTo(WIDTH, HEIGHT/2);
                                                    //                                                        canvasCtx.stroke();
                                                    //                                                      };
                                                    //                                                      draw();
                                                    //                                                    });
                                                    //                                                  });





                                                });</script>
                                            <div id="comment_add_form_html">
                                                <?php $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']); ?>

                                                <?php
                                                if ($huddle_permission == '200' || $huddle_permission == '210' ||
                                                        ($isCreator && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')
                                                ):
                                                    ?>

                                                    <?php
                                                    $comment_box_display = 'style="display:block;"';
                                                    $comment_box_add_btn = 'style="display:none;"';
                                                    ?>
                                                    <?php
                                                    $selected_tag_ids = array();

                                                    if (isset($vidComments['default_tags']) && count($vidComments['default_tags']) > 0) {

                                                        foreach ($vidComments['default_tags'] as $default_tag) {
                                                            $selected_tag_ids[] = $default_tag['account_tags']['account_tag_id'];
                                                            if ($default_tag['AccountCommentTag']['ref_type'] != 2) {
                                                                ?>
                                                                <script type="text/javascript">
                                                                    $(document).ready(function () {
                                                                        $("#txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>").addTag("# <?php echo trim(empty($default_tag['account_tags']['tag_title']) ? $default_tag['AccountCommentTag']['tag_title'] : $default_tag['account_tags']['tag_title']); ?>");
                                                                    });</script>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <style type="text/css">
                                                        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
                                                            color:    #424242;
                                                        }
                                                        :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
                                                            color:    #424242;
                                                            opacity:  1;
                                                        }
                                                        ::-moz-placeholder { /* Mozilla Firefox 19+ */
                                                            color:    #424242;
                                                            opacity:  1;
                                                        }
                                                        :-ms-input-placeholder { /* Internet Explorer 10-11 */
                                                            color:    #424242;
                                                        }
                                                        div.tagsinput{
                                                            width:422px !important;
                                                            position: relative;
                                                            top: -5px;
                                                            border-radius: 0px;
                                                        }
                                                        div.tagsinput input {
                                                            width:105px !important;
                                                        }
                                                        #txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>_tag {
                                                            width: 117px;
                                                            background: url(/app/img/tag_standard_bg.png) no-repeat;
                                                            padding-left: 20px;
                                                            color: #b2b2b2 !important;
                                                        }
                                                        .edit_comment_left{
                                                            float:left;
                                                            width: 52%;
                                                            box-sizing:border-box;
                                                        }
                                                        .edit_comment_right{
                                                            float:right;
                                                            width:48%;
                                                            padding:0px 20px;
                                                            box-sizing:border-box;

                                                        }
                                                        .scroll_box{
                                                            overflow-y:scroll;
                                                            max-height:420px;
                                                            overflow-x:hidden;
                                                            margin-bottom:12px;
                                                        }
                                                        .block_width_adjs{
                                                            padding-left:25px;
                                                        }
                                                        .edit_comment_form{
                                                            padding:0px 22px !important;
                                                        }
                                                        .edit_comment_right h1{
                                                            font-size:25px;
                                                            border-bottom: dotted 1px #ccc;
                                                            font-weight:normal;
                                                            margin-bottom:10px;
                                                        }
                                                        .edit_btn_adjs{
                                                            top: 1px;
                                                            position: relative;
                                                            padding-bottom: 6px !important;
                                                        }
                                                        #txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>_tag{
                                                            width:105px !important;
                                                        }
                                                    </style>

                                                    <div class="edit_comment_left">
                                                        <?php if ($this->Custom->is_enable_tags($account_id)): ?>
                                                            <?php if ($this->Custom->is_enable_huddle_tags($account_folder_id)): ?>
                                                                <div class="divblockwidth block_width_adjs">
                                                                    <?php
                                                                    if (count($tags) > 0) {
                                                                        $count = 1;
                                                                        foreach ($tags as $tag) {
                                                                            ?>
                                                                            <a class="default_tag1<?php echo $vidComments['Comment']['id'] ?> <?php echo (in_array($tag['AccountTag']['account_tag_id'], $selected_tag_ids)) ? defaulttagsclasses($count) . "bg" : defaulttagsclasses($count); ?>" href="javascript:#" position_id="<?php echo $count; ?>" status_flag="<?php echo (in_array($tag['AccountTag']['account_tag_id'], $selected_tag_ids)) ? '1' : '0'; ?>" alt="<?php echo $tag['AccountTag']['tag_title']; ?>"># <?php echo $tag['AccountTag']['tag_title']; ?></a>
                                                                            <?php
                                                                            $count++;
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                        <div class="clear"></div>

                                                        <div id="comment_form_main" <?php echo $comment_box_display; ?>>
                                                            <form class="edit_comment_form" method="post" id="comment_form_<?php echo $vidComments['Comment']['id'] ?>"
                                                                  class="new_comment" accept-charset="UTF-8"
                                                                  action="<?php echo $this->base . '/Huddles/editComments/' . $vidComments['Comment']['id']; ?>" >
                                                                <div style="margin:0;padding:0;display:inline">
                                                                    <input type="hidden" value="✓" name="utf8">
                                                                </div>
                                                                <input type="hidden" id="comments_text_<?php echo $vidComments['Comment']['id'] ?>" value="<?php echo strip_tags($vidComments['Comment']['comment']) ?>"/>
                                                                <input type="hidden" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" name="authenticity_token" id="tokentag">
                                                                <input type="hidden" value="<?php echo $vidComments['Comment']['id'] ?>" name="comment_id" />
                                                                <input type="hidden" value="<?php echo $vidComments['Comment']['ref_id'] ?>" name="videoId" />
                                                                <?php
                                                                $input_group_style = "margin-top: 20px;";

                                                                if ($this->Custom->is_enable_tags($account_id)):
                                                                    if ($this->Custom->is_enable_huddle_tags($account_folder_id)):
                                                                        $input_group_style = "margin-top: 20px;";
                                                                    endif;
                                                                endif;
                                                                ?>




                                                                <div class="input-group" style="<?php echo $input_group_style; ?>">
                                                                    <?php if ($vidComments['Comment']['ref_type'] != '6') { ?>  <textarea rows="3" placeholder="Edit comments" style="border-radius:0px;" name="comment[comment]" id="comment_comments_<?php echo $vidComments['Comment']['id'] ?>" ><?php echo $vidComments['Comment']['comment']; ?></textarea> <?php } ?>
                                                                    <?php
                                                                    if ($vidComments['Comment']['ref_type'] == '6') {
                                                                        if (Configure::read('use_cloudfront') == true) {
                                                                            $url = $vidComments['Comment']['comment'];
                                                                            $videoFilePath = pathinfo($url);
                                                                            $videoFileName = $videoFilePath['filename'];
                                                                            $ext = '.' . $videoFilePath['extension'];
                                                                            if ($videoFilePath['dirname'] == ".") {

                                                                                $video_path = $this->Custom->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . ".m4a");
                                                                                $relative_video_path = $videoFileName . ".m4a";
                                                                            } else {

                                                                                $video_path = $this->Custom->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".m4a");
                                                                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".m4a";
                                                                            }
                                                                        } else {
                                                                            $url = $vidComments['Comment']['comment'];
                                                                            $videoFilePath = pathinfo($url);
                                                                            $videoFileName = $videoFilePath['filename'];
                                                                            $ext = '.' . $videoFilePath['extension'];
                                                                            if ($videoFilePath['dirname'] == ".") {

                                                                                $video_path = $this->Custom->getSecureAmazonUrl($videoFileName . ".m4a");
                                                                                $relative_video_path = $videoFileName . ".m4a";
                                                                            } else {

                                                                                $video_path = $this->Custom->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName . ".m4a");
                                                                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".m4a";
                                                                            }
                                                                        }
                                                                        ?>
                                                                        <div class="comment-body comment">
                                                                            <audio controls>
                                                                                <source src="<?php echo $video_path; ?>">

                                                                                Your browser does not support the audio element.
                                                                            </audio>
                                                                        </div>
                                                                        <br>
                                                                    <?php } ?>

                                                                    <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                                                                        <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                                                                            <div class="tags divblockwidth">
                                                                                <div class="video-tags-row row">
                                                                                    <input type="text" name="txtVideostandard1<?php echo $vidComments['Comment']['id']; ?>" data-default="Tag Standard..." id="txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>" value="" placeholder=""  required/>
                                                                                </div>
                                                                                <div class="clear" style="clear: both;"></div>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>

                                                                    <div class="tags1 divblockwidth">
                                                                        <div class="video-tags-row row">
                                                                            <input type="text" name="txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>" data-default="Tags..." id="txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>" value="" placeholder="" readonly="readonly"/>
                                                                        </div>
                                                                        <div class="clear" style="clear: both;"></div>
                                                                    </div>


                                                                    <div class="clear" style="clear: both;"></div>
                                                                    <input type="hidden" name="assessment_value" id="synchro_time_class_tags<?php echo $vidComments['Comment']['id'] ?>">

                                                                    <?php if ($this->Custom->is_enable_assessment($account_id) && $this->Custom->is_enabled_framework_and_standards($account_id) && $this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                                                                        <style>
                                                                            .default_rating {
                                                                                max-width: 95px;
                                                                                overflow: hidden;
                                                                                text-overflow: ellipsis;
                                                                                white-space: nowrap;
                                                                            }
                                                                        </style>
                                                                        <?php if ($huddle_type == 313): ?>
                                                                            <?php
                                                                            foreach ($vidComments['default_tags'] as $row) {
                                                                                if ($row['AccountCommentTag']['ref_type'] == 2) {
                                                                                    $current_tag = $row['AccountCommentTag']['tag_title'];
                                                                                }
                                                                            }
                                                                            ?>
                                                                            <div class="divblockwidth2">
                                                                                <?php
                                                                                if (count($ratings) > 0) {
                                                                                    $count = 1;
                                                                                    foreach ($ratings as $tag) {
                                                                                        ?>
                                                                                        <a rel="tooltip" data-original-title="<?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?>" class="default_rating1<?php echo $vidComments['Comment']['id'] ?> <?php echo (substr($tag['AccountMetaData']['meta_data_name'], 13) == $current_tag ) ? defaulttagsclasses($count) . "bg rating_black_active" : defaulttagsclasses($count) . " rating_black"; ?>" href="javascript:#" position_id="<?php echo $count; ?>" status_flag="0" value="<?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?>"># <?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?></a>
                                                                                        <?php
                                                                                        $count++;
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>



                                                                </div>
                                                                <div class="clear" style="clear: both;"></div>
                                                                <input type="hidden" value="nested" name="comment[access_level]" id="comment_access_level">
                                                                <div class="input-group">
                                                                    <input type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green edit_btn_adjs" id="btn_form_sub_<?php echo $vidComments['Comment']['id'] ?>" value="Edit Comment" name="submit">
                                                                    <a class="btn btn-transparent" id="close_edit_form_<?php echo $vidComments['Comment']['id'] ?>" data-dismiss="modal">Cancel</a>
                                                                </div>
                                                            </form>

                                                        </div>
                                                    </div>
                                                    <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                                                        <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                                                            <div class="edit_comment_right">
                                                                <h1>Framework</h1>
                                                                <div class="search-box standard-search" style="position: relative;">
                                                                    <input type="button" id="btnSearchTags" class="btn-search" value="">
                                                                    <input class="text-input" id="txtSearchTags1" type="text" value="" placeholder="Search Standards..." style="margin-right: 0px;">
                                                                    <span id="clearTagsButton1" class="clear-video-input-box" style="display:none;right: 33px;top: 20px;">X</span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="scroll_box">
                                                                    <div id="indicator" style="display:none;"><img alt="Indicator" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/indicator.gif'); ?>" /></div>
                                                                    <div class="standards">
                                                                        <ul id="expList" class="expList" style="padding-left:0px">
                                                                            <?php
                                                                            if (!empty($standardsL2)) {
                                                                                for ($i = 0; $i < count($standardsL2); $i++) {
                                                                                    ?>
                                                                                    <li class="frame_work_heading" ><?php echo $standardsL2[$i]['AccountTag']['tag_code'] . ' - ' . $standardsL2[$i]['AccountTag']['tag_title']; ?></li>
                                                                                    <?php
                                                                                    foreach ($standards as $standard) {
                                                                                        if ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['parent_account_tag_id']) {
                                                                                            ?>
                                                                                            <li class="standard_<?php echo $vidComments['Comment']['id']; ?> standard-cls" >
                                                                                                <input class="check_class_edit<?php echo $vidComments['Comment']['id']; ?>" type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" <?php echo in_array($standard['AccountTag']['account_tag_id'], $selected_standard) ? 'checked="checked"' : ''; ?>/>
                                                                                                <?php
                                                                                                $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                                                                echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                                                                ?>
                                                                                            </li>

                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                foreach ($standards as $standard) {
                                                                                    ?>
                                                                                    <li class="standard_<?php echo $vidComments['Comment']['id']; ?> standard-cls" >
                                                                                        <input class="check_class_edit<?php echo $vidComments['Comment']['id']; ?>" type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" <?php echo in_array($standard['AccountTag']['account_tag_id'], $selected_standard) ? 'checked="checked"' : ''; ?>/>
                                                                                        <?php
                                                                                        $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                                                        echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                                                        ?>
                                                                                    </li>

                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                            <li id="noresults">No standards match your search criteria.</li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php
                                                endif;
                                                ?>
                                            </div>

                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                //level1
                                echo $this->element('ajax/vidCommentsReplies', array(
                                    "vidComments" => $vidComments,
                                    "huddle_permission" => $huddle_permission,
                                    "user_current_account" => $user_current_account,
                                    "huddle" => $huddle,
                                    "user_permissions" => $user_permissions,
                                    "counter" => 0,
                                    'huddle_type' => $huddle_type,
                                ));
                                ?>

                        </li>


                    </div>
                    <script type="text/javascript">
                        $(document).ready(function (e) {
                            isSubmitting = false;
                            $(document).on("click", "#btn_form_sub_<?php echo $vidComments['Comment']['id'] ?>", function (event) {
                                event.preventDefault();
                                if (isSubmitting)
                                    return false;
                                isSubmitting = true;
                                $.ajax({
                                    type: 'POST',
                                    url: home_url + '/Huddles/editComments/<?php echo $vidComments['Comment']['id']; ?>',
                                    data: $("#comment_form_<?php echo $vidComments['Comment']['id'] ?>").serialize(),
                                    success: function (response) {
                                        $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").modal('hide');
                                        getVideoComments();
                                        isSubmitting = false;
                                    }
                                });
                                return false;
                            });
                            isSubmitting = false;
                            var tagIDsd = $('[name=txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                            $(document).on("click", "#close_edit_form_<?php echo $vidComments['Comment']['id'] ?>", function () {
                                if (isSubmitting)
                                    return false;
                                isSubmitting = true;
                                getVideoComments();
                            });
                            isSubmitting = false;
                            //                            $(document).on('change', '.standard_<?php //echo $vidComments['Comment']['id'];                                                                                                                                                   ?> input', function () {
                            //                                if (isSubmitting)
                            //                                    return false;
                            //                                isSubmitting = true;
                            //                                var tagIDsd = $('[name=txtVideostandard1<?php //echo $vidComments['Comment']['id']                                                                                                                                                   ?>]').attr('id');
                            //                                var tag_code = $(this).attr('st_code');
                            //                                var tag_value = '';
                            //                                var tag_array = {};
                            //                                var tag_name = $(this).attr('st_name');
                            //                                //console.log($(this).attr('st_name'));
                            //                                tag_array = tag_name.split(" ");
                            //                                if ($(this).is(':checked')) {
                            //                                    tag_array = tag_name.split(" ");
                            //                                    tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                            //                                    if ($('#' + tagIDsd).tagExist(tag_value)) {
                            //                                        $('#' + tagIDsd).removeTag(tag_value);
                            //                                    }
                            //                                    $('#' + tagIDsd).addTag(tag_value);
                            //                                    //$('#txtVideostandard_tag').remove();
                            //                                } else {
                            //                                    tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                            //                                    $('#' + tagIDsd).removeTag(tag_value);
                            //                                }
                            //                                if ($('input[name="name1"]:checked').length > 0) {
                            //                                    //$('#txtVideostandard_tag').hide();
                            //                                }
                            //                                isSubmitting = false;
                            //                                //return false;
                            //                            });

                            $('input.check_class_edit<?php echo $vidComments['Comment']['id'] ?>').on('change', function () {



                                //   $('input.check_class_edit<?php //echo $vidComments['Comment']['id']                                                                                                                                    ?>').not(this).prop('checked', false);


                                $(this).each(function () {

                                    var tag_code = $(this).attr('st_code');
                                    var tagIDsd = $('[name=txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                                    var tag_value = '';
                                    var tag_array = {};
                                    var tag_name = $(this).attr('st_name');
                                    tag_array = tag_name.split(" ");
                                    if ($(this).is(':checked')) {
                                        tag_array = tag_name.split(" ");
                                        tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                        //  alert(tag_value);
                                        $('#' + tagIDsd).addTag(tag_value);
                                        if ($('input[name="name1"]:checked').length > 0) {
                                            $('#txtVideostandard_tag').hide();
                                        }
                                    } else {
                                        tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                        $('#' + tagIDsd).removeTag(tag_value);
                                    }

                                });
                            });
                            $('#' + tagIDsd).tagsInput({
                                defaultText: 'Tag Standard...',
                                width: '110px',
                                readonly_input: true,
                                placeholderColor: '#b2b2b2',
                            });
                            var tagIDsd = $('[name=txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                            //$('#'+tagIDsd).setAttribute( 'style', 'background-image: url( "http://placekitten.com.s3.amazonaws.com/homepage-samples/96/139.jpg" ) !important' );
                            $('#' + tagIDsd + '_tag').css('width', '105px !important');
                            //console.log('#'+tagIDsd+'_tag');


                            var tagID = $('[name=txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                            $('#' + tagID).tagsInput({
                                defaultText: 'Tags...',
                                //width: '100px',
                                placeholderColor: '#b2b2b2',
                            });
                            $('.video-tags-row label').css('display', 'none');
                        });</script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            var tagID = $('[name=txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                            $('#clearTagsButton1').click(function () {
                                $('#txtSearchTags1').val('');
                                $('#clearTagsButton1').css('display', 'none');
                                $('input#txtSearchTags1').quicksearch('ul#expList li', {
                                    noResults: 'li#noresults'
                                });
                            });
                            $("#inline-crop-panel").fancybox({
                                width: '70%',
                                height: '70%',
                                type: "iframe",
                                closeClick: false,
                                helpers: {
                                    title: null,
                                    overlay: {closeClick: false}
                                }
                            });
                            $("#inline-crop-panel").click(function () {
                                pausePlayer();
                            });
            <?php if (isset($_REQUEST['open_trim']) && $_REQUEST['open_trim'] == '1'): ?>
                                pausePlayer();
                                $("#inline-crop-panel").trigger('click');
            <?php endif; ?>

                            $('.btn-trim-video').click(function () {
                                $(this).next().submit();
                            });
                            $('.default_tag1<?php echo $vidComments['Comment']['id'] ?>').on('click', function (e) {
                                e.preventDefault();
                                var posid = '';
                                //tag_name = $(this).text();
                                tag_name = $(this).attr('alt');
                                tag_name.replace(/\s/g, "");
                                posid = $(this).attr('position_id');
                                $('.default_tag1<?php echo $vidComments['Comment']['id'] ?>').each(function (index) {
                                    var tag_name1 = $(this).text().trim();
                                    tag_name1.replace(/\s/g, "");
                                    $('#' + tagID).removeTag(tag_name1);
                                    $(this).attr('status_flag', '0');
                                    if ($(this).hasClass(defaulttagsclasses(index))) {
                                        $('#' + tagID).removeTag(tag_name1);
                                        $(this).removeClass(defaulttagsclasses(index) + 'bg');
                                    }
                                });
                                $('.default_tag1<?php echo $vidComments['Comment']['id'] ?>').each(function (index) {
                                    if ($(this).attr('position_id') != posid) {
                                        $(this).removeClass(defaulttagsclasses($(this).attr('position_id')));
                                        $(this).addClass(defaulttagsclasses1($(this).attr('position_id')));
                                    }
                                });
                                if ($(this).hasClass(defaulttagsclasses(posid))) {
                                    $('#' + tagID).removeTag(tag_name);
                                    $(this).addClass(defaulttagsclasses(0));
                                    $(this).removeClass(defaulttagsclasses(posid));
                                    $(this).attr('status_flag', '0');
                                    $('#comment_comment').attr("placeholder", "Add a comment...");
                                    $('#synchro_time_class').val('');
                                }
                                else {
                                    $('#' + tagID).addTag("# " + tag_name);
                                    $(this).addClass(defaulttagsclasses(posid));
                                    $(this).removeClass(defaulttagsclasses(0));
                                    $(this).attr('status_flag', '1')
                                    var spt = $(this).text().slice(2);
                                }
                            });
                            $('.default_rating1<?php echo $vidComments['Comment']['id'] ?>').on('click', function (e) {
                                e.preventDefault();
                                var posid = '';
                                tag_name = $(this).text();
                                tag_name.replace(/\s/g, "");
                                tag_name_mod = tag_name.replace('# ', '');
                                posid = $(this).attr('position_id');
                                $('.default_rating1<?php echo $vidComments['Comment']['id'] ?>').each(function (index) {
                                    var tag_name1 = $(this).text();
                                    tag_name1.replace(/\s/g, "");
                                    $('#txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>').removeTag(tag_name1);
                                    if ($(this).hasClass(defaulttagsclasses(index))) {
                                        $('#txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>').removeTag(tag_name1);
                                        $(this).removeClass(defaulttagsclasses(index) + 'bg');
                                        $(this).removeClass('rating_black_active');
                                    }
                                });
                                $('.default_rating1<?php echo $vidComments['Comment']['id'] ?>').each(function (index) {
                                    if ($(this).attr('position_id') != posid) {
                                        $(this).removeClass(defaulttagsclasses($(this).attr('position_id')));
                                        $(this).addClass(defaulttagsclasses(0));
                                        $(this).removeClass('rating_black_active');
                                        $(this).addClass('rating_black');
                                    }
                                });
                                if ($(this).hasClass(defaulttagsclasses(posid))) {
                                    $('#txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>').removeTag(tag_name);
                                    $(this).addClass(defaulttagsclasses(0));
                                    $(this).removeClass(defaulttagsclasses(posid));
                                    $(this).removeClass('rating_black_active');
                                    $(this).addClass('rating_black');
                                    $(this).attr('status_flag', '0');
                                    $('#comment_comment').attr("placeholder", "Add a comment");
                                    $('#synchro_time_class').val('');
                                } else {
                                    //$('#txtVideoTags').addTag(tag_name);
                                    $(this).addClass(defaulttagsclasses(posid));
                                    $(this).removeClass(defaulttagsclasses(0));
                                    $(this).addClass('rating_black_active');
                                    $(this).removeClass('rating_black');
                                    $(this).attr('status_flag', '1');
                                    //$('#comment_comment').focus();
                                    //var spt=$(this).text().split(" ");
                                    //var spt = $(this).text().slice(2);
                                    //$('#comment_comment').attr("placeholder", "Add " + spt);

                                    $('#synchro_time_class_tags<?php echo $vidComments['Comment']['id'] ?>').val(tag_name_mod);
                                    $('#synchro_time_class').val('short_tag_' + posid);
                                }
                            });
                        });</script>
                    <?php $video_comment_counter++; ?>
                </ul>
                <?php
            endforeach;
        ?>
        <?php
    else:
        ?>

    <?php endif; ?>

</div>
<script>
    $(document).ready(function () {
        var showChar = 200;
        var ellipsestext = "...";
        var moretext = "more";
        var lesstext = "less";
        $('.more').each(function () {
            var content = $(this).text();
            if (content.length > showChar) {
                var n = content.indexOf(' ', showChar);
                if (n == -1)
                    n = showChar;
                var c = content.substr(0, n);
                var h = content.substr(n);
                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h
                        + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                $(this).html(html);
            }

        });
        $(".morelink").click(function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
        $('.input-group').find('textarea').overlay([
            {
                match: /\B#\w+\S+/g,
                css: {
                    'background-color': '#d8dfea'
                }
            }
        ]);
    });
    function deleteComment(commentId) {
        //$(formName).submit();
        //getVideoComments();
        //return false;
        var video_id = '<?php echo $video_id; ?>';
        $('#comment_box_' + commentId).closest("ul").hide();
        $.ajax({
            type: 'GET',
            url: home_url + '/Huddles/deleteVideoComments/' + commentId + '/' + video_id,
            success: function (response) {
                getVideoComments();
            }
        });
        return false;
    }

    function deleteCommentreplies(commentId) {
        //$(formName).submit();
        //getVideoComments();
        //return false;
        $('#comment_box_' + commentId).hide();
        $.ajax({
            type: 'GET',
            url: home_url + '/Huddles/deleteVideoComments/' + commentId,
            success: function (response) {
                getVideoComments();
            }
        });
        return false;
    }


    function deleteCommentTag1(tagId) {
        //$(formName).submit();
        $.ajax({
            type: 'GET',
            url: home_url + '/Huddles/deleteCommentTag/' + tagId,
            success: function (response) {
                getVideoComments();
            }
        });
        return false;
    }

</script>
<script type="text/javascript">
    $(function () {
        $('input#txtSearchTags1').quicksearch('ul#expList li', {
            noResults: 'li#noresults',
            'onAfter': function () {
                if (typeof $('#txtSearchTags1').val() !== "undefined" && $('#txtSearchTags1').val().length > 0) {
                    $('#clearTagsButton1').css('display', 'block');
                }
            }
        });
    });
    function defaulttagsclasses(posid) {
        var class_name = 'tags_qucls';
        if (posid == 1) {
            class_name = 'tags_quclsbg';
        }
        else if (posid == 2) {
            class_name = 'tags_sugclsbg';
        }
        else if (posid == 3) {
            class_name = 'tags_notesclsbg';
        }
        else if (posid == 4) {
            class_name = 'tags_strangthclsbg';
        }
        else if (posid == 5) {
            class_name = 'tags_strangthclsbg';
        }
        return class_name;
    }
    function defaulttagsclasses1(posid) {
        var class_name = 'tags_qucls';
        if (posid == 1) {
            class_name = 'tags_qucls';
        }
        else if (posid == 2) {
            class_name = 'tags_sugcls';
        }
        else if (posid == 3) {
            class_name = 'tags_notescls';
        }
        else if (posid == 4) {
            class_name = 'tags_strangthcls';
        }
        else if (posid == 5) {
            class_name = 'tags_strangthcls';
        }
        return class_name;
    }
    function nl2br(str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }

    $(document).on("click", ".timestamp_attachment_no", function () {
        $('#attachment').trigger('click');
    });


</script>
