
<?php $uninvited_users = $data; ?>
<?php if (count($uninvited_users) > 0): ?>
    <ul>
        <?php foreach($uninvited_users as $user):?>
            <li>
                <?php
                    $userInfo = array(
                        'id' => $user['User']['id'],
                        'name' => $user['User']['first_name'] . ' ' . $user['User']['last_name']
                    );
                ?>
                <input type="hidden" name="unselected_user[]" class="user-info" value="<?php echo $userInfo['id'] . '::' . $userInfo['name']; ?>" />
                <div class="user-name">
                    <label>
                        <input type="checkbox" name="user_id" value="<?php echo $userInfo['id'];?>"/>
                        <span><?php echo $userInfo['name'];?></span>
                    </label>
                </div>
                <div class="user-role">
                    <select name="unselected_user_role[]">
                        <option value="200">Admin</option>
                        <option value="210">Member</option>
                        <option value="220" selected="selected">Viewer</option>
                    </select>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
