<?php if ($topics): ?>
    <?php foreach ($topics as $topic): ?>
        <div id="chkContainerTopic<?php echo $topic['Topic']['id'] ?>" class="check-container">
                                                       
	    <input id="topic_ids<?php echo $topic['Topic']['id'] ?>" name="topic_ids<?php echo $topic['Topic']['id'] ?>" class="checkbox-3 topic_checkbox"  type="checkbox" value="<?php echo $topic['Topic']['id'] ?>">
	    
	    <label for="topic_ids<?php echo $topic['Topic']['id'] ?>" class="title"><?php echo $topic['Topic']['name'] ?></label>
	    <input type="text" id="txttopic_ids<?php echo $topic['Topic']['id'] ?>" class="edit-field" value="<?php echo $topic['Topic']['name'] ?>"/>
	    <div class="controls controls<?php echo $topic['Topic']['name'] ?>">
            <?php
                $imgedit_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/playlist8x8.png');
                $imgdel_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/delete-icon8x9.png');
                $imgsave_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/save8x8.png');
                $imgundo_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/undo8x8.png');
            ?>
	       <?php echo $this->Html->image($imgedit_path, array('id'=>"imgEditTopic".$topic['Topic']['id'], 'class' => 'edit-controls edit-controlsTopic'.$topic['Topic']['id'])); ?>
	       <?php echo $this->Html->image($imgdel_path, array('id'=>"imgDeleteTopic".$topic['Topic']['id'], 'class' => 'edit-controls edit-controlsTopic'.$topic['Topic']['id'])); ?>
	       <?php echo $this->Html->image($imgsave_path, array('id'=>"imgSaveTopic".$topic['Topic']['id'], 'class' => 'save-controls save-controlsTopic'.$topic['Topic']['id'])); ?>
	       <?php echo $this->Html->image($imgundo_path, array('id'=>"imgUndoTopic".$topic['Topic']['id'], 'class' => 'save-controls save-controlsTopic'.$topic['Topic']['id'])); ?>
	    </div>
	</div>

	<script type="text/javascript">

	       
	            
	            $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

	            $('#imgEditTopic<?php echo $topic['Topic']['id'] ?>').click(function() {

	                $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').css('display', 'none');
	                $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').css('display', 'block');

	                $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val($('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').html());

	                $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'block');

	                $('.edit-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');
	            });

	            $('#imgUndoTopic<?php echo $topic['Topic']['id'] ?>').click(function() {

	                $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').css('display', 'block');
	                $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

	                $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

	                $('.edit-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'block');
	            });

	            $('#imgSaveTopic<?php echo $topic['Topic']['id'] ?>').click(function() {

	                var topic_id = $(this).attr('id').replace('imgSaveTopic','');

	                $.ajax({
	                    type: 'POST',
	                    data: {
	                        topic: $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val()
	                    },
	                    url: home_url + '/VideoLibrary/updateTopic/' +  topic_id,
	                    success: function(response) {
	                        
	                        $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').html($('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val());

	                        $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').css('display', 'block');
	                        $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

	                        $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

	                        $('.edit-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'block');

	                    },
	                    errors: function(response) {
	                        alert(response.contents);
	                    }

	                });

	            });

	            $('#imgDeleteTopic<?php echo $topic['Topic']['id'] ?>').click(function() {

	                var topic_id = $(this).attr('id').replace('imgDeleteTopic','');

	                $.ajax({
	                    type: 'POST',
	                    data: {
	                        topic: $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val()
	                    },
	                    url: home_url + '/VideoLibrary/deleteTopic/' +  topic_id,
	                    success: function(response) {
	                        
	                        $('#chkContainerTopic<?php echo $topic['Topic']['id'] ?>').remove();

	                    },
	                    errors: function(response) {
	                        alert(response.contents);
	                    }

	                });

	            });

	       

	    </script>
    <?php endforeach; ?>
<?php endif; ?>