<?php if ($allAccounts): ?>
<div class="">
    <?php 
        $count = 1; 
    ?>
    <?php foreach ($allAccounts as $accounts): ?>
    <?php

        if ($accounts['accounts']['is_suspended'] == true) continue;

        $role = '';
        if (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '100') {
            $role = $language_based_content['account_owner'];
        } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '110') {
            $role = $language_based_content['super_admin'];
        } elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '120') {
            $role = $language_based_content['user'];
        }
        elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '115') {
            $role = $language_based_content['admin'];
        }
        elseif (isset($accounts['roles']['role_id']) && $accounts['roles']['role_id'] == '125') {
            $role = $language_based_content['viewer'];
        }        
        else {
            $role = $language_based_content['user'];
        }
        $div = '';
        $style = '';
        $lastClass = '';
        if ($count % 5 == 0) {
            $style = 'border-right: none;';
            $div = '</div><div class="box  span3 " style="padding: 0px 0px 0px;">';
        }
        $logo_image = $this->Custom->get_site_settings('launchpad_logo');
        
        $logo_path = $this->Custom->getSecureSibmecdnImageUrl("static/companies/" . $accounts['accounts']['account_id'] . "/" . $accounts['accounts']['image_logo'], $accounts['accounts']['image_logo']);
        if (!empty($accounts['accounts']['image_logo'])) {
            $logo_image = $logo_path;
        }
    ?>
    <a href="<?php echo $this->base . '/launchpad/account/' . $accounts['accounts']['account_id'] ?>" title=" <?php echo isset($accounts['accounts']['company_name']) ? $accounts['accounts']['company_name'] : ''; ?>">
        <div class="launchpad_logos" style="<?php echo ($accounts['accounts']['account_id'] == $account_id ? "background-color: #f5f5f5;" : ""); ?>">
            <div class="client_logo">
            <div class="client_logo_inner">
            <?php
            echo $this->Html->image($logo_image, array('height' => '45', 'width' => '140'));
            ?>
            <!--<img src="/app/img/sibme_logo_3.png" width="158" height="51" alt=""/> -->
            </div>
            
            </div>
            <h4 style=" text-align: center;" class="nomargin-vertical compact wrap">
                <?php echo isset($accounts['accounts']['company_name']) ? $accounts['accounts']['company_name'] : ''; ?>  
            </h4> 
            <p><?php echo $role; ?></p>
            <?php if (isset($accounts['accounts']['parent_account_id']) && $accounts['accounts']['parent_account_id'] == $account_id): ?>
                <div class="text-center">
                    <div class="action-buttons btn-group">                           
                        <a data-original-title="Edit" href="<?php echo $this->base . '/accounts/edit/' . $accounts['accounts']['account_id'] ?>" class="btn icon2-edit" rel="tooltip"></a>
                        <a href="<?php echo $this->base . '/accounts/delete/' . $accounts['accounts']['account_id'] ?>"  onclick="return confirm('Are you sure you want to permanently delete this account?')" class="btn icon2-trash" data-method="delete" rel="tooltip nofollow" title="Remove"></a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        </a>
        <?php $count++; ?>
    <?php endforeach; ?>
    <div class="clear"></div>
    </div>

<?php else: ?>
    <div style="min-height: 50px;margin:10px;text-align: center;">
        <?php echo $language_based_content['no_account_found']; ?>
    </div>
<?php endif; ?>