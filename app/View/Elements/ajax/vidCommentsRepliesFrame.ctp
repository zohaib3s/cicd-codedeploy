<?php
$users = $this->Session->read('user_current_account');

$user_id = $users['User']['id'];
$account_folder_id = $huddle[0]['AccountFolder']['account_folder_id'];
?>
<ul class="comment-replies" id="nested_comment_<?php echo $vidComments['Comment']['id'] ?>" style="<?php echo ($counter == 2 ? "padding-left:0px;" : "") ?>">
    <?php
    $replies = $Comment->commentReplys($vidComments['Comment']['id']);
    if (isset($replies) && count($replies) > 0):
        ?>
        <?php
        $count = 0;
        foreach ($replies as $reply):
            if ($this->Custom->check_if_eval_huddle($account_folder_id) == 1) {
                if ($this->Custom->check_if_evaluated_participant($account_folder_id, $user_id)) {
                    if ($user_id != $reply['Comment']['created_by']) {
                        continue;
                    }
                }
            }
            ?>
            <?php
            $commentDate = $reply['Comment']['created_date'];
            $replyDate = $this->Custom->formateDate($commentDate);
            ?>
            <script type="text/javascript">
                $(document).ready(function () {


                    function generate_reply_html(comment_id, postData) {

                        var html = '<div id="comment_box_7302">';

                        html += '<li class="comment">';

                        html += '<a href="#">';

                        html += ' <img width="21" height="21" src="/img/home/photo-default.png" class="photo photo inline" rel="image-uploader" alt="Photo-default">';

                        html += ' </a>';

                        html += ' <div class="comment-header comment-reply">' + postData.comment_from + '<span class="reply-to-icon">&nbsp;</span> <span style="color: #9c9c9c; font-size: 11px;">' + postData.comment_to + '</span></div>';

                        html += '<div class="comment-body comment more">' + postData.comment_current + '</div>';

                        html += '<div class="comment-footer">';

                        html += '<span class="comment-date">Saving Reply...</span>';

                        html += '</div>';

                        html += '<ul class="comment-replies" id="nested_comment_7302" style="">';

                        html += '</ul>';

                        html += '</li>';

                        html += '</div>';

                        $('#nested_comment_' + comment_id).prepend(html);

                    }


                    var isSubmitting = false;
                    $('#comments_form_<?php echo $reply['Comment']['id'] ?>').on('submit', function (event) {


                        event.preventDefault();
                        $('.input-group-reply').hide();
                        var comment_from = '<?php echo $users['User']['first_name'] . " " . $users['User']['last_name']; ?>';
                        var comment_to = '<?php echo str_replace("'", "\\'", $reply['User']['first_name']) . " " . str_replace("'", "\\'", $reply['User']['last_name']); ?>';

                        var comment_current = $('#comment_comment_<?php echo $reply['Comment']['id'] ?>').val();
                        var postData = {
                            comment_from: comment_from,
                            comment_to: comment_to,
                            comment_current: comment_current
                        };

                        generate_reply_html("<?php echo $reply['Comment']['id'] ?>", postData);


                        if (isSubmitting)
                            return false;
                        isSubmitting = true;

                        var $form = $(this);
                        $.ajax({
                            type: $form.attr('method'),
                            url: $form.attr('action'),
                            data: $form.serialize(),
                            success: function () {
                                getVideoComments();
                                isSubmitting = false;
                            }
                        });
                    });

                    $("#reply_comment_form_<?php echo $reply['Comment']['id'] ?>").hide();
                    $("#reply_button_<?php echo $reply['Comment']['id'] ?>").click(function () {
                        $("#reply_comment_form_<?php echo $reply['Comment']['id'] ?>").show("slow");
                    });

                    $("#close_reply_form_<?php echo $reply['Comment']['id'] ?>").click(function () {
                        $("#reply_comment_form_<?php echo $reply['Comment']['id'] ?>").hide("slow");
                    });

                    $("#reply_comment_form_<?php echo $reply['Comment']['id'] ?>").hide();

                    $("#edit-reply-main-comments_<?php echo $reply['Comment']['id'] ?>").click(function () {
                        var comment = $('#edit_reply_comments_<?php echo $reply['Comment']['id'] ?>').val();
                        comment = comment.replace('<br />', '');
                        $('#comments_comment_<?php echo $reply['Comment']['id'] ?>').val(comment);

                        $("#edit_reply_comment_form_<?php echo $reply['Comment']['id'] ?>").show("slow");
                    });

                    $("#close_edit_reply_form_<?php echo $reply['Comment']['id'] ?>").click(function () {
                        $("#edit_reply_comment_form_<?php echo $reply['Comment']['id'] ?>").hide("slow");
                    });

                });
            </script>
            <div id="comment_box_<?php echo $reply['Comment']['id'] ?>">
                <li class="comment">
                    <a href="#<?php //echo $this->base . '/users/editUser/' . $reply['Comment']['user_id'];        ?>">
                        <?php
                        if (isset($reply['User']['image']) && $reply['User']['image'] != ''):
                            $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $reply['User']['user_id'] . "/" . $reply['User']['image']);
                            ?>
                            <?php echo $this->Html->image($profile_image, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '21', 'width' => '21', 'align' => 'left')); ?>
                        <?php else: ?>
                            <img width="21" height="21"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                    </a>
                    <div class="comment-header comment-reply"><?php echo $reply['User']['first_name'] . " " . $reply['User']['last_name']; ?> <span class="reply-to-icon">&nbsp;</span> <span style="color: #9c9c9c; font-size: 11px;"><?php echo $vidComments['User']['first_name'] . " " . $vidComments['User']['last_name']; ?></span></div>
                    <?php
                    if ($reply['Comment']['ref_type'] == '6') {
                        if (Configure::read('use_cloudfront') == true) {
                            $url = $reply['Comment']['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];
                            $ext = '.' . $videoFilePath['extension'];
                            if ($videoFilePath['dirname'] == ".") {

                                $video_path = $this->Custom->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . $ext);
                                $relative_video_path = $videoFileName . $ext;
                            } else {

                                $video_path = $this->Custom->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . $ext);
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . $ext;
                            }
                        } else {
                            $url = $reply['Comment']['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];
                            $ext = '.' . $videoFilePath['extension'];
                            if ($videoFilePath['dirname'] == ".") {

                                $video_path = $this->Custom->getSecureAmazonUrl($videoFileName . $ext);
                                $relative_video_path = $videoFileName . $ext;
                            } else {

                                $video_path = $this->Custom->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName . $ext);
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . $ext;
                            }
                        }
                        ?>
                        <div class="comment-body comment">
                            <audio controls>
                                <source src="<?php echo $video_path; ?>">

                                Your browser does not support the audio element.
                            </audio>

                        <?php } else {
                            ?>
                            <div class="comment-body comment more">  <?php
                                echo nl2br($reply['Comment']['comment']);
                            }
                            ?>
                        </div>
                        <div class="comment-footer">
                            <span class="comment-date"><?php echo $replyDate; ?></span>
                            <div class="comment-actions" style="width: 150px;">
                                <form id="delete-reply-comments-frm<?php echo $reply['Comment']['id']; ?>" action="<?php echo $this->base . '/Huddles/deleteVideoComments/' . $reply['Comment']['id']; ?>" accept-charset="UTF-8">
                                    <?php if (($huddle_permission == '200' || $huddle_permission == '210') || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                        <?php if ($user_current_account['User']['id'] != $reply['Comment']['created_by']): ?>
                                                            <!--<a class="comment-reply" style="cursor: pointer;" id="reply_button_<?php echo $reply['Comment']['id'] ?>">Reply</a>-->
                                        <?php endif; ?>
                                    <?php endif; ?>

                                    <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                        <?php if ($reply['Comment']['ref_type'] != '6') { ?>
                                                    <!--<a  rel="nofollow"  id="edit-reply-main-comments_<?php echo $reply['Comment']['id'] ?>" class="comment-edit"  style="cursor: pointer; display: none;">Edit</a>-->
                                        <?php } ?>
                                                                                        <!--<a rel="nofollow" data-confirm="Are you sure you want to delete this comment?" id="delete-reply-comments" class="comment-delete" href="javascript:deleteCommentreplies('<?php echo $reply['Comment']['id']; ?>');">Delete</a>-->
                                    <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $reply['Comment']['created_by'])): ?>
                                        <?php if ($reply['Comment']['ref_type'] != '6') { ?>
                                                    <!--<a rel="nofollow"  id="edit-reply-main-comments_<?php echo $reply['Comment']['id'] ?>" class="comment-edit"  style="cursor: pointer;">Edit</a>-->
                                        <?php } ?>
                                                                                        <!--<a rel="nofollow" data-confirm="Are you sure you want to delete this comment?" id="delete-reply-comments" class="comment-delete" href="javascript:deleteCommentreplies('<?php echo $reply['Comment']['id']; ?>');">Delete</a>-->
                                    <?php endif; ?>

                                    <script type="text/javascript">
                                        $(document).ready(function () {

                                            function generate_edit_reply_html(comment_id, postData) {

                                                // var html = '<div id="comment_box_7302">';

                                                var html = '<li class="comment">';

                                                html += '<a href="#">';

                                                html += ' <img width="21" height="21" src="/img/home/photo-default.png" class="photo photo inline" rel="image-uploader" alt="Photo-default">';

                                                html += ' </a>';

                                                html += ' <div class="comment-header comment-reply">' + postData.comment_from + '<span class="reply-to-icon">&nbsp;</span> <span style="color: #9c9c9c; font-size: 11px;">' + postData.comment_to + '</span></div>';

                                                html += '<div class="comment-body comment more">' + postData.comment_current + '</div>';

                                                html += '<div class="comment-footer">';

                                                html += '<span class="comment-date">Editing Reply...</span>';

                                                html += '</div>';

                                                html += '<ul class="comment-replies" id="nested_comment_7302" style="">';

                                                html += '</ul>';

                                                html += '</li>';



                                                $('#comment_box_' + comment_id).html(html);

                                            }


                                            $('#delete-reply-comments-frm<?php echo $reply['Comment']['id']; ?>').on('submit', function (event) {
                                                var $form = $(this);
                                                var $target = $($form.attr('data-target'));
                                                $.ajax({
                                                    type: $form.attr('method'),
                                                    url: $form.attr('action'),
                                                    data: $form.serialize(),
                                                    success: function (data, status) {
                                                        getVideoComments();
                                                    }
                                                });
                                                event.preventDefault();
                                            });
                                            $('#edit_reply_comments_form_<?php echo $reply['Comment']['id']; ?>').on('submit', function (event) {
                                                var $form = $(this);
                                                var $target = $($form.attr('data-target'));


                                                var comment_from = '<?php echo $users['User']['first_name'] . " " . $users['User']['last_name']; ?>';
                                                var comment_to = '<?php echo str_replace("'", "\\'", $vidComments['User']['first_name']) . " " . str_replace("'", "\\'", $vidComments['User']['last_name']); ?>';

                                                var comment_current = $('#comments_comment_<?php echo $reply['Comment']['id'] ?>').val();
                                                var postData = {
                                                    comment_from: comment_from,
                                                    comment_to: comment_to,
                                                    comment_current: comment_current
                                                };

                                                generate_edit_reply_html("<?php echo $reply['Comment']['id'] ?>", postData);



                                                $.ajax({
                                                    type: $form.attr('method'),
                                                    url: $form.attr('action'),
                                                    data: $form.serialize(),
                                                    success: function (data, status) {
                                                        getVideoComments();
                                                    }
                                                });
                                                event.preventDefault();
                                            });

                                            $("#comment_comment_<?php echo $reply['Comment']['id'] ?>").keyup(function (e) {
                                                var str_length = $("#comment_comment_<?php echo $reply['Comment']['id'] ?>").val().trim();
                                                if (str_length.length > 0 && e.which == 13) {
                                                    $("#add-replies_<?php echo $reply['Comment']['id'] ?>").trigger("click");
                                                }
                                            });


                                            $("#comments_comment_<?php echo $reply['Comment']['id'] ?>").keyup(function (e) {
                                                var str_length = $("#comments_comment_<?php echo $reply['Comment']['id'] ?>").val().trim();
                                                if (str_length.length > 0 && e.which == 13) {
                                                    $(this).blur();
                                                    $("#edit-replies_<?php echo $reply['Comment']['id'] ?>").trigger("click");
                                                }
                                            });


                                            //                              $(document).on("click", "#save_<?php //echo $reply['Comment']['id']          ?>", function(){
                                            //                                                    Fr.voice.export(function(blob){
                                            //                                                      var formData = new FormData();
                                            //                                                      formData.append('file', blob);
                                            //
                                            //                                                      $.ajax({
                                            //                                                        url: home_url + "/huddles/upload_audio_comment",
                                            //                                                        type: 'POST',
                                            //                                                        data: formData,
                                            //                                                        contentType: false,
                                            //                                                        processData: false,
                                            //                                                        success: function(url) {
                                            //                                                          $("#audio").attr("src", url);
                                            //                                                          $("#audio")[0].play();
                                            //                                                          $('#add_audio_comments_<?php //echo $reply['Comment']['id']          ?>').val(url);
                                            //                                                          alert("Saved In Server1. See audio element's src for URL");
                                            //                                                        }
                                            //                                                      });
                                            //                                                    }, "blob");
                                            //                                                  restore();
                                            //                                                  });
                                            //
                                            //                                                   $(document).on("click", "#record_<?php //echo $reply['Comment']['id']          ?>", function(){
                                            //                                                    elem = $(this);
                                            //                                                    Fr.voice.record($("#live").is(":checked"), function(){
                                            //                                                      elem.addClass("disabled");
                                            //                                                      $("#live").addClass("disabled");
                                            //                                                      $(".one").removeClass("disabled");
                                            //
                                            //                                                      /**
                                            //                                                       * The Waveform canvas
                                            //                                                       */
                                            //                                                      analyser = Fr.voice.context.createAnalyser();
                                            //                                                      analyser.fftSize = 2048;
                                            //                                                      analyser.minDecibels = -90;
                                            //                                                      analyser.maxDecibels = -10;
                                            //                                                      analyser.smoothingTimeConstant = 0.85;
                                            //                                                      Fr.voice.input.connect(analyser);
                                            //
                                            //                                                      var bufferLength = analyser.frequencyBinCount;
                                            //                                                      var dataArray = new Uint8Array(bufferLength);
                                            //
                                            //                                                      WIDTH = 500, HEIGHT = 200;
                                            //                                                      canvasCtx = $("#level")[0].getContext("2d");
                                            //                                                      canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);
                                            //
                                            //                                                      function draw() {
                                            //                                                        drawVisual = requestAnimationFrame(draw);
                                            //                                                        analyser.getByteTimeDomainData(dataArray);
                                            //                                                        canvasCtx.fillStyle = 'rgb(200, 200, 200)';
                                            //                                                        canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
                                            //                                                        canvasCtx.lineWidth = 2;
                                            //                                                        canvasCtx.strokeStyle = 'rgb(0, 0, 0)';
                                            //
                                            //                                                        canvasCtx.beginPath();
                                            //                                                        var sliceWidth = WIDTH * 1.0 / bufferLength;
                                            //                                                        var x = 0;
                                            //                                                        for(var i = 0; i < bufferLength; i++) {
                                            //                                                          var v = dataArray[i] / 128.0;
                                            //                                                          var y = v * HEIGHT/2;
                                            //
                                            //                                                          if(i === 0) {
                                            //                                                            canvasCtx.moveTo(x, y);
                                            //                                                          } else {
                                            //                                                            canvasCtx.lineTo(x, y);
                                            //                                                          }
                                            //
                                            //                                                          x += sliceWidth;
                                            //                                                        }
                                            //                                                        canvasCtx.lineTo(WIDTH, HEIGHT/2);
                                            //                                                        canvasCtx.stroke();
                                            //                                                      };
                                            //                                                      draw();
                                            //                                                    });
                                            //                                                  });



                                        });


                                        function deleteComment(commentId) {
                                            $.ajax({
                                                type: 'GET',
                                                url: home_url + '/Huddles/deleteVideoComments/' + commentId,
                                                success: function (response) {
                                                    getVideoComments();
                                                }
                                            });

                                            return false;
                                        }










                                    </script>
                                </form>
                            </div>
                            <div id="reply_comment_form_<?php echo $reply['Comment']['id'] ?>" style="display: none;">
                                <form method="post" id="comments_form_<?php echo $reply['Comment']['id']; ?>"
                                      class="new_comment" action="<?php echo $this->base . '/Huddles/addReply/' . $reply['Comment']['id']; ?>"
                                      accept-charset="UTF-8">
                                    <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"></div>
                                    <input type="hidden" value="D91PQMV0iwdtcBfDzvuRAfxMrDboT3D1kX75GvptfOI=" name="authenticity_token" id="tokentag">
                                    <input type="hidden" value="<?php echo $vidComments['Comment']['id']; ?>" name="ref_id" id="ref_id">
                                    <div class="input-group input-group-reply">
                                        <textarea rows="3" placeholder="Add Reply..." name="comment[comment]" required id="comment_comment_<?php echo $reply['Comment']['id'] ?>" cols="35"></textarea>
                                    </div>
                                    <input type="hidden" value="nested_multilevel" name="comment[access_level]" id="comment_access_level">

                                    <div class="input-group input-group-reply">
                                        <input id="add-replies_<?php echo $reply['Comment']['id'] ?>" type="submit" class="btn btn-green" value="Add Reply" name="submit">
                                        <a class="btn btn-transparent" id="close_reply_form_<?php echo $reply['Comment']['id'] ?>">Cancel</a>
            <!--                            <input name="comment[audio_comments]" id="add_audio_comments_<?php //echo $reply['Comment']['id']          ?>" type="hidden" value=""/>
                                            <img alt="Indicator" id="record_<?php //echo $reply['Comment']['id']          ?>" class="show_recorder" src="<?php //echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/recorder.jpg');          ?>" />
                                                <img alt="Indicator" id="play_<?php //echo $reply['Comment']['id']          ?>" class="show_player" style="display:inline;" src="<?php //echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/icons/total_videos.png');          ?>" />
                                                <a class="button disabled one" id="save_<?php //echo $reply['Comment']['id']          ?>">Upload</a>-->
                                    </div>
                                </form>
                            </div>
                            <div id="edit_reply_comment_form_<?php echo $reply['Comment']['id'] ?>" style="display: none;">
                                <form method="post" id="edit_reply_comments_form_<?php echo $reply['Comment']['id']; ?>"
                                      class="new_comment" action="<?php echo $this->base . '/Huddles/editComments/' . $reply['Comment']['id']; ?>"
                                      accept-charset="UTF-8">
                                    <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"></div>
                                    <input type="hidden" value="D91PQMV0iwdtcBfDzvuRAfxMrDboT3D1kX75GvptfOI=" name="authenticity_token" id="tokentag">
                                    <input type="hidden" value="<?php echo $vidComments['Comment']['id']; ?>" name="ref_id" id="ref_id">
                                    <input type="hidden" name="comments" value="<?php echo $reply['Comment']['comment']; ?>" id="edit_reply_comments_<?php echo $reply['Comment']['id'] ?>"/>
                                    <input type="hidden" value="<?php echo $reply['Comment']['id'] ?>" name="comment_id" />
                                    <div class="input-group input-group-reply">
                                        <textarea rows="3" placeholder="Add Reply..." name="comment[comment]" required id="comments_comment_<?php echo $reply['Comment']['id'] ?>" cols="35"></textarea>
                                    </div>
                                    <input type="hidden" value="nested_multilevel" name="comment[access_level]" id="comment_access_level">

                                    <div class="input-group input-group-reply">
                                        <input id="edit-replies_<?php echo $reply['Comment']['id'] ?>" type="submit" class="btn btn-green" value="Edit" name="submit">
                                        <a class="btn btn-transparent" id="close_edit_reply_form_<?php echo $reply['Comment']['id'] ?>">Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php
                        echo $this->element('ajax/vidCommentsRepliesFrame', array(
                            "vidComments" => $reply,
                            "huddle_permission" => $huddle_permission,
                            "user_current_account" => $user_current_account,
                            "huddle" => $huddle,
                            "user_permissions" => $user_permissions,
                            "counter" => ($counter < 2 ? ($counter + 1) : $counter )
                        ));
                        ?>
                </li>
            </div>

            <?php
            $count++;
        endforeach;
        ?>
    <?php endif; ?>

</ul>
