<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
?>

<?php if (is_array($documents) && count($documents) > 0): ?>
    <?php foreach ($documents as $row): ?>
        <?php $associatedVideo = $Document->getVideoDocuments($row['Document']['id'], $huddle_id); ?>
        <li id="li_doc_<?php echo $row['Document']['id']; ?>" class="padd">
            <form class="delete-document" action="<?php echo $this->base . '/Huddles/deleteDocument/' . $row['Document']['id'] . '/' . $huddle_id; ?>" accept-charset="UTF-8">
                <?php if ($huddle_permission == '200' || ($user_current_account['User']['id'] == $row['Document']['created_by'])): ?>
                    <a rel="nofollow" data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_resource']; ?>" id="delete-main-comments" class="icon-doc icon-indent blue-link delete" href="<?php echo $this->base . '/Huddles/delDocument/' . $row['Document']['id'] . '/' . $huddle_id; ?>">&nbsp;</a>
                <?php endif; ?>
                <label class="ui-checkbox checkbox-2">
                    <input class="checkbox-2 download_doc_checkbox"  name="document_ids[]" type="checkbox" value="<?php echo $row['Document']['id']; ?>"/>
                </label>
                <?php if ($huddle_permission == 200 || $huddle_permission == 210 || $huddle_permission == 220 || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>

                    <?php if (isset($row['Document']['stack_url']) && $row['Document']['stack_url'] != '') {
                        ?>
                        <?php
                        $url_code = explode('/', $row['Document']['stack_url']);
                        $url_code = $url_code[3];
                        ?>
                        <a  id="view_resource_<?php echo $row['Document']['id']; ?>" target="_blank" href="<?php echo $this->base . '/huddles/view_document/' . $url_code; ?>" class="nomargin-vertical wrap">
                            <span class="<?php echo $this->Custom->getIcons($row['Document']['url']); ?>">&nbsp;</span>
                            <?php echo $row['afd']['title'] ?>
                        </a>

                    <?php } else { ?>
                        <a href="<?php echo $this->base . '/Huddles/download/' . $row['Document']['id']; ?>" class="nomargin-vertical wrap">
                            <span class="<?php echo $this->Custom->getIcons($row['Document']['url']); ?>">&nbsp;</span>
                            <?php echo $row['afd']['title'] ?>
                        </a>

                    <?php } ?>
                <?php else: ?>
                    <a  class="nomargin-vertical wrap" >
                        <?php echo $row['afd']['title'] ?>
                    </a>
                <?php endif; ?>
                <strong class="type "><?php echo $this->Custom->docType($row['Document']['url']); ?></strong>
                <strong class="date"><?php echo date('M d, Y', strtotime($row['Document']['created_date'])); ?></strong>

                <div class="doc_video_association">
                    <div class="subject-row row">
                        <?php $showAssociateVideo = $videos && ($huddle_permission == 210 || $huddle_permission == 200); ?>
                        <div class="video-dropdown" style="<?php echo!$showAssociateVideo ? 'visibility:hidden;' : ''; ?>">Videos</div>
                        <?php if ($showAssociateVideo): ?>
                            <div class="video-dropdown-container">
                                <div class="container">
                                    <ul>
                                        <?php foreach ($videos as $video): ?>
                                            <li>
                                                <?php
                                                $is_video_associated = "";
                                                if (is_array($associatedVideo) && count($associatedVideo) > 0):
                                                    foreach ($associatedVideo as $row2):
                                                        if (is_array($row2) && count($row2) > 0 && $row2['vafd']['video_id'] == $video['Document']['id']):
                                                            $is_video_associated = "checked";
                                                        endif;
                                                    endforeach;
                                                else:
                                                endif;
                                                ?>
                                                <label class="ui-checkbox checkbox-2">
                                                    <input id="video_ids_<?php echo $video['Document']['id'] . '_' . $row['Document']['id']; ?>" name="video_ids<?php echo $video['Document']['id'] ?>" <?php echo $is_video_associated; ?>
                                                           class="checkbox-3 doc_checkbox_<?php echo $row['Document']['id']; ?>" type="checkbox" value="<?php echo $video['Document']['id'] ?>">
                                                </label>
                                                <label for="video_ids_<?php echo $video['Document']['id'] . '_' . $row['Document']['id']; ?>" title="<?php echo $video['afd']['title']; ?>" class="title"><?php echo $video['afd']['title']; ?></label>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                                <div class="video-dropdown-container-bottom">
                                    <input type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>"  class="btnDoneVideoLibrary" id="btnDoneDoc-<?php echo $row['Document']['id']; ?>" value="Save"/>
                                    <input type="button" class="btnCancelVideoLibrary btn btn-grey"  id="btnDoneDoc-<?php echo $row['Document']['id']; ?>" value="Cancel"/>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </form>
        </li>

        <script>
            $("#view_resource_<?php echo $row['Document']['id']; ?>").click(function () {
                var id = '<?php echo $row['Document']['id']; ?>';

                $.ajax({
                    url: home_url + '/huddles/view_resource/' + id,
                    type: 'POST',
                    success: function (response) {


                    },
                    error: function () {

                    }
                });
            });
        </script>
    <?php endforeach; ?>
<?php else: ?>
    <?php
    if (isset($_POST['state']) && $_POST['state'] == 'init'):
        ?>
        <li><?php echo $alert_messages['no_resources_have_been_uploaded_in_huddle'];?><li>
            <?php
        else:
            ?>
        <li><?php echo $alert_messages['none_of_your_resources_have_matched'];?><li>
        <?php
        endif;
        ?>
    <?php endif; ?>
