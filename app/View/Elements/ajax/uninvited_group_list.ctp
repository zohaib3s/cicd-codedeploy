
<?php $uninvited_groups = $data; ?>
<?php if (count($uninvited_groups) > 0): ?>
    <ul>
        <?php foreach($uninvited_groups as $group):?>
            <li>
                <?php
                    $groupInfo = array(
                        'id' => $group['Group']['id'],
                        'name' => $group['Group']['name']
                    );
                ?>
                <input type="hidden" name="unselected_group[]" class="group-info" value="<?php echo $groupInfo['id'] . '::' . $groupInfo['name']; ?>" />
                <div class="group-name">
                    <label>
                        <input type="checkbox" name="group_id" value="<?php echo $groupInfo['id'];?>" />
                        <span><?php echo $groupInfo['name'];?></span>
                    </label>
                </div>
                <div class="group-role">
                    <select name="unselected_group_role[]">
                        <option value="200">Admin</option>
                        <option value="210">Member</option>
                        <option value="220" selected="selected">Viewer</option>
                    </select>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
