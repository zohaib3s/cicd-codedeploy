<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
$amazon_base_url = Configure::read('amazon_base_url');
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$users = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$account_id = $users['accounts']['account_id'];

$isFirefoxBrowser = $this->Browser->isFirefox();
?>
<?php if (is_array($videos) && isset($videos[0]['Document']['id'])): ?>
    <?php foreach ($videos as $row): ?>
        <?php
        $transcoding_status = $this->Custom->transcoding_status($row['Document']['id']);
        $videoID = $row['Document']['id'];
        $videoFilePath = pathinfo($row['Document']['url']);
        $videoFileName = $videoFilePath['filename'];

        if ($huddle_type == 3):
            $created_by = $row['Document']['created_by'];
            $is_avaluator = $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']);
            $evaluators_ids = $this->Custom->get_evaluator_ids($huddle_id);
            $participants_ids = $this->Custom->get_participants_ids($huddle_id);
            if ($is_avaluator && $users['roles']['role_id'] == 120) {
                $isEditable = ($user_current_account['User']['id'] == $row['Document']['created_by'] || (is_array($participants_ids) && in_array($row['Document']['created_by'], $participants_ids)));
            } elseif ($is_avaluator && ($users['roles']['role_id'] == 110 || $users['roles']['role_id'] == 100 || $users['roles']['role_id'] == 115 )) {
                $isEditable = ($is_avaluator || ($user_current_account['User']['id'] == $row['Document']['created_by'] || (is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))));
            } else {
                $isEditable = (($user_current_account['User']['id'] == $row['Document']['created_by'] || (is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))));
            }
            if ($isEditable):
                $total_videos++;
                ?>
                <li class="videos-list__item">
                    <?php if ($users['roles']['role_id'] != 125): ?>
                        <div class="ac-btn">
                            <?php if ($user_current_account['User']['id'] == $row['Document']['created_by'] || $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id'])): ?>
                                <?php if ($this->Custom->check_if_submission_date_passed($huddle_id, $user_current_account['User']['id'])): ?>
                                    <a href="javascript:void(0);" onclick="alert('<?php echo $alert_messages['you_cannot_delete_video_submission_expired']; ?>');" data-original-title="<?php echo $language_based_content['delete_videos']; ?>" rel="tooltip" data-method="delete" class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;padding: 0px;"></a>
                                <?php else: ?>
                                    <?php if ($this->Custom->dis_mem_del_video($account_id, $huddle_permission, $huddle_type)): ?>
                                        <a href="<?php echo $this->base . '/Huddles/deleteHuddleVideo/' . $huddle_id . '/1/' . $row['Document']['id'] ?>"
                                           data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                                           data-original-title="<?php echo $language_based_content['delete_videos']; ?>" rel="tooltip" data-method="delete"
                                           class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;padding: 0px;"></a>
                                       <?php endif; ?>
                                   <?php endif; ?>
                               <?php endif; ?>

                            <?php if (!$isFirefoxBrowser && $row['Document']['published'] == 1 && ($user_current_account['User']['id'] == $row['Document']['created_by'] || $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']) )): ?>
                                <a class="btn-trim-video btn icon2-crop right smargin-right fl-btn trim-btn-checks"
                                   title="<?php echo $language_based_content['edit_videos']; ?>" style="border:none;background: none;border-radius: 0px;padding: 0px;"
                                   rel="tooltip"></a>
                                <form method="post" action="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>/1" style="display:none;">
                                    <input type="hidden" name="open_trim" value ="1"/>
                                </form>
                            <?php endif; ?>
                            <?php if ($row['Document']['published'] == 1 && ($user_current_account['User']['id'] == $row['Document']['created_by'] || $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']) )): ?>
                                <a class="copy" id="copy-huddle-<?php echo $row['Document']['id'] ?>"
                                   data-document-id ="<?php echo $row['Document']['id'] ?>"
                                   data-total-comments ="<?php echo $row['Document']['total_comments'] ?>"
                                   data-original-title="<?php echo $language_based_content['copy_videos']; ?>" rel="tooltip"
                                   data-toggle="modal" data-target="#moveFiles"><?php echo $language_based_content['copy_videos']; ?></a>
                               <?php endif; ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#copy-huddle-<?php echo $row['Document']['id'] ?>').click(function () {
                                        $('.copy-document-ids').val($(this).attr('data-document-id'));
                                        if ($(this).attr('data-total-comments') == 0)
                                            $('.copy_video_box').css('display', 'none');
                                        else
                                            $('.copy_video_box').css('display', 'block');
                                    });
                                });
                            </script>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                    <div class="videos-list__item-thumb">

                        <?php
                        $videoID = $row['Document']['id'];

                        $document_files_array = $this->Custom->get_document_url($row['Document']);

                        if (empty($document_files_array['url'])) {
                            $row['Document']['published'] = 0;
                            $document_files_array['url'] = $row['Document']['original_file_name'];
                            $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            @$row['Document']['duration'] = $document_files_array['duration'];
                        }
                        ?>

                        <?php
                        if ($row['Document']['published'] == '1' && $transcoding_status != 5):
                            $seconds = $row['Document']['duration'] % 60;
                            $minutes = ($row['Document']['duration'] / 60) % 60;
                            $hours = gmdate("H", $row['Document']['duration']);
                            ?>
                            <a href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                <?php
                                $thumbnail_image_path = $document_files_array['thumbnail'];

                                echo $this->Html->image($thumbnail_image_path, array('height' => 146, 'class' => 'thumb_img'));
                                ?>
                                <div class="play-icon"></div>
                                <?php if ($transcoding_status != 2): ?>
                                    <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                                <?php endif; ?>
                            </a>
                        <?php else: ?>
                            <a id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                <div  class="video-unpublished ">
                                    <span class="huddles-unpublished">
                                        <?php if ($row['Document']['encoder_status'] == 'Error' || $transcoding_status == 5): ?>
                                            Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                        <?php else : ?>
                                            <img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br><?=$alert_messages["Your_video_is_currently_processing"]; ?>
                                        <?php endif; ?>
                                    </span>
                                </div>
                            </a>
                        <?php endif; ?>
                    </div>
                    <div class="videos-list__item-aside">
                        <!--                <div class="videos-list__item-title">
                                            <a class="wrap" id="vide-title-<?php //echo $row['Document']['id'];                                 ?>" href="<?php //echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id'];                                 ?>" title="<?php //echo $row['afd']['title']                                 ?>" ><?php //echo (strlen($row['afd']['title']) > 26 ? substr($row['afd']['title'], 0, 26) . "..." : $row['afd']['title'])                                 ?></a>
                                        </div>-->

                        <div id="input-title-<?php echo $row['afd']['id']; ?>" style="display: none;height:35px;">
                            <input id="input-field-<?php echo $row['afd']['id']; ?>" type="text" name="title" value="" required="required" style="margin-right: 5px;width:150px; padding:6px 4px;"/>
                            <a onclick="return update_title_grid(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php
                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/icons/save-btn.png');
                                echo $this->Html->image($chimg);
                                ?></a>
                            <a onclick="return cancel(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php
                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/icons/cancel-btn.png');
                                echo $this->Html->image($chimg);
                                ?></a>
                        </div>
                        <?php if ($user_current_account['User']['id'] == $row['Document']['created_by'] || $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id'])): ?>

                            <div id="videos-title-<?php echo $row['afd']['id']; ?>" class="videos-title cursor-pointer" data-title="<?php echo $row['afd']['title']; ?>" style="width:200px;">
                                <div class="videos-list__item-title">
                                    <a class="wrap wrap2" id="vide-title-<?php echo $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>"><?php echo (strlen($row['afd']['title']) > 35 ? mb_substr($row['afd']['title'], 0, 35) . "..." : $row['afd']['title']) ?></a>
                                </div>
                                <div style="clear: both;"> </div>
                            </div>
                        <?php else: ?>
                            <div class="videos-list__item-title">
                                <a class="wrap" id="vide-title-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" ><?php echo (strlen($row['afd']['title']) > 52 ? mb_substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title']) ?></a>
                            </div>

                        <?php endif; ?>


                        <div class="videos-list__item-author"><?php echo $language_based_content['by_videos']; ?> <a href="#" title="<?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?>" ><?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?></a></div>
                        <div class="videos-list__item-added"><?php echo $language_based_content['uploaded_videos']; ?> <?php if($_SESSION['LANG'] == 'es'){ echo $this->Custom->SpanishDate(strtotime($row['Document']['created_date']) , 'most_common' );  } else { echo date('M d, Y', strtotime($row['Document']['created_date']));} ?>
                            <div class="clear"></div>
                            <div style="float:left;    position: relative;top: 6px; padding-bottom: 4px;color: #1873bd;">

                                <span>
                                    <img src="<?php echo $this->webroot . 'img/comment_numbers.png'; ?>"> <?php echo $this->Custom->get_video_comment_numbers($row['Document']['id'], $huddle_id, $user_current_account['User']['id']); ?>
                                </span>

                                <span>
                                    <img src="<?php echo $this->webroot . 'img/attachment_numbers.png'; ?>"> <?php
                                    $is_avaluator = $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']);
                                    if ($is_avaluator) {
                                        echo $this->Custom->get_video_attachment_numbers($row['Document']['id']);
                                    } else {
                                        echo $this->Custom->get_video_attachment_numbers($row['Document']['id'], 1);
                                    }
                                    ?>
                                </span>

                            </div>
                            <div style="float: right;    margin-right: -10px;margin-top: -7px;    position: relative; top: 4px;">
                                <?php if (($row['Document']['published'] == '1') && ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $row['Document']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1'))): ?>

                                    <?php
                                    $video_path = $document_files_array['url'];
                                    ?>

                                    <a style="float: left;" href="<?php echo $this->webroot . 'Huddles/download/' . $row['Document']['id']; ?>" target="_blank" download="<?php echo $videoFileName . "_enc.mp4"; ?>">
                                        <img alt="Download" class="right smargin-right" style="height: 28px;margin-top: -2px;" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>" title="download video"  />
                                    </a>
                                <?php elseif (($row['Document']['published'] == '1') && $huddle_type == 2): ?>
                                    <?php
                                    $video_path = $document_files_array['url'];
                                    ?>
                                    <?php if (($coachee_permissions && $huddle_permission == '210') || $huddle_permission == '200'): ?>

                                    <a style="float: left;" href="<?php echo $this->webroot . 'Huddles/download/' . $row['Document']['id']; ?>" target="_blank" download="<?php echo $videoFileName . "_enc.mp4"; ?>">
                                        <img alt="Download" class="right smargin-right" style="height: 28px;margin-top: -2px;" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>" title="download video"  />
                                    </a>
                                
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                </li>
                <script>
                    $(document).ready(function (e) {
                        var $input = "#input-field-<?php echo $row['afd']['id']; ?>";
                        var $id = "#videos-title-<?php echo $row['afd']['id']; ?>";
                        var $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
                        $($id).click(function (e) {
                            var $title = $(this).attr('data-title');
                            $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
                            $input = "#input-field-<?php echo $row['afd']['id']; ?>";
                            $(this).css("display", 'none');
                            $($title_block).css("display", 'block');
                            $($input).val($title);
                        });
                        $($id).mouseover(function () {
                            $(this).css("backgroundColor", "#FAFABE")
                        });
                        $($id).mouseout(function () {
                            $(this).css("backgroundColor", "#ffffff")
                        });
                    });

                </script>

            <?php endif; ?>
        <?php else: ?>
            <li class="videos-list__item">
                <?php
                $isEditable = ($huddle_permission == 200) ||
                        ($huddle_permission == 210 && $user_current_account['User']['id'] == $row['Document']['created_by']) ||
                        ($user_current_account['User']['id'] == $row['Document']['created_by'] && $user_permissions['UserAccount']['permission_maintain_folders'] == '1');
                ?>
                <?php if ($isEditable): ?>
                    <?php if ($users['roles']['role_id'] != 125): ?>
                        <div class="ac-btn">
                            <?php if ($this->Custom->dis_mem_del_video($account_id, $huddle_permission, $huddle_type)): ?>
                                <a href="<?php echo $this->base . '/Huddles/deleteHuddleVideo/' . $huddle_id . '/1/' . $row['Document']['id'] ?>"
                                   data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                                   data-original-title="<?php echo $language_based_content['delete_videos']; ?>" rel="tooltip" data-method="delete"
                                   class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;padding: 0px;"></a>
                               <?php endif; ?>

                            <?php if (!$isFirefoxBrowser && $row['Document']['published'] == 1): ?>
                                <a class="btn-trim-video btn icon2-crop right smargin-right fl-btn"
                                   title="<?php echo $language_based_content['edit_videos']; ?>" style="border:none;background: none;border-radius: 0px;padding: 0px;"
                                   rel="tooltip"></a>
                                <form method="post" action="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>/1" style="display:none;">
                                    <input type="hidden" name="open_trim" value ="1"/>
                                </form>
                            <?php endif; ?>
                            <?php if ($row['Document']['published'] == 1): ?>
                                <a class="copy" id="copy-huddle-<?php echo $row['Document']['id'] ?>"
                                   data-document-id ="<?php echo $row['Document']['id'] ?>"
                                   data-total-comments ="<?php echo $row['Document']['total_comments'] ?>"
                                   data-original-title="<?php echo $language_based_content['copy_videos']; ?>" rel="tooltip"
                                   data-toggle="modal" data-target="#moveFiles"><?php echo $language_based_content['copy_videos']; ?></a>
                               <?php endif; ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#copy-huddle-<?php echo $row['Document']['id'] ?>').click(function () {
                                        $('.copy-document-ids').val($(this).attr('data-document-id'));
                                        if ($(this).attr('data-total-comments') == 0)
                                            $('.copy_video_box').css('display', 'none');
                                        else
                                            $('.copy_video_box').css('display', 'block');
                                    });
                                });
                            </script>
                        </div>
                    <?php endif; ?>
                    <?php
                elseif ($huddle_type == 2):

                    $is_avaluator = $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']);
                    ?>
                    <?php if ($users['roles']['role_id'] != 125): ?>
                        <div class="ac-btn">
                            <?php if (($coachee_permissions && $huddle_permission == 210 ) || $huddle_permission == 200): ?>  <?php if ($this->Custom->dis_mem_del_video($account_id, $huddle_permission, $huddle_type)): ?>     <a href="<?php echo $this->base . '/Huddles/deleteHuddleVideo/' . $huddle_id . '/1/' . $row['Document']['id'] ?>"
                                       data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                                       data-original-title="<?php echo $language_based_content['delete_videos']; ?>" rel="tooltip" data-method="delete"
                                       class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;padding: 0px;"></a> <?php endif; ?><?php endif; ?>

                            <?php if (!$isFirefoxBrowser && $row['Document']['published'] == 1): ?>
                                <?php if (($coachee_permissions && $huddle_permission == 210 ) || $huddle_permission == 200): ?> <a class="btn-trim-video btn icon2-crop right smargin-right fl-btn"
                                       title="<?php echo $language_based_content['edit_videos']; ?>" style="border:none;background: none;border-radius: 0px;padding: 0px;"
                                       rel="tooltip"></a> <?php endif; ?>
                                <form method="post" action="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>/1" style="display:none;">
                                    <input type="hidden" name="open_trim" value ="1"/>
                                </form>
                            <?php endif; ?>

                            <?php if (($coachee_permissions && $huddle_permission == 210 ) || $huddle_permission == 200): ?>      <a class="copy" id="copy-huddle-<?php echo $row['Document']['id'] ?>"
                                   data-document-id ="<?php echo $row['Document']['id'] ?>"
                                   data-total-comments ="<?php echo $row['Document']['total_comments'] ?>"
                                   data-original-title="<?php echo $language_based_content['copy_videos']; ?>" rel="tooltip"
                                   data-toggle="modal" data-target="#moveFiles"><?php echo $language_based_content['copy_videos']; ?></a> <?php endif; ?>

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#copy-huddle-<?php echo $row['Document']['id'] ?>').click(function () {
                                        $('.copy-document-ids').val($(this).attr('data-document-id'));
                                        if ($(this).attr('data-total-comments') == 0)
                                            $('.copy_video_box').css('display', 'none');
                                        else
                                            $('.copy_video_box').css('display', 'block');
                                    });
                                });
                            </script>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
                <div class="clearfix"></div>
                <div class="videos-list__item-thumb">

                    <?php
                    $videoID = $row['Document']['id'];

                    $document_files_array = $this->Custom->get_document_url($row['Document']);

                    if (empty($document_files_array['url'])) {
                        $row['Document']['published'] = 0;
                        $document_files_array['url'] = $row['Document']['original_file_name'];
                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                    } else {
                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        @$row['Document']['duration'] = $document_files_array['duration'];
                    }
                    ?>

                    <?php
                    if ($row['Document']['published'] == '1' && $transcoding_status != 5):
                        $seconds = $row['Document']['duration'] % 60;
                        $minutes = ($row['Document']['duration'] / 60) % 60;
                        $hours = gmdate("H", $row['Document']['duration']);
                        ?>
                        <a href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                            <?php
                            $thumbnail_image_path = $document_files_array['thumbnail'];

                            echo $this->Html->image($thumbnail_image_path, array('height' => 146, 'class' => 'thumb_img'));
                            ?>
                            <div class="play-icon"></div>

                            <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>

                        </a>
                    <?php else: ?>
                        <a id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                            <div  class="video-unpublished ">
                                <span class="huddles-unpublished">
                                    <?php if ($row['Document']['encoder_status'] == 'Error' || $transcoding_status == 5): ?>
                                        Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                    <?php elseif ($row['Document']['published'] == '0' && $row['Document']['video_is_saved'] == '0' && $row['Document']['is_processed'] == '4'): ?>
                                        <br>This Live Video Recording was not Saved.
                                    <?php else : ?>
                                        <img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br><?=$alert_messages["Your_video_is_currently_processing"]; ?>
                                    <?php endif; ?>
                                </span>
                            </div>
                        </a>
                    <?php endif; ?>
                </div>
                <div class="videos-list__item-aside">
                    <!--                <div class="videos-list__item-title">
                                        <a class="wrap" id="vide-title-<?php //echo $row['Document']['id'];                                 ?>" href="<?php //echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id'];                                 ?>" title="<?php //echo $row['afd']['title']                                 ?>" ><?php //echo (strlen($row['afd']['title']) > 26 ? substr($row['afd']['title'], 0, 26) . "..." : $row['afd']['title'])                                 ?></a>
                                    </div>-->

                    <div id="input-title-<?php echo $row['afd']['id']; ?>" style="display: none;height:35px;">
                        <input id="input-field-<?php echo $row['afd']['id']; ?>" type="text" name="title" value="" required="required" style="margin-right: 5px;width:150px; padding:6px 4px;"/>
                        <a onclick="return update_title_grid(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php
                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/icons/save-btn.png');
                            echo $this->Html->image($chimg);
                            ?></a>
                        <a onclick="return cancel(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php
                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/icons/cancel-btn.png');
                            echo $this->Html->image($chimg);
                            ?></a>
                    </div>
                    <div id="videos-title-<?php echo $row['afd']['id']; ?>" class="videos-title cursor-pointer" data-title="<?php echo $row['afd']['title']; ?>" style="width:200px;">
                        <div class="videos-list__item-title">
                            <a class="wrap wrap2" id="vide-title-<?php echo $row['Document']['id']; ?>"  title="<?php echo $row['afd']['title'] ?>" ><?php echo (strlen($row['afd']['title']) > 35 ? mb_substr($row['afd']['title'], 0, 35) . "..." : $row['afd']['title']) ?></a>
                        </div>
                        <div style="clear: both;"> </div>
                    </div>


                    <div class="videos-list__item-author"> <?php echo $language_based_content['by_videos']; ?> <a href="#" title="<?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?>" ><?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?></a></div>
                    <div class="videos-list__item-added"><?php echo $language_based_content['uploaded_videos']; ?> <?php if($_SESSION['LANG'] == 'es'){ echo $this->Custom->SpanishDate(strtotime($row['Document']['created_date']) , 'most_common' );  } else { echo date('M d, Y', strtotime($row['Document']['created_date']));} ?>
                        <div class="clear"></div>
                        <div style="float:left;    position: relative;top: 6px; padding-bottom: 4px;color: #1873bd;">

                            <span>
                                <img src="<?php echo $this->webroot . 'img/comment_numbers.png'; ?>"> <?php echo $this->Custom->get_video_comment_numbers($row['Document']['id'], $huddle_id, $user_current_account['User']['id']); ?>
                            </span>

                            <span>
                                <img src="<?php echo $this->webroot . 'img/attachment_numbers.png'; ?>"> <?php
                                $is_avaluator = $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']);
                                if ($is_avaluator) {
                                    echo $this->Custom->get_video_attachment_numbers($row['Document']['id']);
                                } else {
                                    echo $this->Custom->get_video_attachment_numbers($row['Document']['id'], 1);
                                }
                                ?>
                            </span>

                        </div>
                        <div style="float: right;    margin-right: -10px;margin-top: -7px;    position: relative; top: 4px;">
                            <?php if (($row['Document']['published'] == '1') && ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $row['Document']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1'))): ?>

                                <?php
                                $video_path = $document_files_array['url'];
                                ?>

                                <a style="float: left;" href="<?php echo $this->webroot . 'Huddles/download/' . $row['Document']['id']; ?>" target="_blank" download="<?php echo $videoFileName . "_enc.mp4"; ?>">
                                    <img alt="Download" class="right smargin-right" style="height: 28px;margin-top: -2px;" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>" title="download video"  />
                                </a>
                            <?php elseif (($row['Document']['published'] == '1') && $huddle_type == 2): ?>
                                <?php
                                $video_path = $document_files_array['url'];
                                ?>
                            
                            <?php if (($coachee_permissions && $huddle_permission == '210') || $huddle_permission == '200'): ?>

                                <a style="float: left;" href="<?php echo $this->webroot . 'Huddles/download/' . $row['Document']['id']; ?>" target="_blank" download="<?php echo $videoFileName . "_enc.mp4"; ?>">
                                    <img alt="Download" class="right smargin-right" style="height: 28px;margin-top: -2px;" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>" title="download video"  />
                                </a>
                            <?php endif; ?>
                            
                            <?php endif; ?>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </li>

            <script>
                $(document).ready(function (e) {
                    var $input = "#input-field-<?php echo $row['afd']['id']; ?>";
                    var $id = "#videos-title-<?php echo $row['afd']['id']; ?>";
                    var $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
                    $($id).click(function (e) {
                        var $title = $(this).attr('data-title');
                        $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
                        $input = "#input-field-<?php echo $row['afd']['id']; ?>";
                        $(this).css("display", 'none');
                        $($title_block).css("display", 'block');
                        $($input).val($title);
                    });
                    $($id).mouseover(function () {
                        $(this).css("backgroundColor", "#FAFABE")
                    });
                    $($id).mouseout(function () {
                        $(this).css("backgroundColor", "#ffffff")
                    });
                });


            </script>
        <?php endif; ?>
    <?php endforeach; ?>
<?php else: ?>
    <li class="videos-list__item_noitem">
        <div><?php echo $language_based_content['none_of_your_videos_huddles']; ?></div>
    </li>
<?php endif; ?>
<script>
    $('[rel~="tooltip"]').tooltip({container: 'body'});
</script>


<script>

    function update_title_grid($document_id) {
        var $value = '#input-field-' + $document_id;
        var $video_title = "#videos-title-" + $document_id;
        var $title_block = "#input-title-" + $document_id;
        var $newText = $($value).val();
        if ($newText == '') {
            $($value).css('border', '1px red solid');
            return false;
        }
        var $nexText = $.trim($newText);
        $.ajax({
            url: home_url + "/MyFiles/editTitle",
            data: {title: $nexText, document_id: $document_id},
            type: 'POST',
            success: function (response) {
                var response_edit = response;
                if (response_edit.length > 35)
                {
                    response_edit = response_edit.substr(0, 35) + '...';
                }

                $($video_title).attr('data-title', response);
                $($video_title).html('<div class="videos-list__item-title"><a class="wrap wrap2">' + response_edit + '</a></div>');
                $($value).val("");
                $($title_block).css('display', 'none');
                $($video_title).css('display', 'block');
            },
            error: function () {
                alert("<?php echo $alert_messages['network_error_occured']; ?>");
            }
        });
    }

    function cancel($document_id) {
        var $video_title = "#videos-title-" + $document_id;
        var $title_block = "#input-title-" + $document_id;
        $($title_block).css('display', 'none');
        $($video_title).css('display', 'block');
    }

    $('.btn-trim-video').click(function () {
<?php if ($huddle_type == 3): ?>
            var that = this;
            $.ajax({
                url: '<?php echo $this->base . '/Huddles/check_videos_count' ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    huddle_id: '<?php echo $huddle_id ?>',
                    user_id: '<?php echo $user_id; ?>'
                },
                success: function (response) {
                    if (response.can_upload == 'yes') {
                        $(that).next().submit();
                    } else {
                        alert(response.message);
                    }
                }
            });
<?php else: ?>
            $(this).next().submit();
<?php endif; ?>


    });


</script>

<style>
    .wrap2{
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }

</style>