<?php
$amazon_base_url = Configure::read('amazon_base_url');
$user_permissions = $this->Session->read('user_permissions');
$language_based_content = $this->Custom->get_page_lang_based_content('VideoLibrary');
 $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
if (is_array($videos) && count($videos) > 0) {

    foreach ($videos as $vc) {
        ?>


        <?php
        $transcoding_status = $this->Custom->transcoding_status($vc['document_id']);
        $video_library_id = $vc['id'];
        $videoFilePath = pathinfo($vc['video_url']);
        $videoFileName = $videoFilePath['filename'];

        $commentDate = $vc['created_date'];
        $commentsDate = $this->Custom->formateDate($commentDate);

        if (!isset($vc['url']) || empty($vc['url']))
            $vc['url'] = $vc['video_url'];

        $vc['id'] = $vc['document_id'];

        $document_files_array = $this->Custom->get_document_url($vc);

        if (empty($document_files_array['url'])) {
            $vc['published'] = 0;
            $document_files_array['url'] = $vc['original_file_name'];
            $vc['encoder_status'] = $document_files_array['encoder_status'];
        } else {
            $vc['encoder_status'] = $document_files_array['encoder_status'];
            @$vc['duration'] = $document_files_array['duration'];
        }
        ?>
        <li class="videos-library__list-item" data-itemid="<?php echo $video_library_id; ?>">
            <div class="video-library-header"><h4 class="videos-library__list-title"><?php echo (strlen($vc['video_title']) > '40') ? '<a rel="tooltip"  data-original-title="' . $vc['video_title'] . '"  >' . $vc['video_title'] . "..." . "</a>" : $vc['video_title']; ?></h4></div>
            <div class="videos-library__list-thumb">
                <a class="videos-library__list-item-play" href="<?php echo $this->base . "/VideoLibrary/view/" . $video_library_id . "/" . $user_id; ?>"></a>
                <span class="videos-library__list-item-time" style="visibility:hidden;">33:10</span>
        <?php
        if ($vc['published'] == '1' && $transcoding_status != 5) :

            $seconds = $vc['duration'] % 60;
            $minutes = ($vc['duration'] / 60) % 60;
            $thumbnail_image_path = $document_files_array['thumbnail'];
            ?>
                    <img src="<?php echo $thumbnail_image_path; ?>" width="225" height="148" alt=""/>
                    <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;"><?php printf("%02d:%02d", $minutes, $seconds); ?></div>
        <?php else: ?>
                    <?php if ($vc['encoder_status'] == 'Error' || $transcoding_status == 5): ?>
                        <div class="video-unpublished-videolib"><span>Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.</span></div>
                    <?php else : ?>
                        <div class="video-unpublished-videolib"><span style="margin-top: 35px;"><img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br><?=$alert_messages["Your_video_is_currently_processing"]; ?></span></div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <!--<div class="videos-library__list-footer">
                <span class="videos-library__list-views"><?php echo $vc['view_count'] ?> views</span>
                <span class="videos-library__list-date"><?php echo $commentsDate; ?></span>
            </div>-->
            <div class="videos-library__list-footer">
                <span class="videos-library__list-views"><?php echo $vc['view_count'] ?> <?php echo $language_based_content['views_vl']; ?></span>
        <?php $extraPermission = (int) $user_permissions['UserAccount']['permission_video_library_upload']; ?>
                <?php if ($vc['published'] == '1' && (
                        ($user_permissions['roles']['role_id'] == '100' || $user_permissions['roles']['role_id'] == '110' || $user_permissions['roles']['role_id'] == '115' || $user_permissions['roles']['role_id'] == '120') && ($extraPermission))):
                    ?>
                    <?php
                    $filePath = $document_files_array['url'];
                    ?>
                    <?php $downloadUrl = $filePath; ?>
                    <!--<a href="<?php echo $downloadUrl; ?>" target="_blank" download="<?php echo $videoFileName . "_enc.mp4"; ?>">-->
                    <a href="<?php echo $this->webroot . 'Huddles/download/' . $vc['document_id'] ?>">
                        <img alt="Download" class="right videos-library__list-download" height="14" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download.png'); ?>" title="<?php echo $language_based_content['download_video_vl']; ?>" width="14" />
                    </a>

        <?php elseif ($vc['published'] == '1' && ($user_permissions['roles']['role_id'] == '120' && ($extraPermission))): ?>
            <?php
            $filePath = $document_files_array['url'];
            ?>
                    <?php $downloadUrl = $filePath; ?>
                            <!--<a href="<?php echo $downloadUrl; ?>" target="_blank" download="<?php echo $videoFileName . "_enc.mp4"; ?>">-->
                    <a href="<?php echo $this->webroot . 'Huddles/download/' . $vc['document_id'] ?>">
                        <img alt="Download" class="right videos-library__list-download" height="14" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download.png'); ?>" title="<?php echo $language_based_content['download_video_vl']; ?>" width="14" />
                    </a>
                <?php endif; ?>

                <span class="videos-library__list-date"><?php echo $commentsDate; ?></span>
            </div>
        </li>


        <?php
    }
}else {
    ?>

    <?php
}
?>
