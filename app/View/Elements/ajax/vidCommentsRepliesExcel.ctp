<ul class="comment-replies" id="nested_comment_<?php echo $vidComments['Comment']['id'] ?>" style="<?php echo ($counter == 2 ? "padding-left:0px;" : "") ?>">

    <?php
    $replies = $Comment->commentReplys($vidComments['Comment']['id']);

    if (isset($replies) && count($replies) > 0):
        ?>
        <?php
        $count = 0;
        foreach ($replies as $reply):
            ?>
            <?php
            $commentDate = $reply['Comment']['created_date'];
            $replyDate   = $this->Custom->formateDate($commentDate);
            ?>
            <li class="comment" style="margin-left: 40px;">
                <?php echo ($reply['Comment']['time'] == 0 ? "" : formatSeconds($reply['Comment']['time']) ); ?>
                <strong><?php echo $reply['User']['first_name'] . " " . $reply['User']['last_name']; ?></strong> <i>replied to: </i><span class="reply-to-icon">&nbsp;</span> <span style="color: #9c9c9c; font-size: 11px;"><?php echo $vidComments['User']['first_name'] . " " . $vidComments['User']['last_name']; ?></span>&nbsp;<i>Posted on: &nbsp;<?php echo date('m/d/Y',  strtotime($commentDate)); ?></i><br/>
                <p>
                    <?php echo wordwrap($reply['Comment']['comment'], 80, "\n", true); ?>
                </p>
                <?php
                //level1 
                echo $this->element('ajax/vidCommentsRepliesPdf', array(
                    "vidComments" => $reply,
                    "huddle_permission" => $huddle_permission,
                    "user_current_account" => $user_current_account,
                    "huddle" => $huddle,
                    "user_permissions" => $user_permissions,
                    "counter" => ($counter < 2 ? ($counter + 1) : $counter ),
                    "ref_id" => $vidComments['Comment']['id']
                ));
                ?> 
            </li>
            <?php
            $count++;
        endforeach;
        ?>
    <?php endif; ?>
</ul>