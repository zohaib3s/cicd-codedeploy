<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$loggedInUserRole = '';

$users_shown = array();
$groups_shown = array();
$is_group = '';
?>

<div class="lmargin-bottom">
    <div id="search-huddle-count" style="display: none;"> <?php echo '( ' . $total_huddles . ' )' ?></div>
    <?php if (isset($video_huddles) && !empty($video_huddles)): ?>


        <?php if (isset($view_mode) && $view_mode == 'grid'): ?>

            <div class="row huddles-list huddles-list--grid">

                <?php foreach ($video_huddles as $row): ?>

                    <div class="box style2 span4 compact">
                        <input type="checkbox" id="huddleselectedtomove" name="huddleselectedtomove" value="<?php echo $row['AccountFolder']['account_folder_id']; ?>"  class="huddle-to-move" />
                        <div class="box-wrapper">
                            <h4 class="nomargin-vertical wrap">
                                <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                    <a title="<?php echo $row['AccountFolder']['name']; ?>" class="invitation-title" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                                        <?php echo $row['AccountFolder']['name']; ?>
                                    </a>
                                <?php else: ?>
                                    <a title="<?php echo $row['AccountFolder']['name']; ?>" class="invitation-title" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                                        <?php echo $row['AccountFolder']['name']; ?>
                                    </a>
                                <?php endif; ?>
                            </h4>

                            <div style="overflow:hidden;">
                                <p class="smargin-vertical" style="clear:both;padding-top:5px;">
                                    <?php
                                    $number_char = 100;
                                    $desc = trim($row['AccountFolder']['desc']);
                                    if ($number_char < strlen($desc)) {
                                        $pos = strpos($desc, ' ', $number_char);
                                        if ($pos === false)
                                            $pos = $number_char;
                                        $desc = mb_substr($row['AccountFolder']['desc'], 0, $pos) . '...';
                                    }
                                    ?>
                                    <i><?php
                                        echo date('M d, Y', strtotime($row['AccountFolder']['created_date']));
                                        if (!empty($desc)) {
                                            ?> - <?php echo $desc; ?>
                                        <?php } ?>
                                    </i>
                                </p>
                            </div>

                            <?php
                            $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                            $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                            $userGroups = $AccountFolderGroup->getHuddleGroups($row['AccountFolder']['account_folder_id']);

                            if (isset($huddleGroups[0]['Group']['name']) && $huddleGroups[0]['Group']['name'] != '') {
                                $is_group = TRUE;
                            } else {
                                $is_group = FALSE;
                            }

                            $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $userGroups, $user_current_account['User']['id']);
                            ?>
                            <hr class="full dashed nomargin-vertical">
                            <?php if ($row['AccountFolder']['meta_data_value'] == '2') { ?>
                                <h5 class="smargin-vertical">Coach</h5>
                            <?php } else { ?>
                                <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                    <h5 class="smargin-vertical">Participants in the Folder</h5>
                                <?php else: ?>
                                    <h5 class="smargin-vertical">Participants in the Huddle</h5>
                                <?php endif; ?>
                            <?php } ?>

                            <?php if ($huddleUsers): ?>
                                <div class="widget-scrollable nopadding-left nopadding-top smargin-vertical">
                                    <div class="scrollbar" style="height: 44px;">
                                        <div class="track" style="height: 44px;">
                                            <div class="thumb" style="top: 0px; height: 15.3651px;">
                                                <div class="end"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="viewport vshort2" <?php echo $is_group == TRUE ? '' : 'style="height: 140px"' ?>>
                                        <div class="overview" style="top: 0px;">
                                            <?php
                                            $users_shown = array();
                                            $coach_html = '';
                                            $coachee_html = '';
                                            ?>
                                            <?php if ($row['AccountFolder']['meta_data_value'] == '2') {
                                                ?>
                                                <?php foreach ($huddleUsers as $invitedUsers): ?>
                                                    <?php if ($invitedUsers['huddle_users']["is_coach"] == '1') { ?>
                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>

                                                            <?php
                                                            $chimg = $this->Custom->getSecureAmazonSibmeUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image']);
                                                            $coach_html .= $this->Html->image($chimg, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left'));
                                                            ?>
                                                        <?php
                                                        else:
                                                            $chimg = $this->Custom->getSecureAmazonSibmeUrl('app/img/home/photo-default.png');
                                                            $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                            ?>

                                                        <?php endif; ?>
                                                    <?php }elseif ($invitedUsers['huddle_users']["is_mentee"] == '1') { ?>
                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                            <?php
                                                            $chimg = $this->Custom->getSecureAmazonSibmeUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image']);
                                                            $coachee_html .= $this->Html->image($chimg, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left'));
                                                            ?>
                                                        <?php
                                                        else:
                                                            $chimg = $this->Custom->getSecureAmazonSibmeUrl('app/img/home/photo-default.png');
                                                            $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                            ?>
                                                        <?php endif; ?>
                                                    <?php }else { ?>
                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                        <?php if ($invitedUsers['huddle_users']["role_id"] == '200') { ?>
                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                <?php
                                                                $chimg = $this->Custom->getSecureAmazonSibmeUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image']);
                                                                $coach_html .= $this->Html->image($chimg, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left'));
                                                                ?>
                                                            <?php
                                                            else:
                                                                $chimg = $this->Custom->getSecureAmazonSibmeUrl('app/img/home/photo-default.png');
                                                                $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                ?>
                                                            <?php endif; ?>
                                                        <?php }else { ?>
                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                <?php
                                                                $chimg = $this->Custom->getSecureAmazonSibmeUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image']);
                                                                $coachee_html .= $this->Html->image($chimg, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left'));
                                                                ?>
                                                            <?php
                                                            else:
                                                                $chimg = $this->Custom->getSecureAmazonSibmeUrl('app/img/home/photo-default.png');
                                                                $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                ?>
                                                            <?php endif; ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php
                                                    $users_shown[] = $invitedUsers['User']["id"];
                                                endforeach;
                                            }else {
                                                foreach ($huddleUsers as $invitedUsers):
                                                    ?>
                                                    <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                    <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                        <?php
                                                        $chimg = $this->Custom->getSecureAmazonSibmeUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image']);
                                                        $coach_html .= $this->Html->image($chimg, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left'));
                                                        ?>
                                                    <?php
                                                    else:
                                                        $chimg = $this->Custom->getSecureAmazonSibmeUrl('app/img/home/photo-default.png');
                                                        $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                        ?>
                                                    <?php endif; ?>
                        <?php
                        $users_shown[] = $invitedUsers['User']["id"];
                    endforeach;
                }
                echo $coach_html;
                if ($coachee_html != '') {
                    echo '<div class="clear"></div>';
                    echo '<h5 class="smargin-vertical" style="clear:both;padding-top:10px;" >Coachee</h5>';
                    echo $coachee_html;
                }
                $users_shown = array();
                ?>
                                        </div>
                                    </div>
                                </div>
            <?php else: ?>
                                <div style="height: 60px;"> No People in the Huddle.</div>
            <?php endif; ?>

            <?php if (($is_group && $row['AccountFolder']['meta_data_value'] != '3') || ($is_group && $row['AccountFolder']['meta_data_value'] == '3' && $this->Custom->check_if_evalutor($row['AccountFolder']['account_folder_id'], $user_id) )): ?>
                                <hr class="full dashed nomargin-vertical">
                                <h5 class="nopadding-left smargin-vertical">Groups in huddle</h5>
                                <div class="widget-scrollable nopadding-left nopadding-vertical">
                                    <div class="scrollbar disable" style="height: 50px;">
                                        <div class="track" style="height: 50px;">
                                            <div class="thumb" style="top: 0px; height: 50px;">
                                                <div class="end"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="viewport vshort3">
                                        <div class="overview">
                                            <?php
                                            $counter = 0;
                                            $groups_shown = array();
                                            foreach ($huddleGroups as $grp):
                                                ?>
                                                <?php if (isset($grp['huddle_groups']['group_id']) && in_array($grp['huddle_groups']['group_id'], $groups_shown)) continue; ?>
                                                <?php
                                                if ($counter > 0)
                                                    echo ", ";
                                                echo $grp['Group']['name'];
                                                ?>
                                    <?php
                                    $counter +=1;

                                    $groups_shown[] = $grp['huddle_groups']['group_id'];

                                endforeach;

                                $groups_shown = array();
                                ?>
                                        </div>
                                    </div>
                                </div>
                                   <?php endif; ?>

                        </div>
                        <hr class="full smargin-vertical">
                        <div class="text-center">
                            <div class="action-buttons btn-group">
                                <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                    <a class="btn" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">View</a>
                <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                        <a rel="tooltip" class="btn icon2-cog" href="<?php echo $this->base . '/Folder/edit/' . $row['AccountFolder']['account_folder_id'] ?>"
                                           data-original-title="Edit"></a>
                                       <?php endif; ?>
                <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                        <a rel="tooltip" data-method="delete" data-confirm="Are you sure you want to permanently delete this Folder?"
                                           class="btn icon2-trash" href="<?php echo $this->base . '/Folder/delete/' . $row['AccountFolder']['account_folder_id'] ?>"
                                           data-original-title="Delete"></a>
                                       <?php endif; ?>
            <?php else: ?>
                                    <a class="btn" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">View</a>
                <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                        <a rel="tooltip" class="btn icon2-cog" href="<?php echo $this->base . '/add_huddle_angular/edit/' . $row['AccountFolder']['account_folder_id'] ?>"
                                           data-original-title="Edit"></a>
                        <?php endif; ?>
                <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                        <a rel="tooltip" data-method="delete" data-confirm="Are you sure you want to permanently delete this huddle?"
                                           class="btn icon2-trash" href="<?php echo $this->base . '/Huddles/delete/' . $row['AccountFolder']['account_folder_id'] ?>"
                                           data-original-title="Delete"></a>
                <?php endif; ?>
            <?php endif; ?>

                            </div>
                        </div>

                    </div>
        <?php endforeach; ?>

            </div>

    <?php elseif (isset($view_mode) && $view_mode == 'list'): ?>
            <style>
                .box.style2{
                    margin:0px;
                    border: 1px solid #dddad2;
                    margin-bottom:-1px;
                    border-radius:0px;
                    padding:8px;
                    min-height:70px;
                }
                .huddle_list_left{
                    float:left;
                    width:710px;
                }
                .huddle_list_right{
                    float:right;
                }
            </style>
            <!----List View---->
            <div class="row huddles-list huddles-list--list" style="margin-top: -14px;">
                <ol class="huddle-list-view" style="padding: 0 0 0 20px;">
                    <?php
                    $grups = array();
                    foreach ($video_huddles as $row):
                        $first_letter = $this->Custom->py_slice(trim($row['AccountFolder']['name']), '0');
                        $grups[$row['AccountFolder']['account_folder_id']] = strtoupper($first_letter);
                        $grups = array_unique($grups);
                        ?>
                                <?php endforeach; ?>

        <?php foreach ($video_huddles as $row): ?>
            <?php
            $invitedUser = '';
            $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
            $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
            $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
            ?>
                        <li class="box style2" data-group="<?php echo ($sort == 'name') ? isset($grups[$row['AccountFolder']['account_folder_id']]) ? $grups[$row['AccountFolder']['account_folder_id']] : '' : ''; ?>">
                            <input type="checkbox" id="huddleselectedtomove" name="huddleselectedtomove" value="<?php echo $row['AccountFolder']['account_folder_id']; ?>"  class="huddle-to-move" />
                            <h4>
                                <a title="<?php echo $row['AccountFolder']['name']; ?>" class="invitation-title" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
            <?php echo $row['AccountFolder']['name']; ?>
                                </a>
                            </h4>
                            <div class="huddle_list_left">
                                <p><?php echo (strlen($row['AccountFolder']['desc']) > 130 ? mb_substr($row['AccountFolder']['desc'], 0, 130) . "..." : $row['AccountFolder']['desc']) ?></p>
                            </div>
                            <div class="huddle_list_right">
                                <div class="right" style="width: 256px">
                                    <div class="left">
                                        <span class="col icon5-admin"> (<?php echo count($huddleUsers); ?>)</span>
                                        <span class="col icon-calendar" style="width: 105px;">&nbsp;<?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])) ?></span>
                                    </div>
                                    <div class="right">
                                        <div class="action-buttons btn-group" style="margin-top: -5px;">
            <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                <a rel="tooltip" class="btn icon2-cog" href="<?php echo $this->base . '/add_huddle_angular/edit/' . $row['AccountFolder']['account_folder_id'] ?>" data-original-title="Edit"></a>
                <?php endif; ?>
                <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                <a title="Remove" rel="tooltip nofollow" data-method="delete" data-confirm="Are you sure you want to permanently delete this huddle?" class="btn icon2-trash" href="<?php echo $this->base . '/Huddles/delete/' . $row['AccountFolder']['account_folder_id'] ?>"></a>
                    <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </l i>
                <?php endforeach; ?>
                </ol>
            </div>
    <?php endif; ?>
<?php else: ?>
        <div class="text-center">
    <?php $chimg = $this->Custom->getSecureAmazonSibmeUrl('app/img/new/video-brown.png'); ?>
            <p><?php echo $this->Html->image($chimg, array('alt' => 'Video-brown')); ?></p>
            <h2 class="heading--info">None of your Huddles or Folders matched this search.  Try another search.</h2>
            <p style="display:none;">To view sample Coaching Huddle, <a href="/video_huddles?sample=yes" class="blue-link">click here</a>!</p>
        </div>
        <div class="<?php if ($user_current_account['roles']['role_id'] != '120' || $user_permissions['UserAccount']['permission_maintain_folders'] == '1'): ?>btn-annotation<?php else: ?>btn-annotation2<?php endif; ?>">
    <?php if ($user_current_account['roles']['role_id'] != '120' || $user_permissions['UserAccount']['permission_maintain_folders'] == '1'): ?>
                Click here to create your first Huddle!
    <?php endif; ?>
            A Huddle is a great way to work with others to improve teaching and learning. Share videos and related resources and participate in discussions with other Huddle participants.
        </div>

        <a href="#" id="view_browser_notification" style="display: none;" data-toggle="modal" data-target="BrowserMessageModal">browser message</a>
        <div id="BrowserMessageModal" class="modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="header">
                        <h4 class="header-title nomargin-vertical smargin-bottom">Upgade Your Browser</h4>
                        <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                    </div>

                    <p>You're Using an unsupported browser. Please upgrade your browser to view <?php $this->Custom->get_site_settings('site_title') ?> Application</p>
                </div>
            </div>
        </div>
<?php endif; ?>
</div>
<script type="text/javascript">
    $(".widget-scrollable").tinyscrollbar();
    $("[rel~=tooltip]").tooltip({container: 'body'});
</script>
