<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
$account_folder_id = $huddle[0]['AccountFolder']['account_folder_id'];

function formatSeconds($seconds) {
    $hours = 0;
    $milliseconds = str_replace("0.", '', $seconds - floor($seconds));

    if ($seconds > 3600) {
        $hours = floor($seconds / 3600);
    }
    $seconds = $seconds % 3600;


    return str_pad($hours, 2, '0', STR_PAD_LEFT)
            . gmdate(':i:s', $seconds)
            . ($milliseconds ? ".$milliseconds" : '');
}

if (!function_exists('_make_url_clickable_cb')) {

    function _make_url_clickable_cb($matches) {
        $ret = '';
        $url = $matches[2];

        if (empty($url))
            return $matches[0];
// removed trailing [.,;:] from URL
        if (in_array(substr($url, -1), array('.', ',', ';', ':')) === true) {
            $ret = substr($url, -1);
            $url = substr($url, 0, strlen($url) - 1);
        }
        return $matches[1] . "<a href=\"$url\" target=\"_blank\" rel=\"nofollow\">$url</a>" . $ret;
    }

}

if (!function_exists('_make_web_ftp_clickable_cb')) {

    function _make_web_ftp_clickable_cb($matches) {
        $ret = '';
        $dest = $matches[2];
        $dest = 'http://' . $dest;

        if (empty($dest))
            return $matches[0];
// removed trailing [,;:] from URL
        if (in_array(substr($dest, -1), array('.', ',', ';', ':')) === true) {
            $ret = substr($dest, -1);
            $dest = substr($dest, 0, strlen($dest) - 1);
        }
        return $matches[1] . "<a href=\"$dest\" target=\"_blank\" rel=\"nofollow\">$dest</a>" . $ret;
    }

}

if (!function_exists('_make_email_clickable_cb')) {

    function _make_email_clickable_cb($matches) {
        $email = $matches[2] . '@' . $matches[3];
        return $matches[1] . "<a href=\"mailto:$email\" target=\"_blank\">$email</a>";
    }

}

if (!function_exists('make_clickable')) {

    function make_clickable($ret) {

        $ret = str_replace("&nbsp;", " ", $ret);
        $ret = ' ' . $ret;
// in testing, using arrays here was found to be faster
        $ret = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_url_clickable_cb', $ret);
        $ret = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_web_ftp_clickable_cb', $ret);
        $ret = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', '_make_email_clickable_cb', $ret);

// this one is not in an array because we need it to run last, for cleanup of accidental links within links
        $ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
        $ret = trim($ret);
        return $ret;
    }

}

function defaulttagsclasses($posid) {
    $class_name = 'tags_qucls';
    if ($posid == 1) {
        $class_name = 'tags_qucls';
    } else if ($posid == 2) {
        $class_name = 'tags_sugcls';
    } else if ($posid == 3) {
        $class_name = 'tags_notescls';
    } else if ($posid == 4) {
        $class_name = 'tags_strangthcls';
    } else if ($posid == 5) {
        $class_name = 'tags_strangthcls';
    }
    return $class_name;
}

function defaulttagsclasses2($posid) {
    $class_name = 'tags_qucls';
    if ($posid == 1) {
        $class_name = 'tags_quclsbg';
    } else if ($posid == 2) {
        $class_name = 'tags_sugclsbg';
    } else if ($posid == 3) {
        $class_name = 'tags_notesclsbg';
    } else if ($posid == 4) {
        $class_name = 'tags_strangthclsbg';
    } else if ($posid == 5) {
        $class_name = 'tags_strangthclsbg';
    }
    return $class_name;
}

function _gettagclass($comment_tags, $default_tags) {
    $return = -1;
    foreach ($comment_tags as $comment_tag) {
        foreach ($default_tags as $key => $default_tag_value) {
            if (!empty($comment_tag['account_tags']['account_tag_id'])) {
                if ($default_tag_value['AccountTag']['account_tag_id'] == $comment_tag['AccountCommentTag']['account_tag_id']) {
                    $return = $key;
                    break;
                }
            }
        }
    }
    $return = $return + 1;
    return $return;
}
?>



<?php if ($vidComments): ?>
    <?php foreach ($vidComments as $vidComments): ?>
        <?php
        $video_comment_counter = 0;
        $commentDate = $vidComments['Comment']['created_date'];
        $commentsDate = $this->Custom->formateDate($commentDate);
        $syn_class = _gettagclass($vidComments['default_tags'], $defaulttags);
        if ($syn_class == 0 || $vidComments['Comment']['time'] == 0) {
            $syn_class = 'synchro';
        } else {
            $syn_class = 'synchro_' . $syn_class;
        }
        ?>

        <div rel="<?php echo $vidComments['Comment']['id'] ?>" id="comment_box_<?php echo $vidComments['Comment']['id'] ?>" style="<?php echo ($video_comment_counter == 0) ? " margin-top: 12px;" : "" ?>">
            <li class="<?php echo $syn_class; ?>">
                <div class="synchro-inner">
                    <i></i><span data-time="<?php echo $vidComments['Comment']['time']; ?>" class="synchro-time"><?php echo ($vidComments['Comment']['time'] == 0 ? "All" : formatSeconds($vidComments['Comment']['time']) ); ?></span>
                </div>
            </li>

            <li class="comment thread">
                <a href="#<?php //echo $this->base . '/users/editUser/' . $vidComments['Comment']['user_id'];     ?>">
                    <?php if (isset($vidComments['User']['image']) && $vidComments['User']['image'] != ''): ?>
                        <?php
                        $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $vidComments['User']['user_id'] . "/" . $vidComments['User']['image']);
                        echo $this->Html->image($chimg, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '21', 'width' => '21', 'align' => 'left'));
                        ?>
                    <?php else: ?>
                        <img width="21" height="21"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo photo inline" rel="image-uploader" alt="Photo-default">
                    <?php endif; ?>
                </a>
                <div class="comment-header"><?php echo $vidComments['User']['first_name'] . " " . $vidComments['User']['last_name']; ?></div>
                <div class="comment-body comment more lllq">
                    <?php echo nl2br($vidComments['Comment']['comment']); ?>
                </div>

                <div class="comment-footer">
                    <span class="comment-date">
                        <?php echo $commentsDate; ?>
                    </span>

                    <div class="comment-actions" style="width: 150px;">
                        <form id="delete-main-comments-frm<?php echo $vidComments['Comment']['id']; ?>" action="<?php echo $this->base . '/Huddles/deleteVideoComments/' . $vidComments['Comment']['id']; ?>" accept-charset="UTF-8">
                            <?php if (($huddle_permission == '200' || $huddle_permission == '210') || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                <?php if ($user_current_account['User']['id'] != $vidComments['Comment']['created_by']): ?>
                                    <a class="comment-reply" style="cursor: pointer;" id="reply_button_<?php echo $vidComments['Comment']['id'] ?>">Reply</a>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                <a   data-toggle="modal" data-target="#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>"  class="comment-edit"  style="cursor: pointer;">Edit</a>
                                <a rel="nofollow" data-confirm="Are you sure you want to delete this comment?" id="delete-main-comments" class="comment-delete" href="javascript:deleteObComment('<?php echo $vidComments['Comment']['id']; ?>', '<?php echo $internal_comment_id; ?>', '<?php echo $huddle_id; ?>', '<?php echo $video_id; ?>', '<?php echo $vidComments['Comment']['id']; ?>');">Delete</a>
                            <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $vidComments['Comment']['created_by'])): ?>
                                <a   data-toggle="modal" data-target="#edit_comment_form__<?php echo $vidComments['Comment']['id'] ?>"  class="comment-edit"  style="cursor: pointer;">Edit</a>
                                <a rel="nofollow" data-confirm="Are you sure you want to delete this comment?" id="delete-main-comments" class="comment-delete" href="javascript:deleteObComment('<?php echo $vidComments['Comment']['id']; ?>', '<?php echo $internal_comment_id; ?>', '<?php echo $huddle_id; ?>', '<?php echo $video_id; ?>', '<?php echo $vidComments['Comment']['id']; ?>');">Delete</a>
                            <?php endif; ?>
                        </form>
                    </div>
                </div>

                <div id="docs-standards" style="margin-bottom: 20px;">
                    <?php
                    if (isset($vidComments['default_tags']) && count($vidComments['default_tags']) > 0) {

                        foreach ($vidComments['default_tags'] as $default_tag) {
                            ?>
                            <div id="tagFilterList">
                                <div id="tagFilterContainer" class="btnwraper" style="margin-right:5px;">
                                    <button class="btn btn-lilghtgray"># <?php echo empty($default_tag['account_tags']['tag_title']) ? $default_tag['AccountCommentTag']['tag_title'] : $default_tag['account_tags']['tag_title']; ?></button>

                                    <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                        <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag('<?php echo $default_tag['AccountCommentTag']['account_comment_tag_id']; ?>', '<?php echo $internal_comment_id; ?>', '<?php echo $huddle_id; ?>', '<?php echo $video_id; ?>', '<?php echo $vidComments['Comment']['id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                    <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $vidComments['Comment']['created_by'])): ?>
                                        <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag('<?php echo $default_tag['AccountCommentTag']['account_comment_tag_id']; ?>', '<?php echo $internal_comment_id; ?>', '<?php echo $huddle_id; ?>', '<?php echo $video_id; ?>', '<?php echo $vidComments['Comment']['id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                    <?php endif; ?>

                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <?php
                    $selected_standard = array();
                    if (isset($vidComments['standard']) && count($vidComments['standard']) > 0) {

                        foreach ($vidComments['standard'] as $standard) {
                            $selected_standard[] = $standard['account_tags']['account_tag_id'];
                            ?>
                            <div id="tagFilterList">
                                <div id="tagFilterContainer_<?php echo $standard['account_tags']['account_tag_id']; ?>" class="tagFilterContainer_ btnwraper" style="margin-right:5px;">
                                    <button class="btn btn-lilghtgray"># <?php echo $standard['account_tags']['tag_code'] . '-' . $standard['account_tags']['tag_title']; ?></button>
                                    <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                        <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag('<?php echo $standard['AccountCommentTag']['account_comment_tag_id']; ?>', '<?php echo $internal_comment_id; ?>', '<?php echo $huddle_id; ?>', '<?php echo $video_id; ?>', '<?php echo $vidComments['Comment']['id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                    <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $vidComments['Comment']['created_by'])): ?>
                                        <a style="margin-right: -5px;" rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag('<?php echo $standard['AccountCommentTag']['account_comment_tag_id']; ?>', '<?php echo $internal_comment_id; ?>', '<?php echo $huddle_id; ?>', '<?php echo $video_id; ?>', '<?php echo $vidComments['Comment']['id']; ?>');"><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/crossb.png'); ?>" class="crossBtn"></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php
                            //'# ' + tag_code + '-' + tag_array[0] + '...'
                            $stand_title = explode(" ", $standard['AccountCommentTag']['tag_title']);
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $("#txtVideostandard1<?php echo $vidComments['Comment']['id']; ?>").addTag("# <?php echo $standard['account_tags']['tag_code'] . '-' . $stand_title[0] . '...'; ?>");
                                });
                            </script>
                            <?php
                        }
                    }
                    ?>
                    <div class="clear"></div>
                </div>
                <?php
                echo '<div style="clear:both;"></div>';
                ?>
                <div id="reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>" style="display: none;">
                    <script type="text/javascript">
                        $(document).ready(function () {
                            var isSubmitting = false;

                            $("#reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide();
                            $("#reply_button_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                $("#reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>").show("slow", function () {
                                    $('#comment_comment_<?php echo $vidComments['Comment']['id'] ?>').focus();
                                });
                            });

                            $("#close_reply_form_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                $("#reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide("slow");
                            });


                            $('#comments_form_<?php echo $vidComments['Comment']['id'] ?>').on('submit', function (event) {
                                event.preventDefault();
                                if (isSubmitting)
                                    return false;
                                isSubmitting = true;

                                var $form = $(this);
                                $.ajax({
                                    type: $form.attr('method'),
                                    url: $form.attr('action'),
                                    data: $form.serialize(),
                                    success: function () {
                                        getVideoComments();
                                        isSubmitting = false;
                                    }
                                });

                                return false;
                            });

                            $('#delete-main-comments-frm<?php echo $vidComments['Comment']['id']; ?>').on('submit', function (event) {

                                var $form = $(this);
                                $.ajax({
                                    type: $form.attr('method'),
                                    url: $form.attr('action'),
                                    data: $form.serialize(),
                                    success: function (data, status) {
                                        getVideoComments();
                                        ob_getVideoComments();
                                    }
                                });

                                event.preventDefault();
                                return false;

                            });

                        });
                    </script>

                    <form method="post" id="comments_form_<?php echo $vidComments['Comment']['id'] ?>"
                          class="new_comment"
                          action="<?php echo $this->base . '/Huddles/addReply/' . $vidComments['Comment']['id']; ?>"
                          accept-charset="UTF-8">
                        <div style="margin:0;padding:0;display:inline">
                            <input type="hidden" value="✓" name="utf8">
                        </div>
                        <input type="hidden" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" name="authenticity_token" id="tokentag">
                        <div class="input-group">
                            <textarea rows="3" placeholder="Add Reply..." name="comment[comment]" id="comment_comment_<?php echo $vidComments['Comment']['id'] ?>" ></textarea>
                        </div>
                        <input type="hidden" value="nested" name="comment[access_level]" id="comment_access_level">

                        <div class="input-group">
                            <input type="submit" class="btn btn-green" value="Add Reply" name="submit">
                            <a class="btn btn-transparent" id="close_reply_form_<?php echo $vidComments['Comment']['id'] ?>">Cancel</a>
                        </div>
                    </form>
                </div>
                <div id="edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>" class="modal" role="dialog" style="display: none;">
                    <div class="modal-dialog" style="width: 900px;">
                        <div class="modal-content">
                            <div class="header">
                                <h4 class="header-title nomargin-vertical smargin-bottom">Edit Comment</h4>
                                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var tagIDsd = $('[name=txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                                    var tagID = $('[name=txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                                    $('#' + tagIDsd + '_tag').css('width', '110px');
                                    var isSubmitting = false;
                                    $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide();

                                    $("#edit-main-comments_<?php echo $vidComments['Comment']['id'] ?>").click(function () {

                                        var $comments_text = $('#comments_text_<?php echo $vidComments['Comment']['id'] ?>').val();
                                        var comment_comment_edit = '<?php echo preg_replace("/\r|\n/", "", html_entity_decode((addslashes($vidComments['Comment']['comment'])))); ?>';
                                        $('#comment_comments_<?php echo $vidComments['Comment']['id'] ?>').val(comment_comment_edit);

                                        $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").show("slow", function () {
                                            $('#comment_comment_<?php echo $vidComments['Comment']['id'] ?>').focus();
                                        });

                                    });
                                    $("#close_edit_form_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                        $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide("slow");
                                    });
                                    $('#comment_form_<?php echo $vidComments['Comment']['id'] ?>').on('submit', function (event) {
                                        event.preventDefault();
                                        if (isSubmitting)
                                            return false;
                                        isSubmitting = true;

                                        var $form = $(this);

                                        $.ajax({
                                            type: $form.attr('method'),
                                            url: $form.attr('action'),
                                            data: $form.serialize(),
                                            success: function (data, status) {
        <?php if ($type != 'get_video_comments_obs'): ?>
                                                    getVideoComments();
        <?php else: ?>
                                                    ob_getVideoComments();
        <?php endif; ?>
                                                isSubmitting = false;
                                            }
                                        });

                                        return false;
                                    });
                                });
                            </script>
                            <div id="comment_add_form_html">
                                <?php $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']); ?>

                                <?php
                                if ($huddle_permission == '200' || $huddle_permission == '210' ||
                                        ($isCreator && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')
                                ):
                                    ?>

                                    <?php
                                    $comment_box_display = 'style="display:block;"';
                                    $comment_box_add_btn = 'style="display:none;"';
                                    ?>
                                    <?php
                                    $selected_tag_ids = array();

                                    if (isset($vidComments['default_tags']) && count($vidComments['default_tags']) > 0) {

                                        foreach ($vidComments['default_tags'] as $default_tag) {
                                            $selected_tag_ids[] = $default_tag['account_tags']['account_tag_id'];
                                            if ($default_tag['AccountCommentTag']['ref_type'] != 2) {
                                                ?>
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $("#txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>").addTag("# <?php echo trim(empty($default_tag['account_tags']['tag_title']) ? $default_tag['AccountCommentTag']['tag_title'] : $default_tag['account_tags']['tag_title']); ?>");
                                                    });
                                                </script>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <style type="text/css">
                                        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
                                            color:    #424242;
                                        }
                                        :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
                                            color:    #424242;
                                            opacity:  1;
                                        }
                                        ::-moz-placeholder { /* Mozilla Firefox 19+ */
                                            color:    #424242;
                                            opacity:  1;
                                        }
                                        :-ms-input-placeholder { /* Internet Explorer 10-11 */
                                            color:    #424242;
                                        }
                                        div.tagsinput{
                                            width:422px !important;
                                            position: relative;
                                            top: -5px;
                                            border-radius: 0px;
                                        }
                                        div.tagsinput input {
                                            width:105px !important;
                                        }
                                        #txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>_tag {
                                            width: 117px;
                                            background: url(/app/img/tag_standard_bg.png) no-repeat;
                                            padding-left: 20px;
                                            color: #b2b2b2 !important;
                                        }
                                        .edit_comment_left{
                                            float:left;
                                            width: 52%;
                                            box-sizing:border-box;
                                        }
                                        .edit_comment_right{
                                            float:right;
                                            width:48%;
                                            padding:0px 20px;
                                            box-sizing:border-box;

                                        }
                                        .scroll_box{
                                            overflow-y:scroll;
                                            max-height:420px;
                                            overflow-x:hidden;
                                            margin-bottom:12px;
                                        }
                                        .block_width_adjs{
                                            padding-left:25px;
                                        }
                                        .edit_comment_form{
                                            padding:0px 22px !important;
                                        }
                                        .edit_comment_right h1{
                                            font-size:25px;
                                            border-bottom: dotted 1px #ccc;
                                            font-weight:normal;
                                            margin-bottom:10px;
                                        }
                                        .edit_btn_adjs{
                                            top: 1px;
                                            position: relative;
                                            padding-bottom: 6px !important;
                                        }
                                        #txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>_tag{
                                            width:105px !important;
                                        }
                                    </style>

                                    <div class="edit_comment_left">
                                        <?php if ($this->Custom->is_enable_tags($account_id)): ?>
                                            <?php if ($this->Custom->is_enable_huddle_tags($account_folder_id)): ?>
                                                <div class="divblockwidth block_width_adjs">
                                                    <?php
                                                    if (count($tags) > 0) {
                                                        $count = 1;
                                                        foreach ($tags as $tag) {
                                                            ?>
                                                            <a class="default_tag1<?php echo $vidComments['Comment']['id'] ?> <?php echo (in_array($tag['AccountTag']['account_tag_id'], $selected_tag_ids)) ? defaulttagsclasses($count) . "bg" : defaulttagsclasses($count); ?>" href="javascript:#" position_id="<?php echo $count; ?>" status_flag="<?php echo (in_array($tag['AccountTag']['account_tag_id'], $selected_tag_ids)) ? '1' : '0'; ?>" alt="<?php echo $tag['AccountTag']['tag_title']; ?>"># <?php echo $tag['AccountTag']['tag_title']; ?></a>
                                                            <?php
                                                            $count++;
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <div class="clear"></div>

                                        <div id="comment_form_main" <?php echo $comment_box_display; ?>>
                                            <form class="edit_comment_form" method="post" id="comment_form_<?php echo $vidComments['Comment']['id'] ?>"
                                                  class="new_comment" accept-charset="UTF-8"
                                                  action="<?php echo $this->base . '/Huddles/editComments/' . $vidComments['Comment']['id']; ?>" >
                                                <div style="margin:0;padding:0;display:inline">
                                                    <input type="hidden" value="âœ“" name="utf8">
                                                </div>
                                                <input type="hidden" id="comments_text_<?php echo $vidComments['Comment']['id'] ?>" value="<?php echo strip_tags($vidComments['Comment']['comment']) ?>"/>
                                                <input type="hidden" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" name="authenticity_token" id="tokentag">
                                                <input type="hidden" value="<?php echo $vidComments['Comment']['id'] ?>" name="comment_id" />
                                                <input type="hidden" value="<?php echo $vidComments['Comment']['ref_id'] ?>" name="videoId" />
                                                <?php
                                                $input_group_style = "margin-top: 20px;";

                                                if ($this->Custom->is_enable_tags($account_id)):
                                                    if ($this->Custom->is_enable_huddle_tags($account_folder_id)):
                                                        $input_group_style = "margin-top: 20px;";
                                                    endif;
                                                endif;
                                                ?>




                                                <div class="input-group" style="<?php echo $input_group_style; ?>">
                                                    <textarea rows="3" placeholder="Edit comments" style="border-radius:0px;" name="comment[comment]" id="comment_comments_<?php echo $vidComments['Comment']['id'] ?>" ><?php echo $vidComments['Comment']['comment']; ?></textarea>
                                                    <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                                                        <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                                                            <div class="tags divblockwidth">
                                                                <div class="video-tags-row row">
                                                                    <input type="text" name="txtVideostandard1<?php echo $vidComments['Comment']['id']; ?>" data-default="Tag Standard..." id="txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>" value="" placeholder=""  required/>
                                                                </div>
                                                                <div class="clear" style="clear: both;"></div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>

                                                    <div class="tags1 divblockwidth">
                                                        <div class="video-tags-row row">
                                                            <input type="text" name="txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>" data-default="Tags..." id="txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>" value="" placeholder="" readonly="readonly"/>
                                                        </div>
                                                        <div class="clear" style="clear: both;"></div>
                                                    </div>


                                                    <div class="clear" style="clear: both;"></div>
                                                </div>
                                                <div class="clear" style="clear: both;"></div>
                                                <input type="hidden" value="nested" name="comment[access_level]" id="comment_access_level">
                                                <div class="input-group">
                                                    <input type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green edit_btn_adjs" id="btn_form_sub_<?php echo $vidComments['Comment']['id'] ?>" value="Edit Comment" name="submit">
                                                    <a class="btn btn-transparent" id="close_edit_form_<?php echo $vidComments['Comment']['id'] ?>" data-dismiss="modal">Cancel</a>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                    <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                                        <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                                            <div class="edit_comment_right">
                                                <h1>Framework</h1>
                                                <div class="search-box standard-search" style="position: relative;">
                                                    <input type="button" id="btnSearchTags" class="btn-search" value="">
                                                    <input class="text-input" id="txtSearchTags1" type="text" value="" placeholder="Search Standards..." style="margin-right: 0px;">
                                                    <span id="clearTagsButton1" class="clear-video-input-box" style="display:none;right: 33px;top: 20px;">X</span>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="scroll_box">
                                                    <div id="indicator" style="display:none;"><img alt="Indicator" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/indicator.gif'); ?>" /></div>
                                                    <div class="standards">
                                                        <ul id="expList" class="expList" style="padding-left:0px">
                                                            <?php
                                                            if (!empty($standardsL2)) {
                                                                for ($i = 0; $i < count($standardsL2); $i++) {
                                                                    ?>
                                                                    <li class="frame_work_heading" ><?php echo $standardsL2[$i]['AccountTag']['tag_code'] . ' - ' . $standardsL2[$i]['AccountTag']['tag_title']; ?></li>
                                                                    <?php
                                                                    foreach ($standards as $standard) {
                                                                        if ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['parent_account_tag_id']) {
                                                                            ?>
                                                                            <li class="standard_<?php echo $vidComments['Comment']['id']; ?> standard-cls" >
                                                                                <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" <?php echo in_array($standard['AccountTag']['account_tag_id'], $selected_standard) ? 'checked="checked"' : ''; ?>/>
                                                                                <?php
                                                                                $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                                                echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                                                ?>
                                                                            </li>

                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                foreach ($standards as $standard) {
                                                                    ?>
                                                                    <li class="standard_<?php echo $vidComments['Comment']['id']; ?> standard-cls" >
                                                                        <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" <?php echo in_array($standard['AccountTag']['account_tag_id'], $selected_standard) ? 'checked="checked"' : ''; ?>/>
                                                                        <?php
                                                                        $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                                        echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                                        ?>
                                                                    </li>

                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                            <li id="noresults">No standards match your search criteria.</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php
                                endif;
                                ?>
                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>
                </div>

                <?php
                //level1
                echo $this->element('ajax/vidCommentsReplies', array(
                    "vidComments" => $vidComments,
                    "huddle_permission" => $huddle_permission,
                    "user_current_account" => $user_current_account,
                    "huddle" => $huddle,
                    "user_permissions" => $user_permissions,
                    "counter" => 0
                ));
                ?>

            </li>


        </div>
        <script type="text/javascript">
            $(document).ready(function (e) {
                isSubmitting = false;
                $(document).on("click", "#btn_form_sub_<?php echo $vidComments['Comment']['id'] ?>", function (event) {
                    event.preventDefault();
                    if (isSubmitting)
                        return false;
                    isSubmitting = true;
                    $.ajax({
                        type: 'POST',
                        url: home_url + '/Huddles/editComments/<?php echo $vidComments['Comment']['id']; ?>',
                        data: $("#comment_form_<?php echo $vidComments['Comment']['id'] ?>").serialize(),
                        success: function (response) {
                            $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").modal('hide');
                            getVideoComments();
                            isSubmitting = false;
                        }
                    });
                    return false;
                });
                isSubmitting = false;
                var tagIDsd = $('[name=txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                $(document).on("click", "#close_edit_form_<?php echo $vidComments['Comment']['id'] ?>", function () {
                    if (isSubmitting)
                        return false;
                    isSubmitting = true;
                    getVideoComments();
                });
                isSubmitting = false;
                $(document).on('change', '.standard_<?php echo $vidComments['Comment']['id']; ?> input', function () {
                    if (isSubmitting)
                        return false;
                    isSubmitting = true;
                    var tagIDsd = $('[name=txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                    var tag_code = $(this).attr('st_code');
                    var tag_value = '';
                    var tag_array = {};
                    var tag_name = $(this).attr('st_name');
                    //console.log($(this).attr('st_name'));
                    tag_array = tag_name.split(" ");
                    if ($(this).is(':checked')) {
                        tag_array = tag_name.split(" ");
                        tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                        if ($('#' + tagIDsd).tagExist(tag_value)) {
                            $('#' + tagIDsd).removeTag(tag_value);
                        }
                        $('#' + tagIDsd).addTag(tag_value);
                        //$('#txtVideostandard_tag').remove();
                    } else {
                        tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                        $('#' + tagIDsd).removeTag(tag_value);
                    }
                    if ($('input[name="name1"]:checked').length > 0) {
                        //$('#txtVideostandard_tag').hide();
                    }
                    isSubmitting = false;
                    //return false;
                });

                $('#' + tagIDsd).tagsInput({
                    defaultText: 'Tag Standard...',
                    width: '110px',
                    readonly_input: true,
                    placeholderColor: '#b2b2b2',
                });

                var tagIDsd = $('[name=txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                //$('#'+tagIDsd).setAttribute( 'style', 'background-image: url( "http://placekitten.com.s3.amazonaws.com/homepage-samples/96/139.jpg" ) !important' );
                $('#' + tagIDsd + '_tag').css('width', '105px !important');
                //console.log('#'+tagIDsd+'_tag');


                var tagID = $('[name=txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                $('#' + tagID).tagsInput({
                    defaultText: 'Tags...',
                    //width: '100px',
                    placeholderColor: '#b2b2b2',
                });
                $('.video-tags-row label').css('display', 'none');
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                var tagID = $('[name=txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                $('#clearTagsButton1').click(function () {
                    $('#txtSearchTags1').val('');
                    $('#clearTagsButton1').css('display', 'none');
                    $('input#txtSearchTags1').quicksearch('ul#expList li', {
                        noResults: 'li#noresults'
                    });
                });
                $("#inline-crop-panel").fancybox({
                    width: '70%',
                    height: '70%',
                    type: "iframe",
                    closeClick: false,
                    helpers: {
                        title: null,
                        overlay: {closeClick: false}
                    }
                });

                $("#inline-crop-panel").click(function () {
                    pausePlayer();
                });

        <?php if (isset($_REQUEST['open_trim']) && $_REQUEST['open_trim'] == '1'): ?>
                    pausePlayer();
                    $("#inline-crop-panel").trigger('click');
        <?php endif; ?>

                $('.btn-trim-video').click(function () {
                    $(this).next().submit();
                });
                $('.default_tag1<?php echo $vidComments['Comment']['id'] ?>').on('click', function (e) {
                    e.preventDefault();
                    var posid = '';
                    //tag_name = $(this).text();
                    tag_name = $(this).attr('alt');
                    tag_name.replace(/\s/g, "");

                    posid = $(this).attr('position_id');
                    $('.default_tag1<?php echo $vidComments['Comment']['id'] ?>').each(function (index) {
                        var tag_name1 = $(this).text().trim();
                        tag_name1.replace(/\s/g, "");
                        $('#' + tagID).removeTag(tag_name1);
                        $(this).attr('status_flag', '0');
                        if ($(this).hasClass(defaulttagsclasses(index))) {
                            $('#' + tagID).removeTag(tag_name1);
                            $(this).removeClass(defaulttagsclasses(index) + 'bg');
                        }
                    });
                    $('.default_tag1<?php echo $vidComments['Comment']['id'] ?>').each(function (index) {
                        if ($(this).attr('position_id') != posid) {
                            $(this).removeClass(defaulttagsclasses($(this).attr('position_id')));
                            $(this).addClass(defaulttagsclasses1($(this).attr('position_id')));

                        }
                    });

                    if ($(this).hasClass(defaulttagsclasses(posid))) {
                        $('#' + tagID).removeTag(tag_name);
                        $(this).addClass(defaulttagsclasses(0));
                        $(this).removeClass(defaulttagsclasses(posid));
                        $(this).attr('status_flag', '0');
                        $('#comment_comment').attr("placeholder", "Add a comment...");
                        $('#synchro_time_class').val('');
                    }
                    else {
                        $('#' + tagID).addTag("# " + tag_name);
                        $(this).addClass(defaulttagsclasses(posid));
                        $(this).removeClass(defaulttagsclasses(0));
                        $(this).attr('status_flag', '1')
                        var spt = $(this).text().slice(2);
                    }
                });
            });
        </script>
    <?php endforeach; ?>
<?php endif; ?>



<script>
    $(document).ready(function () {
        var showChar = 200;
        var ellipsestext = "...";
        var moretext = "more";
        var lesstext = "less";
        $('.more').each(function () {
            var content = $(this).text();

            if (content.length > showChar) {
                var n = content.indexOf(' ', showChar);
                if (n == -1)
                    n = showChar;
                var c = content.substr(0, n);
                var h = content.substr(n);

                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h
                        + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

        });

        $(".morelink").click(function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
        $('.input-group').find('textarea').overlay([
            {
                match: /\B#\w+\S+/g,
                css: {
                    'background-color': '#d8dfea'
                }
            }
        ]);
    });

    function deleteComment(commentId) {
        //$(formName).submit();
        //getVideoComments();
        //return false;
        $.ajax({
            type: 'GET',
            url: home_url + '/Huddles/deleteVideoComments/' + commentId,
            success: function (response) {
                getVideoComments();
            }
        });

        return false;
    }

    function deleteCommentTag(tagId) {
        //$(formName).submit();
        $.ajax({
            type: 'GET',
            url: home_url + '/Huddles/deleteCommentTag/' + tagId,
            success: function (response) {
                getVideoComments();
            }
        });

        return false;
    }

</script>
<script type="text/javascript">
    $(function () {
        $('input#txtSearchTags1').quicksearch('ul#expList li', {
            noResults: 'li#noresults',
            'onAfter': function () {
                if (typeof $('#txtSearchTags1').val() !== "undefined" && $('#txtSearchTags1').val().length > 0) {
                    $('#clearTagsButton1').css('display', 'block');
                }
            }
        });

    });
    function defaulttagsclasses(posid) {
        var class_name = 'tags_qucls';
        if (posid == 1) {
            class_name = 'tags_quclsbg';
        }
        else if (posid == 2) {
            class_name = 'tags_sugclsbg';
        }
        else if (posid == 3) {
            class_name = 'tags_notesclsbg';
        }
        else if (posid == 4) {
            class_name = 'tags_strangthclsbg';
        }
        else if (posid == 5) {
            class_name = 'tags_strangthclsbg';
        }
        return class_name;
    }
    function defaulttagsclasses1(posid) {
        var class_name = 'tags_qucls';
        if (posid == 1) {
            class_name = 'tags_qucls';
        }
        else if (posid == 2) {
            class_name = 'tags_sugcls';
        }
        else if (posid == 3) {
            class_name = 'tags_notescls';
        }
        else if (posid == 4) {
            class_name = 'tags_strangthcls';
        }
        else if (posid == 5) {
            class_name = 'tags_strangthcls';
        }
        return class_name;
    }
</script>
