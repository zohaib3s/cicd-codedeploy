<?php if ($subjects): ?>
	<?php foreach ($subjects as $subject): ?>

	    <div id="chkContainer<?php echo $subject['Subject']['id'] ?>" class="check-container">
	        <input id="subject_ids<?php echo $subject['Subject']['id'] ?>" name="subject_ids<?php echo $subject['Subject']['id'] ?>" class="checkbox-3 subject_checkbox" type="checkbox" value="<?php echo $subject['Subject']['id'] ?>">
	        <label for="subject_ids<?php echo $subject['Subject']['id'] ?>" class="title"><?php echo $subject['Subject']['name'] ?></label>
	        <input type="text" id="txtsubject_ids<?php echo $subject['Subject']['id'] ?>" class="edit-field" value="<?php echo $subject['Subject']['name'] ?>"/>
	        <div class="controls controls<?php echo $subject['Subject']['name'] ?>">
                <?php
                    $imgedit_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/playlist8x8.png');
                    $imgdel_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/delete-icon8x9.png');
                    $imgsave_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/save8x8.png');
                    $imgundo_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/undo8x8.png');
                ?>
	           <?php echo $this->Html->image($imgedit_path, array('id'=>"imgEdit".$subject['Subject']['id'], 'class' => 'edit-controls edit-controls'.$subject['Subject']['id'])); ?>
	           <?php echo $this->Html->image($imgdel_path, array('id'=>"imgDelete".$subject['Subject']['id'], 'class' => 'edit-controls edit-controls'.$subject['Subject']['id'])); ?>
	           <?php echo $this->Html->image($imgsave_path, array('id'=>"imgSave".$subject['Subject']['id'], 'class' => 'save-controls save-controls'.$subject['Subject']['id'])); ?>
	           <?php echo $this->Html->image($imgundo_path, array('id'=>"imgUndo".$subject['Subject']['id'], 'class' => 'save-controls save-controls'.$subject['Subject']['id'])); ?>
	        </div>
        </div>
        <script type="text/javascript">

            
                
                $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                $('#imgEdit<?php echo $subject['Subject']['id'] ?>').click(function() {

                    $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').css('display', 'none');
                    $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').css('display', 'block');

                    $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val($('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').html());

                    $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'block');

                    $('.edit-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');
                });

                $('#imgUndo<?php echo $subject['Subject']['id'] ?>').click(function() {

                    $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').css('display', 'block');
                    $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                    $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                    $('.edit-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'block');
                });

                $('#imgSave<?php echo $subject['Subject']['id'] ?>').click(function() {

                    var subject_id = $(this).attr('id').replace('imgSave','');

                    $.ajax({
                        type: 'POST',
                        data: {
                            subject: $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val()
                        },
                        url: home_url + '/VideoLibrary/updateSubject/' +  subject_id,
                        success: function(response) {
                            
                            $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').html($('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val());

                            $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').css('display', 'block');
                            $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                            $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                            $('.edit-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'block');

                        },
                        errors: function(response) {
                            alert(response.contents);
                        }

                    });

                });

                $('#imgDelete<?php echo $subject['Subject']['id'] ?>').click(function() {

                    var subject_id = $(this).attr('id').replace('imgDelete','');

                    $.ajax({
                        type: 'POST',
                        data: {
                            subject: $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val()
                        },
                        url: home_url + '/VideoLibrary/deleteSubject/' +  subject_id,
                        success: function(response) {
                            
                            $('#chkContainer<?php echo $subject['Subject']['id'] ?>').remove();

                        },
                        errors: function(response) {
                            alert(response.contents);
                        }

                    });

                });

        </script>
	<?php endforeach; ?>
<?php endif; ?>