<head>

    <?php if (IS_PROD): ?>

        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-KQKHC78');</script>

    <?php endif; ?>

    <?php echo $this->Html->charset(); ?>
    <?php //echo $this->Html->meta('icon'); ?>
    <title> <?php echo $title_for_layout; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">

    <?php
    $use_local_store = Configure::read('use_local_file_store');
    $amazon_assets_url = Configure::read('amazon_assets_url');
    if ($this->params['controller'] == 'Users' && $this->params['action'] == 'login') {
        if ($use_local_store) {
            echo $this->Minify->css(array('sibme.min', 'sibme', 'style-extention', 'pro_dropdown_2', 'jquery.loadmask.css', 'alerts'));
        } else {
            echo '<link href="' . $amazon_assets_url . 'static/css/min-accounts-users.zip.css" media="screen" rel="stylesheet" type="text/css" />';
        }
    } else {
        if ($use_local_store) {
            echo $this->Minify->css(array('app', 'sibme', 'style-extention', 'pro_dropdown_2', 'jquery.loadmask.css', 'alerts', 'bootstrap.min', 'v4/bootstrap.min', 'v4/template'));
        } else {
            echo '<link href="' . $amazon_assets_url . 'static/css/min-accounts-general.zip.css" media="screen" rel="stylesheet" type="text/css" />';
        }
    }
    ?>

    <script type="text/javascript">
        var home_url = '<?php echo $this->base; ?>';
        var supress_app_init = false;
        var bucket_name = "<?php echo Configure::read('bucket_name'); ?>";
    </script>

    <?php
    $js_scripts = array(

       'jquery-2.1.1', 'application', 'jquery.validate.min', 'jquery.form', 'v4/jquery-3.2.1.slim.min', 'v4/bootstrap.min', 'v4/editor'
    );

    if ($use_local_store) {
        echo $this->Html->script($js_scripts);
    } else {
        echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-accounts-general.js"></script>';
    }
    ?>

    <link href="https://desk-customers.s3.amazonaws.com/shared/email_widget.css" media="screen" rel="stylesheet" type="text/css" />
    <?php if (isset($_SESSION['site_id']) && $_SESSION['site_id'] == 2): ?>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon_hmh.ico" />
    <?php else: ?>
        <link rel="shortcut icon" type="image/x-icon" href="/favicon_sibme.ico" />
    <?php endif; ?>
</head>
