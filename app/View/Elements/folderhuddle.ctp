<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
$btn_1_color = $this->Custom->get_site_settings('primary_bg_color');
$btn_2_color = $this->Custom->get_site_settings('secondary_bg_color');
?>
<div id="showflashmessage1"></div>
<link rel="stylesheet" href="/app/js/dist/themes/default/style.min.css" />
<script src="/app/js/dist/jstree.min.js"></script>
<?php if ($user_permissions['UserAccount']['manage_collab_huddles'] == '1' || $user_permissions['UserAccount']['manage_coach_huddles'] == '1' || $user_permissions['UserAccount']['manage_evaluation_huddles'] == '1'): ?>
    <a id="btn-new-huddle" href="<?php echo $this->base . '/Folder/createhuddle/' . $folder_id ?>" class="btn btn-green right"><?php echo $new_huddle_button ? $new_huddle_button : ''; ?> </a>
<?php endif; ?>
<?php if (( ($user_current_account['roles']['role_id'] == '120' && $user_permissions['UserAccount']['folders_check'] == '1' ) || ($user_current_account['roles']['role_id'] == '110' && $user_permissions['UserAccount']['folders_check'] == '1' ) || ($user_current_account['roles']['role_id'] == '100' && $user_permissions['UserAccount']['folders_check'] == '1' ) || ($user_current_account['roles']['role_id'] == '115' ))): ?>
    <a id="btn-new-huddle" data-toggle="modal" data-target="#createfolder" style="margin-right:5px;background-color: <?php echo $btn_2_color ?>; border-color:<?php echo $btn_2_color ?>;" href="<?php //echo $this->base . '/Folder/create/' . $folder_id                      ?>" class="btn btn-orange right" >New Folder  </a>
<?php endif; ?>
<div id="createfolder" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog"  style="width: 400px;">
        <div class="modal-content">
            <div class="header" style="padding-left: 25px;padding-bottom: 0px;margin-bottom: 0px;padding-top: 15px;">
                <h4 class="header-title nomargin-vertical smargin-bottom">New Huddle Folder</h4>
                <a id="cross1" class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">x</a>
            </div>
            <form method="post" id="new_video_huddle2" enctype="multipart/form-data" class="new_video_huddle"
                  action="<?php echo $this->base . '/Folder/create'; ?>" accept-charset="UTF-8">
                <div id="tabs">

                    <div>
                        <div id="step-1" style="margin-left: -20px;">
                            <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8">
                                <input name="data[folder_id]" id="hidden_folder_id" type="hidden" value="<?php echo $folder_id; ?>"/></div>

                            <a class="close-reveal-modal"></a>
                            <div class="span5">
                                <div class="row"  style="margin-bottom: 20px;">
                                    <span class="wiz-step1-radio"><input type="radio" id="collab-huddle" name="type" class="cls_coaching_trig_2" checked="checked" value="1" style="display: none;" checked="checked"></span>
                                    <div style="clear: both;height:10px;"></div>
                                </div>
                                <div class="row" style="margin-bottom: 20px;margin-left: 0px;">
                                    <input name="data[hname]" id="video_folder_name" class="size-big huddle-name "  placeholder="Folder Name" size="30" type="text" style="width: 350px;" required/>
                                    <label id="error" for="video_folder_name" class="error" style="display:none;background-color: none !important;">This field is required.</label>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="row" style="display:none;">
                                    <?php echo $this->Form->textarea('hdescription', array('rows' => '4', 'placeholder' => 'Folder Description (Optional)', 'id' => 'video_huddle_description', 'cols' => '40', 'class' => 'size-big')); ?>
                                </div>
                                <?php echo $this->Form->input('created_by', array('value' => $user_id, 'type' => 'hidden', 'id' => 'video_huddle_created_by')); ?>
                            </div>
                            <div style="clear: both;height:10px;"></div>
                            <!--  <div class="row-fluid">
                                <div class="groups-table span12">

                                    <div class="span4 huddle-span4" style="margin-left:0px;">
                                        <div class="groups-table-header groups-table-header-create">
                                            <div style="width: 200px; float: left; margin-left: 8px;">
                            <?php if ($user_permissions['UserAccount']['permission_maintain_folders'] == 1 && $user_permissions['UserAccount']['permission_administrator_user_new_role'] == 1): ?>
                                                                                                                                        <a style="margin-left: -8px;" id="pop-up-btn" data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="btn btn-small btn-green" href="#"><span class="plus">+</span>
                                                                                                                                        </a>
                            <?php else: ?>
                                                                                                                                        <a style="margin-left: -8px;"><span class="">&nbsp;&nbsp;</span>
                                                                                                                                        </a>
                            <?php endif; ?>
                                            </div>
                                            <div style="width: 248px; padding: 0px;float: left;margin-left: 426px;" class="search-box">
                                                <div  id="header-container" class="filterform">
                                                </div>
                                            </div>
                                            <a class="appendix right" href="#" id="Coaches_info" >?</a>
                                            <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                                <h3>Info</h3>
                                                <p>Admin: Can add/remove users; upload/download/copy/delete all videos and documents; clip all videos; add/edit/delete all video annotations and comments; create/participate/edit all Folder discussions; delete Huddle.</p>
                                                <p>Members: Can upload/download/copy/delete videos and documents they add to the Huddle; clip their videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                                <p>Viewers: View videos and documents only.</p>
                                            </div>
                                            <a class="appendix right" href="#" id="Coachee_info" style="display:none;">?</a>
                                            <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                                <h3>Info</h3>
                                                <p>Coach(es):  Can edit coaching Huddle, by adding or removing other coaches in the Huddle; upload/download/copy all videos and supporting resources; clip/edit videos; add/edit/delete all video annotations; enable/disable tags/account framework; create/participate/edit all discussions; delete the coaching Huddle.</p>
                                                <p>Coachee: Can upload/download/copy/delete videos and resources they add to the Huddle; clip/edit their own videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                            </div>
                                            <div style="clear:both"></div>
                            <?php if (count($super_users) > 1 || (isset($folderUsers) && count($folderUsers) > 1) || (isset($users_groups) && count($users_groups) > 0)): ?>
                                                                                                                                    <div id="select-all-none-cnt" class="select-all-none selectnone"  style="float: left;margin-left: 454px;margin-top: 1border.pngpx;min-width: 123px;max-width: 200px;">
                                                                                                                                        <label id="select-all-none" for="select-all"><input type="checkbox" name="select-all" id="select-all" style="display:inline-block;" /> <span id="select-all-label">Select None</span></label>
                                                                                                                                    </div>
                            <?php endif; ?>
                            <?php if (1 == 1): ?>
                                                                                                                                    <div id="select_all_coaches_panel" class="huddles-select-all-block coach_mentee">
                                                                                                                                        <span class="caoch-checkbox" >
                                                                                                                                            <input type="checkbox" id="caoch-checkbox" name="is_coach"> Coach</span>
                                                                                                                                        <span class="mentee-checkbox"><input type="checkbox" id="mentee-checkbox" name="is_mentor" >Coachee</span>
                                                                                                                                    </div>
                            <?php endif; ?>

                                            <div class="huddles-select-all-block coach_perms">
                                                <span class="admin-radio"><input type="radio" id="admin-radio" name="select-all"> Admin</span>
                                                <span class="member-radio"><input type="radio" id="member-radio" name="select-all" style="margin-left: -8px;"> Member</span>
                                                <span class="viewer-radio"><input type="radio" id="viewers-radio" name="select-all"> Viewer</span>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                        <div class="groups-table-content">
                                            <div class="widget-scrollable">
                                                <div class="scrollbar" style="height: 155px;">
                                                    <div class="track" style="height: 155px;">
                                                        <div class="thumb" style="top: 0px; height: 90.3195px;">
                                                            <div class="end"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="viewport short">
                                                    <div class="overview" style="top: 0px;">
                                                        <div  id="people-lists">
                                                            <ul id="list-containers">
                                                                <input type="hidden" id="chk_is_coachee" value="0" name="chk_is_coachee"  />
                            <?php
                            if (count($users_record) > 0):
                                ?>
                                <?php if ($users_record): ?>
                                    <?php foreach ($users_record as $row): ?>
                                        <?php
                                        if ($row['id'] == $user_id)
                                            continue;
                                        ?>
                                        <?php if ($row['is_user'] == 'admin'): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                <li style="display:none;">
                                                                                                                                                                                                                                                                                                                                                                                                                                    <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="super-user" type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"><?php echo trim($row['first_name'] . " " . $row['last_name']); ?> </a></label>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">

                                            <?php if (1 == 1): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <div class="permissions coach_mentee">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <span class="caoch-checkbox" ><input type="radio" value="1" id="caoch-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>" >Coach</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <span class="mentee-checkbox"><input value="2" type="radio" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"> <span id="mentee-chk"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div>
                                            <?php endif; ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="permissions coach_perms">
                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200"><input  class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn"  type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="<?php echo $row['id'] ?>" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Folder Participant</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                </li>

                                        <?php elseif ($row['is_user'] == 'member'): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                <li>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="member-user"  type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"> <?php echo $row['first_name'] . " " . $row['last_name'] ?></a></label>

                                            <?php if (1 == 1): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <div class="permissions coach_mentee">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <span class="caoch-checkbox" >
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Coach</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <span class="mentee-checkbox"><input type="radio" value="2"   id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div>
                                            <?php endif; ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="permissions coach_perms">
                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200"><input class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="<?php echo $row['id'] ?>" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Folder Participant</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                </li>

                                        <?php elseif ($row['is_user'] == 'group'): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                <li>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <label  class="huddle_permission_editor_row"><input class="viewer-user" type="checkbox" value="<?php echo $row['id']; ?>" name="group_ids[]" id="group_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"><?php echo $row['name']; ?></a></label>

                                            <?php if (1 == 1): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <div class="permissions coach_mentee">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <span class="caoch-checkbox" >
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Coach</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div>
                                            <?php endif; ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="permissions coach_perms">
                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="group_role_<?php echo $row['id'] ?>_200"><input  class="admin-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="group_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="group_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                    <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="1" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Folder Participant</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                                                                                                                                        <li>
                                                                                                                                                            To Invite users into the account <a data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="" href="#"><span class="">click here</span></a>
                                                                                                                                                        </li>
                            <?php endif; ?>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="form-actions" style="text-align: left;margin-left: 22px;margin-top:0px;">
                                <input id="creatingfolder" type="button" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>; border-color:<?php echo $this->Custom->get_site_settings('primary_border_color'); ?>" value="Create Folder" name="commit" onclick="CreateFolder1();" class="btn btn-green" style="background-color: <?php echo $btn_1_color ?>;border-color:<?php echo $btn_1_color ?>;"  >
                                <a id="cancel" class="btn btn-white" style="font-size: 14px;" onclick="$('#createfolder').modal('hide');">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var hudd_id = '';
        $(document).on("click", ".move_huddle", function () {
            var huddle_id = $(this).attr("huddle_id");
            $('#tree-container').jstree({
                'plugins': ['wholerow'],
                'core': {
                    'data': {
                        "url": "<?php echo $this->base . '/folder/treeview_detail'; ?>",
                        "dataType": "json" // needed only if you do not supply JSON headers
                    }
                }
            })
            $('#tree-container').jstree(true).settings.core.data.url = home_url + '/folder/treeview_detail/' + huddle_id;
            $('#tree-container').jstree('refresh');
            $("#txthuddleid").val($(this).attr("huddle_id"));
            $("#txtfoldid").val($(this).attr("fold_id"));
            hudd_id = $(this).attr("id");
        });
        $(document).on("click", ".moveFolder", function () {
            var user_id = "<?php echo $user_id; ?>";
            var view_mode = "<?php echo $view_mode; ?>";
            var account_id = "<?php echo $account_id; ?>";
            var sort = "<?php echo $sort; ?>";

            $('#moveFolderBtn').prop('disabled', true);
            var present_folder_id = $("#txtfolderid").val();
            var move_to_folder_id = $("#txtdestfolderid").val();
            //var move_to_folder_id = $(this).attr("id").replace('_anchor','');
            var huddle = $("#txthuddleid").val();
            var fold_id = $("#txtfoldid").val();
            if (move_to_folder_id == present_folder_id) {
                $('#moveFolderBtn').prop('disabled', false);
                $('#movefolderto').modal('hide');
                return true;
            }

            $.ajax({
                url: home_url + '/Folder/movehuddletofolder',
                data: {huddle: huddle, present_id: present_folder_id, move_to_folder: move_to_folder_id},
                dataType: 'JSON',
                type: 'POST',
                success: function (response) {
                    $('#moveFolderBtn').prop('disabled', false);
                    $('#movefolderto').modal('hide');
                    $(document).ajaxStop($.unblockUI);
                    if (response == true) {
                        $("#flashMessage").hide();
                        $("#showflashmessage1").fadeIn();
                        //  $('#huddle-listings').html(response.contents);
                        // $('.page-title__counter').html($('#search-huddle-count').html());
                        var show_msg = '';
                        if (fold_id > 0)
                            show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">Folder moved Successfully.</div>';
                        else
                            show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">Huddle moved Successfully.</div>';
                        $("#showflashmessage1").prepend(show_msg);
                        $("#txtSearchFolderHuddles").val("");
                        $("#clearSearchHuddlesfolder").hide();
                        $('#huddle_row_' + huddle).hide();
                        if (view_mode == 'grid')
                        {
                            $.ajax({
                                url: home_url + '/Folder/refresh_folders_stats/' + move_to_folder_id,
                                data: {huddle: huddle, present_id: present_folder_id, move_to_folder: move_to_folder_id, user_id: user_id, sort: sort, account_id: account_id},
                                dataType: 'JSON',
                                type: 'POST',
                                success: function (response) {
                                    $('#huddle_row_' + move_to_folder_id).html(response);
                                }

                            });
                        }
                    } else {
                        //   $('.page-title__counter').html($('#search-huddle-count').html());
                        //   $('#huddle-listings').html(response.contents);
                    }
                },
                error: function () {
                    $('#moveFolderBtn').prop('disabled', false);
                }
            });

        });

    });
</script>
<?php
$params = $this->request->pass;

if (isset($this->request->data["txtSearchHuddles"]))
    $huddleSearch = $this->request->data["txtSearchHuddles"];
else if (isset($params[2]))
    $huddleSearch = $params[2];
else
    $huddleSearch = '';
?>

<div id="sort-mode" class="dropdown right mmargin-right" style="width: 200px !important;">
    <ul>
        <li><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/' . $sort . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" title="View all" class="active"><?php
                if ($sort == 'name') {
                    echo 'Title';
                } elseif ($sort == 'date') {
                    echo 'Date Created';
                } elseif ($sort == 'coaching') {
                    echo 'Coaching Huddles';
                } elseif ($sort == 'collaboration') {
                    echo 'Collaboration Huddles';
                } elseif ($sort == 'evaluation') {
                    echo 'Assessment Huddles';
                } elseif ($sort == 'folders' && !empty($folders)) {
                    echo 'Folders';
                } else {
                    echo 'Sort';
                }
                ?></a></li>
        <li <?php
        if ($sort == 'name') {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/name' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Title</a></li>
        <li <?php
        if ($sort == 'date') {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/date' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Date Created</a></li>
        <li <?php
        if ($sort == 'coaching' || empty($coach_huddles_dropdown)) {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/coaching' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Coaching Huddles</a></li>
        <li <?php
        if ($sort == 'collaboration' || empty($collab_huddles_dropdown)) {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/collaboration' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Collaboration Huddles</a></li>
            <?php
            if ($this->Custom->check_if_eval_huddle_active($account_id)) {
                ?>
            <li <?php
            if ($sort == 'evaluation' || empty($eval_huddles_dropdown)) {
                echo 'style="display:none;"';
            }
            ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/evaluation' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Assessment Huddles</a></li>
            <?php } ?>
        <li <?php
        if ($sort == 'folders' || empty($folders_check_dropdown)) {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/folders' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Folders</a></li>
    </ul>
</div>
<input type="hidden" id="txtfolderid" name="txtfolderid" value="<?php echo $folder_id; ?>" />
<div id="view" class="mmargin-right right">
    <a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/list' ?>" class="icon-list <?php
    if ($view_mode == 'list') {
        echo 'active';
    }
    ?>" rel="tooltip" title="List View"></a>
    <a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/grid' ?>" class="icon-grid <?php
    if ($view_mode != 'list') {
        echo 'active';
    }
    ?>" rel="tooltip" title="Grid View"></a>
</div>
<div class="search-box12" style="width: 335px;float: right;position: relative;margin-top: 4px;">
    <input type="hidden" id="search-mode" action="" class="btn-search" value="<?php echo $view_mode; ?>"/>
    <input type="button" id="btnHuddleSearchfolder" action="" class="btn-search" value="" style="width:27px;"/>
    <input class="text-input" name="txtSearchFolderHuddles" id="txtSearchFolderHuddles" type="text" value="" placeholder="Search ..." />
    <span id="clearSearchHuddlesfolder"  style="display: none; margin-right: -15px;" class="clear-video-input-box cross-search search-x-huddles">X</span>
</div>
<h1 class="page-title nomargin-top"><?php echo $page_title ? $page_title : ''; ?> <span class="page-title__counter"><?php echo '( ' . $total_huddles . ' )' ?></span></h1>
<hr class="style2">
<style>
    .cross-search{
        margin-left: 6px;
        position: absolute;
        right: 54px;
        width: 327px;
        cursor: pointer;
    }
    .tree_scroll_box ul li:first-child a i{
        background: url(/app/img/crhomeicon.png) no-repeat !important;
        top: 2px;
    }
    .tree_scroll_box ul li ul li:first-child a i{

        background-image: url(/app/js/dist/themes/default/32px.png) !important;
        background-position: -218px -67px !important;
        top:0px !important;
    }

</style>
<script type="text/javascript">

    $("#txtSearchFolderHuddles").on("keypress", function (e) {
        $("#clearSearchHuddlesfolder").show();
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            dofolderHuddleSearch_textbox();
            return false;
        }
    });

    $("#btnHuddleSearchfolder").click(function () {
        if ($("#txtSearchFolderHuddles").val().length > 0) {
            dofolderHuddleSearch_textbox();
        }
    });

    $("#clearSearchHuddlesfolder").click(function () {
        $("#txtSearchFolderHuddles").val("");
        $("#clearSearchHuddlesfolder").hide();
        dofolderHuddleSearch();
        return false;
    });

    function dofolderHuddleSearch() {
        var $title = $('#txtSearchFolderHuddles').val();
        var $mode = $('#search-mode').val();
        var $folder_id = $('#txtfolderid').val();
        //$.blockUI({message: $('#domMessage').html()});
        $.ajax({
            url: home_url + '/Folder/search',
            data: {title: $title, mode: $mode, folder_id: $folder_id},
            dataType: 'JSON',
            type: 'POST',
            success: function (response) {
                //$(document).ajaxStop($.unblockUI);
                if (response.status == true) {
                    $('#huddle-listings').html(response.contents);
                    $('.page-title__counter').html($('#search-huddle-count').html());

                } else {
                    $('.page-title__counter').html($('#search-huddle-count').html());
                    $('#huddle-listings').html(response.contents);
                }
//                $(".btn-move-to").hide();
//                $(".sel-move-to").hide();
//                $(".huddle-to-move").hide();
//                $(".btn-move").show();
            },
            error: function () {

            }
        });
    }

    function dofolderHuddleSearch_textbox() {
        var $title = $('#txtSearchFolderHuddles').val();
        var $mode = $('#search-mode').val();
        var $folder_id = $('#txtfolderid').val();
        //$.blockUI({message: $('#domMessage').html()});
        $.ajax({
            url: home_url + '/Folder/search_textbox',
            data: {title: $title, mode: $mode, folder_id: $folder_id},
            dataType: 'JSON',
            type: 'POST',
            success: function (response) {
                //$(document).ajaxStop($.unblockUI);
                if (response.status == true) {
                    $('#huddle-listings').html(response.contents);
                    $('.page-title__counter').html($('#search-huddle-count').html());

                } else {
                    $('.page-title__counter').html($('#search-huddle-count').html());
                    $('#huddle-listings').html(response.contents);
                }
//                $(".btn-move-to").hide();
//                $(".sel-move-to").hide();
//                $(".huddle-to-move").hide();
//                $(".btn-move").show();
            },
            error: function () {

            }
        });
    }

</script>
<?php // echo "<pre>";print_r($folder_tree); echo "</pre>";  ?>
<div id="movefolderto" class="modal in" role="dialog" aria-hidden="false">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom">Choose a destination Folder</h4>
                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">x</a>
            </div>
            <div class="foldrmn">
                <input type="hidden" id="txthuddleid" name="txthuddleid" value="" />
                <input type="hidden" id="txtfoldid" name="txtfoldid" value="" />
                <input type="hidden" id="txtdestfolderid" name="txtfolderid" value="" />
                <div class="tree_scroll_box">
                    <div id="tree-container"></div>
                </div>
                <div class="clear"></div>
                <div class="btns" style="float: right;position: relative;margin-bottom: 10px;">
                    <input class="moveFolder btn btn-green" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="moveFolderBtn" type="button" value="Move" />
                    <input class="btn btn-white" type="reset" value="Cancel" onclick="$('#movefolderto').modal('hide');"/>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    function CreateFolder1() {
        folder_id = $("#hidden_folder_id").val();
        folder_name = $("#video_folder_name").val();
        if (folder_name === '')
        {
            $("#error").css("display", "inline-block");

//             $('#createfolder').modal('hide');
//             $("#flashMessage").hide();
//             $("#showflashmessage1").fadeIn();
//             var show_msg = '<div id="flashMessage" class="message error" style="cursor: pointer;">Please enter folder name.</div>';
//             $("#showflashmessage1").prepend(show_msg);
//             $("#txtSearchHuddles:input").val('');
//            dofolderHuddleSearch();

        }
        else {
            $.ajax({url: home_url + '/Folder/create',
                data: {hname: $("#video_folder_name").val(), hdescription: '', folder_id: folder_id},
                type: 'post',
                success: function (output) {
                    $('#createfolder').modal('hide');
                    $("#flashMessage").hide();
                    $("#showflashmessage1").fadeIn();
                    if (output == 'folderexists')
                    {
                        var show_msg = '<div id="flashMessage" class="message error" style="cursor: pointer;">A folder with same name already exist, please try another name. You might not see the folder with name "' + folder_name + '" because you are not participating in that.</div>';
                    }
                    else {
                        var show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">' + folder_name + ' has been saved successfully.</div>';
                    }
                    $("#showflashmessage1").prepend(show_msg);
                    $("#txtSearchHuddles:input").val('');
                    dofolderHuddleSearch();

                }
            });

        }

    }
    $(document).on("click", "#showflashmessage1", function () {
        $("#showflashmessage1").fadeOut(200);
    });
    $(document).on("click", "#cancel", function () {
        $("#video_folder_name").val('');
        $("#error").hide();

    });
    $(document).on("click", "#cross1", function () {
        $("#video_folder_name").val('');
        $("#error").hide();

    });
    $(document).on("click", "#creatingfolder", function () {
        $("#video_folder_name").val('');
    });

    $(document).on("click", "#flashMessage", function () {
        $("#flashMessage").fadeOut(200);
    });


    $('#tree-container')
            .on("changed.jstree", function (e, data) {
                if (data.selected.length) {
                    $("#txtdestfolderid").val(data.instance.get_node(data.selected[0]).id);
                }
            });
    $('#new_video_huddle2').submit(function (evt) {
        evt.preventDefault();

    });

    $("#video_folder_name").keyup(function (e) {
        var str_length = $("#video_folder_name").val().trim();
        if (str_length.length > 0 && e.which == 13) {
            $("#creatingfolder").trigger('click');
        }
    });

</script>