<style>
    th:first-child{
        width: 140px!important;
    }
    td{
        padding: 10px 10px;
    }
    th.ui-th-column div{
        white-space:normal !important;
        height:auto !important;
        padding:2px;
    }
    .ui-jqgrid .ui-jqgrid-resize {height:100% !important;}
    .data_grid_adjs {
        height: auto !important;
    }
    .sel_dur_grph{
        width:200px;
        float:left;
    }
    .cl_token_field{
        width:200px;
        float:left;
        margin-left:5px;
        margin-right:5px;
    }
    .filter_box select{
        -moz-appearance: tab-scroll-arrow-forward;
        vertical-align: top;
    }
    .filter_box .tokenfield{
        display:inline-block;
    }
    .token .close{
        margin-top:0px;
        margin-right:0px;
    }
    .token .token-label{
        max-width: 60px !important;
    }
    .tokenfield{
        min-height: 37px !important;
    }
    .tokenfield .token-input{
        height: 20px !important;
    }
    .colCell {position: sticky;
              left: 0;
              width: 165px !important;
              z-index: 9;
              background: #fff;}
    #grid_account_name {position: sticky;
                        left: 0;
                        width: 165px !important;
                        z-index: 9;
                        background: #e0e0e0;}

    .search_mod_class
    {
        position: sticky;
        left: 0;
        width: 165px !important;
        z-index: 9;
        background: #e0e0e0;

    }

    #ui-datepicker-div
    {
        z-index:9 !important;
    }

</style>
<form method="post" id="frmExcelExportAnalytics" name="frmExcelExportAnalytics" action="<?php echo $this->base . "/Dashboard/excelExport"; ?>">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="acctName" id="acctName" value="" />
</form>
<table id="grid"></table>
<div id="pager2"></div>

<?php
$frameworks = $user_data_frameworks['frameworks'];
$account_id = $user_data_frameworks['account_id'];
?>

<script type="text/javascript">

    $(function () {
        var data = "";
        var library_check = <?php echo (!empty($this->Custom->get_account_video_permissions($account_id))) ? $this->Custom->get_account_video_permissions($account_id) : 0; ?>;
        $grid = $("#grid");
        if (library_check)
        {
            columns_titles = ['id', 'Account ID', 'Name', 'Email', 'Account Name', 'User Role', 'User Type', 'Videos Uploaded to Workspace', 'Videos Uploaded to Huddles', 'Videos Shared to Huddles', 'Videos Uploaded to Video Library', 'Videos Shared to Video Library', 'Workspace Video Views', 'Huddle Video Views', 'Library Video Views', 'Total Video Hours Uploaded', 'Total Video Hours Viewed', 'Huddles Created', 'Workspace Video Notes', 'Huddle Video Comments', 'Huddle Video Replies', 'Resources Uploaded', 'Resources Viewed', 'Scripted Video Observations', 'Scripted Observations', 'Total Logins'];
        } else
        {
            columns_titles = ['id', 'Account ID', 'Name', 'Email', 'Account Name', 'User Role', 'User Type', 'Videos Uploaded to Workspace', 'Videos Uploaded to Huddles', 'Videos Shared to Huddles', 'Workspace Video Views', 'Huddle Video Views', 'Total Video Hours Uploaded', 'Total Video Hours Viewed', 'Huddles Created', 'Workspace Video Notes', 'Huddle Video Comments', 'Huddle Video Replies', 'Resources Uploaded', 'Resources Viewed', 'Scripted Observations', 'Scripted Video Observations', 'Total Logins'];
        }
        if (library_check) {

            columns_data = [
                {name: "user_id", width: 10, hidden: true},
                {name: "account_id", width: 10, hidden: true},
                {name: "account_name", width: 165, search: true, classes: 'colCell', searchoptions: {sopt: ['cn']}},
                {name: "email", width: 150, search: false},
                {name: "company_name", width: 150, search: false}, {name: "user_role", width: 150, search: false},
                {name: "user_type", width: 150, search: false},
                {name: "workspace_upload_counts", width: 85, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(4,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                cellvalue + "</a>";
                    }},
                {name: "video_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(4,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                cellvalue + "</a>";
                    }},
                {name: "shared_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(22,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                cellvalue + "</a>";
                    }},
                {name: "library_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(2,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                cellvalue + "</a>";
                    }},
                {name: "library_shared_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(22,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                cellvalue + "</a>";
                    }},
                {name: "workspace_videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                cellvalue + "</a>";
                    }},
                {name: "videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                cellvalue + "</a>";
                    }},
                {name: "library_videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                cellvalue + "</a>";
                    }},
                {name: "total_hours_uploaded", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(24,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                cellvalue + "</a>";
                    }},
                {name: "total_hours_viewed", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(25,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                cellvalue + "</a>";
                    }},
                {name: "huddle_created_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(1,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "workspace_comments_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(5,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                cellvalue + "</a>";
                    }},
                {name: "comments_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(5,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                cellvalue + "</a>";
                    }},
                {name: "replies_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(8,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "documents_uploaded_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(3,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "documents_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(13,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "scripted_video_observations", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(20,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "scripted_observations", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(23,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "web_login_counts", width: 70, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(9,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, ];
        }

        else
        {
            columns_data = [
                {name: "user_id", width: 10, hidden: true},
                {name: "account_id", width: 10, hidden: true},
                {name: "account_name", width: 165, search: true, classes: 'colCell', searchoptions: {sopt: ['cn']}},
                {name: "email", width: 150, search: false},
                {name: "company_name", width: 150, search: false}, {name: "user_role", width: 150, search: false},
                {name: "user_type", width: 150, search: false},
                {name: "workspace_upload_counts", width: 85, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(4,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                cellvalue + "</a>";
                    }},
                {name: "video_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(4,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                cellvalue + "</a>";
                    }},
                {name: "shared_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(22,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                cellvalue + "</a>";
                    }},
                {name: "workspace_videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                cellvalue + "</a>";
                    }},
                {name: "videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                cellvalue + "</a>";
                    }},
                {name: "total_hours_uploaded", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(24,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                cellvalue + "</a>";
                    }},
                {name: "total_hours_viewed", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(25,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                cellvalue + "</a>";
                    }},
                {name: "huddle_created_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(1,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "workspace_comments_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(5,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                cellvalue + "</a>";
                    }},
                {name: "comments_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(5,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                cellvalue + "</a>";
                    }},
                {name: "replies_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(8,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "documents_uploaded_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(3,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "documents_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(13,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "scripted_video_observations", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(20,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "scripted_observations", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(23,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, {name: "web_login_counts", width: 70, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                        return '<a href=javascript:paramLink(9,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                cellvalue + "</a>";
                    }}, ];
        }

<?php
if ($this->Custom->is_enabled_framework_and_standards($account_id)):
    if (!empty($frameworks) && count($frameworks) > 0):
        ?>

                var library_check = <?php echo (!empty($this->Custom->get_account_video_permissions($account_id))) ? $this->Custom->get_account_video_permissions($account_id) : 0; ?>;
                if (library_check)
                {
                    columns_titles = ['id', 'Account ID', 'Name', 'Email', 'Account Name', 'User Role', 'User Type', 'Videos Uploaded to Workspace', 'Videos Uploaded to Huddles', 'Videos Shared to Huddles', 'Videos Uploaded to Video Library', 'Videos Shared to Video Library', 'Workspace Video Views', 'Huddle Video Views', 'Library Video Views', 'Total Video Hours Uploaded', 'Total Video Hours Viewed', 'Huddles Created', 'Workspace Video Notes', 'Huddle Video Comments', 'Huddle Video Replies', 'Resources Uploaded', 'Resources Viewed', 'Scripted Video Observations', 'Scripted Observations', 'Total Logins'];
                }
                else
                {
                    columns_titles = ['id', 'Account ID', 'Name', 'Email', 'Account Name', 'User Role', 'User Type', 'Videos Uploaded to Workspace', 'Videos Uploaded to Huddles', 'Videos Shared to Huddles', 'Workspace Video Views', 'Huddle Video Views', 'Total Video Hours Uploaded', 'Total Video Hours Viewed', 'Huddles Created', 'Workspace Video Notes', 'Huddle Video Comments', 'Huddle Video Replies', 'Resources Uploaded', 'Resources Viewed', 'Scripted Video Observations', 'Scripted Observations', 'Total Logins'];
                }
                if (library_check) {
                    columns_data = [
                        {name: "user_id", width: 10, hidden: true},
                        {name: "account_id", width: 10, hidden: true},
                        {name: "account_name", width: 165, search: true, classes: 'colCell', searchoptions: {sopt: ['cn']}},
                        {name: "email", width: 150, search: false},
                        {name: "company_name", width: 150, search: false}, {name: "user_role", width: 150, search: false},
                        {name: "user_type", width: 150, search: false},
                        {name: "workspace_upload_counts", width: 85, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(4,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "video_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(4,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "shared_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(22,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "library_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(2,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "library_shared_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(22,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "workspace_videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "library_videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "total_hours_uploaded", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(24,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "total_hours_viewed", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(25,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "huddle_created_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(1,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "workspace_comments_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(5,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "comments_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(5,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "replies_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(8,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "documents_uploaded_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(3,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "documents_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(13,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "scripted_video_observations", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(20,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "scripted_observations", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(23,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "web_login_counts", width: 70, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(9,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                                //                    {name: "detail", align: 'center', formatter: 'link', formatter: 'showlink', formatoptions: {baseLinkUrl: 'javascript:', showAction: "Link('", addParam: "');"}, width: 90, search: false}
                    ];
                }

                else
                {

                    columns_data = [
                        {name: "user_id", width: 10, hidden: true},
                        {name: "account_id", width: 10, hidden: true},
                        {name: "account_name", width: 165, search: true, classes: 'colCell', searchoptions: {sopt: ['cn']}},
                        {name: "email", width: 150, search: false},
                        {name: "company_name", width: 150, search: false}, {name: "user_role", width: 150, search: false},
                        {name: "user_type", width: 150, search: false},
                        {name: "workspace_upload_counts", width: 85, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(4,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "video_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(4,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "shared_upload_counts", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(22,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "workspace_videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "videos_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(11,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "total_hours_uploaded", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(24,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "total_hours_viewed", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(25,' + rowObject.user_id + ',' + rowObject.account_id + ',2)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "huddle_created_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(1,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "workspace_comments_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(5,' + rowObject.user_id + ',' + rowObject.account_id + ',3)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "comments_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(5,' + rowObject.user_id + ',' + rowObject.account_id + ',1)>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "replies_initiated_count", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(8,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "documents_uploaded_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(3,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "documents_viewed_count", width: 80, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(13,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "scripted_video_observations", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(20,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "scripted_observations", width: 90, align: 'center', sorttype: parseInt, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(23,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                        {name: "web_login_counts", width: 70, align: 'center', sorttype: parseInt, search: false, formatter: 'showlink', formatter: function (cellvalue, options, rowObject) {
                                return '<a href=javascript:paramLink(9,' + rowObject.user_id + ',' + rowObject.account_id + ')>' +
                                        cellvalue + "</a>";
                            }},
                                //                    {name: "detail", align: 'center', formatter: 'link', formatter: 'showlink', formatoptions: {baseLinkUrl: 'javascript:', showAction: "Link('", addParam: "');"}, width: 90, search: false}
                    ];
                }
        <?php
    endif;
endif;
?>

    });
    var request_type = '<?php echo $request_type; ?>';
    if (request_type == true) {
        $(function () {
            var account_id = '';
            var filter_type = $('#filter_type').val();
            if (filter_type == 'custom') {
                var start_date = $('#start_date').val();
                var end_date = $('#end_date').val();
                account_id = $('#subAccountGrph_custom').val();
            } else {
                var durationFrom = $('#yearFrom').val() + '-' + $('#monthFrom').val();
                account_id = $('#subAccountGrph').val();
            }
            $grid.jqGrid({
                url: "/Analytics/accountUsersAjax",
                datatype: 'json',
                mtype: 'POST',
                postData: {
                    start_date: start_date,
                    end_date: end_date,
                    filter_type: filter_type,
                    startDate: durationFrom,
                    duration: $('#quarterFrom').val(),
                    subAccount: account_id,
                    rows: 0,
                    page: 1
                },
                gridview: true,
                colNames: columns_titles, colModel: columns_data,
                shrinkToFit: false,
                pager: '#pager2',
                rowNum: 10,
                width: 983,
                height: 391,
                sortname: 'account_name',
                pginput: true,
                emptyrecords: 'No records to display',
                jsonReader: {root: "rows",
                    page: "page",
                    repeatitems: false,
                },
                multiselect: false,
                afterSubmit: function (response) {
                    console.log(response);
                }

            });
            $grid.jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false});
            $grid.jqGrid('navGrid', '#pager2', {refresh: false, search: false, view: false, del: false, add: false, edit: false, excel: true})
                    .navButtonAdd('#pager2', {
                        caption: "&nbsp;<img src='<?php echo $this->webroot . "img/icons/excel_icon.png"; ?>' /> Export to Excel",
                        buttonicon: "ui-icon-save",
                        onClickButton: function () {
                            exportExcel();
                        },
                        position: "last"
                    });

        });
    }

    function exportExcel() {
        var filter_type = $('#filter_type').val();
        if (filter_type == 'custom') {
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
           var subAccount = $('#subAccountGrph_custom').val();
        } else {
            var durationFrom = $('#yearFrom').val() + '-' + $('#monthFrom').val();
            subAccount = $('#subAccountGrph').val();
        }
        var library_check = <?php echo (!empty($this->Custom->get_account_video_permissions($account_id))) ? $this->Custom->get_account_video_permissions($account_id) : 0; ?>;
        var mya = new Array();

        $.ajax({
            url: "/Analytics/accountUsersAjax",
            dataType: 'json',
            type: 'POST',
            async: "false",
            data: {
                rows: 'export',
                page: 20000,
                start_date: start_date,
                end_date: end_date, filter_type: filter_type,
                startDate: durationFrom,
                duration: $('#quarterFrom').val(),
                subAccount:subAccount
            },
            success: function (res) {
                myData = res.rows;
                mya = $("#grid").getDataIDs(); // Get All IDs
                var data = $("#grid").getRowData(mya[0]); // Get First row to get the labels
                var colNames = new Array();
                var ii = 0;
                for (var i in data) {
                    colNames[ii++] = i;
                }    // capture col names
                var html = "";
                if (library_check) {
                    for (k = 0; k < 1; k++)
                    {
                        html = html + 'Name' + '|' + "\t";
                        html = html + 'Email' + '|' + "\t";
                        html = html + 'Account Name' + '|' + "\t";
                        html = html + 'User Role' + '|' + "\t";
                        html = html + 'User Type' + '|' + "\t";
                        html = html + 'Videos Uploaded to Workspace' + '|' + "\t";
                        html = html + 'Videos Uploaded to Huddles' + '|' + "\t";
                        html = html + 'Videos Shared to Huddles' + '|' + "\t";
                        html = html + 'Videos Uploaded to Video Library' + '|' + "\t";
                        html = html + 'Videos Shared to Video Library' + '|' + "\t";
                        html = html + 'Workspace Video Views' + '|' + "\t";
                        html = html + 'Huddle Video Views' + '|' + "\t";
                        html = html + 'Library Video Views' + '|' + "\t";
                        html = html + 'Total Videos Hours Uploaded' + '|' + "\t";
                        html = html + 'Total Videos Hours Viewed' + '|' + "\t";
                        //   html = html + 'Videos Viewed' + '|' + "\t";
                        html = html + 'Huddles Created' + '|' + "\t";
                        html = html + 'Workspace Video Notes' + '|' + "\t";
                        html = html + 'Huddle Video Comments' + '|' + "\t";
                        html = html + 'Huddle Video Replies' + '|' + "\t"; //   html = html + 'Total Video Comments' + '|' + "\t";
                        html = html + 'Resources Uploaded' + '|' + "\t";
                        html = html + 'Resources Viewed' + '|' + "\t";
                        html = html + 'Scripted Video Observations' + '|' + "\t";
                        html = html + 'Scripted Observations' + '|' + "\t";
                        html = html + 'Total Logins' + '|' + "\t";
                    }

                    html = html + "\t\r";
                    for (var i = 0; i < myData.length; i++) {
                        html = html + $.trim(myData[i].user_name) + '|' + "\t";
                        html = html + $.trim(myData[i].email) + '|' + "\t";
                        html = html + $.trim(myData[i].company_name) + '|' + "\t";
                        html = html + $.trim(myData[i].user_role) + '|' + "\t";
                        html = html + $.trim(myData[i].user_type) + '|' + "\t";
                        html = html + $.trim(myData[i].workspace_upload_counts) + '|' + "\t";
                        html = html + $.trim(myData[i].video_upload_counts) + '|' + "\t";
                        html = html + $.trim(myData[i].shared_upload_counts) + '|' + "\t";
                        //    html = html + $.trim(myData[i].videos_viewed_count) + '|' + "\t";
                        html = html + $.trim(myData[i].library_upload_counts) + '|' + "\t";
                        html = html + $.trim(myData[i].library_shared_upload_counts) + '|' + "\t";
                        html = html + $.trim(myData[i].workspace_videos_viewed_count) + '|' + "\t";
                        html = html + $.trim(myData[i].videos_viewed_count) + '|' + "\t";
                        html = html + $.trim(myData[i].library_videos_viewed_count) + '|' + "\t";
                        html = html + $.trim(myData[i].total_hours_uploaded) + '|' + "\t";
                        html = html + $.trim(myData[i].total_hours_viewed) + '|' + "\t";
                        html = html + $.trim(myData[i].huddle_created_count) + '|' + "\t";
                        html = html + $.trim(myData[i].workspace_comments_initiated_count) + '|' + "\t";
                        html = html + $.trim(myData[i].comments_initiated_count) + '|' + "\t";
                        html = html + $.trim(myData[i].replies_initiated_count) + '|' + "\t";
                        html = html + $.trim(myData[i].documents_uploaded_count) + '|' + "\t";
                        html = html + $.trim(myData[i].documents_viewed_count) + '|' + "\t";
                        html = html + $.trim(myData[i].scripted_video_observations) + '|' + "\t";
                        html = html + $.trim(myData[i].scripted_observations) + '|' + "\t";
                        html = html + $.trim(myData[i].web_login_counts) + '|' + "\t\r";
                    }
                }

                else {

                    for (k = 0; k < 1; k++)
                    {
                        html = html + 'Name' + '|' + "\t";
                        html = html + 'Email' + '|' + "\t";
                        html = html + 'Account Name' + '|' + "\t";
                        html = html + 'User Role' + '|' + "\t";
                        html = html + 'User Type' + '|' + "\t";
                        html = html + 'Videos Uploaded to Workspace' + '|' + "\t";
                        html = html + 'Videos Uploaded to Huddles' + '|' + "\t";
                        html = html + 'Videos Shared to Huddles' + '|' + "\t";
                        //      html = html + 'Videos Uploaded to Video Library' + '|' + "\t";
                        //      html = html + 'Videos Shared to Video Library' + '|' + "\t";
                        html = html + 'Workspace Video Views' + '|' + "\t";
                        html = html + 'Huddle Video Views' + '|' + "\t";
                        //      html = html + 'Library Video Views' + '|' + "\t";

                        //   html = html + 'Videos Viewed' + '|' + "\t";                 html = html + 'Total Videos Hours Uploaded' + '|' + "\t";
                        html = html + 'Total Videos Hours Uploaded' + '|' + "\t";
                        html = html + 'Total Videos Hours Viewed' + '|' + "\t";
                        html = html + 'Huddles Created' + '|' + "\t";
                        html = html + 'Workspace Video Notes' + '|' + "\t";
                        html = html + 'Huddle Video Comments' + '|' + "\t";
                        html = html + 'Huddle Video Replies' + '|' + "\t"; //   html = html + 'Total Video Comments' + '|' + "\t";
                        html = html + 'Resources Uploaded' + '|' + "\t";
                        html = html + 'Resources Viewed' + '|' + "\t";
                        html = html + 'Scripted Video Observations' + '|' + "\t";
                        html = html + 'Scripted Observations' + '|' + "\t";
                        html = html + 'Total Logins' + '|' + "\t";
                    }

                    html = html + "\t\r";
                    for (var i = 0; i < myData.length; i++) {
                        html = html + $.trim(myData[i].user_name) + '|' + "\t";
                        html = html + $.trim(myData[i].email) + '|' + "\t";
                        html = html + $.trim(myData[i].company_name) + '|' + "\t";
                        html = html + $.trim(myData[i].user_role) + '|' + "\t";
                        html = html + $.trim(myData[i].user_type) + '|' + "\t";
                        html = html + $.trim(myData[i].workspace_upload_counts) + '|' + "\t";
                        html = html + $.trim(myData[i].video_upload_counts) + '|' + "\t";
                        html = html + $.trim(myData[i].shared_upload_counts) + '|' + "\t";
                        //    html = html + $.trim(myData[i].videos_viewed_count) + '|' + "\t";
                        //     html = html + $.trim(myData[i].library_upload_counts) + '|' + "\t";
                        //    html = html + $.trim(myData[i].library_shared_upload_counts) + '|' + "\t";
                        html = html + $.trim(myData[i].workspace_videos_viewed_count) + '|' + "\t";
                        html = html + $.trim(myData[i].videos_viewed_count) + '|' + "\t";
                        //   html = html + $.trim(myData[i].library_videos_viewed_count) + '|' + "\t";
                        html = html + $.trim(myData[i].total_hours_uploaded) + '|' + "\t";
                        html = html + $.trim(myData[i].total_hours_viewed) + '|' + "\t";
                        html = html + $.trim(myData[i].huddle_created_count) + '|' + "\t";
                        html = html + $.trim(myData[i].workspace_comments_initiated_count) + '|' + "\t";
                        html = html + $.trim(myData[i].comments_initiated_count) + '|' + "\t";
                        html = html + $.trim(myData[i].replies_initiated_count) + '|' + "\t";
                        html = html + $.trim(myData[i].documents_uploaded_count) + '|' + "\t";
                        html = html + $.trim(myData[i].documents_viewed_count) + '|' + "\t";
                        html = html + $.trim(myData[i].scripted_video_observations) + '|' + "\t";
                        html = html + $.trim(myData[i].scripted_observations) + '|' + "\t";
                        html = html + $.trim(myData[i].web_login_counts) + '|' + "\t\r";
                    }
                }
                html = html + "\t\r"; // end of line at the end
                document.forms['frmExcelExportAnalytics'].csvBuffer.value = html;
                var acc_name = $('#subAccountUsers option:selected').text();
                document.forms['frmExcelExportAnalytics'].acctName.value = acc_name;
                document.forms['frmExcelExportAnalytics'].method = 'POST';
                if (library_check) {
                    document.forms['frmExcelExportAnalytics'].action = '<?php echo $this->base . "/Dashboard/excelExport/24"; ?>'; // send it to server which will open this contents in excel file
                }
                else
                {
                    document.forms['frmExcelExportAnalytics'].action = '<?php echo $this->base . "/Dashboard/excelExport/21"; ?>'; // send it to server which will open this contents in excel file
                }
                //document.forms[0].target='_blank';
                document.forms['frmExcelExportAnalytics'].submit();
            }
        });
    }
    function Link(id) {
        var row = id.split("=");
        var row_ID = row[1];
        var userid = $("#grid").getCell(row_ID, 'user_id');
        var subAccount = $("#grid").getCell(row_ID, 'account_id');
        if (subAccount == '')
            subAccount = 0;
//        var re = /\//gi;
//        var stdate = $('#fromUsr').val();
//        var startDate = stdate.replace(re, '-');
//        var edate = $('#toUsr').val();
//        var endDate = edate.replace(re, '-');

        var startDate = $('#f_start_date').val();
        var endDate = $('#f_end_dates').val();
        var url = "<?php echo $this->base . '/dashboard/userGraph/'; ?>" + userid + "/" + subAccount + "/" + startDate + "/" + endDate + "/3";
        window.open(url);
    }
    function paramLink(type, userid, account_id, workspace_huddle_library = 0) {

        var subAccount = account_id;
        if (subAccount == '')
            subAccount = 0;
        var startDate = $('#f_start_date').val();
        var endDate = $('#f_end_dates').val();
        var url = "<?php echo $this->base . '/dashboard/userDetail/'; ?>" + userid + "/" + subAccount + "/" + startDate + "/" + endDate + "/" + type + "/" + workspace_huddle_library;
        window.location = (url);
    }
</script>