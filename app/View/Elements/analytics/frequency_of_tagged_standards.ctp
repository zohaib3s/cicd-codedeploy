<?php
$captions = '';
if ($this->Custom->is_enabled_framework_and_standards($account_id)) {
    $captions = 'Frequency of Tagged Standards ';
} else {
    $captions = 'Frequency of Custom Tags';
}
?>
<div class="am_chart1" style=" max-height: 400px;">
    <?php if ($standarads_tag != '[]'): ?>
        <script>

            var chart = AmCharts.makeChart("chartdiv", {
                "type": "pie",
                "dataProvider": <?php echo $standarads_tag ?>,
                "titleField": "country",
                "path": "/amcharts/",
                "valueField": "litres",
                "colorField": "color",
                "startEffect": 'easeInSine',
                "startDuration": 0.5,
                caption: "<?php echo $captions; ?>",
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "labelsEnabled": true,
                "legend": {
                    "valueText": "",
                    "fontSize": 10,
                    "truncateLabels": 20,
                    "labelText": "[[title]]"
                },
                "labelText": "[[percents]]%",
                "marginTop": 0,
                "marginBottom": 0,
                "export": {
                    "enabled": true,
                    "menu": [{
                            "class": "export-main",
                            "menu": ["PNG", "JPG", "SVG", "PDF"]
                        }]
                },
                "allLabels": [{
                        "text": "<?php echo $captions; ?>",
                        "align": "center",
                        "bold": false,
                        "size": 15,
                        "y": 2
                    }],
            });
            AmCharts.addInitHandler(function (chart) {
                if (chart.legend === undefined || chart.legend.truncateLabels === undefined)
                    return;

                // init fields
                var titleField = chart.titleField;
                var legendTitleField = chart.titleField + "Legend";

                // iterate through the data and create truncated label properties
                for (var i = 0; i < chart.dataProvider.length; i++) {
                    var label = chart.dataProvider[i][chart.titleField];
                    if (label && label.length && label.length > chart.legend.truncateLabels)
                        label = label.substr(0, chart.legend.truncateLabels - 1) + '...'
                    chart.dataProvider[i][legendTitleField] = label;
                }
                // replace chart.titleField to show our own truncated field
                chart.titleField = legendTitleField;

                // make the balloonText use full title instead
                chart.balloonText = chart.balloonText.replace(/\[\[title\]\]/, "[[" + titleField + "]]");

            }, ["pie"]);

        </script>

    </script>
    <div id="chartdiv" style="width: 100%; height: 400px;"></div>
<?php else: ?>
    <div style="padding: 12px; font-size: 15px;">
        <h4 style="font-size: 15px;font-weight: 600; text-align: center"><?php echo $captions; ?></h4>
    </div>
    <div  style="width: 100%; height: 400px; text-align: center;">
        No Tagged Standard Found!
    </div>
<?php endif; ?>
</div>
