<div class="analytic_overview">
    <h3 id="company_name_overview">Overview</h3>
    <div class="over_datecls">
        <p><span class="filter_date"><?php echo $filter_date; ?></span></p>
    </div>
    <?php if (isset($is_single) && $is_single): ?>
        <div class="analytic_counts">


            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analytichuddle_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_account_huddles; ?></h4>
                    <h5>Huddles</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticplay_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_account_videos; ?></h4>
                    <h5>Total Videos Uploaded</h5>
                </div>
                <div class="clear"></div>
            </div>

            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticcomments_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_comments_added; ?></h4>
                    <h5>Comments Added</h5>
                </div>
                <div class="clear"></div>
            </div>

            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticvideo_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_viewed_videos; ?></h4>
                    <h5>Videos Viewed</h5>
                </div>
                <div class="clear"></div>
            </div>

            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticupload_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_video_duration; ?></h4>
                    <h5>Video Hours Uploaded</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticvideo_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_min_watched ?></h4>
                    <h5>Video Hours Viewed</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    <?php else: ?>
        <div class="analytic_counts">
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/tot_users_img.jpg' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_account_users; ?></h4>
                    <h5>Total Users</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/tot_users_img.jpg' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $active_users; ?></h4>
                    <h5>Active Users</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analytichuddle_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_account_huddles; ?></h4>
                    <h5>Huddles</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticplay_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_account_videos; ?></h4>
                    <h5>Total Videos Uploaded</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticcomments_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_comments_added; ?></h4>
                    <h5>Comments Added</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticvideo_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_viewed_videos; ?></h4>
                    <h5>Video Views</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticupload_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_video_duration; ?></h4>
                    <h5>Video Hours Uploaded</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="analytic_countsbox">
                <div class="anbox1">
                    <img src="<?php echo $this->webroot . 'img/analyticvideo_icon.png' ?>" />
                </div>
                <div class="anbox2">
                    <h4><?php echo $total_min_watched ?></h4>
                    <h5>Video Hours Viewed</h5>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    <?php endif; ?>
</div>
<?php
//if ($folder_type == 1) {
//    $huddle_title = $total_account_huddles . ' Collaboration Huddle Video Sessions';
//} elseif ($folder_type == 2) {
//    $huddle_title = $total_account_huddles . ' Coaching Huddle Video Sessions';
//} else {
//    $huddle_title = $total_account_huddles . ' Assessment Huddle Video Sessions';
//}
?>
<script type="text/javascript">
//    $(document).ready(function (e) {
//        $('#total_huddles_box_title').text('<?php echo $huddle_title; ?>');
//        $('.filter_date').text('<?php echo $filter_date; ?>');
//    })

</script>