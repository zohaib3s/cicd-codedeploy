<?php
$users = $this->Session->read('user_current_account');
//$account_id = $this->Session->read('account_id');
$account_id = $users['accounts']['account_id'];
//echo $account_id;
?>


<style>
    #chart_<?php echo $count; ?> {
        width		: 100%;
        height		: 220px;
        font-size	: 11px;
    }
    #chart_<?php echo $count; ?> .amcharts-export-menu-top-right{
        top: -34px !important;
    }
</style>
<div class="am_chart2" style="">

    <script>

        var folder_type = '<?php echo $folder_type; ?>';
        var coaching_perfomance_check = '<?php echo $this->Custom->coaching_perfomance_level_without_role($account_id); ?>';
//        alert(coaching_perfomance_check);
        if (folder_type == '3' || (folder_type == '2' && coaching_perfomance_check == '1'))
        {
            var chart_1 = AmCharts.makeChart("chart_<?php echo $count; ?>", {
                "type": "serial",
                "path": "/amcharts/",
                "theme": "light",
                "categoryField": "date",
                "rotate": false,
                "marginTop": 10,
                "marginBottom": 10,
                "startEffect": 'easeInSine',
                "startDuration": 0.5,
                "autoMargins": true,
                "categoryAxis": {
                    "gridPosition": "start",
                    "position": "left",
                },
                "legend": {
                    "equalWidths": false,
                    "autoMargins": false,
                    "position": "top",
                    "valueAlign": "left",
                    "markerType": "square",
                    "valueWidth": 2,
                    "verticalGap": 2,
                    divId: "legenddiv_<?php echo $count; ?>",
                    "data": [{
                            "title": "<?php echo $title . ': ' . $total_tagged_standards; ?>",
                            "color": "<?php echo $color; ?>"
                        },
                        {
                            "title": "<?php echo $ratting_title . ': ' . $total_avg; ?>",
                            "color": "<?php echo $color_rating; ?>"
                        }
                    ]
                },
                "graphs": [
                    {
                        "valueAxis": "ValueAxis-<?php echo $count; ?>",
                        "colorField": "color",
                        "fixedColumnWidth": 10,
                        "balloonText": "<?php echo $title; ?>:[[value]]",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-<?php echo $count; ?>",
                        "lineAlpha": 0.2,
                        "title": "<?php echo $title; ?>",
//                        "colors": "<?php echo $color; ?>",
                        "type": "column",
//                        "lineColor": "[[color]]",
                        "valueField": "total_tags"
                    },
                    {
                        "valueAxis": "ValueAxis-r",
                        "colorField": "color_rating",
                        "balloonText": "[[average_rating_name]]:[[value]]",
                        "fixedColumnWidth": 10,
                        "fillAlphas": 0.8,
                        "lineAlpha": 0.2,
                        "title": "Performance Level",
                        "type": "column",
//                        "lineColor": "color_rating",
                        "valueField": "standard_rating_avg",
                    }
                ],
                "guides": [
                ],
                "valueAxes": [
                    {
                        "id": "ValueAxis-<?php echo $count; ?>",
                        "position": "left",
                        "axisAlpha": 1,
                        "title": "Number of Standard Tags",
                        "titleBold": false,
                        "titleFontSize": "10",
                    },
                    {
                        "id": "ValueAxis-r",
                        "axisAlpha": 1,
                        "autoGridCount": true,
                        "gridCount": 6,
                        "integersOnly": true,
                        "dashLength": 1,
                        "position": "right",
                        /*"showLastLabel": true,
                         "showFirstLabel": true,*/
                        //"labelFrequency": 5,
                        "minimum": 0,
                        "maximum": 6,
                        "valueText": "standard_rating_avg",
                        "labelFunction": formatValue,
                        "title": "Performance Level",
                        "titleBold": false,
                        "titleFontSize": "10",
                        "minVerticalGap": 1,
                    }

                ],
                "dataProvider":<?php echo $data_provider; ?>,
                "export": {
                    "enabled": true,
                    "menu": [{
                            "class": "export-main",
                            "menu": ["PNG", "JPG", "SVG", "PDF"]
                        }]
                },
            });

        }

        else
        {

            var chart_1 = AmCharts.makeChart("chart_<?php echo $count; ?>", {
                "type": "serial",
                "path": "/amcharts/",
                "theme": "light",
                "categoryField": "date",
                "rotate": false,
                "marginTop": 10,
                "marginBottom": 10,
                "startEffect": 'easeInSine',
                "startDuration": 0.5,
                "autoMargins": true,
                "categoryAxis": {
                    "gridPosition": "start",
                    "position": "left",
                },
                "legend": {
                    "equalWidths": false,
                    "autoMargins": false,
                    "position": "top",
                    "valueAlign": "left",
                    "markerType": "square",
                    "valueWidth": 2,
                    "verticalGap": 2,
                    divId: "legenddiv_<?php echo $count; ?>",
                    "data": [{
                            "title": "<?php echo $title . ': ' . $total_tagged_standards; ?>",
                            "color": "<?php echo $color; ?>"
                        }
                    ]
                },
                "graphs": [
                    {
                        "valueAxis": "ValueAxis-<?php echo $count; ?>",
                        "colorField": "color",
                        "fixedColumnWidth": 10,
                        "balloonText": "<?php echo $title; ?>:[[value]]",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-<?php echo $count; ?>",
                        "lineAlpha": 0.2,
                        "title": "<?php echo $title; ?>",
//                        "colors": "<?php echo $color; ?>",
                        "type": "column",
//                        "lineColor": "[[color]]",
                        "valueField": "total_tags"
                    },
                ],
                "guides": [
                ],
                "valueAxes": [
                    {
                        "id": "ValueAxis-<?php echo $count; ?>",
                        "position": "left",
                        "axisAlpha": 1,
                        "title": "Number of Standard Tags",
                        "titleBold": false,
                        "titleFontSize": "10",
                    },
                ],
                "dataProvider":<?php echo $data_provider; ?>,
                "export": {
                    "enabled": true,
                    "menu": [{
                            "class": "export-main",
                            "menu": ["PNG", "JPG", "SVG", "PDF"]
                        }]
                },
            });


        }
    </script>

    <!--    <div style="padding: 12px; font-size: 15px;">
            <h5 style="font-size:16px;font-weight: 400;"><?php echo $title; ?></h5>
        </div>-->
    <div id="legenddiv_<?php echo $count; ?>" style="margin: 0px 0px 2px 38px; position: relative;height: 38px;"></div>

    <div id="chart_<?php echo $count; ?>"></div>
</div>