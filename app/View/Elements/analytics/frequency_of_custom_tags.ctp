<div class="am_chart1" style="max-height: 400px;">

    <?php if ($custom_markers_tag != '[]'): ?>
        <script>

            var chart = AmCharts.makeChart("custom_markers_tags", {
                "type": "pie",
                "dataProvider": <?php echo $custom_markers_tag ?>,
                "path": "/amcharts/",
                "titleField": "country",
                "valueField": "litres",
                "colorField": "color",
                "startEffect": 'easeInSine',
                "startDuration": 0.5,
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "labelsEnabled": true,
                "legend": {
                    "valueText": "",
                    "fontSize": 10,
    //                    "truncateLabels": 20,
                    "labelText": "[[title]]"
                },
                "labelText": "[[percents]]%",
                "marginTop": 0,
                "marginBottom": 0,
                "export": {
                    "enabled": true,
                    "debug": false,
                    "menu": [{
                            "class": "export-main",
                            "menu": ["PNG", "JPG", "SVG", "PDF"]
                        }]
                },
                "allLabels": [{
                        "text": "Frequency of Custom Marker Tags",
                        "align": "center",
                        "bold": false,
                        "size": 15,
                        "y": 5
                    }],
            });


        </script>
        <div id="custom_markers_tags" style="width: 100%; height: 400px;">

        </div>
    <?php else: ?>
        <div style="padding: 12px; font-size: 15px;">
            <h4 style="font-size: 15px;font-weight: 600; text-align: center">Frequency of Custom Marker Tags</h4>
        </div>
        <div id="custom_markers_tags" style="width: 100%; height: 400px; text-align: center;">
            No Custom Markers Found!
        </div>
    <?php endif; ?>
</div>