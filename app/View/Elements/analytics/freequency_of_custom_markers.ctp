<div class="am_chart2">
    <!-- Styles -->
    <style>
        #chart_1 {
            width		: 100%;
            height		: 500px;
            font-size	: 11px;
        }
        #chart_2 {
            width		: 100%;
            height		: 500px;
            font-size	: 11px;
        }
        #chart_3 {
            width		: 100%;
            height		: 500px;
            font-size	: 11px;
        }
        #chart_4 {
            width		: 100%;
            height		: 500px;
            font-size	: 11px;
        }
        #chart_5 {
            width		: 100%;
            height		: 500px;
            font-size	: 11px;
        }
    </style>

    <script>

        var chart_1 = AmCharts.makeChart("chart_1", {
            "type": "serial",
            "path": "/amcharts/",
            "theme": "light",
            "categoryField": "year",
            "rotate": false,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left",
            },
            "trendLines": [],
            "graphs": [
                {
                    "fixedColumnWidth": 10,
                    "balloonText": "Income:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income"
                },
                {
                    "balloonText": "Expenses:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "PEDAGOGY",
                    "type": "column",
                    "valueField": "expenses"
                }
            ],
            "guides": [
            ],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [
                {
                    "year": 'Jan 2017',
                    "income": 5,
//                    "expenses": 18.1
                },
                {
                    "year": 'Feb 2017',
                    "income": 3,
//                    "expenses": 6
                },
                {
                    "year": 'Mar 2017',
                    "income": 4,
//                    "expenses": 5
                },
                {
                    "year": 'Apr 2017',
                    "income": 6,
//                    "expenses": 4
                },
                {
                    "year": 'May 2017',
                    "income": 4,
//                    "expenses": 3
                },
                {
                    "year": 'Jun 2017',
                    "income": 3,
//                    "expenses": 3
                },
                {
                    "year": 'Jul 2017',
                    "income": 2,
//                    "expenses": 4
                }
            ],
            "export": {
                "enabled": true
            }

        });
        var chart_2 = AmCharts.makeChart("chart_2", {
            "type": "serial",
            "theme": "light",
            "path": "/amcharts/",
            "categoryField": "year",
            "rotate": false,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "bottom",
                "position": "left",
                "autoRotateAngle": 90,
            },
            "trendLines": [],
            "graphs": [
                {
                    "fixedColumnWidth": 10,
                    "balloonText": "Income:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income"
                },
                {
                    "balloonText": "Expenses:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "VISION",
                    "type": "column",
                    "valueField": "expenses"
                }
            ],
            "guides": [
            ],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [
                {
                    "year": 'Jan 2017',
                    "income": 5,
//                    "expenses": 18.1
                },
                {
                    "year": 'Feb 2017',
                    "income": 3,
//                    "expenses": 6
                },
                {
                    "year": 'Mar 2017',
                    "income": 4,
//                    "expenses": 5
                },
                {
                    "year": 'Apr 2017',
                    "income": 6,
//                    "expenses": 4
                },
                {
                    "year": 'May 2017',
                    "income": 4,
//                    "expenses": 3
                },
                {
                    "year": 'Jun 2017',
                    "income": 3,
//                    "expenses": 3
                },
                {
                    "year": 'Jul 2017',
                    "income": 2,
//                    "expenses": 4
                }
            ],
            "export": {
                "enabled": true
            }

        });
        var chart_3 = AmCharts.makeChart("chart_3", {
            "type": "serial",
            "theme": "light",
            "path": "/amcharts/",
            "categoryField": "year",
            "rotate": false,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "bottom",
                "position": "left",
                "autoRotateAngle": 90,
            },
            "trendLines": [],
            "graphs": [
                {
                    "fixedColumnWidth": 10,
                    "balloonText": "Income:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income"
                },
                {
                    "balloonText": "Expenses:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "PEDAGOGY",
                    "type": "column",
                    "valueField": "expenses"
                }
            ],
            "guides": [
            ],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [
                {
                    "year": 'Jan 2017',
                    "income": 5,
//                    "expenses": 18.1
                },
                {
                    "year": 'Feb 2017',
                    "income": 3,
//                    "expenses": 6
                },
                {
                    "year": 'Mar 2017',
                    "income": 4,
//                    "expenses": 5
                },
                {
                    "year": 'Apr 2017',
                    "income": 6,
//                    "expenses": 4
                },
                {
                    "year": 'May 2017',
                    "income": 4,
//                    "expenses": 3
                },
                {
                    "year": 'Jun 2017',
                    "income": 3,
//                    "expenses": 3
                },
                {
                    "year": 'Jul 2017',
                    "income": 2,
//                    "expenses": 4
                }
            ],
            "export": {
                "enabled": true
            }

        });
        var chart_4 = AmCharts.makeChart("chart_4", {
            "type": "serial",
            "theme": "light",
            "path": "/amcharts/",
            "categoryField": "year",
            "rotate": false,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "bottom",
                "position": "left",
                "autoRotateAngle": 90,
            },
            "trendLines": [],
            "graphs": [
                {
                    "fixedColumnWidth": 10,
                    "balloonText": "Income:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income"
                },
                {
                    "balloonText": "Expenses:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0,
                    "title": "VISION",
                    "type": "column",
                    "valueField": "expenses"
                }
            ],
            "guides": [
            ],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [
                {
                    "year": 'Jan 2017',
                    "income": 5,
//                    "expenses": 18.1
                },
                {
                    "year": 'Feb 2017',
                    "income": 3,
//                    "expenses": 6
                },
                {
                    "year": 'Mar 2017',
                    "income": 4,
//                    "expenses": 5
                },
                {
                    "year": 'Apr 2017',
                    "income": 6,
//                    "expenses": 4
                },
                {
                    "year": 'May 2017',
                    "income": 4,
//                    "expenses": 3
                },
                {
                    "year": 'Jun 2017',
                    "income": 3,
//                    "expenses": 3
                },
                {
                    "year": 'Jul 2017',
                    "income": 2,
//                    "expenses": 4
                }
            ],
            "export": {
                "enabled": true
            }

        });
        var chart_5 = AmCharts.makeChart("chart_5", {
            "type": "serial",
            "theme": "light",
            "path": "/amcharts/",
            "categoryField": "year",
            "rotate": false,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "bottom",
                "position": "left",
            },
            "trendLines": [],
            "graphs": [
                {
                    "fixedColumnWidth": 10,
                    "balloonText": "Income:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income"
                },
                {
                    "balloonText": "Expenses:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "PEDAGOGY",
                    "type": "column",
                    "valueField": "expenses"
                }
            ],
            "guides": [
            ],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [
                {
                    "year": 'Jan 2017',
                    "income": 5,
//                    "expenses": 18.1
                },
                {
                    "year": 'Feb 2017',
                    "income": 3,
//                    "expenses": 6
                },
                {
                    "year": 'Mar 2017',
                    "income": 4,
//                    "expenses": 5
                },
                {
                    "year": 'Apr 2017',
                    "income": 6,
//                    "expenses": 4
                },
                {
                    "year": 'May 2017',
                    "income": 4,
//                    "expenses": 3
                },
                {
                    "year": 'Jun 2017',
                    "income": 3,
//                    "expenses": 3
                },
                {
                    "year": 'Jul 2017',
                    "income": 2,
//                    "expenses": 4
                }
            ],
            "export": {
                "enabled": true
            }

        });
    </script>

    <!-- HTML -->
    <div style="padding: 12px; font-size: 15px;">
        <h4 style="font-size: 24px;font-weight: 400;">Frequency</h4>
    </div>
    <div style="padding: 12px; font-size: 15px;">
        <h5 style="font-size:16px;font-weight: 400;">PEDAGOGY</h5>
    </div>
    <div id="chart_1"></div>
    <div style="padding: 12px; font-size: 15px;">
        <h5 style="font-size:16px;font-weight: 400;">VISION</h5>
    </div>
    <div id="chart_2"></div>
    <div style="padding: 12px; font-size: 15px;">
        <h4 style="font-size:16px;font-weight: 400;">PEDAGOGY</h4>
    </div>
    <div id="chart_3"></div>
    <div style="padding: 12px; font-size: 15px;">
        <h4 style="font-size:16px;font-weight: 400;">VISION</h4>
    </div>
    <div id="chart_4"></div>
    <div style="padding: 12px; font-size: 15px;">
        <h4 style="font-size:16px;font-weight: 400;">PEDAGOGY</h4>
    </div>
    <div id="chart_5"></div>
</div>