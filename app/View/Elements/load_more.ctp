<?php $language_based_content = $this->Custom->get_page_lang_based_content('load_more'); ?>

<?php if (isset($load_more_what) && $load_more_what == 'observations'): ?>
    <?php
    if ($current_page * $number_per_page < $total_items): ?>
        <div class="load_more" id="load_more_observations">
            <a class="btn btn-orange" href="javascript:loadMoreObservations();"><?php echo $language_based_content['load_more_observation']; ?></a>
        </div>
    <?php endif; ?>
<?php elseif(isset($load_more_what) && $load_more_what == 'huddles'): ?>
<?php
    if ($current_page * $number_per_page < $total_items): ?>
        <div class="load_more" id="load_more_huddles">
            <a id="load_more_button" class="btn btn-green" href="javascript:loadMoreObservations();"><?php echo $language_based_content['load_more_huddles']; ?></a>
        </div>
    <?php endif; ?>
<?php else: ?>
    <?php if ($current_page * $number_per_page < $total_items): ?>
        <div class="load_more">
            <a id="load_more_videos" class="btn btn-green" href="javascript:loadMoreVideos();"><?php echo $language_based_content['load_more_videos']; ?></a>
        </div>
    <?php endif; ?>
<?php endif; ?>
