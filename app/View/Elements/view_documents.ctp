 <head>
<title><?php echo $this->Custom->get_site_settings('site_title'); ?></title>
<!--<script src="https://api.filestackapi.com/filestack.js"></script>-->
<script src="//static.filestackapi.com/v3/filestack.js"></script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<div id="file_viewer_myfiles" class="modal in" role="dialog" aria-hidden="true"  style="width:100%;height:100%;overflow:hidden;z-index:200000;">
    <div class="modal-dialog"  style="width:100%;height:100%;margin:0px;">
        <div class="modal-content" style="width:100%;height:100%;border:0px;box-shadow:none;">
        <?php if (isset($_SESSION['site_id']) && $_SESSION['site_id'] != 1) { ?>
            
            <iframe style="overflow:hidden;" frameBorder="0" src="https://cdn.filestackcontent.com/preview/<?php echo $url_code ?>?css=<?php echo Configure::read('sibme_base_url'); ?>/css/filestack-hmh.css" name="myFrame" width="100%" height="100%"></iframe>
        <?php }else{ ?>
            <iframe style="overflow:hidden;" frameBorder="0" src="https://cdn.filestackcontent.com/preview/<?php echo $url_code ?>?css=<?php echo Configure::read('sibme_base_url'); ?>/css/filestack-sibme.css" name="myFrame" width="100%" height="100%"></iframe>
        <?php }?>
        </div>
        <div style="clear: both"></div>
    </div> 
    <div style="clear: both"></div>
</div>
<script>

$(document).ready(function(e){
   $('#file_viewer_myfiles').modal('show');
});
</script>
<!--<iframe style="overflow:hidden;" frameBorder="0" src="http://www.filestackapi.com/api/preview/<?php echo $url_code ?>?css=http://<?php echo $_SERVER['HTTP_HOST']; ?>/css/filestack.css" name="myFrame" width="100%" height="100%"></iframe>-->