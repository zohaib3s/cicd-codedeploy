
<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$account_id = $user_current_account['accounts']['account_id'];
$btn_1_color = $this->Custom->get_site_settings('primary_bg_color');
$btn_2_color = $this->Custom->get_site_settings('secondary_bg_color');
?>
<div id="showflashmessage"></div>
<?php if (($user_current_account['roles']['role_id'] != '110' && $user_current_account['roles']['role_id'] != '120' && $user_current_account['roles']['role_id'] != '125') || $user_permissions['UserAccount']['manage_collab_huddles'] == '1' || $user_permissions['UserAccount']['manage_coach_huddles'] == '1' || $user_permissions['UserAccount']['manage_evaluation_huddles'] == '1'): ?>

    <a id="btn-new-huddle" href="<?php echo $this->base . '/Huddles/add' ?>" class="btn btn-green right"><?php echo $new_huddle_button ? $new_huddle_button : ''; ?> </a>
<?php endif; ?>

<?php if (( ($user_current_account['roles']['role_id'] == '120' && $user_permissions['UserAccount']['folders_check'] == '1' ) || ($user_current_account['roles']['role_id'] == '110') || ($user_current_account['roles']['role_id'] == '100' ) || ($user_current_account['roles']['role_id'] == '115' ))): ?>
    <a id="btn-new-huddle" data-toggle="modal" data-target="#createfolder" style="margin-right:5px;background-color: <?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color ?>;" href="#<?php //echo $this->base . '/Folder/create'                   ?>" class="btn btn-orange right">New Folder </a>
<?php endif; ?>
<div id="createfolder" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog"  style="width: 400px;">
        <div class="modal-content">
            <div class="header" style="padding-left: 25px;padding-bottom: 0px;margin-bottom: 0px;padding-top: 15px;">
                <h4 class="header-title nomargin-vertical smargin-bottom">New Huddle Folder</h4>
                <a id="cross" class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">x</a>
            </div>
            <div class="foldrmn">
                <form method="post" id="new_video_huddle" enctype="multipart/form-data" class="new_video_huddle"
                      action="<?php echo $this->base . '/Folder/create'; ?>" accept-charset="UTF-8">
                    <div id="tabs">

                        <div>
                            <div id="step-1">
                                <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8">
                                </div>

                                <a class="close-reveal-modal"></a>
                                <div class="span5">
                                    <div class="row"  style="margin-bottom: 20px;">
                                        <span class="wiz-step1-radio"><input type="radio" id="collab-huddle" name="type" class="cls_coaching_trig_2" checked="checked" value="1" style="display: none;" checked="checked"></span>
                                        <div style="clear: both;height:10px;"></div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;margin-left:-20px;">
                                        <input name="data[hname]" id="video_folder_name" class="size-big huddle-name "  placeholder="Folder Name" size="30" type="text" style="width: 350px;" required/>
                                        <label id="error" for="video_folder_name" class="error" style="display:none; background-color: none !important;">This field is required.</label>
                                    </div>
                                    <div style="clear: both;"></div>
                                    <div class="row" style="display:none;">
                                        <?php echo $this->Form->textarea('hdescription', array('rows' => '4', 'placeholder' => 'Folder Description (Optional)', 'id' => 'video_huddle_description', 'cols' => '40', 'class' => 'size-big')); ?>
                                    </div>
                                    <?php echo $this->Form->input('created_by', array('value' => $user_id, 'type' => 'hidden', 'id' => 'video_huddle_created_by')); ?>
                                </div>
                                <div style="clear: both;height:10px;"></div>
                                <div class="form-actions" style="text-align: left;margin-top:0px;">
                                    <input id="creatingfolder" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>; border-color:<?php echo $this->Custom->get_site_settings('primary_border_color'); ?>" type="button" value="Create Folder" name="commit" onclick="CreateFolder();" class="btn btn-green" style="background-color: <?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color ?>;"  >
                                    <input id="newfoldercancel" class="btn btn-white" type="reset" value="Cancel" style="font-size: 14px;" onclick="$('#createfolder').modal('hide');"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php
$params = $this->request->pass;

if (isset($this->request->data["txtSearchHuddles"]))
    $huddleSearch = $this->request->data["txtSearchHuddles"];
else if (isset($params[2]))
    $huddleSearch = $params[2];
else
    $huddleSearch = '';
?>

<div id="sort-mode" class="dropdown right mmargin-right" style="width: 200px !important;">
    <ul>
        <li><a href="<?php echo $this->base . '/Huddles/index/' . $view_mode . '/' . $sort . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" title="View all" class="active"><?php
                if ($sort == 'name') {
                    echo 'Title';
                } elseif ($sort == 'date') {
                    echo 'Date Created';
                } elseif ($sort == 'coaching') {
                    echo 'Coaching Huddles';
                } elseif ($sort == 'collaboration') {
                    echo 'Collaboration Huddles';
                } elseif ($sort == 'evaluation') {
                    echo 'Assessment Huddles';
                } elseif ($sort == 'folders' && !empty($folders)) {
                    echo 'Folders';
                } elseif ($sort == 'flat_view') {
                    echo 'All Huddles';
                } else {
                    echo 'Sort';
                }
                ?></a></li>
        <li <?php
        if ($sort == 'name') {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Huddles/index/' . $view_mode . '/name' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Title</a></li>
        <li <?php
        if ($sort == 'date') {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Huddles/index/' . $view_mode . '/date' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Date Created</a></li>
            <?php if ($user_current_account['roles']['role_id'] != '125'): ?>
            <li <?php
            if ($sort == 'coaching' || empty($coach_huddles_dropdown)) {
                echo 'style="display:none;"';
            }
            ?> ><a href="<?php echo $this->base . '/Huddles/index/' . $view_mode . '/coaching' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Coaching Huddles</a></li>
            <?php endif; ?>
        <li <?php
        if ($sort == 'collaboration' || empty($collab_huddles_dropdown)) {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Huddles/index/' . $view_mode . '/collaboration' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Collaboration Huddles</a></li>
            <?php
            if ($this->Custom->check_if_eval_huddle_active($account_id)) {
                ?>
                <?php if ($user_current_account['roles']['role_id'] != '125'): ?>
                <li <?php
                if ($sort == 'evaluation' || empty($eval_huddles_dropdown)) {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Huddles/index/' . $view_mode . '/evaluation' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Assessment Huddles</a></li>
                <?php endif; ?>
            <?php } ?>
        <li <?php
        if ($sort == 'folders' || empty($folders_check_dropdown)) {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Huddles/index/' . $view_mode . '/folders' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Folders</a></li>
        <li <?php
        if ($sort == 'flat_view') {
            echo 'style="display:none;"';
        }
        ?> ><a href="<?php echo $this->base . '/Huddles/index/' . $view_mode . '/flat_view' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">All Huddles</a></li>
    </ul>
</div>

<div id="view" class="mmargin-right right">
    <a href="<?php echo $this->base . '/Huddles/index/list' ?>" class="icon-list <?php
    if ($view_mode == 'list') {
        echo 'active';
    }
    ?>" rel="tooltip" title="List View"></a>
    <a href="<?php echo $this->base . '/Huddles/index/grid' ?>" class="icon-grid <?php
    if ($view_mode != 'list') {
        echo 'active';
    }
    ?>" rel="tooltip" title="Grid View"></a>
</div>
<!--    <div class="box container">
        <div class="header header-huddle">
    <h2 class="title"><?php echo $page_title ? $page_title : ''; ?> <span class="blue_count"><?php echo $total_huddles ?></span></h2>
</div>
        <div>Search</div>
<hr class="style2">
    <div id="new_huddle_listings"></div>
    </div>-->
<div class="search-box12" style="width: 335px;float: right;position: relative;margin-top: 4px;">
    <input type="hidden" id="search-mode" action="" class="btn-search" value="<?php echo $view_mode; ?>"/>
    <input type="button" id="btnHuddleSearch" action="" class="btn-search" value="" style="width:27px;"/>
    <input class="text-input" name="txtSearchHuddles" id="txtSearchHuddles" type="text" value="" placeholder="Search ..." style="margin-right: 25px;"/>
    <span id="clearSearchHuddles"  style="display: none;margin-right: -16px;" class="clear-video-input-box cross-search search-x-huddles">X</span>
</div>
<h1 class="page-title nomargin-top"><?php echo $page_title ? $page_title : ''; ?> <span class="page-title__counter"><?php echo '( ' . $total_huddles . ' )' ?></span></h1>
<hr class="style2">
<style>
    .cross-search{
        margin-left: 6px;
        position: absolute;
        right: 54px;
        width: 327px;
        cursor: pointer;
    }
</style>


<script>
    function CreateFolder() {
        folder_name = $("#video_folder_name").val();
        if (folder_name == '') {
            $("#error").css("display", "inline-block");

//                $('#createfolder').modal('hide');
//           $("#flashMessage").hide();
//           $("#showflashmessage").fadeIn();
//           var show_msg = '<div id="flashMessage" class="message error" style="cursor: pointer;">Please enter folder name.</div>';
//           $("#showflashmessage").prepend(show_msg);
//           $("#txtSearchHuddles:input").val('');
//            doHuddleSearch();
        }
        else {
            $.ajax({url: home_url + '/Folder/create',
                data: {hname: $("#video_folder_name").val(), hdescription: ''},
                type: 'post',
                success: function (output) {
                    $('#createfolder').modal('hide');
                    $("#flashMessage").hide();
                    $("#showflashmessage").fadeIn();
                    if (output == 'folderexists')
                    {
                        var show_msg = '<div id="flashMessage" class="message error" style="cursor: pointer;">A folder with same name already exist, please try another name. You might not see the folder with name "' + folder_name + '" because you are not participating in that</div>';
                    }
                    else {
                        var show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">' + folder_name + ' has been saved successfully.</div>';
                        $("#sort-mode").html(output);
                    }
                    $("#showflashmessage").prepend(show_msg);
                    $("#txtSearchHuddles:input").val('');

                    doHuddleSearch();

                }
            });

        }

    }
    $(document).on("click", "#showflashmessage", function () {
        $("#showflashmessage").fadeOut(200);
    });

    $(document).on("click", "#flashMessage", function () {
        $("#flashMessage").fadeOut(200);
    });

    $(document).on("click", "#newfoldercancel", function () {
        $("#video_folder_name").val('');
        $("#error").hide();

    });
    $(document).on("click", "#cross", function () {
        $("#video_folder_name").val('');
        $("#error").hide();

    });

    $(document).on("click", "#creatingfolder", function () {
        $("#video_folder_name").val('');
    });

    $('#new_video_huddle').submit(function (evt) {
        evt.preventDefault();

    });

    $("#video_folder_name").keyup(function (e) {
        var str_length = $("#video_folder_name").val().trim();
        if (str_length.length > 0 && e.which == 13) {
            $("#creatingfolder").trigger('click');
        }
    });
</script>
