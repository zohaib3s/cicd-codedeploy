<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$btn_res_color = $this->Custom->get_site_settings('primary_bg_color');
?>

<div id="showflashmessage"></div>
<?php if (isset($unarchive) && $unarchive == 1) { ?>
    <a id="unarchive" class="btn btn-green right" style="border-color:<?php echo $btn_res_color ?>; background-color: <?php echo $btn_res_color ?>" ><?php echo $language_based_content['unarchive']; ?></a>
<?php } else { ?>

    <a id="archive" class="btn btn-green right" style="border-color:<?php echo $btn_res_color ?>; background-color: <?php echo $btn_res_color ?>"><?php echo $language_based_content['add_to_archive']; ?></a>
<?php } ?>


<?php
$params = $this->request->pass;

if (isset($this->request->data["txtSearchHuddles"]))
    $huddleSearch = $this->request->data["txtSearchHuddles"];
else if (isset($params[2]))
    $huddleSearch = $params[2];
else
    $huddleSearch = '';
?>
<?php if (isset($unarchive) && $unarchive == 1) { ?>
    <?php if ($huddleorfolder == 2) { ?>
        <div id="sort-mode" class="dropdown right mmargin-right" style="width: 200px !important;">
            <ul>
                <li><a href="<?php echo $this->base . '/Huddles/unarchive_contents/2/0/' . $view_mode . '/' . $sort . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" title="View all" class="active"><?php
                        if ($sort == 'name') {
                            echo $language_based_content['title'];
                        } elseif ($sort == 'date') {
                            echo $language_based_content['date_created'];;
                        } else {
                            echo $language_based_content['sort'];
                        }
                        ?></a></li>
                <li <?php
                if ($sort == 'name') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Huddles/unarchive_contents/2/0/' . $view_mode . '/name' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class=""><?php echo $language_based_content['title']; ?></a></li>
                <li <?php
                if ($sort == 'date') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Huddles/unarchive_contents/2/0/' . $view_mode . '/date' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class=""><?php echo $language_based_content['date_created']; ?></a></li>

            </ul>
        </div>
    <?php } ?>
    <?php if ($huddleorfolder == 1) { ?>
        <div id="sort-mode" class="dropdown right mmargin-right" style="width: 200px !important;">
            <ul>
                <li><a href="<?php echo $this->base . '/Huddles/unarchive_contents/1/0/' . $view_mode . '/' . $sort . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" title="View all" class="active"><?php
                        if ($sort == 'name') {
                            echo $language_based_content['title'];
                        } elseif ($sort == 'date') {
                           echo $language_based_content['date_created'];;
                        } else {
                            echo $language_based_content['sort'];
                        }
                        ?></a></li>
                <li <?php
                if ($sort == 'name') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Huddles/unarchive_contents/1/0/' . $view_mode . '/name' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class=""><?php echo $language_based_content['title'];?></a></li>
                <li <?php
                if ($sort == 'date') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Huddles/unarchive_contents/1/0/' . $view_mode . '/date' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class=""><?php echo $language_based_content['date_created']; ?></a></li>

            </ul>
        </div>
    <?php } ?>

<?php } else { ?>

    <?php if ($huddleorfolder == 2) { ?>
        <div id="sort-mode" class="dropdown right mmargin-right" style="width: 200px !important;">
            <ul>
                <li><a href="<?php echo $this->base . '/Huddles/archive_contents/2/0/' . $view_mode . '/' . $sort . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" title="View all" class="active"><?php
                        if ($sort == 'name') {
                            echo $language_based_content['title'];;
                        } elseif ($sort == 'date') {
                            echo $language_based_content['date_created'];
                        } else {
                            echo $language_based_content['sort'];
                        }
                        ?></a></li>
                <li <?php
                if ($sort == 'name') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Huddles/archive_contents/2/0/' . $view_mode . '/name' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class=""><?php echo $language_based_content['title']; ?></a></li>
                <li <?php
                if ($sort == 'date') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Huddles/archive_contents/2/0/' . $view_mode . '/date' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class=""><?php echo $language_based_content['date_created']; ?></a></li>

            </ul>
        </div>
    <?php } ?>
    <?php if ($huddleorfolder == 1) { ?>
        <div id="sort-mode" class="dropdown right mmargin-right" style="width: 200px !important;">
            <ul>
                <li><a href="<?php echo $this->base . '/Huddles/archive_contents/1/0/' . $view_mode . '/' . $sort . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" title="View all" class="active"><?php
                        if ($sort == 'name') {
                            echo $language_based_content['title'];
                        } elseif ($sort == 'date') {
                            echo $language_based_content['date_created'];
                        } else {
                            echo $language_based_content['sort'];
                        }
                        ?></a></li>
                <li <?php
                if ($sort == 'name') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Huddles/archive_contents/1/0/' . $view_mode . '/name' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class=""><?php echo $language_based_content['title']; ?></a></li>
                <li <?php
                if ($sort == 'date') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Huddles/archive_contents/1/0/' . $view_mode . '/date' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class=""><?php echo $language_based_content['date_created']; ?></a></li>

            </ul>
        </div>
    <?php } ?>
<?php } ?>



<div class="search-box12" style="width: 335px;float: right;position: relative;margin-top: 4px;">
    <input type="hidden" id="search-mode_archive" action="" class="btn-search" value="<?php echo $view_mode; ?>"/>
    <input type="button" id="btnHuddleSearch_archive" action="" class="btn-search" value="" style="width:27px;"/>
    <input class="text-input" name="txtSearchHuddles" id="txtSearchHuddles_archive" type="text" value="" placeholder="<?php echo $language_based_content['search']; ?> ..." style="margin-right: 25px;"/>
    <span id="clearSearchHuddles"  style="display: none;margin-right: -16px;" class="clear-video-input-box cross-search search-x-huddles">X</span>
</div>
<?php if ($huddleorfolder == 2) { ?>
    <h1 class="page-title nomargin-top"><?php echo $language_based_content['folder_list']; ?> <span class="page-title__counter"><?php echo '( ' . $count_folders . ' )' ?></span></h1>
<?php } elseif ($huddleorfolder == 1) { ?>
    <h1 class="page-title nomargin-top"><?php echo $language_based_content['huddle_list']; ?> <span class="page-title__counter"><?php echo '( ' . $count_huddles . ' )' ?></span></h1>
<?php } ?>
<hr class="style2">
<style>
    .cross-search{
        margin-left: 6px;
        position: absolute;
        right: 54px;
        width: 327px;
        cursor: pointer;
    }
</style>


<script>

    $(document).on("click", "#archive", function () {
        document.getElementById('archive_form').submit();
    });

    $(document).on("click", "#unarchive", function () {
        document.getElementById('unarchive_form').submit();
    });

    $(document).on("click", "#showflashmessage", function () {
        $("#showflashmessage").fadeOut(200);
    });

    $(document).on("click", "#flashMessage", function () {
        $("#flashMessage").fadeOut(200);
    });

    $(document).on("click", "#newfoldercancel", function () {
        $("#video_folder_name").val('');
        $("#error").hide();

    });
    $(document).on("click", "#cross", function () {
        $("#video_folder_name").val('');
        $("#error").hide();

    });

    $(document).on("click", "#creatingfolder", function () {
        $("#video_folder_name").val('');
    });

    $('#new_video_huddle').submit(function (evt) {
        evt.preventDefault();

    });

    $("#video_folder_name").keyup(function (e) {
        var str_length = $("#video_folder_name").val().trim();
        if (str_length.length > 0 && e.which == 13) {
            $("#creatingfolder").trigger('click');
        }
    });
</script>
