<section class="account_basic">
<h1>Account Overview</h1>

<div class="counts_container clearfix">
    <div class="all_counts_box">
        <div><img src="<?php echo $this->webroot;?>img/icons/active_user.png" width="30" height="20" alt=""/></div>
        <div class="all_countsMain_count"><?php echo $total_account_users;?></div>
        <div>total users</div>
    </div>
    <div class="all_counts_box">
        <div><img src="<?php echo $this->webroot;?>img/icons/huddles.png" width="30" height="20" alt=""/></div>
        <div class="all_countsMain_count"><?php echo $total_account_huddles;?></div>
        <div>huddles</div>
    </div>
    <div class="all_counts_box">
        <div><img src="<?php echo $this->webroot;?>img/icons/total_videos.png" width="30" height="20" alt=""/></div>
        <div class="all_countsMain_count"><?php echo $total_account_videos;?></div>
        <div>total videos</div>
    </div>
    <div class="all_counts_box">
        <div><img src="<?php echo $this->webroot;?>img/icons/video_views.png" width="30" height="20" alt=""/></div>
        <div class="all_countsMain_count"><?php echo $total_viewed_videos;?></div>
        <div>videos viewed</div>
    </div>
    <div class="all_counts_box">
        <div><img src="<?php echo $this->webroot;?>img/icons/comments_added.png" width="30" height="20" alt=""/></div>
        <div class="all_countsMain_count"><?php echo $total_comments_added;?></div>
        <div>comments added</div>
    </div>
</div>

</section>
<!--<section class="sibme_counts clearfix">
    <div class="counts_box blue_counts_box clearfix">
        <div class="counts_box_left"><img src="<?php echo $this->webroot."img/icons/users.png";?>" width="105" height="82" style="top:30px;"/></div>
        <div class="counts_box_right">
            <h2>Active Users</h2>
            <h3><?php echo $total_account_users;?></h3>
        </div>
    </div> 
    <div class="counts_box green_counts_box clearfix">
        <div class="counts_box_left"><img src="<?php echo $this->webroot."img/icons/huddle.png";?>" width="115" height="107" style="top:22px;"/></div>
        <div class="counts_box_right">
            <h2>Huddles</h2>
            <h3><?php echo $total_account_huddles;?></h3>
        </div>
    </div>       
</section>
<section class="clearfix videos_box">
    <div class="videos_counts">
        <div class="videos_counts_left">
            <div class="video_icon_round Dullgreen_bg"><img src="<?php echo $this->webroot."img/icons/video_icon.png";?>" width="35" height="35" alt=""/></div>
      </div>
        <div class="videos_counts_right">
            <h3>Total Videos</h3>
            <h2><?php echo $total_account_videos;?></h2>
        </div>
    </div>
    <div class="videos_counts">
        <div class="videos_counts_left">
            <div class="video_icon_round red_bg"><img src="<?php echo $this->webroot."img/icons/video_view_icon.png";?>" width="50" height="40" alt=""/></div>
      </div>
        <div class="videos_counts_right">
            <h3>Videos Viewed</h3>
            <h2><?php echo $total_viewed_videos;?></h2>
        </div>
    </div>
    <div class="videos_counts">
        <div class="videos_counts_left">
            <div class="video_icon_round yellow_bg"><img src="<?php echo $this->webroot."img/icons/comments_icon.png";?>" width="53" height="35" alt=""/></div>
      </div>
        <div class="videos_counts_right">
            <h3>Comments Added</h3>
            <h2><?php echo $total_comments_added;?></h2>
        </div>
    </div>
</section>-->
<div class="clrcls1"></div>
          