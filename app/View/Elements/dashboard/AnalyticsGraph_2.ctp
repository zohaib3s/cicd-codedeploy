
<div class="gra-div">
    <div id="chartdivCoach_2" style="width: 100%; height: 500px;"></div>   
    <div id="legend"></div>
</div> <!--- ga -->


<script>
    /*var chart = AmCharts.makeChart("chartdivCoach", {
     "type": "serial",
     "theme": "dark",    
     "autoMarginOffset": 30,
     "legend": {
     "equalWidths": false,
     "useGraphSettings": true,
     "valueAlign": "left",
     "valueWidth": 120
     },
     "dataProvider": <?php echo $total_coaching_tags; ?>,
     
     "valueAxes": [{
     "id": "tagAxis",
     "axisAlpha": 0.1,
     "dashLength": 1,
     "position": "left", 
     "integersOnly": true ,
     "baseValue":0,
     "minimum":0,
     "title": "Total Tags"
     }, {
     "id": "sessionAxis",
     "axisAlpha": 0.1,
     "gridAlpha": 0,
     "integersOnly": true ,
     "position": "right",
     "baseValue":0,
     "minimum":0,
     "title": "Sessions"
     }],
     "graphs": [{
     "balloonText": "[[category]]<br/><b><span style='font-size:14px;'>Frequency: [[value]]</span></b>",
     "bullet": "round",
     "bulletBorderAlpha": 2,
     "bulletColor": "#FFFFFF",
     "hideBulletsCount": 50,
     "title": "Total Tags",
     "valueField": "total_tags",
     "valueAxis": "tagAxis", 
     "useLineColorForBulletBorder": true
     },{
     "bullet": "none",
     "lineAlpha":0,
     "bulletBorderAlpha": 1,
     "bulletBorderThickness": 1,
     "dashLengthField": "dashLength",
     "balloonText": "<b><span style='font-size:14px;'>Sessions: [[value]]</span>",
     "title": "Sessions",
     "fillAlphas": 0,
     "valueField": "tag_session",
     "valueAxis": "sessionAxis"
     }],
     "chartCursor": {
     "categoryBalloonEnabled": false,
     "cursorAlpha": 1,
     "zoomable": false
     },
     "categoryField": "tag_title",
     "categoryAxis": {
     "gridPosition": "start",
     "gridAlpha": 0,
     "labelRotation": 30,
     "axisAlpha": 0.1,
     "labelFunction": function(label) {
     if (label.length > 15)
     return label.substr(0, 15) + '...';
     return label;
     }
     }
     });*/
    /*var chart = AmCharts.makeChart("chartdiv", {
     "type": "serial",
     "theme": "light",
     "legend": {
     "useGraphSettings": true
     },
     "dataProvider": chartData,
     "valueAxes": [{
     "id":"v1",
     "axisColor": "#FF6600",
     "axisThickness": 2,
     "gridAlpha": 0,
     "axisAlpha": 1,
     "position": "left"
     }, {
     "id":"v2",
     "axisColor": "#FCD202",
     "axisThickness": 2,
     "gridAlpha": 0,
     "axisAlpha": 1,
     "position": "right"
     }, {
     "id":"v3",
     "axisColor": "#B0DE09",
     "axisThickness": 2,
     "gridAlpha": 0,
     "offset": 50,
     "axisAlpha": 1,
     "position": "left"
     }],
     "graphs": [{
     "valueAxis": "v1",
     "lineColor": "#FF6600",
     "bullet": "round",
     "bulletBorderThickness": 1,
     "hideBulletsCount": 30,
     "title": "red line",
     "valueField": "visits",
     "fillAlphas": 0
     }, {
     "valueAxis": "v2",
     "lineColor": "#FCD202",
     "bullet": "square",
     "bulletBorderThickness": 1,
     "hideBulletsCount": 30,
     "title": "yellow line",
     "valueField": "hits",
     "fillAlphas": 0
     }, {
     "valueAxis": "v3",
     "lineColor": "#B0DE09",
     "bullet": "triangleUp",
     "bulletBorderThickness": 1,
     "hideBulletsCount": 30,
     "title": "green line",
     "valueField": "views",
     "fillAlphas": 0
     }],
     "chartScrollbar": {},
     "chartCursor": {
     "cursorPosition": "mouse"
     },
     "categoryField": "date",
     "categoryAxis": {
     "parseDates": true,
     "axisColor": "#DADADA",
     "minorGridEnabled": true
     },
     "export": {
     "enabled": true,
     "position": "bottom-right"
     }
     });*/
//var t = <?php echo $total_coaching_tags; ?>;
//console.log(t);
    var chartCoach2 = AmCharts.makeChart("chartdivCoach_2", {
        "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
        "type": "serial",
        "theme": "dark",
        "legend": {
            "useGraphSettings": true,
            "valueText": "[[value]]",
            "fontSize": 10
        },
        "balloon": {
            "textAlign": "left",
            "maxWidth": 400,
            "verticalPadding": 8
        },
        /*"dataProvider": [{
         "date": "2012-01-01",
         "distance": 227,
         "townName": "New York",
         "townName2": "New York",
         "townSize": 25,
         "latitude": 40.71,
         "duration": 408
         }, {
         "date": "2012-01-02",
         "distance": 371,
         "townName": "Washington",
         "townSize": 14,
         "latitude": 38.89,
         "duration": 482
         }, {
         "date": "2012-01-03",
         "distance": 433,
         "townName": "Wilmington",
         "townSize": 6,
         "latitude": 34.22,
         "duration": 562
         }, {
         "date": "2012-01-04",
         "distance": 345,
         "townName": "Jacksonville",
         "townSize": 7,
         "latitude": 30.35,
         "duration": 379
         }, {
         "date": "2012-01-05",
         "distance": 480,
         "townName": "Miami",
         "townName2": "Miami",
         "townSize": 10,
         "latitude": 25.83,
         "duration": 501
         }, {
         "date": "2012-01-06",
         "distance": 386,
         "townName": "Tallahassee",
         "townSize": 7,
         "latitude": 30.46,
         "duration": 443
         }, {
         "date": "2012-01-07",
         "distance": 348,
         "townName": "New Orleans",
         "townSize": 10,
         "latitude": 29.94,
         "duration": 405
         }, {
         "date": "2012-01-08",
         "distance": 238,
         "townName": "Houston",
         "townName2": "Houston",
         "townSize": 16,
         "latitude": 29.76,
         "duration": 309
         }, {
         "date": "2012-01-09",
         "distance": 218,
         "townName": "Dalas",
         "townSize": 17,
         "latitude": 32.8,
         "duration": 287
         }, {
         "date": "2012-01-10",
         "distance": 349,
         "townName": "Oklahoma City",
         "townSize": 11,
         "latitude": 35.49,
         "duration": 485
         }, {
         "date": "2012-01-11",
         "distance": 603,
         "townName": "Kansas City",
         "townSize": 10,
         "latitude": 39.1,
         "duration": 890
         }, {
         "date": "2012-01-12",
         "distance": 534,
         "townName": "Denver",
         "townName2": "Denver",
         "townSize": 18,
         "latitude": 39.74,
         "duration": 810
         }, {
         "date": "2012-01-13",
         "townName": "Salt Lake City",
         "townSize": 12,
         "distance": 425,
         "duration": 670,
         "latitude": 40.75,
         "dashLength": 8,
         "alpha": 0.4
         }, {
         "date": "2012-01-14",
         "latitude": 36.1,
         "duration": 470,
         "townName": "Las Vegas",
         "townName2": "Las Vegas"
         }],*/
        "dataProvider": <?php echo $total_coaching_tags; ?>,
        "valueAxes": [{
                "id": "v1",
                "integersOnly": true,
                "axisColor": "#FF6600",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "left",
                "title": "Total Tags"
            }, {
                "id": "v11",
                "integersOnly": true,
                "axisColor": "#FCD202",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "right",
                "title": "Total Sessions"
            }],
        "graphs": [{
                "valueAxis": "v1",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_0]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_0",
                "valueField": "tag0",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v2",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_1]]: [[value]]</span>",
                //"legendValueText":"[[tag_title_1]]",
                "valueField": "tag1",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_1",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v3",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_2]]: [[value]]</span>",
                //"legendValueText":"[[tag_title_2]]",
                "valueField": "tag2",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_2",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v4",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_3]]: [[value]]</span>",
                "valueField": "tag3",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_3",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v5",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_4]]: [[value]]</span>",
                //"legendValueText":"tag_title_0",
                "valueField": "tag4",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_4",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v6",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_5]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_5",
                "valueField": "tag5",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v7",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_6]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_6",
                "valueField": "tag6",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v8",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_7]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_7",
                "valueField": "tag7",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v9",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_8]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_8",
                "valueField": "tag8",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v10",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_9]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_9",
                "valueField": "tag9",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v11",
                "bullet": "square",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Total Sessions",
                "dashLength": 5,
                "showBalloon": false,
                "valueField": "total_sessions",
                "fillAlphas": 0
            }],
        "chartScrollbar": {},
        "chartCursor": {
            "cursorPosition": "mouse"
        },
        "categoryField": "date",
        "categoryAxis": {
            "axisColor": "#DADADA",
            "minorGridEnabled": true,
        },
        "export": {
            "enabled": true,
            "position": "bottom-right"
        }
    });
    AmCharts.handleClick(chartCoach2);

    $("#graph_rubric_link").click(function (e) {
        $("#assessment_graph").hide();
        $("#coaching_graph").show();
        $("#graph_rating_link").show();
        $("#graph_rubric_link").hide();
        var chart1 = AmCharts.makeChart("chartCoachVideo", {
            "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
            "type": "serial",
            "theme": "light",
            "dataProvider": <?php echo $video_tags; ?>,
            "valueAxes": [{
                    "axisAlpha": 1,
                    "integersOnly": true,
                    "gridColor": "#FFFFFF",
                    "dashLength": 1
                }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                    "balloonText": "[[category]]: <b>[[value]]</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "total_tags"
                }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "tag_title",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20,
                "labelFunction": function (label) {
                    if (label.length > 15)
                        return label.substr(0, 14) + '...';
                    return label;
                }
            },
            "export": {
                "enabled": true
            }

        });
        AmCharts.checkEmptyData = function (chart1) {
            if (0 == chart1.dataProvider.length) {
                // set min/max on the value axis
                chart1.valueAxes[0].minimum = 0;
                chart1.valueAxes[0].maximum = 100;

                // add dummy data point
                var dataPoint = {
                    dummyValue: 0
                };
                dataPoint[chart1.categoryField] = '';
                chart1.dataProvider = [dataPoint];

                // add label
                chart1.addLabel(0, '50%', 'The chart contains no data', 'center');

                // set opacity of the chart div
                chart1.chartDiv.style.opacity = 0.5;

                // redraw it
                chart1.validateNow();
            }
        }

        AmCharts.checkEmptyData(chart1);
    });
    $("#graph_rating_link").click(function (e) {
        $("#coaching_graph").hide();
        $("#assessment_graph").show();
        $("#graph_rating_link").hide();
        $("#graph_rubric_link").show();
    });
    var assessments = ['no value', 'bad', 'average', 'good'];

    var chart = AmCharts.makeChart("chartdivCoach_33", {
        "type": "serial",
        "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
        "theme": "light",
        "dataProvider": <?php echo $assessment_graph; ?>,
        "valueAxes": [{
                "axisAlpha": 1,
                "integersOnly": true,
                "dashLength": 1,
                "position": "left",
                "maximum": <?php echo count($assessment_array) - 1; ?>,
                "valueText": "assessment_point",
                "labelFunction": formatValue
            }],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "showLastLabel": false,
        "chartScrollbar": {
        },
        "graphs": [{
                "id": "g1",
                "balloonText": "[[Assessment]]",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "hideBulletsCount": 50,
                "title": "red line",
                "valueField": "assessment_point",
                "useLineColorForBulletBorder": true,
                "balloon": {
                    "drop": true
                }
            }],
        "chartCursor": {
            "limitToGraph": "g1"
        },
        "categoryField": "created_date",
        "categoryAxis": {
            "axisColor": "#DADADA",
            "dashLength": 1,
            "parseDates": true,
            "minPeriod": "DD",
            "equalSpacing": true,
            "ignoreAxisWidth": true,
            "inside": true
        },
        "export": {
            "enabled": true
        }
    });

    AmCharts.checkEmptyData = function (chart) {
        if (0 == chart.dataProvider.length) {
            // set min/max on the value axis
            chart.valueAxes[0].minimum = 0;
            chart.valueAxes[0].maximum = 100;

            // add dummy data point
            var dataPoint = {
                dummyValue: 0
            };
            dataPoint[chart.categoryField] = '';
            chart.dataProvider = [dataPoint];

            // add label
            chart.addLabel(0, '50%', 'The chart contains no data', 'center');

            // set opacity of the chart div
            chart.chartDiv.style.opacity = 0.5;

            // redraw it
            chart.validateNow();
        }
    }
    AmCharts.checkEmptyData(chart);
    chart.addListener("dataUpdated", zoomChart);
    zoomChart();
    function zoomChart() {
        if (chart) {
            if (chart.zoomToIndexes) {
//                chart.zoomToIndexes(130, chartData.length - 1);
            }
        }
    }
    function formatValue(value, formattedValue, valueAxis) {
        var assessment_array = new Array();
<?php foreach ($assessment_array as $key => $val) { ?>
            assessment_array.push("<?php echo $val; ?>");
<?php } ?>
        if (value == 0) {
            return assessment_array[0];
        }
        else if (value == 1) {
            return assessment_array[1];
        }
        else if (value == 2) {
            return assessment_array[2];
        }
        else if (value == 3) {
            return assessment_array[3];
        }
        else if (value == 4) {
            return assessment_array[4];
        }
        else if (value >= 5) {
            return assessment_array[5];
        }
    }
    
    AmCharts.handleClick = function (chart){
//function handleClick(event){
    //console.log(chart.graphs);
    var d = chart.dataProvider[0];
    var garr = [];
    for (var i in chart.graphs) {
        var g = chart.graphs[i];
        //var j = 'tag_title_'+i;
        if (typeof d['tag_title_'+i]!='undefined'){
            g.visibleInLegend = true;
            if(d['tag_title_'+i].length > 30)
                g.title = d['tag_title_'+i].substr(0, 30) + '..';
            else
                g.title = d['tag_title_'+i];
            garr.push(d['tag_title_'+i]);
        }else{
            g.visibleInLegend = false;
        }
        
    }
    
    var g = chart.graphs[10];
    //console.log(g);
    g.visibleInLegend = true;
    g.title = 'Total Sessions';
    g.showBalloon = true;
    var html = "<b><span style='font-size:10px;padding-bottom: 4px;display: inline-block;'>Total Sessions: [[value]]</span><br>";
    //console.log(tag_title_0);
    for(var i in garr){
        html += "<span style='font-size:10px;padding-bottom: 4px;display: inline-block;'>"+garr[i]+": [[tag"+i+"]]</span><br>"
    }
    //html +="</span>";
    g.balloonText = html;
    chart.validateNow();
}
</script>   
<style type="text/css">   

    .virtual_data_Right{ overflow: hidden;}
    .videos_box_container{    overflow: auto;overflow-y: hidden;}
    .video_sub_box.scrool-cl > div{padding: 0 8px;}
</style>