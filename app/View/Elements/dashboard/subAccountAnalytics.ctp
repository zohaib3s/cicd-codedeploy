<div class="overviewleft">
                <div class="col-sm-33">
                  
                   
                        <span class="counter"><?php echo $total_account_users;?></span>
                        <span class="features-info">Active Users</span>
                 
                </div>
                <div class="col-sm-33">
                    

                        <span class="counter"><?php echo $total_account_huddles;?></span>
                        <span class="features-info">Huddles</span>
                 
                </div>
          </div>
          
          <div class="overviewright">
              <h4>Videos</h4>
            <div class="video_ana">
                   <span class="counter"><?php echo $total_account_videos;?></span>
                   <span class="features-info">Total Videos</span>
                   
                        
           </div>
           <div class="video_ana">
                     <span class="counter"><?php echo $total_viewed_videos;?></span>
                   <span class="features-info">Videos Viewed</span>
                 
                        
           </div>
           <div class="video_ana">
                   <span class="counter"><?php echo $total_comments_added;?></span>
                   <span class="features-info">Comments Added</span>
                   
                        
           </div>         
               <div class="clrcls1"></div>
          </div>    <!-- -right- -->  
                <div class="clrcls1"></div>
          <div class="gra-div">
              <!--<h3>Standards Frequency for Collaboration Huddles</h3>
              <div id="chartdiv" style="width: 100%; height: 362px;"></div>   -->
              <h3>Standards Frequency for Coaching Huddles</h3>
              <div id="chartdivCoachSub" style="width: 100%; height: 362px;"></div>   
           
         </div> <!--- ga -->
         
<script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/amcharts.js"></script>
    <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/serial.js"></script>
    <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/themes/light.js"></script>
<script>      
/*var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "dark",    
    "autoMarginOffset": 30,
    "dataProvider": <?php echo $total_colab_tags;?>,
    
    "valueAxes": [{
        "axisAlpha": 0.1,
        "dashLength": 1,
        "position": "left",
        "integersOnly": true   
    }],
    "graphs": [{
        "id": "g1",
        "balloonText": "[[category]]<br/><b><span style='font-size:14px;'>Frequency: [[value]]</span></b>",
        "bullet": "round",
        "bulletBorderAlpha": 2,
        "bulletColor": "#FFFFFF",
        "hideBulletsCount": 50,
        "title": "red line",
        "valueField": "total_tags",
        "useLineColorForBulletBorder": true
    }],
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 1,
        "zoomable": false
    },
    "categoryField": "tag_title",
    "categoryAxis": {
        "gridPosition": "start",
        "gridAlpha": 0,
        "labelRotation": 30,
         "axisAlpha": 0.1,
         "labelFunction": function(label) {
          if (label.length > 15)
            return label.substr(0, 15) + '...';
          return label;
        }
    }
});
AmCharts.checkEmptyData = function (chart) {
    if ( 0 == chart.dataProvider.length ) {
        // set min/max on the value axis
        chart.valueAxes[0].minimum = 0;
        chart.valueAxes[0].maximum = 100;
        
        // add dummy data point
        var dataPoint = {
            dummyValue: 0
        };
        dataPoint[chart.categoryField] = '';
        chart.dataProvider = [dataPoint];
        
        // add label
        chart.addLabel(0, '50%', 'The chart contains no data', 'center');
        
        // set opacity of the chart div
        chart.chartDiv.style.opacity = 0.5;
        
        // redraw it
        chart.validateNow();
    }
}

AmCharts.checkEmptyData(chart);*/
var chart = AmCharts.makeChart("chartdivCoachSub", {
    "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
    "type": "serial",
    "theme": "dark",    
    "autoMarginOffset": 30,
    "dataProvider": <?php echo $total_coaching_tags;?>,
    
    "valueAxes": [{
        "axisAlpha": 0.1,
        "dashLength": 1,
        "position": "left", 
        "integersOnly": true
    }],
    "graphs": [{
        "id": "g1",
        "balloonText": "[[category]]<br/><b><span style='font-size:14px;'>Frequency: [[value]]</span></b>",
        "bullet": "round",
        "bulletBorderAlpha": 2,
        "bulletColor": "#FFFFFF",
        "hideBulletsCount": 50,
        "title": "red line",
        "valueField": "total_tags", 
        "useLineColorForBulletBorder": true
    }],
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 1,
        "zoomable": false
    },
    "categoryField": "tag_title",
    "categoryAxis": {
        "gridPosition": "start",
        "gridAlpha": 0,
        "labelRotation": 30,
         "axisAlpha": 0.1,
         "labelFunction": function(label) {
          if (label.length > 15)
            return label.substr(0, 15) + '...';
          return label;
        }
    }
});
AmCharts.checkEmptyData = function (chart) {
    if ( 0 == chart.dataProvider.length ) {
        // set min/max on the value axis
        chart.valueAxes[0].minimum = 0;
        chart.valueAxes[0].maximum = 100;
        
        // add dummy data point
        var dataPoint = {
            dummyValue: 0
        };
        dataPoint[chart.categoryField] = '';
        chart.dataProvider = [dataPoint];
        
        // add label
        chart.addLabel(0, '50%', 'The chart contains no data', 'center');
        
        // set opacity of the chart div
        chart.chartDiv.style.opacity = 0.5;
        
        // redraw it
        chart.validateNow();
    }
}

AmCharts.checkEmptyData(chart);
</script>         