<div class="gra-div">
    <div id="chartdivCoach" style="width: 100%; height: 500px;"></div>
    <div id="legend"></div>
</div> <!--- ga -->


<script>
    /*var chart = AmCharts.makeChart("chartdivCoach", {
     "type": "serial",
     "theme": "dark",
     "autoMarginOffset": 30,
     "legend": {
     "equalWidths": false,
     "useGraphSettings": true,
     "valueAlign": "left",
     "valueWidth": 120
     },
     "dataProvider": <?php echo $total_coaching_tags; ?>,

     "valueAxes": [{
     "id": "tagAxis",
     "axisAlpha": 0.1,
     "dashLength": 1,
     "position": "left",
     "integersOnly": true ,
     "baseValue":0,
     "minimum":0,
     "title": "Total Tags"
     }, {
     "id": "sessionAxis",
     "axisAlpha": 0.1,
     "gridAlpha": 0,
     "integersOnly": true ,
     "position": "right",
     "baseValue":0,
     "minimum":0,
     "title": "Sessions"
     }],
     "graphs": [{
     "balloonText": "[[category]]<br/><b><span style='font-size:14px;'>Frequency: [[value]]</span></b>",
     "bullet": "round",
     "bulletBorderAlpha": 2,
     "bulletColor": "#FFFFFF",
     "hideBulletsCount": 50,
     "title": "Total Tags",
     "valueField": "total_tags",
     "valueAxis": "tagAxis",
     "useLineColorForBulletBorder": true
     },{
     "bullet": "none",
     "lineAlpha":0,
     "bulletBorderAlpha": 1,
     "bulletBorderThickness": 1,
     "dashLengthField": "dashLength",
     "balloonText": "<b><span style='font-size:14px;'>Sessions: [[value]]</span>",
     "title": "Sessions",
     "fillAlphas": 0,
     "valueField": "tag_session",
     "valueAxis": "sessionAxis"
     }],
     "chartCursor": {
     "categoryBalloonEnabled": false,
     "cursorAlpha": 1,
     "zoomable": false
     },
     "categoryField": "tag_title",
     "categoryAxis": {
     "gridPosition": "start",
     "gridAlpha": 0,
     "labelRotation": 30,
     "axisAlpha": 0.1,
     "labelFunction": function(label) {
     if (label.length > 15)
     return label.substr(0, 15) + '...';
     return label;
     }
     }
     });*/
    /*var chart = AmCharts.makeChart("chartdiv", {
     "type": "serial",
     "theme": "light",
     "legend": {
     "useGraphSettings": true
     },
     "dataProvider": chartData,
     "valueAxes": [{
     "id":"v1",
     "axisColor": "#FF6600",
     "axisThickness": 2,
     "gridAlpha": 0,
     "axisAlpha": 1,
     "position": "left"
     }, {
     "id":"v2",
     "axisColor": "#FCD202",
     "axisThickness": 2,
     "gridAlpha": 0,
     "axisAlpha": 1,
     "position": "right"
     }, {
     "id":"v3",
     "axisColor": "#B0DE09",
     "axisThickness": 2,
     "gridAlpha": 0,
     "offset": 50,
     "axisAlpha": 1,
     "position": "left"
     }],
     "graphs": [{
     "valueAxis": "v1",
     "lineColor": "#FF6600",
     "bullet": "round",
     "bulletBorderThickness": 1,
     "hideBulletsCount": 30,
     "title": "red line",
     "valueField": "visits",
     "fillAlphas": 0
     }, {
     "valueAxis": "v2",
     "lineColor": "#FCD202",
     "bullet": "square",
     "bulletBorderThickness": 1,
     "hideBulletsCount": 30,
     "title": "yellow line",
     "valueField": "hits",
     "fillAlphas": 0
     }, {
     "valueAxis": "v3",
     "lineColor": "#B0DE09",
     "bullet": "triangleUp",
     "bulletBorderThickness": 1,
     "hideBulletsCount": 30,
     "title": "green line",
     "valueField": "views",
     "fillAlphas": 0
     }],
     "chartScrollbar": {},
     "chartCursor": {
     "cursorPosition": "mouse"
     },
     "categoryField": "date",
     "categoryAxis": {
     "parseDates": true,
     "axisColor": "#DADADA",
     "minorGridEnabled": true
     },
     "export": {
     "enabled": true,
     "position": "bottom-right"
     }
     });*/
//var t = <?php echo $total_coaching_tags; ?>;
//console.log(t);
    var chart = AmCharts.makeChart("chartdivCoach", {
        "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
        "type": "serial",
        "theme": "dark",
        "legend": {
            "useGraphSettings": true,
            "valueText": "[[value]]",
            "fontSize": 10
        },
        "balloon": {
            "textAlign": "left",
            "maxWidth": 400,
            "verticalPadding": 8
        },
        "dataProvider": <?php echo $total_coaching_tags; ?>,
        "valueAxes": [{
                "id": "v1",
                "integersOnly": true,
                "axisColor": "#FF6600",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "left",
                "title": "Total Tags"
            }, {
                "id": "v11",
                "integersOnly": true,
                "axisColor": "#FCD202",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "right",
                "title": "Total Sessions"
            }],
        "graphs": [{
                "valueAxis": "v1",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_0]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_0",
                "valueField": "tag0",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v2",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_1]]: [[value]]</span>",
                //"legendValueText":"[[tag_title_1]]",
                "valueField": "tag1",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_1",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v3",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_2]]: [[value]]</span>",
                //"legendValueText":"[[tag_title_2]]",
                "valueField": "tag2",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_2",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v4",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_3]]: [[value]]</span>",
                "valueField": "tag3",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_3",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v5",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_4]]: [[value]]</span>",
                //"legendValueText":"tag_title_0",
                "valueField": "tag4",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_4",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v6",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_5]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_5",
                "valueField": "tag5",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v7",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_6]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_6",
                "valueField": "tag6",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v8",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_7]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_7",
                "valueField": "tag7",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v9",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_8]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_8",
                "valueField": "tag8",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v10",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_9]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_9",
                "valueField": "tag9",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v11",
                "bullet": "square",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Total Sessions",
                "dashLength": 5,
                "showBalloon": false,
                "valueField": "total_sessions",
                "fillAlphas": 0
            }],
        "chartScrollbar": {},
        "chartCursor": {
            "cursorPosition": "mouse"
        },
        "categoryField": "date",
        "categoryAxis": {
            "axisColor": "#DADADA",
            "minorGridEnabled": true,
        },
        "export": {
            "enabled": true,
            "position": "bottom-right",
            "menu": [{
                    "class": "export-main",
                    "menu": ["PNG", "JPG", "SVG", "PDF"]
                }]
        }
    });


    AmCharts.checkEmptyData = function (chart) {
        if (0 == chart.dataProvider.length) {
            // set min/max on the value axis
            chart.valueAxes[0].minimum = 0;
            chart.valueAxes[0].maximum = 100;

            // add dummy data point
            var dataPoint = {
                dummyValue: 0
            };
            dataPoint[chart.categoryField] = '';
            chart.dataProvider = [dataPoint];

            // add label
            chart.addLabel(0, '50%', 'The chart contains no data', 'center');

            // set opacity of the chart div
            chart.chartDiv.style.opacity = 0.5;

            // redraw it
            chart.validateNow();
        }
    }

    AmCharts.checkEmptyData(chart);
//chart.addListener("init", handleClick);
    AmCharts.handleClick = function (chart) {
//function handleClick(event){
        //console.log(chart.graphs);
        var d = chart.dataProvider[0];
        var garr = [];
        for (var i in chart.graphs) {
            var g = chart.graphs[i];
            //var j = 'tag_title_'+i;
            if (typeof d['tag_title_' + i] != 'undefined') {
                g.visibleInLegend = true;
                if (d['tag_title_' + i].length > 30)
                    g.title = d['tag_title_' + i].substr(0, 30) + '..';
                else
                    g.title = d['tag_title_' + i];
                garr.push(d['tag_title_' + i]);
            } else {
                g.visibleInLegend = false;
            }

        }

        var g = chart.graphs[10];
        //console.log(g);
        g.visibleInLegend = true;
        g.title = 'Total Sessions';
        g.showBalloon = true;
        var html = "<b><span style='font-size:10px;padding-bottom: 4px;display: inline-block;'>Total Sessions: [[value]]</span><br>";
        //console.log(tag_title_0);
        for (var i in garr) {
            html += "<span style='font-size:10px;padding-bottom: 4px;display: inline-block;'>" + garr[i] + ": [[tag" + i + "]]</span><br>"
        }
        //html +="</span>";
        g.balloonText = html;
        chart.validateNow();
    }
    AmCharts.handleClick(chart);
</script>