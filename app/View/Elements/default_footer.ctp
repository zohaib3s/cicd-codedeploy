<!--<div id="desk-support-box">
    <a id="desk-support-box-close">&otimes;</a>
    <iframe src="https://sibme.desk.com/customer/widget/emails/new" id="desk-widget-iframe"></iframe>
</div>-->
<?php
$user_current_account = $this->Session->read('user_current_account');
$full_name = $user_current_account['User']['first_name'].' '.$user_current_account['User']['last_name'];
$email = $user_current_account['User']['email'];
?>

<style>
.live_re_msg {
    cursor: pointer;
    color: #fff;
    background: #2B4553;
    text-align: left;
    padding: 20px;
    position: fixed;
    top: 130px;
    right: 0;
    border-radius: 8px;
    overflow: hidden;
    opacity: 0.9;
}
.live_re_msg h3{
color:#fff;
font-family: "Segoe UI",Helvetica,Arial,sans-serif;
margin:0px 0px 0px 0px;
font-size:15px;
}
.live_re_msg p{
font-weight:normal;
}
.notification_cross{
position:absolute;
right:8px;
top:5px;
font-weight:normal;
z-index: 15;
}
.icon_box_notification{
margin: -20px;
    padding: 25px;
    background: #DD554B;
    float: left;
    margin-right: 20px;
    height: 80px;
    width: 95px;
    text-align: center;
}
.noteificationRight{
float: left;
    margin-right: 16px;
}



</style>
<div id="push_notify" class="push_notify_class">
    
<?php echo $sibme_learning_center_modal;?>

<!--    <div id="flashMessage" class="live_re_msg" style="cursor: pointer;">
        <div class="notification_cross">x</div>
        <a href = "https://qa.sibme.com" style="color:#fff;"> 
        <div class="icon_box_notification"><img src="/img/recording_live_video.png" /></div>    
            <div class="noteificationRight">    
                <h3>Sibme QA</h3>
                <p>Sibme QA is now Live in the Michael Conn Huddle</p>
            </div>    
        <div class="clear"></div>
        </a>
    </div>-->
    
</div>

<script>
  $(document).on("click", ".notification_cross", function () {
        $("#push_notify").html('');
  });
  $(document).ready(function() {
        $(document).on("click", "#top_menu_sibme_learning_center", function(e){
            e.preventDefault();
            $.get( home_url + "/app/sibme_learning_center", function( data ) {
                if(data.show_popup==true){
                    $("#sibme_learning_center_modal").modal("show");
                } else {
                    $("#sibme_learning_center_modal").modal("hide");
                    if(data.learning_center_login_url){
                        window.location.href=data.learning_center_login_url;
                    }
                    
                }
            });
        });
        $(document).on("click", "#sibme_learning_center_modal .btn-success", function(){
            if($("#sibme_learning_center_modal #password").val()=="" || $("#sibme_learning_center_modal #password").val()!=$("#sibme_learning_center_modal #confirm_password").val()){
                alert("Please match the password with confirm password!");
                return false;
            }
            $.get( home_url + "/app/create_new_thinkific_account", {password:$("#sibme_learning_center_modal #password").val()}, function( data ) {
                if(data.success==false){
                    alert(data.message);
                } else {

                    $("#sibme_learning_center_modal").modal("hide");
                    if(data.learning_center_login_url){
                        window.location.href=data.learning_center_login_url;
                    }

                }
            });
        });
  });
</script>    

<?php if(IS_QA): ?>
<!--<img id="wootric_id" style="    position: fixed;
     cursor: pointer;
    right: 31px;
    bottom: 85px;" src="<?php //echo $this->webroot . 'app/img/smiley.png'; ?>">-->
<?php endif; ?>
<div id="desk-darkness"></div>
<!-- <a class="desk-cta desk-sidetab" href="https://sibme.desk.com/customer/widget/emails/new">Contact Us</a> -->
<?php if (isset($_SESSION['site_id']) && $_SESSION['site_id'] == 1) { ?>
<script>!function(e,o,n){window.HSCW=o,window.HS=n,n.beacon=n.beacon||{};var t=n.beacon;t.userConfig={},t.readyQueue=[],t.config=function(e){this.userConfig=e},t.ready=function(e){this.readyQueue.push(e)},o.config={docs:{enabled:!0,baseUrl:"//sibme.helpscoutdocs.com/"},contact:{enabled:!0,formId:"b3a8274d-557b-11e6-aae8-0a7d6919297d"}};var r=e.getElementsByTagName("script")[0],c=e.createElement("script");c.type="text/javascript",c.async=!0,c.src="https://djtflbt20bdde.cloudfront.net/",r.parentNode.insertBefore(c,r)}(document,window.HSCW||{},window.HS||{});</script>
<script>
  /*HS.beacon.config({
    color: '#336699',
    topArticles: true,
    showContactFields: true,
    topics: [
      { val: 'getting-started-help', label: 'I need help getting started' },
      { val: 'access-issue', label: 'I can\'t access my account'},
      { val: 'technical-issue', label: 'I have a technical issue'},
      { val: 'feature-suggestion', label: 'I\'d like to suggest a feature'},
      { val: 'bug', label: 'I think I found a bug'},
      { val: 'need-help', label: 'I just need some help'},
      { val: 'billing-question', label: 'I have a billing question'},
      { val: 'others', label: 'Other'}
    ],
    attachment: true
  });
      HS.beacon.ready(function() {
        HS.beacon.identify({
          name: '<?php echo $full_name; ; ?>',
          email: '<?php echo $email ; ?>',
        });
      });*/
</script>
<?php } ?>
<script src="https://desk-customers.s3.amazonaws.com/shared/email_widget.js" type="text/javascript"></script>

<!-- start Mixpanel -->
<script type="text/javascript">(function(e, b) {
        if (!b.__SV) {
            var a, f, i, g;
            window.mixpanel = b;
            a = e.createElement("script");
            a.type = "text/javascript";
            a.async = !0;
            a.src = ("https:" === e.location.protocol ? "https:" : "http:") + '//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';
            f = e.getElementsByTagName("script")[0];
            f.parentNode.insertBefore(a, f);
            b._i = [];
            b.init = function(a, e, d) {
                function f(b, h) {
                    var a = h.split(".");
                    2 == a.length && (b = b[a[0]], h = a[1]);
                    b[h] = function() {
                        b.push([h].concat(Array.prototype.slice.call(arguments, 0)))
                    }
                }
                var c = b;
                "undefined" !== typeof d ? c = b[d] = [] : d = "mixpanel";
                c.people = c.people || [];
                c.toString = function(b) {
                    var a = "mixpanel";
                    "mixpanel" !== d && (a += "." + d);
                    b || (a += " (stub)");
                    return a
                };
                c.people.toString = function() {
                    return c.toString(1) + ".people (stub)"
                };
                i = "disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
                for (g = 0; g < i.length; g++)
                    f(c, i[g]);
                b._i.push([a, e, d])
            };
            b.__SV = 1.2
        }
    })(document, window.mixpanel || []);
    mixpanel.init("bce282aad9b8a17fd9b963b136a81970");
</script>
<!-- end Mixpanel -->
