<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
?>
<div class="tab-content <?php
if ($tab == '3'): echo 'tab-active';
else: echo 'tab-hidden';
endif;
?>" id="tabbox3" <?php
     if ($tab == '3'): echo 'style="visibility: visible"';
     else: echo 'style="display: none"';
     endif;
     ?>>
    <div class="tab-3" id="discussion_list">
        <?php if (is_array($discussions) && count($discussions) > 0 && $replys == '' && $key != 'add'): ?>
            <h1 style="margin-bottom:10px;"><?php echo $language_based_content['huddle_discussion_view']; ?> (<?php echo count($discussions); ?>)</h1>
            <div class="select" style="float: right;margin-left: 0px;">
                <select id="cmbDiscussionSort" name="upload-date">
                    <option value="Comment.title ASC"><?php echo $language_based_content['discussion_title']; ?></option>
                    <option value="Comment.created_date DESC"><?php echo $language_based_content['date_created_view']; ?></option>
                </select>
            </div>
            <div class="search-box12" style="width: 313px;float: right;position: relative;margin-top: 1px;">
                <input type="button" id="btnHuddleDiscussionSearch" action="" class="btn-search" value="" style="width:27px;"/>
                <input class="text-input" name="txtSearchHuddleDiscussions" id="txtSearchHuddleDiscussions" type="text" value="" placeholder="<?php echo $language_based_content['search_discussions']; ?>" style="margin-right: 0px;width: 270px !important;padding-right: 0px !important;"/>
                <span id="clearSearchHuddleDiscussions"  style="display: none;right: 12px !important;" class="clear-video-input-box cross-search search-x-huddles">X</span>
            </div>
            <ul id="ulDiscussions">
                <?php if (is_array($discussions) && count($discussions) > 0): ?>
                    <?php foreach ($discussions as $row): ?>
                        <?php
                        $discussionReplyCount = $Comment->getReplysCount($row['Comment']['id']);
                        $discussionReply = $Comment->getReplysAttachments($row['Comment']['id']);
                        ?>
                        <li>
                            <div class="tab-3-box">
                                <div class="tab-3-box-left">
                                    <h2><a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/3/' . $row['Comment']['id']; ?>"><?php echo $row['Comment']['title']; ?></a></h2>
                                    <?php
                                    $profile_image = isset($row['User']['image']) ? $row['User']['image'] : '';
                                    $profile_image_real_path = WWW_ROOT . "/img/users/$profile_image";

                                    //if (file_exists($profile_image_real_path) && $profile_image != '') {
                                    if ($profile_image != '') {
                                        //$profile_image = $this->webroot . "img/users/$profile_image";
                                        $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $row['User']['user_id'] . "/" . $row['User']['image']);
                                    } else {
                                        //$profile_image = $this->webroot . "img/profile.jpg";
                                        $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/profile.jpg');
                                    }
                                    ?>

                                    <div class="frame"><img src="<?php echo $profile_image; ?>" alt="img" height="42px" width="42px"/></div>
                                    <div class="text"> <strong class="title"><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?></strong>
                                        <div style="float: left;">
                                            <?php if ($discussionReplyCount > 0): ?>
                                                <p><?php echo strlen($row['Comment']['comment']) > 200 ? strip_tags(mb_substr($row['Comment']['comment'], 0, 200), '<p><a><br>') . '...' : strip_tags($row['Comment']['comment'], '<p><a><br>') ?></p>
                                            <?php else: ?>
                                                <p><p><?php echo strlen($row['Comment']['comment']) > 130 ? strip_tags(mb_substr($row['Comment']['comment'], 0, 130), '<p><a><br>') . '...' : strip_tags($row['Comment']['comment'], '<p><a><br>') ?></p></p>
                                            <?php endif; ?>
                                        </div>
                                        <div style="float: right" class="docs-box">
                                            <ul>
                                                <?php $attachedDocuments = $Document->getByCommentId($row['Comment']['id']); ?>
                                                <?php foreach ($attachedDocuments as $att): ?>
                                                    <?php if ($att['Document']['url'] != ''): ?>
                                                        <?php
                                                        $videoFilePath = pathinfo($att['Document']['url']);
                                                        $ext = $videoFilePath['extension'];
                                                        $parentDiscussion = $Comment->get($row['Comment']['id']);
                                                        $download_url = '';

                                                        $href = '';
                                                        if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')) {
                                                            $download_url = $this->base . '/Huddles/download/' . $att['Document']['id'];
                                                            $href = 'href="' . $download_url . '"';
                                                        }
                                                        ?>
                                                        <li>
                                                            <a <?php echo $href; ?> target="_blank" style="font-size:14px;">
                                                                <div class="" style="padding-top: 10px;">
                                                                    <img class="icon24" src="<?php echo $this->Custom->getDocTypeIconLink($ext); ?>" alt="doc type icon" />
                                                                </div>
                                                            </a>
                                                        </li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>

                                                <?php if (count($discussionReply) != ''): ?>
                                                    <?php
                                                    foreach ($discussionReply as $row2) {
                                                        if ($row2['Document']['url'] != ''):
                                                            $videoFilePath = pathinfo($row2['Document']['url']);
                                                            $ext = $videoFilePath['extension'];
                                                            $download_url = '';

                                                            $href = '';
                                                            if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')) {
                                                                $download_url = $this->base . '/Huddles/download/' . $row2['Document']['document_id'];
                                                                $href = 'href="' . $download_url . '"';
                                                            }
                                                            ?>
                                                            <li>
                                                                <a <?php echo $href; ?> target="_blank" style="font-size:14px;">
                                                                    <div class="" style="padding-top: 10px;">
                                                                        <img class="icon24" src="<?php echo $this->Custom->getDocTypeIconLink($ext); ?>" alt="doc type icon" />
                                                                    </div>
                                                                </a>
                                                            </li>
                                                        <?php endif; ?>
                                                    <?php } ?>
                                                <?php endif;
                                                ?>
                                            </ul>
                                        </div>
                                        <div style="clear: both;"></div>
                                        <div>
                                            <?php
                                            $commentDate = $row['Comment']['created_date'];
                                            $commentsDate = $this->Custom->formatDate2($commentDate);
                                            ?>
                                            <strong class="posted"><?php echo $language_based_content['posted_discussion']; ?> <?php echo $commentsDate; ?></strong>
                                        </div>

                                    </div>
                                </div>
                                <div class="right-box-docs">
                                    <ul>
                                        <li>
                                            <strong class="tip">
                                                <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/3/' . $row['Comment']['id'] ?>"><?php echo $discussionReplyCount; ?></a>
                                            </strong>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        <?php elseif ($replys != '' && $key != 'add'): ?>
            <?php echo $replys; ?>
        <?php elseif ($addForm != '' && $key == 'add'): ?>
            <?php echo $addForm; ?>
        <?php else: ?>
            <h1><?php echo $language_based_content['huddle_discussion']; ?></h1>
            <ul style="float:left;width:100%;">
                <li><?php echo $language_based_content['no_discussion_started_yet']; ?></li>
            </ul>
        <?php endif; ?>
    </div>
    <!-- add discussion form -->
    <div id="discussion_form" style="display: none;">
        <?php
        $total_participants_count = 0;
        $predefined_users_ids = '';

        if (isset($huddles_users)) {
            for ($i = 0; $i < count($huddles_users); $i++) {
                $user = $huddles_users[$i];

                if ($user_current_account['User']['id'] == $user['huddle_users']['user_id'])
                    continue;

                if (!empty($predefined_users_ids))
                    $predefined_users_ids .= ',';
                $predefined_users_ids .= $user['huddle_users']['user_id'];
                $total_participants_count += 1;
            }
        }
        if (isset($videoHuddleGroups) && !empty($videoHuddleGroups[0]['User']['id']) && count($videoHuddleGroups) > 0) {
            for ($i = 0; $i < count($huddles_users); $i++) {
                $user = $huddles_users[$i]['User'];
                if (isset($user['id']) && !empty($user['id'])) {
                    if ($user_current_account['User']['id'] == $user['id']) {
                        continue;
                    }
                    if (!empty($predefined_users_ids)) {
                        $predefined_users_ids .= ',';
                        $predefined_users_ids .= $user['id'];
                        $total_participants_count += 1;
                    }
                }
            }
        }
        ?>
        <style>
            .wysihtml5-sandbox{
                padding:10px !important;
            }
        </style>

        <div>
            <!--<a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/3'; ?>" class="back back-add-discussion" id="d_list">Back to all discussions</a>-->
            <a class="back back-add-discussion" id="d_list" style="cursor: pointer;"><?php echo $language_based_content['back_to_all_discussion']; ?></a>
        </div>
        <div style="clear: both;"></div>
        <form action="<?php echo $this->base . '/Huddles/comments/' . $huddle_id . '/3'; ?>" id="frmAddDiscussion" name="comments" method="post" enctype="multipart/form-data" method="post" target="">
            <input id="tokentag" type="hidden" name="data[user_id]" value="<?php echo $user_id ?>" />
            <input id="comment_html_2" type="hidden" name="data[comment]" value="" />
            <input id="comment_access_level" name="data[access_level]" type="hidden" value="admins" />
            <input id="comment_access_level" name="data[commentable_id]" type="hidden" value="" />
            <div class="input-group large"  >
                <?php echo $this->Form->input('title', array('placeholder' => $language_based_content['type_subject_of_discussion'], 'class' => 'larg-input', 'div' => FALSE, 'label' => FALSE, 'required' => true)); ?>
            </div>
            <div id="desc-container" class="input-group">
                <div id="editor2-toolbar" class="editor-toolbar" style="display: none;">
                    <a data-wysihtml5-command="bold">bold</a>
                    <a data-wysihtml5-command="italic">italic</a>
                    <a data-wysihtml5-command="insertOrderedList">ol</a>
                    <a data-wysihtml5-command="insertUnorderedList">ul</a>
                    <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                </div>
                <style>
                    .editor-textarea{
                        color:#222 !important;
                    }
                </style>
                <?php echo $this->Form->textarea('comment_editor', array('id' => 'editor-2', 'class' => 'editor-textarea larg-input', 'cols' => '48', 'rows' => '8', 'placeholder' => $language_based_content['type_message_here_discussion'], 'required' => true)); ?>
                <?php echo $this->Form->error('videoHuddle.message'); ?>
                <input id="txtPreDefinedNotifiers" type="hidden" value="<?php echo $predefined_users_ids; ?>" />
                <input name="data[notification_user_ids][]" id="txtNotifications" type="hidden" value="0" />
            </div>
            <div class="input-group">
                <a href="#" class="btn btn-white btn-light btn-gray-text icon2-clip" data-remote="true" id="attachment-file"><?php echo $language_based_content['attach_file_discussion']; ?></a>
            </div>
            <div class="input-group" id="browse-attachment" style="display: none;">
                <ul id="attachment_list" class="js-attachment-list"></ul>
                <div class='clearfix'></div>
                <input class="attachment" name="data[attachment][]" placeholder="attach file" type="file" />
            </div>
            <div class="input-group">
                <label><input type="radio" id="rdoPreDefined" value="1" name="send_email" style="margin-right: 10px;" checked/><?php echo $language_based_content['post_this_message_discussion']; ?></label>
            </div>
            <div class="input-group">
                <label><input type="radio" id="rdoLetMeChoose" value="2" name="send_email" style="margin-right: 10px;"/><?php echo $language_based_content['let_me_choose_discussion']; ?></label>
            </div>

            <div class="input-group" style="display: none;" id="users-list">
                <div class="email-to" id="user_list">
                    <table>
                        <?php
                        $user_current_account = $this->Session->read('user_current_account');
                        if ($huddles_users):
                            ?>
                            <tr>
                                <?php
                                $count = 1;
                                foreach ($huddles_users as $supper):
                                    $tr = '';
                                    if ($supper['huddle_users']['role_id'] == '220') {
                                        continue;
                                    }

                                    if ($user_current_account['User']['id'] == $supper['huddle_users']['user_id']) {
                                        continue;
                                    }
                                    if ($count % 4 == 0) {
                                        $tr = '</tr><tr>';
                                    }
                                    ?>
                                    <td style="width: 150px;">
                                        <label class="ui-checkbox checkbox-2">
                                            <input class="user_checkbox" id="comment_notification_user_ids_reply_<?php echo $supper['huddle_users']['user_id'] ?>" name="chk_notification_user_ids" type="checkbox" value="<?php echo $supper['huddle_users']['user_id'] ?>" />
                                        </label>
                                        <label for="comment_notification_user_ids_<?php echo $supper['huddle_users']['user_id'] ?>"><?php echo strlen($supper['User']['first_name'] . ' ' . $supper['User']['last_name']) >= 16 ? mb_substr($supper['User']['first_name'] . ' ' . $supper['User']['last_name'], 0, 14) . '...' : $supper['User']['first_name'] . ' ' . $supper['User']['last_name'] ?></label>
                                    </td>
                                    <?php echo $tr; ?>
                                    <?php $count++; ?>
                                <?php endforeach; ?>
                            </tr>

                            <div class='clearfix'></div>
                            <?php if ($huddles_users): ?>
                                <p class="check-buttons" style="margin-left: 10px;">
                                    <a class="" id="reply_check_none" style="margin-right: 10px; text-decoration: underline;" href="javascript:uncheckAllUsers('.user_checkbox')">Select None</a>
                                    <a class="" id="reply_check_all" style="margin-right: 10px; text-decoration: underline;" href="javascript:checkAllUsers('.user_checkbox')">Select All</a>
                                </p>
                            <?php endif; ?>

                        </table>
                    <?php endif; ?>
                </div>
                <div class='clearfix'></div>
            </div>
            <div class='clearfix'></div>
            <div class="input-group">
                <label><input type="radio" id="rdoNoneSelected" value="3" name="send_email" style="margin-right: 10px;"/><?php echo $language_based_content['dont_let_email_discussion']; ?></label>
            </div>
            <div class="input-group">
                <input class="btn btn-green inline" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" name="commit" type="button" onclick="postDiscussion()" value="<?php echo $language_based_content['post_comment_discussion']; ?>"  />
            </div>
        </form>

    </div>
    <!-- end add discussion form -->
</div>
<script type="text/javascript">
    $("#gDiscussion").click(function () {
        $("#discussion_list").hide();
        $("#discussion_form").show();
    });
    $("#d_list").click(function () {
        $("#discussion_form").hide();
        $("#discussion_list").show();
    });
</script>