<?php
$amazon_base_url = Configure::read('amazon_base_url');
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$user_role = $this->Custom->get_user_role_name($users['users_accounts']['role_id']);
$in_trial_intercom = $this->Custom->check_if_account_in_trial_intercom($users['accounts']['account_id']);
$user_id = $users['User']['id'];
$account_id = $users['accounts']['account_id'];
$account_folder_id = $huddle[0]['AccountFolder']['account_folder_id'];
$huddle_account_id = $huddle[0]['AccountFolder']['account_id'];
$isFirefoxBrowser = $this->Browser->isFirefox();

function defaulttagsclasses3($posid) {
    $class_name = 'tags_qucls';
    if ($posid == 1) {
        $class_name = 'tags_qucls';
    } else if ($posid == 2) {
        $class_name = 'tags_sugcls';
    } else if ($posid == 3) {
        $class_name = 'tags_notescls';
    } else if ($posid == 4) {
        $class_name = 'tags_strangthcls';
    } else if ($posid == 5) {
        $class_name = 'tags_strangthcls';
    }
    return $class_name;
}
?>
<input type="hidden" value="<?php echo count($videoCommentsArray) ?>" id="count_comments" >
<style type="text/css">
    .observation_left{
        float:left;
        width:50%;
        padding:10px;
        box-sizing:border-box;
        border-right:solid 1px #dadada;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_left p{
        margin:0px;
        font-size:13px;
    }
    .observation_left h2{
        text-align:center;
        font-size:20px;
        font-weight:normal;
        margin: 12px 0px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_left h3{
        color:#707070;
        font-size:14px;
        text-align:center;
        font-weight:normal;
        margin:0px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_right{
        float:right;
        width:50%;
        padding:10px;
        box-sizing:border-box;
        margin-top: -20px;
    }
    .observation_btn_box{
        text-align:center;
        margin:15px 0px;
    }
    .observation_btn{
        width:235px;
        background:url(/img/observation_bg.png) repeat-x;
        font-weight:bold;
        font-size:18px;
        color:#fff;
        text-shadow:1px 1px 0px #205f15;
        border:solid 2px #2a8f17;
        border-radius:5px;
        outline:none;
        text-align:center;
        padding:15px 0px;
        cursor:pointer;
    }
    .timer_box{
        padding:0px 0px;
        text-align:center;
        color:#5daf46;
        font-weight:300 !important;
        font-size:4em ;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
    }
    .timer_box span{
        font-weight:300 !important;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
    }
    .stop_watch_sec{
        color:#707070;
        font-size:20px;
        font-weight:400;
    }
    .start_btn_box{
        text-align:center;
    }
    .start_time_btn{
        width:235px;
        background:url(/img/observation_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #8e631f;
        border:solid 1px #2a8f17;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .start_time_sml_btn{
        width:85px;
        background:url(/img/observation_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #8e631f;
        border:solid 1px #2a8f17;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .pause_time_btn{
        width:85px;
        background: url(/img/pause_btn_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #832920;
        border:solid 1px #f5ab35;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .stop_time_btn{
        width:85px;
        background: url(/img/stop_btn_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:1px 1px 0px #205f15;
        border:solid 1px #d23727;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .note_box{
        border:solid 1px #cccccc;
        margin:20px 0px;
    }
    .note_box textarea{
        border:0px;
        padding:10px;
        resize:none;
        outline:none;
        height:95px;
        color:#707070;
    }
    .tags_standard_box{
        border-top:solid 1px #cccccc;
        border-bottom:solid 1px #cccccc;
        padding:11px 15px;
        color:#898686;
        font-size:13px;
        height:42px;
        box-sizing:border-box;
    }
    .tags_box{
        height:42px;
        padding:9px 15px;
        box-sizing:border-box;
    }
    .tags_box img{
        vertical-align:middle;
    }
    .tags_box input[type="text"]{
        border:0px;
        outline:none;
    }
    .call_notes_data_box{
        margin:5px 0px;
    }
    .call_notes_data_body{
        font-size:13px;
        padding:10px;
        background:#f2f2f2;
        color:#454545;
    }
    .call_notes_data_header{
        padding:10px;
        color:#858585;
    }
    .call_notes_data_header img{
        vertical-align:middle;
    }
    .callNotes_actionBox{
        float:right;
        margin-top: 3px;
    }
    .optioncls a {
        width: 20px;
        height: 20px;
        border-radius: 50%;
        border: solid 2px #1297e0;
        display: inline-block;
        position: relative;
        text-align: center;
        line-height: 12px;
        text-align:center;
    }
    .call_notes_data_box .tags_standard_box{
        border-top:0px;
    }
    .call_notes_data_box .note_box{
        margin:0px;
    }
    .associate_video{
        width:482px;
        height:190px;
        background:#000;
        margin:20px auto;
        text-align:center;
        position: relative;
    }
    .associate_video .videos-list__item-thumb{
        width: 100%;
        margin: 0 auto;
        position: relative;
    }
    .associate_video .videos-list__item-thumb video{
        width: 95%;
    }
    .associate_video .videos-list__item-thumb .play-icon {
        top: 135px !important;
    }
    .associate_video img{
        margin-top:22px;
        cursor:pointer;
    }
    .video_upload_dialog{
        padding:40px;
        text-align:center;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        /*width:600px;*/
        padding-top: 20px;
    }
    .Associate_Video_btn{
        display:inline-block;
        margin:0px 10px;
        /*border:solid 1px #add_fromHuddle;*/
        padding:15px;
        color:#b4b1aa;
        border:solid 1px #cccccc;
    }
    .Associate_Video_btn a{
        color:#b4b1aa;
        text-decoration:none;
        font-weight: normal;
    }
    .Associate_Video_btn img{
        vertical-align:middle;
        position:relative;
        margin-right:5px;	
    }
    .video_box_workSpace{
        padding:10px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        width:600px;
    }
    .video_box_workSpace img{
        vertical-align:middle;
        position:relative;
    }
    .workspace_header{
        color:#979797;
    }
    .back_btn_dialog{
        float:right;
        font-size:13px;
        margin-top:7px;
        cursor: pointer;
    }
    .find_video{
        width:100%;
        box-sizing:border-box;
        padding:10px;
        border:solid 1px #ececec;
        color:#b4b1aa;
        margin:6px 0px;
        -webkit-box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
        -moz-box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
        box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
    }
    .videos_list_box{
        height:270px;
        overflow-y:scroll;
    }
    .video_detail{
        padding:6px;
    }
    .video_detail_left{
        float:left;
        clear: both;
    }
    .video_detail_left img{
        vertical-align: top;
        width: 74px !important;
        height: 55px !important;
        min-width: 74px !important;
        min-height: 60px !important;
    }
    .video_detail_text{
        float:left;
        color:#616160;
        padding:0px 0px 0px 20px;
        font-size:14px;
    }
    .video_detail_text a{
        color:#5a80a0;
        text-decoration:none;
    }
    #observations-1 .observation_left{
        float: left;
        width: 100% !important;
        padding: 10px;
        box-sizing: border-box;
        border-right: solid 0px #dadada;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .videos-list__item {
        margin: 0 0 40px 30px !important;
    }
    .video_viewercls{
        width:auto;
    }
    .observation_mainContainer{
        margin: -10px -20px;
    } 
    .coach_cochee_container{
        width: 100%;
        margin: 10px 0px;
    }
    .doted_container{
        padding:0px;
        color: #959595;
        font-size: 15px;
        margin-bottom: 10px;
    }
    .timer_box2{
        background: #ed1c24;
        font-size: 9px;
        color: #fff;
        padding: 2px 5px;
        position: absolute;
        right: 0px;
        top: -18px;
    }
    .note_box{
        position: relative;
    }
    #tab-area .tab-content{
        padding-top:-10px;
        margin-top: -10px;
    }
    .standard-search{
        position: relative;
        float: none;
        margin: 0 auto;
        width: 390px;
    }
    .standard-search .text-input{
        width: 340px !important;
    }
    .observation_search_bg{
        width: 94% !important;
        background:  url(/img/observation_search_bg.jpg) no-repeat right;
    }
    .printable_icons{
        text-align: right;
        padding-right: 10px;
    }
    #observatons-added-notes{
        position: relative;
    }
    .videos_list_box .videos-list__item-thumb{
        min-height: 56px;
        margin-left: 23px;
    }
    .videos_list_box .video-unpublished{
        overflow:hidden;
        position: relative;
    }
    .videos_list_box .video-unpublished img{
        min-height: 55px !important;
    }
    .videos_list_box .video-unpublished .play-icon {
        top: 74px !important;
        width: 25px;
        height: 25px;
        background-size: 25px;
        left: 48px;
    }
    #scrollbar1 {
        width: 100%;
    }
    .observationCls { padding-left: 0; margin-left: 0px;}
    .observationCls li{ margin-bottom: 15px;    display: inline-block;}
    .observationCls li .tab-3-box-left{    margin-bottom: 15px;}
    a.publish_obser1{ 
        background: #2dcc70;
        padding: 4px 12px;
        color: #fff !important;
        text-decoration: none;
        position: relative;
        top: -10px;
        margin-left: 8px;
    }
    #observations-comments:focus{
        z-index: 20;
        /*        box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82,168,236,0.6) !important;
                border-color: rgba(82,168,236,0.8) !important;*/
        border-color: #fff !important;
        box-shadow: none !important;
    }
    .petsCls {
        float: right;
        margin-right: 10px;
        /*margin-top: -27px;*/
        right: 0;
        position: relative;
        margin-bottom: 3px;
    }
    .petsCls label {
        font-weight: normal !important;
        font-size: 13px;
        float: left;
    }
    input#press_enter_to_send {
        margin-top: 3px;
        float: left;
        margin-right: 4px;
    }
    button.publish_obser1{
        background: #2dcc70;
    padding: 4px 12px;
    color: #fff !important;
    text-decoration: none;
    position: relative;
    top: -10px;
    margin-left: 8px;
    border:0px;
    }
</style>
<script>
    $(document).ready(function() {
        $("#observations-comments").keyup(function(e) {
            var str_length = $("#observations-comments").val().trim();
            if (e.which == 13 && $("input#press_enter_to_send").prop('checked') == true) {
                $("#add-notes").trigger('click');
            }
        });
    });

</script>
<style type="">
    .your-class::-webkit-input-placeholder {
        color: #b2b2b2;
    }
    .your-class:-moz-placeholder {
        /* FF 4-18 */
        color: #b2b2b2;
    }
    .your-class::-moz-placeholder {
        /* FF 19+ */
        color: #b2b2b2;
    }
    .your-class:-ms-input-placeholder {
        /* IE 10+ */
        color: #b2b2b2;
    }
</style>
<script type="text/javascript">

    var observation_notes_array = [];

    function load_details(document_id) {
        $.ajax({
            type: "POST",
            url: home_url + "/huddles/observation_details",
            dataType: 'json',
            data: {
                huddle_id: $('#huddle_id').val(),
                video_id: document_id
            },
            success: function(response) {
                $('.tabset').hide();
                $('.action-buttons').hide();
                $('.btn-box').hide();
                $('#observations-1').hide();
                $('#observations-2').hide();
                $('#observations-2').hide();
                $('#observations-1').hide('slow');
                $('#observations-7').html(response.html);
                $('#observations-7').show('slow');
            },
            error: function(e) {

            }
        });
    }
    $(document).on('click', '.back_to_all_observations', function(e) {
        e.preventDefault();
        update_observation_tab();
        $('.tabset').show();
        $('.action-buttons').show();
        $('.btn-box').show();
        $('#observations-1').slideDown();
        $('#observations-2').slideUp();
        $('#observations-7').slideUp();

    });
    function update_observation_tab() {
        $.ajax({
            type: "POST",
            url: home_url + "/huddles/load_observation_tab",
            dataType: 'json',
            data: {
                account_id: $('#account_id').val(),
                user_id: $('#user_id').val(),
                huddle_id: $('#huddle_id').val(),
                video_id: $('#video_id').val()
            },
            success: function(response) {
                $('#ob-videos-list').html(response.html);
            },
            error: function(e) {

            }
        });
    }
    $(document).on('click', '#publish-observations', function(e) {
        e.preventDefault();
        var document_id = $(this).attr('data-document-id');
        var huddle_id = $('#huddle_id').val();
        $.ajax({
            type: "POST",
            url: home_url + "/huddles/publish_observation",
            dataType: 'json',
            data: {
                account_id: $('#account_id').val(),
                user_id: $('#user_id').val(),
                huddle_id: huddle_id,
                video_id: document_id
            },
            success: function(response) {
                window.location.href = home_url + '/Huddles/observation_details/' + huddle_id + '/' + document_id
//                load_details(document_id);
            },
            error: function(e) {

            }
        });
    });
    $(document).on('click', '#add_video', function(e) {
        e.preventDefault();
        if ($('#sw_s').val() == '00') {
            alert('Please start observation before adding comments.');
            return false;
        }
        var huddle_id = $('#huddle_id').val();
        var video_id = $('#video_id').val();
        $.ajax({
            type: "POST",
            url: home_url + "/huddles/add_observation_video",
            dataType: 'json',
            data: {
                loaded_video_id: $("#add-huddle-video input[type='checkbox']:checked").val(),
                observation_title: $('#ObservationTitle').text(),
                account_id: $('#account_id').val(),
                user_id: $('#user_id').val(),
                huddle_id: $('#huddle_id').val(),
                video_id: $('#video_id').val()
            },
            success: function(response) {
                window.location.href = home_url + '/Huddles/observation_details/' + huddle_id + '/' + video_id
                $("ul#expList2").find('input:checkbox').removeAttr('checked');
                $('#observations-comments').val('');
                $('#txtVideoTags1_tagsinput span').remove();
                $('#txtVideostandard1_tagsinput span').remove();
                $('#txtVideostandard1_tag').show();
                $('.timer_box2').text('00:00:00');
                $('#observation-comments').html(response.comments);
                $('#video_id').val(response.document_id);
                $('#txtVideoTags1').removeAllTag();
                $('#txtVideostandard1').removeAllTag();
                //$("#expList2").find('input:checkbox').removeAttr('checked');
                $('#observations-comments').attr("placeholder", "Add a comment...");
                $('#observations-comments').addClass("your-class");
                $('#observations-7').html(response.html);
                $('#observations-7').slideDown();
                $('#associate-videos-model').modal('hide');
                $('.modal-backdrop').remove();
                $("body").removeClass("wysihtml5-supported modal-open");
            },
            error: function(e) {

            }
        });
    });

    $(document).on('click', '#add-notes', function(e) {
        e.preventDefault();
        if ($('#observations-comments').val() == '' && ($('#txtVideostandard1').val() == '' || $('#txtVideostandard1').length == 0) && $('#txtVideoTags1').val() == '') {
            $('#observations-comments').css('border', '1px solid red');
            $('#observations-comments').css('border-radius', '0');
            return true;
        }
        if ($('.timer_box2').text() == '00:00:00') {
            var ob_time = $('#sw_h').text() + ':' + $('#sw_m').text() + ':' + $('#sw_s').text();
        } else {
            var ob_time = $('.timer_box2').text();
        }
        $('#observations-comments').css('border', 'none');
        if ($('#txtVideostandard1').length > 0)
        {
            var standardswhenpresent = $('#txtVideostandard1').val();
        }
        else {
            standardswhenpresent = '';
        }
        var postData = {
            observation_title: $('#ObservationTitle').text(),
            observations_comments: $('#observations-comments').val(),
            observations_standards: standardswhenpresent,
            observations_tags: $('#txtVideoTags1').val(),
            observations_time: ob_time,
            internal_comment_id: observation_notes_array.length + 1,
            account_id: $('#account_id').val(),
            user_id: $('#user_id').val(),
            huddle_id: $('#huddle_id').val(),
            video_id: $('#video_id').val(),
            comments_optimized: 1,
            assessment_value: $('#synchro_time_class_tags1').val()
        };

        $("ul#expList2").find('input:checkbox').removeAttr('checked');
        $('#observations-comments').val('');
        $('#txtVideoTags1_tagsinput span').remove();
        $('#txtVideostandard1_tagsinput span').remove();
        $('#txtVideostandard1_tag').show();
        $('.timer_box2').text('00:00:00');
        $('.timer_box2').hide();

        var tagid = '';
        var quick_tag_counter = 0;
        $(".divblockwidth a").each(function(value) {
            $(this).attr('status_flag', '0');
            $(this).removeAttr('class');

            if (quick_tag_counter == 0) {
                $(this).attr('class', 'default_tag123 tags_qucls');
            }

            if (quick_tag_counter == 1) {
                $(this).attr('class', 'default_tag123 tags_sugcls');
            }

            if (quick_tag_counter == 2) {
                $(this).attr('class', 'default_tag123 tags_notescls');
            }

            if (quick_tag_counter == 3) {
                $(this).attr('class', 'default_tag123 tags_strangthcls');
            }

            quick_tag_counter++;
        });
        var tagid = '';
        var quick_tag_counter = 0;
        $(".divblockwidth2 a").each(function(value) {
            $(this).attr('status_flag', '0');
            $(this).removeAttr('class');

            if (quick_tag_counter == 0) {
                $(this).attr('class', 'default_rating123 tags_qucls');
            }

            if (quick_tag_counter == 1) {
                $(this).attr('class', 'default_rating123 tags_sugcls');
            }

            if (quick_tag_counter == 2) {
                $(this).attr('class', 'default_rating123 tags_notescls');
            }

            if (quick_tag_counter == 3) {
                $(this).attr('class', 'default_rating123 tags_strangthcls');
            }

            if (quick_tag_counter == 4) {
                $(this).attr('class', 'default_rating123 tags_strangthcls');
            }

            quick_tag_counter++;
        });
        $('#synchro_time_class_tags1').val('');
        //$('#observation-comments').html(response.comments);
        //$('#video_id').val(response.document_id);
        $('#txtVideoTags1').removeAllTag();
        $('#txtVideostandard1').removeAllTag();
        //$("#expList2").find('input:checkbox').removeAttr('checked');
        $('#observations-comments').attr("placeholder", "Add a comment...");
        $('#observations-comments').addClass("your-class");
        $('.printable_icons').hide();
        $('#comments').trigger('click');

        observation_notes_array.push(postData);

        var count_comments = observation_notes_array.length;

        if (count_comments == 1) {

            $('#observation-comments').append($('#observatons-added-notes'));
            $('#observatons-added-notes').show();
        }

        count_comments = parseInt(count_comments);
        $("#comments").html('Notes (' + count_comments + ')');
        $('#comments').trigger('click');
        $('#count_comments').val(count_comments);
        $('#no_notes_added').hide();

        generate_comment_html(postData);
        $('#observations-comments').css('height','100px');
        

        $.ajax({
            type: "POST",
            url: home_url + "/Huddles/add_call_notes",
            dataType: 'json',
            data: postData,
            success: function(response) {

                var internal_id = response.internal_comment_id;
                $('#normal_comment_' + internal_id).html(response.comments);

            },
            error: function(e) {

            }
        });

        //$('#observatons-added-notes').slideDown();
    });


    function generate_comment_html(postData) {

        var html = '<ul class="comments comments_huddles" id="normal_comment_' + postData.internal_comment_id + '" style="overflow-x: hidden;">';
        html += '<div rel="' + postData.internal_comment_id + '" id="comment_box' + postData.internal_comment_id + '" style="margin-top: 12px;">';

        html += '<li class="synchro"> ';
        html += '<div class="synchro-inner"> ';
        html += '<i></i><span data-time="' + postData.observations_time + '" class="synchro-time">' + postData.observations_time + '</span>';
        html += '</div></li>';

        html += '<li class="comment thread" style="display: block;float: left;">';
        html += $('#comments_user_meta_data').html();

        html += '<div class="comment-body comment more">' + nl2br(postData.observations_comments) + '</div>'
        html += '<div class="comment-footer">';
        html += '<span class="comment-date">Saving comment....</span> <div class="comment-actions" style="width: 150px;">&nbsp;</div>'
        html += ' </div>';


        html += '<div id="docs-standards" class="docs-standards" style="margin-bottom: 20px;display: block;float: left;">';

        if (postData.observations_standards != '') {

            html += '<div id="tagFilterList"><div id="tagFilterContainer" class="btnwraper">';

            var standards_selected = postData.observations_standards.split(',');

            for (var i = 0; i < standards_selected.length; i++) {

                html += '<button class="btn btn-lilghtgray" style="margin-right: 2px;">' + standards_selected[i] + '</button>';
            }

            html += '</div></div>';

        }

        if (postData.observations_tags != '') {

            html += '<div id="tagFilterList"><div id="tagFilterContainer" class="btnwraper">';

            var standards_selected = postData.observations_tags.split(',');

            for (var i = 0; i < standards_selected.length; i++) {

                html += '<button class="btn btn-lilghtgray" style="margin-right: 2px;">' + standards_selected[i] + '</button>';
            }

            html += '</div></div>';

        }

        html += '</div>';


        html += '</li>';

        html += '</div></ul>';

        $('#observatons-added-notes').prepend(html);

    }

</script>
<style type="text/css">

    #txtVideoTags1_tagsinput {
        border-radius: 0px;
        border-left: none;
        width: 484px !important;
        border-bottom: none;
    }

    #txtVideostandard1_tagsinput{
        width: 484px !important;
        border-bottom: 0 !important;
        border-radius: 0!important;
        border-left: 0 !important;
    }
    #observations-2 #txtVideoTags_tagsinput {
        border-radius: 0px;
        border-left: none;
        width: 484px !important;
        border-bottom: none;
    }
    #txtVideostandard1_tag{
        width: 100%  !important;
        cursor: pointer;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(e) {

        $('#txtVideoTags1').tagsInput({
            defaultText: 'Tags...',
            width: '200px',
            placeholderColor: '#898686'
        });
        $('.video-tags-row label').css('display', 'none');
        $('#txtVideostandard1').tagsInput({
            defaultText: 'Tag Standards...',
            width: '350px',
            readonly_input: true,
            placeholderColor: '#898686',
            onRemoveTag: function(val) {
                if ($("div[id=txtVideostandard1_tagsinput]").children('.tag').length < 1) {
                    $('#txtVideostandard1_tag').show();
                }

                var removed_tag_val = val;
                var checkboxes = $('#expList2 li input[type="checkbox"]');
                for (var i = 0; i < checkboxes.length; i++) {

                    var chkStandard = $(checkboxes[i]);
                    var tag_code = chkStandard.attr('st_code');
                    var tag_value = '';
                    var tag_array = {};
                    var tag_name = chkStandard.attr('st_name');
                    tag_array = tag_name.split(" ");
                    if (tag_array && tag_array.length > 0) {

                        var tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                        if (removed_tag_val == tag_value) {
                            chkStandard.prop('checked', false);
                        }
                    }

                }

            },
        });
        $('.video-tags-row label').css('display', 'none');
        $('#txtVideoTags1_tag').attr('disabled', 'disabled');
        $('#txtVideostandard1_tag').attr('disabled', 'disabled');
    });</script>


<script type="text/javascript">
    function getObComments(document_id) {
        $.ajax({
            type: "POST",
            url: home_url + "/huddles/get_all_ob_comments/" + document_id,
            dataType: 'json',
            data: {
                huddle_id: $('#huddle_id').val(),
                video_id: document_id
            },
            success: function(response) {
                $('#observatons-added-notes').html(response.comments);
                $('.printable_icons').hide();
            },
            error: function(e) {

            }
        });
    }
    $(function() {

        $('input#txtSearchTags2').quicksearch('ul#expList2 li', {
            noResults: 'li#noresults',
            'onAfter': function() {
                if (typeof $('#txtSearchTags2').val() !== "undefined" && $('#txtSearchTags2').val().length > 0) {
                    $('#clearTagsButton1').css('display', 'block');
                }
            }
        });

    });
    $(document).ready(function() {

        $('.input-group').find('textarea').overlay([
            {
                match: /\B#\w+\S+/g,
                css: {
                    'background-color': '#d8dfea'
                }
            }
        ]);
    });

    function deleteObservation(account_folder_id, document_id) {

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: home_url + '/Huddles/deleteDocument/' + document_id + '/' + account_folder_id,
            success: function(response) {
                doObVideoSearchAjax();
            },
            error: function() {
                doObVideoSearchAjax();
            }
        });

        return false;
    }

    function deleteObComment(commentId, internal_comment_id, huddle_id, video_id, comment_id) {
        //$(formName).submit();
        //getVideoComments();
        //return false;
        //commentId = $(this).attr('delete-id-attr');
        //alert(commentId);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                internal_comment_id: internal_comment_id,
                huddle_id: huddle_id,
                video_id: video_id,
                comment_id: comment_id
            },
            url: home_url + '/Huddles/deleteVideoComments/' + commentId,
            success: function(response) {

                var internal_id = response.internal_comment_id;
                $('#normal_comment_' + internal_id).remove();

                var found_index = -1;
                for (var i = 0; i < observation_notes_array.length; i++) {

                    if (observation_notes_array[i].internal_comment_id == internal_id)
                        found_index = i;

                }

                if (found_index >= 0)
                    observation_notes_array.splice(found_index, 1);

                var count_comments = observation_notes_array.length;
                count_comments = parseInt(count_comments);
                $("#comments").html('Notes (' + count_comments + ')');
                $('#comments').trigger('click');
                $('#count_comments').val(count_comments);
                $('#no_notes_added').hide();

            }
        });

        return false;
    }

    function deleteCommentTag(tagId, internal_comment_id, huddle_id, video_id, comment_id) {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            data: {
                internal_comment_id: internal_comment_id,
                huddle_id: huddle_id,
                video_id: video_id,
                comment_id: comment_id
            },
            url: home_url + '/Huddles/deleteCommentTag/' + tagId,
            success: function(response) {

                var internal_id = response.internal_comment_id;
                $('#normal_comment_' + internal_id).html(response.comments);
            }
        });

        return false;
    }

</script>

<div id="print_icons_container">
    <div  class="printable_icons" style="display:none;">
        <a href="#"><img src="/img/email.png"></a>
        <a target="_blank" href="<?php echo $this->base . '/Huddles/print_pdf_comments/' . $video_id . '/2/2.pdf' ?>"><img src="/img/icons/pdf48.png" style="width:28px;" /></a>

        <a href="<?php echo $this->base . '/Huddles/print_excel_comments/' . $video_id . '/2' ?>"><img src="/img/icons/excel48.png" style="width:28px;"  /></a>
    </div>
</div>

<div id="observatons-added-notes" style="display: none;height: 576px;overflow-y: auto;">
    <style>
        .call_notes_data_header {
            padding: 6px 7px;
            color: #858585;
        }
        .callNotes_actionBox {
            margin-top: 10px;
        }
        .call_notes_data_header .comments {
            list-style-type: none;
            padding: inherit;
            margin: inherit;
            font-size: inherit;
            color: inherit; 
            float:left;
        }
        .call_notes_data_header .synchro {
            border-top:0px;
        }
        .call_notes_data_header .synchro-inner{
            position: inherit;
            top:inherit;
            left: inherit;
            padding: inherit;
            background-color:inherit;
            border-radius: inherit;
        }
        .bubble_adjs .btnwraper{
            margin-right:10px;
        }
        #tagFilterList{
            float: left;
        }
        .bubble_adjs{
            width:100% !important;
        }

    </style>
    <style>
        .vjs-tooltip{display: none !important; }
    </style>

    <div class="call_notes_container" id="vidComments">

    </div>
</div>

<div id="comments_user_meta_data" style="display:none;">
    <a href="#<?php //echo $this->base . '/users/editUser/' . $users['User']['id'];   ?>">
        <?php if (isset($users['User']['image']) && $users['User']['image'] != ''): ?>
            <?php
            $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
            echo $this->Html->image($chimg, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '21', 'width' => '21', 'align' => 'left'));
            ?>
        <?php else: ?>
            <img width="21" height="21"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo photo inline" rel="image-uploader" alt="Photo-default">
        <?php endif; ?>
    </a>
    <div class="comment-header"><?php echo $users['User']['first_name'] . " " . $users['User']['last_name']; ?></div>
</div>

<div class="tab-content <?php echo ($tab == '5') ? 'tab-active' : 'tab-hidden'; ?>"  id="tabbox5" style="padding-top:5px;">
    <div id="observations-1">
        <div style="padding: 12px 0px 0px 0px;">
            <h1 id="huddle_ob_video_title_strong" style="margin-bottom:-30px;font: bold 25px 'Segoe UI';"><?php echo $language_based_content['observation_count_discussion']; ?> (<?php echo $this->Custom->chck_publish_count($videos, $huddle_permission); ?>)</h1> 
            <input type="hidden" value="<?php echo $this->Custom->chck_publish_count($videos, $huddle_permission); ?>" id="count_ob" >
            <input type="hidden" value="<?php echo $this->Custom->chck_publish_count($videos, $huddle_permission); ?>" id="count_ob_temp" >
            <div class="search-box" style=" width: 480px !important;position:relative;padding-bottom:10px;">
                <input type="button" class="btn-search" value="">
                <input class="text-input" id="txtSearchObVideos" type="text" value="" placeholder="<?php echo $language_based_content['search_observations_discussion']; ?>" style="margin-right: 12px;">
                <span id="clearObVideoButton" style="top: 4px;right: 207px !important;display: none;" class="clear-video-input-box">X</span>
                <div class="select">
                    <select id="cmbObVideoSort"  name="upload-date">
                        <option value="afd.title ASC"><?php echo $language_based_content['observation_title_discussion']; ?></option>
                        <option value="Document.created_date DESC" selected><?php echo $language_based_content['date_created_discussion']; ?></option>
                        <!--                        <option value="User.first_name DESC">Uploaded By</option>-->
                    </select>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <style>
            .observation_detail li{
                margin-bottom: 5px;
                position: relative;
            }
            .observation_detail li .tab-3-box{
                box-shadow: none;
                padding: 10px;
            }
            .observation_detail li .tab-3-box .tab-3-box-left{
                margin-bottom: 0px;
            }
            .observation_detail li .tab-3-box .tab-3-box-left h2{
                margin-bottom: 0px;
            }
            .observation_detail li .tab-3-box .tab-3-box-left h2 a{
                color: #2678C0;
                text-decoration: none;
            }
            .observation_detail li .tab-3-box .tab-3-box-left .frame{
                background: none;
            }
            .observation_detail li .tab-3-box .tab-3-box-left .text{
                padding-top: 5px;
            }
            .video_icon{
                height: 36px;
                position: absolute;
                right: 15px;
                top: 40px;
            }
            .timer_container{
                background: #F5F7F6;
                padding:15px  10px;
            }
            .right-box2 {
                width: 100% !important;
                margin-top: 1px;
                margin-right: -2px;
            }
            .no_notes{
                text-align: center;
                margin:20px;
                font-size: 15px;
                color:#AFAFAF;
            }
        </style>            

        <div style="clear: both;" class='clearfix'></div>
        <div id="ob-videos-list" style="margin-top: -15px;">
            <p id="notification" style="display: none;padding-left: 29px;padding-right: 9px;padding-bottom: 0px;"></p>
            <ul id="ulDiscussions" class="observationCls observation_detail">
                <?php if (is_array($videos) && count($videos) > 0): ?>
                    <?php foreach ($videos as $row): ?>

                        <?php
                        if ($row['Document']['current_duration'] > 0 && $row['Document']['is_associated'] == 0 && $huddle_permission == '210' && $row['Document']['is_processed'] > 0) {
                            $bool = 1;
                        } elseif ($huddle_permission == '210' && $row['Document']['current_duration'] == 0 && $row['Document']['is_associated'] == 0) {
                            $bool = 1;
                        } else {
                            $bool = 0;
                        }
                        ?>

                        <?php if ($bool == 0): ?>  

                            <li>
                                <div class="tab-3-box">
                                    <div class="tab-3-box-left">
                                        <h2><a class="wrap" id="vide-title-<?php echo $row['Document']['id']; ?>" 
                                            <?php
                                            if ($row['Document']['current_duration'] > 0 && $row['Document']['is_processed'] == 4 && $row['Document']['is_associated'] > 0) {
                                                echo 'href="' . $this->base . '/video_details/home/' . $huddle_id . '/'  . $row['Document']['is_associated'] . '"';
                                            } elseif ($row['Document']['current_duration'] > 0 && $row['Document']['is_processed'] == 4 && $row['Document']['is_associated'] == 0) {
                                                echo 'href="' . $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $row['Document']['id'] . '"';
                                            } elseif ($row['Document']['current_duration'] > 0) {
                                                echo 'href="' . $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $row['Document']['id'] . '"';
                                            } else {
                                                echo 'href="' . $this->base . '/video_details/scripted_observations/' . $huddle_id . '/' . $row['Document']['id'] . '"';
                                            }
                                            ?>  title="<?php echo $row['afd']['title'] ?>" ><?php echo (strlen($row['afd']['title']) > 52 ? mb_substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title']) ?></a></h2>

                                            <?php $user_image_details = $this->Custom->get_user_detail($row['Document']['created_by']); ?>
                                        <div class="frame"><?php if (isset($user_image_details['User']['image']) && $user_image_details['User']['image'] != ''): ?>
                                                <?php
                                                $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user_image_details['User']['id'] . "/" . $user_image_details['User']['image']);
                                                echo $this->Html->image($chimg, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '42', 'width' => '42', 'align' => 'left'));
                                                ?>
                                            <?php else: ?>
                                                <img width="42" height="42"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo photo inline" rel="image-uploader" alt="Photo-default">
                                            <?php endif; ?></div>
                                        <div class="text"> <strong class="title"><?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?></strong>
                                            <div style="float: right" class="docs-box"> </div>
                                            <div style="clear: both;"></div>
                                            <div>
                                                <strong class="posted">Posted <?php echo date('M d, Y', strtotime($row['Document']['created_date'])) ?></strong>
                                               
                                                <a <?php if($huddle_permission == '210') {echo 'style = "visibility:hidden;"';} ?> rel="nofollow" data-confirm="<?php echo $language_based_content['are_you_sure_want_to_del_observation']; ?>" id="delete-main-observation" class="icon2-trash right smargin-right" href="javascript:deleteObservation('<?php echo $account_folder_id; ?>', '<?php echo $row['Document']['id']; ?>');" style="border: none !important;background: transparent !important;color: #808080;font-weight: normal;text-decoration: initial;float: left;margin-top: -12px;"></a>
                                                
                                                <?php if ($row['Document']['current_duration'] > 0 && $row['Document']['is_processed'] == 4 && $row['Document']['is_associated'] == 0 && $row['Document']['upload_status'] != 'cancelled') : ?>
                                                    <button id ="publish-observations_<?php echo $row['Document']['id'] ?>" class="publish_obser1" href="javascript:void(0)" ><?php echo $language_based_content['publish_observation_for_coachee'];  ?></button>
                                                <?php elseif ($row['Document']['current_duration'] == 0 && $row['Document']['is_associated'] == 0 && $row['Document']['published'] == 0) : ?>
                                                    <a class="publish_obser1" href="<?php echo $this->base . '/Huddles/publish_scripted_observation/' . $huddle_id . '/' . $row['Document']['id'] ?>" ><?php echo $language_based_content['publish_observation_for_coachee'];  ?></a>
                                                <?php endif; ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <?php if ($row['Document']['current_duration'] > 0 ){//|| $row['Document']['scripted_current_duration'] === NULL) {
                                    ?>
                                    <div class="video_icon"> <img src="/app/img/video_color.png"></div>
                                    <?php
                                } else {
                                    ?> <div class="video_icon"> <img src="/app/img/scripted_observation.png"></div>  
                                    <?php }
                                    ?> <div  style="position: absolute;right: 15px;top: 93px;">
                                    <?php if ($row['Document']['current_duration'] > 0 && $row['Document']['is_processed'] == 4 && $row['Document']['is_associated'] > 0): ?>
                                        <?php echo $language_based_content['published_view']; ?>
                                    <?php elseif ($row['Document']['current_duration'] == 0 && $row['Document']['is_associated'] == 1): ?>
                                        <?php echo $language_based_content['published_view']; ?>
                                    <?php endif; ?>
                                </div>


                                <?php
                                if ($row['Document']['current_duration'] > 0 && $row['Document']['is_processed'] != 4 && $row['Document']['is_associated'] < 1) {
                                    ?>

                                    <div class="live_record" style="position: absolute;right: 15px;top: 93px;">
                                        <img src="/app/webroot/img/recording_img.png" /> <?php echo $language_based_content['live_recording_view']; ?>
                                    </div>
                                <?php } ?>

                            </li>

                            <?php
                            $videoID = $row['Document']['id'];

                            $document_files_array = $this->Custom->get_document_url($row['Document']);

                            if (empty($document_files_array['url'])) {
                                $row['Document']['published'] = 0;
                                $document_files_array['url'] = $row['Document']['original_file_name'];
                                $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            } else {
                                $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                @$row['Document']['duration'] = $document_files_array['duration'];
                            }

                            $videoFilePath = pathinfo($document_files_array['url']);
                            $videoFileName = $videoFilePath['filename'];
                            ?>
                            <li style="display:none;" class="videos-list__item">
                                <?php
                                $isEditable = ($huddle_permission == 200) ||
                                        ($huddle_permission == 210 && $user_current_account['User']['id'] == $row['Document']['created_by']) ||
                                        ($user_current_account['User']['id'] == $row['Document']['created_by'] && $user_permissions['UserAccount']['permission_maintain_folders'] == '1');
                                ?>
                                <?php if ($isEditable): ?>

                                <?php endif; ?>
                                <div style="display:none;" class="clearfix"></div>
                                <div style="display:none;" class="videos-list__item-thumb">
                                    <?php
                                    if ($row['Document']['published'] == '1'):

                                        $seconds = $row['Document']['duration'] % 60;
                                        $minutes = ($row['Document']['duration'] / 60) % 60;
                                        $hours = gmdate("H", $row['Document']['duration']);
                                        ?>
                                        <a href="<?php echo $this->base . '/Huddles/observation_details/' . $huddle_id . '/' . $row['Document']['id'] ?>"  data-document-id="<?php echo $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                            <?php
                                            $thumbnail_image_path = $document_files_array['thumbnail'];

                                            echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                            ?>
                                            <div class="play-icon"></div>
                                            <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                                        </a>
                                    <?php else: ?>
                                        <a href="<?php echo $this->base . '/Huddles/observation_details/' . $huddle_id . '/' . $row['Document']['id'] ?>"  data-document-id="<?php echo $row['Document']['id']; ?>" id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                            <div  class="video-unpublished ">
                                                <span class="huddles-unpublished" style="color:#fff;">
                                                    <?php if ($row['Document']['encoder_status'] == 'Error'): ?>
                                                        Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                                    <?php else : ?>
                                                        Video is not published.
                                                    <?php endif; ?>
                                                </span>
                                            </div>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </li>


                        <?php endif; ?>
                        <script>

                            $(document).on('click', '#publish-observations_<?php echo $row['Document']['id'] ?>', function(e) {
                                $(this).attr('disabled',true);
                                e.preventDefault();
                                video_id = '<?php echo $row['Document']['id']; ?>';
                                huddle_id = '<?php echo $huddle_id; ?>';
                                user_id = '<?php echo $user_id ?>';
                                account_id = '<?php echo $account_id; ?>';

                                $.ajax({
                                    type: "POST",
                                    url: home_url + "/Huddles/publish_observation",
                                    dataType: 'json',
                                    data: {
                                        account_id: account_id,
                                        user_id: user_id,
                                        huddle_id: huddle_id,
                                        video_id: video_id
                                    },
                                    success: function(response) {
                                        // console.log(response.document_id);
                                        //json_decode(response);
                                        var new_id = response.document_id;
                                        // alert(new_id);
                                        window.location.href = home_url + '/Huddles/view/' + huddle_id + '/' + '1/' + new_id;
                                    },
                                    error: function(e) {

                                    }
                                });
                            });
                        </script>

                    <?php endforeach; ?>

                    <script type="text/javascript">

                        $(document).ready(function() {

                            $('.regen-thumbnail-image').click(function() {

                                var r = confirm('Do you want to Regenerate Thumbnail?');
                                if (r == true) {

                                    var document_id = $(this).attr('document_id');
                                    var huddle_id = $(this).attr('huddle_id');
                                    $.ajax({
                                        type: 'POST',
                                        url: home_url + '/Huddles/regenerate_thumbnail/' + document_id + '/' + huddle_id,
                                        success: function(res) {

                                            alert('Thumbnail generated successfully.');
                                            d = new Date();
                                            var src_image = $("#img_" + document_id).attr("src");
                                            $("#img_" + document_id).attr("src", src_image + "&dd=" + d.getTime());
                                        },
                                        errors: function(response) {
                                            alert('Unable to generate Thumbnail, please try again later.');
                                        }
                                    });
                                }

                            });
                        });</script>

                <?php else: ?>
                    <li class="videos-list__item_noitem">
                        <div style="margin-left: -15px;"> <?php echo $language_based_content['no_observations_completed_observation']; ?> </div>
                    </li>
                <?php endif; ?>
                <div class="clear">

                </div>

            </ul>

            <script type="text/javascript">
                var page = 2;
                function loadMoreObservations() {
                    $.ajax({
                        type: 'POST',
                        data: {
                            type: 'get_video_comments',
                            sort: $('#cmbVideoSort').val(),
                            page: page,
                            load_more: 1
                        },
                        dataType: 'json',
                        url: home_url + '/Huddles/getObVideoSearch/' + $('#txtHuddleID').val() + '/' + $('#txtSearchVideos').val(),
                        success: function(response) {
                            if (page == 0) {
                                $('#ob-videos-list').html(response.html);
                                $('#huddle_video_title_strong').html('<?php echo $language_based_content['video_number_videos']; ?> (' + $('#videos-list li.videos-list__item').length + ')');
                            } else {
                                var els = $(response.html);
                                els.appendTo($('#ob-videos-list'));//.hide().fadeIn('slow');
                                var top = els.eq(0).offset().top;
                                if (top > 0) {
                                    $('body').animate({scrollTop: top}, 500);
                                }
                                $('#huddle_video_title_strong').html('<?php echo $language_based_content['video_number_videos']; ?> (' + $('#videos-list li.videos-list__item').length + ')');
                            }
                            page++;
                        },
                        errors: function(response) {
                            alert(response.contents);
                        }

                    });
                }
                function hideLoadMore() {
                    $('.load_more').hide();
                }
            </script>
        </div>

        <?php
        print $this->element('load_more', array(
                    'total_items' => $totalVideos,
                    'count_items' => count($videos),
                    'current_page' => $current_page,
                    'number_per_page' => $video_per_page,
                    'load_more_what' => 'observations'
        ));
        ?>

    </div>
</div>
<div class="clear"></div>  
<div id="observations-2" class="observation_mainContainer" style="display: none;">
    <script type="text/javascript">
        var formSubmitting = false;
        var setFormSubmitting = function() {
            formSubmitting = true;
        };
    </script>
    <div class="observation_left" style="padding-top: 0px;">

        <div class="coach_cochee_container" style="margin-top:0px;">  
            <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/5' ?>" class="back huddle_videos_all">Back to all observations </a>           
            <div class="clear"></div>           
        </div>

        <div class="doted_container">
            <div id="ObservationTitle" name="click to edit title" style="font-weight: bold;cursor:pointer;">Observation_<?php echo date('m-d-Y'); ?></div>
            <div class="editArea">
                <input id="ajaxInput" value="Observation_note 12-12-1234" type="text">               
            </div>
            <input type="hidden"  name="video_id" value="" id="video_id"/>
        </div>

        <a  style="margin-left:450px; display:none;" id="show_hide" href="javascript:void(0);" class="back huddle_videos_all">Hide</a>
        <br>

        <div class="timer_container">
            <h3><?php echo date('M-d-Y'); ?></h3>
            <div class="timer_box">
                <span id="sw_h">00</span>:<span id="sw_m">00</span>:<span id="sw_s">00</span>               
            </div>
            <div class="start_btn_box">        
                <button type="button" id="sw_start" class="start_time_btn">Start</button> 
                <button type="button" id="sw_pause" class="pause_time_btn">Pause</button> 
                <button type="button" id="sw_stop" class="stop_time_btn">Stop</button>
            </div>
        </div>
        <input type="hidden" name="assessment_value" id="synchro_time_class_tags1">
        <?php if ($this->Custom->is_enable_tags($account_id)): ?>
            <?php if ($this->Custom->is_enable_huddle_tags($account_folder_id)): ?>

                <div class="divblockwidth" style="margin-top: 15px; display: none;" id="tag_div">
                    <?php
                    if (count($tags) > 0) {
                        $count = 1;
                        foreach ($tags as $tag) {
                            ?>
                            <a class="default_tag123 <?php echo defaulttagsclasses3($count) ?>" href="javascript:#" position_id="<?php echo $count; ?>" status_flag="0"># <?php echo $tag['AccountTag']['tag_title']; ?></a>
                            <?php
                            $count++;
                        }
                    }
                    ?>
                </div>   
            <?php endif; ?>
        <?php endif; ?>
        <div class="clear"></div>   
        <div id="note_box_container" class="note_box" style="display:none;">
            <div class="timer_box2" style="display: none;">00:00:00</div> 
            <div class="commentsOueterCls" style="margin-right: 1px;">
                <textarea cols="50" onkeyup="textAreaAdjust(this)" style="overflow:hidden;resize: none;border-radius:0px;margin-bottom: 5px;border: none;" id="observations-comments"  name="comment[comment]" placeholder="Add a comment..." rows="4"></textarea>
                <div class="petsCls">
                    <input style="" type="checkbox" id="press_enter_to_send" name="press_enter_to_send" myssss="<?php echo $press_enter_to_send; ?>" <?php echo $press_enter_to_send == '1' ? 'checked' : ''; ?>>
                    <label style="" for="press_enter_to_send">Press Enter to post</label>
                </div>
                <div class="clearfix"></div>
            </div>
            <!--<textarea disabled="disabled" cols="50" id="observations-comments" required name="comment[comment]" placeholder="Add a note..." rows="4" style="position: relative; outline: 0px; background: transparent;border: none;box-shadow: 0 0px 0px rgba(0,0,0,0.09) inset;" ></textarea>-->
            <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                    <div class="tags divblockwidth">
                        <div class="video-tags-row row">
                            <input type="text" name="txtVideostandard1" data-default="Tag Standard..." id="txtVideostandard1" value="" placeholder="" style="width:464px !important"  disabled="disabled"  required/>
                        </div>
                        <div class="clear" style="clear: both;"></div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <div class="tags divblockwidth">
                <div class="video-tags-row row">
                    <input type="text" name="txtVideoTags1" data-default="Tags..." id="txtVideoTags1" value="" placeholder=""  style="width:112px !important;" readonly="readonly" disabled="disabled"/>
                </div>
                <div class="clear" style="clear: both;"></div>
            </div>
            <div class="clear" style="clear: both;"></div>
        </div>
        <div id="add-notes-container" class="input-group" style="text-align:right;display:none;">
            <?php if ($this->Custom->is_enable_assessment($account_id) && $this->Custom->is_enabled_framework_and_standards($account_id) && $this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                <style>
                    .default_rating123{
                        max-width: 92px;
                        overflow: hidden;
                        text-overflow: ellipsis;
                        white-space: nowrap;
                    }
                </style>

                <div class="divblockwidth2" style="float: left;margin-bottom: 20px;text-align: right;" id="tag_div">
                    <?php
                    if (count($ratings) > 0) {
                        $count = 1;
                        foreach ($ratings as $tag) {
                            ?>
                            <a rel="tooltip" data-original-title="<?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?>" class="default_rating123 <?php echo defaulttagsclasses3($count) ?>" href="javascript:#" position_id="<?php echo $count; ?>" status_flag="0" value="<?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?>"># <?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?></a>
                            <?php
                            $count++;
                        }
                    }
                    ?>
                </div>  
                <div class="clear" style="clear: both;"></div> 
            <?php endif; ?>

            <input type="hidden" name="is_ob_started" id="is_ob_started" value="0">
            <input id="add-notes" type="submit" name="submit" value="Add Note" class="btn btn-green" style="height:35px;">
            <div class="clear" style="clear: both;"></div>
        </div> 
        <div class="clear" style="clear: both;"></div>        

    </div>
    <div class="observation_right" style="margin-top: -10px;">
        <div class="right-box2">
            <ul class="tabset commentstabs">
                <li class="">
                    <a id="comments" class="tab active" href="#commentsTab">
                        Notes (<?php echo $videoCommentsArray ? count($videoCommentsArray) : '0'; ?>)
                    </a>
                </li>
                <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                    <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                        <li class="">
                            <a id="frameWorkss" class="tab " href="#frameWorksTab">
                                Framework
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>

            </ul>
            <div class="clear"></div>
            <div id="commentsTab" style="visibility: visible;padding:10px 0px;"> 
                <div id="no_notes_added" class="no_notes">No Notes Added Yet</div>

                <div id="observation-comments"></div> </div>
            <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                    <div class="tab-content tab" id="frameWorksTab" style="visibility: visible;">
                        <div class="search-box standard-search" style="position: relative;width:100%;">
                            <input type="button" id="btnSearchTags" class="btn-search" value="">
                            <input class="text-input observation_search_bg" id="txtSearchTags2" type="text" value="" placeholder="Search Standards..." style="margin-right: 0px;width: 94% !important;">
                            <span id="clearTagsButton" class="clear-video-input-box" style="display:none;right: 42px;top: 20px;">X</span>
                        </div>
                        <div id="scrollbar1" style="float: left;">
                            <div class="viewport short" style="overflow:scroll;overflow-x: hidden;">
                                <div class="overview p-left0" style="top: 0px;padding: 0px;">
                                    <div id="listContainer">
                                        <style type="text/css">
                                            .standardRed {
                                                color: red;display: inline;
                                            }
                                            .standardBlue {
                                                color: blue;display: inline;
                                            }
                                            .standardBlack {
                                                color: #000;display: inline;
                                            }
                                            .standardOrange {
                                                color: orange;display: inline;
                                            }
                                            .standardGreen {
                                                color: green;display: inline;
                                            }
                                            .frame_work_heading{
                                                font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
                                                font-weight: 300 !important;
                                                font-size: 20px !important;
                                                margin:10px 0px !important;
                                                cursor:auto !important;
                                            }
                                        </style>
                                        <ul id="expList2" class="expList1" style="padding-left:0px">

                                            <?php
                                            if (!empty($standardsL2)) {
                                                for ($i = 0; $i < count($standardsL2); $i++) {
                                                    ?>
                                                    <li class="frame_work_heading" ><?php echo $standardsL2[$i]['AccountTag']['tag_code'] . ' - ' . $standardsL2[$i]['AccountTag']['tag_title']; ?></li>
                                                    <?php
                                                    foreach ($standards as $standard) {
                                                        if ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['parent_account_tag_id'] || ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['framework_id'])) {
                                                            ?>
                                                            <li class="standard standard-cls" >
                                                                <input class="check_class1" type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                                <?php
                                                                $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                                echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                                ?>                                        
                                                            </li>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (!empty($standards)) {
                                                    foreach ($standards as $standard) {
                                                        ?>
                                                        <li class="standard standard-cls">
                                                            <input class="check_class1" type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                            <?php
                                                            $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                            echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                            ?>                                        
                                                        </li>

                                                    <?php
                                                    }
                                                }
                                            }
                                            ?> 
                                            <li id="noresults" style="width: 280px;">No standards match your search criteria.</li>                             
                                        </ul>
                                        <script type="text/javascript">

        $(document).ready(function() {
//            $('.standard input').on('change', function() {
//                var tag_code = $(this).attr('st_code');
//                var tag_value = '';
//                var tag_array = {};
//                var tag_name = $(this).attr('st_name');
//                tag_array = tag_name.split(" ");
//                if ($(this).is(':checked')) {
//                    tag_array = tag_name.split(" ");
//                    tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
//                    $('#txtVideostandard').addTag(tag_value);
//                    if ($('input[name="name1"]:checked').length > 0) {
//                        $('#txtVideostandard_tag').hide();
//                    }
//                } else {
//                    tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
//                    $('#txtVideostandard').removeTag(tag_value);
//                }
//            });
        });</script>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
    <?php endif; ?>
<?php endif; ?> 
        </div>
    </div>
    <div class="clear"></div>  
</div>
<div class="clear"></div>
<div id="observations-7" class="observation_mainContainer" style="display: none;">
    <style>
        .stop_observation_container{
            margin: 10px;
        }
        .printable_icons {
            border-bottom: solid 1px #E0E0E0;
            margin-bottom: 15px;
            padding:0px 0px 10px 0px;
        }
    </style>



    <div class="stop_observation_container">
        <div class="coach_cochee_container" >
            <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/5' ?>" class="back huddle_videos_all">Back to all observations</a>            
            <div class="clear"></div>           
        </div>


        <div id="commentsTab1" style="visibility: visible;">

            <div id="ObservationTitle1" name="click to edit title" style="font-weight: bold;width: 410px;">
                Observation_note_<?php //echo date('m-d-Y');  ?>
            </div>  
            <div class="call_notes_container" id="stop-call-notes">

            </div></div>
    </div> 
    <div class="right-box" style="display:none;">


<?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
    <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                <div class="tab-content tab" id="frameWorksTab1" style="visibility: visible;">
                    <div class="search-box standard-search" style="position: relative;width:100%;">
                        <input type="button" id="btnSearchTags" class="btn-search" value="">
                        <input class="text-input observation_search_bg" id="txtSearchTags" type="text" value="" placeholder="Search Standards..." style="margin-right: 0px;width: 94% !important;">
                        <span id="clearTagsButton" class="clear-video-input-box" style="display:none;right: 33px;top: 20px;">X</span>
                    </div>
                    <div id="scrollbar1" style="float: left;">
                        <div class="viewport short" style="overflow:scroll;overflow-x: hidden;">
                            <div class="overview p-left0" style="top: 0px;padding: 0px;">
                                <div id="listContainer">
                                    <style type="text/css">
                                        .standardRed {
                                            color: red;display: inline;
                                        }
                                        .standardBlue {
                                            color: blue;display: inline;
                                        }
                                        .standardBlack {
                                            color: #000;display: inline;
                                        }
                                        .standardOrange {
                                            color: orange;display: inline;
                                        }
                                        .standardGreen {
                                            color: green;display: inline;
                                        }
                                        .frame_work_heading{
                                            font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
                                            font-weight: 300 !important;
                                            font-size: 20px !important;
                                            margin:10px 0px !important;
                                            cursor:auto !important;
                                        }
                                    </style>
                                    <ul id="expList1" class="expList1" style="padding-left:0px">

                                        <?php
                                        if (!empty($standardsL2)) {
                                            for ($i = 0; $i < count($standardsL2); $i++) {
                                                ?>
                                                <li class="frame_work_heading" ><?php echo $standardsL2[$i]['AccountTag']['tag_code'] . ' - ' . $standardsL2[$i]['AccountTag']['tag_title']; ?></li>
                                                <?php
                                                foreach ($standards as $standard) {
                                                    if ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['parent_account_tag_id']) {
                                                        ?>
                                                        <li class="standard standard-cls" >
                                                            <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                            <?php
                                                            $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                            echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                            ?>                                        
                                                        </li>

                                                        <?php
                                                    }
                                                }
                                            }
                                        } else {
                                            foreach ($standards as $standard) {
                                                ?>
                                                <li class="standard standard-cls">
                                                    <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                    <?php
                                                    $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                    echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                    ?>                                        
                                                </li>

            <?php }
        }
        ?> 
                                        <li id="noresults" style="width: 280px;">No standards match your search criteria.</li>                             
                                    </ul>
                                    <script type="text/javascript">

                                        $(document).ready(function() {
//                                            $('.standard input').on('change', function() {
//                                                var tag_code = $(this).attr('st_code');
//                                                var tag_value = '';
//                                                var tag_array = {};
//                                                var tag_name = $(this).attr('st_name');
//                                                tag_array = tag_name.split(" ");
//                                                if ($(this).is(':checked')) {
//                                                    tag_array = tag_name.split(" ");
//                                                    tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
//                                                    $('#txtVideostandard1').addTag(tag_value);
//                                                    if ($('input[name="name1"]:checked').length > 0) {
//                                                        $('#txtVideostandard1_tag').hide();
//                                                    }
//                                                } else {
//                                                    tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
//                                                    $('#txtVideostandard1').removeTag(tag_value);
//                                                }
//
//                                            });
                                        });</script>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
    <?php endif; ?>
<?php endif; ?> 
    </div>
    <div class="clear"></div> 
</div>
<div class="clear"></div> 

<script type="text/javascript">
    var time_not_saved = true; // initially, it is saved (no action has been done)

    window.onbeforeunload = confirmExit;
    function confirmExit() {
        if (!time_not_saved) {
            return "You did not save, do you want to do it now?";
        }
    }
</script>

<script>
    $(document).ready(function() {
        $('#clearObVideoButton').click(function() {

            $('#clearObVideoButton').hide();

        });

        /*document.getElementById("txtVideostandard1_tagsinput").addEventListener("click", ActivateFramework);
         
         function ActivateFramework() {
         $('#frameWorkss').trigger('click');
         }*/


        $("#show_hide").click(function() {

            if ($("#show_hide").html() == 'Hide')
            {
                $("#show_hide").html('Show');
            }
            else
            {
                $("#show_hide").html('Hide');
            }

            $(".timer_container").slideToggle("slow", function() {
                // Animation complete.
            });
        });


        function load_huddles_videos(is_stop_request) {
            $.ajax({
                type: "POST",
                url: home_url + "/huddles/get_huddle_document",
                dataType: 'json',
                data: {
                    huddle_id: $('#huddle_id').val(),
                    video_id: $('#video_id').val(),
                    is_stop: false
                },
                success: function(response) {
                    $('#observations-7').html(response.model);
                    $('.pause_time_btn').hide();
                    $('#sw_start').show();
                    $('#observations-2').slideUp();
                    $('#observations-7').slideDown();
                    $('#stop-call-notes').html($('#observation-comments').html());
                    $('#ObservationTitle1').text($('#ObservationTitle').text());
                    $('.printable_icons').remove();
                },
                error: function(e) {

                }
            });
        }

        $('#observations-comments').keypress(function(e) {
            $hours = $('#sw_h').text();
            $min = $('#sw_m').text();
            $sec = $('#sw_s').text();
            $time = $hours + ':' + $min + ':' + $sec;
            if ($('#observations-comments').val() == '') {
                $('.timer_box2').text($time);
                $('.timer_box2').show();

            }
            $(this).css('border', '0');
            $(this).css('border-radius', '0');
        });
        var fieldWidth = parseInt($("#ObservationTitle").css("width"));
        // $("#ajaxInput").css("width", (fieldWidth - 50) + "px");
        // $(".editArea").css("width", (fieldWidth) + "px");
        $("#ObservationTitle").mouseover(function() {
            $(this).css("backgroundColor", "#FAFABE")
        });
        $("#ObservationTitle").mouseout(function() {
            $(this).css("backgroundColor", "#ffffff");
        });
        $("#ObservationTitle").click(function() {
            $(this).css("display", "none");
            $(".editArea").css("display", "inline-block");
            $("#ajaxInput").val($(this).text().trim());
            $("#ajaxInput").focus();
        });
        $("#ajaxInput").blur(function() {

            $(".editArea").css("display", "none");
            $("#ObservationTitle").css("display", "inline-block");
            $("#ObservationTitle").text($(this).val());
            $.ajax({
                type: "POST",
                url: home_url + "/huddles/update_note",
                data: {
                    huddle_id: $('#huddle_id').val(),
                    video_id: $('#video_id').val(),
                    title: $("#ObservationTitle").text()
                },
                success: function(response) {
                    $("#ObservationTitle1").html($("#ObservationTitle").text())
                },
                error: function(e) {

                }
            });


        });
        $(document).on('click', '#select-from-workspace', function(e) {
            e.preventDefault();
            $('.video_upload_dialog').slideUp();
            $('.select-from-workspace').slideDown();
            $('.select-from-huddle').slideUp();
            $('#add_video').show();
        });
        $(document).on('click', '#select-from-huddle', function(e) {
            e.preventDefault();
            $('.video_upload_dialog').slideUp();
            $('.select-from-huddle').slideDown();
            $('.select-from-workspace').slideUp();
            $('#add_video').show();
        });
        $(document).on('click', '#back-to-select-container', function(e) {
            e.preventDefault();
            $('.select-from-workspace').slideUp();
            $('.select-from-huddle').slideUp();
            $('#add_video').hide();
            $('.video_upload_dialog').slideDown();
        });
        $(document).on('click', '.select-video-btn', function(e) {
            $('#movefolderto').modal('hide');
            $('#name-container').hide();
            $('#publish-button-container').show();
        });
        $('.pause_time_btn').hide();
        $('.stop_time_btn').hide();
        function create_empty_record() {
            $.ajax({
                type: "POST",
                url: home_url + "/Huddles/add_call_notes",
                dataType: 'json',
                data: {
                    observation_title: $('#ObservationTitle').text(),
                    observations_comments: $('#observations-comments').val(),
                    observations_standards: $('#txtVideostandard1').val(),
                    observations_tags: $('#txtVideoTags1').val(),
                    observations_time: $('.timer_box2').text(),
                    account_id: $('#account_id').val(),
                    user_id: $('#user_id').val(),
                    huddle_id: $('#huddle_id').val(),
                    video_id: ''
                },
                success: function(response) {
//                    console.log(response.comments);
                    var huddle_id =$('#huddle_id').val();
                    var video_id = response.document_id;
                    window.location.href= home_url + "/video_details/scripted_observations/" + huddle_id + "/" + video_id ; 
                    $("ul#expList2").find('input:checkbox').removeAttr('checked');
                    $('#observations-comments').val('');
                    $('#txtVideoTags1_tagsinput span').remove();
                    $('#txtVideostandard1_tagsinput span').remove();
                    $('#txtVideostandard1_tag').show();
                    $('.timer_box2').text('00:00:00');
                    $('.timer_box2').hide();
                    $('#observation-comments').html(response.comments);
                    $('#video_id').val(response.document_id);
                    $('#txtVideoTags1').removeAllTag();
                    $('#txtVideostandard1').removeAllTag();
                    //$("#expList2").find('input:checkbox').removeAttr('checked');
                    $('#observations-comments').attr("placeholder", "Add a comment...");
                    $('#observations-comments').addClass("your-class");
                    
                    
                },
                error: function(e) {

                }
            });
        }
        (function($) {
            $.extend({
                APP: {
                    formatTimer: function(a) {
                        if (a < 10) {
                            a = '0' + a;
                        }
                        return a;
                    },
                    startTimer: function(dir) {

                        var a;
                        // save type
                        $.APP.dir = dir;
                        // get current date
                        $.APP.d1 = new Date();
                        switch ($.APP.state) {

                            case 'pause' :

                                // resume timer
                                // get current timestamp (for calculations) and
                                // substract time difference between pause and now
                                $.APP.t1 = $.APP.d1.getTime() - $.APP.td;
                                break;
                            default :

                                // get current timestamp (for calculations)
                                $.APP.t1 = $.APP.d1.getTime();
                                // if countdown add ms based on seconds in textfield
                                if ($.APP.dir === 'cd') {
                                    $.APP.t1 += parseInt($('#cd_seconds').val()) * 1000;
                                }

                                break;
                        }

                        // reset state
                        $.APP.state = 'alive';
                        $('#' + $.APP.dir + '_status').html('Running');
                        // start loop
                        $.APP.loopTimer();
                    },
                    pauseTimer: function() {

                        // save timestamp of pause
                        $.APP.dp = new Date();
                        $.APP.tp = $.APP.dp.getTime();
                        // save elapsed time (until pause)
                        $.APP.td = $.APP.tp - $.APP.t1;
                        // change button value
                        $('#' + $.APP.dir + '_start').val('Resume');
                        // set state
                        $.APP.state = 'pause';
                        $('#' + $.APP.dir + '_status').html('Paused');
                    },
                    stopTimer: function() {

                        // change button value
                        $('#' + $.APP.dir + '_start').val('Restart');
                        // set state
                        $.APP.state = 'stop';
                        $('#' + $.APP.dir + '_status').html('Stopped');
                    },
                    resetTimer: function() {

                        // reset display
                        $('#' + $.APP.dir + '_ms,#' + $.APP.dir + '_s,#' + $.APP.dir + '_m,#' + $.APP.dir + '_h').html('00');
                        // change button value
                        $('#' + $.APP.dir + '_start').val('Start');
                        // set state
                        $.APP.state = 'reset';
                        $('#' + $.APP.dir + '_status').html('Reset & Idle again');
                    },
                    endTimer: function(callback) {

                        // change button value
                        $('#' + $.APP.dir + '_start').val('Restart');
                        // set state
                        $.APP.state = 'end';
                        // invoke callback
                        if (typeof callback === 'function') {
                            callback();
                        }

                    },
                    loopTimer: function() {

                        var td;
                        var d2, t2;
                        var ms = 0;
                        var s = 0;
                        var m = 0;
                        var h = 0;
                        if ($.APP.state === 'alive') {

                            // get current date and convert it into 
                            // timestamp for calculations
                            d2 = new Date();
                            t2 = d2.getTime();
                            // calculate time difference between
                            // initial and current timestamp
                            if ($.APP.dir === 'sw') {
                                td = t2 - $.APP.t1;
                                // reversed if countdown
                            } else {
                                td = $.APP.t1 - t2;
                                if (td <= 0) {
                                    // if time difference is 0 end countdown
                                    $.APP.endTimer(function() {
                                        $.APP.resetTimer();
                                        $('#' + $.APP.dir + '_status').html('Ended & Reset');
                                    });
                                }
                            }

                            // calculate milliseconds
                            ms = td % 1000;
                            if (ms < 1) {
                                ms = 0;
                            } else {
                                // calculate seconds
                                s = (td - ms) / 1000;
                                if (s < 1) {
                                    s = 0;
                                } else {
                                    // calculate minutes   
                                    var m = (s - (s % 60)) / 60;
                                    if (m < 1) {
                                        m = 0;
                                    } else {
                                        // calculate hours
                                        var h = (m - (m % 60)) / 60;
                                        if (h < 1) {
                                            h = 0;
                                        }
                                    }
                                }
                            }

                            // substract elapsed minutes & hours
                            ms = Math.round(ms / 100);
                            s = s - (m * 60);
                            m = m - (h * 60);
                            // update display
                            //$('#' + $.APP.dir + '_ms').html($.APP.formatTimer(ms));
                            $('#' + $.APP.dir + '_s').html($.APP.formatTimer(s));
                            $('#' + $.APP.dir + '_m').html($.APP.formatTimer(m));
                            $('#' + $.APP.dir + '_h').html($.APP.formatTimer(h));
                            // loop
                            $.APP.t = setTimeout($.APP.loopTimer, 1);
                        } else {

                            // kill loop
                            clearTimeout($.APP.t);
                            return true;
                        }

                    }

                }

            });
            $(document).on('click', '#sw_start', function() {

                $.APP.startTimer('sw');
                $(this).hide();
                $('.pause_time_btn').show();
                $('.stop_time_btn').show();
                $('#show_hide').show();
                $('#add-notes-container').show();
                $('#note_box_container').show();
                $('#tag_div').show();
                $('#is_ob_started').val('1');
                $('#txtVideoTags1_tag').removeAttr('disabled');
                $('#txtVideostandard1_tag').removeAttr('disabled');
                $('#observations-comments').removeAttr('disabled');
                // create_empty_record();
                time_not_saved = false;
            });
            $(document).on('click', '#cd_start', function() {
                $.APP.startTimer('cd');

            });
            $(document).on('click', '#sw_stop,#cd_stop', function() {
                if (confirm("Are you sure you want to stop this Observation?")) {
                    <?php //if(IS_QA): ?>
                        var metadata = {
                        observation_completed: true,
                        user_role : '<?php echo $user_role; ?>',
                        is_in_trial : '<?php echo $in_trial_intercom; ?>',
                        Platform : 'Web'

                      };
                      Intercom('trackEvent', 'observation-completed', metadata);
                      <?php //endif; ?>
                      $hours = parseInt($('#sw_h').text());
                      $min = parseInt($('#sw_m').text());
                      $sec = parseInt($('#sw_s').text());
                      
                    var final_time = ($hours*60*60)+($min*60)+($sec);
                    var video_id = $('#video_id').val();
                    
                      $.ajax({
                type: 'POST',
                url: home_url + '/Huddles/update_scripted_observation_duration/' + video_id ,
                data: {
                    duration: final_time,
                  
                },
                success: function(res) {
                },
                errors: function(response) {

                }
            });

                    $.APP.stopTimer();
                    $.APP.resetTimer();
                    $(this).hide();
                    is_stop_request = true;

                    time_not_saved = true;
                    location.href = '<?php echo $this->base . '/Huddles/observation_details_1/'; ?>' + $('#huddle_id').val() + '/' + $('#video_id').val() + '/1';
                    return;

                    $('#observations-2').hide();
                    $('#is_ob_started').val('0');
                    $('#stop-call-notes').html($('#observation-comments').html());
                    $('#observation-comments').remove();
                    $('#observations-7').show();
                    $('.printable_icons').show();
                    $('.callNotes_actionBox').hide();
                    $('.crossBtn').hide();
                    $('.comment-edit').hide();
                    $('.comment-delete').hide();
                    $('.docs-standards').addClass('bubble_adjs');
                    //$('.crossBtn').attr('src', '');



//                        load_huddles_videos(is_stop_request);
                }
                else {
                    return false;
                }
            });
            $(document).on('click', '#sw_reset,#cd_reset', function() {
                $.APP.resetTimer();
            });
            $(document).on('click', '#sw_pause,#cd_pause', function() {
                $('#sw_start').removeClass('start_time_btn');
                $('#sw_start').addClass('start_time_sml_btn');
                $('#sw_start').show();
                $(this).hide();
                $.APP.pauseTimer();
            });
        })(jQuery);

        $(document).on('click', '#gObservationStart', function(e) {
//            $.APP.resetTimer();
//            $('#sw_start').show();
//            $('#sw_pause').hide();
//            $('#sw_stop').hide();
//            e.preventDefault();
//            $('.tabset').hide();
//            $('.commentstabs').show();
//            $('#video_id').val('');
//            $('.action-buttons').hide();
//            $('.btn-box').hide();
//            $('#observations-1').slideUp();
//            $('#observations-2').slideDown();
//            $('#txtVideoTags1_tag').attr('disabled', 'disabled');
//            $('#txtVideostandard1_tag').attr('disabled', 'disabled');
//            $('#observations-comments').attr('disabled', 'disabled');
//            $('#observatons-added-notes').hide();
//            $("#is_ob_started").val('1');
            create_empty_record();

        });
        $(document).on('click', '.ob-details-view', function(e) {
            e.preventDefault();
            document_id = $(this).attr('data-document-id');
            load_details(document_id);

        });
        $('.default_tag123').on('click', function(e) {
            e.preventDefault();
            var posid = '';
            tag_name = $(this).text();
            tag_name.replace(/\s/g, "");

            posid = $(this).attr('position_id');
            //$( ".default_tag" ).addClass(defaulttagsclasses(0));
            //var numItems = $('.yourclass').length;
            $('.default_tag123').each(function(index) {
                var tag_name1 = $(this).text();
                tag_name1.replace(/\s/g, "");
                $('#txtVideoTags1').removeTag(tag_name1);
                if ($(this).hasClass(defaulttagsclasses3(index))) {
                    $('#txtVideoTags1').removeTag(tag_name1);
                    $(this).removeClass(defaulttagsclasses3(index) + 'bg');
                }
            });
            $('.default_tag123').each(function(index) {
                if ($(this).attr('position_id') != posid) {
                    $(this).removeClass(defaulttagsclasses3($(this).attr('position_id')));
                    $(this).addClass(defaulttagsclasses3(0));

                }
            });

            if ($(this).hasClass(defaulttagsclasses3(posid))) {
                $('#txtVideoTags1').removeTag(tag_name);
                $(this).addClass(defaulttagsclasses3(0));
                $(this).removeClass(defaulttagsclasses3(posid));
                $(this).attr('status_flag', '0');
                $('#comment_comment').attr("placeholder", "Add a comment");
                $('#synchro_time_class').val('');
            }
            else {
                $('#txtVideoTags1').addTag(tag_name);
                $(this).addClass(defaulttagsclasses3(posid));
                $(this).removeClass(defaulttagsclasses3(0));
                $(this).attr('status_flag', '1')
                $('#comment_comment').focus();
                //var spt=$(this).text().split(" ");
                var spt = $(this).text().slice(2);
                $('#comment_comment').attr("placeholder", "Add " + spt);
                $('#synchro_time_class').val('short_tag_' + posid);
            }
        });

        $('.default_rating123').on('click', function(e) {
            e.preventDefault();
            var posid = '';
            tag_name = $(this).text();
            tag_name.replace(/\s/g, "");
            tag_name_mod = tag_name.replace('# ', '');

            posid = $(this).attr('position_id');
            $('.default_rating123').each(function(index) {
                var tag_name1 = $(this).text();
                tag_name1.replace(/\s/g, "");
                $('#txtVideoTags').removeTag(tag_name1);
                if ($(this).hasClass(defaulttagsclasses3(index))) {
                    $('#txtVideoTags').removeTag(tag_name1);
                    $(this).removeClass(defaulttagsclasses3(index) + 'bg');
                }
            });
            $('.default_rating123').each(function(index) {
                if ($(this).attr('position_id') != posid) {
                    $(this).removeClass(defaulttagsclasses3($(this).attr('position_id')));
                    $(this).addClass(defaulttagsclasses3(0));

                }
            });

            if ($(this).hasClass(defaulttagsclasses3(posid))) {
                $('#txtVideoTags').removeTag(tag_name);
                $(this).addClass(defaulttagsclasses3(0));
                $(this).removeClass(defaulttagsclasses3(posid));
                $(this).attr('status_flag', '0');
                $('#comment_comment').attr("placeholder", "Add a comment");
                $('#synchro_time_class').val('');
                $('#synchro_time_class_tags1').val('');
            }
            else {
                //$('#txtVideoTags').addTag(tag_name);
                $(this).addClass(defaulttagsclasses3(posid));
                $(this).removeClass(defaulttagsclasses3(0));
                $(this).attr('status_flag', '1');
                //$('#comment_comment').focus();
                //var spt=$(this).text().split(" ");
                //var spt = $(this).text().slice(2);
                //$('#comment_comment').attr("placeholder", "Add " + spt);
                $('#synchro_time_class_tags1').val(tag_name_mod);
                $('#synchro_time_class').val('short_tag_' + posid);
            }
        });

    });
    function defaulttagsclasses3(posid) {
        var class_name = 'tags_qucls';
        if (posid == 1) {
            class_name = 'tags_quclsbg';
        }
        else if (posid == 2) {
            class_name = 'tags_sugclsbg';
        }
        else if (posid == 3) {
            class_name = 'tags_notesclsbg';
        }
        else if (posid == 4) {
            class_name = 'tags_strangthclsbg';
        }
        else if (posid == 5) {
            class_name = 'tags_strangthclsbg';
        }
        return class_name;
    }

    function defaulttagsclasses1(posid) {
        var class_name = 'tags_qucls';
        if (posid == 1) {
            class_name = 'tags_qucls';
        }
        else if (posid == 2) {
            class_name = 'tags_sugcls';
        }
        else if (posid == 3) {
            class_name = 'tags_notescls';
        }
        else if (posid == 4) {
            class_name = 'tags_strangthcls';
        }
        else if (posid == 5) {
            class_name = 'tags_strangthcls';
        }
        return class_name;
    }


    $("#txtSearchTags2").on("keypress", function(e) {
        $("#clearTagsButton").show();

    });

    $(document).on('click', '#clearTagsButton', function(e) {
        $("input#txtSearchTags2").val('');
        $("#clearTagsButton").hide();

        $('input#txtSearchTags2').quicksearch('ul#expList2 li', {
            noResults: 'li#noresults',
            'onAfter': function() {
                if (typeof $('#txtSearchTags2').val() !== "undefined" && $('#txtSearchTags2').val().length > 0) {
                    $('#clearTagsButton').css('display', 'block');
                }
            }
        });

    });
    $(document).ready(function() {
        $('#press_enter_to_send').click(function() {
            var value = $("#press_enter_to_send").is(':checked') ? 1 : 0;
            var user_id = "<?php echo $user_id; ?>";
            var account_id = "<?php echo $account_id; ?>";
            $.ajax({
                type: 'POST',
                url: home_url + '/Huddles/press_enter_to_send/',
                data: {
                    value: value,
                    user_id: user_id,
                    account_id: account_id
                },
                success: function(res) {
                },
                errors: function(response) {

                }
            });
        });
        
        $('input.check_class1').on('change', function() {


          //  $('input.check_class1').not(this).prop('checked', false);

            $(this).each(function() {
                //       var sThisVal = (this.checked ? $(this).val() : "");
                var tag_code = $(this).attr('st_code');
                var tag_value = '';
                var tag_array = {};
                var tag_name = $(this).attr('st_name');
                tag_array = tag_name.split(" ");
                if ($(this).is(':checked')) {
                    tag_array = tag_name.split(" ");
                    tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                    $('#txtVideostandard1').addTag(tag_value);
                    //$('#txtVideostandard_vid_tag').remove();
                    //                                                            $('#txtVideostandard_vid_tag').hide();
                    if ($('input[name="name1"]:checked').length > 0) {
                        $('#txtVideostandard1_tag').hide();
                    }
                } else {
                    tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                    $('#txtVideostandard1').removeTag(tag_value);
                    //    $('#txtVideostandard_vid_tag').show();
                        
                }
            });




        });

        
    });
    function textAreaAdjust(o) {
        o.style.height = "1px";
        o.style.minHeight = "75px";
        o.style.height = (25 + o.scrollHeight) + "px";
        o.style.maxHeight = (5 + o.scrollHeight) + "px";
//                            alert(o.parentElement.style.minHeight);
//                            o.parentElement.style.height = (5 + parseInt(o.parentElement.style.minHeight)) + "px";
    }
    function nl2br(str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }

</script>

