<style>
    #remove_file_names .remove
    {
        display:block !important;
    }

</style>
<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>  
<?php
$users = $this->Session->read('user_current_account');
$user_name = $users['User']['first_name'] . ' ' . $users['User']['last_name'];

$total_participants_count = 0;
$predefined_users_ids = '';

$huddle = $huddle[0];

if (isset($huddles_users)) {
    for ($i = 0; $i < count($huddles_users); $i++) {
        $user = $huddles_users[$i];

        if ($users['User']['id'] == $user['huddle_users']['user_id'])
            continue;

        if (!empty($predefined_users_ids))
            $predefined_users_ids .= ',';
        $predefined_users_ids .= $user['huddle_users']['user_id'];
        $total_participants_count += 1;
    }
}
?>
<li class="border-none last-textarea" style="width: 100%; padding-left: 70px;">
    <div class="span1" style="margin: 0;">

        <?php
        $last_notifiers = array();
        if (is_array($discussionReply) && count($discussionReply) > 0) {
            if (!empty($discussionReply[0]['Comment']['notification_user_ids'])) {
                $last_notifiers = explode(',', $discussionReply[0]['comments']['notification_user_ids']);
            }
        } else {
            $parentDiscussion = $Comment->get($detail_id);
            $CommentUsers = $Comment->getUsers($detail_id);
            if (count($CommentUsers) > 0) {
                for ($i = 0; $i < count($CommentUsers); $i++) {
                    $last_notifiers[] = $CommentUsers[$i]['CommentUserUser']['id'];
                }
            }
        }


        $users = $this->Session->read('user_current_account');
        $profile_image = isset($users['User']['image']) ? $users['User']['image'] : '';
        $profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";

        //if (file_exists($profile_image_real_path) != FALSE && $profile_image != '') {
        if ($profile_image != '') {
            //$profile_image = $this->webroot . "img/users/$profile_image";
            $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
        } else {
            //$profile_image = $this->webroot . "img/profile.jpg";
            $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/profile.jpg');
        }
        ?>

        <img src="<?php echo $profile_image; ?>" alt="img" height="42px" width="42px" class='circle'/>
    </div>
    <div class="text" style="width: 92%;border: 2px solid #c5c5c5; border-radius:5px;" >
        <form action="<?php echo $this->base . '/Huddles/comments_ajax/' . $huddle_id . '/3/' . $detail_id ?>" name="comments" method="post" enctype="multipart/form-data" id="new_comment_reply" onsubmit=""
              method="post" target="">
            <div class="inner discussion-editor-row" style="padding: 10px;">
                <input id="tokentag" type="hidden" name="data[user_id]" value="<?php echo $user_id ?>" />
                <input id="tokentag" type="hidden" name="data[commentable_id]" value="<?php echo $detail_id ?>" />
                <input id="comment_html_3" type="hidden" name="data[comment]" value="" />
                <input id="comment_access_level" name="data[access_level]" type="hidden" value="admins" />
                <div class="input-group">
                    <div id="editor2-toolbar" class="editor-toolbar" style="display: none;">
                        <a data-wysihtml5-command="bold">bold</a>
                        <a data-wysihtml5-command="italic">italic</a>
                        <a data-wysihtml5-command="insertOrderedList">ol</a>
                        <a data-wysihtml5-command="insertUnorderedList">ul</a>
                        <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                    </div>
                    <?php echo $this->Form->textarea('comment_editor', array('id' => 'editor-3', 'class' => 'editor-textarea unique_text_class', 'cols' => '80', 'rows' => '3', 'placeholder' => $language_based_content['type_message_here_discussion'], 'style="width:800px; border-style:none;border-color:none;border-radius:none;"')); ?>
                    <?php echo $this->Form->error('videoHuddle.message'); ?>
                </div>
                <div id="attachment-file-group" class="input-group">
                    <a href="#attachment-file-group" class="btn btn-white btn-light btn-gray-text icon2-clip" data-remote="true" id="attachment-file_2"><?php echo $language_based_content['attach_file_discussion']; ?></a>
                </div>

                <div class="input-group " id="browse-attachment_2" style="display: none;">
                    <ul id ="remove_file_names" class="js-attachment-list"></ul>
                    <div class='clearfix'></div>
                    <input class="attachment" id="comment_attachment" name="data[attachment][]" placeholder="attach file" type="file" />
                </div>


                <!--                <div class="input-group" id="browse-attachment" style="display: none;">
                                    <ul id="attachment_list" class="js-attachment-list"></ul>
                                    <div class='clearfix'></div>
                                    <input class="attachment" name="data[attachment][]" placeholder="attach file" type="file" />
                                </div>-->
                <div class="input-group">
                    <label><input type="radio" id="rdoPreDefined" value="1" name="send_email" style="margin-right: 10px;" checked/><?php echo $language_based_content['post_this_message_discussion']; ?></label>
                </div>
                <div class="input-group">
                    <label><input type="radio" id="rdoLetMeChoose" value="2" name="send_email" style="margin-right: 10px;"/><?php echo $language_based_content['let_me_choose_discussion']; ?></label>
                </div>

                <div class="input-group" style="display: none;" id="users-list">
                    <div class="email-to" id="user_list">
                        <table>
                            <?php
                            $user_current_account = $this->Session->read('user_current_account');
                            if ($huddles_users):
                                ?>
                                <tr>
                                    <?php
                                    $count = 1;
                                    foreach ($huddles_users as $supper):
                                        $tr = '';
                                        if ($supper['huddle_users']['role_id'] == '220') {
                                            continue;
                                        }

                                        if ($user_current_account['User']['id'] == $supper['huddle_users']['user_id']) {
                                            continue;
                                        }
                                        if ($count % 4 == 0) {
                                            $tr = '</tr><tr>';
                                        }
                                        ?>
                                        <td style="width: 150px;">
                                            <label class="ui-checkbox checkbox-2">
                                                <input class="user_checkbox" id="comment_notification_user_ids_reply_<?php echo $supper['huddle_users']['user_id'] ?>" name="chk_notification_user_ids" type="checkbox" value="<?php echo $supper['huddle_users']['user_id'] ?>" />
                                            </label>
                                            <label for="comment_notification_user_ids_<?php echo $supper['huddle_users']['user_id'] ?>"><?php echo strlen($supper['User']['first_name'] . ' ' . $supper['User']['last_name']) >= 16 ? mb_substr($supper['User']['first_name'] . ' ' . $supper['User']['last_name'], 0, 14) . '...' : $supper['User']['first_name'] . ' ' . $supper['User']['last_name'] ?></label>
                                        </td>
                                        <?php echo $tr; ?>
                                        <?php $count++; ?>
                                    <?php endforeach; ?>
                                </tr>

                                <div class='clearfix'></div>
                                <?php if ($huddles_users): ?>
                                    <p class="check-buttons" style="margin-left: 10px;">
                                        <a class="" id="reply_check_none" style="margin-right: 10px; text-decoration: underline;" href="javascript:uncheckAllUsers('.user_checkbox')">Select None</a>
                                        <a class="" id="reply_check_all" style="margin-right: 10px; text-decoration: underline;" href="javascript:checkAllUsers('.user_checkbox')">Select All</a>
                                    </p>
                                <?php endif; ?>

                            </table>
                        <?php endif; ?>
                    </div>
                    <div class='clearfix'></div>
                </div>
                <div class='clearfix'></div>
                <div class="input-group">
                    <label><input type="radio" id="rdoNoneSelected" value="3" name="send_email" style="margin-right: 10px;"/><?php echo $language_based_content['dont_let_email_discussion']; ?></label>
                </div>
                <p style="margin-top:5px;margin-top: 5px !important;">
                    <input id="add-discussion-reply" class="btn btn-green inline" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" name="commit" type="button"
                           value="<?php echo $language_based_content['add_comment_discussion']; ?>" />
                    <a id="attachment_cancel" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/3' ?>"  class="btn btn-transparent"><?php echo $language_based_content['cancel_discussion']; ?></a>
                </p>
            </div>
            <input id="txtPreDefinedNotifiers" type="hidden" value="<?php echo $predefined_users_ids; ?>" />
            <input name="data[notification_user_ids][]" id="txtNotifications" type="hidden" value="0" />
        </form>
    </div>
</li>

<script type="text/javascript">
    $("#attachment-file").click(function () {
        $("#browse-attachment").show();
        $(this).hide();
    });


    $("#attachment-file_2").click(function () {
        $("#browse-attachment_2").show();
        $(this).hide();
    });


    $("#add-discussion-reply").click(function () {

        //  postDiscussionReply();

        //  var text_value = $(this).closest("editor-textarea").text();
        // var myIFrame = document.getElementsByClassName("unique_text_class");
        var text_value = $('ul > li.last-textarea > div').eq(1).find('iframe').contents().find('.wysihtml5-editor').html();
        var full_name = '<?php echo $user_name; ?>';




        var html = '<li class="replaceable_li">';
        html += '<div style="margin: 0px;" class="span1">';
        html += '<img src="/img/profile.jpg" alt="img" height="42px" width="42px" class="circle">';
        html += '</div>';
        html += '<div class="text text-width span10">';
        html += '<div class="discussion-editor-row" style="padding-top:0px;">';
        html += '<label class="discussion-username">' + full_name + '</label>';
        html += '<div class="discussion-editor-row-content view-controls">' + text_value + '</div>';
        html += '<div class="clearfix"></div>';
        html += '<div class="input-group edit-controls" style="display:none;">';
        html += '<div id="editor7994-toolbar" class="editor-toolbar">';
        html += '<a data-wysihtml5-command="bold" href="javascript:;" unselectable="on">bold</a>';
        html += '<a data-wysihtml5-command="italic" href="javascript:;" unselectable="on">italic</a>';
        html += '<a data-wysihtml5-command="insertOrderedList" href="javascript:;" unselectable="on">ol</a>';
        html += '<a data-wysihtml5-command="insertUnorderedList" href="javascript:;" unselectable="on">ul</a>';
        html += '<a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote" href="javascript:;" unselectable="on">quote</a>';
        html += '</div>';
        html += '<div class="textoverlay-wrapper" style="margin: 0px; padding: 0px; overflow: hidden; display: inline-block; position: relative;"><div class="textoverlay" style="position: absolute; color: transparent; white-space: pre-wrap; word-wrap: break-word; overflow: hidden; margin: 0px; padding: 10px; font-family: sans-serif; font-weight: 400; font-size: 13px; background-color: rgb(255, 255, 255); top: 1px; right: 1px; bottom: 1px; left: 1px;">Oh hello man what&nbsp;</div><textarea id="editor-7994" class="editor-textarea larg-input" cols="48" rows="8" style="background: transparent; position: relative; outline: 0px; display: none;" placeholder="<?php echo $language_based_content['type_message_here_discussion']; ?>">Oh hello man what&nbsp;</textarea><input type="hidden" name="_wysihtml5_mode" value="1"><iframe class="wysihtml5-sandbox" security="restricted" allowtransparency="true" frameborder="0" width="0" height="0" marginwidth="0" marginheight="0" style="background-color: transparent; border-collapse: separate; border-color: rgb(197, 197, 197); border-style: solid; border-width: 1px; clear: none; display: inline-block; float: none; margin: 0px; outline: 0px; outline-offset: 0px; padding: 10px; position: relative; z-index: auto; vertical-align: top; text-align: start; box-sizing: border-box; box-shadow: rgba(0, 0, 0, 0.09) 0px 1px 2px 0px inset; border-radius: 3px; width: 600px; height: auto; top: auto; left: auto; right: auto; bottom: auto;"></iframe></div>';
        html += '<input type="hidden" class="hid_id" id="txtCommentID7994" value="7994">';

        html += '</div>';



        html += '<div class="controls view-controls span9">';
        html += '<div class="posted-date"><?php echo $language_based_content['saving_comment_discussion']; ?></div>';
        html += '</div>';

        html += '<div class="edit-controls" style="display:none;">';
        html += '<button class="btn btn-green inline js-update-btn" name="commit"><?php echo $language_based_content['update_discussion']; ?></button>';
        html += '<a id="cancel-edit-discussion7994" class="btn btn-transparent js-cancel-btn"><?php echo $language_based_content['cancel_discussion']; ?></a>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</li> ';
        $('.append-li-discussion').append(html);



        var editor = false;
        for (var i = 0; i < gApp.editorItems.length; i++) {
            var txtElement = $(gApp.editorItems[i].textareaElement);
            if ((txtElement.attr('id') == "editor-2") || (txtElement.attr('id') == "editor-3")) {
                editor = gApp.editorItems[i];
                break;
            }
        }

        if (editor) {
            var html = editor.getValue();
            $('#comment_html_2').val(html);
            $('#comment_html_3').val(html);
            $('#txtNotifications').val(getCheckedReplyUsers());
            if ($('#rdoPreDefined').prop('checked') == true) {
                $('#txtNotifications').val($('#txtPreDefinedNotifiers').val());
            } else if ($('#rdoLetMeChoose').prop('checked') == true) {
                $('#txtNotifications').val(getCheckedUsers());
                if ($('#txtNotifications').val() == '') {
                    alert('<?php echo $alert_messages['please_select_user_send_notification']; ?>');
                    return false;
                }
            } else {
                $('#txtNotifications').val('');
            }

        }

        var $form = $('#new_comment_reply');
        var formname = $('#new_comment_reply').attr('name');

        var myForm = document.getElementById('new_comment_reply');
        formData = new FormData($('#new_comment_reply').get(0));
        //formData = FormDataNameValuePairs('new_comment_reply');
        //formData = JSON.stringify(formData);
        //console.log(formData);
        $('input[type=file]#comment_attachment', myForm).each(function () {
            var files = $(this).prop('files');
            if (files != undefined && files.length <= 0) {
                formData.delete($(this).attr('name'));
            }
        });
        formData.append('data[attachment][]', $('input[type=file]#comment_attachment')[0].files[0])
        console.log(formData);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
                $('.replaceable_li').remove();
                $('.append-li-discussion').append(data);
                $('#new_comment_reply')[0].reset();
                $('#remove_file_names').empty();
            }
        });
        event.preventDefault();
    });

    function getCheckedReplyUsers() {
        var user_ids = [];
        $('.user_checkbox_reply').each(function (index, el) {
            el = $(el);
            if (el.prop('checked') == true) {
                user_ids.push(el.attr('value'));
            }
        });
        return user_ids.join(',');
    }


    function getCheckedUsers() {
        var users_id = '';
        $.each($('.user_checkbox[name="chk_notification_user_ids"]'), function (index, el) {
            if ($(this).is(':checked')) {
                if ($(this).val() != '') {
                    if (users_id != '') {
                        users_id += ',' + $(this).val()
                    }
                    else {
                        users_id += $(this).val()
                    }
                }
            }
        });
        if (users_id == '') {
            return '';
        } else {
            return users_id;
        }

    }








</script>

