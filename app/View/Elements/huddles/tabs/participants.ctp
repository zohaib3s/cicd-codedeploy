<?php
$users_shown = array();
$groups_shown = array();
//echo '<pre>';
//print_r($people);
//echo $huddle_type;
?>
<hr/>
<div class="tab-header">
    <h1 class="tab-header__title"><?php echo $language_based_content['participants_huddle_tab']; ?></h1>
</div>
<?php
$admins = array();
$coach = array();
$evaluator = array();
$evaluated_participant = array();
$members = array();
$coachee = array();
$viewers = array();
foreach ($people as $participant) {
    if ($participant['user_role'] == '200' && $participant['h_type'] == '2') {
        $coach[] = $participant;
    }
    if ($participant['user_role'] == '200' && $participant['h_type'] == '1') {
        $admins[] = $participant;
    }
    if ($participant['user_role'] == '210' && $participant['h_type'] == '2') {
        $coachee[] = $participant;
    }
    if ($participant['user_role'] == '210' && $participant['h_type'] == '1') {
        $members[] = $participant;
    }
    if ($participant['user_role'] == '200' && $participant['h_type'] == '3') {
        $evaluator[] = $participant;
    }
    if ($participant['user_role'] == '210' && $participant['h_type'] == '3') {
        $evaluated_participant[] = $participant;
    }
    if ($participant['user_role'] == '220') {
        $viewers[] = $participant;
    }
}

foreach ($group as $participant) {

    if ($participant['user_role'] == '200' && $participant['h_type'] == '2') {
        $coach[] = $participant;
    }
    if ($participant['user_role'] == '200' && $participant['h_type'] == '1') {
        $admins[] = $participant;
    }
    if ($participant['user_role'] == '210' && $participant['h_type'] == '2') {
        $coachee[] = $participant;
    }
    if ($participant['user_role'] == '210' && $participant['h_type'] == '1') {
        $members[] = $participant;
    }
    if ($participant['user_role'] == '200' && $participant['h_type'] == '3') {
        $evaluator[] = $participant;
    }
    if ($participant['user_role'] == '210' && $participant['h_type'] == '3') {
        $evaluated_participant[] = $participant;
    }
    if ($participant['user_role'] == '220') {
        $viewers[] = $participant;
    }
}
?>
<?php if ((isset($huddle_type) && $huddle_type == 1)): ?>
    <?php if ((isset($admins) && count($admins) > 0)): ?>
        <h4 class="huddle_participant_title"><?php echo $language_based_content['admin_participants_tab']; ?></h4>

        <div class="video-huddle-users">
            <?php
            if (isset($admins) && count($admins) > 0) :

                foreach ($admins as $user):
                    if (isset($user["id"]) && in_array($user["id"], $users_shown))
                        continue;
                    ?>

                    <a style="cursor: pointer;">
                        <?php if (isset($user['image']) && $user['image'] != ''): ?>
                            <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                            ?>
                            <?php echo $this->Html->image($avatar_path, array('alt' => $user['name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                        <?php else: ?>
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                        <br/>
                        <div style="clear:both"><?php
                            if (!empty($user['first_name'])) {
                                echo $user['first_name'] . ' ' . $user['last_name'];
                            } else {
                                echo $user['name'];
                            }
                            ?></div>
                    </a>

                    <?php
                    $users_shown[] = $user["id"];

                endforeach;
                ?>

                <?php
            endif;
            ?>
            <hr/>
        </div>
    <?php endif; ?>
    <?php if ((isset($members) && count($members) > 0)): ?>
        <div class="video-huddle-users">
            <h4 class="huddle_participant_title"><?php echo $language_based_content['members_participants_tab']; ?></h4>
            <?php if (isset($members) && count($members) > 0): ?>
                <?php
                foreach ($members as $user):

                    if (isset($user["id"]) && in_array($user["id"], $users_shown))
                        continue;
                    ?>

                    <a style="cursor: pointer;">
                        <?php if (isset($user['image']) && $user['image'] != ''): ?>
                            <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                            ?>
                            <?php echo $this->Html->image($avatar_path, array('alt' => $user['name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                        <?php else: ?>
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                        <br/>
                        <div style="clear:both"><?php
                            if (!empty($user['first_name'])) {
                                echo $user['first_name'] . ' ' . $user['last_name'];
                            } else {
                                echo $user['name'];
                            }
                            ?></div>
                    </a>

                    <?php
                    $users_shown[] = $user["id"];
                    ?>

                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <hr/>
    <?php endif; ?>
    <?php if ((isset($viewers) && count($viewers) > 0)): ?>
        <div class="video-huddle-users">
            <h4 class="huddle_participant_title"><?php echo $language_based_content['viewers_participants_tab']; ?></h4>
            <?php if (isset($viewers) && count($viewers) > 0): ?>
                <?php
                foreach ($viewers as $user):
                    ?>
                    <?php
                    if (isset($user["id"]) && in_array($user["id"], $users_shown))
                        continue;
                    ?>
                    <a style="cursor: pointer;">
                        <?php if (isset($user['image']) && $user['image'] != ''): ?>
                            <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                            ?>
                            <?php echo $this->Html->image($avatar_path, array('alt' => $user['name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                        <?php else: ?>
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                        <br/>
                        <?php
                        if (isset($user['first_name'])) {
                            ?>
                            <div style="clear:both"><?php echo @$user['first_name'] . ' ' . $user['last_name']; ?></div>
                            <?php
                        } else {
                            ?>
                            <div style="clear:both"><?php echo @$user['name']; ?></div>
                            <?php
                        }
                        ?>
                    </a>
                    <?php
                    $users_shown[] = $user["id"];
                    ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <hr/>
    <?php endif; ?>
<?php endif; ?>
<?php if ((isset($huddle_type) && $huddle_type == 2)): ?>
    <?php if ((isset($coach) && count($coach) > 0)): ?>
        <h4 class="huddle_participant_title"><?php echo $language_based_content['coach_participants_tab']; ?></h4>

        <div class="video-huddle-users">
            <?php
            if (isset($coach) && count($coach) > 0) :

                foreach ($coach as $user):
                    if (isset($user["id"]) && in_array($user["id"], $users_shown))
                        continue;
                    ?>

                    <a style="cursor: pointer;">
                        <?php if (isset($user['image']) && $user['image'] != ''): ?>
                            <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                            ?>
                            <?php echo $this->Html->image($avatar_path, array('alt' => $user['name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                        <?php else: ?>
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                        <br/>
                        <div style="clear:both"><?php
                            if (!empty($user['first_name'])) {
                                echo $user['first_name'] . ' ' . $user['last_name'];
                            } else {
                                echo $user['name'];
                            }
                            ?></div>
                    </a>

                    <?php
                    $users_shown[] = $user["id"];

                endforeach;
                ?>

                <?php
            endif;
            ?>
            <hr/>
        </div>
    <?php endif; ?>
    <?php if ((isset($coachee) && count($coachee) > 0)): ?>
        <div class="video-huddle-users">
            <h4 class="huddle_participant_title"><?php echo $language_based_content['coachee_participants_tab']; ?></h4>
            <?php if (isset($coachee) && count($coachee) > 0): ?>
                <?php
                foreach ($coachee as $user):

                    if (isset($user["id"]) && in_array($user["id"], $users_shown))
                        continue;
                    ?>
                    <a style="cursor: pointer;">
                        <?php if (isset($user['image']) && $user['image'] != ''): ?>
                            <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                            ?>
                            <?php echo $this->Html->image($avatar_path, array('alt' => $user['name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                        <?php else: ?>
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                        <br/>
                        <div style="clear:both"><?php
                            if (!empty($user['first_name'])) {
                                echo $user['first_name'] . ' ' . $user['last_name'];
                            } else {
                                echo $user['name'];
                            }
                            ?></div>
                    </a>

                    <?php
                    $users_shown[] = $user["id"];
                    ?>

                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <hr/>
    <?php endif; ?>


    <?php if ((isset($admins) && count($admins) > 0)): ?>
        <h4 class="huddle_participant_title"><?php echo $language_based_content['admin_participants_tab']; ?></h4>

        <div class="video-huddle-users">
            <?php
            if (isset($admins) && count($admins) > 0) :

                foreach ($admins as $user):
                    if (isset($user["id"]) && in_array($user["id"], $users_shown))
                        continue;
                    ?>

                    <a style="cursor: pointer;">
                        <?php if (isset($user['image']) && $user['image'] != ''): ?>
                            <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                            ?>
                            <?php echo $this->Html->image($avatar_path, array('alt' => $user['name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                        <?php else: ?>
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                        <br/>
                        <div style="clear:both"><?php
                            if (!empty($user['first_name'])) {
                                echo $user['first_name'] . ' ' . $user['last_name'];
                            } else {
                                echo $user['name'];
                            }
                            ?></div>
                    </a>

                    <?php
                    $users_shown[] = $user["id"];

                endforeach;
                ?>

                <?php
            endif;
            ?>
            <hr/>
        </div>
    <?php endif; ?>
    <?php if ((isset($members) && count($members) > 0)): ?>
        <div class="video-huddle-users">
            <h4 class="huddle_participant_title"><?php echo $language_based_content['members_participants_tab']; ?></h4>
            <?php if (isset($members) && count($members) > 0): ?>
                <?php
                foreach ($members as $user):

                    if (isset($user["id"]) && in_array($user["id"], $users_shown))
                        continue;
                    ?>
                    <a style="cursor: pointer;">

                        <?php if (isset($user['image']) && $user['image'] != ''): ?>
                            <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                            ?>
                            <?php echo $this->Html->image($avatar_path, array('alt' => $user['name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                        <?php else: ?>
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                        <br/>
                        <div style="clear:both"><?php
                            if (!empty($user['first_name'])) {
                                echo $user['first_name'] . ' ' . $user['last_name'];
                            } else {
                                echo $user['name'];
                            }
                            ?></div>
                    </a>

                    <?php
                    $users_shown[] = $user["id"];
                    ?>

                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <hr/>
    <?php endif; ?>


<?php endif; ?>

<?php if ((isset($huddle_type) && $huddle_type == 3)): ?>
    <?php if ((isset($evaluator) && count($evaluator) > 0)): ?>
        <h4 class="huddle_participant_title"><?php echo $language_based_content['assessor_participants_tab']; ?></h4>

        <div class="video-huddle-users">
            <?php
            if (isset($evaluator) && count($evaluator) > 0) :

                foreach ($evaluator as $user):
                    if (isset($user["id"]) && in_array($user["id"], $users_shown))
                        continue;
                    ?>

                    <a style="cursor: pointer;">
                        <?php if (isset($user['image']) && $user['image'] != ''): ?>
                            <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                            ?>
                            <?php echo $this->Html->image($avatar_path, array('alt' => $user['name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                        <?php else: ?>
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                        <br/>
                        <div style="clear:both"><?php
                            if (!empty($user['first_name'])) {
                                echo $user['first_name'] . ' ' . $user['last_name'];
                            } else {
                                echo $user['name'];
                            }
                            ?></div>
                    </a>

                    <?php
                    $users_shown[] = $user["id"];

                endforeach;
                ?>

                <?php
            endif;
            ?>
            <hr/>
        </div>
    <?php endif; ?>
    <?php if ((isset($evaluated_participant) && count($evaluated_participant) > 0)): ?>
        <div class="video-huddle-users">
            <h4 class="huddle_participant_title"><?php echo $language_based_content['assessed_participants_tab']; ?></h4>
            <?php if (isset($evaluated_participant) && count($evaluated_participant) > 0): ?>
                <?php
                foreach ($evaluated_participant as $user):

                    if (isset($user["id"]) && in_array($user["id"], $users_shown))
                        continue;
                    ?>
                    <a style="cursor: pointer;">
                        <?php if (isset($user['image']) && $user['image'] != ''): ?>
                            <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                            ?>
                            <?php echo $this->Html->image($avatar_path, array('alt' => $user['name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                        <?php else: ?>
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                        <br/>
                        <div style="clear:both"><?php
                            if (!empty($user['first_name'])) {
                                echo $user['first_name'] . ' ' . $user['last_name'];
                            } else {
                                echo $user['name'];
                            }
                            ?></div>
                    </a>

                    <?php
                    $users_shown[] = $user["id"];
                    ?>

                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <hr/>
    <?php endif; ?>
<?php endif; ?>