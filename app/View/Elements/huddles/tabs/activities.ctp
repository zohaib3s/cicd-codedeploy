<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<style>
    .huddle_activity
    .recent_activity_content {
        margin-top: 40px;
    }

    .huddle_activity li>div {
        display: inline-block;
        vertical-align: middle;
        min-width: 83px;
        text-align: center;
    }

    .huddle_activity ul {
        list-style-type: none;
        padding: 0;
        /* padding-left: 10px; */
    }

    .huddle_activity h5 {
        font-size: 12px;
        color: #2c5f94;
        font-weight: 600;
    }

    .huddle_activity p {
        color: #6d696a;
    }

    .huddle_activity p a {
        color: black !important;
        border-bottom: 1px dotted;
    }

    .huddle_activity h4 {
        color: black;
        font-size: 18px;
    }
</style>
<div class="tab-content huddle_activity <?php echo ($tab == '' || $tab == '7') ? 'tab-active' : 'tab-hidden'; ?>" style="display:none;" id="tabbox7" style="padding-top:0px;">
    <ul class="recent_activity_content">
        <?php
//    echo '<pre> my huddels';
//    print_r($myHuddles);
//    echo '</pre> my huddels end <br><br>';
//    echo '<pre> $UserActivityLogs';
//    print_r($UserActivityLogs);
//    echo '</pre><br><br>';
//
//
//    echo '<pre> $_SESSION';
//    print_r($_SESSION);
//    echo '</pre> $_SESSION';
//    echo '<pre> $users';
//    print_r($User);
//    echo '</pre><br><br>';



        $activityLogs = $UserActivityLogs;

        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_permissions = $this->Session->read('user_permissions');
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];



        if (isset($activityLogs) && !empty($activityLogs)):


            $recent_activity_des = '';
            $counter = 0;
            ?>
            <?php foreach ($activityLogs as $row): ?>
                <?php
                if ($counter >= 15) {
                    break;
                }
                $id = '';
                if ($row['UserActivityLog']['type'] == '1' || $row['UserActivityLog']['type'] == '2' || $row['UserActivityLog']['type'] == '3' || $row['UserActivityLog']['type'] == '5' || $row['UserActivityLog']['type'] == '6' || $row['UserActivityLog']['type'] == '22') {



                    $p_huddle_users = $AccountFolder->getHuddleUsers($row['UserActivityLog']['account_folder_id']);
                    $p_huddle_info = $AccountFolder->get($row['UserActivityLog']['account_folder_id']);
                    $userGroups = $AccountFolderGroup->getHuddleGroups($row['UserActivityLog']['account_folder_id']);
                    $loggedInUserRole = $this->Custom->has_admin_access($p_huddle_users, $userGroups, $user_current_account['User']['id']);

                    $huddle_found = false;
                    if ($myHuddles && count($myHuddles) > 0) {
                        for ($i = 0; $i < count($myHuddles); $i++) {
                            $myHuddle = $myHuddles[$i];
                            if ($myHuddles[$i]['AccountFolder']['account_folder_id'] == $row['UserActivityLog']['account_folder_id']) {
                                $huddle_found = true;
                                break;
                            }
                        }
                    }
                    if ($this->Custom->check_if_eval_huddle($row['UserActivityLog']['account_folder_id'])) {
                        $evaluators_ids = $this->Custom->get_evaluator_ids($row['UserActivityLog']['account_folder_id']);
                        if ($this->Custom->check_if_evaluated_participant($row['UserActivityLog']['account_folder_id'], $user_id)) {
                            if ($row['UserActivityLog']['user_id'] != $user_id && !(isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['UserActivityLog']['user_id'], $evaluators_ids))) {
                                continue;
                            }
                        }
                    }
                    $video_details = $this->Custom->get_video_details($row['UserActivityLog']['ref_id']);
                    // print_r($video_details);die;

                    if ($video_details['Document']['doc_type'] == 3 && ($video_details['Document']['is_associated'] == 0 || empty($video_details['Document']['is_associated']) )) {
                        continue;
                    }

                    //if ($huddle_found == false) continue;
                } elseif ($row['UserActivityLog']['type'] == '4') {
                    if ($user_permissions['UserAccount']['permission_access_video_library'] == '0') {
                        continue;
                    }
                } elseif ($row['UserActivityLog']['type'] == '7') {
                    $huddle_found = false;
                    if ($myHuddles && count($myHuddles) > 0) {

                        for ($i = 0; $i < count($myHuddles); $i++) {

                            $myHuddle = $myHuddles[$i];
                            if ($myHuddles[$i]['AccountFolder']['account_folder_id'] == $row['UserActivityLog']['account_folder_id']) {
                                $huddle_found = true;
                                break;
                            }
                        }
                    }

//                            if ($huddle_found == false)
//                                continue;

                    if ($user_permissions['UserAccount']['permission_administrator_user_new_role'] == '0') {
                        continue;
                    }
                } elseif ($row['UserActivityLog']['type'] == '17') {

                    $huddle_found = false;
                    if ($myHuddles && count($myHuddles) > 0) {

                        for ($i = 0; $i < count($myHuddles); $i++) {

                            $myHuddle = $myHuddles[$i];
                            if ($myHuddles[$i]['AccountFolder']['account_folder_id'] == $row['UserActivityLog']['account_folder_id']) {
                                $huddle_found = true;

                                break;
                            }
                        }
                    }

//                            if ($huddle_found == false)
//                                continue;

                    if ($user_permissions['UserAccount']['permission_administrator_user_new_role'] == '0') {
                        continue;
                    }
                } elseif ($row['UserActivityLog']['type'] == '18') {

                    $huddle_found = false;
                    if ($myHuddles && count($myHuddles) > 0) {

                        for ($i = 0; $i < count($myHuddles); $i++) {

                            $myHuddle = $myHuddles[$i];
                            if ($myHuddles[$i]['AccountFolder']['account_folder_id'] == $row['UserActivityLog']['account_folder_id']) {
                                $huddle_found = true;

                                //  break;
                            }
                        }
                    }

                    if (!$huddle_found) {
                        continue;
                    }
//                            if ($huddle_found == false)
//                                continue;
//                            if ($user_permissions['UserAccount']['permission_administrator_user_new_role'] == '0') {
//                                continue;
//                            }
                } elseif ($row['UserActivityLog']['type'] == '19') {

                    $huddle_found = false;
                    if ($myHuddles && count($myHuddles) > 0) {

                        for ($i = 0; $i < count($myHuddles); $i++) {

                            $myHuddle = $myHuddles[$i];
                            if ($myHuddles[$i]['AccountFolder']['account_folder_id'] == $row['UserActivityLog']['account_folder_id']) {
                                $huddle_found = true;

                                //    break;
                            }
                        }
                    }
                    if (!$huddle_found) {
                        continue;
                    }

//                            if ($huddle_found == false)
//                                continue;
//                            if ($user_permissions['UserAccount']['permission_administrator_user_new_role'] == '0') {
//                                continue;
//                            }
                } elseif ($row['UserActivityLog']['type'] == '8') {

                    $huddle_found = false;
                    if ($myHuddles && count($myHuddles) > 0) {

                        for ($i = 0; $i < count($myHuddles); $i++) {

                            $myHuddle = $myHuddles[$i];
                            if ($myHuddles[$i]['AccountFolder']['account_folder_id'] == $row['UserActivityLog']['account_folder_id']) {
                                $huddle_found = true;

                                break;
                            }
                        }
                    }

                    if ($huddle_found == false)
                        continue;
                }


                $activityLogs_users = '';
                $activityDescription = $row['UserActivityLog']['desc'];
                $activityType = $row['UserActivityLog']['type'];
                $activityUrl = $row['UserActivityLog']['url'];
                $activityDate = $row['UserActivityLog']['date_added'];

                if ($row['UserActivityLog']['user_id'] == $user_id) {
                    
                    if($_SESSION['LANG'] == 'es')
                    {
                        $activityLogs_users = 'Tú';
                    }
                    else {
                        $activityLogs_users = 'You';
                    }
                    
                } else {
                    $activityLogs_users = $row['User']['first_name'] . " " . $row['User']['last_name'];
                }

                $comment = '';
                $comment2 = '';
                $bool = false;
                $huddle_name = $AccountFolder->getHuddleName($row['UserActivityLog']['account_folder_id']);
                if (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '1') {
                    $comment2 = 'create huddle';
                    if ($row['UserActivityLog']['user_id'] == $user_id) {
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $row['UserActivityLog']['account_folder_id'], 0, 1);
                    } elseif ($row['UserActivityLog']['user_id'] != $user_id && $this->Custom->user_present_huddle($user_id, $row['UserActivityLog']['account_folder_id'])) {
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $row['UserActivityLog']['account_folder_id'], 0, 2);
                    } else {
                        continue;
                        // $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate,$row['UserActivityLog']['account_folder_id'],0,2);
                    }
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '2') {
                    if ($row['UserActivityLog']['user_id'] != $user_id && !$this->Custom->user_present_huddle($user_id, $row['UserActivityLog']['account_folder_id'])) {
                        continue;
                    }
                    $comment2 = 'Upload Video';
                    $video_name = $AccountFolderDocument->get_document_name($row['UserActivityLog']['ref_id']);
                    $video_name = isset($video_name) ? $video_name : 'Untitled Video';
                    if (strlen($video_name) > 50) {
                        $video_name = mb_substr($video_name, 0, 50) . '...';
                    }
                    $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $row['UserActivityLog']['ref_id'], 1, $row['UserActivityLog']['account_folder_id']);
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '22') {
                    if ($row['UserActivityLog']['user_id'] != $user_id && !$this->Custom->user_present_huddle($user_id, $row['UserActivityLog']['account_folder_id'])) {
                        continue;
                    }
                    $comment2 = 'Upload Video';
                    $video_name = $AccountFolderDocument->get_document_name($row['UserActivityLog']['ref_id']);
                    $video_name = isset($video_name) ? $video_name : 'Untitled Video';
                    if (strlen($video_name) > 50) {
                        $video_name = mb_substr($video_name, 0, 50) . '...';
                    }
                    $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $row['UserActivityLog']['ref_id'], 1, $row['UserActivityLog']['account_folder_id']);
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '3') {
                    if ($row['UserActivityLog']['user_id'] != $user_id && !$this->Custom->user_present_huddle($user_id, $row['UserActivityLog']['account_folder_id'])) {
                        continue;
                    }
                     $video_id = explode('/',$row['UserActivityLog']['url']);
                            $get_video_details = $this->Custom->get_video_details($video_id[5]);
                            $users = $this->Session->read('user_current_account');
                            $user_id_per = $users['User']['id'];
                            $role_id_per = $users['roles']['role_id'];
                            if($this->Custom->_check_evaluator_permissions($row['UserActivityLog']['account_folder_id'],$get_video_details['Document']['created_by'],$user_id_per,$role_id_per) == false )
                            {
                                continue;
                            }
                    if ($row['UserActivityLog']['user_id'] == $user_id) {
                        $comment2 = 'You';
                    } else {
                        $comment2 = $row['User']['first_name'] . " " . $row['User']['last_name'];
                    }
                    $document_name = $AccountFolderDocument->get_document_name($row['UserActivityLog']['ref_id']);
                    if (strlen($document_name) > 50) {
                        $document_name = mb_substr($document_name, 0, 50) . '...';
                    }

                    $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $document_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, 0, 1, $row['UserActivityLog']['account_folder_id']);
                } elseif ($row['UserActivityLog']['type'] == '4') {
                    $comment2 = 'Video Library';
                    $video_title = $huddle_name;
                    $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate);
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '5') {
                    if ($row['UserActivityLog']['user_id'] != $user_id && !$this->Custom->user_present_huddle($user_id, $row['UserActivityLog']['account_folder_id'])) {
                        continue;
                    }
                    $video_comments = $Comment->get($row['UserActivityLog']['ref_id']);
                      $get_video_details = $this->Custom->get_video_details($video_comments['Comment']['ref_id']);
                            $users = $this->Session->read('user_current_account');
                            $user_id_per = $users['User']['id'];
                            $role_id_per = $users['roles']['role_id'];
                            if($this->Custom->_check_evaluator_permissions($row['UserActivityLog']['account_folder_id'],$get_video_details['Document']['created_by'],$user_id_per,$role_id_per) == false )
                            {
                                continue;
                            }
                    $video_title = $AccountFolderDocument->get_document_name(isset($video_comments['Comment']['ref_id']) ? $video_comments['Comment']['ref_id'] : '');
                    $video_id = $video_comments['Comment']['ref_id'];
                    if ($video_comments['Comment']['ref_type'] == '6') {
                        $bool = true;
                    }

                    if (isset($video_comments['Comment']) && $video_comments['Comment']['active'] == '1') {
                        $comment = isset($video_comments['Comment']['comment']) ? $video_comments['Comment']['comment'] : '';
                        if (strlen($comment) > 20) {
                            $comment = mb_substr($comment, 0, 20) . '...';
                        }
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $huddle_name, $comment, $activityUrl, $activityDate, $video_title, $video_id, 1, $row['UserActivityLog']['account_folder_id']);
                    }
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '6') {
                    if ($row['UserActivityLog']['user_id'] != $user_id && !$this->Custom->user_present_huddle($user_id, $row['UserActivityLog']['account_folder_id'])) {
                        continue;
                    }
                    $orignal_comment = $Comment->get_parent_discussion($row['UserActivityLog']['ref_id']);
                    if (count($orignal_comment) > 0) {

                        if ($orignal_comment['Comment']['active'] == '1') {

                            $commnet_title = $orignal_comment['Comment']['title'];
                            $comment = $activityDescription;
                            $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $commnet_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, 0, 1, $row['UserActivityLog']['account_folder_id']);
                        }
                    } else {
                        $recent_activity_des = '';
                    }
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '8') {
                    if ($row['UserActivityLog']['user_id'] != $user_id && !$this->Custom->user_present_huddle($user_id, $row['UserActivityLog']['account_folder_id'])) {
                        continue;
                    }
                    $child_comments = $Comment->get_parent_discussion($row['UserActivityLog']['ref_id']);

                    if (count($child_comments) > 0 && $child_comments['Comment']['parent_comment_id'] != '') {
                        $topic_name = $Comment->get_parent_discussion($child_comments['Comment']['parent_comment_id']);

                        if ($topic_name['Comment']['active'] == '1') {

                            $commnet_title = $topic_name['Comment']['title'];
                            $comment = $activityDescription;
                            $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $commnet_title, $activityDescription, $activityUrl, $activityDate, $huddle_name);
                        }
                    } else {
                        $recent_activity_des = '';
                    }
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '7') {

//            if ($huddle_name != '') {
//                continue;
//            }



                    if ($row['UserActivityLog']['type'] == '7' && $row['UserActivityLog']['account_folder_id'] != '') {
                        $comment2 = 'User Invited';
                        $username = $User->findByid($row['UserActivityLog']['ref_id']);

                        $activityDescription = $username['User']['first_name'] . "  " . $username['User']['last_name'];
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate);
                    } else {
                        $comment2 = 'User Invited';
                        $username = $User->findByid($row['UserActivityLog']['ref_id']);
                        $activityDescription = $username['User']['first_name'] . "  " . $username['User']['last_name'];
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, '', $activityDescription, $activityUrl, $activityDate);
                    }
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '17') {
                    if ($huddle_name != '') {
                        continue;
                    }
                    if ($row['UserActivityLog']['type'] == '17' && $row['UserActivityLog']['account_folder_id'] != '') {
                        $comment2 = 'User Invited';
                        $username = $User->findByid($row['UserActivityLog']['ref_id']);
                        $activityDescription = $username['User']['first_name'] . "  " . $username['User']['last_name'];
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate);
                    } else {
                        $comment2 = 'User Invited';
                        $username = $User->findByid($row['UserActivityLog']['ref_id']);
                        $activityDescription = $username['User']['first_name'] . "  " . $username['User']['last_name'];
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, '', $activityDescription, $activityUrl, $activityDate);
                    }
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '18') {
                    
                    $users = $this->Session->read('user_current_account');
                                $user_id_per = $users['User']['id'];
                                $role_id_per = $users['roles']['role_id'];
                                if($this->Custom->_check_evaluator_permissions_invite($row['UserActivityLog']['account_folder_id'],$user_id_per,$row['UserActivityLog']['ref_id']) == false )
                                {
                                    continue;
                                }

                    if ($row['UserActivityLog']['type'] == '18' && $row['UserActivityLog']['account_folder_id'] != '') {
                        $comment2 = 'User Invited';
                        $username = $User->findByid($row['UserActivityLog']['ref_id']);
                        $activityDescription = $username['User']['first_name'] . "  " . $username['User']['last_name'];
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, '', 0, 1, $row['UserActivityLog']['account_folder_id']);
                    } else {
                        $comment2 = 'User Invited';
                        $username = $User->findByid($row['UserActivityLog']['ref_id']);
                        $activityDescription = $username['User']['first_name'] . "  " . $username['User']['last_name'];
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, '', $activityDescription, $activityUrl, $activityDate);
                    }
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '19') {
                    
                    $users = $this->Session->read('user_current_account');
                                $user_id_per = $users['User']['id'];
                                $role_id_per = $users['roles']['role_id'];
                                if($this->Custom->_check_evaluator_permissions_invite($row['UserActivityLog']['account_folder_id'],$user_id_per,$row['UserActivityLog']['ref_id']) == false )
                                {
                                    continue;
                                }

                    if ($row['UserActivityLog']['type'] == '19' && $row['UserActivityLog']['account_folder_id'] != '') {
                        $comment2 = 'User Invited';
                        $username = $User->findByid($row['UserActivityLog']['ref_id']);
                        $activityDescription = $username['User']['first_name'] . "  " . $username['User']['last_name'];
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, '', 0, 1, $row['UserActivityLog']['account_folder_id']);
                    } else {
                        $comment2 = 'User Invited';
                        $username = $User->findByid($row['UserActivityLog']['ref_id']);
                        $activityDescription = $username['User']['first_name'] . "  " . $username['User']['last_name'];
                        $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, '', $activityDescription, $activityUrl, $activityDate);
                    }
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '20') {
                    if ($row['UserActivityLog']['user_id'] != $user_id && !$this->Custom->user_present_huddle($user_id, $row['UserActivityLog']['account_folder_id'])) {
                        continue;
                    }
                     if ($this->Custom->check_if_evaluated_participant($row['UserActivityLog']['account_folder_id'], $user_id)) {
                                continue;
                            }
                    $comment2 = 'Observation Started';
                    $video_name = $AccountFolderDocument->get_document_name($row['UserActivityLog']['ref_id']);
                    $video_name = isset($video_name) ? $video_name : 'Untitled Video';
                    if (strlen($video_name) > 50) {
                        $video_name = mb_substr($video_name, 0, 50) . '...';
                    }
                    $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $row['UserActivityLog']['ref_id'], 1, $row['UserActivityLog']['account_folder_id']);
                    if (!$this->Custom->check_if_recording_stopped($row['UserActivityLog']['ref_id'])) {
                        $recent_activity_des = $recent_activity_des . '<img src="/img/recording_img.png" id="img_recording_live" style="margin-right: 0px;margin-left: 10px;"> '.$alert_messages['live_recording_alert'];
                    }

                    $id = "processing-message-" . $row['UserActivityLog']['id'];
                } elseif (isset($row['UserActivityLog']['type']) && $row['UserActivityLog']['type'] == '21') {
                    if ($row['UserActivityLog']['user_id'] != $user_id && !$this->Custom->user_present_huddle($user_id, $row['UserActivityLog']['account_folder_id'])) {
                        continue;
                    }
                     if ($this->Custom->check_if_evaluated_participant($row['UserActivityLog']['account_folder_id'], $user_id)) {
                                continue;
                            }
                    $comment2 = 'Observation Stopped';
                    $video_name = $AccountFolderDocument->get_document_name($row['UserActivityLog']['ref_id']);
                    $video_name = isset($video_name) ? $video_name : 'Untitled Video';
                    if (strlen($video_name) > 50) {
                        $video_name = mb_substr($video_name, 0, 50) . '...';
                    }
                    $recent_activity_des = $this->Custom->log_activity($activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $row['UserActivityLog']['ref_id'], 1, $row['UserActivityLog']['account_folder_id']);
                }
                ?>
                <?php
                if (isset($comment) && ($comment != '' || $comment2 != '')):
                    ?>
                    <?php if ($recent_activity_des != ''): ?>
                        <li id="<?php echo $id; ?>">
                            <div class="recentActivity_left">
                                <h4><?php if($_SESSION['LANG'] == 'es') {echo $this->Custom->SpanishDate(strtotime($activityDate),'dashboard');}else{ echo date('M d', strtotime($activityDate));} ?></h4>
                                <h5><?php echo date('h:i A', strtotime($activityDate)); ?></h5>
                            </div>
                            <div class="recentActivity_right">
                                <p style="white-space: nowrap; max-width: 800px;overflow: hidden;text-overflow:ellipsis; ">
                                    <?php
                                    echo $recent_activity_des;
                                    ?>

                                    <?php
                                    if ($recent_activity_des != '') {
                                        $counter++;
                                    }
                                    ?>

                                    <?php if (isset($comment) && $comment != ''): ?>
                                        <?php
                                        if ($bool) {
                                            echo 'You have added an audio Annotation';
                                        } else {
                                            echo (strlen($comment) > 200 ? mb_substr(strip_tags($comment, ""), 0, 120) . "..." : strip_tags($comment, ""));
                                        }
                                        ?>
                                    <?php endif; ?>
                                </p>
                            </div>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
                <?php $bool = 0; ?>
            <?php endforeach; ?>
        <?php else: ?>
            <li>
                No recent activity found for this Huddle.
            </li>
        <?php endif; ?>
    </ul>
</div>