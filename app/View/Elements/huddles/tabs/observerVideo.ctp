<?php
$amazon_base_url = Configure::read('amazon_base_url');
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');


$isFirefoxBrowser = $this->Browser->isFirefox();
?>
<input type="hidden" id="ob_account_folder_id" name="ob_account_folder_id" value="<?php echo $huddle_id; ?>" />
<div class="tab-content <?php echo ($tab && $tab == '5') ? 'tab-active' : 'tab-hidden'; ?>"  id="tabbox5Area">
    <div class="left-box">
        <div> <a class="back" href="./">Back to all observations</a></div>
        <div style="clear: both;">&nbsp;</div><br>
        <div class="observerphotos">
            <div><h3>Observer:</h3>
                <?php
                if (isset($observations[0]['observer'])):
                    foreach ($observations[0]['observer'] as $observerRow):
                        ?>
                        <?php if (isset($observerRow['image']) && $observerRow['image'] != ''): ?>
                            <?php
                            $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $observerRow['id'] . "/" . $observerRow['image']);
                            echo $this->Html->image($chimg, array('alt' => $observerRow['first_name'] . ' ' . $observerRow['last_name'], 'class' => 'photo', 'data-original-title' => $observerRow['first_name'] . ' ' . $observerRow['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left'));
                            ?>
                        <?php else: ?>
                            <img width="34" height="34" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" rel="tooltip" class="photo observee-new-img" alt="<?php echo $observerRow['first_name'] . ' ' . $observerRow['last_name']; ?>" data-original-title="<?php echo $observerRow['first_name'] . ' ' . $observerRow['last_name']; ?>">
                        <?php endif; ?>
                        <?php
                    endforeach;
                endif;
                ?>
            </div>
            <div style="clear: both;"></div><br>

            <div>
                <h3>Observee:</h3>
                <a href="#"><?php echo $observations[0]['observee']['first_name'] . ' ' . $observations[0]['observee']['last_name']; ?></a>

                <span class="datet"><?php echo $observations[0]['observation_on']; ?></span><span class="dateh">Date: </span>
            </div>

        </div>



        <div class="left-box video-detail">
            <div class="video-outer">
                <?php
                if (isset($videoDetail['Document']) && $videoDetail['Document'] != ''):
                    ?>
                    <input id="video-id" type="hidden" value="<?php echo $videoDetail['Document']['id'] ?>"/><?php
                    $video_title = '';
                    if (isset($videoDetail['afd']['title']) && $videoDetail['afd']['title'] != '') {
                        $video_title = $videoDetail['afd']['title'];
                    } else {
                        $video_title = 'Untitled Video';
                    }
                    ?>
                    <div id="video_span" style="float:left;width:500px;">
                        <?php
                        $isEditable = ($huddle_permission == 200) ||
                                ($huddle_permission == 210 && $user_current_account['User']['id'] == $videoDetail['Document']['created_by']) ||
                                ($user_current_account['User']['id'] == $videoDetail['Document']['created_by'] && $user_permissions['UserAccount']['permission_maintain_folders'] == '1');
                        ?>
                        <?php if (!$isFirefoxBrowser && $videoDetail['Document']['published'] == 1 && $isEditable): ?>
                        <?php endif; ?>
                        <p id="notification" style="display:none;"></p>
                        <?php if ($isEditable): ?>
    <?php endif; ?>
                        <div class="appendix-content appendix-narrow card" style="display: none;">
                            <p style="word-wrap: break-word; padding: 10px; margin-top: 0px;">If your video is not loading or playing, please upgrade your browser to the most recent version. <?php $this->Custom->get_site_settings('site_title') ?> is compatible with <a style="float: none;" target="blank" href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie-9/worldwide-languages">Internet Explorer</a>, <a style="float: none;" target="blank" href="https://www.google.com/intl/en/chrome/browser/?&brand=CHMB&utm_campaign=en&utm_source=en-ha-na-us-sk&utm_medium=ha">Google Chrome</a>, and <a style="float: none;" href="http://www.apple.com/safari/" target="blank">Safari</a>. If your video is still not playing in one of these browsers, please contact &nbsp; <a style="float: none;" href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.</p>
                        </div>
                        <div style="clear: both;"></div>
                        <!-- Temporary video player replacement -->
                        <?php
                        $timeS = array();

                        $videoID = $videoDetail['Document']['id'];
                        $videoFilePath = pathinfo($videoDetail['Document']['url']);
                        $videoFileName = $videoFilePath['filename'];
                        if ($videoComments) {
                            foreach ($videoComments as $cmt) {
                                if (!empty($cmt['Comment']['time']))
                                    $timeS[] = $cmt['Comment']['time'];
                            }
                        }
                        ?>

    <?php if ($huddle_permission == '220'): ?>
                            <style>
                                .vjs-tooltip{display: none !important; }
                            </style>
    <?php endif; ?>

                        <input type="hidden" id="txtCurrentVideoID" value="<?php echo $videoID; ?>" />
                        <input type="hidden" id="txtCurrentVideoUrl" value="<?php echo $this->base . '/Huddles/view/' . $huddle_id . "/1/$videoID" ?>" />
                        <?php
                        $document_files_array = $this->Custom->get_document_url($videoDetail['Document']);
                        if (empty($document_files_array['url'])) {
                            $videoDetail['Document']['published'] = 0;
                            $document_files_array['url'] = $videoDetail['Document']['original_file_name'];
                            $videoDetail['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $videoDetail['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        }

                        if (isset($videoDetail['Document']['published']) && $videoDetail['Document']['published'] == 1):
                            ?>
                            <?php
                            $video_path = $document_files_array['url'];
                            $thumbnail_image_path = $document_files_array['thumbnail'];
                            ?>
                            <video id="example_video_<?php echo $videoDetail['Document']['id'] ?>" class="video-js vjs-default-skin" controls preload="metadata" width="500" height="321" poster="<?php echo $thumbnail_image_path; ?>" data-markers="[<?php echo implode(',', $timeS); ?>]">
                                <source src="<?php echo $video_path; ?>" type='video/mp4'/>
                            </video>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $.ajax({
                                        type: 'POST',
                                        url: home_url + '/Huddles/update_view_count/<?php echo $videoID; ?>',
                                        success: function (response) {
                                        },
                                        errors: function (response) {
                                        }
                                    });
                                });
                            </script>
                            <div style="padding-top: 15px;">
                                <p class="tip">Tip: Click on the video player timeline bar above to add time-specific comments.
                                    <span class="badge right" data-original-title="video played" rel="tooltip"><?php echo $videoDetail['Document']['view_count']; ?></span>
                                </p>
                            </div>
                            <div style="clear: both;"></div>
    <?php elseif (isset($videoDetail['Document']['published']) && $videoDetail['Document']['published'] == 0): ?>
                            <div  style="width: 500px; height: 321px; background: #000; float: left; text-align: center; color: #fff; position:relative;" >
                                <div class="empty_video_box" style="padding-top:125px !important; padding-left: 121px !important; ">
        <?php if ($videoDetail['Document']['encoder_status'] == 'Error'): ?>
                                        Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;position: absolute;top: 170px;left: 170px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                    <?php else : ?>
                                        <img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br>Your video is currently processing. You will receive an email notification when your video is ready to be viewed.
                                    <?php endif; ?>
                                </div>
                            </div>

    <?php elseif ($videoFilePath == ''): ?>
                            <div id="docs-container" style="width: 500px; height: 321px;">
                                <div class="heading"><h3>Video</h3></div>
                                <div style=" padding: 10px; font-weight: bold;">
                                    This video is not currently available please remove this video and try again.
                                </div>
                            </div>
    <?php endif; ?>

                        <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $videoDetail['Document']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#copy-huddle').click(function (e) {
                                        $('#copy-document-id').val($(this).attr('data-document-id'));
                                    });
                                    var fieldWidth = parseInt($("#vidTitle").css("width"));
                                    $(".submit").mousedown(function () {
                                        ajaxField.update("vidTitle", "<?php echo $this->base . '/Huddles/changeTitle' ?>");
                                    });
                                    $("#ajaxInput").css("width", (fieldWidth - 150) + "px");
                                    $(".editArea").css("width", (fieldWidth) + "px");
                                    $("#vidTitle").mouseover(function () {
                                        $(this).css("backgroundColor", "#FAFABE")
                                    });
                                    $("#vidTitle").mouseout(function () {
                                        $(this).css("backgroundColor", "#ffffff")
                                    });
                                    $("#vidTitle").click(function () {
                                        $(this).css("display", "none");
                                        $(".editArea").css("display", "inline-block");
                                        $("#ajaxInput").val($(this).text().trim());
                                        $("#ajaxInput").focus();
                                    });
                                    $("#ajaxInput").blur(function () {
                                        $(".editArea").css("display", "none");
                                        $("#vidTitle").css("display", "inline-block");
                                    });
                                });
                                var ajaxField, startValue = $("#vidTitle").text();
                                ajaxField = function () {                                     //variables
                                    var xmlHttp = null;
                                    if (typeof String.prototype.trim !== 'function') { //this will add trim to IE8
                                        String.prototype.trim = function () {
                                            return this.replace(/^\s+|\s+$/g, '');
                                        }
                                    }
                                    return {
                                        init: function () {
                                        },
                                        update: function (responseField, url) {
                                            xmlHttp = new XMLHttpRequest();
                                            xmlHttp.onreadystatechange = function () {
                                                if (xmlHttp.readyState != 4) {
                                                    return;
                                                }
                                                if (xmlHttp.status != 200 && xmlHttp.status != 304) {
                                                    alert('HTTP error ' + xmlHttp.status);
                                                    return;
                                                }
                                                if (xmlHttp.readyState == 4) {
                                                    ajaxField.handleResponse(responseField, xmlHttp.responseText);
                                                }
                                            };
                                            newText = document.getElementById('ajaxInput').value.trim();
                                            videoId = $('#ob_account_folder_id').val();
                                            huddleId = '<?php echo $huddle_id; ?>';
                                            url = url + "/" + huddleId + "/" + videoId + "/" + newText;
                                            xmlHttp.open("GET", url, true);
                                            xmlHttp.send(null);
                                            $("#vidTitle").css("display", "inline-block");
                                            $(".editArea").css("display", "none");
                                        },
                                        handleResponse: function (responseField, response) {
                                            document.getElementById(responseField).innerHTML = response;
                                        }

                                    };
                                }();

                            </script>
    <?php endif; ?>
                    </div>
                    <?php else: ?>
                    <input id="video-id" type="hidden" value="<?php echo $huddle_id; ?>"/>
                    <div id="video_span">
                        <div class="no-vid" style="background-image:none;">
                            <img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/img_no_vodio.png'); ?>" />
                        </div>
                    </div>
<?php endif; ?>
            </div>
        </div>
        <div class="clear"></div>
        <h3 class="comments-title">Comments</h3>
        <span class="tags">
            <a id="pop-up-btn" data-original-title="View Frameworks" rel="tooltip" data-toggle="modal" data-target="#observationFrameWorks">Tags</a>
        </span>
        <span>
            <img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/tag-icon.png'); ?>" style="float:right">
        </span>
        <div id="comment_form" class="comment_form" style="display:block !important">
            <form method="post" id="comments-form1" class="new_comment" action="<?php echo $this->base; ?>/Huddles/ob_addComments" accept-charset="UTF-8">
                <div style="margin:0;padding:0;display:inline"><input type="hidden" value="âœ“" name="utf8"></div>
                <input type="hidden" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" name="authenticity_token" id="tokentag">
                <div class="input-group">
                    <textarea rows="4" placeholder="Add a comment..." name="comment[comment]" required="" id="comment_comment" cols="65"></textarea>
                </div>
                <input type="hidden" value="" name="comment[access_level]" id="comment_access_level">
                <input type="hidden" value="{:value=&gt;&quot;&quot;}" name="synchro_time" id="synchro_time">
                <input type="hidden" value="<?php echo isset($videoDetail['Document']['id']) ? $videoDetail['Document']['id'] : $huddle_id; ?>" name="videoId" id="videoId">
                <input type="hidden" value="<?php echo $user_id; ?>" name="user_id" id="user_id">
                <input type="hidden" value="<?php echo $hud_id; ?>" name="huddle_id" id="huddle_id">
                <input type="hidden" value="<?php echo $huddle_id; ?>" name="detail_id" id="detail_id">

                <div class="input-group" id="comment-for">
                    For:
                    <label for="for_synchro_time">
<?php if (isset($videoDetail['Document']['published']) && $videoDetail['Document']['published'] == 1): ?>
                            <input type="radio" value="synchro_time" name="for" id="for_synchro_time"> Time-specific (<span id="right-now-time">0:00</span>)
                        <?php else: ?>
                            <input type="radio" value="synchro_time" name="for" id="for_synchro_time" disabled=""> Time-specific (<span id="right-now-time">--:--</span>)
                        <?php endif; ?>
                    </label>

                    <label for="for_entire_video">
                        <input type="radio" checked="" value="entire_video" name="for" id="for_entire_video"> Whole video
                    </label>
                </div>

                <div class="input-group">
                    <input type="submit" class="btn btn-green" value="Add Comment" name="submit">
                    <input type="hidden" value="add_video_comments" name="type">
                    <a class="btn btn-transparent js-close-comments-form" id="clearComment">Cancel</a>
                </div>
            </form>
            <div style="display:none;" id="indicator"><img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/indicator.gif'); ?>" alt="Indicator"></div>
        </div>
        <div id="docs-container" class="docs-container">
            <div id="obs-docs-container">
            </div>
        </div>
    </div>
    <div class="right-box-1">
        <div class="right-box-2">
            <!--                 FILTER SECTION -->
            <div id='tagFilterList'></div>
            <div style="clear: both;"></div>
            <!--                 FILTER SECTION END -->
            <div id="scrollbar1">
                <div class="scrollbar" style="height: auto;">
                    <div class="track" style="height: auto;">
                        <div class="thumb" style="top: 0px; height: auto">
                            <div class="end"></div>
                        </div>
                    </div>
                </div>
                <div class="viewport short">
                    <div class="overview" style="top: 0px;">


                        <div id="vidComments_obs">
                            <input type="hidden" id="video_ID" name="video_ID" value="<?php echo isset($videoDetail['Document']['id']) ? $videoDetail['Document']['id'] : '' ?>">
                            <div> Loading Comments...</div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="right-box-3">

            <span><img alt="" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/tag-icon.png'); ?>" style="float:left"></span><span class="tagsleft">Tags</span>
            <div style="clear: both;"></div>

            <div id="scrollbar2">
                <div class="scrollbar" style="height: 500px;">
                    <div class="track" style="height: 500px;">
                        <div class="thumb" style="height: 271.73913043478257px; top: 0px;">
                            <div class="end"></div>
                        </div>
                    </div>
                </div>
                <div class="viewport short">
                    <div class="overview" style="top: 0px;">
                        <div id="tagCloudContainer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    <hr class="full">
    <div class="form-actions">
        <input type="submit" id="btnSaveHuddle" value="Submit Observation" name="commit" class="btn btn-green">
        <a class="btn btn-transparent" href="/Huddles">Cancel</a>
    </div>
</div>
<div id="observationFrameWorks" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom">Framework</h4>
                <div style="width: 356px;float: right;position: relative;" class="search-box12">
                    <input type="button" style="width:27px;" value="" class="btn-search" action="" id="searchTagsBtn">
                    <input type="text" style="margin-right: 25px;" placeholder="Search Frameworks..." value="" id="txtSearchTags" name="txtSearchTags" class="text-input">
                    <span class="clear-video-input-box cross-search search-x-huddles" style="" id="clearSearchTags">X</span>
                </div>
                <a data-dismiss="modal" class="close-reveal-modal btn btn-grey close style2">×</a>
            </div>
            <div id="scrollbar3">
                <div class="scrollbar" style="height: auto;">
                    <div class="track" style="height: auto;">
                        <div class="thumb">
                            <div class="end"></div>
                        </div>
                    </div>
                </div>
                <div class="viewport short">
                    <div class="overview" style="top: 25px;">

                        <div id="listContainer">
<?php echo makeList($tags_frameworks); ?>
                        </div>
                    </div></div>

            </div>
        </div>
    </div>
</div>
<?php
if (isset($videoDetail['Document']) && $videoDetail['Document'] != '') {
    echo "<script>hideAddEvidenceVideoBtn()</script>";
}

function makeListItems($a) {
    $out = '';
    foreach ($a as $item) {
        $out .= '<li>';
        $out .= $item['framework_code'] . ": " . $item['framework_title'] . "";

        if (array_key_exists('children', $item)) {
            $out .= makeList($item['children']);
        }
        $out .= '</li>';
    }

    return $out;
}

function makeList($a) {
    $out = '<ul id="expList">';
    $out .= makeListItems($a);
    $out .= '</ul>';

    return $out;
}
?>