<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>  
<?php

$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');

$isAdminRole  = $huddle_permission == '200';
$isUserRole   = $huddle_permission == '210';
$isViewerRole = $huddle_permission == '220';
$canMaintainFolder = $this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) &&
    $user_permissions['UserAccount']['permission_maintain_folders'] == '1';
$canDownloadFile = $isAdminRole || $isUserRole || $canMaintainFolder;

if(!function_exists('_make_url_clickable_cb')){

    function _make_url_clickable_cb($matches) {
        $ret = '';
        $url = $matches[2];

        if ( empty($url) )
            return $matches[0];
        // removed trailing [.,;:] from URL
        if ( in_array(substr($url, -1), array('.', ',', ';', ':')) === true ) {
            $ret = substr($url, -1);
            $url = substr($url, 0, strlen($url)-1);
        }
        return $matches[1] . "<a href=\"$url\" target=\"_blank\" rel=\"nofollow\">$url</a>" . $ret;
    }

}

if(!function_exists('_make_web_ftp_clickable_cb')){

    function _make_web_ftp_clickable_cb($matches) {
        $ret = '';
        $dest = $matches[2];
        $dest = 'http://' . $dest;

        if ( empty($dest) )
            return $matches[0];
        // removed trailing [,;:] from URL
        if ( in_array(substr($dest, -1), array('.', ',', ';', ':')) === true ) {
            $ret = substr($dest, -1);
            $dest = substr($dest, 0, strlen($dest)-1);
        }
        return $matches[1] . "<a href=\"$dest\" target=\"_blank\" rel=\"nofollow\">$dest</a>" . $ret;
    }
}

if(!function_exists('_make_email_clickable_cb')){

    function _make_email_clickable_cb($matches) {
        $email = $matches[2] . '@' . $matches[3];
        return $matches[1] . "<a href=\"mailto:$email\" target=\"_blank\">$email</a>";
    }
}

if(!function_exists('make_clickable')){

    function make_clickable($ret) {

        $ret = str_replace("&nbsp;", " ", $ret);

        $ret = ' ' . $ret;
        // in testing, using arrays here was found to be faster
        $ret = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_url_clickable_cb', $ret);
        $ret = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_web_ftp_clickable_cb', $ret);
        $ret = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', '_make_email_clickable_cb', $ret);

        // this one is not in an array because we need it to run last, for cleanup of accidental links within links
        $ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
        $ret = trim($ret);
        return $ret;
    }

}

?>
<style>
    .editor-textarea{
        padding:10px !important;
    }
</style>
<input id="download_button_img_path" type = "hidden" value = "<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>">
<div class="">
    <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/3' ?>" class="back back-add-discussion" style="margin:0 0 5px 0; font-weight: bold;"><?php echo $language_based_content['new_discussion_discussion']; ?></a>
</div>

<div class="tab-4" style="box-shadow: 1px #F0F0F0; border: 1px #F0F0F0 solid; padding: 5px;">
    <?php
    $parentDiscussion = $Comment->get($detail_id);
    $profile_image = isset($parentDiscussion['User']['image']) ? $parentDiscussion['User']['image'] : '';
    $profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";
    //if (file_exists($profile_image_real_path) && $profile_image != '') {
    if ($profile_image != '') {
        //$profile_image = $this->webroot . "img/users/$profile_image";
        $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $parentDiscussion['User']['user_id'] . "/" . $parentDiscussion['User']['image']);
    } else {
        //$profile_image = $this->webroot . "img/profile.jpg";
        $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/profile.jpg');
    }
    ?>

    <div class="tab-header" style="border-bottom: 2px #f2f2f2 solid">

        <div style="clear: both;"></div>
        <div class="span9" style="margin-left: 0px;">
            <div class="tab-header__title" id="discussion-title-box"><?php echo $parentDiscussion['Comment']['title']; ?></div>
            <div class="tab-header__title" id="discussion-title-input" style="display:none;">
                <input required="required" type="text" value="<?php echo $parentDiscussion['Comment']['title']; ?>" name="comment_title" id="comment_title"/>
                <input type="hidden" name="discussion_id" id="discussion_parent_id" value="<?php echo $parentDiscussion['Comment']['id']; ?>"/>
            </div>
        </div>

        <div style="clear: both;"></div>
        <div style="font-weight: bold;" class="title"><?php echo $language_based_content['posted_by_discussion']; ?> <?php echo $parentDiscussion['User']['first_name'] . ' ' . $parentDiscussion['User']['last_name'] ?> <?php echo $language_based_content['on_discussion']; ?> <?php
            $commentDate = $parentDiscussion['Comment']['created_date'];
            $start_date = new DateTime($commentDate);
            echo $start_date->format('d M');
            ?>
        </div>
        <div class="span12" style="margin-left: 0px; margin-top: 20px;">
            <div class="span1" style="margin-left: 0px;">
                <img src="<?php echo $profile_image; ?>" alt="img" height="42px" width="42px" class="circle" />
            </div>
            <div class="text text-width span10">
                <div class="discussion-editor-row" style="float: none;">
                    <div class="discussion-editor-row-content view-controls"><?php echo stripslashes(make_clickable($parentDiscussion['Comment']['comment'])); ?></div>
                    <div class="clearfix"></div>
                    <div class="input-group edit-controls" style="display:none;">
                        <div id="<?php echo "editor" . $parentDiscussion['Comment']['id'] . "-toolbar" ?>" class="editor-toolbar">
                            <a data-wysihtml5-command="bold">bold</a>
                            <a data-wysihtml5-command="italic">italic</a>
                            <a data-wysihtml5-command="insertOrderedList">ol</a>
                            <a data-wysihtml5-command="insertUnorderedList">ul</a>
                            <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                        </div>
                        <textarea id="<?php echo "editor-" . $parentDiscussion['Comment']['id'] ?>"
                                  class="editor-textarea larg-input" cols="48" rows="4"
                                  placeholder="<?php echo $language_based_content['type_message_here_discussion']; ?>"><?php echo $parentDiscussion['Comment']['comment']; ?></textarea>
                         <div style ="display:none;" id="target_div<?php echo $parentDiscussion['Comment']['id']; ?>"><?php echo html_entity_decode(htmlspecialchars($parentDiscussion['Comment']['comment'])); ?></div>
                        <input type="hidden" class="hid_id" id="txtCommentID<?php echo $parentDiscussion['Comment']['id']; ?>" value="<?php echo $parentDiscussion['Comment']['id']; ?>"/>
                    </div>

                    <div class="file-area">
                        <div class="input-group">
                            <ul class="js-attachment-list">
                                <?php $attachedDocuments = $Document->getByCommentId($parentDiscussion['Comment']['id']); ?>
                                <?php if (!empty($attachedDocuments)): ?>
                                    <?php foreach($attachedDocuments as $doc): ?>
                                <li>
                                            <?php
                                            $videoFilePath = pathinfo($doc['Document']['url']);
                                            $ext = $videoFilePath['extension'];
                                            if ($canDownloadFile) {
                                                $href = $this->base . '/Huddles/download/' . $doc['Document']['id'];
                                            } else {
                                                $href = '';
                                            }
                                            ?>
                                    <a id="url_generator_<?php echo $doc['Document']['id'];?>" href="javascript:void(0);<?php //echo $href;?>" target="_blank">
                                        <img class="icon24" src="<?php echo $this->Custom->getDocTypeIconLink($ext);?>" alt="doc type icon" />
                                        <span style="width:auto;"><?php echo $doc['Document']['original_file_name'];?></span>
                                    </a>
                                    
                                       <span style="width:auto;"><a style ="width:auto; cursor: pointer;" download href="<?php echo $href; ?>">
                 <img alt="Download" class="right smargin-right" style="height: 24px;" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>" title="download document"  />
                
                </a> </span>
                                    
                                                                                                           <span del_document_id="<?php echo $doc['Document']['id'];?>" style="display:none;
    cursor: pointer;
    font-weight: 400;
    text-decoration: underline;
    color: red;
    font-size: 14px;
   
    position: relative;
    width:auto;" class = "delete_doc<?php echo$parentDiscussion['Comment']['id']; ?>" id="delete_discussion_file_<?php echo $doc['Document']['id'];?>">Remove</span>
    
                  
                                    
                                    
                                    
                                    
                                    <i class="remove edit-controls" style="display:none;">remove</i>
                                    <input type="hidden" name="document_id" value="<?php echo $doc['Document']['id'];?>" />
                                </li>
                                
                                
                                
                                <script>
                           
                                    
                                    
                                    
                                    
        $('#url_generator_<?php echo $doc['Document']['id'];?>').on('click', function (e) {
                     
                     
             $.ajax({
            url: home_url + "/huddles/generate_url/<?php echo $doc['Document']['id'];?>",
            type: 'POST',
            success: function (response) {
                
                filepicker.setKey('A3w6JbXR2RJmKr3kfnbZtz');            

                 filepicker.storeUrl(
                response,
                {filename: 'something.png'},
                function(Blob){
                    
                var result=Blob.url.split('/');
                 window.open(home_url + '/app/view_document/'+result[3]);
                }
                );
          //  console.log(JSON.stringify(Blob.url));
            
            },
            error: function () {
                alert("<?php echo $alert_messages['network_error_occured']; ?>");
            }
        });           
                     
                     
  
        
    });
                                    
                                </script>    
                                
                                
                                
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>

                            <div class='clearfix'></div>
                            <input type="file" name="data[attachment][]" class="attachment edit-controls" placeholder="attach file" style="display:none;" />
                            <input type="hidden" name="data[remove_attachment]" class="js-remove-attachment" />
                        </div>
                    </div>

                    <div class="controls view-controls">
                        <?php $isDiscussionReplyOwner = ($parentDiscussion['Comment']['created_by'] == $user_current_account['User']['id']); ?>
                        <?php if ($isAdminRole || $canMaintainFolder || ($isUserRole && $isDiscussionReplyOwner)): ?>
                        <a id="edit<?php echo$parentDiscussion['Comment']['id']; ?>" href="#" class="edit" id="parentDiscussionEdit"><?php echo $language_based_content['edit_discussion']; ?></a>
                        <a href="<?php echo $this->base . '/Huddles/deleteComment/' . $huddle_id . "/" . $parentDiscussion['Comment']['id']; ?>"
                           id = "delete_comment-<?php echo $parentDiscussion['Comment']['id']; ?>"
                           class="delete delete-comment main"><?php echo $language_based_content['delete_discussion']; ?></a>
                        <?php endif; ?>
                    </div>

                    <div class="edit-controls" style="display:none;">
                        <button class="btn btn-green inline js-update-btn" name="commit"><?php echo $language_based_content['update_discussion']; ?></button>
                        <a id="cancel_header" class="btn btn-transparent js-cancel-btn"><?php echo $language_based_content['cancel_discussion']; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
                  $('#edit<?php echo $parentDiscussion['Comment']['id']; ?>').on('click', function (e) {
        $('.delete_doc<?php echo $parentDiscussion['Comment']['id']; ?>').show();
        var text  = $('#target_div<?php echo $parentDiscussion['Comment']['id']; ?>').html();
        $('.wysihtml5-sandbox').contents().find('.wysihtml5-editor').html(text);
        $('ul > li.last-textarea > div').eq(1).find('iframe').contents().find('.wysihtml5-editor').html('<?php echo $language_based_content['type_message_here_discussion'];?>');
        
    });   
    
                 $('#cancel_header').on('click', function (e) {
        $('.delete_doc<?php echo$parentDiscussion['Comment']['id']; ?>').hide();
        
    });  
    
    
       $(document).on("click", ".delete_doc<?php echo$parentDiscussion['Comment']['id']; ?>", function(){
             var r = confirm('Are you sure want to delete this file?');
                if (r) {
                  $(this).parent('li').remove();
                                    var document_id = $(this).attr("del_document_id");
                                           $.ajax({
                                           url: home_url + "/huddles/delete_document_file/" + document_id,
                                           type: 'POST',
                                           success: function (response) {

                                           },
                                           error: function () {

                                           }
                                       });
                }
                else{
                    return false;
                }
            
                                    
                                    
                                });
          
    
    
    
        </script>

    <div class="span10" style="font-weight: bold;margin: 0px;margin-top: 10px;margin-bottom: 10px;    width: 100%;
    text-align: center;
    font-size: 20px;"><?php echo $language_based_content['discuss_this_topic_discussion']; ?></div>
    <ul class="append-li-discussion" style="padding-left: 70px;">
    <?php if (count($discussionReply) > 0): ?>
        <?php foreach ($discussionReply as $row): ?>
        <li class="discussion-reply" id="discussion-reply-<?php echo $row['Comment']['id']; ?>">
                <?php
                $profile_image = isset($row['User']['image']) ? $row['User']['image'] : '';
                $profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";
                //if (file_exists($profile_image_real_path) && $profile_image != '') {
                if ($profile_image != '') {
                    //$profile_image = $this->webroot . "img/users/$profile_image";
                    $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $row['User']['user_id'] . "/" . $row['User']['image']);
                } else {
                    //$profile_image = $this->webroot . "img/profile.jpg";
                    $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/profile.jpg');
                }
                ?>
            <div style="margin: 0px;" class="span1">
                <img src="<?php echo $profile_image; ?>" alt="img" height="42px" width="42px" class="circle"/>
            </div>
            <div class="text text-width span10">
                <div class="discussion-editor-row" style="padding-top:0px;">
                    <label class="discussion-username"><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?></label>
                    <div class="discussion-editor-row-content view-controls"><?php echo stripslashes(make_clickable($row['Comment']['comment'])) ?></div>
                    <div class="clearfix"></div>
                    <div class="input-group edit-controls" style="display:none;">
                        <div id="<?php echo "editor" . $row['Comment']['id'] . "-toolbar" ?>" class="editor-toolbar">
                            <a data-wysihtml5-command="bold">bold</a>
                            <a data-wysihtml5-command="italic">italic</a>
                            <a data-wysihtml5-command="insertOrderedList">ol</a>
                            <a data-wysihtml5-command="insertUnorderedList">ul</a>
                            <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                        </div>
                        <textarea id="<?php echo "editor-" . $row['Comment']['id'] ?>"
                                  class="editor-textarea larg-input" cols="48" rows="8"
                                  placeholder="<?php echo $language_based_content['type_message_here_discussion']; ?>"><?php echo $row['Comment']['comment']; ?></textarea>
                        <div style ="display:none;" id="target_div<?php echo $row['Comment']['id']; ?>"><?php echo html_entity_decode(htmlspecialchars($row['Comment']['comment'])); ?></div>
                        <input type="hidden" class="hid_id" id="txtCommentID<?php echo $row['Comment']['id']; ?>" value="<?php echo $row['Comment']['id']; ?>"/>

                    </div>

                    <div class="file-area">
                        <div class="input-group">
                            <ul class="js-attachment-list">
                                    <?php $attachedDocuments = $Document->getByCommentId($row['Comment']['id']); ?>
                                    <?php if (!empty($attachedDocuments)): ?>
                                        <?php foreach($attachedDocuments as $doc): ?>
                                <li>
                                                <?php
                                                $videoFilePath = pathinfo($doc['Document']['url']);
                                                $ext = $videoFilePath['extension'];
                                                if ($canDownloadFile) {
                                                    $href = $this->base . '/Huddles/download/' . $doc['Document']['id'];
                                                } else {
                                                    $href = '';
                                                }
                                                ?>
                                    <a  id="url_generator_<?php echo $doc['Document']['id'];?>" href="javascript:void(0);<?php //echo $href;?>" target="_blank">
                                        <img class="icon24" src="<?php echo $this->Custom->getDocTypeIconLink($ext);?>" alt="doc type icon" />
                                        <span style="width:auto;"><?php echo $doc['Document']['original_file_name'];?></span>
   
                                    </a>
                                    
                                    
                              <span style="width:auto;"><a style ="width:auto; cursor: pointer;" download href="<?php echo $href; ?>">
                 <img alt="Download" class="right smargin-right" style="height: 24px;" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>" title="download document"  />
                
                </a> </span>
                                    
                                    
                                    
                                    
                                    
                                                                         <span del_document_id="<?php echo $doc['Document']['id'];?>" style="display:none;
    cursor: pointer;
    font-weight: 400;
    text-decoration: underline;
    color: red;
    font-size: 14px;
   
    position: relative;
    width:auto;" class = "delete_doc<?php echo $row['Comment']['id']; ?>" id="delete_discussion_file_<?php echo $doc['Document']['id'];?>">Remove</span>
                                    <i class="remove edit-controls" style="display:none;">remove</i>
                                    <input type="hidden" name="document_id" value="<?php echo $doc['Document']['id'];?>" />

                                    
                                
                                    
                                </li>
                                <script>
//                                $('#delete_discussion_file_<?php //echo $doc['Document']['id'];?>').on('click', function (e) {
//                                    $(this).parent('li').remove();
//                                    var document_id = '<?php //echo $doc['Document']['id'];?>';
//                                           $.ajax({
//                                           url: home_url + "/huddles/delete_document_file/" + document_id,
//                                           type: 'POST',
//                                           success: function (response) {
//
//                                           },
//                                           error: function () {
//
//                                           }
//                                       });
//                                    
//                                });
//       
//                               
                                               $('#url_generator_<?php echo $doc['Document']['id'];?>').on('click', function (e) {
                     
                     
             $.ajax({
            url: home_url + "/huddles/generate_url/<?php echo $doc['Document']['id'];?>",
            type: 'POST',
            success: function (response) {
                
                filepicker.setKey('A3w6JbXR2RJmKr3kfnbZtz');            

                 filepicker.storeUrl(
                response,
                {filename: 'something.png'},
                function(Blob){
                    
                var result=Blob.url.split('/');
                 window.open(home_url + '/app/view_document/'+result[3]);
                }
                );
          //  console.log(JSON.stringify(Blob.url));
            
            },
            error: function () {
                alert("<?php echo $alert_messages['network_error_occured']; ?>");
            }
        });           
                     
                     
  
        
    });
                                        
                                        
                                        
                                </script>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                            </ul>
                            <div class='clearfix'></div>
                            <input type="file" name="data[attachment][]" class="attachment edit-controls" placeholder="attach file" style="display:none;" />
                            <input type="hidden" name="data[remove_attachment]" class="js-remove-attachment" />
                        </div>
                    </div>

                    <div class="controls view-controls span9">
                            <?php
                            $commentDate  = $row['Comment']['created_date'];
                            $commentsDate = $this->Custom->formateDate($commentDate);
                            $isDiscussionReplyOwner = ($row['Comment']['created_by'] == $user_current_account['User']['id']);
                            ?>
                        <div class="posted-date"><?php echo $language_based_content['posted_discussion']; ?> <?php echo $commentsDate; ?></div>
                            <?php if ($isAdminRole || $canMaintainFolder || ($isUserRole && $isDiscussionReplyOwner)): ?>
                        <a href="#desc-container-<?php echo $row['Comment']['id']; ?>" id="edit<?php echo $row['Comment']['id']; ?>" class="edit"><?php echo $language_based_content['edit_discussion']; ?></a>
                        <a href="<?php echo $this->base . '/discussion/delete/' . $huddle_id . "/" . $row['Comment']['id']; ?>"
                           id = "delete_comment-<?php echo $row['Comment']['id']; ?>"
                           class="delete delete-comment"><?php echo $language_based_content['delete_discussion']; ?></a>
                            <?php endif; ?>
                    </div>

                    <div class="edit-controls" style="display:none;">
                        <button class="btn btn-green inline js-update-btn" name="commit"><?php echo $language_based_content['update_discussion']; ?></button>
                        <a id="cancel-edit-discussion<?php echo $row['Comment']['id']; ?>" class="btn btn-transparent js-cancel-btn"><?php echo $language_based_content['cancel_discussion']; ?></a>
                    </div>
                </div>
            </div>
        </li>
        
        <script>
          
          
        $('#edit<?php echo $row['Comment']['id']; ?>').on('click', function (e) {
        $('.delete_doc<?php echo $row['Comment']['id']; ?>').show();
        var text  = $('#target_div<?php echo $row['Comment']['id']; ?>').html();
        $('.wysihtml5-sandbox').contents().find('.wysihtml5-editor').html(text);
        $('ul > li.last-textarea > div').eq(1).find('iframe').contents().find('.wysihtml5-editor').html('<?php echo $language_based_content['type_message_here_discussion'];?>');
        
    });
    
    
     $('#cancel-edit-discussion<?php echo $row['Comment']['id']; ?>').on('click', function (e) {
        $('.delete_doc<?php echo $row['Comment']['id']; ?>').hide();
        $('.editor-textarea').hide();
        
        
    });
    
    
    
     $(document).on("click", ".delete_doc<?php echo $row['Comment']['id']; ?>", function(){
             var r = confirm('Are you sure want to delete this file?');
                if (r) {
                  $(this).parent('li').remove();
                                    var document_id = $(this).attr("del_document_id");
                                           $.ajax({
                                           url: home_url + "/huddles/delete_document_file/" + document_id,
                                           type: 'POST',
                                           success: function (response) {

                                           },
                                           error: function () {

                                           }
                                       });
                }
                else{
                    return false;
                }
            
                                    
                                    
                                });
          
    
            </script>
        
        <?php endforeach; ?>
    <?php endif; ?>
    </ul>
<!--    <div class="tab-header" style="border-bottom: 2px #f2f2f2 solid">       -->
        <!--<div class="span12" style="margin-left: 0px; margin-top: 20px;">-->
            <!--<div class="text text-width span10">-->
                      
<!--                    <div class="file-area">
                        <div class="input-group discussion-editor-row" id="browse-attachment" style="display: block;">
                            <ul class="js-attachment-list"></ul>
                            <div class='clearfix'></div>
                            <input class="attachment" id="comment_attachment" name="data[attachment][]" placeholder="attach file" type="file" />
                        </div>
                    </div> -->
                
            <!--</div>-->
        <!--</div>-->
    <!--</div>-->
    <ul>
    <?php echo $replayForm; ?>
    </ul>

</div>

<script>
    
   
    
   
    
        function strip_tags(str) {
    str = str.toString();
    return str.replace(/<\/?[^>]+>/gi, '');
}

    
    
      
                                
    
    
    
    
</script>
