<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
?>
<style>
.tab-box2{
    width:100%;
}
.table{
    width:100%;
}
.head{
    width:100%;
}
.table ul li{
    width: 100%;
}
.head strong.name{
    padding: 0 320px 0 0;
}
.head strong.type{
    padding: 0 130px 0 0;
}
.head strong.date{
    padding: 0 125px 0 0;
}
.table ul li a{
    width: 368px;
}
.table ul li strong.type{
    width: 170px;
}
.doc_video_association .row{
    width: 115px;
}
</style>

<div class="tab-content <?php echo $tab == '2' ? 'tab-active' : 'tab-hidden';?>" id="tabbox2">
    <div class="tab-2">
        <form id="huddle-download-form" action="<?php echo $this->base . '/Huddles/downloadZip/' . $huddle_id ?>" method="post" style="margin-left:14px;">
            <div class="tab-header">
                <div class="tab-header__title"> <strong><?php echo $language_based_content['resources_tab_huddles']; ?></strong>
                    <div style="clear: both;"></div>

                    <div class="docs-list__head2 span3" style="margin: 0px; margin-top: 9px;">
                        <?php if ($huddle_permission == 200 || $huddle_permission == 210 || $huddle_permission == 220 || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                            <a style="cursor: pointer; margin-right: 30px; font-weight: normal;" id="btnDownloadAsZip" class="js-file-download blue-link icon-download2" onclick="return downloadItemsAsZip();"><?php echo $language_based_content['download_rescource_tab_huddles']; ?></a>
                        <?php endif; ?>
                        <?php if ($huddle_permission == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                            <a  id="delete-account-doc" onclick="return deleteDoc();" style="cursor: pointer" class="icon-delete2 blue-link delete js-file-delete"><?php echo $language_based_content['delete_rescource_tab_huddles']; ?></a>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="tab-header__right">
                    <div style="float:left;position:relative;">
                        <input type="button" id="btnSearchDocuments" class="btn-search" value="">
                        <input type="hidden" name="download_as_zip_docs" id="download_as_zip_docs" value=""/>
                        <input type="hidden" name="huddle_id" id="huddle_id" value="<?php echo $huddle_id ?>"/>
                        <input class="text-input" type="text" value="" id="txtSearchDocuments" placeholder="<?php echo $language_based_content['search_resources_placeholder']; ?>">
                        <span id="clearButton" style="display:none;right:40px;top:4px;position:absolute;cursor:pointer;" class="clear-input-box">X</span>
                    </div>
                    <div class="select"  style="float:left;margin-left:12px;">
                        <select id="cmbDocumentSort" name="upload-date">
                            <option value="Document.created_date DESC"><?php echo $language_based_content['date_uploaded_resource_tab']; ?></option>
                            <option value="afd.title ASC"><?php echo $language_based_content['search_resources_placeholder']; ?></option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <form id="huddle-delete-form" action="<?php echo $this->base . '/Huddles/deleteDocs/' . $huddle_id ?>" method="post">
            <input type="hidden" id="document_ids" name="document_ids" value=""/>
            <input type="hidden" id="account-folder-id" name="account_folder_id" value=""/>
        </form>
    </div>
    <div class="tab-box2">
        <div class="table">
            <div id="extra-row-li" style="display: none">
                <div class="padd">
                    <a rel="nofollow" data-confirm="<?php echo $language_based_content['are_you_sure_del_resource']; ?>" id="delete-main-comments" class="icon-doc icon-indent blue-link delete">&nbsp;</a>
                    <label class="ui-checkbox checkbox-2">
                        <input class="checkbox-2 download_doc_checkbox" disabled="disabled" name="document_ids[]" type="checkbox" value="">
                    </label>
                    <a href="#" class="nomargin-vertical wrap">
                        <span class="image">&nbsp;</span> <span id="doc-title"></span>
                    </a>
                    <strong id="doc-type" class="type "></strong>
                    <strong id="doc-date" class="date"><?php echo date('M d, Y', time()) ?></strong>
                    <div class="wrap" style="width: 75px; float: left;"> ----- </div>
                    <div class="doc_video_association">
                        <div class="subject-row row">
                            <div class="video-dropdown">Videos</div>
                        </div>
                    </div>
                </div>
                <form id="delete-document0" action="<?php echo $this->base . '/Huddles/deleteDocument/0'; ?>" accept-charset="UTF-8"></form>
                <input type="hidden" name="account_folder_id" id="huddle_id" value="<?php echo $huddle_id ?>"/>
            </div>

            <div class="head">
                <label class="ui-checkbox left" style="margin: 11px 12px 0 12px;">
                    <input class="head-checkbox" type="checkbox">
                </label>
                <strong class="name"><?php echo $language_based_content['name_resource_list']; ?></strong>
                <strong class="type"><?php echo $language_based_content['type_resource_list']; ?></strong>
                <strong class="date"><?php echo $language_based_content['date_uploaded_list']; ?></strong>
                <strong class="associated"><?php echo $language_based_content['associated_videos_list']; ?></strong>
            </div>

            <ul class="ul-docs"></ul>
        </div>
    </div>
</div>
