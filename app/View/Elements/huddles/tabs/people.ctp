<?php
$users_shown = array();
$groups_shown = array();
//echo '<pre>';
//print_r($people);
//exit;
?>

<?php 
    $admins = array();
    $coach = array();
    $members = array();
    $coachee = array();
    $viewers = array();

    if (isset($people['admin']['users'])) {

        foreach($people['admin']['users'] as $participant){
            if($participant['is_coach'] == '1'){
                $coach[] = $participant;
            }
            if($participant['is_coachee'] == '1'){
                $coachee[] = $participant;
            }
            if($participant['is_coach'] == '0' && $participant['is_coachee'] == '0'){
                if($user_id == $participant['id']){
                    $coach[] = $participant;    
                }else{
                    $admins[] = $participant;   
                }
            }
        }

    }

    if (isset($people['member']['users'])) {

        foreach($people['member']['users'] as $participant){        
            if($participant['is_coach'] == '0' && $participant['is_coachee'] == '1'){
                $coachee[] = $participant;
            }
            if($participant['is_coach'] == '1' && $participant['is_coachee'] == '0'){
                $coach[] = $participant;
            }
            if($participant['is_coach'] == '0' && $participant['is_coachee'] == '0'){
                $members[] = $participant;
            }
        }

    }
    
    if(isset($people['admin']['group'])){
        foreach($people['admin']['group'] as $participant){
            $coach[] = $participant;
        }
    }
    if(isset($people['member']['group'])){
        foreach($people['member']['group'] as $participant){
            $coachee[] = $participant;
        }
    }

//print_r($coach);    
//print_r($coachee);    
//print_r($admins);    
//die;    
?>
<div class="tab-content <?php if ($tab == '4'): echo 'tab-active'; else: echo 'tab-hidden'; endif; ?>" id="tabbox4" <?php if ($tab == '4'): echo 'style="visibility: visible"'; else:echo 'style="display: none"'; endif; ?>>
    <div class="tab-header">
        <h1 class="tab-header__title" style="font: bold 25px 'Segoe UI';"><?php echo $language_based_content['participants_huddle_tab'];  ?></h1>
    </div>
<?php if ((isset($huddle_type) && $huddle_type == 1 )): ?>

<?php if ((isset($people['admin']['users']) && count($people['admin']['users']) > 0 ) || (isset($people['admin']['group']) && count($people['admin']['group']) > 0)): ?>
    <h4 class="huddle_participant_title"><?php echo $language_based_content['admins_participants_tab']; ?></h4>
    <div class="video-huddle-users">
        <?php
        if (isset($people['admin']['users']) && count($people['admin']['users']) > 0) :

            foreach ($people['admin']['users'] as $user):

                if (isset($user["id"]) && in_array($user["id"], $users_shown))
                    continue;
                ?>
                <a style="cursor: pointer;">
                    <?php if (isset($user['image']) && $user['image'] != ''): ?>
                        <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                        ?>
                        <?php echo $this->Html->image($avatar_path, array('alt' => $user['first_name'] . " " . $user['last_name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                    <?php else: ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                    <?php endif; ?>
                    <br/>
                    <div style="clear:both"><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></div>
                </a>

                <?php
                $users_shown[] = $user["id"];

            endforeach;
            ?>

            <?php
        endif;
        if (isset($people['admin']['group']) && count($people['admin']['group']) > 0):
            ?>
            <?php foreach ($people['admin']['group'] as $grp): ?>

                <?php if (isset($grp["group_id"]) && in_array($grp["group_id"], $groups_shown)) continue; ?>
                <a class ="is_group" group_id ="<?php echo $grp["group_id"]; ?>" style="cursor: pointer;">
                <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/groupimg.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                <br/>
                <div style="clear:both"><?php echo $grp['name']; ?></div>
                </a>
                <?php
                $groups_shown[] = $grp["group_id"];
                ?>

            <?php endforeach; ?>
        <?php endif; ?>
        <hr/>
    </div>
<?php endif; ?>

<?php if ((isset($people['member']['users']) && count($people['member']['users']) > 0) || (isset($people['member']['group']) && count($people['member']['group']) > 0)): ?>
    <div class="video-huddle-users">
        <h4 class="huddle_participant_title"><?php echo $language_based_content['members_participants_tab']; ?></h4>
        <?php if (isset($people['member']['users']) && count($people['member']['users']) > 0): ?>
            <?php
            foreach ($people['member']['users'] as $user):

                if (isset($user["id"]) && in_array($user["id"], $users_shown))
                    continue;
                ?>
                <a style="cursor: pointer;">
                    <?php
                    
                        $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                    ?>
                    <?php if (isset($user['image']) && $user['image'] != ''): ?>
                        <?php echo $this->Html->image($avatar_path, array('alt' => $user['first_name'] . " " . $user['last_name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                    <?php else: ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                    <?php endif; ?>
                    <br/>
                    <div style="clear:both"><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></div>
                </a>

                <?php
                $users_shown[] = $user["id"];
                ?>

            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (isset($people['member']['group']) && count($people['member']['group']) > 0): ?>
            <?php foreach ($people['member']['group'] as $grp): ?>
                <?php if (isset($grp["group_id"]) && in_array($grp["group_id"], $groups_shown)) continue; ?>
                <a class ="is_group" group_id ="<?php echo $grp["group_id"]; ?>" style="cursor: pointer;">
                <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/groupimg.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                <br/>
                <div style="clear:both"><?php echo $grp['name']; ?></div>
                </a>
                <?php
                $groups_shown[] = $grp["group_id"];
                ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <hr/>
<?php endif; ?>

<?php if ((isset($people['viewer']['users']) && count($people['viewer']['users']) > 0) || (isset($people['viewer']['group']) && count($people['viewer']['group']) > 0)): ?>
    <div class="video-huddle-users">
        <h4 class="huddle_participant_title"><?php echo $language_based_content['viewers_participants_tab']; ?></h4>
        <?php if (isset($people['viewer']['users']) && count($people['viewer']['users']) > 0): ?>
            <?php
            foreach ($people['viewer']['users'] as $user):
                ?>
                <?php
                if (isset($user["id"]) && in_array($user["id"], $users_shown))
                    continue;
                ?>
                <a style="cursor: pointer;">
                    <?php
                     
                        $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                    ?>
                    <?php if (isset($user['image']) && $user['image'] != ''): ?>
                        <?php echo $this->Html->image($avatar_path, array('alt' => $user['first_name'] . " " . $user['last_name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                    <?php else: ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
            <?php endif; ?>
                    <br/>
                    <div style="clear:both"><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></div>
                </a>
                <?php
                $users_shown[] = $user["id"];
                ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (isset($people['viewer']['group']) && count($people['viewer']['group']) > 0): ?>
            <?php foreach ($people['viewer']['group'] as $grp): ?>
                <?php if (isset($grp["group_id"]) && in_array($grp["group_id"], $groups_shown)) continue; ?>
                <a class ="is_group" group_id ="<?php echo $grp["group_id"]; ?>" style="cursor: pointer;" >
                <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/groupimg.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                <br/>
                <div style="clear:both"><?php echo $grp['name']; ?></div>
                </a>
                <?php
                $groups_shown[] = $grp["group_id"];
                ?>
            <?php endforeach; ?>
    <?php endif; ?>
    </div>
    <hr/>
<?php endif; ?>

<?php endif; ?>
<?php if ((isset($huddle_type) && $huddle_type == 2 )): ?>
<?php if ((isset($coach) && count($coach) > 0 )): ?>
    <h4 class="huddle_participant_title"><?php echo $language_based_content['coach_participants_tab']; ?></h4>

    <div class="video-huddle-users">
        <?php
        if (isset($coach) && count($coach) > 0) :

            foreach ($coach as $user):
                if(isset($user["group_id"]))
                    $usr = $user["group_id"];
                else
                    $usr = $user["id"];
                if (isset($usr) && in_array($usr, $users_shown))
                    continue;
                $username = isset($user['first_name'])?$user['first_name'].' '.$user['last_name']:$user['name'];
                ?>
                
                <a style="cursor: pointer;">
                    <?php if (isset($user['image']) && $user['image'] != ''): ?>
                        <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                        ?>
                        <?php echo $this->Html->image($avatar_path, array('alt' => $username, 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                    <?php else: ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                    <?php endif; ?>
                    <br/>
                    <div style="clear:both"><?php echo $username; ?></div>
                </a>

                <?php
                if(isset($user["group_id"]))
                    $users_shown[] = $user["group_id"];
                else    
                    $users_shown[] = $user["id"];

            endforeach;
            ?>

            <?php
        endif;
         ?>
        <hr/>
    </div>
<?php endif; ?>
<?php if ((isset($coachee) && count($coachee) > 0)): ?>
    <div class="video-huddle-users">
    <h4 class="huddle_participant_title"><?php echo $language_based_content['coachee_participants_tab']; ?></h4>
        <?php if (isset($coachee) && count($coachee) > 0): ?>
            <?php
            foreach ($coachee as $user):
                if(isset($user["group_id"]))
                    $usr = $user["group_id"];
                else
                    $usr = $user["id"];
                if (isset($usr) && in_array($usr, $users_shown))
                    continue;
                $username = isset($user['first_name'])?$user['first_name'].' '.$user['last_name']:$user['name'];
                ?>
                <a style="cursor: pointer;">
                    <?php if (isset($user['image']) && $user['image'] != ''): ?>
                        <?php
                         
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $usr . "/" . $user['image'], $user['image']);
                        ?>
                        <?php echo $this->Html->image($avatar_path, array('alt' => $username, 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                    <?php else: ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                    <?php endif; ?>
                    <br/>
                    <div style="clear:both"><?php echo $username; ?></div>
                </a>

                <?php
                if(isset($user["group_id"]))
                    $users_shown[] = $user["group_id"];
                else    
                    $users_shown[] = $user["id"];
                ?>

            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <hr/>
<?php endif; ?>
<?php if ((isset($admins) && count($admins) > 0 )): ?>
    <h4 class="huddle_participant_title"><?php echo $language_based_content['admin_participants_tab']; ?></h4>

    <div class="video-huddle-users">
        <?php
        if (isset($admins) && count($admins) > 0) :

            foreach ($admins as $user):
                if(isset($user["group_id"]))
                    $usr = $user["group_id"];
                else
                    $usr = $user["id"];
                if (isset($usr) && in_array($usr, $users_shown))
                    continue;
                $username = isset($user['first_name'])?$user['first_name'].' '.$user['last_name']:$user['name'];
                ?>
                
                <a style="cursor: pointer;">
                    <?php if (isset($user['image']) && $user['image'] != ''): ?>
                        <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                        ?>
                        <?php echo $this->Html->image($avatar_path, array('alt' => $username, 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                    <?php else: ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                    <?php endif; ?>
                    <br/>
                    <div style="clear:both"><?php echo $username; ?></div>
                </a>

                <?php
                if(isset($user["group_id"]))
                    $users_shown[] = $user["group_id"];
                else    
                    $users_shown[] = $user["id"];

            endforeach;
            ?>

            <?php
        endif;
         ?>
        <hr/>
    </div>
<?php endif; ?>
<?php if ((isset($members) && count($members) > 0)): ?>
    <div class="video-huddle-users">
    <h4 class="huddle_participant_title"><?php echo $language_based_content['members_participants_tab']; ?></h4>
        <?php if (isset($members) && count($members) > 0): ?>
            <?php
            foreach ($members as $user):
                if(isset($user["group_id"]))
                    $usr = $user["group_id"];
                else
                    $usr = $user["id"];
                if (isset($usr) && in_array($usr, $users_shown))
                    continue;
                    
                $username = isset($user['first_name'])?$user['first_name'].' '.$user['last_name']:$user['name'];
                ?>
                <a style="cursor: pointer;">
                    <?php if (isset($user['image']) && $user['image'] != ''): ?>
                        <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                        ?>
                        <?php echo $this->Html->image($avatar_path, array('alt' => $username, 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                    <?php else: ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                    <?php endif; ?>
                    <br/>
                    <div style="clear:both"><?php echo $username; ?></div>
                </a>

                <?php
                if(isset($user["group_id"]))
                    $users_shown[] = $user["group_id"];
                else    
                    $users_shown[] = $user["id"];
                ?>

            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <hr/>
<?php endif; ?>
<?php endif; ?>
<?php if ((isset($huddle_type) && $huddle_type == 3 )): ?>
    <?php if ((isset($coach) && count($coach) > 0 )): ?>
    <h4 class="huddle_participant_title"><?php echo $language_based_content['assessor_participants_tab']; ?></h4>

    <div class="video-huddle-users">
        <?php
        if (isset($coach) && count($coach) > 0) :

            foreach ($coach as $user):
                if(isset($user["group_id"]))
                    $usr = $user["group_id"];
                else
                    $usr = $user["id"];
                if (isset($usr) && in_array($usr, $users_shown))
                    continue;
                $username = isset($user['first_name'])?$user['first_name'].' '.$user['last_name']:$user['name'];
                ?>
                
                <a style="cursor: pointer;">
                    <?php if (isset($user['image']) && $user['image'] != ''): ?>
                        <?php
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['id'] . "/" . $user['image'], $user['image']);
                        ?>
                        <?php echo $this->Html->image($avatar_path, array('alt' => $username, 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                    <?php else: ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                    <?php endif; ?>
                    <br/>
                    <div style="clear:both"><?php echo $username; ?></div>
                </a>

                <?php
                if(isset($user["group_id"]))
                    $users_shown[] = $user["group_id"];
                else    
                    $users_shown[] = $user["id"];

            endforeach;
            ?>

            <?php
        endif;
         ?>
        <hr/>
    </div>
<?php endif; ?>
    <?php if ((isset($coachee) && count($coachee) > 0)): ?>
    <div class="video-huddle-users">
    <h4 class="huddle_participant_title"><?php echo $language_based_content['assessed_participants_tab']; ?></h4>
        <?php if (isset($coachee) && count($coachee) > 0): ?>
            <?php
            foreach ($coachee as $user):
                $bool_group = false;
                if(isset($user["group_id"]))
                {
                    $usr = $user["group_id"];
                    $bool_group = true;
                }   
                else
                {
                    $usr = $user["id"];
                }
                if (isset($usr) && in_array($usr, $users_shown))
                    continue;
                $username = isset($user['first_name'])?$user['first_name'].' '.$user['last_name']:$user['name'];
                ?>
                <a style="cursor: pointer;" <?php if($bool_group) { echo 'group_id ='.$user["group_id"]. ' class ="is_group"'; } ?>>
                    <?php if (isset($user['image']) && $user['image'] != ''): ?>
                        <?php
                         
                            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $usr . "/" . $user['image'], $user['image']);
                        ?>
                        <?php echo $this->Html->image($avatar_path, array('alt' => $username, 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '53', 'width' => '53')); ?>
                    <?php else: ?>
                        <?php if(isset($user["group_id"])): ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/groupimg.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php else: ?>
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <?php endif; ?>
                    <?php endif; ?>
                    <br/>
                    <div style="clear:both"><?php echo $username; ?></div>
                </a>

                <?php
                if(isset($user["group_id"]))
                    $users_shown[] = $user["group_id"];
                else    
                    $users_shown[] = $user["id"];
                ?>

            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <hr/>
<?php endif; ?>
<?php endif; ?> 
</div>
<style type="text/css">
    .modal_table tbody tr:first-child td {
    border-top-width: 2px;
}
.modal_table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 1rem;
    background-color: #fff;
    max-height: 300px;
    display: inline;
    overflow-y: scroll;
}
.modal_table tbody td {
    font-weight: 500!important;
}
.modal_table thead td{font-weight: 800;}
.okbtn { text-align: right;  padding: 9px 26px;}
.okbtn a{background: #518fcc;
    color: #fff;
    border: 0;
    display: inline-block;
    padding: 9px 20px;
    border-radius: 4px; cursor: pointer;
   
    }
</style>


<script>
    
        $(document).on('click', '.is_group', function(e) {
        e.preventDefault();
        var group_id = $(this).attr('group_id');
        $.ajax({
            type: "POST",
            url: home_url + "/Huddles/get_group_details/" + group_id,
            dataType: 'json',
            data: {
            },
            success: function(response) {
                console.log(response);
                $('#group_users_append').html(response.html);
                $('.group_name').html(response.group_name);
                $("#modal_click").click();
                
            },
            error: function(e) {

            }
        });
    });
    
</script>

<a data-toggle="modal" data-target="#is_group" style = "display:none;" id = "modal_click"></a>

<div id="is_group" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog"  style="width: 500px;">
        <div class="modal-content">
            <div class="header" style="padding-left: 25px;padding-bottom: 15px;margin-bottom: 0px;padding-top: 15px;">
                <h4 class="header-title nomargin-vertical smargin-bottom group_name">Group Users</h4>
                <a id="cross1" class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">x</a>
            </div>
            
            <div class="content_modal">
                
                <table class="table modal_table">
    	
    	<thead>
    		<tr _ngcontent-c9="">
    			<td style = "width: 130px;max-width: 130px;"><label >Name</label> </td>
    			<td style = "width: 350px;max-width: 350px;" > <label >Email</label></td>
    		</tr>
    	</thead>
    	<tbody id = "group_users_append">
    		

    	</tbody>

    </table>
                <div class="okbtn">
                <a id="cross1" data-dismiss="modal">Ok</a>
                </div>
                 
                 
            </div>
            
        </div>
    </div>
 </div>   