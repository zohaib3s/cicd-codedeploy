<?php
$total_participants_count = 0;
$predefined_users_ids = '';
$users = $this->Session->read('user_current_account');
$huddle = $huddle[0];

if (isset($huddles_users)) {
    for ($i = 0; $i < count($huddles_users); $i++) {
        $user = $huddles_users[$i];

        if ($users['User']['id'] == $user['huddle_users']['user_id'])
            continue;

        if (!empty($predefined_users_ids))
            $predefined_users_ids .= ',';
        $predefined_users_ids .= $user['huddle_users']['user_id'];
        $total_participants_count += 1;
    }
}
if (isset($videoHuddleGroups) && !empty($videoHuddleGroups[0]['User']['id']) && count($videoHuddleGroups) > 0) {
    for ($i = 0; $i < count($huddles_users); $i++) {
        $user = $huddles_users[$i]['User'];
        if (isset($user['id']) && !empty($user['id'])) {
            if ($users['User']['id'] == $user['id']) {
                continue;
            }
            if (!empty($predefined_users_ids)) {
                $predefined_users_ids .= ',';
                $predefined_users_ids .= $user['id'];
                $total_participants_count += 1;
            }
        }
    }
}
?>
<style>
    .wysihtml5-sandbox{
        padding:10px !important;
    }
</style>
<div>
    <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/3'; ?>" class="back back-add-discussion" >Back to all disscussions</a>
</div>
<div style="clear: both;"></div>
<form action="<?php echo $this->base . '/Huddles/comments/' . $huddle_id . '/3'; ?>" id="frmAddDiscussion" name="comments" method="post" enctype="multipart/form-data" method="post" target="">
    <input id="tokentag" type="hidden" name="data[user_id]" value="<?php echo $user_id ?>" />
    <input id="comment_html_2" type="hidden" name="data[comment]" value="" />
    <input id="comment_access_level" name="data[access_level]" type="hidden" value="admins" />
    <input id="comment_access_level" name="data[commentable_id]" type="hidden" value="" />
    <div class="input-group large"  >
        <?php echo $this->Form->input('title', array('placeholder' => $language_based_content['type_subject_of_discussion'], 'class' => 'larg-input', 'div' => FALSE, 'label' => FALSE, 'required' => true)); ?>
    </div>
    <div id="desc-container" class="input-group">
        <div id="editor2-toolbar" class="editor-toolbar" style="display: none;">
            <a data-wysihtml5-command="bold">bold</a>
            <a data-wysihtml5-command="italic">italic</a>
            <a data-wysihtml5-command="insertOrderedList">ol</a>
            <a data-wysihtml5-command="insertUnorderedList">ul</a>
            <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
        </div>
        <?php echo $this->Form->textarea('comment_editor', array('id' => 'editor-2', 'class' => 'editor-textarea larg-input', 'cols' => '48', 'rows' => '8', 'placeholder' => $language_based_content['type_message_here_discussion'], 'required' => true)); ?>
        <?php echo $this->Form->error('videoHuddle.message'); ?>
        <input id="txtPreDefinedNotifiers" type="hidden" value="<?php echo $predefined_users_ids; ?>" />
        <input name="data[notification_user_ids][]" id="txtNotifications" type="hidden" value="0" />
    </div>
    <div class="input-group">
        <a href="#" class="btn btn-white btn-light btn-gray-text icon2-clip" data-remote="true" id="attachment-file"><?php echo $language_based_content['attach_file_discussion']; ?></a>
    </div>
    <div class="input-group" id="browse-attachment" style="display: none;">
        <ul id="attachment_list" class="js-attachment-list"></ul>
        <div class='clearfix'></div>
        <input class="attachment" name="data[attachment][]" placeholder="attach file" type="file" />
    </div>
    <div class="input-group">
        <label><input type="radio" id="rdoPreDefined" value="1" name="send_email" style="margin-right: 10px;" checked/><?php echo $language_based_content['post_this_message_discussion']; ?></label>
    </div>
    <div class="input-group">
        <label><input type="radio" id="rdoLetMeChoose" value="2" name="send_email" style="margin-right: 10px;"/><?php echo $language_based_content['let_me_choose_discussion']; ?></label>
    </div>

    <div class="input-group" style="display: none;" id="users-list">
        <div class="email-to" id="user_list">
            <table>
                <?php
                $user_current_account = $this->Session->read('user_current_account');
                if ($huddles_users):
                    ?>
                    <tr>
                        <?php
                        $count = 1;
                        foreach ($huddles_users as $supper):
                            $tr = '';
                            if ($supper['huddle_users']['role_id'] == '220') {
                                continue;
                            }

                            if ($user_current_account['User']['id'] == $supper['huddle_users']['user_id']) {
                                continue;
                            }
                            if ($count % 4 == 0) {
                                $tr = '</tr><tr>';
                            }
                            ?>
                            <td style="width: 150px;">
                                <label class="ui-checkbox checkbox-2">
                                    <input class="user_checkbox" id="comment_notification_user_ids_reply_<?php echo $supper['huddle_users']['user_id'] ?>" name="chk_notification_user_ids" type="checkbox" value="<?php echo $supper['huddle_users']['user_id'] ?>" />
                                </label>
                                <label for="comment_notification_user_ids_<?php echo $supper['huddle_users']['user_id'] ?>"><?php echo strlen($supper['User']['first_name'] . ' ' . $supper['User']['last_name']) >= 16 ? mb_substr($supper['User']['first_name'] . ' ' . $supper['User']['last_name'], 0, 14) . '...' : $supper['User']['first_name'] . ' ' . $supper['User']['last_name'] ?></label>
                            </td>
                            <?php echo $tr; ?>
                            <?php $count++; ?>
                        <?php endforeach; ?>
                    </tr>

                    <div class='clearfix'></div>
                    <?php if ($huddles_users): ?>
                        <p class="check-buttons" style="margin-left: 10px;">
                            <a class="" id="reply_check_none" style="margin-right: 10px; text-decoration: underline;" href="javascript:uncheckAllUsers('.user_checkbox')">Select None</a>
                            <a class="" id="reply_check_all" style="margin-right: 10px; text-decoration: underline;" href="javascript:checkAllUsers('.user_checkbox')">Select All</a>
                        </p>
                    <?php endif; ?>

                </table>
            <?php endif; ?>
        </div>
        <div class='clearfix'></div>
    </div>
    <div class='clearfix'></div>
    <div class="input-group">
        <label><input type="radio" id="rdoNoneSelected" value="3" name="send_email" style="margin-right: 10px;"/><?php echo $language_based_content['dont_let_email_discussion']; ?></label>
    </div>
    <div class="input-group">
        <input class="btn btn-green inline" name="commit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" onclick="postDiscussion()" value="<?php echo $language_based_content['post_comment_discussion']; ?>"  />
    </div>
</form>
