<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<script type="text/javascript" src="//api.filepicker.io/v2/filepicker.js"></script>
<style>
    #tagFilterContainer .btn:active {
        box-shadow: none;
        cursor: default !important;
    }

    #tagFilterContainer .btn:focus {
        outline: none;
        cursor: default !important;
    }

    #tagFilterContainer .btn:hover {
        cursor: default !important;
    }

    #tagFilterContainer .btn {
        box-shadow: none !important;
        cursor: default !important;
        text-shadow: none !important;
        background: #f4f4f4;
    }

    .no-vid {
        width: 538px;
        height: 346px;
        background: transparent url(/app/img/new/no-video.jpg) no-repeat center;
        margin: 3px 0 0 -21px;
        text-align: center;
        padding: 1px 0 0 0;
        background-size: 93%;
    }

    .v_control button.fast {
        background: #3498DB;
    }

    .v_control button {
        cursor: pointer;
        border: 0;
        padding: 7px;
        border-radius: 5px;
        color: #fff;
    }

    .v_control button.wrin {
        background: #3a79a4;
    }

    .v_control {
        margin-top: 10px;
    }

    .v_control button img {
        width: 35px;
    }

    #tab-area .tab-content .tab-content {
        padding-top: 16px;
        margin-top: 3px;
    }

    .document_outer {
        background: #fff;
        padding: 10px;
        position: absolute;
        z-index: 99;
        border: 1px solid #ddd;
        float: right;
        right: 9px;
        top: 53px;
        display: none;
    }

    .document_outer:before {
        pointer-events: none;
        position: absolute;
        z-index: -1;
        content: '';
        border-style: solid;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-property: transform;
        transition-property: transform;
        left: calc(50% - 10px);
        top: 0;
        border-width: 0 10px 10px 10px;
        border-color: transparent transparent #e1e1e1 transparent;
        right: 1px;
        left: inherit;
        -webkit-transform: translateY(-10px);
        transform: translateY(-10px);
    }

    .document_outer a {
        display: block !important;
        height: inherit !important;
        margin: 0 5px !important;
    }

    .document_click {
        float: right;
        cursor: pointer;
        width: 22px;
    }
    #expList_vid .L1 {
        margin-left: 0px;
        list-style: none;
    }
    #expList_vid .L2 {
        margin-left: 35px;
        list-style: none;
    }
    #expList_vid .L3 {
        margin-left: 70px;
        list-style: none;
    }
    #expList_vid .L4 {
        margin-left: 105px;
        list-style: none;
    }
    #expList .L1 {
        margin-left: 0px;
        list-style: none;
    }
    #expList .L2 {
        margin-left: 35px;
        list-style: none;
    }
    #expList .L3 {
        margin-left: 70px;
        list-style: none;
    }
    #expList .L4 {
        margin-left: 105px;
        list-style: none;
    }
    .expList li.standard-cls{
        margin-top: 10px !important
    }
    .video-outer {
        float: left;
        clear: left;
        padding-bottom: 0px;
        width: 100% !important;
    }

</style>
<style>
    #comment_comment:focus {
        z-index: 20;
        /*        box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82,168,236,0.6) !important;
                border-color: rgba(82,168,236,0.8) !important;*/
        border-color: #fff !important;
        box-shadow: none !important;
    }

    .v_control button.fast {
        background: #3498DB;
    }

    .v_control button {
        cursor: pointer;
        border: 0;
        padding: 7px;
        border-radius: 5px;
        color: #fff;
    }

    .v_control button.wrin {
        background: #3a79a4;
    }

    .v_control {
        margin-top: 10px;
    }

    .v_control button img {
        width: 35px;
    }

    .timer_box2 {
        background: #ed1c24;
        font-size: 9px;
        color: #fff;
        padding: 2px 5px;
        position: absolute;
        right: 0px;
        top: -18px;
    }

    .video_recording_container {
        background: #000 url('/app/webroot/img/camera.png') no-repeat center center;
        padding: 20px;
        position: relative;
        height: 235px;
        /*//margin-top:10px;*/

    }

    .video_recording_container .timer_box {
        color: #fff;
        font-size: 13px;
        position: absolute;
        right: 10px;
        top: 10px;
    }

    .live_record {
        position: absolute;
        left: 10px;
        top: 10px;
        color: #fff;
        font-size: 13px;
    }

    .rec_border {
        border-bottom: solid 1px #D3D2D6;
        height: 1px;
        margin: 10px 0px;
    }

    .petsCls {
        float: right;
        margin-right: 10px;
        /*margin-top: -27px;*/
        right: 0;
        position: relative;
    }

    .petsCls label {
        font-weight: normal !important;
        font-size: 13px;
        float: left;
    }

    input#press_enter_to_send {
        margin-top: 3px;
        float: left;
        margin-right: 4px;
    }

    .input-group {
        clear: both;
    }

    .document_outer {
        background: #fff;
        padding: 10px;
        position: absolute;
        z-index: 99;
        border: 1px solid #ddd;
        float: right;
        right: 9px;
        top: 84px;
        display: none;
    }

    .document_outer:before {
        pointer-events: none;
        position: absolute;
        z-index: -1;
        content: '';
        border-style: solid;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-property: transform;
        transition-property: transform;
        left: calc(50% - 10px);
        top: 0;
        border-width: 0 10px 10px 10px;
        border-color: transparent transparent #e1e1e1 transparent;
        right: 1px;
        left: inherit;
        -webkit-transform: translateY(-10px);
        transform: translateY(-10px);
    }

    .document_outer a {
        display: block !important;
        height: inherit !important;
        margin: 0 5px !important;
    }

    .document_click {
        float: right;
        cursor: pointer;
        width: 22px;
    }
    .left-box {
        width: 49%!important;
        float: left;
    }
    .left-box .video-js {
        width: 100% !important;
        height: 246px !important;
        margin: 0 auto;
        float: none;
    }
    .left-box #video_span {
        width: 100% !important;
    }

    .right-box {

        overflow: hidden;
        width: 49% !important;
        float: right;
        margin-top: 0px;
        height: 390px;
    }
    #main {        padding: 0 10px !important; margin: 0 !important; border: 0 !important;     overflow: hidden !important;}
    #tab-area {
        margin: 0px 0 0 0!important;
        padding: 10px!important;
        #tab-area .tab-content{
            padding-top: 0px !important;
        }
    }
    body {    font-family: "LatoWeb","Helvetica Neue",Helvetica,Arial,sans-serif !important;}
</style>

<script type="text/javascript">
    var observation_notes_array = [];

    $(document).ready(function () {
        $('.document_click').click(function (e) {
            $('.document_outer').slideToggle();
        });

    });


</script>
<?php
$amazon_base_url = Configure::read('amazon_base_url');
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$user_role = $this->Custom->get_user_role_name($users['users_accounts']['role_id']);


$account_id = $users['accounts']['account_id'];
$account_folder_id = $huddle[0]['AccountFolder']['account_folder_id'];
$huddle_account_id = $huddle[0]['AccountFolder']['account_id'];
$isFirefoxBrowser = $this->Browser->isFirefox();
$srtType = $this->Session->read('srtType');

function defaulttagsclasses1($posid) {
    $class_name = 'tags_qucls';
    if ($posid == 1) {
        $class_name = 'tags_qucls';
    } else if ($posid == 2) {
        $class_name = 'tags_sugcls';
    } else if ($posid == 3) {
        $class_name = 'tags_notescls';
    } else if ($posid == 4) {
        $class_name = 'tags_strangthcls';
    } else if ($posid == 5) {
        $class_name = 'tags_strangthcls';
    }
    return $class_name;
}

function gettagclass($comment_tags, $default_tags) {
    $return = -1;
    foreach ($comment_tags as $comment_tag) {
        foreach ($default_tags as $key => $default_tag_value) {
            if (!empty($comment_tag['account_tags']['account_tag_id'])) {
                if ($default_tag_value['AccountTag']['account_tag_id'] == $comment_tag['AccountCommentTag']['account_tag_id']) {
                    $return = $key;
                    break;
                }
            }
        }
    }
    $return = $return + 1;
    return $return;
}
?>
<div class="tab-content <?php echo ($tab == '' || $tab == '1') ? 'tab-active' : 'tab-hidden'; ?>" id="tabbox1" style="padding-top:0px;">
    <p id="notification" style="display: none;clear:both;width:51%;"></p>
    <?php if ($videos != '' && $videoDetail == ''): ?>
        <script type="text/javascript">
            $(document).ready(function (e) {
                //var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
                var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
                if (iOS) {
                    var now = new Date().valueOf();
                    setTimeout(function () {
                        if (new Date().valueOf() - now > 100)
                            return;
                        //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                        return;
                    }, 50);
                    window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $videoDetail['Document']['id']; ?>&isWorkspace=false&account_id=<?php echo $huddle_account_id ?>";
                }


                var ua = navigator.userAgent.toLowerCase();
                var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
                if (isAndroid) {
                    var now = new Date().valueOf();
                    setTimeout(function () {
                        if (new Date().valueOf() - now > 100)
                            return;
                        //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                        return;
                    }, 50);
                    window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $videoDetail['Document']['id']; ?>&isWorkspace=false&account_id=<?php echo $huddle_account_id ?>";
                }


            });
        </script>
        <style>
            .videos-list__item-thumb img {
                height: 148px;
            }

            .wrap {
                text-overflow: inherit;
                overflow: hidden;
                white-space: normal;
                display: block;
                height: 35px;
            }

            .wrap2 {
                white-space: nowrap;
                text-overflow: ellipsis;
                overflow: hidden;
            }
        </style>
        <div style="padding-bottom:30px; padding-top: 39px;">
            <strong id="huddle_video_title_strong">Videos (<?php echo count($videos); ?>)</strong>
            <input type="hidden" name="videos_count" value="<?php echo count($videos); ?>" id="videos_count"/>
            <div class="search-box" style=" width: 480px !important;position:relative;">
                <input type="button" id="btnSearchVideos" class="btn-search" value="">
                <input class="text-input" id="txtSearchVideos" type="text" value="" placeholder="Search Videos..."
                       style="margin-right: 12px;">
                <span id="clearVideoButton" style="top: 4px;right: 207px !important;display: none;"
                      class="clear-video-input-box">X</span>
                <div class="select">
                    <select id="cmbVideoSort" name="upload-date">
                        <option value="afd.title ASC">Video Title</option>
                        <option value="Document.created_date DESC" selected>Date Uploaded</option>
                        <option value="User.first_name ASC">Uploaded By</option>
                        <?php if ($this->Custom->check_if_eval_huddle($huddle_id) == 1 && $this->Custom->check_if_evalutor($huddle_id, $user_id) == 1): ?>
                            <option value="published_feedback">Published feedback</option>
                            <option value="unpublished_feedback">Unpublished feedback</option>
                        <?php endif; ?>

                    </select>
                </div>
            </div>
        </div>
        <input id="txtHuddleID" type="hidden" value="<?php echo $huddle_id ?>"/>
        <div id="temp-list" style="display: none;">
            <li class="videos-list__item">
                <div class="videos-list__item-thumb">
                    <div class="video-unpublished ">
                        <span class="huddles-unpublished" style="padding: 15% 17px !important;">
                            <img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                                 style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;"/><br><?=$alert_messages["Your_video_is_currently_processing"]; ?></span>
                    </div>
                </div>
                <div class="videos-list__item-aside">
                    <div id="temp-video-title" class="videos-list__item-title"></div>
                    <div class="videos-list__item-author">By
                        <a href="#"
                           title="<?php echo $user_current_account['User']['first_name'] . " " . $user_current_account['User']['last_name'] ?>">
                               <?php echo $user_current_account['User']['first_name'] . " " . $user_current_account['User']['last_name'] ?>
                        </a>
                    </div>
                    <div class="videos-list__item-added">Uploaded
                        <?php echo date('M d, Y', time()) ?>
                    </div>
                </div>
            </li>
        </div>

        <div style="clear: both;" class='clearfix'></div>
        <div id="videos-list">
            <p id="notification" style="display: none;padding-left: 29px;padding-right: 9px;padding-bottom: 0px;"></p>
            <ul class="videos-list" id="videos-list">
                <?php if (is_array($videos) && count($videos) > 0): ?>
                    <?php
                    $total_videos = 0;
                    foreach ($videos as $row):

                        $videoID = $row['Document']['id'];
                        $transcoding_status = $this->Custom->transcoding_status($row['Document']['id']);

                        $document_files_array = $this->Custom->get_document_url($row['Document']);

                        if (empty($document_files_array['url'])) {
                            $row['Document']['published'] = 0;
                            $document_files_array['url'] = $row['Document']['original_file_name'];
                            $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            @$row['Document']['duration'] = $document_files_array['duration'];
                        }

                        $videoFilePath = pathinfo($document_files_array['url']);
                        $videoFileName = $videoFilePath['filename'];
                        ?>
                        <?php
                        if ($huddle_type == 3):
                            $created_by = $row['Document']['created_by'];
                            $is_avaluator = $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']);
                            $evaluators_ids = $this->Custom->get_evaluator_ids($huddle_id);
                            $participants_ids = $this->Custom->get_participants_ids($huddle_id);
                            if ($is_avaluator && $users['roles']['role_id'] == 120) {
                                $isEditable = ($user_current_account['User']['id'] == $row['Document']['created_by'] || (is_array($participants_ids) && in_array($row['Document']['created_by'], $participants_ids)));
                            } elseif ($is_avaluator && ($users['roles']['role_id'] == 110 || $users['roles']['role_id'] == 100)) {
                                $isEditable = ($is_avaluator || ($user_current_account['User']['id'] == $row['Document']['created_by'] || (is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))));
                            } else {
                                $isEditable = (($user_current_account['User']['id'] == $row['Document']['created_by'] || (is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))));
                            }
                            ?>
                            <?php
                            if ($isEditable):
                                $total_videos++;
                                ?>
                                <li class="videos-list__item">
                                    <?php if ($users['roles']['role_id'] != 125): ?>
                                        <div class="ac-btn">
                                            <?php if ($user_current_account['User']['id'] == $row['Document']['created_by'] || $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id'])): ?>
                                                <?php if ($this->Custom->check_if_submission_date_passed($huddle_id, $user_current_account['User']['id'])): ?>

                                                <?php else: ?>
                                                    <?php if ($this->Custom->dis_mem_del_video($account_id, $huddle_permission, $huddle_type)): ?>

                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <?php if (!$isFirefoxBrowser && $row['Document']['published'] == 1 && ($user_current_account['User']['id'] == $row['Document']['created_by'] || $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']))): ?>
                                                <!--                                            <a class="btn-trim-video btn icon2-crop right smargin-right fl-btn" title="edit video" style="border:none;background: none;border-radius: 0px;padding: 0px;" rel="tooltip"></a>
                                                                                                                                <form method="post" action="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>" style="display:none;">
                                                                                                                                    <input type="hidden" name="open_trim" value="1" />
                                                                                                                                </form>-->
                                            <?php endif; ?>
                                            <?php if ($row['Document']['published'] == 1 && ($user_current_account['User']['id'] == $row['Document']['created_by'] || $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']))): ?>

                                            <?php endif; ?>
                                            <?php if ($row['Document']['published'] == 1): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--                                            <span id="thumb_regen_<?php echo $row['Document']['id'] ?>" document_id="<?php echo $row['Document']['id'] ?>" huddle_id="<?php echo $row['afd']['account_folder_id'] ?>" class="regen-thumbnail-image regen-thumbnail-image-workspace
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              regen-thumbnail-image-inner regen-thumbnail-image-huddle-inner" title="Regenerate Thumnail" rel="tooltip" style="display:none;">&nbsp;</span>-->
                                            <?php endif; ?>

                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $(document).on("click", '#copy-huddle-<?php echo $row['Document ']['id '] ?>', function () {
                                                        $('.copy-document-ids').val($(this).attr('data-document-id'));
                                                        if ($(this).attr('data-total-comments') == 0)
                                                            $('.copy_video_box').css('display', 'none');
                                                        else
                                                            $('.copy_video_box').css('display', 'block');
                                                    });
                                                });
                                            </script>
                                        </div>
                                    <?php endif; ?>
                                    <div class="clearfix"></div>
                                    <div class="videos-list__item-thumb">
                                        <?php
                                        if ($row['Document']['published'] == '1' && $transcoding_status != 5):
                                            $seconds = $row['Document']['duration'] % 60;
                                            $minutes = ($row['Document']['duration'] / 60) % 60;
                                            $hours = gmdate("H", $row['Document']['duration']);
                                            ?>
                                            <a href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>"
                                               title="<?php echo $row['afd']['title'] ?>">
                                                   <?php
                                                   $thumbnail_image_path = $document_files_array['thumbnail'];

                                                   echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                                   ?>
                                                <div class="play-icon"></div>
                                                <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;">
                                                    <?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?>
                                                </div>
                                            </a>
                                        <?php else: ?>

                                        <?php endif; ?>
                                    </div>
                                    <div class="videos-list__item-aside">
                                        <!--                    <div class="videos-list__item-title">
                                                                                                                        <a class="wrap" id="vide-title-<?php //echo $row['Document']['id'];
                                        ?>" href="<?php //echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id'];
                                        ?>" title="<?php echo $row['afd']['title'] ?>" ><?php //echo (strlen($row['afd']['title']) > 52 ? substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title'])
                                        ?></a>
                                                                                                                    </div>-->

                                        <div id="input-title-<?php echo $row['afd']['id']; ?>" style="display: none;height:35px;">
                                            <input id="input-field-<?php echo $row['afd']['id']; ?>" type="text" name="title"
                                                   value="" required="required"
                                                   style="margin-right: 5px;width:150px; padding:6px 4px;"/>
                                            <a onclick="return update_title_grid(<?php echo $row['afd']['id']; ?>)"
                                               style="cursor: pointer"><?php
                                                   $chimg = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/icons/save-btn.png');
                                                   echo $this->Html->image($chimg);
                                                   ?></a>
                                            <a onclick="return cancel(<?php echo $row['afd']['id']; ?>)"
                                               style="cursor: pointer"><?php
                                                   $chimg = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/icons/cancel-btn.png');
                                                   echo $this->Html->image($chimg);
                                                   ?></a>
                                        </div>
                                        <?php if ($user_current_account['User']['id'] == $row['Document']['created_by'] || $this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id'])): ?>

                                            <div id="videos-title-<?php echo $row['afd']['id']; ?>"
                                                 class="videos-title cursor-pointer"
                                                 data-title="<?php echo $row['afd']['title']; ?>" style="width:200px;">
                                                <div class="videos-list__item-title">
                                                    <a class="wrap wrap2" id="vide-title-<?php echo $row['Document']['id']; ?>"
                                                       title="<?php echo $row['afd']['title'] ?>"><?php echo(strlen($row['afd']['title']) > 35 ? mb_substr($row['afd']['title'], 0, 35) . "..." : $row['afd']['title']) ?></a>
                                                </div>
                                                <div style="clear: both;"></div>
                                            </div>
                                        <?php else: ?>
                                            <div class="videos-list__item-title">
                                                <a class="wrap" id="vide-title-<?php echo $row['Document']['id']; ?>"
                                                   href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>"
                                                   title="<?php echo $row['afd']['title'] ?>"><?php echo(strlen($row['afd']['title']) > 52 ? mb_substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title']) ?></a>
                                            </div>

                                        <?php endif; ?>


                                        <div class="videos-list__item-author">By
                                            <a href="<?php echo empty($row[0]['AutoCreated']) ? '#' : 'mailto:' . $row[0]['WebUploaderEmail']; ?>"
                                               title="<?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?>">
                                                   <?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?>
                                            </a>
                                        </div>
                                        <div class="videos-list__item-added">Uploaded
                                            <?php echo date('M d, Y', strtotime($row['Document']['created_date'])) ?>

                                            <div class="clear"></div>
                                            <div style="float:left;    position: relative;top: 6px; padding-bottom: 4px;color: #1873bd;">

                                                <span>
                                                    <img src="<?php echo $this->webroot . 'img/comment_numbers.png'; ?>"> <?php echo $this->Custom->get_video_comment_numbers($row['Document']['id'], $account_folder_id, $user_current_account['User']['id']); ?>
                                                </span>

                                                <span>
                                                    <img src="<?php echo $this->webroot . 'img/attachment_numbers.png'; ?>"> <?php echo $this->Custom->get_video_attachment_numbers($row['Document']['id']); ?>
                                                </span>

                                            </div>

                                            <div style="float: right;    margin-right: -10px;margin-top: -7px;    position: relative; top: 4px;">
                                                <?php if (($row['Document']['published'] == '1') && ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $row['Document']['created_by'])))): ?>
                                                    <?php
                                                    $video_path = '';
                                                    if (Configure::read('use_cloudfront') == true) {
                                                        $video_path = $this->Custom->getSecureAmazonCloudFrontUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4", $row['Document']['original_file_name']);
                                                    } else {
                                                        $video_path = $this->Custom->getSecureAmazonUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4", $row['Document']['original_file_name']);
                                                    }
                                                    ?>

                                                    <a style="float: left;"
                                                       href="<?php echo $this->webroot . 'Huddles/download/' . $row['Document']['id'] ?>">
                                                        <img alt="Download" class="right smargin-right"
                                                             style="height: 28px;margin-top: -2px;" rel="tooltip"
                                                             src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>"
                                                             title="download video"/>
                                                    </a>
                                                <?php elseif ($huddle_type == 2): ?>
                                                    <?php
                                                    $video_path = '';
                                                    if (Configure::read('use_cloudfront') == true) {
                                                        $video_path = $this->Custom->getSecureAmazonCloudFrontUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4", $row['Document']['original_file_name']);
                                                    } else {
                                                        $video_path = $this->Custom->getSecureAmazonUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4", $row['Document']['original_file_name']);
                                                    }
                                                    ?>

                                                    <?php if (($coachee_permissions && $huddle_permission == 210) || $huddle_permission == 200): ?>
                                                        <a style="float: left;"
                                                           href="<?php echo $this->webroot . 'Huddles/download/' . $row['Document']['id'] ?>">
                                                            <img alt="Download" class="right smargin-right"
                                                                 style="height: 28px;margin-top: -2px;" rel="tooltip"
                                                                 src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>"
                                                                 title="download video"/>
                                                        </a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </li>

                                <script>
                                    $(document).ready(function (e) {
                                        var $input = "#input-field-<?php echo $row['afd']['id']; ?>";
                                        var $id = "#videos-title-<?php echo $row['afd']['id']; ?>";
                                        var $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
                                        $($id).click(function (e) {
                                            var $title = $(this).attr('data-title');
                                            $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
                                            $input = "#input-field-<?php echo $row['afd']['id']; ?>";
                                            $(this).css("display", 'none');
                                            $($title_block).css("display", 'block');
                                            $($input).val($title);
                                        });
                                        $($id).mouseover(function () {
                                            $(this).css("backgroundColor", "#FAFABE")
                                        });
                                        $($id).mouseout(function () {
                                            $(this).css("backgroundColor", "#ffffff")
                                        });
                                    });
                                </script>
                            <?php endif; ?>
                        <?php else: ?>
                            <li class="videos-list__item">
                                <?php
                                $isEditable = ($huddle_permission == 200) ||
                                        ($huddle_permission == 210 && $user_current_account['User']['id'] == $row['Document']['created_by']) ||
                                        ($user_current_account['User']['id'] == $row['Document']['created_by'] && $user_permissions['UserAccount']['permission_maintain_folders'] == '1');
                                ?>

                                <?php if ($isEditable): ?>
                                    <?php if ($users['roles']['role_id'] != 125): ?>
                                        <div class="ac-btn">
                                            <?php if ($this->Custom->dis_mem_del_video($account_id, $huddle_permission, $huddle_type)): ?>
                                                <a href="<?php echo $this->base . '/Huddles/deleteHuddleVideo/' . $huddle_id . '/1/' . $row['Document']['id'] ?>"
                                                   data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                                                   data-original-title="Delete" rel="tooltip" data-method="delete"
                                                   class="btn icon2-trash right smargin-right fl-btn"
                                                   style="border:none;background: none;border-radius: 0px;padding: 0px;"></a>
                                               <?php endif; ?>
                                               <?php if (!$isFirefoxBrowser && $row['Document']['published'] == 1): ?>
                                                <a class="btn-trim-video btn icon2-crop right smargin-right fl-btn"
                                                   title="edit video"
                                                   style="border:none;background: none;border-radius: 0px;padding: 0px;"
                                                   rel="tooltip"></a>
                                                <form method="post"
                                                      action="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>/1"
                                                      style="display:none;">
                                                    <input type="hidden" name="open_trim" value="1"/>
                                                </form>
                                            <?php endif; ?>
                                            <?php if ($row['Document']['published'] == 1): ?>
                                                <a class="copy" id="copy-huddle-<?php echo $row['Document']['id'] ?>"
                                                   data-document-id="<?php echo $row['Document']['id'] ?>"
                                                   data-total-comments="<?php echo $row['Document']['total_comments'] ?>"
                                                   data-original-title="Copy" rel="tooltip" data-toggle="modal"
                                                   data-target="#moveFiles">Copy</a>
                                               <?php endif; ?>
                                               <?php if ($row['Document']['published'] == 1): ?>
                                                <span id="thumb_regen_<?php echo $row['Document']['id'] ?>"
                                                      document_id="<?php echo $row['Document']['id'] ?>"
                                                      huddle_id="<?php echo $row['afd']['account_folder_id'] ?>" class="regen-thumbnail-image regen-thumbnail-image-workspace
                                                      regen-thumbnail-image-inner regen-thumbnail-image-huddle-inner"
                                                      title="Regenerate Thumnail" rel="tooltip"
                                                      style="display:none;">&nbsp;</span>
                                                  <?php endif; ?>

                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $(document).on("click", '#copy-huddle-<?php echo $row['Document']['id'] ?>', function () {
                                                        $('.copy-document-ids').val($(this).attr('data-document-id'));
                                                        if ($(this).attr('data-total-comments') == 0)
                                                            $('.copy_video_box').css('display', 'none');
                                                        else
                                                            $('.copy_video_box').css('display', 'block');
                                                    });
                                                });
                                            </script>
                                        </div>
                                    <?php endif; ?>
                                <?php elseif ($huddle_type == 2): ?>
                                    <?php if ($users['roles']['role_id'] != 125): ?>
                                        <div class="ac-btn">
                                            <?php if (($coachee_permissions && $huddle_permission == 210) || $huddle_permission == 200): ?>
                                                <?php if ($this->Custom->dis_mem_del_video($account_id, $huddle_permission, $huddle_type)): ?>
                                                    <a href="<?php echo $this->base . '/Huddles/deleteHuddleVideo/' . $huddle_id . '/1/' . $row['Document']['id'] ?>"
                                                       data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                                                       data-original-title="Delete" rel="tooltip" data-method="delete"
                                                       class="btn icon2-trash right smargin-right fl-btn"
                                                       style="border:none;background: none;border-radius: 0px;padding: 0px;"></a>
                                                   <?php endif; ?>
                                               <?php endif; ?>

                                            <?php if (!$isFirefoxBrowser && $row['Document']['published'] == 1): ?>
                                                <?php if (($coachee_permissions && $huddle_permission == 210) || $huddle_permission == 200): ?>
                                                    <a class="btn-trim-video btn icon2-crop right smargin-right fl-btn"
                                                       title="edit video"
                                                       style="border:none;background: none;border-radius: 0px;padding: 0px;"
                                                       rel="tooltip"></a>
                                                   <?php endif; ?>
                                                <form method="post"
                                                      action="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>/1"
                                                      style="display:none;">
                                                    <input type="hidden" name="open_trim" value="1"/>
                                                </form>
                                            <?php endif; ?>
                                            <?php if ($row['Document']['published'] == 1): ?>
                                                <?php if (($coachee_permissions && $huddle_permission == 210) || $huddle_permission == 200): ?>
                                                    <a class="copy" id="copy-huddle-<?php echo $row['Document']['id'] ?>"
                                                       data-document-id="<?php echo $row['Document']['id'] ?>"
                                                       data-total-comments="<?php echo $row['Document']['total_comments'] ?>"
                                                       data-original-title="Copy" rel="tooltip" data-toggle="modal"
                                                       data-target="#moveFiles">Copy</a>
                                                   <?php endif; ?>
                                               <?php endif; ?>
                                               <?php if ($row['Document']['published'] == 1): ?>
                                                <span id="thumb_regen_<?php echo $row['Document']['id'] ?>"
                                                      document_id="<?php echo $row['Document']['id'] ?>"
                                                      huddle_id="<?php echo $row['afd']['account_folder_id'] ?>" class="regen-thumbnail-image regen-thumbnail-image-workspace
                                                      regen-thumbnail-image-inner regen-thumbnail-image-huddle-inner"
                                                      title="Regenerate Thumnail" rel="tooltip"
                                                      style="display:none;">&nbsp;</span>
                                                  <?php endif; ?>

                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $(document).on("click", '#copy-huddle-<?php echo $row['Document']['id'] ?>', function () {
                                                        $('.copy-document-ids').val($(this).attr('data-document-id'));
                                                        if ($(this).attr('data-total-comments') == 0)
                                                            $('.copy_video_box').css('display', 'none');
                                                        else
                                                            $('.copy_video_box').css('display', 'block');
                                                    });
                                                });
                                            </script>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <div class="clearfix"></div>

                                <div class="videos-list__item-thumb">
                                    <?php
                                    if ($row['Document']['published'] == '1' && $transcoding_status != 5):
                                        $seconds = $row['Document']['duration'] % 60;
                                        $minutes = ($row['Document']['duration'] / 60) % 60;
                                        $hours = gmdate("H", $row['Document']['duration']);
                                        ?>
                                        <a href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>"
                                           title="<?php echo $row['afd']['title'] ?>">
                                               <?php
                                               $thumbnail_image_path = $document_files_array['thumbnail'];

                                               echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                               ?>
                                            <div class="play-icon"></div>
                                            <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;">
                                                <?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?>
                                            </div>
                                        </a>
                                    <?php else: ?>
                                        <a id="processing-message-<?php echo $row['Document']['id']; ?>"
                                           href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $row['Document']['id']; ?>"
                                           title="<?php echo $row['afd']['title'] ?>">
                                            <div class="video-unpublished ">
                                                <span class="huddles-unpublished" style="padding: 15% 17px !important;">
                                                    <?php if ($row['Document']['encoder_status'] == 'Error' || $transcoding_status == 5): ?>
                                                        Video failed to process successfully. Please try again or contact
                                                        <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"
                                                           style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>
                                                        <i style="position: absolute;top: 170px;right: 185px;">.</i>
                                                    <?php elseif ($row['Document']['published'] == '0' && $row['Document']['video_is_saved'] == '0' && $row['Document']['is_processed'] == '4'): ?>
                                                        <br><br>This Live Video Recording was not Saved.
                                                    <?php else : ?>
                                                        <img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                                                             style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;"/>
                                                        <br><?=$alert_messages["Your_video_is_currently_processing"]; ?>
                                                    <?php endif; ?>
                                                </span>
                                            </div>
                                        </a>
                                    <?php endif; ?>
                                </div>
                                <div class="videos-list__item-aside">
                                    <!--                    <div class="videos-list__item-title">
                                                                                                    <a class="wrap" id="vide-title-<?php //echo $row['Document']['id'];                                                                                                                                                                                                                      ?>" href="<?php //echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id'];                                                                                                                                                                                                                      ?>" title="<?php echo $row['afd']['title'] ?>" ><?php //echo (strlen($row['afd']['title']) > 52 ? substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title'])                                                                                                                                                                                                                      ?></a>
                                                                                                </div>-->

                                    <div id="input-title-<?php echo $row['afd']['id']; ?>" style="display: none;height:35px;">
                                        <input id="input-field-<?php echo $row['afd']['id']; ?>" type="text" name="title"
                                               value="" required="required"
                                               style="margin-right: 5px;width:150px; padding:6px 4px;"/>
                                        <a onclick="return update_title_grid(<?php echo $row['afd']['id']; ?>)"
                                           style="cursor: pointer"><?php
                                               $chimg = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/icons/save-btn.png');
                                               echo $this->Html->image($chimg);
                                               ?></a>
                                        <a onclick="return cancel(<?php echo $row['afd']['id']; ?>)"
                                           style="cursor: pointer"><?php
                                               $chimg = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/icons/cancel-btn.png');
                                               echo $this->Html->image($chimg);
                                               ?></a>
                                    </div>
                                    <div id="videos-title-<?php echo $row['afd']['id']; ?>" class="videos-title cursor-pointer"
                                         data-title="<?php echo $row['afd']['title']; ?>" style="width:200px;">
                                        <div class="videos-list__item-title">
                                            <a class="wrap wrap2" id="vide-title-<?php echo $row['Document']['id']; ?>"
                                               title="<?php echo $row['afd']['title'] ?>"><?php echo(strlen($row['afd']['title']) > 35 ? mb_substr($row['afd']['title'], 0, 35) . "..." : $row['afd']['title']) ?></a>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>


                                    <div class="videos-list__item-author">By
                                        <a href="<?php echo empty($row[0]['AutoCreated']) ? '#' : 'mailto:' . $row[0]['WebUploaderEmail']; ?>"
                                           title="<?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?>">
                                               <?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?>
                                        </a>
                                    </div>
                                    <div class="videos-list__item-added">Uploaded
                                        <?php echo date('M d, Y', strtotime($row['Document']['created_date'])) ?>
                                        <div class="clear"></div>
                                        <div style="float:left;    position: relative;top: 6px; padding-bottom: 4px;color: #1873bd;">

                                            <span>
                                                <img src="<?php echo $this->webroot . 'img/comment_numbers.png'; ?>"> <?php echo $this->Custom->get_video_comment_numbers($row['Document']['id'], $account_folder_id, $user_current_account['User']['id']); ?>
                                            </span>

                                            <span>
                                                <img src="<?php echo $this->webroot . 'img/attachment_numbers.png'; ?>"> <?php echo $this->Custom->get_video_attachment_numbers($row['Document']['id']); ?>
                                            </span>

                                        </div>
                                        <div style="float: right;    margin-right: -10px;margin-top: -7px;    position: relative; top: 4px;">
                                            <?php if (($row['Document']['published'] == '1') && ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $row['Document']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1'))): ?>
                                                <?php
                                                $video_path = '';
                                                if (Configure::read('use_cloudfront') == true) {
                                                    $video_path = $this->Custom->getSecureAmazonCloudFrontUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4", $row['Document']['original_file_name']);
                                                } else {
                                                    $video_path = $this->Custom->getSecureAmazonUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4", $row['Document']['original_file_name']);
                                                }
                                                ?>

                                                <a style="float: left;"
                                                   href="<?php echo $this->webroot . 'Huddles/download/' . $row['Document']['id'] ?>">
                                                    <img alt="Download" class="right smargin-right"
                                                         style="height: 28px;margin-top: -2px;" rel="tooltip"
                                                         src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>"
                                                         title="download video"/>
                                                </a>
                                            <?php elseif ($huddle_type == 2): ?>
                                                <?php
                                                $video_path = '';
                                                if (Configure::read('use_cloudfront') == true) {
                                                    $video_path = $this->Custom->getSecureAmazonCloudFrontUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4", $row['Document']['original_file_name']);
                                                } else {
                                                    $video_path = $this->Custom->getSecureAmazonUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4", $row['Document']['original_file_name']);
                                                }
                                                ?>

                                                <?php if (($coachee_permissions && $huddle_permission == 210) || $huddle_permission == 200): ?>
                                                    <a style="float: left;"
                                                       href="<?php echo $this->webroot . 'Huddles/download/' . $row['Document']['id'] ?>">
                                                        <img alt="Download" class="right smargin-right"
                                                             style="height: 28px;margin-top: -2px;" rel="tooltip"
                                                             src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>"
                                                             title="download video"/>
                                                    </a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                </div>

                            </li>
                            <script>
                                $(document).ready(function (e) {
                                    var $input = "#input-field-<?php echo $row['afd']['id']; ?>";
                                    var $id = "#videos-title-<?php echo $row['afd']['id']; ?>";
                                    var $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
                                    $($id).click(function (e) {
                                        var $title = $(this).attr('data-title');
                                        $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
                                        $input = "#input-field-<?php echo $row['afd']['id']; ?>";
                                        $(this).css("display", 'none');
                                        $($title_block).css("display", 'block');
                                        $($input).val($title);
                                    });
                                    $($id).mouseover(function () {
                                        $(this).css("backgroundColor", "#FAFABE")
                                    });
                                    $($id).mouseout(function () {
                                        $(this).css("backgroundColor", "#ffffff")
                                    });
                                });
                            </script>
                        <?php endif; ?>

                    <?php endforeach; ?>

                    <script type="text/javascript">
                        $(document).ready(function () {

        <?php if ($huddle_type == 3): ?>
                                $('#huddle_video_title_strong').html('Videos (<?php echo $total_videos; ?>)');
        <?php else: ?>
                                $('#huddle_video_title_strong').html('Videos (<?php echo $totalVideos; ?>)');

        <?php endif; ?>

                            $('.regen-thumbnail-image').click(function () {

                                var r = confirm('Do you want to Regenerate Thumbnail?');

                                if (r == true) {

                                    var document_id = $(this).attr('document_id');
                                    var huddle_id = $(this).attr('huddle_id');

                                    $.ajax({
                                        type: 'POST',
                                        url: home_url + '/Huddles/regenerate_thumbnail/' + document_id + '/' + huddle_id,
                                        success: function (res) {

                                            alert('Thumbnail generated successfully.');

                                            d = new Date();
                                            var src_image = $("#img_" + document_id).attr("src");

                                            $("#img_" + document_id).attr("src", src_image + "&dd=" + d.getTime());

                                        },
                                        errors: function (response) {
                                            alert('Unable to generate Thumbnail, please try again later.');
                                        }
                                    });

                                }

                            });
                        });
                    </script>

                <?php else: ?>
                    <li class="videos-list__item_noitem">
                        <div style="margin-left: 10px;">No video(s) have been uploaded to this Huddle.</div>
                    </li>
                <?php endif; ?>
            </ul>
            <img id="loading_gif" style="margin-left:473px;margin-bottom:-32px;display:none;"
                 src="<?php echo $this->webroot . 'img/loading.gif' ?>">
                 <?php
                 print $this->element('load_more', array(
                             'total_items' => $totalVideos,
                             'count_items' => count($videos),
                             'current_page' => $current_page,
                             'number_per_page' => $video_per_page
                 ));
                 ?>
        </div>

    <?php elseif ($videoDetail != ''): ?>
        <script type="text/javascript">
            $(document).ready(function (e) {
                //var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
                var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
                if (iOS) {
                    var now = new Date().valueOf();
                    setTimeout(function () {
                        if (new Date().valueOf() - now > 100)
                            return;
                        //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                        return;
                    }, 50);
                    window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $videoDetail['Document']['id']; ?>&isWorkspace=false&account_id=<?php echo $huddle_account_id ?>";
                }

                var ua = navigator.userAgent.toLowerCase();
                var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
                if (isAndroid) {
                    var now = new Date().valueOf();
                    setTimeout(function () {
                        if (new Date().valueOf() - now > 100)
                            return;
                        //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                        return;
                    }, 50);
                    window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $videoDetail['Document']['id']; ?>&isWorkspace=false&account_id=<?php echo $huddle_account_id ?>";
                }


            });
            function doDocumentSearchAjaxInit() {
                return true;
                //                var keywords = $('#txtSearchDocuments').val();
                //                if (keywords == 'Search Documents...') {
                //                    keywords = '';
                //                }
                //                $.ajax({
                //                    type: 'POST',
                //                    data: {
                //                        sort: $('#cmbDocumentSort').val(),
                //                        state: 'init'
                //                    },
                //                    url: home_url + '/Huddles/getDocumentSearch/' + $('#txtHuddleID').val() + '/' + keywords,
                //                    success: function (response) {
                //                        $('.ul-docs').html(response);
                //                        gApp.updateCheckboxEvent($('.ul-docs .ui-checkbox input'));
                //                        if (typeof callback == 'function') {
                //                            callback();
                //                        }
                //                    },
                //                    errors: function (response) {
                //                        alert(response.contents);
                //                    }
                //                });
            }
            function initVideoComments() {
                return true;
            }
            function hideProcessOverlay() {
                return true;
            }
            function isFirefox() {
                return false;
            }


        </script>

        <div class="left-box video-detail">

            <input id="txtHuddleID" type="hidden" value="<?php echo $huddle_id ?>"/>
            <div class="video-outer">
                <?php
                $no_video = 'style="margin-bottom:10px;"';
                if (isset($videoDetail['Document']) && $videoDetail['Document'] != ''):
                    $video_title = '';
                    if (isset($videoDetail['afd']['title']) && $videoDetail['afd']['title'] != '') {
                        $video_title = $videoDetail['afd']['title'];
                    } else {
                        $video_title = 'Untitled Video';
                    }
                    ?>
                    <div id="video_span" style="float:left;width:500px;">
                        <?php //echo (strlen($video_title) > 75) ? "<a title='" . $video_title . "'>" . substr($video_title, 0, 75) . "...</a>" : $video_title; ?></a>


                        <?php
                        $isEditable = ($huddle_permission == 200) ||
                                ($huddle_permission == 210 && $user_current_account['User']['id'] == $videoDetail['Document']['created_by']) ||
                                ($user_current_account['User']['id'] == $videoDetail['Document']['created_by'] && $user_permissions['UserAccount']['permission_maintain_folders'] == '1');
                        ?>
                        <?php if (!$isFirefoxBrowser && $videoDetail['Document']['published'] == 1 && $isEditable): ?>
                            <?php if ($huddle_type != 3): ?>
                                <?php if ($users['roles']['role_id'] != 125): ?>
                                    <!--                                    <a id="inline-crop-panel"
                                                                           href="<?php echo $this->base . '/Huddles/trim/' . $videoDetail['Document']['id'] . '/' . $huddle_id ?>"
                                                                           class="iframe fancybox.iframe right mmargin-top crop-image" title="edit video"
                                                                           rel="tooltip">&nbsp;</a>-->
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php elseif (!$isFirefoxBrowser && $videoDetail['Document']['published'] == 1 && $huddle_type == 2): ?>
                            <?php if (($coachee_permissions && $huddle_permission == 210) || $huddle_permission == 200): ?><?php if ($users['roles']['role_id'] != 125): ?>
                                    <!--                                    <a id="inline-crop-panel"
                                                                           href="<?php echo $this->base . '/Huddles/trim/' . $videoDetail['Document']['id'] . '/' . $huddle_id ?>"
                                                                           class="iframe fancybox.iframe right mmargin-top crop-image" title="edit video"
                                                                           rel="tooltip">&nbsp;</a>-->
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <p id="notification" style="display:none;"></p>

                        <?php if ($isEditable): ?>
                            <?php if ($huddle_type == 3 && $this->Custom->check_if_submission_date_passed($huddle_id, $user_current_account['User']['id'])): ?>
                                <?php if ($users['roles']['role_id'] != 125): ?>

                                <?php endif; ?>
                            <?php else: ?>
                                <?php if ($this->Custom->dis_mem_del_video($account_id, $huddle_permission, $huddle_type)): ?>
                                    <?php if ($users['roles']['role_id'] != 125): ?>

                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php elseif ($huddle_type == 2): ?>
                            <?php if (($coachee_permissions && $huddle_permission == 210) || $huddle_permission == 200): ?>
                                <?php if ($this->Custom->dis_mem_del_video($account_id, $huddle_permission, $huddle_type)): ?>
                                    <?php if ($users['roles']['role_id'] != 125): ?>

                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>

                        <div style="clear: both;"></div>
                        <!-- Temporary video player replacement -->
                        <?php
                        $timeS = array();
                        $timecls = array();
                        $videoID = $videoDetail['Document']['id'];
                        $document_files_array = $this->Custom->get_document_url($videoDetail['Document']);

                        if (empty($document_files_array['url'])) {
                            $videoDetail['Document']['published'] = 0;
                            $document_files_array['url'] = $videoDetail['Document']['original_file_name'];
                            $videoDetail['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $videoDetail['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        }

                        $videoFilePath = pathinfo($document_files_array['url']);
                        $videoFileName = $videoFilePath['filename'];
                        if ($videoCommentsArray) {
                            foreach ($videoCommentsArray as $cmt) {
                                if (!empty($cmt['Comment']['time'])) {
                                    $timeS[] = $cmt['Comment']['time'];
                                    if (!empty($cmt['default_tags'])) {
                                        $timecls[] = gettagclass($cmt['default_tags'], $tags);
                                    } else {
                                        $timecls[] = 0;
                                    }
                                }
                            }
                        }
                        ?>

                        <?php if ($huddle_permission == '220'): ?>
                            <style>
                                .vjs-tooltip {
                                    display: none !important;
                                }
                            </style>
                        <?php endif; ?>

                        <input type="hidden" id="txtCurrentVideoID1" value="<?php echo $videoID; ?>"/>
                        <input type="hidden" id="txtCurrentVideoUrl"
                               value="<?php echo $this->base . '/video_details/home/' . $huddle_id . "/$videoID " ?>"/>
                               <?php $transcoding_status = $this->Custom->transcoding_status($videoID); ?>
                               <?php if (isset($videoDetail['Document']['published']) && $videoDetail['Document']['published'] == 1 && $transcoding_status != 5): ?>
                                   <?php
                                   $thumbnail_image_path = $document_files_array['thumbnail'];
                                   $video_path = $document_files_array['url'];
                                   ?>
                            <video oncontextmenu="return false;"
                                   id="example_video_<?php echo $videoDetail['Document']['id'] ?>"
                                   class="video-js vjs-default-skin" controls preload="metadata" width="500"
                                   height="321" poster="<?php echo $thumbnail_image_path; ?>"
                                   data-markers="[<?php echo implode(',', array_reverse($timeS)); ?>]"
                                   data-cls="[<?php echo implode(',', array_reverse($timecls)); ?>]">
                                <source src="<?php echo $video_path; ?>" type='video/mp4'/>
                            </video>
                            <!--                <div class="v_control">
                                            <button onclick="skip(-10)" class="wrin"><img src="<?php //echo  $this->webroot.'img/fast_forward_icon2.png'                                                                                                                                                                                                                     ?>"> Rewind</button>
                                            <button onclick="skip(10)" class="fast">Fast Forward <img src="<?php //echo  $this->webroot.'img/fast_forward_icon.png'                                                                                                                                                                                                                     ?>"></button>
                                        </div>-->
                            <div style="padding-top: 15px;">
                                <div> <?php echo $huddle_info['AccountFolder']['name'] ?> </div>
                                <div>
                                    By <span style="color: blue;"><?php echo $created_by['User']['first_name'] . ' ' . $created_by['User']['last_name'] ?></span>
                                    &nbsp;&nbsp;Submission On <?php echo date('D d, Y', strtotime($videoDetail['Document']['created_date'])) ?>
                                    Uploaded  <?php echo date('D d, Y', strtotime($videoDetail['Document']['created_date'])) ?>
                                </div>
                                <br/>
                                <div>
                                    <a style="color: #008ee2" target="_blank" href="<?php echo $this->base . '/video_details/home/' . $huddle_id . '/' . $video_id ?>">
                                        <img src="<?php echo $this->base . '/sibme_lti_canvas/images/link.svg' ?>" alt="" />&nbsp;&nbsp;View in Sibme
                                    </a>
                                </div>

                                <p class="tip" style="display: none;">
                                    Tip: Click on the video player timeline bar above to add time-specific
                                    annotations.
                                    <?php if ($user['users_accounts']['permission_video_library_upload'] == 1): ?>
                                        <?php if (isset($videoDetail['doc']['published']) && $videoDetail['doc']['published'] == 1): ?>
                                            <a data-total-comments="<?php echo $total_comments; ?>" id="copy-video"
                                               class="js-file-copy  copy huddle-contents-single" data-reveal-id=""
                                               data-toggle="modal" rel="nofollow" data-target="#copyFiles_2" href="#">Copy</a>
                                           <?php endif; ?>
                                       <?php endif; ?>
                                    <span class="badge right" data-original-title="video played"
                                          rel="tooltip"><?php echo $videoDetail['doc']['view_count']; ?></span>
                                    <a href="<?php echo $this->webroot . 'Huddles/download/' . $videoDetail['doc']['id'] ?>">
                                        <img alt="Download" class="right smargin-right" height="14" rel="tooltip"
                                             src="<?php echo $chimgDownload; ?>" title="download video" width="14"/>
                                    </a>
                                </p>
                            </div>
                            <div style="clear: both;"></div>
                        <?php elseif (isset($videoDetail['Document']['published']) && $videoDetail['Document']['published'] == 0): ?>
                            <div style="width: 500px; height: 321px; background: #000; float: left; text-align: center; color: #fff; position:relative;">
                                <div class="empty_video_box"
                                     style="padding-top:125px !important; padding-left: 10px !important;padding-right: 10px !important; width:100%; ">
                                    <?php if ($videoDetail['Document']['encoder_status'] == 'Error'): ?> Video failed to process successfully. Please try again or contact
                                        <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"
                                           style="color: blue;text-decoration: underline;position: absolute;top: 170px;left: 170px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>
                                        <i style="position: absolute;top: 170px;right: 185px;">.</i>
                                    <?php elseif ($videoDetail['Document']['published'] == '0' && $videoDetail['Document']['video_is_saved'] == '0' && $videoDetail['Document']['is_processed'] == '4'): ?>
                                        <br>This Live Video Recording was not Saved.
                                    <?php else : ?> <img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                                             style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;"/>
                                        <br><?=$alert_messages["Your_video_is_currently_processing"]; ?>
                                    <?php endif; ?>
                                </div>
                            </div>

                        <?php elseif (isset($videoDetail['Document']['published']) && $videoDetail['Document']['published'] == 1): ?>
                            <div style="width: 500px; height: 321px; background: #000; float: left; text-align: center; color: #fff; position:relative;">
                                <div class="empty_video_box"
                                     style="padding-top:125px !important; padding-left: 10px !important;padding-right: 10px !important; width:100%; ">
                                    <?php if ($videoDetail['Document']['encoder_status'] == 'Error' || $transcoding_status == 5): ?> Video failed to process successfully. Please try again or contact
                                        <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"
                                           style="color: blue;text-decoration: underline;position: absolute;top: 170px;left: 170px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>
                                        <i style="position: absolute;top: 170px;right: 185px;">.</i>
                                    <?php else : ?> <img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                                             style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;"/>
                                        <br><?=$alert_messages["Your_video_is_currently_processing"]; ?>
                                    <?php endif; ?>
                                </div>
                            </div>


                        <?php elseif ($videoFilePath == ''): ?>
                            <div id="docs-container" style="width: 500px; height: 321px;">
                                <div class="heading">
                                    <h3>Video</h3>
                                </div>
                                <div style=" padding: 10px; font-weight: bold;">
                                    This video is not currently available please remove this video and try again.
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $videoDetail['Document']['created_by']))): ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#copy-huddle').click(function (e) {
                                        $('.copy-document-ids').val($(this).attr('data-document-id'));
                                        if ($(this).attr('data-total-comments') == 0)
                                            $('.copy_video_box').css('display', 'none');
                                        else
                                            $('.copy_video_box').css('display', 'block');
                                    });
                                    var fieldWidth = parseInt($("#vidTitle").css("width"));
                                    $(".submit").mousedown(function () {
                                        ajaxField.update("vidTitle", "<?php echo $this->base . '/Huddles/changeTitle' ?>");
                                    });
                                    //$("#ajaxInput").css("width", (fieldWidth - 150) + "px");
                                    //$(".editArea").css("width", (fieldWidth) + "px");
                                    $("#vidTitle").mouseover(function () {
                                        $(this).css("backgroundColor", "#FAFABE")
                                    });
                                    $("#vidTitle").mouseout(function () {
                                        $(this).css("backgroundColor", "#ffffff")
                                    });
                                    $("#vidTitle").click(function () {
                                        $(this).css("display", "none");
                                        $(".editArea").css("display", "inline-block");
                                        //$("#ajaxInput").val($(this).text().trim());
                                        $("#ajaxInput").val($("#title_change").val());
                                        $("#ajaxInput").focus();
                                    });
                                    $("#ajaxInput").blur(function () {
                                        $(".editArea").css("display", "none");
                                        $("#vidTitle").css("display", "inline-block");
                                    });
                                });
                                var ajaxField, startValue = $("#title_change").val();
                                ajaxField = function () { //variables
                                    var xmlHttp = null;
                                    if (typeof String.prototype.trim !== 'function') { //this will add trim to IE8
                                        String.prototype.trim = function () {
                                            return this.replace(/^\s+|\s+$/g, '');
                                        }
                                    }
                                    return {
                                        init: function () {
                                        },
                                        update: function (responseField, url) {
                                            xmlHttp = new XMLHttpRequest();
                                            xmlHttp.onreadystatechange = function () {
                                                if (xmlHttp.readyState != 4) {
                                                    return;
                                                }
                                                if (xmlHttp.status != 200 && xmlHttp.status != 304) {
                                                    alert('HTTP error ' + xmlHttp.status);
                                                    return;
                                                }
                                                if (xmlHttp.readyState == 4) {
                                                    ajaxField.handleResponse(responseField, xmlHttp.responseText);
                                                }
                                            };
                                            newText = document.getElementById('ajaxInput').value.trim();
                                            videoId = $('#video-id').val();
                                            huddleId = '<?php echo $huddle_id; ?>';
                                            url = url + "/" + huddleId + "/" + videoId;
                                            var formData = new FormData();
                                            formData.append("title", newText);
                                            xmlHttp.open("POST", url, true);
                                            xmlHttp.send(formData);
                                            $("#vidTitle").css("display", "inline-block");
                                            $(".editArea").css("display", "none");
                                            $("#title_change").val(newText);
                                        },
                                        handleResponse: function (responseField, response) {
                                            document.getElementById(responseField).innerHTML = response;
                                        }

                                    };
                                }();
                            </script>
                        <?php elseif ($huddle_type == 2): ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#copy-huddle').click(function (e) {
                                        $('.copy-document-ids').val($(this).attr('data-document-id'));
                                        if ($(this).attr('data-total-comments') == 0)
                                            $('.copy_video_box').css('display', 'none');
                                        else
                                            $('.copy_video_box').css('display', 'block');
                                    });
                                    var fieldWidth = parseInt($("#vidTitle").css("width"));
                                    $(".submit").mousedown(function () {
                                        ajaxField.update("vidTitle", "<?php echo $this->base . '/Huddles/changeTitle' ?>");
                                    });
                                    //$("#ajaxInput").css("width", (fieldWidth - 150) + "px");
                                    //$(".editArea").css("width", (fieldWidth) + "px");
                                    $("#vidTitle").mouseover(function () {
                                        $(this).css("backgroundColor", "#FAFABE")
                                    });
                                    $("#vidTitle").mouseout(function () {
                                        $(this).css("backgroundColor", "#ffffff")
                                    });
                                    $("#vidTitle").click(function () {
                                        $(this).css("display", "none");
                                        $(".editArea").css("display", "inline-block");
                                        $("#ajaxInput").val($(this).text().trim());
                                        $("#ajaxInput").focus();
                                    });
                                    $("#ajaxInput").blur(function () {
                                        $(".editArea").css("display", "none");
                                        $("#vidTitle").css("display", "inline-block");
                                    });
                                });
                                var ajaxField, startValue = $("#vidTitle").text();
                                ajaxField = function () { //variables
                                    var xmlHttp = null;
                                    if (typeof String.prototype.trim !== 'function') { //this will add trim to IE8
                                        String.prototype.trim = function () {
                                            return this.replace(/^\s+|\s+$/g, '');
                                        }
                                    }
                                    return {
                                        init: function () {
                                        },
                                        update: function (responseField, url) {
                                            xmlHttp = new XMLHttpRequest();
                                            xmlHttp.onreadystatechange = function () {
                                                if (xmlHttp.readyState != 4) {
                                                    return;
                                                }
                                                if (xmlHttp.status != 200 && xmlHttp.status != 304) {
                                                    alert('HTTP error ' + xmlHttp.status);
                                                    return;
                                                }
                                                if (xmlHttp.readyState == 4) {
                                                    ajaxField.handleResponse(responseField, xmlHttp.responseText);
                                                }
                                            };
                                            newText = document.getElementById('ajaxInput').value.trim();
                                            videoId = $('#video-id').val();
                                            huddleId = '<?php echo $huddle_id; ?>';
                                            url = url + "/" + huddleId + "/" + videoId + "/" + newText;
                                            xmlHttp.open("GET", url, true);
                                            xmlHttp.send(null);
                                            $("#vidTitle").css("display", "inline-block");
                                            $(".editArea").css("display", "none");
                                        },
                                        handleResponse: function (responseField, response) {
                                            document.getElementById(responseField).innerHTML = response;
                                        }

                                    };
                                }();
                            </script>
                        <?php elseif ($huddle_type == 3 && ($this->Custom->check_if_evalutor($huddle_id, $user_current_account['User']['id']) || $this->Custom->is_creator($user_current_account['User']['id'], $videoDetail['Document']['created_by']))): ?>

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#copy-huddle').click(function (e) {
                                        $('.copy-document-ids').val($(this).attr('data-document-id'));
                                        if ($(this).attr('data-total-comments') == 0)
                                            $('.copy_video_box').css('display', 'none');
                                        else
                                            $('.copy_video_box').css('display', 'block');
                                    });
                                    var fieldWidth = parseInt($("#vidTitle").css("width"));
                                    $(".submit").mousedown(function () {

                                        ajaxField.update("vidTitle", "<?php echo $this->base . '/Huddles/changeTitle' ?>");
                                    });
                                    //$("#ajaxInput").css("width", (fieldWidth - 150) + "px");
                                    //$(".editArea").css("width", (fieldWidth) + "px");
                                    $("#vidTitle").mouseover(function () {
                                        $(this).css("backgroundColor", "#FAFABE")
                                    });
                                    $("#vidTitle").mouseout(function () {
                                        $(this).css("backgroundColor", "#ffffff")
                                    });
                                    $("#vidTitle").click(function () {
                                        $(this).css("display", "none");
                                        $(".editArea").css("display", "inline-block");
                                        $("#ajaxInput").val($(this).text().trim());
                                        $("#ajaxInput").focus();
                                    });
                                    $("#ajaxInput").blur(function () {
                                        $(".editArea").css("display", "none");
                                        $("#vidTitle").css("display", "inline-block");
                                    });
                                });
                                var ajaxField, startValue = $("#vidTitle").text();
                                ajaxField = function () { //variables
                                    var xmlHttp = null;
                                    if (typeof String.prototype.trim !== 'function') { //this will add trim to IE8
                                        String.prototype.trim = function () {
                                            return this.replace(/^\s+|\s+$/g, '');
                                        }
                                    }
                                    return {
                                        init: function () {
                                        },
                                        update: function (responseField, url) {
                                            xmlHttp = new XMLHttpRequest();
                                            xmlHttp.onreadystatechange = function () {
                                                if (xmlHttp.readyState != 4) {
                                                    return;
                                                }
                                                if (xmlHttp.status != 200 && xmlHttp.status != 304) {
                                                    alert('HTTP error ' + xmlHttp.status);
                                                    return;
                                                }
                                                if (xmlHttp.readyState == 4) {
                                                    ajaxField.handleResponse(responseField, xmlHttp.responseText);
                                                }
                                            };
                                            newText = document.getElementById('ajaxInput').value.trim();
                                            videoId = $('#video-id').val();
                                            huddleId = '<?php echo $huddle_id; ?>';
                                            url = url + "/" + huddleId + "/" + videoId + "/" + newText;
                                            xmlHttp.open("GET", url, true);
                                            xmlHttp.send(null);
                                            $("#vidTitle").css("display", "inline-block");
                                            $(".editArea").css("display", "none");
                                        },
                                        handleResponse: function (responseField, response) {
                                            document.getElementById(responseField).innerHTML = response;
                                        }

                                    };
                                }();
                            </script>
                        <?php endif; ?>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#comment_form").hide();
                            });
                        </script>
                    </div>
                <?php else:
                    ?>
                    <div id="video_span">
                        <div class="no-vid">
                            <p style="margin-top:25px;" class="info">No Video Available.</p>
                        </div>
                    </div>
                <?php endif; ?>


            </div>

            <div class="clear" <?php echo $no_video; ?>></div>
            <div class="clear"></div>

        </div>

        <div class="right-box">
            <ul class="tabset commentstabs">
                <li class="">
                    <a id="comments" class="tab active" href="#commentsTab">
                        <!--Comments (<?php // echo $videoCommentsArray ? count($videoCommentsArray) : '0';                                                                                                                                                                                ?>)-->
                        <?php if (empty($videoCommentsArray)) {
                            ?>
                            Comments (<?php echo '0'; ?>)

                        <?php } else { ?>
                            Comments (<?php echo isset($comments_count) && !empty($comments_count) ? $comments_count : '0'; ?>)
                        <?php } ?>
                    </a>
                </li>
                <li class="">
                    <a id="attachment" class="tab" href="#attachmentTab">Attachments
                        (<?php echo $this->Custom->get_video_attachment_numbers_for_lti($video_id, $huddle_id, $user_id); ?>)</a>
                </li>

                <?php
                if ($huddle_permission == '200' || $huddle_permission == '210' ||
                        ($isCreator && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')):
                    ?>
                    <?php
                    $cls_hidden_icon = '';
                    $cls_mail_icon = '';
                    if ($huddle_type == 3) {
                        if ($videoCommentsArray != '') {
                            $cls_hidden_icon = "display: block";
                        } else {
                            $cls_hidden_icon = "display: none";
                        }
                        if ($this->Custom->check_if_eval_huddle_active($account_id)) {
                            if ($videoCommentsArray != '') {
                                $cls_mail_icon = "display: block";
                            } else {
                                $cls_mail_icon = "display: none";
                            }
                        }
                    } else {
                        $cls_hidden_icon = "display: block";
                        $cls_mail_icon = "display: none";
                    }
                    ?>

                    <div class="export-btns" style="padding-bottom: 0;">
        <!--                        <img src="/img/amchart-download.png" class="document_click">
                        <div class="document_outer">
                            <a href="<?php echo $export_excel_url ?>" title="Export comments as Excel"
                               id="comment-excel" data-video-id="<?php echo $video_id; ?>"
                               class="comment-excel tab-doc-cls"
                               style="margin-left: 0px;width: 23px; <?php echo $cls_hidden_icon ?>">Print Excel</a>
                            <a href="<?php echo $export_pdf_url ?>" title="Export comments as PDF" id="comment-acro"
                               data-video-id="<?php echo $video_id; ?>" class="comment-acro tab-doc-cls" target="_blank"
                               style="margin-left: -1px;width: 23px;<?php echo $cls_hidden_icon ?>">Print PDF</a>
                            <a title="Send Email" id="email_send" data-toggle="modal" data-target="#email_ob" href="#"
                               class="tab-doc-cls"
                               style="background-size: 100%;background-position: 0px 0px;float: right;margin-top: -2px; width: 23px; <?php echo $cls_mail_icon ?>"><img
                                    src="/img/email.png"></a>

                        </div>-->


                        <div style="clear: both;"></div>
                    </div>
                    <?php
                endif;
                ?>
            </ul>
            <div class="table-container p-left0">
                <div class="tab-content tab-active" id="commentsTab" style="visibility: visible;position: relative">
                    <div class="clear"></div>
                    <?php $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']); ?>
                    <?php
                    if ($huddle_permission == '200' || $huddle_permission == '210' ||
                            ($isCreator && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')):

                        if ($videoComments) {
                            $comment_box_display = 'style="display:none;"';
                            $comment_box_add_btn = 'style="display:block;"';
                        } else {
                            $comment_box_display = 'style="display:block;"';
                            $comment_box_add_btn = 'style="display:none;"';
                        }
                        if ($type != 'get_video_comments_obs') {
                            ?>

                                                    <!--<div style="float: left;margin-right: 7px;margin-top: 7px;">Autoscroll&nbsp; <input type="checkbox" name="auto_scroll_switch" id="auto_scroll_switch" myssss="<?php echo $auto_scroll_switch; ?>" <?php echo $auto_scroll_switch == '1' ? 'checked' : ''; ?>/></div>-->

                            <div style="clear: both;"></div>
                            <?php if ($this->Custom->is_enable_tags($account_id)) { ?>
                                <?php if ($this->Custom->is_enable_huddle_tags($account_folder_id)) { ?>

                                <?php } ?>
                            <?php } ?>
                            <div class="clear"></div>

                        <?php } ?>
                        <style>
                            .uncheck {
                                color: #7fc44f;
                                display: inline-block;
                                text-decoration: none;
                                border: 1px solid #7fc44f;
                                border-radius: 18px;
                                font-weight: 200;
                                padding: 1px 15px;
                            }
                        </style>
                    <?php else: ?>
                        <script type="text/javascript">
                            $(document).ready(function (e) {
                                $('.vjs-seek-handle').hover(function (e) {
                                    $('.vjs-tooltip').remove();
                                });
                            })
                        </script>
                    <?php endif; ?>
                    <div class="clear"></div>
                    <div id="vidComments">
                        <input type="hidden" id="video_ID" name="video_ID"
                               value="<?php echo $videoDetail['Document']['id'] ?>">
                        <div id="check">
                            <?php echo $html_comments ?>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="tab-content tab" id="attachmentTab" style="visibility: hidden;">

                    <!-- docs start -->
                    <div id="docs-container"></div>
                    <!-- docs end -->
                </div>
                <!--tab2-->
                <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                    <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                        <div class="tab-content tab" id="frameWorksTab" style="visibility: hidden;">
                            <div class="search-box standard-search" style="position: relative;">
                                <input type="button" id="btnSearchTags" class="btn-search" value="">
                                <input class="text-input" id="txtSearchTags" type="text" value=""
                                       placeholder="Search Standards..." style="margin-right: 0px;">
                                <span id="clearTagsButton" class="clear-video-input-box"
                                      style="display:none;right: 33px;top: 20px;">X</span>
                            </div>
                            <div id="scrollbar1" style="float: left;">
                                <div class="viewport short" style="overflow:scroll;overflow-x: hidden;">
                                    <div class="overview p-left0" style="top: 0px;padding: 0px;">
                                        <div id="listContainer">
                                            <style type="text/css">
                                                .standardRed {
                                                    color: red;
                                                    display: inline;
                                                }

                                                .standardBlue {
                                                    color: blue;
                                                    display: inline;
                                                }

                                                .standardBlack {
                                                    color: #000;
                                                    display: inline;
                                                }

                                                .standardOrange {
                                                    color: orange;
                                                    display: inline;
                                                }

                                                .standardGreen {
                                                    color: green;
                                                    display: inline;
                                                }

                                                .standardPurple {
                                                    color: purple;
                                                    display: inline;
                                                }

                                                .frame_work_heading {
                                                    font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
                                                    font-weight: 300 !important;
                                                    font-size: 20px !important;
                                                    margin: 10px 0px !important;
                                                    cursor: auto !important;
                                                }
                                            </style>
                                            <ul id="expList_vid" class="expList" style="padding-left:0px">
                                                <?php
                                                if (!empty($framework_data)) {

                                                    $checkbox_settings = $framework_data['account_framework_settings']['AccountFrameworkSetting']['checkbox_level'];
                                                    foreach ($framework_data['account_tag_type_0'] as $row) {
                                                        ?>
                                                        <?php if ($checkbox_settings == $row['AccountTag']['standard_level']): ?>
                                                            <li class="standard standard-cls L<?php echo $row['AccountTag']['standard_level'] ?>">
                                                                <?php if ($huddle_type == 3): ?>
                                                                    <?php if ($this->Custom->check_if_evalutor($huddle_id, $user_id)): ?>
                                                                        <input class="check_class" type="checkbox" name="name1" account_tag_id="<?php echo $row['AccountTag']['account_tag_id']; ?>" st_code="<?php echo $row['AccountTag']['tag_code']; ?>" st_name="<?php echo $row['AccountTag']['tag_title']; ?>"/>
                                                                    <?php endif ?>
                                                                <?php else: ?>
                                                                    <input class="check_class" type="checkbox" name="name1" account_tag_id="<?php echo $row['AccountTag']['account_tag_id']; ?>" st_code="<?php echo $row['AccountTag']['tag_code']; ?>" st_name="<?php echo $row['AccountTag']['tag_title']; ?>"/>
                                                                <?php endif ?>

                                                                <?php
                                                                $account_tag = explode(':', $row['AccountTag']['tag_html']);
                                                                echo '<span style="color: #7c7c69;font-weight: bold;">' . $row['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ': </span>' . $account_tag[1];
                                                                ?>
                                                            </li>
                                                        <?php else: ?>
                                                            <li class="standard standard-cls L<?php echo $row['AccountTag']['standard_level'] ?>">
                                                                <?php echo $row['AccountTag']['tag_code'] . ' - ' . $row['AccountTag']['tag_html']; ?>
                                                            </li>
                                                        <?php endif; ?>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <li id="noresults" style="width: 280px;">No standards match your search
                                                    criteria.
                                                </li>
                                            </ul>
                                            <script type="text/javascript">
                                                        //                                    $(document).ready(function () {
                                                                //                                        $('.standard input').on('change', function () {
                                                                        //                                            var tag_code = $(this).attr('st_code');
                                                                                //                                            var tag_value = '';
                                                                                        //                                            var tag_array = {};
                                                                                                //                                            var tag_name = $(this).attr('st_name');
                                                                                                        //                                            tag_array = tag_name.split(" ");
                                                                                                                //                                            if ($(this).is(':checked')) {
                                                                                                                        //                                                tag_array = tag_name.split(" ");
                                                                                                                                //                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                                                                                                        //                                                $('#txtVideostandard_vid').addTag(tag_value);
                                                                                                                                                //
                                                                                                                                                        //                                                if ($('input[name="name1"]:checked').length > 0) {
                                                                                                                                                                //                                                    $('#txtVideostandard_vid_tag').hide();
                                                                                                                                                                        //                                                }
                                                                                                                                                                                //                                            } else {
                                                                                                                                                                                        //                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                                                                                                                                                                //                                                $('#txtVideostandard_vid').removeTag(tag_value);
                                                                                                                                                                                                        //
                                                                                                                                                                                                                //
                                                                                                                                                                                                                        //                                            }
                                                                                                                                                                                                                                //
                                                                                                                                                                                                                                        //                                        });
                                                                                                                                                                                                                                                //                                    });
                                            </script>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <?php
            $created_by = $videoDetail['Document']['created_by'];
            $evaluated_email = '';
            //            if($this->Custom->check_if_evalutor($huddle_id,$user_current_account['User']['id'])){
            //               $evaluated_email =$user_current_account['User']['email'];
            //            }
            //            else{
            //               $evaluated_email = $this->Custom->get_created_by($huddle_id,$created_by);
            //            }
            //            $evaluated_email = $this->Custom->get_created_by($created_by);
            $evaluated_email = '';
            $evaluated_emails = $this->Custom->get_huddle_evaluator_emails($huddle_id);
            if (!empty($evaluated_emails)):
                foreach ($evaluated_emails as $evaluated_email_single):
                    $evaluated_email .= $evaluated_email_single . ",";
                endforeach;
            endif;
            $evaluated_email = rtrim($evaluated_email, ',');
            ?>

            <div id="email_ob" class="modal in" role="dialog" aria-hidden="true">
                <div class="modal-dialog" style="width:600px;">
                    <div class="modal-content">
                        <div class="header" style="margin-bottom:0px;">
                            <h4 class="header-title nomargin-vertical smargin-bottom">Send Email</h4>
                            <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                        </div>

                        <div class="send_message_dialog">
                            <form id="feedback-form" method="post"
                                  action="<?php echo $this->base . '/huddles/sent_feedback_email' ?>"
                                  enctype="multipart/form-data">
                                <label class="label" for="subject">Subject</label><br>
                                <input id="subject" value="<?php echo $video_title ?>" type="text" name="subject"
                                       class="input-xlarge"><br>
                                <label class="label" for="email">To:</label><br>
                                <input id="email" value="<?php echo $evaluated_email ?>" type="email" name="email"
                                       class="input-xlarge"><br>

                                <div class="attachment_box">
                                    <!--<img src="/app/img/attachment.png" />-->
                                    <span id="filename_email_span"></span>
                                </div>
                                <label class="label" for="message">Additional Attachment</label><br>
                                <input type="file" name="additional_attachemnt"/><br/>

                                <label class="label" for="message">Enter a Message</label><br>
                                <textarea id="message" name="message" class="input-xlarge"
                                          style="height: 80px;"></textarea>
                                <input id="account_id" name="account_id" type="hidden"
                                       value="<?php echo $account_id ?>">
                                <input id="filename_email" name="filename_email" type="hidden" value="">
                                <input id="video_id" name="video_id" type="hidden"
                                       value="<?php echo $videoDetail['Document']['id'] ?>">
                                <input id="huddle_id" name="huddle_id" type="hidden" value="<?php echo $huddle_id ?>">
                                <input id="send_email" type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" value="Send Email" class="btn btn-green"></input>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endif; ?>
    <style>
        .copy_video_tabs {
            margin: 0 20px;
            margin-bottom: 55px;
        }

        .copy_video_tabs section {
            display: none;
            padding: 10px;
            border: 1px solid #ddd;
            background: #fff;
            font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        }

        .copy_video_tabs input {
            display: none;
        }

        .copy_video_tabs .btn-green {
            display: inline-block;
            position: absolute;
            bottom: 12px;
            left: 20px;
        }

        .tabs_box {
            display: inline-block;
            margin: 0 0 -1px;
            padding: 15px 12px;
            font-weight: 600;
            text-align: center;
            color: #bbb;
            border: 1px solid transparent;
            font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
            font-size: 13px;
        }

        .tabs_box img {
            width: 18px;
            height: 18px;
            vertical-align: middle;
            margin-right: 5px;
        }

        .tabs_box:hover {
            color: #888;
            cursor: pointer;
        }

        .copy_video_tabs input:checked + .tabs_box {
            color: #555;
            border: 1px solid #ddd;
            border-top: 2px solid #5daf46;
            border-bottom: 1px solid #fff;
            background: #fff;
        }

        .copy_video_tabs #ctab1:checked ~ #content1,
        .copy_video_tabs #ctab2:checked ~ #content2,
        .copy_video_tabs #ctab3:checked ~ #content3,
        .copy_video_tabs #tab4:checked ~ #content4,
        .copy_video_tabs #tab5:checked ~ #content5,
        .copy_video_tabs #tab6:checked ~ #content6 {
            display: block;
        }

        .copy_filter {
            box-sizing: border-box;
            padding: 8px;
            border: solid 1px #ececec;
            font-size: 13px;
            width: 100%;
            display: block !important;
            outline: none;
            -webkit-box-shadow: inset 0px 10px 10px -11px rgba(0, 0, 0, 0.39);
            -moz-box-shadow: inset 0px 10px 10px -11px rgba(0, 0, 0, 0.39);
            box-shadow: inset 0px 10px 10px -11px rgba(0, 0, 0, 0.39);
        }

        .filter_data_box {
            height: 150px;
            overflow-y: auto;
            overflow-x: hidden;
            margin-top: 10px;
        }

        .filter_data_box .inset-area {
            border: 0px;
            border-radius: 0px;
            box-shadow: none;
            padding: 0px;
            margin: 0px;
        }

        .filter_data_box form {
            padding: 0px;
        }

        .filter_data_box .files-list > li ~ li {
            margin-top: 12px;
        }

        .filter_data_box .files-list li {
            padding-left: 5px;
        }

        @media screen and (max-width: 650px) {
            .tabs_box {
                font-size: 0;
            }

            .copy_video_tabs .tabs_box:before {
                margin: 0;
                font-size: 18px;
            }
        }

        .copy_link {
            font-size: 15px !important;
            color: #5a80a0 !important;
            font-weight: normal !important;
        }

        .smargin-bottom {
            margin-bottom: 0px !important;
        }

        @media screen and (max-width: 400px) {
            .tabs_box {
                padding: 15px;
            }
        }

        .copy_video_box {
            position: absolute;
            right: 24px;
            bottom: 17px;
            font-size: 14px;
            color: #5a90bf;
        }

        .copy_video_box .ui-checkbox {
            top: 2px;
            width: 15px;
            height: 15px;
        }

        .send_message_dialog {
            padding: 20px;
        }

        .send_message_dialog input[type="text"],
        .send_message_dialog input[type="email"],
        .send_message_dialog textarea {
            width: 100%;
            margin-bottom: 8px;
            margin-top: 3px;
        }

        .attachment_box {
            margin: 5px 0px;
        }

        #comment_comment:focus {
            z-index: 20;
            /*        box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82,168,236,0.6) !important;
                    border-color: rgba(82,168,236,0.8) !important;*/
            border-color: #fff !important;
            box-shadow: none !important;
        }

        .petsCls {
            float: right;
            margin-right: 10px;
            /*margin-top: -27px;*/
            right: 0;
            position: relative;
        }

        .petsCls label {
            font-weight: normal !important;
            font-size: 13px;
            float: left;
        }

        input#press_enter_to_send {
            margin-top: 3px;
            float: left;
            margin-right: 4px;
        }
    </style>
    <div id="moveFiles" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom">Copy video</h4>
                    <a id="copy_video_huddles" class="close-reveal-modal btn btn-grey close style2"
                       data-dismiss="modal">x</a>
                </div>
                <div class="copy_video_tabs">
                    <input id="ctab1" type="radio" name="tabs" checked>
                    <label class="tabs_box" for="ctab1"><img src="/img/dbi-huddles.png" alt=""> Copy to
                        Huddle(s)</label>
                    <?php if ($user_permissions['UserAccount']['permission_video_library_upload'] == '1'): ?>
                        <?php if ($this->Custom->get_account_video_permissions($user_current_account['users_accounts']['account_id'])): ?>
                            <input id="ctab2" type="radio" name="tabs">
                            <label class="tabs_box" for="ctab2"><img src="/img/dbi-video.png" alt=""> Copy to Video
                                Library</label>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if (count($all_accounts) > 0): ?>
                        <input id="ctab3" type="radio" name="tabs">
                        <?php if (count($all_accounts) == 1): ?>
                            <label class="tabs_box" for="ctab3"><img src="/img/copy_to_account.png" alt=""> Copy to
                                Workspace</label>
                        <?php else: ?>
                            <label class="tabs_box" for="ctab3"><img src="/img/copy_to_account.png" alt=""> Copy to
                                Account(s)</label>
                        <?php endif; ?>
                    <?php endif; ?>
                    <section id="content1">
                        <!--                    <input class="copy_filter" type="text" placeholder="Find Huddles">-->
                        <div id="header-container" class="filterform">
                        </div>
                        <div class="widget-scrollable2">
                            <div class="filter_data_box">
                                <form accept-charset="UTF-8" id="huddle-copy-form"
                                      action="<?php echo $this->base . '/Huddles/copy/' ?>"
                                      enctype="multipart/form-data" method="post">
                                    <input name="document_id" type="hidden" class="copy-document-ids"
                                           id="copy-document-id" value=""/>
                                    <input name="current_huddle_id" type="hidden" value="<?php echo $huddle_id; ?>"/>

                                    <ul id="list-containers"
                                        class="inset-area clear-list files-list" <?php if (isset($all_huddle) && count($all_huddle) > 6): ?>
                                        <?php endif; ?>>
                                            <?php if (!empty($all_huddle)): ?>
                                                <?php
                                                $li = '';
                                                foreach ($all_huddle as $row):
                                                    ?>
                                                    <?php
                                                    if ($row['AccountFolder']['account_folder_id'] == $huddle_id) {
                                                        continue;
                                                    }
                                                    $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                                                    $userGroups = $AccountFolderGroup->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                                                    $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $userGroups, $user_current_account['User']['id']);

                                                    $current_user = $this->Session->read('user_current_account');
                                                    $account_id = $current_user['accounts']['account_id'];
                                                    $user_id = $current_user['User']['id'];

                                                    $permissions = $AccountFolderUser->getUserAccount($row['AccountFolder']['account_folder_id'], $user_id);

                                                    $huddle_role = '';
                                                    if (isset($permissions) && isset($permissions['AccountFolderUser'])) {
                                                        $huddle_role = $permissions['AccountFolderUser']['role_id'];
                                                    } else {
                                                        $permissions = $UserGroup->get_acount_folder_user_group($row['AccountFolder']['account_folder_id'], $user_id);
                                                        if (isset($permissions) && isset($permissions['account_folder_groups'])) {
                                                            $huddle_role = $permissions['account_folder_groups']['role_id'];
                                                        } else {
                                                            $huddle_role = '220';
                                                        }
                                                    }
                                                    ?>

                                                <?php if (($huddle_role == '200' || $huddle_role == '210') || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                    <?php
                                                    $li .= '<li>';
                                                    $li .= '<label class="ui-checkbox model">';
                                                    $li .= '<input class="copyFiles_checkbox" name="account_folder_id[]" id="account_folder_name_' . $row['AccountFolder']['account_folder_id'] . '" type="checkbox" value="' . $row['AccountFolder']['account_folder_id'] . '">';
                                                    $li .= '</label>';
                                                    $li .= '<label  for="account_folder_name_' . $row['AccountFolder']['account_folder_id'] . '"><a class="copy_link">';
                                                    $li .= $row['AccountFolder']['name'];
                                                    $li .= '</a></label>';
                                                    $li .= '</li>';
                                                    ?>
                                                <?php endif; ?>

                                            <?php endforeach; ?>
                                            <?php
                                            if (!empty($li)) {
                                                echo $li;
                                            } else {
                                                echo "<li>You are not participating in any other Huddles.</li>";
                                            }
                                            ?>
                                        <?php else: ?>
                                            <li>You are not participating in any other Huddles</li>
                                        <?php endif; ?>

                                    </ul>
                                    <div class="copy_video_box">
                                        <label class="ui-checkbox model">
                                            <input name="copy_notes" id="copy_notes" type="checkbox"
                                                   class="copyFiles_checkbox" value="1">
                                        </label>
                                        <label for="copy_notes">Copy Video Comments</label>
                                    </div>
                                    <input class="btn btn-green" type="submit" name="submit" id="copy-huddle-btn1"
                                           value="Copy">
                                </form>
                            </div>
                        </div>
                    </section>

                    <section id="content2">

                        <div class="filter_data_box">
                            <form accept-charset="UTF-8" id="huddle-copy-form1"
                                  action="<?php echo $this->base . '/Huddles/copy/' ?>" enctype="multipart/form-data"
                                  method="post">
                                <input name="document_id" type="hidden" class="copy-document-ids" id="copy-document-id"
                                       value=""/>
                                <input name="current_huddle_id" type="hidden" value="<?php echo $huddle_id; ?>"/>

                                <ul class="inset-area clear-list files-list" <?php if (isset($all_huddle) && count($all_huddle) > 6): ?>
                                    <?php endif; ?>>
                                        <?php if ($user_permissions['UserAccount']['permission_video_library_upload'] == '1'): ?>
                                            <?php if ($this->Custom->get_account_video_permissions($user_current_account['users_accounts']['account_id'])): ?>
                                            <li>
                                                <label class="ui-checkbox model">
                                                    <input class="copyFiles_checkbox" name="account_folder_id[]"
                                                           id="account_folder_name_-1" type="checkbox" value="-1">
                                                </label>
                                                <label for="account_folder_name_-1">Copy to Video Library </label>
                                            </li>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                </ul>
                                <input class="btn btn-green" type="submit" name="submit" id="copy-huddle-btn-library"
                                       value="Copy">
                            </form>
                        </div>
                    </section>

                    <section id="content3">

                        <!--                        <input class="copy_filter" type="text" placeholder="Find Accounts">-->
                        <div id="header-container-accounts" class="filterform">
                        </div>
                        <div class="widget-scrollable2">
                            <div class="filter_data_box">
                                <form id="show-accounts" accept-charset="UTF-8"
                                      action="<?php echo $this->base . '/Huddles/copytoaccounts/' ?>"
                                      enctype="multipart/form-data" method="post">

                                    <input name="document_id" type="hidden" class="copy-document-ids"
                                           id="copy-document-id" value=""/>
                                    <input name="huddle_id" type="hidden" id="huddle_id"
                                           value="<?php echo $huddle_id; ?>"/>
                                    <ul id="list-containers-accounts"
                                        class="inset-area clear-list files-list" <?php if (isset($all_accounts) && count($all_accounts) > 5): ?>
                                        <?php endif; ?>>
                                            <?php
                                            foreach ($all_accounts as $account):

//                                                if ($account['accounts']['account_id'] == $present_account_id && !$user_current_account['users_accounts']['huddle_to_workspace']):
//                                                    continue;
//                                                endif;
                                                ?>
                                            <li>
                                                <label class="ui-checkbox model">
                                                    <input name="account_ids[]"
                                                           id="account_id_<?php echo $account['accounts']['account_id']; ?>"
                                                           type="checkbox" class="copyFiles_checkbox"
                                                           value="<?php echo $account['accounts']['account_id']; ?>">
                                                </label>
                                                <label for="account_id_<?php echo $account['accounts']['account_id']; ?>">
                                                    <a class="copy_link"><?php echo $account['accounts']['company_name']; ?></a>
                                                    - <label style="color: #000000;font-weight: 600;">Workspace</label>
                                                </label>

                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <div class="copy_video_box">
                                        <label class="ui-checkbox model">
                                            <input name="copy_notes" id="copy_notes" type="checkbox"
                                                   class="copyFiles_checkbox" value="1">
                                        </label>
                                        <label for="copy_notes">Copy Video Comments</label>
                                    </div>
                                    <input class="btn btn-green" type="submit" name="submit" id="<?php ?>"
                                           onclick="return verifyaccountBeforeCopyFiles();" value="Copy">
                                    <script type="text/javascript">
                                                                                                                                                                                                                                        function verifyaccountBeforeCopyFiles() {

                                                                                                                                                                                                                                            count = 0;
                                                                                                                                                                                                                                            var video_lib_selected = false;
                                                                                                                                                                                                                                            $.each($('.copyFiles_checkbox'), function (index, value) {

                                                                                                                                                                                                                                                if ($(this).prop('checked') == true) {
                                                                                                                                                                                                                                                    count++;
                                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                            });
                                                                                                                                                                                                                                            if (count == 0) {
                                                                                                                                                                                                                                                alert('Please Select at least one account.');
                                                                                                                                                                                                                                                return false;
                                                                                                                                                                                                                                            }

                                                                                                                                                                                                                                            return true;
                                                                                                                                                                                                                                        }

                                                                                                                                                                                                                                        $('#copy-huddle-btn1').on('click', function (e) {
                                                                                                                                                                                                                                            e.preventDefault();
                                                                                                                                                                                                                                            count = 0;
                                                                                                                                                                                                                                            var video_lib_selected = false;
                                                                                                                                                                                                                                            $.each($('.copyFiles_checkbox'), function (index, value) {

                                                                                                                                                                                                                                                if ($(this).prop('checked') == true) {
                                                                                                                                                                                                                                                    count++;
                                                                                                                                                                                                                                                }

                                                                                                                                                                                                                                            });
                                                                                                                                                                                                                                            if (count == 0) {
                                                                                                                                                                                                                                                alert('Please Select at least one Huddle.');
                                                                                                                                                                                                                                                return false;
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                            $('#copy-huddle-btn').val('Copying...');
                                                                                                                                                                                                                                            $.ajax({
                                                                                                                                                                                                                                                url: home_url + "/Huddles/copy",
                                                                                                                                                                                                                                                data: $('#huddle-copy-form').serialize(),
                                                                                                                                                                                                                                                type: 'POST',
                                                                                                                                                                                                                                                dataType: 'json',
                                                                                                                                                                                                                                                success: function (response) {
                                                                                                                                                                                                                                                    $('#copy-huddle-btn').val('Copy');
                                                                                                                                                                                                                                                    $('#notification').css('display', 'block');
                                                                                                                                                                                                                                                    if (response.status == true) {
                                                                                                                                                                                                                                                        $('#notification').html('<div class="message success" style="cursor:pointer">' + response.message + '</div>');
                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                        $('#notification').html('<div class="message error"  style="cursor:pointer">' + response.message + '</div>');
                                                                                                                                                                                                                                                    }

                                                                                                                                                                                                                                                    $('#moveFiles').modal('hide');
                                                                                                                                                                                                                                                },
                                                                                                                                                                                                                                                error: function () {
                                                                                                                                                                                                                                                    alert("Network Error Occured");
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                            });
                                                                                                                                                                                                                                        });
                                    </script>
                                </form>
                            </div>
                        </div>
                    </section>


                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
                                                                                                                                                                                                    $("#email_send").click(function () {
                                                                                                                                                                                                        var video_id = $('#video_ID').val();
                                                                                                                                                                                                        $.ajax({
                                                                                                                                                                                                            type: 'POST',
                                                                                                                                                                                                            url: home_url + '/Huddles/print_pdf_comments_1/' + video_id,
                                                                                                                                                                                                            success: function (response) {
                                                                                                                                                                                                                $('#filename_email').val(response);
                                                                                                                                                                                                                //alert($('#filename_email').val());
                                                                                                                                                                                                                //                                                                                                                                                                                                                $('#filename_email_span').html('PdfReport' + response);

                                                                                                                                                                                                            }
                                                                                                                                                                                                        });


                                                                                                                                                                                                    });
                                                                                                                                                                                                    $("#send_email").click(function () {
                                                                                                                                                                                                        $('#email_ob').modal('hide');
                                                                                                                                                                                                        $('#feedback-form').submit();
                                                                                                                                                                                                        var formData = new FormData($('#feedback-form'));
                                                                                                                                                                                                        //        var postData = {
                                                                                                                                                                                                        //            name: $('#name').val(),
                                                                                                                                                                                                        //            email: $('#email').val(),
                                                                                                                                                                                                        //            message: $('#message').val(),
                                                                                                                                                                                                        //            account_id: $('#account_id').val(),
                                                                                                                                                                                                        //            subject: $('#subject').val(),
                                                                                                                                                                                                        //            filename: $('#filename_email').val(),
                                                                                                                                                                                                        //            video_id: $('#video_ID').val(),
                                                                                                                                                                                                        //            huddle_id: $('#huddle_id').val()
                                                                                                                                                                                                        //        };


                                                                                                                                                                                                        $.ajax({
                                                                                                                                                                                                            type: 'POST',
                                                                                                                                                                                                            url: home_url + '/Huddles/sent_feedback_email/',
                                                                                                                                                                                                            data: formData,
                                                                                                                                                                                                            processData: false,
                                                                                                                                                                                                            contentType: false,
                                                                                                                                                                                                            success: function (response) {
                                                                                                                                                                                                                var show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">Email Sent Successfully</div>';
                                                                                                                                                                                                                $("#showflashmessage1").prepend(show_msg);

                                                                                                                                                                                                            }
                                                                                                                                                                                                        });
                                                                                                                                                                                                    });
</script>
<script type="text/javascript">
                                                                                                                                                                                                    $(function () {

                                                                                                                                                                                                        $('input#txtSearchTags').quicksearch('ul#expList_vid li', {
                                                                                                                                                                                                            noResults: 'li#noresults',
                                                                                                                                                                                                            'onAfter': function () {
                                                                                                                                                                                                                if (typeof $('#txtSearchTags').val() !== "undefined" && $('#txtSearchTags').val().length > 0) {
                                                                                                                                                                                                                    $('#clearTagsButton').css('display', 'block');
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        });

                                                                                                                                                                                                    });


</script>
<script type="text/javascript">

                                                                                                                                                                                                    function sbtfrm() {
                                                                                                                                                                                                        var tags_chk_values = [];
                                                                                                                                                                                                        $('input.check_class').each(function () {
                                                                                                                                                                                                            if ($(this).is(':checked')) {
                                                                                                                                                                                                                tags_chk_values.push($(this).attr('account_tag_id'));
                                                                                                                                                                                                            }
                                                                                                                                                                                                        });
                                                                                                                                                                                                        if (tags_chk_values !== '') {
                                                                                                                                                                                                            $('#txtVideostandard_vid_acc_tag_ids').val(tags_chk_values);
                                                                                                                                                                                                            var standardswhenpresent_acc_tag_ids = $('#txtVideostandard_vid_acc_tag_ids').val();
                                                                                                                                                                                                        } else {
                                                                                                                                                                                                            standardswhenpresent_acc_tag_ids = '';
                                                                                                                                                                                                        }

                                                                                                                                                                                                        var $form = $('#comments-form2');

                                                                                                                                                                                                        var sync_time = $('#synchro_time').val();
                                                                                                                                                                                                        var totalSec = '0';
                                                                                                                                                                                                        if ($('#for_synchro_time').is(':checked') && sync_time !== '{:value=>""}') {
                                                                                                                                                                                                            totalSec = $('#synchro_time').val();
                                                                                                                                                                                                        }


                                                                                                                                                                                                        $("#attached_image_name").html('');

                                                                                                                                                                                                        var attached_image_name = $('#comment_attachment').val().replace(/C:\\fakepath\\/i, '');

                                                                                                                                                                                                        if (attached_image_name != '') {
                                                                                                                                                                                                            var attachment_no = $("#attachment").text();
                                                                                                                                                                                                            var attach_new = attachment_no.substring(13, 14);
                                                                                                                                                                                                            attachment_no = parseInt(attach_new) + 1;
                                                                                                                                                                                                            $("#attachment").text('Attachments (' + attachment_no + ')');

                                                                                                                                                                                                            var html = '';
                                                                                                                                                                                                            html += '<li class="attached_document_class" id="li_video_doc_29430" style="width: 100%;">';
                                                                                                                                                                                                            html += '<span data-time="" class="synchro-time" style="cursor: pointer;margin-right: 25px;">Uploading...</span>';
                                                                                                                                                                                                            html += '<a id="view_resource_29430" target="_blank" href="/app/view_document/3IOhA5QUSAKprRbhpLuI" class="wrap"><span class="' + getIcons(attached_image_name) + '">Icon</span>' + attached_image_name + '</a>';
                                                                                                                                                                                                            html += '<form id="delete-video-document29430" data-async="" accept-charset="UTF-8" action="/Huddles/deleteVideoDocument/29430/29429/22143">';
                                                                                                                                                                                                            //   html +=   '<a rel="nofollow" data-confirm="Are you sure you want to delete this Resource?" id="delete-main-comments" class="del" href="javascript:$("#delete-video-document29430").submit()">&nbsp;</a>';
                                                                                                                                                                                                            html += '</form>';
                                                                                                                                                                                                            //  html +=      '<a download="" class="file_download_cls" href="http://www.filestackapi.com/api/file/3IOhA5QUSAKprRbhpLuI">';
                                                                                                                                                                                                            //   html +=      '<img alt="Download" class="right smargin-right" style="height: 24px;" rel="tooltip" src="/img/new/download-2.png" title="download video">';
                                                                                                                                                                                                            //   html +=   '</a>';
                                                                                                                                                                                                            html += '</li>';


                                                                                                                                                                                                            if ($('ul.video-docs li').length == 1) {
                                                                                                                                                                                                                if ($('ul.video-docs li').text() == 'No Attachments have been added.' || $('ul.video-docs li').text() == 'No Attachments have been added.') {
                                                                                                                                                                                                                    $('ul.video-docs').html(html);
                                                                                                                                                                                                                }
                                                                                                                                                                                                                else {
                                                                                                                                                                                                                    $('ul.video-docs').last().append(html);
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                            else {
                                                                                                                                                                                                                $('ul.video-docs').last().append(html);
                                                                                                                                                                                                            }
                                                                                                                                                                                                        }


                                                                                                                                                                                                        var myForm = document.getElementById('comments-form2');
                                                                                                                                                                                                        formData = new FormData(myForm);

                                                                                                                                                                                                        var huddle_id = $('#huddle_id').val();
                                                                                                                                                                                                        var video_id = $('#videoId').val();
                                                                                                                                                                                                        var account_id = '<?php echo $account_id; ?>';
                                                                                                                                                                                                        if (typeof ($('#comment_attachment').val()) != 'undefined' && $('#comment_attachment').val() != '') {
                                                                                                                                                                                                            $.ajax({
                                                                                                                                                                                                                type: $form.attr('method'),
                                                                                                                                                                                                                url: home_url + '/Huddles/upload_comment_file/' + huddle_id + '/' + video_id + '/' + account_id,
                                                                                                                                                                                                                processData: false,
                                                                                                                                                                                                                contentType: false,
                                                                                                                                                                                                                data: formData,
                                                                                                                                                                                                                dataType: 'json',
                                                                                                                                                                                                                success: function (data) {

                                                                                                                                                                                                                    if (totalSec == '{:value=>""}') {
                                                                                                                                                                                                                        totalSec = '0';
                                                                                                                                                                                                                    }

                                                                                                                                                                                                                    $.ajax({
                                                                                                                                                                                                                        url: home_url + "/huddles/generate_url/" + data.document_id,
                                                                                                                                                                                                                        type: 'POST',
                                                                                                                                                                                                                        // dataType: 'json',
                                                                                                                                                                                                                        success: function (response) {

                                                                                                                                                                                                                            filepicker.setKey('A3w6JbXR2RJmKr3kfnbZtz');

                                                                                                                                                                                                                            filepicker.storeUrl(
                                                                                                                                                                                                                                    response,
                                                                                                                                                                                                                                    {filename: data.file_name},
                                                                                                                                                                                                                            function (blob) {
                                                                                                                                                                                                                                // var result = blob.url.split('/');
                                                                                                                                                                                                                                $.ajax({
                                                                                                                                                                                                                                    type: 'POST',
                                                                                                                                                                                                                                    url: home_url + '/Huddles/update_stack_url/' + data.document_id + '/' + video_id,
                                                                                                                                                                                                                                    data: {stack_url: blob.url, timestamp_duration: totalSec},
                                                                                                                                                                                                                                    success: function (response) {
                                                                                                                                                                                                                                        //var attachment_no = '<?php //echo $this->Custom->get_video_attachment_numbers($video_id);                                                                                                                                                     ?>' ;
                                                                                                                                                                                                                                        // attachment_no = parseInt(attachment_no) + 1;
                                                                                                                                                                                                                                        $('#txtUploadedUrl').val(blob.url);
                                                                                                                                                                                                                                        $('#doc-title').html(blob.filename);
                                                                                                                                                                                                                                        $('#doc-type').html(blob.mimetype);
                                                                                                                                                                                                                                        $('#add-document-row').html($('#extra-row-li').html());
                                                                                                                                                                                                                                        var attachment_no = parseInt(response);
                                                                                                                                                                                                                                        $("#attachment").text('Attachments (' + attachment_no + ')');
                                                                                                                                                                                                                                        CloseDocumentUpload();
                                                                                                                                                                                                                                        getVideoComments();


                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                });


                                                                                                                                                                                                                            }
                                                                                                                                                                                                                            );


                                                                                                                                                                                                                        },
                                                                                                                                                                                                                        error: function () {
                                                                                                                                                                                                                            alert("Network Error Occured");
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                    });


                                                                                                                                                                                                                }
                                                                                                                                                                                                            });
                                                                                                                                                                                                        }

                                                                                                                                                                                                        var published = '<?php echo $this->Custom->get_video_published($video_id); ?>';
                                                                                                                                                                                                        if (published == '1') {
                                                                                                                                                                                                            playPlayer();
                                                                                                                                                                                                        }
                                                                                                                                                                                                        var audio_path = $('#add_audio_comments').val();

                                                                                                                                                                                                        if ($('#comment_comment').val() == '' && ($('#txtVideostandard_vid').val() == '' || $('#txtVideostandard_vid').length == 0) && $('#txtVideoTags').val() == '' && audio_path == '') {
                                                                                                                                                                                                            $('#comment_comment').css('border', '1px solid red');
                                                                                                                                                                                                            $('#comment_comment').css('border-radius', '0');
                                                                                                                                                                                                            return true;
                                                                                                                                                                                                        }
                                                                                                                                                                                                        $('#comment_comment').css('border', 'none');
                                                                                                                                                                                                        var result = '00:00:00';
                                                                                                                                                                                                        if ($('#for_synchro_time').is(':checked')) {
                                                                                                                                                                                                            var totalSec = $('#synchro_time').val();
                                                                                                                                                                                                            var hours = Math.floor(totalSec / 3600);
                                                                                                                                                                                                            var minutes = Math.floor((totalSec - (hours * 3600)) / 60);
                                                                                                                                                                                                            var seconds = totalSec - (hours * 3600) - (minutes * 60);

                                                                                                                                                                                                            result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
                                                                                                                                                                                                        }
                                                                                                                                                                                                        $("ul#expList_vid").find('input:checkbox').removeAttr('checked');
                                                                                                                                                                                                        if ($('#txtVideostandard_vid').length > 0) {
                                                                                                                                                                                                            var standardswhenpresent = $('#txtVideostandard_vid').val();
                                                                                                                                                                                                        } else {
                                                                                                                                                                                                            standardswhenpresent = '';
                                                                                                                                                                                                        }
//        if ($('#txtVideostandard_vid_acc_tag_ids').length > 0) {
//            var standardswhenpresent_acc_tag_ids = $('#txtVideostandard_vid_acc_tag_ids').val();
//        } else {
//             standardswhenpresent_acc_tag_ids = '';
//        }

                                                                                                                                                                                                        var postData = {
                                                                                                                                                                                                            observation_title: $('#ObservationTitle').text(),
                                                                                                                                                                                                            observations_comments: $('#comment_comment').val(),
                                                                                                                                                                                                            observations_standards: standardswhenpresent,
                                                                                                                                                                                                            observations_standard_acc_tag_id: standardswhenpresent_acc_tag_ids,
                                                                                                                                                                                                            observations_tags: $('#txtVideoTags').val(),
                                                                                                                                                                                                            observations_time: result,
                                                                                                                                                                                                            account_id: $('#account_id').val(),
                                                                                                                                                                                                            user_id: $('#user_id').val(),
                                                                                                                                                                                                            huddle_id: $('#huddle_id').val(),
                                                                                                                                                                                                            video_id: $('#videoId').val(),
                                                                                                                                                                                                            internal_comment_id: observation_notes_array.length + 1,
                                                                                                                                                                                                            comments_optimized: 1,
                                                                                                                                                                                                            assessment_value: $('#synchro_time_class_tags').val(),
                                                                                                                                                                                                            audio_path: audio_path
                                                                                                                                                                                                        };
                                                                                                                                                                                                        var hud_type = '<?php echo $huddle_type; ?>';

                                                                                                                                                                                                        if (hud_type == '1') {
                                                                                                                                                                                                            var huddle_name = 'Collaboration Huddle';
                                                                                                                                                                                                        }
                                                                                                                                                                                                        else if (hud_type == '2') {
                                                                                                                                                                                                            var huddle_name = 'Coaching Huddle';
                                                                                                                                                                                                        }
                                                                                                                                                                                                        else {
                                                                                                                                                                                                            var huddle_name = 'Assessment Huddle';
                                                                                                                                                                                                        }
<?php //if (IS_QA):     ?>
                                                                                                                                                                                                        var metadata = {
                                                                                                                                                                                                            comment_added: $('#comment_comment').val(),
                                                                                                                                                                                                            huddle_type: huddle_name,
                                                                                                                                                                                                            user_role: '<?php echo $user_role; ?>'
                                                                                                                                                                                                        };
                                                                                                                                                                                                        Intercom('trackEvent', 'comment-added', metadata);
<?php //endif;     ?>

                                                                                                                                                                                                        //this.submit.disabled = 1;
                                                                                                                                                                                                        $("#comments").trigger("click");
                                                                                                                                                                                                        var $form = $(this);
                                                                                                                                                                                                        //$form[0].submit.disabled = 0;
                                                                                                                                                                                                        if ($('#comment_comment').length > 0) {
                                                                                                                                                                                                            //$('#comment_comment').val('');
                                                                                                                                                                                                            $('#txtVideoTags_tagsinput span').remove();

                                                                                                                                                                                                            $('#txtVideostandard_tagsinput span').remove();
                                                                                                                                                                                                            $('#txtVideostandard_vid_tagsinput span').remove();
                                                                                                                                                                                                            $('#txtVideostandard_tag').show();
                                                                                                                                                                                                            $('#txtVideostandard_vid_tag').show();

                                                                                                                                                                                                            var tagid = '';
                                                                                                                                                                                                            var quick_tag_counter = 0;
                                                                                                                                                                                                            $(".divblockwidth a").each(function (value) {
                                                                                                                                                                                                                $(this).attr('status_flag', '0');
                                                                                                                                                                                                                $(this).removeAttr('class');

                                                                                                                                                                                                                if (quick_tag_counter == 0) {
                                                                                                                                                                                                                    $(this).attr('class', 'default_tag tags_qucls');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (quick_tag_counter == 1) {
                                                                                                                                                                                                                    $(this).attr('class', 'default_tag tags_sugcls');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (quick_tag_counter == 2) {
                                                                                                                                                                                                                    $(this).attr('class', 'default_tag tags_notescls');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (quick_tag_counter == 3) {
                                                                                                                                                                                                                    $(this).attr('class', 'default_tag tags_strangthcls');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                quick_tag_counter++;
                                                                                                                                                                                                            });
                                                                                                                                                                                                            var tagid = '';
                                                                                                                                                                                                            var quick_tag_counter = 0;
                                                                                                                                                                                                            $(".divblockwidth2 a").each(function (value) {
                                                                                                                                                                                                                $(this).attr('status_flag', '0');
                                                                                                                                                                                                                $(this).removeAttr('class');

                                                                                                                                                                                                                if (quick_tag_counter == 0) {
                                                                                                                                                                                                                    $(this).attr('class', 'default_rating tags_qucls rating_black');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (quick_tag_counter == 1) {
                                                                                                                                                                                                                    $(this).attr('class', 'default_rating tags_sugcls rating_black');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (quick_tag_counter == 2) {
                                                                                                                                                                                                                    $(this).attr('class', 'default_rating tags_notescls rating_black');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (quick_tag_counter == 3) {
                                                                                                                                                                                                                    $(this).attr('class', 'default_rating tags_strangthcls rating_black');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                if (quick_tag_counter == 4) {
                                                                                                                                                                                                                    $(this).attr('class', 'default_rating tags_strangthcls rating_black');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                quick_tag_counter++;
                                                                                                                                                                                                            });
                                                                                                                                                                                                            $("#expList").find('input:checkbox').removeAttr('checked');
                                                                                                                                                                                                            $('#txtSearchVideos').val('');
                                                                                                                                                                                                            $('#comment_comment').attr("placeholder", "Add a comment...");
                                                                                                                                                                                                            $('#comment_comment').val('');
                                                                                                                                                                                                            $('#synchro_time_class_tags').val('');
                                                                                                                                                                                                            $('#txtVideoTags').removeAllTag();
                                                                                                                                                                                                            $('#txtVideostandard_vid').removeAllTag();
                                                                                                                                                                                                            $('#txtVideostandard_vid_acc_tag_ids').removeAllTag();
                                                                                                                                                                                                        }
                                                                                                                                                                                                        /*var synchro_time_class = 'short_tag_0';
                                                                                                                                                                                                         if (typeof ($("#synchro_time_class").val()) != "undefined" && $("#synchro_time_class").val() !== null && $("#synchro_time_class").val().length > 0) {
                                                                                                                                                                                                         synchro_time_class = $("#synchro_time_class").val();
                                                                                                                                                                                                         }
                                                                                                                                                                                                         VideoInstance.addMarker($('[name="synchro_time"]').val(), synchro_time_class);
                                                                                                                                                                                                         playPlayer();*/
                                                                                                                                                                                                        observation_notes_array.push(postData);
                                                                                                                                                                                                        var huddle_type = '<?php echo $huddle_type; ?>'
                                                                                                                                                                                                        generate_comment_html(postData);
                                                                                                                                                                                                        $('#comment_comment').css('height', '100px');
                                                                                                                                                                                                        console.log(audio_path);
                                                                                                                                                                                                        if (audio_path == '') {
                                                                                                                                                                                                            $.ajax({
                                                                                                                                                                                                                type: "POST",
                                                                                                                                                                                                                url: home_url + "/huddles/add_video_comments",
                                                                                                                                                                                                                dataType: 'json',
                                                                                                                                                                                                                data: postData,
                                                                                                                                                                                                                success: function (response) {
                                                                                                                                                                                                                    if (huddle_type == 3 || (huddle_type == 2 || '<?php echo $this->Custom->is_enabled_coach_feedback($huddle_id); ?>')) {  //coaching huddle feedback
                                                                                                                                                                                                                        get_inactive_comments();
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                    var internal_id = response.internal_comment_id;
                                                                                                                                                                                                                    $('#normal_comment_' + internal_id).html(response.comments);
                                                                                                                                                                                                                    //get_comment_count($('#videoId').val());

                                                                                                                                                                                                                    var synchro_time_class = 'short_tag_0';
                                                                                                                                                                                                                    if (typeof ($("#synchro_time_class").val()) != "undefined" && $("#synchro_time_class").val() !== null && $("#synchro_time_class").val().length > 0) {
                                                                                                                                                                                                                        synchro_time_class = $("#synchro_time_class").val();
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                    VideoInstance.addMarker($('[name="synchro_time"]').val(), synchro_time_class);

                                                                                                                                                                                                                    var synchro_time_class = 'short_tag_0';
                                                                                                                                                                                                                    if (typeof ($("#synchro_time_class").val()) != "undefined" && $("#synchro_time_class").val() !== null && $("#synchro_time_class").val().length > 0) {
                                                                                                                                                                                                                        synchro_time_class = $("#synchro_time_class").val();
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                    VideoInstance.addMarker($('[name="synchro_time"]').val(), synchro_time_class);

                                                                                                                                                                                                                    getVdoComments();
                                                                                                                                                                                                                },
                                                                                                                                                                                                                error: function (e) {

                                                                                                                                                                                                                }
                                                                                                                                                                                                            });
                                                                                                                                                                                                        } else {
                                                                                                                                                                                                            $.ajax({
                                                                                                                                                                                                                type: "POST",
                                                                                                                                                                                                                url: home_url + "/huddles/add_video_comments/1",
                                                                                                                                                                                                                dataType: 'json',
                                                                                                                                                                                                                data: postData,
                                                                                                                                                                                                                success: function (response) {

                                                                                                                                                                                                                    var internal_id = response.internal_comment_id;
                                                                                                                                                                                                                    $('#normal_comment_' + internal_id).html(response.comments);
                                                                                                                                                                                                                    //get_comment_count($('#videoId').val());
                                                                                                                                                                                                                    getVdoComments();
                                                                                                                                                                                                                },
                                                                                                                                                                                                                error: function (e) {

                                                                                                                                                                                                                }
                                                                                                                                                                                                            });

                                                                                                                                                                                                        }

                                                                                                                                                                                                        function get_inactive_comments() {
                                                                                                                                                                                                            video_id = $('#videoId').val();
                                                                                                                                                                                                            $.ajax({
                                                                                                                                                                                                                type: "GET",
                                                                                                                                                                                                                url: home_url + "/huddles/get_inactive_comments/" + video_id,
                                                                                                                                                                                                                dataType: 'json',
                                                                                                                                                                                                                success: function (response) {
                                                                                                                                                                                                                    if (response.total_comments > 0) {
                                                                                                                                                                                                                        $('#publish-feeback').show();
                                                                                                                                                                                                                        $('#comment-excel').show();
                                                                                                                                                                                                                        $('#comment-acro').show();
                                                                                                                                                                                                                        $('#email_send').show();
                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                        $('#publish-feeback').hide();
                                                                                                                                                                                                                        $('#comment-excel').hide();
                                                                                                                                                                                                                        $('#comment-acro').hide();
                                                                                                                                                                                                                        $('#email_send').hide();
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                },
                                                                                                                                                                                                                error: function (e) {

                                                                                                                                                                                                                }
                                                                                                                                                                                                            });
                                                                                                                                                                                                        }

                                                                                                                                                                                                        function generate_comment_html(postData) {

                                                                                                                                                                                                            var html = '<ul class="comments comments_huddles" id="normal_comment_' + postData.internal_comment_id + '" style="overflow-x: hidden;">';
                                                                                                                                                                                                            html += '<div rel="' + postData.internal_comment_id + '" id="comment_box' + postData.internal_comment_id + '" style="margin-top: 12px;">';

                                                                                                                                                                                                            html += '<li class="synchro"> ';
                                                                                                                                                                                                            html += '<div class="synchro-inner"> ';
                                                                                                                                                                                                            html += '<i></i><span data-time="' + postData.observations_time + '" class="synchro-time">' + postData.observations_time + '</span>';
                                                                                                                                                                                                            html += '</div></li>';

                                                                                                                                                                                                            html += '<li class="comment thread" style="display: block;float: left;">';
                                                                                                                                                                                                            html += $('#comments_user_meta_data').html();

                                                                                                                                                                                                            html += '<div class="comment-body comment more">' + nl2br(postData.observations_comments) + '</div>'
                                                                                                                                                                                                            html += '<div class="comment-footer">';
                                                                                                                                                                                                            html += '<span class="comment-date">Saving comment....</span> <div class="comment-actions" style="width: 150px;">&nbsp;</div>'
                                                                                                                                                                                                            html += ' </div>';


                                                                                                                                                                                                            html += '<div id="docs-standards" class="docs-standards" style="margin-bottom: 20px;display: block;float: left;">';

                                                                                                                                                                                                            if (postData.observations_standards != '') {

                                                                                                                                                                                                                html += '<div id="tagFilterList"><div id="tagFilterContainer" class="btnwraper">';

                                                                                                                                                                                                                var standards_selected = postData.observations_standards.split(',');

                                                                                                                                                                                                                for (var i = 0; i < standards_selected.length; i++) {

                                                                                                                                                                                                                    html += '<button class="btn btn-lilghtgray" style="margin-right: 2px;">' + standards_selected[i] + '</button>';
                                                                                                                                                                                                                }

                                                                                                                                                                                                                html += '</div></div>';

                                                                                                                                                                                                            }

                                                                                                                                                                                                            if (postData.observations_tags != '') {

                                                                                                                                                                                                                html += '<div id="tagFilterList"><div id="tagFilterContainer" class="btnwraper">';

                                                                                                                                                                                                                var standards_selected = postData.observations_tags.split(',');

                                                                                                                                                                                                                for (var i = 0; i < standards_selected.length; i++) {

                                                                                                                                                                                                                    html += '<button class="btn btn-lilghtgray" style="margin-right: 2px;">' + standards_selected[i] + '</button>';
                                                                                                                                                                                                                }

                                                                                                                                                                                                                html += '</div></div>';

                                                                                                                                                                                                            }

                                                                                                                                                                                                            html += '</div>';


                                                                                                                                                                                                            html += '</li>';

                                                                                                                                                                                                            html += '</div></ul>';

                                                                                                                                                                                                            $('#vidCommentsNew').prepend(html);

                                                                                                                                                                                                        }

                                                                                                                                                                                                        $('#add_audio_comments').val('');
                                                                                                                                                                                                        $('#record').css('display', 'inline');
                                                                                                                                                                                                        $('#play').css('display', 'none');

                                                                                                                                                                                                    }

                                                                                                                                                                                                    $(document).ready(function () {
                                                                                                                                                                                                        $('#clearTagsButton').click(function () {
                                                                                                                                                                                                            $('#txtSearchTags').val('');
                                                                                                                                                                                                            $('#clearTagsButton').css('display', 'none');
                                                                                                                                                                                                            $('input#txtSearchTags').quicksearch('ul#expList_vid li', {
                                                                                                                                                                                                                noResults: 'li#noresults'
                                                                                                                                                                                                            });
                                                                                                                                                                                                        });
                                                                                                                                                                                                        $('#comment_comment').click(function () {
                                                                                                                                                                                                            $(this).removeAttr('placeholder');
                                                                                                                                                                                                        });
                                                                                                                                                                                                        $("#inline-crop-panel").fancybox({
                                                                                                                                                                                                            width: '70%',
                                                                                                                                                                                                            height: '70%',
                                                                                                                                                                                                            type: "iframe",
                                                                                                                                                                                                            closeClick: false,
                                                                                                                                                                                                            helpers: {
                                                                                                                                                                                                                title: null,
                                                                                                                                                                                                                overlay: {
                                                                                                                                                                                                                    closeClick: false
                                                                                                                                                                                                                }
                                                                                                                                                                                                            }
                                                                                                                                                                                                        });

                                                                                                                                                                                                        $("#inline-crop-panel").click(function () {
                                                                                                                                                                                                            pausePlayer();
                                                                                                                                                                                                        });

<?php if (isset($_REQUEST['open_trim']) && $_REQUEST['open_trim'] == '1'): ?>
                                                                                                                                                                                                            pausePlayer();
                                                                                                                                                                                                            $("#inline-crop-panel").trigger('click');
<?php endif; ?>

                                                                                                                                                                                                        $('.btn-trim-video').click(function () {
                                                                                                                                                                                                            $(this).next().submit();
                                                                                                                                                                                                        });
                                                                                                                                                                                                        $('.default_tag').on('click', function (e) {
                                                                                                                                                                                                            e.preventDefault();
                                                                                                                                                                                                            var posid = '';
                                                                                                                                                                                                            tag_name = $(this).text();
                                                                                                                                                                                                            tag_name.replace(/\s/g, "");

                                                                                                                                                                                                            posid = $(this).attr('position_id');
                                                                                                                                                                                                            //$( ".default_tag" ).addClass(defaulttagsclasses(0));
                                                                                                                                                                                                            //var numItems = $('.yourclass').length;
                                                                                                                                                                                                            $('.default_tag').each(function (index) {
                                                                                                                                                                                                                var tag_name1 = $(this).text();
                                                                                                                                                                                                                tag_name1.replace(/\s/g, "");
                                                                                                                                                                                                                $('#txtVideoTags').removeTag(tag_name1);
                                                                                                                                                                                                                if ($(this).hasClass(defaulttagsclasses(index))) {
                                                                                                                                                                                                                    $('#txtVideoTags').removeTag(tag_name1);
                                                                                                                                                                                                                    $(this).removeClass(defaulttagsclasses(index) + 'bg');
                                                                                                                                                                                                                }
                                                                                                                                                                                                            });
                                                                                                                                                                                                            $('.default_tag').each(function (index) {
                                                                                                                                                                                                                if ($(this).attr('position_id') != posid) {
                                                                                                                                                                                                                    $(this).removeClass(defaulttagsclasses($(this).attr('position_id')));
                                                                                                                                                                                                                    $(this).addClass(defaulttagsclasses(0));

                                                                                                                                                                                                                }
                                                                                                                                                                                                            });

                                                                                                                                                                                                            if ($(this).hasClass(defaulttagsclasses(posid))) {
                                                                                                                                                                                                                $('#txtVideoTags').removeTag(tag_name);
                                                                                                                                                                                                                $(this).addClass(defaulttagsclasses(0));
                                                                                                                                                                                                                $(this).removeClass(defaulttagsclasses(posid));
                                                                                                                                                                                                                $(this).attr('status_flag', '0');
                                                                                                                                                                                                                $('#comment_comment').attr("placeholder", "Add a comment");
                                                                                                                                                                                                                $('#synchro_time_class').val('');
                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                $('#txtVideoTags').addTag(tag_name);
                                                                                                                                                                                                                $(this).addClass(defaulttagsclasses(posid));
                                                                                                                                                                                                                $(this).removeClass(defaulttagsclasses(0));
                                                                                                                                                                                                                $(this).attr('status_flag', '1')
                                                                                                                                                                                                                $('#comment_comment').focus();
                                                                                                                                                                                                                //var spt=$(this).text().split(" ");
                                                                                                                                                                                                                var spt = $(this).text().slice(2);
                                                                                                                                                                                                                $('#comment_comment').attr("placeholder", "Add " + spt);
                                                                                                                                                                                                                $('#synchro_time_class').val('short_tag_' + posid);
                                                                                                                                                                                                            }
                                                                                                                                                                                                        });
                                                                                                                                                                                                        $('.default_rating').on('click', function (e) {
                                                                                                                                                                                                            e.preventDefault();
                                                                                                                                                                                                            var posid = '';
                                                                                                                                                                                                            tag_name = $(this).text();
                                                                                                                                                                                                            tag_name.replace(/\s/g, "");
                                                                                                                                                                                                            tag_name_mod = tag_name.replace('# ', '');

                                                                                                                                                                                                            posid = $(this).attr('position_id');
                                                                                                                                                                                                            $('.default_rating').each(function (index) {
                                                                                                                                                                                                                var tag_name1 = $(this).text();
                                                                                                                                                                                                                tag_name1.replace(/\s/g, "");
                                                                                                                                                                                                                $('#txtVideoTags').removeTag(tag_name1);
                                                                                                                                                                                                                if ($(this).hasClass(defaulttagsclasses(index))) {
                                                                                                                                                                                                                    $('#txtVideoTags').removeTag(tag_name1);
                                                                                                                                                                                                                    $(this).removeClass(defaulttagsclasses(index) + 'bg');
                                                                                                                                                                                                                }
                                                                                                                                                                                                            });
                                                                                                                                                                                                            $('.default_rating').each(function (index) {
                                                                                                                                                                                                                if ($(this).attr('position_id') != posid) {
                                                                                                                                                                                                                    $(this).removeClass(defaulttagsclasses($(this).attr('position_id')));
                                                                                                                                                                                                                    $(this).removeClass('rating_black_active');
                                                                                                                                                                                                                    $(this).addClass('rating_black');

                                                                                                                                                                                                                    $(this).addClass(defaulttagsclasses(0));

                                                                                                                                                                                                                }
                                                                                                                                                                                                            });

                                                                                                                                                                                                            if ($(this).hasClass(defaulttagsclasses(posid))) {
                                                                                                                                                                                                                $('#txtVideoTags').removeTag(tag_name);
                                                                                                                                                                                                                $(this).addClass(defaulttagsclasses(0));
                                                                                                                                                                                                                $(this).removeClass(defaulttagsclasses(posid));
                                                                                                                                                                                                                $(this).removeClass('rating_black_active');
                                                                                                                                                                                                                $(this).addClass('rating_black');
                                                                                                                                                                                                                $(this).attr('status_flag', '0');
                                                                                                                                                                                                                $('#comment_comment').attr("placeholder", "Add a comment");
                                                                                                                                                                                                                $('#synchro_time_class').val('');
                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                //$('#txtVideoTags').addTag(tag_name);
                                                                                                                                                                                                                $(this).addClass(defaulttagsclasses(posid));
                                                                                                                                                                                                                $(this).removeClass(defaulttagsclasses(0));
                                                                                                                                                                                                                $(this).attr('status_flag', '1');
                                                                                                                                                                                                                $(this).addClass('rating_black_active');
                                                                                                                                                                                                                $(this).removeClass('rating_black');
                                                                                                                                                                                                                //$('#comment_comment').focus();
                                                                                                                                                                                                                //var spt=$(this).text().split(" ");
                                                                                                                                                                                                                //var spt = $(this).text().slice(2);
                                                                                                                                                                                                                //$('#comment_comment').attr("placeholder", "Add " + spt);
                                                                                                                                                                                                                $('#synchro_time_class_tags').val(tag_name_mod);
                                                                                                                                                                                                                $('#synchro_time_class').val('short_tag_' + posid);
                                                                                                                                                                                                            }
                                                                                                                                                                                                        });
                                                                                                                                                                                                        /*document.getElementById("txtVideostandard_vid_tagsinput").addEventListener("click", ActivateFramework);

                                                                                                                                                                                                         function ActivateFramework() {
                                                                                                                                                                                                         $('#frameWorkss').trigger('click');
                                                                                                                                                                                                         }*/

                                                                                                                                                                                                    });

                                                                                                                                                                                                    function defaulttagsclasses(posid) {
                                                                                                                                                                                                        var class_name = 'tags_qucls';
                                                                                                                                                                                                        if (posid == 1) {
                                                                                                                                                                                                            class_name = 'tags_quclsbg';
                                                                                                                                                                                                        } else if (posid == 2) {
                                                                                                                                                                                                            class_name = 'tags_sugclsbg';
                                                                                                                                                                                                        } else if (posid == 3) {
                                                                                                                                                                                                            class_name = 'tags_notesclsbg';
                                                                                                                                                                                                        } else if (posid == 4) {
                                                                                                                                                                                                            class_name = 'tags_strangthclsbg';
                                                                                                                                                                                                        } else if (posid == 5) {
                                                                                                                                                                                                            class_name = 'tags_strangthclsbg';
                                                                                                                                                                                                        }
                                                                                                                                                                                                        return class_name;
                                                                                                                                                                                                    }

                                                                                                                                                                                                    function defaulttagsclasses1(posid) {

                                                                                                                                                                                                        var class_name = 'tags_qucls';
                                                                                                                                                                                                        if (posid == 1) {
                                                                                                                                                                                                            class_name = 'tags_qucls';
                                                                                                                                                                                                        } else if (posid == 2) {
                                                                                                                                                                                                            class_name = 'tags_sugcls';
                                                                                                                                                                                                        } else if (posid == 3) {
                                                                                                                                                                                                            class_name = 'tags_notescls';
                                                                                                                                                                                                        } else if (posid == 4) {
                                                                                                                                                                                                            class_name = 'tags_strangthcls';
                                                                                                                                                                                                        } else if (posid == 5) {
                                                                                                                                                                                                            class_name = 'tags_strangthcls';
                                                                                                                                                                                                        }
                                                                                                                                                                                                        return class_name;
                                                                                                                                                                                                    }
</script>

<script type="text/javascript">
                                                                                                                                                                                                    $('#copy-huddle-btn-library').on('click', function (e) {
                                                                                                                                                                                                        e.preventDefault();
                                                                                                                                                                                                        count = 0;
                                                                                                                                                                                                        var video_lib_selected = false;
                                                                                                                                                                                                        $.each($('.copyFiles_checkbox'), function (index, value) {

                                                                                                                                                                                                            if ($(this).prop('checked') == true) {
                                                                                                                                                                                                                count++;
                                                                                                                                                                                                            }

                                                                                                                                                                                                        });
                                                                                                                                                                                                        if (count == 0) {
                                                                                                                                                                                                            alert('Please Select at least any Option.');
                                                                                                                                                                                                            return false;
                                                                                                                                                                                                        }
                                                                                                                                                                                                        $('#copy-huddle-btn-library').val('Copying...');
                                                                                                                                                                                                        $.ajax({
                                                                                                                                                                                                            url: home_url + "/Huddles/copy",
                                                                                                                                                                                                            data: $('#huddle-copy-form1').serialize(),
                                                                                                                                                                                                            type: 'POST',
                                                                                                                                                                                                            dataType: 'json',
                                                                                                                                                                                                            success: function (response) {
                                                                                                                                                                                                                $('#copy-huddle-btn-library').val('Copy');
                                                                                                                                                                                                                $('#notification').css('display', 'block');
                                                                                                                                                                                                                if (response.status == true) {
                                                                                                                                                                                                                    $('#notification').html('<div class="message success" style="cursor:pointer">' + response.message + '</div>');
                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                    $('#notification').html('<div class="message error"  style="cursor:pointer">' + response.message + '</div>');
                                                                                                                                                                                                                }

                                                                                                                                                                                                                $('#moveFiles').modal('hide');
                                                                                                                                                                                                            },
                                                                                                                                                                                                            error: function () {
                                                                                                                                                                                                                alert("Network Error Occured");
                                                                                                                                                                                                            }
                                                                                                                                                                                                        });
                                                                                                                                                                                                    });
                                                                                                                                                                                                    jQuery.expr[':'].Contains = function (a, i, m) {
                                                                                                                                                                                                        return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
                                                                                                                                                                                                    };

                                                                                                                                                                                                    function listFilter(header, list, btncross, inputfilter, srchPHolder, emptySrch) {
                                                                                                                                                                                                        var form = $("<div>").attr({
                                                                                                                                                                                                            "class": "filterform"
                                                                                                                                                                                                        }),
                                                                                                                                                                                                                input = $("<input id='" + inputfilter + "'  placeholder='" + srchPHolder + "'><div  id='" + btncross + "'  type='text' style=' width: 10px;  position: absolute; right: 40px; top: 161px; display:none;cursor: pointer; '>X</div>").attr({
                                                                                                                                                                                                            "class": "filterinput",
                                                                                                                                                                                                            "type": "text"
                                                                                                                                                                                                        });
                                                                                                                                                                                                        $(form).append(input).appendTo(header);
                                                                                                                                                                                                        $('.filterform input').css({
                                                                                                                                                                                                            'width': '100%',
                                                                                                                                                                                                            'display': 'block'
                                                                                                                                                                                                        });
                                                                                                                                                                                                        $(input).change(function () {
                                                                                                                                                                                                            var filter = $(this).val();
                                                                                                                                                                                                            $('#liNodataFound').remove();
                                                                                                                                                                                                            if (filter) {
                                                                                                                                                                                                                $search_count = $(list).find("a:Contains(" + filter + ")").length;
                                                                                                                                                                                                                if ($search_count > 4) {
                                                                                                                                                                                                                    $('div.thumb').css('top', '0px');
                                                                                                                                                                                                                    $('div.overview').css('top', '0px');
                                                                                                                                                                                                                    $('.scrollbar').css('display', 'block');
                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                    $('div.thumb').css('top', '0px');
                                                                                                                                                                                                                    $('div.overview').css('top', '0px');
                                                                                                                                                                                                                    $('.scrollbar').css('display', 'none');
                                                                                                                                                                                                                }
                                                                                                                                                                                                                $(list).find("a:not(:Contains(" + filter + "))").parent().parent().slideUp(400, function () {
                                                                                                                                                                                                                    //$('.widget-scrollable2').tinyscrollbar_update();
                                                                                                                                                                                                                });
                                                                                                                                                                                                                $(list).find("a:Contains(" + filter + ")").parent().parent().slideDown(400, function () {
                                                                                                                                                                                                                    //$('.widget-scrollable2').tinyscrollbar_update();
                                                                                                                                                                                                                });
                                                                                                                                                                                                                jQuery("#" + btncross).css('display', 'block');
                                                                                                                                                                                                                if ($(list).find("a:Contains(" + filter + ")").length == 0) {
                                                                                                                                                                                                                    $('#liNodataFound' + btncross).remove();
                                                                                                                                                                                                                    $(list).append('<li id="liNodataFound' + btncross + '">' + emptySrch + '</li>');
                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                    $('#liNodataFound' + btncross).remove();
                                                                                                                                                                                                                }

                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                jQuery("#" + btncross).css('display', 'none');
                                                                                                                                                                                                                $('.scrollbar').css('display', 'block');
                                                                                                                                                                                                                $(list).find("li").slideDown(400, function () {
                                                                                                                                                                                                                    //$('.widget-scrollable2').tinyscrollbar_update();
                                                                                                                                                                                                                });

                                                                                                                                                                                                            }
                                                                                                                                                                                                            return false;
                                                                                                                                                                                                        }).keyup(function () {
                                                                                                                                                                                                            $(this).change();
                                                                                                                                                                                                        });

                                                                                                                                                                                                        $("#" + btncross).click(function (e) {
                                                                                                                                                                                                            jQuery("#" + btncross).css('display', 'none');
                                                                                                                                                                                                            $('div.thumb').css('top', '0px');
                                                                                                                                                                                                            $('div.overview').css('top', '0px');
                                                                                                                                                                                                            $('.scrollbar').css('display', 'block');
                                                                                                                                                                                                            jQuery('#' + inputfilter).val('');
                                                                                                                                                                                                            $('#liNodataFound' + btncross).remove();
                                                                                                                                                                                                            $(list).find("li").slideDown(400, function () {
                                                                                                                                                                                                                //$('.widget-scrollable2').tinyscrollbar_update();
                                                                                                                                                                                                            });
                                                                                                                                                                                                        })
                                                                                                                                                                                                    }

                                                                                                                                                                                                    $(document).ready(function () {
                                                                                                                                                                                                        listFilter($("#header-container"), $("#list-containers"), 'cancel-btn', 'input-filter', 'Search Huddles', 'No Huddles match this search. Please try again.');
                                                                                                                                                                                                        listFilter($("#header-container-accounts"), $("#list-containers-accounts"), 'cancel-btn2', 'input-filter1', 'Search Account(s)', 'No Account(s) match this search.Please try again.');
                                                                                                                                                                                                    });
</script>

<script>
                                                                                                                                                                                                    $(document).ready(function () {
                                                                                                                                                                                                        $("#comment_comment").keydown(function (e) {
                                                                                                                                                                                                            var str_length = $("#comment_comment").val().trim();
                                                                                                                                                                                                            if (e.which == 13 && $("input#press_enter_to_send").prop('checked') == true) {
                                                                                                                                                                                                                e.preventDefault();
                                                                                                                                                                                                                //document.getElementById("add-notes1").click();
                                                                                                                                                                                                                sbtfrm();
                                                                                                                                                                                                            }
                                                                                                                                                                                                        });
                                                                                                                                                                                                    });
</script>
<script type="text/javascript">
                                                                                                                                                                                                    $(window).bind("load", function () {
                                                                                                                                                                                                        $("#add-notes1").removeAttr('disabled');
                                                                                                                                                                                                    });

                                                                                                                                                                                                    $('.copy').click(function () {
                                                                                                                                                                                                        $.each($('.copyFiles_checkbox'), function (index, value) {

                                                                                                                                                                                                            $(this).closest('label').removeClass('active');
                                                                                                                                                                                                            $(this).prop('checked', false);


                                                                                                                                                                                                        });
                                                                                                                                                                                                    });

                                                                                                                                                                                                    $('#pause_while_type').click(function () {
                                                                                                                                                                                                        var value = $("#pause_while_type").is(':checked') ? 1 : 0;
                                                                                                                                                                                                        var user_id = "<?php echo $user_id; ?>";
                                                                                                                                                                                                        var account_id = "<?php echo $account_id; ?>";

                                                                                                                                                                                                        $.ajax({
                                                                                                                                                                                                            type: 'POST',
                                                                                                                                                                                                            url: home_url + '/Huddles/type_pause/',
                                                                                                                                                                                                            data: {
                                                                                                                                                                                                                value: value,
                                                                                                                                                                                                                user_id: user_id,
                                                                                                                                                                                                                account_id: account_id
                                                                                                                                                                                                            },
                                                                                                                                                                                                            success: function (res) {
                                                                                                                                                                                                            },
                                                                                                                                                                                                            errors: function (response) {

                                                                                                                                                                                                            }
                                                                                                                                                                                                        });


                                                                                                                                                                                                    });

                                                                                                                                                                                                    $('#load_more_videos').click(function () {

                                                                                                                                                                                                        $('#load_more_videos').hide();
                                                                                                                                                                                                        $('#loading_gif').show();

                                                                                                                                                                                                    });

                                                                                                                                                                                                    function hide_sidebar() {
                                                                                                                                                                                                        $(".appendix-content").delay(500).fadeOut(500);
                                                                                                                                                                                                        // $("#collab_help").delay(500).fadeOut(500);


                                                                                                                                                                                                    }

                                                                                                                                                                                                    function skip(value) {
                                                                                                                                                                                                        var video = videojs('example_video_<?php echo $videoDetail['
            Document ']['
            id '] ?>');
                                                                                                                                                                                                        var current_time = video.currentTime();
                                                                                                                                                                                                        video.currentTime(current_time + value);
                                                                                                                                                                                                    }

                                                                                                                                                                                                    $('input.check_class').on('change', function () {


                                                                                                                                                                                                        //     $('input.check_class').not(this).prop('checked', false);

                                                                                                                                                                                                        $(this).each(function () {
                                                                                                                                                                                                            //       var sThisVal = (this.checked ? $(this).val() : "");
                                                                                                                                                                                                            var tag_code = $(this).attr('st_code');
                                                                                                                                                                                                            var account_tag_id = $(this).attr('account_tag_id');
                                                                                                                                                                                                            var tag_value = '';
                                                                                                                                                                                                            var tag_array = {};
                                                                                                                                                                                                            var tag_name = $(this).attr('st_name');
                                                                                                                                                                                                            tag_array = tag_name.split(" ");
                                                                                                                                                                                                            if ($(this).is(':checked')) {
                                                                                                                                                                                                                tag_array = tag_name.split(" ");
                                                                                                                                                                                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                                                                                                                                                                                $('#txtVideostandard_vid').addTag(tag_value);
                                                                                                                                                                                                                $('#txtVideostandard_vid_acc_tag_ids').addTag(account_tag_id);
                                                                                                                                                                                                                //$('#txtVideostandard_vid_tag').remove();
                                                                                                                                                                                                                //$('#txtVideostandard_vid_tag').hide();
                                                                                                                                                                                                                if ($('input[name="name1"]:checked').length > 0) {
                                                                                                                                                                                                                    $('#txtVideostandard_vid_tag').hide();
                                                                                                                                                                                                                }
                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                                                                                                                                                                                $('#txtVideostandard_vid').removeTag(tag_value);
                                                                                                                                                                                                                $('#txtVideostandard_vid_acc_tag_ids').removeTag(account_tag_id);
                                                                                                                                                                                                                //    $('#txtVideostandard_vid_tag').show();

                                                                                                                                                                                                            }
                                                                                                                                                                                                        });


                                                                                                                                                                                                    });
</script>
<!--<script src="<?php //echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'js/audio_src/src/recorder.js');                                                                                                                                                                                                                      ?>"></script>
<script src="<?php //echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'js/audio_src/src/Fr.voice.js');                                                                                                                                                                                                                      ?>"></script>
<script src="<?php //echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'js/audio_src/js/app.js');                                                                                                                                                                                                                      ?>"></script>-->

<script>
                                                                                                                                                                                                    function update_title_grid($document_id) {
                                                                                                                                                                                                        var $value = '#input-field-' + $document_id;
                                                                                                                                                                                                        var $video_title = "#videos-title-" + $document_id;
                                                                                                                                                                                                        var $title_block = "#input-title-" + $document_id;
                                                                                                                                                                                                        var $newText = $($value).val();
                                                                                                                                                                                                        if ($newText == '') {
                                                                                                                                                                                                            $($value).css('border', '1px red solid');
                                                                                                                                                                                                            return false;
                                                                                                                                                                                                        }
                                                                                                                                                                                                        var $nexText = $.trim($newText);
                                                                                                                                                                                                        $.ajax({
                                                                                                                                                                                                            url: home_url + "/MyFiles/editTitle",
                                                                                                                                                                                                            data: {
                                                                                                                                                                                                                title: $nexText,
                                                                                                                                                                                                                document_id: $document_id
                                                                                                                                                                                                            },
                                                                                                                                                                                                            type: 'POST',
                                                                                                                                                                                                            success: function (response) {
                                                                                                                                                                                                                var response_edit = response;
                                                                                                                                                                                                                if (response_edit.length > 35) {
                                                                                                                                                                                                                    response_edit = response_edit.substr(0, 35) + '...';
                                                                                                                                                                                                                }
                                                                                                                                                                                                                $($video_title).attr('data-title', response);
                                                                                                                                                                                                                $($video_title).html('<div class="videos-list__item-title"><a class="wrap wrap2">' + response_edit + '</a></div>');
                                                                                                                                                                                                                $($value).val("");
                                                                                                                                                                                                                $($title_block).css('display', 'none');
                                                                                                                                                                                                                $($video_title).css('display', 'block');
                                                                                                                                                                                                            },
                                                                                                                                                                                                            error: function () {
                                                                                                                                                                                                                alert("Network Error Occured");
                                                                                                                                                                                                            }
                                                                                                                                                                                                        });
                                                                                                                                                                                                    }

                                                                                                                                                                                                    function cancel($document_id) {
                                                                                                                                                                                                        var $video_title = "#videos-title-" + $document_id;
                                                                                                                                                                                                        var $title_block = "#input-title-" + $document_id;
                                                                                                                                                                                                        $($title_block).css('display', 'none');
                                                                                                                                                                                                        $($video_title).css('display', 'block');
                                                                                                                                                                                                    }

                                                                                                                                                                                                    $(document).ready(function () {
//                                                                                                                                                                                                        $('#press_enter_to_send').click(function() {
//                                                                                                                                                                                                            var value = $("#press_enter_to_send").is(':checked') ? 1 : 0;
//                                                                                                                                                                                                            var user_id = "<?php echo $user_id; ?>";
//                                                                                                                                                                                                            var account_id = "<?php echo $account_id; ?>";
//                                                                                                                                                                                                            $.ajax({
//                                                                                                                                                                                                                type: 'POST',
//                                                                                                                                                                                                                url: home_url + '/Huddles/press_enter_to_send/',
//                                                                                                                                                                                                                data: {
//                                                                                                                                                                                                                    value: value,
//                                                                                                                                                                                                                    user_id: user_id,
//                                                                                                                                                                                                                    account_id: account_id
//                                                                                                                                                                                                                },
//                                                                                                                                                                                                                success: function(res) {
//                                                                                                                                                                                                                },
//                                                                                                                                                                                                                errors: function(response) {
//
//                                                                                                                                                                                                                }
//                                                                                                                                                                                                            });
                                                                                                                                                                                                        //                                                                                                                                                                                                        });
                                                                                                                                                                                                    });

                                                                                                                                                                                                    function textAreaAdjust(o) {
                                                                                                                                                                                                        o.style.height = "1px";
                                                                                                                                                                                                        o.style.minHeight = "75px";
                                                                                                                                                                                                        o.style.height = (25 + o.scrollHeight) + "px";
                                                                                                                                                                                                        o.style.maxHeight = (5 + o.scrollHeight) + "px";
                                                                                                                                                                                                        //                            alert(o.parentElement.style.minHeight);
//                            o.parentElement.style.height = (5 + parseInt(o.parentElement.style.minHeight)) + "px";
                                                                                                                                                                                                    }

                                                                                                                                                                                                    function nl2br(str, is_xhtml) {
                                                                                                                                                                                                        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
                                                                                                                                                                                                        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
                                                                                                                                                                                                    }

                                                                                                                                                                                                    $(document).ready(function () {
                                                                                                                                                                                                        $('textarea#comment_comment').overlay('destroy');
                                                                                                                                                                                                    });

                                                                                                                                                                                                    $(document).ready(function () {
                                                                                                                                                                                                        $('.copy').click(function (e) {
<?php
if ($huddle_type == 3):
    ?>
                                                                                                                                                                                                                var doc_id = $(this).attr('data-document-id');
                                                                                                                                                                                                                $('.copy-document-ids').val(doc_id);
<?php endif; ?>
                                                                                                                                                                                                        });

                                                                                                                                                                                                    });


                                                                                                                                                                                                    $(document).ready(function () {
                                                                                                                                                                                                        $('.modal').modal('hide');

                                                                                                                                                                                                    });
                                                                                                                                                                                                    $('#auto_scroll_switch').click(function () {
                                                                                                                                                                                                        var value = $(this).is(':checked') ? 1 : 0;
                                                                                                                                                                                                        var user_id = $('#user_id').val();
                                                                                                                                                                                                        var account_id = $('#account_id').val();
                                                                                                                                                                                                        $.ajax({
                                                                                                                                                                                                            type: 'POST',
                                                                                                                                                                                                            url: home_url + '/app/autoscroll_switch',
                                                                                                                                                                                                            data: {
                                                                                                                                                                                                                value: value,
                                                                                                                                                                                                                user_id: user_id,
                                                                                                                                                                                                                account_id: account_id
                                                                                                                                                                                                            },
                                                                                                                                                                                                            success: function (res) {
                                                                                                                                                                                                            },
                                                                                                                                                                                                            errors: function (response) {

                                                                                                                                                                                                            }
                                                                                                                                                                                                        });
                                                                                                                                                                                                    });


</script>
<script>
                                                                                                                                                                                                    function getIcons($file) {
                                                                                                                                                                                                        var $ext = $file.split('.');

                                                                                                                                                                                                        if ($ext[1] == 'doc' || $ext[1] == 'docx') {
                                                                                                                                                                                                            return 'wordpress';
                                                                                                                                                                                                        } else if ($ext[1] == 'xlsx' || $ext[1] == 'xls') {
                                                                                                                                                                                                            return 'x-icon';
                                                                                                                                                                                                        } else if ($ext[1] == 'pdf') {
                                                                                                                                                                                                            return 'acro';
                                                                                                                                                                                                        } else if ($ext[1] == 'pptx' || $ext[1] == 'ppt') {
                                                                                                                                                                                                            return 'ppt';
                                                                                                                                                                                                        } else if ($ext[1] == 'jpeg' || $ext[1] == 'jpg' || $ext[1] == 'png' || $ext[1] == 'gif') {
                                                                                                                                                                                                            return 'image';
                                                                                                                                                                                                        } else {
                                                                                                                                                                                                            return 'file';
                                                                                                                                                                                                        }

                                                                                                                                                                                                    }
</script>

