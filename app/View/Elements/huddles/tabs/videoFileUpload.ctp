<?php
$huddle = $huddle[0];
?>
<div id="videoUploadPopup" style="display:none;width:500px;">

</div>

<div id="docUploadPopup" style="display:none;width:500px;">

</div>



<script type="text/javascript">


    $(document).ready(function() {


        $("#videoUploadPopup").fancybox({
            maxWidth: 800,
            maxHeight: 400,
            type: 'iframe',
            href: home_url + '/Huddles/uploadVideos/<?php echo $huddle['AccountFolder']['account_folder_id']; ?>',
            fitToView: false,
            width: '70%',
            height: '100%',
            autoSize: true,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none'
        });

        $("#docUploadPopup").fancybox({
            maxWidth: 800,
            maxHeight: 400,
            type: 'iframe',
            href: home_url + '/Huddles/uploadDocuments/<?php echo $huddle['AccountFolder']['account_folder_id']; ?>',
            fitToView: false,
            width: '70%',
            height: '100%',
            autoSize: true,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none'
        });

    });



    function OpenVideoUpload() {

        $("#videoUploadPopup").trigger('click');
    }

    function OpenDocumentUpload() {

        $("#docUploadPopup").trigger('click');
    }



</script>