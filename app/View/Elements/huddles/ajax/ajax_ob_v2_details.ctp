<?php

function formatSeconds($seconds) {

    $hours = 0;
    $milliseconds = str_replace("0.", '', $seconds - floor($seconds));

    if ($seconds > 3600) {
        $hours = floor($seconds / 3600);
    }
    $seconds = $seconds % 3600;


    return str_pad($hours, 2, '0', STR_PAD_LEFT)
            . gmdate(':i:s', $seconds)
            . ($milliseconds ? ".$milliseconds" : '');
}

function gettagclass2($comment_tags, $default_tags) {
    $return = -1;
    foreach ($comment_tags as $comment_tag) {
        foreach ($default_tags as $key => $default_tag_value) {
            if (!empty($comment_tag['account_tags']['account_tag_id'])) {
                if ($default_tag_value['AccountTag']['account_tag_id'] == $comment_tag['AccountCommentTag']['account_tag_id']) {
                    $return = $key;
                    break;
                }
            }
        }
    }
    $return = $return + 1;
    return $return;
}
?>
<div class="observation_left">
    <div class="coach_cochee_container" >
        <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/5' ?>" class="back huddle_videos_all">Back to all observations</a>
        <div class="clear"></div>
    </div>
    <div id="name-container">
        <div class="doted_container">
            <div id="ObservationTitle1" name="click to edit title" style="font-weight: bold; width: 410px">
                <?php echo $videos['Document']['original_file_name']; ?>
            </div>
        </div>
        <h2>Observation Date</h2>
        <h3><?php echo date('M-d-Y'); ?></h3>
    </div>
    <?php if ($videos['Document']['published'] == '0' and $videos['Document']['is_processed'] == 4): ?>
        <div id="publish-button-container" style="text-align: center; display:block;">
            <input type="submit" id="publish-observations" data-document-id="<?php echo $videos['Document']['id']; ?>" class="observation_btn" value="Publish Video" />
        </div>
    <?php endif; ?>


    <?php if (is_array($videos) && count($videos) > 0) { ?>

        <?php
        $timeS = array();
        $timecls = array();
        $videoID = $videos['Document']['id'];
        $document_files_array = $this->Custom->get_document_url($videos['Document']);

        if (empty($document_files_array['url'])) {
            $videos['Document']['published'] = 0;
            $document_files_array['url'] = $videos['Document']['original_file_name'];
            $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
        } else {
            $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
        }
//                        echo "<pre>";
//                        print_r($videos);
//                        echo "</pre>";
        $videoFilePath = pathinfo($document_files_array['url']);
        $videoFileName = $videoFilePath['filename'];
        if ($videoCommentsArray) {
            foreach ($videoCommentsArray as $cmt) {
                if (!empty($cmt['Comment']['time'])) {
                    $timeS[] = $cmt['Comment']['time'];
                    if (!empty($cmt['default_tags']) && count($cmt['default_tags']) > 0) {
                        $timecls[] = gettagclass2($cmt['default_tags'], $tags);
                    } else {
                        $timecls[] = 0;
                    }
                }
            }
        }

        $videoID = $videos['Document']['id'];
        $document_files_array = $this->Custom->get_document_url($videos['Document']);
        if (empty($document_files_array['url'])) {
            $videos['Document']['published'] = 0;
            $document_files_array['url'] = $videos['Document']['original_file_name'];
            $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
        } else {
            $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
            @$videos['Document']['duration'] = $document_files_array['duration'];
        }

        $videoFilePath = pathinfo($document_files_array['url']);
        $videoFileName = $videoFilePath['filename'];
        ?>
        <div>

            <input id="txtHuddleID" type="hidden" value="<?php echo $huddle_id ?>"/>
            <div>
                <div id="video_span" style="width:100%;">

                    <div class="associate_video" id="open-dialog" style="height: 322px;">
                        <div class="timer_box2" style=""><?php echo formatSeconds($videos['Document']['current_duration']); ?></div>
                        <div class="videos-list__item-thumb">
                            <style>
                                .associate_video .videos-list__item-thumb video {
                                    width: 100%;
                                }
                            </style>
    <?php
    if ($videos['Document']['published'] == '1' && $videos['Document']['is_processed'] == '4') {
        $thumbnail_image_path = $document_files_array['thumbnail'];
        $video_path = $document_files_array['url'];
        ?>
                                <input type="hidden" id="txtCurrentVideoID" value="<?php echo $video_id; ?>" />
                                <input type="hidden" id="txtCurrentVideoUrl" value="<?php echo $this->base . '/Huddles/view/' . $huddle_id . "/1/$video_id" ?>" />
                                <video id="example_video_<?php echo $videos['Document']['id'] ?>" class="video-js vjs-default-skin" controls preload="metadata" width="100%" height="320" poster="<?php echo $thumbnail_image_path; ?>" data-markers="[<?php echo implode(',', array_reverse($timeS)); ?>]" data-cls="[<?php echo implode(',', array_reverse($timecls)); ?>]">
                                    <source src="<?php echo $video_path; ?>" type='video/mp4'/>
                                </video>

                            <?php } elseif ($videos['Document']['published'] == '0' && $videos['Document']['is_processed'] == '4') { ?>
                                <?php
                                $thumbnail_image_path = $document_files_array['thumbnail'];
                                $video_path = $document_files_array['url'];
                                ?>
                                <input type="hidden" id="txtCurrentVideoID" value="<?php echo $video_id; ?>" />
                                <input type="hidden" id="txtCurrentVideoUrl" value="<?php echo $this->base . '/Huddles/view/' . $huddle_id . "/1/$video_id" ?>" />
                                <video id="example_video_<?php echo $videos['Document']['id'] ?>" class="video-js vjs-default-skin" controls preload="metadata" width="100%" height="320" poster="<?php echo $thumbnail_image_path; ?>" data-markers="[<?php echo implode(',', array_reverse($timeS)); ?>]" data-cls="[<?php echo implode(',', array_reverse($timecls)); ?>]">
                                    <source src="<?php echo $video_path; ?>" type='video/mp4'/>
                                </video>

                            <?php } else { ?>
                                <a id="processing-message-<?php echo $videos['Document']['id']; ?>" href="" title="<?php echo $videos['afd']['title'] ?>">
                                    <div  class="video-unpublished ">
                                        <div class="huddles-unpublished" style="color:#fff;">
                                            <?php if ($videos['Document']['published'] == 0 && $videos['Document']['is_associated'] == '1') { ?>
                                                <p>Video is currently processing. You will receive an email when your video is ready to be viewed.</p>
                                            <?php } else { ?>
                                                <div class="associate_video" id="open-dialog">
                                                    <a href="#" rel="tooltip" data-toggle="modal" data-target="#associate-videos-model" class="move_huddle">
                                                        <img src="/img/associate_video.png" style="margin-top: 50px;"  alt=""/>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div style="clear: both;"></div>
        </div>


    <?php } ?>
    <div style="clear: both;"></div>
    <span class="call_notes_container" id="stop-call-notes"> <?php echo $comments; ?></span>
</div>
<div class="observation_right">
    <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
        <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
            <div class="tab-content tab" id="frameWorksTab" style="visibility: visible;">
                <div class="search-box standard-search" style="position: relative;width:100%;">
                    <input type="button" id="btnSearchTags" class="btn-search" value="">
                    <input class="text-input observation_search_bg" id="txtSearchTags" type="text" value="" placeholder="Search Standards..." style="margin-right: 0px;width: 94% !important;">
                    <span id="clearTagsButton" class="clear-video-input-box" style="display:none;right: 33px;top: 20px;">X</span>
                </div>
                <div id="scrollbar1" style="float: left;">
                    <div class="viewport short" style="overflow:scroll;overflow-x: hidden;">
                        <div class="overview p-left0" style="top: 0px;padding: 0px;">
                            <div id="listContainer">
                                <style type="text/css">
                                    .standardRed {
                                        color: red;display: inline;
                                    }
                                    .standardBlue {
                                        color: blue;display: inline;
                                    }
                                    .standardBlack {
                                        color: #000;display: inline;
                                    }
                                    .standardOrange {
                                        color: orange;display: inline;
                                    }
                                    .standardGreen {
                                        color: green;display: inline;
                                    }
                                    .frame_work_heading{
                                        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
                                        font-weight: 300 !important;
                                        font-size: 20px !important;
                                        margin:10px 0px !important;
                                        cursor:auto !important;
                                    }
                                </style>
                                <ul id="expList1" class="expList1" style="padding-left:0px">

                                    <?php
                                    if (!empty($standardsL2)) {
                                        for ($i = 0; $i < count($standardsL2); $i++) {
                                            ?>
                                            <li class="frame_work_heading" ><?php echo $standardsL2[$i]['AccountTag']['tag_code'] . ' - ' . $standardsL2[$i]['AccountTag']['tag_title']; ?></li>
                                            <?php
                                            foreach ($standards as $standard) {
                                                if ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['parent_account_tag_id']) {
                                                    ?>
                                                    <li class="standard standard-cls" >
                                                        <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                        <?php
                                                        $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                        echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                        ?>
                                                    </li>

                                                <?php
                                                }
                                            }
                                        }
                                    } else {
                                        foreach ($standards as $standard) {
                                            ?>
                                            <li class="standard standard-cls">
                                                <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                <?php
                                                $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                ?>
                                            </li>

            <?php }
        } ?>
                                    <li id="noresults" style="width: 280px;">No standards match your search criteria.</li>
                                </ul>
                                <script type="text/javascript">

                                    $(document).ready(function () {
                                        $('.standard input').on('change', function () {
                                            var tag_code = $(this).attr('st_code');
                                            var tag_value = '';
                                            var tag_array = {};
                                            var tag_name = $(this).attr('st_name');
                                            tag_array = tag_name.split(" ");
                                            if ($(this).is(':checked')) {
                                                tag_array = tag_name.split(" ");
                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                $('#txtVideostandard').addTag(tag_value);
                                                if ($('input[name="name1"]:checked').length > 0) {
                                                    $('#txtVideostandard_tag').hide();
                                                }
                                            } else {
                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                $('#txtVideostandard').removeTag(tag_value);
                                            }
                                        });
                                    });</script>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
    <?php endif; ?>
<?php endif; ?>

</div>
