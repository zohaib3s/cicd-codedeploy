<?php
$users = $this->Session->read('user_current_account');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$loggedInUserRole = '';

$users_shown = array();
$groups_shown = array();
$is_group = '';
?>

<div id="search-huddle-count" style="display: none">(<?php echo $total_observ ?>)</div>
<?php if ($total_observ > 0): ?>
    <table id='observee-table-list-o'>
        <thead>
            <tr>
                <th>Date</th>
                <th>Huddle</th>
                <th>Observee</th>
                <th>Observer(s)</th>
                <th>Location</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($observs as $row): ?>

                <tr>
                    <td><h3><?php echo $row['observation_on']; ?></h3><?php echo $row['observation_at']; ?></td>

                    <td><?php echo $row['name'] ?></td>
                    <td>
                        <?php if (isset($row['observee']['image']) && $row['observee']['image'] != ''): ?>
                            <?php
                            $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $row['observee']['id'] . "/" . $row['observee']['image']);
                            echo $this->Html->image($chimg, array('alt' => $row['observee']['first_name'] . ' ' . $row['observee']['last_name'], 'class' => 'photo', 'data-original-title' => $row['observee']['first_name'] . ' ' . $row['observee']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left'));
                            ?>
                        <?php else: ?>
                            <img width="34" height="34" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" rel="tooltip" class="photo observee-new-img" alt="<?php echo $row['observee']['first_name'] . ' ' . $row['observee']['last_name']; ?>" data-original-title="<?php echo $row['observee']['first_name'] . ' ' . $row['observee']['last_name']; ?>">
                        <?php endif; ?>

                        <b><?php echo $row['observee']['first_name'] . ' ' . $row['observee']['last_name']; ?></b>
                        <?php if ($row['is_private'] != '0'): ?>
                            <img rel="tooltip" data-original-title="Private Observation" src="<?php $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/lock-user.png'); ?>" class="lock-observee-1-a">
                        <?php endif; ?>
                    </td>
                    <td>
                        <?php foreach ($row['observer'] as $observerRow): ?>
                            <?php if (isset($observerRow['image']) && $observerRow['image'] != ''): ?>
                                <?php
                                $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $observerRow['id'] . "/" . $observerRow['image']);
                                echo $this->Html->image($chimg, array('alt' => $observerRow['first_name'] . ' ' . $observerRow['last_name'], 'class' => 'photo', 'data-original-title' => $observerRow['first_name'] . ' ' . $observerRow['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left'));
                                ?>
                            <?php else: ?>
                                <img width="34" height="34" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" rel="tooltip" class="photo observee-new-img" alt="<?php echo $observerRow['first_name'] . ' ' . $observerRow['last_name']; ?>" data-original-title="<?php echo $observerRow['first_name'] . ' ' . $observerRow['last_name']; ?>">
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </td>
                    <td><?php echo $row['location_name'] ?></td>
                    <td>
                        <ul class="observee-view-del-alert">

                            <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['creator_id']) || $user_permissions['UserAccount']['permission_administrator_observation_new_role'] == '1' || $users['roles']['role_id'] != '120')):
                                ?>
                                <li><a href="<?php echo $this->base . '/Observe/edit/' . $row['account_folder_observation_id']; ?>">
                                        <img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/pencil.png'); ?>"  rel='tooltip'></a>
                                </li>
                                <li><a id="pop-up-btn" data-original-title="Manage Alerts" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" style="cursor: pointer" onclick="setObservationID(<?php echo $row['account_folder_observation_id']; ?>)">
                                        <img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/alert-light-g.png'); ?>"></a>
                                </li>
                                <li>
                                    <a href="<?php echo $this->base . '/Observe/delete/' . $row['account_folder_observation_id']; ?>" data-confirm="Are you sure you want to delete this Observation?" data-method="delete">
                                        <img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/delete-light-grey.png'); ?>">
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <div class="text-center">
        <h2 class="heading--info">No Observations </h2>
    </div>
    <a href="#" id="view_browser_notification" style="display: none;" data-toggle="modal" data-target="BrowserMessageModal">browser message</a>
    <div id="BrowserMessageModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom">Upgade Your Browser</h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>

                <p>You're Using an unsupported browser. Please upgrade your browser to view <?php $this->Custom->get_site_settings('site_title') ?> Application</p>
            </div>
        </div>
    </div>
<?php endif; ?>
<div id="addSuperAdminModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/add-user.png'); ?>" />Notification Time</h4>
                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal" id="close-button">&times;</a>
            </div>
            <form accept-charset="UTF-8" action="" enctype="multipart/form-data" method="post" name="admin_form" onsubmit="return false;" style="padding-top:0px;">
                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                <div id="flashMessage2" class="message error" style="display:none;"></div>
                <div class="way-form">
                    <h3>Update Notification Time</h3>

                    <ul class="autoadd autoadd-sfont fmargin-list-left" style="list-style: none;" id="pleasewait">
                        <li>Please Wait...</li>
                    </ul>
                    <ul class="autoadd autoadd-sfont fmargin-list-left" style="list-style: none;display: none" id="displayArea">

                        <li>
                            <label class="icon3-user"><input class="input-dashed" id="_dlg_notify_at" name="notify_at" required placeholder="30" type="number" value="" /></label>
                            <label class="icon3-email">
                                <select class="observee-list-members-b notifications-1-a-time" name="_dlg_notification_type" id="_dlg_notification_type">
                                    <option value="1" >Minute</option>
                                    <option value="2" >Hours</option>
                                    <option value="3" >Days</option>
                                </select>
                            </label>
                        </li>
                    </ul>
                    <input id="observation_id_for_alert" name="user_type" type="hidden" value="" />
                    <button id="btnUpdateNoticifationTime" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green  fmargin-right" type="button">Update Notification Time</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type='text/javascript'>
    $(function () {
        $("#observee-table-list-o").slimtable({
            colSettings: [
                {colNumber: 0, enableSort: false},
                {colNumber: 1, enableSort: false},
                {colNumber: 2, enableSort: false},
                {colNumber: 3, enableSort: false},
                {colNumber: 4, enableSort: false},
                {colNumber: 5, enableSort: false}
            ]
        });
        $('#observee-table-list-o').find('.lock-observee-1-a').parent().addClass('in-active-lock');
        //$("[rel~=tooltip]").tooltip({container: 'body'});
    });
    function setObservationID(observID) {
        $('#pleasewait').show();
        $('#observation_id_for_alert').val('');
        $('#observation_id_for_alert').val(observID);
        if ($('#observation_id_for_alert').val() != '') {
            $.ajax({
                type: 'POST',
                data: {
                    observation_id: $('#observation_id_for_alert').val()
                },
                url: home_url + '/Observe/getObservationNoteTime',
                success: function (response) {
//                    console.log(response);
                    if (response != '') {
                        var results = jQuery.parseJSON(response);
                        $('#_dlg_notify_at').val(results.notify_at);
                        $('#_dlg_notification_type').val(results.notify_at_unit)
                        $('#displayArea').show();
                        $('#pleasewait').hide();
                        if ($('#_dlg_notify_at').hasClass('error')) {
                            $('#_dlg_notify_at').removeClass('error');
                        }
                    }
                }
            });
        }
    }
    $(document).ready(function () {
        $('#btnUpdateNoticifationTime').on('click', function () {
            if ($('#_dlg_notify_at').val() == '') {
                $('#_dlg_notify_at').addClass('error');
            } else {
                $('#_dlg_notify_at').removeClass('error');
                $.ajax({
                    type: 'POST',
                    data: {
                        observation_id: $('#observation_id_for_alert').val(),
                        notify_at: $('#_dlg_notify_at').val(),
                        notification_type: $('#_dlg_notification_type').val()
                    },
                    url: home_url + '/Observe/updateNotificationTime',
                    success: function (response) {
//                    console.log(response);
                        if (response != '' && response == 'true') {
                            $('#addSuperAdminModal').modal('hide');
                            alert('Notification Time Updated');
                        }
                    }
                });
            }
        });
    });



</script>
