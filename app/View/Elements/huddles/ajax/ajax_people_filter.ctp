<div>
    <ul>
        <?php
        if (count($users_record) > 0):
            $user_ids = array();
            $role_admin = array();
            $role_user = array();
            $role_viewer = array();

            foreach ($supperUsers as $supperUser) {

                $user_ids[] = $supperUser['AccountFolderUser']['user_id'];

                if ($supperUser['AccountFolderUser']['role_id'] == '200') {
                    $role_admin[] = $supperUser['AccountFolderUser']['user_id'];
                }
                if ($supperUser['AccountFolderUser']['role_id'] == '210') {
                    $role_user[] = $supperUser['AccountFolderUser']['user_id'];
                }
                if ($supperUser['AccountFolderUser']['role_id'] == '220') {
                    $role_viewer[] = $supperUser['AccountFolderUser']['user_id'];
                }
            }

            foreach ($huddleUsers as $huddleUser) {

                $user_ids[] = $huddleUser['AccountFolderUser']['user_id'];

                if ($huddleUser['AccountFolderUser']['role_id'] == '200') {
                    $role_admin[] = $huddleUser['AccountFolderUser']['user_id'];
                }
                if ($huddleUser['AccountFolderUser']['role_id'] == '210') {
                    $role_user[] = $huddleUser['AccountFolderUser']['user_id'];
                }
                if ($huddleUser['AccountFolderUser']['role_id'] == '220') {
                    $role_viewer[] = $huddleUser['AccountFolderUser']['user_id'];
                }
            }

            foreach ($huddleViewers as $huddleViewer) {
                $user_ids[] = $huddleViewer['AccountFolderUser']['user_id'];

                if ($huddleViewer['AccountFolderUser']['role_id'] == '200') {
                    $role_admin[] = $huddleViewer['AccountFolderUser']['user_id'];
                }
                if ($huddleViewer['AccountFolderUser']['role_id'] == '210') {
                    $role_user[] = $huddleViewer['AccountFolderUser']['user_id'];
                }
                if ($huddleViewer['AccountFolderUser']['role_id'] == '220') {
                    $role_viewer[] = $huddleViewer['AccountFolderUser']['user_id'];
                }
            }
            ?>
            <?php if ($users_record): ?>                                                    
                <?php
                $display = '';
                foreach ($users_record as $row):
                    ?>
                    <?php
                    if (!in_array($row['id'], $role_admin) &&
                            !in_array($row['id'], $role_user) &&
                            !in_array($row['id'], $role_viewer)
                    ) {
                        $role_user[] = $row['id'];
                    }

                    if (isset($huddles['AccountFolder']) && $huddles['AccountFolder']['created_by'] == $row['id']) {
                        $display = 'style="display:none"';
                    } else {
                        $display = '';
                    }
                    ?>
                    <?php if ($row['is_user'] == 'admin'): ?>
                        <li <?php echo $display; ?>>
                            <label for="super_admin_ids_<?php echo $row['id'] ?>"><input type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) !== FALSE) ? 'checked="checked"' : '') ?>> <?php echo $row['first_name'] . " " . $row['last_name'] ?></label>
                            <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">
                            <div class="permissions">
                                <label for="user_role_<?php echo $row['id'] ?>_200"><input type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) !== FALSE)) ? 'checked="checked"' : '') ?>/>Admin
                                </label>
                                <label for="user_role_<?php echo $row['id'] ?>_210"><input type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) !== FALSE)) ? 'checked="checked"' : '') ?>/>Member
                                </label>
                                <label for="user_role_<?php echo $row['id'] ?>_220"><input type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) !== FALSE)) ? 'checked="checked"' : '') ?>/>Viewer
                                </label>
                            </div>
                        </li>
                    <?php elseif ($row['is_user'] == 'member'): ?>
                        <li <?php echo $display; ?>>
                            <label for="super_admin_ids_<?php echo $row['id'] ?>"><input type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" <?php echo ((isset($user_ids) && is_array($user_ids) && count($user_ids) > 0 && in_array($row['id'], $user_ids) !== FALSE) ? 'checked="checked"' : '') ?>> <?php echo $row['first_name'] . " " . $row['last_name'] ?></label>
                            <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">
                            <div class="permissions">
                                <label for="user_role_<?php echo $row['id'] ?>_200"><input type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) !== FALSE)) ? 'checked="checked"' : '') ?>/>Admin
                                </label>
                                <label for="user_role_<?php echo $row['id'] ?>_210"><input type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) !== FALSE)) ? 'checked="checked"' : '') ?>/>Member
                                </label>
                                <label for="user_role_<?php echo $row['id'] ?>_220"><input type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) !== FALSE)) ? 'checked="checked"' : '') ?>/>Viewer
                                </label>
                            </div>
                        </li>
                    <?php elseif ($row['is_user'] == 'group'): ?>
                        <?php
                        foreach ($groups as $group) {
                            $grupIds[] = $group['AccountFolderGroup']['group_id'];

                            if ($group['AccountFolderGroup']['role_id'] == '200') {
                                $role_admin[] = $group['AccountFolderGroup']['group_id'];
                            }
                            if ($group['AccountFolderGroup']['role_id'] == '210') {
                                $role_user[] = $group['AccountFolderGroup']['group_id'];
                            }
                            if ($group['AccountFolderGroup']['role_id'] == '220') {
                                $role_viewer[] = $group['AccountFolderGroup']['group_id'];
                            }
                            if (!in_array($row['id'], $role_admin) &&
                                    !in_array($row['id'], $role_user) &&
                                    !in_array($row['id'], $role_viewer)
                            ) {
                                $role_user[] = $row['id'];
                            }
                        }
                        ?>
                        <li <?php echo $display; ?>>
                            <label><input type="checkbox" value="<?php echo $row['id']; ?>" name="group_ids[]" id="group_ids_" <?php echo ((isset($grupIds) && is_array($grupIds) && count($grupIds) > 0 && in_array($row['id'], $grupIds) !== FALSE) ? 'checked="checked"' : '') ?>><?php echo $row['name']; ?></label>
                            <div class="permissions">
                                <label for="group_role_<?php echo $row['id'] ?>_200"><input type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_200" value="200" <?php echo (((in_array($row['id'], $role_admin) !== FALSE)) ? 'checked="checked"' : '') ?>/>Admin
                                </label>
                                <label for="group_role_<?php echo $row['id'] ?>_210"><input type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_210" value="210" <?php echo (((in_array($row['id'], $role_user) !== FALSE)) ? 'checked="checked"' : '') ?>/>Member
                                </label>
                                <label for="group_role_<?php echo $row['id'] ?>_220"><input type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_220" value="220" <?php echo (((in_array($row['id'], $role_viewer) !== FALSE)) ? 'checked="checked"' : '') ?>/>Viewer
                                </label>
                            </div>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php else: ?>
            <li>
                To Invite users into the account <a data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="" href="#"><span class="">click here</span></a>  
            </li>
        <?php endif; ?>        
    </ul>
</div>