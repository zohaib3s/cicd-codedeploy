<?php
$amazon_base_url = Configure::read('amazon_base_url');
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');


$isFirefoxBrowser = $this->Browser->isFirefox();
?>
<?php if (is_array($observations) && count($observations) > 0): ?>
    <input id="txtobservationCount" type="hidden" value="<?php echo count($observations) ?>">
    <?php foreach ($observations as $row): ?>
        <li class="videos-list__item observee-wrape-1">
            <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['creator_id']) || $user_permissions['UserAccount']['permission_administrator_observation_new_role'] == '1' || $users['roles']['role_id'] != '120')): ?>
                <div class="ac-btn">
                    <a href="<?php echo $this->base . '/Observe/edit/' . $row['account_folder_observation_id']; ?>" data-original-title="Edit" rel="tooltip" data-method="edit" class="btn icon2-edit right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;width: 25px;padding: 0px;  margin-right: 45px !important;"></a>
                </div>
                <div class="ac-btn">
                    <a href="<?php echo $this->base . '/Observe/delete/' . $row['account_folder_observation_id']; ?>" data-confirm="Are you sure you want to delete this Observation?" data-original-title="Delete" rel="tooltip" data-method="delete" class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;width: 25px;padding: 0px;"></a>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>
            <div class="videos-list__item-thumb">
                <a href="<?php echo $this->base.'/Huddles/view/'.$huddle_id.'/5/'.$row['account_folder_id'];?>" title="<?php echo $row['name'] . ' - ' . $row['location_name']; ?>">
                    <?php if ($documentController->countVideos($row['account_folder_id']) > 0): ?>
                        <img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/JOkQsGdQQqDpujd8Pf6URob-Demos-QtA_1429637345_thumb_00001.png'); ?>" height="146" class="thumb_img" alt="">
                        <div class="play-icon"></div>
                    <?php else: ?>
                        <img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/no-vid-observ.png'); ?>" height="146" class="thumb_img" alt="">
                    <?php endif; ?>
                </a>
            </div>
            <div class="videos-list__item-aside observee-wrape-box">
                <div class="videos-list__item-title">
                    <a class="wrap" id="vide-title-1066" href="<?php echo $this->base.'/Huddles/view/'.$huddle_id.'/5/'.$row['account_folder_id'];?>" title="<?php echo $row['name']; ?>"><?php echo $row['name']; ?></a>
                </div>

                <div class="videos-list__item-added"><?php echo $row['observation_on'] . ' (' . $row['observation_at'] . ')'; ?> - <?php echo $row['location_name'] ?>
                    <div style="clear: both;"></div>
                </div>

                <div class="observee-new-video-1 <?php echo $row['is_private'] == '1' ? 'in-active-lock' : ''; ?>">
                    <h3> Observee</h3>
                    <?php if (isset($row['observee']['image']) && $row['observee']['image'] != ''): ?>
                         <?php 
                         $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/".$row['observee']['id']."/".$row['observee']['image']);
                         echo $this->Html->image($chimg, array('alt' => $row['observee']['first_name'] . ' ' . $row['observee']['last_name'], 'class' => 'photo', 'data-original-title' => $row['observee']['first_name'] . ' ' . $row['observee']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                    <?php else: ?>
                        <img align="left" width="34" height="34" rel="tooltip" data-original-title="<?php echo $row['observee']['first_name'] . ' ' . $row['observee']['last_name']; ?>" class="photo observee-new-img" alt="<?php echo $row['observee']['first_name'] . ' ' . $row['observee']['last_name']; ?>" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>"> 
                    <?php endif; ?>
                    <h4><?php echo $row['observee']['first_name'] . ' ' . $row['observee']['last_name']; ?></h4>
                    <b>Admin</b>
                    <?php if ($row['is_private'] == '1'): ?>
                        <img rel="tooltip" data-original-title="Private Observation" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/lock-user.png'); ?>" class="lock-observee-1-a">
                    <?php endif; ?>
                </div>


                <div class="observee-observers-list-1"> <h5 class="smargin-vertical">Observer</h5>
                    <?php foreach ($row['observer'] as $observerRow): ?>
                        <?php if (isset($observerRow['image']) && $observerRow['image'] != ''): ?>
                            <?php 
                            $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/".$observerRow['id']."/".$observerRow['image']);
                            echo $this->Html->image($chimg, array('alt' => $observerRow['first_name'] . ' ' . $observerRow['last_name'], 'class' => 'photo', 'data-original-title' => $observerRow['first_name'] . ' ' . $observerRow['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                        <?php else: ?>
                            <img align="left" width="34" height="34" rel="tooltip" data-original-title="<?php echo $observerRow['first_name'] . ' ' . $observerRow['last_name']; ?>" class="photo observee-new-img" alt="<?php echo $observerRow['first_name'] . ' ' . $observerRow['last_name']; ?>" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>"> 
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
        </li>
    <?php endforeach; ?>
<?php endif; ?>
<?php
if ($hideLoadMore) {
    echo "<script>hideLoadMoreObservations();</script>";
}
?>