<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$loggedInUserRole = '';
$users = $this->Session->read('user_current_account');
$user_id = $users['User']['id'];
$account_id = $users['accounts']['account_id'];

$users_shown = array();
$groups_shown = array();
$is_group = '';
$language_data = $this->Custom->get_page_lang_based_content('huddles/archive_contents');

?> 
<style>
                                    .box.style2{
                                        margin:0px;
                                        border: 1px solid #dddad2;
                                        margin-bottom:-1px;
                                        border-radius:0px;
                                        padding:8px;
                                        min-height:70px;
                                    }
                                    .huddle_list_left{
                                        float:left; 
                                        width:710px;              
                                    }
                                    .huddle_list_right{
                                        float:right; 
                                    }
                                    .row{
                                        margin-left:0px;
                                    }
                                    [data-group] {
                                        margin-top: 6px;
                                    }
                                    .grid_listHeader{
                                            font-size: 15px;
                                            color: #424240;
                                            padding: 0px 30px;
                                            font-weight: 600;
                                            margin-bottom:10px;
                                    }
                                    .grid_listHeader span{
                                        display:inline-block;
                                    }
                                    .header_name{
                                        float:left !important;
                                    }
                                    .header_rightBox{
                                        float:right;
                                    }
                                    .header_video{
                                        margin-left:10px;
                                    }
                                    .header_resources{
                                        margin-left:10px;
                                    }
                                    .header_date{
                                        margin-left:10px;
                                            width: 100px;
                                    }
                                    .header_huddle{
                                        margin-left:10px;
                                    }
                                    .header_participant{
                                        margin-left:10px;
                                    }
                                    .header_action{
                                        margin-left:10px;
                                        width: 58px;
                                    }
                                    
                                </style>
                                <div class="grid_listHeader">
                                    <a style="margin-top: -32px;margin-left: -36px;" href="<?php echo $this->base . '/accounts/account_settings_all/'.$account_id.'/3' ?>" class="back"><?=$language_data['back_to_account_settings'];?></a>
                                    <div class="clear"></div>
                                    <span class="header_name archive_cl"><input type="checkbox" id="select_all"/> <?=$language_data['select_all'];?></span>
                                    <span class="header_name archive_cl"></span>
                                    
                                    <div class="header_rightBox">
                                    <span class="header_video"><?=$language_data['coaching_huddles'];?></span>
                                    <span class="header_resources"><?=$language_data['collaboration_huddles'];?></span>
                                    
                                    <span class="header_huddle"><?=$language_data['folders'];?></span>
                                    <span class="header_date"><?=$language_data['date_created'];?></span>
                                    
                                    
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="row huddles-list huddles-list--list" id="dropdown">
                    <?php if (isset($folders) && !empty($folders)): ?>
                    <ol class="huddle-list-view mainfold" style="margin: 0px;">
                        <?php
                        $grups = array();
                        foreach ($folders as $row):
                            $first_letter = $this->Custom->py_slice(trim($row['AccountFolder']['name']), '0');
                            $grups[$row['AccountFolder']['account_folder_id']] = strtoupper($first_letter);
                            $grups = array_unique($grups);
                            ?>
                        <?php endforeach; ?>
                        <form id="unarchive_form" method="post" enctype="multipart/form-data" action="<?php echo $this->base . '/Huddles/unarchive/2'; ?>" accept-charset="UTF-8">
                        <?php foreach ($folders as $row): ?>
                            <?php
                            $invitedUser = '';
                            $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                            $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                            $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                            ?>

                                            <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                        <li class="folderlist list_folder stackDrop2" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" folder_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>" data-group="<?php echo ($sort == 'name') ? isset($grups[$row['AccountFolder']['account_folder_id']]) ? $grups[$row['AccountFolder']['account_folder_id']] : '' : ''; ?>">
                                            <div class="huddle_list_detail_folder">
                                                <span><input name="selector[]" id="ad_Checkbox_<?php echo $row['AccountFolder']['account_folder_id']; ?>" class="ads_Checkbox" type="checkbox" value="<?php echo $row['AccountFolder']['account_folder_id']; ?>" /></span>
                                                <span class="foldrcl1"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                                                <span class="foldrcl2" style="white-space: nowrap;display: inline-block;text-overflow: ellipsis;overflow: hidden;width: 45%;position: relative;top: 4px;"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="javascript:void(0)">
                                            <?php echo $row['AccountFolder']['name']; ?>
                                                    </a></span>
                                            <?php else: ?>
                                                <li class="huddlist" data-group="<?php echo ($sort == 'name') ? isset($grups[$row['AccountFolder']['account_folder_id']]) ? $grups[$row['AccountFolder']['account_folder_id']] : '' : ''; ?>">
                                                    <span class="foldrcl1"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                                            <?php echo $row['AccountFolder']['name']; ?>
                                                        </a></span>
                <?php endif; ?>               
                                                         <span class="foldrcl3" style="margin-top: 7px;">&nbsp;<?php if($_SESSION['LANG'] == 'es'){ echo $this->Custom->SpanishDate(strtotime($row['AccountFolder']['created_date']),'archive_module');  } else{ echo date('M d, Y', strtotime($row['AccountFolder']['created_date']));} ?></span> 
                                                        <span class="foldrcl4" style="text-align: center;  width: 85px;font-size:14px;margin-top:5px;"><?php echo $this->Custom->count_archive_folders_in_folder($row['AccountFolder']['account_folder_id'],$user_id,$account_id); ?></span>
                                                        
                                          
                                                        <span style="margin-left:62px;" class="foldrcl7"><?php echo $this->Custom->count_archive_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'],$user_id,2); ?></span>
                                                        <span style="margin-left:138px;" class="foldrcl6"><?php echo $this->Custom->count_archive_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'],$user_id,1); ?> </span>
                                          
                                                        <div class="clear"></div>
                                            </div>
                                        </li>
                <?php endforeach; ?>
                                     </form>
                                    </ol>

                    <?php endif; ?>

