<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>  
 <?php
   
   
   $huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');

$isAdminRole  = $huddle_permission == '200';
$isUserRole   = $huddle_permission == '210';
$isViewerRole = $huddle_permission == '220';
$canMaintainFolder = $this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) &&
    $user_permissions['UserAccount']['permission_maintain_folders'] == '1';
$canDownloadFile = $isAdminRole || $isUserRole || $canMaintainFolder;
   
   ?>

<li class="discussion-reply" id="discussion-reply-<?php echo $row['Comment']['id']; ?>">
                <?php
                $profile_image = isset($row['User']['image']) ? $row['User']['image'] : '';
                $profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";
                //if (file_exists($profile_image_real_path) && $profile_image != '') {
                if ($profile_image != '') {
                    //$profile_image = $this->webroot . "img/users/$profile_image";
                    $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $row['User']['user_id'] . "/" . $row['User']['image']);
                } else {
                    //$profile_image = $this->webroot . "img/profile.jpg";
                    $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/profile.jpg');
                }
                ?>
            <div style="margin: 0px;" class="span1">
                <img src="<?php echo $profile_image; ?>" alt="img" height="42px" width="42px" class="circle"/>
            </div>
            <div class="text text-width span10">
                <div class="discussion-editor-row" style="padding-top:0px;">
                    <label class="discussion-username"><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?></label>
                    <div class="discussion-editor-row-content view-controls"><?php echo stripslashes(make_clickable($row['Comment']['comment'])) ?></div>
                    <div class="clearfix"></div>
                    <div class="input-group edit-controls" style="display:none;">
                        <div id="<?php echo "editor" . $row['Comment']['id'] . "-toolbar" ?>" class="editor-toolbar">
                            <a data-wysihtml5-command="bold">bold</a>
                            <a data-wysihtml5-command="italic">italic</a>
                            <a data-wysihtml5-command="insertOrderedList">ol</a>
                            <a data-wysihtml5-command="insertUnorderedList">ul</a>
                            <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                        </div>
                        <textarea id="<?php echo "editor-" . $row['Comment']['id'] ?>"
                                  class="editor-textarea larg-input" cols="48" rows="8"
                                  placeholder="<?php echo $language_based_content['type_message_here_discussion']; ?>"><?php echo $row['Comment']['comment']; ?></textarea>
                        <div style ="display:none;" id="target_div<?php echo $row['Comment']['id']; ?>"><?php echo html_entity_decode(htmlspecialchars($row['Comment']['comment'])); ?></div>
                        <input type="hidden" class="hid_id" id="txtCommentID<?php echo $row['Comment']['id']; ?>" value="<?php echo $row['Comment']['id']; ?>"/>

                    </div>

                    <div class="file-area">
                        <div class="input-group">
                            <ul class="js-attachment-list">
                                    <?php $attachedDocuments = $Document->getByCommentId($row['Comment']['id']); ?>
                                    <?php if (!empty($attachedDocuments)): ?>
                                        <?php foreach($attachedDocuments as $doc): ?>
                                <li>
                                                <?php
                                                $videoFilePath = pathinfo($doc['Document']['url']);
                                                $ext = $videoFilePath['extension'];
                                                if ($canDownloadFile) {
                                                    $href = $this->base . '/Huddles/download/' . $doc['Document']['id'];
                                                } else {
                                                    $href = '';
                                                }
                                                ?>
                                    <a id="url_generator_<?php echo $doc['Document']['id'];?>" href="javascript:void(0);<?php //echo $href;?>" target="_blank">
                                        <img class="icon24" src="<?php echo $this->Custom->getDocTypeIconLink($ext);?>" alt="doc type icon" />
                                        <span style="width:auto;"><?php echo $doc['Document']['original_file_name'];?></span>
                                    </a>
                                    
                                      <span style ="width:auto;">    <a style ="width:auto; cursor: pointer;" download href="<?php echo $href; ?>">
                 <img alt="Download" class="right smargin-right" style="height: 24px;" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>" title="download document"  />
                
                </a> </span>
                                    
                                    
                                    
                                                                                                             <span del_document_id="<?php echo $doc['Document']['id'];?>" style="display:none;
    cursor: pointer;
    font-weight: 400;
    text-decoration: underline;
    color: red;
    font-size: 14px;
   
    position: relative;
    width:auto;" class = "delete_doc<?php echo $row['Comment']['id']; ?>" id="delete_discussion_file_<?php echo $doc['Document']['id'];?>">Remove</span>
    
                  
                       
    
    
                                    <i class="remove edit-controls" style="display:none;">remove</i>
                                    <input type="hidden" name="document_id" value="<?php echo $doc['Document']['id'];?>" />
                                    
                                    
                                
                                    
                                </li>
                                <script>
                                        
                                                         $('#url_generator_<?php echo $doc['Document']['id'];?>').on('click', function (e) {
                     
                     
             $.ajax({
            url: home_url + "/huddles/generate_url/<?php echo $doc['Document']['id'];?>",
            type: 'POST',
            success: function (response) {
                
                filepicker.setKey('A3w6JbXR2RJmKr3kfnbZtz');            

                 filepicker.storeUrl(
                response,
                {filename: 'something.png'},
                function(Blob){
                    
                var result=Blob.url.split('/');
                 window.open(home_url + '/app/view_document/'+result[3]);
                }
                );
          //  console.log(JSON.stringify(Blob.url));
            
            },
            error: function () {
                alert("<?php echo $alert_messages['network_error_occured']; ?>");
            }
        });           
                     
                     
  
        
    });
                                        
                                        
                                </script>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                            </ul>
                            <div class='clearfix'></div>
                            <input type="file" name="data[attachment][]" class="attachment edit-controls" placeholder="attach file" style="display:none;" />
                            <input type="hidden" name="data[remove_attachment]" class="js-remove-attachment" />
                        </div>
                    </div>

                    <div class="controls view-controls span9">
                            <?php
                            $commentDate  = $row['Comment']['created_date'];
                            $commentsDate = $this->Custom->formateDate($commentDate);
                            $isDiscussionReplyOwner = ($row['Comment']['created_by'] == $user_current_account['User']['id']);
                            ?>
                        <div class="posted-date"><?php echo $language_based_content['posted_discussion']; ?> <?php echo $commentsDate; ?></div>
                            <?php if ($isAdminRole || $canMaintainFolder || ($isUserRole && $isDiscussionReplyOwner)): ?>
                        <a href="#desc-container-<?php echo $row['Comment']['id']; ?>" id="edit<?php echo $row['Comment']['id']; ?>" class="edit"><?php echo $language_based_content['edit_discussion']; ?></a>
                        <a href="<?php echo $this->base . '/discussion/delete/' . $huddle_id . "/" . $row['Comment']['id']; ?>"
                           id = "delete_comment-<?php echo $row['Comment']['id']; ?>"
                           class="delete delete-comment"><?php echo $language_based_content['delete_discussion']; ?></a>
                            <?php endif; ?>
                    </div>

                    <div class="edit-controls" style="display:none;">
                        <button class="btn btn-green inline js-update-btn" name="commit"><?php echo $language_based_content['update_discussion']; ?></button>
                        <a id="cancel-edit-discussion<?php echo $row['Comment']['id']; ?>" class="btn btn-transparent js-cancel-btn"><?php echo $language_based_content['cancel_discussion']; ?></a>
                    </div>
                </div>
            </div>
        </li>
        
        
        
                <script>
          
          
           $('#edit<?php echo $row['Comment']['id']; ?>').on('click', function (e) {
        $('.delete_doc<?php echo $row['Comment']['id']; ?>').show();
        
    });
    
    
     $('#cancel-edit-discussion<?php echo $row['Comment']['id']; ?>').on('click', function (e) {
        $('.delete_doc<?php echo $row['Comment']['id']; ?>').hide();
        $('#<?php echo "editor-" . $row['Comment']['id'] ?>').hide();
        
        
    });
    
    
    
     $(document).on("click", ".delete_doc<?php echo $row['Comment']['id']; ?>", function(){
             var r = confirm('<?php echo $language_based_content['are_you_sure_want_to_del_file']; ?>');
                if (r) {
                  $(this).parent('li').remove();
                                    var document_id = $(this).attr("del_document_id");
                                           $.ajax({
                                           url: home_url + "/huddles/delete_document_file/" + document_id,
                                           type: 'POST',
                                           success: function (response) {

                                           },
                                           error: function () {

                                           }
                                       });
                }
                else{
                    return false;
                }
            
                                    
                                    
                                });
          
    
            </script>
        
        
        <?php
        
      

    function make_clickable($ret) {

        $ret = str_replace("&nbsp;", " ", $ret);

        $ret = ' ' . $ret;
        // in testing, using arrays here was found to be faster
        $ret = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_url_clickable_cb', $ret);
        $ret = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', '_make_web_ftp_clickable_cb', $ret);
        $ret = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', '_make_email_clickable_cb', $ret);

        // this one is not in an array because we need it to run last, for cleanup of accidental links within links
        $ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
        $ret = trim($ret);
        return $ret;
    }


        ?>
        <script>
//            $('.edit').on('click', function (e) {
//        $('.delete_doc').show();
//        
//    });
//    
//    $('#cancel-edit-discussion').on('click', function (e) {
//        $('.delete_doc').hide();
//        $('.editor-textarea').hide();
//        
//        
//    });


    $('#edit<?php echo $row['Comment']['id']; ?>').on('click', function (e) {
        var text  = $('#target_div<?php echo $row['Comment']['id']; ?>').html();
        $('#<?php echo "editor-" . $row['Comment']['id'] ?>').val(strip_tags(text.replace(/\s*<br\s*\/?>\s*/g, '\n')));
        $('#<?php echo "editor-" . $row['Comment']['id'] ?>').show();
    });
    
    
     $('.discussion-editor-row .controls a.delete-comment').click(function(event) {
        event.preventDefault();
        if (confirm('<?php echo $language_based_content['do_you_want_to_del_this_document_huddles']; ?>') == true) {
            var selected_del = $(this);
            var comment_id = $(this).attr('id').split('-')[1];
            var huddle_id = $('#huddle_id').val();
            var parent_comment_id = $('.hid_id').eq(0).val();

            $.ajax({
                type: 'POST',
                url: home_url + '/Huddles/deleteComment/' + comment_id,
                success: function() {
                    if (selected_del && selected_del.hasClass('main')) {
                        location.href = home_url + '/Huddles/view/' + huddle_id + '/3';
                    } else {
                     //   location.href = home_url + '/Huddles/view/' + huddle_id + '/3/' + parent_comment_id;
                     $('#discussion-reply-'+comment_id).hide();
                    }
                },
                errors: function(response) {
                    alert(response.contents);
                }
            });
        }
    });
    
    
      $('.discussion-editor-row .controls a.edit').click(function(event) {
        $('.editor-toolbar').hide();
        event.preventDefault();

        if ($(this).attr('id') == 'parentDiscussionEdit') {
            $('#discussion-title-box').hide();
            $('#discussion-title-input').show();
        }

        var row = $(this).closest('div.discussion-editor-row'),
                attachmentList = row.find('.js-attachment-list'),
                attachmentBackup
                ;
        row.find('.js-attachment-backup').remove();

        attachmentBackup = attachmentList.clone();
        attachmentBackup.addClass('js-attachment-backup').removeClass('js-attachment-list').hide();
        attachmentBackup.insertAfter(attachmentList);

        row.find('.view-controls').hide();
        row.find('.edit-controls').show();

        return false;
    });
    
    
    $('.discussion-editor-row button.js-update-btn').click(function() {
        var row = $(this).closest('div.discussion-editor-row'),
                comment_id = row.find('input.hid_id[type=hidden]').val(),
                editor = false
                ;
        for (var i = 0; i < gApp.editorItems.length; i++) {
            var txtElement = $(gApp.editorItems[i].textareaElement);
            if (txtElement.attr('id').replace('editor-', '') == comment_id) {
                editor = gApp.editorItems[i];
                break;
            }
        }
        console.log(comment_id);
        console.log($('#editor_'+comment_id));
//        if (editor)
            updateComment($(this), row, comment_id, $('#editor-'+comment_id).val());
    });
    
    

    function updateComment(button, container, id, comment) {
        console.log(comment);
        var title = $('#comment_title').val(),
                parent_id = $('#discussion_parent_id').val(),
                data
                ;
        if (title == '') {
            alert('<?php echo $alert_messages['title_field_required']; ?>');
            return false;
        }
        comment.replace(/\s*<br\s*\/?>\s*/g, '\n');
        if (window.FormData) {
            data = new FormData();
            data.append('comment', $.trim(comment.replace(/\n/g, "<br />")));
            data.append('title', title);
            data.append('discussion_parent_id', parent_id);
            data.append('remove_attachment', container.find('.js-remove-attachment').val());

            var files = container.find('input[type=file]');
            files.each(function(idx, el) {
                if (el.files.length == 0)
                    return;
                for (var i = 0; i < el.files.length; i++)
                    data.append('attachment[]', el.files[i]);
            });
        } else {
            data = {
                comment: jQuery.trim(comment),
                title: title,
                discussion_parent_id: parent_id
            };
        }
        button.html('<?php echo $language_based_content['updating_discussion']; ?> <span class="loading"><span></span><span></span><span></span></span>');
        $.ajax({
            type: 'POST',
            url: home_url + '/Huddles/editComment/' + id,
            data: data,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(res) {
                if (res.ok) {
                    $('#discussion-title-box').html(title).show();
                    $('#discussion-title-input').hide();

                    updateDocumentList(container, res.documents,id);

                    container.find('.discussion-editor-row-content').html(res.content);
                    container.find('.edit-controls').hide();
                    container.find('.view-controls').show();
                    $('#target_div'+ id ).html(comment);

                    button.html('Update');
                } else {
                    alert('<?php echo $alert_messages['update_discussion_fail']; ?>');
                }
            },
            errors: function(response) {
                alert(response.contents);
            }
        });
    };
    
        function updateDocumentList(container, documents,comment_id) {
        var list = container.find('.js-attachment-list');
        list.html('');
        for (var i = 0; i < documents.length; i++) {
            var li = $('<li></li>');
            var link = home_url + '/Huddles/download/' + documents[i].id;
            var icon = $('<img class="icon24" src="' + getDocTypeIconLink(documents[i].name) + '" />')
            var a = $('<a id="url_generator_'+ documents[i].id +'" href="' + link + '"> </a>');
            var remove = $('<i class="remove edit-controls" style="display:none;">remove</i>');
            var id = $('<input type="hidden" name="document_id" value="' + documents[i].id + '" />');
            var cross_button = $('<span del_document_id="' + documents[i].id + '" style="display:none;cursor: pointer;font-weight: 400;text-decoration: underline;color: red;position: relative;width:auto;" class = "delete_doc'+ comment_id +'" id="delete_discussion_file_' + documents[i].id + '">Remove</span>');
            var download_button = $('<span style="width:auto;"><a style ="width:auto; cursor: pointer;" download href="'+ link +'"><img alt="Download" class="right smargin-right" style="height: 24px;" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download-2.png'); ?>" title="download document"  /></a></span>');
            a.append(icon);
            a.append('<span style="width:auto;">' + documents[i].name + '</span>');
            li.append(a);
            li.append(remove);
            li.append(id);
            li.append(cross_button);
            li.append(download_button);
            
            

            remove.click(removeFile);

            list.append(li);
        }
    };
    
    
     function getDocTypeIconLink(filename) {
        var ext = filename.split('.').pop();
        switch (ext) {
            case 'doc':
            case 'docx':
                $link = 'img/icons/wd48.png';
                break;
            case 'xls':
            case 'xlsx':
                $link = 'img/icons/excel48.png';
                break;
            case 'pdf':
                $link = 'img/icons/pdf48.png';
                break;
            case 'png':
            case 'gif':
            case 'jpg':
            case 'jpeg':
                $link = 'img/icons/jpeg48.png';
                break;
            case 'ppt':
            case 'pptx':
                $link = 'img/icons/ms-powerpoint.png';
                break;
            default:
                $link = 'img/icons/file48.png';
        }
        return home_url + '/' + $link;
    };
    
    
    
    function removeFile() {
        var self = $(this);
        var row = self.closest('.discussion-editor-row');
        var document_id = self.closest('li').find('input[name=document_id]').val();
        var removeAttachedFiles = row.find('.js-remove-attachment');

        // check if document_id input isn't exists
        if (document_id) {
            if (removeAttachedFiles.val().length == 0) {
                removeAttachedFiles.val(document_id);
            } else {
                removeAttachedFiles.val(removeAttachedFiles.val() + ',' + document_id);
            }
        }

        self.parent().remove();
    };
    
    
        $('.discussion-editor-row a.js-cancel-btn').click(function(event) {
        event.preventDefault();
        $('#discussion-title-box').show();
        $('#discussion-title-input').hide();

//        var row = $(this).closest('div.discussion-editor-row'),
//                attachmentBackup = row.find('.js-attachment-backup'),
//                attachmentList = row.find('.js-attachment-list'),
//                removeAttachedFiles = row.find('.js-remove-attachment')
//                ;
//
//        attachmentList.html(attachmentBackup.html());
//        attachmentList.find('i.remove').click(Huddles.removeFile);
//        attachmentBackup.remove();
//
//        removeAttachedFiles.val('');
        var row = $(this).closest('div.discussion-editor-row');
        row.find('.view-controls').show();
        row.find('.edit-controls').hide();

        return false;
    });
    
    function strip_tags(str) {
    str = str.toString();
    return str.replace(/<\/?[^>]+>/gi, '');
}
    
        </script>