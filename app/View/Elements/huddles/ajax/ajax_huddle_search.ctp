<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$loggedInUserRole = '';
$account_id = $user_current_account['accounts']['account_id'];
$user_id = $user_current_account['User']['id'];


$users_shown = array();
$groups_shown = array();
$is_group = '';
?>
<div id="showflashmessage"></div>
<div class="lmargin-bottom" style="margin-left: 0px;">
    <div id="search-huddle-count" style="display: none;"> <?php echo '( ' . $total_huddles . ' )' ?></div>
    <?php if ((isset($video_huddles) && !empty($video_huddles)) || !empty($folders)): ?>

        <?php if (isset($view_mode) && $view_mode == 'grid'): ?>
            <?php if (isset($folders) && !empty($folders)): ?>
                <?php
                foreach ($folders as $row):
                    $invitedUser = '';
                    $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                    $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                    $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                    ?>
                    <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                        <div class="huddle_Mfolder compact">
                            <div class="stackDrop1 col-4sm" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" folder_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">
                                <div class="topsection">
                                    <span class="ficoncls"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                                    <span class="namecls"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                                            <?php echo $row['AccountFolder']['name']; ?>
                                        </a></span>
                                    <span class="optioncls">
                                        <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                            <a  id="editfolder" huddle_id ="<?php echo $row['AccountFolder']['account_folder_id'] ?>" rel="tooltip"  data-original-title="Edit" href="#<?php //echo $this->base . '/Folder/edit/' . $row['AccountFolder']['account_folder_id']           ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                        <?php endif; ?>
                                        <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                            <a rel="tooltip" id="deletefolder" huddle_id_del ="<?php echo $row['AccountFolder']['account_folder_id'] ?>" data-original-title="Delete" data-method="delete"  href="#<?php //echo $this->base . '/Folder/delete/' . $row['AccountFolder']['account_folder_id']           ?>"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                                        <?php endif; ?>
                                        <?php if (isset($folders) && count($folders) > 1 || (isset($total_folders_for_users) && count($total_folders_for_users) > 1)): ?>
                                            <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                <a href="#" rel="tooltip" data-original-title="Move" data-toggle="modal" data-target="#movefolderto" class="move_huddle" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>" fold_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </span>
                                </div><!--top-->
                                <style>
                                    .huddle_date{
                                        font-weight: 100;
                                        color: #333;
                                        margin:0px 0px 8px 0px;
                                    }
                                    .folder_details{
                                        margin: 0px;
                                        padding:0px;
                                        height:150px;
                                    }
                                    .folder_details li{
                                        padding: 14px 0px;
                                        color: #333;
                                        font-weight: 600;
                                        border-bottom: solid 1px #c4c4c4;
                                    }
                                    .folder_details li:last-child{
                                        border-bottom:0px;
                                    }
                                    .huddle_createrName{
                                        color:#858585;
                                        font-size: 14px;
                                        position: absolute;
                                        bottom: 2px;
                                    }
                                    .count_huddle{
                                        float:right;
                                        margin-right: 10px;

                                    }
                                    .col-4sm{
                                        position: relative;
                                    }
                                    .folder_link{
                                        font-weight: normal;
                                    }
                                    .topsection{
                                        height: 30px;
                                    }
                                </style>


                                <div class="huddle_date"><?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])); ?><br></div>

                                <a href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>" class="folder_link">
                                    <ul class="folder_details">
                                        <?php if ($this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 2) > 0) { ?> <li><img src="/app/img/coaching_huddle.png" />  Coaching Huddles<div class="count_huddle"><?php echo $this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 2); ?></div></li> <?php } ?>
                                        <?php if ($this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 1) > 0) { ?> <li><img src="/app/img/Collaboration_huddle.png" />  Collaboration Huddles<div class="count_huddle"><?php echo $this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 1); ?></div></li><?php } ?>
                                        <?php if ($this->Custom->check_if_eval_huddle_active($account_id)) { ?>
                                                <?php if ($this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 3) > 0) { ?> <li><img src="/app/img/evaluation.png" />  Assessment Huddles<div class="count_huddle"><?php echo $this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 3); ?></div></li><?php } ?>
                                            <?php } ?>
                                            <?php if ($this->Custom->count_folders_in_folder($row['AccountFolder']['account_folder_id'], $user_id, $account_id) > 0) { ?><li><img src="/app/img/folder_inside.png" />  Folders<div class="count_huddle"><?php echo $this->Custom->count_folders_in_folder($row['AccountFolder']['account_folder_id'], $user_id, $account_id); ?></div></li></li><?php } ?>
                                    </ul>
                                </a>
                                <div class="huddle_createrName">  <?php
                                    echo 'Created By: ';
                                    echo $this->Custom->user_name_from_id($row['AccountFolder']['created_by']);
                                    ?></div>
                            </div>
                        </div>
                    </div><!---->
                <?php endif; ?>
            <?php endforeach; ?>


        <?php endif; ?>
        <?php if (isset($video_huddles) && !empty($video_huddles)): ?>

            <?php
            foreach ($video_huddles as $row):
//                if ($this->Custom->check_if_eval_huddle_active($account_id) == false) {
//                    if ($row[0]['folderType'] == 'evaluation') {
//                        continue;
//                    }
//                }
                $invitedUser = '';
                $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                ?>

                <div class="col-4sm <?php if (isset($folders) && !empty($folders)): ?><?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>card<?php endif; ?><?php endif; ?>" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">
                    <div class="handle" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">
                        <div class="topsection">
                            <span class="datecls"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                <?php echo $row['AccountFolder']['name']; ?>
                                </a></span>
                            <span class="optioncls">
                <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                    <a rel="tooltip" data-original-title="Edit" href="<?php echo $this->base . '/add_huddle_angular/edit/' . $row['AccountFolder']['account_folder_id'] ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                <?php endif; ?>
                                <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                    <a rel="tooltip" data-method="delete" data-confirm="Are you sure you want to permanently delete this huddle?" href="<?php echo $this->base . '/Huddles/delete/' . $row['AccountFolder']['account_folder_id'] ?>"
                                       data-original-title="Delete"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                <?php endif; ?>
                                   <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                       <?php //if ($this->custom->check_folders_in_account($user_current_account['users_accounts']['account_id'])): ?>
                                       <?php if (isset($folders) && !empty($folders) || (isset($total_folders_for_users) && !empty($total_folders_for_users))): ?>
                                        <a href="#" rel="tooltip" data-original-title="Move" data-toggle="modal" data-target="#movefolderto" class="move_huddle" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                                       <?php endif; ?>
                                <?php endif; ?>
                            </span>

                        </div><!--top-->
                    </div>

                    <a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                        <div class="postdate" style="font-weight: 100;"><?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])); ?></div>
                        <div class="postedfile" style="font-weight: 100;">
                            <span><?php echo $this->Html->image('icons_vdo.png'); ?> <?php echo $row['AccountFolder']['total_videos']; ?></span>
                            <span><?php echo $this->Html->image('icons_fle.png'); ?><?php echo $row['AccountFolder']['total_docs']; ?></span>

                        </div>
                <?php
                $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                $userGroups = $AccountFolderGroup->getHuddleGroups($row['AccountFolder']['account_folder_id']);

                if (isset($huddleGroups[0]['Group']['name']) && $huddleGroups[0]['Group']['name'] != '') {
                    $is_group = TRUE;
                } else {
                    $is_group = FALSE;
                }

                $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $userGroups, $user_current_account['User']['id']);
                ?>
                        <div class="partcls" style="min-height: 160px;">
                        <?php
                        if ($row['AccountFolder']['meta_data_value'] == '2') {
                            $part_height = '';
                            ?>
                                <label>Coach</label>
                                <?php
                            } elseif ($row['AccountFolder']['meta_data_value'] == '3') {
                                $part_height = '';
                                echo "<label>Assessor</label>";
                            } else {
                                if ($is_group) {
                                    $part_height = '';
                                } else {
                                    $part_height = 'height:130px';
                                }
                                ?>
                                <label>Participants in the Huddle</label>
                            <?php } ?>
                            <?php if ($huddleUsers): ?>
                                <div class="widget-scrollable nopadding-left nopadding-top" style="margin-top: 4px !important;margin-bottom: 4px !important;">
                                    <div class="scrollbar" style="height: 44px;">
                                        <div class="track" style="height: 44px;">
                                            <div class="thumb" style="top: 0px; height: 15.3651px;">
                                                <div class="end"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="viewport vshort2" style="<?php echo $part_height; ?>">
                                        <div class="overview" style="top: 0px;">
                    <?php
                    $users_shown = array();
                    $coach_html = '';
                    $coachee_html = '';
                    ?>
                                            <?php if ($row['AccountFolder']['meta_data_value'] == '2') {
                                                ?>
                                                <?php foreach ($huddleUsers as $invitedUsers): ?>
                                                    <?php
                                                    $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image'], $invitedUsers['User']['image']);
                                                    ?>
                                                    <?php if ($invitedUsers['huddle_users']["is_coach"] == '1') { ?>
                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                            <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                            <?php
                                                        else:
                                                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                            $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                            ?>

                                                        <?php endif; ?>
                                                    <?php }elseif ($invitedUsers['huddle_users']["is_mentee"] == '1') { ?>
                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                            <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                            <?php
                                                        else:
                                                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                            $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                            ?>
                                                        <?php endif; ?>
                                                    <?php }else { ?>
                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                        <?php if ($invitedUsers['huddle_users']["role_id"] == '200') { ?>
                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                <?php
                                                            else:
                                                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                ?>
                                                            <?php endif; ?>
                                                        <?php }else { ?>
                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                <?php
                                                            else:
                                                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                ?>
                                                            <?php endif; ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php
                                                    $users_shown[] = $invitedUsers['User']["id"];
                                                endforeach;
                                            }elseif ($row['AccountFolder']['meta_data_value'] == '3') {
                                                foreach ($huddleUsers as $invitedUsers):
                                                    ?>
                                                    <?php
                                                    $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image'], $invitedUsers['User']['image']);
                                                    ?>
                                                    <?php if ($invitedUsers['huddle_users']["is_coach"] == '1') { ?>
                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                            <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                            <?php
                                                        else:
                                                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                            $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                            ?>

                                                        <?php endif; ?>
                                                    <?php }elseif ($invitedUsers['huddle_users']["is_mentee"] == '1') { ?>
                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                            <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                            <?php
                                                        else:
                                                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                            $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                            ?>
                                                        <?php endif; ?>
                                                    <?php }else { ?>
                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                        <?php if ($invitedUsers['huddle_users']["role_id"] == '200') { ?>
                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                <?php
                                                            else:
                                                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                ?>
                                                            <?php endif; ?>
                                                        <?php }else { ?>
                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                <?php
                                                            else:
                                                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                ?>
                                                            <?php endif; ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php
                                                    $users_shown[] = $invitedUsers['User']["id"];
                                                endforeach;
                                            }else {
                                                foreach ($huddleUsers as $invitedUsers):
                                                    ?>
                                                    <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                    <?php
                                                    $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image'], $invitedUsers['User']['image']);
                                                    ?>
                                                    <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                        <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                        <?php
                                                    else:
                                                        $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                        $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                        ?>
                                                    <?php endif; ?>
                                                    <?php
                                                    $users_shown[] = $invitedUsers['User']["id"];
                                                endforeach;
                                            }
                                            echo $coach_html;
                                            if ($coachee_html != '') {
                                                $style = '';
                                                echo '</div></div></div>';
                                                if ($row['AccountFolder']['meta_data_value'] == '2') {
                                                    echo '<label>Coachee</label>';
                                                } elseif ($row['AccountFolder']['meta_data_value'] == '3') {
                                                    if ($this->Custom->check_if_eval_huddle($row['AccountFolder']['account_folder_id'])) {
                                                        if ($this->Custom->check_if_evalutor($row['AccountFolder']['account_folder_id'], $user_id)) {
                                                            echo '<label>Assessed Participant(s)</label>';
                                                            $style = 'display: block';
                                                        } else {
                                                            $style = 'display: none;';
                                                        }
                                                    }
                                                }
                                                ?>
                                                <div class="widget-scrollable nopadding-left nopadding-top" style="margin-top: 4px !important;margin-bottom: 4px !important; <?php echo $style; ?>">
                                                    <div class="scrollbar" style="height: 44px;">
                                                        <div class="track" style="height: 44px;">
                                                            <div class="thumb" style="top: 0px; height: 15.3651px;">
                                                                <div class="end"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="viewport vshort2">
                                                        <div class="overview" style="top: 0px;">
                        <?php
                        echo $coachee_html;
                    }
                    $users_shown = array();
                    ?>
                                                    </div>
                                                </div>
                                            </div>
                <?php else: ?>
                                            <div style="height: 60px;"> No People in the Huddle.</div>
                                        <?php endif; ?>
                                        <?php if (($is_group && $row['AccountFolder']['meta_data_value'] != '3')): // || ($is_group && $row['AccountFolder']['meta_data_value'] == '3' && $this->Custom->check_if_evalutor($row['AccountFolder']['account_folder_id'], $user_id) ) ):  ?>
                                            <label>Groups in huddle</label>
                                            <div class="widget-scrollable nopadding-left nopadding-vertical">
                                                <div class="scrollbar disable" style="height: 50px;">
                                                    <div class="track" style="height: 50px;">
                                                        <div class="thumb" style="top: 0px; height: 50px;">
                                                            <div class="end"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="viewport vshort3">
                                                    <div class="overview">
                    <?php
                    $counter = 0;
                    $groups_shown = array();
                    foreach ($huddleGroups as $grp):
                        ?>
                                                            <?php if (isset($grp['huddle_groups']['group_id']) && in_array($grp['huddle_groups']['group_id'], $groups_shown)) continue; ?>
                                                            <?php
                                                            if ($counter > 0)
                                                                echo ", ";
                                                            echo $grp['Group']['name'];
                                                            ?>
                                                            <?php
                                                            $counter +=1;

                                                            $groups_shown[] = $grp['huddle_groups']['group_id'];

                                                        endforeach;

                                                        $groups_shown = array();
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                <?php endif; ?>

                                    </div>
                                    </a>
                <?php if ($row['AccountFolder']['meta_data_value'] == '2'): ?>
                                        <div style="float: right;font-weight: bold;font-size: 12px;height:0px;    position: relative;bottom: 11px;">Coaching</div>
                                    <?php elseif ($row['AccountFolder']['meta_data_value'] == '3'): ?>
                                        <div style="float: right;font-weight: bold;font-size: 12px;height:0px;     position: relative;bottom: 14px;">Assessment</div>
                                    <?php else: ?>
                                        <div style="float: right;font-weight: bold;font-size: 12px;height:0px;    position: relative;bottom: 14px;">Collaboration </div>
                                    <?php endif; ?>
                                    <div style="clear: both;"></div>
                                </div> <!---->
            <?php endforeach; ?>
                        </div>
                        <?php endif; ?>

                <?php elseif (isset($view_mode) && $view_mode == 'list'): ?>

                    <!----List View---->
                    <style>
                        .box.style2{
                            margin:0px;
                            border: 1px solid #dddad2;
                            margin-bottom:-1px;
                            border-radius:0px;
                            padding:8px;
                            min-height:70px;
                        }
                        .huddle_list_left{
                            float:left;
                            width:710px;
                        }
                        .huddle_list_right{
                            float:right;
                        }
                        .row{
                            margin-left:0px;
                        }
                        [data-group] {
                            margin-top: 6px;
                        }
                        .grid_listHeader{
                            font-size: 15px;
                            color: #424240;
                            padding: 0px 30px;
                            font-weight: 600;
                            margin-bottom:10px;
                        }
                        .grid_listHeader span{
                            display:inline-block;
                        }
                        .header_name{
                            float:left !important;
                        }
                        .header_rightBox{
                            float:right;
                        }
                        .header_video{
                            margin-left:10px;
                        }
                        .header_resources{
                            margin-left:10px;
                        }
                        .header_date{
                            margin-left:10px;
                            width: 100px;
                        }
                        .header_huddle{
                            margin-left:10px;
                        }
                        .header_participant{
                            margin-left:10px;
                        }
                        .header_action{
                            margin-left:20px;
                            width: 58px;
                        }
                    </style>
                    <div class="grid_listHeader">
                        <span class="header_name">Name</span>
                        <div class="header_rightBox">
                            <span class="header_video" style="margin-right:20px;">Type</span>
                            <span class="header_video">Videos</span>
                            <span class="header_resources">Resources</span>
                            <span class="header_date">Date Created</span>
        <?php if (isset($folders) && !empty($folders)): ?>
                                <span class="header_huddle">Huddles</span>
                            <?php endif; ?>
                            <span class="header_participant">Participants</span>
                            <span class="header_action">Action</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="row huddles-list huddles-list--list" style="margin-top: -14px;margin:0 30px 0 15px;" id="dropdown">
        <?php if (isset($folders) && !empty($folders)): ?>
                            <ol class="huddle-list-view mainfold" style="margin-bottom: 0px;">
                            <?php
                            $grups = array();
                            foreach ($folders as $row):
                                $first_letter = $this->Custom->py_slice(trim($row['AccountFolder']['name']), '0');
                                $grups[$row['AccountFolder']['account_folder_id']] = strtoupper($first_letter);
                                $grups = array_unique($grups);
                                ?>
                                <?php endforeach; ?>

                                <?php foreach ($folders as $row): ?>
                                    <?php
                                    $invitedUser = '';
                                    $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                                    $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                                    $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                                    ?>

                                    <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                        <li class="folderlist list_folder stackDrop2" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" folder_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>" data-group="<?php echo ($sort == 'name') ? isset($grups[$row['AccountFolder']['account_folder_id']]) ? $grups[$row['AccountFolder']['account_folder_id']] : '' : ''; ?>">
                                            <div class="huddle_list_detail_folder">
                                                <span class="foldrcl1"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                                                <span class="foldrcl2" style="white-space: nowrap;display: inline-block;text-overflow: ellipsis;overflow: hidden;width: 45%;position: relative;top: 4px;"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                    <?php echo $row['AccountFolder']['name']; ?>
                                                    </a></span>
                                                    <?php else: ?>
                                                <li class="huddlist" data-group="<?php echo ($sort == 'name') ? isset($grups[$row['AccountFolder']['account_folder_id']]) ? $grups[$row['AccountFolder']['account_folder_id']] : '' : ''; ?>">
                                                    <span class="foldrcl1"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                    <?php echo $row['AccountFolder']['name']; ?>
                                                        </a></span>
                                                        <?php endif; ?>
                                                <span class="foldrcl5" style="display:inline-block;min-height:5px;">
                                                <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                        <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                                            <a  id="editfolder" huddle_id ="<?php echo $row['AccountFolder']['account_folder_id'] ?>" rel="tooltip"  data-original-title="Edit" href="#<?php //echo $this->base . '/Folder/edit/' . $row['AccountFolder']['account_folder_id']        ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                        <?php else: ?>
                                                            <a href="<?php echo $this->base . '/add_huddle_angular/edit/' . $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                        <a rel="tooltip nofollow" id="deletefolder" huddle_id_del="<?php echo $row['AccountFolder']['account_folder_id'] ?>" data-original-title="Delete" data-method="delete"  href="#<?php //echo $this->base . '/Folder/delete/' . $row['AccountFolder']['account_folder_id']        ?>"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                                                    <?php endif; ?>
                                                    <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                        <?php if (isset($folders) && count($folders) > 1 || (isset($total_folders_for_users) && count($total_folders_for_users) > 1)): ?>
                                                            <a href="#" class="move_huddle" rel="tooltip" data-original-title="Move" data-toggle="modal" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>" data-target="#movefolderto" fold_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </span>
                                                <span class="foldrcl4" style="width: 85px; text-align: center;">&nbsp;</span>
                                                <span class="foldrcl4" style="text-align: center;  width: 85px;"><?php echo $this->Custom->count_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_current_account['User']['id']); ?></span>
                                                <span class="foldrcl3" style="text-align:center;">&nbsp;<?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])) ?></span>
                <?php if ($row['AccountFolder']['folder_type'] != '5'): ?>
                                                    <span class="foldrcl7"><?php echo $this->Html->image('icons_fle.png'); ?> <i><?php echo $row['AccountFolder']['total_docs']; ?></i></span>
                                                    <span class="foldrcl6"><?php echo $this->Html->image('icons_vdo.png'); ?><i><?php echo $row['AccountFolder']['total_videos']; ?></i> </span>
                <?php endif; ?>
                                                <div class="clear"></div>
                                        </div>
                                    </li>
            <?php endforeach; ?>
                            </ol>

        <?php endif; ?>

                        <?php if (isset($video_huddles) && !empty($video_huddles)): ?>
                            <ol class="huddle-list-view mainfold launchPad2" style="margin-top: -2px;" >
                            <?php
                            $grups = array();
                            foreach ($video_huddles as $row):
//                                    if ($this->Custom->check_if_eval_huddle_active($account_id) == false) {
//                                        if ($row[0]['folderType'] == 'evaluation') {
//                                            continue;
//                                        }
//                                    }
                                $first_letter = $this->Custom->py_slice(trim($row['AccountFolder']['name']), '0');
                                $grups[$row['AccountFolder']['account_folder_id']] = strtoupper($first_letter);
                                $grups = array_unique($grups);
                                ?>
                                <?php endforeach; ?>

                                <?php
                                foreach ($video_huddles as $row):
//                                    if ($this->Custom->check_if_eval_huddle_active($account_id) == false) {
//                                        if ($row[0]['folderType'] == 'evaluation') {
//                                            continue;
//                                        }
//                                    }
                                    $invitedUser = '';
                                    $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                                    $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                                    $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                                    ?>
                                    <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                        <li class="folderlist" data-group="<?php echo ($sort == 'name') ? isset($grups[$row['AccountFolder']['account_folder_id']]) ? $grups[$row['AccountFolder']['account_folder_id']] : '' : ''; ?>">
                                            <span class="foldrcl1"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                                            <span class="foldrcl2" style="white-space: nowrap;display: inline-block;text-overflow: ellipsis;overflow: hidden;width: 45%;position: relative;top: 4px;"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                    <?php echo $row['AccountFolder']['name']; ?>
                                                </a></span>
                                                <?php else: ?>
                                        <li class="huddlist <?php if (isset($folders) && !empty($folders)): ?><?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>card<?php endif; ?><?php endif; ?>" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">

                                            <div class="huddle_list_detail handle">

                                                <span class="foldrcl1" style="white-space: nowrap;display: inline-block;text-overflow: ellipsis;overflow: hidden;width: 38%;position: relative;"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                    <?php echo $row['AccountFolder']['name']; ?>
                                                    </a></span>
                                                    <?php endif; ?>
                                            <span class="foldrcl5" style="display:inline-block;min-height:5px;">
                                            <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                                    <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                                        <a rel="tooltip" data-original-title="Edit" href="<?php echo $this->base . '/Folder/edit/' . $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                    <?php else: ?>
                                                        <a rel="tooltip" data-original-title="Edit" href="<?php echo $this->base . '/add_huddle_angular/edit/' . $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                                <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                                    <a rel="tooltip nofollow" data-original-title="Delete" data-method="delete" data-confirm="Are you sure you want to permanently delete this huddle?" href="<?php echo $this->base . '/Huddles/delete/' . $row['AccountFolder']['account_folder_id'] ?>"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                                                <?php endif; ?>
                                                <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                                    <?php //if ($this->custom->check_folders_in_account($user_current_account['users_accounts']['account_id'])):  ?>
                                                    <?php if (isset($folders) && !empty($folders)): ?>
                                                        <a href="#" class="move_huddle" rel="tooltip" data-original-title="Move" data-toggle="modal" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>" data-target="#movefolderto" > <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </span>
                                            <span class="foldrcl4"style="width: 85px; text-align: center;"><?php echo count($huddleUsers); ?></span>
                <?php if (isset($folders) && !empty($folders) || (isset($total_folders_for_users) && !empty($total_folders_for_users))): ?>          <span class="foldrcl4" style="text-align: center;  width: 85px;">&nbsp;</span> <?php endif; ?>
                                            <span class="foldrcl3" style="text-align:center;">&nbsp;<?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])) ?></span>
                                            <?php if ($row['AccountFolder']['folder_type'] != '5'): ?>
                                                <span class="foldrcl7" style="text-align:center;width: 80px;"><?php echo $row['AccountFolder']['total_docs']; ?></span>
                                                <span class="foldrcl7" style="text-align:center;"><?php echo $row['AccountFolder']['total_videos']; ?></span>

                    <?php if ($row['AccountFolder']['meta_data_value'] == '2'): ?>
                                                    <span class="foldrcl6" style="font-weight: bold;font-size: 12px;width: 76px;">Coach</span>
                                                <?php elseif ($row['AccountFolder']['meta_data_value'] == '3'): ?>
                                                    <span class="foldrcl6" style="float: right;font-weight: bold;font-size: 12px;width: 76px;">Assess</span>
                                                <?php else: ?>
                                                    <span class="foldrcl6" style="float: right;font-weight: bold;font-size: 12px;width: 76px;">Collab </span>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <div class="clear"></div>

                                        </div>
                                    </li>
            <?php endforeach; ?>
                            </ol>

        <?php endif; ?>
                    </div>
                    <?php endif; ?>
            <?php else: ?>
                <div class="text-center">
                <?php $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/video-brown.png'); ?>
                    <p><?php echo $this->Html->image($chimg, array('alt' => 'Video-brown')); ?></p>
                    <h2 class="heading--info">None of your Huddles or Folders matched this search.  Try another search.</h2>
                    <p style="display:none;">To view sample Coaching Huddle, <a href="/video_huddles?sample=yes" class="blue-link">click here</a>!</p>
                </div>
                <div class="<?php if ($user_current_account['roles']['role_id'] != '120' || $user_permissions['UserAccount']['permission_maintain_folders'] == '1'): ?>btn-annotation<?php else: ?>btn-annotation2<?php endif; ?>">
    <?php if ($user_current_account['roles']['role_id'] != '120' || $user_permissions['UserAccount']['permission_maintain_folders'] == '1'): ?>
                        Click here to create your first Huddle!
                    <?php endif; ?>
                    A Huddle is a great way to work with others to improve teaching and learning. Share videos and related resources and participate in discussions with other Huddle participants.
                </div>

                <a href="#" id="view_browser_notification" style="display: none;" data-toggle="modal" data-target="BrowserMessageModal">browser message</a>
                <div id="BrowserMessageModal" class="modal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="header">
                                <h4 class="header-title nomargin-vertical smargin-bottom">Upgade Your Browser</h4>
                                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                            </div>

                            <p>You're Using an unsupported browser. Please upgrade your browser to view <?php $this->Custom->get_site_settings('site_title') ?> Application</p>
                        </div>
                    </div>
                </div>
<?php endif; ?>
            <div class="clear"></div>
        </div>
        <script type="text/javascript">
            $(".widget-scrollable").tinyscrollbar();
            $("[rel~=tooltip]").tooltip({container: 'body'});
        </script>
