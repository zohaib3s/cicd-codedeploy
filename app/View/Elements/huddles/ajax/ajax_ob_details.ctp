<?php

function gettagclass2($comment_tags, $default_tags) {
    $return = -1;
    foreach ($comment_tags as $comment_tag) {
        foreach ($default_tags as $key => $default_tag_value) {
            if (!empty($comment_tag['account_tags']['account_tag_id'])) {
                if ($default_tag_value['AccountTag']['account_tag_id'] == $comment_tag['AccountCommentTag']['account_tag_id']) {
                    $return = $key;
                    break;
                }
            }
        }
    }
    $return = $return + 1;
    return $return;
}
$alert_messages = $this->Custom->get_page_lang_based_content('alert_messages');
?>
<div class="observation_left">
    <div class="coach_cochee_container" >
        <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/5' ?>" class="back huddle_videos_all">Back to all observations</a>
        <div class="clear"></div>
    </div>
    <div id="name-container">
        <div class="doted_container">
            <div id="ObservationTitle1" name="click to edit title" style="font-weight: bold; width: 410px">
                <?php echo $videos['Document']['original_file_name']; ?>
            </div>
        </div>
        <h2>Observation Date</h2>
        <h3><?php echo date('M-d-Y'); ?></h3>
    </div>
    <?php if ($videos['Document']['published'] == 0 && $videos['Document']['is_associated'] != 0): ?>

        <div id="publish-button-container" style="text-align: center; display:block;">
            <input type="submit" id="publish-observations" data-document-id="<?php echo $videos['Document']['id']; ?>" class="observation_btn" value="Publish Video" />
        </div>
    <?php endif; ?>


    <?php if (is_array($videos) && count($videos) > 0) { ?>

        <?php
        $timeS = array();
        $timecls = array();
        $videoID = $videos['Document']['id'];
        $document_files_array = $this->Custom->get_document_url($videos['Document']);
        if (empty($document_files_array['url'])) {
            $videos['Document']['published'] = 0;
            $document_files_array['url'] = $videos['Document']['original_file_name'];
            $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
        } else {
            $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
        }

        $videoFilePath = pathinfo($document_files_array['url']);
        $videoFileName = $videoFilePath['filename'];
        if ($videoCommentsArray) {
            foreach ($videoCommentsArray as $cmt) {
                if (!empty($cmt['Comment']['time'])) {
                    $timeS[] = $cmt['Comment']['time'];
                    if (!empty($cmt['default_tags']) && count($cmt['default_tags']) > 0) {
                        $timecls[] = gettagclass2($cmt['default_tags'], $tags);
                    } else {
                        $timecls[] = 0;
                    }
                }
            }
        }

        $videoID = $videos['Document']['id'];
        $document_files_array = $this->Custom->get_document_url($videos['Document']);
        if (empty($document_files_array['url'])) {
            $videos['Document']['published'] = 0;
            $document_files_array['url'] = $videos['Document']['original_file_name'];
            $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
        } else {
            $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
            @$videos['Document']['duration'] = $document_files_array['duration'];
        }

        $videoFilePath = pathinfo($document_files_array['url']);
        $videoFileName = $videoFilePath['filename'];
        ?>
        <div>

            <input id="txtHuddleID" type="hidden" value="<?php echo $huddle_id ?>"/>
            <div >
                <div id="video_span" style="width:100%;">
                    <div class="associate_video" id="open-dialog">
                        <div class="videos-list__item-thumb">
                            <style>
                                .associate_video .videos-list__item-thumb video {
                                    width: 100%;
                                }
                            </style>
                            <?php
                            if ($videos['Document']['published'] == '1' && $videos['Document']['is_associated'] == '1') {
                                $thumbnail_image_path = $document_files_array['thumbnail'];
                                $video_path = $document_files_array['url'];
                                ?>
                                <input type="hidden" id="txtCurrentVideoID" value="<?php echo $video_id; ?>" />
                                <input type="hidden" id="txtCurrentVideoUrl" value="<?php echo $this->base . '/Huddles/view/' . $huddle_id . "/1/$video_id" ?>" />
                                <video id="example_video_<?php echo $videos['Document']['id'] ?>" class="video-js vjs-default-skin" controls preload="metadata" width="100%" height="320" poster="<?php echo $thumbnail_image_path; ?>" data-markers="[<?php echo implode(',', array_reverse($timeS)); ?>]" data-cls="[<?php echo implode(',', array_reverse($timecls)); ?>]">
                                    <source src="<?php echo $video_path; ?>" type='video/mp4'/>
                                </video>

                            <?php } else { ?>
                                <a id="processing-message-<?php echo $videos['Document']['id']; ?>" href="" title="<?php echo $videos['afd']['title'] ?>">
                                    <div  class="video-unpublished ">
                                        <div class="huddles-unpublished" style="color:#fff;">
                                            <?php if ($videos['Document']['published'] == 0 && $videos['Document']['is_associated'] == '1') { ?>

                                                <?php
                                                $videos['Document']['published'] = 1;
                                                $document_files_array = $this->Custom->get_document_url($videos['Document']);
                                                $thumbnail_image_path = $document_files_array['thumbnail'];
                                                $videos['Document']['published'] = 0;
                                                echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $videos['Document']['id'], 'style' => 'margin-left: -19px; margin-top: 22px;'));
                                                ?>
                                            <?php } else { ?>
                                                <div class="associate_video" id="open-dialog">
                                                    <a href="#" rel="tooltip" data-toggle="modal" data-target="#associate-videos-model" class="move_huddle">
                                                        <img src="/img/associate_video.png" style="margin-top: 30px;"  alt=""/>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div style="clear: both;"></div>
        </div>


    <?php } ?>
    <div style="clear: both;"></div>
    <span class="call_notes_container" id="stop-call-notes"> <?php echo $comments; ?></span>
</div>
<div class="observation_right">
    <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
        <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
            <div class="tab-content tab" id="frameWorksTab" style="visibility: visible;">
                <div class="search-box standard-search" style="position: relative;width:100%;">
                    <input type="button" id="btnSearchTags" class="btn-search" value="">
                    <input class="text-input observation_search_bg" id="txtSearchTags" type="text" value="" placeholder="Search Standards..." style="margin-right: 0px;width: 94% !important;">
                    <span id="clearTagsButton" class="clear-video-input-box" style="display:none;right: 33px;top: 20px;">X</span>
                </div>
                <div id="scrollbar1" style="float: left;">
                    <div class="viewport short" style="overflow:scroll;overflow-x: hidden;">
                        <div class="overview p-left0" style="top: 0px;padding: 0px;">
                            <div id="listContainer">
                                <style type="text/css">
                                    .standardRed {
                                        color: red;display: inline;
                                    }
                                    .standardBlue {
                                        color: blue;display: inline;
                                    }
                                    .standardBlack {
                                        color: #000;display: inline;
                                    }
                                    .standardOrange {
                                        color: orange;display: inline;
                                    }
                                    .standardGreen {
                                        color: green;display: inline;
                                    }
                                    .frame_work_heading{
                                        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
                                        font-weight: 300 !important;
                                        font-size: 20px !important;
                                        margin:10px 0px !important;
                                        cursor:auto !important;
                                    }
                                </style>
                                <ul id="expList1" class="expList1" style="padding-left:0px">

                                    <?php
                                    if (!empty($standardsL2)) {
                                        for ($i = 0; $i < count($standardsL2); $i++) {
                                            ?>
                                            <li class="frame_work_heading" ><?php echo $standardsL2[$i]['AccountTag']['tag_code'] . ' - ' . $standardsL2[$i]['AccountTag']['tag_title']; ?></li>
                                            <?php
                                            foreach ($standards as $standard) {
                                                if ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['parent_account_tag_id']) {
                                                    ?>
                                                    <li class="standard standard-cls" >
                                                        <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                        <?php
                                                        $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                        echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                        ?>
                                                    </li>

                                                    <?php
                                                }
                                            }
                                        }
                                    } else {
                                        foreach ($standards as $standard) {
                                            ?>
                                            <li class="standard standard-cls">
                                                <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                <?php
                                                $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                ?>
                                            </li>

                                            <?php
                                        }
                                    }
                                    ?>
                                    <li id="noresults" style="width: 280px;">No standards match your search criteria.</li>
                                </ul>
                                <script type="text/javascript">

                                    $(document).ready(function () {
                                        $('.standard input').on('change', function () {
                                            var tag_code = $(this).attr('st_code');
                                            var tag_value = '';
                                            var tag_array = {};
                                            var tag_name = $(this).attr('st_name');
                                            tag_array = tag_name.split(" ");
                                            if ($(this).is(':checked')) {
                                                tag_array = tag_name.split(" ");
                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                $('#txtVideostandard').addTag(tag_value);
                                                if ($('input[name="name1"]:checked').length > 0) {
                                                    $('#txtVideostandard_tag').hide();
                                                }
                                            } else {
                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                $('#txtVideostandard').removeTag(tag_value);
                                            }
                                        });
                                    });</script>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        <?php endif; ?>
    <?php endif; ?>

</div>
<div class="modal fade" id="associate-videos-model" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="add-huddle-video" style="padding: 0">
                <div class="modal-header">
                    <h4 class="header-title nomargin-vertical smargin-bottom">Associate Video</h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">×</a>
                </div>
                <div class="modal-body">
                    <div class="video_upload_dialog">
                        <div class="Associate_Video_btn"><a id="select-from-workspace" href=""><img src="http://qa.sibme.com/img/my_workspace.png" style="top:-2px;" /> Select From Workspace</a></div>
                        <div class="Associate_Video_btn"><a id="select-from-huddle" href=""><img src="http://qa.sibme.com/img/dbi-huddles.png" /> Select From Huddle</a></div>
                    </div>
                    <div class="video_box_workSpace select-from-huddle" style="display: none;width:577px">
                        <div class="workspace_header">
                            <img src="http://qa.sibme.com/img/dbi-huddles.png" style="top:-2px;" /> Select from Huddle
                            <div class="back_btn_dialog" id="back-to-select-container"><img src="/img/icon_backArrow.png" width="12" height="8" alt=""/> Back</div>
                        </div>
                        <input type="text" placeholder="Find Video" class="find_video">
                        <div class="videos_list_box">
                            <?php if ($huddle_videos) { ?>
                                <?php foreach ($huddle_videos as $row) { ?>
                                    <?php
                                    $videoID = $row['Document']['id'];

                                    $document_files_array = $this->Custom->get_document_url($row['Document']);

                                    if (empty($document_files_array['url'])) {
                                        $row['Document']['published'] = 0;
                                        $document_files_array['url'] = $row['Document']['original_file_name'];
                                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                    } else {
                                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        @$row['Document']['duration'] = $document_files_array['duration'];
                                    }

                                    $videoFilePath = pathinfo($document_files_array['url']);
                                    $videoFileName = $videoFilePath['filename'];
                                    ?>
                                    <div class="video_detail">
                                        <div class="video_detail_left">
                                            <input type="checkbox" name="my_file_id" value="<?php echo $row['Document']['id']; ?>" class="select-radio" style="float: left;margin-right: 10px;margin-top: 21px;">
                                            <div class="videos-list__item-thumb">
                                                <?php
                                                if ($row['Document']['published'] == '1'):
                                                    $seconds = $row['Document']['duration'] % 60;
                                                    $minutes = ($row['Document']['duration'] / 60) % 60;
                                                    $hours = gmdate("H", $row['Document']['duration']);
                                                    ?>
                                                    <div  class="video-unpublished " style="height: 55px;width: 74px;">
                                                        <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                                            <?php
                                                            $thumbnail_image_path = $document_files_array['thumbnail'];

                                                            echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                                            ?>
                                                            <div class="play-icon"></div>
                                                            <div style="font-size: 10px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top:  117px;right: 0px;padding: 2px;border-radius: 3px;z-index: 999;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                                                        </a>
                                                    </div>
                                                <?php else: ?>
                                                    <a id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                                        <div  class="video-unpublished " style="height: 55px;width: 74px;">
                                                            <span class="huddles-unpublished"  style="color:#fff;">
                                                                <?php if ($row['Document']['encoder_status'] == 'Error'): ?>
                                                                    Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                                                <?php else : ?>
                                                                    <img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br><?=$alert_messages["Your_video_is_currently_processing"]; ?>
                                                                <?php endif; ?>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="video_detail_text">
                                            <div class="videos-list__item-title">
                                                <a class="wrap1" id="vide-title-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" ><?php echo (strlen($row['afd']['title']) > 52 ? mb_substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title']) ?></a>
                                            </div>
                                            <div class="videos-list__item-author">By <a href="<?php echo empty($row[0]['AutoCreated']) ? '#' : 'mailto:' . $row[0]['WebUploaderEmail']; ?>" title="<?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?>" ><?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?></a></div>
                                            <div class="videos-list__item-added">Uploaded <?php echo date('M d, Y', strtotime($row['Document']['created_date'])) ?></div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <div style="clear: both;"></div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="video_box_workSpace1 select-from-workspace" style="display: none;">
                        <div class="workspace_header">
                            <img src="http://qa.sibme.com/img/my_workspace.png" style="top:-2px;" /> Select From Workspace
                            <div class="back_btn_dialog" id="back-to-select-container">
                                <img src="/img/icon_backArrow.png" width="12" height="8" alt=""/> Back
                            </div>
                        </div>
                        <input type="text" placeholder="Find Video" class="find_video find_video">
                        <div class="videos_list_box">
                            <?php if (is_array($files) && count($files) > 0) { ?>
                                <?php foreach ($files as $row) { ?>
                                    <?php
                                    $videoID = $row['Document']['id'];
                                    $videoFilePath = pathinfo($row['Document']['url']);
                                    $videoFileName = $videoFilePath['filename'];

                                    $document_files_array = $this->Custom->get_document_url($row['Document']);

                                    if (empty($document_files_array['url'])) {
                                        $row['Document']['published'] = 0;
                                        $document_files_array['url'] = $row['Document']['original_file_name'];
                                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        $row['Document']['duration'] = '00:00';
                                    } else {
                                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        @$row['Document']['duration'] = $document_files_array['duration'];
                                    }
                                    ?>

                                    <div class="video_detail">
                                        <div class="video_detail_left">
                                            <input type="checkbox" name="my_file_id" value="<?php echo $row['Document']['id']; ?>" class="select-radio" style="float: left;margin-right: 10px;margin-top: 21px;">
                                            <div class="videos-list__item-thumb" style="">
                                                <?php
                                                if ($row['Document']['published'] == '1') :
                                                    $seconds = $row['Document']['duration'] % 60;
                                                    $minutes = ($row['Document']['duration'] / 60) % 60;
                                                    $hours = gmdate("H", $row['Document']['duration']);
                                                    ?>
                                                    <div  class="video-unpublished " style="height: 55px;width: 74px;">
                                                        <a id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/MyFiles/view/1/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                            <?php
                                                            if ($row['Document']['encoder_provider'] == '2') {
                                                                $path = "uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName .
                                                                        (empty($row['Document']['thumbnail_number']) ?
                                                                                '_thumb_00001.png' :
                                                                                '_thumb_' . sprintf('%05d', $row['Document']['thumbnail_number']) . '.png'
                                                                        );
                                                            } else {
                                                                $path = "uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_thumb.png";
                                                            }

                                                            $thumbnail_image_path = $document_files_array['thumbnail'];

                                                            echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                                            ?>
                                                            <div class="play-icon"></div>
                                                            <div style="font-size: 10px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top:  41px;right: 0px;padding: 2px;border-radius: 3px;z-index: 999;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                                                        </a>
                                                    </div>

                                                <?php else: ?>
                                                    <a id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/MyFiles/view/1/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                        <div class="video-unpublished" style="height: 55px;width: 74px;">
                                                            <span>
                                                                <?php if ($row['Document']['encoder_status'] == 'Error'): ?>
                                                                    Video failed to process, please try again.
                                                                <?php else : ?>
                                                                    Video is currently processing.
                                                                <?php endif; ?>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <?php endif; ?>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                        <div class="video_detail_text">
                                            <div class="videos-list__item-title">
                                                <a class="wrap1" id="vide-title-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" ><?php echo (strlen($row['afd']['title']) > 52 ? mb_substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title']) ?></a>
                                            </div>
                                            <div class="videos-list__item-author">By <a href="<?php echo empty($row[0]['AutoCreated']) ? '#' : 'mailto:' . $row[0]['WebUploaderEmail']; ?>" title="<?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?>" ><?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?></a></div>
                                            <div class="videos-list__item-added">Uploaded <?php echo date('M d, Y', strtotime($row['Document']['created_date'])) ?></div>
                                            <div class="clear"></div>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <div style="clear: both;"></div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
                    <button type="button" id="add_video" class="btn btn btn-green"  style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>; width: 99px;border-radius: 5px; display: none;">Select </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->

</div>