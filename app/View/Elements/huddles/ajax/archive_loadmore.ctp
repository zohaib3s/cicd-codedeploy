<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$loggedInUserRole = '';
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];

$users_shown = array();
$groups_shown = array();
$is_group = '';
?> 
<style>
                                    .box.style2{
                                        margin:0px;
                                        border: 1px solid #dddad2;
                                        margin-bottom:-1px;
                                        border-radius:0px;
                                        padding:8px;
                                        min-height:70px;
                                    }
                                    .huddle_list_left{
                                        float:left; 
                                        width:710px;              
                                    }
                                    .huddle_list_right{
                                        float:right; 
                                    }
                                    .row{
                                        margin-left:0px;
                                    }
                                    [data-group] {
                                        margin-top: 6px;
                                    }
                                    .grid_listHeader{
                                            font-size: 15px;
                                            color: #424240;
                                            padding: 0px 30px;
                                            font-weight: 600;
                                            margin-bottom:10px;
                                    }
                                    .grid_listHeader span{
                                        display:inline-block;
                                    }
                                    .header_name{
                                        float:left !important;
                                    }
                                    .header_rightBox{
                                        float:right;
                                    }
                                    .header_video{
                                        margin-left:10px;
                                    }
                                    .header_resources{
                                        margin-left:10px;
                                    }
                                    .header_date{
                                        margin-left:10px;
                                            width: 100px;
                                    }
                                    .header_huddle{
                                        margin-left:10px;
                                    }
                                    .header_participant{
                                        margin-left:10px;
                                    }
                                    .header_action{
                                        margin-left:10px;
                                        width: 58px;
                                    }
                                    
                                </style>
                                
        

            <?php if (isset($video_huddles) && !empty($video_huddles)):?>
                                  
                    <?php
                    $grups = array();
                    foreach ($video_huddles as $row):
                        $first_letter = $this->Custom->py_slice(trim($row['AccountFolder']['name']), '0');
                        $grups[$row['AccountFolder']['account_folder_id']] = strtoupper($first_letter);
                        $grups = array_unique($grups);
                        ?>
                    <?php endforeach; ?>
                                           
                    <?php foreach ($video_huddles as $row): ?>    
                        <?php
                        $invitedUser = '';
                        $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                        $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                        $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                        ?>
                        <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                        <li class="folderlist" data-group="<?php echo ($sort == 'name') ? isset($grups[$row['AccountFolder']['account_folder_id']]) ? $grups[$row['AccountFolder']['account_folder_id']] : '' : ''; ?>">
                                           
                                            <span class="foldrcl1"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                                            <span class="foldrcl2"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                                            <?php echo $row['AccountFolder']['name']; ?>
                                                </a></span>
                                            <?php else: ?>
                                        <li class="huddlist <?php if (isset($folders) && !empty($folders)): ?><?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>card<?php endif;?><?php endif;?>" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">

                                            <div class="huddle_list_detail handle">
                                                 <span><input name="selector[]" id="ad_Checkbox_<?php echo $row['AccountFolder']['account_folder_id']; ?>" class="ads_Checkbox" type="checkbox" value="<?php echo $row['AccountFolder']['account_folder_id']; ?>" /></span>
                                                <span class="foldrcl1" style="white-space: nowrap;display: inline-block;text-overflow: ellipsis;overflow: hidden;width: 48%;position: relative;"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="javascript:void(0)">
                                            <?php echo $row['AccountFolder']['name']; ?>
                                                    </a></span>
                <?php endif; ?>               
                                            
                                                    
                                                    
                                                    <span class="foldrcl3" style="text-align:center;">&nbsp;<?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])) ?></span>
                                          <?php if ($row['AccountFolder']['folder_type'] != '5'): ?>
                                                    <span class="foldrcl7" style="text-align:center;width: 80px;"><?php echo $row['AccountFolder']['total_docs'];?></span>
                                                    <span class="foldrcl6" style="text-align:center;"><?php echo $row['AccountFolder']['total_videos'];?></span>
                                          <?php endif; ?>
                                                    <div class="clear"></div>

                                            </div>
                                        </li>
           <?php endforeach; ?> 
                                    

            <?php endif; ?> 
       <div id="load_more_div" style="margin-top:15px">                                 
     <?php
            print $this->element('load_more', array(
                        'total_items' => $total_huddles,
                        'current_page' => $current_page,
                        'number_per_page' => 20,
                        'load_more_what'=>'huddles'
            ));
            ?>
</div>


