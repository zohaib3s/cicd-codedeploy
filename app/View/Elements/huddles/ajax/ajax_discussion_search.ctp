<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
?>


<?php if (is_array($discussions) && count($discussions) > 0 && $replys == '' && $key != 'add'): ?>
    <?php if (is_array($discussions) && count($discussions) > 0): ?>
        <?php foreach ($discussions as $row): ?>
            <?php
            $discussionReplyCount = $Comment->getReplysCount($row['Comment']['id']);
            $discussionReply = $Comment->getReplysAttachments($row['Comment']['id']);
            ?>
            <li class="discussion_row">
                <div class="tab-3-box">
                    <div class="tab-3-box-left">
                        <h2><a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/3/' . $row['Comment']['id']; ?>"><?php echo $row['Comment']['title']; ?></a></h2>
                        <?php
                        $profile_image = isset($row['User']['image']) ? $row['User']['image'] : '';
                        $profile_image_real_path = WWW_ROOT . "/img/users/$profile_image";

                        //if (file_exists($profile_image_real_path) && $profile_image != '') {
                        if ($profile_image != '') {
                            //$profile_image = $this->webroot . "img/users/$profile_image";
                            $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/".$row['User']['user_id']."/".$row['User']['image']);
                        } else {
                            //$profile_image = $this->webroot . "img/profile.jpg";
                            $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . "img/profile.jpg");
                        }

                        ?>

                        <div class="frame"><img src="<?php echo $profile_image; ?>" alt="img" height="42px" width="42px"/></div>
                        <div class="text"> <strong class="title"><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?></strong>
                            <div style="float: left;">
                                <?php if ($discussionReplyCount > 0): ?>
                                    <p><?php echo strlen($row['Comment']['comment']) > 200 ? strip_tags(mb_substr($row['Comment']['comment'], 0, 200), '<p><a><br>') . '...' : strip_tags($row['Comment']['comment'], '<p><a><br>') ?></p>
                                <?php else: ?>
                                    <p><p><?php echo strlen($row['Comment']['comment']) > 130 ? strip_tags(mb_substr($row['Comment']['comment'], 0, 130), '<p><a><br>') . '...' : strip_tags($row['Comment']['comment'], '<p><a><br>') ?></p></p>
                                <?php endif; ?>
                            </div>
                            <div style="float: right" class="docs-box">
                                <ul>
                                    <?php $attachedDocuments = $Document->getByCommentId($row['Comment']['id']); ?>
                                    <?php foreach ($attachedDocuments as $att): ?>
                                        <?php if ($att['Document']['url'] != ''): ?>
                                            <?php
                                            $videoFilePath = pathinfo($att['Document']['url']);
                                            $ext = $videoFilePath['extension'];
                                            $parentDiscussion = $Comment->get($row['Comment']['id']);
                                            $download_url = '';

                                            $href = '';
                                            if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')) {
                                                $download_url = $this->base . '/Huddles/download/' . $att['Document']['id'];
                                                $href = 'href="' . $download_url . '"';
                                            }
                                            ?>
                                            <li>
                                                <a <?php echo $href; ?> target="_blank" style="font-size:14px;">
                                                    <div class="" style="padding-top: 10px;">
                                                        <img class="icon24" src="<?php echo $this->Custom->getDocTypeIconLink($ext);?>" alt="doc type icon" />
                                                    </div>
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>

                                    <?php if (count($discussionReply) != ''): ?>
                                        <?php
                                        foreach ($discussionReply as $row2) {
                                            if ($row2['Document']['url'] != ''):
                                                $videoFilePath = pathinfo($row2['Document']['url']);
                                                $ext = $videoFilePath['extension'];
                                                $download_url = '';

                                                $href = '';
                                                if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')) {
                                                    $download_url = $this->base . '/Huddles/download/' . $row2['Document']['document_id'];
                                                    $href = 'href="' . $download_url . '"';
                                                }
                                                ?>
                                                <li>
                                                    <a <?php echo $href; ?> target="_blank" style="font-size:14px;">
                                                        <div class="" style="padding-top: 10px;">
                                                            <img class="icon24" src="<?php echo $this->Custom->getDocTypeIconLink($ext);?>" alt="doc type icon" />
                                                        </div>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                        <?php } ?>
                                    <?php endif;
                                    ?>
                                </ul>
                            </div>
                            <div style="clear: both;"></div>
                            <div>
                                <?php
                                $commentDate  = $row['Comment']['created_date'];
                                $commentsDate = $this->Custom->formatDate2($commentDate);
                                ?>
                                <strong class="posted">Posted <?php echo $commentsDate; ?></strong>
                            </div>

                        </div>
                    </div>
                    <div class="right-box-docs">
                        <ul>
                            <li>
                                <strong class="tip">
                                    <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/3/' . $row['Comment']['id'] ?>"><?php echo $discussionReplyCount; ?></a>
                                </strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>
    <?php endif; ?>
<?php else: ?>
    <li style="width:100%;"><?php echo $language_based_content['none_of_your_discussions_matched']; ?></li>
<?php endif; ?>
