

<style type="text/css">
    .analytics_new{    background-color: #FFF; border: 1px solid #DAD6CE;border-radius: 3px 3px 3px 3px; box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); padding-bottom:25px; font-family: "Segoe UI",Helvetica,Arial,sans-serif;}
    .analytics_new img{ max-width:100%;}
    .analytics_top{ background:#fff; border-bottom:2px solid #e8e8e8; padding:8px 10px 15px 8px;}
    .analytic_img_cls { float:left;    width: 98px;}
    .analytic_detail_cls{ float:left;padding: 7px 17px 9px 16px;    width: 532px;}
    .analytic_detail_cls h3{    font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-weight: 400;font-size: 23px;}
    .analytic_detail_cls h4{    font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-weight: 400;font-size:15px;}
    .analytic_detail_cls label{ color:#668dab;}
    .analytic_detail_cls span{ display:inline-block; margin-right:7px;}
    .analytic_topnav { float:right;    font-family: "Segoe UI",Helvetica,Arial,sans-serif;    font-weight: 600;     width:365px;    text-align: right;}
    .analytic_topnav a{ color:#668dab;    font-family: "Segoe UI",Helvetica,Arial,sans-serif;    font-weight: 600;}
    .analytic_topnav  img{ margin:0 4px;}
    .analytic_overview { background:#fcfcfc; padding:12px;     border-bottom: 2px solid #e8e8e8;}
    .analytic_overview  h3{    font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-weight: 400;font-size: 23px;}
    .over_datecls {color:#668dab;    font-size: 14px;padding: 4px 0 5px 0;}
    .over_datecls img{ margin-right:5px; position:relative; top:-1px;}
    .analytic_countsbox {    border: 1px solid #DAD6CE; border-radius: 2px;box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); float:left; width:23%; background:#fff;     margin: 12px 1%;}
    .analytic_countsbox .anbox1{ background:#fcfcfc; text-align:center;    padding: 21px 14px;width:25%;    float: left;}
    .analytic_countsbox .anbox2{    padding: 10px 8px; width:75%; float:right;}
    .analytic_countsbox .anbox2 h4{  font-family: "Segoe UI",Helvetica,Arial,sans-serif;    font-weight: 600; font-size:18px; color:#000000;}
    .analytic_countsbox .anbox2 h5{  font-family: "Segoe UI",Helvetica,Arial,sans-serif;    font-weight: 400; font-size:13px; color:#878787;}
    .analytic_overview2 { background:#fff; padding:12px;     border-bottom: 2px solid #e8e8e8; position:relative;}
    .over_select { position:absolute; top:12px; right:15px;}
    .over_select select{ background:#668dab; width:170px; height: 38px;border-radius: 36px; color: #fff;padding: 6px 6px 6px 10px;}
    .am_chart1 { border:2px solid #e8e8e8; padding:10px; float:left; width:47%; margin:20px 1%;}
    .am_chart2 { padding:20px 0;}
	.analytic_filter { float:left; width:100%;     padding: 0 15px 15px 20px;}
	.analytic_filter h3{font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-weight: 400;font-size: 23px; margin:-6px 0 5px 0;}
	.analytic_filter select { width:100px; border:0; border-bottom:2px solid #ddd; margin:0 6px 0 4px;}
	.analytic_filter input[type=text] { width:70px; border:1px solid #ddd; margin:0 6px 0 4px;}
	.analytic_filter .btn{ margin-left:10px;}
	.analytic_custombtn { float: right;color:#668dab; cursor:pointer;}
	.analytic_custombtn a{color:#668dab; text-decoration:none;}
	.analytic_custom2 { display:none;}
	.framework_filter{background: #fcfcfc; padding: 12px; border-bottom: 2px solid #e8e8e8;}
	.flter_leftcls { float:left; width:45%;}
	.flter_leftcls select { width:150px; border:0; border-bottom:2px solid #ddd; margin:0 6px 0 4px;background: #fcfcfc;;}
	.flter_rightcls { float:left; width:55%;}
	.flter_rightcls input[type=text]{ border:0; outline:0 !important;     box-shadow: 0 0px 0px rgba(0,0,0,0.09) inset;     background: #fcfcfc; color:#668dab;     width: 119px;}
	
	.flter_rightcls ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #668dab;
}
.flter_rightcls ::-moz-placeholder { /* Firefox 19+ */
  color: #668dab;
}
.flter_rightcls :-ms-input-placeholder { /* IE 10+ */
  color: #668dab;
}
.flter_rightcls :-moz-placeholder { /* Firefox 18- */
   color: #668dab;
}

	.flter_rightcls .token_filter{    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    display: inline-block;
    border: 1px solid #b9b9b9;
    background-color: #fff;
    white-space: nowrap;
 
    vertical-align: top;
    cursor: default; border-radius:5px;    }
	.flter_rightcls .token-label{display: inline-block;
    overflow: hidden;
    text-overflow: ellipsis;
    padding-left: 4px;
    vertical-align: top; max-width:100px;     font-size: 12px;}
	.flter_rightcls .close {
    font-family: Arial;
    display: inline-block;
    line-height: 100%;
    font-size: 1.1em;
    line-height: 1.49em;
    margin-left: 5px;
    float: none;
    height: 100%;
    vertical-align: top;
    padding-right: 4px; 
	position: absolute;
	    right: 15px;
    top: 6px;
	    font-weight: 400;
    color: #998c8c;
}

.flter_rightcls .token {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    display: inline-block;
    border: 1px solid #d9d9d9;
    background-color: #fff;
    white-space: nowrap;
    margin: 4px 5px 5px 0;
   
    vertical-align: top;
    cursor: default; position: relative;
	    border-radius: 34px;
       padding: 3px 20px 0px 4px;
}

.performance_left { float:left; width:49%; border-right:2px solid #ddd; padding:2%;   height: 350px;
    overflow: auto;}
.performance_left h3{    font-family: "Segoe UI",Helvetica,Arial,sans-serif; font-size:17px;}
.performance_left h5{    font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-size:13px;     margin: 16px 0 13px 0;
}
.per_r3 a{    cursor: pointer;
font-weight: normal;
}
.per_r3 a:hover{color: #3fbd53;}
.performance_right { float:left; width:49%;  padding:2%;     max-height: 350px;
    overflow: auto;}
.performance_outer { padding:20px;}
.per_r1 { width:30%; float:left;}
.per_r2 { width:11%; float:left;     text-align: center;}
.per_r3 { width:59%; float:left;}
.per_row { margin:5px 0;}

.per_r1 select{ width:100%; border-radius:4px; border:1px solid #ddd;    height: 28px;}
.per_r2 span{ width:28px; height:28px; border-radius:50%;  display:inline-block;     line-height: 26px; text-align:center; color:#fff; font-size:12px;}
.per_green{background:#2cb742;}
.per_blu{ background:#3498db;}
.per_r3 {}



.performance_before_select h3{    font-family: "Segoe UI",Helvetica,Arial,sans-serif; margin:15px 0; font-size:15px; color:#4f4f4f;}
.performance_before_select { text-align:center; padding-top: 73px;}
.performance_right h4{ font-family: "Segoe UI",Helvetica,Arial,sans-serif; margin:10px 0; font-size:14px; color:#4f4f4f;}
#performance_poup .modal-content form{padding:0;}

.selected_highlight{color: #3fbd53;}


</style>

<?php 
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user = $this->Session->read('user_current_account');
$user_role = $this->Custom->get_user_role_name($user['users_accounts']['role_id']);
?>



<div class="performance_left">
              <h3><?php echo $standard_name; ?></h3>
             <form id="add_rating_form" method="post" enctype="multipart/form-data"    
                                  action="<?php echo $this->base . '/huddles/add_multiple_standard_ratings/'; ?>" accept-charset="UTF-8">
              
                    <?php
                                                                            if (!empty($standardsL2)) {
                                                                                for ($i = 0; $i < count($standardsL2); $i++) {
                                                                                    ?>
                                                                                    <h5><?php echo $standardsL2[$i]['AccountTag']['tag_code'] . ' - ' . $standardsL2[$i]['AccountTag']['tag_title']; ?></h5>
                                                                                    <?php
                                                                                    foreach ($standards as $standard) {
                                                                                        if ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['parent_account_tag_id']) {
                                                                                            ?>
                                                                                    
                                                                                            <?php $account_tag = explode(':', $standard['AccountTag']['tag_html']); ?>
                                                                                    
                                                                                            <div class="per_row">
                                                                                              
                                                                                            <?php if($huddle_permission == '200'): ?><div class="per_r1"><select name="rating_id_<?php echo $standard['AccountTag']['account_tag_id']; ?>[]" <?php if($this->Custom->get_standard_tagged_count($video_id,$standard['AccountTag']['account_tag_id']) == 'N/O') {echo 'style="visibility:hidden;"';}  ?> class = "select_class" standard_id = "<?php echo $standard['AccountTag']['account_tag_id'];  ?>"><option value="0">Select</option><?php foreach($ratings as $tag) { ?>  <option <?php $selected_rating = $this->custom->get_selected_rating($standard['AccountTag']['account_tag_id'],$account_id,$huddle_id,$video_id); if(!empty($selected_rating) && $selected_rating['DocumentStandardRating']['rating_id'] == $tag['AccountMetaData']['account_meta_data_id']) { echo 'selected="selected"';} ?>  account_meta_data_id = "<?php echo $tag['AccountMetaData']['account_meta_data_id'];  ?>" meta_data_value = "<?php echo $tag['AccountMetaData']['meta_data_value'];  ?>"  value="<?php echo $tag['AccountMetaData']['account_meta_data_id'];  ?>"><?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?></option><?php } ?></select></div><?php endif; ?>
                                                                                            <input name="standard_ids[]" type = "hidden" value="<?php echo $standard['AccountTag']['account_tag_id']; ?>">
                                                                                            <div class="per_r2"><span  class="<?php if($this->Custom->get_standard_tagged_count($video_id,$standard['AccountTag']['account_tag_id']) == 'N/O' ) { echo 'per_blu'; } else { echo 'per_green'; } ?>"><?php echo $this->Custom->get_standard_tagged_count($video_id,$standard['AccountTag']['account_tag_id']); ?></span></div>
                                                                                            <div class="per_r3"><a rating_name = "<?php 
                                                                                            $bool = 1;    
                                                                                                foreach($ratings as $tag) {
                                                                                                    $selected_rating = $this->custom->get_selected_rating($standard['AccountTag']['account_tag_id'],$account_id,$huddle_id,$video_id);
                                                                                                    if(!empty($selected_rating) && $selected_rating['DocumentStandardRating']['rating_id'] == $tag['AccountMetaData']['account_meta_data_id']) { 
                                                                                                        echo substr($tag['AccountMetaData']['meta_data_name'], 13);
                                                                                                        $bool = 0;
                                                                                                    } 
                                                                                                }
                                                                                                
                                                                                                if($bool)
                                                                                                {
                                                                                                  echo 'No Rating';  
                                                                                                    
                                                                                                }
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                            ?>" class="load_comments" tagged_count = "<?php echo $this->Custom->get_standard_tagged_count($video_id,$standard['AccountTag']['account_tag_id']); ?>" header_name ="<?php echo $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] ;?>" account_tag_id = "<?php echo $standard['AccountTag']['account_tag_id']; ?>" >
                                                                                                
                                                                                                <?php 
                                                                                                
                                                                                                 if($huddle_permission == '210')
                                                                                                {
                                                                                                $bool = 1;    
                                                                                                foreach($ratings as $tag) {
                                                                                                    $selected_rating = $this->custom->get_selected_rating($standard['AccountTag']['account_tag_id'],$account_id,$huddle_id,$video_id);
                                                                                                    if(!empty($selected_rating) && $selected_rating['DocumentStandardRating']['rating_id'] == $tag['AccountMetaData']['account_meta_data_id']) { 
                                                                                                        echo $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0].' : '.substr($tag['AccountMetaData']['meta_data_name'], 13);
                                                                                                        $bool = 0;
                                                                                                    } 
                                                                                                }
                                                                                                
                                                                                                if($bool)
                                                                                                {
                                                                                                  echo $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0].' : No Rating';  
                                                                                                    
                                                                                                }
                                                                                                
                                                                                                
                                                                                                }
                                                                                                elseif($huddle_permission == '200')
                                                                                                {
                                                                                                echo $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] ;
                                                                                                }
                                                                                                
                                                                                                
                                                                                                ?>
                                                                                                
                                                                                                
                                                                                                </a></div>
                                                                                            <div class="clear"></div>
                                                                                          </div>
                   
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                foreach ($standards as $standard) {
                                                                                    ?>
                                                                                    <?php $account_tag = explode(':', $standard['AccountTag']['tag_html']); ?>
                                                                                    
                                                                                            <div class="per_row">
                                                                                                 
                                                                                            <?php if($huddle_permission == '200'): ?><div class="per_r1"><select name="rating_id_<?php echo $standard['AccountTag']['account_tag_id']; ?>[]" <?php if($this->Custom->get_standard_tagged_count($video_id,$standard['AccountTag']['account_tag_id']) == 'N/O') {echo 'style="visibility:hidden;"';}  ?> class = "select_class" standard_id = "<?php echo $standard['AccountTag']['account_tag_id'];  ?>"><option value="0">Select</option><?php foreach($ratings as $tag) { ?>  <option <?php $selected_rating = $this->custom->get_selected_rating($standard['AccountTag']['account_tag_id'],$account_id,$huddle_id,$video_id); if(!empty($selected_rating) && $selected_rating['DocumentStandardRating']['rating_id'] == $tag['AccountMetaData']['account_meta_data_id']) { echo 'selected="selected"';} ?>  account_meta_data_id = "<?php echo $tag['AccountMetaData']['account_meta_data_id'];  ?>" meta_data_value = "<?php echo $tag['AccountMetaData']['meta_data_value'];  ?>" value="<?php echo $tag['AccountMetaData']['account_meta_data_id'];  ?>" ><?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?></option><?php } ?></select></div><?php endif; ?>
                                                                                            <input name="standard_ids[]" type = "hidden" value="<?php echo $standard['AccountTag']['account_tag_id']; ?>">
                                                                                            <div class="per_r2"><span class="<?php if($this->Custom->get_standard_tagged_count($video_id,$standard['AccountTag']['account_tag_id']) == 'N/O' ) { echo 'per_blu'; } else { echo 'per_green'; } ?>"><?php echo $this->Custom->get_standard_tagged_count($video_id,$standard['AccountTag']['account_tag_id']); ?></span></div>
                                                                                            <div class="per_r3"><a rating_name ="<?php 
                                                                                            $bool = 1;    
                                                                                                foreach($ratings as $tag) {
                                                                                                    $selected_rating = $this->custom->get_selected_rating($standard['AccountTag']['account_tag_id'],$account_id,$huddle_id,$video_id);
                                                                                                    if(!empty($selected_rating) && $selected_rating['DocumentStandardRating']['rating_id'] == $tag['AccountMetaData']['account_meta_data_id']) { 
                                                                                                        echo substr($tag['AccountMetaData']['meta_data_name'], 13);
                                                                                                        $bool = 0;
                                                                                                    } 
                                                                                                }
                                                                                                
                                                                                                if($bool)
                                                                                                {
                                                                                                  echo 'No Rating';  
                                                                                                    
                                                                                                }
                                                                                            
                                                                                            
                                                                                            ?>" class="load_comments" tagged_count = "<?php echo $this->Custom->get_standard_tagged_count($video_id,$standard['AccountTag']['account_tag_id']); ?>" header_name = "<?php echo $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] ;?>" account_tag_id = "<?php echo $standard['AccountTag']['account_tag_id']; ?>" >
                                                                                                
                                                                                                     
                                                                                                    
                                                                                                <?php 
                                                                                                if($huddle_permission == '210')
                                                                                                {
                                                                                                $bool = 1;    
                                                                                                foreach($ratings as $tag) {
                                                                                                    $selected_rating = $this->custom->get_selected_rating($standard['AccountTag']['account_tag_id'],$account_id,$huddle_id,$video_id);
                                                                                                    if(!empty($selected_rating) && $selected_rating['DocumentStandardRating']['rating_id'] == $tag['AccountMetaData']['account_meta_data_id']) { 
                                                                                                        echo $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0].' : '.substr($tag['AccountMetaData']['meta_data_name'], 13);
                                                                                                        $bool = 0;
                                                                                                    } 
                                                                                                }
                                                                                                
                                                                                                if($bool)
                                                                                                {
                                                                                                  echo $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0].' : No Rating';  
                                                                                                    
                                                                                                }
                                                                                                
                                                                                                
                                                                                                }
                                                                                                elseif($huddle_permission == '200')
                                                                                                {
                                                                                                echo $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] ;
                                                                                                }
                                                                                                
                                                                                                
                                                                                                ?>
                                                                                                
                                                                                                
                                                                                                
                                                                                                </a></div>
                                                                                            <div class="clear"></div>
                                                                                          </div>

                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
              
              
              
          
              <input name="huddle_id" type="hidden" value="<?php echo $huddle_id;?>">
              <input name="video_id" type="hidden" value="<?php echo $video_id;?>">
             
         
              </form>
               
            </div>
            
            <div class="performance_right">
                <h4 id="header_name_2" ></h4>
                <div class="performance_before_select">
                    <div class="click_standard">
                   <img id="perform_level_icon" src="<?php echo $this->webroot . 'img/performance_levelicon.png' ?>" />
                   <h3 id="click_standard_text_change">Please Click Standard</h3>
                   </div>
                   
                   <a style="cursor: default; color: grey;<?php if($this->Custom->get_average_video_rating($video_id,$account_id,$huddle_id) == 'N/O') { echo "display:inline-block;" ;} else {echo "display:none;" ; } ?> "  id ="not_observed" href="#">Not Observed</a>
                   
                </div>
                
                <div class="">
               <h4 id="header_name" ></h4>
                <div id="check">
                     <div id="vidComments_performance" style="overflow-y: auto;">


                                                                      
                                                                                
                            </div>
               </div>
                </div>
                
                
            </div>
            
            <div class="clear"></div>
            
            
            <script>
    
    $(".load_comments").click(function() {
    $('.load_comments').removeClass('selected_highlight');
    $(this).addClass('selected_highlight'); 
    var account_tag_id =  $(this).attr('account_tag_id');
    var rating_name = $(this).attr('rating_name');
    var huddle_permission = '<?php echo $huddle_permission ?>';
    var header_name = $(this).attr('header_name');
    if(huddle_permission == '210')
    {
    header_name = $(this).attr('header_name') + ' : ' + rating_name ;
    }
    var tagged_count = $(this).attr('tagged_count');
        
   $.ajax({
                type: 'POST',
                data: {
                 
                    huddle_id: <?php echo $huddle_id; ?>,
                    account_id: <?php echo $account_id; ?>,
                    video_id: <?php echo $video_id; ?>,
                    account_tag_id : account_tag_id,
                },
                url: home_url + '/huddles/load_perfomance_level_comments',
                success: function(response) {
                    
                    if(tagged_count == 'N/O')
                    {
                       $('#vidComments_performance').html(response);
                       $('.performance_before_select').show();
                       $('#header_name_2').text(header_name);
                       $('.click_standard').show();
                       $('#header_name').text('');
                      // $('#not_observed').show();
                       $("#perform_level_icon").attr("src","<?php echo $this->webroot . 'img/performance_levelicon_black.png' ?>");
                       $('#click_standard_text_change').text('This standard is not observed');
                       
                    }
                    

                    else
                        {
                    
                    $('#vidComments_performance').html(response);
                    $('.click_standard').hide();
                    $('#header_name_2').text('');
                    $('#header_name').text(header_name);
                    $('.performance_before_select').hide();
                        }
                    

                },
                errors: function(response) {

                }

            });
            
            });
            
            
        
        
        
       $("#add_rating_btn").click(function(){
       
       <?php //if(IS_QA): ?>
        var metadata = {
          perfomance_level_saved: 'true',
          user_role : '<?php echo $user_role; ?>'

        };
       Intercom('trackEvent', 'perfomance-level-saved', metadata);
       
       <?php //endif; ?>
       
       //var rating_id = $(this).find('option:selected').attr('account_meta_data_id');
      // var rating_value = $(this).find('option:selected').attr('meta_data_value');
       var form=$("#add_rating_form");
       // var standard_id = $(this).attr('standard_id');
        
        $.ajax({
                type:   'POST',
                url:    form.attr("action"),
                data:   form.serialize(),
                dataType: "json",
                
                success: function(response) {
                    $('#performance_level_span_number').show();
                    $('#performance_level_span_number').text(response.rating_score);
                     $('#performance_poup').modal('hide');
                     $('#performance_btn').attr('data-original-title',response.rating_name);
                    

                },
                errors: function(response) {

                }

            });
        
        
        
        
        });
        
        
//        $("#not_observed").click(function() {
//        
//        $.ajax({
//                type: 'POST',
//                data: {
//                 
//                    huddle_id: <?php //echo $huddle_id; ?>,
//                    account_id: <?php //echo $account_id; ?>,
//                    video_id: <?php //echo $video_id; ?>,
//                  
//                },
//                url: home_url + '/huddles/reset_document_ratings',
//                success: function(response) {
//                    $('#performance_level_span_number').show();
//                    $('#performance_level_span_number').text(response);
//                    $('#performance_poup').modal('hide');
//                    
//
//                },
//                errors: function(response) {
//
//                }
//
//            });
//        
//        
//        
//        
//        
//        });
        
 
    
    
</script>

