
<div id="ob-videos-list">
    <p id="notification" style="display: none;padding-left: 29px;padding-right: 9px;padding-bottom: 0px;"></p>
    <ul id="ulDiscussions" class="videos-list observationCls" id="ob-videos-list">
        <?php if (is_array($videos) && count($videos) > 0): ?>
            <?php foreach ($videos as $row): ?>

                <li>
                    <div class="tab-3-box">
                        <div class="tab-3-box-left">
                            <h2><a class="wrap" id="vide-title-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/observation_details/' . $huddle_id . '/' . $row['Document']['id'] ?>" title="<?php echo $row['afd']['title'] ?>" ><?php echo (strlen($row['afd']['title']) > 52 ? mb_substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title']) ?></a></h2>
                            <?php
                            $profile_image = isset($row['User']['image']) ? $row['User']['image'] : '';
                            $profile_image_real_path = WWW_ROOT . "/img/users/$profile_image";

                            //if (file_exists($profile_image_real_path) && $profile_image != '') {
                            if ($profile_image != '') {
                                //$profile_image = $this->webroot . "img/users/$profile_image";
                                $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $row['User']['user_id'] . "/" . $row['User']['image']);
                            } else {
                                //$profile_image = $this->webroot . "img/profile.jpg";
                                $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/profile.jpg');
                            }
                            ?>

                            <div class="frame"><img src="<?php echo $profile_image; ?>" alt="img" height="42px" width="42px"/></div>
                            <div class="text"> <strong class="title"><?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?></strong>

                                <div style="float: right" class="docs-box">

                                </div>
                                <div style="clear: both;"></div>
                                <div>
                                    <strong class="posted">Posted <?php echo date('M d, Y', strtotime($row['Document']['created_date'])) ?></strong>
                                </div>

                            </div>
                        </div>
                    </div>
                </li>

                <?php
                $videoID = $row['Document']['id'];

                $document_files_array = $this->Custom->get_document_url($row['Document']);

                if (empty($document_files_array['url'])) {
                    $row['Document']['published'] = 0;
                    $document_files_array['url'] = $row['Document']['original_file_name'];
                    $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                } else {
                    $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                    @$row['Document']['duration'] = $document_files_array['duration'];
                }

                $videoFilePath = pathinfo($document_files_array['url']);
                $videoFileName = $videoFilePath['filename'];
                ?>
                <li style="display:none;" class="videos-list__item">
                    <?php
                    $isEditable = ($huddle_permission == 200) ||
                            ($huddle_permission == 210 && $user_current_account['User']['id'] == $row['Document']['created_by']) ||
                            ($user_current_account['User']['id'] == $row['Document']['created_by'] && $user_permissions['UserAccount']['permission_maintain_folders'] == '1');
                    ?>
                    <?php if ($isEditable): ?>

                    <?php endif; ?>
                    <div style="display:none;" class="clearfix"></div>
                    <div style="display:none;" class="videos-list__item-thumb">
                        <?php
                        if ($row['Document']['published'] == '1'):

                            $seconds = $row['Document']['duration'] % 60;
                            $minutes = ($row['Document']['duration'] / 60) % 60;
                            $hours = gmdate("H", $row['Document']['duration']);
                            ?>
                            <a href="<?php echo $this->base . '/Huddles/observation_details/' . $huddle_id . '/' . $row['Document']['id'] ?>"  data-document-id="<?php echo $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                <?php
                                $thumbnail_image_path = $document_files_array['thumbnail'];

                                echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                ?>
                                <div class="play-icon"></div>
                                <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                            </a>
                        <?php else: ?>
                            <a href="<?php echo $this->base . '/Huddles/observation_details/' . $huddle_id . '/' . $row['Document']['id'] ?>"  data-document-id="<?php echo $row['Document']['id']; ?>" id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                <div  class="video-unpublished ">
                                    <span class="huddles-unpublished" style="color:#fff;">
                                        <?php if ($row['Document']['encoder_status'] == 'Error'): ?>
                                            Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                        <?php else : ?>
                                            Video is not published.
                                        <?php endif; ?>
                                    </span>
                                </div>
                            </a>
                        <?php endif; ?>
                    </div>
                </li>


            <?php endforeach; ?>

            <script type="text/javascript">

                $(document).ready(function () {

                    $('.regen-thumbnail-image').click(function () {

                        var r = confirm('Do you want to Regenerate Thumbnail?');
                        if (r == true) {

                            var document_id = $(this).attr('document_id');
                            var huddle_id = $(this).attr('huddle_id');
                            $.ajax({
                                type: 'POST',
                                url: home_url + '/Huddles/regenerate_thumbnail/' + document_id + '/' + huddle_id,
                                success: function (res) {

                                    alert('Thumbnail generated successfully.');
                                    d = new Date();
                                    var src_image = $("#img_" + document_id).attr("src");
                                    $("#img_" + document_id).attr("src", src_image + "&dd=" + d.getTime());
                                },
                                errors: function (response) {
                                    alert('Unable to generate Thumbnail, please try again later.');
                                }
                            });
                        }

                    });
                });</script>

        <?php else: ?>
            <li class="videos-list__item_noitem">
                <div style="margin-left: 10px;">No observation(s) have been uploaded to this huddle. </div>
            </li>
        <?php endif; ?>
        <div class="clear">

        </div>
    </ul>
    <?php
//            print $this->element('load_more', array(
//                        'total_items' => $totalVideos,
//                        'count_items' => count($videos),
//                        'current_page' => $current_page,
//                        'number_per_page' => $video_per_page,
//                        'load_more_what'=>'observations'
//    ));
    ?>
</div>