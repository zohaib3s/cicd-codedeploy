<?php

$priviliges_disables = '';
$disabled_class = '';
if ($permission_set == 'not_allow' || $permotion_set == 'not_allow') {
    $priviliges_disables = 'disabled';
    $disabled_class = 'disables-box';
}
$disabled_class2 = '';
if ($permission_set == 'not_allow') {
    $disabled_class2 = 'disables-box';
}

$disabled_role_change_class = $disabled_class;
$priviliges_role_disables = $priviliges_disables;
$permission_role_set = $permission_set;

if ($current_user['users_accounts']['role_id'] == '100' && $users['UserAccount']['role_id'] == '100') {
    $disabled_role_change_class = 'disables-box';
    $priviliges_role_disables = 'disabled';
    $permission_role_set = 'not_allow';
}
$account_owner_disable = '';
$super_user_disable = '';
if ($current_user['roles']['role_id'] == '110') {
    if ($users['roles']['role_id'] == '120') {
        $account_owner_disable = 'disabled';
        $super_user_disable = '';
    } elseif ($users['roles']['role_id'] == '110') {
        $account_owner_disable = 'disabled';
    } else {
        $account_owner_disable = '';
        $super_user_disable = '';
    }
}
if ($current_user['roles']['role_id'] == '120') {
    if ($users['roles']['role_id'] == '120') {
        $account_owner_disable = 'disabled';
        $super_user_disable = 'disabled';
    } elseif ($users['roles']['role_id'] == '110') {
        $account_owner_disable = 'disabled';
    } else {
        $account_owner_disable = '';
        $super_user_disable = '';
    }
}
?>
<?php
if ($huddles && count($huddles) > 0):
    $user_ids = array();
    $role_admin = array();
    $role_user = array();
    $role_viewer = array();
    if ($huddleRoles && count($huddleRoles) > 0) {
        foreach ($huddleRoles as $row) {
            $account_folder_ids[] = $row['AccountFolderUser']['account_folder_id'];
            if ($row['AccountFolderUser']['role_id'] == '200') {
                $role_admin[] = $row['AccountFolderUser']['account_folder_id'];
            }
            if ($row['AccountFolderUser']['role_id'] == '210') {
                $role_user[] = $row['AccountFolderUser']['account_folder_id'];
            }
            if ($row['AccountFolderUser']['role_id'] == '220') {
                $role_viewer[] = $row['AccountFolderUser']['account_folder_id'];
            }
        }
    }
    ?>
    <div>
        <ul>
            <?php
            $count = 0;
            $roles_id = '';
            foreach ($huddles as $row):
                if (!in_array($row['AccountFolder']['account_folder_id'], $role_admin) && !in_array($row['AccountFolder']['account_folder_id'], $role_user) && !in_array($row['AccountFolder']['account_folder_id'], $role_viewer)) {
                    $role_viewer[] = $row['AccountFolder']['account_folder_id'];
                }
                ?>
                <li>
                    <label for="account_folder_ids_<?php echo $row['AccountFolder']['account_folder_id'] ?>">
                        <input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?>   class="checkbox" id="participation_ids_"<?php echo ((isset($associate_huddle_users) && is_array($associate_huddle_users) && count($associate_huddle_users) > 0 && in_array($row['AccountFolder']['account_folder_id'], $associate_huddle_users) !== FALSE) ? 'checked="checked"' : '') ?> name="account_folder_ids[]" type="checkbox" value="<?php echo $row['AccountFolder']['account_folder_id'] ?>">
                        <?php echo $row['AccountFolder']['name'] ?>
                    </label>
                    <div class="permissions">
                        <label for="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_200"><input  type="radio"  <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $role_admin) !== FALSE)) ? 'checked="checked"' : '') ?>  value="200" id="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_200 ?>" name="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>">Admin
                        </label>
                        <label for="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_210"><input type="radio"   <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $role_user) !== FALSE)) ? 'checked="checked"' : '') ?> value="210" id="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_210>" name="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>">Member
                        </label>
                        <label for="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_220"><input type="radio"   <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $role_viewer) !== FALSE)) ? 'checked="checked"' : '') ?> value="220" id="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_220" name="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>">Viewer
                        </label>
                    </div>
                </li>
                <?php $count++; ?>
            <?php endforeach; ?>
        </ul>
    </div>
<?php else: ?>
    <div style="text-align: center;font-size: 15px; color: #000;">No Record Found.</div>
<?php endif; ?>
