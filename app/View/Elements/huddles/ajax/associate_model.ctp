<div class="modal fade" id="associate-videos-model" tabindex="-1" role="dialog" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="add-huddle-video" style="padding: 0">
                <div class="modal-header">
                    <h4 class="header-title nomargin-vertical smargin-bottom">Associate Video</h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">×</a>
                </div>
                <div class="modal-body">
                    <div class="video_upload_dialog">
                        <div class="Associate_Video_btn"><a id="select-from-workspace" href=""><img src="http://qa.sibme.com/img/my_workspace.png" style="top:-2px;" /> Select From Workspace</a></div>
                        <div class="Associate_Video_btn"><a id="select-from-huddle" href=""><img src="http://qa.sibme.com/img/dbi-huddles.png" /> Select From Huddle</a></div>
                    </div>
                    <div class="video_box_workSpace select-from-huddle" style="display: none;width:577px">
                        <div class="workspace_header">
                            <img src="http://qa.sibme.com/img/dbi-huddles.png" style="top:-2px;" /> Select from Huddle
                            <div class="back_btn_dialog" id="back-to-select-container"><img src="/img/icon_backArrow.png" width="12" height="8" alt=""/> Back</div>
                        </div>
                        <input type="text" placeholder="Find Video" class="find_video">
                        <div class="videos_list_box">
                            <?php if ($huddle_videos) { ?>
                                <?php foreach ($huddle_videos as $row) { ?>
                                    <?php
                                    $videoID = $row['Document']['id'];

                                    $document_files_array = $this->Custom->get_document_url($row['Document']);

                                    if (empty($document_files_array['url'])) {
                                        $row['Document']['published'] = 0;
                                        $document_files_array['url'] = $row['Document']['original_file_name'];
                                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                    } else {
                                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        @$row['Document']['duration'] = $document_files_array['duration'];
                                    }

                                    $videoFilePath = pathinfo($document_files_array['url']);
                                    $videoFileName = $videoFilePath['filename'];
                                    ?>
                                    <div class="video_detail">
                                        <div class="video_detail_left">
                                            <input type="checkbox" name="my_file_id" value="<?php echo $row['Document']['id']; ?>" class="select-radio" style="float: left;margin-right: 10px;margin-top: 21px;">
                                            <div class="videos-list__item-thumb">
                                                <?php
                                                if ($row['Document']['published'] == '1'):
                                                    $seconds = $row['Document']['duration'] % 60;
                                                    $minutes = ($row['Document']['duration'] / 60) % 60;
                                                    $hours = gmdate("H", $row['Document']['duration']);
                                                    ?>
                                                    <div  class="video-unpublished " style="height: 55px;width: 74px;">
                                                        <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                                            <?php
                                                            $thumbnail_image_path = $document_files_array['thumbnail'];

                                                            echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                                            ?>
                                                            <div class="play-icon"></div>
                                                            <div style="font-size: 10px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top:  117px;right: 0px;padding: 2px;border-radius: 3px;z-index: 999;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                                                        </a>
                                                    </div>
                                                <?php else: ?>
                                                    <a id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" >
                                                        <div  class="video-unpublished " style="height: 55px;width: 74px;">
                                                            <span class="huddles-unpublished"  style="color:#fff;">
                                                                <?php if ($row['Document']['encoder_status'] == 'Error'): ?>
                                                                    Video failed to process, please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                                                <?php else : ?>
                                                                    <img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br>Your video is currently processing. You will receive an email notification when your video is ready to be viewed.
                                                                <?php endif; ?>
                                                            </span>
                                                        </div>
                                                    </a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="video_detail_text">
                                            <div class="videos-list__item-title">
                                                <a class="wrap1" id="vide-title-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" ><?php echo (strlen($row['afd']['title']) > 52 ? mb_substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title']) ?></a>
                                            </div>
                                            <div class="videos-list__item-author">By <a href="<?php echo empty($row[0]['AutoCreated']) ? '#' : 'mailto:' . $row[0]['WebUploaderEmail']; ?>" title="<?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?>" ><?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?></a></div>
                                            <div class="videos-list__item-added">Uploaded <?php echo date('M d, Y', strtotime($row['Document']['created_date'])) ?></div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                <?php }
                            }
                            ?>
                            <div style="clear: both;"></div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="video_box_workSpace1 select-from-workspace" style="display: none;">
                        <div class="workspace_header">
                            <img src="http://qa.sibme.com/img/my_workspace.png" style="top:-2px;" /> Select From Workspace
                            <div class="back_btn_dialog" id="back-to-select-container">
                                <img src="/img/icon_backArrow.png" width="12" height="8" alt=""/> Back
                            </div>
                        </div>
                        <input type="text" placeholder="Find Video" class="find_video find_video">
                        <div class="videos_list_box">
                            <?php if (is_array($files) && count($files) > 0) { ?>
                                <?php foreach ($files as $row) { ?>
                                    <?php
                                    $videoID = $row['Document']['id'];
                                    $videoFilePath = pathinfo($row['Document']['url']);
                                    $videoFileName = $videoFilePath['filename'];

                                    $document_files_array = $this->Custom->get_document_url($row['Document']);

                                    if (empty($document_files_array['url'])) {
                                        $row['Document']['published'] = 0;
                                        $document_files_array['url'] = $row['Document']['original_file_name'];
                                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        $row['Document']['duration'] = '00:00';
                                    } else {
                                        $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        @$row['Document']['duration'] = $document_files_array['duration'];
                                    }
                                    ?>

                                    <div class="video_detail">
                                        <div class="video_detail_left">
                                            <input type="checkbox" name="my_file_id" value="<?php echo $row['Document']['id']; ?>" class="select-radio" style="float: left;margin-right: 10px;margin-top: 21px;">
                                            <div class="videos-list__item-thumb" style="">
                                                <?php
                                                if ($row['Document']['published'] == '1') :
                                                    $seconds = $row['Document']['duration'] % 60;
                                                    $minutes = ($row['Document']['duration'] / 60) % 60;
                                                    $hours = gmdate("H", $row['Document']['duration']);
                                                    ?>
                                                    <div  class="video-unpublished " style="height: 55px;width: 74px;">
                                                        <a id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/MyFiles/view/1/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                            <?php
                                                            if ($row['Document']['encoder_provider'] == '2') {
                                                                $path = "uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName .
                                                                        (empty($row['Document']['thumbnail_number']) ?
                                                                                '_thumb_00001.png' :
                                                                                '_thumb_' . sprintf('%05d', $row['Document']['thumbnail_number']) . '.png'
                                                                        );
                                                            } else {
                                                                $path = "uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_thumb.png";
                                                            }

                                                            $thumbnail_image_path = $document_files_array['thumbnail'];

                                                            echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                                            ?>
                                                            <div class="play-icon"></div>
                                                            <div style="font-size: 10px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top:  41px;right: 0px;padding: 2px;border-radius: 3px;z-index: 999;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                                                        </a>
                                                    </div>

        <?php else: ?>
                                                    <a id="processing-message-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/MyFiles/view/1/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                        <div class="video-unpublished" style="height: 55px;width: 74px;">
                                                            <span>
                                                                <?php if ($row['Document']['encoder_status'] == 'Error'): ?>
                                                                    Video failed to process, please try again.
                                                                <?php else : ?>
                                                                    Video is currently processing.
            <?php endif; ?>
                                                            </span>
                                                        </div>
                                                    </a>
        <?php endif; ?>
                                            </div>
                                            <div style="clear: both;"></div>
                                        </div>
                                        <div class="video_detail_text">
                                            <div class="videos-list__item-title">
                                                <a class="wrap1" id="vide-title-<?php echo $row['Document']['id']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/1/' . $row['Document']['id']; ?>" title="<?php echo $row['afd']['title'] ?>" ><?php echo (strlen($row['afd']['title']) > 52 ? mb_substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title']) ?></a>
                                            </div>
                                            <div class="videos-list__item-author">By <a href="<?php echo empty($row[0]['AutoCreated']) ? '#' : 'mailto:' . $row[0]['WebUploaderEmail']; ?>" title="<?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?>" ><?php echo empty($row[0]['AutoCreated']) ? $row['User']['first_name'] . " " . $row['User']['last_name'] : $row[0]['WebUploaderName']; ?></a></div>
                                            <div class="videos-list__item-added">Uploaded <?php echo date('M d, Y', strtotime($row['Document']['created_date'])) ?></div>
                                            <div class="clear"></div>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>
                                <?php }
                            }
                            ?>
                            <div style="clear: both;"></div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
                    <button type="button" id="add_video" class="btn btn btn-green"  style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>; width: 99px;border-radius: 5px; display: none;">Select </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
