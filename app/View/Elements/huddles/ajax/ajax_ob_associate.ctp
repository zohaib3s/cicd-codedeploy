<?php

$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
//$result['s'] = '11586';
//$date = gmdate("H:i:s", (int)$result['s']);
//print_r($date);
//$date = explode(':',$date);
//print_r($date);
//die();

?>

<style>
    .stop_observation_container{
        margin: 10px;
    }
    .printable_icons {
        border-bottom: solid 1px #E0E0E0;
        margin-bottom: 15px;
        padding:0px 0px 10px 0px;
    }
</style>


<div class="stop_observation_container" <?php if($processed_check>0 && $processed_check!=4){ echo 'style="display:none;"'; }else{ echo 'style="display:block;"'; }  ?>>
    <div class="coach_cochee_container" >
        <a href="<?php echo $this->base.'/Huddles/view/'.$huddle_id.'/5'?>" class="back huddle_videos_all">Back to all observations</a>            
        <div class="clear"></div>           
    </div>


    <div id="commentsTab1" style="visibility: visible;">

        <div id="ObservationTitle1" name="click to edit title" style="font-weight: bold;width: 410px;position: absolute;top: 37px;">
            Observation_note_<?php echo date('m-d-Y');?>
        </div>  

        <!--        <div class="printable_icons" style="">
                    <a href="#"><img src="/img/email.png"></a>
                    <a href="/Huddles/print_pdf_comments/35491/2/2.pdf"><img src="/img/icons/pdf48.png" style="width:28px;"></a>
        
                    <a href="/Huddles/print_excel_comments/35491/2"><img src="/img/icons/excel48.png" style="width:28px;"></a>
                </div>-->


        <div class="call_notes_container call" id="stop-call-notes">
                 <?php echo $comments_html?>
        </div></div>
</div> 



<div class="observation_left" <?php if($processed_check>0 && $processed_check!=4){ echo 'style="display:block;"'; }else{ echo 'style="display:none;"'; }  ?>>


    <div class="coach_cochee_container" style="margin-top:0px;">  
        <a href="<?php echo $this->base.'/Huddles/view/'.$huddle_id.'/5'?>" class="back huddle_videos_all back_to_all_observations">Back to all observations</a>           
        <div class="clear"></div>           
    </div>

    <div class="doted_container">
        <div id="ObservationTitle" name="click to edit title" style="font-weight: bold;">Observation_note_<?php echo date('m-d-Y');?></div>
        <div class="editArea">
            <input id="ajaxInput" value="Observation_note 12-12-1234" type="text">               
        </div>
        <input type="hidden"  name="video_id" value="<?php echo $video_id ?>" id="video_id"/>
    </div>
    <div class="timer_container">
        <h3><?php echo date('M-d-Y');?></h3>
        <div class="timer_box">
            <span id="sw_h">00</span>:<span id="sw_m">00</span>:<span id="sw_s">00</span>               
        </div>
        <div class="start_btn_box">        
            <button type="button" id="sw_start" class="start_time_btn">Start</button> 
            <button type="button" id="sw_pause" class="pause_time_btn">Pause</button> 
            <button type="button" id="sw_stop" class="stop_time_btn">Stop</button>
        </div>
    </div>
    <div class="note_box">
        <div class="timer_box2" style="display: none;">00:00:00</div>            
        <textarea  cols="50" id="observations-comments" required name="comment[comment]" placeholder="Add a comment..." rows="4" style="position: relative; outline: 0px; background: transparent;border: none;box-shadow: 0 0px 0px rgba(0,0,0,0.09) inset;" ></textarea>
        <div class="tags divblockwidth">
            <div class="video-tags-row row">
                <input type="text" name="txtVideostandard1" data-default="Tag Standard..." id="txtVideostandard1" value="" placeholder="" style="width:464px !important"  disabled="disabled"  required/>
            </div>
            <div class="clear" style="clear: both;"></div>
        </div>
        <div class="tags divblockwidth">
            <div class="video-tags-row row">
                <input type="text" name="txtVideoTags1" data-default="Tags..." id="txtVideoTags1" value="" placeholder=""  style="width:112px !important;" readonly="readonly" disabled="disabled"/>
            </div>
            <div class="clear" style="clear: both;"></div>
        </div>
        <div class="clear" style="clear: both;"></div>
    </div>
    <div class="input-group" style="text-align:right;">
        <input id="add-notes" type="submit" name="submit" value="Add Note" class="btn btn-green" style="height:35px;">
        <div class="clear" style="clear: both;"></div>
    </div> 
    <div class="clear" style="clear: both;"></div>        


</div>






<div class="observation_right" <?php if($processed_check>0 && $processed_check!=4){ echo 'style="display:block;"'; }else{ echo 'style="display:none;"'; }  ?>>
    <div class="right-box2">
        <ul class="tabset commentstabs">
            <li class="">
                <a id="comments" class="tab active" href="#commentsTab">
                    Notes (<?php echo $comments_html ? count($comments_html) : '0'; ?>)
                </a>
            </li>
                <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                    <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
            <li class="">
                <a id="frameWorkss" class="tab " href="#frameWorksTab">
                    Framework
                </a>
            </li>
                    <?php endif; ?>
                <?php endif; ?>

        </ul>
        <div class="clear"></div>
        <div id="commentsTab" style="visibility: visible;padding:10px 0px;"> 
            <div class="no_notes">No Notes Added Yet</div>


            <div id="observation-comments"></div> 
        <?php echo $comments_html?>
        </div>
        <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
           <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
        <div class="tab-content tab" id="frameWorksTab" style="visibility: visible;">
            <div class="search-box standard-search" style="position: relative;width:100%;">
                <input type="button" id="btnSearchTags" class="btn-search" value="">
                <input class="text-input observation_search_bg" id="txtSearchTags" type="text" value="" placeholder="Search Standards..." style="margin-right: 0px;width: 94% !important;">
                <span id="clearTagsButton" class="clear-video-input-box" style="display:none;right: 33px;top: 20px;">X</span>
            </div>
            <div id="scrollbar1" style="float: left;">
                <div class="viewport short" style="overflow:scroll;overflow-x: hidden;">
                    <div class="overview p-left0" style="top: 0px;padding: 0px;">
                        <div id="listContainer">
                            <style type="text/css">
                                .standardRed {
                                    color: red;display: inline;
                                }
                                .standardBlue {
                                    color: blue;display: inline;
                                }
                                .standardBlack {
                                    color: #000;display: inline;
                                }
                                .standardOrange {
                                    color: orange;display: inline;
                                }
                                .standardGreen {
                                    color: green;display: inline;
                                }
                                .frame_work_heading{
                                    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
                                    font-weight: 300 !important;
                                    font-size: 20px !important;
                                    margin:10px 0px !important;
                                    cursor:auto !important;
                                }
                            </style>
                            <ul id="expList_3" class="expList_3" style="padding-left:0px">

                                            <?php 
                                            if(!empty($standardsL2)){
                                                for($i = 0; $i < count($standardsL2);$i++){ ?>
                                <li class="frame_work_heading" ><?php echo $standardsL2[$i]['AccountTag']['tag_code'].' - '.$standardsL2[$i]['AccountTag']['tag_title'];?></li>
                                                    <?php
                                                    foreach ($standards as $standard) { 
                                                        if($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['parent_account_tag_id'] || ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['framework_id'])){
                                                        ?>
                                <li class="standard standard-cls" >
                                    <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                            <?php
                                                            $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                            echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                            ?>                                        
                                </li>

                                                    <?php }
                                                    }
                                                }    
                                            }else{
                                                if(!empty($standards)){
                                              foreach ($standards as $standard) { ?>
                                <li class="standard standard-cls">
                                    <input type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                        <?php
                                                        $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                        echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                        ?>                                        
                                </li>

                                            <?php } }}?> 
                                <li id="noresults" style="width: 280px;">No standards match your search criteria.</li>                             
                            </ul>
                            <script type="text/javascript">

                                $(document).ready(function () {
                                    $('.standard input').on('change', function () {
                                        var tag_code = $(this).attr('st_code');
                                        var tag_value = '';
                                        var tag_array = {};
                                        var tag_name = $(this).attr('st_name');
                                        tag_array = tag_name.split(" ");
                                        if ($(this).is(':checked')) {
                                            tag_array = tag_name.split(" ");
                                            tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                            $('#txtVideostandard').addTag(tag_value);
                                            if ($('input[name="name1"]:checked').length > 0) {
                                                $('#txtVideostandard_tag').hide();
                                            }
                                        } else {
                                            tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                            $('#txtVideostandard').removeTag(tag_value);
                                        }
                                    });
                                });</script>
                        </div>
                    </div>
                </div>

            </div>

        </div>
                    <?php endif; ?>
                <?php endif; ?> 
    </div> 

</div>

<script>
    $(document).ready(function () {
        setInterval(function ()
        {
            var video_id = $("#video_id").val();
            $.ajax({
                type: 'post',
                url: home_url + '/Huddles/mobile_video/' + video_id,
                data: {video_id: $("#video_id").val()},
                dataType: 'json',
                success: function (data)
                {
                    //do something with response dataalert
                    console.log(data.Document.current_duration);

                    var date = new Date(null);
                    date.setSeconds(data.Document.current_duration); // specify value for SECONDS here
                    var time = date.toISOString().substr(11, 8);
                    console.log(time);
                    time = time.split(":");
                    console.log(time[1]);

                    $("#sw_h").html(time[0]);
                    $("#sw_m").html(time[1]);
                    $("#sw_s").html(time[2]);
                    console.log(data.Document.is_processed);
                    if (data.Document.is_processed === '4')
                    {
                        $(".observation_left").hide();
                        $(".observation_right").hide();
                        $(".stop_observation_container").show();

                        //$(".stop_observation_container").html($("#observation-comments").html());

                    }


                }
            });
        }, 3000);//time in milliseconds



    });

</script>   
<script type="text/javascript">
    $(document).ready(function (e) {
        function sbtfrm() {
            $("ul#expList_3").find('input:checkbox').removeAttr('checked');
        }
        $('#txtVideoTags1').tagsInput({
            defaultText: 'Tags...',
            width: '200px',
            placeholderColor: '#898686'
        });
        $('.video-tags-row label').css('display', 'none');
        $('#txtVideostandard1').tagsInput({
            defaultText: 'Tag Standards...',
            width: '350px',
            readonly_input: true,
            placeholderColor: '#898686',
            onRemoveTag: function (val) {
                if ($("div[id=txtVideostandard1_tagsinput]").children('.tag').length < 1) {
                    $('#txtVideostandard1_tag').show();
                }

                var removed_tag_val = val;
                var checkboxes = $('#expList_3 li input[type="checkbox"]');
                for (var i = 0; i < checkboxes.length; i++) {

                    var chkStandard = $(checkboxes[i]);
                    var tag_code = chkStandard.attr('st_code');
                    var tag_value = '';
                    var tag_array = {};
                    var tag_name = chkStandard.attr('st_name');
                    tag_array = tag_name.split(" ");
                    if (tag_array && tag_array.length > 0) {

                        var tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                        if (removed_tag_val == tag_value) {
                            chkStandard.prop('checked', false);
                        }
                    }

                }

            },
        });

    });
    $(function () {

        $('input#txtSearchTags').quicksearch('ul#expList_3 li', {
            noResults: 'li#noresults',
            'onAfter': function () {
                if (typeof $('#txtSearchTags2').val() !== "undefined" && $('#txtSearchTags2').val().length > 0) {
                    $('#clearTagsButton1').css('display', 'block');
                }
            }
        });

    });

</script>








