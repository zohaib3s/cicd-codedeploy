<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
?>
<h1 class="page-title nomargin-top leftcls"><?php echo $page_title ? $page_title : ''; ?> <span class="page-title__counter"><?php echo '( ' . $total_observ . ' )' ?></span></h1>

<?php if ($user_current_account['roles']['role_id'] != '120' || $user_permissions['UserAccount']['permission_administrator_observation_new_role'] == '1'): ?>
    <a id="btn-new-huddle" href="<?php echo $this->base . '/Observe/add' ?>" class="btn btn-green right"><?php echo $new_huddle_button ? $new_huddle_button : ''; ?> </a>
    <?php
endif;
$params = $this->request->pass;
if (isset($this->request->data["txtSearchObservations"]))
    $huddleSearch = $this->request->data["txtSearchObservations"];
else
    $huddleSearch = '';

//SETTING ACTIVE CLASS FOR SORT DROP DOWN
if (is_array($params) && count($params) > 0) {
    $sortParam = $params[0];
} else {
    $sortParam = 'all';
}
if ($sortParam == 'all') {
    $noSort = 'active';
    $nameSort = '';
    $dateSort = '';
} elseif ($sortParam == 'name') {
    $noSort = '';
    $nameSort = 'active';
    $dateSort = '';
} elseif ($sortParam == 'date') {
    $noSort = '';
    $nameSort = '';
    $dateSort = 'active';
}
//SETTING ACTIVE CLASS FOR SORT DROP DOWN
if (is_array($params) && count($params) > 0) {
    $filterParam = $params[1];
} else {
    $filterParam = 'all';
}
if ($filterParam == 'all') {
    $noFilter = 'active';
    $pastFilter = '';
    $futureFilter = '';
} elseif ($filterParam == 'past') {
    $noFilter = '';
    $pastFilter = 'active';
    $futureFilter = '';
} elseif ($filterParam == 'future') {
    $noFilter = '';
    $pastFilter = '';
    $futureFilter = 'active';
}
?>

<div id="sort-mode" class="dropdown right mmargin-right">
    <ul>
        <li id='none'><a href="<?php echo $this->base . '/Observe/index/all/' . $filterParam ?>" title="View all" class="<?php echo $noSort ?>">Sort</a></li>
        <li id='name'><a href="<?php echo $this->base . '/Observe/index/name/' . $filterParam; ?>" class="<?php echo $nameSort ?>">By Title</a></li>
        <li id='date'><a href="<?php echo $this->base . '/Observe/index/date/' . $filterParam; ?>" class="<?php echo $dateSort ?>">By Observation Date</a></li>
    </ul>
</div>

<div id="filter-mode" class="dropdown right mmargin-right">
    <ul>
        <li id='all' class="allimgcls">   <a href="<?php echo $this->base . '/Observe/index/' . $sortParam . '/all'; ?>" title="View All" class="<?php echo $noFilter ?>"><img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/filter-a-a.png'); ?>">Show All</a></li>
        <li id='past'>  <a href="<?php echo $this->base . '/Observe/index/' . $sortParam . '/past'; ?>" class="<?php echo $pastFilter ?>">Past Observations</a></li>
        <li id='future'><a href="<?php echo $this->base . '/Observe/index/' . $sortParam . '/future'; ?>" class="<?php echo $futureFilter ?>">Future Observations</a></li>
    </ul>
</div>

<div id="view" class="mmargin-right right">
</div>
<div class="search-box12 ob-search">
    <input type="button" id="btnObservationSearch" action="" class="btn-search" value="" style="width:27px;"/>
    <input class="text-input" name="txtSearchObservations" id="txtSearchObservations" type="text" value="" placeholder="Search Observations..." style="margin-right: 25px;"/>
    <span id="clearSearchObservation"  style="display: none;" class="clear-video-input-box cross-search search-x-huddles">X</span>
</div>
    <br clear="both"/>
<hr class="style2">
<style>
    .cross-search{
        margin-left: 6px;
        position: absolute;
        right: 54px;
        width: 327px;
        cursor: pointer;
    }
</style>
