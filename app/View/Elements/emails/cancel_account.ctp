<div id=":6r" style="overflow: hidden;">
    <p>
        You have canceled your <?php echo $this->Custom->get_site_settings('site_title','en') ?> account and all associated user accounts. Once your plan subscription period expires all account data will be deleted and will not be recoverable.
    </p>
    <br>
    <p>We appreciate your business and feedback.</p>
    <p>Best,</p>
    <br>
    <p>The <?php echo $this->Custom->get_site_settings('site_title','en') ?> team,</p>
</div>