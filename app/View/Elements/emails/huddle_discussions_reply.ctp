<?php
$sibme_base_url = Configure::read('sibme_base_url');
$users = $this->Session->read('user_current_account');

$profile_image = isset($users['User']['image']) ? $users['User']['image'] : '';
$profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . "img/profile.jpg");
}
?>
<table>
    <tbody>
        <tr>
            <td style="width: 100px;">
                <p style="margin-bottom: 226px;">
                    <img src="<?php echo $_SERVER['HTTP_HOST'] . $profile_image; ?>" alt="img" height="75px" width="75px"/>
                </p>
            </td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td style="padding-top: 0px; padding-bottom: 10px;">
                                <strong><?php echo $data['created_by'] ?> posted a new message</strong>.
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;">
                                <?php echo $data['message'] ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;">
                                <strong>Click the link below to view the discussion thread. </strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;">
                                <a href="<?php echo $data['discussion_link']; ?>">Click here to view discussion</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <strong>People participating in Huddle include:</strong>
                                    <?php
                                    if (isset($data['participting_users']) && count($data['participting_users']) > 0) {
                                        $user_counter = 0;
                                        foreach ($data['participting_users'] as $row) {
                                            if ($user_counter > 0)
                                                echo " , ";
                                            echo $participated_user = $row['User']['first_name'] . " " . $row['User']['last_name'];
                                            $user_counter +=1;
                                        }
                                    }
                                    ?>
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Delivered by <?php $this->Custom->get_site_settings('site_title') ?> <a href="<?php echo $_SERVER['HTTP_HOST']; ?> " target="_blank">www.sibme.com</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>&#160;</td>
            <td>
                <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id; ?>/3"> Unsubscribe Now  </a>
            </td>
        </tr>
    </tbody>
</table>