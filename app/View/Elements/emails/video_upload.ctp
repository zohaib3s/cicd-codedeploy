<?php

$base_url = $this->base;
if (!empty($result['video_url'])) {
    $imageUrl = $result['video_url'];
} else {
    $imageUrl = 'https://d1c4hn49irt1g3.cloudfront.net/app/img/profile.jpg';
}
$table_html = '<table><tbody><tr><td style=\"width: 100px;\"><p>';
$table_html .='<img src="' . $imageUrl . '" alt=\"img\" height=\"75px\" width=\"75px\"/></p></td>';
$table_html .='<td><table><tbody><tr><td style=\"padding-top: 0px; padding-bottom: 10px;\">';


$table_html .="<table><tbody><tr><td style=\"width: 100px;\"><p>";
$table_html .="<img src=\"$imageUrl\" alt=\"img\" height=\"75px\" width=\"75px\"/></p></td>";
$table_html .="<td><table><tbody><tr><td style=\"padding-top: 0px; padding-bottom: 10px;\">";
if ($folder_type == 1) {
    //Huddles
    if ($doc_type == 1 && $accountFolderMetaValue == "3") {
        $table_html .= "<strong>$creator_name added a video to the Evaluation Huddle: $huddle_name</strong>";
    } else if ($doc_type == 1 && $accountFolderMetaValue == "2") {
        $table_html .="<strong>$creator_name added a video to the Coaching Huddle: $huddle_name</strong>";
    } else if ($doc_type == 3 && $accountFolderMetaValue == "2") {
        $table_html .="<strong>$creator_name completed a live note video observation in the Coaching Huddle: $huddle_name</strong>";
    } else if ($doc_type == 1 && $accountFolderMetaValue == "1") {
        $table_html .="<strong>$creator_name added a video to the Collaboration Huddle: $huddle_name</strong>";
    } else {
        $table_html .="<strong>$creator_name added a video in the following:  $huddle_name</strong>";
    }
} else if ($folder_type == 2) {
    $table_html .="<strong>You added a video to the $account_title Video Library: $huddle_name </strong>";
} else {
    //workspace items
    if ($doc_type == 3) {
        $table_html .="<strong>You published synced notes in your Workspace</strong>";
    } else {
        $table_html .="<strong>You added a video to your Workspace: $video_title</strong>";
    }
}
if ($doc_type == 3 && $accountFolderMetaValue == "3") {
    //Click the link below to review the Video before publishing it for
    $table_html .="</td></tr><tr><td><strong>Click the link below to review the Video.</strong></td></tr><tr><td><h2>";
} else if ($doc_type == 3 && $accountFolderMetaValue == "2") {
    //Click the link below to review the Video before publishing it for
    $table_html .="</td></tr><tr><td><strong>Click the link below to review the Video before publishing it for $huddle_name.</strong></td></tr><tr><td><h2>";
} else {
    $table_html .="</td></tr><tr><td><strong>Click the link below to view the Video.</strong></td></tr><tr><td><h2>";
}
if ($folder_type == 1) {
    if ($doc_type == 3) {
        $table_html .="<a href=\"$base_url/huddles/observation_details_3/$account_folder_id/$document_id\">Click to view</a>";
    } else {
        $table_html .="<a href=\"$base_url/huddles/view/$account_folder_id/1/$document_id\">Click to view</a>";
    }
} else if ($folder_type == 2) {
    $table_html .="<a href=\"$base_url/VideoLibrary/view/$account_folder_id\">Click to view</a>";
} else {

    $table_html .="<a href=\"$base_url/MyFiles/view/1/$account_folder_id\">Click to view</a>";
}
$table_html .="</h2></td></tr>";
// participants
if ($accountFolderMetaValue != "3") {

    if ($huddleParticipants != null && count($huddleParticipants) > 0) {
        $table_html .="<tr><td>Other people participating in Huddle include: ";
        for ($i = 0; $i < count($huddleParticipants); $i++) {
            $participant_name = $huddleParticipants[$i]['first_name'] . ' ' . $huddleParticipants[$i]['last_name'];
            if ($i > 0) {
                $table_html .=", ";
            }
            $table_html .=$participant_name;
        }
        $table_html .="</td></tr>";
    }
}
$table_html .="<tr><td>Delivered by " . $this->Custom->get_site_settings('site_title') . " <a href=\"https://www.sibme.com\" target=\"_blank\">www.sibme.com</a></td></tr></tbody></table></td></tr><tr><td>&nbsp;</td><td>";
$table_html .="<a href=\"$base_url/subscription/unsubscirbe_now/$user_id/$email_format_id\"> Unsubscribe Now  </a></td></tr></tbody></table>";
echo $table_html;
?>