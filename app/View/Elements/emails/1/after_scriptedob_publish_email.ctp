
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Sibme - Video Observation Published</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style>
            img { max-width:100%;}
        </style>

    </head>
    <?php
    if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'];
    } else {
        $redirect = 'http://' . $_SERVER['HTTP_HOST'];
    }
    ?>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial, Helvetica, sans-serif;">

            <tr>

                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                        <tr>
                            <td style="text-align:center; padding:30px 0;">
                                <img src="<?php echo $redirect; ?>/img/logo-dark.png" alt="Sibme" width="180" />
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; font-size:16px; padding:30px 0; line-height:26px; font-family:Arial, Helvetica, sans-serif;">



                                <b><?php echo $creator['User']['first_name'] . " " . $creator['User']['last_name'] ?></b> <br />
                                published a video observation <br />
                                <b><?php echo $huddle_name; ?></b>



                            </td>

                        </tr>

                        <tr>
                            <td style="text-align:center; padding:10px 0 30px 0;">
                                <a href="<?php echo $redirect . "/huddles/observation_details_1/" . $huddle_id . "/1/" . $video_id; ?>" title="Click to View Observation"> <img src="<?php echo $redirect; ?>/img/hmh_observation.png" width="330" height="50" alt="Click to View Observation " /></a>
                            </td>
                        </tr>



                        <tr>
                            <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
                                <a href="<?php echo $redirect; ?>/subscription/unsubscirbe_now/<?php echo $user_id; ?>/6" style="color:#616161; text-decoration:underline;">Unsubscribe Now</a> <br /><br />
                                <a href="www.sibme.com" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                            </td>
                        </tr>




                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>