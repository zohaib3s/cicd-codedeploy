<div id=":6r" style="overflow: hidden;">
    <p>Hi <?php echo ($first_name ? $first_name : '') . ' ' . ($last_name ? $last_name : '') ?>,</p>
    <p>
        We hope you enjoyed your free trial.  Unfortunately, your trial has expired.  Your account will be locked for 30 days.  After 30 days, your account will be wiped and deleted forever.
    </p>

    <a href="<?php echo $_SERVER['HTTP_HOST']; ?>/app/accounts/account_settings_all/<?php echo $account_id ?>/4"><strong>One Last Chance to Subscribe Now!</strong></a><br/>
    <br/>
    <p>Was there something we could have done better?  Of course we would love to have you back at anytime. If you change your mind, just let us know.</p>
    <p>Have questions or need help?  Please email us at <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['info']; ?>" target="_blank"><?php echo $this->custom->get_site_settings('static_emails')['info']; ?></a></p>
    <p>Having technical issues?  Please email us at  <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" target="_blank"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a></p>
    <br/>
    <p>Sorry to see you go,</p>
    <br/>
    <p>The <?php $this->Custom->get_site_settings('site_title') ?> team,</p>
    <p><a target="_blank" href="//<?php echo $this->custom->get_site_settings('site_url'); ?>"><?php echo $this->custom->get_site_settings('site_url'); ?></a></p>
    <div class="yj6qo"></div>
    <div class="adL">
    </div>

</div>