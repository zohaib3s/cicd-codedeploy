<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo $this->Custom->get_site_settings('site_title') ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style>
            img { max-width:100%;}
        </style>

    </head>
    <?php
    if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'];
    } else {
        $redirect = 'http://' . $_SERVER['HTTP_HOST'];
    }
    ?>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';">
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                        <tr>
                            <td style="text-align:center; padding:20px 0;">
                                <img src="<?php echo $redirect ?>/img/logo-dark.png" width="180" height="87"/>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; font-size:16px; padding:20px 0;">
                                <b style="font-size:20px;">Carlos Afanador </b><br />
                                added a resource in the following huddle: <b style="font-size:20px">Assessing Carlos Again</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding:20px 0;">
                                <img src="<?php echo $redirect ?>/img/view_partic2.png" />
                            </td>
                        </tr>

                        <tr>
                            <td style="font-size: 15px; text-align: center; color: #212121; padding-top:25px;">
                                Delivered by Sibme www.sibme.com
                            </td>
                        </tr>

                        <tr>
                            <td style="font-size: 15px; text-align: center; color: #212121; padding:20px 0;">
                                Unsubscribe Now
                            </td>
                        </tr>


                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>