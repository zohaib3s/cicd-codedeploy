
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Sibme</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style>
            img { max-width:100%;}
        </style>
    </head>
    <?php
    if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'];
    } else {
        $redirect = 'http://' . $_SERVER['HTTP_HOST'];
    }
    ?>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';">
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                        <tr>
                            <td style="text-align:center; padding:20px 0;">
                                <img src="<?php echo $redirect ?>/img/logohmh.png"  width="320" height="59"/>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; font-size:16px; padding:20px 0;">

                                <?php if ($htype == 2) : ?>
                                    <strong style="font-size:20px;">
                                        <?php echo $users['User']['first_name'] . " " . $users['User']['last_name']; ?>
                                    </strong>
                                    <!-- <br/> 
                                    added a video to the Coaching Huddle 
                                    <br/> -->
                                    <br/> 
                                    added a video to the Huddle 
                                    <br/>
                                    <b><?php echo $data['huddle_name']; ?></b>
                                <?php elseif ($htype == 3) : ?>
                                <strong style="font-size:20px;">
                                    <?php echo $users['User']['first_name'] . " " . $users['User']['last_name']; ?>
                                </strong>
                                    <!-- <br/>  
                                        added a video to the Assessment Huddle 
                                    <br/> -->
                                    <br/>  
                                    added a video to the Huddle 
                                    <br/>
                                    <b><?php echo $data['huddle_name']; ?></b>
                                <?php else : ?>
                                <strong style="font-size:20px;">
                                    <?php echo $users['User']['first_name'] . " " . $users['User']['last_name']; ?>
                                </strong>
                                    <!-- <br/>
                                    added a video to the Collaboration Huddle
                                    <br/> -->
                                    <br/>
                                    added a video to the Huddle
                                    <br/>
                                    <b><?php echo $data['huddle_name']; ?></b>
                                <?php endif; ?>
                            </td>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding:20px 0;">
                                <a href="<?php echo $redirect . $data['video_link']; ?>"> <img src="<?php echo $redirect ?>/img/view_video.png" /> </a>
                            </td>
                        </tr>

                        <tr>
                            <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
                                <a target="_blank" href="<?php echo $redirect; ?>/subscription/unsubscirbe_now/<?php echo $user_id; ?>/7" style="color:#616161; text-decoration:underline;">Unsubscribe Now</a> <br /><br />
                                <a href="www.sibme.com" target="_blank" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                            </td>
                        </tr>


                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>