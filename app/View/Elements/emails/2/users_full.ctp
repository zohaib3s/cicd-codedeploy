<html xmlns="http://www.w3.org/1999/xhtml">
    <style>
        img { max-width:100%;}
    </style>

    <?php
    if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'];
    } else {
        $redirect = 'http://' . $_SERVER['HTTP_HOST'];
    }
    ?>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial, Helvetica, sans-serif;">

            <tr>

                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                        <tr>
                            <td style="text-align:center; padding:30px 0;">
                                <img src="<?php echo $redirect ?>/img/logohmh.png" width="320" height="59"/>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left; font-size:16px; padding:30px 0; line-height:26px; font-family:Arial, Helvetica, sans-serif;">
                                Your Sibme account has reached its user limit of <b><?php echo $this->Custom->get_allowed_users($account_id); ?> users</b>.<br/>
                                Please contact <b><?php echo $account_owner_email ?>,</b> the HMH Coaching Studio account owner, for further assistance.
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
                                <a href="www.sibme.com" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>