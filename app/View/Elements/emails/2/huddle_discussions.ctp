<html xmlns="http://www.w3.org/1999/xhtml">
    <style>
        img { max-width:100%;}
    </style>

    <?php
    if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'];
    } else {
        $redirect = 'http://' . $_SERVER['HTTP_HOST'];
    }
    ?>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial, Helvetica, sans-serif;">

            <tr>

                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                        <tr>
                            <td style="text-align:center; padding:30px 0;">
                                <img src="<?php echo $redirect ?>/img/logohmh.png"  width="320" height="59"/>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; font-size:16px; padding:30px 0; line-height:26px; font-family:Arial, Helvetica, sans-serif;">
                                <b> <?php echo $data['created_by'] ?></b> <br />
                                created a new discussion topic <br />
                                <b> <?php echo $data['discussion_topic']; ?></b> in <b> <?php echo $data['huddle_name'] ?></b>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding:10px 0 30px 0;">
                                <a href="<?php echo $data['discussion_link']; ?>" title="Click to view discussion"> <img src="<?php echo $redirect ?>/img/huddle_discussion_hmh.png" width="330" height="50" alt="Click to view discussion" /></a>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
                                <a href="<?php echo $redirect; ?>/subscription/unsubscirbe_now/<?php echo $user_id; ?>/2" style="color:#616161; text-decoration:underline;">Unsubscribe Now</a> <br /><br />
                                <a href="www.sibme.com" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>