<?php
$sibme_base_url = Configure::read('sibme_base_url');
$users = $this->Session->read('user_current_account');

$profile_image = isset($users['User']['image']) ? $users['User']['image'] : '';
$profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . "img/profile.jpg");
}

if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
    $redirect = 'https://' . $_SERVER['HTTP_HOST'];
} else {
    $redirect = 'http://' . $_SERVER['HTTP_HOST'];
}

if($data['huddle_type']=='1'){
    $huddle_type = 'Collaboration';
}elseif($data['huddle_type']=='2'){
    $huddle_type = 'Coaching';
}elseif($data['huddle_type']=='3'){
    $huddle_type = 'Assessment';
}
?>
<?php if ($data['huddle_type'] == '3') {
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>HMH - Assessment Huddle Invitation</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style>
    img { max-width:100%;}
  </style>
  
</head>
<body style="margin: 0; padding: 0;">
 <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial, Helvetica, sans-serif;">
 
  <tr>
  
   <td>
    	<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
        
         <tr>
          <td style="text-align:center; padding:30px 0;">
             <img src="<?php echo $redirect;?>/img/logohmh.png" alt="HMH" width="320" height="59"/>
          </td>
          </tr>
          
          <tr>
          <td style="text-align:center; border-bottom:1px solid #ddd;">
           
          </td>
          </tr>
          
		  <tr>
		    <td style="text-align:center; font-size:16px; padding:30px 0; line-height:26px; font-family:Arial, Helvetica, sans-serif;">
            


<b><?php echo $data['User']['first_name'] . " " . $data['User']['last_name'] ?> </b> <br />
invited you to participate in an assessment huddle<br />
<b><?php echo $data['AccountFolder']['name']; ?></b><br />
Due: <?php echo $data['submission_deadline_date'].' at '.$data['submission_deadline_time']; ?>

         
     
    
     </td> 
           
		 </tr>
         
          <tr>
            <td style="text-align:center; padding:10px 0 30px 0;">
              <a href="<?php echo $sibme_base_url; ?>/Huddles/view/<?php echo $data['AccountFolder']['account_folder_id']; ?>" title="Click here to start participating"> <img src="<?php echo $redirect;?>/img/view_partic.png" width="330" height="50" alt="Click here to start participating " /></a>
            </td>
          </tr>
         
          <tr>
            <td style="font-size: 14px; text-align: left; color: #3C4147; padding-top:0px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
              If you have questions about this huddle contact <br /> <?php echo $users['User']['first_name'] . " " . $users['User']['last_name'] ?> at <a href="mailto:<?php echo $users['User']['email']; ?>"><?php echo $users['User']['email']; ?></a>
            </td>
          </tr>



		 <tr>
		  <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
		   <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id; ?>/4" style="color:#616161; text-decoration:underline;">Unsubscribe Now</a> <br /><br />
           <a href="www.sibme.com" target="_blank" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
           </td>
		 </tr>
         
         
         
         
		</table>
   </td>
  </tr>
 </table>
</body>
</html>
    <?php
}



else {
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title><?php $this->Custom->get_site_settings('site_title') ?> - <?php echo $huddle_type; ?> Huddle Invitation</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style>
    img { max-width:100%;}
  </style>
  
</head>
<body style="margin: 0; padding: 0;">
 <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial, Helvetica, sans-serif;">
 
  <tr>
  
   <td>
    	<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
        
         <tr>
          <td style="text-align:center; padding:30px 0;">
             <img src="<?php echo $redirect;?>/img/logohmh.png" alt="HMH" width="320" height="59"/>
          </td>
          </tr>
          
          <tr>
          <td style="text-align:center; border-bottom:1px solid #ddd;">
           
          </td>
          </tr>
          
		  <tr>
		    <td style="text-align:center; font-size:16px; padding:30px 0; line-height:26px; font-family:Arial, Helvetica, sans-serif;">
            


<b><?php echo $data['User']['first_name'] . " " . $data['User']['last_name'] ?></b> <br />
invited you to participate in a <?php echo $huddle_type; ?> huddle<br />
<b><?php echo $data['AccountFolder']['name']; ?></b>

         
     
    
     </td> 
           
		 </tr>
         
          <tr>
            <td style="text-align:center; padding:10px 0 30px 0;">
              <a href="<?php echo $sibme_base_url; ?>/Huddles/view/<?php echo $data['AccountFolder']['account_folder_id']; ?>" title="Click here to start participating"> <img src="<?php echo $redirect;?>/img/view_partic.png" width="330" height="50" alt="Click here to start participating " /></a>
            </td>
          </tr>
         
          <tr>
            <td style="font-size: 14px; text-align: left; color: #3C4147; padding-top:0px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
              If you have questions about this huddle contact <br /> <?php echo $users['User']['first_name'] . " " . $users['User']['last_name'] ?> at <a href="mailto:<?php echo $users['User']['email']; ?>"><?php echo $users['User']['email']; ?></a>
            </td>
          </tr>
          
		 <tr>
		  <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
		   <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id; ?>/4" style="color:#616161; text-decoration:underline;">Unsubscribe Now</a> <br /><br />
           <a href="www.sibme.com" target="_blank" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
          </td>
		 </tr>
         
         
         
         
		</table>
   </td>
  </tr>
 </table>
</body>
</html>
<?php } ?>
