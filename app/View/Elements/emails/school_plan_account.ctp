<div id=":6r" style="overflow: hidden;">
    <p>
        You've made a wise decision by signing up for <strong>Sibme's School Plan</strong>.   If you need to make changes to your payment information and/or account information, you can do so by adjusting your account settings from within the application.

    </p>
    <br/>
    <strong>Click below to begin using Sibme:</strong><br/>
    <br/>
    <strong><a target="_blank" href="<?php echo $_SERVER['HTTP_HOST']; ?>/app/users/login">Login</a> </strong>
    <br/>
    <p> <strong>Questions? </strong>Contact us at  <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['info']; ?>" target="_blank"><?php echo $this->custom->get_site_settings('static_emails')['info']; ?></a></p>
    <p>For support using our software   <a href="#" target="_blank">click here</a>.</p>
    <br/>
    <p>We appreciate your business.</p>
    <p>The <?php $this->Custom->get_site_settings('site_title') ?> team,</p>
</div>