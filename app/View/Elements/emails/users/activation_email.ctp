<?php
$sibme_base_url = Configure::read('sibme_base_url');
$users = $this->Session->read('user_current_account');
?>

<p>
    <strong><?php echo $data['name'] ?></strong>
</p>
<br/>

<p><strong>Thanks for signing up!</strong></p>
<p>You're all set. Please click the activation link below to sign in.</p>
<hr/>
<h4>
    <strong> Account name: <?php echo $data['account_name'] ?> </strong>
</h4>
<br/>
<hr/>
<p>
    <?php if(isset($account_folder_id)): ?>
    Access your account now by clicking this activation link: <a href="<?php echo $sibme_base_url . 'Users/activate_user/' . md5($data['user_id']) . '/' .$account_folder_id ?>" target="_blank">Click here to activate your account</a>
    <?php else: ?>
    Access your account now by clicking this activation link: <a href="<?php echo $sibme_base_url . 'Users/activate_user/' . md5($data['user_id']) ?>" target="_blank">Click here to activate your account</a>
    <?php endif; ?>
</p>
<br/>
<hr/>

