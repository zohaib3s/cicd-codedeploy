<?php
$sibme_base_url = Configure::read('sibme_base_url');
$users = $this->Session->read('user_current_account');
?>

<p>
    <strong><?php echo $data['name'] ?></strong>
</p>
<br/>

<p><strong>Thanks for signing up!</strong></p>
<p>Your two factor authentication code: <span style="font-size: 18px;font-weight: bold"><?php echo $data['verification_code'] ?> </span></p>
<hr/>
<br/>
<hr/>

<br/>
<hr/>