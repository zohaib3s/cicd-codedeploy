<?php
$sibme_base_url = Configure::read('sibme_base_url');
//$users = $this->Session->read('user_current_account');
?>
<h4>
    <strong>You've been added to <?php echo $data['company_name']; ?>'s account successfully!</strong>
</h4>
<br/>

<p>
    <strong><?php $this->Custom->get_site_settings('site_title') ?> is an awesome video coaching and collaboration platform we use to improve teaching and learning.</strong>
</p>
<br />
<a href="<?php echo $sibme_base_url; ?>">Please click here and login</a>
<br />
<br />
<p>Read <strong><?php echo $data['first_name'] . ' ' . $data['last_name'] ?></strong>'s  message below:</p>

<p><?php echo isset($data['message']) ? $data['message'] : ''; ?></p>

<p>Still have questions?</p>

<p>Contact <strong><?php echo $data['first_name'] . ' ' . $data['last_name'] ?></strong> at <?php echo $data['account_owner_email'] ?></p>
<p>
    Delivered by <?php $this->Custom->get_site_settings('site_title') ?> <a href='www.sibme.com' target="_blank">www.sibme.com</a>
</p>