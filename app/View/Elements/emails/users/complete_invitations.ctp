<p>
    <strong><?php echo $data['name'] ?></strong>
</p>
<br/>

<p><strong>Thanks for creating an account.</strong></p>
<p>You're all set. You'll find your username and a link to sign in below.</p>
<hr/>
<h4>
    <strong> Account name: <?php echo $data['account_name'] ?> </strong>
</h4>
<p>Congrats!  You've successfully created your account.</p>
<br/>
<hr/>
<p><b>Your new username is:  <?php echo $data['newUsername']; ?></b></p>
<p>
    Access your account now: <a href="<?php echo $data['base_url']; ?>users/login" target="_blank"><?php echo $data['base_url']; ?>users/login</a>
</p>
<br/>
<hr/>
<p>If you have questions, please contact your Account Owner at: <a href="mailto:<?php echo $data['owner_email'] ?>"><?php echo $data['owner_email'] ?></a>.</p>
