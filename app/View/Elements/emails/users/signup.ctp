<?php
$sibme_base_url = Configure::read('sibme_base_url');
$trial_duration = Configure::read('trial_duration');
?>
<div id=":6r" style="overflow: hidden;">
    <?php if (empty($user['last_name'])): ?>
        <p>Hi <?php echo ($user['first_name'] ? $user['first_name'] : '') ?>, thanks for signing up for your free <?php print $trial_duration; ?> day trial!</p>
    <?php else: ?>
        <p>Hi <?php echo ($user['first_name'] ? $user['first_name'] : '') . ' ' . ($user['last_name'] ? $user['last_name'] : '') ?>, thanks for signing up for your free <?php print $trial_duration; ?> day trial!</p>
    <?php endif; ?>
    <br/>
    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We welcome feedback at Sibme, so please let us know how we can improve.</p>
    <br/>
    <p>Have questions? Contact us at <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['info']; ?>" target="_blank"><?php echo $this->custom->get_site_settings('static_emails')['info']; ?></a> or visit the <a href="http://help.sibme.com"><?php $this->Custom->get_site_settings('site_title') ?> help desk</a>.</p>
    <br/>
    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>
    <br/>
    <a href="<?php echo $sibme_base_url; ?>accounts/account_settings_all/<?php echo $user['account_id']; ?>/4">Subscribe Now</a>
    <br/>
    <p>
        <strong>Sign into your account</strong><br/>
        <a href="<?php echo $sibme_base_url; ?>Users/login" target="_blank"><?php echo $sibme_base_url; ?>Users/login</a>
    </p>

    <?php if (isset($user['username']) && $user['username'] != ''): ?>
        <p><strong>Username:</strong> <?php echo $user['username']; ?></p>
    <?php endif; ?>

    <div class="yj6qo"></div>
    <div class="adL"></div>

</div>
