<?php
$sibme_base_url = Configure::read('sibme_base_url');
$users = $this->Session->read('user_current_account');
?>
<h4>
    <strong>You've been invited to join <?php echo $users['accounts']['company_name']; ?>'s account</strong>
</h4>
<br/>
<?php echo $users['User']['first_name'] . ' ' . $users['User']['last_name'] ?> invited you to become <?php echo $data['role_title'] ?> in <?php echo $users['accounts']['company_name']; ?>'s Account:
<br/>
<h2 style="color:#3d9bc2;"><?php echo $users['accounts']['company_name'] ?></h2>
<p>
    <strong><?php $this->Custom->get_site_settings('site_title') ?> is an awesome video coaching and collaboration platform we use to improve teaching and learning.</strong>
</p>
<br/>
<a href="<?php echo $sibme_base_url . 'Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']) . "/" . $account_folder_id ?>">Accept this invitation to get started</a>
<br />
<br/>
<p>Read <strong><?php echo $users['User']['first_name'] . ' ' . $users['User']['last_name'] ?></strong>'s  message below:</p>

<p><?php echo isset($data['message']) ? $data['message'] : ''; ?></p>

<p>Still have questions?</p>

<p>Contact <strong><?php echo $users['User']['first_name'] . ' ' . $users['User']['last_name'] ?></strong> at <?php echo $users['User']['email'] ?></p>
<p>
    Delivered by <?php $this->Custom->get_site_settings('site_title') ?> <a href='www.sibme.com' target="_blank">www.sibme.com</a>
</p>
