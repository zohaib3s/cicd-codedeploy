<?php
$sibme_base_url = Configure::read('sibme_base_url');
$users = $this->Session->read('user_current_account');

$profile_image = isset($users['User']['image']) ? $users['User']['image'] : '';
$profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . "img/profile.jpg");
}
?>
<table>
    <tbody>
        <tr>
            <td style="width: 100px;">
                <p>
                    <img src="<?php echo $_SERVER['HTTP_HOST'] . $profile_image; ?>" alt="img" height="75px" width="75px"/>
                </p>
            </td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td style="padding-top: 0px; padding-bottom: 10px;">
                                <strong>Write above this line to post a reply</strong>.
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;">
                                <?php echo $data['message'] ?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Delivered by <?php $this->Custom->get_site_settings('site_title') ?> <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>