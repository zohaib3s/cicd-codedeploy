<div id=":6r" style="overflow: hidden;">
    <p>Hi <?php echo ($first_name ? $first_name : '') . ' ' . ($last_name ? $last_name : '') ?>,</p>
    <p>
        We hope your trial is going well.  You only have 1 week left in your trial!  Hurry and signup today!
    </p>
    <p><strong>Your free trial runs until <?php echo $trial_end_date; ?>.</strong></p>
    <a href="<?php echo $_SERVER['HTTP_HOST']; ?>/app/accounts/account_settings_all/<?php echo $account_id ?>/4"><strong>Subscribe Now!</strong></a><br/>
    <br/>

    <p>Have questions or need help?  Please email us at <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['info']; ?>" target="_blank"><?php echo $this->custom->get_site_settings('static_emails')['info']; ?></a></p>
    <p>Having technical issues?  Please email us at  <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" target="_blank"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a></p>
    <br/>
    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>
    <p>The <?php $this->Custom->get_site_settings('site_title') ?> team,</p>
    <p><a target="_blank" href="//<?php echo $this->custom->get_site_settings('site_url'); ?>"><?php echo $this->custom->get_site_settings('site_url'); ?></a></p>
    <div class="yj6qo"></div>
    <div class="adL">
    </div>

</div>