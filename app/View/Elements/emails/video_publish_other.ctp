<?php
$sibme_base_url = Configure::read('sibme_base_url');
$users = $this->Session->read('user_current_account');

$profile_image = isset($users['User']['image']) ? $users['User']['image'] : '';
$profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . "img/profile.jpg");
}
//echo "<pre>";
//print_r($creator);
?>

<table>
    <tbody>
        <tr>
            <td style="width: 100px;">
                <p>
                    <img src="<?php echo $_SERVER['HTTP_HOST'] . $profile_image; ?>" alt="img" height="75px" width="75px"/>
                </p>
            </td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td style="padding-top: 0px; padding-bottom: 10px;">
                                <strong> <?php echo $creator['User']['first_name'] . " " . $creator['User']['last_name'] ?> added a video in the following Huddle: <?php echo $huddle_name; ?></strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Click the link below to view the Video.</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h2>
                                    <a href="<?php echo $sibme_base_url . "/huddles/view/" . "/1/" . $video_id; ?>">Click to view</a>
                                </h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Delivered by <?php $this->Custom->get_site_settings('site_title') ?> <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>