<?php
$sibme_base_url = Configure::read('sibme_base_url');
$users = $this->Session->read('user_current_account');

$profile_image = isset($users['User']['image']) ? $users['User']['image'] : '';
$profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . "img/profile.jpg");
}
?>
<table>
    <tbody>
        <tr>
            <td style="width: 100px;">
                <p>
                    <img src="<?php echo $_SERVER['HTTP_HOST'] . $profile_image; ?>" alt="img" height="75px" width="75px"/>
                </p>
            </td>
            <td>
                <table>
                    <tbody>
                        <?php if ($huddle_type == 3) : ?>
                            <tr>
                                <td style="padding-top: 0px; padding-bottom: 10px;">
                                    <strong> You added a video to your Workspace: <?php echo $huddle_name; ?></strong>
                                </td>
                            </tr>
                        <?php elseif ($huddle_type == 2) : ?>

                            <tr>
                                <td style="padding-top: 0px; padding-bottom: 10px;">
                                    <strong>You added a video to the <?php echo $account_name ?> Video Library: <?php echo $huddle_name; ?></strong>
                                </td>
                            </tr>

                        <?php else : ?>
                            <tr>
                                <td style="padding-top: 0px; padding-bottom: 10px;">
                                    <?php if ($htype == 2) : ?>
                                        <strong> <?php echo $creator['User']['first_name'] . " " . $creator['User']['last_name'] ?> added a video to the : <?php echo $huddle_name; ?></strong>
                                    <?php elseif ($htype == 3) : ?>
                                        <strong> <?php echo $creator['User']['first_name'] . " " . $creator['User']['last_name'] ?> added a video to the : <?php echo $huddle_name; ?></strong>
                                    <?php else : ?>
                                        <strong> <?php echo $creator['User']['first_name'] . " " . $creator['User']['last_name'] ?> added a video to the : <?php echo $huddle_name; ?></strong>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td>
                                <strong>Click the link below to view the Video.</strong>
                            </td>
                        </tr>
                        <?php
                        if ($huddle_type == 1) :
                            ?>
                            <tr>
                                <td>
                                    <h2>
                                        <a href="<?php echo $url; ?>"><?php echo trim('Click') . ' ' . trim('to') . ' ' . trim('view') ?>  </a>
                                    </h2>
                                </td>
                            </tr>
                        <?php elseif ($huddle_type == 3): ?>
                            <tr>
                                <td>
                                    <h2>
                                        <a href="<?php echo $sibme_base_url . "/MyFiles/view/1/" . $huddle_id; ?>">Click to view</a>
                                    </h2>
                                </td>
                            </tr>
                        <?php elseif ($huddle_type == 2): ?>
                            <tr>
                                <td>
                                    <h2>
                                        <a href="<?php echo $sibme_base_url . "/VideoLibrary/view/" . $huddle_id; ?>">Click to view</a>
                                    </h2>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php
                        if (isset($participated_user [0]['huddle_users']['first_name']) && ($participated_user [0]['huddle_users']['first_name'] != '' || $participated_user [0]['huddle_users']['last_name'] != '')) {
                            ?>
                            <tr>
                                <td>
                                    Other people participating in Huddle include:
                                    <?php
                                    if (isset($participated_user) && count($participated_user) > 0) {
                                        $user_counter = 0;
                                        foreach ($participated_user as $row) {
                                            if ($user_counter > 0)
                                                echo " , ";
                                            echo $participated_user = $row['User']['first_name'] . " " . $row['User']['last_name'];
                                            $user_counter +=1;
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td>
                                <a href="www.sibme.com" target="_blank" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>&#160;</td>
            <td><a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $huddle_user_id . "/" . $email_type; ?>"> Unsubscribe Now  </a></td>
        </tr>
    </tbody>
</table>