<?php
$sibme_base_url = Configure::read('sibme_base_url');
?>
<p>
    <?php $this->Custom->get_site_settings('site_title') ?> is delighted to announce its newest release, providing major updates to the previous application.  The newest version includes many new features and improved stability and performance.  We have worked diligently these past few months to address feedback we received during the 2012-13 school year.
</p>
<p>
    Our goal since day one has been to break down the walls of a school.   As educators it was never easy to collaborate efficiently or observe our colleagues teach.  We knew there had to be a more efficient process for teachers and instructional leaders to improve instruction. Through our partnership with Swivl coupled with our native iOs app, we have made the self- recording, uploading, and sharing classroom video process seamless.
</p>
<p>
    Our vision for minimalism has been accomplished and we will continue to make improvements that only add value to improving teaching and learning.   It has always been our belief that technology should enhance the teacher development process, not supplant the need for human interaction. We do not purport to have all the answers and understand that teaching and learning are incredibly complex.
</p>
<p>
    Given the new architecture of our application, we have ported over existing <?php $this->Custom->get_site_settings('site_title') ?> accounts along with its data. <strong>You have been assigned a new temporary password below that you can change immediately in your user settings after logging into</strong> <a href="http://app.sibme.com">app.sibme.com</a>. For all accounts currently on a trial, we will reset your account for a new 60-day trial. If you have any questions or concerns, please contact us at <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>
</p>
<p>
    <strong>New Features</strong>
</p>
<ul>
    <li>Account video library to create, store, tag, and search exemplars in your network</li>
    <li>Launchpad for enterprise accounts</li>
    <li>Improved Huddle/Workspace experience with added security and permissions</li>
    <li>Dropbox, Google Drive, and SkyDrive integration</li>
    <li>New native iOs app (note: please download or update to the newest release in the iTunes store on Thursday, Oct. 10th)</li>
    <li>Swivl integration  (Swivl V2 will be released in January 2014, so contact us if you're interested in pre-ordering the new Swivl which is compatible with iOs, Android, and DSLR devices.</li>
    <li>My Workspace, a place to reflect and store personal data (will be released on Oct. 31st)</li>
    <li>Android app (will be released on Jan 1st 2014 in preparation for the V2 Swivl release)</li>
</ul>
<p>
    <strong>Temporary Password</strong><br/>
    <?php echo $temp_password; ?>
</p>
<p>
    <strong>URL to change password</strong><br/>
    <a href="<?php echo $sibme_base_url; ?>/Users/editUser/<?php echo $user_id; ?>">Click here to change password</a>
</p>
<p>
    Delivered by <?php $this->Custom->get_site_settings('site_title') ?> <a href='www.sibme.com' target="_blank">www.sibme.com</a>
</p>
