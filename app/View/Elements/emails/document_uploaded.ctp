<?php
$sibme_base_url = Configure::read('sibme_base_url');
$users = $this->Session->read('user_current_account');

$profile_image = isset($users['User']['image']) ? $users['User']['image'] : '';
$profile_image_real_path = realpath(dirname(__FILE__) . '/../../../../../') . "/app/webroot/img/users/$profile_image";

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . "img/profile.jpg");
}
?>

<table>
    <tbody>
        <tr>
            <td>
                <p>
                    <img src="<?php echo $_SERVER['HTTP_HOST'] . $profile_image; ?>" alt="img" height="75px" width="75px"/>
                </p>
            </td>
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td style="padding-top: 0px; padding-bottom: 10px;">
                                <strong>
                                    <?php echo $users['User']['first_name'] . " " . $users['User']['last_name']; ?> added a resource in the following Huddle: <?php echo $data['huddle_name']; ?>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;">
                                <strong>Click the link below to view the resource</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;">
                                <strong><a href="<?php echo $sibme_base_url . $data['video_link']; ?>">Click to view</a></strong>
                            </td>
                        </tr>
                        <?php if ($data['huddle_type'] != '3'): ?>
                            <tr>
                                <td>
                                    <p>
                                        Other people participating in Huddle include:
                                        <?php
                                        if (isset($data['participating_users']) && count($data['participating_users']) > 0) {
                                            $user_counter = 0;
                                            foreach ($data['participating_users'] as $row) {
                                                if ($user_counter > 0)
                                                    echo " , ";
                                                echo $participated_user = $row['User']['first_name'] . " " . $row['User']['last_name'];
                                                $user_counter +=1;
                                            }
                                        }else {
                                            echo "No other people participating.";
                                        }
                                        ?>
                                    </p>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td>
                                Delivered by <?php $this->Custom->get_site_settings('site_title') ?> <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr><td>&#160;</td>
            <td>
                <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $data['user_id'] ?>/1"> Unsubscribe Now  </a>
            </td>
        </tr>
    </tbody>
</table>