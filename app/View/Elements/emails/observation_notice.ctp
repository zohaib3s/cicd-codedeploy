<?php
$sibme_base_url = Configure::read('sibme_base_url');

$obsTime = new DateTime($data['observations']['observation_date_time']);
?>
<html>
    <body>
        <table>
            <tbody>
                <tr>
                    <td style="width: 100px;">
                        <p style="margin-bottom: 270px;">

                        </p>
                    </td>
                    <td>
                        <table>
                            <tbody>
                                <tr>
                                    <td style="padding-bottom: 10px;"><?php echo "<h3 style='color:#3d9bc2'>" . $data['account_folders']['name'] . "</h3> " . $data['observations']['location_name']; ?></td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom: 10px;"><?php echo $obsTime->format('D, d M Y h:i a'); ?></td>
                                </tr>

                                <tr>
                                    <td style="padding-bottom: 10px;">This is a Notification Email, to notify you that You have an Observation </td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom: 10px;"><?php
                                        echo ' at ' . $data['observations']['location_name'] . ' on ' . $obsTime->format('D, d M Y h:i a');
                                        ;
                                        ?> </td>
                                </tr>


                                <tr>
                                    <td><?php $this->Custom->get_site_settings('site_title') ?> is an awesome collaboration tool we use to improve teaching and learning.</td>
                                </tr>
                                <tr>
                                    <td>
                                        Delivered by <?php $this->Custom->get_site_settings('site_title') ?> <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&#160;</td>
                    <td>
                        <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $data['AccountFolderObservationUsers']['user_id']; ?>"> Unsubscribe Now  </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
