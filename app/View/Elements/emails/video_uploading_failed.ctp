
<p>
    The video "<?php echo $video_title ?>" in [<?php echo $video_type ?>] failed to process, please upload the video again or contact us at <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a> to troubleshoot.
</p>
<p>
    Regards,<br/>
    <?php $this->Custom->get_site_settings('site_title') ?> Team.
</p>
<p>Still have questions?</p>
<p>Contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['info']; ?>"><?php echo $this->custom->get_site_settings('static_emails')['info']; ?></a></p>
