<?php

if ($total_items > $count_items) {
    $total_page = ceil($total_items / $number_per_page);
    if ($total_page > 7) {
        if (0 > $total_page - 3) {
            $start_show_page = $current_page - 3;
        } else {
            $start_show_page = 1;
        }
        $end_show_page   = $start_show_page + 6;
        if ($end_show_page > $total_page) $end_show_page = $total_page;
    } else {
        $start_show_page = 1;
        $end_show_page = $total_page;
    }
    $first = $current_page > 1 ? 1 : 0;
    $last  = $current_page < $total_page ? $total_page : 0;
    $prev  = $current_page > 1 ? $current_page - 1 : 0;
    $next  = $current_page < $total_page ? $current_page + 1 : 0;
    ?>
    <div class="pagination">
        <ul>
            <?php if ($current_page > 1): ?>
                <li class="active" data-page="1">First</li>
                <li class="active" data-page="<?php print $current_page - 1; ?>">Previous</li>
            <?php endif; ?>

            <?php for($i = $start_show_page; $i <= $end_show_page; $i++): ?>
                <li class="active<?php print ($i == $current_page)?' current':'';?>" data-page="<?php print $i;?>"><?php print $i;?></li>
            <?php endfor; ?>

            <?php if ($current_page < $total_page): ?>
                <li class="active" data-page="<?php print $current_page + 1; ?>">Next</li>
                <li class="active" data-page="<?php print $total_page; ?>">Last</li>
            <?php endif; ?>
        </ul>
    </div>
<?php
}
?>