<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>SIBME</title>

        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <!--<![endif]-->



        <style type="text/css">
            body{
                font-family:roboto,'helvetica neue',helvetica,arial,'sans-serif' !important;
                margin:0;
                padding:0;
            }
            @media only screen and (max-width:480px){
                .fullWidth{
                    width:100% !important;
                }

            }	@media only screen and (max-width:480px){
                .halfWidth{
                    width:50% !important;
                }

            }	@media only screen and (max-width:480px){
                .textCenter{
                    text-align:center !important;
                }

            }	@media only screen and (max-width:480px){
                .noFloat{
                    float:none !important;
                    margin:0 auto !important;
                }

            }	@media only screen and (max-width:480px){
                .marginBottom10{
                    margin-bottom:10px !important;
                }

            }	@media only screen and (max-width:480px){
                .paddingTop10{
                    padding-top:10px !important;
                }

            }	@media only screen and (max-width:480px){
                .noPaddingBottom{
                    padding-bottom:0px !important;
                }

            }	@media only screen and (max-width:480px){
                .noPaddingTop{
                    padding-top:0px !important;
                }

            }	@media only screen and (max-width:480px){
                .displayBlock{
                    display:block !important;
                    width:100% !important;
                }

            }	@media only screen and (max-width:480px){
                .hide{
                    display:none !important;
                }

            }	@media only screen and (max-width:480px){
                .noBorder{
                    border-width:0px !important;
                }

            }</style></head>
    <body style="margin:0px;padding:0px;-webkit-text-size-adjust:none;background-color:#f1f1f1" yahoo="fix">
        <!-- WRAPPER -->
        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;">
            <tr>
                <td align="center">

                    <!-- START -->
                    <table cellpadding="0" cellspacing="0" border="0" width="650" class="fullWidth">
                        <tr>
                            <td>
                                <!-- HEADER -->
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td style="line-height:0px"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/e124ef0a-1fee-4433-832f-811b7742f251.png" width="650" height="2" border="0" style="display:block;margin:0;max-width:100%"></td>
                                    </tr>
                                    <tr>
                                        <td background="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/e0b351c9-c75f-475c-821d-2ab55ed7cab2.png" bgcolor="#1f3d2b" height="237" valign="middle" style="background-repeat:no-repeat">
                                            <!--[if gte mso 9]>
                                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:650px;height:237px;">
                                            <v:fill type="frame" src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/e0b351c9-c75f-475c-821d-2ab55ed7cab2.png" color="#1f3d2b" />
                                            <v:textbox inset="0,0,0,0">
                                            <![endif]-->
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td height="237" align="center">

                                                        <table cellpadding="0" cellspacing="0" border="0" width="570" align="center" class="fullWidth">
                                                            <tr>
                                                                <td align="center">
                                                                    <table cellpadding="0" cellspacing="0" border="0" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-weight:400;font-size:18px;line-height:21px;color:#ffffff;" class="fullWidth" width="100%">
                                                                        <tr>
                                                                            <td valign="top" class="textCenter displayBlock" mc:edit="header_text" style="padding-bottom:15px">
                                                                                <span style="font-size:24px">Blackshear Elementary</span><br>
                                                                                <span style="font-size:39px;font-weight:300;line-height:42px">WEEKLY SUMMARY</span><br>
                                                                                <span style="color:#5dd00b;">August 21-27</span>
                                                                            </td>
                                                                            <td valign="top" align="right" class="textCenter displayBlock"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/9ecda741-a0ad-42f5-80b1-39277257d6eb.png" alt="" border="0" width="105" height="48"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if gte mso 9]>
                                            </v:textbox>
                                            </v:rect>
                                            <![endif]-->
                                        </td>
                                    </tr>
                                </table>
                                <!-- /HEADER -->



                                <!-- MAIN -->
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-color:#f5f5f5">
                                    <tr>
                                        <td valign="top">
                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td style="padding:35px 60px" align="center">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="98%" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:22px;color:#033c4b;padding-bottom:20px" align="center" class="fullWidth">
                                                            <tr>
                                                                <td mc:edit="bodytoptext">
                                                                    Hope you had a great week! Here's a summary of what happened in your <?php $this->Custom->get_site_settings('site_title') ?> account last week.
                                                                </td>
                                                            </tr>
                                                        </table>


                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td valign="top" width="30%" style="border-right:1px solid #e0e0e0;padding-top:10px;padding-bottom:20px" class="fullWidth displayBlock noBorder">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td align="left" class="textCenter">
                                                                                <table width="115" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:28px;color:#033c4b;" class="noFloat">
                                                                                    <tr>
                                                                                        <td valign="top" height="67"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/c4a4f001-f2a3-4faa-b3f1-70eda8dcef1f.png" width="52" height="57" alt="Users" border="0" style="display:block;margin:0 auto;"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="padding-bottom:20px;"><span style="font-weight:300">Users</span></td>
                                                                                    </tr>
                                                                                </table>

                                                                                <table width="115" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:14.5px;font-weight:500;color:#033c4b;" class="noFloat">
                                                                                    <tr>
                                                                                        <td style="padding:7px 0px"><span>ADDED</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <table width="115" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;background:#b2ebf2;border-radius:7px">
                                                                                                <tr>
                                                                                                    <td style="padding:4px 10px" mc:edit="usersadded">3</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <br>
                                                                                <table width="115" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:14.5px;font-weight:500;color:#033c4b;" class="noFloat">
                                                                                    <tr>
                                                                                        <td style="padding:7px 0px"><span>ACTIVE</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <table width="115" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;background:#b2ebf2;border-radius:7px">
                                                                                                <tr>
                                                                                                    <td style="padding:4px 10px" mc:edit="usersactive">24<span style="color:#75c3cd">/50</span></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <br>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td valign="top" width="40%" style="border-right:1px solid #e0e0e0;padding-top:10px;padding-bottom:20px" class="fullWidth displayBlock noBorder">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td align="center">
                                                                                <table width="115" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:28px;color:#033c4b;">
                                                                                    <tr>
                                                                                        <td valign="top" height="67"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/fdbcea38-fe44-48a8-af4b-94f682e45ce8.png" width="61" height="47" alt="Videos" border="0" style="display:block;margin:0 auto;"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="padding-bottom:20px;"><span style="font-weight:300">Videos</span></td>
                                                                                    </tr>
                                                                                </table>

                                                                                <table cellpadding="0" cellspacing="0" border="0" width="180">
                                                                                    <tr>
                                                                                        <td width="50%" align="left">
                                                                                            <table width="78" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:14.5px;font-weight:500;color:#033c4b;">
                                                                                                <tr>
                                                                                                    <td style="padding:7px 0px"><span>UPLOADED</span></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="center">
                                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;background:#c5ff9a;border-radius:7px">
                                                                                                            <tr>
                                                                                                                <td style="padding:4px 10px" mc:edit="videosuploaded">3</td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="50%" align="right">
                                                                                            <table width="78" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:14.5px;font-weight:500;color:#033c4b;">
                                                                                                <tr>
                                                                                                    <td style="padding:7px 0px"><span>VIEWED</span></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="center">
                                                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;background:#c5ff9a;border-radius:7px">
                                                                                                            <tr>
                                                                                                                <td style="padding:4px 10px" mc:edit="videosviewed">3</td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <br>
                                                                                <table width="180" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:14.5px;font-weight:500;color:#033c4b;">
                                                                                    <tr>
                                                                                        <td style="padding:7px 0px"><span>VIDEO COMMENTS ADDED</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <table width="70%" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;background:#b2ebf2;border-radius:7px">
                                                                                                <tr>
                                                                                                    <td style="padding:4px 10px" mc:edit="videoscomments">45</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <br>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td valign="top" width="30%" style="padding-top:10px;padding-bottom:20px" class="fullWidth displayBlock">
                                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                        <tr>
                                                                            <td align="right" class="textCenter">
                                                                                <table width="138" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:28px;color:#033c4b;" class="noFloat">
                                                                                    <tr>
                                                                                        <td valign="top" height="67"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/30b0786f-fe53-43b3-a6b0-0b9c0acbe766.png" width="60" height="60" alt="Huddles" border="0" style="display:block;margin:0 auto;"></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="padding-bottom:20px;"><span style="font-weight:300">Huddles</span></td>
                                                                                    </tr>
                                                                                </table>

                                                                                <table width="138" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:14.5px;font-weight:500;color:#033c4b;" class="noFloat">
                                                                                    <tr>
                                                                                        <td style="padding:7px 0px"><span>COACHING</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <table width="115" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;background:#f3ffa7;border-radius:7px">
                                                                                                <tr>
                                                                                                    <td style="padding:4px 10px" mc:edit="huddlescoaching">4</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <br>
                                                                                <table width="138" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:14.5px;font-weight:500;color:#033c4b;" class="noFloat">
                                                                                    <tr>
                                                                                        <td style="padding:7px 0px"><span>COLLABORATION</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <table width="115" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;background:#f3ffa7;border-radius:7px">
                                                                                                <tr>
                                                                                                    <td style="padding:4px 10px" mc:edit="huddlescollab">9</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <table width="138" cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#033c4b;" class="noFloat">
                                                                                    <tr>
                                                                                        <td style="padding-top:8px">Stats above reflect the number of current Huddles in your account.</td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>




                                                        <table cellpadding="0" cellspacing="0" border="0" style="text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17.5px;font-weight:500;color:#033c4b;" align="center">
                                                            <tr>
                                                                <td style="padding-top:20px;padding-right:14px;" class="displayBlock"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/a549d173-0704-4f3a-bf79-a019684e3e18.png" width="31" height="31"></td>
                                                                <td style="padding-top:20px" class="displayBlock noPaddingTop" mc:edit="textbottom">Looking for more stats? Check out your <strong style="color:#85db46">teams analytics page.</strong></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <!-- /MAIN -->






                                <!-- FOOTER -->
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-color:#ffffff;">
                                    <tr>
                                        <td valign="middle" style="padding:30px 35px">
                                            <table cellpadding="0" cellspacing="0" border="0" width="380	" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse" class="fullWidth">
                                                <tr>
                                                    <td align="left">
                                                        <table cellpadding="0" cellspacing="0" border="0" width="97" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;" align="left" class="fullWidth">
                                                            <tr>
                                                                <td align="left" class="textCenter"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/a825b7c6-90a7-4d15-ae8b-43273bcf505e.png" alt="" width="97" height="45" border="0" style="display:block;margin:0 auto"></td>
                                                            </tr>
                                                        </table>

                                                        <table cellpadding="0" cellspacing="0" border="0" width="70%" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:15px;color:#577c8f" align="right" class="fullWidth">
                                                            <tr>
                                                                <td valign="bottom" align="right" height="35" class="textCenter"><a href="http://www.sibme.com/" target="_blank" style="text-decoration:none;color:#577c8f">Website</a> &nbsp;&bull;&nbsp; <a href="http://help.sibme.com/" target="_blank" style="text-decoration:none;color:#577c8f">Help Center</a> &nbsp;&bull;&nbsp; <a href="<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" target="_blank" style="text-decoration:none;color:#577c8f">Support</a></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table cellpadding="0" cellspacing="0" border="0" width="145" align="right" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse" class="noFloat">
                                                <tr>
                                                    <td align="right" height="35" valign="bottom"><a href="https://www.facebook.com/sibmeapp/?ref=br_rs" target="_blank"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/ce5935e8-8092-4b90-910b-aa655c5f7b1b.png" alt="facebook" border="0" style="display:block;margin:0"></a></td>
                                                    <td align="right" height="35" valign="bottom"><a href="https://twitter.com/SibmeApp" target="_blank"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/93bffbf9-552d-4b87-8043-bd4ab365ddab.png" alt="twitter" border="0" style="display:block;margin:0"></a></td>
                                                    <td align="right" height="35" valign="bottom"><a href="https://plus.google.com/+Sibmeapp" target="_blank"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/06bb2d09-61c0-40e7-a6da-cb05da45aa5a.png" alt="gmail" border="0" style="display:block;margin:0"></a></td>
                                                    <td align="right" height="35" valign="bottom"><a href="https://www.linkedin.com/company/sibme/" target="_blank"><img src="https://gallery.mailchimp.com/114583afc7fffefe861f23afe/images/649a11d8-f89d-49e3-a552-e0e7dff01e92.png" alt="linkedin" border="0" style="display:block;margin:0"></a></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <!-- /FOOTER -->



                            </td>
                        </tr>
                    </table>
                    <!-- END -->

                </td>
            </tr>
        </table>
        <!-- /WRAPPER -->


    </body>
</html>