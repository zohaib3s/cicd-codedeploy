<!DOCTYPE html>
<style>
    .sep{ height:2px; background:#ddd; margin:25px 0;}
    .invoice_cont hr{height:2px; background:#ddd;     border: 0;}
    .invoice_cont { max-width:800px; margin:0 auto;     padding-top: 25px;}
    .in_cel2 { text-align:center;}
    .in_cel3 { text-align:center;}
    .in_cel4 { text-align:right;}
    .main_table   { width:800px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;}
    .child_table {font-family: sans-serif;}
    .invoice {font-size:34px;}
    .international {font-size:15px;color:lightgrey;}
    .tbl_heading {background-color:#5FAE45;color:#ffffff;font-family:sans-serif;}
</style>

<div class="invoice_cont">
    <table class="main_table" border="0" cellspacing="0" cellpadding="0" >
        <tbody>
            <tr>     
                <td valign="top"><img src="<?php echo $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/app/img/sibme_transaction.jpg' ;?>" style="width: 250px;margin-top: 10px;" /></td>
                <td valign="top" align="right">
                    <table class="child_table">
                        <tr>
                            <td align="right" class="invoice">INVOICE</td>
                        </tr>
                        <tr>
                            <td align="right" >Sibme</td>
                        </tr>
                        <tr>
                            <td align="right" >1113 Vine Street., Suite 208 Houston</td>
                        </tr>
                        <tr>
                            <td align="right" >TX 77002</td>
                        </tr>
                        <tr>
                            <td align="right" >United States</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="sep"></div>
    <table class="main_table" border="0" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td>BILL TO</td>
                            </tr>
                            <tr>
                                <td><?php echo $company; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $firstName.''.$lastName; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $email; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td align="right">
                    <table>
                        <tbody>
                            <tr>
                                <td align="right"><b>Invoice Number:</b></td>
                                <td><?php echo $invoice_number; ?></td>
                            </tr>
                            <tr>
                                <td align="right"><b>Invoice Date:</b></td>
                                <td><?php echo $created_at; ?></td>
                            </tr>
                            <tr>
                                <td align="right"><b>Payment Due:</b></td>
                                <td><?php echo $created_at; ?></td>
                            </tr>
                            <tr>
                                <td align="right"><b>Amount Due(USD):</b></td>
                                <td>$<?php echo $amount; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="sep"></div>
    <table class="main_table" border="0" cellspacing="0" cellpadding="8">
        <tbody>
            <tr class="tbl_heading">
                <th align="left" colspan="3">Plan</th>
<!--                <th align="center">Quantity</th>
                <th align="center">Amount</th>-->
                <th align="right">Price</th>
            </tr>
            <tr>
                <td class="in_cel1" colspan="3"><?php echo $plan_category.' '.$plan_name.' '.$billing_cycle; ?></td>
                <!--<td class="in_cel2">1</td>-->
                <!--<td class="in_cel3"><?php //echo $amount ;?></td>-->
                <td class="in_cel4"><?php echo $amount ;?></td>
            </tr>



            <tr><td colspan="4"><hr></td></tr>
            <tr>
                <td colspan="3" align="right"><b>Total:</b></td>
                <td class="in_cel4">$<?php echo $amount ;?></td>

            </tr>
        </tbody>
    </table>
</div>


