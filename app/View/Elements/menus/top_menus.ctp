<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
$user_current_account = $this->Session->read('user_current_account');
$user_type = $this->Custom->get_user_role_name($user_current_account['users_accounts']['role_id']);
$user_role = $this->Custom->get_user_role_name($user_current_account['users_accounts']['role_id']);
$in_trial_intercom = $this->Custom->check_if_account_in_trial_intercom($user_current_account['accounts']['account_id']);
$date = new DateTime($user_current_account['accounts']['created_at']); // Y-m-d
$date->add(new DateInterval('P30D'));
$expiry_date = $date->format('m/d/Y');
$user_permissions = $this->Session->read('user_permissions');
$analytics_permission = $user_permissions['UserAccount']['permission_view_analytics'];

$selectedAccount = '';
$selectedAccount = $this->Session->read('user_current_account_id');
$allAccounts = $this->Session->read('user_accounts');
$translation = $this->Custom->get_page_lang_based_content('top_menus');
$ChurnZero = Configure::read('ChurnZero');
?>
<?php
//$logo_image = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/new/logo_small.png');

if (isset($_SESSION['site_id']) && $_SESSION['site_id'] == 2) {
    $header_background_color = "#808184";
    $logo_image = $this->base . '/img/logo_3.svg';
} else {
    $logo_image = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/logo-dark.svg');
    $header_background_color = "#004061";
}

$nav_bg_color = "#668dab";
$logo_background_color = "#fff";

if (!empty($user_current_account['accounts']['header_background_color'])) {
    $header_background_color = "#" . $user_current_account['accounts']['header_background_color'];
}

if (!empty($user_current_account['accounts']['nav_bg_color'])) {
    $nav_bg_color = "#" . $user_current_account['accounts']['nav_bg_color'];
}

if (!empty($user_current_account['accounts']['usernav_bg_color'])) {
    $logo_background_color = "#" . $user_current_account['accounts']['usernav_bg_color'];
}

$logo_path = $this->Custom->getSecureSibmecdnImageUrl("static/companies/" . $user_current_account['accounts']['account_id'] . "/" . $user_current_account['accounts']['image_logo'], $user_current_account['accounts']['image_logo']);

if (!empty($user_current_account['accounts']['image_logo'])) {
    $logo_image = $logo_path;
}
?>
<?php if (IS_QA && isset($_SESSION['site_id']) && $_SESSION['site_id'] == 1): ?>

    <!-- begin Wootric code -->
    <script type="text/javascript">
        wootric_survey_immediately = false; // Shows survey immediately for testing purposes. TODO: Comment out for production.
        window.wootricSettings =
                {email: '<?php echo $user_current_account['User']['email']; ?>',
                    account_token: 'NPS-d79c6654',
                    created_at: '<?php echo time(); ?>'} // TODO: The current logged in user's email address. OPTIONAL // external_id: 'abc123', // TODO: The current logged in user's unique ID in your system. Reference field for external integrations only. OPTIONAL created_at: 1234567890, // TODO: The current logged in user's sign-up date as a 10 digit Unix timestamp in seconds. OPTIONAL account_token: 'NPS-d79c6654' // This is your unique account token. }
        ;
    </script>
    <script type="text/javascript" src="https://disutgh7q0ncc.cloudfront.net/beacon.js"></script>
    <script type="text/javascript">
        // This loads the Wootric survey
        //$(document).on("click", "#wootric_id", function () {
    <?php //if ($user_current_account['User']['survey_check'] == '0'):         ?>
        window.wootric('run');
    <?php //endif;         ?>
        //});
    </script>
    <script>
        $(document).on("click", "#wootric-submit", function () {


            var postData = {
                user_id: '<?php echo $user_current_account['User']['id']; ?>',
            };

            $.ajax({
                type: 'POST',
                url: home_url + '/huddles/survey_check/',
                data: postData,
                success: function (response) {

                }
            });


        });



    </script>
    <!-- end Wootric code -->
<?php endif; ?>
<?php if (isset($_SESSION['site_id']) && $_SESSION['site_id'] == 2) { ?>
    
    <script>
    window.intercomSettings = {
        app_id: "<?php echo Configure::read('intercom_app_id_hmh'); ?>",
        name: '<?php echo $user_current_account['User']['first_name'] . ' ' . $user_current_account['User']['last_name']; ?>', // Full name
        email: '<?php echo $user_current_account['User']['email']; ?>', // Email address
        created_at: '<?php echo strtotime($user_current_account['User']['created_date']); ?>', // Signup Date
        user_role: '<?php echo $user_role; ?>',
        is_in_trial: '<?php echo $in_trial_intercom; ?>',
        AccountID: '<?php echo $user_current_account['accounts']['account_id']; ?>'

    };
</script>
<script>(function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;
            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = '<?php echo Configure::read('intercom_app_id_link_hmh'); ?>';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })()</script>
    
<?php } 
else{

?>  
    
<script>
    window.intercomSettings = {
        app_id: "<?php echo Configure::read('intercom_app_id'); ?>",
        name: '<?php echo $user_current_account['User']['first_name'] . ' ' . $user_current_account['User']['last_name']; ?>', // Full name
        email: '<?php echo $user_current_account['User']['email']; ?>', // Email address
        created_at: '<?php echo strtotime($user_current_account['User']['created_date']); ?>', // Signup Date
        user_role: '<?php echo $user_role; ?>',
        is_in_trial: '<?php echo $in_trial_intercom; ?>',
        AccountID: '<?php echo $user_current_account['accounts']['account_id']; ?>'

    };
</script>
<script>(function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;
            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = '<?php echo Configure::read('intercom_app_id_link'); ?>';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })()</script>

<?php } ?>

<?php if($ChurnZero == '1'){ ?>
 <script>
 
    var ChurnZero = ChurnZero || [];
     (function() {
     var cz = document.createElement('script'); cz.type = 'text/javascript';
     cz.async = true;
     cz.src = 'https://analytics.churnzero.net/churnzero.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(cz, s);
     })();
     
     
     ChurnZero.push(['setAppKey', 'invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg']); // AppKey from ChurnZero
     ChurnZero.push(['setContact', '<?php echo $user_current_account['accounts']['account_id']; ?>', '<?php echo $user_current_account['User']['email']; ?>']);
 
 </script>
 
<?php } ?>



<?php if (IS_PROD && isset($_SESSION['site_id']) && $_SESSION['site_id'] == 1): ?>
    <style>

        #intercom-container .intercom-launcher-discovery-frame
        {
            right:20px !important;
        }
        #intercom-container .intercom-launcher-frame
        {
            /*left:20px !important;*/
        }

        #intercom-container .intercom-messenger-frame
        {
            /*left:20px !important;*/
        }
        .intercom-launcher-badge-frame
        {
            right:65px !important;
        }
        .intercom-notifications-frame
        {
            right:20px !important;
        }

    </style>
<?php endif; ?>




<?php if (IS_PROD && isset($_SESSION['site_id']) && $_SESSION['site_id'] == 1): ?>

    <!-- begin Wootric code -->
    <script type="text/javascript">
        //wootric_survey_immediately = true; // Shows survey immediately for testing purposes. TODO: Comment out for production.
        window.wootricSettings =
                {email: '<?php echo $user_current_account['User']['email']; ?>',
                    account_token: 'NPS-de8dfc8a',
                    created_at: '<?php echo time(); ?>'} // TODO: The current logged in user's email address. OPTIONAL // external_id: 'abc123', // TODO: The current logged in user's unique ID in your system. Reference field for external integrations only. OPTIONAL created_at: 1234567890, // TODO: The current logged in user's sign-up date as a 10 digit Unix timestamp in seconds. OPTIONAL account_token: 'NPS-d79c6654' // This is your unique account token. }
        ;
    </script>
    <script type="text/javascript" src="https://disutgh7q0ncc.cloudfront.net/beacon.js"></script>
    <script type="text/javascript">
        // This loads the Wootric survey
        //$(document).on("click", "#wootric_id", function () {
    <?php //if ($user_current_account['User']['survey_check'] == '0'): ?>
            window.wootric('run');
    <?php //endif; ?>
        //});
    </script>
    <script>
        $(document).on("click", "#wootric-submit", function () {


            var postData = {
                user_id: '<?php echo $user_current_account['User']['id']; ?>',
            };

            $.ajax({
                type: 'POST',
                url: home_url + '/huddles/survey_check/',
                data: postData,
                success: function (response) {

                }
            });


        });



    </script>
    <!-- end Wootric code -->

<?php endif; ?>



<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>
<script src="//static.filestackapi.com/v3/filestack.js"></script>
<style type="text/css">
    #hs-beacon{
        display: none;
    }
    #logo{
        background : <?php echo $logo_background_color; ?>
    }

    #logo a {
        width: 150px;
        height: 63px;
        margin:0 auto;
        background: url("<?php echo $logo_image; ?>") no-repeat center center;
        display: block;
        top: 0;
        background-size: 107px;
    }

    #header {
        /*  background-color: <?php //echo $header_background_color;           ?>;
          background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);
          background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(255, 255, 255, 0.07)), color-stop(100%, rgba(0, 0, 0, 0.07)));
          background-image: -webkit-linear-gradient(top, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);
          background-image: -o-linear-gradient(top, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);
          background-image: -ms-linear-gradient(top, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);
          background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);*/
        // background:#3D6A98;
        background: <?php echo $header_background_color; ?>
    }

    #user .dropdown-content a:hover {
        background: #4383b4;
        text-shadow: none;
        color: #fff;
    }

    .nav-header a {
        display: block;
        padding: 8px 11px;
        background: none;
        color: #cccccc;
        text-shadow: none;
        font-weight: normal;
        font-size: 15px;
        margin-top: -9px;
        top: 2px;
        position: relative;
    }
    .span-wrap{
        /*        width: 107px;*/
        text-align: left;
        margin-left: 0px;
    }
    .ph{
        float: left;
    }
    .sibme_coaching{
        margin-left: 186px;
        display: inline-block;
        font-size: 15px;
        color: #fff;
        top: 3px;
        position: relative;
    }
    .sibme_coaching p{
        font-size:12px;
        line-height: 22px;
    }
    .sibme_coaching img {
        max-width: 15px;

        margin-left: 3px;
    }
    .sibme_coaching span{
        color:#ccc;
        font-size:13px;
    }
    .sibme_coaching span a{
        font-weight:normal;
        color:#fff;
        text-decoration:underline;
    }

    .user_newDropDown {
        background: #fff;
        padding: 18px 22px;
        width: 356px;
        position: absolute;
        right: 0px;
        top: 71px;
        z-index: 20;
        box-shadow: 0px 0 4px #c7c7c7;
    }
    .account_info{
        margin-top: 15px;
        background: #eaeaea;
        color: #5b5b5b;
        padding: 6px 16px 7px 16px;
        border-radius: 3px;
        text-align: center;
        display: block;}
    .user_info_bottom {background: #eaeaea;
                       color: #5b5b5b;
                       margin: 19px -22px -20px -22px;
                       cursor: pointer;
                       padding: 5px 4px 0px 0px;}
    .user_info_bottom span{    display: inline-block;
                               white-space: nowrap;
                               text-overflow: ellipsis;
                               overflow: hidden;
                               max-width: 88%; font-size: 13px;}
    .user_info_bottom img {        width: 15px;
                                   position: relative;
                                   top: -4px;
                                   margin-left: 2px;}
    .user_info_bottom:before{display: none;}
    .user_info_bottom a {

        background: none!important;
        line-height: 18px!important;
        top: 0;
        position: relative;
        padding: 6px 16px !important;
        display: block !important;
        font-weight: 400 !important;
        text-shadow: 0 0px 0 #fff !important;    text-decoration: none;
    }
    .user_newDropDown:before {
        content: '';
        right: 7%;
        margin-left: -9px;
        bottom: 100%;
        width: 0px;
        height: 0px;
        border-style: solid;
        border-width: 0 7px 7px 7px;
        border-color: transparent transparent #fff transparent;
        position: absolute;
        z-index: 1;
    }

    .user_dropDown_info{
        float: left;
        width: 155px;
        word-wrap: break-word;
        font-size: 13px;
    }
    .user_dropDown_info p{
        display:block;
        color:#424242;
        font-size:16px;
        font-weight: 600;
    }
    .user_newDropDown ol{
        float: right;
        border-left: solid 1px #eaeaea;
        list-style: none;
        margin: 0px;
        padding-bottom: 0px;
        width: 140px;
        min-height: 88px;
        padding-left: 18px;
        margin-left: 15px !important;

    }
    #user .user_newDropDown ol li{
        padding:0px;
    }

    #user .user_newDropDown ol li a{
        display: block;
        padding: 0px 0px;
        color: #424242;
        font-size: 12px;
        font-weight: normal;
        line-height: 26px !important;
    }
    #user .user_newDropDown ol li a:hover{
        color:#2074bd;
        background:none;
    }
    .sibme_coaching p a {color:#ccc;    font-weight: 400;}
    .sibme_coaching p a:hover{color:#fff;}
    .sibme_coaching p .ac_whiteicon{ display:none;}
    .sibme_coaching p a:hover .ac_gryicon{ display:none;}
    .sibme_coaching p a:hover .ac_whiteicon{ display: inline-block;}

    .svg path {
        transition:0.3s all !important;
    }
    #user .photo{
        padding: 0px !important;      
        width: 40px;
        height: 40px;
        border-radius: 50%;
        -o-object-fit: cover;
        object-fit: cover;
        position: relative;
        top: -9px;
    }
</style>
<div class="container">

    <?php
    $controllerName = strtolower($this->params['controller']);
    if ($controllerName != 'launchpad') {
        ?>
        <div id="logo">
            <a href="<?php echo $this->base . '/dashboard/home'; ?>"></a>
        </div>
    <?php } ?>

    <?php if ($controllerName != 'launchpad') {
        ?>
        <div class="sibme_coaching">
            <?php if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1 && isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>
                <p><a href = "/launchpad" data-original-title="<?php echo $translation['change_account']; ?>" rel="tooltip" data-placement="bottom" class="ftooltip"> <?php echo $user_current_account['accounts']['company_name']; ?>   <?php if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1 && isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>  <img class="ac_whiteicon" width="19" src="<?php echo $this->webroot . 'app/img/icon-switch-account.svg' ?>" /> <img class="ac_gryicon" width="19" src="<?php echo $this->webroot . 'app/img/icon-switch-account_gry.svg' ?>" />


                        <?php endif; ?></a></p>
            <?php else: ?>
                <p><a href = "javascript:void(0);" data-placement="bottom" class="ftooltip"><?php echo $user_current_account['accounts']['company_name']; ?></a></p>
            <?php endif; ?>
            <?php if ($user_current_account['accounts']['in_trial'] == '1'): ?>
                <span><?php echo $translation['menu_trial_expires']; ?> <?php echo $expiry_date; ?> <a href="<?php echo $this->base . '/accounts/account_settings_all/' . $user_current_account['accounts']['account_id'] . '/' . '4' ?>"><?php echo $translation['menu_upgrade']; ?></a></span>
            <?php endif; ?>
        </div>
    <?php } ?>
    <div id="user" class="dropdown">
        <div class="dropdown-toggle">
            <?php
            $user_img = $this->Session->read('user_top_photo');
            if ($user_img) {
                $user_photo = $user_img;
                $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user_current_account['User']['id'] . "/" . $user_photo, $user_photo);
            } else {
                $user_photo = $user_current_account['User']['image'];
                $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user_current_account['User']['id'] . "/" . $user_photo, $user_photo);
            }
            ?>
            <?php if (isset($user_photo) && $user_photo != ''): ?>
                <?php echo $this->Html->image($avatar_path, array('alt' => $user_current_account['User']['first_name'] . ' ' . $user_current_account['User']['last_name'], 'id' => 'user_top_photo', 'class' => 'photo ph', 'height' => '25', 'rel' => 'tooltip', 'width' => '25')); ?>
            <?php else: ?>
                <img width="25" height="25" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo" alt="<?php echo $user_current_account['User']['first_name'] . ' ' . $user_current_account['User']['last_name']; ?>" style="float: left !important;">
            <?php endif; ?>
            <?php
            if (isset($user_current_account['User']) && $user_current_account['User'] != ''):
                echo "<span class='span-wrap wrap' style='text-overflow: ellipsis;overflow: hidden;white-space: nowrap;'>" . $user_current_account['User']['first_name'] . "</span>";
            endif;
            ?>
        </div>
        <div style ="display:none;" class="user_newDropDown">
            <div class="user_dropDown_info">
                <p><?php echo $user_current_account['User']['first_name'] . ' ' . $user_current_account['User']['last_name']; ?></p>
                <span><?php echo $user_current_account['User']['email']; ?></span>
                <span class="account_info"><?php echo $user_type; ?></span>
            </div>
            <ol>
                <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>
                    <li><a href="<?php echo $this->base . '/home/user-settings' ?>"><?php echo $translation['menu_user_settings']; ?></a></li>
                <?php endif; ?>
                <?php if ($user_permissions['UserAccount']['role_id'] == '100' || $user_permissions['UserAccount']['role_id'] == '110'):
                    ?>
                    <li><a href="<?php echo $this->base . '/accounts/account_settings_main/' . $user_current_account['accounts']['account_id']; ?>"><?php echo $translation['menu_account_settings']; ?></a></li>
                <?php endif; ?>
                <?php if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1 && isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>
                    <!-- <li><a href="/launchpad">Launchpad</a></li> -->
                <?php endif; ?>
                <!-- <li><a id="top_menu_sibme_learning_center" href="<?php // echo $this->base . '/app/sibme_learning_center';?>"><?php // echo "Sibme Learning Center";//$translation['menu_sibme_learning_center']; ?></a></li> -->
                <li><a href="<?php echo $this->base . '/users/logout' ?>"><?php echo $translation['menu_logout']; ?></a></li>
            </ol>
            <div class="clear"></div>

            <div class="user_info_bottom">
                <?php if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1 && isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>  <a href="/launchpad"> <?php endif; ?>
                    <span><?php echo $user_current_account['accounts']['company_name']; ?></span><?php if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1 && isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?> <img width="19" src="<?php echo $this->webroot . 'app/img/icon-switch-account_grey.svg' ?>" />  <?php endif; ?>
                    <?php if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1 && isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>    </a> <?php endif; ?>
            </div>
        </div>
        <ul style="display:none;" id="nav" class="dropdown-content">
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>
                <li><a tabindex="-1"  href="<?php echo $this->base . '/home/user-settings' ?>"><?php echo $translation['menu_user_settings']; ?></a></li>
            <?php endif; ?>
            <?php if ($user_current_account['accounts']['allow_launchpad'] == '1'): ?>
                <li><a tabindex="-1" href="<?php echo $this->base . '/Accounts/create/' . $user_current_account['User']['id'] . "/" . $user_current_account['accounts']['account_id']; ?>"><?php echo $translation['menu_create_account']; ?></a></li>
            <?php endif; ?>
            <?php
            if ($user_permissions['UserAccount']['role_id'] == '100'):
                ?>
                <li><a tabindex="-1" href="<?php echo $this->base . '/accounts/account_settings_main/' . $user_current_account['accounts']['account_id']; ?>"><?php echo $translation['menu_account_settings']; ?></a></li>
                <?php
            endif;
            ?>

            <?php if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1 && isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>
                <!--   <li>
                      <a tabindex="-1" href="/launchpad" class="fly" href="#">Launchpad</a> -->
                <!--                    <ul>
                <?php //foreach ($allAccounts as $accounts):   ?>
                <?php
                //if ($user_current_account['accounts']['account_id'] == $accounts['accounts']['account_id'])
                //   continue;
                ?>
                                            <li class="<?php //if ($selectedAccount == $accounts['accounts']['account_id']):              ?> down-active <?php //endif;              ?>">
                                                <a <?php //echo strlen($accounts['accounts']['company_name']) > 17 ? ' title="' . $accounts['accounts']['company_name'] . '"' : ''                ?> tabindex="-1" href="<?php //echo $this->base . '/launchpad/account/' . $accounts['accounts']['account_id']                ?>">
                <?php //echo strlen($accounts['accounts']['company_name']) > 17 ? substr($accounts['accounts']['company_name'], 0, 18) . '...' : $accounts['accounts']['company_name'];   ?>
                                                </a>
                                            </li>
                <?php //endforeach;   ?>
                                    </ul>-->
                <!-- </li> -->
            <?php endif; ?>
<!--            <li><a href="<?php //echo $this->base . '/app/language_update_in_cookie_session/'.$translation['language_value_menu'] ?>"><?php //echo $translation['language_menu']; ?></a></li>-->
            <li><a tabindex="-1" href="<?php echo $this->base . '/users/logout' ?>" data-method="delete" rel="nofollow"><?php echo $translation['menu_logout']; ?></a></li>
        </ul>

    </div>
    <?php
    $lst_login = $this->Session->read('last_logged_in_user');
    if (!empty($lst_login['id'])):
        ?>
        <div style="float: right;"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>"><?php echo $translation['menu_go_back_to_original_user']; ?></a></div>
        <?php
    endif;
    if ($controllerName != 'launchpad') {
        ?>
        <nav class="nav nav-header">
            <ul>
                <?php if ($this->Session->read('totalAccounts') > 1) { ?>
                                            <!--                    <li <?php //if (isset($this->name) && $this->name == 'Launchpad' && isset($this->action) && $this->action == 'index'):          ?> class="active"<?php //endif;          ?>><?php //if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1):          ?><a href="<?php //echo $this->base . '/launchpad'          ?>">Launchpad</a><?php //else:          ?><a class="inactive-tab">Launchpad</a> <?php //endif;          ?></li>-->
                    <?php
                }
                ?>
                <!-- <li <?php //if (isset($this->name) && $this->name == 'Dashboard' && isset($this->action) && $this->action == 'home'):          ?> class="active"<?php //endif;          ?>><?php //if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1):          ?><a href="<?php //echo $this->base . '/Dashboard/home'          ?>">Dashboard</a><?php //else:          ?><a class="inactive-tab">Dashboard</a> <?php //endif;          ?></li> -->
                <?php if ($user_permissions['UserAccount']['parmission_access_my_workspace'] == '1'): ?>
                    <?php if ($user_permissions['UserAccount']['role_id'] != '125'): ?>
                        <li <?php if (isset($this->name) && $this->name == 'MyFiles'): ?>class="active"<?php endif; ?> style="display:block;">
                            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><a href="<?php echo $this->base . '/workspace/workspace/home' ?>"><?php echo $translation['menu_workspace']; ?></a><?php else: ?><a class="inactive-tab"><?php echo $translation['menu_my_workspace']; ?></a> <?php endif; ?>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
                <li <?php if (isset($this->name) && $this->name == 'Huddles'): ?> class="active"<?php endif; ?>><?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><a href="<?php echo $this->base . '/video_huddles/list' ?>"><?php echo $translation['menu_huddles']; ?></a><?php else: ?><a class="inactive-tab"><?php echo $translation['menu_huddles']; ?></a> <?php endif; ?></li>
                <?php /*  ?>
                  <li <?php if (isset($this->name) && $this->name == 'Observe'): ?> class="active"<?php endif; ?>><?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><a href="<?php echo $this->base . '/Observe' ?>">Observe</a><?php else: ?><a class="inactive-tab">Observe</a> <?php endif; ?></li>
                  <?php */ ?>
                <?php if ($user_permissions['UserAccount']['role_id'] == '100' || $user_permissions['UserAccount']['role_id'] == '110' || ($user_permissions['UserAccount']['role_id'] == '120' && $user_permissions['UserAccount']['permission_access_video_library'] == '1') || ($user_permissions['UserAccount']['role_id'] == '115' && $user_permissions['UserAccount']['permission_access_video_library'] == '1') || ($user_permissions['UserAccount']['role_id'] == '125' && $user_permissions['UserAccount']['permission_access_video_library'] == '1')): ?>
                    <?php if ($this->Custom->get_account_video_permissions($user_current_account['accounts']['account_id'])): ?>
                        <li <?php if (isset($this->name) && $this->name == 'VideoLibrary'): ?> class="active"<?php endif; ?>><?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><a href="<?php echo $this->base . '/VideoLibrary/' ?>"><?php echo $translation['menu_library']; ?></a><?php else: ?><a class="inactive-tab"><?php echo $translation['menu_library']; ?></a> <?php endif; ?></li>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ($this->Custom->check_enable_analytics($user_current_account['accounts']['account_id'])) { ?>
                    <?php if ($analytics_permission == 1 || $this->Session->read('role') == '100' || $this->Session->read('role') == '110' || $this->Session->read('role') == '115') { ?>


                        <li
                            <?php if (isset($this->name) && $this->name == 'Analytics'): ?> class="active"<?php endif; ?>><?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><a href="<?php echo $this->base . '/analytics_angular/home' ?>"><?php echo $translation['menu_analytics']; ?></a><?php else: ?><a class="inactive-tab"><?php echo $translation['menu_analytics']; ?></a> <?php endif; ?>

                        </li>

                    <?php } ?>

                <?php } ?>



                <?php if ($user_permissions['UserAccount']['role_id'] == '110' || $user_permissions['UserAccount']['role_id'] == '100'): ?>
                    <li
                        <?php if (isset($this->name) && $this->name == 'Users' || isset($this->name) && $this->name == 'Permissions' && $this->action == "assign_user"): ?> class="active"<?php endif; ?>><?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><a href="<?php echo $this->base . '/users/administrators_groups' ?>"><?php echo $translation['menu_people']; ?></a><?php else: ?><a class="inactive-tab"><?php echo $translation['menu_people']; ?></a> <?php endif; ?>

                    </li>
                <?php endif; ?>
                <?php if (($user_permissions['UserAccount']['role_id'] == '120' && $user_permissions['UserAccount']['permission_administrator_user_new_role'] == '1') || ($user_permissions['UserAccount']['role_id'] == '115' && $user_permissions['UserAccount']['permission_administrator_user_new_role'] == '1')): ?>
                    <li <?php if (isset($this->name) && $this->name == 'Users' || isset($this->name) && $this->name == 'administrators_groups' || $this->action == "assign_user"): ?>class="active"<?php endif; ?>>
                        <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><a href="<?php echo $this->base . '/users/administrators_groups' ?>"><?php echo $translation['menu_people']; ?></a><?php else: ?><a class="inactive-tab"><?php echo $translation['menu_people']; ?></a> <?php endif; ?>
                    </li>
                <?php endif; ?>
                <?php
                if (isset($user_permissions['accounts']['enable_edtpa']) && $user_permissions['accounts']['enable_edtpa'] == '1' && $this->Session->read('role') != '125') {
                    ?>
                    <li <?php if (isset($this->name) && $this->name == 'Edtpa'): ?>class="active"<?php endif; ?>>
<!--                        <a  href="<?php //echo $this->base . '/Edtpa/edtpa' ?>">edTPA</a>-->
                    </li>
                <?php } ?>
            </ul>
        </nav>
    <?php } ?>
</div>

<style type="text/css">
    .inactive-tab{
        background-color: #ccc !important;
    }
</style>
<script type="text/javascript">
    var current_user_login = '<?php echo $user_current_account['User']['username'] ?>';
    var current_user_id = '<?php echo $user_current_account['User']['id'] ?>';
</script>

<?php //if (IS_QA): ?>
<script>
    $(document).on("click", ".fsp-button--video", function () {
        var metadata = {
            video_recorded: 'Web',
            user_role: '<?php echo $user_role; ?>',
            is_in_trial: '<?php echo $in_trial_intercom; ?>',
            Platform: 'Web'

        };
        if ($(this).attr("title") == 'Start')
        {
            Intercom('trackEvent', 'video-recorded', metadata);
        }
    });

</script>
<?php //endif; ?>

<script>

    $(".dropdown-toggle").click(function (e) {
        $(".user_newDropDown").show();
        e.stopPropagation();
    });

    $(".user_newDropDown").click(function (e) {
        e.stopPropagation();
    });

    $(document).click(function () {
        $(".user_newDropDown").hide();
    });
    $(document).on("click", "div.fsp-source-list__item:nth-last-child(1)", function () {
        if ($.trim($('div.fsp-source-list__item:nth-last-child(1)').text()) == "Record Video") {
            alert("<?php echo $alert_messages['warning_if_your_internet_is_unstable']; ?>");
        }

    });

</script>