<?php 
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$loggedInUserRole = '';
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];

?>



            <div class="topsection">
                <span class="ficoncls"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                <span class="namecls"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                    <?php echo $row['AccountFolder']['name']; ?>
                    </a></span>
                <span class="optioncls">
                             <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1' || $user_permissions['UserAccount']['role_id']=='120'))): ?>
                    <a  id="editfolder" huddle_id ="<?php echo $row['AccountFolder']['account_folder_id']?>" rel="tooltip"  data-original-title="Edit" href="#<?php //echo $this->base . '/Folder/edit/' . $row['AccountFolder']['account_folder_id'] ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                <?php endif; ?>
                                <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1' || $user_permissions['UserAccount']['role_id']=='120'))): ?>
                    <a id="deletefolder" huddle_id_del="<?php echo $row['AccountFolder']['account_folder_id']  ?>" rel="tooltip" data-original-title="Delete" data-method="delete"  href="#<?php //echo $this->base . '/Folder/delete/' . $row['AccountFolder']['account_folder_id'] ?>"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                                <?php endif; ?>
                                <?php if (isset($folders) && count($folders)>1): ?>
                                <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1' || $user_permissions['UserAccount']['role_id']=='120'))): ?>
                    <a href="#" rel="tooltip" data-original-title="Move" data-toggle="modal" data-target="#movefolderto" class="move_huddle" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>" fold_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                    <?php endif; ?>
                    <?php endif; ?>
                </span>
            </div><!--top-->
            <style>
                .huddle_date{
                        font-weight: 100;
                        color: #333;
                    margin:0px 0px 8px 0px;
                }
                .folder_details{
                    margin: 0px;
                    padding:0px;
                   height:150px;
                }
                .folder_details li{
                    padding: 14px 0px;
                    color: #333;
                    font-weight: 600;
                    border-bottom: solid 1px #c4c4c4;
                }
                .folder_details li:last-child{
                    border-bottom:0px;
                }
                .huddle_createrName{
                    color:#858585;
                    font-size: 14px;
                    position: absolute;
    bottom: 10px;
                }
                .count_huddle{
                    float:right;
                    margin-right: 10px;
                    
                }
                .col-4sm{
                    position: relative;
                }
                .folder_link{
                    font-weight: normal;
                }
                .topsection{
                    height: 30px;
                }
            </style>
            
            
            <div class="huddle_date"><?php echo date('M d, Y',strtotime($row['AccountFolder']['created_date'])) ; ?><br></div>
           
            <a href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>" class="folder_link">
            <ul class="folder_details">
              <?php if($this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'],$user_id,2)>0){ ?> <li><img src="/app/img/coaching_huddle.png" />  Coaching Huddles<div class="count_huddle"><?php echo $this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'],$user_id,2); ?></div></li> <?php } ?>
               <?php if($this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'],$user_id,1)>0){ ?> <li><img src="/app/img/Collaboration_huddle.png" />  Collaboration Huddles<div class="count_huddle"><?php echo $this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'],$user_id,1); ?></div></li><?php } ?>
                <?php if ($this->Custom->check_if_eval_huddle_active($account_id)) { ?>
                                                <?php if ($this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 3) > 0) { ?> <li><img src="/app/img/evaluation.png" />  Assessment Huddles<div class="count_huddle"><?php echo $this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 3); ?></div></li><?php } ?>
                                            <?php } ?>
               <?php if($this->Custom->count_folders_in_folder($row['AccountFolder']['account_folder_id'],$user_id,$account_id)>0){ ?><li><img src="/app/img/folder_inside.png" />  Folders<div class="count_huddle"><?php echo $this->Custom->count_folders_in_folder($row['AccountFolder']['account_folder_id'],$user_id,$account_id); ?></div></li></li><?php } ?>
           </ul>
            </a>  
            <div class="huddle_createrName">  <?php echo 'Created By: '; echo $this->Custom->user_name_from_id($row['AccountFolder']['created_by']); ?></div>
           
     
