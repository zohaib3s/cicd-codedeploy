<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
$amazon_base_url = Configure::read('amazon_base_url');
$user = $this->Session->read('user_current_account');
$user_id = $user['User']['id'];
$user_role = $this->Custom->get_user_role_name($user['users_accounts']['role_id']);
$account_id = $user['accounts']['account_id'];
$isFirefoxBrowser = $this->Browser->isFirefox();
$chimgIndicator = $this->Custom->getSecureAmazonSibmeUrl('app/img/new/indicator.gif');
$chimgDefault = $this->Custom->getSecureAmazonSibmeUrl('app/img/home/photo-default.png');
$chimgDownload = $this->Custom->getSecureAmazonSibmeUrl('app/img/new/download.png');

function defaulttagsclasses1($posid) {
    $class_name = 'uncheck';
    if ($posid == 1) {
        $class_name = 'tags_qucls';
    } else if ($posid == 2) {
        $class_name = 'tags_sugcls';
    } else if ($posid == 3) {
        $class_name = 'tags_notescls';
    } else if ($posid == 4) {
        $class_name = 'tags_strangthcls';
    }
    return $class_name;
}

if (isset($videoDetail['doc']['id']) && $videoDetail['doc']['id'] != '') {
    $video_id = $videoDetail['doc']['id'];
    $video_id_rev_for = 'example_video_' . $videoDetail['doc']['id'];
}

$ob_date = gmdate("H:i:s", (int) $videoDetail['doc']['current_duration']);
$ob_date = explode(':', $ob_date);
?>
<input id="account_id" type="hidden" value="<?php echo $account_id ?>">
<input id="user_id" type="hidden" value="<?php echo $user_id ?>">
<input id="huddle_id" type="hidden" value="<?php echo $huddle_id ?>">
<input id="video_id" type="hidden" value="<?php echo $video_id ?>">
<input id="count_comments" type="hidden" value="<?php echo $count_comments ?>">
<style>
    #comment_comment:focus {
        z-index: 20;
        /*        box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82,168,236,0.6) !important;
                border-color: rgba(82,168,236,0.8) !important;*/
        border-color: #fff !important;
        box-shadow: none !important;
    }

    .v_control button.fast {
        background: #3498DB;
    }

    .v_control button {
        cursor: pointer;
        border: 0;
        padding: 7px;
        border-radius: 5px;
        color: #fff;
    }

    .v_control button.wrin {
        background: #3a79a4;
    }

    .v_control {
        margin-top: 10px;
    }

    .v_control button img {
        width: 35px;
    }

    .timer_box2 {
        background: #ed1c24;
        font-size: 9px;
        color: #fff;
        padding: 2px 5px;
        position: absolute;
        right: 0px;
        top: -18px;
    }

    .video_recording_container {
        background: #000 url('/app/webroot/img/camera.png') no-repeat center center;
        padding: 20px;
        position: relative;
        height: 235px;
        /*//margin-top:10px;*/

    }

    .video_recording_container .timer_box {
        color: #fff;
        font-size: 13px;
        position: absolute;
        right: 10px;
        top: 10px;
    }

    .live_record {
        position: absolute;
        left: 10px;
        top: 10px;
        color: #fff;
        font-size: 13px;
    }

    .rec_border {
        border-bottom: solid 1px #D3D2D6;
        height: 1px;
        margin: 10px 0px;
    }

    .petsCls {
        float: right;
        margin-right: 10px;
        /*margin-top: -27px;*/
        right: 0;
        position: relative;
    }

    .petsCls label {
        font-weight: normal !important;
        font-size: 13px;
        float: left;
    }

    input#press_enter_to_send {
        margin-top: 3px;
        float: left;
        margin-right: 4px;
    }

    .input-group {
        clear: both;
    }

    .document_outer {
        background: #fff;
        padding: 10px;
        position: absolute;
        z-index: 99;
        border: 1px solid #ddd;
        float: right;
        right: 9px;
        top: 84px;
        display: none;
    }

    .document_outer:before {
        pointer-events: none;
        position: absolute;
        z-index: -1;
        content: '';
        border-style: solid;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-property: transform;
        transition-property: transform;
        left: calc(50% - 10px);
        top: 0;
        border-width: 0 10px 10px 10px;
        border-color: transparent transparent #e1e1e1 transparent;
        right: 1px;
        left: inherit;
        -webkit-transform: translateY(-10px);
        transform: translateY(-10px);
    }

    .document_outer a {
        display: block !important;
        height: inherit !important;
        margin: 0 5px !important;
    }

    .document_click {
        float: right;
        cursor: pointer;
        width: 22px;
    }

</style>

<script type="text/javascript">


    $(document).ready(function () {
        $('.document_click').click(function (e) {
            $('.document_outer').slideToggle();
        });

    });


</script>


<div id="comments_user_meta_data" style="display:none;">
    <a href="<?php echo $this->base . '/users/editUser/' . $user['User']['id']; ?>">
        <?php if (isset($user['User']['image']) && $user['User']['image'] != ''): ?>
            <?php
            $profile_image = $this->Custom->getSecureAmazonSibmeUrl("static/users/" . $user['User']['id'] . "/" . $user['User']['image'], $user['User']['image']);
            echo $this->Html->image($profile_image, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '21', 'width' => '21', 'align' => 'left'));
            ?>
        <?php else: ?>
            <img width="21" height="21" src="<?php echo $chimgDefault; ?>" class="photo photo inline"
                 rel="image-uploader" alt="Photo-default">
             <?php endif; ?>
    </a>
    <div class="comment-header"><?php echo $user['User']['first_name'] . " " . $user['User']['last_name']; ?></div>
</div>

<div class="tab-content <?php echo $tab != 2 ? 'tab-active' : 'tab-hidden'; ?>" id="tabbox1"
     style="visibility: visible;">
         <?php if ($videos != '' && $videoDetail == ''): ?>

        <?php
        $vdo_data = array(
            'total_videos' => $videos_total
        );
        echo $this->element('MyFiles/myFilesToolsBarVideos', $vdo_data);
        ?>
        <div class="docs-list">
            <?php if (isset($files_mode) && $files_mode == 'list'): ?>
                <div class="docs-list__head">
                    <input id="video-id" type="hidden" value="none"/>
                    <a><label class="ui-checkbox" style="margin-left: -30px;"><input type="checkbox"
                                                                                     class="selectall-videos"/></label></a>
                    <a onclick="return deleteVideos()" style="cursor: pointer;" rel="nofollow" id="delete-main-comments"
                       class="icon-delete blue-link delete js-file-delete"><?php echo $language_based_content['delete_videos_workspace']; ?></a>
                    <a href="#" class="js-file-download icon-download" onclick="return MyFiles.downloadFiles('video')"><?php echo $language_based_content['download_rescource_tab_huddles_workspace']; ?></a>
                    <a onclick="return rename();" style="cursor: pointer;" class="js-file-rename icon-rename"><?php echo $language_based_content['rename_workspace']; ?></a>
                    <a id="copy-video" onclick="return copy();" class="huddle-contents js-file-copy icon-copy"
                       data-reveal-id="" href="#"><?php echo $language_based_content['copy_videos_workspace']; ?></a>
                </div>
                <table class="docs-list__table my-file-videos-list">
                    <?php echo $videos_html; ?>
                </table>
            <?php elseif (isset($files_mode) && $files_mode == 'grid'): ?>
                <div class="my-file-videos-list">
                    <?php echo $videos_html; ?>
                </div>
            <?php endif; ?>

        </div>
        <img id="loading_gif" style="margin-left:473px;margin-bottom:-32px;display:none;"
             src="<?php echo $this->webroot . 'img/loading.gif' ?>">
        <div class="load_more">
            <a class="btn btn-green" id="video-load-more" style="display:none;"><?php echo $alert_messages['load_more_videos_alert']; ?></a>
        </div>
        <?php if ($videos_total > 12) : ?>
            <script type="text/javascript">
                        $('#video-load-more').show();</script>
        <?php endif; ?>
    <?php elseif ($videos != '' && $videoDetail != ''): ?>

        <script type="text/javascript">
            $(document).ready(function (e) {
                //var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
                var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
                if (iOS) {
                    var now = new Date().valueOf();
                    setTimeout(function () {
                        if (new Date().valueOf() - now > 100)
                            return;
                        //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                        return;
                    }, 50);
                    window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $videoDetail['doc']['id']; ?>&isWorkspace=true&account_id=<?php echo $videoDetail['doc']['account_id'] ?>";
                }

                var ua = navigator.userAgent.toLowerCase();
                var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
                if (isAndroid) {
                    var now = new Date().valueOf();
                    setTimeout(function () {
                        if (new Date().valueOf() - now > 100)
                            return;
                        //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                        return;
                    }, 50);
                    window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $videoDetail['doc']['id']; ?>&isWorkspace=true&account_id=<?php echo $videoDetail['doc']['account_id'] ?>";
                }


            });

        </script>
        <div class="left-box video-detail">
            <a href="<?php echo $this->base . '/MyFiles' ?>" class="back">Back to all videos</a>
            <input id="txtHuddleID" type="hidden" value="<?php echo $huddle_id ?>"/>

            <div class="video-outer">
                <?php
                if (isset($videoDetail['doc']) && $videoDetail['doc'] != ''):
                    $video_title = '';
                    if (!empty($videoDetail['afd']['title'])) {
                        $video_title = $videoDetail['afd']['title'];
                    } else {
                        $video_title = 'Untitled Video';
                    }
                    ?>
                    <div id="video_span">
                        <div id="vidTitle" class="mmargin-top"
                             name="click to edit title"> <?php echo (strlen($video_title) > 75) ? "<a title='" . $video_title . "'>" . mb_substr($video_title, 0, 75) . "...</a>" : $video_title; ?></div>
                        <div class="editArea">
                            <input id="ajaxInput" type="text">
                            <input id="video-id" type="hidden" value="<?php echo $videoDetail['doc']['id'] ?>"/>
                            <input type="hidden" id="title_change" value="<?php echo $video_title; ?>">
                            <div class="console" style="right:18px;">
                                <input class="submit btn btn-green" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type='button' value="Save"/>
                                <input class="btn btn-white" type="button" value="Cancel"/>
                            </div>
                        </div>

                        <?php if (!$isFirefoxBrowser && $videoDetail['doc']['published'] == 1): ?>
                            <a id="inline-crop-panel"
                               href="<?php echo $this->base . '/MyFiles/trim/' . $videoDetail['doc']['id'] . '/' . $huddle_id ?>"
                               class="iframe fancybox.iframe right mmargin-top crop-image"
                               title="edit video"
                               rel="tooltip">&nbsp;</a>
                           <?php endif; ?>

                        <?php if ($videoDetail['doc']['published'] == 1): ?>
                            <span id="thumb_regen_<?php echo $videoDetail['doc']['id'] ?>"
                                  document_id="<?php echo $videoDetail['doc']['id'] ?>"
                                  huddle_id="<?php echo $videoDetail['afd']['account_folder_id'] ?>"
                                  class="regen-thumbnail-image regen-thumbnail-image-workspace regen-thumbnail-image-inner"
                                  title="Regenerate Thumnail"
                                  rel="tooltip" style="display:none;">&nbsp;</span>


                            <script type="text/javascript">

                                $(document).ready(function () {

                                    $('.regen-thumbnail-image').click(function () {

                                        var r = confirm('Do you want to Regenerate Thumbnail?');
                                        if (r == true) {

                                            var document_id = $(this).attr('document_id');
                                            var huddle_id = $(this).attr('huddle_id');
                                            $.ajax({
                                                type: 'POST',
                                                url: home_url + '/Huddles/regenerate_thumbnail/' + document_id + '/' + huddle_id,
                                                success: function (res) {

                                                    alert('<?php echo $alert_messages['thumbnail_generated_successfully']; ?>');
                                                },
                                                errors: function (response) {
                                                    alert('<?php echo $alert_messages['unable_to_generate_thumbnail']; ?>');
                                                }
                                            });
                                        }

                                    });
                                });</script>

                        <?php endif; ?>

                        <a href="<?php echo $this->base . '/MyFiles/deleteFile/' . $videoDetail['doc']['id'] . '/1' ?>"
                           class="icon-trash right mmargin-top smargin-left"
                           data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                           data-method="delete"
                           title="delete"
                           rel="nofollow tooltip"></a>

                        <a onMouseOut="hide_sidebar()" href="#" class="appendix right mmargin-top">?</a>
                        <div class="appendix-content appendix-narrow card" style="display: none;">
                            <p style="word-wrap: break-word; padding: 10px; margin-top: 0px;">If your video is not
                                loading or playing, please upgrade your browser to the most recent version. <?php $this->Custom->get_site_settings('site_title') ?> is
                                compatible with <a style="float: none;" target="blank"
                                                   href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie-9/worldwide-languages">Internet
                                    Explorer</a>,<a style="float: none;" target="blank"
                                                href="https://www.google.com/intl/en/chrome/browser/?&brand=CHMB&utm_campaign=en&utm_source=en-ha-na-us-sk&utm_medium=ha">Google
                                    Chrome</a>, and <a style="float: none;" href="http://www.apple.com/safari/"
                                                   target="blank">Safari</a>. If your video is still not playing in
                                one of these browsers, please contact &nbsp; <a style="float: none;"
                                                                                href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                            </p>
                        </div>


                        <div class="clearfix"></div>
                        <?php
                        $timeS = array();
                        $videoID = $videoDetail['doc']['id'];

                        $document_files_array = $this->Custom->get_document_url($videoDetail['doc']);

                        if (empty($document_files_array['url'])) {
                            $videoDetail['doc']['published'] = 0;
                            $document_files_array['url'] = $videoDetail['doc']['original_file_name'];
                            $videoDetail['doc']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $videoDetail['doc']['encoder_status'] = $document_files_array['encoder_status'];
                        }
                        $videoFilePath = pathinfo($document_files_array['url']);
                        $videoFileName = $videoFilePath['filename'];
                        if ($videoComments) {
                            foreach ($videoComments as $cmt) {
                                if (!empty($cmt['Comment']['time']))
                                    $timeS[] = $cmt['Comment']['time'];
                            }
                        }
                        ?>

                        <input type="hidden" id="txtCurrentVideoID" value="<?php echo $videoID; ?>"/>
                        <input type="hidden" id="txtCurrentVideoUrl"
                               value="<?php echo $this->base . '/Huddles/view/' . "/1/$videoID" ?>"/>
                               <?php
                               $document_files_array = $this->Custom->get_document_url($videoDetail['doc']);

                               if (empty($document_files_array['url'])) {
                                   $videoDetail['doc']['published'] = 0;
                                   $document_files_array['url'] = $videoDetail['doc']['original_file_name'];
                                   $videoDetail['doc']['encoder_status'] = $document_files_array['encoder_status'];
                               } else {
                                   $videoDetail['doc']['encoder_status'] = $document_files_array['encoder_status'];
                               }

                               if (isset($videoDetail['doc']['published']) && $videoDetail['doc']['published'] == 1 && $document_files['DocumentFiles']['transcoding_status'] != 5):
                                   ?>
                                   <?php
                                   $video_url = $document_files_array['url'];
                                   $thumbnail_image_path = $document_files_array['thumbnail'];
                                   ?>
                            <video oncontextmenu="return false;"
                                   id="example_video_<?php echo $videoDetail['doc']['id'] ?>"
                                   class="video-js vjs-default-skin"
                                   controls preload="metadata" width="500" height="321"
                                   poster="<?php echo $thumbnail_image_path; ?>"
                                   data-markers="[<?php echo implode(',', $timeS); ?>]">
                                <source src="<?php echo $video_url; ?>" type='video/mp4'>
                            </video>
                            <!--                <div class="v_control">
                                                <button onclick="skip(-10)" class="wrin"><img src="<?php //echo  $this->webroot.'img/fast_forward_icon2.png'
                                   ?>"> Rewind</button>
                                                <button onclick="skip(10)" class="fast">Fast Forward <img src="<?php //echo  $this->webroot.'img/fast_forward_icon.png'
                                   ?>"></button>
                                            </div>-->

                            <div style="padding-top: 15px;">
                                <p class="tip">Tip: Click on the video player timeline bar above to add time-specific
                                    annotations.
                                    <?php if ($user['users_accounts']['permission_video_library_upload'] == 1): ?>
                                        <?php if (isset($videoDetail['doc']['published']) && $videoDetail['doc']['published'] == 1): ?>
                                            <a data-total-comments="<?php echo $total_comments; ?>" id="copy-video"
                                               class="js-file-copy  copy huddle-contents-single" data-reveal-id=""
                                               data-toggle="modal" rel="nofollow" data-target="#copyFiles_2" href="#">Copy</a>
                                           <?php endif; ?>
                                       <?php endif; ?>
                                    <span class="badge right" data-original-title="video played"
                                          rel="tooltip"><?php echo $videoDetail['doc']['view_count']; ?></span>
                                    <a href="<?php echo $this->webroot . 'Huddles/download/' . $videoDetail['doc']['id'] ?>">
                                        <img alt="Download" class="right smargin-right" height="14" rel="tooltip"
                                             src="<?php echo $chimgDownload; ?>" title="download video" width="14"/>
                                    </a>
                                </p>
                            </div>
                        <?php else: ?>
                            <?php
                            if ($videoDetail['doc']['published'] == 0 && $videoDetail['doc']['doc_type'] == 3 && $videoDetail['doc']['is_processed'] == 5) {
                                ?>
                                <div id="div_video_recording_container_parent">
                                    <div id="div_video_recording_container" class="video_recording_container">
                                        <div class="live_record">
                                            <img src="/app/webroot/img/recording_img.png"/> Live Recording
                                        </div>
                                        <div class="timer_box">
                                            <span id="sw_h"><?php echo $ob_date[0] ?></span>:<span
                                                id="sw_m"><?php echo $ob_date[1] ?></span>:<span
                                                id="sw_s"><?php echo $ob_date[2] ?></span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            } else if ($videoDetail['doc']['published'] == 0 && $videoDetail['doc']['doc_type'] == 3 && $videoDetail['doc']['is_processed'] == 4) {
                                ?>
                                <?php
                                if ($videoDetail['doc']['doc_type'] == 3 && $videoDetail['doc']['is_processed'] == 4 && $videoDetail['doc']['published'] == 0 && $videoDetail['doc']['upload_status'] != 'uploaded' && $videoDetail['doc']['upload_progress'] < 100) {
                                    ?>
                                    <div id="uploading-box-container-<?php echo $videoDetail['doc']['id'] ?>"
                                         style="width: 500px;height: 321px;background: #000;text-align: center;color: #fff;position: relative;">
                                        <div id="uploading-box-<?php echo $videoDetail['doc']['id'] ?>" class="vnb"
                                             style="position: absolute;background: #3e7554;z-index: 2;width:<?php echo $videoDetail['doc']['upload_progress'] ?>%;height: 146px;text-align: center;color: #fff;padding-top: 65px;opacity:0.5;height: 322px;">
                                            &nbsp;
                                        </div>
                                        <span id="uploading-box-span-<?php echo $videoDetail['doc']['id'] ?>"
                                              style="width: 110px;color: #fff;position: absolute;left: 206px;top: 40%;text-align: center;text-transform: uppercase;"><?php echo $videoDetail['doc']['upload_progress'] ?>
                                            % <?php echo $videoDetail['doc']['upload_status'] ?></span>
                                    </div>

                                    <?php
                                } else {
                                    ?>

                                    <div id="uploading-box-container-<?php echo $videoDetail['doc']['id'] ?>"
                                         style="width: 500px;height: 321px;background: #000;text-align: center;color: #fff;">
                                        <div class="empty_video_box"
                                             style="padding-top:125px !important; padding-left: 2px !important;width: 500px; ">
                                            <!--<p>Video is currently uploading. Once your video has successfully uploaded it will take a few minutes to process and sync your notes. You will receive an email when your video is ready to be viewed.</p>-->
                                            <img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                                                 style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;"/>
                                            <p>Your video is currently processing. You will receive an email
                                                notification when your video is ready to be viewed.</p>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                            <?php } else {
                                ?>
                                <div style="width: 500px; height: 321px; background: #000; text-align: center; color: #fff;">
                                    <div class="empty_video_box"
                                         style="padding-top:125px !important; padding-left: 2px !important;width: 500px; ">
                                             <?php if ($videoDetail['doc']['encoder_status'] == 'Error' || $document_files['DocumentFiles']['transcoding_status'] == 5): ?>
                                            <p>Video failed to process successfully. Please try again or contact <a
                                                    href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"
                                                    style="color: blue;text-decoration: underline;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                            </p>
                                        <?php else : ?>
                                            <img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                                                 style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;"/>
                                            <p>Your video is currently processing. You will receive an email
                                                notification when your video is ready to be viewed.</p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php endif; ?>

                        <input type="hidden" id="huddle_id" value="<?php echo $huddle_id; ?>"/>
                    </div>
                <?php else: ?>
                    <div id="video_span">
                        <div class="no-vid">
                            <p class="info">Select a video from the list below.</p>
                        </div>
                    </div>
                <?php endif; ?>

            </div>

            <div class="clear"></div>
            <?php
            $comment_box_display = 'style="display:block;"';
            $comment_box_add_btn = 'style="display:none;"';
            ?>
            <script type="text/javascript">
                $(document).ready(function (e) {
                    $('#txtVideoTags').tagsInput({
                        defaultText: 'Tags...',
                    });
                    $('.tagsinput').css('border', '1px solid #CCC');
                });</script>
            <div id="comment_add_form_html">

                <?php if ($this->Custom->is_enable_tags($account_id)): ?>
                    <!--                    <div class="divblockwidth">

                    <?php
                    if (count($tags) > 0) {
                        $count = 1;
                        foreach ($tags as $tag) {
                            ?>
                                                                                                                                                                                                                        <a class="default_tag <?php echo defaulttagsclasses($count) ?>" href="#" position_id="<?php echo $count; ?>" status_flag="0"># <?php echo $tag['AccountTag']['tag_title']; ?></a>
                            <?php
                            $count++;
                        }
                    }
                    ?>
                                        </div> -->
                <?php endif; ?>
                <div class="clear" style="margin-bottom:10px;"></div>
                <div class="top-right-box">
                    <a id="add_comment_button" class="btn btn-green left" <?php echo $comment_box_add_btn; ?>>Add
                        Comment</a>
                    <div style="clear: both;"></div>
                </div>

                <div id="comment_form_main" <?php echo $comment_box_display; ?>>
                    <form accept-charset="UTF-8" action="<?php echo $this->base; ?>/Huddles/addComments"
                          class="new_comment" id="comments-form1" method="post">
                        <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden"
                                                                              value="&#x2713;"/></div>
                        <input id="tokentag" type="hidden" name="authenticity_token"
                               value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE="/>
                        <input type="hidden" name="associate_default_tags" value="1"/>
                        <div class="input-group"
                             <?php if ($videoDetail['doc']['published'] == 0 && $videoDetail['doc']['doc_type'] == 3 && $videoDetail['doc']['is_processed'] == 5) { ?>style="margin-top: 20px;"<?php } ?>>
                             <?php
                             if ($videoDetail['doc']['published'] == 0 && $videoDetail['doc']['doc_type'] == 3) {
                                 if ($videoDetail['doc']['is_processed'] == 5) {
                                     ?>
                                    <div class="timer_box2" style="display: none;">00:00:00</div>
                                    <?php
                                }
                            }
                            ?>
                            <?php if ($videoDetail['doc']['is_processed'] == 5 || $videoDetail['doc']['published'] == 1) { ?>
                                <div class="commentsOueterCls"
                                     style="border:1px solid rgb(204, 204, 204);margin-right: 1px;">
                                    <textarea cols="50" onkeyup="textAreaAdjust(this)"
                                              style="overflow:hidden;resize: none;border-radius:0px;margin-bottom: 5px;border: none;"
                                              id="comment_comment" name="comment[comment]" placeholder="Add a note..."
                                              rows="4"></textarea>
                                    <div class="petsCls">
                                        <input style="" type="checkbox" id="press_enter_to_send"
                                               name="press_enter_to_send"
                                               myssss="<?php echo $press_enter_to_send; ?>" <?php echo $press_enter_to_send == '1' ? 'checked' : ''; ?>>
                                        <label style="" for="press_enter_to_send">Press Enter to post</label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clear" style="clear: both;"></div>
                                <?php if ($this->Custom->is_enable_tags($account_id)): ?>
                                    <?php //if (count($tags) > 0) { ?>
                                    <div class="tags divblockwidth">
                                        <div style="position:relative;" class="video-tags-row row">
                                            <input type="text" name="txtVideoTags" data-default="Tags..."
                                                   id="txtVideoTags" value="" placeholder=""
                                                   style="width:112px !important; display: none;" readonly="readonly"/>
                                            <input style="display:none;" id='comment_attachment' type="file"
                                                   name="data[comment_attachment]">
                                            <img style="cursor:pointer;position: absolute;right: 5px;top: 5px;"
                                                 id='comment_attachment_img'
                                                 src="<?php echo $this->webroot . 'img/comment_box_attachment.jpg'; ?>">
                                        </div>
                                        <script>
                                            $('#comment_attachment_img').on('click', function (e) {
                                                $("#comment_attachment").click();
                                            });

                                            document.getElementById('comment_attachment').onchange = function () {
                                                var attached_image_name = $('#comment_attachment').val().replace(/C:\\fakepath\\/i, '');
                                                $("#attached_image_name").html('Attached File : ' + attached_image_name);

                                            };


                                        </script>
                                        <div class="clear" style="clear: both;"></div>
                                    </div>
                                    <?php //}        ?>
                                <?php endif; ?>
                            <?php } ?>
                            <div class="clear" style="clear: both;"></div>
                        </div>
                        <span style="position: relative; top: -14px;" id="attached_image_name"></span>
                        <input id="comment_access_level" name="comment[access_level]" type="hidden" value=""/>
                        <input id="synchro_time" name="synchro_time" type="hidden" value="{:value=&gt;&quot;&quot;}"/>
                        <input id="synchro_time_class" name="synchro_time_class" type="hidden" value="short_tag_0"/>
                        <input type="hidden" id="videoId" name="videoId" value="<?php echo $video_id ?>">
                        <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id ?>">
                        <input type="hidden" id="account_id" name="account_id" value="<?php echo $account_id ?>">

                        <input type="hidden" id="notifiable" name="notifiable"
                               value="<?php echo isset($notifiable) ? $notifiable : 1; ?>"/>

                        <?php if ($videoDetail['doc']['published'] == 1) { ?>

                            <div id="comment-for" class="input-group">
                                For:
                                <label for="for_synchro_time">
                                    <input id="for_synchro_time" name="for" type="radio" value="synchro_time"/>
                                    Time-specific (<span id="right-now-time">0:00</span>)
                                </label>

                                <label for="for_entire_video">
                                    <input id="for_entire_video" name="for" type="radio" value="entire_video" checked/>
                                    Whole video
                                </label>

                                <input style="margin-left: -7px;" type="checkbox" id="pause_while_type"
                                       name="pause_while_type" <?php echo $type_pause == '1' ? 'checked' : ''; ?>><label
                                       style="margin-left: 321px; margin-top: -25px;" for="pause_while_type">Pause
                                    video while typing</label>

                            </div>

                        <?php } else {
                            ?>
                            <input id="for_synchro_time" name="for" type="hidden" value="synchro_time"/>
                        <?php } ?>

                        <?php if ($videoDetail['doc']['is_processed'] == 5 || $videoDetail['doc']['published'] == 1) { ?>
                            <div class="input-group" style="clear: both;">
                                <input id="add-notes_myfiles" type="submit" name="submit" value="Add Note"
                                       class="btn btn-green" style="float: left;">
                                <input type="hidden" name="type" value="add_video_comments"/>
                                <input id="add_audio_comments" type="hidden" name="comment[audio_comments]" value=""/>
                                <a id="close_comment_form" class="btn btn-transparent js-close-comments-form"
                                   style="float: left;">Cancel</a>

                            </div>
                        <?php } ?>
                    </form>
                    <div id="indicator" style="display:none;"><img alt="Indicator"
                                                                   src="<?php echo $chimgIndicator; ?>"/>
                    </div>
                </div>


            </div>

        </div>
        <div class="right-box">
            <ul class="tabset commentstabs">
                <li class="">
                    <a id="comments" class="tab active" href="#commentsTab">
                        Notes (<?php echo $videoCommentsArray ? count($videoCommentsArray) : '0'; ?>)
                    </a>
                </li>
                <li class="">
                    <a id="attachment" class="tab" href="#attachmentTab">Attachments
                        (<?php echo $this->Custom->get_video_attachment_numbers($video_id); ?>)</a>
                </li>
                <div class="export-btns">
                    <img src="/img/amchart-download.png" class="document_click">
                    <div class="document_outer">
                        <a href="<?php echo $export_excel_url ?>" title="Export comments as Excel" id="comment-excel"
                           data-video-id="<?php echo $video_id; ?>" class="comment-excel tab-doc-cls"
                           style="margin-left: 5px;" rel="tooltip">Print Excel</a>
                        <a href="<?php echo $export_pdf_url ?>" title="Export comments as PDF" id="comment-acro"
                           data-video-id="<?php echo $video_id; ?>" class="comment-acro tab-doc-cls" target="_blank"
                           style="margin-left: 5px;" rel="tooltip">Print PDF</a>
                    </div>
                    <div style="clear: both;"></div>

                </div>
            </ul>

            <div class="table-container p-left0">
                <div class="tab-content tab-active" id="commentsTab"
                     style="visibility: visible;    position: relative;">
                         <?php if ($type != 'get_video_comments_obs') {
                             ?>
                        <div class="rightsearchcls" style="width: 267px;margin-top: 15px;">
                            <input type="button" id="btnSearchVideos" class="btn-search" value="">
                            <input class="text-input" id="txtSearchVideos" type="text" value=""
                                   placeholder="Search notes.." style="margin-right: 0px;width: 153px !important">
                            <span id="clearVideoButton" style="display: none; right: 98px;"
                                  class="clear-video-input-box clear_comment">X</span>
                        </div>
                        <div class="scrol_btn_cs">
                            <span class="scrolspan">Autoscroll</span>
                            <div class="onoffswitch">
                                <input type="checkbox" name="auto_scroll_switch" class="onoffswitch-checkbox"
                                       id="auto_scroll_switch"
                                       myss="<?php echo $auto_scroll_switch; ?>" <?php echo $auto_scroll_switch == '1' ? 'checked' : ''; ?>>

                                <label class="onoffswitch-label" for="auto_scroll_switch">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>
                            </div>
                        </div>
                        <div class="right-btns" style="width: 180px;">
                            <a id="add_comment_button" class="btn btn-green left"
                               style="width:120px;text-align:center;display:none;margin-top: 13px;float: left;">Add
                                Note</a>
                            <div class="srt-dropdowns select myspaceoption-cls" style="float: right">
                                <select id="cmt-sortings" class="" name="comments_sorting">
                                    <option <?php echo isset($srtType) && $srtType == 5 ? 'selected="selected"' : '' ?>
                                        value="5">newest
                                    </option>
                                    <option <?php echo isset($srtType) && $srtType == 1 ? 'selected="selected"' : '' ?>
                                        value="1">oldest
                                    </option>
                                    <option <?php echo isset($srtType) && $srtType == 2 ? 'selected="selected"' : '' ?>
                                        value="2">timestamp
                                    </option>

                                </select>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div style="clear: both;"></div>
                        <div id="vidComments" style="position:relative">
                            <input type="hidden" id="video_ID" name="video_ID"
                                   value="<?php echo $videoDetail['doc']['id'] ?>">

                            <?php echo $html_comments ?>

                        </div>
                    <?php } ?>
                    <div class="clear"></div>
                </div>
                <div class="tab-content tab" id="attachmentTab" style="visibility: hidden;">
                    <!-- docs start -->
                    <div id="docs-container"></div>
                    <!-- docs end -->
                </div> <!--tab2-->
            </div>

        </div>
        <script type="text/javascript">

            var observation_notes_array = [];</script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#inline-crop-panel").fancybox({
                    width: '70%',
                    height: '70%',
                    type: "iframe",
                    closeClick: false,
                    helpers: {
                        title: null,
                        overlay: {closeClick: false}
                    }
                });
                $("#inline-crop-panel").click(function () {
                    pausePlayer();
                });
    <?php if (isset($_REQUEST['open_trim']) && $_REQUEST['open_trim'] == '1'): ?>
                    pausePlayer();
                    $("#inline-crop-panel").trigger('click');
    <?php endif; ?>
                $('.btn-trim-video').click(function () {
                    $(this).next().submit();
                });
                $('.default_tag').on('click', function (e) {
                    e.preventDefault();
                    var posid = '';
                    tag_name = $(this).text();
                    tag_name.replace(/\s/g, "");
                    posid = $(this).attr('position_id');
                    //$( ".default_tag" ).addClass(defaulttagsclasses(0));
                    //var numItems = $('.yourclass').length;
                    $('.default_tag').each(function (index) {
                        console.log(index + ": " + $(this).text());
                        var tag_name1 = $(this).text();
                        tag_name1.replace(/\s/g, "");
                        $('#txtVideoTags').removeTag(tag_name1);
                        if ($(this).hasClass(defaulttagsclasses(index))) {
                            $('#txtVideoTags').removeTag(tag_name1);
                            $(this).removeClass(defaulttagsclasses(index) + 'bg');
                        }
                    });
                    $('.default_tag').each(function (index) {
                        if ($(this).attr('position_id') != posid) {
                            $(this).removeClass(defaulttagsclasses($(this).attr('position_id')));
                            $(this).addClass(defaulttagsclasses(0));
                        }
                    });
                    if ($(this).hasClass(defaulttagsclasses(posid))) {
                        $('#txtVideoTags').removeTag(tag_name);
                        $(this).addClass(defaulttagsclasses(0));
                        $(this).removeClass(defaulttagsclasses(posid));
                        $(this).attr('status_flag', '0');
                    }
                    else {
                        $('#txtVideoTags').addTag(tag_name);
                        $(this).addClass(defaulttagsclasses(posid));
                        $(this).removeClass(defaulttagsclasses(0));
                        $(this).attr('status_flag', '1')
                        $('#comment_comment').focus();
                    }
                });
                $("#comment_comment").keypress(function (e) {
    <?php if ($videoDetail['doc']['published'] == 0 && $videoDetail['doc']['doc_type'] == 3 && $videoDetail['doc']['is_processed'] == 5): ?>
                        $hours = $('#sw_h').text();
                        $min = $('#sw_m').text();
                        $sec = $('#sw_s').text();
                        $time = $hours + ':' + $min + ':' + $sec;
                        //   if ($('.timer_box2').text() == '00:00:00') {
                        $('.timer_box2').text($time);
                        $('.timer_box2').show();
                        var seconds = new Date('1970-01-01T' + $time + 'Z').getTime() / 1000;
                        $('#synchro_time').val('');
                        $('#synchro_time').val(seconds);
                        //       }

    <?php endif; ?>
                    $(this).css('border', '0');
                    $(this).css('border-radius', '0');
                    var str_length = $("#comment_comment").val().trim();
                    if (e.which == 13 && $("input#press_enter_to_send").prop('checked') == true) {
                        // $(this).blur();
                        e.preventDefault();
                        $("#add-notes_myfiles").trigger('click');
                    }
                });

                function generate_comment_html(postData) {

                    var html = '<ul class="comments comments_huddles" id="normal_comment_' + postData.internal_comment_id + '" style="overflow-x: hidden;">';
                    html += '<div rel="' + postData.internal_comment_id + '" id="comment_box' + postData.internal_comment_id + '" style="margin-top: 12px;">';
                    html += '<li class="synchro"> ';
                    html += '<div class="synchro-inner"> ';
                    html += '<i></i><span data-time="' + postData.observations_time + '" class="synchro-time">' + postData.observations_time + '</span>';
                    html += '</div></li>';
                    html += '<li class="comment thread" style="display: block;float: left;">';
                    html += $('#comments_user_meta_data').html();
                    html += '<div class="comment-body comment more">' + nl2br(postData.observations_comments) + '</div>'
                    html += '<div class="comment-footer">';
                    html += '<span class="comment-date">Saving comment....</span> <div class="comment-actions" style="width: 150px;">&nbsp;</div>'
                    html += ' </div>';
                    /*html += '<div id="docs-standards" class="docs-standards" style="margin-bottom: 20px;display: block;float: left;">';

                     if (postData.observations_standards != '') {

                     html += '<div id="tagFilterList"><div id="tagFilterContainer" class="btnwraper">';

                     var standards_selected = postData.observations_standards.split(',');

                     for (var i=0; i < standards_selected.length; i++) {

                     html += '<button class="btn btn-lilghtgray" style="margin-right: 2px;">'+ standards_selected[i] +'</button>';
                     }

                     html += '</div></div>';

                     }*/

                    if (postData.observations_tags != '') {
                        console.log(postData.observations_tags);
                        html += '<div id="tagFilterList"><div id="tagFilterContainer" class="btnwraper">';
                        var standards_selected = postData.observations_tags.split(',');
                        for (var i = 0; i < standards_selected.length; i++) {

                            html += '<button class="btn btn-lilghtgray" style="margin-right: 2px;">' + standards_selected[i] + '</button>';
                        }

                        html += '</div></div>';
                    }

                    html += '</div>';
                    html += '</li>';
                    html += '</div></ul>';
                    if ($('#vidCommentsNew').length > 0) {
                        $('#vidCommentsNew').prepend(html);
                    }
                    else {
                        $("#observatons-added-notes").prepend(html);
                    }

                }

    <?php if ($videoDetail['doc']['published'] == 0 && $videoDetail['doc']['doc_type'] == 3 && $videoDetail['doc']['is_processed'] == 5): ?>

                    (function workspace_live_duration_poll() {


                        setTimeout(function () {

                            var video_id = $("#video_id").val();
                            var huddle_id = $("#huddle_id").val();
                            $.ajax({
                                type: 'post',
                                url: home_url + '/Huddles/mobile_video/' + video_id,
                                data: {video_id: $("#video_id").val()},
                                dataType: 'json',
                                success: function (data) {
                                    console.log(data);
                                    var date = new Date(null);
                                    date.setSeconds(data.Document.current_duration); // specify value for SECONDS here
                                    var time = date.toISOString().substr(11, 8);
                                    time = time.split(":");
                                    $("#sw_h").html(time[0]);
                                    $("#sw_m").html(time[1]);
                                    $("#sw_s").html(time[2]);
                                    if (data.Document.doc_type == '3' && data.Document.is_processed == 4 && data.Document.published == 0 && data.Document.upload_status != 'uploaded' && data.Document.upload_progress < 100) {
                                        if ($('#div_video_recording_container').length > 0) {
                                            if (data.Document.upload_status == null)
                                                data.Document.upload_status = 'Initializing';
                                            if ($('#div_video_recording_container').length > 0) {
                                                $('#comment_add_form_html').css('display', 'none');
                                                $('#div_video_recording_container_parent').html('<div id="uploading-box-container-' + data.Document.id + '" style="width: 500px;height:321px;background: #000;text-align: center;color: #fff;position: relative;"><div id="uploading-box-' + data.Document.id + '"class="vnb" style="position: absolute;background: #3e7554;z-index: 2;width:' + data.Document.upload_progress + '%;height:146px;text-align:center;color: #fff;padding-top: 65px;opacity:0.5;height: 322px;">&nbsp;</div><span id="uploading-box-span-' + data.Document.id + '"style="width:110px;color:#fff;position:absolute;left:206px;top:40%;text-align:center;text-transform: uppercase;">' + data.Document.upload_progress + '% ' + data.Document.upload_status + '</span></div>');
                                            }
                                        }

                                        $('#uploading-box-' + data.Document.id).css('width', data.Document.upload_progress + '%');
                                        $('#uploading-box-span-' + data.Document.id).html(data.Document.upload_progress + '% ' + data.Document.upload_status);
                                        workspace_live_duration_poll();
                                    }

                                    else if (data.Document.doc_type == '3' && data.Document.is_processed == 4 && data.Document.published == 0 && (data.Document.upload_progress == 100 || data.Document.upload_status == 'uploaded')) {
                                        $('#uploading-box-container-' + data.Document.id).html('<div class="empty_video_box" style="padding-top:125px !important; padding-left: 2px !important;width: 500px; "><img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><p><?=$alert_messages["Your_video_is_currently_processing"]; ?></p></div>');
                                        workspace_live_duration_poll();
                                    }
                                    else if (data.Document.published === '1' && data.Document.is_processed === '4') {
                                        window.location = home_url + '/MyFiles/view/1/' + huddle_id;
                                    } else {
                                        workspace_live_duration_poll();
                                    }
                                    if (data.Document.published === '1') {
                                        $('#gVideoUpload').show();
                                    } else {
                                        $('#gVideoUpload').hide();
                                    }
                                },
                                error: function (jqXHR) {
                                    if (jqXHR.status == 0) {
                                        workspace_live_duration_poll();
                                    }
                                }
                            });
                        }, 1000);
                    })();
    <?php endif; ?>

    <?php if ($videoDetail['doc']['published'] == 0 && $videoDetail['doc']['doc_type'] == 3 && $videoDetail['doc']['is_processed'] == 4): ?>


                    (function workspace_live_duration_poll() {
                        //                    $('#gVideoUpload').hide();

                        setTimeout(function () {

                            var video_id = $("#video_id").val();
                            var huddle_id = $("#huddle_id").val();
                            $.ajax({
                                type: 'post',
                                url: home_url + '/Huddles/mobile_video/' + video_id,
                                data: {video_id: $("#video_id").val()},
                                dataType: 'json',
                                success: function (data) {

                                    var date = new Date(null);
                                    date.setSeconds(data.Document.current_duration); // specify value for SECONDS here
                                    var time = date.toISOString().substr(11, 8);
                                    time = time.split(":");
                                    $("#sw_h").html(time[0]);
                                    $("#sw_m").html(time[1]);
                                    $("#sw_s").html(time[2]);
                                    if (data.Document.doc_type == '3' && data.Document.is_processed == 4 && data.Document.published == 0 && data.Document.upload_status != 'uploaded' && data.Document.upload_progress < 100) {


                                        if (data.Document.upload_status == null)
                                            data.Document.upload_status = 'Initializing';
                                        if ($('#div_video_recording_container').length > 0) {

                                            $('#comment_add_form_html').css('display', 'none');
                                            $('#div_video_recording_container_parent').html('<div id="uploading-box-container-' + data.Document.id + '" style="width: 500px;height:321px;background: #000;text-align: center;color: #fff;position: relative;"><div id="uploading-box-' + data.Document.id + '"class="vnb" style="position: absolute;background: #3e7554;z-index: 2;width:' + data.Document.upload_progress + '%;height:146px;text-align:center;color: #fff;padding-top: 65px;opacity:0.5;height: 322px;">&nbsp;</div><span id="uploading-box-span-' + data.Document.id + '"style="width:110px;color:#fff;position:absolute;left:206px;top:40%;text-align:center;text-transform: uppercase;">' + data.Document.upload_progress + '% ' + data.Document.upload_status + '</span></div>');
                                        }
                                        $('#uploading-box-' + data.Document.id).css('width', data.Document.upload_progress + '%');
                                        $('#uploading-box-span-' + data.Document.id).html(data.Document.upload_progress + '% ' + data.Document.upload_status);
                                        workspace_live_duration_poll();
                                    }
                                    else if (data.Document.doc_type == '3' && data.Document.is_processed == 4 && data.Document.published == 0 && data.Document.upload_status == 'uploaded') {
                                        $('#uploading-box-container-' + data.Document.id).html('<div class="empty_video_box" style="padding-top:125px !important; padding-left: 2px !important;width: 500px; "><img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><p><?=$alert_messages["Your_video_is_currently_processing"]; ?></p></div>');
                                        workspace_live_duration_poll();
                                    }
                                    else if (data.Document.published === '1') {

                                        window.location = home_url + '/MyFiles/view/1/' + huddle_id;
                                    } else {
                                        workspace_live_duration_poll();
                                    }
                                    if (data.Document.published === '1') {
                                        $('#gVideoUpload').show();
                                    } else {
                                        $('#gVideoUpload').hide();
                                    }
                                },
                                error: function (jqXHR) {
                                    if (jqXHR.status == 0) {
                                        workspace_live_duration_poll();
                                    }
                                }
                            });
                        }, 1000);
                    })();
    <?php endif; ?>

                $(document).on('click', '#add-notes_myfiles', function (e) {

    <?php if ($videoDetail['doc']['published'] == 0 && $videoDetail['doc']['doc_type'] == 3 && $videoDetail['doc']['is_processed'] == 5): ?>
                        $hours = $('#sw_h').text();
                        $min = $('#sw_m').text();
                        $sec = $('#sw_s').text();
                        $time = $hours + ':' + $min + ':' + $sec;

                        $('.timer_box2').text($time);
                        $('.timer_box2').show();
                        var seconds = new Date('1970-01-01T' + $time + 'Z').getTime() / 1000;
                        $('#synchro_time').val('');
                        $('#synchro_time').val(seconds);


    <?php endif; ?>





                    var $form = $('#comments-form1');


                    var totalSec = '0';
                    if ($('#for_synchro_time').is(':checked') && $('#synchro_time').val() !== '{:value=>""}') {
                        totalSec = $('#synchro_time').val();


                    }
                    //  totalSec = '100';

                    $("#attached_image_name").html('');


                    var attached_image_name = $('#comment_attachment').val().replace(/C:\\fakepath\\/i, '');

                    if (attached_image_name != '') {
                        var attachment_no = $("#attachment").text();
                        var attach_new = attachment_no.substring(13, 14);
                        attachment_no = parseInt(attach_new) + 1;
                        $("#attachment").text('Attachments (' + attachment_no + ')');

                        var html = '';
                        html += '<li class="attached_document_class" id="li_video_doc_29430" style="width: 100%;">';
                        html += '<span data-time="" class="synchro-time" style="cursor: pointer;margin-right: 25px;">Uploading...</span>';
                        html += '<a id="view_resource_29430" target="_blank" href="/app/view_document/3IOhA5QUSAKprRbhpLuI" class="wrap"><span class="' + getIcons(attached_image_name) + '">Icon</span>' + attached_image_name + '</a>';
                        html += '<form id="delete-video-document29430" data-async="" accept-charset="UTF-8" action="/Huddles/deleteVideoDocument/29430/29429/22143">';
                        // html +=   '<a rel="nofollow" data-confirm="Are you sure you want to delete this Resource?" id="delete-main-comments" class="del" href="javascript:$("#delete-video-document29430").submit()">&nbsp;</a>';
                        html += '</form>';
                        //  html +=      '<a download="" class="file_download_cls" href="http://www.filestackapi.com/api/file/3IOhA5QUSAKprRbhpLuI">';
                        //  html +=      '<img alt="Download" class="right smargin-right" style="height: 24px;" rel="tooltip" src="/img/new/download-2.png" title="download video">';
                        //  html +=   '</a>';
                        html += '</li>';
                        if ($('ul.video-docs li').length == 1) {
                            if ($('ul.video-docs li').text() == 'No Attachments have been added.' || $('ul.video-docs li').text() == 'No Attachments have been added.') {
                                $('ul.video-docs').html(html);
                            }
                            else {
                                $('ul.video-docs').last().append(html);
                            }
                        }
                        else {
                            $('ul.video-docs').last().append(html);
                        }
                    }

                    var myForm = document.getElementById('comments-form1');
                    formData = new FormData(myForm);

                    var huddle_id = $('#huddle_id').val();
                    var video_id = $('#videoId').val();
                    var account_id = '<?php echo $account_id; ?>';
                    if (typeof ($('#comment_attachment').val()) != 'undefined' && $('#comment_attachment').val() != '') {
                        $.ajax({
                            type: $form.attr('method'),
                            url: home_url + '/Huddles/upload_comment_file/' + huddle_id + '/' + video_id + '/' + account_id,
                            processData: false,
                            contentType: false,
                            data: formData,
                            dataType: 'json',
                            success: function (data) {
                                if (totalSec == '{:value=>""}') {
                                    totalSec = '0';
                                }

                                $.ajax({
                                    url: home_url + "/huddles/generate_url/" + data.document_id,
                                    type: 'POST',
                                    // dataType: 'json',
                                    success: function (response) {

                                        filepicker.setKey('A3w6JbXR2RJmKr3kfnbZtz');

                                        filepicker.storeUrl(
                                                response,
                                                {filename: data.file_name},
                                        function (blob) {
                                            // var result = blob.url.split('/');
                                            $.ajax({
                                                type: 'POST',
                                                url: home_url + '/Huddles/update_stack_url/' + data.document_id + '/' + video_id,
                                                data: {stack_url: blob.url, timestamp_duration: totalSec},
                                                success: function (response) {

                                                    $('#txtUploadedUrl').val(blob.url);
                                                    $('#doc-title').html(blob.filename);
                                                    $('#doc-type').html(blob.mimetype);
                                                    $('#add-document-row').html($('#extra-row-li').html());
                                                    var attachment_no = parseInt(response);
                                                    $("#attachment").text('Attachments (' + attachment_no + ')');
                                                    CloseDocumentUpload();
                                                    getVideoComments_workspace();


                                                }
                                            });


                                        }
                                        );


                                    },
                                    error: function () {
                                        alert("<?php echo $alert_messages['network_error_occured']; ?>");
                                    }
                                });


                            }
                        });

                    }
                    // alert($('#add_audio_comments').val());
                    var ob_time = '';
    <?php if ($videoDetail['doc']['published'] == 0 && $videoDetail['doc']['doc_type'] == 3 && $videoDetail['doc']['is_processed'] == 5): ?>
                        if ($('.timer_box2').text() == '00:00:00') {
                            ob_time = $('#sw_h').text() + ':' + $('#sw_m').text() + ':' + $('#sw_s').text();
                        } else {
                            ob_time = $('.timer_box2').text();
                        }
    <?php endif; ?>

                    var qtags = '';
                    if ($('#txtVideoTags').length > 0)
                        qtags = $('#txtVideoTags').val();
                    $('#comment_comment').css('border', '1px solid #b2b2b2');
                    if ($('#comment_comment').val() == '' && $('#txtVideoTags').val() == '') {
                        e.preventDefault();
                        $('#comment_comment').css('border', '1px solid red');
                        $('#comment_comment').css('border-radius', '0');
                        return true;
                    }

                    $('#comment_comment').css('border', 'none');
                    if (ob_time) {
                        var observation_time = ob_time;
                    } else if ($("#synchro_time").val() == '{:value=>""}') {
                        var observation_time = 'All';
                    } else if ($('#synchro_time').val() != '') {
                        var date = new Date(null);
                        date.setSeconds($('#synchro_time').val()); // specify value for SECONDS here
                        //date.toISOString().substr(11, 8);
                        var observation_time = date.toISOString().substr(11, 8);
                    } else {
                        var observation_time = 'All';
                    }
                    var postData = {
                        observation_title: $('#ObservationTitle').text(),
                        observations_comments: $('#comment_comment').val(),
                        observations_standards: '',
                        observations_tags: qtags,
                        observations_time: observation_time,
                        account_id: $('#account_id').val(),
                        user_id: $('#user_id').val(),
                        huddle_id: $('#huddle_id').val(),
                        video_id: $('#videoId').val(),
                        internal_comment_id: observation_notes_array.length + 1,
                        comments_optimized: 1,
                        assessment_value: $('#synchro_time_class_tags').val()
                    };
    <?php //if (IS_QA):    ?>
                    var metadata = {
                        comment_added: $('#comment_comment').val(),
                        workspace_comment: 'True',
                        user_role: '<?php echo $user_role; ?>'
                    };
                    Intercom('trackEvent', 'comment-added', metadata);

    <?php //endif;    ?>


                    $('.timer_box2').text('00:00:00');
                    $('.timer_box2').hide();
                    observation_notes_array.push(postData);
                    generate_comment_html(postData);
                    $('#comment_comment').css('height', '100px');
                    // $('#add_audio_comments').val('');


                });
            });

            function defaulttagsclasses(posid) {
                var class_name = 'tags_qucls';
                if (posid == 1) {
                    class_name = 'tags_quclsbg';
                }
                else if (posid == 2) {
                    class_name = 'tags_sugclsbg';
                }
                else if (posid == 3) {
                    class_name = 'tags_notesclsbg';
                }
                else if (posid == 4) {
                    class_name = 'tags_strangthclsbg';
                }
                return class_name;
            }

            function defaulttagsclasses1(posid) {
                var class_name = 'uncheck';
                if (posid == 1) {
                    class_name = 'tags_qucls';
                }
                else if (posid == 2) {
                    class_name = 'tags_sugcls';
                }
                else if (posid == 3) {
                    class_name = 'tags_notescls';
                }
                else if (posid == 4) {
                    class_name = 'tags_strangthcls';
                }
                return class_name;
            }

            $('#pause_while_type').click(function () {
                var value = $("#pause_while_type").is(':checked') ? 1 : 0;
                var user_id = "<?php echo $user_id; ?>";
                var account_id = "<?php echo $account_id; ?>";
                $.ajax({
                    type: 'POST',
                    url: home_url + '/Huddles/type_pause/',
                    data: {value: value, user_id: user_id, account_id: account_id},
                    success: function (res) {
                    },
                    errors: function (response) {

                    }
                });
            });
            $('#press_enter_to_send').click(function () {
                var value = $("#press_enter_to_send").is(':checked') ? 1 : 0;
                var user_id = "<?php echo $user_id; ?>";
                var account_id = "<?php echo $account_id; ?>";
                $.ajax({
                    type: 'POST',
                    url: home_url + '/Huddles/press_enter_to_send/',
                    data: {
                        value: value,
                        user_id: user_id,
                        account_id: account_id
                    },
                    success: function (res) {
                    },
                    errors: function (response) {

                    }
                });
            });

            function textAreaAdjust(o) {
                o.style.height = "1px";
                o.style.minHeight = "75px";
                o.style.height = (25 + o.scrollHeight) + "px";
                o.style.maxHeight = (5 + o.scrollHeight) + "px";
                //                            alert(o.parentElement.style.minHeight);
                //                            o.parentElement.style.height = (5 + parseInt(o.parentElement.style.minHeight)) + "px";
            }

            function nl2br(str, is_xhtml) {
                var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
                return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
            }

            function hide_sidebar() {
                $(".appendix-content").delay(500).fadeOut(500);
                // $("#collab_help").delay(500).fadeOut(500);


            }

            function skip(value) {

                var video = videojs('<?php echo $video_id_rev_for; ?>');
                var current_time = video.currentTime();
                video.currentTime(current_time + value);
            }

            $('#auto_scroll_switch').click(function () {
                var value = $(this).is(':checked') ? 1 : 0;
                var user_id = $('#user_id').val();
                var account_id = $('#account_id').val();
                $.ajax({
                    type: 'POST',
                    url: home_url + '/app/autoscroll_switch',
                    data: {
                        value: value,
                        user_id: user_id,
                        account_id: account_id
                    },
                    success: function (res) {
                    },
                    errors: function (response) {

                    }
                });
            });
        </script>

    <?php else: ?>
        <div class="no-video"><strong>No video(s) have been uploaded to your Workspace.</strong></div>
    <?php endif; ?>
</div>
<script type="text/javascript">
    function getIcons($file) {
        var $ext = $file.split('.');

        if ($ext[1] == 'doc' || $ext[1] == 'docx') {
            return 'wordpress';
        } else if ($ext[1] == 'xlsx' || $ext[1] == 'xls') {
            return 'x-icon';
        } else if ($ext[1] == 'pdf') {
            return 'acro';
        } else if ($ext[1] == 'pptx' || $ext[1] == 'ppt') {
            return 'ppt';
        } else if ($ext[1] == 'jpeg' || $ext[1] == 'jpg' || $ext[1] == 'png' || $ext[1] == 'gif') {
            return 'image';
        } else {
            return 'file';
        }

    }


</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", '#copy-video', function () {
            if ($(this).attr('data-total-comments') == 0)
                $('.copy_video_box').css('display', 'none');
            else
                $('.copy_video_box').css('display', 'block');
        });
    });
    $(document).on('click', '.huddle-contents-single', function (e) {
        $.ajax({
            type: 'POST',
            data: {
                type: 'get_video_comments',
                sort: $('#myFilesVideoSort').val(),
                limit: "",
                page: ""
            },
            dataType: 'json',
            url: home_url + '/MyFiles/getCopyHuddlesList',
            success: function (response) {
                var list = $('.list-container-box-single');
                if (response.html != '') {
                    list.html(response.html);
                    gApp.updateCheckboxEvent($('.list-container-box-single .ui-checkbox input'));
                } else {
                    list.html("No Huddles found in your account.");

                }
            },
            errors: function (response) {
                console.log(response);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
                //location.reload(true);
            }
        });
    });
</script>