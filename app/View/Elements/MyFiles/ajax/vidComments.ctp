<?php
$user_current_account = $this->Session->read('user_current_account');
$srtType = $this->Session->read('srtType_myfiles');
$user_permissions = $this->Session->read('user_permissions');
$user = $this->Session->read('user_current_account');
$account_id = $user['accounts']['account_id'];

$chimgIndicator = $this->Custom->getSecureAmazonSibmeUrl('app/img/new/indicator.gif');
$chimgDefault = $this->Custom->getSecureAmazonSibmeUrl('app/img/home/photo-default.png');
$chimgCross = $this->Custom->getSecureAmazonSibmeUrl('app/img/crossb.png');

function formatSeconds($seconds) {
    $hours = 0;
    $milliseconds = str_replace("0.", '', $seconds - floor($seconds));

    if ($seconds > 3600) {
        $hours = floor($seconds / 3600);
    }
    $seconds = $seconds % 3600;


    return str_pad($hours, 2, '0', STR_PAD_LEFT)
            . gmdate(':i:s', $seconds)
            . ($milliseconds ? ".$milliseconds" : '');
}

function defaulttagsclasses($posid) {
    $class_name = 'tags_qucls';
    if ($posid == 1) {
        $class_name = 'tags_qucls';
    } else if ($posid == 2) {
        $class_name = 'tags_sugcls';
    } else if ($posid == 3) {
        $class_name = 'tags_notescls';
    } else if ($posid == 4) {
        $class_name = 'tags_strangthcls';
    }
    return $class_name;
}
?>
<div>
    <?php if ($videoComments && $type != 'get_video_comments_obs') { ?>

        <?php
    }

    $comment_box_display = '';
    $comment_box_add_btn = '';
    if ($videoComments) {
        $comment_box_display = 'style="display:none;"';
        $comment_box_add_btn = 'display:block;';
    } else {
        $comment_box_display = 'style="display:block;"';
        $comment_box_add_btn = 'display:none;';
    }
    ?>

    <script type="text/javascript">

<?php
$timeS = array();
if ($videoComments) {
    foreach ($videoComments as $cmt) {
        if (!empty($cmt['Comment']['time']))
            $timeS[] = $cmt['Comment']['time'];
    }
}
?>

        $('#example_video_<?php echo $video_id ?>_html5_api').attr('data-markers', '[<?php echo implode(',', $timeS); ?>]');

    </script>
    <div id="comment_form"  style="display:none;">
        <form accept-charset="UTF-8" action="<?php echo $this->base; ?>/Huddles/addComments" class="new_comment" id="comments-form1" method="post">
            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
            <input id="tokentag" type="hidden" name="authenticity_token" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" />
            <div class="input-group">
                <textarea cols="50" id="comment_comment" required name="comment[comment]" placeholder="Add a comment..." rows="4"></textarea>
            </div>
            <input id="comment_access_level" name="comment[access_level]" type="hidden" value="" />
            <input id="synchro_time" name="synchro_time" type="hidden" value="{:value=&gt;&quot;&quot;}" />
            <input type="hidden" id="videoId" name="videoId" value="<?php echo $video_id ?>">
            <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id ?>">
            <input type="hidden" id="workspace_video_id" name="workspace_video_id" value="<?php echo $videos['AccountFolder']['account_folder_id'] ?>">
            <input type="hidden" id="notifiable" name="notifiable" value="<?php echo isset($notifiable) ? $notifiable : 1; ?>" />

            <div id="comment-for" class="input-group">
                For:
                <label for="for_synchro_time">
                    <input id="for_synchro_time"  name="for" type="radio" value="synchro_time" /> Time-specific (<span id="right-now-time">0:00</span>)
                </label>

                <label for="for_entire_video">
                    <input id="for_entire_video"  name="for" type="radio" value="entire_video" checked/> Whole video
                </label>
            </div>

            <div class="input-group" style="clear: both;">
                <input type="submit" name="submit" value="Add Note" class="btn btn-green">
                <input type="hidden" name="type" value="add_video_comments"/>
                <a id="close_comment_form" class="btn btn-transparent js-close-comments-form">Cancel</a>
                <div style="clear: both;"></div>
            </div>
        </form>
        <div id="indicator" style="display:none;"><img alt="Indicator" src="<?php echo $chimgIndicator; ?>" /></div>
    </div>
    <a id="reload_button" style="display: none;"></a>
    <form accept-charset="UTF-8" action="<?php echo $this->base; ?>/Huddles/load_timecomment" data-remote="true" enctype="multipart/form-data" id="time_comment_form" method="post" name="time_comment_form">
        <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>

        <input id="synchro_time" name="synchro_time" type="hidden" value="{:value=&gt;&quot;&quot;}" />
        <input id="commentable_id" name="commentable_id" type="hidden" value="<?php echo $user_id ?>" />
        <input id="commentable_id" name="user_id" type="hidden" value="<?php echo $user_id ?>" />
        <input id="commentable_type" name="commentable_type" type="hidden" value="Video" />
        <input id="time_submit_button" name="commit" style="display: none" type="submit" value="Show Time Comment" />

    </form>
    <div id="indicator" style="display:none;"><img alt="Indicator" src="<?php echo $chimgIndicator; ?>" /></div>

    <div class="clear"></div>
    <div id="check">
        <div id="vidCommentsNew" style="height: 634px;overflow-y: auto;">


            <?php if (isset($videoComments) && $videoComments != ''): ?>
                <?php
                $timeS = '';
                $video_comment_counter = 0;
                if (count($videoComments) < 1 && $searchCmt != '')
                    echo '<li style="text-align:center;">None of your comments or tags matched this search.<br>Try another search.</li>';
                else
                    foreach ($videoComments as $vidComments):
                        ?>
                        <?php
                        $commentDate = $vidComments['Comment']['created_date'];
                        $commentsDate = $this->Custom->formateDate($commentDate);
                        ?>
                        <ul class="comments comments_huddles" id="normal_comment_<?php echo $vidComments['Comment']['time']; ?>" style="width:100%;">
                            <div class="comment_box_<?php echo $vidComments['Comment']['time']; ?>" rel="<?php echo $vidComments['Comment']['id'] ?>" id="comment_box_<?php echo $vidComments['Comment']['id'] ?>" style="<?php echo ($video_comment_counter == 0) ? " margin-top: 12px;" : "" ?>">
                                <li class="synchro">
                                    <div class="synchro-inner">
                                        <i></i><span  data-time="<?php echo $vidComments['Comment']['time']; ?>" class="synchro-time"><?php echo ($vidComments['Comment']['time'] == 0 ? "All" : formatSeconds($vidComments['Comment']['time']) ); ?></span>
                                    </div>
                                </li>

                                <li class="comment thread">
                                    <a href="#<?php //echo $this->base . '/users/editUser/' . $vidComments['Comment']['user_id'];                                                                                                                            ?>">
                                        <?php if (isset($vidComments['User']['image']) && $vidComments['User']['image'] != ''): ?>
                                            <?php
                                            $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $vidComments['User']['user_id'] . "/" . $vidComments['User']['image']);
                                            $chimg = $profile_image;
                                            echo $this->Html->image($chimg, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '21', 'width' => '21', 'align' => 'left'));
                                            ?>
                                        <?php else: ?>
                                            <img width="21" height="21"  src="<?php echo $chimgDefault; ?>" class="photo photo inline" rel="image-uploader" alt="Photo-default">
                                        <?php endif; ?>
                                    </a>
                                    <div class="comment-header"><?php echo $vidComments['User']['first_name'] . " " . $vidComments['User']['last_name']; ?></div>
                                    <?php
                                    if ($vidComments['Comment']['ref_type'] == '6') {
                                        if (Configure::read('use_cloudfront') == true) {
                                            $url = $vidComments['Comment']['comment'];
                                            $videoFilePath = pathinfo($url);
                                            $videoFileName = $videoFilePath['filename'];
                                            $ext = '.' . $videoFilePath['extension'];
                                            if ($videoFilePath['dirname'] == ".") {

                                                $video_path = $this->Custom->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . $ext);
                                                $relative_video_path = $videoFileName . $ext;
                                            } else {

                                                $video_path = $this->Custom->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . $ext);
                                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . $ext;
                                            }
                                        } else {
                                            $url = $vidComments['Comment']['comment'];
                                            $videoFilePath = pathinfo($url);
                                            $videoFileName = $videoFilePath['filename'];
                                            $ext = '.' . $videoFilePath['extension'];
                                            if ($videoFilePath['dirname'] == ".") {

                                                $video_path = $this->Custom->getSecureAmazonUrl($videoFileName . $ext);
                                                $relative_video_path = $videoFileName . $ext;
                                            } else {

                                                $video_path = $this->Custom->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName . $ext);
                                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . $ext;
                                            }
                                        }
                                        ?>
                                        <div class="comment-body comment">
                                            <audio controls>
                                                <source src="<?php echo $video_path; ?>">

                                                Your browser does not support the audio element.
                                            </audio>

                                        <?php } else {
                                            ?>
                                            <div class="comment-body comment more">  <?php
                                                echo nl2br($vidComments['Comment']['comment']);
                                            }
                                            ?>
                                        </div>

                                        <div class="comment-footer">
                                            <span class="comment-date">
                                                <?php echo $commentsDate; ?>
                                            </span>
                                            <div style="width:136px;" class="comment-actions">
                                                <form id="delete-main-comments-frm<?php echo $vidComments['Comment']['id']; ?>" action="<?php echo $this->base . '/Huddles/deleteVideoComments/' . $vidComments['Comment']['id']; ?>" accept-charset="UTF-8">
                                                    <?php if (!empty($video_id)): ?>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <!--<a rel="nofollow"  id="edit-main-comments_<?php echo $vidComments['Comment']['id'] ?>" class="comment-edit"  style="cursor: pointer;">Edit</a>-->
                                                        <a   data-toggle="modal" data-target="#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>"  class="comment-edit"  style="cursor: pointer;">Edit</a>
                                                        <a rel="nofollow" data-confirm="Are you sure you want to delete this comment?" id="delete-main-comments" class="comment-delete" href="javascript:deleteComment('#delete-main-comments-frm<?php echo $vidComments['Comment']['id']; ?>','<?php echo $vidComments['Comment']['id']; ?>');">Delete</a>
                                                        <?php if ($this->Custom->match_timestamp_count_comment($vidComments['Comment']['id'], $video_id) != 0): ?> <span class="timestamp_attachment_no" style="color: #5a80a0;cursor:pointer;"><img style="width:12px;" src = "<?php echo $this->webroot . 'img/comment_box_attachment.jpg'; ?>"> <?php echo $this->Custom->match_timestamp_count_comment($vidComments['Comment']['id'], $video_id); ?></span>  <?php endif; ?>
                                                    <?php endif; ?>
                                                </form>


                                            </div>
                                        </div>
                                        <?php if ($this->Custom->is_enable_tags($account_id)): ?>
                                            <div id="docs-standards" style="margin-bottom: 20px;">
                                                <?php
                                                if (isset($vidComments['default_tags']) && count($vidComments['default_tags']) > 0) {
                                                    foreach ($vidComments['default_tags'] as $default_tag) {
                                                        ?>
                                                        <div id="tagFilterList">
                                                            <div id="tagFilterContainer" class="btnwraper">
                                                                <button class="btn btn-lilghtgray"># <?php echo empty($default_tag['account_tags']['tag_title']) ? $default_tag['AccountCommentTag']['tag_title'] : $default_tag['account_tags']['tag_title']; ?></button>

                                                                <?php if ($huddle_permission == '200' || ($this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                                    <a rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag('<?php echo $default_tag['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $chimgCross; ?>" class="crossBtn"></a>
                                                                <?php elseif ($huddle_permission == '210' && ($user_current_account['User']['id'] == $vidComments['Comment']['created_by'])): ?>
                                                                    <a rel="nofollow" data-confirm="Are you sure you want to delete this tag?" id="delete-comment-tag" class="tag-delete" href="javascript:deleteCommentTag('<?php echo $default_tag['AccountCommentTag']['account_comment_tag_id']; ?>');"><img alt="" src="<?php echo $chimgCross; ?>" class="crossBtn"></a>
                                                                <?php endif; ?>

                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </div>
                                            <?php
                                        endif;
                                        echo '<div style="clear:both;"></div>';
                                        ?>

                                        <div id="edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>" class="modal" role="dialog" style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="header">
                                                        <h4 class="header-title nomargin-vertical smargin-bottom">Edit Note</h4>
                                                        <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                                                    </div>
                                                    <script type="text/javascript">
                                                        $(document).ready(function () {
                                                            $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide();

                                                            $("#edit-main-comments_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                                                $comments_text = $('#comments_text_<?php echo $vidComments['Comment']['id'] ?>').val();
                                                                $('#comment_comments_<?php echo $vidComments['Comment']['id'] ?>').val($comments_text);

                                                                $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").show("slow", function () {
                                                                    $('#comment_comment_<?php echo $vidComments['Comment']['id'] ?>').focus();
                                                                });
                                                            });
                                                            $("#close_edit_form_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                                                $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide("slow");
                                                            });
                                                            $('#comment_form_<?php echo $vidComments['Comment']['id'] ?>').on('submit', function (event) {
                                                                var $form = $(this);
                                                                var $target = $($form.attr('data-target'));

                                                                $.ajax({
                                                                    type: $form.attr('method'),
                                                                    url: $form.attr('action'),
                                                                    data: $form.serialize(),
                                                                    success: function (data, status) {
                                                                        getVideoComments_workspace();
                                                                    }
                                                                });
                                                                event.preventDefault();
                                                            });
                                                        });
                                                    </script>

                                                                            <!--<form method="post" id="comment_form_<?php echo $vidComments['Comment']['id'] ?>"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          class="new_comment" accept-charset="UTF-8"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          action="<?php echo $this->base . '/Huddles/editComments/' . $vidComments['Comment']['id']; ?>">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div style="margin:0;padding:0;display:inline">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <input type="hidden" value="âœ“" name="utf8">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <input type="hidden" id="comments_text_<?php echo $vidComments['Comment']['id'] ?>" value="<?php echo $vidComments['Comment']['comment'] ?>"/>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <input type="hidden" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" name="authenticity_token" id="tokentag">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <input type="hidden" value="<?php echo $vidComments['Comment']['id'] ?>" name="comment_id" />
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div class="input-group">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <textarea rows="3" placeholder="Edit comments" name="comment[comment]" id="comment_comments_<?php echo $vidComments['Comment']['id'] ?>" cols="50"></textarea>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <input type="hidden" value="nested" name="comment[access_level]" id="comment_access_level">

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div class="input-group">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <input type="submit" class="btn btn-green" value="Edit" name="submit">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <a class="btn btn-transparent" id="close_edit_form_<?php echo $vidComments['Comment']['id'] ?>">Cancel</a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </form>-->

                                                    <div id="comment_add_form_html">
                                                        <?php $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $huddle[0]['AccountFolder']['created_by']); ?>



                                                        <?php
                                                        $comment_box_display = 'style="display:block;"';
                                                        $comment_box_add_btn = 'style="display:none;"';
                                                        ?>
                                                        <?php
                                                        $selected_tag_ids = array();

                                                        if (isset($vidComments['default_tags']) && count($vidComments['default_tags']) > 0) {

                                                            foreach ($vidComments['default_tags'] as $default_tag) {
                                                                $selected_tag_ids[] = $default_tag['account_tags']['account_tag_id'];
                                                                ?>
                                                                <script type="text/javascript">
                                                                    $(document).ready(function () {
                                                                        $("#txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>").addTag("# <?php echo trim(empty($default_tag['account_tags']['tag_title']) ? $default_tag['AccountCommentTag']['tag_title'] : $default_tag['account_tags']['tag_title']); ?>");
                                                                    });
                                                                </script>
                                                                <?php
                                                            }
                                                        }
                                                        ?>

                                                        <style type="text/css">
                                                            ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
                                                                color:    #424242;
                                                            }
                                                            :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
                                                                color:    #424242;
                                                                opacity:  1;
                                                            }
                                                            ::-moz-placeholder { /* Mozilla Firefox 19+ */
                                                                color:    #424242;
                                                                opacity:  1;
                                                            }
                                                            :-ms-input-placeholder { /* Internet Explorer 10-11 */
                                                                color:    #424242;
                                                            }
                                                            div.tagsinput{
                                                                width:422px !important;
                                                                position: relative;
                                                                top: -5px;
                                                                border-radius: 0px;
                                                            }
                                                            #txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>_tag {
                                                                width: 117px !important;
                                                                background: url(/app/img/tag_standard_bg.png) no-repeat;
                                                                padding-left: 20px;
                                                                color: #b2b2b2 !important;
                                                            }
                                                            .edit_comment_left{
                                                                float:left;
                                                                width: 52%;
                                                                box-sizing:border-box;
                                                            }
                                                            .edit_comment_right{
                                                                float:right;
                                                                width:48%;
                                                                padding:0px 20px;
                                                                box-sizing:border-box;

                                                            }
                                                            .scroll_box{
                                                                overflow-y:scroll;
                                                                max-height:450px;
                                                                overflow-x:hidden;
                                                                margin-bottom:12px;
                                                            }
                                                            .block_width_adjs{
                                                                padding-left:25px;
                                                            }
                                                            .edit_comment_form{
                                                                padding:0px 22px !important;
                                                            }
                                                            .edit_comment_right h1{
                                                                font-size:25px;
                                                                border-bottom: dotted 1px #ccc;
                                                                font-weight:normal;
                                                                margin-bottom:10px;
                                                            }
                                                            .edit_btn_adjs{
                                                                top: 1px;
                                                                position: relative;
                                                                padding-bottom: 6px !important;
                                                            }
                                                        </style>

                                                        <script>
                                                            //                                                    $(document).on('change', '.time_secondsCls_1', function () {
                                                            //                                                        var seconds_val = $(this).val();
                                                            //                                                        var max_val_sec_db = $(this).data('max_val_db');
                                                            //
                                                            //                                                        var minutes_val = $(this).siblings(".time_minutesCls_1").val();
                                                            //                                                        var minutes_val_set = $(this).siblings(".time_minutesCls_1").attr('max');
                                                            //                                                        var max_val_minute_db = $(this).siblings(".time_minutesCls_1").data('max_val_db');
                                                            //                                                        if (parseInt(seconds_val) > parseInt(max_val_sec_db) && parseInt(minutes_val_set) > 0 && parseInt(minutes_val_set) == parseInt(max_val_minute_db)) {
                                                            //                                                            var new_max_val_minutes = parseInt(minutes_val_set) - 1;
                                                            //                                                            $(this).siblings(".time_minutesCls_1").attr('max', new_max_val_minutes);
                                                            //                                                            if (parseInt(minutes_val) > 0) {
                                                            //                                                                $(this).siblings(".time_minutesCls_1").val(parseInt(minutes_val) - 1);
                                                            //                                                            }
                                                            //                                                        } else if (parseInt(seconds_val) <= parseInt(max_val_sec_db) && parseInt(minutes_val_set) >= 0 && parseInt(minutes_val_set) < parseInt(max_val_minute_db)) {
                                                            //                                                            var new_max_val_minutes = parseInt(minutes_val_set) + 1;
                                                            //                                                            $(this).siblings(".time_minutesCls_1").attr('max', new_max_val_minutes);
                                                            //                                                            if (parseInt(minutes_val) > 0) {
                                                            //                                                                $(this).siblings(".time_minutesCls_1").val(parseInt(minutes_val) + 1);
                                                            //                                                            }
                                                            //                                                        }
                                                            //                                                    });
                                                            //                                                    $(document).on('change', '.time_minutesCls_1', function () {
                                                            //                                                        var minutes_val = $(this).val();
                                                            //                                                        var max_val_minutes_db = $(this).data('max_val_db');
                                                            //                                                        var minutes_val_set = $(this).attr('max');
                                                            //
                                                            //                                                        var hours_val = $(this).siblings(".time_hoursCls_1").val();
                                                            //                                                        var hours_val_set = $(this).siblings(".time_hoursCls_1").attr('max');
                                                            //                                                        var max_val_hour_db = $(this).siblings(".time_hoursCls_1").data('max_val_db');
                                                            //
                                                            //                                                        if (parseInt(minutes_val) > parseInt(max_val_minutes_db) && parseInt(hours_val_set) > 0 && parseInt(hours_val_set) == parseInt(max_val_hour_db)) {
                                                            //                                                            var new_max_val_minutes = parseInt(hours_val_set) - 1;
                                                            //                                                            $(this).siblings(".time_hoursCls_1").attr('max', new_max_val_minutes);
                                                            //                                                            if (parseInt(hours_val) > 0) {
                                                            //                                                                $(this).siblings(".time_hoursCls_1").val(parseInt(hours_val) - 1);
                                                            //                                                            }
                                                            //                                                        } else if (parseInt(minutes_val) <= parseInt(max_val_minutes_db) && parseInt(hours_val_set) >= 0 && parseInt(hours_val_set) < parseInt(max_val_hour_db)) {
                                                            //                                                            var new_max_val_minutes = parseInt(hours_val_set) + 1;
                                                            //                                                            $(this).siblings(".time_hoursCls_1").attr('max', new_max_val_minutes);
                                                            //                                                            if (parseInt(hours_val) != 0) {
                                                            //                                                                $(this).siblings(".time_hoursCls_1").val(parseInt(hours_val) + 1);
                                                            //                                                            }
                                                            //                                                        }
                                                            //                                                        //Check seconds current val if its already greater than db val
                                                            //                                                        var seconds_val = $(this).siblings(".time_secondsCls_1").val();
                                                            //                                                        var max_sec_val_db = $(this).siblings(".time_secondsCls_1").data('max_val_db');
                                                            //                                                        if (parseInt(seconds_val) > parseInt(max_sec_val_db) && parseInt(minutes_val) == parseInt(max_val_minutes_db) && parseInt(minutes_val) > 0) {
                                                            //                                                            if (parseInt(minutes_val_set) > 0) {
                                                            //                                                                var new_max_val_minutes = parseInt(minutes_val_set) - 1;
                                                            //                                                                $(this).attr('max', new_max_val_minutes);
                                                            //                                                            }
                                                            //                                                            $(this).val(parseInt(minutes_val) - 1);
                                                            //                                                        }
                                                            //                                                    });
                                                            //                                                    $(document).on('change', '.time_hoursCls_1', function () {
                                                            //                                                        var hours_val = $(this).val();
                                                            //                                                        var max_val_minutes_db = $(this).data('max_val_db');
                                                            //                                                        var hours_val_set = $(this).attr('max');
                                                            //                                                        //Check minutes current val if its already greater than db val
                                                            //                                                        var minutes_val = $(this).siblings(".time_minutesCls_1").val();
                                                            //                                                        var max_minute_val_db = $(this).siblings(".time_minutesCls_1").data('max_val_db');
                                                            //                                                        if (parseInt(minutes_val) > parseInt(max_minute_val_db) && parseInt(hours_val) == parseInt(max_val_minutes_db) && parseInt(hours_val) > 0) {
                                                            //                                                            if (parseInt(hours_val_set) > 0) {
                                                            //                                                                var new_max_val_hours = parseInt(hours_val_set) - 1;
                                                            //                                                                $(this).attr('max', new_max_val_hours);
                                                            //                                                            }
                                                            //                                                            $(this).val(parseInt(hours_val) - 1);
                                                            //                                                        }
                                                            //                                                    });
                                                        </script>
                                                        <div class="edit_comment_left">

                                                            <div class="clear"></div>

                                                            <div id="comment_form_main" <?php echo $comment_box_display; ?>>
                                                                <form class="edit_comment_form" method="post" id="comment_form_<?php echo $vidComments['Comment']['id'] ?>"
                                                                      class="new_comment" accept-charset="UTF-8"
                                                                      action="<?php echo $this->base . '/Huddles/editComments/' . $vidComments['Comment']['id']; ?>" >
                                                                    <div style="margin:0;padding:0;display:inline">
                                                                        <input type="hidden" value="Ã¢Å“â€œ" name="utf8">
                                                                    </div>
                                                                    <input type="hidden" id="comments_text_<?php echo $vidComments['Comment']['id'] ?>" value="<?php echo strip_tags($vidComments['Comment']['comment']) ?>"/>
                                                                    <input type="hidden" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" name="authenticity_token" id="tokentag">
                                                                    <input type="hidden" value="<?php echo $vidComments['Comment']['id'] ?>" name="comment_id" />
                                                                    <input type="hidden" value="<?php echo $vidComments['Comment']['ref_id'] ?>" name="videoId" />
                                                                    <?php
                                                                    $input_group_style = "margin-top: 20px;";
                                                                    ?>




                                                                    <div class="input-group" style="<?php echo $input_group_style; ?>">
                                                                        <?php if ($vidComments['Comment']['ref_type'] != '6') { ?>  <textarea rows="3" placeholder="Edit comments" style="width: 422px !important;border-radius:0px;height: 100px;" name="comment[comment]" id="comment_comments_<?php echo $vidComments['Comment']['id'] ?>"><?php echo $vidComments['Comment']['comment']; ?></textarea> <?php } ?>
                                                                        <?php
                                                                        if ($vidComments['Comment']['ref_type'] == '6') {
                                                                            if (Configure::read('use_cloudfront') == true) {
                                                                                $url = $vidComments['Comment']['comment'];
                                                                                $videoFilePath = pathinfo($url);
                                                                                $videoFileName = $videoFilePath['filename'];
                                                                                if ($videoFilePath['dirname'] == ".") {

                                                                                    $video_path = $this->Custom->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . ".m4a");
                                                                                    $relative_video_path = $videoFileName . ".m4a";
                                                                                } else {

                                                                                    $video_path = $this->Custom->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".m4a");
                                                                                    $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".m4a";
                                                                                }
                                                                            } else {
                                                                                $url = $vidComments['Comment']['comment'];
                                                                                $videoFilePath = pathinfo($url);
                                                                                $videoFileName = $videoFilePath['filename'];
                                                                                if ($videoFilePath['dirname'] == ".") {

                                                                                    $video_path = $this->Custom->getSecureAmazonUrl($videoFileName . ".m4a");
                                                                                    $relative_video_path = $videoFileName . ".m4a";
                                                                                } else {

                                                                                    $video_path = $this->Custom->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName . ".m4a");
                                                                                    $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".m4a";
                                                                                }
                                                                            }
                                                                            ?>
                                                                            <div class="comment-body comment">
                                                                                <audio controls>
                                                                                    <source src="<?php echo $video_path; ?>">

                                                                                    Your browser does not support the audio element.
                                                                                </audio>
                                                                            </div>
                                                                            <br>
                                                                        <?php } ?>

                                                                        <div class="tags1 divblockwidth">
                                                                            <div class="video-tags-row row">
                                                                                <input type="text" name="txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>" data-default="Tags..." id="txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>" value="" placeholder="" readonly="readonly"/>
                                                                            </div>
                                                                            <div class="clear" style="clear: both;"></div>
                                                                        </div>


                                                                        <div class="clear" style="clear: both;"></div>
                                                                        <?php
                                                                        $time = explode(':', formatSeconds($vidComments['Comment']['time']));
                                                                        $video_time = explode(':', formatSeconds($videos['doc']['current_duration']));
                                                                        ?>
                                                                        <?php if (isset($videos['doc']['doc_type']) && $videos['doc']['doc_type'] == 3): ?>
                                                                            <?php if (($videos['doc']['published'] == 1 || $videos['doc']['published'] == 0) && $videos['doc']['is_processed'] != 5): ?>
                                                                                <input min="0" max="1000" class="time_hoursCls_1" data-max_val_db="<?php echo $video_time['0']; ?>" type="number" name="time_hours" value="<?php echo $time[0] ?>" class="hours" id="hours_<?php echo $vidComments['Comment']['id'] ?>" style="width: 50px !important;" onkeydown="return false" />
                                                                                <input min="0"  max="59" class="time_minutesCls_1" data-max_val_db="<?php echo $video_time['1']; ?>" type="number" name="time_minutes" value="<?php echo $time[1] ?>" id="minutes_<?php echo $vidComments['Comment']['id'] ?>" style="width: 50px !important;" onkeydown="return false" />
                                                                                <input  min="0" max="59" class="time_secondsCls_1" data-max_val_db="<?php echo $video_time['2']; ?>" type="number" name="time_seconds" value="<?php echo $time[2] ?>" id="seconds_<?php echo $vidComments['Comment']['id'] ?>" style="width: 50px !important;" onkeydown="return false" />
                                                                            <?php endif; ?>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                    <div class="clear" style="clear: both;"></div>
                                                                    <input type="hidden" value="nested" name="comment[access_level]" id="comment_access_level">
                                                                    <div class="input-group">
                                                                        <input type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green edit_btn_adjs" id="btn_form_sub_<?php echo $vidComments['Comment']['id'] ?>" value="Edit Note" name="submit">
                                                                        <a class="btn btn-transparent" id="close_edit_form_<?php echo $vidComments['Comment']['id'] ?>" data-dismiss="modal">Cancel</a>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                        </div>

                                                        <script type="text/javascript">
                                                            $(document).ready(function (e) {
                                                                $(document).on("click", "#btn_form_sub_<?php echo $vidComments['Comment']['id'] ?>", function () {


                                                                    var seconds = $('#seconds_<?php echo $vidComments['Comment']['id']; ?>').val();
                                                                    var minutes = $('#minutes_<?php echo $vidComments['Comment']['id']; ?>').val();
                                                                    var hours = $('#hours_<?php echo $vidComments['Comment']['id']; ?>').val();

                                                                    var db_total_time = '<?php echo $videos['doc']['current_duration']; ?>';
                                                                    db_total_time = parseInt(db_total_time);
                                                                    var total_time = (parseInt(hours) * 60 * 60) + (parseInt(minutes) * 60) + parseInt(seconds);

                                                                    if (total_time > db_total_time)
                                                                    {
                                                                        alert('Timestamp cannot be greater than video duration.');
                                                                        return false;
                                                                    }



                                                                    $.ajax({
                                                                        type: 'POST',
                                                                        url: home_url + '/Huddles/editComments/<?php echo $vidComments['Comment']['id']; ?>',
                                                                        data: $("#comment_form_<?php echo $vidComments['Comment']['id'] ?>").serialize(),
                                                                        success: function (response) {
                                                                            $("#edit_comment_form_<?php echo $vidComments['Comment']['id'] ?>").modal('hide');
                                                                            getVideoComments_workspace();
                                                                        }
                                                                    });
                                                                });
                                                                isSubmitting = false;
                                                                $(document).on("click", "#close_edit_form_<?php echo $vidComments['Comment']['id'] ?>", function () {
                                                                    if (isSubmitting)
                                                                        return false;
                                                                    isSubmitting = true;
                                                                    getVideoComments_workspace();
                                                                });
                                                                $(document).on('change', '.standard_<?php echo $vidComments['Comment']['id']; ?> input', function () {
                                                                    var tag_code = $(this).attr('st_code');
                                                                    var tag_value = '';
                                                                    var tag_array = {};
                                                                    var tag_name = $(this).attr('st_name');
                                                                    tag_array = tag_name.split(" ");
                                                                    if ($(this).is(':checked')) {
                                                                        tag_array = tag_name.split(" ");
                                                                        tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                                        $('[name="txtVideostandard1<?php echo $vidComments['Comment']['id']; ?>"]').addTag(tag_value);
                                                                        //$('#txtVideostandard_tag').remove();
                                                                    } else {
                                                                        tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                                                                        $('[name="txtVideostandard1<?php echo $vidComments['Comment']['id']; ?>"]').removeTag(tag_value);
                                                                    }
                                                                    if ($('input[name="name1"]:checked').length > 0) {
                                                                        $('#txtVideostandard_tag').hide();
                                                                    }
                                                                });

                                                                var tagID = $('[name=txtVideostandard1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                                                                $('#' + tagID).tagsInput({
                                                                    defaultText: 'Tag Standard...',
                                                                    //width: '100px',
                                                                    readonly_input: true,
                                                                    placeholderColor: '#b2b2b2',
                                                                });




                                                                var tagID = $('[name=txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                                                                $('#' + tagID).tagsInput({
                                                                    defaultText: 'Tags...',
                                                                    //width: '100px',
                                                                    placeholderColor: '#b2b2b2',
                                                                });
                                                                $('.video-tags-row label').css('display', 'none');
                                                            });
                                                        </script>
                                                        <script type="text/javascript">
                                                            $(document).ready(function () {
                                                                var tagID = $('[name=txtVideoTags1<?php echo $vidComments['Comment']['id'] ?>]').attr('id');
                                                                $('#clearTagsButton').click(function () {
                                                                    $('#txtSearchTags').val('');
                                                                    $('#clearTagsButton').css('display', 'none');
                                                                    $('input#txtSearchTags').quicksearch('ul#expList li', {
                                                                        noResults: 'li#noresults'
                                                                    });
                                                                });
                                                                $("#inline-crop-panel").fancybox({
                                                                    width: '70%',
                                                                    height: '70%',
                                                                    type: "iframe",
                                                                    closeClick: false,
                                                                    helpers: {
                                                                        title: null,
                                                                        overlay: {closeClick: false}
                                                                    }
                                                                });

                                                                $("#inline-crop-panel").click(function () {
                                                                    pausePlayer();
                                                                });

            <?php if (isset($_REQUEST['open_trim']) && $_REQUEST['open_trim'] == '1'): ?>
                                                                    pausePlayer();
                                                                    $("#inline-crop-panel").trigger('click');
            <?php endif; ?>

                                                                $('.btn-trim-video').click(function () {
                                                                    $(this).next().submit();
                                                                });
                                                                $('.default_tag1<?php echo $vidComments['Comment']['id'] ?>').on('click', function (e) {
                                                                    e.preventDefault();
                                                                    var posid = '';
                                                                    //tag_name = $(this).text();
                                                                    tag_name = $(this).attr('alt');
                                                                    tag_name.replace(/\s/g, "");

                                                                    posid = $(this).attr('position_id');
                                                                    $('.default_tag1<?php echo $vidComments['Comment']['id'] ?>').each(function (index) {
                                                                        var tag_name1 = $(this).text().trim();
                                                                        tag_name1.replace(/\s/g, "");
                                                                        $('#' + tagID).removeTag(tag_name1);
                                                                        $(this).attr('status_flag', '0');
                                                                        if ($(this).hasClass(defaulttagsclasses(index))) {
                                                                            $('#' + tagID).removeTag(tag_name1);
                                                                            $(this).removeClass(defaulttagsclasses(index) + 'bg');
                                                                        }
                                                                    });
                                                                    $('.default_tag1<?php echo $vidComments['Comment']['id'] ?>').each(function (index) {
                                                                        if ($(this).attr('position_id') != posid) {
                                                                            $(this).removeClass(defaulttagsclasses($(this).attr('position_id')));
                                                                            $(this).addClass(defaulttagsclasses1($(this).attr('position_id')));

                                                                        }
                                                                    });

                                                                    if ($(this).hasClass(defaulttagsclasses(posid))) {
                                                                        $('#' + tagID).removeTag(tag_name);
                                                                        $(this).addClass(defaulttagsclasses(0));
                                                                        $(this).removeClass(defaulttagsclasses(posid));
                                                                        $(this).attr('status_flag', '0');
                                                                        $('#comment_comment').attr("placeholder", "Add a comment...");
                                                                        $('#synchro_time_class').val('');
                                                                    }
                                                                    else {
                                                                        $('#' + tagID).addTag("# " + tag_name);
                                                                        $(this).addClass(defaulttagsclasses(posid));
                                                                        $(this).removeClass(defaulttagsclasses(0));
                                                                        $(this).attr('status_flag', '1')
                                                                        var spt = $(this).text().slice(2);
                                                                    }
                                                                });
                                                            });
                                                        </script>


                                                    </div>

                                                    <div class="clear"></div>

                                                </div>
                                                <div id="reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>" style="display: none;">
                                                    <script type="text/javascript">
                                                        $(document).ready(function () {
                                                            $("#reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide();
                                                            $("#reply_button_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                                                $("#reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>").show("slow", function () {
                                                                    $('#comment_comment_<?php echo $vidComments['Comment']['id'] ?>').focus();
                                                                });
                                                            });

                                                            $("#close_reply_form_<?php echo $vidComments['Comment']['id'] ?>").click(function () {
                                                                $("#reply_comment_form_<?php echo $vidComments['Comment']['id'] ?>").hide("slow");
                                                            });

                                                            $('#comments_form_<?php echo $vidComments['Comment']['id'] ?>').on('submit', function (event) {

                                                                var $form = $(this);
                                                                var $target = $($form.attr('data-target'));

                                                                $.ajax({type: $form.attr('method'), url: $form.attr('action'),
                                                                    data: $form.serialize(),
                                                                    success: function (data, status) {
                                                                        getVideoComments_workspace();
                                                                    }
                                                                });

                                                                event.preventDefault();

                                                            });

                                                            $('#delete-main-comments-frm<?php echo $vidComments['Comment']['id']; ?>').on('submit', function (event) {

                                                                var $form = $(this);
                                                                var $target = $($form.attr('data-target'));

                                                                $.ajax({type: $form.attr('method'), url: $form.attr('action'),
                                                                    data: $form.serialize(),
                                                                    success: function (data, status) {
                                                                        getVideoComments_workspace();
                                                                    }
                                                                });

                                                                event.preventDefault();

                                                            });

                                                        });
                                                    </script>

                                                    <form method="post" id="comments_form_<?php echo $vidComments['Comment']['id'] ?>" class="new_comment" action="<?php echo $this->base . '/Huddles/addReply/' . $vidComments['Comment']['id']; ?>" accept-charset="UTF-8">
                                                        <div style="margin:0;padding:0;display:inline">
                                                            <input type="hidden" value="âœ“" name="utf8"></div>
                                                        <input type="hidden" value="G9YjmvNm1h8CpIRPRD5Ysoad5bSE1sHuMpnvp//UDHE=" name="authenticity_token" id="tokentag">
                                                        <div class="input-group">
                                                            <textarea rows="3" placeholder="Add Reply..." name="comment[comment]" id="comment_comment_<?php echo $vidComments['Comment']['id'] ?>" cols="35"></textarea>
                                                        </div>
                                                        <input type="hidden" value="nested" name="comment[access_level]" id="comment_access_level">

                                                        <div class="input-group">
                                                            <input type="submit" class="btn btn-green" value="Add Reply" name="submit">
                                                            <a class="btn btn-transparent" id="close_reply_form_<?php echo $vidComments['Comment']['id'] ?>">Cancel</a>
                                                        </div>
                                                    </form>

                                                </div>

                                                <ul class="comment-replies" id="nested_comment_<?php echo $vidComments['Comment']['id'] ?>">
                                                    <?php
                                                    $replies = $Comment->commentReplys($vidComments['Comment']['id']);
                                                    if (isset($replies) && count($replies) > 0):
                                                        $count = 0;
                                                        foreach ($replies as $reply):
                                                            $commentDate = $reply['Comment']['created_date'];
                                                            $commentDate = $this->Custom->formateDate($commentDate);
                                                            ?>
                                                            <script type="text/javascript">
                                                                $(document).ready(function () {
                                                                    $('#comments_form_<?php echo $reply['Comment']['id'] ?>').on('submit', function (event) {
                                                                        var $form = $(this);
                                                                        var $target = $($form.attr('data-target'));
                                                                        $.ajax({
                                                                            type: $form.attr('method'),
                                                                            url: $form.attr('action'),
                                                                            data: $form.serialize(),
                                                                            success: function (data, status) {
                                                                                getVideoComments_workspace();
                                                                            }
                                                                        });
                                                                        event.preventDefault();
                                                                    });

                                                                    $("#reply_comment_form_<?php echo $reply['Comment']['id'] ?>").hide();
                                                                    $("#reply_button_<?php echo $reply['Comment']['id'] ?>").click(function () {
                                                                        $("#reply_comment_form_<?php echo $reply['Comment']['id'] ?>").show("slow");
                                                                    });

                                                                    $("#close_reply_form_<?php echo $reply['Comment']['id'] ?>").click(function () {
                                                                        $("#reply_comment_form_<?php echo $reply['Comment']['id'] ?>").hide("slow");
                                                                    });
                                                                });
                                                            </script>
                                                            <div id="comment_box_<?php echo $reply['Comment']['id'] ?>">
                                                                <li class="comment">
                                                                    <a href="<?php echo $this->base . '/users/editUser/' . $reply['Comment']['user_id']; ?>">
                                                                        <?php if (isset($reply['User']['image']) && $reply['User']['image'] != ''): ?>
                                                                            <?php
                                                                            $profile_image = $this->Custom->getSecureAmazonSibmeUrl("static/users/" . $reply['User']['id'] . "/" . $reply['User']['image'], $reply['User']['image']);

                                                                            $chimg = $profile_image;
                                                                            echo $this->Html->image($chimg, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '21', 'width' => '21', 'align' => 'left'));
                                                                            ?>
                                                                        <?php else: ?>
                                                                            <img width="21" height="21"  src="<?php echo $chimgDefault; ?>" class="photo photo inline" rel="image-uploader" alt="Photo-default">
                                                                        <?php endif; ?>
                                                                    </a>
                                                                    <div class="comment-header comment-reply"><?php echo $reply['User']['first_name'] . " " . $reply['User']['last_name']; ?> <span class="reply-to-icon">&nbsp;</span> <span style="color: #9c9c9c; font-size: 11px;"><?php echo $vidComments['User']['first_name'] . " " . $vidComments['User']['last_name']; ?></span></div>
                                                                    <div class="comment-body comment more">
                                                                        <?php echo $reply['Comment']['comment']; ?>
                                                                    </div>
                                                                    <div class="comment-footer">
                                                                        <span class="comment-date"><?php echo $replyDate; ?></span>

                                                                        <div class="comment-actions">

                                                                            <form id="delete-reply-comments-frm<?php echo $reply['Comment']['id']; ?>" action="<?php echo $this->base . '/Huddles/deleteVideoComments/' . $reply['Comment']['id']; ?>" accept-charset="UTF-8">

                                                                                <a rel="nofollow" data-confirm="Are you sure you want to delete this comment?" id="delete-reply-comments" class="comment-delete" href="javascript:deleteComment('#delete-reply-comments-frm<?php echo $reply['Comment']['id']; ?>','');">Delete</a>

                                                                                <script type="text/javascript">
                                                                                    $(document).ready(function () {


                                                                                        $('#delete-reply-comments-frm<?php echo $reply['Comment']['id']; ?>').on('submit', function (event) {

                                                                                            var $form = $(this);
                                                                                            var $target = $($form.attr('data-target'));

                                                                                            $.ajax({
                                                                                                type: $form.attr('method'),
                                                                                                url: $form.attr('action'),
                                                                                                data: $form.serialize(),
                                                                                                success: function (data, status) {
                                                                                                    getVideoComments_workspace();
                                                                                                }
                                                                                            });

                                                                                            event.preventDefault();

                                                                                        });
                                                                                    });
                                                                                </script>
                                                                            </form>
                                                                        </div>
                                                                        <div id="reply_comment_form_<?php echo $reply['Comment']['id'] ?>" style="display: none;">
                                                                            <form method="post" id="comments_form_<?php echo $reply['Comment']['id']; ?>" class="new_comment" action="<?php echo $this->base . '/Huddles/addReply/' . $vidComments['Comment']['id']; ?>" accept-charset="UTF-8">
                                                                                <div style="margin:0;padding:0;display:inline"><input type="hidden" value="âœ“" name="utf8"></div>
                                                                                <input type="hidden" value="D91PQMV0iwdtcBfDzvuRAfxMrDboT3D1kX75GvptfOI=" name="authenticity_token" id="tokentag">
                                                                                <div class="input-group">
                                                                                    <textarea rows="3" placeholder="Add Reply..." name="comment[comment]" required id="comment_comment_<?php echo $reply['Comment']['id'] ?>" cols="35"></textarea>
                                                                                </div>
                                                                                <input type="hidden" value="nested_multilevel" name="comment[access_level]" id="comment_access_level">

                                                                                <div class="input-group">
                                                                                    <input type="submit" class="btn btn-green" value="Add Reply" name="submit">
                                                                                    <a class="btn btn-transparent" id="close_reply_form_<?php echo $reply['Comment']['id'] ?>">Cancel</a>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </div>

                                                            <?php
                                                            $count++;
                                                            $video_comment_counter++;
                                                        endforeach;
                                                        ?>
                                                    <?php endif; ?>
                                                </ul>
                                                </li>


                                            </div>
                                            </ul>
                                            <?php
                                        endforeach;
                                    ?>
                                    <?php
                                else:
                                    ?>
                                <?php endif; ?>

                            </div>
                        </div>

                </div>
                <script>
                    $(document).ready(function () {
                        var showChar = 200;
                        var ellipsestext = "...";
                        var moretext = "more";
                        var lesstext = "less";
                        $('.more').each(function () {
                            var content = $(this).text();

                            if (content.length > showChar) {

                                var n = content.indexOf(' ', showChar);
                                if (n == -1)
                                    n = showChar;
                                var c = content.substr(0, n);
                                var h = content.substr(n);

                                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h +
                                        '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                                $(this).html(html);
                            }

                        });

                        $(".morelink").click(function () {
                            if ($(this).hasClass("less")) {
                                $(this).removeClass("less");
                                $(this).html(moretext);
                            } else {
                                $(this).addClass("less");
                                $(this).html(lesstext);
                            }
                            $(this).parent().prev().toggle();
                            $(this).prev().toggle();
                            return false;
                        });

                    });

                    function deleteComment(formName, comment_id) {

                        $('#comment_box_' + comment_id).closest("div").hide();
                        $(formName).submit();
                        return false;
                    }
                    function deleteCommentTag(tagId) {
                        //$(formName).submit();
                        $.ajax({
                            type: 'GET',
                            url: home_url + '/Huddles/deleteCommentTag/' + tagId,
                            success: function (response) {
                                getVideoComments_workspace();
                            }
                        });

                        return false;
                    }

                    function getVideoComments_workspace(callback) {

                        if ($('#video-id').length == 0)
                            return;


                        account_folder_id = $('#workspace_video_id').val();

                        var videoId = $('#video-id').val();
                        get_comment_count(videoId);
                        var url = home_url + '/MyFiles/getVideoComments/' + videoId;
                        var srtType = $('#cmt-sortings').val();
                        var txtcomment = $('#txtSearchVideos').val();
                        if (txtcomment === undefined || txtcomment === null) {
                            txtcomment = '';
                        }
                        var check_tags = $('#tagsDivcls2').val();
                        var tagid = '';
                        if (check_tags === undefined || check_tags === null) {
                            tagid = '';
                        }
                        else {
                            $(this).parent().find('[status_flag="1"]').each(function (value) {
                                tagid += $(this).attr("tag_id") + ',';
                            });
                        }
                        $.ajax({
                            dataType: 'JSON',
                            type: 'POST',
                            data: {type: 'get_video_comments', srtType: srtType, searchCmt: txtcomment, tags: tagid, video_id: account_folder_id}, url: url,
                            success: function (response) {

                                if (typeof callback == 'function') {
                                    callback();
                                }
                                if (typeof initVideoComments == 'function') {
                                    initVideoComments();
                                }

                                $('.synchro-time').on('click', function () {
                                    setTimeout(function () {
                                        $('[name="synchro_time"]').val($(this).data('time'));
                                        if ($('#video_span').length && small_scroll) {
                                            $('html, body').animate({
                                                scrollTop: $('#video_span').offset().top
                                            }, 400);
                                        }
                                    }, 1000);
                                });
                                $('.modal').modal('hide');
                                $('#vidComments').html(response.contents);

                            },
                            errors: function (response) {
                                alert(response.contents);
                            }
                        });
                    }

                    $(document).on("click", ".timestamp_attachment_no", function () {
                        $('#attachment').trigger('click');
                    });



                </script>
