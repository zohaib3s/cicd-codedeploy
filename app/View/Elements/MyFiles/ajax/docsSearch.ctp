<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php

if (count($documents) > 0): ?>
<?php foreach ($documents as $row): ?>
<tr class="docs-list__item">
    <td>
        <label class="ui-checkbox docSearchTab">
            <input class="download_file_doc_checkbox documentTab" data-account-folder-document-id2="<?php echo $row['afd']['id'] ?>" name="document_ids[]" type="checkbox" value="<?php echo $row['Document']['id'] ?>">
        </label>
        <span class="<?php echo $this->Custom->getIcons($row['Document']['url']); ?>">&nbsp;&nbsp;&nbsp;&nbsp;</span>

        <?php if(isset($row['Document']['stack_url']) && $row['Document']['stack_url'] !='')
        {  ?>
         <?php 
         $url_code =  explode('/',$row['Document']['stack_url'] ) ; 
         $url_code = $url_code[3];
         ?>
        <a  id="view_resource_myfiles_<?php echo $row['Document']['id']; ?>" target="_blank" href="<?php echo $this->base.'/MyFiles/view_document/'.$url_code; ?>" >
            <span id="doc-title-<?php echo $row['afd']['id']; ?>"><?php echo strlen($row['afd']['title']) > 25 ? mb_substr($row['afd']['title'], 0, 25) . "..." : $row['afd']['title'] ?></span>
        </a> 
        
        
        <?php } else { ?>
        <a href="<?php echo $this->base . '/MyFiles/download/' . $row['Document']['id'] ?>/2" title="<?php echo $row['afd']['title'] ?>" >
            <span id="doc-title-<?php echo $row['afd']['id']; ?>"><?php echo strlen($row['afd']['title']) > 25 ? mb_substr($row['afd']['title'], 0, 25) . "..." : $row['afd']['title'] ?></span>
        </a>
        <?php } ?>

    </td>
    <td style="width: 300px;">
        <div id="input-title-<?php echo $row['afd']['id']; ?>" style="display: none;">
            <input id="input-field-<?php echo $row['afd']['id']; ?>" type="text" name="title" value="" required="required"/>
            <a onclick="return update_title(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php 
            $chimg = $this->Custom->getSecureAmazonSibmeUrl('app/img/icons/save-btn.png');
            echo $this->Html->image($chimg); ?></a>
            <a onclick="return cancel(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php 
            $chimg = $this->Custom->getSecureAmazonSibmeUrl('app/img/icons/cancel-btn.png');
            echo $this->Html->image($chimg); ?></a>
        </div>
        <div id="videos-title-<?php echo $row['afd']['id']; ?>" class="cursor-pointer" data-title="<?php echo $row['afd']['title']; ?>">
            <?php echo $row['afd']['title']; ?>
            <div style="clear: both;"> </div>
        </div>
        <div> <?php echo $language_based_content['uploaded_by_videos_workspace']; ?> <a href="#" title="<?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?>" ><?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?></a></div>
    </td>
    <td class="my-workspace-type" style="border: 0"><?php echo $this->Custom->docType($row['Document']['url']); ?></td>
    <td class="my-workspace-data" style="border: 0; width:none"><?php if($_SESSION['LANG'] == 'es'){ echo $this->Custom->SpanishDate(strtotime($row['Document']['created_date']) , 'most_common' );  } else { echo date('M d, Y', strtotime($row['Document']['created_date']));} ?></td>
<!--    <td style="border: 0">
        <div class="doc_video_association">
            <div class="subject-row row">
                <div class="video-dropdown">Videos</div>
                <div class="video-dropdown-container">
                    <div class="container">
                        <ul>
                        <?php// $assVideoMap = isset($associatedVideoMap[$row['Document']['id']]) ? $associatedVideoMap[$row['Document']['id']] : array(); ?>
                        <?php //if ($videos): ?>
                        <?php //foreach ($videos as $video): ?>
                            <li>
                                <label class="ui-checkbox checkbox-2">
                                    <input  id="video_ids_<?php //echo $row['Document']['id'] . '_' . $video['doc']['id'];?>"
                                            name="video_ids<?php// echo $video['doc']['id'];?>"
                                            class="checkbox-3 doc_checkbox_<?php //echo $row['Document']['id']; ?>"
                                            type="checkbox" value="<?php //echo $video['doc']['id'];?>"
                                        <?php //echo isset($assVideoMap[$video['doc']['id']]) ? 'checked = "checked"' : ''; ?>
                                            />
                                </label>

                                <label for="video_ids_<?php //echo $row['Document']['id'] . '_' . $video['doc']['id'];?>"
                                       title="<?php //echo $video['afd']['title']; ?>"
                                       class="title">
                                    <?php //echo (strlen($video['afd']['title']) > 15 ? substr($video['afd']['title'], 0, 15) . "..." : $video['afd']['title'] ) ?>
                                </label>
                            </li>
                        <?php //endforeach; ?>
                        <?php //endif; ?>
                        </ul>
                    </div>
                    <div class="video-dropdown-container-bottom">
                        <input type="button" class="btnDoneVideoLibrary" id="btnDoneDoc-<?php //echo $row['Document']['id']; ?>" value="Done"/>
                        <input type="button" class="btnCancelVideoLibrary btn btn-grey"  id="btnDoneDoc-<?php //echo $row['Document']['id']; ?>" value="Cancel"/>
                        <input type="hidden" name="account_folder_id" value="<?php //echo $row['afd']['account_folder_id'];?>" />
                    </div>
                </div>
            </div>
        </div>
    </td>-->
</tr>
<script type="text/javascript">
    $(document).ready(function (e) {
        var $input = "#input-field-<?php echo $row['afd']['id']; ?>";
        var $id = "#videos-title-<?php echo $row['afd']['id']; ?>";
        var $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
        $($id).click(function (e) {
            var $title = $(this).attr('data-title');
            $title_block = "#input-title-<?php echo $row['afd']['id']; ?>";
            $input = "#input-field-<?php echo $row['afd']['id']; ?>";
            $(this).css("display", 'none');
            $($title_block).css("display", 'block');
            $($input).val($title);
        });
        $($id).mouseover(function () {
            $(this).css("backgroundColor", "#FAFABE")
        });
        $($id).mouseout(function () {
            $(this).css("backgroundColor", "#ffffff")
        });
    });
    function update_title($document_id) {
        var $value = '#input-field-' + $document_id;
        var $video_title = "#videos-title-" + $document_id;
        var $title_block = "#input-title-" + $document_id;
        var $doc_title = "#doc-title-" + $document_id;

        var $newText = $($value).val();
        if ($newText == '') {
            $($value).css('border', '1px red solid');
            return false;
        }
        var $nexText = $.trim($newText);
        $.ajax({
            url: home_url + "/MyFiles/editTitle",
            data: {title: $nexText, document_id: $document_id},
            type: 'POST',
            success: function (response) {
                $($video_title).attr('data-title', response)
                $($video_title).html(response);
                $($doc_title).html(response);
                $($value).val("");
                $($title_block).css('display', 'none');
                $($video_title).css('display', 'block');
            },
            error: function () {
                alert('<?php echo $alert_messages['network_error_occured']; ?>');
            }
        });
    }
    function cancel($document_id) {
        var $video_title = "#videos-title-" + $document_id;
        var $title_block = "#input-title-" + $document_id;
        $($title_block).css('display', 'none');
        $($video_title).css('display', 'block');
    }
      $("#view_resource_<?php echo $row['Document']['id']; ?>").click(function () {
                var id = '<?php echo $row['Document']['id']; ?>';
                                       $.ajax({
                                            url: home_url + '/huddles/view_resource/' + id,
                                            type: 'POST',
                                            success: function (response) {
                                              

            },
            error: function () {

            }
        });
            });
</script> 
<script>
    $("#view_resource_myfiles_<?php echo $row['Document']['id']; ?>").click(function () {
        var id = '<?php echo $row['Document']['id']; ?>';
        
        $.ajax({
            url: home_url + '/huddles/view_resource/' + id,
            type: 'POST',
            success: function (response) {


            },
            error: function () {

            }
        });
    });

</script>
<?php endforeach; ?>
<script>
    $(document).ready(function(e){
        $(document).on('click','.docs-huddle-contents',function (e) {
            $.ajax({
                type: 'POST',
                data: {
                    type: 'get_video_comments',
                    sort: $('#myFilesVideoSort').val(),
                    limit: "",
                    page: ""
                },
                dataType: 'json',
                url: home_url + '/MyFiles/getCopyHuddlesList',
                success: function (response) {
                    var list = $('.list-containers-docs-box');

                    if (response.html !='') {
                        list.html(response.html);
                        gApp.updateCheckboxEvent($('.list-containers-docs-box .ui-checkbox input'));
                    }else{
                        list.html("No Huddles found in your account.");

                    }
                },
                errors: function (response) {
                    console.log(response);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
                    //location.reload(true);
                }
            });
        });
    })

</script>
<?php else: ?>
<tr class="docs-list__item">
    <td>
        <div style="text-align: center; padding: 10px;">
            <?php 
                if (empty($title) && count($documents) == 0) {
            ?>
            <?php echo $language_based_content['no_resources_workspace']; ?>
            <?php 
                } else {
            ?>
            <?php echo $language_based_content['none_of_your_resources_workspace']; ?>
            <?php 
                }
            ?>
        </div>
    </td>
</tr>
<?php endif; ?>
