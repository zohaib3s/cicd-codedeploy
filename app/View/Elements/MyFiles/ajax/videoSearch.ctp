<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
$amazon_base_url = Configure::read('amazon_base_url');

$browser = "default";

if (isset($_SERVER['HTTP_USER_AGENT'])) {
    $agent = $_SERVER['HTTP_USER_AGENT'];
}

if (strlen(strstr($agent, 'Firefox')) > 0) {
    $browser = 'firefox';
}
$chimgSave = $this->Custom->getSecureAmazonSibmeUrl('app/img/icons/save-btn.png');
$chimgCancel = $this->Custom->getSecureAmazonSibmeUrl('app/img/icons/cancel-btn.png');
$chimgDownload = $this->Custom->getSecureAmazonSibmeUrl('app/img/new/download-2.png');
?>
<?php if (isset($files_mode) && $files_mode == 'list'): ?>
    <?php if (is_array($files) && count($files) > 0): ?>

        <?php foreach ($files as $row): ?>
            <?php
            $transcoding_status = $this->Custom->transcoding_status($row['Document']['id']);
            $videoID = $row['Document']['id'];
            $videoFilePath = pathinfo($row['Document']['url']);
            $videoFileName = $videoFilePath['filename'];

            $document_files_array = $this->Custom->get_document_url($row['Document']);

            if (empty($document_files_array['url'])) {
                $row['Document']['published'] = 0;
                $document_files_array['url'] = $row['Document']['original_file_name'];
                $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                $row['Document']['duration'] = '00:00';
            } else {
                $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                @$row['Document']['duration'] = $document_files_array['duration'];
            }
            ?>

            <?php if ($row['Document']['doc_type'] == 3 && ($row['Document']['is_processed'] == 5 || $row['Document']['is_processed'] == 1)) { ?>
                <tr class="docs-list__item" id="processing-list-container-<?php echo $row['Document']['id']; ?>">
                    <td style="width: 157px;" class="myfiles_video_icon">
                        <label style="float: left;margin-right: 1.5em;height: 5px;"></label>
                        <div class="thumb1" style="float: left;position:relative;">
                            <a id="processing-message-<?php echo $row['Document']['id']; ?>"
                               href="<?php echo $this->base . '/workspace_video/video_observation/' . $row['AccountFolder']['account_folder_id'].'/'.$row['Document']['id']; ?>"
                               title="<?php echo $row['afd']['title'] ?>">
                                <div class="video-unpublished " style="height: 55px;width: 74px;">
                                    <span class="" style="line-height: 14px;font-size: 8px;">
                                        <img src="<?php echo $this->webroot ?>img/recording_img.png"
                                             style="position: absolute;right: 0;left: 2px;top: 8px;">
                                        <?php echo $alert_messages['live_recording_short']; ?>
                                    </span>
                                </div>
                            </a>
                        </div>
                    </td>

                    <td style="width: 200px;">
                        <div id="input-title-<?php echo $row['afd']['id']; ?>" style="display: none;">
                            <input id="input-field-<?php echo $row['afd']['id']; ?>" type="text" name="title" value=""
                                   required="required" style="margin-right: 5px"/>
                            <a onclick="return update_title(<?php echo $row['afd']['id']; ?>)"
                               style="cursor: pointer"><?php
                                $chimg = $chimgSave;
                                echo $this->Html->image($chimg);
                                ?></a>
                            <a onclick="return cancel(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php
                                $chimg = $chimgCancel;
                                echo $this->Html->image($chimg);
                                ?></a>
                        </div>
                        <div id="videos-title-<?php echo $row['afd']['id']; ?>" class="videos-title cursor-pointer"
                             data-title="<?php echo $row['afd']['title']; ?>">
                            <?php echo $row['afd']['title']; ?>
                            <div style="clear: both;"></div>
                        </div>
                        <div> <?php echo $language_based_content['uploaded_by_videos_workspace']; ?> <a href="#"
                                             title="<?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?>"><?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?></a>
                        </div>
                        <div>
                            <?php
                            if ($browser != 'firefox' && $row['Document']['published'] == '1'):
                                ?>
                                <form id="frmOpenTrim<?php echo $row['AccountFolder']['account_folder_id'] ?>"
                                      method="post"
                                      action="<?php echo $this->base . '/workspace_video/home/' . $row['AccountFolder']['account_folder_id'].'/'.$row['Document']['id'] ?>/1">
                                    <input type="hidden" name="open_trim" value="1"/>
                                </form>
                                <a href="#"
                                   onclick="document.getElementById('frmOpenTrim<?php echo $row['AccountFolder']['account_folder_id'] ?>').submit()"
                                   class="right mmargin-top crop-image crop-image-workspace"><?php echo $language_based_content['edit_videos_C_workspace']; ?></a>
                            <?php
                            endif;
                            ?>

                            <?php if ($row['Document']['published'] == 1): ?>
                                <span id="thumb_regen_<?php echo $row['Document']['id'] ?>_<?php echo $row['AccountFolder']['account_folder_id'] ?>"
                                      document_id="<?php echo $row['Document']['id'] ?>"
                                      huddle_id="<?php echo $row['AccountFolder']['account_folder_id'] ?>"
                                      class="regen-thumbnail-image regen-thumbnail-image-workspace"
                                      title="Regenerate Thumbnail"
                                      rel="tooltip" style="display:none;">Regenerate Thumbnail</span>
                            <?php endif; ?>
                        </div>

                    </td>
                    <td class="my-workspace-type" style="border: 0;">Video</td>
                    <td class="my-workspace-date"
                        style="border: 0;"><?php if($_SESSION['LANG'] == 'es'){ echo $this->Custom->SpanishDate(strtotime($row['Document']['created_date']) , 'most_common' );  } else { echo date('M d, Y', strtotime($row['Document']['created_date']));} ?></td>
                    <td colspan="3" class="copy-table-last-a">&nbsp;&nbsp;</td>
                </tr>
            <?php } ?>

            <tr class="docs-list__item">


                <td style="width: 157px;" class="myfiles_video_icon">
                    <?php if ($row['Document']['published'] == '1'): ?>
                        <label class="ui-checkbox videoSearchTab" style="float: left;">
                            <input class="download_vid_checkbox videoTab"
                                   data-account-folder-document-id="<?php echo $row['afd']['id'] ?>"
                                   data-total-comments="<?php echo $row['Document']['total_comments'] ?>"
                                   name="download_as_zip_docs[]"
                                   type="checkbox"
                                   value="<?php echo $row['Document']['id'] ?>"
                                   style="margin-right: 5px;">
                        </label>
                    <?php else: ?>
                        <label style="float: left;margin-right: 1.5em;height: 5px;"></label>
                    <?php endif; ?>

                    <div class="thumb" style="float: left;position:relative;">
                        <?php
                        if ($row['Document']['published'] == '1' && $transcoding_status != 5) :
                            $seconds = $row['Document']['duration'] % 60;
                            $minutes = ($row['Document']['duration'] / 60) % 60;
                            $hours = gmdate("H", $row['Document']['duration']);
                            ?>
                            <a id="processing-message-<?php echo $row['Document']['id']; ?>"
                               href="<?php echo $this->base . '/workspace_video/home/' . $row['AccountFolder']['account_folder_id'].'/'.$row['Document']['id'] ?>">
                                <?php
                                if ($row['Document']['encoder_provider'] == '2') {
                                    $path = "uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName .
                                        (empty($row['Document']['thumbnail_number']) ?
                                            '_thumb_00001.png' :
                                            '_thumb_' . sprintf('%05d', $row['Document']['thumbnail_number']) . '.png'
                                        );
                                } else {
                                    $path = "uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_thumb.png";
                                }

                                $thumbnail_image_path = $document_files_array['thumbnail'];

                                echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                                ?>
                                <div class="play-icon"></div>
                                <?php if ($row['Document']['duration'] != '00:00'): ?>
                                    <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 38px;right: 0px;padding: 2px;border-radius: 3px;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                                <?php endif; ?>
                            </a>
                        <?php else: ?>
                            <a id="processing-message-<?php echo $row['Document']['id']; ?>"
                               href="<?php echo $this->base . '/workspace_video/home/' . $row['AccountFolder']['account_folder_id'].'/'.$row['Document']['id'] ?>">
                                <div class="video-unpublished" style="height: 55px;width: 74px;">
                                    <?php if ($row['Document']['encoder_status'] == 'Error' || $transcoding_status == 5): ?>
                                        <span><?php echo $alert_messages['video_failed_to_process_short']; ?></span>
                                    <?php else : ?>
                                    <?php
                                    if ($row['Document']['doc_type'] == 3 && $row['Document']['is_processed'] == 4 && $row['Document']['published'] == 0 && $row['Document']['upload_status'] != 'uploaded' && $row['Document']['upload_progress'] < 100) {
                                    ?>
                                        <span id="spnLiveRecording<?php echo $row['Document']['id']; ?>" class=""
                                              style="font-size: 8px; line-height: 16px; background: rgb(62, 117, 84); height: 55px; width: <?php echo $row['Document']['upload_progress'] ?>%; opacity: 0.5;"><div
                                                    style="font-size: 9px;">&nbsp;</div><span
                                                    style="width: 74px;color: #fff;position: absolute;left: 0px;top: 32%;text-align: center;text-transform: uppercase;"><?php echo $row['Document']['upload_progress'] ?>
                                                % <?php echo $row['Document']['upload_status'] ?></span>
                                            </span>
                                        <script type="text/javascript">

                                            observation_inreview.push(<?php echo $row['Document']['id']; ?>);
                                        </script>
                                    <?php
                                    } else {
                                    ?>
                                        <span><img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                                                   style="margin-bottom:10px;width: 18px;height: 18px;min-width: 0px;min-height: 0px;position: absolute;margin: 14px 7px;"/><?php echo $alert_messages['video_currently_processing_short']; ?></span>
                                        <?php
                                    }
                                        ?>
                                    <?php endif; ?>
                                </div>
                            </a>
                        <?php endif; ?>
                    </div>
                </td>
                <td style="width: 200px;">
                    <div id="input-title-<?php echo $row['afd']['id']; ?>" style="display: none;">
                        <input id="input-field-<?php echo $row['afd']['id']; ?>" type="text" name="title" value=""
                               required="required" style="margin-right: 5px"/>
                        <a onclick="return update_title(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php
                            $chimg = $chimgSave;
                            echo $this->Html->image($chimg);
                            ?></a>
                        <a onclick="return cancel(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php
                            $chimg = $chimgCancel;
                            echo $this->Html->image($chimg);
                            ?></a>
                    </div>
                    <div id="videos-title-<?php echo $row['afd']['id']; ?>" class="videos-title cursor-pointer"
                         data-title="<?php echo $row['afd']['title']; ?>">
                        <?php echo $row['afd']['title']; ?>
                        <div style="clear: both;"></div>
                    </div>
                    <div> <?php echo $language_based_content['uploaded_by_videos_workspace']; ?> <a href="#"
                                         title="<?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?>"><?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?></a>
                    </div>
                    <div>
                        <?php
                        if ($browser != 'firefox' && $row['Document']['published'] == '1'):
                            ?>
                            <form id="frmOpenTrim<?php echo $row['AccountFolder']['account_folder_id'] ?>" method="post"
                                  action="<?php echo $this->base . '/workspace_video/home/' . $row['AccountFolder']['account_folder_id'].'/'.$row['Document']['id'] ?>/1">
                                <input type="hidden" name="open_trim" value="1"/>
                            </form>
                            <a href="#"
                               onclick="document.getElementById('frmOpenTrim<?php echo $row['AccountFolder']['account_folder_id'] ?>').submit()"
                               class="right mmargin-top crop-image crop-image-workspace"><?php echo $language_based_content['edit_videos_C_workspace']; ?></a>
                        <?php
                        endif;
                        ?>

                        <?php if ($row['Document']['published'] == 1): ?>
                            <span id="thumb_regen_<?php echo $row['Document']['id'] ?>_<?php echo $row['AccountFolder']['account_folder_id'] ?>"
                                  document_id="<?php echo $row['Document']['id'] ?>"
                                  huddle_id="<?php echo $row['AccountFolder']['account_folder_id'] ?>"
                                  class="regen-thumbnail-image regen-thumbnail-image-workspace"
                                  title="Regenerate Thumbnail"
                                  rel="tooltip" style="display:none;">Regenerate Thumbnail</span>
                        <?php endif; ?>
                    </div>

                </td>
                <td class="my-workspace-type" style="border: 0;">Video</td>
                <td class="my-workspace-date"
                    style="border: 0;"><?php if($_SESSION['LANG'] == 'es'){ echo $this->Custom->SpanishDate(strtotime($row['Document']['created_date']) , 'most_common' );  } else { echo date('M d, Y', strtotime($row['Document']['created_date']));} ?></td>
                <td colspan="3" class="copy-table-last-a">&nbsp;&nbsp;</td>
            </tr>

        <?php endforeach; ?>
        <style>
            .video-unpublished span.video_custom_message {
                margin-top: 2px !important;
                padding: 22% 17px !important;
            }
        </style>

        <script type="text/javascript">

            $(document).ready(function () {

                $('.regen-thumbnail-image').click(function () {

                    var r = confirm('Do you want to Regenerate Thumbnail?');

                    if (r == true) {

                        var document_id = $(this).attr('document_id');
                        var huddle_id = $(this).attr('huddle_id');

                        $.ajax({
                            type: 'POST',
                            url: home_url + '/Huddles/regenerate_thumbnail/' + document_id + '/' + huddle_id,
                            success: function (res) {

                                alert('<?php echo $alert_messages['thumbnail_generated_successfully']; ?>');

                                d = new Date();
                                var src_image = $("#img_" + document_id).attr("src");

                                $("#img_" + document_id).attr("src", src_image + "&dd=" + d.getTime());

                            },
                            errors: function (response) {
                                alert('<?php echo $alert_messages['unable_to_generate_thumbnail']; ?>');
                            }
                        });

                    }

                });

            });

        </script>

    <?php
    else:
        ?>
        <tr>
            <td>
                <div style="text-align: center;"><?php echo $message; ?> </div>
            </td>
        </tr>
    <?php endif; ?>
<?php elseif (isset($files_mode) && $files_mode == 'grid'): ?>
    <style>
        .videos-list__item-thumb img {
            height: 148px;
        }

        .wrap {
            text-overflow: inherit;
            overflow: hidden;
            white-space: normal;
            display: block;
            height: 35px;
        }

        .wrap2 {
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }
    </style>
    <div id="temp-list" style="display: none;">
        <li class="videos-list__item">
            <div class="videos-list__item-thumb">
                <div class="video-unpublished ">
                    <span class="huddles-unpublished" style="padding: 15% 17px !important;">
                        <!--<img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br>Your video is currently processing. You will receive an email notification when your video is ready to be viewed.</span>-->
                        <img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                             style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;"/><br>Your video is currently uploading...</span>
                </div>
            </div>
            <div class="videos-list__item-aside">
                <div id="temp-video-title" class="videos-list__item-title"></div>
                <div class="videos-list__item-author"><?php echo $language_based_content['by_videos_workspace']; ?> <a href="#"
                                                            title="<?php echo $user_current_account['User']['first_name'] . " " . $user_current_account['User']['last_name'] ?>"><?php echo $user_current_account['User']['first_name'] . " " . $user_current_account['User']['last_name'] ?></a>
                </div>
                <div class="videos-list__item-added"><?php echo $language_based_content['uploaded_videos_workspace']; ?> <?php echo date('M d, Y', time()) ?></div>
            </div>
        </li>
    </div>
    <!--<div style="clear: both;" class='clearfix'></div>-->
    <!--<div id="videos-list">-->
    <p id="notification" style="display: none;padding-left: 29px;padding-right: 9px;padding-bottom: 0px;"></p>
    <ul class="videos-list" id="videos-list">
        <?php if (is_array($files) && count($files) > 0): ?>

            <?php foreach ($files as $row): ?>
            <?php
            $transcoding_status = $this->Custom->transcoding_status($row['Document']['id']);
            $videoID = $row['Document']['id'];
            $videoFilePath = pathinfo($row['Document']['url']);
            $videoFileName = $videoFilePath['filename'];

            $document_files_array = $this->Custom->get_document_url($row['Document']);

            if (empty($document_files_array['url'])) {
                $row['Document']['published'] = 0;
                $document_files_array['url'] = $row['Document']['original_file_name'];
                $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
            } else {
                $row['Document']['encoder_status'] = $document_files_array['encoder_status'];
                @$row['Document']['duration'] = $document_files_array['duration'];
            }
            ?>
            <li class="videos-list__item">

                <?php
                if ($row['Document']['doc_type'] == 3 && ($row['Document']['is_processed'] == 5 || $row['Document']['is_processed'] == 1)) {
                    ?>
                    <div style="margin-right: 10px;position: absolute;right: 0;left: 10px;top: 13px;color: white;font-weight: bold;font-size: 10px;">
                        <img src="/img/recording_img.png" id="img_recording_live">&nbsp;<?php echo $alert_messages['live_recording_alert']; ?>
                    </div>
                    <?php
                }
                ?>
                <div class="ac-btn">
                    <a href="<?php echo $this->base . '/MyFiles/deleteFile/' . $row['Document']['id'] . '/1'; ?>"
                       data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                       data-original-title="<?php echo $language_based_content['delete_videos_workspace']; ?>" rel="tooltip" data-method="delete"
                       class="btn icon2-trash right smargin-right fl-btn"
                       style="border:none;background: none;border-radius: 0px;padding: 0px;"></a>


                    <?php
                    if ($browser != 'firefox' && $row['Document']['published'] == '1'):
                        ?>
                        <form id="frmOpenTrim<?php echo $row['AccountFolder']['account_folder_id'] ?>" method="post"
                              action="<?php echo $this->base . '/workspace_video/home/' . $row['AccountFolder']['account_folder_id'].'/'.$row['Document']['id'] ?>/1"
                              style="display: none;">
                            <input type="hidden" name="open_trim" value="1"/>
                        </form>
                        <a href="#"
                           onclick="document.getElementById('frmOpenTrim<?php echo $row['AccountFolder']['account_folder_id'] ?>').submit()"
                           class="btn-trim-video btn icon2-crop right smargin-right fl-btn" title="<?php echo $language_based_content['edit_videos_workspace']; ?>"
                           style="border:none;background: none;border-radius: 0px;padding: 0px;" rel="tooltip"></a>
                    <?php
                    endif;
                    ?>
                    <?php if ($row['Document']['published'] == 1): ?>
                        <a class="copy huddle-contents" id="copy-huddle-<?php echo $row['Document']['id'] ?>"
                           data-document-id="<?php echo $row['afd']['id'] ?>"
                           data-total-comments="<?php echo $row['Document']['total_comments'] ?>"
                           data-original-title="<?php echo $language_based_content['copy_videos_workspace']; ?>" rel="tooltip"
                           data-toggle="modal" data-target="#copyFiles"><?php echo $language_based_content['copy_videos_workspace']; ?></a>
                    <?php endif; ?>
                    <?php if ($row['Document']['published'] == 1): ?>
                        <span id="thumb_regen_<?php echo $row['Document']['id'] ?>"
                              document_id="<?php echo $row['Document']['id'] ?>"
                              huddle_id="<?php echo $row['afd']['account_folder_id'] ?>"
                              class="regen-thumbnail-image regen-thumbnail-image-workspace
                                  regen-thumbnail-image-inner regen-thumbnail-image-huddle-inner"
                              title="Regenerate Thumnail"
                              rel="tooltip" style="display:none;">&nbsp;</span>
                    <?php endif; ?>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            $(document).on("click", '#copy-huddle-<?php echo $row['Document']['id'] ?>', function () {
                                $('.copy-document-ids').val($(this).attr('data-document-id'));
                                if ($(this).attr('data-total-comments') == 0)
                                    $('.copy_video_box').css('display', 'none');
                                else
                                    $('.copy_video_box').css('display', 'block');
                            });
                        });
                    </script>
                </div>

                <div class="clearfix"></div>
                <?php
                $videos_list__item_thumb_style = "";

                if ($row['Document']['doc_type'] == 3 && $row['Document']['is_processed'] == 4 && $row['Document']['published'] == 0 && $row['Document']['upload_status'] != 'uploaded' && $row['Document']['upload_progress'] < 100) {

                    $videos_list__item_thumb_style = "background: #000;width: 100%;position: relative;";
                }
                ?>
                <div class="videos-list__item-thumb" style="<?php echo $videos_list__item_thumb_style; ?>">
                    <?php
                    if ($row['Document']['published'] == '1' && $transcoding_status != 5):
                        $seconds = $row['Document']['duration'] % 60;
                        $minutes = ($row['Document']['duration'] / 60) % 60;
                        $hours = gmdate("H", $row['Document']['duration']);
                        ?>
                        <a id="processing-message-<?php echo $row['Document']['id']; ?>"
                           href="<?php echo $this->base . '/workspace_video/home/' . $row['AccountFolder']['account_folder_id'].'/'.$row['Document']['id']; ?>"
                           title="<?php echo $row['afd']['title'] ?>">
                            <?php
                            $thumbnail_image_path = $document_files_array['thumbnail'];

                            echo $this->Html->image($thumbnail_image_path, array('id' => 'img_' . $row['Document']['id']));
                            ?>

                            <div class="play-icon"
                                 style="background-size: 50px;height: 50px;margin: -60px 0 0 -25px;width: 50px;"></div>
                            <?php if ($transcoding_status != 2): ?>
                                <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;"> <?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                            <?php endif; ?>
                        </a>
                    <?php else: ?>

                        <?php
                        if ($row['Document']['doc_type'] == 3) {
                            ?>

                            <a id="processing-message-<?php echo $row['Document']['id']; ?>"
                               href="<?php echo $this->base . '/workspace_video/video_observation/' . $row['AccountFolder']['account_folder_id'].'/'.$row['Document']['id']; ?>"
                               title="<?php echo $row['afd']['title'] ?>">

                                <?php if ($row['Document']['encoder_status'] == 'Error' || $transcoding_status == 5): ?>
                                    <div class="video-unpublished ">
                                            <span class="huddles-unpublished video_custom_message"
                                                  style="padding: 16% 17px !important;">
                                                <?php echo $language_based_content['video_failed_to_process_successfully']; ?> <a
                                                        href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"
                                                        style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                            </span>
                                    </div>
                                <?php else : ?>
                                <?php if ($row['Document']['is_processed'] == 5 || $row['Document']['is_processed'] == 1) : ?>

                                    <div class="video-unpublished"><span class="huddles-unpublished ">
                                                    <?php
                                                    if ($row['ual']['environment_type'] == 1) {
                                                        echo $alert_messages['you_are_recording_live_from_ios_device'];
                                                    } else if ($row['ual']['environment_type'] == 3) {
                                                        echo $alert_messages['you_are_recording_live_from_android'];
                                                    } else {
                                                        echo $alert_messages['you_are_recording_live'];
                                                    }
                                                    ?>
                                            <script type="text/javascript">

                                                        observation_inreview.push(<?php echo $row['Document']['id']; ?>);
                                                    </script>
                                                </span></div>

                                <?php else: ?>

                                <?php
                                if ($row['Document']['doc_type'] == 3 && $row['Document']['is_processed'] == 4 && $row['Document']['published'] == 0 && $row['Document']['upload_status'] != 'uploaded' && $row['Document']['upload_progress'] < 100) {
                                ?>
                                    <div id="uploading-box-<?php echo $row['Document']['id'] ?>" class="vnb"
                                         style="position: absolute;background: #3e7554; z-index: 2;width:<?php echo $row['Document']['upload_progress'] ?>%; height: 146px;text-align: center; color: #fff; padding-top: 65px;opacity:0.5;">
                                        &nbsp;
                                    </div><span
                                            style="width: 110px;color: #fff;position: absolute;left: 58px;top: 40%;text-align: center;text-transform: uppercase;"><?php echo $row['Document']['upload_progress'] ?>
                                        % <?php echo $row['Document']['upload_status'] ?></span>
                                    <script type="text/javascript">

                                        observation_inreview.push(<?php echo $row['Document']['id']; ?>);
                                    </script>
                                <?php
                                } else {
                                ?>
                                    <div class="video-unpublished ">
                                                    <span class="huddles-unpublished video_custom_message"
                                                          style="padding: 15% 17px !important;">
                                                        <img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                                                             style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;"/><br><?php echo $alert_messages["video_is_currently_processing_alert"]; ?>
                                                    </span>
                                    </div>
                                    <?php
                                }
                                    ?>

                                <?php endif; ?>

                                <?php endif; ?>

                            </a>

                            <?php
                        } else {
                            ?>

                            <a id="processing-message-<?php echo $row['Document']['id']; ?>"
                               href="<?php echo $this->base . '/workspace_video/home/' . $row['AccountFolder']['account_folder_id'].'/'.$row['Document']['id']; ?>"
                               title="<?php echo $row['afd']['title'] ?>">
                                <div class="video-unpublished ">
                                        <span class="huddles-unpublished" style="padding: 15% 17px !important;">
                                            <?php if ($row['Document']['encoder_status'] == 'Error' || $transcoding_status == 5): ?>
                                                <?php echo $language_based_content['video_failed_to_process_successfully']; ?> <a
                                                        href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"
                                                        style="color: blue;text-decoration: underline;position: absolute;top: 92px;left: 32px;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.
                                            <?php else : ?>
                                                <img src="<?php echo $this->webroot . 'img/loading.gif' ?>"
                                                     style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;"/>
                                                <br><?php echo $alert_messages["Your_video_is_currently_processing"]; ?>
                                            <?php endif; ?>
                                        </span>
                                </div>
                            </a>

                            <?php
                        }
                        ?>

                    <?php endif; ?>
                </div>
                <div class="videos-list__item-aside">
                    <!--                <div class="videos-list__item-title">
                                            <a class="wrap" id="vide-title-<?php //echo $row['Document']['id'];                               ?>" href="<?php //echo $this->base . '/MyFiles/view/1/' . $row['AccountFolder']['account_folder_id'];                              ?>" title="<?php //echo $row['afd']['title']                             ?>" ><?php //echo (strlen($row['afd']['title']) > 52 ? substr($row['afd']['title'], 0, 52) . "..." : $row['afd']['title'])                              ?></a>
                                        </div>-->
                    <div id="input-title-<?php echo $row['afd']['id']; ?>" style="display: none;height:35px;">
                        <input id="input-field-<?php echo $row['afd']['id']; ?>" type="text" name="title" value=""
                               required="required" style="margin-right: 5px;width:150px; padding:6px 4px;"/>
                        <a onclick="return update_title_grid(<?php echo $row['afd']['id']; ?>)"
                           style="cursor: pointer"><?php
                            $chimg = $chimgSave;
                            echo $this->Html->image($chimg);
                            ?></a>
                        <a onclick="return cancel(<?php echo $row['afd']['id']; ?>)" style="cursor: pointer"><?php
                            $chimg = $chimgCancel;
                            echo $this->Html->image($chimg);
                            ?></a>
                    </div>
                    <div id="videos-title-<?php echo $row['afd']['id']; ?>" class="videos-title cursor-pointer"
                         data-title="<?php echo $row['afd']['title']; ?>" style="width:200px;">
                        <div class="videos-list__item-title">
                            <a class="wrap wrap2" id="vide-title-<?php echo $row['Document']['id']; ?>"
                               title="<?php echo $row['afd']['title'] ?>"><?php echo(strlen($row['afd']['title']) > 35 ? mb_substr($row['afd']['title'], 0, 35) . "..." : $row['afd']['title']) ?></a>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                    <div class="videos-list__item-author"><?php echo $language_based_content['by_videos_workspace']; ?> <a href="#"
                                                                title="<?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?>"><?php echo $row['User']['first_name'] . " " . $row['User']['last_name'] ?></a>
                    </div>
                    <div class="videos-list__item-added">
                        <?php echo $language_based_content['uploaded_videos_workspace']; ?> <?php if($_SESSION['LANG'] == 'es'){ echo $this->Custom->SpanishDate(strtotime($row['Document']['created_date']) , 'most_common' );  } else { echo date('M d, Y', strtotime($row['Document']['created_date']));} ?>
                        <div class="clear"></div>
                        <div style="float:left;    position: relative;top: 6px; padding-bottom: 4px;color: #1873bd;">

                                <span>
                                    <img src="<?php echo $this->webroot . 'img/comment_numbers.png'; ?>"> <?php echo $this->Custom->get_video_comment_numbers($row['Document']['id']); ?>
                                </span>

                            <span>
                                    <img src="<?php echo $this->webroot . 'img/attachment_numbers.png'; ?>"> <?php echo $this->Custom->get_video_attachment_numbers($row['Document']['id']); ?>
                                </span>

                        </div>
                        <div style="float: right;    margin-right: -10px;margin-top: -7px;    position: relative; top: 4px;">
                            <?php if ($row['Document']['published'] == '1'): ?>

                                <a style="float: left;"
                                   href="<?php echo $this->webroot . 'MyFiles/download/' . $row['Document']['id'] ?>">
                                    <img alt="Download" class="right smargin-right"
                                         style="height: 28px;margin-top: -2px;" rel="tooltip"
                                         src="<?php echo $chimgDownload; ?>" title="<?php echo $language_based_content['download_videos_workspace']; ?>"/>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </li>
        <?php endforeach; ?>

            <script type="text/javascript">

                $(document).ready(function () {

                    $('.regen-thumbnail-image').click(function () {

                        var r = confirm('Do you want to Regenerate Thumbnail?');

                        if (r == true) {

                            var document_id = $(this).attr('document_id');
                            var huddle_id = $(this).attr('huddle_id');

                            $.ajax({
                                type: 'POST',
                                url: home_url + '/Huddles/regenerate_thumbnail/' + document_id + '/' + huddle_id,
                                success: function (res) {

                                    alert('<?php echo $alert_messages['thumbnail_generated_successfully']; ?>');

                                    d = new Date();
                                    var src_image = $("#img_" + document_id).attr("src");

                                    $("#img_" + document_id).attr("src", src_image + "&dd=" + d.getTime());

                                },
                                errors: function (response) {
                                    alert('<?php echo $alert_messages['unable_to_generate_thumbnail']; ?>');
                                }
                            });

                        }

                    });

                });

            </script>

        <?php else: ?>
            <li class="videos-list__item_noitem">
                <div style="margin-left: 10px;"><?php echo $message; ?></div>
            </li>
        <?php endif; ?>
    </ul>

    <!--</div>-->

<?php endif; ?>

<script type="text/javascript">
    function update_title_grid($document_id) {
        var $value = '#input-field-' + $document_id;
        var $video_title = "#videos-title-" + $document_id;
        var $title_block = "#input-title-" + $document_id;
        var $newText = $($value).val();
        if ($newText == '') {
            $($value).css('border', '1px red solid');
            return false;
        }
        var $nexText = $.trim($newText);
        $.ajax({
            url: home_url + "/MyFiles/editTitle",
            data: {title: $nexText, document_id: $document_id},
            type: 'POST',
            success: function (response) {
                var response_edit = response;
                if (response_edit.length > 35) {
                    response_edit = response_edit.mb_substr(0, 35) + '...';
                }
                $($video_title).attr('data-title', response);
                $($video_title).html('<div class="videos-list__item-title"><a class="wrap wrap2">' + response_edit + '</a></div>');
                $($value).val("");
                $($title_block).css('display', 'none');
                $($video_title).css('display', 'block');
            },
            error: function () {
                alert("<?php echo $alert_messages['network_error_occured']; ?>");
            }
        });
    }

    $(document).on('click', '.huddle-contents', function (e) {
        $.ajax({
            type: 'POST',
            data: {
                type: 'get_video_comments',
                sort: $('#myFilesVideoSort').val(),
                limit: "",
                page: ""
            },
            dataType: 'json',
            url: home_url + '/MyFiles/getCopyHuddlesList',
            success: function (response) {
                var list = $('.list-container-box');
                if (response.html != '') {
                    list.html(response.html);
                    gApp.updateCheckboxEvent($('.list-container-box .ui-checkbox input'));
                } else {
                    list.html("No Huddles found in your account.");

                }
            },
            errors: function (response) {
                console.log(response);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
                //location.reload(true);
            }
        });
    });

</script>
