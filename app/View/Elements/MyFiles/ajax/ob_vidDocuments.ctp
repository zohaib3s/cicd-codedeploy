<div class="heading">
    <h3>Documents</h3>
    <a href="javascript:ob_OpenFilePicker('doc');" class="upload">Upload</a>
</div>
<pre>
</pre>
<ul class="video-docs scroll">
    <?php if (isset($vidDocuments) && count($vidDocuments) > 0): ?>
    <?php foreach ($vidDocuments as $row): ?>
        <li id="li_video_doc_<?php echo $row['Document']['id']; ?>" style="width: 100%;">
            <form id="obs-delete-video-document<?php echo $row['Document']['id']; ?>" data-async accept-charset="UTF-8"
                  action="<?php echo $this->base . '/Huddles/deleteOservationDocument/' . $row['Document']['id'] . '/' . $row['afd']['account_folder_id']; ?>">
                <a rel="nofollow" data-confirm="Are you sure you want to delete this Resource?"
                   id="delete-main-comments" class="del"
                   href="javascript:$('#obs-delete-video-document<?php echo $row['Document']['id']; ?>').submit()">&nbsp;</a>
            </form>
            <?php $href = 'href="' . $this->base . '/Huddles/download/' . $row['Document']['id'] . '"'; ?>
            <a <?php echo $href; ?> class="wrap">
                <span class="<?php echo $this->Custom->getIcons($row['Document']['url']); ?>">Icon</span>
                <?php echo $row['afd']['title'] ?>
            </a>
        </li>
    <?php endforeach; ?>
    <?php else: ?>
    <li style="clear:left;">No Attachments have been added.</li>
    <?php endif; ?>
</ul>
