<div class="tab-content <?php echo $tab == 2 ? 'tab-active' : 'tab-hidden'; ?>" id="tabbox2">
    <form id='myFilesDoc' action="<?php echo $this->base . '/MyFiles/downloadZip/2' ?>" method="post">
        <input type="hidden" name="download_as_zip_docs" class="download_as_zip_docs" value="">
    </form>
    <form id="myFilesDocDelete" action="<?php echo $this->base . '/MyFiles/deleteFiles/2' ?>" method="post">
        <input type="hidden" id="accountFolderIDocsIds" name="accountFolderIds" value=""/>
    </form>
    <div class="tab-header">
        <div class="tab-header__right myfiles__right">
            <input class="btn-search" id="btnSearchMyFileDocuments" type="button" value="">
            <input class="text-input" id="txtSearchMyFilesDocuments" type="text" value=""
                   placeholder="Search Resources...">
            <span id="My-File-Doc-Clear" style="display: none;right: 255px;" class="my-file-clear-btn">X</span>
            <div class="select select--alt right lmargin-left">
                <select id="cmbMyFileDocsSort" name="upload-date">
                    <option value="Document.created_date DESC">Date Uploaded</option>
                    <option value="afd.title ASC">Resource Title</option>
                </select>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
    <div class="docs-list">
        <div class="docs-list__head">
            <a id="doc_checkbox" <?php if (isset($doc_bool) && $doc_bool == 0) {
                echo 'style="display:none;"';
            } ?>><label class="ui-checkbox" style="margin-left: -30px;"><input type="checkbox" class="selectall-docs"/></label></a>
            <a style="cursor: pointer" onclick="return MyFiles.deleteFiles()" id="delete-account-folder"
               class="icon-delete blue-link delete js-file-delete">Delete</a>
            <a style="cursor: pointer" id="js-file-doc-download" class="js-file-download icon-download"
               onclick="return MyFiles.downloadFiles('document')">Download</a>
            <a style="cursor: pointer" onclick="return MyFiles.rename_doc();"
               class="js-file-rename icon-rename">Rename</a>
            <a onclick="return copy2();" id="copy-docs-files" class="docs-huddle-contents js-file-copy icon-copy"
               data-reveal-id="" href="#">Copy</a>
        </div>
        <table class="docs-list__table my-file-documents-list MyWorkSpace">
            <?php echo $docs_html ?>
        </table>
    </div>
    <div class="load_more">
        <a class="btn btn-green" id="doc-load-more" style="display:none;">Load more Resources...</a>
    </div>
</div>
