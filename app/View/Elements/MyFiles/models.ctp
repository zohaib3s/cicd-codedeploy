<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$user_id = $user_current_account['User']['id'];
$loggedInUserRole = '';
?>
<style>
    ::-webkit-input-placeholder {
        color: #c5c5c5;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: #c5c5c5;
    }

    ::-moz-placeholder { /* Firefox 19+ */
        color: #c5c5c5;
    }

    :-ms-input-placeholder {
        color: #c5c5c5;
    }

    .copy_video_tabs {
        margin: 0 20px;
        margin-bottom: 55px;
    }

    .copy_video_tabs section {
        display: none;
        padding: 10px;
        border: 1px solid #ddd;
        background: #fff;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }

    .copy_video_tabs input {
        display: none;
    }

    .copy_video_tabs .btn-green {
        display: inline-block;
        position: absolute;
        bottom: 0px;
        left: -10px;
    }

    .filter_data_box {
        position: relative;
        margin-top: 12px;
    }

    .tabs_box {
        display: inline-block;
        margin: 0 0 -1px;
        padding: 15px 12px;
        font-weight: 600;
        text-align: center;
        color: #bbb;
        border: 1px solid transparent;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size: 13px;

    }

    .copy_btn_box {
        position: absolute;
        bottom: -54px;
        width: 103%;
        z-index: 20;
    }

    .tabs_box img {
        width: 18px;
        height: 18px;
        vertical-align: middle;
        margin-right: 5px;
    }

    .tabs_box:hover {
        color: #888;
        cursor: pointer;
    }

    .copy_video_tabs input:checked + .tabs_box {
        color: #555;
        border: 1px solid #ddd;
        border-top: 2px solid <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;
        border-bottom: 1px solid #fff;
        background: #fff;
    }

    .copy_video_tabs #tab1:checked ~ #content1,
    .copy_video_tabs #tab2:checked ~ #content2,
    .copy_video_tabs #tab3:checked ~ #content3,
    .copy_video_tabs #tab4:checked ~ #content4,
    .copy_video_tabs #tab5:checked ~ #content5,
    .copy_video_tabs #tab6:checked ~ #content6,
    .copy_video_tabs #tab7:checked ~ #content7,
    .copy_video_tabs #tab8:checked ~ #content8,
    .copy_video_tabs #tab9:checked ~ #content9 {
        display: block;
    }

    .copy_filter {
        box-sizing: border-box;
        padding: 8px;
        border: solid 1px #ececec;
        font-size: 13px;
        width: 100%;
        display: block !important;
        outline: none;
        -webkit-box-shadow: inset 0px 10px 10px -11px rgba(0, 0, 0, 0.39);
        -moz-box-shadow: inset 0px 10px 10px -11px rgba(0, 0, 0, 0.39);
        box-shadow: inset 0px 10px 10px -11px rgba(0, 0, 0, 0.39);
    }

    .filter_data_box ul {
        height: 150px;
        overflow-y: auto;
        overflow-x: hidden;
        margin-top: 10px;
    }

    .filter_data_box .inset-area {
        border: 0px;
        border-radius: 0px;
        box-shadow: none;
        padding: 0px;
        margin: 0px;
    }

    .filter_data_box form {
        padding: 0px;
    }

    .filter_data_box .files-list > li ~ li {
        margin-top: 12px;
    }

    .filter_data_box .files-list li {
        padding-left: 5px;
    }

    @media screen and (max-width: 650px) {
        .tabs_box {
            font-size: 0;
        }

        .copy_video_tabs .tabs_box:before {
            margin: 0;
            font-size: 18px;
        }
    }

    .filterform {
        position: relative;
    }

    .copy_link {
        font-size: 15px !important;
        color: #5a80a0 !important;
        font-weight: normal !important;
    }

    .smargin-bottom {
        margin-bottom: 0px !important;
    }

    @media screen and (max-width: 400px) {
        .tabs_box {
            padding: 15px;
        }
    }

    .copy_video_box {
        position: absolute;
        right: 16px;
        bottom: 7px;
        font-size: 14px;
        color: #5a90bf;
    }

    .copy_video_box .ui-checkbox {
        top: 2px;
        width: 15px;
        height: 15px;
    }
</style>

<?php
$get_account_video_permissions = $this->Custom->get_account_video_permissions($user_current_account['users_accounts']['account_id']);
if (!empty($allHuddles)) {
    for ($i = 0; $i < count($allHuddles); $i++) {

        $row = $allHuddles[$i];
        if (!isset($row))
            continue;
        $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
        $userGroups = $AccountFolderGroup->getHuddleGroups($row['AccountFolder']['account_folder_id']);
        $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $userGroups, $user_current_account['User']['id']);
        $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']);


        $get_eval_participant_videos = $this->Custom->get_eval_participant_video_details($row['AccountFolder']['account_folder_id'], $user_id);

        $check_if_eval_huddle = $this->Custom->check_if_eval_huddle($row['AccountFolder']['account_folder_id']);
        $check_if_evaluated_participant = $this->Custom->check_if_evaluated_participant($row['AccountFolder']['account_folder_id'], $user_id);


        $row['AccountFolder']['loggedInUserRole'] = $loggedInUserRole;
        $row['AccountFolder']['isCreator'] = $isCreator;
        $row['AccountFolder']['get_eval_participant_videos'] = $get_eval_participant_videos;
        $row['AccountFolder']['check_if_eval_huddle'] = $check_if_eval_huddle;
        $row['AccountFolder']['check_if_evaluated_participant'] = $check_if_evaluated_participant;

        $allHuddles[$i] = $row;
    }
}
?>
<div id="copyFiles" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom"><?php echo $alert_messages['copy_videos_msg']; ?></h4>
                <a id="copy_video_myfiles" data-dismiss="modal" class="close-reveal-modal btn btn-grey close style2"
                   data-dismiss="modal">×</a>
            </div>
            <div class="copy_video_tabs">
                <input id="tab1" type="radio" name="tabs" checked>
                <label class="tabs_box" for="tab1"><img src="/img/dbi-huddles.png" alt=""> <?php echo $alert_messages['copy_to_huddle_msg']; ?></label>
                <?php if ($user_permissions['UserAccount']['permission_video_library_upload'] == '1'): ?>
                    <?php if ($get_account_video_permissions): ?>
                        <input id="tab2" type="radio" name="tabs">
                        <label class="tabs_box" for="tab2"><img src="/img/dbi-video.png" alt=""> <?php echo $alert_messages['copy_video_library_msg']; ?></label>
                    <?php endif; ?>
                <?php endif; ?>
                <?php /* if (count($all_accounts) > 0): ?>
                  <input id="tab3" type="radio" name="tabs">
                  <?php if (count($all_accounts) == 1): ?>
                  <label class="tabs_box" for="tab3"><img src="/img/copy_to_account.png" alt=""> Copy to Workspace</label>
                  <?php else: ?>
                  <label class="tabs_box" for="tab3"><img src="/img/copy_to_account.png" alt=""> Copy to
                  Account(s)</label>
                  <?php endif; ?>
                  <?php endif; */ ?>

                <?php if (count($all_accounts) > 1): ?>
                    <input id="tab3" type="radio" name="tabs">
                    <label class="tabs_box" for="tab3"><img src="/img/copy_to_account.png" alt=""> <?php echo $alert_messages['copy_to_accounts_msg']; ?></label>
                <?php endif; ?>

                <section id="content1">
                    <!--                <input class="copy_filter" type="text" placeholder="Find Huddles">-->
                    <div id="header-container" class="filterform">
                    </div>
                    <div class="widget-scrollable2">

                        <div class="filter_data_box">
                            <form id="show-huddles" accept-charset="UTF-8"
                                  action="<?php echo $this->base . '/MyFiles/copy/' ?>" enctype="multipart/form-data"
                                  method="post">
                                <input name="document_ids" type="hidden" class="copy-document-ids" value=""/>
                                <input name="tab" type="hidden" class="copy-documents-tab" value=""/>
                                <ul id="list-containers" class="list-container-box inset-area clear-list files-list">
                                    <div style="text-align: center">
                                        <img id="doc_list_loading_gif" style=""
                                             src="<?php echo $this->webroot . 'img/loading.gif' ?>">
                                    </div>
                                </ul>
                                <div class="copy_btn_box">
                                    <div class="copy_video_box">
                                        <label class="ui-checkbox model">
                                            <input name="copy_notes" id="copy_notes" type="checkbox"
                                                   class="copyFiles_checkbox" value="1">
                                        </label>
                                        <label for="copy_notes"><?php echo $alert_messages['copy_video_notes_msg']; ?></label>
                                    </div>

                                    <input class="btn btn-green blocked-btn" disabled="disabled" type="submit"
                                           name="submit" value="<?php echo $alert_messages['copy_msg'] ?>" style="display: none; background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>">
                                    <input class="btn btn-green" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>" type="submit" name="submit" id="<?php ?>"
                                           onclick="return verifyBeforeCopyFiles(this);" value="<?php echo $alert_messages['copy_msg'] ?>">
                                </div>
                            </form>

                        </div>
                    </div>
                </section>

                <section id="content2">
                    <div class="filter_data_box">
                        <form id="show-huddles" accept-charset="UTF-8"
                              action="<?php echo $this->base . '/MyFiles/copy/' ?>" enctype="multipart/form-data"
                              method="post">
                            <input name="document_ids" type="hidden" class="copy-document-ids" value=""/>
                            <input name="tab" type="hidden" class="copy-documents-tab" value=""/>
                            <ul class="inset-area clear-list files-list">
                                <?php if ($user_permissions['UserAccount']['permission_video_library_upload'] == '1'): ?>
                                    <?php if ($get_account_video_permissions): ?>
                                        <li>
                                            <label class="ui-checkbox model">
                                                <input name="account_folder_id[]" id="account_folder_name_-1"
                                                       class="copyFiles_checkbox" type="checkbox" value="-1">
                                            </label>
                                            <label for="account_folder_name_-1"><?php echo $alert_messages['copy_video_library_msg']; ?></label>
                                        </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </ul>
                            <div class="copy_btn_box">
                                <input class="btn btn-green blocked-btn" disabled="disabled" type="submit" name="submit"
                                       value="<?php echo $alert_messages['copy_msg'] ?>" style="display: none;background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>">
                                <input class="btn btn-green" type="submit" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>" name="submit" id="<?php ?>"
                                       onclick="return verifyBeforeCopyFiles(this);" value="<?php echo $alert_messages['copy_msg'] ?>">
                            </div>
                        </form>
                    </div>
                </section>

                <section id="content3">
                    <div id="header-container-accounts" class="filterform">
                    </div>
                    <div class="widget-scrollable2">
                        <div class="filter_data_box">
                            <form id="show-accounts" accept-charset="UTF-8"
                                  action="<?php echo $this->base . '/MyFiles/copytoaccounts/' ?>"
                                  enctype="multipart/form-data" method="post">

                                <input name="document_ids" type="hidden" class="copy-document-ids" value=""/>
                                <ul id="list-containers-accounts"
                                    class="inset-area clear-list files-list"<?php if (isset($all_accounts) && count($all_accounts) > 5): ?><?php endif; ?>>
                                        <?php
                                        foreach ($all_accounts as $account):
                                            ?>
                                        <li>
                                            <label class="ui-checkbox model">
                                                <input name="account_ids[]"
                                                       id="account_id_<?php echo $account['accounts']['account_id']; ?>"
                                                       type="checkbox" class="copyFiles_checkbox"
                                                       value="<?php echo $account['accounts']['account_id']; ?>">
                                            </label>
                                            <label for="account_id_<?php echo $account['accounts']['account_id']; ?>">
                                                <a class="copy_link"><?php echo $account['accounts']['company_name']; ?></a>
                                                - <label style="color: #000000;font-weight: 600;"><?php echo $alert_messages['workspace_alert']; ?></label>
                                            </label>

                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="copy_btn_box">
                                    <div class="copy_video_box">
                                        <label class="ui-checkbox model">
                                            <input name="copy_notes" id="copy_notes" type="checkbox"
                                                   class="copyFiles_checkbox" value="1">
                                        </label>
                                        <label for="copy_notes"><?php echo $alert_messages['copy_video_notes_msg'] ?></label>
                                    </div>
                                    <input class="btn btn-green" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>" type="submit" name="submit" id="<?php ?>"
                                           onclick="return verifyaccountBeforeCopyFiles();" value="<?php echo $alert_messages['copy_msg'] ?>">
                                </div>
                                <script type="text/javascript">
                                    function verifyaccountBeforeCopyFiles() {

                                        count = 0;
                                        var video_lib_selected = false;
                                        $.each($('.copyFiles_checkbox'), function (index, value) {

                                            if ($(this).prop('checked') == true) {
                                                count++;
                                            }

                                        });
                                        if (count == 0) {
                                            alert('<?php echo $alert_messages['please_select_atleast_one_account']; ?>');
                                            return false;
                                        }

                                        return true;
                                    }
                                </script>
                            </form>
                        </div>
                    </div>
                </section>

            </div>
            <script type="text/javascript">
                function verifyBeforeCopyFiles(event) {
                    count = 0;
                    var video_lib_selected = false;
                    $.each($('.copyFiles_checkbox'), function (index, value) {

                        if ($(this).prop('checked') == true) {
                            count++;
                        }

                    });
                    
                    if(count == 1 && $('#copy_notes').prop('checked') == true )
                    {
                        count = 0;  
                    }

                    if (count == 0) {
                        alert('<?php echo $alert_messages['please_select_atleast_one_huddle']; ?>');
                        return false;
                    }
                    $(event).hide();
                    $('.blocked-btn').show();
                    return true;
                }
            </script>
        </div>
    </div>
</div>

<div id="copyFiles_2" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom"><?php echo $alert_messages['copy_videos_msg']; ?></h4>
                <a data-dismiss="modal" class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">×</a>
            </div>
            <div class="copy_video_tabs">
                <input id="tab7" type="radio" name="tabsI" checked>
                <label class="tabs_box" for="tab7"><img src="/img/dbi-huddles.png" alt=""> <?php echo $alert_messages['copy_to_huddle_msg']; ?></label>
                <?php if ($user_permissions['UserAccount']['permission_video_library_upload'] == '1'): ?>
                    <?php if ($get_account_video_permissions): ?>
                        <input id="tab8" type="radio" name="tabsI">
                        <label class="tabs_box" for="tab8"><img src="/img/dbi-video.png" alt=""> <?php echo $alert_messages['copy_video_library_msg']; ?></label>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if (count($all_accounts) > 0): ?>
                    <input id="tab9" type="radio" name="tabsI">
                    <?php if (count($all_accounts) == 1): ?>
                        <label class="tabs_box" for="tab9"><img src="/img/copy_to_account.png" alt=""> <?php echo $alert_messages['copy_to_workspace_msg']; ?></label>
                    <?php else: ?>
                        <label class="tabs_box" for="tab9"><img src="/img/copy_to_account.png" alt=""> <?php echo $alert_messages['copy_to_accounts_msg']; ?></label>
                    <?php endif; ?>
                <?php endif; ?>


                <section id="content7">
                    <div id="header-container_1" class="filterform">

                    </div>
                    <div class="widget-scrollable2">

                        <div class="filter_data_box">
                            <form accept-charset="UTF-8"
                                  action="<?php echo $this->base . '/MyFiles/files_copy/' . $video_id ?>"
                                  enctype="multipart/form-data" method="post">
                                <input name="document_ids" type="hidden" class="copy-document-ids"
                                       value="<?php echo $document_id; ?>"/>
                                <input name="tab" type="hidden" class="copy-documents-tab" value="<?php echo $tab; ?>"/>

                                <ul id="list-container_1"
                                    class="inset-area list-container-box-single clear-list files-list"<?php if (isset($allHuddles) && count($allHuddles) > 5): ?><?php endif; ?>>

                                    <div style="text-align: center">
                                        <img id="doc_list_loading_gif" style=""
                                             src="<?php echo $this->webroot . 'img/loading.gif' ?>">
                                    </div>

                                </ul>
                                <div class="copy_btn_box">
                                    <?php if ($total_comments > 0) { ?>
                                        <div class="copy_video_box">
                                            <label class="ui-checkbox model">
                                                <input name="copy_notes" id="copy_notes" type="checkbox"
                                                       class="copyFiles_checkbox" value="1">
                                            </label>
                                            <label for="copy_notes"><?php echo $alert_messages['copy_video_notes_msg']; ?></label>
                                        </div>
                                    <?php } ?>
                                    <input class="btn btn-green blocked-btn" type="submit" name="submit"
                                           disabled="disabled" value="<?php echo $alert_messages['copy_msg'] ?>"  style="display:none;background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>">
                                    <input class="btn btn-green" type="submit" name="submit" id="<?php ?>"
                                           onclick="return verifyBeforeCopyFiles(this);" value="<?php echo $alert_messages['copy_msg'] ?>" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>">
                                </div>

                            </form>

                        </div>
                    </div>
                </section>

                <section id="content8">
                    <div class="filter_data_box">
                        <form accept-charset="UTF-8"
                              action="<?php echo $this->base . '/MyFiles/files_copy/' . $video_id ?>"
                              enctype="multipart/form-data" method="post">
                            <input name="document_ids" type="hidden" class="copy-document-ids"
                                   value="<?php echo $document_id; ?>"/>
                            <input name="tab" type="hidden" class="copy-documents-tab" value="<?php echo $tab; ?>"/>
                            <ul class="inset-area clear-list files-list">
                                <?php if ($user_permissions['UserAccount']['permission_video_library_upload'] == '1'): ?>
                                    <?php if ($get_account_video_permissions): ?>
                                        <li>
                                            <label class="ui-checkbox model">
                                                <input name="account_folder_id[]" id="account_folder_name_-1"
                                                       class="copyFiles_checkbox" type="checkbox" value="-1">
                                            </label>
                                            <label for="account_folder_name_-1"><?php echo $alert_messages['copy_video_library_msg']; ?></label>
                                        </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </ul>
                            <div class="copy_btn_box">
                                <input class="btn btn-green blocked-btn" disabled="disabled" type="submit" name="submit"
                                       value="<?php echo $alert_messages['copy_msg'] ?>" style="display:none;background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>">
                                <input class="btn btn-green" type="submit" name="submit" id="<?php ?>"
                                       onclick="return verifyBeforeCopyFiles(this);" value="<?php echo $alert_messages['copy_msg'] ?>" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>">
                            </div>
                        </form>
                    </div>
                </section>

                <section id="content9">
                    <div id="header-container-accounts" class="filterform">
                    </div>
                    <div class="widget-scrollable2">
                        <div class="filter_data_box">
                            <form id="show-accounts" accept-charset="UTF-8"
                                  action="<?php echo $this->base . '/MyFiles/copytoaccounts/' ?>"
                                  enctype="multipart/form-data" method="post">
                                <input name="document_ids" type="hidden" class="copy-document-ids"
                                       value="<?php echo $document_id; ?>"/>
                                <input name="tab" type="hidden" class="copy-documents-tab" value="<?php echo $tab; ?>"/>
                                <ul id="list-containers-accounts"
                                    class="inset-area clear-list files-list"<?php if (isset($all_accounts) && count($all_accounts) > 5): ?><?php endif; ?>>
                                        <?php
                                        foreach ($all_accounts as $account):
                                            ?>
                                        <li>
                                            <label class="ui-checkbox model">
                                                <input name="account_ids[]"
                                                       id="account_id_<?php echo $account['accounts']['account_id']; ?>"
                                                       type="checkbox" class="copyFiles_checkbox"
                                                       value="<?php echo $account['accounts']['account_id']; ?>">
                                            </label>
                                            <label for="account_id_<?php echo $account['accounts']['account_id']; ?>">
                                                <a class="copy_link"><?php echo $account['accounts']['company_name']; ?></a>
                                                - <label style="color: #000000;font-weight: 600;"><?php echo $alert_messages['workspace_alert']; ?></label>
                                            </label>

                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="copy_btn_box">
                                    <?php if ($total_comments > 0) { ?>
                                        <div class="copy_video_box">
                                            <label class="ui-checkbox model">
                                                <input name="copy_notes" id="copy_notes" type="checkbox"
                                                       class="copyFiles_checkbox" value="1">
                                            </label>
                                            <label for="copy_notes"><?php echo $alert_messages['copy_video_notes_msg'] ?></label>
                                        </div>
                                    <?php } ?>
                                    <input class="btn btn-green" type="submit" name="submit" id="<?php ?>"
                                           onclick="return verifyaccountBeforeCopyFiles();" value="<?php echo $alert_messages['copy_msg'] ?>" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>">
                                </div>
                                <script type="text/javascript">
                                    function verifyaccountBeforeCopyFiles() {

                                        count = 0;
                                        var video_lib_selected = false;
                                        $.each($('.copyFiles_checkbox'), function (index, value) {

                                            if ($(this).prop('checked') == true) {
                                                count++;
                                            }

                                        });
                                        if (count == 0) {
                                            alert('<?php echo $alert_messages['please_select_atleast_one_account']; ?>');
                                            return false;
                                        }

                                        return true;
                                    }
                                </script>
                            </form>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
</div>

<div id="copyDocFiles" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom"><?php echo $alert_messages['copy_resource']; ?></h4>
                <a data-dismiss="modal" class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">×</a>
            </div>
            <div class="copy_video_tabs">
                <input id="tab4" type="radio" name="tabsD" checked="checked">
                <label class="tabs_box" for="tab4"><img src="/img/dbi-huddles.png" alt=""> <?php echo $alert_messages['copy_to_huddle_msg']; ?></label>
                <?php if (count($all_accounts) > 0): ?>
                    <input id="tab5" type="radio" name="tabsD">
                    <?php if (count($all_accounts) == 1): ?>
                        <label class="tabs_box" for="tab5"><img src="/img/copy_to_account.png" alt=""> <?php echo $alert_messages['copy_to_workspace_msg']; ?></label>
                    <?php else: ?>
                        <label class="tabs_box" for="tab5"><img src="/img/copy_to_account.png" alt=""> <?php echo $alert_messages['copy_to_accounts_msg']; ?></label>

                    <?php endif; ?>
                <?php endif; ?>

                <section id="content4">
                    <!--                <input class="copy_filter" type="text" placeholder="Find Huddles">-->
                    <div id="header-container-doc" class="filterform">
                    </div>
                    <div class="widget-scrollable2">
                        <div class="filter_data_box">
                            <form accept-charset="UTF-8" action="<?php echo $this->base . '/MyFiles/copy/' ?>"
                                  enctype="multipart/form-data" method="post">
                                <input name="document_ids" type="hidden" class="copy-document-ids" value=""/>
                                <input name="tab" type="hidden" class="copy-documents-tab" value=""/>
                                <ul id="list-containers-doc"
                                    class="list-containers-docs-box inset-area clear-list files-list"<?php if (isset($allHuddles) && count($allHuddles) > 5): ?> style="overflow-y: scroll; height: 148px;" <?php endif; ?>>
                                    <div style="text-align: center">
                                        <img id="doc_list_loading_gif" style=""
                                             src="<?php echo $this->webroot . 'img/loading.gif' ?>">
                                    </div>

                                </ul>
                                <div class="copy_btn_box">
                                    <input class="btn btn-green" onclick="return verifyaccountBeforeCopyFiles();" type="submit" name="submit" id="<?php ?>" value="<?php echo $alert_messages['copy_msg'] ?>" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>">
                                </div>
                            </form>
                        </div>

                    </div>
                </section>

                <section id="content5">
                    <!--                <input class="copy_filter" type="text" placeholder="Find Accounts">-->
                    <div id="header-container-doc-accounts" class="filterform">
                    </div>
                    <div class="widget-scrollable2">
                        <div class="filter_data_box">
                            <form id="show-accounts" accept-charset="UTF-8"
                                  action="<?php echo $this->base . '/MyFiles/copytoaccounts/' ?>"
                                  enctype="multipart/form-data" method="post">
                                <input name="tab" type="hidden" class="copy-documents-tab" value=""/>
                                <input name="document_ids" type="hidden" class="copy-document-ids" value=""/>
                                <ul id="list-containers-doc-accounts"
                                    class="inset-area clear-list files-list"<?php if (isset($all_accounts) && count($all_accounts) > 5): ?><?php endif; ?>>
                                        <?php
                                        foreach ($all_accounts as $account):
                                            ?>
                                        <li>
                                            <label class="ui-checkbox model">
                                                <input name="account_ids[]"
                                                       id="account_id_<?php echo $account['accounts']['account_id']; ?>"
                                                       type="checkbox" class="copyFiles_checkbox"
                                                       value="<?php echo $account['accounts']['account_id']; ?>">
                                            </label>
                                            <label for="account_id_<?php echo $account['accounts']['account_id']; ?>">
                                                <a class="copy_link"><?php echo $account['accounts']['company_name']; ?> </a>-
                                                <label style="color: #000000;font-weight: 600;"><?php echo $alert_messages['workspace_alert']; ?></label>
                                            </label>

                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                                <div class="copy_btn_box">
                                    <input class="btn btn-green" type="submit" name="submit" id="<?php ?>"
                                           onclick="return verifyaccountBeforeCopyFiles();" value="<?php echo $alert_messages['copy_msg'] ?>" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>">
                                </div>
                                <script type="text/javascript">
                                    function verifyaccountBeforeCopyFiles() {

                                        count = 0;
                                        var video_lib_selected = false;
                                        $.each($('.copyFiles_checkbox'), function (index, value) {

                                            if ($(this).prop('checked') == true) {
                                                count++;
                                            }

                                        });
                                        if (count == 0) {
                                            alert('<?php echo $alert_messages['please_select_atleast_one_account']; ?>');
                                            return false;
                                        }

                                        return true;
                                    }
                                </script>
                            </form>
                        </div>
                    </div>
                </section>


            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery.expr[':'].Contains = function (a, i, m) {
        return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    function listFilter(header, list, btncross, inputfilter, srchPHolder, emptySrch) {

        var form = $("<div>").attr({"class": "filterform"}),
                input = $("<input id='" + inputfilter + "'  placeholder='" + srchPHolder + "'><div  id='" + btncross + "'  type='text' style=' width: 10px;  position: absolute;     right: 9px; top: 7px; display:none;cursor: pointer; '>X</div>").attr({
            "class": "filterinput",
            "type": "text"
        });
        $(form).append(input).appendTo(header);
        $('.filterform input').css({'width': '100%', 'display': 'block'});
        $(input).change(function () {
            var filter = $(this).val();
            $('#liNodataFound').remove();
            if (filter) {
                $search_count = $(list).find("a:Contains(" + filter + ")").length;
                if ($search_count > 4) {
                    $('div.thumb').css('top', '0px');
                    $('div.overview').css('top', '0px');
                    $('.scrollbar').css('display', 'block');
                } else {
                    $('div.thumb').css('top', '0px');
                    $('div.overview').css('top', '0px');
                    $('.scrollbar').css('display', 'none');
                }
                $(list).find("a:not(:Contains(" + filter + "))").parent().parent().slideUp(400, function () {
                    //$('.widget-scrollable2').tinyscrollbar_update();
                });
                $(list).find("a:Contains(" + filter + ")").parent().parent().slideDown(400, function () {
                    //$('.widget-scrollable2').tinyscrollbar_update();
                });
                jQuery("#" + btncross).css('display', 'block');
                if ($(list).find("a:Contains(" + filter + ")").length == 0) {
                    $('#liNodataFound' + btncross).remove();
                    $(list).append('<li id="liNodataFound' + btncross + '">' + emptySrch + '</li>');
                } else {
                    $('#liNodataFound' + btncross).remove();
                }

            } else {
                jQuery("#" + btncross).css('display', 'none');
                $('.scrollbar').css('display', 'block');
                $(list).find("li").slideDown(400, function () {
                    //$('.widget-scrollable2').tinyscrollbar_update();
                });

            }
            return false;
        }).keyup(function () {
            $(this).change();
        });

        $("#" + btncross).click(function (e) {
            jQuery("#" + btncross).css('display', 'none');
            $('div.thumb').css('top', '0px');
            $('div.overview').css('top', '0px');
            $('.scrollbar').css('display', 'block');
            jQuery('#' + inputfilter).val('');
            $('#liNodataFound' + btncross).remove();
            $(list).find("li").slideDown(400, function () {
                //$('.widget-scrollable2').tinyscrollbar_update();
            });
        })
    }

    $(document).ready(function () {
        listFilter($("#header-container"), $("#list-containers"), 'cancel-btn', 'input-filter', '<?php echo $alert_messages['search_huddles_copy_modal']; ?>', '<?php echo $alert_messages['no_huddles_match_this_search']; ?>');
        listFilter($("#header-container_1"), $("#list-container_1"), 'cancel-btn', 'input-filter', '<?php echo $alert_messages['search_huddles_copy_modal']; ?>', '<?php echo $alert_messages['no_huddles_match_this_search']; ?>');
        listFilter($("#header-container-accounts"), $("#list-containers-accounts"), 'cancel-btn2', 'input-filter1', '<?php echo $alert_messages['search_accounts_msg']; ?>', '<?php echo $alert_messages['no_account_match_the_search']; ?>');
        listFilter($("#header-container-doc"), $("#list-containers-doc"), 'cancel-btn-doc', 'input-filter2', '<?php echo $alert_messages['search_huddles_copy_modal']; ?>', '<?php echo $alert_messages['no_huddles_match_this_search']; ?>');
        listFilter($("#header-container-doc-accounts"), $("#list-containers-doc-accounts"), 'cancel-btn-doc-account', 'input-filter3', '<?php echo $alert_messages['search_accounts_msg']; ?>', '<?php echo $alert_messages['no_account_match_the_search']; ?>');
    });
</script>
<script>
$('[data-dismiss=modal]').on('click', function (e) {
    $('#cancel-btn').click();
});
$(document).click(function(e) {
    if (!$(e.target).closest('.modal').length) {
         $('#cancel-btn').click();
    }
});
</script>