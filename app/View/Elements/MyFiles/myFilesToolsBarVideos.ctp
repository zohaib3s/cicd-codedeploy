<div class="tab-header">
    <div class="tab-header__right myfiles__right" style="width: 100%;">
<!--        <strong id="huddle_video_title_strong">Videos (<span id="myFileVideosCount"><?php //echo $total_videos; ?></span>)</strong>-->
        <div style="    position: relative;width: 630px;float: right;">
            <input class="btn-search" id="btnMyFileSearchVideo" type="button" value="">
            <input class="text-input" id="txtMyFilesSearchVideos" type="text" value="" placeholder="<?php echo $language_based_content['search_videos_videos_workspace']; ?>">
            <span id="My-File-Video-Clear" style="display: none;right: 350px;top:3px;"
                  class="my-file-clear-btn">X</span>
            <input type="hidden" name="download_as_zip_docs" id="download_as_zip_docs" value="">
            <div id="view" class="mmargin-right right" style="margin-top: 2px;padding-left: 100px; float: left;">
                <a href="<?php echo $this->base . '/MyFiles/index/list' ?>"
                   class="icon-list <?php if ($files_mode == 'list') {
                       echo 'active';
                   } ?>" rel="tooltip" title="<?php echo $language_based_content['list_view_workspace']; ?>"></a>
                <a href="<?php echo $this->base . '/MyFiles/index/grid' ?>"
                   class="icon-grid <?php if ($files_mode != 'list') {
                       echo 'active';
                   } ?>" rel="tooltip" title="<?php echo $language_based_content['grid_view_workspace']; ?>"></a>
            </div>
            <div class="select select--alt right lmargin-left" style="margin-top:-21px;">
                <select id="myFilesVideoSort" name="upload-date">
                    <option value="Document.created_date DESC"><?php echo $language_based_content['date_uploaded_videos_workspace']; ?></option>
                    <option value="afd.title ASC"><?php echo $language_based_content['video_title_videos_workspace']; ?></option>
                </select>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
<form id="myFilesVideoDelete" action="<?php echo $this->base . '/MyFiles/deleteFiles/1' ?>" method="post">
    <input type="hidden" id="accountFolderIds" name="accountFolderIds" value=""/>
</form>
<form id='myFiles' action="<?php echo $this->base . '/MyFiles/downloadZip/1' ?>" method="post">
    <input type="hidden" name="download_as_zip_docs" class="download_as_zip_docs" value="">
</form>
