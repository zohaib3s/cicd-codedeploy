<?php
$amazon_base_url = Configure::read('amazon_base_url');
?>
<div id="addVideoModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom">Upload Video</h4>
                <a class="close-reveal-modal close style2" data-dismiss="modal">&#215;</a>
            </div>
            <form accept-charset="UTF-8" action="/videos" class="form-horizontal" enctype="multipart/form-data" id="new_video" method="post">
                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                <input id="video_video_huddle_id" name="video[video_huddle_id]" type="hidden" value="46" />
                <input id="video_user_id" name="video[user_id]" type="hidden" value="199" />
                <div class="input-group">
                    <input class="size-medium" id="video_title" name="video[title]" placeholder="Title" size="30" type="text" />
                </div>
                <div class="input-group">
                    <input class="size-medium" id="video_video" name="video[video]" placeholder="Video" type="file" />
                </div>
                <p>
                    <input class="btn btn-green" name="commit" type="submit" value="Upload Video" />
                </p>
            </form>
        </div>
    </div>
</div>

<div id="addDocumentModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom">Upload Document</h4>
                <a class="close-reveal-modal close style2">&#215;</a>
            </div>
            <form accept-charset="UTF-8" action="<?php echo $this->base . '/documents/upload/' . $video_huddle_id . '/' . $video_id ?>" class="form-horizontal" enctype="multipart/form-data" id="new_document" method="post">
                <?php
                echo $this->Form->input('video_huddle_id', array('value' => $video_huddle_id, 'type' => 'hidden', 'id' => 'document_video_huddle_id'));
                echo $this->Form->input('user_id', array('value' => $user_id, 'type' => 'hidden', 'id' => 'document_user_id'));
                ?>
                <div class="input-group">
                    <input class="size-medium" id="document_title" name="data[title]" placeholder="Title" size="30" type="text" required />
                </div>
                <div class="input-group">
                    <input class="size-medium" id="document_document" name="data[file]" placeholder="Document" type="file" required/>
                </div>
                <p>
                    <input class="btn btn-green" name="commit" type="submit" value="Upload Document" />
                </p>
            </form>
        </div>
    </div>
</div>

<?php if (isset($video_id) && $video_id != ''): //echo"left : " . $this->Session->read('left'); ?>
    <div class="video-list open">
        <a class="video-list__toggle" href="#"></a>
        <a class="video-list__left" href="#"></a>
        <div class="video-list__items-wrap">
            <ul class="video-list__items" style="left:<?php echo $this->Session->read('left'); ?>">
                <?php if ($videos): ?>
                    <?php
                    $vol = 1;

                    foreach ($videos as $video):
                        $selected = '';
                        if ($video['Document']['id'] == $video_id) {
                            $selected = 'style="border: 1px solid red;"';
                        }

                        $videoID = $video['Document']['id'];
                        $videoFilePath = pathinfo($video['Document']['url']);
                        $videoFileName = $videoFilePath['filename'];

                        $document_files_array = $this->Custom->get_document_url($video['Document']);
                        if (empty($document_files_array['url'])) {
                            $video['Document']['published'] = 0;
                            $document_files_array['url'] = $video['Document']['original_file_name'];
                            $video['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $video['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        }
                        ?>

                        <li id='items-<?php echo $video['Document']['id']; ?>' data-url='<?php echo $this->base . '/' . 'Huddles/view/' . $video_huddle_id . '/1/' . $video['Document']['id'] ?>' class="video-list__item media<?php echo($videoID == $video_id ? ' active' : ''); ?>">
                            <div class="media__img">
                                <span class="video-list__item-num"><?php echo $vol; ?></span>

            <?php
            $video_url = $document_files_array['url'];
            $thumbnail_image_path = $document_files_array['thumbnail'];

            echo $this->Html->image($thumbnail_image_path, array('class' => 'video-list__item-thumb'));
            ?>
                            </div>
                            <div class="media__body">
                                <div class="video-list__item-name"><?php echo $video['afd']['title']; ?></div>
                                <div class="video-list__item-author">By <?php echo $video['User']['first_name'] . " " . $video['User']['last_name'] ?></div>
                            </div>
                        </li>
                        <!--onclick="javascript: window.location = '<?php //echo $this->base . '/' . 'Huddles/view/' . $video_huddle_id . '/1/' . $video['Document']['id']                          ?>'-->

            <?php
            $vol++;
        endforeach;
        ?>
                <?php endif; ?>
                <script type="text/javascript">
                    $(document).ready(function (e) {
                        $('ul.video-list__items li').click(function (e) {
                            $left = $('ul.video-list__items').css('left');
                            $url = $(this).attr('data-url');
                            $.ajax({
                                type: "POST",
                                url: home_url + "/huddles/save_css_attributes",
                                dataType: "html",
                                data: {left: $left},
                                success: function (response) {
                                    window.location = $url;
                                },
                                errors: function (response) {

                                }
                            });
                        });
                    });
                </script>
            </ul>
        </div>
        <a class="video-list__right" href="#"></a>
    </div>
<?php endif; ?>
<a href="#" id="view_browser_notification" style="display: none;" data-toggle="modal" data-target="BrowserMessageModal">browser message</a>
<div id="BrowserMessageModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom">Upgade Your Browser</h4>
                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
            </div>

            <p>You're Using an unsupported browser. Please upgrade your browser to view <?php $this->Custom->get_site_settings('site_title') ?> Application</p>
        </div>
    </div>
</div>
