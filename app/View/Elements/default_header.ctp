<head>
    <?php if (IS_PROD): ?>

        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-KQKHC78');</script>

    <?php endif; ?>
    <meta name="msapplication-config" content="none"/>
    <meta name="viewport" content="width=1024">
    <?php //echo $this->Html->charset(); ?>

    <link rel="apple-touch-icon" href="<?php echo $this->webroot; ?>img/apple-touch-icon.png">
    <title> <?php echo $title_for_layout; ?></title>
    <?php
    $controllerName = strtolower($this->params['controller']);
    $actionName = strtolower($this->params['action']);
    $isLoginPage = 'users' == $controllerName && 'login' == $actionName;
    $isPeoplePage = 'users' == $controllerName && 'administrators_groups' == $actionName;
    $cdn_ver = Configure::read('cdn_ver');
    $isAnalytics = false;
    if ($controllerName == 'dashboard' || $controllerName == 'Dashboard' || $controllerName == 'Analytics' || $controllerName == 'analytics') {

        if ('analytics' == $actionName || 'usergraph' == $actionName || 'userdetail' == $actionName || 'coach_tracker' == $actionName || 'assessment_tracker' == $actionName || 'play_card' == $actionName || 'index' == $actionName) {

            $isAnalytics = true;
        }
    }

    if ($isLoginPage || $isPeoplePage) {
        $cssFiles = array(
            'sibme', 'style-extention', 'fancybox/jquery.fancybox.css', 'pro_dropdown_2', 'jquery.loadmask.css',
            'alerts', 'bootstrap.min', 'custom.css', 'jquery.timepicker.css', 'jquery.datetimepicker.css'
        );
    } else {
        $cssFiles = array(
            'app', 'sibme', 'style-extention', 'fancybox/jquery.fancybox.css', 'pro_dropdown_2', 'jquery.loadmask.css',
            'alerts', 'bootstrap.min', 'custom.css', 'jquery.timepicker.css', 'newstyle.css', 'jquery.treeview.css', 'jquery.datetimepicker.css'
        );
    }
    $cssFiles[] = 'bootstrap-tour/bootstrap-tour.min';
    if ($controllerName == 'videolibrary') {
        $cssFiles[] = 'video-js.css';
    }
    $use_local_store = Configure::read('use_local_file_store');
    $amazon_assets_url = Configure::read('amazon_assets_url');
    if ($use_local_store) {
        echo $this->Minify->css($cssFiles);
    } else {
        if ($isLoginPage || $isPeoplePage) {
            echo '<link href="' . $amazon_assets_url . 'static/css/min-people.zip.css?v=' . $cdn_ver . '" media="screen" rel="stylesheet" type="text/css" />';
        } else {
            echo '<link href="' . $amazon_assets_url . 'static/css/min-general.zip.css?v=' . $cdn_ver . '" media="screen" rel="stylesheet" type="text/css" />';
        }
        if ($controllerName == 'videolibrary') {
            echo '<link href="' . $amazon_assets_url . 'static/css/video-js.css?v=' . $cdn_ver . '" media="screen" rel="stylesheet" type="text/css" />';
        }
    }
    ?>
    <!--<link rel="stylesheet" href="<?php echo $this->webroot; ?>css/newstyle.css" />-->
    <link href="https://desk-customers.s3.amazonaws.com/shared/email_widget.css" media="screen" rel="stylesheet" type="text/css" />

    <?php $user_current_account = $this->Session->read('user_current_account'); ?>
    <?php $current_user_id = !empty($user_current_account['User']['id']) ? $user_current_account['User']['id'] : 0; ?>
    <?php
    $api_url = Configure::read('sibme_api_url');
    if($_SESSION['site_id']=='2'){
        $api_url = Configure::read('hmh_api_url');
    }
    ?>
    <script type="text/javascript">
        var home_url = '<?php echo Configure::read('sibme_base_url'); ?>';
        var api_url = '<?php echo $api_url; ?>';
        var supress_app_init = false;
        var bucket_name = "<?php echo Configure::read('bucket_name'); ?>";
        var current_user_id = <?php echo $current_user_id; ?>;
    </script>
    <?php
    $js_scripts = array(
        'jquery-1.11.0.min', 'jquery-ui.js', 'advanced', 'wysihtml5-0.3.0', 'bootstrap.min', 'jquery.validate.min', 'tabs', 'video-js/video.dev.js',
        'jquery.tinyscrollbar.min', 'jquery.jcarousel', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.blockUI',
        'jquery.tagsinput', 'jquery.jqpagination', 'masonry.pkgd.min',
        'common', 'script', 'slimtable.min.js', 'jquery.quicksearch', 'jquery.cookie.js', 'jquery.treeview.js'
    );
    if ($controllerName == 'huddles' && ($actionName == 'edit' || $actionName == 'add')) {
        $js_scripts[] = 'huddle.edit';
    } elseif ($controllerName == 'videolibrary') {
        //$js_scripts[] = 'videojs.js';
    } elseif ($controllerName != 'edtpa') {
        $js_scripts[] = 'jquery.datetimepicker.js';
        $js_scripts[] = 'jquery.timepicker.js';
        $js_scripts[] = 'customobservation.js';
        $js_scripts[] = 'demo.js';
    }

    if ($use_local_store) {
        echo $this->Minify->script($js_scripts);
    } else {
        if ($controllerName == 'huddles' && ($actionName == 'edit' || $actionName == 'add')) {
            echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-js-huddle.js?v=' . $cdn_ver . '"></script>';
        } elseif ($controllerName == 'videolibrary') {
            echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-js-video.js?v=' . $cdn_ver . '"></script>';
        } else {
            echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-js-general.js?v=' . $cdn_ver . '"></script>';
        }
    }
    ?>

    <?php if ($isAnalytics) { ?>

        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/analytics/stylecss.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $this->webroot; ?>css/analytics/lib/js/themes/redmond/jquery-ui.custom.css"></link>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $this->webroot; ?>css/analytics/lib/js/jqgrid/css/ui.jqgrid.css"></link>
        <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/analytics/lib/js/themes/redmond/green_theme.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/analytics/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/analytics.css">
        <!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">-->
        <link href="<?php echo $this->webroot; ?>css/analytics/sliptree/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
        <!-- Tokenfield CSS -->
        <link href="<?php echo $this->webroot; ?>css/analytics/sliptree/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
        <!-- Docs CSS -->
        <link href="<?php echo $this->webroot; ?>css/analytics/sliptree/pygments-manni.css" type="text/css" rel="stylesheet">
        <link href="<?php echo $this->webroot; ?>css/analytics/sliptree/docs.css" type="text/css" rel="stylesheet">
        <link href="<?php echo $this->webroot; ?>css/virtual_tracker.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->webroot; ?>amcharts/plugins/export/export.css" rel="stylesheet" type="text/css">





        <style>
            #chartdiv {
                width    : 100%;
                height    : 500px;
            }
            .ui-widget-content a{
                text-decoration: underline;
            }
            .ui-widget-content a:hover{
                text-decoration: underline;
            }
            .ui-state-highlight a, .ui-widget-content .ui-state-highlight a, .ui-widget-header .ui-state-highlight a{
                text-decoration: underline;
            }
        </style>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/amcharts.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/pie.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/serial.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/themes/light.js"></script>
        <script src="<?php echo $this->webroot; ?>amcharts/plugins/export/export.js"></script>
        <!--<script src="<?php echo $this->webroot; ?>css/analytics/lib/js/jquery.min.js" type="text/javascript"></script>-->
        <?php if($_SESSION['LANG'] =='es'):?>
            <script src="<?php echo $this->webroot; ?>css/analytics/lib/js/jqgrid/js/i18n/grid.locale-es.js" type="text/javascript"></script>
        <?php else:?>
            <script src="<?php echo $this->webroot; ?>css/analytics/lib/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
        <?php endif;?>    
        
        <script src="<?php echo $this->webroot; ?>css/analytics/lib/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->webroot; ?>css/analytics/lib/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>

        <script src="<?php echo $this->webroot; ?>js/analytics/waypoints.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/analytics/jquery.counterup.min.js"></script>
        <script src="<?php echo $this->webroot; ?>js/analytics/app.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/analytics/sliptree/bootstrap-tokenfield.js" charset="UTF-8"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/analytics/sliptree/scrollspy.js" charset="UTF-8"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/analytics/sliptree/affix.js" charset="UTF-8"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/analytics/sliptree/typeahead.bundle.min.js" charset="UTF-8"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/analytics/sliptree/docs.min.js" charset="UTF-8"></script>


    <?php }
    ?>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s)
        {
            if (f.fbq)
                return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)
                f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '191802657955580');
        fbq('track', 'PageView');
    </script>
    <noscript>
<img height="1" width="1"
     src="https://www.facebook.com/tr?id=191802657955580&ev=PageView
     &noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<?php if (isset($_SESSION['site_id']) && $_SESSION['site_id'] == 2): ?>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon_hmh.ico" />
<?php else: ?>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon_sibme.ico" />
<?php endif; ?>
</head>
