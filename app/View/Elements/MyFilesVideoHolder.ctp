<?php
$amazon_base_url = Configure::read('amazon_base_url');
?>
<?php if (isset($video_id) && $video_id != ''): ?>
    <!--<div class="video-list open">-->
    <div class="video-list">
        <a class="video-list__toggle" href="#"></a>
        <a class="video-list__left" href="#"></a>
        <div class="video-list__items-wrap">

            <ul class="video-list__items">
                <?php if ($videos): ?>
                    <?php
                    $vol = 1;
                    foreach ($videos as $video):
                        $videoID = $video['AccountFolder']['account_folder_id'];
                        //$videoFilePath = pathinfo($video['doc']['url']);
                        //$videoFileName = $videoFilePath['filename'];  
                        
                        $document_files_array = $this->Custom->get_document_holder_url($video['doc']);

                        if (empty($document_files_array['url'])) {
                            $video['doc']['published'] =0;
                            $document_files_array['url'] = $video['doc']['original_file_name'];
                            $video['doc']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $video['doc']['encoder_status'] = $document_files_array['encoder_status'];
                        }
                        ?>

                        <li class="video-list__item media<?php echo($videoID == $video_id ? ' active' : ''); ?>" onclick="javascript: window.location = '<?php echo $this->base . '/' . 'MyFiles/view/1/' . $video['AccountFolder']['account_folder_id'] ?>'">
                            <div class="media__img">

                                <span class="video-list__item-num"><?php echo $vol; ?></span>
                                <?php

                                    $thumbnail_image_path = $document_files_array['thumbnail'];

                                    //if ($video['doc']['published'] == '1')  {
                                    if (!empty($thumbnail_image_path))  {

                                        echo $this->Html->image($thumbnail_image_path, array('class' => 'video-list__item-thumb')); 

                                    } else { ?>

                                    <a id="processing-message-<?php echo $video['doc']['id']; ?>" style="position:relative;display: block;width: 76px;" href="#">
                                        <div class="video-unpublished" style="height: 50px;width: 65px;top: -20px;position: absolute;left: 15px;overflow: hidden;"><span>
                                        <?php if ($video['doc']['encoder_status'] == 'Error'): ?>
                                            Video failed to process, please try again.
                                        <?php else : ?>
                                            Video is currently processing.
                                        <?php endif; ?>
                                        </span></div>
                                    </a>

                                <?php

                                    }

                                    

                                ?>
                            </div>
                            <div class="media__body">
                                <div class="video-list__item-name"><?php echo $video['afd']['title']; ?></div>
                                <div class="video-list__item-author">By <?php echo $video['User']['first_name'] . " " . $video['User']['last_name'] ?></div>
                            </div>
                        </li>
                        <?php
                        $vol++;
                    endforeach;
                    ?>
                <?php endif; ?>
            </ul>
        </div>
        <a class="video-list__right" href="#"></a>
    </div>
    <?php endif; ?>

    <script type="text/javascript">
        $(document).ready(function(e) {
            $('.vjs-big-play-button').on('click', function(event) {
                event.preventDefault();
                var $videoList = $('.video-list');
                //$videoList[ $videoList.hasClass('open') ? 'removeClass' : 'addClass' ]('open');
                $videoList[ 'removeClass' ]('open');
            });
/*            $('.vjs-play-control').on('click', function(event) {
                event.preventDefault();
                var $videoList = $('.video-list');     
                var $videoPlayer = $('.vjs-play-control');     
                if ($videoPlayer.hasClass('vjs-paused')) {
                    $videoList[ 'addClass' ]('open');
                }
                if ($videoPlayer.hasClass('vjs-playing')) {
                    $videoList[ 'removeClass' ]('open');
                }
                //$videoList[ $videoList.hasClass('open') ? 'removeClass' : 'addClass' ]('open');
            });*/
            $('.video-list__right').click(function() {
                var margin_left = $('.video-list__items').offset().left;
                $.ajax({
                    dataType: 'JSON',
                    type: 'POST',
                    data: {
                        left: margin_left
                    },
                    url: '<?php echo $this->base ?>/MyFiles/setSession/',
                    success: function(response) {
                        alert(response);
                    },
                    errors: function(response) {
                        alert(response.contents);
                    }
                });
            });
            var margin_left = $('#left-margin').val();
            $('ul.video-list__items').css('left', margin_left + 'px;');
        })
    </script>
