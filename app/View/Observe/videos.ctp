<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<style type="text/css">
    .inactive-tab{
        background-color: #ccc !important;
    }
</style>
<script type="text/javascript">
    var current_user_login = 'sibmin';
    var current_user_id = '117';
</script>    </header>
<div style="margin: 32px auto 0px;width: 948px;background: red;color: #fff;padding: 10px;display:none;">We are currently experiencing technical difficulties with video processing. If one of your huddles was inactivated after uploading a video, please let us know immediately. Thank you for your patience as we resolve this issue.</div>
<div id="main" class="box container" style="overflow: visible;">
    <div class="header header-huddle">
        <div class="action-buttons btn-group right mmargin-top">
            <a href="/Huddles" class="btn" style="">Back to all Huddles</a>
            <a href="/Huddles/edit/668" class="btn icon2-pen" rel="tooltip" title="edit"></a>
            <a href="/Huddles/delete/668"
               data-confirm="Are you sure you want to delete this Huddle?" data-method="delete" class="btn icon2-trash" rel="tooltip" title="delete"></a>
        </div>

        <h2 class="title">Acme H.S. Science Department PLC </h2>
        <p class="title-desc">Science department PLC for 2014-15 school year.</p>

    </div>
    <div class="tab-box" style="overflow: visible;">
        <div id="tab-area">

            <div class="btn-box">
                <a id="gDocumentUpload" style="display: none;" class="btn btn-green icon2-video tab1" href="#">Upload Document</a>
                <a class="btn btn-green right" href="/Huddles/add" id="btn-new-huddle">New Observation</a>
                <a id="gDiscussion" href="/Huddles/view/668/3/add" class="btn btn-creat btn-blue" style="display: none;" >Create new discussion</a>
            </div>

            <ul class="tabset">
                <li class="">
                    <a id="tab1"  class="tab active" href="#tabbox1">Observations</a>
                </li>
                <li class="hidden">
                    <a id="tab2"  class="tab " href="#tabbox2">Documents</a>
                </li>
                <li class="hidden">
                    <a id="tab3" class="tab " href="#tabbox3">Huddle Discussion</a>
                </li>
                <li class="hidden">
                    <a id="tab4" class="tab " href="#tabbox4">People</a>
                </li>
            </ul>
            <div class="table-container">

                <div class="tab-content tab-active"  id="tabbox5">
                    <div style="padding-bottom:30px; padding-top: 10px;">
                        <strong>Videos</strong>
                        <div class="search-box" style="width: 524px;">
                            <input type="button" id="btnSearchVideos" class="btn-search" value="">
                            <input class="text-input" id="txtSearchVideos" type="text" value="" placeholder="Search Videos..." style="margin-right: 25px;">
                            <span id="clearVideoButton" style="display: none;" class="clear-video-input-box">X</span>
                            <div class="select">
                                <select id="cmbVideoSort" name="upload-date">
                                    <option value="afd.title ASC">Video Title</option>
                                    <option value="Document.created_date DESC">Date Uploaded</option>
                                    <option value="User.first_name DESC">Uploaded By</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <input id="txtHuddleID" type="hidden" value="668"/>
                    <div id="temp-list" style="display: none;">
                        <li class="videos-list__item">
                            <div class="videos-list__item-thumb">
                                <div class="video-unpublished "><span class="huddles-unpublished"><img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br><?=$alert_messages["Your_video_is_currently_processing"]; ?></span></div>
                            </div>
                            <div class="videos-list__item-aside">
                                <div id="temp-video-title" class="videos-list__item-title"> </div>
                                <div class="videos-list__item-author">By <a href="#" title="Dave Wakefield">Dave Wakefield</a></div>
                                <div class="videos-list__item-added">Uploaded May 21, 2015</div>
                            </div>
                        </li>
                    </div>
                    <div style="clear: both;" class='clearfix'></div>
                    <div id="videos-list">
                        <p id="notification" style="display: none;padding-left: 29px;padding-right: 9px;padding-bottom: 0px;"></p>
                        <ul class="videos-list">









                            <li class="videos-list__item observee-wrape-1">
                                <div class="ac-btn">
                                    <a href="/Huddles/deleteHuddleVideo/668/1/1066"
                                       data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                                       data-original-title="Delete" rel="tooltip" data-method="delete"
                                       class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;width: 15px;padding: 0px;"></a>




                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#copy-huddle-1066').click(function () {
                                                $('#copy-document-id').val($(this).attr('data-document-id'));
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="clearfix"></div>
                                <div class="videos-list__item-thumb">
                                    <a href="/Huddles/view/668/1/1066" title="Spicer - Physics" >
                                        <img src="/img/JOkQsGdQQqDpujd8Pf6URob-Demos-QtA_1429637345_thumb_00001.png" height="146" class="thumb_img" alt="" />                        <div class="play-icon"></div>
                                    </a>
                                </div>
                                <div class="videos-list__item-aside observee-wrape-box">
                                    <div class="videos-list__item-title">
                                        <a class="wrap" id="vide-title-1066" href="/Huddles/view/668/1/1066" title="Spicer - Physics" >App-sibme-29-03-2015...</a>
                                    </div>

                                    <div class="videos-list__item-added">May, 06 2014 - Classroom 1A<div style="float: right;margin-right: -15px;">
                                            <a style="float: left;" href="https://sibme-production.s3.amazonaws.com/uploads/34/668/2014/02/16/TEtoD15jTi29Rg9YSAo8Zimmer-Physics_enc.mp4?response-content-disposition=attachment%3B%20filename%3DZimmer%20-%20Physics.mov&AWSAccessKeyId=AKIAJ4ZWDR5X5JKB7CZQ&Expires=1432230291&Signature=3c8RtDmsQTLGiTTQ1RWwtnK2Emw%3D" target="_blank" download="TEtoD15jTi29Rg9YSAo8Zimmer-Physics_enc.mp4">

                                            </a>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>








                                    <div class="observee-new-video-1">
                                        <h3> Observee</h3>
                                        <img align="left" width="34" height="34" rel="tooltip" data-original-title="Lindsey Anderson" class="photo observee-new-img" alt="Lindsey Anderson" src="/img/users/392321_10150571467603916_1423098013_n.jpg">

                                        <h4>Denise Sanchez</h4>
                                        <b>Admin</b>
                                        <img rel="tooltip" data-original-title="Private Observation" src="/img/lock-user.png" class="lock-observee-1-a" />
                                    </div>


                                    <div class="observee-observers-list-1"> <h5 class="smargin-vertical">Observer</h5>
                                        <img align="left" width="34" height="34" src="/img/users/392321_10150571467603916_1423098013_n.jpg" alt="Lindsey Anderson" class="photo observee-new-img" data-original-title="Lindsey Anderson" rel="tooltip">
                                        <img align="left" width="34" height="34" src="/img/users/392321_10150571467603916_1423098013_n.jpg" alt="Lindsey Anderson" class="photo observee-new-img" data-original-title="Lindsey Anderson" rel="tooltip"></div>

                                </div>
                            </li>





















                            <li class="videos-list__item observee-wrape-1">
                                <div class="ac-btn">
                                    <a href="/Huddles/deleteHuddleVideo/668/1/1066"
                                       data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                                       data-original-title="Delete" rel="tooltip" data-method="delete"
                                       class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;width: 15px;padding: 0px;"></a>




                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#copy-huddle-1066').click(function () {
                                                $('#copy-document-id').val($(this).attr('data-document-id'));
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="clearfix"></div>
                                <div class="videos-list__item-thumb">
                                    <a href="/Huddles/view/668/1/1066" title="Spicer - Physics" >
                                        <img src="/img/JOkQsGdQQqDpujd8Pf6URob-Demos-QtA_1429637345_thumb_00001.png" height="146" class="thumb_img" alt="" />                        <div class="play-icon"></div>
                                    </a>
                                </div>
                                <div class="videos-list__item-aside observee-wrape-box">
                                    <div class="videos-list__item-title">
                                        <a class="wrap" id="vide-title-1066" href="/Huddles/view/668/1/1066" title="Spicer - Physics" >App-sibme-29-03-2015...</a>
                                    </div>

                                    <div class="videos-list__item-added">May, 06 2014 - Classroom 1A<div style="float: right;margin-right: -15px;">
                                            <a style="float: left;" href="https://sibme-production.s3.amazonaws.com/uploads/34/668/2014/02/16/TEtoD15jTi29Rg9YSAo8Zimmer-Physics_enc.mp4?response-content-disposition=attachment%3B%20filename%3DZimmer%20-%20Physics.mov&AWSAccessKeyId=AKIAJ4ZWDR5X5JKB7CZQ&Expires=1432230291&Signature=3c8RtDmsQTLGiTTQ1RWwtnK2Emw%3D" target="_blank" download="TEtoD15jTi29Rg9YSAo8Zimmer-Physics_enc.mp4">

                                            </a>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>








                                    <div class="observee-new-video-1">
                                        <h3> Observee</h3>
                                        <img align="left" width="34" height="34" rel="tooltip" data-original-title="Lindsey Anderson" class="photo observee-new-img" alt="Lindsey Anderson" src="/img/users/392321_10150571467603916_1423098013_n.jpg">

                                        <h4>Denise Sanchez</h4>
                                        <b>Admin</b>
                                        <img rel="tooltip" data-original-title="Private Observation" src="/img/lock-user.png" class="lock-observee-1-a" />
                                    </div>


                                    <div class="observee-observers-list-1"> <h5 class="smargin-vertical">Observer</h5>
                                        <img align="left" width="34" height="34" src="/img/users/392321_10150571467603916_1423098013_n.jpg" alt="Lindsey Anderson" class="photo observee-new-img" data-original-title="Lindsey Anderson" rel="tooltip">
                                        <img align="left" width="34" height="34" src="/img/users/392321_10150571467603916_1423098013_n.jpg" alt="Lindsey Anderson" class="photo observee-new-img" data-original-title="Lindsey Anderson" rel="tooltip"></div>

                                </div>
                            </li>































                            <li class="videos-list__item observee-wrape-1">
                                <div class="ac-btn">
                                    <a href="/Huddles/deleteHuddleVideo/668/1/1066"
                                       data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                                       data-original-title="Delete" rel="tooltip" data-method="delete"
                                       class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;width: 15px;padding: 0px;"></a>




                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#copy-huddle-1066').click(function () {
                                                $('#copy-document-id').val($(this).attr('data-document-id'));
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="clearfix"></div>
                                <div class="videos-list__item-thumb">
                                    <a href="/Huddles/view/668/1/1066" title="Spicer - Physics" >
                                        <img src="/img/JOkQsGdQQqDpujd8Pf6URob-Demos-QtA_1429637345_thumb_00001.png" height="146" class="thumb_img" alt="" />                        <div class="play-icon"></div>
                                    </a>
                                </div>
                                <div class="videos-list__item-aside observee-wrape-box">
                                    <div class="videos-list__item-title">
                                        <a class="wrap" id="vide-title-1066" href="/Huddles/view/668/1/1066" title="Spicer - Physics" >App-sibme-29-03-2015...</a>
                                    </div>

                                    <div class="videos-list__item-added">May, 06 2014 - Classroom 1A<div style="float: right;margin-right: -15px;">
                                            <a style="float: left;" href="https://sibme-production.s3.amazonaws.com/uploads/34/668/2014/02/16/TEtoD15jTi29Rg9YSAo8Zimmer-Physics_enc.mp4?response-content-disposition=attachment%3B%20filename%3DZimmer%20-%20Physics.mov&AWSAccessKeyId=AKIAJ4ZWDR5X5JKB7CZQ&Expires=1432230291&Signature=3c8RtDmsQTLGiTTQ1RWwtnK2Emw%3D" target="_blank" download="TEtoD15jTi29Rg9YSAo8Zimmer-Physics_enc.mp4">

                                            </a>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>








                                    <div class="observee-new-video-1">
                                        <h3> Observee</h3>
                                        <img align="left" width="34" height="34" rel="tooltip" data-original-title="Lindsey Anderson" class="photo observee-new-img" alt="Lindsey Anderson" src="img/users/392321_10150571467603916_1423098013_n.jpg">

                                        <h4>Denise Sanchez</h4>
                                        <b>Admin</b>
                                        <img rel="tooltip" data-original-title="Private Observation" src="/img/lock-user.png" class="lock-observee-1-a" />
                                    </div>


                                    <div class="observee-observers-list-1"> <h5 class="smargin-vertical">Observer</h5>
                                        <img align="left" width="34" height="34" src="/img/users/392321_10150571467603916_1423098013_n.jpg" alt="Lindsey Anderson" class="photo observee-new-img" data-original-title="Lindsey Anderson" rel="tooltip">
                                        <img align="left" width="34" height="34" src="/img/users/392321_10150571467603916_1423098013_n.jpg" alt="Lindsey Anderson" class="photo observee-new-img" data-original-title="Lindsey Anderson" rel="tooltip"></div>

                                </div>
                            </li>





















                            <li class="videos-list__item observee-wrape-1">
                                <div class="ac-btn">
                                    <a href="/Huddles/deleteHuddleVideo/668/1/1066"
                                       data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>"
                                       data-original-title="Delete" rel="tooltip" data-method="delete"
                                       class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;width: 15px;padding: 0px;"></a>




                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#copy-huddle-1066').click(function () {
                                                $('#copy-document-id').val($(this).attr('data-document-id'));
                                            });
                                        });
                                    </script>
                                </div>
                                <div class="clearfix"></div>
                                <div class="videos-list__item-thumb">
                                    <a href="/Huddles/view/668/1/1066" title="Spicer - Physics" >
                                        <img src="/img/JOkQsGdQQqDpujd8Pf6URob-Demos-QtA_1429637345_thumb_00001.png" height="146" class="thumb_img" alt="" />                        <div class="play-icon"></div>
                                    </a>
                                </div>
                                <div class="videos-list__item-aside observee-wrape-box">
                                    <div class="videos-list__item-title">
                                        <a class="wrap" id="vide-title-1066" href="/Huddles/view/668/1/1066" title="Spicer - Physics" >App-sibme-29-03-2015...</a>
                                    </div>

                                    <div class="videos-list__item-added">May, 06 2014 - Classroom 1A<div style="float: right;margin-right: -15px;">
                                            <a style="float: left;" href="https://sibme-production.s3.amazonaws.com/uploads/34/668/2014/02/16/TEtoD15jTi29Rg9YSAo8Zimmer-Physics_enc.mp4?response-content-disposition=attachment%3B%20filename%3DZimmer%20-%20Physics.mov&AWSAccessKeyId=AKIAJ4ZWDR5X5JKB7CZQ&Expires=1432230291&Signature=3c8RtDmsQTLGiTTQ1RWwtnK2Emw%3D" target="_blank" download="TEtoD15jTi29Rg9YSAo8Zimmer-Physics_enc.mp4">

                                            </a>
                                        </div>
                                        <div style="clear: both;"></div>
                                    </div>








                                    <div class="observee-new-video-1">
                                        <h3> Observee</h3>
                                        <img align="left" width="34" height="34" rel="tooltip" data-original-title="Lindsey Anderson" class="photo observee-new-img" alt="Lindsey Anderson" src="img/users/392321_10150571467603916_1423098013_n.jpg">

                                        <h4>Denise Sanchez</h4>
                                        <b>Admin</b>
                                        <img rel="tooltip" data-original-title="Private Observation" src="/img/lock-user.png" class="lock-observee-1-a" />
                                    </div>


                                    <div class="observee-observers-list-1"> <h5 class="smargin-vertical">Observer</h5>
                                        <img align="left" width="34" height="34" src="/img/users/392321_10150571467603916_1423098013_n.jpg" alt="Lindsey Anderson" class="photo observee-new-img" data-original-title="Lindsey Anderson" rel="tooltip">
                                        <img align="left" width="34" height="34" src="/img/users/392321_10150571467603916_1423098013_n.jpg" alt="Lindsey Anderson" class="photo observee-new-img" data-original-title="Lindsey Anderson" rel="tooltip"></div>

                                </div>
                            </li>













                        </ul>

                    </div>

                    <script>
                        var page = 2;
                        function loadMoreVideos() {
                            $.ajax({
                                type: 'POST',
                                data: {
                                    type: 'get_video_comments',
                                    sort: $('#cmbVideoSort').val(),
                                    page: page,
                                    load_more: 1
                                },
                                url: home_url + '/Huddles/getVideoSearch/' + $('#txtHuddleID').val() + '/' + $('#txtSearchVideos').val(),
                                success: function (response) {
                                    if (page == 0) {
                                        $('.videos-list').html(response);
                                    } else {
                                        var els = $(response);
                                        els.appendTo($('.videos-list'));//.hide().fadeIn('slow');
                                        var top = els.eq(0).offset().top;
                                        if (top > 0) {
                                            $('body').animate({scrollTop: top}, 500);
                                        }
                                    }
                                    page++;
                                },
                                errors: function (response) {
                                    alert(response.contents);
                                }

                            });
                        }
                        function hideLoadMore() {
                            $('.load_more').hide();
                        }
                    </script>

                    <script>
                        var yTrigger = 325;
                        var viz = false;
                        var doc = document.documentElement;
                        var body = document.body;
                        var minHeight = 640;
                        var minWidth = 950;
                        var top;
                        var position;
                        var rightbox = $('.right-box');
                        var leftbox = $('.left-box');

                        var small_scroll = false;

                        $(document).ready(function () {
                            var currentWinH = $(window).height();
                            var checkH = (currentWinH <= minHeight) ? addScroll() : doNothing();
                            var currentWinW, checkW;
                            if (!small_scroll) {
                                currentWinW = $(window).width();
                                checkW = (currentWinW <= minWidth) ? addScroll() : removeScroll();
                            }

                            window.onscroll = function () {
                                where();
                            };

                            $(window).resize(function () {
                                //console.log("resized");
                                currentWinH = $(window).height();
                                checkH = (currentWinH <= minHeight) ? addScroll() : removeScroll();
                                if (!small_scroll) {
                                    currentWinW = $(window).width();
                                    checkW = (currentWinW <= minWidth) ? addScroll() : removeScroll();
                                }
                            });
                        });
                        function where() {
                            if (leftbox.outerHeight() > rightbox.outerHeight())
                                return;

                            position = $(document).scrollTop();
                            if (position >= yTrigger && viz == false && !small_scroll) {
                                $(".video-outer").addClass("videoPosition");
                                $("#comment_add_form_html").addClass("docsPosition");
                                viz = true;
                            } else if ((position < yTrigger && viz == true) || small_scroll) {
                                $(".video-outer").removeClass("videoPosition");
                                $("#comment_add_form_html").removeClass("docsPosition");
                                viz = false;
                            }
                        }

                        function addScroll() {
                            $("#docs-container ul").addClass("scroll-small");
                            small_scroll = true;
                            where();
                        }

                        function removeScroll() {
                            $("#docs-container ul").removeClass("scroll-small");
                            small_scroll = false;
                            where();
                        }

                        function doNothing() {             //ha ha!
                        }

                    </script>

                    <div id="moveFiles"  class="modal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="header">
                                    <h4 class="header-title nomargin-vertical smargin-bottom">Copy video to another Huddle  or the Account Video Library</h4>
                                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">�</a>
                                </div>
                                <form accept-charset="UTF-8"  id="huddle-copy-form" action="/Huddles/copy/" enctype="multipart/form-data" method="post">
                                    <input name="document_id" type="hidden" class="copy-document-ids" id="copy-document-id" value=""/>
                                    <ul class="inset-area clear-list files-list"  style="overflow-y: scroll; height: 148px;">
                                        <li>
                                            <label class="ui-checkbox model">
                                                <input name="account_folder_id[]" id="account_folder_name_-1" type="checkbox" value="-1">';
                                            </label>
                                            <label  for="account_folder_name_-1">Copy to Video Library </label>
                                        </li>
















                                        <li><label class="ui-checkbox model"><input name="account_folder_id[]" id="account_folder_name_657" type="checkbox" value="657"></label><label  for="account_folder_name_657">Acme H.S. Social Studies Department PLC</label></li><li><label class="ui-checkbox model"><input name="account_folder_id[]" id="account_folder_name_564" type="checkbox" value="564"></label><label  for="account_folder_name_564">Beth Campbell: English I</label></li><li><label class="ui-checkbox model"><input name="account_folder_id[]" id="account_folder_name_565" type="checkbox" value="565"></label><label  for="account_folder_name_565">David Wakefield:  ELA Acme Elementary</label></li><li><label class="ui-checkbox model"><input name="account_folder_id[]" id="account_folder_name_42" type="checkbox" value="42"></label><label  for="account_folder_name_42">David Wakefield: Geography Acme HS</label></li><li><label class="ui-checkbox model"><input name="account_folder_id[]" id="account_folder_name_569" type="checkbox" value="569"></label><label  for="account_folder_name_569">Leah Riddle and Mark Jones: Algebra 1</label></li><li><label class="ui-checkbox model"><input name="account_folder_id[]" id="account_folder_name_562" type="checkbox" value="562"></label><label  for="account_folder_name_562">Lindsey Anderson: Physics Acme ISD </label></li><li><label class="ui-checkbox model"><input name="account_folder_id[]" id="account_folder_name_568" type="checkbox" value="568"></label><label  for="account_folder_name_568">Peter Rosenquist: ELA</label></li><li><label class="ui-checkbox model"><input name="account_folder_id[]" id="account_folder_name_566" type="checkbox" value="566"></label><label  for="account_folder_name_566">Tiffany Reeves: English Acme HS</label></li>
                                    </ul>
                                    <input class="btn btn-green" type="submit" name="submit" id="copy-huddle-btn" value="Copy">
                                </form>

                            </div>
                        </div>

                    </div>
                    <script type="text/javascript">
                        $('#copy-huddle-btn').on('click', function (e) {
                            e.preventDefault();
                            $('#copy-huddle-btn').val('Copying...');
                            $.ajax({
                                url: home_url + "/Huddles/copy",
                                data: $('#huddle-copy-form').serialize(),
                                type: 'POST',
                                dataType: 'json',
                                success: function (response) {
                                    $('#copy-huddle-btn').val('Copy');
                                    $('#notification').css('display', 'block');
                                    if (response.status == true) {
                                        $('#notification').html('<div class="message success" style="cursor:pointer">' + response.message + '</div>');
                                    } else {
                                        $('#notification').html('<div class="message error"  style="cursor:pointer">' + response.message + '</div>');
                                    }

                                    $('#moveFiles').modal('hide');
                                },
                                error: function () {
                                    alert("Network Error Occured");
                                }
                            });
                        });
                        $('#notification').hover(function (e) {
                            $('#notification').fadeOut();
                        });
                        $(document).ready(function () {
                            $('.vjs-play-progress').css('padding-right', '8px');
                        });
                    </script>
                </div>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $("#inline-crop-panel").fancybox({
                            width: '70%',
                            height: '70%',
                            type: "iframe",
                            closeClick: false,
                            helpers: {
                                title: null,
                                overlay: {closeClick: false}
                            }
                        });


                        $('.btn-trim-video').click(function () {
                            $(this).next().submit();
                        })
                    });
                </script>


                <div class="tab-content tab-hidden" id="tabbox2">
                    <div class="tab-box2">
                        <form id="huddle-download-form" action="/Huddles/downloadZip/668" method="post" style="margin-left:14px;">
                            <div class="tab-header">
                                <div class="tab-header__title"> <strong>Documents</strong>
                                    <div style="clear: both;"></div>

                                    <div class="docs-list__head2 span3" style="margin: 0px; margin-top: 9px;">
                                        <a style="cursor: pointer; margin-right: 30px; font-weight: normal;" id="btnDownloadAsZip" class="js-file-download blue-link icon-download2" onclick="return downloadItemsAsZip();">Download</a>
                                        <a  id="delete-account-doc" onclick="return deleteDoc();" style="cursor: pointer" class="icon-delete2 blue-link delete js-file-delete">Delete</a>
                                    </div>
                                </div>

                                <div class="tab-header__right">
                                    <div style="float:left;position:relative;">
                                        <input type="button" id="btnSearchDocuments" class="btn-search" value="">
                                        <input type="hidden" name="download_as_zip_docs" id="download_as_zip_docs" value=""/>
                                        <input type="hidden" name="huddle_id" id="huddle_id" value="668"/>
                                        <input class="text-input" type="text" value="" id="txtSearchDocuments" placeholder="Search Documents...">
                                        <span id="clearButton" style="display:none;right:40px;top:4px;position:absolute;cursor:pointer;" class="clear-input-box">X</span>
                                    </div>
                                    <div class="select"  style="float:left;margin-left:12px;">
                                        <select id="cmbDocumentSort" name="upload-date">
                                            <option value="Document.created_date DESC">Date Uploaded</option>
                                            <option value="afd.title ASC">Document Title</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form id="huddle-delete-form" action="/Huddles/deleteDocs/668" method="post">
                            <input type="hidden" id="document_ids" name="document_ids" value=""/>
                            <input type="hidden" id="account-folder-id" name="account_folder_id" value=""/>
                        </form>
                    </div>
                    <div class="tab-box2">
                        <div class="table">
                            <div id="extra-row-li" style="display: none">
                                <div class="padd">
                                    <a rel="nofollow" data-confirm="Are you sure you want to delete this Resource?" id="delete-main-comments" class="icon-doc icon-indent blue-link delete">&nbsp;</a>
                                    <label class="ui-checkbox checkbox-2">
                                        <input class="checkbox-2 download_doc_checkbox" disabled="disabled" name="document_ids[]" type="checkbox" value="">
                                    </label>
                                    <a href="#" class="nomargin-vertical wrap">
                                        <span class="image">&nbsp;</span> <span id="doc-title"></span>
                                    </a>
                                    <strong id="doc-type" class="type "></strong>
                                    <strong id="doc-date" class="date">May 21, 2015</strong>
                                    <div class="wrap" style="width: 75px; float: left;"> ----- </div>
                                    <div class="doc_video_association">
                                        <div class="subject-row row">
                                            <div class="video-dropdown">Videos</div>
                                        </div>
                                    </div>
                                </div>
                                <form id="delete-document0" action="/Huddles/deleteDocument/0" accept-charset="UTF-8"></form>
                                <input type="hidden" name="account_folder_id" id="huddle_id" value="668"/>
                            </div>

                            <div class="head">
                                <label class="ui-checkbox left" style="margin: 11px 12px 0 12px;">
                                    <input class="head-checkbox" type="checkbox">
                                </label>
                                <strong class="name">Name</strong>
                                <strong class="type">Type</strong>
                                <strong class="date">Date Uploaded</strong>
                                <strong class="associated">Associated Video(s)</strong>
                            </div>

                            <ul class="ul-docs"></ul>
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $('.ul-docs').on('submit', '.delete-document', function (event) {
                            var form = $(this);
                            var li = form.closest('li');
                            $.ajax({
                                type: form.attr('method'),
                                url: form.attr('action'),
                                data: form.serialize(),
                                success: function () {
                                    li.remove();
                                    if ($('ul.ul-docs li.padd').length == 0) {
                                        $('ul.ul-docs').html('<li>No Attachments have been added.</li>');
                                    }
                                }
                            });
                            event.preventDefault();
                            return false;
                        });
                        $('#tabbox2 .head').on('click', '.head-checkbox', getToogleCheckAllDocuments);

                        initDocuments();
                        doDocumentSearchAjax();
                    });

                    function getCheckedDocuments() {
                        var users_id = '';
                        $.each($('.download_doc_checkbox'), function (index, value) {
                            if ($(this).prop('checked') == true) {
                                if (users_id != '')
                                    users_id += ',';
                                users_id += $(this).attr('value');
                            }
                        });

                        return users_id;
                    }

                    function getToogleCheckAllDocuments() {
                        if ($(this).prop('checked') == true) {
                            $.each($('.download_doc_checkbox'), function (index, value) {
                                $(this).prop('checked', true).trigger('change');
                            });
                        } else {
                            $.each($('.download_doc_checkbox'), function (index, value) {
                                $(this).prop('checked', false).trigger('change');
                            });
                        }
                    }

                    function downloadItemsAsZip() {
                        var checked_items = getCheckedDocuments();
                        if (checked_items == '') {
                            alert('Please select at least one document');
                            return false;
                        }
                        $('#download_as_zip_docs').val(checked_items);
                        $('#huddle-download-form').submit();
                        return true;
                    }

                    function deleteDoc() {
                        var checked_items = getCheckedDocuments();
                        if (checked_items == '') {
                            alert('Please select at least one document');
                            return false;
                        }
                        if (!confirm('Are you sure you want to delete this resource?')) {
                            return false;
                        }
                        $('#document_ids').val(checked_items);
                        $('#account-folder-id').val($('#huddle_id').val());
                        $('#huddle-delete-form').submit();
                        return true;
                    }

                    function initDocuments() {
                        $('.ul-docs')
                                .on('click', '.video-dropdown', function () {
                                    if ($(this).hasClass('video-dropdown-selected')) {
                                        $(this).removeClass('video-dropdown-selected');
                                        $($(this).siblings('.video-dropdown-container')[0]).css('display', 'none');
                                    } else {
                                        CloseAllDropDown();
                                        $(this).addClass('video-dropdown-selected');
                                        $($(this).siblings('.video-dropdown-container')[0]).css('display', 'block');
                                    }
                                })
                                .on('click', '.btnDoneVideoLibrary', CloseDropDown)
                                .on('click', '.btnCancelVideoLibrary', CloseAllDropDown)
                                ;
                    }

                    function getCheckedVideos(document_id) {
                        var checked_videos = [];
                        $('.doc_checkbox_' + document_id).each(function (idx, el) {
                            el = $(el);
                            if (el.prop('checked')) {
                                checked_videos.push(el.attr('value'));
                            }
                        });
                        return checked_videos;
                    }

                    function CloseDropDown() {
                        var document_id = this.id.split('-')[1];
                        var checked_videos = getCheckedVideos(document_id);
                        $.ajax({
                            type: 'POST',
                            data: {
                                associated_videos: checked_videos.join(',')
                            },
                            url: home_url + '/Huddles/associateVideoDocuments/' + document_id + '/668',
                            success: function () {
                                CloseAllDropDown();
                            },
                            errors: function (response) {
                                alert(response.contents);
                                location.href = home_url + '/Huddles/view/668/2/';
                            }
                        });
                    }

                    function CloseAllDropDown() {
                        var btnDones = $('.btnDoneVideoLibrary');
                        for (var i = 0; i < btnDones.length; i++) {
                            var btnDone = $(btnDones[i]);
                            var current_dropdwn = $(btnDone.parent().parent().prev()[0]);
                            if (current_dropdwn.hasClass('video-dropdown-selected')) {
                                current_dropdwn.removeClass('video-dropdown-selected');
                                $(current_dropdwn.siblings('.video-dropdown-container')[0]).css('display', 'none');
                            }
                        }
                    }
                </script>
                <div class="tab-content tab-hidden" id="tabbox3" style="display: none">
                    <div class="tab-3">
                        <h1>Huddle Discussion</h1>
                        <ul style="float:left;width:100%;">
                            <li>No Discussion started yet.</li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content tab-hidden" id="tabbox4" style="display: none">
                    <div class="tab-header">
                        <h2 class="tab-header__title">People</h2>
                    </div>

                    <h4>Admins:</h4>
                    <div class="video-huddle-users">
                        <a style="cursor: pointer;">
                            <img src="/img/users/735415012thumb.jpg" alt="Brian McGill" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Brian McGill</div>
                        </a>

                        <a style="cursor: pointer;">
                            <img src="/img/users/1460631889thumb.jpg" alt="Dave Wakefield" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Dave Wakefield</div>
                        </a>


                        <hr/>
                    </div>

                    <div class="video-huddle-users">
                        <h4>Members :</h4>
                        <a style="cursor: pointer;">
                            <img src="/img/users/546510_477201972292793_1867372539_n.jpeg" alt="Lindsey Anderson" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Lindsey Anderson</div>
                        </a>


                        <a style="cursor: pointer;">
                            <img src="/img/users/photo_3.jpeg" alt="Mark Jones" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Mark Jones</div>
                        </a>


                        <a style="cursor: pointer;">
                            <img src="/img/users/216594_10100676743986489_2074824_n.jpeg" alt="Tiffany Reeves" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Tiffany Reeves</div>
                        </a>


                        <a style="cursor: pointer;">
                            <img src="/img/users/383749_10150408531063244_830231116_n.jpeg" alt="Leah Riddle" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Leah Riddle</div>
                        </a>


                        <a style="cursor: pointer;">
                            <img src="/img/users/302950_10150324259541445_2075933453_n.jpeg" alt="Oscar Rodriguez" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Oscar Rodriguez</div>
                        </a>


                        <a style="cursor: pointer;">
                            <img src="/img/users/peter_clayburgh.jpeg" alt="Peter Rosenquist" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Peter Rosenquist</div>
                        </a>


                        <a style="cursor: pointer;">
                            <img src="/img/users/392321_10150571467603916_1423098013_n.jpeg" alt="Denise Sanchez" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Denise Sanchez</div>
                        </a>


                        <a style="cursor: pointer;">
                            <img src="/img/users/225560c.jpeg" alt="Andrew Towne" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                            <div style="clear:both">Andrew Towne</div>
                        </a>


                    </div>
                    <hr/>

                    <div class="video-huddle-users">
                        <h4>Viewers :</h4>
                        <a style="cursor: pointer;">
                            <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                            <br/>
                            <div style="clear:both">Jerry Karry</div>
                        </a>
                    </div>
                    <hr/>
                </div>
            </div>
            <input id="txtUploadedFilePath" type="hidden" value="" />
            <input id="txtUploadedFileName" type="hidden" value="" />
            <input id="txtUploadedFileMimeType" type="hidden" value="" />
            <input id="txtUploadedFileSize" type="hidden" value="" />
            <input id="txtUploadedDocType" type="hidden" value="" />
        </div>
    </div>


    <script>
        $(document).ready(function () {
            var tab = '0';
            if (tab == 1) {
                $('.tab1').css('display', 'none');
                $('.tab2').css('display', 'block');
            }
            else if (tab == 2) {
                $('.tab1').css('display', 'block');
                $('.tab2').css('display', 'none');
            }
            else if (tab == 3) {
                $('.tab1').css('display', 'none');
                $('.tab2').css('display', 'none');
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#tab1').click(function () {
                $('#tabbox1').css('display', 'block !important');
            });
            $('#tab2').click(function () {
                $('#tabbox2').css('display', 'block !important');
            });
            $('#tab3').click(function () {
                $('#tabbox3').css('display', 'block !important');
            });
            $('#tab4').click(function () {
                $('#tabbox4').css('display', 'block !important');
            });
        });
    </script>
    <script type="text/javascript">

        $(function () {
            var gVideoUpload = $('#gVideoUpload');
            var gDocumentUpload = $('#gDocumentUpload');
            var gDiscussion = $('#gDiscussion');
            $('ul.tabset li a').on('click', function (e) {
                var href = $(this).attr('href');
                if (href === '#tabbox1') {
                    gVideoUpload.show();
                    gDocumentUpload.hide();
                    gDiscussion.hide();
                }
                else if (href === '#tabbox2') {
                    gDocumentUpload.show();
                    gVideoUpload.hide();
                    gDiscussion.hide();
                }
                else if (href === '#tabbox3') {
                    gDocumentUpload.hide();
                    gVideoUpload.hide();
                    gDiscussion.show();
                }
                else {
                    gDocumentUpload.hide();
                    gVideoUpload.hide();
                    gDiscussion.hide();
                }
                e.preventDefault();
                return false;
            });
            $('ul.tabset li').removeClass('hidden');

            $("#attachment-file").click(function () {
                $("#browse-attachment").show();
                $('#attachment_cancel').show();
                $(this).hide();
                return false;
            });

            $("#attachment_cancel").click(function () {
                /*For IE*/
                $("#comment_attachment").replaceWith($("#comment_attachment").clone(true));
                /*For other browsers*/
                $("#comment_attachment").val("");
                $("#browse-attachment").hide();
                $("#attachment-file").show();
                $(this).hide();
            });

            $('input[name="send_email"]').click(function () {
                if ($('input[name=send_email]:checked').val() == '2') {
                    $('#users-list').fadeIn(1500);
                } else {
                    $('#users-list').fadeOut(1500);
                }
            });

            getVideoComments();
            $('#gVideoUpload').click(function () {
                OpenFilePicker('video');
            });

            $('#gDocumentUpload').click(function () {
                OpenFilePicker('doc');
            });

            $('#docs-container').on('submit', 'form', function (event) {
                var $form = $(this);
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: $form.serialize(),
                    success: function () {
                        $form.closest('li').remove();
                        if ($('ul.video-docs li').length == 0) {
                            $('ul.video-docs').html('<li style="clear:left;">No Attachments have been added.</li>');
                        }
                        doDocumentSearchAjax();
                    }
                });
                event.preventDefault();
            });

            refreshAttachedDocumentList();
        });

        function OpenFilePicker(docType) {
            if (docType == 'doc' && window.isCurrentVideoPlaying == true) {
                if (!confirm('Uploading a document while your video is playing will restart the video.  Are you sure you want to upload a document?'))
                    return;
            }

            filepicker.setKey('A3w6JbXR2RJmKr3kfnbZtz');
            var uploadPath = '/tempupload/34/2015/05/21/';
            $('#txtUploadedFilePath').val("");
            $('#txtUploadedFileName').val("");
            $('#txtUploadedFileMimeType').val("");
            $('#txtUploadedFileSize').val("");
            $('#txtUploadedDocType').val(docType);

            var filepickerOptions;

            if (docType == 'video') {
                filepickerOptions = {
                    multiple: true,
                    services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX'],
                    extensions: ['3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma']
                }
            } else {
                filepickerOptions = {
                    multiple: true,
                    extensions: ['bmp', 'gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'swf', 'pdf', 'txt', 'docx', 'ppt', 'pptx', 'potx', 'xls', 'xlsx', 'xlsm', 'rtf', 'odt', 'doc'],
                    services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX']
                }
            }

            // fix for iOS 7
            if (isIOS()) {
                filepickerOptions.multiple = false;
            }

            // new window for iPhone
            if (!!navigator.userAgent.match(/iPhone/i)) {
                filepickerOptions.container = 'window';
            }

            filepicker.pickAndStore(
                    filepickerOptions, {
                        location: "S3",
                        path: uploadPath
                    },
            function (inkBlob) {
                if (inkBlob && inkBlob.length > 0) {
                    for (var i = 0; i < inkBlob.length; i++) {
                        var blob = inkBlob[i];
                        $('#txtUploadedFilePath').val(blob.key);
                        $('#txtUploadedFileName').val(blob.filename);
                        $('#txtUploadedFileMimeType').val(blob.mimetype);
                        $('#txtUploadedFileSize').val(blob.size);
                        if (docType == 'video') {
                            $('#temp-video-title').html(blob.filename);
                            $('ul.videos-list').prepend($('#temp-list').html());
                            PostHuddleVideo(docType);
                        } else {
                            $('#doc-title').html(blob.filename);
                            $('#doc-type').html(blob.mimetype);
                            $('#add-document-row').html($('#extra-row-li').html());
                            PostHuddleDocument();
                        }
                    }
                }
            },
                    function (FPError) {
                        var error_desc = 'Unkown Error';
                        //as per filepicker documentation these are possible two errors
                        if (FPError.code == 101) {
                            error_desc = 'The user closed the dialog without picking a file';
                        } else if (FPError.code = 151) {
                            error_desc = 'The file store couldnt be reached';
                        }

                        $.ajax({
                            type: 'POST',
                            data: {
                                type: 'Huddles',
                                id: '668',
                                error_id: FPError.code,
                                error_desc: error_desc,
                                docType: docType,
                                current_user_id: current_user_id
                            },
                            url: home_url + '/Huddles/logFilePickerError/',
                            success: function (response) {
                                //Do nothing.
                            },
                            errors: function (response) {
                                alert('Error occured while logging, please report the error code to the system administrator: ' + FPError.code);
                            }
                        });
                    }

            );
        }

        function PostHuddleVideo(doc_type) {
            var video_id_url = '';
            if (doc_type == 'doc' && $('ul.tabset li a.active').length > 0 && $('ul.tabset li a.active').html() == 'Videos' && $('#txtCurrentVideoID').length > 0) {
                video_id_url = "/" + $('#txtCurrentVideoID').val();
            }

            $.ajax({
                url: home_url + (doc_type == 'video' ? '/Huddles/uploadVideos' : '/Huddles/uploadDocuments') + '/668/34/117' + video_id_url,
                method: 'POST',
                success: function (data) {
                    doVideoSearchAjax();
                    if ($('#flashMessage').length == 0) {
                        $('#main').prepend('<div class="success" id="flashMessage" style="cursor: pointer;"><?php echo $alert_messages['sit_tight_your_video_processing']; ?></div>');
                        $('#flashMessage').click(function () {
                            $(this).css('display', 'none')
                        })
                    } else {
                        var html = $('#flashMessage').html();
                        $('#flashMessage').html(html + '<br/><?php echo $alert_messages['sit_tight_your_video_processing']; ?>');
                    }
                },
                data: {
                    video_title: $('#txtUploadedFileName').val(),
                    video_desc: '',
                    video_url: $('#txtUploadedFilePath').val(),
                    video_file_name: $('#txtUploadedFileName').val(),
                    video_mime_type: $('#txtUploadedFileMimeType').val(),
                    video_file_size: $('#txtUploadedFileSize').val(),
                    rand_folder: $('#txtVideoPopupRandomNumber').val()
                }
            });
        }

        function CloseVideoUpload(doc_type) {
            var ts = Math.round((new Date()).getTime() / 1000);
            var active = $('ul.tabset li a.active');

            if (doc_type == 'doc' && active.length > 0 && active.html() == 'Videos' && $('#txtCurrentVideoID').length > 0) {
                location.href = $('#txtCurrentVideoUrl').val() + '?ts=' + ts;
            } else if (doc_type == 'doc') {
                location.href = home_url + '/Huddles/view/668/2';
            } else {
                location.href = home_url + '/Huddles/view/668';
            }

        }

        function PostHuddleDocument() {
            showProcessOverlay($('#docs-container'));
            showProcessOverlay($('#tabbox2'));
            var video_id_url = "/" + $('#txtCurrentVideoID').val();
            $.ajax({
                url: home_url + '/Huddles/uploadDocuments' + '/668/34/117' + video_id_url,
                method: 'POST',
                success: function (data) {
                    if (parseInt(data) == 0) {
                        alert('Unable to save data, please try again');
                    } else {
                        CloseDocumentUpload();
                    }
                },
                data: {
                    video_title: $('#txtUploadedFileName').val(),
                    video_desc: "",
                    video_url: $('#txtUploadedFilePath').val(),
                    video_file_name: $('#txtUploadedFileName').val(),
                    video_mime_type: $('#txtUploadedFileMimeType').val(),
                    video_file_size: $('#txtUploadedFileSize').val(),
                    rand_folder: "0"
                }
            });
        }

        function CloseDocumentUpload() {
            refreshAttachedDocumentList();
            ob_refreshAttachedDocumentList();
            doDocumentSearchAjax(function () {
                hideProcessOverlay($('#tabbox2'));
            });
        }

        function refreshAttachedDocumentList() {
            if ($('#tab-area a.active').attr('href') != '#tabbox1')
                return;
            var videoInp = $('#txtCurrentVideoID');
            if (videoInp.length == 0)
                return;
            var url = home_url + '/MyFiles/getAttachedDocuments/' + videoInp.val() + '?t=' + (new Date()).getTime();
            $.getJSON(url).done(function (data) {
                var container = $('#docs-container');
                container.html(data.html);
                hideProcessOverlay(container);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                if (textStatus == 'error') {
                    alert('An error have just happened. The site will be refresh after you close this box.');
                    location.reload(true);
                }
            });
        }

    </script>
</div>

<script src="//static.filestackapi.com/v3/filestack.js"></script>
<div id="desk-support-box">
    <a id="desk-support-box-close">&otimes;</a>
    <iframe src="https://sibme.desk.com/customer/widget/emails/new" id="desk-widget-iframe"></iframe>
</div>
<div id="desk-darkness"></div>
<a class="desk-cta desk-sidetab" href="https://sibme.desk.com/customer/widget/emails/new">Contact Us</a>


<script src="https://desk-customers.s3.amazonaws.com/shared/email_widget.js" type="text/javascript"></script>

<!-- start Mixpanel -->
<script type="text/javascript">(function (e, b) {
            if (!b.__SV) {
                var a, f, i, g;
                window.mixpanel = b;
                a = e.createElement("script");
                a.type = "text/javascript";
                a.async = !0;
                a.src = ("https:" === e.location.protocol ? "https:" : "http:") + '//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';
                f = e.getElementsByTagName("script")[0];
                f.parentNode.insertBefore(a, f);
                b._i = [];
                b.init = function (a, e, d) {
                    function f(b, h) {
                        var a = h.split(".");
                        2 == a.length && (b = b[a[0]], h = a[1]);
                        b[h] = function () {
                            b.push([h].concat(Array.prototype.slice.call(arguments, 0)))
                        }
                    }
                    var c = b;
                    "undefined" !== typeof d ? c = b[d] = [] : d = "mixpanel";
                    c.people = c.people || [];
                    c.toString = function (b) {
                        var a = "mixpanel";
                        "mixpanel" !== d && (a += "." + d);
                        b || (a += " (stub)");
                        return a
                    };
                    c.people.toString = function () {
                        return c.toString(1) + ".people (stub)"
                    };
                    i = "disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
                    for (g = 0; g < i.length; g++)
                        f(c, i[g]);
                    b._i.push([a, e, d])
                };
                b.__SV = 1.2
            }
        })(document, window.mixpanel || []);
        mixpanel.init("bce282aad9b8a17fd9b963b136a81970");
</script>