<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
    $(document).ready(function () {
        $('#observation-date-picker').click(function () {
            $(this).datepicker({
                inline: true
            });
        });

    });
</script>
<div id="main" class="box container" style="overflow: visible;">
    <div class="header header-huddle">
        <div class="action-buttons btn-group right mmargin-top">
            <a href="/Huddles" class="btn" style="">Back to all Huddles</a>
            <a href="/Huddles/edit/668" class="btn icon2-pen" rel="tooltip" title="edit"></a>
            <a href="/Huddles/delete/668"
               data-confirm="Are you sure you want to delete this Huddle?" data-method="delete" class="btn icon2-trash" rel="tooltip" title="delete"></a>
        </div>

        <h2 class="title">Acme H.S. Science Department PLC </h2>
        <p class="title-desc">Science department PLC for 2014-15 school year.</p>

    </div>
    <div class="tab-box" style="overflow: visible;">
        <div id="tab-area">

            <div class="btn-box">
                <a id="gDocumentUpload" style="display: none;" class="btn btn-green icon2-video tab1" href="#">Upload Document</a>
                <a class="btn btn-green right" href="/Huddles/add" id="btn-new-huddle">New Observation</a>
                <a id="gDiscussion" href="/Huddles/view/668/3/add" class="btn btn-creat btn-blue" style="display: none;" >Create new discussion</a>
            </div>

            <ul class="tabset">
                <li class="">
                    <a id="tab1" class="tab" href="#tabbox2">Videos</a>
                </li>
                <li class="hidden">
                    <a id="tab2"  class="tab " href="#tabbox2">Documents</a>
                </li>
                <li>
                    <a id="tab3" class="tab " href="#tabbox3">Huddle Discussion</a>
                </li>
                </li>
                <li>
                    <a id="tab3" class="tab active"  href="#tabbox1">Observation</a>
                </li>
                <li>
                    <a id="tab4" class="tab " href="#tabbox4">People</a>
                </li>
            </ul>
            <div class="table-container">

                <div class="tab-content tab-active"  id="tabbox1">





                    <a href="#"><h3 class="heading-back-t-a">Back to all Observations</h3></a>



                    <form method="post" id="new_video_huddle" enctype="multipart/form-data" class="new_video_huddle"
                          action="/Huddles/add" accept-charset="UTF-8">
                        <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"></div>



                        <div class="span5">
                            <div class="row observetion-input-area" style="margin-bottom: 20px;">
                                <input id="observation-date-picker" class="size-big huddle-name new-observation-1-a" placeholder="Observation Date" size="30" type="text" required/>





                                <input id="observation-time-picker" class="size-big huddle-name new-observation-time time" placeholder="09:00 AM" size="30" type="text" required/>
                                <script type="text/javascript">
                                    $(function () {
                                        $('#observation-time-picker').timepicker();

                                    });
                                </script>


                                <div class="clear"></div>
                                <select class="observee-list-members-b">
                                    <option value="Denise Sanchez">Denise Sanchez</option>
                                    <option value="Peter Rosenquist">Peter Rosenquist</option>
                                    <option value="Denise Sanchez">Denise Sanchez</option>

                                </select>
                                </ul>
                            </div>

                            <div class="row observetion-input-area" style="margin-bottom: 20px;">



                                <h3>Notifications</h3>
                                <div class="clear"></div>
                                <input class="size-big huddle-name notifications-1-a" placeholder="30" size="30" type="text" required/>



                                <select class="observee-list-members-b notifications-1-a-time">
                                    <option value="Denise Sanchez">Minute</option>
                                    <option value="Peter Rosenquist">Hours</option>
                                    <option value="Denise Sanchez">Days</option>

                                </select>





                                </ul>
                            </div>






                        </div>























                        <div class="span5 float-right-ob">
                            <div style="margin-bottom: 20px;" class="row observetion-input-area">
                                <input type="text" required="" size="30" placeholder="Location" class="size-big huddle-name location-observer-a" >








                                <div class="clear"></div>



                                <div class="fancy-check-box-ob"><input type='checkbox' name='thing' value='valuable' id="thing2"/>
                                    <label for="thing2"></label><b>This is a private observation</b></div>

                            </div>




                    </form>





























                </div>
                <div style="clear: both;"></div>




                <h3 class="alret-people-a">Select people from your huddle, who are observing this observation</h3>
                <a style="margin-left: -8px;" id="pop-up-btn" data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="btn btn-small btn-green plus-Invite-main" href="#"><span class="plus-Invite">Invite</span>
                </a>

                <div class="row-fluid">
                    <div class="groups-table span12 observee-table-header-main">

                        <div class="span4 huddle-span4" style="margin-left:0px;">
                            <div class="groups-table-header observee-table-header">

                                <div style="width: 302px; padding: 0px;float: left;margin-left: 330px;" class="search-box">
                                    <div  id="header-container" class="filterform">
                                    </div>
                                </div>

                                <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                    <h3>Info</h3>
                                    <p>Admin: Can add/remove users; upload/download/copy/delete all videos and documents; clip all videos; add/edit/delete all video annotations and comments; create/participate/edit all Huddle discussions; delete Huddle.</p>
                                    <p>Members: Can upload/download/copy/delete videos and documents they add to the Huddle; clip their videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                    <p>Viewers: View videos and documents only.</p>
                                </div>
                                <div style="clear:both"></div>
                                <div class="select-all-none" style="float: left; margin-left: -8px; margin-top: 5px;">
                                    <label id="select-all-none" for="select-all"><input type="checkbox" name="select-all" id="select-all"/> <span id="select-all-label">Select All</span></label>
                                </div>

                                <div class="huddles-select-all-block">
                                    <span class="admin-radio"> Admin</span>


                                </div>
                                <div style="clear:both"></div>
                            </div>
                            <div class="groups-table-content">
                                <div class="widget-scrollable">
                                    <div class="scrollbar" style="height: 155px;">
                                        <div class="track" style="height: 155px;">
                                            <div class="thumb" style="top: 0px; height: 90.3195px;">
                                                <div class="end"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="viewport short">
                                        <div class="overview" style="top: 0px;">
                                            <div  id="people-lists">
                                                <ul id="list-containers" class="observee-list-admin">
                                                    <li>
                                                        <label><input class="viewer-user" type="checkbox" value="76" name="group_ids[]" id="group_ids_"> <a style="color: #757575; font-weight: normal;">Allyson Burnett</a></label>
                                                        <div class="permissions">
                                                            <label for="group_role_76_200"> Admin
                                                            </label>


                                                        </div>
                                                    </li>
                                                    <li>
                                                        <label for="super_admin_ids_418"><input class="super-user" type="checkbox" value="418" name="super_admin_ids[]" id="super_admin_ids_418"> <a style="color: #757575; font-weight: normal;">Andrew Towne</a></label>
                                                        <input type="hidden" value="hummingbird96@sbcglobal.net" name="super_admin_email_418" id="super_admin_email_418">
                                                        <div class="permissions">
                                                            <label for="user_role_418_200"> Admin
                                                            </label>
                                                        </div>
                                                    </li>

                                                    <li>
                                                        <label for="super_admin_ids_1050"><input class="member-user"  type="checkbox" value="1050" name="super_admin_ids[]" id="super_admin_ids_1050"> <a style="color: #757575; font-weight: normal;">Beth Cambell</a></label>
                                                        <div class="permissions">
                                                            <label for="user_role_1050_200"> Admin
                                                            </label>

                                                        </div>
                                                    </li>

                                                    <li>
                                                        <label for="super_admin_ids_1056"><input class="member-user"  type="checkbox" value="1056" name="super_admin_ids[]" id="super_admin_ids_1056"> <a style="color: #757575; font-weight: normal;"> Beth Cambell</a></label>
                                                        <div class="permissions">
                                                            <label for="user_role_1056_200"> Admin
                                                            </label>

                                                        </div>
                                                    </li>

                                                    <li>
                                                        <label for="super_admin_ids_1601"><input class="super-user" type="checkbox" value="1601" name="super_admin_ids[]" id="super_admin_ids_1601"> <a style="color: #757575; font-weight: normal;">Brian McGill</a></label>
                                                        <input type="hidden" value="brianm@sibme.com" name="super_admin_email_1601" id="super_admin_email_1601">
                                                        <div class="permissions">
                                                            <label for="user_role_1601_200"> Admin
                                                            </label>

                                                        </div>
                                                    </li>

                                                    <li>
                                                        <label for="super_admin_ids_1046"><input class="member-user"  type="checkbox" value="1046" name="super_admin_ids[]" id="super_admin_ids_1046"> <a style="color: #757575; font-weight: normal;"> Denise Sanchez</a></label>
                                                        <div class="permissions">
                                                            <label for="user_role_1046_200"> Admin
                                                            </label>

                                                        </div>
                                                    </li>




                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div><br/><br/>
                <div class="input-group mmargin-top">
                    <div id="editor2-toolbar" class="editor-toolbar" style="display: none;">
                        <a data-wysihtml5-command="bold">bold</a>
                        <a data-wysihtml5-command="italic">italic</a>
                        <a data-wysihtml5-command="insertOrderedList">ol</a>
                        <a data-wysihtml5-command="insertUnorderedList">ul</a>
                        <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                    </div>
                </div>
                <hr class="full">
                <div class="form-actions">
                    <input type="submit" id="btnSaveHuddle" value="Create Huddle" name="commit" class="btn btn-green"  >
                    <a class="btn btn-transparent" href="/Huddles">Cancel</a>
                </div>
                </form>

                <div id="addSuperAdminModal" class="modal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="header">
                                <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/add-user.png'); ?>" /> New User</h4>
                                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                            </div>
                            <form accept-charset="UTF-8" action="/Huddles/addUsers" enctype="multipart/form-data" method="post" name="admin_form" onsubmit="return false;" style="padding-top:0px;">
                                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                                <div id="flashMessage2" class="message error" style="display:none;"></div>
                                <div class="way-form">
                                    <h3>Add name and email</h3>
                                    <ol class="autoadd autoadd-sfont fmargin-list-left">
                                        <li>
                                            <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" required placeholder="Full Name" type="text" value="" /></label>
                                            <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" required placeholder="Email Address" type="email" value="" /></label>
                                            <a href="#" class="close">x</a>
                                        </li>
                                        <li>
                                            <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" placeholder="Full Name" type="text" value="" /></label>
                                            <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" placeholder="Email Address" type="email" value="" /></label>
                                            <a href="#" class="close">x</a>
                                        </li>
                                    </ol>
                                    <input id="controller_source" name="controller_source" type="hidden" value="video_huddles" />
                                    <input id="action_source" name="action" type="hidden" value="add" />
                                    <input id="action_source" name="user_type" type="hidden" value="110" />
                                    <button id="btnAddToAccount" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green fmargin-left" type="button">Add to Account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>





            <div class="tab-content tab-hidden" id="tabbox2">
                <div class="tab-box2">
                    <form id="huddle-download-form" action="/Huddles/downloadZip/668" method="post" style="margin-left:14px;">
                        <div class="tab-header">
                            <div class="tab-header__title"> <strong>Documents</strong>
                                <div style="clear: both;"></div>

                                <div class="docs-list__head2 span3" style="margin: 0px; margin-top: 9px;">
                                    <a style="cursor: pointer; margin-right: 30px; font-weight: normal;" id="btnDownloadAsZip" class="js-file-download blue-link icon-download2" onclick="return downloadItemsAsZip();">Download</a>
                                    <a  id="delete-account-doc" onclick="return deleteDoc();" style="cursor: pointer" class="icon-delete2 blue-link delete js-file-delete">Delete</a>
                                </div>
                            </div>

                            <div class="tab-header__right">
                                <div style="float:left;position:relative;">
                                    <input type="button" id="btnSearchDocuments" class="btn-search" value="">
                                    <input type="hidden" name="download_as_zip_docs" id="download_as_zip_docs" value=""/>
                                    <input type="hidden" name="huddle_id" id="huddle_id" value="668"/>
                                    <input class="text-input" type="text" value="" id="txtSearchDocuments" placeholder="Search Documents...">
                                    <span id="clearButton" style="display:none;right:40px;top:4px;position:absolute;cursor:pointer;" class="clear-input-box">X</span>
                                </div>
                                <div class="select"  style="float:left;margin-left:12px;">
                                    <select id="cmbDocumentSort" name="upload-date">
                                        <option value="Document.created_date DESC">Date Uploaded</option>
                                        <option value="afd.title ASC">Document Title</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form id="huddle-delete-form" action="/Huddles/deleteDocs/668" method="post">
                        <input type="hidden" id="document_ids" name="document_ids" value=""/>
                        <input type="hidden" id="account-folder-id" name="account_folder_id" value=""/>
                    </form>
                </div>
                <div class="tab-box2">
                    <div class="table">
                        <div id="extra-row-li" style="display: none">
                            <div class="padd">
                                <a rel="nofollow" data-confirm="Are you sure you want to delete this Resource?" id="delete-main-comments" class="icon-doc icon-indent blue-link delete">&nbsp;</a>
                                <label class="ui-checkbox checkbox-2">
                                    <input class="checkbox-2 download_doc_checkbox" disabled="disabled" name="document_ids[]" type="checkbox" value="">
                                </label>
                                <a href="#" class="nomargin-vertical wrap">
                                    <span class="image">&nbsp;</span> <span id="doc-title"></span>
                                </a>
                                <strong id="doc-type" class="type "></strong>
                                <strong id="doc-date" class="date">May 21, 2015</strong>
                                <div class="wrap" style="width: 75px; float: left;"> ----- </div>
                                <div class="doc_video_association">
                                    <div class="subject-row row">
                                        <div class="video-dropdown">Videos</div>
                                    </div>
                                </div>
                            </div>
                            <form id="delete-document0" action="/Huddles/deleteDocument/0" accept-charset="UTF-8"></form>
                            <input type="hidden" name="account_folder_id" id="huddle_id" value="668"/>
                        </div>

                        <div class="head">
                            <label class="ui-checkbox left" style="margin: 11px 12px 0 12px;">
                                <input class="head-checkbox" type="checkbox">
                            </label>
                            <strong class="name">Name</strong>
                            <strong class="type">Type</strong>
                            <strong class="date">Date Uploaded</strong>
                            <strong class="associated">Associated Video(s)</strong>
                        </div>

                        <ul class="ul-docs"></ul>
                    </div>
                </div>
            </div>


            <div class="tab-content tab-hidden" id="tabbox3" style="display: none">
                <div class="tab-3">
                    <h1>Huddle Discussion</h1>
                    <ul style="float:left;width:100%;">
                        <li>No Discussion started yet.</li>
                    </ul>
                </div>
            </div>
            <div class="tab-content tab-hidden" id="tabbox4" style="display: none">
                <div class="tab-header">
                    <h2 class="tab-header__title">People</h2>
                </div>

                <h4>Admins:</h4>
                <div class="video-huddle-users">
                    <a style="cursor: pointer;">
                        <img src="/img/users/735415012thumb.jpg" alt="Brian McGill" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Brian McGill</div>
                    </a>

                    <a style="cursor: pointer;">
                        <img src="/img/users/1460631889thumb.jpg" alt="Dave Wakefield" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Dave Wakefield</div>
                    </a>


                    <hr/>
                </div>

                <div class="video-huddle-users">
                    <h4>Members :</h4>
                    <a style="cursor: pointer;">
                        <img src="/img/users/546510_477201972292793_1867372539_n.jpeg" alt="Lindsey Anderson" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Lindsey Anderson</div>
                    </a>


                    <a style="cursor: pointer;">
                        <img src="/img/users/photo_3.jpeg" alt="Mark Jones" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Mark Jones</div>
                    </a>


                    <a style="cursor: pointer;">
                        <img src="/img/users/216594_10100676743986489_2074824_n.jpeg" alt="Tiffany Reeves" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Tiffany Reeves</div>
                    </a>


                    <a style="cursor: pointer;">
                        <img src="/img/users/383749_10150408531063244_830231116_n.jpeg" alt="Leah Riddle" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Leah Riddle</div>
                    </a>


                    <a style="cursor: pointer;">
                        <img src="/img/users/302950_10150324259541445_2075933453_n.jpeg" alt="Oscar Rodriguez" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Oscar Rodriguez</div>
                    </a>


                    <a style="cursor: pointer;">
                        <img src="/img/users/peter_clayburgh.jpeg" alt="Peter Rosenquist" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Peter Rosenquist</div>
                    </a>


                    <a style="cursor: pointer;">
                        <img src="/img/users/392321_10150571467603916_1423098013_n.jpeg" alt="Denise Sanchez" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Denise Sanchez</div>
                    </a>


                    <a style="cursor: pointer;">
                        <img src="/img/users/225560c.jpeg" alt="Andrew Towne" class="photo inline" rel="image-uploader" height="53" width="53" />                                        <br/>
                        <div style="clear:both">Andrew Towne</div>
                    </a>


                </div>
                <hr/>

                <div class="video-huddle-users">
                    <h4>Viewers :</h4>
                    <a style="cursor: pointer;">
                        <img width="53" height="53"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo inline" rel="image-uploader" alt="Photo-default">
                        <br/>
                        <div style="clear:both">Jerry Karry</div>
                    </a>
                </div>
                <hr/>
            </div>
        </div>
        <input id="txtUploadedFilePath" type="hidden" value="" />
        <input id="txtUploadedFileName" type="hidden" value="" />
        <input id="txtUploadedFileMimeType" type="hidden" value="" />
        <input id="txtUploadedFileSize" type="hidden" value="" />
        <input id="txtUploadedDocType" type="hidden" value="" />
    </div>
</div>


<script>
    $(document).ready(function () {
        var tab = '0';
        if (tab == 1) {
            $('.tab1').css('display', 'none');
            $('.tab2').css('display', 'block');
        }
        else if (tab == 2) {
            $('.tab1').css('display', 'block');
            $('.tab2').css('display', 'none');
        }
        else if (tab == 3) {
            $('.tab1').css('display', 'none');
            $('.tab2').css('display', 'none');
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#tab1').click(function () {
            $('#tabbox1').css('display', 'block !important');
        });
        $('#tab2').click(function () {
            $('#tabbox2').css('display', 'block !important');
        });
        $('#tab3').click(function () {
            $('#tabbox3').css('display', 'block !important');
        });
        $('#tab4').click(function () {
            $('#tabbox4').css('display', 'block !important');
        });
    });
</script>

</div>