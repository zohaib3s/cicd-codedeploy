<?php
if (isset($observation)) {
    $date = new DateTime($observation['Observations']['observation_date_time']);
    $observation_date = $date->format("m/d/Y");
    $date = new DateTime($observation['Observations']['observation_date_time']);
    $observation_time = strtoupper($date->format("H:i a"));
} else {
    $observation_date = '';
    $observation_time = '';
}
if (isset($observation['acc_folder']['name'])) {
    $huddles = explode('-', $observation['acc_folder']['name']);
    $huddle_name = $huddles[1];
} else {
    $huddle_name = '';
}
//echo "<pre>";
//print_r($observation);
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<form method="post" id="frm_new_observation" enctype="multipart/form-data" class="new_video_huddle" action="<?php echo $action; ?>" accept-charset="UTF-8">
    <?php
    if (isset($observation) && !empty($observation)) {
        ?>
        <input type="hidden" name="account_folder_id" value="<?php echo $observation['acc_folder']['account_folder_id']; ?>" />
        <input type="hidden" name="account_folder_observation_id" value="<?php echo $observation['Observations']['account_folder_observation_id']; ?>" />
    <?php } ?>
    <input type="hidden" id="account_id" name="account_id" value="<?php echo $account_id; ?>"/>
    <div class="container box newCoachingHuddle">
        <h1><?php echo $Page; ?> Observation</h1>
        <hr>
        <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"></div>
        <div class="span5">
            <div class="row observetion-input-area" style="margin-bottom: 20px;">
                <input id="observation-date-picker" class="size-big huddle-name new-observation-1-a" placeholder="Observation Date" size="30" type="text" name="observation_date_time" value="<?php echo $observation_date; ?>" required tabindex="0"/>
                <input id="observation-time-picker" name="observation-time-picker" class="size-big huddle-name new-observation-time time" placeholder="09:00 AM" size="30" type="text" value="<?php echo $observation_time; ?>" required tabindex="1"/>
                <div class="clear"></div>
                <div class="wrapper-dropdown-2" id="shhuddles" style="margin-top: 57px;" >
                    <input type="text" placeholder="Add Huddle" id="txthuddles" name="txthuddles" value="<?php echo $huddle_name; ?>" tabindex="4" />
                    <input type="hidden" id="accflid" name="accflid" name="accflid" value="<?php echo isset($observation['Observations']['huddle_account_folder_id']) ? $observation['Observations']['huddle_account_folder_id'] : ''; ?>" />
                    <ul class="dropdown" id="ddlhuddles">
                    </ul>
                </div>
            </div>
            <div class="row observetion-input-area" style="margin-bottom: 20px;">
                <h3>Notifications</h3>
                <div class="clear"></div>
                <input class="size-big huddle-name notifications-1-a" placeholder="30" size="30" value="<?php echo isset($observation['notify_at']) ? $observation['notify_at'] : ''; ?>" type="text" name="notification_duration"  required tabindex="6" />
                <select class="observee-list-members-b notifications-1-a-time" name="notification_type" style="height:33px" tabindex="7">
                    <option value="1" <?php echo isset($observation['notify_at_unit']) && $observation['notify_at_unit'] == 1 ? 'selected' : ''; ?>>Minute</option>
                    <option value="2" <?php echo isset($observation['notify_at_unit']) && $observation['notify_at_unit'] == 2 ? 'selected' : ''; ?>>Hours</option>
                    <option value="3" <?php echo isset($observation['notify_at_unit']) && $observation['notify_at_unit'] == 3 ? 'selected' : ''; ?>>Days</option>
                </select>
                </ul>
            </div>
        </div>
        <div class="span5 float-right-ob">
            <div style="margin-bottom: 20px;" class="row observetion-input-area">
                <input type="text"  size="30" placeholder="Location" class="size-big huddle-name location-observer-a" name="txtlocation" value="<?php echo isset($observation['Observations']['location_name']) ? $observation['Observations']['location_name'] : ''; ?>" required tabindex="3">
                <div class="clear"></div>
                <select class="observee-list-members-b" id="sel_observee" name="observee" required style="height:33px;" tabindex="5">
                    <?php if (!isset($observation['Users'])) { ?>
                        <option value="" >Select Observee</option>
                        <?php
                    } else {
                        foreach ($observation['Users'] as $observee) {
                            if ($observee['huddle_users']['user_id'] == $observation['Observee'][0]['User']['id']) {
                                ?>
                                <option value="<?php echo $observee['User']['id']; ?>" selected="selected"><?php echo $observee['User']['first_name'] . ' ' . $observee['User']['last_name']; ?></option>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $observee['User']['id']; ?>" ><?php echo $observee['User']['first_name'] . ' ' . $observee['User']['last_name']; ?></option>
                                <?php
                            }
                        }
                    }
                    ?>


                </select>
                <div class="clear"></div>
                <div class="fancy-check-box-ob"><input type="checkbox" name="thing" value="1" id="thing2" name="isprivate" <?php echo isset($observation['Observations']['is_private']) && $observation['Observations']['is_private'] == 1 ? 'checked' : ''; ?>>
                    <label for="thing2" tabindex="8"></label><b>This is a private observation</b></div>
            </div>

        </div>
        <div style="clear: both;"></div>
        <h3 class="alret-people-a">Select people from your huddle, who are observing this observation</h3>
        <a style="margin-left: -8px;" id="pop-up-btn" data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="btn btn-small btn-green plus-Invite-main" href="#" tabindex="8"><span class="plus-Invite">Invite</span>
        </a>
        <div class="row-fluid">
            <div class="groups-table span12 observee-table-header-main">
                <div class="span4 huddle-span4" style="margin-left:0px;">
                    <div class="groups-table-header observee-table-header">
                        <div style="width: 302px; padding: 0px;float: left;margin-left: 330px;" class="search-box">
                            <div  id="header-container" class="filterform">
                            </div>
                        </div>
                        <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                            <h3>Info</h3>
                            <p>Admin: Can add/remove users; upload/download/copy/delete all videos and documents; clip all videos; add/edit/delete all video annotations and comments; create/participate/edit all Huddle discussions; delete Huddle.</p>
                            <p>Members: Can upload/download/copy/delete videos and documents they add to the Huddle; clip their videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                            <p>Viewers: View videos and documents only.</p>
                        </div>
                        <div style="clear:both"></div>
                        <div class="select-all-none" style="float: left; margin-left: -8px; margin-top: 9px;">
                            <input type='checkbox' class="selectall" name='thing' value='valuable' id="selectAll"/>
                            <label for="thing" id="select-all-none" for="select-all">Select All</label>
                        </div>
                        <div class="huddles-select-all-block">
                            <span class="admin-radio"></span>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="groups-table-content">
                        <div class="widget-scrollable">
                            <div class="scrollbar" style="height: 155px;">
                                <div class="track" style="height: 155px;">
                                    <div class="thumb" style="top: 0px; height: 90.3195px;">
                                        <div class="end"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="viewport short">
                                <div class="overview" style="top: 0px;">
                                    <div  id="people-lists">
                                        <ul id="list-containers" class="observee-list-admin">


                                            <?php if (!isset($observation['Users'])) { ?>
                                                <li>
                                                    <label><a style="color: #757575; font-weight: normal;">No Observer.</a></label>
                                                </li>
                                                <?php
                                            } else {
                                                foreach ($observation['Users'] as $observer) {
                                                    ?>
                                                    <li id="<?php echo $observer['User']['id']; ?>" class="<?php
                                                    if ($observer['User']['id'] == $observation['Observee'][0]['User']['id']) {
                                                        echo 'remove';
                                                    }
                                                    ?>">
                                                        <?php if ($observer['User']['id'] == $user_id) { ?>
                                                            <input class="viewer-user" type="checkbox" value="<?php echo $observer['User']['id']; ?>" name="group_ids[]" id="thing<?php echo $observer['User']['id']; ?>" onclick="return false" >
                                                        <?php } else { ?>
                                                            <input class="viewer-user chkbox thing92" type="checkbox" value="<?php echo $observer['User']['id']; ?>" name="group_ids[]" id="thing<?php echo $observer['User']['id']; ?>" >
        <?php } ?>
                                                        <label for="thing<?php echo $observer['User']['id']; ?>">
                                                            <a style="color: #757575; font-weight: normal;"><?php echo $observer['User']['first_name'] . ' ' . $observer['User']['last_name']; ?></a>
                                                        </label>

                                                        <div class="permissions">
                                                            <label for="group_role_76_200">
                                                                <?php
                                                                if ($observer['huddle_users']['role_id'] == 200) {
                                                                    echo 'Admin';
                                                                } elseif ($observer['huddle_users']['role_id'] == 210) {
                                                                    echo 'Member';
                                                                } else {
                                                                    echo 'Viewer';
                                                                }
                                                                ?>
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                        <?php
                                        if (isset($observation['Observer'])) {
                                            $observer_ids = '';
                                            foreach ($observation['Observer'] as $observer) {
                                                $observer_ids.=$observer['User']['id'] . ',';
                                                ?>
                                                <script type="text/javascript" >

                                                    $("#" +<?php echo $observer['User']['id']; ?> + " input").attr('checked', true);

                                                </script>
    <?php }
    ?>
                                            <input type="hidden" id="unchecked_ids" name="unchecked_ids" value="" />
                                            <input type="hidden" id="checked_ids" name="checked_ids" value="<?php echo $observer_ids; ?>" />
<?php }
?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div><br/><br/>
        <div class="input-group mmargin-top">
            <div id="editor2-toolbar" class="editor-toolbar" style="display: none;">
                <a data-wysihtml5-command="bold">bold</a>
                <a data-wysihtml5-command="italic">italic</a>
                <a data-wysihtml5-command="insertOrderedList">ol</a>
                <a data-wysihtml5-command="insertUnorderedList">ul</a>
                <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
            </div>
        </div>
        <hr class="full">
        <div class="form-actions">
            <input type="submit" id="btnSaveHuddle" value="<?php echo $Page; ?> Observation" name="commit" class="btn btn-green" tabindex="9"  >
            <a class="btn btn-transparent" href="<?php echo $this->base; ?>/Observe" tabindex="10">Cancel</a>
        </div>
</form>
</div>
<div id="addSuperAdminModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/add-user.png'); ?>" /> New User</h4>
                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
            </div>
            <form accept-charset="UTF-8" action="<?php echo $this->base; ?>/Huddles/addUsers" enctype="multipart/form-data" method="post" name="admin_form" onsubmit="return false;" style="padding-top:0px;">
                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                <div id="flashMessage2" class="message error" style="display:none;"></div>
                <div class="way-form">
                    <h3>Add name and email</h3>
                    <ol class="autoadd autoadd-sfont fmargin-list-left">
                        <li>
                            <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" required placeholder="Full Name" type="text" value="" /></label>
                            <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" required placeholder="Email Address" type="email" value="" /></label>
                            <a href="#" class="close">x</a>
                        </li>
                        <li>
                            <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" placeholder="Full Name" type="text" value="" /></label>
                            <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" placeholder="Email Address" type="email" value="" /></label>
                            <a href="#" class="close">x</a>
                        </li>
                    </ol>
                    <input id="controller_source" name="controller_source" type="hidden" value="video_huddles" />
                    <input id="action_source" name="action" type="hidden" value="add" />
                    <input id="action_source" name="user_type" type="hidden" value="110" />
                    <button id="btnAddToAccount2" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green fmargin-left" type="button">Add to Account</button>
                </div>
            </form>
        </div>
    </div>
</div>
</form>

