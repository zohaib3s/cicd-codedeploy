<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$loggedInUserRole = '';

$params = $this->request->pass;
if (is_array($params) && count($params) > 0) {
    $sortParam = $params[0];
    $filterParam = $params[1];
} else {
    $sortParam = 'all';
    $filterParam = 'all';
}
?>
<div id="main" class="container" >
    <?php if (isset($total_observ) && $total_observ > 0): ?>
        <div style="width:100%;text-align: center;" id="loader-gif-div">
            <img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/obs_loader.gif'); ?>" alt='Loading'/>
        </div>
        <div class="observations-grids-main" id="observations-grids-main">
            <script type="text/javascript">
                $(document).ready(function () {
                    function loadData(list_page) {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo $this->base . '/Observe/getAjaxObservations' ?>",
                            data: {page: list_page, sort: '<?php echo $sortParam; ?>', filter: '<?php echo $filterParam; ?>'},
                            success: function (msg) {
                                $('#loader-gif-div').hide();
                                $("#observations-grids-main").html(msg);
                            }
                        });
                    }

                    loadData(1);  /*// For first time list_page load default results*/
                    loadData(1);  /*// For first time list_page load default results*/
                    $(document).on('click', '#observations-grids-main .pagination li.active', function () {
                        $("#observations-grids-main").html('');
                        $('#loader-gif-div').show();
                        var list_page = $(this).attr('p');
                        loadData(list_page);
                    });
                });
            </script>
        </div>
    <?php else: ?>
        <div class="text-center">
            <h2 class="heading--info">No Observations</h2>
        </div>
        <a href="#" id="view_browser_notification" style="display: none;" data-toggle="modal" data-target="BrowserMessageModal">browser message</a>
        <div id="BrowserMessageModal" class="modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="header">
                        <h4 class="header-title nomargin-vertical smargin-bottom">Upgade Your Browser</h4>
                        <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                    </div>

                    <p>You're Using an unsupported browser. Please upgrade your browser to view <?php $this->Custom->get_site_settings('site_title') ?> Application</p>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<script type="text/javascript">
    $(".widget-scrollable").tinyscrollbar();
    $("[rel~=tooltip]").tooltip({container: 'body'});
</script>