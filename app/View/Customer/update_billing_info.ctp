<?php
$user_current_account = $this->Session->read('user_current_account');
?>
<style>
    .nav-tabs a{
        padding: 4px 17px;
    }
</style>
<style>
    .infobox-container {
        position: relative;
        display: inline-block;
        margin: 0;
        padding: 0;
        width: auto;
    }
    .infobox {
        width: 250px;
        padding: 10px 5px 5px 5px;
        margin:10px;
        position: relative;
        z-index: 90;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0px 0px 3px rgba(0,0,0,0.55);
        -moz-box-shadow: 0px 0px 3px rgba(0,0,0,0.55);
        box-shadow: 0px 0px 3px rgba(0,0,0,0.55);
        background: #424242;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#6a6b6b), to(#424242));
        background-image: -moz-linear-gradient(top,#6a6a6a,#424242);
        color: #fff;
        font-size: 90%;
    }
    .infobox h3 {
        position: relative;
        width: 270px;
        color: #fff;
        padding: 10px 5px;
        margin: 0;
        left: -15px;
        z-index: 100;
        -webkit-box-shadow: 0px 0px 3px rgba(0,0,0,0.55);
        -moz-box-shadow: 0px 0px 3px rgba(0,0,0,0.55);
        box-shadow: 0px 0px 3px rgba(0,0,0,0.55);
        background: #3198dd;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#33acfc), to(#3198dd));
        background-image: -moz-linear-gradient(top,#33acfc,#3198dd);
        font-size: 160%;
        text-align: center;
        text-shadow: #2187c8 0 -1px 1px;
        font-weight: bold;
    }

    .infobox-container .triangle-l {
        border-color: transparent #2083c2 transparent transparent;
        border-style:solid;
        border-width:13px;
        height:0;
        width:0;
        position: absolute;
        left: -12px;
        top: 45px;
        z-index: 0; /* displayed under infobox */
    }
    .infobox-container .triangle-r {
        border-color: transparent transparent transparent #2083c2;
        border-style:solid;
        border-width:13px;
        height:0;
        width:0;
        position: absolute;
        left: 266px;
        top: 45px;
        z-index: 0; /* displayed under infobox */
    }
    .infobox a {
        color: #35b0ff;
        text-decoration: none;
        border-bottom: 1px dotted transparent;
    }
    .infobox a:hover, .infobox a:focus {
        text-decoration: none;
        border-bottom: 1px dotted #35b0ff;
    }
</style>

<script type="text/javascript">
    $(document).ready(function (e) {
        if ($('#prevous').is(':checked')) {
            $('.check-out-type').val($('#prevous').val());
            $('#use-previous-info').css('display', 'block');
            $('#use-new-info').css('display', 'none');

        }
        if ($('#new').is(':checked')) {
            $('.check-out-type').val($('#new').val());
            $('#use-previous-info').css('display', 'none');
            $('#use-new-info').css('display', 'block');

        }
        $('#prevous').click(function (e) {
            $('.check-out-type').val($('#prevous').val());
            $('#use-previous-info').css('display', 'block');
            $('#use-new-info').css('display', 'none');
        })
        $('#new').click(function (e) {
            $('.check-out-type').val($('#new').val());
            $('#use-previous-info').css('display', 'none');
            $('#use-new-info').css('display', 'block');
        })

    });
</script>
<div class="container">
    <nav class="nav nav-tabs" style="margin-top: 0px;">
        <ul>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 1): ?> class="active"  <?php endif; ?> ><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/1" ?>">School/District/Institution Settings</a></li><?php endif; ?>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 2): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/2" ?>">Colors &amp; Logo</a></li><?php endif; ?>
            <li <?php if (isset($tab) && $tab == 7): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/7" ?>">Archiving</a></li>
            <li <?php if (isset($tab) && $tab == 3): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/3" ?>">Plans</a></li>
            <li <?php if (isset($tab) && $tab == 4): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/customer/updateBillingInfo/" . $account_id . "/4" ?>">Billing Information</a></li>
            <?php if ($sample_huddle_not_exists): ?>
                <li <?php if (isset($tab) && $tab == 6): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/6" ?>">Restore sample huddle</a></li>
            <?php endif; ?>
            <li <?php if (isset($tab) && $tab == 5): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/5" ?>">Cancel Account</a></li>
        </ul>
    </nav>
</div>
<div id="main" class="container box">
    <p style="font-size: 20px;"></p>
    <div class="header">
        <h2 class="header-title">Billing Information</h2>
    </div>
    <?php
    $new_checked = '';
    $prev_checked = '';
    if (isset($customer_info['plan_info']['Plans']) && count($customer_info['plan_info']['Plans']) > 0 && isset($customer_info['card_info']) && count($customer_info['card_info']) > 0):
        $prev_checked = 'checked="checked"';
    else:
        $new_checked = 'checked="checked"';
    endif;
    ?>
    <div class="span12">
        <div class="input-group">
            <input type="radio" id='prevous' <?php if (empty($customer_info['plan_info'])) echo "disabled='disabled'"; ?> <?php echo $prev_checked; ?> class="radio" value="old" name="subscription_type">&nbsp;&nbsp;Use Previous Card Information
            <input type="radio" id='new' <?php echo $new_checked; ?> class="radio" value="new" name="subscription_type">&nbsp;&nbsp;Use New Card Information
        </div>
    </div>
    <?php if ($customer_info && count($customer_info) > 0): ?>
        <?php
        if (!empty($customer_info['plan_info'])) :
            ?>
            <div class="span12" id="use-previous-info">
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/customer/genrateSubscription/' . $account_id . '/4' ?>" method="post">
                    <input type="hidden"  name="check_out_type" value="" class="check-out-type"/>
                    <div class="input-group">
                        <label>Previous Plan Title</label>
                        <input class="size-medium" type="text" name="plan" readonly="readonly" value=" <?php echo ($customer_info['plan_info']['Plans']['plan_type'] != '') ? $customer_info['plan_info']['Plans']['plan_type'] : ''; ?>"/>
                        <input class="size-medium" type="hidden" name="planId" readonly="readonly" value=" <?php echo ($new_plan['plan_id'] != '') ? $new_plan['plan_id'] : ''; ?>"/>
                    </div>

                    <div class="input-group">
                        <label>Card Number</label>
                        <input class="size-medium" type="text" name="card_mask" readonly="readonly" value="<?php echo ($customer_info['card_info']->maskedNumber != '' ? $customer_info['card_info']->maskedNumber : ''); ?>"/><?php
                        echo $this->Html->image($customer_info['card_info']->imageUrl, array('style ="border:0px;margin-left:5px;"'));
                        ?>
                        <input class="size-medium" type="hidden" name="token" readonly="readonly" value=" <?php echo ($customer_info['card_info']->token != '') ? $customer_info['card_info']->token : ''; ?>"/>
                    </div>
                    <div class="input-group">
                        <input type="submit" value="Update Plan" class="btn btn-green"/>
                        <a href="<?php echo $this->base . '/accounts/accountSettings/' . $account_id . "/3" ?>" class="btn btn-gray">Cancel</a>
                    </div>
                </form>
            </div>
            <?php
        endif;
        ?>
    <?php endif; ?>
    <div class="span12" id="use-new-info"<?php if ($customer_info && count($customer_info) > 0): ?>style="display: none"<?php endif; ?>>
        <form accept-charset="UTF-8" action="<?php echo $this->base . '/customer/genrateSubscription/' . $account_id . '/4' ?>" method="post">
            <input type="hidden" name="check_out_type" value="" class="check-out-type"/>
            <div style="margin:0;padding:0;display:inline">
                <input name="utf8" type="hidden" value="&#x2713;" />
                <input name="authenticity_token" type="hidden" value="PPjtnY2j9d5pyK8cDfEYosjUKHYEmTf1sd+MX0kurf0=" />
            </div>
            <div class="span7">
                <p>
                    <label>
                        <b>Card Number</b>
                    </label>
                </p>
                <div class="input-group">
                    <span class=""><input class="size-medium" id="credit_card_number" name="credit_card[number]" size="30" type="text" required /></span>
                </div>
                <div style="clear: both;"></div>
                <hr>
                <input id="credit_card_expiration_date" name="credit_card[expiration_date]" type="hidden" />
                <p><label><b>Expiration Date</b></label></p>

                <div class="input-group select inline">
                    <select required id="month_select" name="card_expiration_month">
                        <option value="">Month:</option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>
                <div class="input-group select inline" style="margin-left: 0px;">
                    <select required id="year_select" name="card_expiration_year">
                        <option value="2000">Year:</option>
                        <option value="2012">2012</option>
                        <option value="2013">2013</option>
                        <option value="2014">2014</option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                    </select>
                </div>
            </div>
            <?php if (isset($customer_info['plan_info']['Plans']) && count($customer_info['plan_info']['Plans']) > 0 && isset($customer_info['card_info']) && count($customer_info['card_info']) > 0): ?>
                <div class="span5 infobox-container">
                    <div class="triangle-l"></div>
                    <div class="triangle-r"></div>
                    <div class="infobox">
                        <h3><span>Previous Plan</span></h3>
                        <p>Plan Title : <?php echo $customer_info['plan_info']['Plans']['plan_type']; ?><p/>
                        <p>Card Type  : <?php echo $customer_info['card_info']->cardType; ?></p>
                        <p>Card Mask  : <?php echo $customer_info['card_info']->maskedNumber; ?></p>

                    </div>
                </div>
            <?php endif; ?>
            <div style="clear: both;"></div>
            <hr>
            <div class="span7">
                <div class="input-group">
                    <p>
                        <label>
                            <b>CVV</b>
                        </label>
                    </p>
                    <input class="size-small" id="credit_card_cvv" required name="credit_card[cvv]" size="30" type="text" />

                    <a href="#" class="appendix">?</a>
                    <div class="appendix-content card" style="left: 148px">
                        <div class="media">
                            <div class="img"><img alt="Card1" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/card1.jpg'); ?>" /></div>
                            <div class="body"><b>Visa, Mastercard & Discover</b><br>
                                The verification code is a three-digit number on the back of your credit card next to the signature
                                box.
                            </div>
                        </div>
                        <div class="media">
                            <div class="img"><img alt="Card2" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/card2.jpg'); ?>" /></div>
                            <div class="body"><b>American Express</b><br>
                                The verification number is a four-digit number on the front of the card above the credit card number
                                on either the left or right side of your American Express card.
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="span12">
                <div class="span2" style="margin-left: 0px;">
                    <img alt="Braintree-gray" class="braintree" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/braintree-gray.png'); ?>" />
                </div>
                <div class="span6 braintree-p">
                    Our payment billing is handled by our e-commerce partner Braintree.
                    <br/>
                    Payments on your credit card will be shown from Braintree on our behalf.
                </div>
            </div>
            <div style="clear: both;"></div>
            <hr>
            <div style="clear: both;"></div>
            <input class="size-medium" type="hidden" name="planId" readonly="readonly" value=" <?php echo ($new_plan['plan_id'] != '') ? $new_plan['plan_id'] : ''; ?>"/>
            <div class="span12">
                <div style="clear: both;"></div>
                <input id="tr_data" name="tr_data" type="hidden" value="14a49f84c1a9ee723f553bb8055abdcd29539b2d|api_version=2&amp;credit_card%5Bcustomer_id%5D=60750998&amp;kind=create_payment_method&amp;public_key=44g8y8krfcrdyygn&amp;redirect_url=http%3A%2F%2Fapp.sibme.com%2Fcustomer%2Fconfirm_update_billing_info&amp;time=20130630113243" />
                <div class="form-actions lmargin-left lmargin-top">
                    <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green">Save Changes</button>
                    <a href="<?php echo $this->base . '/Dashboard/home' ?>" class="btn btn-transparent">Cancel</a>
                </div>
            </div>
        </form>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function () {
        var month = $('#month_select').val();
        var year = $('#year_select').val();
        $('#month_select').change(function () {
            month = $('#month_select').val();
            $("#credit_card_expiration_date").val(month + '/' + year);
        });
        $('#year_select').change(function () {
            year = $('#year_select').val();
            $("#credit_card_expiration_date").val(month + '/' + year);
        });


    });
</script>