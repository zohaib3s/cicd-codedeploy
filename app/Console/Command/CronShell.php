<?php

app::uses('AppController', 'Controller');
app::uses('CronController', 'Controller');
app::uses('ArchiveController', 'Controller');
app::uses('MailchimpController', 'Controller');
app::uses('ZencoderCronController', 'Controller');
app::uses('AnalyticalReportsController', 'Controller');
app::uses('ApiController', 'Controller');

class CronShell extends AppShell {

    public function startup() {
        $this->cron = new CronController();
        $this->archieve = new ArchiveController();
        $this->mailchimp = new MailchimpController();
        $this->zencoder = new ZencoderCronController();
        $this->analytical = new AnalyticalReportsController();
        $this->api = new ApiController();
        $this->app = new AppController();
    }

    public function main() {
        $this->out('Accessed Successfully!');
    }
    
    public function edTPA_assessments_transfer() {
        $this->out($this->app->edTPA_assessments_transfer());
    }

    public function syncUsersWithHubspot() {
        $this->out($this->app->syncUsersWithHubspot());
    }

    public function insert_collaboration_huddle_data() {
        $this->out($this->app->insert_collaboration_huddle_data());
    }
    
    public function insert_workspace_old_videos_data() {
        $this->out($this->app->insert_workspace_old_videos_data());
    }

    public function add_video_duration_cron() {
        $this->out($this->app->add_video_duration_cron());
    }

    public function migrate_accounts_cron() {
        $this->out($this->app->migrate_accounts_cron());
    }

    public function shell_video_stitch_forced() {
        $this->out($this->api->video_stitch_forced_all());
    }

    public function cron_unpublished_error_videos() {
        $this->out($this->api->cron_unpublished_error_videos());
    }

    public function cron_unpublished_error_mov_videos() {
        $this->out($this->api->cron_unpublished_error_mov_videos());
    }

    public function cron_unpublished_error_mpfour_temp_videos() {
        $this->out($this->api->cron_unpublished_error_mpfour_temp_videos());
    }

    public function cron_unpublished_error_mpfour_tempupload_videos() {
        $this->out($this->api->cron_unpublished_error_mpfour_tempupload_videos());
    }

    public function temp_cron_unpublished_error_mpfour_temp_videos() {
        $this->out($this->api->temp_cron_unpublished_error_mpfour_temp_videos());
    }

    public function shell_cron_test() {
        $this->out($this->cron->test());
    }

    public function archieve_test() {
        $this->out($this->archieve->test());
    }

    public function mailchimp_test() {
        $this->out($this->mailchimp->test());
    }

    public function zencoder_test() {
        $this->out($this->zencoder->test());
    }

    public function analytical_test() {
        $this->out($this->analytical->test());
    }
    
    public function cron_for_stoping_live_videos() {
        $this->out($this->api->cron_for_stoping_live_videos());
    }

    /*
     * Running Cron Controller functions.
     * Test function Command > into App dir > cake cron cron_test
     * */

    public function shell_live_stream_force_stop() {
        $resut = $this->cron->live_stream_force_stop();
        if ($resut) {
            $this->out('Success');
        } else {
            $this->out('Error');
        }
    }
    
     public function shell_observations_force_stop() {
        $resut = $this->cron->observations_force_stop();
        if ($resut) {
            $this->out('Success');
        } else {
            $this->out('Error');
        }
    }

    /*
     * Running Archive Controller functions.
     * Test function Command > into App dir > cake cron archieve_test
     * */

    public function shell_archive_check_for_amazon() {
        $result = $this->archieve->archive_check_for_amazon();
        if ($result) {
            $this->out('Success');
        } else {
            $this->out('Error');
        }
    }

    /*
     * Running mailChimp Controller functions.
     * Test function Command > into App dir > cake cron mailchimp_test
     * */

    public function shell_syncWithMailchimpGlobal() {
        $result = $this->mailchimp->syncWithMailchimpGlobal();
        if ($result) {
            $this->out('Success');
        } else {
            $this->out('Error');
        }
    }

    /*
     * Running ZencoderNotification Controller functions.
     * Test function Command > into App dir > cake cron zencoder_test
     * */

    public function shell_received_notification() {

        $result = $this->zencoder->received_notification($this->args[0]);
        if (!empty($result)) {
            $this->out('Success');
        } else {
            $this->out('Error');
        }
    }

    /*
     * Running AnalyticalReports Controller functions.
     * Test function Command > into App dir > cake cron analytical_test
     * */

    public function shell_weekly_report() {
        $result = $this->analytical->weekly_report();
        if ($result) {
            $this->out('Success');
        } else {
            $this->out('Error');
        }
    }

    public function shell_monthly_report() {
        $result = $this->analytical->monthly_report();
        if ($result) {
            $this->out('Success');
        } else {
            $this->out('Error');
        }
    }

    /*
     * Function Name : get_sendgrid_all_templates
     * Update SendGrid coentents on local database* */

    public function shell_update_sendgrid_contents() {
        $result = $this->App->get_sendgrid_all_templates();
        if ($result) {
            $this->out('Success');
        } else {
            $this->out('Error');
        }
    }

}

?>