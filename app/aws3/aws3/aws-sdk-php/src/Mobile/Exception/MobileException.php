<?php
namespace Aws3\Mobile\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Mobile** service.
 */
class MobileException extends AwsException {}
