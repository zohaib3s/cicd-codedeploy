<?php
namespace Aws3\SageMakerRuntime\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon SageMaker Runtime** service.
 */
class SageMakerRuntimeException extends AwsException {}
