<?php
namespace Aws3\MigrationHub\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Migration Hub** service.
 */
class MigrationHubException extends AwsException {}
