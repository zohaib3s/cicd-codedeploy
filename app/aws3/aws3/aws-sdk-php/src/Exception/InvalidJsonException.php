<?php
namespace Aws3\Exception;

use Aws3\HasMonitoringEventsTrait;
use Aws3\MonitoringEventsInterface;

class InvalidJsonException extends \RuntimeException implements
    MonitoringEventsInterface
{
    use HasMonitoringEventsTrait;
}
