<?php
namespace Aws3\MediaStore;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Elemental MediaStore** service.
 * @method \Aws3\Result createContainer(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createContainerAsync(array $args = [])
 * @method \Aws3\Result deleteContainer(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteContainerAsync(array $args = [])
 * @method \Aws3\Result deleteContainerPolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteContainerPolicyAsync(array $args = [])
 * @method \Aws3\Result deleteCorsPolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteCorsPolicyAsync(array $args = [])
 * @method \Aws3\Result deleteLifecyclePolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteLifecyclePolicyAsync(array $args = [])
 * @method \Aws3\Result describeContainer(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeContainerAsync(array $args = [])
 * @method \Aws3\Result getContainerPolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getContainerPolicyAsync(array $args = [])
 * @method \Aws3\Result getCorsPolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getCorsPolicyAsync(array $args = [])
 * @method \Aws3\Result getLifecyclePolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getLifecyclePolicyAsync(array $args = [])
 * @method \Aws3\Result listContainers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listContainersAsync(array $args = [])
 * @method \Aws3\Result listTagsForResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listTagsForResourceAsync(array $args = [])
 * @method \Aws3\Result putContainerPolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putContainerPolicyAsync(array $args = [])
 * @method \Aws3\Result putCorsPolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putCorsPolicyAsync(array $args = [])
 * @method \Aws3\Result putLifecyclePolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putLifecyclePolicyAsync(array $args = [])
 * @method \Aws3\Result startAccessLogging(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startAccessLoggingAsync(array $args = [])
 * @method \Aws3\Result stopAccessLogging(array $args = [])
 * @method \GuzzleHttp\Promise\Promise stopAccessLoggingAsync(array $args = [])
 * @method \Aws3\Result tagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise tagResourceAsync(array $args = [])
 * @method \Aws3\Result untagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise untagResourceAsync(array $args = [])
 */
class MediaStoreClient extends AwsClient {}
