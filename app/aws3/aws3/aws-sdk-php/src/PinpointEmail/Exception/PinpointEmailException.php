<?php
namespace Aws3\PinpointEmail\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Pinpoint Email Service** service.
 */
class PinpointEmailException extends AwsException {}
