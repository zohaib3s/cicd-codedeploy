<?php
namespace Aws3\WorkMailMessageFlow\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon WorkMail Message Flow** service.
 */
class WorkMailMessageFlowException extends AwsException {}
