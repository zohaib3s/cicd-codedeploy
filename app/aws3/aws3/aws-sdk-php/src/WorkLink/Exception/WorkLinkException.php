<?php
namespace Aws3\WorkLink\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon WorkLink** service.
 */
class WorkLinkException extends AwsException {}
