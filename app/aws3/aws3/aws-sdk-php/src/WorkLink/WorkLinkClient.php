<?php
namespace Aws3\WorkLink;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon WorkLink** service.
 * @method \Aws3\Result associateDomain(array $args = [])
 * @method \GuzzleHttp\Promise\Promise associateDomainAsync(array $args = [])
 * @method \Aws3\Result associateWebsiteAuthorizationProvider(array $args = [])
 * @method \GuzzleHttp\Promise\Promise associateWebsiteAuthorizationProviderAsync(array $args = [])
 * @method \Aws3\Result associateWebsiteCertificateAuthority(array $args = [])
 * @method \GuzzleHttp\Promise\Promise associateWebsiteCertificateAuthorityAsync(array $args = [])
 * @method \Aws3\Result createFleet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createFleetAsync(array $args = [])
 * @method \Aws3\Result deleteFleet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteFleetAsync(array $args = [])
 * @method \Aws3\Result describeAuditStreamConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeAuditStreamConfigurationAsync(array $args = [])
 * @method \Aws3\Result describeCompanyNetworkConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeCompanyNetworkConfigurationAsync(array $args = [])
 * @method \Aws3\Result describeDevice(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeDeviceAsync(array $args = [])
 * @method \Aws3\Result describeDevicePolicyConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeDevicePolicyConfigurationAsync(array $args = [])
 * @method \Aws3\Result describeDomain(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeDomainAsync(array $args = [])
 * @method \Aws3\Result describeFleetMetadata(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeFleetMetadataAsync(array $args = [])
 * @method \Aws3\Result describeIdentityProviderConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeIdentityProviderConfigurationAsync(array $args = [])
 * @method \Aws3\Result describeWebsiteCertificateAuthority(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeWebsiteCertificateAuthorityAsync(array $args = [])
 * @method \Aws3\Result disassociateDomain(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateDomainAsync(array $args = [])
 * @method \Aws3\Result disassociateWebsiteAuthorizationProvider(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateWebsiteAuthorizationProviderAsync(array $args = [])
 * @method \Aws3\Result disassociateWebsiteCertificateAuthority(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateWebsiteCertificateAuthorityAsync(array $args = [])
 * @method \Aws3\Result listDevices(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listDevicesAsync(array $args = [])
 * @method \Aws3\Result listDomains(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listDomainsAsync(array $args = [])
 * @method \Aws3\Result listFleets(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listFleetsAsync(array $args = [])
 * @method \Aws3\Result listWebsiteAuthorizationProviders(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listWebsiteAuthorizationProvidersAsync(array $args = [])
 * @method \Aws3\Result listWebsiteCertificateAuthorities(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listWebsiteCertificateAuthoritiesAsync(array $args = [])
 * @method \Aws3\Result restoreDomainAccess(array $args = [])
 * @method \GuzzleHttp\Promise\Promise restoreDomainAccessAsync(array $args = [])
 * @method \Aws3\Result revokeDomainAccess(array $args = [])
 * @method \GuzzleHttp\Promise\Promise revokeDomainAccessAsync(array $args = [])
 * @method \Aws3\Result signOutUser(array $args = [])
 * @method \GuzzleHttp\Promise\Promise signOutUserAsync(array $args = [])
 * @method \Aws3\Result updateAuditStreamConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateAuditStreamConfigurationAsync(array $args = [])
 * @method \Aws3\Result updateCompanyNetworkConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateCompanyNetworkConfigurationAsync(array $args = [])
 * @method \Aws3\Result updateDevicePolicyConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateDevicePolicyConfigurationAsync(array $args = [])
 * @method \Aws3\Result updateDomainMetadata(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateDomainMetadataAsync(array $args = [])
 * @method \Aws3\Result updateFleetMetadata(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateFleetMetadataAsync(array $args = [])
 * @method \Aws3\Result updateIdentityProviderConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateIdentityProviderConfigurationAsync(array $args = [])
 */
class WorkLinkClient extends AwsClient {}
