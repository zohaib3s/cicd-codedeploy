<?php
namespace Aws3\PersonalizeEvents;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Personalize Events** service.
 * @method \Aws3\Result putEvents(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putEventsAsync(array $args = [])
 */
class PersonalizeEventsClient extends AwsClient {}
