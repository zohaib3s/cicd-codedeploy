<?php
namespace Aws3\ManagedBlockchain\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Managed Blockchain** service.
 */
class ManagedBlockchainException extends AwsException {}
