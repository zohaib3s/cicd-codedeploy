<?php
namespace Aws3\Shield;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Shield** service.
 * @method \Aws3\Result associateDRTLogBucket(array $args = [])
 * @method \GuzzleHttp\Promise\Promise associateDRTLogBucketAsync(array $args = [])
 * @method \Aws3\Result associateDRTRole(array $args = [])
 * @method \GuzzleHttp\Promise\Promise associateDRTRoleAsync(array $args = [])
 * @method \Aws3\Result createProtection(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createProtectionAsync(array $args = [])
 * @method \Aws3\Result createSubscription(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createSubscriptionAsync(array $args = [])
 * @method \Aws3\Result deleteProtection(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteProtectionAsync(array $args = [])
 * @method \Aws3\Result deleteSubscription(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteSubscriptionAsync(array $args = [])
 * @method \Aws3\Result describeAttack(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeAttackAsync(array $args = [])
 * @method \Aws3\Result describeDRTAccess(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeDRTAccessAsync(array $args = [])
 * @method \Aws3\Result describeEmergencyContactSettings(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeEmergencyContactSettingsAsync(array $args = [])
 * @method \Aws3\Result describeProtection(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeProtectionAsync(array $args = [])
 * @method \Aws3\Result describeSubscription(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeSubscriptionAsync(array $args = [])
 * @method \Aws3\Result disassociateDRTLogBucket(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateDRTLogBucketAsync(array $args = [])
 * @method \Aws3\Result disassociateDRTRole(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateDRTRoleAsync(array $args = [])
 * @method \Aws3\Result getSubscriptionState(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getSubscriptionStateAsync(array $args = [])
 * @method \Aws3\Result listAttacks(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listAttacksAsync(array $args = [])
 * @method \Aws3\Result listProtections(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listProtectionsAsync(array $args = [])
 * @method \Aws3\Result updateEmergencyContactSettings(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateEmergencyContactSettingsAsync(array $args = [])
 * @method \Aws3\Result updateSubscription(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateSubscriptionAsync(array $args = [])
 */
class ShieldClient extends AwsClient {}
