<?php
namespace Aws3\ServiceQuotas;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Service Quotas** service.
 * @method \Aws3\Result associateServiceQuotaTemplate(array $args = [])
 * @method \GuzzleHttp\Promise\Promise associateServiceQuotaTemplateAsync(array $args = [])
 * @method \Aws3\Result deleteServiceQuotaIncreaseRequestFromTemplate(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteServiceQuotaIncreaseRequestFromTemplateAsync(array $args = [])
 * @method \Aws3\Result disassociateServiceQuotaTemplate(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateServiceQuotaTemplateAsync(array $args = [])
 * @method \Aws3\Result getAWSDefaultServiceQuota(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getAWSDefaultServiceQuotaAsync(array $args = [])
 * @method \Aws3\Result getAssociationForServiceQuotaTemplate(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getAssociationForServiceQuotaTemplateAsync(array $args = [])
 * @method \Aws3\Result getRequestedServiceQuotaChange(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getRequestedServiceQuotaChangeAsync(array $args = [])
 * @method \Aws3\Result getServiceQuota(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getServiceQuotaAsync(array $args = [])
 * @method \Aws3\Result getServiceQuotaIncreaseRequestFromTemplate(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getServiceQuotaIncreaseRequestFromTemplateAsync(array $args = [])
 * @method \Aws3\Result listAWSDefaultServiceQuotas(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listAWSDefaultServiceQuotasAsync(array $args = [])
 * @method \Aws3\Result listRequestedServiceQuotaChangeHistory(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listRequestedServiceQuotaChangeHistoryAsync(array $args = [])
 * @method \Aws3\Result listRequestedServiceQuotaChangeHistoryByQuota(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listRequestedServiceQuotaChangeHistoryByQuotaAsync(array $args = [])
 * @method \Aws3\Result listServiceQuotaIncreaseRequestsInTemplate(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listServiceQuotaIncreaseRequestsInTemplateAsync(array $args = [])
 * @method \Aws3\Result listServiceQuotas(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listServiceQuotasAsync(array $args = [])
 * @method \Aws3\Result listServices(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listServicesAsync(array $args = [])
 * @method \Aws3\Result putServiceQuotaIncreaseRequestIntoTemplate(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putServiceQuotaIncreaseRequestIntoTemplateAsync(array $args = [])
 * @method \Aws3\Result requestServiceQuotaIncrease(array $args = [])
 * @method \GuzzleHttp\Promise\Promise requestServiceQuotaIncreaseAsync(array $args = [])
 */
class ServiceQuotasClient extends AwsClient {}
