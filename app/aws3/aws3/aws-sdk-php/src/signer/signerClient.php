<?php
namespace Aws3\signer;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Signer** service.
 * @method \Aws3\Result cancelSigningProfile(array $args = [])
 * @method \GuzzleHttp\Promise\Promise cancelSigningProfileAsync(array $args = [])
 * @method \Aws3\Result describeSigningJob(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeSigningJobAsync(array $args = [])
 * @method \Aws3\Result getSigningPlatform(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getSigningPlatformAsync(array $args = [])
 * @method \Aws3\Result getSigningProfile(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getSigningProfileAsync(array $args = [])
 * @method \Aws3\Result listSigningJobs(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listSigningJobsAsync(array $args = [])
 * @method \Aws3\Result listSigningPlatforms(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listSigningPlatformsAsync(array $args = [])
 * @method \Aws3\Result listSigningProfiles(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listSigningProfilesAsync(array $args = [])
 * @method \Aws3\Result putSigningProfile(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putSigningProfileAsync(array $args = [])
 * @method \Aws3\Result startSigningJob(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startSigningJobAsync(array $args = [])
 */
class signerClient extends AwsClient {}
