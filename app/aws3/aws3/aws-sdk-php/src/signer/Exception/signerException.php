<?php
namespace Aws3\signer\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Signer** service.
 */
class signerException extends AwsException {}
