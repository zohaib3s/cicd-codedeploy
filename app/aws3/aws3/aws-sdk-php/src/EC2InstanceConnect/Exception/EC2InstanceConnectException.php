<?php
namespace Aws3\EC2InstanceConnect\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS EC2 Instance Connect** service.
 */
class EC2InstanceConnectException extends AwsException {}
