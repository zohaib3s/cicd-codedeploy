<?php
namespace Aws3\Glue\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Glue** service.
 */
class GlueException extends AwsException {}
