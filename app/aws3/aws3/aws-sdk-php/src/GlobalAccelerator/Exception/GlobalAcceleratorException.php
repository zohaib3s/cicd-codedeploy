<?php
namespace Aws3\GlobalAccelerator\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Global Accelerator** service.
 */
class GlobalAcceleratorException extends AwsException {}
