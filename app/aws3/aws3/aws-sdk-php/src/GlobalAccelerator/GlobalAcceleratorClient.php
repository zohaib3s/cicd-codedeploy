<?php
namespace Aws3\GlobalAccelerator;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Global Accelerator** service.
 * @method \Aws3\Result createAccelerator(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createAcceleratorAsync(array $args = [])
 * @method \Aws3\Result createEndpointGroup(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createEndpointGroupAsync(array $args = [])
 * @method \Aws3\Result createListener(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createListenerAsync(array $args = [])
 * @method \Aws3\Result deleteAccelerator(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteAcceleratorAsync(array $args = [])
 * @method \Aws3\Result deleteEndpointGroup(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteEndpointGroupAsync(array $args = [])
 * @method \Aws3\Result deleteListener(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteListenerAsync(array $args = [])
 * @method \Aws3\Result describeAccelerator(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeAcceleratorAsync(array $args = [])
 * @method \Aws3\Result describeAcceleratorAttributes(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeAcceleratorAttributesAsync(array $args = [])
 * @method \Aws3\Result describeEndpointGroup(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeEndpointGroupAsync(array $args = [])
 * @method \Aws3\Result describeListener(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeListenerAsync(array $args = [])
 * @method \Aws3\Result listAccelerators(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listAcceleratorsAsync(array $args = [])
 * @method \Aws3\Result listEndpointGroups(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listEndpointGroupsAsync(array $args = [])
 * @method \Aws3\Result listListeners(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listListenersAsync(array $args = [])
 * @method \Aws3\Result updateAccelerator(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateAcceleratorAsync(array $args = [])
 * @method \Aws3\Result updateAcceleratorAttributes(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateAcceleratorAttributesAsync(array $args = [])
 * @method \Aws3\Result updateEndpointGroup(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateEndpointGroupAsync(array $args = [])
 * @method \Aws3\Result updateListener(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateListenerAsync(array $args = [])
 */
class GlobalAcceleratorClient extends AwsClient {}
