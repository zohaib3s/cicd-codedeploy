<?php
namespace Aws3\KinesisAnalyticsV2\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Kinesis Analytics** service.
 */
class KinesisAnalyticsV2Exception extends AwsException {}
