<?php
namespace Aws3\ApiGatewayManagementApi\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AmazonApiGatewayManagementApi** service.
 */
class ApiGatewayManagementApiException extends AwsException {}
