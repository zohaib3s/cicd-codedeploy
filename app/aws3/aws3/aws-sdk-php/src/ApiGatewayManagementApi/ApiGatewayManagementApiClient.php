<?php
namespace Aws3\ApiGatewayManagementApi;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AmazonApiGatewayManagementApi** service.
 * @method \Aws3\Result deleteConnection(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteConnectionAsync(array $args = [])
 * @method \Aws3\Result getConnection(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getConnectionAsync(array $args = [])
 * @method \Aws3\Result postToConnection(array $args = [])
 * @method \GuzzleHttp\Promise\Promise postToConnectionAsync(array $args = [])
 */
class ApiGatewayManagementApiClient extends AwsClient {}
