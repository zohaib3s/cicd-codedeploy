<?php
namespace Aws3\IoTEventsData;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS IoT Events Data** service.
 * @method \Aws3\Result batchPutMessage(array $args = [])
 * @method \GuzzleHttp\Promise\Promise batchPutMessageAsync(array $args = [])
 * @method \Aws3\Result batchUpdateDetector(array $args = [])
 * @method \GuzzleHttp\Promise\Promise batchUpdateDetectorAsync(array $args = [])
 * @method \Aws3\Result describeDetector(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeDetectorAsync(array $args = [])
 * @method \Aws3\Result listDetectors(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listDetectorsAsync(array $args = [])
 */
class IoTEventsDataClient extends AwsClient {}
