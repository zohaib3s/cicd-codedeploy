<?php
namespace Aws3\IoTEventsData\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS IoT Events Data** service.
 */
class IoTEventsDataException extends AwsException {}
