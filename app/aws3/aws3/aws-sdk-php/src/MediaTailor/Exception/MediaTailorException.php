<?php
namespace Aws3\MediaTailor\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS MediaTailor** service.
 */
class MediaTailorException extends AwsException {}
