<?php
namespace Aws3\Neptune\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Neptune** service.
 */
class NeptuneException extends AwsException {}
