<?php
namespace Aws3\MQ\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AmazonMQ** service.
 */
class MQException extends AwsException {}
