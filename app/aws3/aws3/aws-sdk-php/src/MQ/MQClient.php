<?php
namespace Aws3\MQ;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AmazonMQ** service.
 * @method \Aws3\Result createBroker(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createBrokerAsync(array $args = [])
 * @method \Aws3\Result createConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createConfigurationAsync(array $args = [])
 * @method \Aws3\Result createTags(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createTagsAsync(array $args = [])
 * @method \Aws3\Result createUser(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createUserAsync(array $args = [])
 * @method \Aws3\Result deleteBroker(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteBrokerAsync(array $args = [])
 * @method \Aws3\Result deleteTags(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteTagsAsync(array $args = [])
 * @method \Aws3\Result deleteUser(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteUserAsync(array $args = [])
 * @method \Aws3\Result describeBroker(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeBrokerAsync(array $args = [])
 * @method \Aws3\Result describeBrokerEngineTypes(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeBrokerEngineTypesAsync(array $args = [])
 * @method \Aws3\Result describeBrokerInstanceOptions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeBrokerInstanceOptionsAsync(array $args = [])
 * @method \Aws3\Result describeConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeConfigurationAsync(array $args = [])
 * @method \Aws3\Result describeConfigurationRevision(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeConfigurationRevisionAsync(array $args = [])
 * @method \Aws3\Result describeUser(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeUserAsync(array $args = [])
 * @method \Aws3\Result listBrokers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listBrokersAsync(array $args = [])
 * @method \Aws3\Result listConfigurationRevisions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listConfigurationRevisionsAsync(array $args = [])
 * @method \Aws3\Result listConfigurations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listConfigurationsAsync(array $args = [])
 * @method \Aws3\Result listTags(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listTagsAsync(array $args = [])
 * @method \Aws3\Result listUsers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listUsersAsync(array $args = [])
 * @method \Aws3\Result rebootBroker(array $args = [])
 * @method \GuzzleHttp\Promise\Promise rebootBrokerAsync(array $args = [])
 * @method \Aws3\Result updateBroker(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateBrokerAsync(array $args = [])
 * @method \Aws3\Result updateConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateConfigurationAsync(array $args = [])
 * @method \Aws3\Result updateUser(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateUserAsync(array $args = [])
 */
class MQClient extends AwsClient {}
