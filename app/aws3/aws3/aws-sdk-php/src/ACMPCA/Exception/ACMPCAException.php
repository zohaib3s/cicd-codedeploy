<?php
namespace Aws3\ACMPCA\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Certificate Manager Private Certificate Authority** service.
 */
class ACMPCAException extends AwsException {}
