<?php
namespace Aws3\IoTEvents\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS IoT Events** service.
 */
class IoTEventsException extends AwsException {}
