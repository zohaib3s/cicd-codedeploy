<?php
namespace Aws3\IoTEvents;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS IoT Events** service.
 * @method \Aws3\Result createDetectorModel(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createDetectorModelAsync(array $args = [])
 * @method \Aws3\Result createInput(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createInputAsync(array $args = [])
 * @method \Aws3\Result deleteDetectorModel(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteDetectorModelAsync(array $args = [])
 * @method \Aws3\Result deleteInput(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteInputAsync(array $args = [])
 * @method \Aws3\Result describeDetectorModel(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeDetectorModelAsync(array $args = [])
 * @method \Aws3\Result describeInput(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeInputAsync(array $args = [])
 * @method \Aws3\Result describeLoggingOptions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeLoggingOptionsAsync(array $args = [])
 * @method \Aws3\Result listDetectorModelVersions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listDetectorModelVersionsAsync(array $args = [])
 * @method \Aws3\Result listDetectorModels(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listDetectorModelsAsync(array $args = [])
 * @method \Aws3\Result listInputs(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listInputsAsync(array $args = [])
 * @method \Aws3\Result listTagsForResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listTagsForResourceAsync(array $args = [])
 * @method \Aws3\Result putLoggingOptions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putLoggingOptionsAsync(array $args = [])
 * @method \Aws3\Result tagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise tagResourceAsync(array $args = [])
 * @method \Aws3\Result untagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise untagResourceAsync(array $args = [])
 * @method \Aws3\Result updateDetectorModel(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateDetectorModelAsync(array $args = [])
 * @method \Aws3\Result updateInput(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateInputAsync(array $args = [])
 */
class IoTEventsClient extends AwsClient {}
