<?php
namespace Aws3\AutoScalingPlans\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Auto Scaling Plans** service.
 */
class AutoScalingPlansException extends AwsException {}
