<?php
namespace Aws3\PersonalizeRuntime;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Personalize Runtime** service.
 * @method \Aws3\Result getPersonalizedRanking(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getPersonalizedRankingAsync(array $args = [])
 * @method \Aws3\Result getRecommendations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getRecommendationsAsync(array $args = [])
 */
class PersonalizeRuntimeClient extends AwsClient {}
