<?php
namespace Aws3\Comprehend\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Comprehend** service.
 */
class ComprehendException extends AwsException {}
