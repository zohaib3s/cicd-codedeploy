<?php
namespace Aws3\Translate\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Translate** service.
 */
class TranslateException extends AwsException {}
