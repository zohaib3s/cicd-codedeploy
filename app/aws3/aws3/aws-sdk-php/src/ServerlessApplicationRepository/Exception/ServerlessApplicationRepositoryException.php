<?php
namespace Aws3\ServerlessApplicationRepository\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWSServerlessApplicationRepository** service.
 */
class ServerlessApplicationRepositoryException extends AwsException {}
