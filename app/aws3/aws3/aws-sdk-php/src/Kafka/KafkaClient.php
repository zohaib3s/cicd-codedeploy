<?php
namespace Aws3\Kafka;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Managed Streaming for Kafka** service.
 * @method \Aws3\Result createCluster(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createClusterAsync(array $args = [])
 * @method \Aws3\Result createConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createConfigurationAsync(array $args = [])
 * @method \Aws3\Result deleteCluster(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteClusterAsync(array $args = [])
 * @method \Aws3\Result describeCluster(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeClusterAsync(array $args = [])
 * @method \Aws3\Result describeClusterOperation(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeClusterOperationAsync(array $args = [])
 * @method \Aws3\Result describeConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeConfigurationAsync(array $args = [])
 * @method \Aws3\Result describeConfigurationRevision(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeConfigurationRevisionAsync(array $args = [])
 * @method \Aws3\Result getBootstrapBrokers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getBootstrapBrokersAsync(array $args = [])
 * @method \Aws3\Result listClusterOperations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listClusterOperationsAsync(array $args = [])
 * @method \Aws3\Result listClusters(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listClustersAsync(array $args = [])
 * @method \Aws3\Result listConfigurationRevisions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listConfigurationRevisionsAsync(array $args = [])
 * @method \Aws3\Result listConfigurations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listConfigurationsAsync(array $args = [])
 * @method \Aws3\Result listNodes(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listNodesAsync(array $args = [])
 * @method \Aws3\Result listTagsForResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listTagsForResourceAsync(array $args = [])
 * @method \Aws3\Result tagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise tagResourceAsync(array $args = [])
 * @method \Aws3\Result untagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise untagResourceAsync(array $args = [])
 * @method \Aws3\Result updateBrokerStorage(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateBrokerStorageAsync(array $args = [])
 * @method \Aws3\Result updateClusterConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateClusterConfigurationAsync(array $args = [])
 */
class KafkaClient extends AwsClient {}
