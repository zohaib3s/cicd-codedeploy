<?php
namespace Aws3\FSx\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon FSx** service.
 */
class FSxException extends AwsException {}
