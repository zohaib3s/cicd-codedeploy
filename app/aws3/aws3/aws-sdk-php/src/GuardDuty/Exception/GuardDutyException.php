<?php
namespace Aws3\GuardDuty\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon GuardDuty** service.
 */
class GuardDutyException extends AwsException {}
