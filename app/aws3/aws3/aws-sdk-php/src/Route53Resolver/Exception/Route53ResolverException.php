<?php
namespace Aws3\Route53Resolver\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Route 53 Resolver** service.
 */
class Route53ResolverException extends AwsException {}
