<?php
namespace Aws3\MediaStoreData\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Elemental MediaStore Data Plane** service.
 */
class MediaStoreDataException extends AwsException {}
