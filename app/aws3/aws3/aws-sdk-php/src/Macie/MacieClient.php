<?php
namespace Aws3\Macie;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Macie** service.
 * @method \Aws3\Result associateMemberAccount(array $args = [])
 * @method \GuzzleHttp\Promise\Promise associateMemberAccountAsync(array $args = [])
 * @method \Aws3\Result associateS3Resources(array $args = [])
 * @method \GuzzleHttp\Promise\Promise associateS3ResourcesAsync(array $args = [])
 * @method \Aws3\Result disassociateMemberAccount(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateMemberAccountAsync(array $args = [])
 * @method \Aws3\Result disassociateS3Resources(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateS3ResourcesAsync(array $args = [])
 * @method \Aws3\Result listMemberAccounts(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listMemberAccountsAsync(array $args = [])
 * @method \Aws3\Result listS3Resources(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listS3ResourcesAsync(array $args = [])
 * @method \Aws3\Result updateS3Resources(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateS3ResourcesAsync(array $args = [])
 */
class MacieClient extends AwsClient {}
