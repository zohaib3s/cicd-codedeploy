<?php
namespace Aws3\Macie\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Macie** service.
 */
class MacieException extends AwsException {}
