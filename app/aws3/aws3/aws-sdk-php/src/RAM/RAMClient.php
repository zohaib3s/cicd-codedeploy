<?php
namespace Aws3\RAM;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Resource Access Manager** service.
 * @method \Aws3\Result acceptResourceShareInvitation(array $args = [])
 * @method \GuzzleHttp\Promise\Promise acceptResourceShareInvitationAsync(array $args = [])
 * @method \Aws3\Result associateResourceShare(array $args = [])
 * @method \GuzzleHttp\Promise\Promise associateResourceShareAsync(array $args = [])
 * @method \Aws3\Result createResourceShare(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createResourceShareAsync(array $args = [])
 * @method \Aws3\Result deleteResourceShare(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteResourceShareAsync(array $args = [])
 * @method \Aws3\Result disassociateResourceShare(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateResourceShareAsync(array $args = [])
 * @method \Aws3\Result enableSharingWithAwsOrganization(array $args = [])
 * @method \GuzzleHttp\Promise\Promise enableSharingWithAwsOrganizationAsync(array $args = [])
 * @method \Aws3\Result getResourcePolicies(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getResourcePoliciesAsync(array $args = [])
 * @method \Aws3\Result getResourceShareAssociations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getResourceShareAssociationsAsync(array $args = [])
 * @method \Aws3\Result getResourceShareInvitations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getResourceShareInvitationsAsync(array $args = [])
 * @method \Aws3\Result getResourceShares(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getResourceSharesAsync(array $args = [])
 * @method \Aws3\Result listPendingInvitationResources(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listPendingInvitationResourcesAsync(array $args = [])
 * @method \Aws3\Result listPrincipals(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listPrincipalsAsync(array $args = [])
 * @method \Aws3\Result listResources(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listResourcesAsync(array $args = [])
 * @method \Aws3\Result rejectResourceShareInvitation(array $args = [])
 * @method \GuzzleHttp\Promise\Promise rejectResourceShareInvitationAsync(array $args = [])
 * @method \Aws3\Result tagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise tagResourceAsync(array $args = [])
 * @method \Aws3\Result untagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise untagResourceAsync(array $args = [])
 * @method \Aws3\Result updateResourceShare(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateResourceShareAsync(array $args = [])
 */
class RAMClient extends AwsClient {}
