<?php
namespace Aws3\MediaConvert\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Elemental MediaConvert** service.
 */
class MediaConvertException extends AwsException {}
