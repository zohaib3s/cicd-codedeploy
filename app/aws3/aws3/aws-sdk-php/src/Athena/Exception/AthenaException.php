<?php
namespace Aws3\Athena\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Athena** service.
 */
class AthenaException extends AwsException {}
