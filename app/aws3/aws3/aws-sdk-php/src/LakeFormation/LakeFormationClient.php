<?php
namespace Aws3\LakeFormation;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Lake Formation** service.
 * @method \Aws3\Result batchGrantPermissions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise batchGrantPermissionsAsync(array $args = [])
 * @method \Aws3\Result batchRevokePermissions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise batchRevokePermissionsAsync(array $args = [])
 * @method \Aws3\Result deregisterResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deregisterResourceAsync(array $args = [])
 * @method \Aws3\Result describeResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeResourceAsync(array $args = [])
 * @method \Aws3\Result getDataLakeSettings(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getDataLakeSettingsAsync(array $args = [])
 * @method \Aws3\Result getEffectivePermissionsForPath(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getEffectivePermissionsForPathAsync(array $args = [])
 * @method \Aws3\Result grantPermissions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise grantPermissionsAsync(array $args = [])
 * @method \Aws3\Result listPermissions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listPermissionsAsync(array $args = [])
 * @method \Aws3\Result listResources(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listResourcesAsync(array $args = [])
 * @method \Aws3\Result putDataLakeSettings(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putDataLakeSettingsAsync(array $args = [])
 * @method \Aws3\Result registerResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise registerResourceAsync(array $args = [])
 * @method \Aws3\Result revokePermissions(array $args = [])
 * @method \GuzzleHttp\Promise\Promise revokePermissionsAsync(array $args = [])
 * @method \Aws3\Result updateResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateResourceAsync(array $args = [])
 */
class LakeFormationClient extends AwsClient {}
