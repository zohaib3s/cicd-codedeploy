<?php
namespace Aws3\ServiceDiscovery\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Route 53 Auto Naming** service.
 */
class ServiceDiscoveryException extends AwsException {}
