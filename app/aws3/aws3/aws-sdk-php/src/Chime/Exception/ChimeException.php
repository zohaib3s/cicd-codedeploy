<?php
namespace Aws3\Chime\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Chime** service.
 */
class ChimeException extends AwsException {}
