<?php
namespace Aws3\MediaPackage\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Elemental MediaPackage** service.
 */
class MediaPackageException extends AwsException {}
