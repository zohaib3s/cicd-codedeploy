<?php
namespace Aws3\ForecastService\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Forecast Service** service.
 */
class ForecastServiceException extends AwsException {}
