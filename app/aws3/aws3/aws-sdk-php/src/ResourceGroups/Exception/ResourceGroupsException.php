<?php
namespace Aws3\ResourceGroups\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Resource Groups** service.
 */
class ResourceGroupsException extends AwsException {}
