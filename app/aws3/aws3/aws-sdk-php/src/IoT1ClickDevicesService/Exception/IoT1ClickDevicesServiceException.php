<?php
namespace Aws3\IoT1ClickDevicesService\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS IoT 1-Click Devices Service** service.
 */
class IoT1ClickDevicesServiceException extends AwsException {}
