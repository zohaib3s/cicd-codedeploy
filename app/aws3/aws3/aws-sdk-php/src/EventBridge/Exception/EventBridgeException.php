<?php
namespace Aws3\EventBridge\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon EventBridge** service.
 */
class EventBridgeException extends AwsException {}
