<?php
namespace Aws3\S3Control\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS S3 Control** service.
 */
class S3ControlException extends AwsException {}
