<?php
namespace Aws3\MediaPackageVod\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Elemental MediaPackage VOD** service.
 */
class MediaPackageVodException extends AwsException {}
