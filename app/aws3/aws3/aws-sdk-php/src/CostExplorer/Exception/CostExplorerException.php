<?php
namespace Aws3\CostExplorer\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Cost Explorer Service** service.
 */
class CostExplorerException extends AwsException {}
