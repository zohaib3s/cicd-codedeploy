<?php
namespace Aws3\Personalize\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Personalize** service.
 */
class PersonalizeException extends AwsException {}
