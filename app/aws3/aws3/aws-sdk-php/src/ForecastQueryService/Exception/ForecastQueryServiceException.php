<?php
namespace Aws3\ForecastQueryService\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Forecast Query Service** service.
 */
class ForecastQueryServiceException extends AwsException {}
