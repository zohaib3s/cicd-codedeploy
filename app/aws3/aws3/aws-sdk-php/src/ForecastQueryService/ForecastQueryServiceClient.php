<?php
namespace Aws3\ForecastQueryService;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Forecast Query Service** service.
 * @method \Aws3\Result queryForecast(array $args = [])
 * @method \GuzzleHttp\Promise\Promise queryForecastAsync(array $args = [])
 */
class ForecastQueryServiceClient extends AwsClient {}
