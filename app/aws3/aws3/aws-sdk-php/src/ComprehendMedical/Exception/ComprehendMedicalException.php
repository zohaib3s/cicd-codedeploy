<?php
namespace Aws3\ComprehendMedical\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Comprehend Medical** service.
 */
class ComprehendMedicalException extends AwsException {}
