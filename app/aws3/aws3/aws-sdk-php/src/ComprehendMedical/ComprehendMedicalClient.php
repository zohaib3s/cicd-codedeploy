<?php
namespace Aws3\ComprehendMedical;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Comprehend Medical** service.
 * @method \Aws3\Result describeEntitiesDetectionV2Job(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeEntitiesDetectionV2JobAsync(array $args = [])
 * @method \Aws3\Result describePHIDetectionJob(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describePHIDetectionJobAsync(array $args = [])
 * @method \Aws3\Result detectEntities(array $args = [])
 * @method \GuzzleHttp\Promise\Promise detectEntitiesAsync(array $args = [])
 * @method \Aws3\Result detectEntitiesV2(array $args = [])
 * @method \GuzzleHttp\Promise\Promise detectEntitiesV2Async(array $args = [])
 * @method \Aws3\Result detectPHI(array $args = [])
 * @method \GuzzleHttp\Promise\Promise detectPHIAsync(array $args = [])
 * @method \Aws3\Result listEntitiesDetectionV2Jobs(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listEntitiesDetectionV2JobsAsync(array $args = [])
 * @method \Aws3\Result listPHIDetectionJobs(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listPHIDetectionJobsAsync(array $args = [])
 * @method \Aws3\Result startEntitiesDetectionV2Job(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startEntitiesDetectionV2JobAsync(array $args = [])
 * @method \Aws3\Result startPHIDetectionJob(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startPHIDetectionJobAsync(array $args = [])
 * @method \Aws3\Result stopEntitiesDetectionV2Job(array $args = [])
 * @method \GuzzleHttp\Promise\Promise stopEntitiesDetectionV2JobAsync(array $args = [])
 * @method \Aws3\Result stopPHIDetectionJob(array $args = [])
 * @method \GuzzleHttp\Promise\Promise stopPHIDetectionJobAsync(array $args = [])
 */
class ComprehendMedicalClient extends AwsClient {}
