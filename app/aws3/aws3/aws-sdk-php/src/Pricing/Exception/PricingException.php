<?php
namespace Aws3\Pricing\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Price List Service** service.
 */
class PricingException extends AwsException {}
