<?php
namespace Aws3\Cloud9\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Cloud9** service.
 */
class Cloud9Exception extends AwsException {}
