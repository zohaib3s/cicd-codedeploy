<?php
namespace Aws3\LicenseManager\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS License Manager** service.
 */
class LicenseManagerException extends AwsException {}
