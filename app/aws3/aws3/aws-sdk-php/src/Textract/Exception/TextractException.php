<?php
namespace Aws3\Textract\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Textract** service.
 */
class TextractException extends AwsException {}
