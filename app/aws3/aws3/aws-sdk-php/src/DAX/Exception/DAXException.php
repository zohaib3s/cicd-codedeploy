<?php
namespace Aws3\DAX\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon DynamoDB Accelerator (DAX)** service.
 */
class DAXException extends AwsException {}
