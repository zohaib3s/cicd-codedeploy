<?php
namespace Aws3\MarketplaceEntitlementService\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Marketplace Entitlement Service** service.
 */
class MarketplaceEntitlementServiceException extends AwsException {}
