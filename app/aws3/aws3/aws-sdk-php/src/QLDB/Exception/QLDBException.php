<?php
namespace Aws3\QLDB\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon QLDB** service.
 */
class QLDBException extends AwsException {}
