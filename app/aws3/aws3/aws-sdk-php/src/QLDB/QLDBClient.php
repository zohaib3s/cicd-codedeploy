<?php
namespace Aws3\QLDB;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon QLDB** service.
 * @method \Aws3\Result createLedger(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createLedgerAsync(array $args = [])
 * @method \Aws3\Result deleteLedger(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteLedgerAsync(array $args = [])
 * @method \Aws3\Result describeJournalS3Export(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeJournalS3ExportAsync(array $args = [])
 * @method \Aws3\Result describeLedger(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeLedgerAsync(array $args = [])
 * @method \Aws3\Result exportJournalToS3(array $args = [])
 * @method \GuzzleHttp\Promise\Promise exportJournalToS3Async(array $args = [])
 * @method \Aws3\Result getBlock(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getBlockAsync(array $args = [])
 * @method \Aws3\Result getDigest(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getDigestAsync(array $args = [])
 * @method \Aws3\Result getRevision(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getRevisionAsync(array $args = [])
 * @method \Aws3\Result listJournalS3Exports(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listJournalS3ExportsAsync(array $args = [])
 * @method \Aws3\Result listJournalS3ExportsForLedger(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listJournalS3ExportsForLedgerAsync(array $args = [])
 * @method \Aws3\Result listLedgers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listLedgersAsync(array $args = [])
 * @method \Aws3\Result listTagsForResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listTagsForResourceAsync(array $args = [])
 * @method \Aws3\Result tagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise tagResourceAsync(array $args = [])
 * @method \Aws3\Result untagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise untagResourceAsync(array $args = [])
 * @method \Aws3\Result updateLedger(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateLedgerAsync(array $args = [])
 */
class QLDBClient extends AwsClient {}
