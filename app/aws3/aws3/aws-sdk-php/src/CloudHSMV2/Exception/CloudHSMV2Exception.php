<?php
namespace Aws3\CloudHSMV2\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS CloudHSM V2** service.
 */
class CloudHSMV2Exception extends AwsException {}
