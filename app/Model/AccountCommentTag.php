<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AccountCommentTag
 *
 * @author 3S
 */
class AccountCommentTag  extends AppModel{
 
    var $useTable = 'account_comment_tags';    
    var $primaryKey = 'account_comment_tag_id';

    
    public function gettagsbycommentid($comment_id,$tag_type){
        
        $table_account_tag=array(array('table' => 'account_tags',                            
                                   'type' => 'LEFT',
                                   'conditions' => array('AccountCommentTag.account_tag_id= account_tags.account_tag_id')));
        $get_tags=$this->find('all',
                array(
                    "fields"=>array("`account_tags`.*,AccountCommentTag.*"),
                    "joins"=>$table_account_tag,
                    "conditions"=>array("comment_id"=>$comment_id,"ref_type"=>$tag_type)
            
            ));
        return $get_tags;
    }
}
