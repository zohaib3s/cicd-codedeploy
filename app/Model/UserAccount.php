<?php

// app/Model/User.php
class UserAccount extends AppModel {

    var $useTable = 'users_accounts';


    function getAll($account_id, $user_id) {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        return $this->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
    }

    function get($account_id, $user_id) {        
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        $joins = array(
            array(
                'table' => 'accounts',
                'type' => 'left',
                'conditions' => array('UserAccount.account_id=accounts.id')
            ),
            array(
                'table' => 'users',
                'type' => 'left',
                'conditions' => array('UserAccount.user_id=users.id')
            ),
            array(
                'table' => 'roles',
                'type' => 'left',
                'conditions' => array('UserAccount.role_id=roles.role_id')
            )
        );
        $fields = array('UserAccount.*', 'accounts.*', 'users.*', 'roles.*');        
        return $this->find('first', array(            
            'joins' => $joins,
            'conditions' => array('UserAccount.account_id' => $account_id, 'UserAccount.user_id' => $user_id),
            'fields' => $fields
        ));
    }
    function get_for_andriod($account_id, $user_id,$site_id) {        
        $account_id = (int) $account_id;
        $this->site_id = $site_id;
        $user_id = (int) $user_id;
        $joins = array(
            array(
                'table' => 'accounts',
                'type' => 'left',
                'conditions' => array('UserAccount.account_id=accounts.id')
            ),
            array(
                'table' => 'users',
                'type' => 'left',
                'conditions' => array('UserAccount.user_id=users.id')
            ),
            array(
                'table' => 'roles',
                'type' => 'left',
                'conditions' => array('UserAccount.role_id=roles.role_id')
            )
        );
        $fields = array('UserAccount.*', 'accounts.*', 'users.*', 'roles.*');        
        return $this->find('first', array(            
            'joins' => $joins,
            'conditions' => array('UserAccount.account_id' => $account_id, 'UserAccount.user_id' => $user_id),
            'fields' => $fields
        ));
    }

    function get_account_owner($account_id) {
        $account_id = (int) $account_id;
        $joins = array(
            array(
                'table' => 'accounts',
                'type' => 'left',
                'conditions' => array('UserAccount.account_id=accounts.id')
            ),
            array(
                'table' => 'users',
                'type' => 'left',
                'conditions' => array('UserAccount.user_id=users.id')
            ),
            array(
                'table' => 'roles',
                'type' => 'left',
                'conditions' => array('UserAccount.role_id=roles.role_id')
            )
        );
        $fields = array('UserAccount.*', 'accounts.*', 'users.*', 'roles.*');
        return $this->find('first', array(
            'joins' => $joins,
            'conditions' => array('UserAccount.account_id' => $account_id, 'UserAccount.role_id' => '100'),
            'fields' => $fields
        ));
    }

}


