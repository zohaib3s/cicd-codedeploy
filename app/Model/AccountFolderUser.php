<?php

// app/Model/User.php
class AccountFolderUser extends AppModel {

    var $useTable = 'account_folder_users';
    var $primaryKey = 'account_folder_user_id';

    function get($user_id) {
        return $this->find('all', array('conditions' => array('user_id' => (int) $user_id)));
    }
    function get_by_huddle($user_id,$account_folder_id) {
        return $this->find('all', array('conditions' => array('user_id' => (int) $user_id,'account_folder_id'=>(int) $account_folder_id)));
    }

    function getHuddleUserType($user_id, $acount_folder_id) {
        return $this->find('all', array('conditions' => array('user_id' => (int) $user_id, 'account_folder_id' => (int) $acount_folder_id)));
    }

    function getCount($user_id) {
        return $this->find('count', array('conditions' => array('user_id' => (int) $user_id)));
    }

    function getUserAccount($account_folder_id, $user_id) {
        return $this->find('first', array('conditions' => array(
                        'user_id' => (int) $user_id,
                        'account_folder_id' => (int) $account_folder_id))
        );
    }

    public function get_all_account_folder_user($user_id, $sort = '') {
        $user_id = (int) $user_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folders as AccountFolder',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = AccountFolderUser.account_folder_id')
            )
        );
        $fields = array(
            'AccountFolder.*',
            'AccountFolderUser.*'
        );

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolderUser.user_id' => $user_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1
            ),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function get_all_account_folders_user_by_account($user_id, $account_id) {
        $user_id = (int) $user_id;
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folders as AccountFolder',
                'type' => 'inner',
                'conditions' => array('AccountFolder.account_folder_id = AccountFolderUser.account_folder_id')
            )
        );
        $fields = array(
            'AccountFolder.*',
            'AccountFolderUser.*'
        );

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolderUser.user_id' => $user_id,
                'AccountFolder.account_id' => $account_id
            ),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function checkuserexistinhuddle($account_folder_id, $user_id) {
        $return = $this->find('count', array(
            "conditions" => array("account_folder_id" => $account_folder_id, "user_id" => $user_id)
        ));
        return $return;
    }

    public function getUserNameCoaches($account_folder_id) {
        $fields = array(
            "CONCAT_WS(' ',users.`first_name`, users.`last_name`) AS user_name",
        );
        $account_folder_users = array(
            array(
                'table' => 'users',
                'type' => 'inner',
                'conditions' => array('`user_id`=users.id')
            )
        );
        $coaches_users = $this->find('all', array('conditions' => array(
                'is_coach' => '1',
                'account_folder_id' => (int) $account_folder_id),
            'joins' => $account_folder_users,
            'fields' => $fields
                )
        );
        $coaches_usrs_name = '';
        foreach ($coaches_users as $coaches_user) {
            if ($coaches_usrs_name == '') {
                if (isset($coaches_user[0])) {
                    $coaches_usrs_name = $coaches_user[0]['user_name'];
                }
            } else {

                if (isset($coaches_user[0])) {
                    $coaches_usrs_name = $coaches_usrs_name . ',' . $coaches_user[0]['user_name'];
                }
            }
        }
        return !empty($coaches_usrs_name) ? $coaches_usrs_name : '';
    }

    public function getUserNameMente($account_folder_id) {
        $fields = array(
            "CONCAT_WS(' ',users.`first_name`, users.`last_name`) AS user_name",
        );
        $account_folder_users = array(
            array(
                'table' => 'users',
                'type' => 'inner',
                'conditions' => array('`user_id`=users.id')
            )
        );
        $coaches_users = $this->find('all', array('conditions' => array(
                'is_mentee' => '1',
                'account_folder_id' => (int) $account_folder_id),
            'joins' => $account_folder_users,
            'fields' => $fields
                )
        );
        $coaches_usrs_name = '';
        foreach ($coaches_users as $coaches_user) {
            if ($coaches_usrs_name == '') {

                if (isset($coaches_user[0])) {
                    $coaches_usrs_name = $coaches_user[0]['user_name'];
                }
            } else {

                if (isset($coaches_user[0])) {
                    $coaches_usrs_name = $coaches_usrs_name . ',' . $coaches_user[0]['user_name'];
                }
            }
        }
        return !empty($coaches_usrs_name) ? $coaches_usrs_name : '';
    }
    
    public function getUsernameEvaluator($account_folder_id, $evaluator_id = '') {
        $is_evaluator = '';
        if ($evaluator_id != '') {
            $is_evaluator = "user_id = $evaluator_id";
        }
        $fields = array(
            "CONCAT_WS(' ',users.`first_name`, users.`last_name`) AS user_name",
        );

        $account_folder_users = array(
            array(
                'table' => 'users',
                'type' => 'inner',
                'conditions' => array('`user_id`=users.id')
            )
        );
        $coaches_users = $this->find('all', array('conditions' => array(
                'is_coach' => '1',
                'account_folder_id' => (int) $account_folder_id,
                $is_evaluator
            ),
            'joins' => $account_folder_users,
            'fields' => $fields,
            'group'=>'user_id'
                )
        );
        $coaches_usrs_name = '';
        foreach ($coaches_users as $coaches_user) {           
            if ($coaches_usrs_name == '') {
                if (isset($coaches_user[0])) {
                    $coaches_usrs_name = $coaches_user[0]['user_name'];
                }
            } else {

                if (isset($coaches_user[0])) {
                    $coaches_usrs_name = $coaches_usrs_name . ',' . $coaches_user[0]['user_name'];
                }
            }
        }
        return !empty($coaches_usrs_name) ? $coaches_usrs_name : '';
    }

    public function getParticipants($account_folder_id) {
        $fields = array(
            "CONCAT_WS(' ',users.`first_name`, users.`last_name`) AS user_name",
            "users.id"
        );
        $account_folder_users = array(
            array(
                'table' => 'users',
                'type' => 'inner',
                'conditions' => array('`user_id`=users.id')
            )
        );
        $coaches_users = $this->find('all', array('conditions' => array(
                'is_mentee' => '1',
                'account_folder_id' => (int) $account_folder_id),
            'joins' => $account_folder_users,
            'fields' => $fields
                )
        );        
         
        return $coaches_users;
    }

}
