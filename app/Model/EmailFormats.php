<?php

// app/Model/User.php
class EmailFormats extends AppModel {

    var $useTable = 'email_formats';

    function getAllEmails() {
        $joins = array(
            array(
                'table' => 'email_unsubscribers AS EU',
                'type' => 'INNER',
                'conditions' => array('EmailFormats.id = EU.email_format_id')
            ),
        );
        return $this->find('all', array(
                    'joins' => $joins,
                    'fields' => array('EU.*', 'EmailFormats.*')
        ));
    }

}
