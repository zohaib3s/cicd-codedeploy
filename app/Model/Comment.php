<?php

// app/Model/User.php
class Comment extends AppModel {

    var $useTable = 'comments';

    function get($comment_id) {
        $comment_id = (int) $comment_id;
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('User.id=Comment.created_by')
            )
        );
        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
        //'Document.url', 'Document.original_file_name', 'Document.id as document_id'
        return $this->find('first', array(
                    'joins' => $joins,
                    'conditions' => array('Comment.id' => $comment_id, 'Comment.active' => '1'),
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => 'Comment.created_date DESC'
        ));
    }

    function get_parent_discussion($comment_id) {
        $comment_id = (int) $comment_id;
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => 'comment_attachments as CommentAttachment',
                'type' => 'left',
                'conditions' => array('Comment.id=CommentAttachment.comment_id')
            ),
            array(
                'table' => 'documents as Document',
                'type' => 'left',
                'conditions' => array('CommentAttachment.document_id=Document.id')
            )
        );
        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image', 'Document.url', 'Document.original_file_name', 'Document.id as document_id');
        return $this->find('first', array(
                    'joins' => $joins,
                    'conditions' => array('Comment.id' => $comment_id),
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => 'Comment.created_date DESC'
        ));
    }

    function getUsers($comment_id) {
        $comment_id = (int) $comment_id;
        $joins = array(
            array(
                'table' => 'comment_users as CommentUser',
                'type' => 'left',
                'conditions' => array('CommentUser.comment_id=Comment.id')
            ),
            array(
                'table' => 'users as CommentUserUser',
                'type' => 'left',
                'conditions' => array('CommentUser.user_id=CommentUserUser.id')
            )
        );
        $fields = array('Comment.*', 'CommentUserUser.id');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('Comment.id' => $comment_id),
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => 'Comment.created_date DESC'
        ));
    }

    function getComments($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => 'comment_attachments as CommentAttachment',
                'type' => 'left',
                'conditions' => array('Comment.id=CommentAttachment.comment_id')
            ),
            array(
                'table' => 'documents as Document',
                'type' => 'left',
                'conditions' => array('CommentAttachment.document_id=Document.id')
            )
        );
        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image', 'Document.url', 'Document.original_file_name', 'Document.id as document_id');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('Comment.ref_type' => '1', 'Comment.active' => '1', 'Comment.parent_comment_id' => null, 'Comment.ref_id' => $account_folder_id, 'user_id!=""'),
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => 'Comment.created_date ASC'
        ));
    }

    function getCommentsSearch($account_folder_id, $title = '', $sort = '') {
        $account_folder_id = (int) $account_folder_id;
        if (empty($sort)) {
            $orderBy = "Comment.title ASC";
        } else {
            $orderBy = $sort;
        }

        $conditions = array('user_id != ""');
        $conditions['Comment.ref_type'] = 1;
        $conditions['Comment.active'] = 1;
        $conditions['Comment.parent_comment_id'] = null;
        $conditions['Comment.ref_id'] = $account_folder_id;

        if (!empty($title)) {
            $conditions['or'] = array(
                'Comment.title like ' => "%$title%",
                'Comment.comment like ' => "%$title%",
                'Comment.id IN (select parent_comment_id from comments where comment like \'%' . $title . '%\' )'
            );
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => 'comment_attachments as CommentAttachment',
                'type' => 'left',
                'conditions' => array('Comment.id=CommentAttachment.comment_id')
            ),
            array(
                'table' => 'documents as Document',
                'type' => 'left',
                'conditions' => array('CommentAttachment.document_id=Document.id')
            )
        );
        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image',
            'Document.url', 'Document.original_file_name', 'Document.id as document_id');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => $orderBy
        ));
    }

    function getVideoComment($video_id, $comment_id) {
        $video_id = (int) $video_id;
        $comment_id = (int) $comment_id;

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => '`account_comment_tags` AS act',
                'type' => 'left',
                'conditions' => array('`Comment`.id=act.`comment_id`')
            ),
        );

        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
        $order_by = 'Comment.time ASC';


        $conditions = array('Comment.ref_type' => '2', 'Comment.ref_id' => $video_id, 'Comment.id' => $comment_id);



        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => $order_by
        ));
    }

    function getVideoComments($video_id, $changType = '', $huddel_id = '', $searchCmt = '', $tags = '', $support_audio_annotation = 0, $get_all_comments = 0, $include_replys = false) {
        $video_id = (int) $video_id;
        if ($huddel_id != '') {
            $huddel_id = (int) $huddel_id;
        }
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => '`account_comment_tags` AS act',
                'type' => 'left',
                'conditions' => array('`Comment`.id=act.`comment_id`')
            ),
        );

        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
        if ($changType == '1') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id ASC';
        } elseif ($changType == '2') {
            $fields = array(
                'Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image',
                'CASE WHEN time IS NULL THEN (SELECT MAX(id) + 1 FROM comments) ELSE time  END time_modified'
            );
            $order_by = 'time_modified ASC';
        } elseif ($changType == '3') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'User.first_name ASC,Comment.time ASC';
        } elseif ($changType == '4') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id DESC';
        } elseif ($changType == '5') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id DESC';
        } elseif ($changType == '6') {
            $order_by = 'Comment.time ASC';
        } else {
            $order_by = 'Comment.time ASC';
        }
        if ($support_audio_annotation == 1) {
            if ($get_all_comments == 1)
                $cmt_type = array('2', '6', '7');
            else
                $cmt_type = array('2', '6');
        }else {
            $cmt_type = array('2');
        }
        if ($include_replys) {
            array_push($cmt_type, '3');
            $parent_condition = '';
        } else {
            $parent_condition = 'Comment.parent_comment_id IS NULL';
        }
        if (!empty($searchCmt)) {
            if (!empty($tags)) {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"), "act.account_tag_id" => $tags);
            } else {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"));
            }
        } else {
            if (!empty($tags)) {
                $conditions = array($parent_condition, 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, "act.account_tag_id" => $tags);
            } else {
                $conditions = array($parent_condition, 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id);
            }
        }




        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => $order_by
        ));
    }
    
    
    function getLiveVideoComments($video_id, $changType = '', $huddel_id = '', $searchCmt = '', $tags = '', $support_audio_annotation = 0, $get_all_comments = 0, $include_replys = false,$limit = '',$bool_active = false) {
        $video_id = (int) $video_id;
        if ($huddel_id != '') {
            $huddel_id = (int) $huddel_id;
        }
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => '`account_comment_tags` AS act',
                'type' => 'left',
                'conditions' => array('`Comment`.id=act.`comment_id`')
            ),
        );

        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
        if ($changType == '1') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id ASC';
        } elseif ($changType == '2') {
            $fields = array(
                'Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image',
                'CASE WHEN time IS NULL THEN (SELECT MAX(id) + 1 FROM comments) ELSE time  END time_modified'
            );
            $order_by = 'time_modified ASC';
        } elseif ($changType == '3') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'User.first_name ASC,Comment.time ASC';
        } elseif ($changType == '4') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id DESC';
        } elseif ($changType == '5') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id DESC';
        } elseif ($changType == '6') {
            $order_by = 'Comment.time ASC';
        } else {
            $order_by = 'Comment.time ASC';
        }
        if ($support_audio_annotation == 1) {
            if ($get_all_comments == 1)
                $cmt_type = array('2', '6', '7');
            else
                $cmt_type = array('2', '6');
        }else {
            $cmt_type = array('2');
        }
        if ($include_replys) {
            array_push($cmt_type, '3');
            $parent_condition = '';
        } else {
            $parent_condition = 'Comment.parent_comment_id IS NULL';
        }
        if (!empty($searchCmt)) {
            if (!empty($tags)) {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"), "act.account_tag_id" => $tags);
            } else {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"));
            }
        } else {
            if (!empty($tags)) {
                $conditions = array($parent_condition, 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, "act.account_tag_id" => $tags);
            } else {
                $conditions = array($parent_condition, 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id);
            }
        }
        
        if($bool_active)
        {
           $conditions['Comment.active'] = '1'; 
        }
        
        if(empty($limit))
        {
            
        return $this->find('all', array(
          'joins' => $joins,
          'conditions' => $conditions,
          'fields' => $fields,
          'group' => 'Comment.id',
          'order' => $order_by,
        ));
            
        }
        else
        {
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => $order_by,
                    'limit' => $limit
        ));
        }
    }
    
    
        function getActiveVideoComments($video_id, $changType = '', $huddel_id = '', $searchCmt = '', $tags = '', $support_audio_annotation = 0, $get_all_comments = 0, $include_replys = false) {
        $video_id = (int) $video_id;
        if ($huddel_id != '') {
            $huddel_id = (int) $huddel_id;
        }
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => '`account_comment_tags` AS act',
                'type' => 'left',
                'conditions' => array('`Comment`.id=act.`comment_id`')
            ),
        );

        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
        if ($changType == '1') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id ASC';
        } elseif ($changType == '2') {
            $fields = array(
                'Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image',
                'CASE WHEN time IS NULL THEN (SELECT MAX(id) + 1 FROM comments) ELSE time  END time_modified'
            );
            $order_by = 'time_modified ASC';
        } elseif ($changType == '3') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'User.first_name ASC,Comment.time ASC';
        } elseif ($changType == '4') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id DESC';
        } elseif ($changType == '5') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id DESC';
        } elseif ($changType == '6') {
            $order_by = 'Comment.time ASC';
        } else {
            $order_by = 'Comment.time ASC';
        }
        if ($support_audio_annotation == 1) {
            if ($get_all_comments == 1)
                $cmt_type = array('2', '6', '7');
            else
                $cmt_type = array('2', '6');
        }else {
            $cmt_type = array('2');
        }
        if ($include_replys) {
            array_push($cmt_type, '3');
            $parent_condition = '';
        } else {
            $parent_condition = 'Comment.parent_comment_id IS NULL';
        }
        if (!empty($searchCmt)) {
            if (!empty($tags)) {
                $conditions = array('Comment.active' => 1 ,'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"), "act.account_tag_id" => $tags);
            } else {
                $conditions = array('Comment.active' => 1 , 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"));
            }
        } else {
            if (!empty($tags)) {
                $conditions = array($parent_condition, 'Comment.active' => 1 , 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, "act.account_tag_id" => $tags);
            } else {
                $conditions = array($parent_condition, 'Comment.active' => 1 , 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id);
            }
        }




        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => $order_by
        ));
    }
    
    
    
        function getVideoComments_perfomance_level($video_id ,$account_tag_id, $changType = '', $huddel_id = '', $searchCmt = '', $tags = '', $support_audio_annotation = 0, $get_all_comments = 0, $include_replys = false) {
        $video_id = (int) $video_id;
        if ($huddel_id != '') {
            $huddel_id = (int) $huddel_id;
        }
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => '`account_comment_tags` AS act',
                'type' => 'left',
                'conditions' => array('`Comment`.id=act.`comment_id`')
            ),
        );

        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
        if ($changType == '1') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id ASC';
        } elseif ($changType == '2') {
            $fields = array(
                'Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image',
                'CASE WHEN time IS NULL THEN (SELECT MAX(id) + 1 FROM comments) ELSE time  END time_modified'
            );
            $order_by = 'time_modified ASC';
        } elseif ($changType == '3') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'User.first_name ASC,Comment.time ASC';
        } elseif ($changType == '4') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id DESC';
        } elseif ($changType == '5') {
            $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
            $order_by = 'Comment.id DESC';
        } elseif ($changType == '6') {
            $order_by = 'Comment.time ASC';
        } else {
            $order_by = 'Comment.time ASC';
        }
        if ($support_audio_annotation == 1) {
            if ($get_all_comments == 1)
                $cmt_type = array('2', '6', '7');
            else
                $cmt_type = array('2', '6');
        }else {
            $cmt_type = array('2');
        }
        if ($include_replys) {
            array_push($cmt_type, '3');
            $parent_condition = '';
        } else {
            $parent_condition = 'Comment.parent_comment_id IS NULL';
        }
        if (!empty($searchCmt)) {
            if (!empty($tags)) {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"), "act.account_tag_id" => $tags);
            } else {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"), "act.account_tag_id" => $account_tag_id);
            }
        } else {
            if (!empty($tags)) {
                $conditions = array($parent_condition, 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, "act.account_tag_id" => $tags);
            } else {
                $conditions = array($parent_condition, 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, "act.account_tag_id" => $account_tag_id);
            }
        }




        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => $order_by
        ));
    }
    
    

    function commentReplys($comment_id, $get_all_comments = 0) {
        $comment_id = (int) $comment_id;
        $joins = array(
            array('table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => 'comment_attachments as CommentAttachment',
                'type' => 'left',
                'conditions' => array('Comment.id=CommentAttachment.comment_id')
            ),
            array(
                'table' => 'documents as Document',
                'type' => 'left',
                'conditions' => array('CommentAttachment.document_id=Document.id')
            )
        );
        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image', 'Document.url', 'Document.original_file_name', 'Document.id as document_id');
        if ($get_all_comments == 1) {
            $active = array(0, 1);
        } else {
            $active = array(1);
        }
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('Comment.parent_comment_id' => $comment_id, 'Comment.active' => $active, 'user_id!=""'),
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => 'Comment.created_date ASC'
        ));
    }
    
    
        function commentReplys_single($comment_id, $single_comment_id , $get_all_comments = 0) {
        $comment_id = (int) $comment_id;
        $joins = array(
            array('table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => 'comment_attachments as CommentAttachment',
                'type' => 'left',
                'conditions' => array('Comment.id=CommentAttachment.comment_id')
            ),
            array(
                'table' => 'documents as Document',
                'type' => 'left',
                'conditions' => array('CommentAttachment.document_id=Document.id')
            )
        );
        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image', 'Document.url', 'Document.original_file_name', 'Document.id as document_id');
        if ($get_all_comments == 1) {
            $active = array(0, 1);
        } else {
            $active = array(1);
        }
        return $this->find('first', array(
                    'joins' => $joins,
                    'conditions' => array('Comment.parent_comment_id' => $comment_id, 'Comment.id' => $single_comment_id , 'Comment.active' => $active, 'user_id!=""'),
                    'fields' => $fields,
                    'group' => 'Comment.id',
                    'order' => 'Comment.created_date ASC'
        ));
    }

    function getReplysCount($discussion_id) {
        $discussion_id = (int) $discussion_id;
        return $this->find('count', array('conditions' => array('parent_comment_id' => $discussion_id, 'active' => 1)));
    }

    function getReplysAttachments($discussion_id) {
        $discussion_id = (int) $discussion_id;
        $joins = array(
            array('table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=Comment.user_id')
            ),
            array(
                'table' => 'comment_attachments as CommentAttachment',
                'type' => 'left',
                'conditions' => array('Comment.id=CommentAttachment.comment_id')
            ),
            array(
                'table' => 'documents as Document',
                'type' => 'left',
                'conditions' => array('CommentAttachment.document_id=Document.id')
            )
        );
        $fields = array('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image', 'Document.url', 'Document.original_file_name', 'Document.id as document_id');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('parent_comment_id' => $discussion_id),
                    'fields' => $fields
        ));
    }

}
