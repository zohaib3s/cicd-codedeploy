<?php

// app/Model/User.php
class Document extends AppModel {

    var $useTable = 'documents';

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function getVideoDetails($document_id, $doc_type = 1) {
        $document_id = (int) $document_id;
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );
        return $this->find('first', array(
                    'joins' => $joins,
                    'conditions' => array('Document.id' => $document_id, 'doc_type' => $doc_type),
                    'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title','afd.account_folder_id', 'afd.desc','afd.id')
        ));
    }

    function getLiveVideoDetails($document_id) {
        $document_id = (int) $document_id;
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );
        $result = $this->find('first', array(
            'joins' => $joins,
            'conditions' => array('Document.id' => $document_id, 'doc_type' => 4),
            'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc'),
        ));
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    function getVideos($account_folder_id, $title = '', $sort = '', $limit = 0, $page = 0, $user_id = 0, $doc_type = 1) {
        $account_folder_id = (int) $account_folder_id;
        $feedbac_sort = '';
        if (empty($sort)) {
            $orderBy = "Document.created_date DESC";
        } elseif ($sort == 'published_feedback') {
            $orderBy = 'cmt.active DESC';
        } elseif ($sort == 'unpublished_feedback') {
            $orderBy = 'cmt.active ASC';
        } else {
            $orderBy = $sort;
        }

        $conditions = array();
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['Document.doc_type'] = $doc_type;
        $conditions['Document.active'] = '1';


        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR (User.first_name like '%" . $title . "%' OR User.last_name like '%" . $title . "%') OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') OR Document.id IN (select act.ref_id from account_comment_tags as act WHERE act.tag_title like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'comments as cmt',
                'type' => 'left',
                'conditions' => array('cmt.ref_id = Document.id')
            )
        );
        $params = array(
            'joins' => $joins,
            'group' => 'Document.id',
            'conditions' => $conditions,
            'order' => $orderBy,
            'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.id', 'afd.account_folder_id',
                '(SELECT "autocreated" FROM account_folders_meta_data WHERE account_folder_id = afd.account_folder_id AND meta_data_name = "autocreated" AND meta_data_value = 1) AS AutoCreated',
                '(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_name" AND afd.site_id = "' . $this->site_id . '") AS WebUploaderName',
                '(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_email" AND afd.site_id = "' . $this->site_id . '") AS WebUploaderEmail'
            )
        );
        if (0 < (int) $limit) {
            $params['limit'] = (int) $limit;
            $params['offset'] = (int) $page * (int) $limit;
        }
        return $this->find('all', $params);
    }

    function getVideos_optimised($account_folder_id, $video_id, $title = '', $sort = '', $limit = 0, $page = 0, $user_id = 0, $doc_type = 1) {
        $account_folder_id = (int) $account_folder_id;


        $conditions = array();
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['Document.doc_type'] = $doc_type;
        $conditions['Document.active'] = '1';
        $conditions['Document.id'] = $video_id;

        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );
        $params = array(
            'joins' => $joins,
            'group' => 'Document.id',
            'conditions' => $conditions,
            'fields' => array('Document.id')
        );

        return $this->find('all', $params);
    }

    function getObVideos($account_folder_id, $title = '', $sort = '', $limit = 0, $page = 0, $user_id = 0, $doc_type = 1) {
        $account_folder_id = (int) $account_folder_id;

        if (empty($sort)) {
            $orderBy = "Document.id DESC";
        } else {
            $orderBy = $sort;
        }

        $conditions = array();
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['Document.doc_type'] = $doc_type;
        $conditions['Document.active'] = '1';

        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR (User.first_name like '%" . $title . "%' OR User.last_name like '%" . $title . "%') OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') OR Document.id IN (select act.ref_id from account_comment_tags as act WHERE act.tag_title like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );
        $params = array(
            'joins' => $joins,
            'conditions' => $conditions,
            'order' => $orderBy,
            'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.id', 'afd.account_folder_id',
                '(SELECT "autocreated" FROM account_folders_meta_data WHERE account_folder_id = afd.account_folder_id AND meta_data_name = "autocreated" AND meta_data_value = 1) AS AutoCreated',
                '(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_name" AND afd.site_id = "' . $this->site_id . '") AS WebUploaderName',
                '(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_email" AND afd.site_id = "' . $this->site_id . '") AS WebUploaderEmail'
            )
        );
        if (0 < (int) $limit) {
            $params['limit'] = (int) $limit;
            $params['offset'] = (int) $page * (int) $limit;
        }
        return $this->find('all', $params);
    }

    function countVideos($account_folder_id, $title = '', $user_id = 0, $doc_type = 1) {
        $account_folder_id = (int) $account_folder_id;

        $conditions = array();
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['Document.doc_type'] = $doc_type;
        $conditions['Document.active'] = '1';

        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );

        return $this->find('count', array(
                    'joins' => $joins,
                    'conditions' => $conditions
        ));
    }

    function countVideos_coachee($account_folder_id, $title = '', $user_id = 0, $doc_type = 1) {
        $account_folder_id = (int) $account_folder_id;

        $conditions = array();
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['Document.doc_type'] = $doc_type;
        $conditions['Document.active'] = '1';
        $conditions['!Document.is_associated'] = '0';


        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );

        return $this->find('count', array(
                    'joins' => $joins,
                    'conditions' => $conditions
        ));
    }

    function countVideosEvaluator($account_folder_id, $title = '', $user_id = 0, $doc_type = 1) {
        $account_folder_id = (int) $account_folder_id;

        $conditions = array();
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['Document.doc_type'] = $doc_type;
        $conditions['Document.active'] = '1';

        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );

        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions
        ));
    }

    function countVideosEvaluator_access($account_folder_id, $user_id = 0, $evaluatorids, $title = '', $doc_type = 1) {
        $account_folder_id = (int) $account_folder_id;

//        $conditions = array();
//        $conditions['afd.account_folder_id'] = $account_folder_id;
//        $conditions['Document.doc_type'] = $doc_type;
//        $conditions['Document.active'] = '1';
//
//        if ($user_id) {
//            $conditions['Document.created_by'] = $user_id;
//            $conditions['Document.created_by'] = $evaluatorids;
//        }
        $evaluatorids = implode(',', $evaluatorids);
        if (empty($evaluatorids)) {

            $conditions = array(
                'afd.account_folder_id' => $account_folder_id,
                'Document.doc_type' => $doc_type,
                'Document.active' => '1',
                "(Document.created_by =$user_id)"
            );
        } else {

            $conditions = array(
                'afd.account_folder_id' => $account_folder_id,
                'Document.doc_type' => $doc_type,
                'Document.active' => '1',
                "(Document.created_by IN($evaluatorids) OR Document.created_by =$user_id)"
            );
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );

        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions
        ));
    }

    function getWorkspaceLiveObservationsInReview($user_id, $document_ids) {

        //1=huddle, 2=video library, 3=my files, etc.
        $conditions = array();
        $conditions['af.folder_type'] = 3;
        $conditions['af.active'] = 1;
        $conditions['Document.doc_type'] = 3;
        $conditions['Document.is_processed'] = '4';
        $conditions['Document.active'] = '1';
        $conditions[] = " Document.id IN ($document_ids) ";


        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => array('af.account_folder_id= afd.account_folder_id')
            ),
            array(
                'table' => 'user_activity_logs as ual',
                'type' => 'inner',
                'conditions' => array('ual.user_id = Document.created_by  AND ual.ref_id = Document.id AND ual.account_id = Document.account_id AND ual.type = 20')
            )
        );

        $fields = array('Document.*', 'af.name', 'af.desc', 'af.account_folder_id', 'User.first_name', 'User.last_name', 'ual.environment_type');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields
        ));
    }

    function getHuddleLiveVideosInReview($user_id, $document_ids) {

        //1=huddle, 2=video library, 3=my files, etc.
        $conditions = array();
        $conditions['af.folder_type'] = 1;
        $conditions['af.active'] = 1;
        $conditions['Document.doc_type'] = 4;
        $conditions['Document.is_processed'] = '4';
        $conditions['Document.active'] = '1';
        $conditions[] = " Document.id IN ($document_ids) ";


        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => array('af.account_folder_id= afd.account_folder_id')
            ),
            array(
                'table' => 'user_activity_logs as ual',
                'type' => 'inner',
                'conditions' => array('ual.user_id = Document.created_by  AND ual.ref_id = Document.id AND ual.account_id = Document.account_id AND ual.type = 24')
            )
        );

        $fields = array('Document.*', 'af.name', 'af.desc', 'af.account_folder_id', 'User.first_name', 'User.last_name', 'ual.environment_type');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields
        ));
    }

    function getWorkspaceLiveObservations($user_id) {

        //1=huddle, 2=video library, 3=my files, etc.
        $conditions = array();
        $conditions['af.folder_type'] = 3;
        $conditions['af.active'] = 1;
        $conditions['Document.doc_type'] = 3;
        $conditions['Document.is_processed'] = '5';
        $conditions['Document.active'] = '1';

        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => array('af.account_folder_id= afd.account_folder_id')
            ),
            array(
                'table' => 'user_activity_logs as ual',
                'type' => 'inner',
                'conditions' => array('ual.user_id = Document.created_by  AND ual.ref_id = Document.id AND ual.account_id = Document.account_id AND ual.type = 20')
            )
        );

        $fields = array('Document.*', 'af.name', 'af.desc', 'af.account_folder_id', 'User.first_name', 'User.last_name', 'ual.environment_type');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'limit' => '1'
        ));
    }

    function getHuddleLiveVideos($user_id, $huddle_id) {

        //1=huddle, 2=video library, 3=my files, etc.
        $conditions = array();
        $conditions['af.folder_type'] = 1;
        $conditions['af.active'] = 1;
        $conditions['af.account_folder_id'] = $huddle_id;
        $conditions['ual.account_folder_id'] = $huddle_id;
        $conditions['Document.doc_type'] = 4;
        $conditions['Document.is_processed'] = array(5, 6);
        $conditions['Document.active'] = '1';

        if ($user_id) {
            // $conditions['Document.created_by'] = $user_id;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => array('af.account_folder_id= afd.account_folder_id')
            ),
            array(
                'table' => 'user_activity_logs as ual',
                'type' => 'inner',
                'conditions' => array('ual.user_id = Document.created_by  AND ual.ref_id = Document.id AND ual.account_id = Document.account_id AND ual.type = 24')
            )
        );

        $fields = array('Document.*', 'af.name', 'af.desc', 'af.account_folder_id', 'User.first_name', 'User.last_name', 'ual.environment_type');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'limit' => '1'
        ));
    }

    function countObservations($account_folder_id, $title = '', $user_id = 0, $doc_type = 3, $is_coach = true) {
        $account_folder_id = (int) $account_folder_id;

        $conditions = array();
        if (!$is_coach) {
            $conditions[] = "Document.is_associated is NOT NULL";
        }
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['Document.doc_type'] = $doc_type;
        $conditions['Document.active'] = '1';

        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );

        return $this->find('count', array(
                    'joins' => $joins,
                    'conditions' => $conditions
        ));
    }

    function countDocuments($account_folder_id, $title = '', $user_id = 0) {
        $account_folder_id = (int) $account_folder_id;

        $conditions = array();
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['Document.doc_type'] = '2';
        $conditions['Document.active'] = '1';

        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );

        return $this->find('count', array(
                    'joins' => $joins,
                    'conditions' => $conditions
        ));
    }

    function getVideoDocuments($document_id, $account_folder_id) {
        $document_id = (int) $document_id;
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folderdocument_attachments as afda',
                'type' => 'inner',
                'conditions' => array(
                    'afda.account_folder_document_id= afd.id'
                )
            ),
            array(
                'table' => 'account_folder_documents as vafd',
                'type' => 'inner',
                'conditions' => array(
                    'afda.attach_id= vafd.document_id',
                    'afd.account_folder_id= vafd.account_folder_id'
                )
            )
        );

        $fields = array('Document.*', 'vafd.title', 'vafd.desc', 'vafd.document_id as video_id');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('Document.id' => $document_id, 'afd.account_folder_id' => $account_folder_id, 'doc_type' => '2'),
                    'fields' => $fields
        ));
    }

    function getVideoDocumentsByVideo($document_id, $account_folder_id) {
        $document_id = (int) $document_id;
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folderdocument_attachments as afda',
                'type' => 'inner',
                'conditions' => array(
                    'afda.account_folder_document_id= afd.id and afda.attach_id=' . $document_id
                )
            )
        );

        $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');
        return $this->find('all', array(
                    'joins' => $joins,
                    //'conditions' => array('afd.account_folder_id' => $account_folder_id, 'doc_type' => '2'),
                    'conditions' => array('doc_type' => '2'),
                    'fields' => $fields,
                    'order' => 'Document.scripted_current_duration ASC'
        ));
    }

    function getVideoDocumentsByVideo_mobile($document_id, $account_folder_id, $user_id, $h_type, $if_evaluator, $is_coach_enable) {
        $document_id = (int) $document_id;
        $account_folder_id = (int) $account_folder_id;
        if ((!$if_evaluator) && (($h_type == '2' && $is_coach_enable) || $h_type == '3')) {
            $result = $this->query("SELECT
                                  `documents`.*,
                                  `afd`.`title`,
                                  `afd`.`desc`,
                                  `afd`.`account_folder_id`
                                FROM
                                  `documents`
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON `documents`.`id` = `afd`.`document_id`
                                  INNER JOIN `account_folderdocument_attachments` AS `afda`
                                    ON `afda`.`account_folder_document_id` = `afd`.`id`
                                WHERE `afda`.`attach_id` = " . $document_id . "
                                  AND `doc_type` = 2
                                  AND documents.site_id =" . $this->site_id . "
                                 AND scripted_current_duration IS NULL


                                UNION

                                SELECT
                                  `documents`.*,
                                  `afd`.`title`,
                                  `afd`.`desc`,
                                  `afd`.`account_folder_id`
                                FROM
                                  `documents`
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON `documents`.`id` = `afd`.`document_id`
                                  LEFT OUTER JOIN `account_folderdocument_attachments` AS `afda`
                                    ON `afda`.`account_folder_document_id` = `afd`.`id`
                                  LEFT OUTER JOIN `comment_attachments` AS `ca`
                                    ON `ca`.`document_id` = `documents`.`id`
                                  LEFT OUTER JOIN `comments` AS `cmt`
                                    ON `cmt`.`id` = `ca`.`comment_id`
                                WHERE `afda`.`attach_id` = " . $document_id . "
                                  AND `doc_type` = 2
                                  AND documents.site_id =" . $this->site_id . "
                                  AND (cmt.active IS NULL OR cmt.active = 1)
                                  AND scripted_current_duration IS NOT NULL ");
        } else {
            $result = $this->query("SELECT
                                  `documents`.*,
                                  `afd`.`title`,
                                  `afd`.`desc`,
                                  `afd`.`account_folder_id`
                                FROM
                                  `documents`
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON `documents`.`id` = `afd`.`document_id`
                                  INNER JOIN `account_folderdocument_attachments` AS `afda`
                                    ON `afda`.`account_folder_document_id` = `afd`.`id`
                                WHERE `afda`.`attach_id` = " . $document_id . "
                                  AND `doc_type` = 2
                                  AND documents.site_id =" . $this->site_id . "
                                 AND scripted_current_duration IS NULL


                                UNION

                                SELECT
                                  `documents`.*,
                                  `afd`.`title`,
                                  `afd`.`desc`,
                                  `afd`.`account_folder_id`
                                FROM
                                  `documents`
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON `documents`.`id` = `afd`.`document_id`
                                  LEFT OUTER JOIN `account_folderdocument_attachments` AS `afda`
                                    ON `afda`.`account_folder_document_id` = `afd`.`id`
                                  LEFT OUTER JOIN `comment_attachments` AS `ca`
                                    ON `ca`.`document_id` = `documents`.`id`
                                  LEFT OUTER JOIN `comments` AS `cmt`
                                    ON `cmt`.`id` = `ca`.`comment_id`
                                WHERE `afda`.`attach_id` = " . $document_id . "
                                  AND `doc_type` = 2
                                  AND documents.site_id =" . $this->site_id . "
                                  AND scripted_current_duration IS NOT NULL ");
        }

        return $result;
    }

    function getVideoDocumentsByObservation($account_folder_id) {
        //$document_id = (int) $document_id;
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folderdocument_attachments as afda',
                'type' => 'left',
                'conditions' => array(
                    'afda.account_folder_document_id= afd.id and afda.attach_id=' . $account_folder_id
                )
            )
        );

        $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');
        $ret = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array('afd.account_folder_id' => $account_folder_id, 'doc_type' => '2'),
//                    'conditions' => array('doc_type' => '2'),
            'fields' => $fields
        ));
        return $ret;
    }

    function getDocumentAssociatedVideos($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folderdocument_attachments as afda',
                'type' => 'inner',
                'conditions' => array(
                    'afda.account_folder_document_id= afd.id'
                )
            )
        );

        $fields = array('Document.*', 'afd.title', 'afd.desc', 'afda.attach_id');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('afd.account_folder_id' => $account_folder_id, 'doc_type' => '2'),
                    'fields' => $fields
        ));
    }

    function getDocumentAssociatedVideosForWorkspace($account_id, $user_id) {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id = afd.document_id')
            ),
            array(
                'table' => 'account_folderdocument_attachments as afda',
                'type' => 'inner',
                'conditions' => array('afda.account_folder_document_id = afd.id')
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => array('af.account_folder_id = afd.account_folder_id')
            )
        );

        $fields = array('Document.*', 'afd.title', 'afd.desc', 'afda.attach_id');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('af.account_id' => $account_id, 'af.created_by' => $user_id, 'doc_type' => '2'),
                    'fields' => $fields
        ));
    }

    function getDocuments($account_folder_id, $title = '', $sort = '', $doc_type = '') {
        $account_folder_id = (int) $account_folder_id;

        if (empty($sort)) {
            $orderBy = "afd.title ASC";
        } else {
            $orderBy = $sort;
        }

        $conditions = array();

        $conditions['afd.account_folder_id'] = $account_folder_id;
        if ($doc_type != '') {
            $conditions['doc_type'] = $doc_type;
        } else {
            $conditions['doc_type'] = '2';
        }
        if (!empty($title)) {
            $conditions['afd.title like '] = "%$title%";
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'order' => "$orderBy",
                    'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.id', 'afd.title', 'afd.desc'),
        ));
    }

    function getCoacheeDocuments($account_folder_id, $title = '', $sort = '', $doc_type = '') {
        $account_folder_id = (int) $account_folder_id;
        $title_condition = "";

        if (empty($sort)) {
            $orderBy = "order by afd.title ASC";
        } else {
            $orderBy = "order by " . $sort;
        }


        if (!empty($title)) {
            $title_condition = "AND afd.title like '%$title%'";
        }

        $result = $this->query("(SELECT
                                  `Document`.*,
                                  `User`.`first_name`,
                                  `User`.`last_name`,
                                  `User`.`email`,
                                  `afd`.`id` AS afdid,
                                  `afd`.`title`,
                                  `afd`.`desc`
                                FROM
                                  `sibmeproduction`.`documents` AS `Document`
                                  INNER JOIN `sibmeproduction`.`users` AS `User`
                                    ON (
                                      `Document`.`created_by` = `User`.`id`
                                    )
                                  INNER JOIN `sibmeproduction`.`account_folder_documents` AS `afd`
                                    ON (
                                      `Document`.`id` = `afd`.`document_id`
                                    )
                                  INNER JOIN `sibmeproduction`.`account_folderdocument_attachments` AS `afda`
                                    ON (
                                      `afda`.`account_folder_document_id` = `afd`.`id`
                                    )
                                  INNER JOIN `sibmeproduction`.`comment_attachments` AS `ca`
                                    ON Document.`id` = ca.`document_id`
                                  INNER JOIN comments AS cmt
                                    ON cmt.`id` = ca.`comment_id`
                                WHERE `afd`.`account_folder_id` = " . $account_folder_id . "
                                  AND `doc_type` = 2
                                  AND cmt.`active` = '1'
                                  AND Document.site_id = " . $this->site_id . "
                              " . $title_condition . "
                                " . $orderBy . " )

                                UNION

                                (
                                SELECT
                                  `Document`.*,
                                  `User`.`first_name`,
                                  `User`.`last_name`,
                                  `User`.`email`,
                                  `afd`.`id` AS afdid,
                                  `afd`.`title`,
                                  `afd`.`desc`
                                FROM
                                  `sibmeproduction`.`documents` AS `Document`
                                  INNER JOIN `sibmeproduction`.`users` AS `User`
                                    ON (
                                      `Document`.`created_by` = `User`.`id`
                                    )
                                  INNER JOIN `sibmeproduction`.`account_folder_documents` AS `afd`
                                    ON (
                                      `Document`.`id` = `afd`.`document_id`
                                    )
                                  LEFT JOIN `sibmeproduction`.`account_folderdocument_attachments` AS `afda`
                                    ON (
                                      `afda`.`account_folder_document_id` = `afd`.`id`
                                    )
                                  LEFT JOIN `sibmeproduction`.`comment_attachments` AS `ca`
                                    ON Document.`id` = ca.`document_id`
                                  LEFT JOIN comments AS cmt
                                    ON cmt.`id` = ca.`comment_id`
                                WHERE `afd`.`account_folder_id` = " . $account_folder_id . "
                                  AND `doc_type` = 2
                                  AND  cmt.`active` IS NULL
                                  AND Document.site_id = " . $this->site_id . "
                                 " . $title_condition . "
                                " . $orderBy . "
                                )");


        return !empty($result) ? $result : array();
    }

    function buildMyFileQueryParams($account_id, $user_id, $title = '', $doc_type = '') {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;

        $conditions = array();
        $conditions['Document.account_id'] = $account_id;
        $conditions[] = " Document.doc_type IN ($doc_type) ";
        $conditions['Document.active'] = '1';
        $conditions['AccountFolder.account_id'] = $account_id;
        $conditions['AccountFolder.created_by'] = $user_id;
        $conditions['AccountFolder.folder_type'] = 3;
        $conditions['AccountFolder.active'] = 1;

        if (!empty($title)) {
            //$conditions['afd.title like '] = "%$title%";
            $keyword = " (afd.title like '%" . $title . "%' OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') OR Document.id IN (select act.ref_id from account_comment_tags as act WHERE act.tag_title like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folders AS AccountFolder',
                'type' => 'inner',
                'conditions' => array('afd.account_folder_id=AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'user_activity_logs as ual',
                'type' => 'left',
                'conditions' => array('ual.user_id = Document.created_by  AND ual.ref_id = Document.id AND ual.account_id = Document.account_id AND ual.type = 20 AND ual.date_added >= Document.created_date')
            )
        );

        return array(
            'joins' => $joins,
            'conditions' => $conditions
        );
    }

    function getMyFilesVideos($account_id, $user_id, $title = '', $sort = '', $limit = 10, $page = 0, $doc_type = 1) {
        if (empty($sort)) {
            $orderBy = "afd.title ASC";
        } else {
            $orderBy = $sort;
        }

        $params = $this->buildMyFileQueryParams($account_id, $user_id, $title, $doc_type);
        $params = $params + array(
            'order' => $orderBy,
            'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.*', 'AccountFolder.*', 'ual.environment_type'),
        );

        if (0 < (int) $limit) {
            $params['limit'] = (int) $limit;
            $params['offset'] = (int) $page * (int) $limit;
        }
        return $this->find('all', $params);
    }

    function countMyFilesVideos($account_id, $user_id, $title = '', $doc_type = 1) {
        $params = $this->buildMyFileQueryParams($account_id, $user_id, $title, $doc_type);
        return $this->find('count', $params);
    }

    function get_document_row($document_id) {
        $result = $this->find('first', array('conditions' => array('id' => (int) $document_id)));
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    function save($data = null, $validate = false, $fieldList = array()) {
        parent::save($data, $validate, $fieldList);
        return $this->id;
    }

    public function getByCommentId($comment_id) {
        $joins = array(
            array(
                'table' => 'comment_attachments as CommentAttachment',
                'type' => 'inner',
                'conditions' => array('CommentAttachment.document_id = Document.id')
            )
        );
        return $this->find('all', array(
                    'conditions' => array('CommentAttachment.comment_id = ' . (int) $comment_id),
                    'joins' => $joins
        ));
    }

    public function getVideoData($account_folder_id) {
        //$document_id = (int) $document_id;
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folderdocument_attachments as afda',
                'type' => 'left',
                'conditions' => array(
                    'afda.account_folder_document_id= afd.id and afda.attach_id=' . $account_folder_id
                )
            )
        );

        $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');
        $ret = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array('afd.account_folder_id' => $account_folder_id, 'doc_type' => '1'),
            'fields' => $fields
        ));
        return $ret;
    }

    function getMyFilesVideosEdtpa($account_id, $user_id, $title = '', $sort = '', $limit = 10, $page = 0, $doc_type = 1) {

        if (empty($sort)) {
            $orderBy = "afd.title ASC";
        } else {
            $orderBy = $sort;
        }
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        $conditions = "Document.account_id = $account_id AND Document.doc_type IN ($doc_type) AND Document.active = '1' AND AccountFolder.account_id = $account_id AND AccountFolder.created_by = $user_id AND AccountFolder.folder_type = 3 AND AccountFolder.active = 1";
        $conditions .=" AND doc_files.`transcoding_status` != 5  AND CASE WHEN Document.doc_type = '3' THEN Document.published = '1' AND Document.is_processed = '4' ELSE Document.published = '1' END ";
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folders AS AccountFolder',
                'type' => 'inner',
                'conditions' => array('afd.account_folder_id=AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'document_files as doc_files',
                'type' => 'inner',
                'conditions' => array('Document.id= doc_files.document_id')
            ),
            array(
                'table' => 'user_activity_logs as ual',
                'type' => 'left',
                'conditions' => array('ual.user_id = Document.created_by  AND ual.ref_id = Document.id AND ual.account_id = Document.account_id AND ual.type = 20')
            )
        );

        $params = array(
            'joins' => $joins,
            'conditions' => array($conditions),
            'groups' => array('Document.id')
        );
//        $params = $this->buildMyFileQueryParams($account_id, $user_id, $title, $doc_type);
        $params = $params + array(
            'order' => $orderBy,
            'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.*', 'AccountFolder.*', 'ual.environment_type', 'doc_files.transcoding_status'),
        );

        if (0 < (int) $limit) {
            $params['limit'] = (int) $limit;
            $params['offset'] = (int) $page * (int) $limit;
        }
        return $this->find('all', $params);
    }

    function countMyFilesVideosEdtpa($account_id, $user_id, $title = '', $doc_type = 1) {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        $conditions = "Document.account_id = $account_id AND Document.doc_type IN ($doc_type) AND Document.active = '1' AND AccountFolder.account_id = $account_id AND AccountFolder.created_by = $user_id AND AccountFolder.folder_type = 3 AND AccountFolder.active = 1";
        $conditions .=" AND CASE WHEN Document.doc_type = '3' THEN Document.published = '1' AND Document.is_processed = '4' ELSE Document.published = '1' END ";
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folders AS AccountFolder',
                'type' => 'inner',
                'conditions' => array('afd.account_folder_id=AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'user_activity_logs as ual',
                'type' => 'left',
                'conditions' => array('ual.user_id = Document.created_by  AND ual.ref_id = Document.id AND ual.account_id = Document.account_id AND ual.type = 20')
            )
        );

        $params = array(
            'joins' => $joins,
            'conditions' => array($conditions),
            'groups' => array('Document.id')
        );
        return $this->find('count', $params);
    }

    function getHuddleVideosEdtpa($account_folder_id, $title = '', $sort = '', $limit = 0, $page = 0, $user_id = 0, $doc_type = 1) {
        $account_folder_id = (int) $account_folder_id;
        $feedbac_sort = '';
        if (empty($sort)) {
            $orderBy = "Document.created_date DESC";
        } elseif ($sort == 'published_feedback') {
            $orderBy = 'cmt.active DESC';
        } elseif ($sort == 'unpublished_feedback') {
            $orderBy = 'cmt.active ASC';
        } else {
            $orderBy = $sort;
        }

        $conditions = array();
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['Document.doc_type'] = $doc_type;
        //$conditions['Document.account_id'] = 'af.account_id';
        $conditions['Document.active'] = '1';

        if ($user_id) {
            $conditions['Document.created_by'] = $user_id;
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR (User.first_name like '%" . $title . "%' OR User.last_name like '%" . $title . "%') OR Document.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') OR Document.id IN (select act.ref_id from account_comment_tags as act WHERE act.tag_title like '%" . $title . "%') ) ";
            $conditions[] = $keyword;
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )/* ,
              array(
              'table' => 'account_folders as af',
              'type' => 'INNER',
              'conditions' => array('af.`account_folder_id` = afd.`account_folder_id')
              ) */,
            array(
                'table' => 'comments as cmt',
                'type' => 'left',
                'conditions' => array('cmt.ref_id = Document.id')
            )
        );
        $params = array(
            'joins' => $joins,
            'conditions' => $conditions,
            'group' => 'Document.id',
            'order' => $orderBy,
            'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.id', 'afd.account_folder_id',
                '(SELECT "autocreated" FROM account_folders_meta_data WHERE account_folder_id = afd.account_folder_id AND meta_data_name = "autocreated" AND meta_data_value = 1) AS AutoCreated',
                '(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_name" AND afd.site_id = "' . $this->site_id . ') AS WebUploaderName',
                '(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_email" AND afd.site_id = "' . $this->site_id . ') AS WebUploaderEmail'
            )
        );
        if (0 < (int) $limit) {
            $params['limit'] = (int) $limit;
            $params['offset'] = (int) $page * (int) $limit;
        }
        return $this->find('all', $params);
    }

    function getDocumentDetails($document_id) {
        $document_id = (int) $document_id;
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folders as AccountFolders',
                'type' => 'inner',
                'conditions' => array('afd.account_folder_id = AccountFolders.account_folder_id')
            )
        );
        return $this->find('first', array(
                    'joins' => $joins,
                    'conditions' => array('Document.id' => $document_id),
                    'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'AccountFolders.folder_type', 'AccountFolders.account_folder_id')
        ));
    }

    function countAsseseeDocuments($afid) {
        $result = $this->query("(SELECT
                Document.*
            FROM
                `sibmeproduction`.`documents` AS `Document`
                INNER JOIN `sibmeproduction`.`users` AS `User`
                ON (
                    `Document`.`created_by` = `User`.`id`
                )
                INNER JOIN `sibmeproduction`.`account_folder_documents` AS `afd`
                ON (
                    `Document`.`id` = `afd`.`document_id`
                )
                INNER JOIN `sibmeproduction`.`account_folderdocument_attachments` AS `afda`
                ON (
                    `afda`.`account_folder_document_id` = `afd`.`id`
                )
                INNER JOIN `sibmeproduction`.`comment_attachments` AS `ca`
                ON Document.`id` = ca.`document_id`
                INNER JOIN comments AS cmt
                ON cmt.`id` = ca.`comment_id`
            WHERE `afd`.`account_folder_id` = $afid
                AND `Document`.`doc_type` = '2'
                AND `Document`.`site_id` = " . $this->site_id . "
                AND cmt.`active` = '1' )


                UNION

            (  SELECT
                Document.*
            FROM
                `sibmeproduction`.`documents` AS `Document`
                INNER JOIN `sibmeproduction`.`users` AS `User`
                ON (
                    `Document`.`created_by` = `User`.`id`
                )
                INNER JOIN `sibmeproduction`.`account_folder_documents` AS `afd`
                ON (
                    `Document`.`id` = `afd`.`document_id`
                )
                LEFT JOIN `sibmeproduction`.`account_folderdocument_attachments` AS `afda`
                ON (
                    `afda`.`account_folder_document_id` = `afd`.`id`
                )
                LEFT JOIN `sibmeproduction`.`comment_attachments` AS `ca`
                ON Document.`id` = ca.`document_id`
                LEFT JOIN comments AS cmt
                ON cmt.`id` = ca.`comment_id`
            WHERE `afd`.`account_folder_id` = $afid
                AND `Document`.`doc_type` = '2'
                AND `Document`.`site_id` = " . $this->site_id . "
                AND (cmt.`active` IS NULL))
            ");

        return count($result);
    }

}
