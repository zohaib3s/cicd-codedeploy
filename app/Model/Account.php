<?php

// app/Model/User.php
class Account extends AppModel {

    var $useTable = 'accounts';

    function getMyAccounts($account_id) {

        $joins = array(
            array(
                'table' => 'plans',
                'type' => 'left',
                'conditions' => array('plans.id=Account.plan_id')
            )
        );
        $fields = array('Account.*', 'plans.*');
        return $this->find('first', array('joins' => $joins, 'conditions' => array('Account.id' => (int) $account_id), 'fields' => $fields));
    }

    function getAccount($account_id) {
        $joins = array(
            array(
                'table' => 'users_accounts AS UserAccount',
                'type' => 'left',
                'conditions' => 'UserAccount.account_id = Account.id'
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('UserAccount.user_id = User.id')
            ),
        );
        $fields = array('User.*', 'UserAccount.account_id', 'UserAccount.role_id', 'Account.id AS accountId', 'Account.company_name', 'Account.braintree_subscription_id');
        return $this->find('first', array(
                    'joins' => $joins,
                    'conditions' => array('Account.id' => (int) $account_id),
                    'fields' => $fields
        ));
    }

    function getAllTrialAccounts() {
        $joins = array(
            array(
                'table' => 'users_accounts AS UserAccount',
                'type' => 'inner',
                'conditions' => 'UserAccount.account_id = Account.id'
            ),
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('UserAccount.user_id = User.id')
            ),
        );
        $fields = array('User.*', 'Account.*');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('Account.in_trial' => '1', 'UserAccount.role_id' => '100'),
                    'fields' => $fields
        ));
    }

    function getPreviousStorageUsed($account_id) {
        $fields = array('Account.storage_used');
        return $this->find('first', array(
                    'conditions' => array('id' => (int) $account_id),
                    'fields' => $fields
        ));
    }

    function haveParent($account_id) {
        $account_id = (int) $account_id;
        $fields = array(
            'Account.parent_account_id'
        );
        $result = $this->find('all', array(
            'conditions' => array(
                'id' => $account_id,
                'parent_account_id !=' => 0,
            ),
            'fields' => $fields
        ));
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function getRelatedAccounts($account_id, $have_parent = false) {
        if ($have_parent) {
            $query = 'SELECT
  Account.*,
  `AccountMeta`.`account_meta_data_id`,
  `AccountMeta`.`meta_data_name`,
  `AccountMeta`.`meta_data_value`
FROM
  (SELECT
    @r AS _id,
    (SELECT
      @r := parent_account_id
    FROM
      accounts
    WHERE id = _id) AS parent_account_id,
    @l := @l + 1 AS lvl
  FROM
    (SELECT
      @r := ' . $account_id . ',
      @l := 0) vars,
    accounts h
  WHERE @r <> 0) T1
  JOIN accounts Account
    ON T1._id = Account.id

  INNER JOIN `account_meta_data` AS `AccountMeta`
    ON (
      `AccountMeta`.`account_id` = Account.id
      AND `AccountMeta`.`meta_data_name` = "enable_video_library"
      AND `AccountMeta`.`meta_data_value` = 1
    )
WHERE Account.site_id = "' . $this->site_id . '"
GROUP BY Account.id
ORDER BY T1.lvl ASC';
            $result = $this->query($query);
            if ($result[0]['Account']['id'] != $account_id) {
                $result = array();
            }
        } else {
            $result = array();
        }

        return $result;
    }

    function is_account_expired($account_id){
        $trial_duration = Configure::read('trial_duration');
        $result =  $this->query("SELECT * FROM accounts WHERE id = $account_id AND in_trial = 1 AND created_at < NOW() - INTERVAL $trial_duration DAY ");
        if (count($result) > 0) {
            return true;
        }
        return false;
    }

}
