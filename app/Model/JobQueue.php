<?php

class JobQueue extends AppModel {

    var $useTable = 'JobQueue';
    
    function get_jobqueue_row($job_id) {
        $result = $this->find('first', array('conditions' => array('JobQueueId' => (int)$job_id)));
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

}
