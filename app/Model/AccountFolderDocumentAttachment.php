<?php

// app/Model/User.php
class AccountFolderDocumentAttachment extends AppModel {

    var $useTable = 'account_folderdocument_attachments';

    

    public function get_by_huddle_id($huddleId) {
        $huddleId = (int) $huddleId;
        $db = $this->getDataSource();
        $rs = $db->fetchAll(
            'select * from account_folderdocument_attachments where attach_id in (select document_id from account_folder_documents where account_folder_id = ?)',
            array($huddleId)
        );
        return $rs;
    }
}

