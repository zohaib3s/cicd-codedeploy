<?php
App::uses('AppModel', 'Model');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EdtpaAssessmentsHandbooks extends AppModel
{
    var $useTable = 'edtpa_assessments_handbooks';
    function get_assessment_handbook($account_id, $assessment_id)
    {

        $result = $this->find('all', array(
            'conditions' => array(
                //'account_id' => $account_id,
                'assessment_id' => $assessment_id
            )
        ));
        if ($result) {
            return $result;
        } else {
            return false;
        }

    }
}

?>