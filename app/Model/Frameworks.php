<?php

class Frameworks extends AppModel {

    var $useTable = 'frameworks';
    var $returnArray = array();

    public function getAllframeWorks() {
        $myArray = $this->find('all', array('order' => array('id ASC')));
        $flatArray = array();
        foreach ($myArray as $subArray) {
            $flatArray[] = $subArray['Frameworks'];
        }
        $rslt = $this->buildTree($flatArray);
        return $rslt;
    }

    private function buildTree(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    function findIfExixts($framework_code) {
        return ($this->find('count', array('conditions' => array('framework_code' => $framework_code))));
        
    }

}
