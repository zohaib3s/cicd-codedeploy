<?php

// app/Model/User.php
class Subject extends AppModel {

    var $useTable = 'subjects';

    function getSubjects($account_id) {
        $account_id = (int) $account_id;

        $conditions = array();
        $conditions['account_id'] = $account_id;

        return $result = $this->find('all', array(
            'conditions' => $conditions,
            'order' => "Subject.name ASC"
        ));
    }

}

