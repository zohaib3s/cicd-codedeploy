<?php

// app/Model/User.php
class AccountFolderSubject extends AppModel {

    var $useTable = 'account_folder_subjects';

    public function get($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'subjects as Subject',
                'type' => 'left',
                'conditions' => array('AccountFolderSubject.subject_id = Subject.id')
            )
        );
        $fields = array('Subject.*');
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array('AccountFolderSubject.account_folder_id' => $account_folder_id),
            'fields' => $fields
        ));
        return $result;
    }

}

