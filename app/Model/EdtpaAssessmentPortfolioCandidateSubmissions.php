<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EdtpaAssessmentPortfolioCandidateSubmissions extends AppModel {

    var $useTable = 'edtpa_assessment_portfolio_candidate_submissions';

    function getWorkspaces_huddles_files($nodeid, $foldertype, $candidate_id) {
        $candidate_id = (int) $candidate_id;
        $joins = array(
            array(
                'table' => 'documents',
                'type' => 'inner',
                'conditions' => array('documents.id = EdtpaAssessmentPortfolioCandidateSubmissions.document_id ')
            ),
            array(
                'table' => 'users',
                'type' => 'inner',
                'conditions' => array('users.id = documents.created_by ')
            )
            ,
            array(
                'table' => 'account_folder_documents',
                'type' => 'inner',
                'conditions' => array('account_folder_documents.document_id = documents.id ')
            )
            ,
            array(
                'table' => 'account_folders',
                'type' => 'inner',
                'conditions' => array('account_folders.account_folder_id = account_folder_documents.account_folder_id ')
            )
        );
        // CONCAT(`users`.`first_name`, ' ',`users`.`last_name`) AS username, `account_folder_documents`.`title` , `users`.`created_date`
        $fields = array('`users`.`first_name`, `users`.`last_name`',
            'account_folder_documents.title',
            '`users`.`created_date`',
            ' `account_folders`.`account_folder_id`',
            'documents.id');
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'documents.doc_type in (1,3)',
                'EdtpaAssessmentPortfolioCandidateSubmissions.edtpa_assessment_portfolio_node_id' => $nodeid,
                'EdtpaAssessmentPortfolioCandidateSubmissions.edtpa_assessment_account_candidate_id' => $candidate_id,
                'account_folders.folder_type' => $foldertype),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getWorkspaces_huddles_docs($nodeid, $foldertype, $candidate_id) {
        $candidate_id = (int) $candidate_id;
        $joins = array(
            array(
                'table' => 'documents',
                'type' => 'inner',
                'conditions' => array('documents.id = EdtpaAssessmentPortfolioCandidateSubmissions.document_id ')
            ),
            array(
                'table' => 'users',
                'type' => 'inner',
                'conditions' => array('users.id = documents.created_by ')
            )
            ,
            array(
                'table' => 'account_folder_documents',
                'type' => 'inner',
                'conditions' => array('account_folder_documents.document_id = documents.id ')
            )
            ,
            array(
                'table' => 'account_folders',
                'type' => 'inner',
                'conditions' => array('account_folders.account_folder_id = account_folder_documents.account_folder_id ')
            )
        );
        // CONCAT(`users`.`first_name`, ' ',`users`.`last_name`) AS username, `account_folder_documents`.`title` , `users`.`created_date`
        $fields = array('`users`.`first_name`, `users`.`last_name`',
            'account_folder_documents.title',
            '`users`.`created_date`',
            ' `account_folders`.`account_folder_id`',
            'documents.id');
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'documents.doc_type in (2)',
                'EdtpaAssessmentPortfolioCandidateSubmissions.edtpa_assessment_portfolio_node_id' => $nodeid,
                'EdtpaAssessmentPortfolioCandidateSubmissions.edtpa_assessment_account_candidate_id' => $candidate_id,
                'account_folders.folder_type' => $foldertype),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

}
