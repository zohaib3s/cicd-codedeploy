<?php

// app/Model/User.php
class User extends AppModel {

    public $validate = array(
        'username' => array(
            'rule' => 'isUnique',
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            )
        )
    );

    function getByHash($auth_token) {
        $result = $this->find('first', array(
            'conditions' => array('md5(id)' => $auth_token)
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function get($id) {
        $id = (int) $id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'inner',
                'conditions' => 'User.id = users_accounts.user_id'
            ),
            array(
                'table' => 'accounts',
                'type' => 'inner',
                'conditions' => 'users_accounts.account_id = accounts.id'
            ),
            array(
                'table' => 'roles',
                'type' => 'inner',
                'conditions' => 'users_accounts.role_id = roles.role_id'
            )
        );
        $fields = array('User.*', 'users_accounts.*', 'roles.name', 'roles.role_id', 'accounts.in_trial', 'accounts.is_suspended', 'accounts.storage_used', 'accounts.allow_launchpad', 'accounts.id as account_id', 'accounts.company_name', 'accounts.image_logo', 'accounts.header_background_color', 'accounts.nav_bg_color', 'accounts.created_at', 'accounts.usernav_bg_color', 'accounts.enable_live_rec', 'accounts.enable_goals');
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array('User.id' => $id,'users_accounts.is_active' => '1'),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function get1($id, $name) {
        $id = (int) $id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'inner',
                'conditions' => 'User.id = users_accounts.user_id'
            ),
            array(
                'table' => 'accounts',
                'type' => 'inner',
                'conditions' => 'users_accounts.account_id = accounts.id'
            ),
            array(
                'table' => 'roles',
                'type' => 'inner',
                'conditions' => 'users_accounts.role_id = roles.role_id'
            )
        );
        $fields = array('User.*', 'users_accounts.*', 'roles.name', 'roles.role_id', 'accounts.in_trial', 'accounts.is_suspended', 'accounts.storage_used', 'accounts.allow_launchpad', 'accounts.id as account_id', 'accounts.company_name', 'accounts.image_logo', 'accounts.header_background_color', 'accounts.nav_bg_color', 'accounts.created_at');
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array('User.id' => $id, 'accounts.company_name Like' => $name . '%'),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getUserInformation($account_id, $id) {
        $account_id = (int) $account_id;
        $id = (int) $id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'inner',
                'conditions' => 'User.id = users_accounts.user_id'
            ),
            array(
                'table' => 'accounts',
                'type' => 'inner',
                'conditions' => 'users_accounts.account_id = accounts.id'
            ),
            array(
                'table' => 'roles',
                'type' => 'inner',
                'conditions' => 'users_accounts.role_id = roles.role_id'
            )
        );
        $fields = array('User.*', 'users_accounts.*', 'roles.name', 'roles.role_id', 'accounts.in_trial', 'accounts.id as account_id', 'accounts.braintree_customer_id', 'accounts.braintree_subscription_id', 'accounts.company_name', 'accounts.image_logo', 'accounts.header_background_color', 'accounts.nav_bg_color', 'accounts.created_at');
        $result = $this->find('first', array(
            'joins' => $joins,
            'conditions' => array('users_accounts.user_id' => $id, 'users_accounts.account_id' => $account_id),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getDefaultAccount($id) {
        $id = (int) $id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'inner',
                'conditions' => 'User.id = users_accounts.user_id'
            ),
            array(
                'table' => 'accounts',
                'type' => 'inner',
                'conditions' => 'users_accounts.account_id = accounts.id'
            ),
            array(
                'table' => 'roles',
                'type' => 'inner',
                'conditions' => 'users_accounts.role_id = roles.role_id'
            )
        );
        $fields = array('User.*', 'roles.name', 'roles.role_id', 'accounts.id as account_id', 'accounts.parent_account_id', 'accounts.company_name');
        $result = $this->find('first', array(
            'joins' => $joins,
            'conditions' => array('User.id' => $id, 'users_accounts.is_default' => '1','users_accounts.is_active' => '1'),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            // Set Default Account and recursively call itself to return default account.
            $this->setDefaultAccountForUser($id);
            return $this->getDefaultAccount($id);
        }
    }

    function setDefaultAccountForUser($user_id, $account_id=null){
        $this->query("UPDATE users_accounts SET is_default=0 WHERE user_id=".$user_id);
        if (!empty($account_id)) {
            $this->query("UPDATE users_accounts SET is_default=1 WHERE user_id=".$user_id." AND account_id=".$account_id." LIMIT 1");
        } else {
            // Set first active found account as default for this user automatically.
            $this->query("UPDATE users_accounts SET is_default=1 WHERE user_id=".$user_id." AND is_active=1 AND status_type='Active' LIMIT 1");
        }
    }

    function getUsersByRole($account_id, $role_ids, $user_id = FALSE) {
        $account_id = (int) $account_id;
        //$role_id = (int) $role_id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'inner',
                'conditions' => 'User.id = users_accounts.user_id'
            ),
            array(
                'table' => 'accounts',
                'type' => 'inner',
                'conditions' => 'users_accounts.account_id = accounts.id'
            ),
            array(
                'table' => 'roles',
                'type' => 'inner',
                'conditions' => 'users_accounts.role_id = roles.role_id'
            )
        );
        $fields = array('User.*', 'accounts.company_name', 'users_accounts.user_id', 'users_accounts.role_id', 'roles.name');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('users_accounts.account_id' => $account_id, 'User.id !=' => $user_id, 'roles.role_id' => $role_ids, 'User.id NOT IN ("2423","2422","2427","4681")'),
                    'fields' => $fields,
                    'group' => 'User.id',
                    'order' => 'User.first_name ASC',
        ));
    }

    function getUsersByRole_ajax($account_id, $role_id, $keyword = '', $user_id = FALSE) {
        $account_id = (int) $account_id;
        $role_id = (int) $role_id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'inner',
                'conditions' => 'User.id = users_accounts.user_id'
            ),
            array(
                'table' => 'accounts',
                'type' => 'inner',
                'conditions' => 'users_accounts.account_id = accounts.id'
            ),
            array(
                'table' => 'roles',
                'type' => 'inner',
                'conditions' => 'users_accounts.role_id = roles.role_id'
            )
        );
        $where = '';
        if ($keyword != '') {
            $keyword = '%' . $keyword . '%';
            $where = array('(User.first_name LIKE "' . $keyword . '" OR User.last_name LIKE "' . $keyword . '")');
        }
        $fields = array('User.*', 'accounts.company_name', 'users_accounts.user_id', 'users_accounts.role_id', 'roles.name');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array(
                        'users_accounts.account_id' => $account_id,
                        'User.id !=' => $user_id,
                        'roles.role_id' => $role_id,
                        $where
                    ),
                    'fields' => $fields,
                    'group' => 'User.id',
                    'order' => 'User.first_name ASC',
        ));
    }

    function getUsersByRoleUserRange($account_id, $role_id, $user_ids) {
        $account_id = (int) $account_id;
        $role_id = (int) $role_id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'inner',
                'conditions' => 'User.id = users_accounts.user_id'
            ),
            array(
                'table' => 'accounts',
                'type' => 'inner',
                'conditions' => 'users_accounts.account_id = accounts.id'
            ),
            array(
                'table' => 'roles',
                'type' => 'inner',
                'conditions' => 'users_accounts.role_id = roles.role_id'
            )
        );
        $fields = array('User.*', 'accounts.company_name', 'users_accounts.user_id', 'roles.name');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array('users_accounts.account_id' => $account_id, 'roles.role_id' => $role_id, 'User.id' => $user_ids),
                    'fields' => $fields,
                    'group' => 'User.id',
                    'order' => 'User.first_name ASC',
        ));
    }

    function isUserExists($user_name, $user_id = 0) {
        $user_id = (int) $user_id;
        $fields = array('User.*');
        return $this->find('count', array(
                    'conditions' => array('User.username' => $user_name, 'User.id <>' => $user_id),
                    'fields' => $fields
        ));
    }

    function isUserEmailExists($email, $user_id = 0) {
        $user_id = (int) $user_id;

        $fields = array('User.*');
        return $this->find('count', array(
                    'conditions' => array('User.email' => $email, 'User.id <>' => $user_id),
                    'fields' => $fields
        ));
    }

    function isUserEmailExistsInAccount($email, $account_id) {
        $account_id = (int) $account_id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'left',
                'conditions' => array('User.id = users_accounts.user_id')
            )
        );
        $fields = array('User.*');
        return $this->find('count', array(
                    'joins' => $joins,
                    'conditions' => array('User.email' => $email, 'users_accounts.account_id' => $account_id),
                    'fields' => $fields
        ));
    }

    function getUsersByAccount($account_id, $role, $limit = '', $start = 0, $keywords = '') {
        $account_id = (int) $account_id;
        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'left',
                'conditions' => array('User.id = users_accounts.user_id')
            ),
            array(
                'table' => 'accounts',
                'type' => 'left',
                'conditions' => array('users_accounts.account_id = accounts.id')
            )
        );
        $fields = array('User.*', 'accounts.company_name', 'users_accounts.account_id', 'users_accounts.user_id', 'users_accounts.role_id as role');
        $conditions = array('users_accounts.account_id' => $account_id, 'users_accounts.role_id' => $role, 'User.is_deleted' => '0');
        if ($keywords != '') {
            $keywords = '%' . $keywords . '%';
            $conditions[] = '(User.first_name LIKE "' . $keywords . '" OR User.last_name LIKE "' . $keywords .
                    '" OR  CONCAT(User.first_name, \' \',User.last_name)  LIKE "' . $keywords . '")';
        }
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => 'User.id',
                    'order' => array('User.last_name ASC', 'User.first_name ASC'),
                    'limit' => $limit ? $limit : '0',
                    'offset' => $start
        ));
    }

    function countUsersByAccount($account_id, $role, $keywords = '') {
        $account_id = (int) $account_id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'left',
                'conditions' => array('User.id = users_accounts.user_id')
            ),
            array(
                'table' => 'accounts',
                'type' => 'left',
                'conditions' => array('users_accounts.account_id = accounts.id')
            )
        );
        $fields = array('User.*', 'accounts.company_name', 'users_accounts.account_id', 'users_accounts.user_id', 'users_accounts.role_id as role');
        $conditions = array('users_accounts.account_id' => $account_id, 'users_accounts.role_id' => $role, 'User.is_deleted' => '0');
        if (!empty($keywords)) {
            $keywords = '%' . $keywords . '%';
            $conditions[] = '(User.first_name LIKE "' . $keywords . '" OR User.last_name LIKE "' . $keywords .
                    '" OR  CONCAT(User.first_name, \' \',User.last_name)  LIKE "' . $keywords . '")';
        }
        return $this->find('count', array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => 'User.id'
        ));
    }

    function getUsersByAccountAjax($account_id, $role, $keywords = '') {
        $account_id = (int) $account_id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'left',
                'conditions' => array('User.id = users_accounts.user_id')
            ),
            array(
                'table' => 'accounts',
                'type' => 'left',
                'conditions' => array('users_accounts.account_id = accounts.id')
            )
        );
        $where = '';
        if ($keywords != '') {
            $keywords = '%' . $keywords . '%';
            $where = array('(User.first_name LIKE "' . $keywords . '" OR User.last_name LIKE "' . $keywords . '" OR  CONCAT(User.first_name, \' \',User.last_name)  LIKE "' . $keywords . '")');
        }
        $fields = array('User.*', 'accounts.company_name', 'users_accounts.account_id', 'users_accounts.user_id', 'users_accounts.role_id as role');
        return $this->find('all', array(
                    'joins' => $joins,
                    'conditions' => array(
                        'users_accounts.account_id' => $account_id,
                        'users_accounts.role_id' => $role,
                        'User.is_deleted' => '0',
                        $where
                    ),
                    'fields' => $fields,
                    'group' => 'User.id',
                    'order' => array('User.last_name ASC', 'User.first_name ASC'),
        ));
    }

    public function beforeSave($options = array()) {
        $this->data[$this->alias]['site_id'] = $this->site_id;
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }

    //Modified by  Hamid
    //Date: 2017-01-05
    // Remove 'User.is_active' => 1 check;
    function getTotalUsers($account_id) {
        $account_id = (int) $account_id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'inner',
                'conditions' => 'User.id = users_accounts.user_id'
            )
        );
        $fields = array('User.*');
        $result = $this->find('count', array(
            'joins' => $joins,
            'conditions' => array(
                'users_accounts.account_id' => $account_id,
                'users_accounts.is_active <>' =>0,
                'users_accounts.role_id NOT IN ("125") AND User.id NOT IN ("2423","2422","2427","4681")
                '),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getUserWAccounts($username, $password) {

        $this->virtualFields['auth_token'] = 0;
        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'left',
                'conditions' => 'User.id = users_accounts.user_id'
            ),
            array(
                'table' => 'accounts',
                'type' => 'left',
                'conditions' => 'accounts.id = users_accounts.account_id'
            )
        );
        $fields = array('User.*', 'users_accounts.role_id as role', 'users_accounts.is_active' ,'users_accounts.is_default', 'users_accounts.permission_maintain_folders', 'users_accounts.permission_access_video_library', 'users_accounts.permission_video_library_upload', 'users_accounts.permission_administrator_user_new_role', 'users_accounts.parmission_access_my_workspace', 'users_accounts.manage_collab_huddles', 'users_accounts.manage_evaluation_huddles', 'users_accounts.manage_coach_huddles', 'users_accounts.folders_check', 'users_accounts.permission_start_synced_scripted_notes', 'accounts.company_name', 'accounts.id as account_id', 'accounts.image_logo,accounts.in_trial,accounts.created_at');
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'OR' => array(
                    array('username' => $username),
                    array('email' => $username)
                ),
                'password' => $password,
                'User.is_active' => '1',
                'User.type' => 'Active',
                'users_accounts.is_active' => '1'
            ),
            'fields' => $fields
        ));

        if ($result) {

            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $item['User']['auth_token'] = md5($item['User']['id']);

                $result[$i] = $item;
            }
            //$result['Users']['auth_token'] = md5($result['Users']['id']);
            return $result;
        } else {
            return FALSE;
        }
    }

    function getUserWAccountsByEmail($email) {

        $this->virtualFields['auth_token'] = 0;
        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'left',
                'conditions' => 'User.id = users_accounts.user_id'
            ),
            array(
                'table' => 'accounts',
                'type' => 'left',
                'conditions' => 'accounts.id = users_accounts.account_id'
            )
        );
        $fields = array('User.*', 'users_accounts.role_id as role', 'users_accounts.is_default', 'users_accounts.permission_maintain_folders', 'users_accounts.permission_access_video_library', 'users_accounts.permission_video_library_upload', 'users_accounts.permission_administrator_user_new_role', 'users_accounts.parmission_access_my_workspace', 'users_accounts.manage_collab_huddles', 'users_accounts.manage_evaluation_huddles', 'users_accounts.manage_coach_huddles', 'users_accounts.folders_check', 'accounts.company_name', 'accounts.id as account_id', 'accounts.image_logo,accounts.in_trial,accounts.created_at');
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'email' => $email
            ),
            'fields' => $fields
        ));

        if ($result) {

            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $item['User']['auth_token'] = md5($item['User']['id']);

                $result[$i] = $item;
            }
            //$result['Users']['auth_token'] = md5($result['Users']['id']);
            return $result;
        } else {
            return FALSE;
        }
    }

    function getUsers($account_id, $role, $array_format = false, $huddle_id = false) {
        $account_id = (int) $account_id;

        $joins = array(
            array(
                'table' => 'users_accounts',
                'type' => 'inner',
                'conditions' => 'User.id = users_accounts.user_id'
            )
        );

        $fields = array('User.*', 'users_accounts.role_id');
        if($huddle_id)
        {
            $joins[] = array(
                'table' => 'account_folder_users',
                'type' => 'left',
                'conditions' => 'User.id = account_folder_users.user_id AND account_folder_users.account_folder_id = '.$huddle_id
            );
            $fields[] = 'account_folder_users.role_id as huddle_role_id';
        }


        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array('users_accounts.account_id' => $account_id, 'users_accounts.role_id' => $role),
            'order' => array('first_name ASC'),
            'fields' => $fields
        ));

        if (is_array($result) && count($result) > 0) {

            if ($array_format) {

                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $temp = $item['User'];
                    if($huddle_id)
                    {
                        $temp['huddle_role_id'] = $item['account_folder_users']['huddle_role_id'];
                    }
                    $temp['role_id'] = $role;
                    $temp['role_name'] = $role == 120 ? 'User' : ($role == 115 ? 'Admin' : ($role == 110 ? 'Super Admin' : ($role == 125 ? 'Viewer' : ($role == 100 ? 'Account Owner' : ''))));
                    $output[] = $temp;
                }

                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    function findByEmail($email, $password) {
        $result = $this->find('first', array('conditions' => array('TRIM(CHAR(9) FROM TRIM(email))' => $email, 'password' => $password)));
        if ($result && count($result) > 0) {
            return $result;
        } else {
            return TRUE;
        }
    }

    function findByUsername($username, $password) {
        $result = $this->find('first', array('conditions' => array('TRIM(CHAR(9) FROM TRIM(username))' => $username, 'password' => $password)));
        if ($result && count($result) > 0) {
            return $result;
        } else {
            return TRUE;
        }
    }

    function findById($ref_id) {
        $ref_id = (int) $ref_id;
        $result = $this->find('first', array('conditions' => array('id' => $ref_id)));
        if ($result && count($result) > 0) {
            return $result;
        } else {
            return TRUE;
        }
    }

    function findById_url($ref_id) {
        $result = $this->find('first', array('conditions' => array('MD5(id)' => $ref_id)));
        if ($result && count($result) > 0) {
            return $result;
        } else {
            return TRUE;
        }
    }

    function findById_url_hmh($ref_id) {
        $result = $this->query("SELECT * FROM users WHERE MD5(id) = '$ref_id' AND site_id = 2");
        if ($result && count($result) > 0) {
            return $result;
        } else {
            return TRUE;
        }
    }

    function getInvitedUsers($account_id, $huddle_id) {
        $joins = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'afu',
                'type' => 'inner',
                'conditions' => 'User.id = afu.user_id'
            ),
            array(
                'table' => 'account_folders',
                'alias' => 'af',
                'type' => 'inner',
                'conditions' => 'afu.account_folder_id = af.account_folder_id'
            )
        );
        $fields = array('User.first_name', 'User.last_name', 'User.id', 'afu.role_id');
        $conditions = array(
            'af.account_id' => $account_id,
            'afu.account_folder_id' => $huddle_id
        );
        $result = $this->find('all', array(
            'joins' => $joins,
            'fields' => $fields,
            'conditions' => $conditions
        ));
        return $result;
    }

    function getUninvitedUsers($keyword, $account_id, $huddle_id, $limit, $offset, $ignoreList) {
        $ignoreList = array_map(function($k) {
            return (int) $k;
        }, $ignoreList);
        $db = $this->getDataSource();
        $query = "
            from users_accounts ufa
                inner join users User on ufa.user_id = User.id
            where account_id = :account_id
                and user_id not in (
                  select afu.user_id
                  from account_folder_users afu
                  inner join account_folders af on afu.account_folder_id = af.account_folder_id
                  where af.account_id = :account_id
                  and afu.account_folder_id = :huddle_id
                )
        ";
        if (!empty($ignoreList)) {
            $query .= ' and user_id not in (' . implode(',', $ignoreList) . ') ';
        }
        $count = 'select count(distinct User.id) as total';
        $select = 'select distinct User.id, ufa.role_id, User.first_name, User.last_name ';
        $limit = ' limit ' . (int) $offset . ', ' . (int) $limit;

        $params = array('account_id' => $account_id, 'huddle_id' => $huddle_id);

        if (!empty($keyword)) {
            $kw = '%' . $keyword . '%';
            $query .= ' and (User.first_name like :keyword OR User.last_name like :keyword)';
            $params['keyword'] = $kw;
        }

        $count = $db->fetchAll($count . $query, $params);
        if (count($count) > 0) {
            $count = $count[0][0]['total'];
        } else {
            $count = 0;
        }

        $data = $db->fetchAll($select . $query . $limit, $params);

        return array('count' => $count, 'data' => $data);
    }

    function getUser($email) {
        $where = "(email = '$email' OR username =  '$email')";
        $result = $this->find('first', array(
            'conditions' => array($where)));
        return $result;
    }

}
