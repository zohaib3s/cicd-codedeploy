<?php
App::uses('AppModel', 'Model');
class AuditErrors extends AppModel {

    var $useTable = 'audit_errors';

    function set_error_data($data) {
        if (isset($_SESSION['user_permissions']['users']['id']) && $_SESSION['user_permissions']['users']['id'] != '') {
            $data['created_by'] = $_SESSION['user_permissions']['users']['id'];
        } else {
            $data['created_by'] = -1;
        }
        $this->save($data);
        if ($this->id != '') {
            return $this->id;
        } else {
            return FALSE;
        }
    }

    function get($error_id) {
        $error_id = (int) $error_id;
        $joins = array(
            'table' => 'users as User',
            'type' => 'left',
            'conditions' => array('User.id=AuditErrors.created_by')
        );
        $fields = array('AuditErrors.*', 'User.first_name', 'User.last_name', 'User.email');
        $result = $this->find('first', array(
            'joins' => array($joins),
            'fields' => $fields,
            'conditions' => array('AuditErrors.error_id' => $error_id)
        ));

        if (isset($result) && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

}
