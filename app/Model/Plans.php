<?php

class Plans extends AppModel {

    var $useTable = 'plans';

    function getSelectedPlan($plan_id) {
        $plan_id = (int) $plan_id;
        $result = $this->find('first', array('conditions' => array('id' => $plan_id)));
        if ($result && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getAll() {
        return $this->find('all');
    }
}
