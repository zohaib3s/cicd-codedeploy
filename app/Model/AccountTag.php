<?php

// app/Model/User.php
class AccountTag extends AppModel {

    var $useTable = 'account_tags';
    var $primaryKey = 'account_tag_id';

    
    public function getstandardbycode($standard_code, $account_id,$framework_id = 0 ){
        if($framework_id == 0 )
        {
        $tag_code=$this->find('first',array('conditions'=>array("account_id" => $account_id,"tag_code"=>$standard_code , "framework_id IS NULL" )));
        }
        else{
         $tag_code=$this->find('first',array('conditions'=>array("account_id" => $account_id,"tag_code"=>$standard_code,"framework_id" => $framework_id )));
        }
        return $tag_code;
    }
    
     public function getstandardbycodeweb($standard_code, $account_id,$framework_id = 0 ){
         
        if(!empty($framework_id))
        {
            $account_tag_info = $this->find('first', array('conditions' => array('account_tag_id' => $standard_code)));
            $account_id = $account_tag_info['AccountTag']['account_id'];
        }
 
         
        if($framework_id == 0 )
        {
//        $tag_code=$this->find('first',array('conditions'=>array("account_id" => $account_id,"tag_code"=>$standard_code , "framework_id IS NULL" )));
        $tag_code=$this->find('first',array('conditions'=>array("account_id" => $account_id,"account_tag_id"=>$standard_code , "framework_id IS NULL" )));
        }
        else{
//         $tag_code=$this->find('first',array('conditions'=>array("account_id" => $account_id,"tag_code"=>$standard_code,"framework_id" => $framework_id )));
         $tag_code=$this->find('first',array('conditions'=>array("account_id" => $account_id,"account_tag_id"=>$standard_code,"framework_id" => $framework_id )));
        }
        return $tag_code;
    }
    
    
    public function gettagbytitle($info, $account_id){
        $tag_code=$this->find('first',array('conditions'=>array("account_id" => $account_id,"tag_title"=>$info)));
        return $tag_code;
    }
    
    public function gettagswithcount($account_id,$ref_type,$document_id){       
       $tag_code=$this->find('all',array('conditions'=>array("account_id" => $account_id,"tag_type" => $ref_type )));
       $bnk = ClassRegistry::init('AccountCommentTag');             
       $tags_with_count=array();
       foreach($tag_code as $tag){
            $count=$bnk->find('count',
                array(
                  'conditions'=> array(
                      "ref_id" => $document_id,
                      "account_tag_id" => $tag['AccountTag']['account_tag_id'],
                      "comment_id in (select id from comments)"
                  )
                )
            );            
            $tags_with_count[]=array_merge($tag,array("total"=>$count))            ;
       }
       return $tags_with_count;       
    }
    public function gettagscountonly($account_id,$ref_type,$document_id){
        $tag_code=$this->find('all',array('conditions'=>array("account_id" => $account_id,"tag_type" => $ref_type )));
       $bnk = ClassRegistry::init('AccountCommentTag');             
       $tags_with_count=array();
       foreach($tag_code as $tag){
            $count=$bnk->find('count',
                array(
                  'conditions'=> array(
                      "ref_id" => $document_id,
                      "account_tag_id" => $tag['AccountTag']['account_tag_id'],
                      "comment_id in (select id from comments)"
                  )
                )
            );             
            $tag_with_count[]=array("account_tag_id"=>$tag['AccountTag']['account_tag_id'],"total"=>$count);
       }
       return $tag_with_count; 
    }
}

