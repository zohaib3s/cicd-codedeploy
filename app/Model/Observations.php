<?php

class Observations extends AppModel {

    var $useTable = 'account_folder_observations';

    public function getAllOservations($user_id, $account_id, $sort, $pageNumber = 1) {

        $user_id = (int) $user_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_observation_users as obs_folder_usrs',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_observation_id=obs_folder_usrs.account_folder_observation_id')
            ),
            array(
                'table' => 'account_folders as account_folders',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_id=account_folders.account_folder_id')
            )
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'account_folders.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'Observations.observation_date_time DESC';
        } else {
            $order_by = 'Observations.observation_date_time DESC';
        }
        $fields = array(
            'Observations.*',
            'account_folders.*',
            'obs_folder_usrs.*'
        );
        $groups = array('Observations.account_folder_observation_id');
        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'obs_folder_usrs.user_id' => $user_id
            ),
            'fields' => $fields,
            'group' => $groups,
            'order' => array($order_by)
        ));

        if (is_array($result) && count($result) > 0) {
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $osevationArray = array();
                $osevationArray['name'] = $item['account_folders']['name'];
                $osevationArray['location_name'] = $item['Observations']['location_name'];

                $observationDate = new DateTime($item['Observations']['observation_date_time']);
                $osevationArray['observation_on'] = $observationDate->format('M d, Y');
                $osevationArray['observation_at'] = $observationDate->format('h:m A');

                $osevationArray['is_private'] = $item['Observations']['is_private'];
                $osevationArray['account_folder_observation_id'] = $item['Observations']['account_folder_observation_id'];
//GET OBSERVEE
                $observee = $this->getInvolvedUseres($item['Observations']['account_folder_observation_id'], true);
                if (is_array($observee) && count($observee) > 0) {
                    $osevationArray['observee'] = $observee['0']['User'];
                }
//GET OBSERVERS
                $observer = $this->getInvolvedUseres($item['Observations']['account_folder_observation_id'], false);
                if (is_array($observer) && count($observer) > 0) {
                    $observerArr = array();
                    for ($loop = 0; $loop < count($observer); $loop++) {
                        $observerArr[] = $observer[$loop]['User'];
                    }
                    $osevationArray['observer'] = $observerArr;
                }
                $output[] = $osevationArray;
            }
            return $output;
        } else {
            return FALSE;
        }
    }

    public function getInvolvedUseres($observationID, $getObserver = false) {
        $observationID = (int) $observationID;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_observation_users as obs_folder_usrs',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_observation_id=obs_folder_usrs.account_folder_observation_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=obs_folder_usrs.user_id')
            )
        );
        $fields = array(
            'User.*'
        );

        $whereArray = array('obs_folder_usrs.account_folder_observation_id' => $observationID);
        if ($getObserver) {
            $whereArray['role_id'] = '300';
        } else {
            $whereArray['role_id'] = '310';
        }
        $obs_result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => $whereArray,
            'fields' => $fields
        ));
        if (is_array($obs_result) && count($obs_result) > 0) {
            return $obs_result;
        } else {
            return FALSE;
        }
    }

    public function searchObservation($user_id, $account_id, $sort, $title, $limit = '', $start = 0, $filter = '', $huddle_id = '') {
        $user_id = (int) $user_id;
        $huddle_id = (int) $huddle_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_observation_users as obs_folder_usrs',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_observation_id=obs_folder_usrs.account_folder_observation_id')
            ),
            array(
                'table' => 'account_folders as account_folders',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_id=account_folders.account_folder_id')
            )
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'account_folders.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'Observations.observation_date_time DESC';
        } else {
            $order_by = 'Observations.observation_date_time DESC';
        }
        if ($huddle_id != '') {
            $conditionsArray = array(
                'Observations.huddle_account_folder_id' => $huddle_id);
        } else {
            $conditionsArray = array(
                'obs_folder_usrs.user_id' => $user_id
            );
        }
        if ($title != '') {
            $conditionsArray['OR'] = array(
                array('account_folders.name LIKE' => '%' . $title . '%'),
                array('Observations.location_name LIKE' => '%' . $title . '%')
            );
        }
        $notInArray = $this->getMyPrivateObservations($user_id);
        $where_not_in = '';
        if ($notInArray != FALSE) {
            $conditionsArray[] = " Observations.account_folder_observation_id NOT IN($notInArray) ";
        }
        if ($filter != 'all') {
            if ($filter == 'past') {
                $conditionsArray[] = array("Observations.observation_date_time < NOW()");
            }
            if ($filter == 'future') {
                $conditionsArray[] = array("Observations.observation_date_time > NOW()");
            }
        }
        $conditionsArray[] = array("account_folders.account_id = " . $account_id . "");
        $fields = array(
            'Observations.*',
            'account_folders.*',
            'obs_folder_usrs.*'
        );
        $groups = array('Observations.account_folder_observation_id');
        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => $conditionsArray,
            'fields' => $fields,
            'group' => $groups,
            'order' => array($order_by),
            'limit' => $limit ? $limit : '0',
            'offset' => $start
        ));
//        $dbo = $this->getDatasource();
//        $logs = $dbo->getLog();
//        $lastLog = end($logs['log']);
//        echo  $lastLog['query'];
        if (is_array($result) && count($result) > 0) {
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $osevationArray = array();
                $osevationArray['name'] = $item['account_folders']['name'];
                $osevationArray['location_name'] = $item['Observations']['location_name'];
                $osevationArray['creator_id'] = $item['Observations']['created_by'];

                $observationDate = new DateTime($item['Observations']['observation_date_time']);
                $osevationArray['observation_on'] = $observationDate->format('M d, Y');
                $osevationArray['observation_at'] = $observationDate->format('h:m A');

                $osevationArray['is_private'] = $item['Observations']['is_private'];
                $osevationArray['account_folder_observation_id'] = $item['Observations']['account_folder_observation_id'];
                $osevationArray['account_folder_id'] = $item['Observations']['account_folder_id'];
                //GET OBSERVEE
                $observee = $this->getInvolvedUseres($item['Observations']['account_folder_observation_id'], true);
                if (is_array($observee) && count($observee) > 0) {
                    $osevationArray['observee'] = $observee['0']['User'];
                }
                //GET OBSERVERS
                $observer = $this->getInvolvedUseres($item['Observations']['account_folder_observation_id'], false);
                if (is_array($observer) && count($observer) > 0) {
                    $observerArr = array();
                    for ($loop = 0; $loop < count($observer); $loop++) {
                        $observerArr[] = $observer[$loop]['User'];
                    }
                    $osevationArray['observer'] = $observerArr;
                }
                $output[] = $osevationArray;
            }
            return $output;
        } else {
            return FALSE;
        }
    }

    public function getObservation($account_folder_observation_id) {
        $observation = array();

        $account_folder_users = array(
            array(
                'table' => 'account_folders as acc_folder',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_id=acc_folder.account_folder_id')
            ),
        );
        $fields = array(
            'Observations.*',
            'acc_folder.*',
        );
        $observation_info = $this->find('all', array(
            'joins' => $account_folder_users,
            "conditions" => array("Observations.account_folder_observation_id" => $account_folder_observation_id),
            'fields' => $fields
        ));
        $observation['Observations'] = $observation_info[0]['Observations'];
        $observation['acc_folder'] = $observation_info[0]['acc_folder'];
        $observer = $this->getInvolvedUseres($account_folder_observation_id);
        $observation['Observer'] = $observer;
        $observee = $this->getInvolvedUseres($account_folder_observation_id, true);
        $observation['Observee'] = $observee;
        return $observation;
    }

    public function getObservationCount($user_id, $account_id, $sort, $title, $limit = '', $start = 0, $filter = '', $huddle_id = '') {

        $user_id = (int) $user_id;
        $huddle_id = (int) $huddle_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_observation_users as obs_folder_usrs',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_observation_id=obs_folder_usrs.account_folder_observation_id')
            ),
            array(
                'table' => 'account_folders as account_folders',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_id=account_folders.account_folder_id')
            )
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'account_folders.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'Observations.observation_date_time DESC';
        } else {
            $order_by = 'Observations.observation_date_time DESC';
        }
        if ($huddle_id != '') {
            $conditionsArray = array(
                'Observations.huddle_account_folder_id' => $huddle_id);
        } else {
            $conditionsArray = array(
                'obs_folder_usrs.user_id' => $user_id
            );
        }
        if ($title != '') {
            $conditionsArray['OR'] = array(
                array('account_folders.name LIKE' => '%' . $title . '%'),
                array('Observations.location_name LIKE' => '%' . $title . '%')
            );
        }
        $notInArray = $this->getMyPrivateObservations($user_id);
        $where_not_in = '';
        if ($notInArray != FALSE) {
            $conditionsArray[] = " Observations.account_folder_observation_id NOT IN($notInArray) ";
        }
        if ($filter != 'all') {
            if ($filter == 'past') {
                $conditionsArray[] = array("Observations.observation_date_time < NOW()");
            }
            if ($filter == 'future') {
                $conditionsArray[] = array("Observations.observation_date_time > NOW()");
            }
        }
        $conditionsArray[] = array("account_folders.account_id = " . $account_id . "");
        $fields = array(
            'Observations.*',
            'account_folders.*',
            'obs_folder_usrs.*'
        );
        $groups = array('Observations.account_folder_observation_id');
        $result = $this->find('count', array(
            'joins' => $account_folder_users,
            'conditions' => $conditionsArray,
            'fields' => $fields,
            'group' => $groups,
                //'order' => array($order_by)
                //'limit' => $limit ? $limit : '0',
                //'offset' => $start
        ));
//        $dbo = $this->getDatasource();
//        $logs = $dbo->getLog();
//        $lastLog = end($logs['log']);
//        echo  $lastLog['query'];
        if ($result == '') {
            return '0';
        } else {
            return $result;
        }
    }

    public function get_account_folders_id($observationID) {
        $observationID = (int) $observationID;
        $account_folder = array(
            array(
                'table' => 'account_folders as account_folders',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_id=account_folders.account_folder_id')
            )
        );
        $fields = array(
            'account_folders.account_folder_id'
        );
        $result = $this->find('first', array(
            'joins' => $account_folder,
            'conditions' => array('Observations.account_folder_observation_id' => $observationID),
            'fields' => $fields
        ));
        if (count($result) > 0) {
            return $result['account_folders']['account_folder_id'];
        } else {
            return 'NULL';
        }
    }

    private function getMyPrivateObservations($user_id) {
        $user_id = (int) $user_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_observation_users as obs_folder_usrs',
                'type' => 'left',
                'conditions' => array('Observations.account_folder_observation_id=obs_folder_usrs.account_folder_observation_id')
            )
        );
        $conditionsArray = array(
            'obs_folder_usrs.user_id' => $user_id,
            'obs_folder_usrs.role_id' => '300',
            'Observations.is_private' => '1',
            'Observations.created_by <> ' . $user_id . ''
        );
        $groups = array('Observations.account_folder_observation_id');
        $result = $this->find('all', array('fields' => 'Observations.account_folder_observation_id',
            'joins' => $account_folder_users,
            'conditions' => $conditionsArray,
            'group' => $groups
        ));
        $resultArray = array();
        if (is_array($result) && count($result) > 0) {
            foreach ($result as $obsRow) {
                $resultArray[] = $obsRow['Observations']['account_folder_observation_id'];
            }
            return implode(',', $resultArray);
        } else {
            return false;
        }
    }

}
