<?php

class UserActivityLog extends AppModel {

    var $useTable = 'user_activity_logs';

    function save($data = null, $validate = false, $fieldList = array()) {
        parent::save($data, $validate, $fieldList);
        return $this->id;
    }

    function get($account_id, $user_id) {
        $account_id = (int) $account_id;

        $nowDate = date("Y-m-d", time());
        $nowDate = strtotime($nowDate);
        $nowDate = strtotime("-7 day", $nowDate);

        $nextDate = date('Y-m-d', $nowDate);

        $joins = array(
            'table' => 'users as User',
            'type' => 'inner',
            'conditions' => array('User.id=UserActivityLog.user_id')
        );
        $fields = array('UserActivityLog.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
        $array = [1, 2, 3, 5, 6, 4, 8, 7, 17, 18, 19, 20, 21, 22,24,25];

        $result = $this->find('all', array(
            'joins' => array($joins),
            'conditions' => array(
                'DATE(UserActivityLog.date_added) >= DATE("' . $nextDate . '")',
                'UserActivityLog.account_id' => $account_id,
                'UserActivityLog.type' => $array,
            ),
            'fields' => $fields,
            'order' => 'UserActivityLog.date_added DESC',
            'limit' => 100
        ));
        // We changed the limit to 100 on Khurram's suggestion. Because there was a specific issue. i.e, If in last 7 days other users of account has done some activity (that shouldn't be displayed to this user) then the activities of current user will be pushed down and will not be displayed on Dashboard. So, increasing limit to 100 will increase the chances but still it's not 100% accurate fix.
        return $result;
    }

    function get_mobile($account_id, $user_id) {
        $account_id = (int) $account_id;

        $nowDate = date("Y-m-d", time());
        $nowDate = strtotime($nowDate);
        $nowDate = strtotime("-7 day", $nowDate);

        $nextDate = date('Y-m-d', $nowDate);

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('User.id=UserActivityLog.user_id')
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => array('af.account_folder_id = UserActivityLog.account_folder_id')
            )
        );
        $fields = array('UserActivityLog.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
        $array = [1, 2, 3, 5 , 4, 7, 17,20,21, 22,24,25,28,29,30];
        //$array = [22,28];
        //$array = [1, 2, 3, 5, 6, 4, 8, 7, 17, 22, 12, 18, 19];
        $folder_type_array = array(1, 3);

        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'UserActivityLog.date_added >=' . $nextDate,
                'UserActivityLog.account_id' => $account_id,
                'UserActivityLog.type' => $array,
                'af.folder_type' => $folder_type_array
            ),
            'fields' => $fields,
            'order' => 'UserActivityLog.date_added DESC',
            'limit' => 10
        ));
        return $result;
    }

    function get_huddle_activity($account_id, $huddle) {
        $account_id = (int) $account_id;

        $nowDate = date("Y-m-d", time());
        $nowDate = strtotime($nowDate);
        $nowDate = strtotime("-7 day", $nowDate);

        $nextDate = date('Y-m-d', $nowDate);

        $joins = array(
            'table' => 'users as User',
            'type' => 'inner',
            'conditions' => array('User.id=UserActivityLog.user_id')
        );
        $fields = array('UserActivityLog.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image');
        $array = [1, 2, 3, 5, 6, 4, 8, 7, 17, 18, 19, 20, 21, 22];

        $result = $this->find('all', array(
            'joins' => array($joins),
            'conditions' => array(
                'UserActivityLog.date_added >=' . $nextDate,
                'UserActivityLog.account_id' => $account_id,
                'UserActivityLog.account_folder_id' => $huddle,
                'UserActivityLog.type' => $array,
            ),
            'fields' => $fields,
            'order' => 'UserActivityLog.date_added DESC',
            'limit' => 15
        ));
        return $result;
    }

}
