<?php

App::uses('AppModel', 'Model');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EdtpaAssessmentPortfolios extends AppModel {

    var $useTable = 'edtpa_assessment_portfolios';

    function getAssessmentPortfolioTasks($edtpa_assessment_id, $account_id, $user_id) {

        $edtpa_assessment_id = (int) $edtpa_assessment_id;
        $joins = array(
            array(
                'table' => 'edtpa_assessment_portfolio_nodes',
                'alias' => 'PortfolioSubNodes',
                'type' => 'left',
                'conditions' => array('EdtpaAssessmentPortfolios.id = PortfolioSubNodes.edtpa_assessment_portfolio_id')
            ),
            array(
                'table' => 'edtpa_assessments',
                'alias' => 'EdtpaAssessments',
                'type' => 'left',
                'conditions' => array('EdtpaAssessmentPortfolios.edtpa_assessment_id = EdtpaAssessments.id')
            ),
            array(
                'table' => 'edtpa_assessment_accounts',
                'alias' => 'EdtpaAssessmentAccounts',
                'type' => 'left',
                'conditions' => array('EdtpaAssessments.id = EdtpaAssessmentAccounts.edtpa_assessment_id')
            ),
            array(
                'table' => 'edtpa_assessment_account_candidates',
                'alias' => 'AssessmentAccountCandidates',
                'type' => 'left',
                'conditions' => array('EdtpaAssessmentAccounts.id = AssessmentAccountCandidates.edtpa_assessment_account_id AND AssessmentAccountCandidates.account_id = ' . $account_id.' and AssessmentAccountCandidates.user_id = '.$user_id)
            ),
            array(
                'table' => 'edtpa_assessment_portfolio_candidate_submissions',
                'alias' => 'AssessmentAccountCandidateSubmissions',
                'type' => 'left',
                'conditions' => array('AssessmentAccountCandidates.id = AssessmentAccountCandidateSubmissions.edtpa_assessment_account_candidate_id AND AssessmentAccountCandidateSubmissions.edtpa_assessment_portfolio_node_id = PortfolioSubNodes.id')
            ),
            array(
                'table' => 'users',
                'alias' => 'usr',
                'type' => 'left',
                'conditions' => array('usr.id=AssessmentAccountCandidates.user_id')

            )
        );
        $fields = array('EdtpaAssessmentPortfolios.*, PortfolioSubNodes.*, GROUP_CONCAT(AssessmentAccountCandidateSubmissions.document_id) AS documents_ids,usr.*');
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'EdtpaAssessmentPortfolios.edtpa_assessment_id' => $edtpa_assessment_id,
                //'AssessmentAccountCandidates.account_id'=>$account_id,
                //'AssessmentAccountCandidates.user_id'=>$user_id

            ),
            'fields' => $fields,
                'group' => array('PortfolioSubNodes.id')
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

}