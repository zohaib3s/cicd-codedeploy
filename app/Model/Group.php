<?php


// app/Model/User.php
class Group extends AppModel {

    var $useTable = 'groups';

    function getGroups($account_id, $array_format = false, $load_users = false, $huddle_id = false) {
        $account_id = (int) $account_id;

        $fields = array('Group.*');
        $joins = [];
        if($huddle_id)
        {
            $joins[] = array(
                'table' => 'account_folder_groups',
                'type' => 'left',
                'conditions' => 'Group.id = account_folder_groups.group_id AND account_folder_groups.account_folder_id = '.$huddle_id
            );
            $fields[] = 'account_folder_groups.role_id as huddle_role_id';
        }
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array('account_id' => $account_id),
            'fields' => $fields,
            'order' => 'Group.name ASC'
        ));

        if ($result) {

            if ($array_format) {

                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $temp = $item['Group'];
                    if($huddle_id)
                    {
                        $temp["huddle_role_id"] = $item['account_folder_groups']['huddle_role_id'];
                    }
                    $temp['first_name'] = $temp['name'];
                    $output[] = $temp;
                }

                $result = $output;
            }


            return $result;
        } else {
            return FALSE;
        }
    }

    function ajax_getGroups($account_id, $keywords = '', $array_format = false, $load_users = false) {
        $account_id = (int) $account_id;

        $fields = array('Group.*');
        $where = '';
        if ($keywords != '') {
            $where = array('Group.name LIKE' => '%' . $keywords . '%');
        }
        $result = $this->find('all', array(
            'conditions' => array('account_id' => $account_id, $where),
            'fields' => $fields,
            'order' => 'Group.name ASC'
        ));

        if ($result) {

            if ($array_format) {

                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $output[] = $item['Group'];
                }

                $result = $output;
            }


            return $result;
        } else {
            return FALSE;
        }
    }

    function saveGroups($data) {
        $this->save($data);
        return $this->id;
    }

    function update($id, $data) {
        $this->updateAll($data, array('id' => (int)$id));
        return $this->getAffectedRows();
    }

    function getInvitedGroups($account_id, $huddle_id) {
        $joins = array(
            array(
                'table' => 'account_folder_groups',
                'alias' => 'afg',
                'type' => 'inner',
                'conditions' => 'Group.id = afg.group_id'
            ),
            array(
                'table' => 'account_folders',
                'alias' => 'af',
                'type' => 'inner',
                'conditions' => 'afg.account_folder_id = af.account_folder_id'
            )
        );
        $fields = array('Group.name', 'Group.id', 'afg.role_id');
        $conditions = array(
            'af.account_id' => $account_id,
            'afg.account_folder_id' => $huddle_id
        );
        $result = $this->find('all', array(
            'joins' => $joins,
            'fields' => $fields,
            'conditions' => $conditions
        ));
        return $result;
    }

    function getUninvitedGroups($keyword, $account_id, $huddle_id, $limit, $offset, $ignoreList) {
        $ignoreList = array_map(function($k) { return (int) $k; }, $ignoreList);
        $db = $this->getDataSource();
        $query = "
        from groups `Group`
        where account_id = :account_id
            and id not in (
                select afg.group_id
                from account_folder_groups afg
                inner join groups `Group` on afg.group_id = `Group`.id
                where `Group`.account_id = :account_id
                and afg.account_folder_id = :huddle_id
            )
        ";
        if (!empty($ignoreList)) {
            $query .= ' and id not in (' . implode(',', $ignoreList) . ') ';
        }
        $count  = ' select count(distinct `Group`.id) as total ';
        $select = ' select distinct `Group`.id, `Group`.name ';
        $limit  = ' limit ' . (int)$offset . ', ' . (int)$limit;

        $params = array('account_id' => $account_id, 'huddle_id' => $huddle_id);

        if (!empty($keyword)) {
            $kw = '%' . $keyword . '%';
            $query .= ' and `Group`.name like :keyword';
            $params['keyword'] = $kw;
        }

        $count = $db->fetchAll( $count . $query, $params );
        if (count($count) > 0) {
            $count = $count[0][0]['total'];
        } else {
            $count = 0;
        }

        $data  = $db->fetchAll( $select . $query . $limit, $params );

        return array('count' => $count, 'data' => $data);
    }
}

