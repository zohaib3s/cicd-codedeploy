<?php

class DocumentFiles extends AppModel {

    var $useTable = 'document_files';

    
    
    function get_document_row($document_id) {
        $result = $this->find('first', array('conditions' => array('document_id' => (int)$document_id)));
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

}
