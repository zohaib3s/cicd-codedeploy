<?php

App::uses('AppModel', 'Model');

/**
 * MailchimpTemplates Model
 *
 * @property User $User
 * @property Plan $Plan
 * @property BraintreeCustomer $BraintreeCustomer
 * @property BraintreeSubscription $BraintreeSubscription
 * @property AccountMetaDatum $AccountMetaDatum
 * @property AuditEmail $AuditEmail
 * @property Document $Document
 * @property Group $Group
 * @property Subject $Subject
 * @property Topic $Topic
 * @property UserActivityLog $UserActivityLog

 */
class EdtpaAssessments extends AppModel {

    function getEtpaAssessment($account_id, $user_id, $limit = false, $offset = false) {

        $joins = array(
            array(
                'table' => 'edtpa_assessment_accounts',
                'alias' => 'AssessmentAccounts',
                'type' => 'inner',
                'conditions' => array('EdtpaAssessments.id = AssessmentAccounts.edtpa_assessment_id')
            ),
            array(
                'table' => 'edtpa_assessment_account_candidates',
                'alias' => 'EdtpaAssessmentAccountCandidates',
                'type' => 'left',
                'conditions' => array('AssessmentAccounts.id = EdtpaAssessmentAccountCandidates.edtpa_assessment_account_id AND EdtpaAssessmentAccountCandidates.account_id = ' . $account_id . ' AND EdtpaAssessmentAccountCandidates.user_id = ' . $user_id . '')
            ),
            array(
                'table' => 'users',
                'alias' => 'usr',
                'type' => 'left',
                'conditions' => array('usr.id=EdtpaAssessmentAccountCandidates.user_id')
            )
        );

        $fields = array('EdtpaAssessments.*, EdtpaAssessmentAccountCandidates.*,usr.*');
        if ($limit && $offset) {
            $result = $this->find('all', array(
                'joins' => $joins,
                'conditions' => array(
                    'AssessmentAccounts.account_id' => $account_id
                ),
                'fields' => $fields,
                'limit' => $limit,
                'offset' => $offset
            ));
        } else {
            $result = $this->find('all', array(
                'joins' => $joins,
                'conditions' => array(
                    'AssessmentAccounts.account_id' => $account_id
                ),
                'fields' => $fields
            ));
        }
        /* echo $this->getLastQuery();
          die; */

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getEtpaAssessment_for_teacher($account_id, $user_ids, $limit = false, $offset = false, $sidx, $sord, $student_name = '', $assessment_name = '') {

        $joins = array(
            array(
                'table' => 'edtpa_assessment_accounts',
                'alias' => 'AssessmentAccounts',
                'type' => 'inner',
                'conditions' => array('EdtpaAssessments.id = AssessmentAccounts.edtpa_assessment_id')
            ),
            array(
                'table' => 'edtpa_assessment_accounts',
                'alias' => 'EdtpaAssessmentAccounts',
                'type' => 'left',
                'conditions' => array('EdtpaAssessments.id = EdtpaAssessmentAccounts.edtpa_assessment_id AND EdtpaAssessmentAccounts.account_id = ' . $account_id)
            ),
            array(
                'table' => 'edtpa_assessment_account_candidates',
                'alias' => 'EdtpaAssessmentAccountCandidates',
                'type' => 'INNER',
                'conditions' => array('EdtpaAssessmentAccounts.id = EdtpaAssessmentAccountCandidates.edtpa_assessment_account_id AND EdtpaAssessmentAccountCandidates.account_id = ' . $account_id)
            ),
            array(
                'table' => 'users',
                'alias' => 'usr',
                'type' => 'INNER',
                'conditions' => array('usr.id=EdtpaAssessmentAccountCandidates.user_id')
            )
        );

        $fields = array('EdtpaAssessments.*, EdtpaAssessmentAccountCandidates.*,usr.*');
        $user_id_conditions = '';
        if ($user_ids) {
            $user_id_conditions = "EdtpaAssessmentAccountCandidates.user_id IN($user_ids)";
        }

        $search_conditions = '';
        if ($student_name != '' && $assessment_name != '') {
            $search_conditions = '(EdtpaAssessments.assessment_name like "%' . $assessment_name . '%" AND CONCAT(usr.first_name," ", usr.last_name) like  "%' . $student_name . '%" )';
        } elseif ($student_name != '' && $assessment_name == '') {
            $search_conditions = 'CONCAT(usr.first_name," ", usr.last_name) like  "%' . $student_name . '%" ';
        } elseif ($student_name == '' && $assessment_name != '') {
            $search_conditions = 'EdtpaAssessments.assessment_name like "%' . $assessment_name . '%"';
        }
        if ($limit && $offset) {
            $result = $this->find('all', array(
                'joins' => $joins,
                'conditions' => array(
                    'AssessmentAccounts.account_id' => $account_id,
                    $search_conditions,
                    $user_id_conditions,
                    'EdtpaAssessmentAccountCandidates.status' => array(0, 1, 2, 3, 4, 5, 8, 9)
                ),
                'fields' => $fields,
                'limit' => $limit,
                'offset' => $offset
            ));
        } else {
            $result = $this->find('all', array(
                'joins' => $joins,
                'conditions' => array(
                    'AssessmentAccounts.account_id' => $account_id,
                    $user_id_conditions,
                    'EdtpaAssessmentAccountCandidates.status' => array(0, 1, 2, 3, 4, 5, 8, 9),
                    $search_conditions
                ),
                'fields' => $fields
            ));
        }


        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getEtpaAssessment_for_summery($account_id, $user_id) {
        $joins = array(
            array(
                'table' => 'edtpa_assessment_accounts',
                'alias' => 'AssessmentAccounts',
                'type' => 'inner',
                'conditions' => array('EdtpaAssessments.id = AssessmentAccounts.edtpa_assessment_id')
            ),
            array(
                'table' => 'edtpa_assessment_accounts',
                'alias' => 'EdtpaAssessmentAccounts',
                'type' => 'left',
                'conditions' => array('EdtpaAssessments.id = EdtpaAssessmentAccounts.edtpa_assessment_id AND EdtpaAssessmentAccounts.account_id = ' . $account_id)
            ),
            array(
                'table' => 'edtpa_assessment_account_candidates',
                'alias' => 'EdtpaAssessmentAccountCandidates',
                'type' => 'INNER',
                'conditions' => array('EdtpaAssessmentAccounts.id = EdtpaAssessmentAccountCandidates.edtpa_assessment_account_id AND EdtpaAssessmentAccountCandidates.account_id = ' . $account_id . ' AND EdtpaAssessmentAccountCandidates.user_id=' . $user_id)
            ),
            array(
                'table' => 'users',
                'alias' => 'usr',
                'type' => 'INNER',
                'conditions' => array('usr.id=EdtpaAssessmentAccountCandidates.user_id')
            )
        );

        $fields = array('EdtpaAssessments.*, EdtpaAssessmentAccountCandidates.*,usr.*');
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'AssessmentAccounts.account_id' => $account_id,
                'EdtpaAssessmentAccountCandidates.user_id' => $user_id,
                'EdtpaAssessmentAccountCandidates.status' => array(0, 1, 2, 3, 4, 5, 6, 7, 8)
            ),
            'fields' => $fields,
        ));



        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

}
