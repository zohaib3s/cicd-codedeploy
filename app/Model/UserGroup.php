<?php

class UserGroup extends AppModel {

    var $useTable = 'user_groups';

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
    }

    function get_users($group_id) {
        $group_id = (int) $group_id;
        $joins = array(
            'table' => 'users',
            'type' => 'inner',
            'conditions' => 'UserGroup.user_id=users.id'
        );
        $fields = array('users.*');
        $result = $this->find('all', array(
            'joins' => array($joins),
            'fields' => $fields,
            'conditions' => array('UserGroup.group_id' => $group_id)
                )
        );
        return $result;
    }

    function get_user_group($user_id) {
        $user_id = (int) $user_id;
        $joins = array(
            'table' => 'account_folder_groups',
            'type' => 'inner',
            'conditions' => 'UserGroup.group_id=account_folder_groups.group_id'
        );
        $fields = array('account_folder_groups.*', 'UserGroup.*');
        $result = $this->find('all', array(
            'joins' => array($joins),
            'fields' => $fields,
            'conditions' => array('UserGroup.user_id' => $user_id)
        ));
        return $result;
    }

    function get_acount_folder_user_group($account_folder_id, $user_id) {
        $account_folder_id = (int) $account_folder_id;
        $user_id = (int) $user_id;
        $joins = array(
            'table' => 'account_folder_groups',
            'type' => 'inner',
            'conditions' => 'UserGroup.group_id=account_folder_groups.group_id'
        );
        $fields = array('account_folder_groups.*', 'UserGroup.*');
        $result = $this->find('first', array(
            'joins' => array($joins),
            'fields' => $fields,
            'conditions' => array('UserGroup.user_id' => $user_id, 'account_folder_groups.account_folder_id' => $account_folder_id)
        ));
        return $result;
    }

}
