<?php

App::uses('AppModel', 'Model');

/**
 * MailchimpTemplates Model
 *
 * @property User $User
 * @property Plan $Plan
 * @property BraintreeCustomer $BraintreeCustomer
 * @property BraintreeSubscription $BraintreeSubscription
 * @property AccountMetaDatum $AccountMetaDatum
 * @property AuditEmail $AuditEmail
 * @property Document $Document
 * @property Group $Group
 * @property Subject $Subject
 * @property Topic $Topic
 * @property UserActivityLog $UserActivityLog
 * @property User $User
 */
class EdtpaAssessmentAccounts extends AppModel {

   
}
