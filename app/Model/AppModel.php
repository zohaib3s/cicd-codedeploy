<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    var $site_id = '';

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        
        $site_id = '';
        $appC = new AppController();
        $site_id = $appC->check_configuration(true);
        if(empty($site_id))
        {
            if (isset($_SESSION['site_id']) && $_SESSION['site_id'] != 1) {
            $site_id = $_SESSION['site_id'];
            } else {
                $site_id = 1;
            }
        }

        $this->site_id = $site_id;
        
    }

    /**
     * Alters  getLastQuery
     *
     * @return query
     */
    public function getLastQuery() {
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        return $lastLog['query'];
    }

    public function beforeFind($queryData) {
        if (parent::beforeFind($queryData) !== false) {
            
            if (php_sapi_name() == 'cli') {
              $defaultConditions = array($this->alias . '.site_id IN (1,2)');
            } else {
              $defaultConditions = array($this->alias . '.site_id' => $this->site_id);
            }
            if(isset($queryData['conditions'])){
                $queryData['conditions'] = array_merge($defaultConditions, $queryData['conditions']);
            } else {
                $queryData['conditions'] = $defaultConditions;
            }
            return $queryData;
        }
        return false;
    }

    public function beforeSave($options = array()) {
        $this->data[$this->alias]['site_id'] = $this->site_id;        
        parent::beforeSave($options);
    }

   // public function query($queryData) {
      //  echo "<br/>";
      // echo $queryData;
      // echo "<br/>";
//////        //echo "<br/>";
////        //echo "<br/>";
//////        //$r= $this->query->where(array('site_id'=>1));
//////        // echo ($queryData);
//////
////        // print_r($queryData);
////        // echo "<br/>";
//////        //$option = $this->_findSpecial('before',$queryData);
////        // var_dump($option);
////        //echo "<br/>";
 //   }

}
