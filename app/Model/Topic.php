<?php

// app/Model/User.php
class Topic extends AppModel {

    var $useTable = 'topics';

    function getTopics($account_id) {
        $account_id = (int) $account_id;

        $conditions = array();
        $conditions['account_id'] = $account_id;

        return $result = $this->find('all', array(
            'conditions' => $conditions,
            'order' => "Topic.name ASC"
        ));
    }

}

