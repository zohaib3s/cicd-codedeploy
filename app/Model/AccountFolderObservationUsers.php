<?php

class AccountFolderObservationUsers extends AppModel {

    var $useTable = 'account_folder_observation_users';
    var $primaryKey = 'account_folder_observation_user_id';
    public function getnotification($account_folder_observation_id){
         $observation_info=$this->find('all',array(            
            "conditions"=>array("account_folder_observation_id"=>$account_folder_observation_id,"role_id"=>310),              
        )) ;
       return $observation_info;
    }
    public function getobserveruser($account_folder_observation_id,$role_type){
         $observation_info=$this->find('all',array(            
            "conditions"=>array("account_folder_observation_id"=>$account_folder_observation_id,"role_id"=>310),     
             "fields"=>"user_id"
        )) ;
         $old_observer=array();
        
         foreach($observation_info as $old){
             $old_observer[]=$old['AccountFolderObservationUsers']['user_id'];
         }         
       return $old_observer;        
    }
}
