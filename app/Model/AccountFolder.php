<?php

// app/Model/User.php
class AccountFolder extends AppModel {

    var $useTable = 'account_folders';
    public $primaryKey = 'account_folder_id';

    public function getHuddle($account_folder_id, $account_id = 0) {
        $account_folder_id = (int) $account_folder_id;
        $account_id = (int) $account_id;
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => 'User.id = AccountFolder.created_by'
            ),
            array(
                'table' => 'account_folders_meta_data',
                'type' => 'left',
                'conditions' => 'account_folders_meta_data.account_folder_id = AccountFolder.account_folder_id'
            )
        );
        $fields = array(
            'AccountFolder.*',
            'User.username',
            'User.first_name',
            'User.last_name',
            'User.email',
            'User.id as user_id',
            'account_folders_meta_data.meta_data_value as message',
            'account_folders_meta_data.account_folder_meta_data_id as message_id',
            'account_folders_meta_data.meta_data_name as meta_data_name'
        );
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_folder_id' => $account_folder_id,
                //'AccountFolder.account_id' => ($account_id ==0 ? 'AccountFolder.account_id' : $account_id),
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1,
            //'account_folders_meta_data.meta_data_name' => 'message'
            ),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getMetadataValueFromHuddleObjects($huddle, $key){
        $value = false;
        if(count($huddle)>0){
            foreach($huddle as $h){
                $metadata_key = $h['account_folders_meta_data']['meta_data_name'];
                $value = isset($h['account_folders_meta_data']['message']) ? $h['account_folders_meta_data']['message'] : $h['account_folders_meta_data']['meta_data_value'];
                if($key==$metadata_key){
                        return $value;
                }
            }
        }
        return $value;
    }

    public function getAllHuddlesSearch($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $search = '') {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=huddle_users.user_id')
            )
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id'
        );
        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            $where_in .= " OR AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }

        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        $conditions = array(
            'AccountFolder.account_id' => $account_id,
            'AccountFolder.folder_type' => 1,
            'AccountFolder.active' => 1,
            $where_in
        );

        if (isset($search) && $search != '') {
            $conditions = array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1,
                'AccountFolder.name LIKE ' => '%' . $search . '%',
                $where_in
            );
        }

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => $conditions,
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));


        if (is_array($result) && count($result) > 0) {
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];

                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);

                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }

                $result = $output;
            }
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getHuddleUsers($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'inner',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('User.id=huddle_users.user_id')
            )
        );
        $fields = array(
            'huddle_users.*',
            'User.id',
            'User.first_name',
            'User.last_name',
            'User.image',
            'User.email'
        );
        $order_by = 'User.last_name ASC';
        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_folder_id' => $account_folder_id),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => array('User.id')
                )
        );

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getHuddleUsersWithGroups($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'inner',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'account_folder_groups',
                'alias' => 'afg',
                'type' => 'left',
                'conditions' => array('afg.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'user_groups',
                'alias' => 'ug',
                'type' => 'left',
                'conditions' => array('ug.group_id = afg.group_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('OR' => array(
                        'User.id=huddle_users.user_id',
                        'User.id=ug.user_id',
                    ))
            )
        );
        $fields = array(
            'huddle_users.*',
            'User.id',
            'User.first_name',
            'User.last_name',
            'User.image',
            'User.email'
        );
        $order_by = 'User.last_name ASC';
        $groups = array('User.id');
        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_folder_id' => $account_folder_id),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups
                )
        );

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getHuddleUser($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=huddle_users.user_id')
            )
        );
        $fields = array(
            'User.id',
            'User.first_name',
            'User.last_name',
            'User.image',
            'User.email',
            'huddle_users.is_coach2',
            'huddle_users.is_mentee',
            'huddle_users.is_coach'
        );
        $order_by = 'User.last_name ASC';
        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array('AccountFolder.account_folder_id' => $account_folder_id),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => array('User.id')
        ));


        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getHuddleGroupsWithUsers($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $account_folder_groups = array(
            array(
                'table' => 'account_folder_groups',
                'alias' => 'huddle_groups',
                'type' => 'left',
                'conditions' => array('huddle_groups.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'groups as Group',
                'type' => 'left',
                'conditions' => array('Group.id=huddle_groups.group_id')
            ),
            array(
                'table' => 'user_groups as UserGroup',
                'type' => 'left',
                'conditions' => array('UserGroup.group_id=Group.id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('UserGroup.user_id=User.id')
            )
        );
        $fields = array(
            'User.*',
            'huddle_groups.role_id',
            'Group.name',
            'Group.id as group_id'
        );

        $result = $this->find('all', array(
            'joins' => $account_folder_groups,
            'conditions' => array('AccountFolder.account_folder_id' => $account_folder_id),
            'fields' => $fields
        ));
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getHuddleGroups($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $account_folder_groups = array(
            array(
                'table' => 'account_folder_groups',
                'alias' => 'huddle_groups',
                'type' => 'left',
                'conditions' => array('huddle_groups.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'groups as Group',
                'type' => 'left',
                'conditions' => array('Group.id=huddle_groups.group_id')
            )
        );
        $fields = array(
            'huddle_groups.*',
            'Group.name'
        );

        $result = $this->find('all', array(
            'joins' => $account_folder_groups,
            'conditions' => array('AccountFolder.account_folder_id' => $account_folder_id),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllHuddles($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if ($folder_id != false) {
            $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        }/* else{
          $where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
          } */

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1,5)',
                'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id =' . $this->site_id);
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            //echo '<pre>';
            $output = array();
            //$result = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '' && $result[$i]['AccountFolder']['folder_type'] != 5) {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;
            //print_r($result);
            //die;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllHuddlesDashboard($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if ($folder_id != false) {
            $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            $where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
        }

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1,5)',
                'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            //echo '<pre>';
            $output = array();
            //$result = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '' && $result[$i]['AccountFolder']['folder_type'] != 5) {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;
            //print_r($result);
            //die;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountHuddles($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $bool = 0, $limit = 0, $page = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );

        $fields = array(
            'User.*',
            "(SELECT
  COUNT(*) AS `count`
FROM
  `documents` AS `Document`
  INNER JOIN `users` AS `User`
    ON (
      `Document`.`created_by` = `User`.`id`
    )
  INNER JOIN `account_folder_documents` AS `afd`
    ON (
      `Document`.`id` = `afd`.`document_id`
    )
  INNER JOIN `documents` AS `doc`
    ON (`afd`.`document_id` = `doc`.`id`)
WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
  AND `Document`.`doc_type` = 1
  AND `Document`.`active` = '1' )  AS v_total ",
            "(SELECT
  COUNT(*) AS `count`
FROM
  `documents` AS `Document`
  INNER JOIN `users` AS `User`
    ON (
      `Document`.`created_by` = `User`.`id`
    )
  INNER JOIN `account_folder_documents` AS `afd`
    ON (
      `Document`.`id` = `afd`.`document_id`
    )
  INNER JOIN `documents` AS `doc`
    ON (`afd`.`document_id` = `doc`.`id`)
WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
  AND `Document`.`doc_type` = 2
  AND `Document`.`active` = '1' )  AS doc_total ",
            //'(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            "(SELECT
                CASE
                     WHEN meta_data_value  = '2' THEN 'coaching'
                     WHEN meta_data_value = '3' THEN 'evaluation'
                     ELSE NULL
                 END
                FROM
                  account_folders_meta_data
                WHERE account_folder_id = AccountFolder.`account_folder_id`
                  AND meta_data_name = 'folder_type'
                  AND (meta_data_value = 3 OR meta_data_value = 2 ) LIMIT 1 )AS folderType",
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if ($folder_id != false) {
            $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            if (!$bool) {
                $where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
            }
        }
        if ($bool) {
            if (!empty($sort) && $sort == 'name') {
                $order_by = 'AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'date') {
                $order_by = 'AccountFolder.created_date DESC';
            } elseif (!empty($sort) && $sort == 'coaching') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'collaboration') {
                $order_by = 'folder_type ASC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'evaluation') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'folder_type') {
                $order_by = 'folder_type DESC';
            } else {
                $order_by = 'AccountFolder.created_date DESC';
            }
            $params = array(
                //  'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                //    $where_in
                ),
                //    'fields' => $fields,
                'order' => array($order_by),
                    //  'group' => $groups,
            );

            if (0 < (int) $limit) {
                $params['limit'] = (int) $limit;
                $params['offset'] = (int) $page * (int) $limit;
            }
            $result = $this->find('all', $params);
        } else {

            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        }
    //   echo $this->getLastQuery();
    //    exit;

        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }

            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching' && $result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'evaluation') {

                    if ($result[$i][0]['folderType'] == 'evaluation') {

                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }

            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $item['AccountFolder']['v_total'] = $item[0]['v_total'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }
    public function getAllHuddleById($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $bool = 0, $limit = 0, $page = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );

        $fields = array(
            'User.*',
            "(SELECT
  COUNT(*) AS `count`
FROM
  `documents` AS `Document`
  INNER JOIN `users` AS `User`
    ON (
      `Document`.`created_by` = `User`.`id`
    )
  INNER JOIN `account_folder_documents` AS `afd`
    ON (
      `Document`.`id` = `afd`.`document_id`
    )
  INNER JOIN `documents` AS `doc`
    ON (`afd`.`document_id` = `doc`.`id`)
WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
  AND `Document`.`doc_type` = 1
  AND `Document`.`active` = '1' )  AS v_total ",
            "(SELECT
  COUNT(*) AS `count`
FROM
  `documents` AS `Document`
  INNER JOIN `users` AS `User`
    ON (
      `Document`.`created_by` = `User`.`id`
    )
  INNER JOIN `account_folder_documents` AS `afd`
    ON (
      `Document`.`id` = `afd`.`document_id`
    )
  INNER JOIN `documents` AS `doc`
    ON (`afd`.`document_id` = `doc`.`id`)
WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
  AND `Document`.`doc_type` = 2
  AND `Document`.`active` = '1' )  AS doc_total ",
            //'(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            "(SELECT
                CASE
                     WHEN meta_data_value  = '2' THEN 'coaching'
                     WHEN meta_data_value = '3' THEN 'evaluation'
                     ELSE NULL
                 END
                FROM
                  account_folders_meta_data
                WHERE account_folder_id = AccountFolder.`account_folder_id`
                  AND meta_data_name = 'folder_type'
                  AND (meta_data_value = 3 OR meta_data_value = 2 ) LIMIT 1)AS folderType",
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if ($folder_id != false) {
           // $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            if (!$bool) {
               // $where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
            }
        }
        if ($bool) {
            if (!empty($sort) && $sort == 'name') {
                $order_by = 'AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'date') {
                $order_by = 'AccountFolder.created_date DESC';
            } elseif (!empty($sort) && $sort == 'coaching') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'collaboration') {
                $order_by = 'folder_type ASC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'evaluation') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'folder_type') {
                $order_by = 'folder_type DESC';
            } else {
                $order_by = 'AccountFolder.created_date DESC';
            }
            $params = array(
                //  'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                //    $where_in
                ),
                //    'fields' => $fields,
                'order' => array($order_by),
                    //  'group' => $groups,
            );

            if (0 < (int) $limit) {
                $params['limit'] = (int) $limit;
                $params['offset'] = (int) $page * (int) $limit;
            }
            $result = $this->find('all', $params);
        } else {

            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        }
     

        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }

            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching' && $result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'evaluation') {

                    if ($result[$i][0]['folderType'] == 'evaluation') {

                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }

            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $item['AccountFolder']['v_total'] = $item[0]['v_total'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountHuddles_optimised($account_id, $huddle_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $bool = 0, $limit = 0, $page = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );

        $fields = array(
            'User.*',
            "(SELECT
  COUNT(*) AS `count`
FROM
  `documents` AS `Document`
  INNER JOIN `users` AS `User`
    ON (
      `Document`.`created_by` = `User`.`id`
    )
  INNER JOIN `account_folder_documents` AS `afd`
    ON (
      `Document`.`id` = `afd`.`document_id`
    )
  INNER JOIN `documents` AS `doc`
    ON (`afd`.`document_id` = `doc`.`id`)
WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
  AND `Document`.`doc_type` = 1
  AND `Document`.`active` = '1' )  AS v_total ",
            "(SELECT
  COUNT(*) AS `count`
FROM
  `documents` AS `Document`
  INNER JOIN `users` AS `User`
    ON (
      `Document`.`created_by` = `User`.`id`
    )
  INNER JOIN `account_folder_documents` AS `afd`
    ON (
      `Document`.`id` = `afd`.`document_id`
    )
  INNER JOIN `documents` AS `doc`
    ON (`afd`.`document_id` = `doc`.`id`)
WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
  AND `Document`.`doc_type` = 2
  AND `Document`.`active` = '1' )  AS doc_total ",
            //'(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            "(SELECT
                CASE
                     WHEN meta_data_value  = '2' THEN 'coaching'
                     WHEN meta_data_value = '3' THEN 'evaluation'
                     ELSE NULL
                 END
                FROM
                  account_folders_meta_data
                WHERE account_folder_id = AccountFolder.`account_folder_id`
                  AND meta_data_name = 'folder_type'
                  AND (meta_data_value = 3 OR meta_data_value = 2 ) LIMIT 1)AS folderType",
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if ($folder_id != false) {
            $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            if (!$bool) {
                $where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
            }
        }
        if ($bool) {
            if (!empty($sort) && $sort == 'name') {
                $order_by = 'AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'date') {
                $order_by = 'AccountFolder.created_date DESC';
            } elseif (!empty($sort) && $sort == 'coaching') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'collaboration') {
                $order_by = 'folder_type ASC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'evaluation') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'folder_type') {
                $order_by = 'folder_type DESC';
            } else {
                $order_by = 'AccountFolder.created_date DESC';
            }
            $params = array(
                //  'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                //    $where_in
                ),
                //    'fields' => $fields,
                'order' => array($order_by),
                    //  'group' => $groups,
            );

            if (0 < (int) $limit) {
                $params['limit'] = (int) $limit;
                $params['offset'] = (int) $page * (int) $limit;
            }
            $result = $this->find('all', $params);
        } else {

            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
                    'AccountFolder.account_folder_id' => $huddle_id,
//                'AccountFolder.parent_folder_id IS NULL',
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        }
//       echo $this->getLastQuery();
//        exit;

        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }

            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching' && $result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'evaluation') {

                    if ($result[$i][0]['folderType'] == 'evaluation') {

                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }

            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $item['AccountFolder']['v_total'] = $item[0]['v_total'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function folder_inside_search_huddle_api($title, $account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $bool = 0, $limit = 0, $page = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );

        $fields = array(
            'User.*',
            //'(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            "(SELECT
                CASE
                     WHEN meta_data_value  = '2' THEN 'coaching'
                     WHEN meta_data_value = '3' THEN 'evaluation'
                     ELSE NULL
                 END
                FROM
                  account_folders_meta_data
                WHERE account_folder_id = AccountFolder.`account_folder_id`
                  AND meta_data_name = 'folder_type'
                  AND (meta_data_value = 3 OR meta_data_value = 2 ) LIMIT 1)AS folderType",
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if ($folder_id != false) {
            $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            if (!$bool) {
                $where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
            }
        }
        if ($bool) {
            if (!empty($sort) && $sort == 'name') {
                $order_by = 'AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'date') {
                $order_by = 'AccountFolder.created_date DESC';
            } elseif (!empty($sort) && $sort == 'coaching') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'collaboration') {
                $order_by = 'folder_type ASC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'evaluation') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'folder_type') {
                $order_by = 'folder_type DESC';
            } else {
                $order_by = 'AccountFolder.created_date DESC';
            }
            $params = array(
                //  'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
                    'AccountFolder.name like ' . "'%$title%'",
//                'AccountFolder.parent_folder_id IS NULL',
                //    $where_in
                ),
                //    'fields' => $fields,
                'order' => array($order_by),
                    //  'group' => $groups,
            );
            if ($folder_id != false) {
                $params['conditions']["AccountFolder.parent_folder_id"] = "$folder_id";
            }
            if (0 < (int) $limit) {
                $params['limit'] = (int) $limit;
                $params['offset'] = (int) $page * (int) $limit;
            }
            $result = $this->find('all', $params);
        } else {

            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                    'AccountFolder.name like ' . "'%$title%'",
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        }
//       echo $this->getLastQuery();
//        exit;

        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }

            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching' && $result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'evaluation') {

                    if ($result[$i][0]['folderType'] == 'evaluation') {

                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }

            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountHuddlesArchive($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $limit = 0, $page = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if ($folder_id != false) {
            $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            //$where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
        }
        $params = array(
            //  'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 0,
                'AccountFolder.archive' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
            //      $where_in
            ),
            //     'fields' => $fields,
            'order' => array($order_by),
                //     'group' => $groups,
        );
        if (0 < (int) $limit) {
            $params['limit'] = (int) $limit;
            $params['offset'] = (int) $page * (int) $limit;
        }

        $result = $this->find('all', $params);
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }
            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function hasAccountHuddlesInFolder($account_id, $user_id, $folder_id) {

        $account_id = (int) $account_id;
        $fields = array(
            'AccountFolder.*'
        );


        $where_in = "( ( account_folder_id IN( select  account_folder_id from account_folder_users where user_id=$user_id ) ) OR (account_folder_id IN (select  account_folder_id from account_folder_groups afg join user_groups ug on afg.group_id = ug.group_id where ug.user_id = $user_id  ) ) ) ";

        if ($folder_id != false) {
            $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            $where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
        }

        $result = $this->find('all', array(
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1,
                $where_in
            ),
            'fields' => $fields
        ));

        if (is_array($result) && count($result) > 0) {
            return true;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountFolders($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $folder_path = '/root/', $bool = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            "concat('$folder_path', AccountFolder.name) as SibmePath"
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folder_type DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folder_type ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        //for folders we only care about current structure in account and parent
        $where_in = '';

        if ($folder_id != false) {
            $where_in .= " AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            if (!$bool) {
                $where_in .= " AccountFolder.parent_folder_id IS NULL ";
            }
        }
        if ($bool) {
            $where = " AccountFolder.parent_folder_id IS NULL ";
            $result = $this->find('all', array(
                //'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 5,
                    'AccountFolder.active' => 1,
                    $where
                ),
                //    'fields' => $fields,
                'order' => array($order_by),
                    //    'group' => $groups,
            ));
        } else {
            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 5,
                    'AccountFolder.active' => 1,
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        }
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    $output[] = $result[$i];
                } elseif (!empty($sort) && $sort == 'evaluation') {
                    if ($result[$i][0]['folderType'] == 'evaluation') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }
            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountFolders_API($title, $account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $folder_path = '/root/', $bool = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            "concat('$folder_path', AccountFolder.name) as SibmePath"
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folder_type DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folder_type ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        //for folders we only care about current structure in account and parent
        $where_in = '';

        if ($folder_id != false) {
            $where_in .= " AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            if (!$bool) {
                $where_in .= " AccountFolder.parent_folder_id IS NULL ";
            }
        }
        if ($bool) {
            //$where = " AccountFolder.parent_folder_id IS NULL ";
            $conditions = array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 5,
                'AccountFolder.active' => 1,
                'AccountFolder.name like ' . "'%$title%'",
                //    $where
            );
            if($folder_id)
            {
                $conditions["AccountFolder.parent_folder_id"] = "$folder_id";
            }
            $result = $this->find('all', array(
                //'joins' => $account_folder_users,
                'conditions' => $conditions,
                //    'fields' => $fields,
                'order' => array($order_by),
                    //    'group' => $groups,
            ));
        } else {
            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 5,
                    'AccountFolder.active' => 1,
                    'AccountFolder.name like ' . "'%$title%'",
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        }
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    $output[] = $result[$i];
                } elseif (!empty($sort) && $sort == 'evaluation') {
                    if ($result[$i][0]['folderType'] == 'evaluation') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }
            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountFoldersComplete($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $folder_path = '/root/', $bool = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            "concat('$folder_path', AccountFolder.name) as SibmePath"
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folder_type DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folder_type ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        //for folders we only care about current structure in account and parent
        $where_in = '';

        if ($folder_id != false) {
            $where_in .= " AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            if (!$bool) {
                //      $where_in .= " AccountFolder.parent_folder_id IS NULL ";
            }
        }
        if ($bool) {
            $where = " AccountFolder.parent_folder_id IS NULL ";
            $result = $this->find('all', array(
                //'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 5,
                    'AccountFolder.active' => 1,
                    $where
                ),
                //    'fields' => $fields,
                'order' => array($order_by),
                    //    'group' => $groups,
            ));
        } else {
            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 5,
                    'AccountFolder.active' => 1,
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        }
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    $output[] = $result[$i];
                } else {
                    $output[] = $result[$i];
                }
            }
            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountFoldersArchive($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $folder_path = '/root/') {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            "concat('$folder_path', AccountFolder.name) as SibmePath"
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folder_type DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folder_type ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        //for folders we only care about current structure in account and parent
        $where_in = '';

        if ($folder_id != false) {
            $where_in .= " AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            $where_in .= " AccountFolder.parent_folder_id IS NULL ";
        }
        $where = " AccountFolder.parent_folder_id IS NULL ";
        $result = $this->find('all', array(
            //   'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 5,
                'AccountFolder.active' => 0,
                'AccountFolder.archive' => 1,
                $where
            ),
            //        'fields' => $fields,
            'order' => array($order_by),
                //      'group' => $groups,
        ));
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    $output[] = $result[$i];
                } else {
                    $output[] = $result[$i];
                }
            }
            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function search($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $title = '') {
        $account_id = (int) $account_id;

        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        $where_in .= '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false)
                $where_in .= " OR ";
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }

        if ($account_folder_ids == false && $accountFolderGroupsIds == false) {
            $where_in .=' AccountFolder.account_folder_id IN(-1)  ';
        }
        $where_in .= ')';


        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1,5)',
                'AccountFolder.active' => 1,
                'AccountFolder.parent_folder_id IS NULL',
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));


        if (is_array($result) && count($result) > 0) {

            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();

            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '' && $result[$i]['AccountFolder']['folder_type'] != 5) {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];

                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);

                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function searchAccountHuddles($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $title = '', $bool = 0) {
        $account_id = (int) $account_id;

        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            '(SELECT "evaluation" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 3) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        $where_in .= '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false)
                $where_in .= " OR ";
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }

        if ($account_folder_ids == false && $accountFolderGroupsIds == false) {
            $where_in .=' AccountFolder.account_folder_id IN(-1)  ';
        }
        $where_in .= ')';


        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if (!$bool) {
            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
                    'AccountFolder.parent_folder_id IS NULL',
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        } else {
            if (!empty($title)) {

                $where = ' (AccountFolder.name like ' . "'%$title%'";
                $where .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
                $where .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
            }
            if (empty($title)) {

                $result = $this->find('all', array(
                    //'joins' => $account_folder_users,
                    'conditions' => array(
                        'AccountFolder.account_id' => $account_id,
                        'AccountFolder.folder_type' => 1,
                        'AccountFolder.active' => 1,
                    // 'AccountFolder.parent_folder_id IS NULL',
                    // $where
                    ),
                    //     'fields' => $fields,
                    'order' => array($order_by),
                        //    'group' => $groups,
                ));
            } else {

                $result = $this->find('all', array(
                    //'joins' => $account_folder_users,
                    'conditions' => array(
                        'AccountFolder.account_id' => $account_id,
                        'AccountFolder.folder_type' => 1,
                        'AccountFolder.active' => 1,
                        // 'AccountFolder.parent_folder_id IS NULL',
                        $where
                    ),
                    //     'fields' => $fields,
                    'order' => array($order_by),
                        //    'group' => $groups,
                ));
            }
        }


        if (is_array($result) && count($result) > 0) {

            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();

            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] != 'collaboration' && $result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'evaluation') {
                    if ($result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;
            /* if ($array_format) {
              $output = array();
              for ($i = 0; $i < count($result); $i++) {
              $item = $result[$i];

              $created_at = new DateTime($item['AccountFolder']['created_date']);
              $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);

              $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
              $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
              $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
              $output[] = $item['AccountFolder'];
              }
              $result = $output;
              } */

            return $result;
        } else {
            return FALSE;
        }
    }

    public function searchAccountHuddles_textbox($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $title = '', $bool = 0) {

        $account_id = (int) $account_id;

        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            ),
            array(
                'table' => 'account_folders_meta_data as afmd',
                'type' => 'left',
                'conditions' => array('afmd.account_folder_id = AccountFolder.account_folder_id')
            )
        );
        $fields = array(
            'User.*', "(SELECT
            COUNT(*) AS `count`
          FROM
            `documents` AS `Document`
            INNER JOIN `users` AS `User`
              ON (
                `Document`.`created_by` = `User`.`id`
              )
            INNER JOIN `account_folder_documents` AS `afd`
              ON (
                `Document`.`id` = `afd`.`document_id`
              )
            INNER JOIN `documents` AS `doc`
              ON (`afd`.`document_id` = `doc`.`id`)
          WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
            AND `Document`.`doc_type` = 1
            AND `Document`.`active` = '1' )  AS v_total ",
            "(SELECT
            COUNT(*) AS `count`
          FROM
            `documents` AS `Document`
            INNER JOIN `users` AS `User`
              ON (
                `Document`.`created_by` = `User`.`id`
              )
            INNER JOIN `account_folder_documents` AS `afd`
              ON (
                `Document`.`id` = `afd`.`document_id`
              )
            INNER JOIN `documents` AS `doc`
              ON (`afd`.`document_id` = `doc`.`id`)
          WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
            AND `Document`.`doc_type` = 2
            AND `Document`.`active` = '1' )  AS doc_total ",
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2 ORDER BY created_date DESC LIMIT 1 ) AS folderType',
            '(SELECT "evaluation" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 3 ORDER BY created_date DESC LIMIT 1) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            'afmd.*'
        );


        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        $where_in .= '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false)
                $where_in .= " OR ";
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }

        if ($account_folder_ids == false && $accountFolderGroupsIds == false) {
            $where_in .=' AccountFolder.account_folder_id IN(-1)  ';
        }
        $where_in .= ')';


        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if (!$bool) {
            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
                    //  'AccountFolder.parent_folder_id IS NULL',
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        } else {
            if (!empty($title)) {

                $where = ' (AccountFolder.name like ' . "'%$title%'";
                $where .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
                $where .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
            }
            if (empty($title)) {

                $result = $this->find('all', array(
                    //'joins' => $account_folder_users,
                    'conditions' => array(
                        'AccountFolder.account_id' => $account_id,
                        'AccountFolder.folder_type' => 1,
                        'AccountFolder.active' => 1,
                    // 'AccountFolder.parent_folder_id IS NULL',
                    // $where
                    ),
                    //     'fields' => $fields,
                    'order' => array($order_by),
                        //    'group' => $groups,
                ));
            } else {

                $result = $this->find('all', array(
                    //'joins' => $account_folder_users,
                    'conditions' => array(
                        'AccountFolder.account_id' => $account_id,
                        'AccountFolder.folder_type' => 1,
                        'AccountFolder.active' => 1,
                        // 'AccountFolder.parent_folder_id IS NULL',
                        $where
                    ),
                    //     'fields' => $fields,
                    'order' => array($order_by),
                        //    'group' => $groups,
                ));
            }
        }



        if (is_array($result) && count($result) > 0) {

            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();

            for ($i = 0; $i < count($result); $i++) {


                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] != 'collaboration' && $result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }

            $result = $output;
            /* if ($array_format) {
              $output = array();
              for ($i = 0; $i < count($result); $i++) {
              $item = $result[$i];

              $created_at = new DateTime($item['AccountFolder']['created_date']);
              $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);

              $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
              $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
              $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
              $output[] = $item['AccountFolder'];
              }
              $result = $output;
              } */

            return $result;
        } else {
            return FALSE;
        }
    }

    public function searchAccountHuddles_archive($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $title = '') {
        $account_id = (int) $account_id;

        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        $where_in .= '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false)
                $where_in .= " OR ";
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }

        if ($account_folder_ids == false && $accountFolderGroupsIds == false) {
            $where_in .=' AccountFolder.account_folder_id IN(-1)  ';
        }
        $where_in .= ')';


        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }

        if (!empty($title)) {

            $where = ' (AccountFolder.name like ' . "'%$title%'";
            $where .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }

        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if (!empty($title)) {
            $result = $this->find('all', array(
                //   'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 0,
                    'AccountFolder.archive' => 1,
                    // 'AccountFolder.parent_folder_id IS NULL',
                    $where
                ),
                //    'fields' => $fields,
                'order' => array($order_by),
                    //   'group' => $groups,
            ));
        } else {
            $result = $this->find('all', array(
                //   'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 0,
                    'AccountFolder.archive' => 1,
                    // 'AccountFolder.parent_folder_id IS NULL',
                    $where_in
                ),
                //    'fields' => $fields,
                'order' => array($order_by),
                    //   'group' => $groups,
            ));
        }


        if (is_array($result) && count($result) > 0) {

            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();

            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;
            /* if ($array_format) {
              $output = array();
              for ($i = 0; $i < count($result); $i++) {
              $item = $result[$i];

              $created_at = new DateTime($item['AccountFolder']['created_date']);
              $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);

              $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
              $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
              $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
              $output[] = $item['AccountFolder'];
              }
              $result = $output;
              } */

            return $result;
        } else {
            return FALSE;
        }
    }

    public function searchAccountFolders($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $title = '') {
        $account_id = (int) $account_id;

        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        $where_in .= '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false)
                $where_in .= " OR ";
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }

        if ($account_folder_ids == false && $accountFolderGroupsIds == false) {
            $where_in .=' AccountFolder.account_folder_id IN(-1)  ';
        }
        $where_in .= ')';


        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%')";
            /* $where_in .= " OR AccountFolder.account_folder_id IN (
              select afd.account_folder_id from account_folder_documents afd
              WHERE afd.title like '%$title%' ) ";
              $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
              from comments as comment join documents as doc on doc.id = comment.ref_id
              join account_folder_documents afd on afd.document_id=doc.id
              WHERE comment.comment like '%" . $title . "%') ) "; */
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 5,
                'AccountFolder.active' => 1,
                'AccountFolder.parent_folder_id IS NULL',
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));


        if (is_array($result) && count($result) > 0) {

            /* for ($i = 0; $i < count($result); $i++) {
              $item = $result[$i];
              $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = '.$item['AccountFolder']['account_folder_id'].' AND meta_data_name = "folder_type"');
              $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value'])?$res[0]['account_folders_meta_data']['meta_data_value']:'';
              }
              $output = array();

              for ($i = 0; $i < count($result); $i++) {
              if(!empty($sort) && $sort == 'coaching'){
              if($result[$i][0]['folderType'] == 'coaching'){
              $output[] = $result[$i];
              }
              }elseif(!empty($sort) && $sort == 'collaboration'){
              if($result[$i][0]['folderType'] == '' && $result[$i]['AccountFolder']['folder_type'] != 5){
              $output[] = $result[$i];
              }
              }else{
              $output[] = $result[$i];
              }
              //$output[] = $item;
              }
              $result = $output;
              if ($array_format) {
              $output = array();
              for ($i = 0; $i < count($result); $i++) {
              $item = $result[$i];

              $created_at = new DateTime($item['AccountFolder']['created_date']);
              $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);

              $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
              $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
              $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
              $output[] = $item['AccountFolder'];
              }
              $result = $output;
              } */

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllHuddlesForPeople($account_id, $sort = '') {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=AccountFolder.created_by')
            )
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
                )
        );

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function get_all_huddle_users($account_id, $sort = '') {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=AccountFolder.created_by')
            ),
           
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
        );

        $order_by = array('AccountFolder.name ASC', 'User.first_name ASC');

        $groups = array('AccountFolder.account_folder_id');
        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));


        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function get_all_huddle_users_ajax($account_id, $keyword = '', $sort = '') {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=AccountFolder.created_by')
            )
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
        );
        if ($keyword != '') {
            $where = array('AccountFolder.name LIKE' => '%' . $keyword . '%');
        } else {
            $where = '';
        }

        $order_by = array('AccountFolder.name ASC', 'User.first_name ASC');
        $groups = array('AccountFolder.account_folder_id');
        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1,
                $where
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
                )
        );

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getVideoLibraryCount($domain, $accId) {
        $accId = (int) $accId;
        if ($domain == "subjects") {
            /* $query = "select count(*) as count
              from subjects s inner join account_folder_subjects sv on s.id=sv.subject_id
              inner join account_folders v on sv.account_folder_id=v.account_folder_id
              where v.active=1 and v.account_id=$accId and v.folder_type=2"; */
            /* $query = "SELECT count(*) as count from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id JOIN account_folder_subjects afs ON afd.account_folder_id = afs.account_folder_id where 1=1 and account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and d.doc_type=1 order by account_folders.created_date desc";
             */
            $query = "SELECT COUNT(DISTINCT(af.account_folder_id)) as count FROM account_folders af LEFT JOIN account_folder_subjects sf ON af.`account_folder_id` = sf.account_folder_id JOIN account_folder_documents afd ON af.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE af.account_id = $accId AND af.folder_type =2 AND af.active = 1 AND d.doc_type = 1 AND sf.id IS NOT NULL AND af.site_id=" . $this->site_id;
            $result = $this->query($query);
            if (is_array($result) && isset($result[0][0]['count']))
                return $result[0][0]['count'];
        } else if ($domain == "topics") {
            /* $query = "select count(*) as count
              from topics t inner join account_folder_topics tv on t.id=tv.topic_id
              inner join account_folders v on tv.account_folder_id=v.account_folder_id
              where  v.active=1 and v.account_id=$accId and v.folder_type=2"; */
            /* $query = "SELECT count(*) as count from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id JOIN account_folder_topics aft ON afd.account_folder_id = aft.account_folder_id where 1=1 and account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and d.doc_type=1 order by account_folders.created_date desc"; */
            $query = "SELECT COUNT(DISTINCT(af.account_folder_id)) as count FROM account_folders af LEFT JOIN account_folder_topics tf ON af.`account_folder_id` = tf.account_folder_id JOIN account_folder_documents afd ON af.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE af.account_id = $accId AND af.folder_type =2 AND af.active = 1 AND d.doc_type = 1 AND tf.id IS NOT NULL AND af.site_id = " . $this->site_id;
            $result = $this->query($query);
            if (is_array($result) && isset($result[0][0]['count']))
                return $result[0][0]['count'];
        } else if ($domain == "unique_subjects") {
            $query = "select count(*) count from (
                select distinct v.account_folder_id as id
                from subjects s inner join account_folder_subjects sv on s.id=sv.subject_id
                inner join account_folders v on sv.account_folder_id=v.account_folder_id
                where  v.active=1 and v.account_id=$accId and v.folder_type=2  and v.site_id = " . $this->site_id . ") as UniqueSubjects";
            $result = $this->query($query);
            if (is_array($result) && isset($result[0][0]['count']))
                return $result[0][0]['count'];
        } else if ($domain == "unique_topics") {
            $query = "select count(*) count from (
                select distinct v.account_folder_id as id
                from topics t inner join account_folder_topics tv on t.id=tv.topic_id
                inner join account_folders v on tv.account_folder_id=v.account_folder_id
                where  v.active=1 and v.account_id=$accId and v.folder_type=2 and v.site_id = " . $this->site_id . ") as UniqueTopics";
            $result = $this->query($query);
            if (is_array($result) && isset($result[0][0]['count']))
                return $result[0][0]['count'];
        } else if ($domain == "all") {
            //$tot = $this->find('count', array('conditions' => array('account_id' => $accId, 'folder_type' => 2, 'active' => '1')));
            $query = "SELECT COUNT(DISTINCT(af.account_folder_id)) AS COUNT FROM account_folders af LEFT JOIN account_folder_subjects sf ON af.`account_folder_id` = sf.account_folder_id JOIN account_folder_documents afd ON af.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE af.account_id = $accId AND af.folder_type =2 AND af.active = 1 AND d.doc_type = 1 AND af.site_id=" . $this->site_id;
            $result = $this->query($query);
            if (is_array($result) && isset($result[0][0]['COUNT']))
                return $result[0][0]['COUNT'];
            //return $tot;
        }else if ($domain == "uncat") {
            //$tot = $this->find('count', array('conditions' => array('account_id' => $accId, 'folder_type' => 2, 'active' => '1')));
            $query = "SELECT COUNT(DISTINCT(af.account_folder_id)) AS COUNT FROM account_folders af LEFT JOIN account_folder_subjects sf ON af.`account_folder_id` = sf.account_folder_id JOIN account_folder_documents afd ON af.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE af.account_id = $accId AND af.folder_type =2 AND af.active = 1 AND d.doc_type = 1 AND sf.id IS NULL AND af.site_id=" . $this->site_id;
            $result = $this->query($query);
            if (is_array($result) && isset($result[0][0]['COUNT']))
                return $result[0][0]['COUNT'];
        }
    }

    function getSubjectTopicVideos($accId, $subjectId, $domain, $order, $start, $rows, $topicId) {
        $accId = (int) $accId;
        $subjectId = (int) $subjectId;
        $topicId = (int) $topicId;

        $orderby = "";
        if ($order != 'n') {
            global $orderby;
            $orderby = " order by $order desc";
        } elseif ($order == 'n') {
            $orderby = " order by account_folders.created_date desc";
        }

        if ($domain == 'subject') {
            $extraWhere = '';
            if ($subjectId > 0) {
                $extraWhere .= " AND afs.subject_id=$subjectId ";
            }

            /* if ($topicId > 0) {
              $extraWhere .= " AND aft.topic_id=$topicId ";
              } */

            //$query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) as total from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id JOIN account_folder_subjects afs ON afd.account_folder_id = afs.account_folder_id where 1=1 $extraWhere and account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and d.doc_type=1" . $orderby;
            $query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) AS total FROM account_folders LEFT JOIN account_folder_subjects afs ON account_folders.`account_folder_id` = afs.account_folder_id JOIN account_folder_documents afd ON account_folders.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE account_folders.account_id = $accId AND account_folders.folder_type =2 AND account_folders.active = 1 AND d.doc_type = 1 AND account_folders.site_id= " . $this->site_id . " $extraWhere AND afs.id IS NOT NULL" . $orderby;
            $query2 = "SELECT account_folders.*, d.id, d.url, d.view_count,d.created_date, d.published, d.encoder_provider, d.encoder_status, d.original_file_name, d.created_by, d.thumbnail_number from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id JOIN account_folder_subjects afs ON account_folders.account_folder_id = afs.account_folder_id where 1=1 $extraWhere and account_folders.active=1 and account_folders.account_id=$accId and account_folders.site_id =" . $this->site_id . " and folder_type=2 and d.doc_type=1 group by account_folders.account_folder_id" . $orderby . " limit $start,$rows";
        } else if ($domain == 'topic') {

            $extraWhere = '';

            if ($topicId > 0) {
                $extraWhere .= " AND afs.subject_id=$topicId ";
            }

            if ($subjectId > 0) {
                $extraWhere .= " AND aft.topic_id=$subjectId ";
            }

            //$query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) as total from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id JOIN account_folder_topics aft ON afd.account_folder_id = aft.account_folder_id where 1=1 $extraWhere and account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and d.doc_type=1" . $orderby;
            $query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) AS total FROM account_folders LEFT JOIN account_folder_topics aft ON account_folders.`account_folder_id` = aft.account_folder_id JOIN account_folder_documents afd ON account_folders.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE account_folders.account_id = $accId AND account_folders.folder_type =2 AND account_folders.active = 1 AND account_folders.site_id =" . $this->site_id . " AND d.doc_type = 1 $extraWhere AND aft.id IS NOT NULL" . $orderby;
            $query2 = "SELECT account_folders.*, d.id, d.url, d.view_count,d.created_date, d.published, d.encoder_provider,d.encoder_status, d.original_file_name, d.created_by, d.thumbnail_number from account_folders left join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id left join documents d on afd.document_id = d.id LEFT JOIN account_folder_topics aft ON account_folders.account_folder_id = aft.account_folder_id where 1=1 $extraWhere and account_folders.active=1 and account_folders.account_id=$accId and account_folders.folder_type=2 AND account_folders.site_id =" . $this->site_id . " group by account_folders.account_folder_id" . $orderby . " limit $start,$rows";
        } else if ($domain == 'all') {
            $query1 = "SELECT count(*) as total from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id  where  account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and account_folders.site_id =" . $this->site_id . "  and d.doc_type=1 $orderby ";
            $query2 = "SELECT account_folders.*, d.id, d.url, d.view_count,d.created_date, d.published, d.encoder_provider, d.encoder_status, d.original_file_name, d.created_by, d.thumbnail_number from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id where account_folders.active=1 and account_folders.account_id=$accId AND account_folders.site_id =" . $this->site_id . " and folder_type=2 and d.doc_type=1" . $orderby . " limit $start,$rows";
        } else if ($domain == 'uncat') {
            $query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) AS total FROM account_folders LEFT JOIN account_folder_subjects afs ON account_folders.`account_folder_id` = afs.account_folder_id JOIN account_folder_documents afd ON account_folders.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE account_folders.account_id = $accId AND account_folders.folder_type =2 AND account_folders.active = 1 AND d.doc_type = 1 AND afs.id IS NULL" . $orderby;
            $query2 = "SELECT account_folders.*, d.id, d.url, d.view_count,d.created_date, d.published, d.encoder_provider, d.encoder_status, d.original_file_name, d.created_by, d.thumbnail_number from account_folders left join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id left join documents d on afd.document_id = d.id LEFT JOIN account_folder_subjects afs ON account_folders.account_folder_id = afs.account_folder_id where 1=1 and account_folders.active=1 and account_folders.account_id=$accId AND account_folders.site_id =" . $this->site_id . " and account_folders.folder_type=2 AND d.doc_type = 1 AND afs.id IS NULL group by account_folders.account_folder_id" . $orderby . " limit $start,$rows";
        }


        $result1 = $this->query($query1);

        $result2 = $this->query($query2);

        $arr = array();
        $i = 0;

        foreach ($result2 as $res) {
            //$viewCount = $this->getViewCount($res['account_folders']['account_folder_id']);
            $arr[$i]['id'] = $res['account_folders']['account_folder_id'];
            $arr[$i]['video_title'] = $res['account_folders']['name'];
            $arr[$i]['document_id'] = $res['d']['id'];
            $arr[$i]['video_url'] = $res['d']['url'];
            $arr[$i]['video_desc'] = $res['account_folders']['desc'];
            $arr[$i]['view_count'] = $res['d']['view_count'];
            $arr[$i]['published'] = $res['d']['published'];
            $arr[$i]['encoder_provider'] = $res['d']['encoder_provider'];
            $arr[$i]['encoder_status'] = $res['d']['encoder_status'];
            $arr[$i]['created_date'] = $res['d']['created_date'];
            $arr[$i]['original_file_name'] = $res['d']['original_file_name'];
            $arr[$i]['created_by'] = $res['d']['created_by'];
            $arr[$i]['thumbnail_number'] = $res['d']['thumbnail_number'];
            $i++;
        }

        if (isset($result1[0][0]['total'])) {
            //$arr['total_rec'] = $result1[0][0]['total'];
            $arr['total'] = $result1[0][0]['total'] - $start;
        }

        return $arr;
    }

    function getViewCount($documentId) {
        $documentId = (int) $documentId;
        $query = "SELECT doc.view_count,doc.created_date from account_folder_documents join documents doc on doc.id= account_folder_documents.document_id where account_folder_documents.account_folder_id =" . $documentId." AND account_folder_documents.site_id=".$this->site_id;
        $result = $this->query($query);
        $viewCount = array(
            'view_count' => $result[0]['doc']['view_count'],
            'created_date' => $result[0]['doc']['created_date']
        );
        return $viewCount;
    }

    function getVideosByTitle($accId, $vidTitle, $order, $start, $rows) {
        $accId = (int) $accId;
        $orderby = "";
        if ($order != 'n') {
            global $orderby;
            $orderby = " order by $order desc";
        } elseif ($order == 'n') {
            $orderby = " order by account_folders.created_date desc";
        }
        if ($vidTitle == 'e1a2c3d4b5')
            global $vidTitle;
        $vidTitle == '';

        $where = " AND ( ";
        $where .= " name like '%$vidTitle%' ";
        //tags
        $where .= " OR account_folders.account_folder_id IN  ( select afmd.account_folder_id from account_folders_meta_data afmd join account_folders af on afmd.account_folder_id = af.account_folder_id where meta_data_name='tag' and meta_data_value like '%$vidTitle%' and af.account_id = $accId  ) ";
        //topics
        $where .= " OR account_folders.account_folder_id in (SELECT account_folder_id from account_folder_topics join topics on account_folder_topics.topic_id = topics.id where topics.name like '%$vidTitle%') ";
        //subjects
        $where .= " OR account_folders.account_folder_id in (SELECT account_folder_id from account_folder_subjects join subjects on account_folder_subjects.subject_id = subjects.id where subjects.name like '%$vidTitle%') ";
        $where .= " ) ";

        $query1 = "SELECT count(*) as total from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id where 1=1 $where and account_folders.account_id=$accId and account_folders.folder_type=2 AND d.doc_type = 1 AND account_folders.active = 1 AND account_folders.site_id='".$this->site_id."' " . $orderby;
        $query2 = "SELECT *, d.url,d.id, d.view_count,d.created_date, d.published, d.encoder_provider, d.encoder_status, d.thumbnail_number from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id where 1=1 $where and account_folders.account_id=$accId and account_folders.folder_type=2 and account_folders.active=1 and account_folders.site_id =".$this->site_id." and d.doc_type=1 " . $orderby . " limit $start,$rows";

        $result1 = $this->query($query1);
        $result2 = $this->query($query2);
        $arr = array();
        $i = 0;

        foreach ($result2 as $res) {
            $arr[$i]['id'] = $res['account_folders']['account_folder_id'];
            $arr[$i]['document_id'] = $res['d']['id'];
            $arr[$i]['video_title'] = $res['account_folders']['name'];
            $arr[$i]['video_url'] = $res['d']['url'];
            $arr[$i]['created_date'] = $res['d']['created_date'];
            $arr[$i]['view_count'] = $res['d']['view_count'];
            $arr[$i]['published'] = $res['d']['published'];
            $arr[$i]['encoder_provider'] = $res['d']['encoder_provider'];
            $arr[$i]['encoder_status'] = $res['d']['encoder_status'];
            $arr[$i]['original_file_name'] = $res['d']['original_file_name'];
            $arr[$i]['video_desc'] = $res['account_folders']['desc'];
            $arr[$i]['thumbnail_number'] = $res['d']['thumbnail_number'];
            $i++;
        }

        if (isset($result1[0][0]['total']))
            $arr['total'] = $result1[0][0]['total'] - $start;

        return $arr;
    }

    function getSubjects($accId) {
        $accId = (int) $accId;

        $query = "select s.id as subject_id,s.name,count(*) as count
                from subjects s inner join account_folder_subjects sv on s.id=sv.subject_id
                inner join account_folders v on sv.account_folder_id=v.account_folder_id
                where  v.active=1 and v.account_id=$accId and v.folder_type=2 and v.site_id = " . $this->site_id . "
                group by s.id
                order by s.id asc";

        $result = $this->query($query);
        $arr = array();
        $i = 0;

        foreach ($result as $res) {
            $arr[$i]['subject_id'] = $res['s']['subject_id'];
            $arr[$i]['name'] = $res['s']['name'];
            $arr[$i]['count'] = $res[0]['count'];
            $i++;
        }
        return $arr;
    }

    function getTopics($accId) {
        $accId = (int) $accId;

        $query = "select t.id as topic_id,t.name,count(*) as count
                from topics t inner join account_folder_topics tv on t.id=tv.topic_id
                inner join account_folders v on tv.account_folder_id=v.account_folder_id
                where  v.active=1 and v.account_id=$accId and v.folder_type=2 and v.site_id = " . $this->site_id . "
                group by t.id
                order by t.id asc";

        $result = $this->query($query);
        $arr = array();
        $i = 0;

        foreach ($result as $res) {
            $arr[$i]['topic_id'] = $res['t']['topic_id'];
            $arr[$i]['name'] = $res['t']['name'];
            $arr[$i]['count'] = $res[0]['count'];
            $i++;
        }
        return $arr;
    }

    public function getVideo($account_folder_id, $account_id) {
        $account_folder_id = (int) $account_folder_id;
        $account_id = (int) $account_id;
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => 'User.id = AccountFolder.created_by'
            )
        );
        $fields = array(
            'AccountFolder.*',
            'User.username',
            'User.first_name',
            'User.last_name',
            'User.email',
            'User.image'
        );
        $result = $this->find('first', array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_folder_id' => $account_folder_id,
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 2,
                'AccountFolder.active' => 1
            ),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getVideoTags($account_folder_id, $account_id) {
        $account_folder_id = (int) $account_folder_id;
        $account_id = (int) $account_id;
        $joins = array(
            array(
                'table' => 'account_folders_meta_data',
                'type' => 'inner',
                'conditions' => 'account_folders_meta_data.account_folder_id = AccountFolder.account_folder_id'
            )
        );
        $fields = array(
            'account_folders_meta_data.*'
        );
        $result = $this->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_folder_id' => $account_folder_id,
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 2,
                'AccountFolder.active' => 1
            ),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getUserVideoCount($account_id, $user_id) {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;

        $fields = array(
            'AccountFolder.*'
        );
        $result = $this->find('count', array(
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 2,
                'AccountFolder.active' => 1,
                'AccountFolder.created_by' => $user_id
            ),
            'fields' => $fields
        ));

        return $result;
    }

    function getHuddleName($account_folder_id) {
        $result = $this->find('first', array('conditions' => array('account_folder_id' => (int) $account_folder_id)));
        $account_folder_name = '';
        if ($result && count($result) > 0) {
            $account_folder_name = $result['AccountFolder']['name'];
        }
        return $account_folder_name;
    }

    function get($account_folder_id) {
        $result = $this->find('first', array('conditions' => array('account_folder_id' => (int) $account_folder_id)));
        if ($result && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getHuddleType($account_folder_id) {
        $result = $this->find('first', array('conditions' => array('account_folder_id' => (int) $account_folder_id)));
        $account_folder_type = '';
        if ($result && count($result) > 0) {
            $account_folder_type = $result['AccountFolder']['folder_type'];
        }
        return $account_folder_type;
    }

    /**
     * @param $account_id
     * @param $user_id
     * @param $doc_type    int    1 - Video, 2 - Doc
     * @param $limit
     * @param $page
     * @param $term
     * @return array|bool
     */
    function getAllMyFilesCount($account_id, $user_id, $doc_type, $limit = 0, $page = 0, $term = '') {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        $cond = '';
        if ($term != '') {
            $cond = "afd.title LIKE '%$term%'";
        }

        $doc_type_where = '';

        if (!empty($doc_type)) {

            $doc_type_where = " doc.doc_type IN($doc_type) ";
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => 'User.id = AccountFolder.created_by'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'left',
                'conditions' => 'afd.account_folder_id=AccountFolder.account_folder_id',
            ),
            array(
                'table' => 'documents as doc',
                'type' => 'left',
                'conditions' => 'afd.document_id=doc.id'
            )
        );

        $fields = array(
            'AccountFolder.*',
            'afd.*',
            'doc.*',
            'User.username',
            'User.first_name',
            'User.last_name',
            'User.email',
            'User.image',
        );

        $params = array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.created_by' => $user_id,
                'AccountFolder.folder_type' => 3,
                'AccountFolder.active' => 1,
                $doc_type_where,
                $cond
            ),
            'order' => 'AccountFolder.created_date DESC',
            'fields' => $fields
        );

        if (0 < (int) $limit) {
            $params['limit'] = (int) $limit;
            $params['offset'] = (int) $page * (int) $limit;
        }

        $result = $this->find('count', $params);

        return $result;
    }

    /**
     * @param $account_id
     * @param $user_id
     * @param $doc_type    int    1 - Video, 2 - Doc
     * @param $limit
     * @param $page
     * @param $term
     * @return array|bool
     */
    function getAllMyFiles($account_id, $user_id, $doc_type, $limit = 0, $page = 0, $term = '') {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        $cond = '';
        if ($term != '') {
            $cond = "afd.title LIKE '%$term%'";
        }

        $doc_type_where = '';

        if (!empty($doc_type)) {

            $doc_type_where = " doc.doc_type IN($doc_type) ";
        }

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => 'User.id = AccountFolder.created_by'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'left',
                'conditions' => 'afd.account_folder_id=AccountFolder.account_folder_id',
            ),
            array(
                'table' => 'documents as doc',
                'type' => 'left',
                'conditions' => 'afd.document_id=doc.id'
            )
        );

        $fields = array(
            'AccountFolder.*',
            'afd.*',
            'doc.*',
            'User.username',
            'User.first_name',
            'User.last_name',
            'User.email',
            'User.image',
        );

        $params = array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.created_by' => $user_id,
                'AccountFolder.folder_type' => 3,
                'AccountFolder.active' => 1,
                $doc_type_where,
                $cond
            ),
            'order' => 'AccountFolder.created_date DESC',
            'fields' => $fields
        );

        if (0 < (int) $limit) {
            $params['limit'] = (int) $limit;
            $params['offset'] = (int) $page * (int) $limit;
        }

        $result = $this->find('all', $params);

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getAllMyFile($account_id, $user_id, $account_folder_id) {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        $account_folder_id = (int) $account_folder_id;

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => 'User.id = AccountFolder.created_by'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'left',
                'conditions' => 'afd.account_folder_id=AccountFolder.account_folder_id',
            ),
            array(
                'table' => 'documents as doc',
                'type' => 'left',
                'conditions' => 'afd.document_id=doc.id'
            )
        );

        $fields = array(
            'AccountFolder.*',
            'afd.*',
            'doc.*',
            'User.username',
            'User.first_name',
            'User.last_name',
            'User.email',
            'User.image',
        );
        $result = $this->find('first', array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_folder_id' => $account_folder_id,
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.created_by' => $user_id,
                'AccountFolder.folder_type' => 3,
                'AccountFolder.active' => 1,
                " doc.doc_type IN (1,3) "
            ),
            'order' => 'AccountFolder.created_date DESC',
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getAllMyFileWithDocId($account_id, $user_id, $document_id) {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        $document_id = (int) $document_id;

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => 'User.id = AccountFolder.created_by'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'left',
                'conditions' => 'afd.account_folder_id=AccountFolder.account_folder_id',
            ),
            array(
                'table' => 'documents as doc',
                'type' => 'left',
                'conditions' => 'afd.document_id=doc.id'
            )
        );

        $fields = array(
            'AccountFolder.*',
            'afd.*',
            'doc.*',
            'User.username',
            'User.first_name',
            'User.last_name',
            'User.email',
            'User.image',
        );
        $result = $this->find('first', array(
            'joins' => $joins,
            'conditions' => array(
                'doc.id' => $document_id,
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.created_by' => $user_id,
                'AccountFolder.folder_type' => 3,
                'AccountFolder.active' => 1,
                'doc.doc_type' => 1
            ),
            'order' => 'AccountFolder.created_date DESC',
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function getAllMyFileWithDocId2($account_id, $user_id, $document_id) {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;
        $document_id = (int) $document_id;
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => 'User.id = AccountFolder.created_by'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'left',
                'conditions' => 'afd.account_folder_id=AccountFolder.account_folder_id',
            ),
            array(
                'table' => 'documents as doc',
                'type' => 'left',
                'conditions' => 'afd.document_id=doc.id'
            )
        );

        $fields = array(
            'AccountFolder.*',
            'afd.*',
            'doc.*',
            'User.username',
            'User.first_name',
            'User.last_name',
            'User.email',
            'User.image',
        );
        $result = $this->find('first', array(
            'joins' => $joins,
            'conditions' => array(
                'afd.id' => $document_id,
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.created_by' => $user_id,
                'AccountFolder.folder_type' => 3,
                'AccountFolder.active' => 1,
                'doc.doc_type' => 1
            ),
            'order' => 'AccountFolder.created_date DESC',
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function findById($id) {
        $result = $this->find('first', array('conditions' => array('account_folder_id' => (int) $id)));
        if ($result && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getUserHuddles($user_id, $account_id) {
        $user_id = (int) $user_id;
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'inner',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('User.id=huddle_users.user_id')
            )
        );
        $fields = array(
            'AccountFolder.*',
            'User.id'
        );

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array('huddle_users.user_id' => $user_id, 'AccountFolder.account_id' => $account_id),
            'fields' => $fields
        ));

        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function gethuddlessbyname($account_id, $name) {
        $results = $this->find('all', array(
            'conditions' => array('name like' => "%$name%", 'account_id' => $account_id, "folder_type" => '1'),
        ));
        return $results;
    }

    function getLastQuery() {
        $dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        return $lastLog['query'];
    }

    public function getfolderHuddles($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=huddle_users.user_id')
            ),
//            array(
//                'table' => 'account_folders_meta_data as AFMD',
//                'type' => 'left',
//                'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
//            )
        );
        $fields = array(
            'User.*',
//            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            "(SELECT
  COUNT(*) AS `count`
FROM
  `documents` AS `Document`
  INNER JOIN `users` AS `User`
    ON (
      `Document`.`created_by` = `User`.`id`
    )
  INNER JOIN `account_folder_documents` AS `afd`
    ON (
      `Document`.`id` = `afd`.`document_id`
    )
  INNER JOIN `documents` AS `doc`
    ON (`afd`.`document_id` = `doc`.`id`)
WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
  AND `Document`.`doc_type` = 1
  AND `Document`.`active` = '1' )  AS v_total ",
            "(SELECT
  COUNT(*) AS `count`
FROM
  `documents` AS `Document`
  INNER JOIN `users` AS `User`
    ON (
      `Document`.`created_by` = `User`.`id`
    )
  INNER JOIN `account_folder_documents` AS `afd`
    ON (
      `Document`.`id` = `afd`.`document_id`
    )
  INNER JOIN `documents` AS `doc`
    ON (`afd`.`document_id` = `doc`.`id`)
WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
  AND `Document`.`doc_type` = 2
  AND `Document`.`active` = '1' )  AS doc_total ",
            "(SELECT
                CASE
                     WHEN meta_data_value  = '2' THEN 'coaching'
                     WHEN meta_data_value = '3' THEN 'evaluation'
                     ELSE NULL
                 END
                FROM
                  account_folders_meta_data
                WHERE account_folder_id = AccountFolder.`account_folder_id`
                  AND meta_data_name = 'folder_type'
                  AND (meta_data_value = 3 OR meta_data_value = 2 ))AS folderType",
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1,5)',
                'AccountFolder.active' => 1,
                'AccountFolder.parent_folder_id' => $folder_id,
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));

        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'evaluation') {

                    if ($result[$i][0]['folderType'] == 'evaluation') {

                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }
            $result = $output;

            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function searchinfolder($folder_id, $account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $title = '') {
        $account_id = (int) $account_id;

        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=huddle_users.user_id')
            )
        );
        $fields = array(
            'User.*',
            //'(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            "(SELECT
                CASE
                     WHEN meta_data_value  = '2' THEN 'coaching'
                     WHEN meta_data_value = '3' THEN 'evaluation'
                     ELSE NULL
                 END
                FROM
                  account_folders_meta_data
                WHERE account_folder_id = AccountFolder.`account_folder_id`
                  AND meta_data_name = 'folder_type'
                  AND (meta_data_value = 3 OR meta_data_value = 2 ))AS folderType",
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        $where_in .= '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false)
                $where_in .= " OR ";
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }

        if ($account_folder_ids == false && $accountFolderGroupsIds == false) {
            $where_in .=' AccountFolder.account_folder_id IN(-1)  ';
        }
        $where_in .= ')';


        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';
        if ($folder_id == 0)
            $in_folder = 'AccountFolder.parent_folder_id IS NULL';
        else
            $in_folder = 'AccountFolder.parent_folder_id = ' . $folder_id;


        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1,5)',
                'AccountFolder.active' => 1,
                $in_folder,
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));


        if (is_array($result) && count($result) > 0) {

            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }

            $output = array();

            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'evaluation') {
                    if ($result[$i][0]['folderType'] == 'evaluation') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '' && $result[$i]['AccountFolder']['folder_type'] != 5) {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;

            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];

                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);

                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function searchinfolder_textbox($folder_id, $account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $title = '') {
        $account_id = (int) $account_id;

        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=huddle_users.user_id')
            )
        );
        $fields = array(
            'User.*',
//            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            "(SELECT
                CASE
                     WHEN meta_data_value  = '2' THEN 'coaching'
                     WHEN meta_data_value = '3' THEN 'evaluation'
                     ELSE NULL
                 END
                FROM
                  account_folders_meta_data
                WHERE account_folder_id = AccountFolder.`account_folder_id`
                  AND meta_data_name = 'folder_type'
                  AND (meta_data_value = 3 OR meta_data_value = 2 ))AS folderType",
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        $where_in .= '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false)
                $where_in .= " OR ";
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }

        if ($account_folder_ids == false && $accountFolderGroupsIds == false) {
            $where_in .=' AccountFolder.account_folder_id IN(-1)  ';
        }
        $where_in .= ')';


        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';
        if ($folder_id == 0)
            $in_folder = 'AccountFolder.parent_folder_id IS NULL';
        else
            $in_folder = 'AccountFolder.parent_folder_id = ' . $folder_id;


        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1,5)',
                'AccountFolder.active' => 1,
                //    $in_folder,
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));


        if (is_array($result) && count($result) > 0) {

            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }

            $output = array();

            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '' && $result[$i]['AccountFolder']['folder_type'] != 5) {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;

            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];

                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);

                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getotherfolders($account_folder_id, $created_by) {
        $condtions = array(
            "AccountFolder.folder_type" => "5",
            "afu.user_id" => $created_by,
                //"AccountFolder.account_folder_id NOT IN(".$account_folder_id.")",
        );
        $joins = array(
            array(
                'table' => 'account_folder_users as afu',
                'type' => 'left',
                'conditions' => 'AccountFolder.account_folder_id = afu.account_folder_id'
            )
        );
        $data = array(
            "conditions" => $condtions,
            "joins" => $joins
        );
        $other_folders = $this->find("all", $data);
        return $other_folders;
    }

    public function getotherfoldersChild($account_folder_id, $created_by) {
        $condtions = array(
            "AccountFolder.folder_type" => "5",
            "afu.user_id" => $created_by,
            "AccountFolder.parent_folder_id" => $account_folder_id,
        );
        $joins = array(
            array(
                'table' => 'account_folder_users as afu',
                'type' => 'left',
                'conditions' => 'AccountFolder.account_folder_id = afu.account_folder_id'
            )
        );
        $data = array(
            "conditions" => $condtions,
            "joins" => $joins
        );
        $other_folders = $this->find("all", $data);
        return $other_folders;
    }

    public function save_huddle_to_new_folder($huddles, $new_folder_id) {
        if ($huddles) {
            //foreach($huddles as $huddle){
            $this->updateall(array(
                "parent_folder_id" => $new_folder_id
                    ), array(
                "account_folder_id" => $huddles
            ));
            //}
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function copyAllHuddles($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=huddle_users.user_id')
            ),
            array(
                'table' => 'account_folders_meta_data as AFMD',
                'type' => 'left',
                'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id AND meta_data_name = "folder_type"')
            )
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }
        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active' => 1,
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            //echo '<pre>';
            $output = array();
            //$result = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;
            //print_r($result);
            //die;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function copyAllHuddlesList($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $limit, $page, $title = false) {

        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=huddle_users.user_id')
            ),
            array(
                'table' => 'account_folders_meta_data as AFMD',
                'type' => 'left',
                'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
            )
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }
        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }
        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active' => 1,
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
            'limit' => (int) $limit,
            'offset' => (int) $page * (int) $limit,
        ));
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            //echo '<pre>';
            $output = array();
            //$result = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;
            //print_r($result);
            //die;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getFolderVideos($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $account_folder_videos = array(
            array(
                'table' => 'account_folder_documents',
                'alias' => 'afd',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = afd.account_folder_id')
            ),
            array(
                'table' => 'documents as d',
                'type' => 'left',
                'conditions' => array('afd.document_id=d.id')
            )
        );
        $results = $this->find('all', array(
            'joins' => $account_folder_videos,
            'conditions' => array(
                'AccountFolder.parent_folder_id' => $account_folder_id,
                'AccountFolder.folder_type' => 1,
                'd.doc_type' => '1',
                'd.published' => '1'
            ),
            'fields' => array('d.*'),
            'order' => array('d.id' => 'DESC'),
            'limit' => 3
        ));

        return $results;
    }

    public function getAllHuddlesCount($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2 LIMIT 1) AS folderType',
            '(SELECT "evaluation" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 3 LIMIT 1) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1,
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));

        if (is_array($result) && count($result) > 0) {

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountFoldersSearch($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $title = "", $folder_path = '/root/', $bool = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2 LIMIT 1) AS folderType',
            '(SELECT "evaluation" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 3 LIMIT 1) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            "concat('$folder_path', AccountFolder.name) as SibmePath"
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folder_type DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folder_type ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        //for folders we only care about current structure in account and parent
        $where_in = '';

        if ($folder_id != false) {
            $where_in .= " AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            $where_in .= " AccountFolder.parent_folder_id IS NULL ";
        }

        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }
        if ($bool) {
            if (!empty($title)) {
                $where = " AccountFolder.parent_folder_id IS NULL ";
                $where .=' AND ';
                $where .=' (AccountFolder.name like ' . "'%$title%'";
                $where .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
                $where .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
            } else {
                $where = " AccountFolder.parent_folder_id IS NULL ";
            }

            $result = $this->find('all', array(
                //  'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 5,
                    'AccountFolder.active' => 1,
                    $where
                ),
                //  'fields' => $fields,
                'order' => array($order_by),
                    //   'group' => $groups,
            ));
        } else {
            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 5,
                    'AccountFolder.active' => 1,
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        }
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    $output[] = $result[$i];
                } elseif (!empty($sort) && $sort == 'evaluation') {
                    if ($result[$i][0]['folderType'] == 'evaluation') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }

            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountFoldersSearch_textbox($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $title = "", $folder_path = '/root/', $bool = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            "concat('$folder_path', AccountFolder.name) as SibmePath"
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folder_type DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folder_type ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        //for folders we only care about current structure in account and parent
        $where_in = '';

        //   if($folder_id !=false){
        //         $where_in .= " AccountFolder.parent_folder_id = '$folder_id' ";
        //     }else{
        //       $where_in .= " AccountFolder.parent_folder_id IS NULL ";
        //    }

        if (!empty($title)) {
            //       $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }
        if ($bool) {
            if (!empty($title)) {
                $where = " AccountFolder.parent_folder_id IS NULL ";
                $where .=' AND ';
                $where .=' (AccountFolder.name like ' . "'%$title%'";
                $where .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
                $where .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
            } else {
                $where = " AccountFolder.parent_folder_id IS NULL ";
            }

            $result = $this->find('all', array(
                //  'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 5,
                    'AccountFolder.active' => 1,
                    $where
                ),
                //  'fields' => $fields,
                'order' => array($order_by),
                    //   'group' => $groups,
            ));
        } else {
            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 5,
                    'AccountFolder.active' => 1,
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
            ));
        }
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    $output[] = $result[$i];
                } else {
                    $output[] = $result[$i];
                }
            }
            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountFoldersSearch_archive($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $title = "", $folder_path = '/root/') {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            "concat('$folder_path', AccountFolder.name) as SibmePath"
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folder_type DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folder_type ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        //for folders we only care about current structure in account and parent
        $where_in = '';

        if ($folder_id != false) {
            $where_in .= " AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            // $where_in .= " AccountFolder.parent_folder_id IS NULL ";
        }

        if (!empty($title)) {
            $where_in .=' AND ';
            $where_in .=' (AccountFolder.name like ' . "'%$title%'";
            $where_in .= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where_in .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        }
        if (!empty($title)) {
            $where = " AccountFolder.parent_folder_id IS NULL ";
            $where .=' AND ';
            $where.=' (AccountFolder.name like ' . "'%$title%'";
            $where.= " OR AccountFolder.account_folder_id IN (
                    select afd.account_folder_id from account_folder_documents afd
                    WHERE afd.title like '%$title%' ) ";
            $where .=" OR AccountFolder.account_folder_id IN (select afd.account_folder_id
                    from comments as comment join documents as doc on doc.id = comment.ref_id
                    join account_folder_documents afd on afd.document_id=doc.id
                    WHERE comment.comment like '%" . $title . "%') ) ";
        } else {
            $where = " AccountFolder.parent_folder_id IS NULL ";
        }

        $result = $this->find('all', array(
            // 'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 5,
                'AccountFolder.active' => 0,
                'AccountFolder.archive' => 1,
                $where
            ),
            //   'fields' => $fields,
            'order' => array($order_by),
                //   'group' => $groups,
        ));
//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    $output[] = $result[$i];
                } else {
                    $output[] = $result[$i];
                }
            }
            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getfolderHuddlesCount($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )/* ,
                  array(
                  'table' => 'account_folders_meta_data as AFMD',
                  'type' => 'left',
                  'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id')
                  ) */
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1,
                'AccountFolder.parent_folder_id' => $folder_id,
                $where_in
            ),
            'fields' => $fields,
            'order' => array($order_by),
            'group' => $groups,
        ));

        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }
            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountHuddlesEdtpa($account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $bool = 0, $limit = 0, $page = 0) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );

        $fields = array(
            'User.*',
            //'(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            "(SELECT
                CASE
                     WHEN meta_data_value  = '2' THEN 'coaching'
                     WHEN meta_data_value = '3' THEN 'evaluation'
                     ELSE NULL
                 END
                FROM
                  account_folders_meta_data
                WHERE account_folder_id = AccountFolder.`account_folder_id`
                  AND meta_data_name = 'folder_type'
                  AND (meta_data_value = 3 OR meta_data_value = 2 ) ORDER BY created_date DESC LIMIT 1)AS folderType",
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'evaluation') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if ($folder_id != false) {
            $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            if (!$bool) {
                $where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
            }
        }
        if ($bool) {
            if (!empty($sort) && $sort == 'name') {
                $order_by = 'AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'date') {
                $order_by = 'AccountFolder.created_date DESC';
            } elseif (!empty($sort) && $sort == 'coaching') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'collaboration') {
                $order_by = 'folder_type ASC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'evaluation') {
                $order_by = 'folder_type DESC,AccountFolder.name ASC';
            } elseif (!empty($sort) && $sort == 'folder_type') {
                $order_by = 'folder_type DESC';
            } else {
                $order_by = 'AccountFolder.created_date DESC';
            }
            $params = array(
                //  'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                //    $where_in
                ),
                //    'fields' => $fields,
                'order' => array($order_by),
                    //  'group' => $groups,
            );

            if (0 < (int) $limit) {
                $params['limit'] = (int) $limit;
                $params['offset'] = (int) $page * (int) $limit;
            }
            $result = $this->find('all', $params);
        } else {
            if (0 < (int) $limit) {
                $params['limit'] = (int) $limit;
                $params['offset'] = (int) $page * (int) $limit;
            }

            $result = $this->find('all', array(
                'joins' => $account_folder_users,
                'conditions' => array(
                    'AccountFolder.account_id' => $account_id,
                    'AccountFolder.folder_type' => 1,
                    'AccountFolder.active' => 1,
//                'AccountFolder.parent_folder_id IS NULL',
                    $where_in
                ),
                'fields' => $fields,
                'order' => array($order_by),
                'group' => $groups,
                'limit' => (int) $limit,
                'offset' => (int) $page * (int) $limit
            ));
        }
//       echo $this->getLastQuery();
//        exit;

        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $this->site_id . '"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }

            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching' && $result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'evaluation') {

                    if ($result[$i][0]['folderType'] == 'evaluation') {

                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
            }

            $result = $output;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['AccountFolder']['created_date']);
                    $updated_at = new DateTime($item['AccountFolder']['last_edit_date']);
                    $item['AccountFolder']['created_at'] = $created_at->format(DateTime::W3C);
                    $item['AccountFolder']['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['AccountFolder']['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item['AccountFolder'];
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllAccountFolderForAnalytics($account_id, $sort = '', $user_id) {
        $account_id = (int) $account_id;
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = huddle_users.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('huddle_users.user_id = User.id')
            )
        );

        $fields = array(
            'User.*',
            //'(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            "(SELECT
                CASE
                     WHEN meta_data_value  = '2' THEN 'coaching'
                     WHEN meta_data_value = '3' THEN 'evaluation'
                     ELSE NULL
                 END
                FROM
                  account_folders_meta_data
                WHERE account_folder_id = AccountFolder.`account_folder_id`
                  AND meta_data_name = 'folder_type'
                  AND (meta_data_value = 3 OR meta_data_value = 2 ))AS folderType",
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
                //'AFMD.meta_data_value'
        );


        $groups = array('AccountFolder.account_folder_id');

        $result = $this->find('all', array(
            'joins' => $account_folder_users,
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'huddle_users.user_id' => $user_id,
                'AccountFolder.active' => 1,
            ),
            'fields' => $fields,
            'group' => $groups,
        ));


        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = $this->query('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['AccountFolder']['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id = "'.$this->site_id.'"');
                $result[$i]['AccountFolder']['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }

            $output = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] != 'coaching' && $result[$i][0]['folderType'] != 'evaluation') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'evaluation') {
                    if ($result[$i][0]['folderType'] == 'evaluation') {

                        $output[] = $result[$i];
                    }
                }
            }
            return $output;
        } else {
            return FALSE;
        }
    }

}
