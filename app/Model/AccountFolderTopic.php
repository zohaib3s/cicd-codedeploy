<?php

// app/Model/User.php
class AccountFolderTopic extends AppModel {

    var $useTable = 'account_folder_topics';

    public function get($account_folder_id){
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'topics as Topic',
                'type' => 'left',
                'conditions' => array('AccountFolderTopic.topic_id=Topic.id')
            )
        );
        $fields = array('Topic.*');
        $result = $this->find('all',array(
            'joins' => $joins,
            'conditions' => array('AccountFolderTopic.account_folder_id'=>$account_folder_id),
            'fields' => $fields
        ));
        return $result;
    }
}

