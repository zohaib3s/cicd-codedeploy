<?php

// app/Model/User.php
class AccountFolderGroup extends AppModel {

    var $useTable = 'account_folder_groups';
    var $primaryKey = 'account_folder_group_id';

    

    function getHuddleGroups($account_folder_id, $site_id) {
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            'table' => 'user_groups',
            'type' => 'inner',
            'conditions' => array('user_groups.group_id=AccountFolderGroup.group_id')
        );
        $fields = array('user_groups.user_id', 'AccountFolderGroup.*');
        return $this->find('all', array(
            'joins' => array($joins),
            'conditions' => array('AccountFolderGroup.account_folder_id' => $account_folder_id,
                'AccountFolderGroup.site_id' => $site_id),
            'fields' => $fields,
        ));
    }

}

