<?php

// app/Model/User.php
class AccountFolderDocument extends AppModel {

    var $useTable = 'account_folder_documents';

    

    function get($document_id) {
        $result = $this->find('first', array('conditions' => array('document_id' => (int)$document_id)));
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function get_doc_details($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'documents as doc',
                'type' => 'left',
                'conditions' => array('doc.id= AccountFolderDocument.document_id')
            ),
        );
        $fields = array('AccountFolderDocument.*', 'doc.published', 'doc.encoder_provider');
        return $this->find('first', array(
            'joins' => $joins,
            'conditions' => array('AccountFolderDocument.account_folder_id' => $account_folder_id),
            'fields' => $fields
        ));
    }

function ob_get_doc_details($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;
        $joins = array(
            array(
                'table' => 'documents as doc',
                'type' => 'left',
                'conditions' => array('doc.id= AccountFolderDocument.document_id')
            ),
        );
        $fields = array('AccountFolderDocument.*', 'doc.published', 'doc.encoder_provider');
        return $this->find('first', array(
            'joins' => $joins,
            'conditions' => array('AccountFolderDocument.account_folder_id' => $account_folder_id,'doc.doc_type'=>'1'),
            'fields' => $fields
        ));
    } 
    function get_document_name($ref_id) {
        $ref_id = (int) $ref_id;
        $result = $this->find('first', array('conditions' => array('document_id' => $ref_id), 'fields' => array('title')));
        if (count($result) > 0) {
            return $result['AccountFolderDocument']['title'];
        } else {
            return false;
        }
    }
    function get_account_folder_document($ref_id) {
        $ref_id = (int) $ref_id;
        $result = $this->find('first', array('conditions' => array('document_id' => $ref_id), 'fields' => array('title','account_folder_id')));
        if (count($result) > 0) {
            return $result;
        } else {
            return false;
        }
    }

    function get_row($conditions) {
        $result = $this->find('all', array('conditions' => array($conditions)));
        if (count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function save($data = null, $validate = false, $fieldList = array()) {
        parent::save($data, $validate, $fieldList);
        return $this->id;
    }

}
