



<?php
use Aws\S3\S3Client;
App::uses('AppController', 'Controller');

class ArchiveController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $uses = array(
        'Account',
        'AccountTag',
        'UsecountFolderUser',
        'AcrsAccount',
        'AccountFolder',
        'AccountFolderUser',
        'AccountFolderMetaData',
        'AccountFolderGroup',
        'UserActivityLog',
        'AccountMetaDatum',
        'User',
        'Group',
        'JobQueue',
        'DocumentFiles',
        'Document',
        'UsersAccount',
        'MailchimpTemplate',
        'AccountsMailchimpCampaign',
        'AccountBraintreeSubscription',
        'AuditEmail',
        'AnalyticsReportLog'
    );

    /**
     * index method
     *
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(
               'archive_check_for_amazon'
        );
    }
    
    
function archive_check_for_amazon()
{
$arr = [
    'version'     => 'latest',
    'region'      => 'us-east-1',
    'credentials' => [
        'key'    => 'AKIAJ4ZWDR5X5JKB7CZQ',
        'secret' => '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',
    ],
    'http' => [
        'verify' => false
    ]
];
$s3Client = S3Client::factory($arr);
//print_r($s3newClient); die;
$result = $s3Client->listBuckets();
//echo '<pre>'; print_r($s3Client->listObjects()); die;

   $newarr = array(
    'Bucket' => 'sibme-development',
    'Prefix' => 'uploads/',
    'Delimiter' => "/",
);
//$objects = $s3Client->getListObjectsIterator();

$objects = $s3Client->listObjects($newarr);

//print_r($objects); die;

$accounts = array();


foreach ($objects['CommonPrefixes'] as $object) {
    echo '<pre>'; print_r($object['Prefix']); 
    
    $accounts[] = $object['Prefix'];
}


$account_huddles = array();

foreach($accounts as $account)
{
      $newarr = array(
    'Bucket' => 'sibme-development',
    'Prefix' => $account,
    'Delimiter' => "/",
); 
      
 $result = $this->Document->query("SELECT * from documents where url like '%".$account."%'");

//echo mysqli_num_rows($result);
if (count($result) > 0) {
   
} else {
   $xml = "<ArchiveExFolder><path>".$account."</path></ArchiveExFolder>";
   $sql = $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '".date("Y-m-d H:i:s")."', NULL, NULL, '".$xml."', '1', '0', NULL)");
}      
      
$objects = $s3Client->listObjects($newarr);    


if(!empty($objects['CommonPrefixes']))
{
foreach ($objects['CommonPrefixes'] as $object) {
    echo '<pre>'; print_r($object['Prefix']); 
    
    $account_huddles[] = $object['Prefix'];
}
}
    
}

$with_years = array();

foreach($account_huddles as $account_huddle)
{
      $newarr = array(
    'Bucket' => 'sibme-development',
    'Prefix' => $account_huddle,
    'Delimiter' => "/",
); 
      

 $result = $this->Document->query("SELECT * from documents where url like '%".$account_huddle."%'");
//echo mysqli_num_rows($result);
if (count($result) > 0) {
   
} else {
   $xml = "<ArchiveExFolder><path>".$account_huddle."</path></ArchiveExFolder>";
   $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '".date("Y-m-d H:i:s")."', NULL, NULL, '".$xml."', '1', '0', NULL)");
}       
      
$objects = $s3Client->listObjects($newarr);    


if(!empty($objects['CommonPrefixes']))
{
foreach ($objects['CommonPrefixes'] as $object) {
    echo '<pre>'; print_r($object['Prefix']); 
    
    $with_years[] = $object['Prefix'];
}
}
    
}

$with_months = array();

foreach($with_years as $with_year)
{
      $newarr = array(
    'Bucket' => 'sibme-development',
    'Prefix' => $with_year,
    'Delimiter' => "/",
); 
      
 $result = $this->Document->query("SELECT * from documents where url like '%".$with_year."%'");
//echo mysqli_num_rows($result);
if (count($result) > 0) {
   
} else {
   $xml = "<ArchiveExFolder><path>".$with_year."</path></ArchiveExFolder>";
   $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '".date("Y-m-d H:i:s")."', NULL, NULL, '".$xml."', '1', '0', NULL)");
}        
      
      
$objects = $s3Client->listObjects($newarr);    


if(!empty($objects['CommonPrefixes']))
{
foreach ($objects['CommonPrefixes'] as $object) {
    echo '<pre>'; print_r($object['Prefix']); 
    
    $with_months[] = $object['Prefix'];
}
}
    
}



$with_dates = array();

foreach($with_months as $with_month)
{
      $newarr = array(
    'Bucket' => 'sibme-development',
    'Prefix' => $with_month,
    'Delimiter' => "/",
); 
      
 $result = $this->Document->query("SELECT * from documents where url like '%".$with_month."%'");
//echo mysqli_num_rows($result);
if (count($result) > 0) {
   
} else {
   $xml = "<ArchiveExFolder><path>".$with_month."</path></ArchiveExFolder>";
   $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '".date("Y-m-d H:i:s")."', NULL, NULL, '".$xml."', '1', '0', NULL)");
}      
      
$objects = $s3Client->listObjects($newarr);    


if(!empty($objects['CommonPrefixes']))
{
foreach ($objects['CommonPrefixes'] as $object) {
    echo '<pre>'; print_r($object['Prefix']); 
    
    $with_dates[] = $object['Prefix'];
}
}
    
}


foreach($with_dates as $with_date)
{
   
$result = $this->Document->query("SELECT * from documents where url like '%".$with_date."%'");
//echo mysqli_num_rows($result);
if (count($result) > 0) {
   
} else {
   $xml = "<ArchiveExFolder><path>".$with_date."</path></ArchiveExFolder>";
   $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '".date("Y-m-d H:i:s")."', NULL, NULL, '".$xml."', '1', '0', NULL)");
}      
      
    
}


return true;

    }
    
    
    function test()
    {
        return true;
    }
    
    

    
    
    
}







