<?php

class PermissionsController extends AppController {

    public $uses = array(
        'User', 'AccountFolder', 'Group', 'UserGroup', 'AccountFolderMetaData', 'AccountFolderUser', 'AccountFolderGroup',
        'UserAccount', 'Document', 'Comment', 'AccountFolderDocument', 'AccountFolderDocumentAttachment', 'CommentUser',
        'CommentAttachment', 'Observations', 'Frameworks', 'AccountTag', 'AccountCommentTag'
    );

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    function beforeFilter() {

        $users = $this->Session->read('user_current_account');
        $totalAccounts = $this->Session->read('totalAccounts');
        $this->set('logged_in_user', $users);
        $this->user_id = $users['User']['id'];

        parent::beforeFilter();
        $this->auth_permission('permissions', 'all');

        if ($users['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
            $this->redirect_on_account_settings();
        }
        $this->check_is_account_expired();
        $this->is_account_activated($users['users_accounts']['account_id'], $users['User']['id']);
    }

    function assign_user($account_id, $user_id) {

        $permissions_set = $this->set_permissions($user_id);
        $current_user = $this->Session->read('user_current_account');
        
        

        if ($current_user['accounts']['account_id'] != $account_id) {
            $this->Session->setFlash($this->language_based_messages['you_dont_have_access_to_this_account_msg'], 'default', array('class' => 'message success'));
            return $this->redirect('/Dashboard/home');
        }
        
        $link_user_role_detail = $this->UserAccount->find('first', array('conditions' => array('user_id' => $user_id , 'account_id' => $account_id )));
        
        $link_user_role = $link_user_role_detail['UserAccount']['role_id'];
        $current_user_role = $current_user['users_accounts']['role_id'];
        
        if($current_user_role > $link_user_role )
        {
            $this->Session->setFlash($this->language_based_messages['you_dont_have_access_to_this_users_permission_msg'], 'default', array('class' => 'message error'));
            return $this->redirect('/Dashboard/home');
            
        }

        return $this->redirect("/home/people/home/people/permission/$account_id/$user_id/".$current_user_role, 301);

        $this->set('users', $this->UserAccount->get($account_id, $user_id));
        $huddles = $this->AccountFolder->get_all_huddle_users($account_id);

        $huddle_data = array();
        if (is_array($huddles) && count($huddles) > 0) {
            foreach ($huddles as $huddle) {
                $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle['AccountFolder']['account_folder_id'], 'meta_data_name' => 'folder_type'), 'fields' => array('meta_data_value')));

                if (empty($h_type)) {
                    $h_type['AccountFolderMetaData'] = array("meta_data_value" => 1);
                }
                $huddle_data[] = array_merge($huddle, $h_type);
            }
        }

        $this->set('huddles', $huddle_data);
        $this->set('huddle_user_folder', $this->get_user_account_folder($user_id));
        $this->set('huddleRoles', $this->AccountFolderUser->get_all_account_folder_user($user_id));
        $this->set('associate_huddle_users', $this->_get_associated_huddles_users($user_id));
        $this->set('account_folder_user', $this->AccountFolderUser->get_all_account_folder_user($user_id));
        $this->set('permission_set', $permissions_set);
        $this->set('permotion_set', $this->set_promotions($user_id));
        $this->set('current_user', $current_user);
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('users/administrators_groups');
        $this->set('language_based_content', $language_based_content);
        
        $this->set('referer', $this->referer('/', true));
        $this->render('home');
    }

    function ajax_filter($account_id, $user_id) {
        $permissions_set = $this->set_permissions($user_id);
        $current_user = $this->Session->read('user_current_account');

        $keywords = $this->request->data['keywords'];

        $params = array(
            'account_folder_user' => $this->AccountFolderUser->get_all_account_folder_user($user_id),
            'huddle_user_folder' => $this->get_user_account_folder($user_id),
            'associate_huddle_users' => $this->_get_associated_huddles_users($user_id),
            'users' => $this->UserAccount->get($account_id, $user_id),
            'huddles' => $this->AccountFolder->get_all_huddle_users_ajax($account_id, $keywords),
            'huddleRoles' => $this->AccountFolderUser->get_all_account_folder_user($user_id),
            'permission_set' => $permissions_set,
            'permotion_set' => $this->set_promotions($user_id),
            'current_user' => $current_user
        );


        $view = new View($this, FALSE);
        $html = $view->element('huddles/ajax/ajax_huddle_filter', $params);
        echo $html;
        die();
    }

    function record_sort($records, $field1, $field2, $reverse = false) {
        $hash = array();

        foreach ($records as $record) {
            if (isset($record[$field1]) && $record[$field1] != '') {
                $hash[$record[$field1]] = $record;
            } else {
                $hash[$record[$field2]] = $record;
            }
        }
        ($reverse) ? krsort($hash) : ksort($hash);
        $records = array();
        foreach ($hash as $record) {
            $records [] = $record;
        }
        return $records;
    }

    function get_user_account_folder($user_id) {
        $account_folder_ids = '';
        $result = $this->AccountFolderUser->get_all_account_folder_user($user_id);
        if ($result && count($result) > 0) {
            foreach ($result as $row) {
                $account_folder_ids[] = $row['AccountFolderUser']['account_folder_id'];
            }
            return $account_folder_ids;
        }
        return FALSE;
    }

    function _get_associated_huddles_users($user_id) {
        $result = $this->AccountFolderUser->get($user_id);
        if ($result && count($result) > 0) {
            $associated_users = array();
            foreach ($result as $row) {
                $associated_users[] = $row['AccountFolderUser']['account_folder_id'];
            }
            return $associated_users;
        }
    }

    function change_permission($account_id, $user_id) {

        if ($this->request->is('post')) {
            
             App::import('Vendor', 'MailChimp');
            $MailChimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
            $account_info = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
            $role = $this->request->data['role'];
            $user_id = $this->request->data['user_id'];
            
            $user_data = $this->User->find('first', array('conditions' => array('id' => $user_id)));
            $user_account_data = $this->UserAccount->find('first', array('conditions' => array('user_id' => $user_id , 'account_id' => $account_id )));
            $email_id = $user_data['User']['email'];
            $old_role = $user_account_data['UserAccount']['role_id'];
            $mailchimp_list_id = $account_info['Account']['mailchimp_list_id'];
            if(!empty($mailchimp_list_id))
            {
            
            
            $segment_details = $MailChimp->get("lists/$mailchimp_list_id/segments");
            $new_segment_id = '';
            $old_segment_id = '';
           
            foreach($segment_details['segments'] as $row)
            {
                if($role == '110' && $row['name'] == 'Super Admins')
                {
                  $new_segment_id =   $row['id'];
                    
                }
                
                if($old_role == '110' && $row['name'] == 'Super Admins')
                {
                  $old_segment_id =   $row['id'];
                    
                }
                
                if($role == '115' && $row['name'] == 'Admins')
                {
                  $new_segment_id =   $row['id'];
                    
                }
                
                if($old_role == '115' && $row['name'] == 'Admins')
                {
                  $old_segment_id =   $row['id'];
                    
                }
                
                if($role == '120' && $row['name'] == 'Users')
                {
                  $new_segment_id =   $row['id'];
                    
                }
                
                 if($old_role == '120' && $row['name'] == 'Users')
                {
                  $old_segment_id =   $row['id'];
                    
                }
                
                if($role == '125' && $row['name'] == 'Viewers')
                {
                  $new_segment_id =   $row['id'];
                    
                }
                
                if($old_role == '125' && $row['name'] == 'Viewers')
                {
                  $old_segment_id =   $row['id'];
                    
                }
                
                
            }
            $subscriber_hash = $MailChimp->subscriberHash($email_id);
            $MailChimp->delete("lists/$mailchimp_list_id/segments/$old_segment_id/members/$subscriber_hash");
            $MailChimp->post('lists/' . $mailchimp_list_id . '/segments/'.$new_segment_id.'/members', [
                                 //   "status" => "subscribed",
                                    "email_address" => $email_id,
                                ]);
            
            }

           

            $exUserAccount = $this->UserAccount->find('first', array('conditions' => array(
                    'user_id' => $user_id,
                    'account_id' => $account_id
            )));

            $data = array(
                'role_id' => "'" . $role . "'",
                'last_edit_date' => "'" . date("Y-m-d h:i:s") . "'"
            );
            if ($role == '110') {
                $data['permission_maintain_folders'] = true;
                $data['permission_access_video_library'] = true;
                $data['permission_video_library_upload'] = true;
                $data['permission_view_analytics'] = true;
                $data['permission_administrator_user_new_role'] = true;
                $data['parmission_access_my_workspace'] = true;
                $data['manage_collab_huddles'] = true;
                $data['manage_coach_huddles'] = true;
                $data['manage_evaluation_huddles'] = true;
                $data['folders_check'] = true;
            } elseif ($role == '115') {
                $data['permission_maintain_folders'] = true;
                $data['permission_access_video_library'] = true;
                $data['permission_video_library_upload'] = '0';
                $data['permission_view_analytics'] = '0';
                $data['permission_administrator_user_new_role'] = true;
                $data['parmission_access_my_workspace'] = true;
                $data['manage_collab_huddles'] = true;
                $data['manage_coach_huddles'] = true;
                $data['manage_evaluation_huddles'] = true;
                $data['folders_check'] = true;
            } elseif ($role == '120') {
                $data['permission_maintain_folders'] = '0';
                $data['permission_access_video_library'] = true;
                $data['permission_video_library_upload'] = '0';
                $data['permission_view_analytics'] = '0';
                $data['permission_administrator_user_new_role'] = '0';
                $data['parmission_access_my_workspace'] = TRUE;
                $data['manage_collab_huddles'] = '0';
                $data['manage_coach_huddles'] = '0';
                $data['folders_check'] = '0';
            } elseif ($role == '125') {
                $data['permission_maintain_folders'] = '0';
                $data['permission_access_video_library'] = true;
                $data['permission_video_library_upload'] = '0';
                $data['permission_view_analytics'] = '0';
                $data['permission_administrator_user_new_role'] = '0';
                $data['parmission_access_my_workspace'] = TRUE;
                $data['manage_collab_huddles'] = '0';
                $data['manage_coach_huddles'] = '0';
                $data['folders_check'] = '0';
                $this->UserGroup->deleteAll(array('user_id' => $user_id));
            }


            $this->UserAccount->updateAll($data, array(
                'user_id' => $user_id,
                'account_id' => $account_id
            ));
            if ($this->UserAccount->getAffectedRows() > 0) {
                if ($role == 125) {
                    $this->modify_account_folder($account_id, $user_id);
                }
                if ($exUserAccount['UserAccount']['role_id'] != $role) {
                    $this->Session->setFlash($this->language_based_messages['role_has_been_changed_successfully_msg'], 'default', array('class' => 'message success'));
                } else {
                    $this->Session->setFlash($this->language_based_messages['privileges_has_been_changed_msg'], 'default', array('class' => 'message success'));
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['privileges_has_not_changed_msg'], 'default', array('class' => 'message'));
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['please_select_correct_privileges_options_msg'], 'default', array('class' => 'message'));
        }
        $this->redirect('/permissions/assign_user/' . $account_id . '/' . $user_id);
    }

    function modify_account_folder($account_id, $user_id) {
        $exUserAccount = $this->UserAccount->find('first', array('conditions' => array(
                'user_id' => $user_id,
                'account_id' => $account_id
        )));
        $result = $this->AccountFolder->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folder_users AS afu',
                    'conditions' => 'afu.account_folder_id = AccountFolder.account_folder_id',
                    'type' => 'inner'
                )
            ),
            'conditions' => array(
                "AccountFolder.account_folder_id IN(SELECT
                    account_folder_users.`account_folder_id`
                  FROM
                    account_folder_users
                  WHERE user_id = $user_id)",
                "AccountFolder.folder_type" => 1,
                "afu.role_id <> 220"
            )
        ));
        $exUserAccount = $this->UserAccount->find('first', array('conditions' => array(
                'account_id' => $account_id,
                'role_id' => 100
        )));
        if ($result) {
            foreach ($result as $row) {
                $account_folder_id = $row['AccountFolder']['account_folder_id'];
                $huddles = $this->AccountFolderUser->find('all', array(
                    'conditions' => array(
                        'AccountFolderUser.account_folder_id' => $account_folder_id,
                        'AccountFolderUser.user_id' => $user_id
                    )
                ));
                if (count($huddles) > 1) {
                    foreach ($huddles as $row) {
                        $account_folder_user_id = $row['AccountFolderUser']['account_folder_user_id'];
                        $this->AccountFolderUser->updateAll(array('role_id' => '220', 'last_edit_date' => "'" . date("Y-m-d h:i:s") . "'"), array('account_folder_user_id' => $account_folder_user_id));
                    }
                } else {
                    $account_folder_user_id = $huddles[0]['AccountFolderUser']['account_folder_user_id'];
                    $ownerUser_id = $exUserAccount['UserAccount']['user_id'];
                    $account_folder_id = $huddles[0]['AccountFolderUser']['account_folder_id'];
                    $data = array(
                        'account_folder_id' => $account_folder_id,
                        'user_id' => $ownerUser_id,
                        'role_id' => 200,
                        'created_by' => $user_id,
                        'created_date' => date("Y-m-d h:i:s"),
                        'last_edit_by' => $user_id,
                        'last_edit_date' => date("Y-m-d h:i:s"),
                        'is_coach2' => 0,
                        'is_mentee' => 0,
                        'is_coach' => 0
                    );
                    $this->AccountFolderUser->save($data);
                    $this->AccountFolderUser->updateAll(array('role_id' => '220', 'last_edit_date' => "'" . date("Y-m-d h:i:s") . "'"), array('account_folder_user_id' => $account_folder_user_id));
                }
            }
        }
    }

    function associate_video_huddle_user($account_id, $user_id) {
        $users = $this->Session->read('user_current_account');
        if ($this->request->is('post')) {
            $user = $this->UserAccount->get($account_id, $user_id);
            $userAccountId = $this->request->data['user_account_id'];

            if ($user['UserAccount']['role_id'] == '120' || $user['UserAccount']['role_id'] == '125' ) {
                if (!isset($this->request->data['manage_coach_huddles']) && !isset($this->request->data['manage_collab_huddles']) && !isset($this->request->data['manage_evaluation_huddles'])) {
                    $extraPermission = array(
                        'folders_check' => isset($this->request->data['folders_check']) ? $this->request->data['folders_check'] : '0',
                        'permission_access_video_library' => isset($this->request->data['permission_access_video_library']) ? $this->request->data['permission_access_video_library'] : '0',
                        'permission_video_library_upload' => isset($this->request->data['permission_video_library_upload']) ? $this->request->data['permission_video_library_upload'] : '0',
                        'permission_view_analytics' => isset($this->request->data['permission_view_analytics']) ? $this->request->data['permission_view_analytics'] : '0',
                        'permission_administrator_user_new_role' => isset($this->request->data['permission_administrator_user_new_role']) ? $this->request->data['permission_administrator_user_new_role'] : '0',
                        'parmission_access_my_workspace' => isset($this->request->data['parmission_access_my_workspace']) ? $this->request->data['parmission_access_my_workspace'] : '0',
                        'manage_collab_huddles' => isset($this->request->data['manage_collab_huddles']) ? $this->request->data['manage_collab_huddles'] : '0',
                        'manage_coach_huddles' => isset($this->request->data['manage_coach_huddles']) ? $this->request->data['manage_coach_huddles'] : '0',
                        'manage_evaluation_huddles' => isset($this->request->data['manage_evaluation_huddles']) ? $this->request->data['manage_evaluation_huddles'] : '0',
                        'huddle_to_workspace' => isset($this->request->data['huddle_to_workspace']) ? $this->request->data['huddle_to_workspace'] : '0',
                        'live_recording' => isset($this->request->data['live_recording']) ? $this->request->data['live_recording'] : '0',
                        'permission_start_synced_scripted_notes' => isset($this->request->data['permission_start_synced_scripted_notes']) ? $this->request->data['permission_start_synced_scripted_notes'] : '0',
                        'permission_maintain_folders' => 0
                    );
                } else {
                    $extraPermission = array(
                        'folders_check' => isset($this->request->data['folders_check']) ? $this->request->data['folders_check'] : '0',
                        'permission_access_video_library' => isset($this->request->data['permission_access_video_library']) ? $this->request->data['permission_access_video_library'] : '0',
                        'permission_video_library_upload' => isset($this->request->data['permission_video_library_upload']) ? $this->request->data['permission_video_library_upload'] : '0',
                        'permission_view_analytics' => isset($this->request->data['permission_view_analytics']) ? $this->request->data['permission_view_analytics'] : '0',
                        'permission_administrator_user_new_role' => isset($this->request->data['permission_administrator_user_new_role']) ? $this->request->data['permission_administrator_user_new_role'] : '0',
                        'parmission_access_my_workspace' => isset($this->request->data['parmission_access_my_workspace']) ? $this->request->data['parmission_access_my_workspace'] : '0',
                        'manage_collab_huddles' => isset($this->request->data['manage_collab_huddles']) ? $this->request->data['manage_collab_huddles'] : '0',
                        'manage_coach_huddles' => isset($this->request->data['manage_coach_huddles']) ? $this->request->data['manage_coach_huddles'] : '0',
                        'manage_evaluation_huddles' => isset($this->request->data['manage_evaluation_huddles']) ? $this->request->data['manage_evaluation_huddles'] : '0',
                        'huddle_to_workspace' => isset($this->request->data['huddle_to_workspace']) ? $this->request->data['huddle_to_workspace'] : '0',
                        'live_recording' => isset($this->request->data['live_recording']) ? $this->request->data['live_recording'] : '0',
                        'permission_start_synced_scripted_notes' => isset($this->request->data['permission_start_synced_scripted_notes']) ? $this->request->data['permission_start_synced_scripted_notes'] : '0',
                        'permission_maintain_folders' => 1
                    );
                }
            }
            
            elseif($user['UserAccount']['role_id'] == '115')
            {
                 if (!isset($this->request->data['manage_coach_huddles']) && !isset($this->request->data['manage_collab_huddles']) && !isset($this->request->data['manage_evaluation_huddles'])) {
                    $extraPermission = array(
                        'folders_check' => isset($this->request->data['folders_check']) ? $this->request->data['folders_check'] : 0,
                        'permission_access_video_library' => isset($this->request->data['permission_access_video_library']) ? $this->request->data['permission_access_video_library'] : true,
                        'permission_video_library_upload' => isset($this->request->data['permission_video_library_upload']) ? $this->request->data['permission_video_library_upload'] : '0',
                        'permission_view_analytics' => isset($this->request->data['permission_view_analytics']) ? $this->request->data['permission_view_analytics'] : '0',
                        'permission_administrator_user_new_role' => isset($this->request->data['permission_administrator_user_new_role']) ? $this->request->data['permission_administrator_user_new_role'] : '0',
                        'parmission_access_my_workspace' => isset($this->request->data['parmission_access_my_workspace']) ? $this->request->data['parmission_access_my_workspace'] : true,
                        'manage_collab_huddles' => isset($this->request->data['manage_collab_huddles']) ? $this->request->data['manage_collab_huddles'] : true,
                        'manage_coach_huddles' => isset($this->request->data['manage_coach_huddles']) ? $this->request->data['manage_coach_huddles'] : true,
                        'manage_evaluation_huddles' => isset($this->request->data['manage_evaluation_huddles']) ? $this->request->data['manage_evaluation_huddles'] : '0',
                        'huddle_to_workspace' => isset($this->request->data['huddle_to_workspace']) ? $this->request->data['huddle_to_workspace'] : '0',
                        'live_recording' => isset($this->request->data['live_recording']) ? $this->request->data['live_recording'] : '0',
                        'permission_start_synced_scripted_notes' => isset($this->request->data['permission_start_synced_scripted_notes']) ? $this->request->data['permission_start_synced_scripted_notes'] : '0',
                        'permission_maintain_folders' => 0
                    );
                } else {
                    $extraPermission = array(
                        'folders_check' => isset($this->request->data['folders_check']) ? $this->request->data['folders_check'] : 0,
                        'permission_access_video_library' => isset($this->request->data['permission_access_video_library']) ? $this->request->data['permission_access_video_library'] : true,
                        'permission_video_library_upload' => isset($this->request->data['permission_video_library_upload']) ? $this->request->data['permission_video_library_upload'] : '0',
                        'permission_view_analytics' => isset($this->request->data['permission_view_analytics']) ? $this->request->data['permission_view_analytics'] : '0',
                        'permission_administrator_user_new_role' => isset($this->request->data['permission_administrator_user_new_role']) ? $this->request->data['permission_administrator_user_new_role'] : '0',
                        'parmission_access_my_workspace' => isset($this->request->data['parmission_access_my_workspace']) ? $this->request->data['parmission_access_my_workspace'] : true,
                        'manage_collab_huddles' => isset($this->request->data['manage_collab_huddles']) ? $this->request->data['manage_collab_huddles'] : true,
                        'manage_coach_huddles' => isset($this->request->data['manage_coach_huddles']) ? $this->request->data['manage_coach_huddles'] : true,
                        'manage_evaluation_huddles' => isset($this->request->data['manage_evaluation_huddles']) ? $this->request->data['manage_evaluation_huddles'] : '0',
                        'huddle_to_workspace' => isset($this->request->data['huddle_to_workspace']) ? $this->request->data['huddle_to_workspace'] : '0',
                        'live_recording' => isset($this->request->data['live_recording']) ? $this->request->data['live_recording'] : '0',
                        'permission_start_synced_scripted_notes' => isset($this->request->data['permission_start_synced_scripted_notes']) ? $this->request->data['permission_start_synced_scripted_notes'] : '0',
                        'permission_maintain_folders' => 1
                    );
                }
                
                
            }
//            elseif($user['UserAccount']['role_id']=='110'){
//                $extraPermission = array(
//                    'folders_check' => isset($this->request->data['folders_check']) ? $this->request->data['folders_check'] : '0',
//                    'manage_collab_huddles' => isset($this->request->data['manage_collab_huddles']) ? $this->request->data['manage_collab_huddles'] : '0',
//                    'manage_coach_huddles' => isset($this->request->data['manage_coach_huddles']) ? $this->request->data['manage_coach_huddles'] : '0'
//
//                    );
//
//            }
//            else{
//                 $extraPermission = array(
//                    'folders_check' => isset($this->request->data['folders_check']) ? $this->request->data['folders_check'] : '0'
//
//                    );
//
//            }


            if ($user && count($user) > 0) {
                $extraPermission['last_edit_date'] = "'" . date('Y-m-d H:i:s') . "'";
                $extraPermission['last_edit_by'] = $users['User']['id'];

                $this->UserAccount->updateAll($extraPermission, array('id' => $userAccountId));
                if ($this->UserAccount->getAffectedRows() > 0) {
                    $this->Session->setFlash($this->language_based_messages['extra_privileges_changed_successfully_msg'], 'default', array('class' => 'message success'));
                } else {
                    $this->Session->setFlash($this->language_based_messages['extra_privileges_not_changed_please_try_again_msg'], 'default', array('class' => 'message error'));
                }
            } else {
                $extraPermission['created_date'] = "'" . date('Y-m-d H:i:s') . "'";
                $extraPermission['created_by'] = $users['User']['id'];
                $this->UserAccount->save($extraPermission);
                if ($this->UserAccount->id > 0) {
                    $this->Session->setFlash($this->language_based_messages['extra_privileges_changed_successfully_msg'], 'default', array('class' => 'messae success'));
                } else {
                    $this->Session->setFlash($this->language_based_messages['you_are_unable_to_change_extra_privileges_msg'], 'default', array('class' => 'messae error'));
                }
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['please_select_correct_video_huddle_option_msg$this']);
        }
        $this->redirect('/permissions/assign_user/' . $account_id . '/' . $user_id);
    }

    function associate_huddle_user($account_id, $users_id) {
        if ($this->request->is('post')) {

            $users = $this->Session->read('user_current_account');
            $user_id = $this->request->data['user_id'];
            $accountFolderUserExist = $this->AccountFolderUser->getCount($user_id);
            $roles_id = $this->_getUsersRoles($account_id, $user_id);
            $error_message = '';
            $accountFolderData = array(
                'user_id' => $user_id,
                'role_id' => $roles_id,
                'created_by' => $users['User']['id'],
                'created_date' => date('Y-m-d H:i:s')
            );
            $accountFolderIds = isset($this->request->data['account_folder_ids']) ? $this->request->data['account_folder_ids'] : '';
            $coacheeaccountFolderIds = isset($this->request->data['coach_account_folder_id']) ? $this->request->data['coach_account_folder_id'] : '';
            $superUsersIds = isset($this->data['super_admin_ids']) ? $this->data['super_admin_ids'] : '';
            $groupIds = isset($this->data['group_ids']) ? $this->data['group_ids'] : '';

            if ($accountFolderUserExist > 0) {

                //get all User Huddles.
                $user_huddles = $this->AccountFolder->getUserHuddles($user_id, $users['accounts']['account_id']);
                for ($i = 0; $i < count($user_huddles); $i++) {
                    $user_huddle = $user_huddles[$i];
                    
                    if(!in_array($user_huddle['AccountFolder']['account_folder_id'], $coacheeaccountFolderIds) && !in_array($user_huddle['AccountFolder']['account_folder_id'], $accountFolderIds) )
                    {
                        $this->AccountFolderUser->deleteAll(array('user_id' => $user_id, 'account_folder_id' => $user_huddle['AccountFolder']['account_folder_id']));
                       
                    }
                    
                    
                    
                }

                if (!empty($accountFolderIds) || !empty($coacheeaccountFolderIds)) {
                    if (!empty($accountFolderIds))
                        for ($i = 0; $i < count($accountFolderIds); $i++) {

                            $huddle_meta_info = $this->AccountFolderMetaData->find('first', array(
                                'conditions' => array(
                                    'account_folder_id' => $accountFolderIds[$i],
                                    'meta_data_name' => 'folder_type'
                                )
                            ));
                            $huddle_type = isset($huddle_meta_info['AccountFolderMetaData']['meta_data_value']) ? $huddle_meta_info['AccountFolderMetaData']['meta_data_value'] : '';

                            $this->AccountFolderUser->create();
                            $accountFolderData['account_folder_id'] = $accountFolderIds[$i];
                            $accountFolderData['role_id'] = $this->request->data['user_role_' . $accountFolderIds[$i]];
                            $accountFolderData['last_edit_by'] = $users['User']['id'];
                            $accountFolderData['last_edit_date'] = date('Y-m-d H:i:s');
                            $accountFolderData['user_id'] = $user_id;
                            $accountFolderData['created_by'] = $users['User']['id'];
                            $accountFolderData['created_date'] = date('Y-m-d H:i:s');
                            $accountFolderData['is_coach'] = '0';
                            $accountFolderData['is_mentee'] = '0';
                            
                            $this->AccountFolderUser->deleteAll(array('user_id' => $user_id, 'account_folder_id' => $accountFolderIds[$i]));
                            $this->AccountFolderUser->save($accountFolderData);
                        }
                    if (!empty($coacheeaccountFolderIds))
                        for ($i = 0; $i < count($coacheeaccountFolderIds); $i++) {

                            $check_value = $this->AccountFolderUser->find('first', array(
                                'conditions' => array(
                                    'account_folder_id' => $coacheeaccountFolderIds[$i],
                                    'user_id' => $user_id
                                )
                            ));
                            if (!empty($check_value)) {
                                $accountFolderData = array();
                                $accountFolderData['is_coach'] = '0';
                                $accountFolderData['is_mentee'] = '0';
                                if ($this->request->data['user_coach_' . $coacheeaccountFolderIds[$i]] == '200') {
                                    $accountFolderData['is_coach'] = '1';
                                    $accountFolderData['role_id'] = '200';
                                    $view = new View($this, FALSE);
                                    $permission = $view->Custom->huddle_check_on_permission_page($coacheeaccountFolderIds[$i],$user_id,200);
                                    
                                    if(isset($permission['success']) && $permission['success'] == true)
                                    {
                                        $error_message .= $permission['message'].'<br>';
                                        continue;
                                    }
                                } else if ($this->request->data['user_coach_' . $coacheeaccountFolderIds[$i]] == '210') {
                                    $accountFolderData['is_mentee'] = '1';
                                    $accountFolderData['role_id'] = '210';
                                    $view = new View($this, FALSE);
                                    $permission = $view->Custom->huddle_check_on_permission_page($coacheeaccountFolderIds[$i],$user_id,210);
                                    
                                    if(isset($permission['success']) && $permission['success'] == true)
                                    {
                                        $error_message .= $permission['message'].'<br>';
                                        continue;
                                    }
                                    
                                    
                                    
                                } else {

                                    $accountFolderData['is_coach'] = '0';
                                    $accountFolderData['is_mentee'] = '0';
                                    $accountFolderData['role_id'] = '220';
                                }
                                $accountFolderData['last_edit_by'] = $users['User']['id'];
                                $accountFolderData['last_edit_date'] = "'" . date('Y-m-d H:i:s') . "'";
                                $this->AccountFolderUser->updateAll($accountFolderData, array(
                                    'user_id' => $user_id,
                                    'account_folder_id' => $coacheeaccountFolderIds[$i]
                                ));
                            } else {
                                $accountFolderData = array();
                                $this->AccountFolderUser->create();

                                $accountFolderData['is_coach'] = '0';
                                $accountFolderData['is_mentee'] = '0';


                                if ($this->request->data['user_coach_' . $coacheeaccountFolderIds[$i]] == '200') {
                                    $accountFolderData['is_coach'] = '1';
                                    $accountFolderData['role_id'] = '200';
                                    $view = new View($this, FALSE);
                                    $permission = $view->Custom->huddle_check_on_permission_page($coacheeaccountFolderIds[$i],$user_id,200);
                                    
                                    if(isset($permission['success']) && $permission['success'] == true)
                                    {
                                        $error_message .= $permission['message'].'<br>';
                                        continue;
                                    }
                                } else if ($this->request->data['user_coach_' . $coacheeaccountFolderIds[$i]] == '210') {
                                    $accountFolderData['is_mentee'] = '1';
                                    $accountFolderData['role_id'] = '210';
                                    $view = new View($this, FALSE);
                                    $permission = $view->Custom->huddle_check_on_permission_page($coacheeaccountFolderIds[$i],$user_id,210);
                                    
                                    if(isset($permission['success']) && $permission['success'] == true)
                                    {
                                        $error_message .= $permission['message'].'<br>';
                                        continue;
                                    }
                                } else {
                                    $accountFolderData['role_id'] = '220';
                                }

                                $accountFolderData['account_folder_id'] = $coacheeaccountFolderIds[$i];
                                $accountFolderData['last_edit_by'] = $users['User']['id'];
                                $accountFolderData['last_edit_date'] = date('Y-m-d H:i:s');
                                $accountFolderData['created_by'] = $users['User']['id'];
                                $accountFolderData['created_date'] = date('Y-m-d H:i:s');
                                $accountFolderData['user_id'] = $user_id;
                                
                                $this->AccountFolderUser->deleteAll(array('user_id' => $user_id, 'account_folder_id' => $coacheeaccountFolderIds[$i]));
                                $this->AccountFolderUser->save($accountFolderData);
                            }
                        }

                    //$this->_add_huddle_permissions($users_id);
                    if(!empty($error_message))
                    {
                     $this->Session->setFlash($error_message, 'default', array('class' => 'message error'));   
                    }
                    else {    
                    $this->Session->setFlash($this->language_based_messages['user_huddle_permission_has_been_changed_msg'], 'default', array('class' => 'message success'));
                    }
                }
            } else {
                if (!empty($accountFolderIds) || !empty($coacheeaccountFolderIds)) {
                    if (!empty($accountFolderIds))
                        for ($i = 0; $i < count($accountFolderIds); $i++) {

                            $huddle_meta_info = $this->AccountFolderMetaData->find('first', array(
                                'conditions' => array(
                                    'account_folder_id' => $accountFolderIds[$i],
                                    'meta_data_name' => 'folder_type'
                                )
                            ));
                            $huddle_type = isset($huddle_meta_info['AccountFolderMetaData']['meta_data_value']) ? $huddle_meta_info['AccountFolderMetaData']['meta_data_value'] : '';

                            $this->AccountFolderUser->create();
                            $accountFolderData['account_folder_id'] = $accountFolderIds[$i];
                            $accountFolderData['role_id'] = $this->request->data['user_role_' . $accountFolderIds[$i]];
                            $accountFolderData['is_coach'] = '0';
                            $accountFolderData['is_mentee'] = '0';
                            $accountFolderData['user_id'] = $user_id;
                            $accountFolderData['created_by'] = $users['User']['id'];
                            $accountFolderData['created_date'] = date('Y-m-d H:i:s');


                            if ($huddle_type == '2') {

                                if ($accountFolderData['role_id'] == '200') {
                                    $accountFolderData['is_coach'] = '1';
                                } else if ($accountFolderData['role_id'] == '210') {
                                    $accountFolderData['is_mentee'] = '1';
                                }
                            }
                            
                            $this->AccountFolderUser->deleteAll(array('user_id' => $user_id, 'account_folder_id' => $accountFolderIds[$i]));
                            $this->AccountFolderUser->save($accountFolderData);
                        }
                    if (!empty($coacheeaccountFolderIds))
                        for ($i = 0; $i < count($coacheeaccountFolderIds); $i++) {

                            $check_value = $this->AccountFolderUser->find('first', array(
                                'conditions' => array(
                                    'account_folder_id' => $coacheeaccountFolderIds[$i],
                                    'user_id' => $user_id
                                )
                            ));
                            if (!empty($check_value)) {
                                $accountFolderData = array();
                                $accountFolderData['is_coach'] = '0';
                                $accountFolderData['is_mentee'] = '0';
                                if ($this->request->data['user_coach_' . $coacheeaccountFolderIds[$i]] == '200') {
                                    $accountFolderData['is_coach'] = '1';
                                    $accountFolderData['role_id'] = '200';
                                    $view = new View($this, FALSE);
                                    $permission = $view->Custom->huddle_check_on_permission_page($coacheeaccountFolderIds[$i],$user_id,200);
                                    
                                    if(isset($permission['success']) && $permission['success'] == true)
                                    {
                                        $error_message .= $permission['message'].'<br>';
                                        continue;
                                    }
                                } else if ($this->request->data['user_coach_' . $coacheeaccountFolderIds[$i]] == '210') {
                                    $accountFolderData['is_mentee'] = '1';
                                    $accountFolderData['role_id'] = '210';
                                    $view = new View($this, FALSE);
                                    $permission = $view->Custom->huddle_check_on_permission_page($coacheeaccountFolderIds[$i],$user_id,210);
                                    
                                    if(isset($permission['success']) && $permission['success'] == true)
                                    {
                                        $error_message .= $permission['message'].'<br>';
                                        continue;
                                    }
                                } else {

                                    $accountFolderData['is_coach'] = '0';
                                    $accountFolderData['is_mentee'] = '0';
                                    $accountFolderData['role_id'] = '220';
                                }

                                $accountFolderData['last_edit_by'] = $users['User']['id'];
                                $accountFolderData['last_edit_date'] = "'" . date('Y-m-d H:i:s') . "'";
                                $this->AccountFolderUser->updateAll($accountFolderData, array(
                                    'user_id' => $user_id,
                                    'account_folder_id' => $coacheeaccountFolderIds[$i]
                                ));
                            } else {
                                $accountFolderData = array();
                                $this->AccountFolderUser->create();

                                $accountFolderData['is_coach'] = '0';
                                $accountFolderData['is_mentee'] = '0';


                                if ($this->request->data['user_coach_' . $coacheeaccountFolderIds[$i]] == '200') {
                                    $accountFolderData['is_coach'] = '1';
                                    $accountFolderData['role_id'] = '200';
                                    $view = new View($this, FALSE);
                                    $permission = $view->Custom->huddle_check_on_permission_page($coacheeaccountFolderIds[$i],$user_id,200);
                                    
                                    if(isset($permission['success']) && $permission['success'] == true)
                                    {
                                        $error_message .= $permission['message'].'<br>';
                                        continue;
                                    }
                                } else if ($this->request->data['user_coach_' . $coacheeaccountFolderIds[$i]] == '210') {
                                    $accountFolderData['is_mentee'] = '1';
                                    $accountFolderData['role_id'] = '210';
                                    $view = new View($this, FALSE);
                                    $permission = $view->Custom->huddle_check_on_permission_page($coacheeaccountFolderIds[$i],$user_id,210);
                                    
                                    if(isset($permission['success']) && $permission['success'] == true)
                                    {
                                        $error_message .= $permission['message'].'<br>';
                                        continue;
                                    }
                                } else {
                                    $accountFolderData['role_id'] = '220';
                                }

                                $accountFolderData['account_folder_id'] = $coacheeaccountFolderIds[$i];
                                $accountFolderData['last_edit_by'] = $users['User']['id'];
                                $accountFolderData['last_edit_date'] = date('Y-m-d H:i:s');
                                $accountFolderData['created_by'] = $users['User']['id'];
                                $accountFolderData['created_date'] = date('Y-m-d H:i:s');
                                $accountFolderData['user_id'] = $user_id;
                                
                                $this->AccountFolderUser->deleteAll(array('user_id' => $user_id, 'account_folder_id' => $coacheeaccountFolderIds[$i]));
                                $this->AccountFolderUser->save($accountFolderData);
                            }
                        }
                    if(!empty($error_message))
                    {
                     $this->Session->setFlash($error_message, 'default', array('class' => 'message error'));   
                    }
                    else {    
                    $this->Session->setFlash($this->language_based_messages['user_huddle_permission_has_been_changed_msg'], 'default', array('class' => 'message success'));
                    }
                } else {
                    $this->Session->setFlash($this->language_based_messages['we_are_sorry_something_went_wrong_msg'], 'default', array('class' => 'message'));
                }
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['we_are_sorry_something_went_wrong_msg'], 'default', array('class' => 'message'));
        }
        $this->redirect('/permissions/assign_user/' . $account_id . '/' . $users_id);
    }

    function _add_huddle_permissions($user_id) {

        $users = $this->Session->read('user_current_account');
        $accountFolderIds = isset($this->request->data['account_folder_ids']) ? $this->request->data['account_folder_ids'] : '';

        $already_exist_in_huddle_user = $this->AccountFolderUser->getCount($user_id);

        if ($accountFolderIds != '' && $already_exist_in_huddle_user > 0) {

            //get all User Huddles.
            $user_huddles = $this->AccountFolder->getUserHuddles($user_id, $users['accounts']['account_id']);
            for ($i = 0; $i < count($user_huddles); $i++) {
                $user_huddle = $user_huddles[$i];

                $this->AccountFolderUser->deleteAll(array('user_id' => $user_id, 'account_folder_id' => $user_huddle['AccountFolder']['account_folder_id']));
            }

            foreach ($accountFolderIds as $account_folder_ids) {
                $assigned_role = isset($this->request->data['user_role_' . $account_folder_ids]) ? $this->request->data['user_role_' . $account_folder_ids] : '';

                $huddle_meta_info = $this->AccountFolderMetaData->find('first', array(
                    'conditions' => array(
                        'account_folder_id' => $account_folder_ids,
                        'meta_data_name' => 'folder_type'
                    )
                ));

                $huddle_type = isset($huddle_meta_info['AccountFolderMetaData']['meta_data_value']) ? $huddle_meta_info['AccountFolderMetaData']['meta_data_value'] : '';


                $user_account_folder = array(
                    'account_folder_id' => $account_folder_ids,
                    'user_id' => $this->request->data['user_id'],
                    'role_id' => $assigned_role,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id'],
                    'is_coach' => 0,
                    'is_mentee' => 0
                );

                if ($huddle_type == '2') {
                    if ($user_account_folder['role_id'] == '200') {
                        $user_account_folder['is_coach'] = '1';
                    } else if ($user_account_folder['role_id'] == '210') {
                        $user_account_folder['is_mentee'] = '1';
                    }
                }

                $this->AccountFolderUser->create();
                $this->AccountFolderUser->save($user_account_folder);
                continue;
            }
        } else {
            foreach ($accountFolderIds as $account_folder_ids) {
                $assigned_role = isset($this->request->data['user_role_' . $account_folder_ids]) ? $this->request->data['user_role_' . $account_folder_ids] : '';

                $huddle_meta_info = $this->AccountFolderMetaData->find('first', array(
                    'conditions' => array(
                        'account_folder_id' => $account_folder_ids,
                        'meta_data_name' => 'folder_type'
                    )
                ));

                $huddle_type = isset($huddle_meta_info['AccountFolderMetaData']['meta_data_value']) ? $huddle_meta_info['AccountFolderMetaData']['meta_data_value'] : '';


                $user_account_folder = array(
                    'account_folder_id' => $account_folder_ids,
                    'user_id' => $this->request->data['user_id'],
                    'role_id' => $assigned_role,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id'],
                    'is_coach' => 0,
                    'is_mentee' => 0
                );

                if ($huddle_type == '2') {
                    if ($user_account_folder['role_id'] == '200') {
                        $user_account_folder['is_coach'] = '1';
                    } else if ($user_account_folder['role_id'] == '210') {
                        $user_account_folder['is_mentee'] = '1';
                    }
                }

                $this->AccountFolderUser->create();
                $this->AccountFolderUser->save($user_account_folder);
                continue;
            }
        }
    }

    function _getUsersRoles($account_id, $user_id) {
        $result = $this->UserAccount->get($account_id, $user_id);
        if ($result && count($result) > 0) {
            $role_id = '';
            $key = $result['roles']['role_id'];
            switch ($key) {
                case 100:
                    return $role_id = '200';
                    break;

                case 110:
                    return $role_id = '210';
                    break;

                case 120:
                    return $role_id = '220';
                    break;
            }
        }
    }

    function users_extra_privileges($account_id) {
        $result = array(
            'message' => '',
            'status' => false,
        );
        if ($this->request->is('post')) {
            if(isset($this->request->data['admin_post']) && $this->request->data['admin_post']==1){
                $adminExtraPermission = array(
                    'permission_video_library_upload' => isset($this->request->data['admin_permission_video_library_upload']) ? $this->request->data['admin_permission_video_library_upload'] : '0',
                    'permission_view_analytics' => isset($this->request->data['admin_view_analytics']) ? $this->request->data['admin_view_analytics'] : '0',
                    'permission_administrator_user_new_role' => isset($this->request->data['admin_permission_administrator_user_new_role']) ? $this->request->data['admin_permission_administrator_user_new_role'] : '0'
                );
                $this->UserAccount->updateAll($adminExtraPermission, array('account_id' => $account_id, 'role_id' => 115));
            } else {
                $extraPermission = array(
                    'folders_check' => isset($this->request->data['folders_check']) ? $this->request->data['folders_check'] : '0',
                    'permission_access_video_library' => isset($this->request->data['permission_access_video_library']) ? $this->request->data['permission_access_video_library'] : '0',
                    'permission_video_library_upload' => isset($this->request->data['permission_video_library_upload']) ? $this->request->data['permission_video_library_upload'] : '0',
                    'permission_view_analytics' => isset($this->request->data['permission_view_analytics']) ? $this->request->data['permission_view_analytics'] : '0',
                    'permission_administrator_user_new_role' => isset($this->request->data['permission_administrator_user_new_role']) ? $this->request->data['permission_administrator_user_new_role'] : '0',
                    'parmission_access_my_workspace' => isset($this->request->data['parmission_access_my_workspace']) ? $this->request->data['parmission_access_my_workspace'] : '0',
                    'manage_collab_huddles' => isset($this->request->data['manage_collab_huddles']) ? $this->request->data['manage_collab_huddles'] : '0',
                    'manage_coach_huddles' => isset($this->request->data['manage_coach_huddles']) ? $this->request->data['manage_coach_huddles'] : '0',
                    'manage_evaluation_huddles' => isset($this->request->data['manage_evaluation_huddles']) ? $this->request->data['manage_evaluation_huddles'] : '0',
                    'permission_view_analytics' => isset($this->request->data['view_analytics']) ? $this->request->data['view_analytics'] : '0',
                    'permission_start_synced_scripted_notes' => isset($this->request->data['start_synced_scripted_notes']) ? $this->request->data['start_synced_scripted_notes'] : '0',
                    'huddle_to_workspace' => isset($this->request->data['huddle_to_workspace']) ? $this->request->data['huddle_to_workspace'] : '0',
                    'permission_maintain_folders' => 0
                );
                $this->UserAccount->updateAll($extraPermission, array('account_id' => $account_id, 'role_id' => 120));
            }

            if ($this->UserAccount->getAffectedRows() > 0) {
                $result['status'] = true;
                $result['message'] = $this->language_based_messages['extra_privileges_changed_successfully_msg'];
            } else {
                $result['message'] = $this->language_based_messages['extra_privileges_not_changed_please_try_again_msg'];
            }
        }
        echo json_encode($result);
        die;
    }

}
