<?php

class LaunchpadController extends AppController {

    public $uses = array('User', 'UserAccount');

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    public function beforeFilter() {

        $users = $this->Session->read('user_current_account');
        $this->set('logged_in_user', $users);
        $users = $this->Session->read('user_current_account');
        $totalAccounts = $this->Session->read('totalAccounts');
        if ($users['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
            $this->redirect_on_account_settings();
        }

        parent::beforeFilter();
    }

    function index() {
        $this->redirect('/home/launchpad');
        exit;
    }

    function get_accounts_api() {
        $user_current_account = $this->Session->read('user_current_account');
        $allAccounts = $this->Session->read('user_accounts');
        $final_result = array();

        if ($this->Session->read('totalAccounts') > 1 ) {
            $user = $this->Auth->user();
            $user_current_account = $this->Session->read('user_current_account');
            // $this->set('account_id', $user_current_account[0]['accounts']['account_id']);
            $allAccounts = $this->Session->read('user_accounts');
            $user_id = null;
            if (isset($user_current_account['User']['id'])) {
                $user_id = $user_current_account['User']['id'];
            }elseif ($user_current_account[0]['User']['id']) {
                $user_id = $user_current_account[0]['User']['id'];
            }

            $result = !empty($user_id) ? $this->User->get($user_id) : false;
            if($result){
                foreach($result as $row){
                    if($row['users_accounts']['is_active']==0){
                        continue;
                    }
                    $final_result[] = $row;
                }
            }  
            // $this->set('allAccounts', $final_result); 
            echo json_encode($final_result);
            exit;
        } elseif($this->Session->read('totalAccounts') == 1) {
            echo $this->getRedirectJson('profile-page', true);
            exit;
        } else {
            $this->no_permissions();
        }
    }

/*
    function index() {
        $user_current_account = $this->Session->read('user_current_account');
        $allAccounts = $this->Session->read('user_accounts');
        $final_result = array();
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('launchpad');
        $this->set('language_based_content',$language_based_content);

        if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1) {
            $user = $this->Auth->user();
            $user_current_account = $this->Session->read('user_current_account');
            $this->set('account_id', $user_current_account['accounts']['account_id']);
            $allAccounts = $this->Session->read('user_accounts');

            if (isset($this->request->data['txtSearchDashboard'])) {

                $result = $this->User->get1($user_current_account['User']['id'], $this->request->data['txtSearchDashboard']);
                if ($this->request->data['txtSearchDashboard'] == '') {
                    $result = $this->User->get($user_current_account['User']['id']);
                }
                if($result){
                    foreach($result as $row){
                        if($row['users_accounts']['is_active']==0){
                            continue;
                        }
                        $final_result[] = $row;
                    }
                } 
                $params = array(
                    'allAccounts' => $final_result,
                );

                $view = new View($this, false);
                $html = $view->element('ajax/search_dashboard', $params);
                echo $html;
                $search_text = $this->request->data['txtSearchDashboard'];
                $this->set('search_text', $search_text);
            } else {
                $result = $this->User->get($user_current_account['User']['id']);                
                if($result){
                    foreach($result as $row){
                        if($row['users_accounts']['is_active']==0){
                            continue;
                        }
                        $final_result[] = $row;
                    }
                }  
                $this->set('allAccounts', $final_result);              
            }
            
        } else {
            $this->no_permissions();
        }
    }
*/
    function search() {
        $user_current_account = $this->Session->read('user_current_account');
        $allAccounts = $this->Session->read('user_accounts');
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('launchpad');
        $final_result = array();

        if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1) {
            $user = $this->Auth->user();
            $user_current_account = $this->Session->read('user_current_account');
            $this->set('account_id', $user_current_account['accounts']['account_id']);
            $allAccounts = $this->Session->read('user_accounts');

            if (isset($this->request->data['txtSearchDashboard'])) {
                $result = $this->User->get1($user_current_account['User']['id'], $this->request->data['txtSearchDashboard']);
                if ($this->request->data['txtSearchDashboard'] == '') {
                    $result = $this->User->get($user_current_account['User']['id']);
                }
                if($result){
                    foreach($result as $row){
                        if($row['users_accounts']['is_active']==0){
                            continue;
                        }
                        $final_result[] = $row;
                    }
                }  
                $params = array(
                    'allAccounts' => $final_result,
                   'language_based_content' => $language_based_content
                );

                $view = new View($this, false);
                $html = $view->element('ajax/search_dashboard', $params);
                echo $html;
                $search_text = $this->request->data['txtSearchDashboard'];
                $this->set('search_text', $search_text);
            } else {
                $result = $this->User->get($user_current_account['User']['id']);
                if($result){
                    foreach($result as $row){
                        if($row['users_accounts']['is_active']==0){
                            continue;
                        }
                        $final_result[] = $row;
                    }
                }  
            }
            $this->set('allAccounts',  $final_result);
        } else {
            $this->no_permissions();
        }

        die();
    }

    function account($user_account_id, $huddle_id = false) {
        $this->redirect('/home/launchpad');
        exit;
    }

    function switch_account_api() {
        if (!$this->request->is('post')) {
            echo json_encode(["success"=>false, "message"=>"Get Request Not Allowed."]);
            exit;
        }
        if (!$this->Auth->loggedIn()) {
            echo $this->getRedirectJson("login", false);
            exit;
        }
        $view = new View($this, false);
        $user_account_id = $this->request->data['account_id'];
        $user_current_account = $this->Session->read('user_current_account');
        $current_account_id = $user_current_account['accounts']['account_id'];
        if($this->Session->read('redirect_uri') == "/users/logout")
        {
            $this->Session->write('redirect_uri', "/home/profile-page");
        }

        if ($user_account_id != $current_account_id) {
            $this->Session->write('filters_data', '');
        }

        $allAccounts = $this->Session->read('user_accounts');
        //var_dump($this->Session->read('redirect_uri'));
        //var_dump(strpos($this->Session->read('redirect_uri'), 'reset_password'));
        //die;

        $account_id = $user_current_account['accounts']['account_id'];
        $user_id = $user_current_account['User']['id'];
        
        $channel_data = array(
              'channel' => 'global-channel',
              'event' => "account-change",
              'account_id' => $account_id,
              'user_id' => $user_id,
              'uuid' => $this->Session->id()
          );
        //$view->Custom->BroadcastEvent($channel_data,false);
        $this->setUserType($account_id,$user_id);
        $trial_duration = Configure::read('trial_duration');

        if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1) {
            // $is_account_expired = $this->Account->is_account_expired($user_account_id);
            if ($this->Account->is_account_expired($user_account_id)) {
                echo $this->getRedirectJson('launchpad', false, false, $view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]));
                exit;
            }
            if (!empty($user_account_id)) {
               
                $this->switch_account($user_account_id);
                $this->Session->setFlash("", 'default', array('class' => 'no-class'));
                $user_current_account = $this->Session->read('user_current_account');
                $start_end_date = $this->get_start_end_date($user_current_account['accounts']['account_id']);
                $st_date = $start_end_date['start_date'];
                $end_date = $start_end_date['end_date'];
                $user_role_id = $user_current_account['roles']['role_id'];
                $analytics_permission = $user_current_account['users_accounts']['permission_view_analytics'];
                if ($user_role_id == 120 && $analytics_permission != 0) {
                    if (!empty($this->Session->read('redirect_uri')) && strpos($this->Session->read('redirect_uri'), 'sysRefreash') === false) {
                        if(strpos($this->Session->read('redirect_uri'), 'reset_password')){
                            // $this->redirect('/home/profile-page');
                            echo $this->getRedirectJson('profile-page');
                            exit;                    
                        }elseif(strpos($this->Session->read('redirect_uri'),'forgot_password')){
                            // $this->redirect('/home/profile-page');
                            echo $this->getRedirectJson('profile-page');
                            exit;                    
                        }
                        $redirect_url = $this->Session->read('redirect_uri');                       
                        // $this->Session->write('redirect_uri', "/");
                        $this->Session->delete('redirect_uri');
                        // $this->redirect($redirect_url);
                        echo $this->getRedirectJson($redirect_url, true);
                        exit;                    
                    } else {    
                        // $this->redirect('/home/profile-page');                    
                        echo $this->getRedirectJson('profile-page');
                        exit;                    
                   // $this->redirect('/analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
                    }
                } else {                   
                    if (!empty($this->Session->read('redirect_uri')) && strpos($this->Session->read('redirect_uri'), 'sysRefreash') === false){
                       if(strpos($this->Session->read('redirect_uri'), 'reset_password')){
                            // $this->redirect('/home/profile-page');
                            echo $this->getRedirectJson('profile-page');
                            exit;
                        }elseif(strpos($this->Session->read('redirect_uri'),'forgot_password')){
                            // $this->redirect('/home/profile-page');
                            echo $this->getRedirectJson('profile-page');
                            exit;                    
                        }
                        $redirect_url = $this->Session->read('redirect_uri');                      
                        // $this->Session->write('redirect_uri', "/");
                        $this->Session->delete('redirect_uri');
                        // $this->redirect($redirect_url);
                        echo $this->getRedirectJson($redirect_url, true);
                        exit;                    
                    } else {
                        // $this->redirect('/home/profile-page');
                        echo $this->getRedirectJson('profile-page');
                        exit;                    
                    }
                }
            } else {
                
                $this->Session->setFlash($this->language_based_messages['please_choose_account_from_launchpad_msg'], 'default', array('class' => 'message error'));
                // $this->redirect('/launchpad');
                echo $this->getRedirectJson('launchpad', false, false, $this->language_based_messages['please_choose_account_from_launchpad_msg']);
                exit;                    
            }
        } else {
            if (!empty($this->Session->read('redirect_uri')) && strpos($this->Session->read('redirect_uri'), 'sysRefreash') === false ) {
                if(strpos($this->Session->read('redirect_uri'), 'reset_password')){
                    // $this->redirect('/home/profile-page');
                    echo $this->getRedirectJson('profile-page');
                    exit;                    
                }elseif(strpos($this->Session->read('redirect_uri'),'forgot_password')){
                    // $this->redirect('/home/profile-page');
                    echo $this->getRedirectJson('profile-page');
                    exit;                    
                }
                $redirect_url = $this->Session->read('redirect_uri');               
                $this->Session->delete('redirect_uri');
                // $this->Session->write('redirect_uri', "/");
                // $this->redirect($redirect_url);
                echo $this->getRedirectJson($redirect_url, true);
                exit;                    
            } else {
                // $this->redirect('/home/profile-page');
                echo $this->getRedirectJson('profile-page');
                exit;                    
            }
        }
    }

    public function setUserType($account_id,$user_id){
        App::uses('AnalyticsController', 'Controller');
        $user_type  = AnalyticsController::get_user_type($account_id,$user_id);

        $update_array = array(
            'user_type_en' => '"'.$user_type.'"',
            'user_type_es' => '"'.$user_type.'"'
        );

        $this->UserAccount->updateAll($update_array, array('account_id' => $account_id, 'user_id' => $user_id), $validation = TRUE);
    }

}
