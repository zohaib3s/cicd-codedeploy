<?php

App::uses('Subscription', 'Braintree');
App::import('Vendor', 'tcpdf/tcpdf');
App::uses('Sanitize', 'Utility');

use Aws\S3\S3Client;

class AccountsController extends AppController {

    public $name = 'Accounts';
    var $uploadDir = '';
    var $dir = 'img/company_logos/';
    public $uses = array('User', 'UserAccount', 'Account', 'AccountMetaData', 'AccountFolderMetaData', "AccountTag", "DocumentStandardRating", "AccountFrameworkSetting", "AccountFrameworkSettingPerformanceLevel", "PerformanceLevelDescription");

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $this->uploadDir = WWW_ROOT . $this->dir;
    }

    public function beforeFilter() {

        $users = $this->Session->read('user_current_account');
        $this->set('logged_in_user', $users);
        $this->set('UserObject', $this->User);

        parent::beforeFilter();
    }

    function index() {
        $users = $this->Session->read('user_current_account');
        $totalAccounts = $this->Session->read('totalAccounts');
        if ($users['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
            $this->redirect_on_account_settings();
        }
        $this->render('index');
    }

    public function get_domain(){
        if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }
        else{
            $protocol = 'http';
        }
        return $protocol . "://" . $_SERVER['SERVER_NAME'];
    }
    public function view_qr($id, $code)
    {
        if (!$this->Account->exists($id)) {
            throw new NotFoundException(__('Invalid account'));
        }
        $this->set('account_id', $id);
        $this->set('code', $code);
        $this->set('registration_link', $this->getRegistrationLink($id, $code));
    }

    public function getRegistrationLink($id, $code)
    {
        return $this->get_domain()."/users/sibme_registration/".base64_encode($id)."?code=$code";
    }
    function create($user_id, $parent_account_id) {

        $loggedInUser = $this->Session->read('user_current_account');
        $current_account_id = $loggedInUser['accounts']['account_id'];
        $current_user_id = $loggedInUser['User']['id'];

        if ($loggedInUser['accounts']['allow_launchpad'] != '1') {
            $this->no_permissions();
            return;
        }

        if ($user_id != $current_user_id || $parent_account_id != $current_account_id) {
            $this->Session->setFlash($this->language_based_messages['selected_account_owner_is_not_valid_msg']);
            $this->redirect('/launchpad');
        }

        if ($this->data && count($this->data) > 0) {

            if (empty($this->data['company_name'])) {
                $this->Session->setFlash($this->language_based_messages['please_enter_company_name_msg']);
                $this->redirect("/Accounts/create/$user_id/$parent_account_id");
            }

            $data = array(
                'company_name' => $this->data['company_name'],
                'parent_account_id' => $current_account_id,
                'created_at' => date('Y-m-d H:i:s'),
                'is_active' => 1,
                'in_trial' => 1
            );

            $new_account_id = $this->Account->save($data);
            if ($new_account_id) {
                $new_account_id = $this->Account->id;
                $data = array(
                    'account_id' => $new_account_id,
                    'user_id' => $user_id,
                    'role_id' => 100
                );

                $new_user_account_id = $this->UserAccount->save($data);

                if ($new_account_id != '' && $new_user_account_id != '') {

                    $user_accounts = $this->User->get($current_user_id);
                    $this->Session->write('user_accounts', $user_accounts);
                    $this->Session->write('totalAccounts', count($user_accounts));

                    $this->Session->setFlash($this->language_based_messages['you_have_successfully_created_account_msg'], 'default', array('class' => 'message success'));
                    $this->redirect('/launchpad');
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['your_account_is_not_created_yet_msg'], 'default', array('class' => 'message error'));
                $this->set('user_id', $user_id);
                $this->set('account_id', $parent_account_id);
            }
        } else {
            $this->set('user_id', $user_id);
            $this->set('account_id', $parent_account_id);
        }
    }

    function accountSettings($account_id, $tab = '1') {

        if (!$account_id)
            $this->redirect('/');

        $loggedInUser = $this->Session->read('user_current_account');
        $this->set('myAccount', $this->getMyAccount($account_id));
        $this->set("account_id", $account_id);
        $this->set('plans', $this->plansList());
        $this->set('tab', $tab);
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $account_information = $this->Account->getMyAccounts($account_id);
        $braintree_subscription_id = $account_information['Account']['braintree_subscription_id'];
        $this->set('deactive_plan', $account_information['Account']['hide_plan_links']);
        $embeded_enable = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "enable_embeded_link"
        )));
        $this->set('is_embeded_enable', empty($embeded_enable['AccountMetaData']['meta_data_value']) ? '0' : $embeded_enable['AccountMetaData']['meta_data_value']);

        $sampleHuddles = $this->AccountFolder->find('all', array(
            'conditions' => array(
                'active' => 1,
                'is_sample' => 1,
                'account_id' => $account_id,
                'folder_type' => 1
            )
        ));
        if (empty($sampleHuddles)) {
            $this->set('sample_huddle_not_exists', true);
        } else {
            $this->set('sample_huddle_not_exists', false);
        }

        if ($loggedInUser['accounts']['is_suspended'] == 1) {
            if ($tab == 1) {
                $this->set('totalUsers', $this->User->getTotalUsers($account_id));
                $this->render('plans');
            }
        } else {
            if ($tab == 1) {
                $this->check_is_account_expired();
                $this->set('user_id', $user_id);
                $get_framwork_value = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                        "account_folder_id" => $account_id,
                        "meta_data_name" => "enable_framework_standard"
                )));
                $this->set('framwork_value', empty($get_framwork_value['AccountFolderMetaData']['meta_data_value']) ? '0' : $get_framwork_value['AccountFolderMetaData']['meta_data_value']);
                $get_tag_value = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                        "account_folder_id" => $account_id,
                        "meta_data_name" => "enable_tags"
                )));
                $this->set('tag_value', empty($get_tag_value['AccountFolderMetaData']['meta_data_value']) ? '0' : $get_tag_value['AccountFolderMetaData']['meta_data_value']);

                $get_matric_value = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "enable_matric"
                )));
                $this->set('matric_value', empty($get_matric_value['AccountMetaData']['meta_data_value']) ? '0' : $get_matric_value['AccountMetaData']['meta_data_value']);

                $get_ch_tracker = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "enable_tracker"
                )));
                $this->set('ch_tracker_value', empty($get_ch_tracker['AccountMetaData']['meta_data_value']) ? '0' : $get_ch_tracker['AccountMetaData']['meta_data_value']);

                $enabletracking = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "enabletracking"
                )));
                $this->set('enabletracking', empty($enabletracking['AccountMetaData']['meta_data_value']) ? '0' : $enabletracking['AccountMetaData']['meta_data_value']);

                $enableanalytics = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "enableanalytics"
                )));
                $this->set('enableanalytics', empty($enableanalytics['AccountMetaData']['meta_data_value']) ? '0' : $enableanalytics['AccountMetaData']['meta_data_value']);


                $get_video_value = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "enable_video_library"
                )));
                $this->set('video_value', empty($get_video_value['AccountMetaData']['meta_data_value']) ? '0' : $get_video_value['AccountMetaData']['meta_data_value']);
                $get_analytics_value = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "analytics_duration"
                )));
                $this->set('duration_value', empty($get_analytics_value['AccountMetaData']['meta_data_value']) ? '08' : $get_analytics_value['AccountMetaData']['meta_data_value']);

                $get_tracking_value = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "tracking_duration"
                )));
                $this->set('tracking_value', empty($get_tracking_value['AccountMetaData']['meta_data_value']) ? '48' : $get_tracking_value['AccountMetaData']['meta_data_value']);

                $get_embed_link = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "external_embed_link"
                )));
                $this->set('external_embeded_link', empty($get_embed_link['AccountMetaData']['meta_data_value']) ? "" : $get_embed_link['AccountMetaData']['meta_data_value']);
                $get_tags = $this->AccountTag->find("all", array("conditions" => array(
                        "account_id" => $account_id,
                        "tag_type" => 1,
                )));
                $get_st_tags = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id
                )));

                $get_metrics = $this->AccountMetaData->find("all", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name like 'metric_value_%'",
                )));

                $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '2', "account_id" => $account_id
                )));
                if (!empty($frameworks) && count($frameworks) > 0)
                    $this->set('frameworks', $frameworks);

                $get_framework_value = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "default_framework"
                )));
                $this->set('default_framework', empty($get_framework_value['AccountMetaData']['meta_data_value']) ? '' : $get_framework_value['AccountMetaData']['meta_data_value']);

                $this->set('tags', $get_tags);
                $this->set('metrics', $get_metrics);
                $this->set('standards', $get_st_tags);
                $this->set('allAccountOwner', $this->accountOwnersList($account_id));

                $this->render('index');
            } elseif ($tab == 2) {
                $this->check_is_account_expired();
                $this->render('colors-logo');
            }
        }

        if ($tab == 3) {
            $this->set('totalUsers', $this->User->getTotalUsers($account_id));
            $this->render('plans');
        } elseif ($tab == 5) {
            $this->set('braintree_subscription_id', $braintree_subscription_id);
            $this->render('cancel-account');
        } elseif ($tab == 6 && empty($sampleHuddles)) {
            $this->render('reset-sample-huddle');
        } elseif ($tab == 7) {
            $this->render('archiving');
        }
    }

    function getMyAccount($account_id) {
        $joins = array(
            'table' => 'plans',
            'type' => 'left',
            'conditions' => array('Account.plan_id = plans.id')
        );
        $fields = array('plans.per_year', 'plans.category', 'plans.plan_type', 'plans.storage', 'plans.id', 'plans.users', 'Account.*');
        $result = $this->Account->find('first', array(
            'joins' => array($joins),
            'conditions' => array('Account.id' => $account_id),
            'fields' => $fields
        ));

        return $result;
    }

    function plansList() {
        $plans = $this->Plans->getAll();
        $plan_dropdown = FALSE;
        if ($plans && count($plans) > 0) {
            foreach ($plans as $row) {
                if ($row['Plans']['per_year'] == '1') {
                    $price = "\${$row['Plans']['price']}.0/yr";
                } else {
                    $price = "\${$row['Plans']['price']}.0/mo";
                }
                $plan_dropdown[$row['Plans']['id']] = $row['Plans']['plan_type'] . ' (' . $row['Plans']['users'] . ' Users, ' . $row['Plans']['storage'] . ' GBs, ' . $price . ')';
            }
        }

        return $plan_dropdown;
    }

    function accountOwnersList($account_id) {
        $result = $this->User->getUsersByRole($account_id, array('110', '100'));
        $view = new View($this, false);
        $accountOwnerList = array();
        if ($result) {
            foreach ($result as $row) {
                $accountOwnerList[$row['User']['id']] = $row['User']['first_name'] . " " . $row['User']['last_name'] . " (" . $view->Custom->get_user_role_name($row['users_accounts']['role_id']) . ")";
            }
        }

        return $accountOwnerList;
    }

    function changeAccountOwner($account_id, $tab) {
        if ($this->data != '') {

            $account_owner_user_id = $this->data['account_owner_user_id'];
            $super_admin_user_id = $this->data['user_id'];
            if ($account_owner_user_id == '' || $super_admin_user_id == '') {
                $this->Session->setFlash($this->language_based_messages['please_select_AO_SU_role_swaping_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . $tab);
            }

            $account_owner_role = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $account_owner_user_id)));
            if ($account_owner_role['UserAccount']['role_id'] != '100') {
                $this->Session->setFlash($this->language_based_messages['only_account_owner_can_swap_roles_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . $tab);
            }

            //super_admin_user_id
            $super_admin_role = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $super_admin_user_id)));
            $data1 = array(
                'role_id' => "'" . $super_admin_role['UserAccount']['role_id'] . "'",
                'permission_maintain_folders' => '1',
                'permission_access_video_library' => '1',
                'permission_video_library_upload' => '1',
                'permission_administrator_user_new_role' => '1',
                'manage_collab_huddles' => '1',
                'manage_coach_huddles' => '1',
                'folders_check' => '1',
                'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
            );
            $this->UserAccount->updateAll($data1, array('user_id' => $account_owner_user_id, 'account_id' => $account_id));

            if ($this->UserAccount->getAffectedRows() > 0) {
                $data2 = array(
                    'role_id' => "'" . $account_owner_role['UserAccount']['role_id'] . "'",
                    'permission_maintain_folders' => '1',
                    'permission_access_video_library' => '1',
                    'permission_video_library_upload' => '1',
                    'permission_administrator_user_new_role' => '1',
                    'manage_collab_huddles' => '1',
                    'manage_coach_huddles' => '1',
                    'folders_check' => '1',
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->UserAccount->updateAll($data2, array('user_id' => $super_admin_user_id, 'account_id' => $account_id));

                if ($this->UserAccount->getAffectedRows() > 0) {

                    $this->setupPostLogin(true);
                    $this->Session->setFlash($this->language_based_messages['account_owner_changed_successfully_msg'], 'default', array('class' => 'message success'));
                    $this->redirect("/");
                } else {

                    $data1 = array(
                        'role_id' => "'" . $account_owner_role['UserAccount']['role_id'] . "'",
                        'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                    );
                    $this->UserAccount->updateAll($data1, array('user_id' => $account_owner_user_id, 'account_id' => $account_id));
                    $this->Session->setFlash($this->language_based_messages['please_select_different_person_msg'].$this->custom->get_site_settings('static_emails')['support'], 'default', array('class' => 'message error'));
                    $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . $tab);
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['please_select_different_person_msg'].$this->custom->get_site_settings('static_emails')['support'], 'default', array('class' => 'message error'));
                $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . $tab);
            }
        }
    }

    function tab_2_edit($account_id) {
        $data = array(
            'updated_at' => "'" . date('Y-m-d H:i:s') . "'"
        );
        if ($this->data['account']['header_background_color'] != '') {
            $data['header_background_color'] = "'" . $this->data['account']['header_background_color'] . "'";
        }
        if ($this->data['account']['nav_bg_color'] != '') {
            $data['usernav_bg_color'] = "'" . $this->data['account']['nav_bg_color'] . "'";
        }

        if (isset($_FILES['account']) && $_FILES['account']['error']['image_logo'] == 0) {
            $fileName = $this->upload_company_logo($_FILES['account'], $account_id);
            if ($fileName) {
                $data['image_logo'] = "'" . $fileName . "'";
            }
        }
        $this->Account->updateAll($data, array('id' => $account_id));

        $this->setupPostLogin();
        $this->switch_account($account_id);

        if ($this->Account->getAffectedRows() > 0) {
            $this->Session->setFlash($this->language_based_messages['you_have_successfully_changed_your_info_msg'], 'default', array('class' => 'message success'));
            $this->redirect('/accounts/account_settings_all/' . $account_id . '/2');
        } else {
            $this->Session->setFlash($this->language_based_messages['your_info_changing_failed_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/accounts/account_settings_all/' . $account_id . '/2');
        }
    }

    function reset_custom_theme($account_id) {
        $data = array(
            'updated_at' => "'" . date('Y-m-d H:i:s') . "'"
        );
        if ($this->site_id == 1) {
            $data['header_background_color'] = "'004061'";
        } else {
            $data['header_background_color'] = "'7b7e81'";
        }

        $data['nav_bg_color'] = "'668dab'";
        $data['image_logo'] = "''";
        $data['usernav_bg_color'] = "'fff'";

        $this->Account->updateAll($data, array('id' => $account_id));

        $this->setupPostLogin();
        $this->switch_account($account_id);

        if ($this->Account->getAffectedRows() > 0) {
            $this->Session->setFlash($this->language_based_messages['you_have_successfully_changed_your_info_msg'], 'default', array('class' => 'message success'));
            //$this->redirect('/accounts/accountSettings/' . $account_id . '/2');
            $this->redirect('/accounts/account_settings_all/' . $account_id . '/2');
        } else {
            $this->Session->setFlash($this->language_based_messages['your_info_changing_failed_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/accounts/account_settings_all/' . $account_id . '/2');
        }
    }

    function tab_1_edit($account_id) {
        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        $data = array(
            'company_name' => "'" . Sanitize::escape(htmlspecialchars($this->data['account']['company_name'])) . "'",
            'updated_at' => "'" . date('Y-m-d H:i:s') . "'"
        );

        $standard_value = $this->request->data['txtchkframework'];
        $default_framework_value = $this->request->data['defaultframework'];
        $duration_value = $this->request->data['txtanalyticsduration'];
        $tracking_value = $this->request->data['txttrackingduration'];
        $tag_value = $this->request->data['txtchktags'];
        $matric_value = $this->request->data['txtchkmatric'];
        $ch_tracker_value = $this->request->data['txtchktracker'];
        $video_value = $this->request->data['txtchkvideos'];
        $link_value = addslashes($this->request->data['txtembedlink']);
        $enabletracking = $this->request->data['enabletracking'];
        $enableanalytics = $this->request->data['enableanalytics'];

        $txtchkenablewstags = $this->request->data['txtchkenablewstags'];
        $txtchkenablewsframework = $this->request->data['txtchkenablewsframework'];

        $new_tags = isset($this->request->data['tags']) ? $this->request->data['tags'] : '';
        $new_metrics = isset($this->request->data['metrics']) ? $this->request->data['metrics'] : '';
        $auto_delete = $this->request->data['auto_delete'];
        $this->Account->updateAll($data, array('id' => $account_id));
        if ($this->Account->getAffectedRows() > 0) {
            if ($e1 = $this->updatefrmework($account_id, $standard_value) && $e2 = $this->updatetags($account_id, $tag_value) && $e3 = $this->updatevideos($account_id, $video_value) && $e4 = $this->updateAnalyticsDuration($account_id, $duration_value) && $e5 = $this->updateEmbedLink($account_id, $link_value) && $e6 = $this->updateMatric($account_id, $matric_value) && $e7 = $this->updateTracker($account_id, $ch_tracker_value) && $e8 = $this->updateTrackingDuration($account_id, $tracking_value) && $e9 = $this->updateDefaultFramework($account_id, $default_framework_value) && $e10 = $this->enabletrack($account_id, $enabletracking) && $e11 = $this->enableanalytics($account_id, $enableanalytics) && $e12 = $this->enableautodelete($account_id, $auto_delete) && $e13 = $this->updatetagsws($account_id, $txtchkenablewstags) && $e14 = $this->updatefrmeworkforws($account_id, $txtchkenablewsframework)) {
                if (!empty($new_tags)) {
                    foreach ($new_tags as $tag) {
                        foreach ($tag as $key => $value) {
                            if (!empty($value)) {
                                if (preg_match('/nw/', $key)) {
                                    $this->AccountTag->create();
                                    $save_tags = array(
                                        "account_id" => $account_id,
                                        "tag_type" => '1',
                                        "tag_title" => $value,
                                        "created_by" => $current_user_id,
                                        "created_date" => date('Y-m-d H:i:s'),
                                        "last_edit_by" => $current_user_id,
                                        "last_edit_date" => date('Y-m-d H:i:s')
                                    );
                                    $this->AccountTag->save($save_tags);
                                } else {
                                    $save_tags = array(
                                        "account_id" => $account_id,
                                        "tag_type" => '1',
                                        "tag_title" => "'" . addslashes($value) . "'",
                                        "last_edit_by" => $current_user_id,
                                        "last_edit_date" => "'" . addslashes(date('Y-m-d H:i:s')) . "'"
                                    );
                                    $this->AccountTag->updateAll($save_tags, array('account_tag_id' => $key));
                                }
                            } else {
                                if (!preg_match('/nw/', $key)) {
                                    $this->AccountTag->deleteAll(array('account_tag_id' => $key), $cascade = true, $callbacks = true);
                                }
                            }
                        }
                    }
                }


                // print_r($new_metrics);die;
                if (!empty($new_metrics)) {
                    foreach ($new_metrics as $tag) {
                        foreach ($tag as $key => $value) {
                            if (!empty($value)) {
                                if (preg_match('/new/', $key)) {
                                    $this->AccountMetaData->create();
                                    $data = array(
                                        'account_id' => $account_id,
                                        'meta_data_name' => 'metric_value_' . $value,
                                        'meta_data_value' => substr($key, 4),
                                        'created_by' => $current_user_id,
                                        'created_date' => date('Y-m-d H:i:s')
                                    );
                                    $this->AccountMetaData->save($data);
                                } else {
                                    $data = array(
                                        'account_id' => $account_id,
                                        'meta_data_name' => "'" . 'metric_value_' . $value . "'",
                                        'created_by' => $current_user_id,
                                        'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                                    );
                                    $this->AccountMetaData->updateAll($data, array('account_meta_data_id' => $key));
                                }
                            } else {
                                if (!preg_match('/new/', $key)) {
                                    $this->AccountMetaData->deleteAll(array('account_meta_data_id' => $key), $cascade = true, $callbacks = true);
                                    $this->DocumentStandardRating->deleteAll(array('rating_id' => $key), $cascade = true, $callbacks = true);
                                }
                            }
                        }
                    }
                }


                $this->Session->setFlash($this->language_based_messages['you_have_successfully_changed_your_info_msg'], 'default', array('class' => 'message success'));
                //$this->redirect('/accounts/accountSettings/' . $account_id . '/' . $this->data['tab']);
                $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . $this->data['tab']);
            } else {
                $this->Session->setFlash($e1 . $e2 . $e3, 'default', array('class' => 'message error'));
                //$this->redirect('/accounts/accountSettings/' . $account_id . '/' . $this->data['tab']);
                $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . $this->data['tab']);
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['your_info_changing_failed_msg'], 'default', array('class' => 'message error'));
            // $this->redirect('/accounts/accountSettings/' . $account_id . '/' . $this->data['tab']);
            $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . $this->data['tab']);
        }
    }

    function upload($file) {
        if (isset($file['error']) && $file['error']['image_logo'] == 0) {
            $result = explode('.', $file['name']['image_logo']);
            $ext = $result[1];
            $newFile = $result[0];
            $new_image = ereg_replace("[^A-Za-z0-9]", "", $newFile) . "." . $ext;

            $filename = $this->uploadDir . Inflector::slug(pathinfo($new_image, PATHINFO_FILENAME)) . '.' . pathinfo($new_image, PATHINFO_EXTENSION);

            if (!move_uploaded_file($file['tmp_name']['image_logo'], $filename)) {
                return FALSE;
            } else {
                return $new_image;
            }
        } else {
            $this->Session->setFlash(__($this->language_based_messages['please_upload_file_msg'], true));
            return FALSE;
        }
    }

    function cancel_subscription($account_id) {

        $subscription = new Subscription();

        if (empty($account_id)) {
            $this->Session->setFlash($this->language_based_messages['please_select_account_for_cancel_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/accounts/account_settings_all/' . $account_id . '/6');
        }
        $result = $this->getMyAccount($account_id);

        if (is_array($result) && count($result) > 0) {

            if ($result['Account']['in_trial'] != 1) {
                $subscription->cancel_subscription($result['Account']['braintree_subscription_id']);
            }

            $this->Account->updateAll(array(
                'braintree_subscription_id' => '"canceled"',
                'is_suspended' => 1,
                'suspended_at' => '"' . date("Y-m-d H:i:s") . '"'
                    ), array('id' => $account_id));

            $UserAccounts = $this->UserAccount->find('all', array(
                'conditions' => array('UserAccount.account_id' => $result['Account']['id'])
            ));

            for ($i = 0; $i < count($UserAccounts); $i++) {

                $UserAccount = $UserAccounts[$i];
                $OtherAccounts = $this->UserAccount->find('all', array(
                    'conditions' => array(
                        'UserAccount.user_id' => $UserAccount['UserAccount']['user_id'],
                        'NOT' => array(
                            'UserAccount.account_id' => $account_id
                        )
                    )
                ));

                if (count($OtherAccounts) <= 0) {
                    $user = $this->User->find('first', array(
                        'conditions' => array('User.id' => $UserAccount['UserAccount']['user_id'])
                    ));

                    $data = array(
                        'is_active' => '0'
                    );
                    $this->User->updateAll($data, array('id' => $user['User']['id']), $validation = TRUE);
                }
            }

            $this->cancel_account();
            $this->Session->setFlash($this->language_based_messages['your_account_has_been_canceled_successfully_msg'], 'default', array('class' => 'message success'));
            $this->Cookie->delete('remember_me_cookie');
            $this->Auth->logout();
            return $this->redirect('/');
        } else {
            $this->Session->setFlash($this->language_based_messages['invalid_account_please_select_valid_msg'], 'default', array('class' => 'message error'));

            $this->redirect('/accounts/account_settings_all/' . $account_id . '/6');
        }
    }

    function account_expired() {
        $user_current_account = $this->Session->read('user_current_account');
        $result = $this->Account->find('first', array('conditions' => array('id' => $user_current_account['accounts']['account_id'], 'in_trial' => '1')));
        $account_create_date = $result['Account']['created_at'];
        $this->Session->write('account_created_at', $account_create_date);
        $trial_duration = Configure::read('trial_duration');
        $account_end_date = date('Y-m-d H:i:s', strtotime('-' . $trial_duration . ' month', strtotime($account_create_date)));
        $this->Account->updateAll(array('created_at' => "'" . $account_end_date . "'", 'is_suspended' => '0'), array('id' => $result['Account']['id']), $validation = TRUE);
        $this->Cookie->delete('remember_me_cookie');
        $this->Auth->logout();
        return $this->redirect('/');
    }

//    public function update_tags() {
//        /*
//         * 1 for framwork and
//         * 2 for tags
//         */
//
//        $this->layout = null;
//        $request_type = $this->request->data['type'];
//        $account_id = $this->request->data['account_id'];
//        $value = $this->request->data['value'];
//        $loggedInUser = $this->Session->read('user_current_account');
//        $current_user_id = $loggedInUser['User']['id'];
//        if ($request_type == '1') {
//            $check_data = $this->AccountFolderMetaData->find("first", array("conditions" => array(
//                    "account_folder_id" => $account_id,
//                    "meta_data_name" => "enable_framework_standard"
//            )));
//            if (!empty($check_data)) {
//                $data = array(
//                    'meta_data_value' => $value,
//                    'last_edit_by' => $current_user_id,
//                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
//                );
//                $this->AccountFolderMetaData->updateAll($data, array('account_folder_id' => $account_id, 'meta_data_name' => 'enable_framework_standard'));
//            } else {
//                $data = array(
//                    'account_folder_id' => $account_id,
//                    'meta_data_name' => 'enable_framework_standard',
//                    'meta_data_value' => $value,
//                    'created_by' => $current_user_id,
//                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
//                );
//                $new_user_account_id = $this->AccountFolderMetaData->save($data);
//            }
//        } else {
//            $check_data = $this->AccountFolderMetaData->find("first", array("conditions" => array(
//                    "account_folder_id" => $account_id,
//                    "meta_data_name" => "enable_tags"
//            )));
//            if (!empty($check_data)) {
//                $data = array(
//                    'meta_data_value' => $value,
//                    'last_edit_by' => $current_user_id,
//                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
//                );
//                $this->AccountFolderMetaData->updateAll($data, array('account_folder_id' => $account_id, 'meta_data_name' => 'enable_tags'));
//            } else {
//                $data = array(
//                    'account_folder_id' => $account_id,
//                    'meta_data_name' => 'enable_tags',
//                    'meta_data_value' => $value,
//                    'created_by' => $current_user_id,
//                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
//                );
//                $new_user_account_id = $this->AccountFolderMetaData->save($data);
//            }
//        }
//        echo json_encode(array("status" => "ok"));
//        die();
//    }
    function updatevideos($account_id, $video_value) {
        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];

        $admin = $this->User->getUsersByAccount($account_id, 120);


        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "enable_video_library"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $video_value,
                    'last_edit_by' => $current_user_id,
                        //     'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'enable_video_library'));
            } else {
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'enable_video_library',
                    'meta_data_value' => $video_value,
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountMetaData->save($data);
            }

            if ($video_value == 1) {
                $extraPermission = array(
                    'permission_access_video_library' => $video_value,
                );
            } else {
                $extraPermission = array(
                    'permission_access_video_library' => $video_value,
                    'permission_video_library_upload' => $video_value,
                );
            }
            if ($this->AccountMetaData->getAffectedRows() > 0) {
                foreach ($admin as $row) {
                    $this->UserAccount->create();
                    $id[] = $row['User']['id'];
                    $this->UserAccount->updateAll($extraPermission, array('user_id' => $row['User']['id']));
                }
            }
            //$this->update_users_video_access($account_id);
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateAnalyticsDuration($account_id, $duration_value) {
        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "analytics_duration"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $duration_value,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'analytics_duration'));
            } else {
                $this->AccountMetaData->create();
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'analytics_duration',
                    'meta_data_value' => $duration_value,
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountMetaData->save($data);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateDefaultFramework($account_id, $default_framework_value) {
        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "default_framework"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => "'" . $default_framework_value . "'",
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'default_framework'));
            } else {
                $this->AccountMetaData->create();
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'default_framework',
                    'meta_data_value' => $default_framework_value,
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountMetaData->save($data);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateTrackingDuration($account_id, $tracking_value) {
        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "tracking_duration"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $tracking_value,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'tracking_duration'));
            } else {
                $this->AccountMetaData->create();
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'tracking_duration',
                    'meta_data_value' => $tracking_value,
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountMetaData->save($data);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateEmbedLink($account_id, $link_value) {

        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "external_embed_link"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => "'" . $link_value . "'",
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'external_embed_link'));
            } else {
                $this->AccountMetaData->create();
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'external_embed_link',
                    'meta_data_value' => "'" . $link_value . "'",
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountMetaData->save($data);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updatefrmeworkforws($account_id, $standard_value) {
        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_framework_standard_ws"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $standard_value,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountFolderMetaData->updateAll($data, array('account_folder_id' => $account_id, 'meta_data_name' => 'enable_framework_standard_ws'));
            } else {
                $data = array(
                    'account_folder_id' => $account_id,
                    'meta_data_name' => 'enable_framework_standard_ws',
                    'meta_data_value' => $standard_value,
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountFolderMetaData->save($data);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updatefrmework($account_id, $standard_value) {
        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_framework_standard"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $standard_value,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountFolderMetaData->updateAll($data, array('account_folder_id' => $account_id, 'meta_data_name' => 'enable_framework_standard'));
            } else {
                $data = array(
                    'account_folder_id' => $account_id,
                    'meta_data_name' => 'enable_framework_standard',
                    'meta_data_value' => $standard_value,
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountFolderMetaData->save($data);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function enabletrack($account_id, $enabletracking) {

        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "enabletracking"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $enabletracking,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'enabletracking'));
            } else {
                $this->AccountMetaData->create();
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'enabletracking',
                    'meta_data_value' => $enabletracking,
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountMetaData->save($data);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function enableanalytics($account_id, $enableanalytics) {

        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "enableanalytics"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $enableanalytics,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'enableanalytics'));
            } else {
                $this->AccountMetaData->create();
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'enableanalytics',
                    'meta_data_value' => $enableanalytics,
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountMetaData->save($data);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function enableautodelete($account_id, $auto_delete) {

        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "auto_delete"
            )));
            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $auto_delete,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'auto_delete'));
            } else {
                $this->AccountMetaData->create();
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'auto_delete',
                    'meta_data_value' => $auto_delete,
                    'created_by' => $current_user_id,
                    'created_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $new_user_account_id = $this->AccountMetaData->save($data);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updatetagsws($account_id, $tag_value) {

        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_tags_ws"
            )));


            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $tag_value,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountFolderMetaData->updateAll($data, array('account_folder_id' => $account_id, 'meta_data_name' => 'enable_tags_ws'));
            } else {

                $this->AccountFolderMetaData->create();
                $data = array(
                    'account_folder_id' => $account_id,
                    'meta_data_name' => 'enable_tags_ws',
                    'meta_data_value' => $tag_value,
                    'created_by' => $current_user_id,
                    'created_date' => date('Y-m-d H:i:s')
                );

                $this->AccountFolderMetaData->save($data, $validation = TRUE);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updatetags($account_id, $tag_value) {

        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_tags"
            )));


            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $tag_value,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountFolderMetaData->updateAll($data, array('account_folder_id' => $account_id, 'meta_data_name' => 'enable_tags'));
            } else {

                $this->AccountFolderMetaData->create();
                $data = array(
                    'account_folder_id' => $account_id,
                    'meta_data_name' => 'enable_tags',
                    'meta_data_value' => $tag_value,
                    'created_by' => $current_user_id,
                    'created_date' => date('Y-m-d H:i:s')
                );

                $this->AccountFolderMetaData->save($data, $validation = TRUE);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateMatric($account_id, $matric_value) {

        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "enable_matric"
            )));


            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $matric_value,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'enable_matric'));
            } else {

                $this->AccountMetaData->create();
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'enable_matric',
                    'meta_data_value' => $matric_value,
                    'created_by' => $current_user_id,
                    'created_date' => date('Y-m-d H:i:s')
                );

                $this->AccountMetaData->save($data, $validation = TRUE);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function updateTracker($account_id, $ch_tracker_value) {

        $loggedInUser = $this->Session->read('user_current_account');
        $current_user_id = $loggedInUser['User']['id'];
        try {
            $check_data = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "enable_tracker"
            )));


            if (!empty($check_data)) {
                $data = array(
                    'meta_data_value' => $ch_tracker_value,
                    'last_edit_by' => $current_user_id,
                    'last_edit_date' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountMetaData->updateAll($data, array('account_id' => $account_id, 'meta_data_name' => 'enable_tracker'));
            } else {

                $this->AccountMetaData->create();
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'enable_tracker',
                    'meta_data_value' => $ch_tracker_value,
                    'created_by' => $current_user_id,
                    'created_date' => date('Y-m-d H:i:s')
                );

                $this->AccountMetaData->save($data, $validation = TRUE);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function update_users_video_access($account_id) {

        $admin = $this->User->getUsersByAccount($account_id, 120);
        if (is_array($admin) && count($admin) > 0) {
            echo '<pre>';
            foreach ($admin as $row) {
                print_r($row['User']['id']);
                $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle['AccountFolder']['account_folder_id'], 'meta_data_name' => 'folder_type'), 'fields' => array('meta_data_value')));
                if (empty($h_type)) {
                    $h_type['AccountFolderMetaData'] = array("meta_data_value" => 1);
                }
            }
            die;
        }
    }

    public function get_folder_size() {
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name');
        $s3 = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));
        $result = $this->Account->find('all', array('conditions' => array('is_active' => '1')));
        if ($result) {
            foreach ($result as $row) {
                $account_id = $row['Account']['id'];
                $size = 0;
                $folder = 'uploads/' . $account_id;
                $objects = $s3->getIterator('ListObjects', array(
                    "Bucket" => $aws_bucket,
                    "Prefix" => $folder
                ));
                if ($objects) {
                    foreach ($objects as $object) {
                        $size = $size + $object['Size'];
                    }
                }
                $data = array(
                    'storage_used' => $size
                );

                $this->Account->updateAll($data, array('id' => $account_id));
            }
        }
        die;
    }

    function account_settings_main($account_id) {
        return $this->redirect('/home/account-settings/', 301);
        $users = $this->Session->read('user_current_account');
        
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('accounts/account_settings_main');
        $this->set('language_based_content',$language_based_content);

        if (!($account_id == $users['users_accounts']['account_id']) || !($users['users_accounts']['role_id'] == 100 || $users['users_accounts']['role_id'] == 110 )) {
            $this->no_permissions();
        }

        $result = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $this->is_account_activated($account_id, $users['User']['id']);
//        $user_count = $this->UserAccount->find('all', array('conditions' => array('account_id' => $account_id)));
//        $user_count = count($user_count);
//        $user_count = $user_count - 3;

        $sampleHuddles = $this->AccountFolder->find('all', array(
            'conditions' => array(
                'active' => 1,
                'is_sample' => 1,
                'account_id' => $account_id,
                'folder_type' => 1
            )
        ));
        if (empty($sampleHuddles)) {
            $this->set('sample_huddle_not_exists', true);
        } else {
            $this->set('sample_huddle_not_exists', false);
        }


        $this->set('account_id', $account_id);
        $this->set('site_id', $this->site_id);
        $myAccount = $this->getMyAccount($account_id);
        $this->set('deactive_plan', $myAccount['Account']['deactive_plan']);
        $this->set('emitplans', array(17, 18, 13, 14, 9, 10));
        $this->set('myAccount', $this->getMyAccount($account_id));
        $this->set('company_name', $result['Account']['company_name']);
        $this->set('user_count', $this->User->getTotalUsers($account_id));
        $this->render('account_settings_main');
    }

    public function auto_login($user_id, $account_id, $account_folder_id = '') {

        $user = $this->User->findById($user_id);
        $user = $user['User'];

        if ($this->Auth->login($user)) {
            $sibme_base_url = Configure::read('sibme_base_url');

            $this->Cookie->delete('remember_me_cookie');
            $this->setupPostLogin(true);
            if ($account_folder_id != '') {
                //$this->Session->setFlash(__('You are not allowed to create new Huddles in this account. To request permission to do so, or if you have questions, please <a id="pop-up-btn" data-original-title="User Permissions" rel="tooltip" data-toggle="modal" data-target="#addPermissionModal" href="#">contact</a> your Account Owner.'));
                $redirect_url = '/Huddles/view/' . $account_folder_id;
            } else {
                $redirect_url = $sibme_base_url . '/accounts/account_settings_all/' . $account_id . '/4';
            }
            return $this->redirect($redirect_url);
        }
    }

    function account_settings_all($account_id, $tab = '', $sub_tab = 1, $auth_token = '', $framework_id = '') {
//        $client = new IntercomClient('dG9rOjg4MjgyOGJmX2Q2MWRfNDM3OF9hNWNjXzQzODU2ZjliMDZlYjoxOjA=', null);
//
//
//       $result = $client->users->getUsers(["email" => "leonk314314@gmail.com"]);
//       $meta_data = array(
//         'first_meta' => 'don',
//         'second_meta' => 'saad',
//
//     );
//
//       // print_r($result->id);
// $client->events->create([
//  "event_name" => "testing",
//   'status' => 'true'  ,
//  "created_at" => time(),
//  "email" => "leonk314314@gmail.com",
//     "metadata" => $meta_data
//]);
//
//die;
        /*
          if($sub_tab > 2 && !$this->request->is('post')){
          $this->Session->setFlash('Currently you have chose incorrect framework, please select correct framework to add/edit standards and performance level', 'default', array('class' => 'message error'));
          $this->redirect('/accounts/account_settings_all/' . $account_id . '/7/2');
          } */
        
        
        if($tab == '1')
        {
            return $this->redirect('/home/account-settings/general-settings/', 301);
        }
        
        if($tab == '2')
        {
            return $this->redirect('/home/account-settings/colors-logo', 301);
        }
        
        if($tab == '3')
        {
           return $this->redirect('/home/account-settings/archiving', 301); 
        }
        
        if($tab == '8')
        {
           return $this->redirect('/home/account-settings/global-export', 301); 
        }
        
        if($tab == '4' || $tab == '5')
        {
           return $this->redirect('/home/account-settings/plans', 301);
        }
        
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('accounts/account_settings_all');
        $this->set('language_based_content',$language_based_content);
        
        

        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );


        $frameworks = $this->AccountTag->find("all", array(
            "conditions" => array(
                "tag_type" => '2',
                "account_id" => $account_id,
            ),
            'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'a.id = AccountTag.account_id'
                )
            ),
            'fields' => array(
                'AccountTag.*',
                'a.company_name'
            )
        ));

        if (!empty($auth_token) && $auth_token != 0) {
            $users_info = $this->User->find('first', array(
                'conditions' => array(
                    'authentication_token' => $auth_token,
                )
            ));

            $this->auto_login($users_info['User']['id'], $account_id);
        }


        $users = $this->Session->read('user_current_account');

        if (!($account_id == $users['users_accounts']['account_id']) || !($users['users_accounts']['role_id'] == 100 || $users['users_accounts']['role_id'] == 110)) {
            $this->no_permissions();
        }


        $subscription_data = $this->Session->read('subscription_data');
        $myAccount = $this->getMyAccount($account_id);
        $this->set('deactive_plan', $myAccount['Account']['deactive_plan']);
        if ($subscription_data == '' && $tab == 5 && $myAccount['Account']['in_trial'] == 1) {
            $this->Session->setFlash($language_based_content['you_are_in_trial_mode_all'], 'default', array('class' => 'message error'));
            $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '4');
        }

        $today_date = date('Y-m-d');

        $subscription_data_table = $this->AccountBraintreeSubscription->find("first", array(
            "conditions" => array(
                "account_id" => $account_id,
                "Subscription_start_date <=" => $today_date,
                "Subscription_end_date >=" => $today_date
            ),
            "order" => 'id DESC'
        ));

        if (!empty($subscription_data_table)) {
            $plane_data = array(
                //'braintree_subscription_id' => "'" .$subscription_data_table['AccountBraintreeSubscription']['braintree_subscription_id'] . "'" ,
                'updated_at' => "'" . date('Y-m-d H:i:s') . "'",
                'plan_id' => $subscription_data_table['AccountBraintreeSubscription']['plan_id'],
                'plan_qty' => $subscription_data_table['AccountBraintreeSubscription']['number_of_users']
            );
            $this->Account->updateAll($plane_data, array('id' => $account_id));
        }


        if ($subscription_data != '') {
            $plans = $this->Plans->find("first", array("conditions" => array(
                    "id" => $subscription_data['plan_id']
            )));
            $this->set('subscription_data', $subscription_data);
            $this->set('selected_plan', $plans);
        }
        $this->set('framework_id', '');

        $current_level_tire = 4;
        $current_level_checkbox = 3;

        if ($framework_id != '') {
            $framework_settings_details = $this->AccountFrameworkSetting->find("first", array("conditions" => array(
                    "account_tag_id" => $framework_id,
            )));

            $current_level_tire = $framework_settings_details['AccountFrameworkSetting']['tier_level'];
            $current_level_checkbox = $framework_settings_details['AccountFrameworkSetting']['checkbox_level'];

            $framework_settings_id = $framework_settings_details['AccountFrameworkSetting']['id'];

            if ($framework_settings_details['AccountFrameworkSetting']['enable_ascending_order'] == '1') {

                $framework_settings_performance_level = $this->AccountFrameworkSettingPerformanceLevel->find("all", array("conditions" => array(
                        "account_framework_setting_id" => $framework_settings_id,
                    ),
                    "order" => 'performance_level_rating ASC'
                ));
            } else {

                $framework_settings_performance_level = $this->AccountFrameworkSettingPerformanceLevel->find("all", array("conditions" => array(
                        "account_framework_setting_id" => $framework_settings_id,
                    ),
                    "order" => 'performance_level_rating DESC'));
            }


            $account_tags_with_unqie_desc = array();
            $rubircs_details = array();
            $account_tag_details_head = $this->AccountTag->find("all", array("conditions" => array(
                    "framework_id" => $framework_id,
                    "parent_account_tag_id IS NULL"
            )));
            if (count($account_tag_details_head) > 0) {

                foreach ($account_tag_details_head as $row) {
                    if (!empty($row['AccountTag']['tag_title'])) {
                        $row['AccountTag']['tag_html'] = strip_tags($row['AccountTag']['tag_title']);
                    }
                    $rubircs_details[] = $row;
                    $parent_account_tag_id = $row['AccountTag']['account_tag_id'];
                    $account_tag_details_child = $this->AccountTag->find("all", array("conditions" => array(
                            "framework_id" => $framework_id,
                            "parent_account_tag_id" => $parent_account_tag_id
                    )));
                    if (count($account_tag_details_child) > 0) {
                        foreach ($account_tag_details_child as $row_p) {
                            $rubircs_details[] = $row_p;
                        }
                    }
                }
            } else {
                $account_tag_details_f = $this->AccountTag->find("all", array("conditions" => array(
                        "framework_id" => $framework_id,
                )));
                if (count($account_tag_details_f) > 0) {
                    foreach ($account_tag_details_f as $row_np) {
                        $rubircs_details[] = $row_np;
                    }
                }
            }
            $account_tag_details = $rubircs_details;

            if ($framework_id != '' && count($framework_settings_details) <= 0) {
                $this->Session->setFlash($this->language_based_messages['rubric_name_cannot_be_empty_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/accounts/account_settings_all/' . $account_id . '/7/2');
            }

            if (!empty($account_tag_details)) {

                foreach ($account_tag_details as $key => $at) {

                    $account_tags_with_unqie_desc[$key]['AccountTagDetail'] = $at;

                    if ($framework_settings_details['AccountFrameworkSetting']['enable_ascending_order'] == '1') {
                        $peformance_level_descs = $this->PerformanceLevelDescription->find("all", array("conditions" => array(
                                "account_tag_id" => $at['AccountTag']['account_tag_id'],
                            ),
                            "order" => 'id DESC'
                        ));
                    } else {
                        $peformance_level_descs = $this->PerformanceLevelDescription->find("all", array("conditions" => array(
                                "account_tag_id" => $at['AccountTag']['account_tag_id'],
                            ),
                            "order" => 'id ASC'
                        ));
                    }

                    if (!empty($peformance_level_descs)) {
                        $account_tags_with_unqie_desc[$key]['performace_level_descriptions'] = $peformance_level_descs;
                    }
                }
            }
            $details_data = array();
            if (count($account_tags_with_unqie_desc) > 0) {
                foreach ($account_tags_with_unqie_desc as $row) {
                    $row['AccountTagDetail']['AccountTag']['tag_html'] = strip_tags($row['AccountTagDetail']['AccountTag']['tag_html']);
                    $details_data[] = $row;
                }
            }

            $account_tags_with_unqie_desc = $details_data;
            $this->set('account_tags_with_unqie_desc', $account_tags_with_unqie_desc);
            $this->set('framework_settings_details', $framework_settings_details);
            $this->set('framework_settings_performance_level', $framework_settings_performance_level);
            $this->set('framework_id', $framework_id);
        }

        $user_id = $users['User']['id'];
        $this->set('user_id', $user_id);
        $this->set('myAccount', $this->getMyAccount($account_id));
        $this->set('allAccountOwner', $this->accountOwnersList($account_id));
        $this->set('account_id', $account_id);
        $this->set('tab', $tab);
        $this->set('sub_tab', $sub_tab);
        $this->set('current_level_tier', $current_level_tire);
        $this->set('current_level_checkbox', $current_level_checkbox);
        $this->set('customer_info', $this->_get_card_informaiton($account_id));
        $this->set('new_plan', $this->Session->read('subscription_data'));
        $this->set('totalUsers', $this->User->getTotalUsers($account_id));
        $this->set('transactions', $this->fetch_transactions_details($account_id));
        $this->set('subscription_details', $this->fetch_subscirption_details($account_id));
        $this->set('subscription_data_table', $subscription_data_table);
        $this->set('framework_list', $frameworks);

        $get_framwork_value_ws = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                "account_folder_id" => $account_id,
                "meta_data_name" => "enable_framework_standard_ws"
        )));
        $this->set('chkenablewsframework', empty($get_framwork_value_ws['AccountFolderMetaData']['meta_data_value']) ? '0' : $get_framwork_value_ws['AccountFolderMetaData']['meta_data_value']);
        $get_framwork_value = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                "account_folder_id" => $account_id,
                "meta_data_name" => "enable_framework_standard"
        )));
        $this->set('framwork_value', empty($get_framwork_value['AccountFolderMetaData']['meta_data_value']) ? '0' : $get_framwork_value['AccountFolderMetaData']['meta_data_value']);
        $get_tag_value_ws = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                "account_folder_id" => $account_id,
                "meta_data_name" => "enable_tags_ws"
        )));
        $this->set('chkenablewstags', empty($get_tag_value_ws['AccountFolderMetaData']['meta_data_value']) ? '0' : $get_tag_value_ws['AccountFolderMetaData']['meta_data_value']);
        $get_tag_value = $this->AccountFolderMetaData->find("first", array("conditions" => array(
                "account_folder_id" => $account_id,
                "meta_data_name" => "enable_tags"
        )));
        $this->set('tag_value', empty($get_tag_value['AccountFolderMetaData']['meta_data_value']) ? '0' : $get_tag_value['AccountFolderMetaData']['meta_data_value']);

        $get_matric_value = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "enable_matric"
        )));
        $this->set('matric_value', empty($get_matric_value['AccountMetaData']['meta_data_value']) ? '0' : $get_matric_value['AccountMetaData']['meta_data_value']);

        $get_ch_tracker = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "enable_tracker"
        )));
        $this->set('ch_tracker_value', empty($get_ch_tracker['AccountMetaData']['meta_data_value']) ? '0' : $get_ch_tracker['AccountMetaData']['meta_data_value']);

        $enabletracking = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "enabletracking"
        )));
        $this->set('enabletracking', empty($enabletracking['AccountMetaData']['meta_data_value']) ? '0' : $enabletracking['AccountMetaData']['meta_data_value']);

        $enableanalytics = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "enableanalytics"
        )));
        $this->set('enableanalytics', empty($enableanalytics['AccountMetaData']['meta_data_value']) ? '0' : $enableanalytics['AccountMetaData']['meta_data_value']);


        $auto_delete = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "auto_delete"
        )));
        $this->set('auto_delete', empty($auto_delete['AccountMetaData']['meta_data_value']) ? '0' : $auto_delete['AccountMetaData']['meta_data_value']);


        $get_video_value = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "enable_video_library"
        )));
        $this->set('video_value', empty($get_video_value['AccountMetaData']['meta_data_value']) ? '0' : $get_video_value['AccountMetaData']['meta_data_value']);
        $get_analytics_value = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        $this->set('duration_value', empty($get_analytics_value['AccountMetaData']['meta_data_value']) ? '08' : $get_analytics_value['AccountMetaData']['meta_data_value']);

        $get_tracking_value = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "tracking_duration"
        )));
        $this->set('tracking_value', empty($get_tracking_value['AccountMetaData']['meta_data_value']) ? '48' : $get_tracking_value['AccountMetaData']['meta_data_value']);

        $get_embed_link = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "external_embed_link"
        )));
        $this->set('external_embeded_link', empty($get_embed_link['AccountMetaData']['meta_data_value']) ? "" : $get_embed_link['AccountMetaData']['meta_data_value']);
        $get_tags = $this->AccountTag->find("all", array("conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 1,
        )));
        $get_st_tags = $this->AccountTag->find("all", array("conditions" => array(
                "tag_type" => '0', "account_id" => $account_id
        )));

        $get_metrics = $this->AccountMetaData->find("all", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name like 'metric_value_%'",
            ),
            "order" => 'meta_data_value ASC'
        ));

        $tagged_standards_count = $this->_get_document_standard_ratings_count($account_id);
        
        $parent_account_details = $this->Account->find('all', array(
            'conditions' => array(
                'id' => $account_id
            )
                )
        );
        $frameworks_data = array();

        $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                "AccountTag.tag_type" => '2', "AccountTag.account_id" => $account_id,'afs.published' => '1'
        ),'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'a.id = AccountTag.account_id'
                ),
                array(
                    'table' => 'account_framework_settings as afs',
                    'type' => 'inner',
                    'conditions' => 'afs.account_tag_id = AccountTag.account_tag_id'
                )
            
            )));
        
        if ($frameworks) {
            foreach ($frameworks as $row) {
                $frameworks_data[$row['AccountTag']['account_tag_id']] = $row;
            }
        }
        $parent_account_id = $parent_account_details[0]['Account']['parent_account_id'];
        
        
        $frameworksParentAccounts = $this->AccountTag->find("all", array("conditions" => array(
                "AccountTag.tag_type" => '2', "AccountTag.account_id" => $parent_account_id,'afs.published' => '1'
        ),'fields' => array('AccountTag.*','afs.parent_child_share'),'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'a.id = AccountTag.account_id'
                ),
                array(
                    'table' => 'account_framework_settings as afs',
                    'type' => 'inner',
                    'conditions' => 'afs.account_tag_id = AccountTag.account_tag_id'
                )
            
            )));
        
        if ($frameworksParentAccounts) {
            foreach ($frameworksParentAccounts as $row) {
                if ($row['afs']['parent_child_share'] == 1) {
                    $frameworks_data[$row['AccountTag']['account_tag_id']] = $row;
                }
            }
        }

        $frameworks = array();
        if ($frameworks_data) {
            foreach ($frameworks_data as $row) {
                $frameworks[] = $row;
            }
        }
        
        if (!empty($frameworks) && count($frameworks) > 0)
            $this->set('frameworks', $frameworks);

        $get_framework_value = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "default_framework"
        )));
        $this->set('default_framework', empty($get_framework_value['AccountMetaData']['meta_data_value']) ? '' : $get_framework_value['AccountMetaData']['meta_data_value']);

        $this->set('tags', $get_tags);
        $this->set('metrics', $get_metrics);
        $this->set('standards', $get_st_tags);
        $this->set('tagged_standards_count', $tagged_standards_count);

        $this->set('allAccountOwner', $this->accountOwnersList($account_id));
        $embeded_enable = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "enable_embeded_link"
        )));
        $this->set('is_embeded_enable', empty($embeded_enable['AccountMetaData']['meta_data_value']) ? '0' : $embeded_enable['AccountMetaData']['meta_data_value']);

        $user_privileges = $this->UserAccount->find("all", array("conditions" => array(
                "account_id" => $account_id,
                "role_id" => 120
        )));

        $admin_privileges = $this->UserAccount->find("all", array("conditions" => array(
                "account_id" => $account_id,
                "role_id" => 115
        )));

        $folders_check = 1;
        $permission_access_video_library = 1;
        $permission_video_library_upload = 1;
        $permission_administrator_user_new_role = 1;
        $parmission_access_my_workspace = 1;
        $manage_collab_huddles = 1;
        $manage_coach_huddles = 1;
        $manage_evaluation_huddles = 1;
        $permission_view_analytics = 1;
        $huddle_to_workspace = 1;

        $admin_permission_video_library_upload = 1;
        $admin_permission_administrator_user_new_role = 1;
        $admin_view_analytics = 1;

        foreach ($user_privileges as $row) {

            if ($row['UserAccount']['folders_check'] == 0) {
                $folders_check = 0;
            }
            if ($row['UserAccount']['permission_access_video_library'] == 0) {
                $permission_access_video_library = 0;
            }
            if ($row['UserAccount']['permission_video_library_upload'] == 0) {
                $permission_video_library_upload = 0;
            }
            if ($row['UserAccount']['permission_administrator_user_new_role'] == 0) {
                $permission_administrator_user_new_role = 0;
            }
            if ($row['UserAccount']['manage_collab_huddles'] == 0) {
                $manage_collab_huddles = 0;
            }
            if ($row['UserAccount']['manage_coach_huddles'] == 0) {
                $manage_coach_huddles = 0;
            }
            if ($row['UserAccount']['manage_evaluation_huddles'] == 0) {
                $manage_evaluation_huddles = 0;
            }
            if ($row['UserAccount']['parmission_access_my_workspace'] == 0) {
                $parmission_access_my_workspace = 0;
            }

            if ($row['UserAccount']['permission_view_analytics'] == 0) {
                $permission_view_analytics = 0;
            }
            if ($row['UserAccount']['huddle_to_workspace'] == 0) {
                $huddle_to_workspace = 0;
            }
        }

        foreach ($admin_privileges as $row) {

            if ($row['UserAccount']['permission_video_library_upload'] == 0) {
                $admin_permission_video_library_upload = 0;
            }
            if ($row['UserAccount']['permission_administrator_user_new_role'] == 0) {
                $admin_permission_administrator_user_new_role = 0;
            }
            if ($row['UserAccount']['permission_view_analytics'] == 0) {
                $admin_view_analytics = 0;
            }
        }
        
        $coaching_perfomance_level = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "coaching_perfomance_level"
        )));
        
        $coaching_perfomance_level_value = isset($coaching_perfomance_level['AccountMetaData']['meta_data_value']) ? $coaching_perfomance_level['AccountMetaData']['meta_data_value'] : '0';
        
        $assessment_perfomance_level = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "assessment_perfomance_level"
        )));
        
        $assessment_perfomance_level_value = isset($assessment_perfomance_level['AccountMetaData']['meta_data_value']) ? $assessment_perfomance_level['AccountMetaData']['meta_data_value'] : '0';


        $this->set('folders_check', $folders_check);
        $this->set('permission_access_video_library', $permission_access_video_library);
        $this->set('permission_video_library_upload', $permission_video_library_upload);
        $this->set('permission_administrator_user_new_role', $permission_administrator_user_new_role);
        $this->set('manage_collab_huddles', $manage_collab_huddles);
        $this->set('manage_coach_huddles', $manage_coach_huddles);
        $this->set('manage_evaluation_huddles', $manage_evaluation_huddles);
        $this->set('parmission_access_my_workspace', $parmission_access_my_workspace);
        $this->set('view_analytics', $permission_view_analytics);
        $this->set('huddle_to_workspace', $huddle_to_workspace);
        $this->set('coaching_perfomance_level_value', $coaching_perfomance_level_value);
        $this->set('assessment_perfomance_level_value', $assessment_perfomance_level_value);
        

        $this->set('admin_permission_video_library_upload', $admin_permission_video_library_upload);
        $this->set('admin_permission_administrator_user_new_role', $admin_permission_administrator_user_new_role);
        $this->set('admin_view_analytics', $admin_view_analytics);

        $account_users = $this->User->getUsersByAccount($account_id, 120);
        $this->set('account_users', $account_users);

        $account_admins = $this->User->getUsersByAccount($account_id, 115);
        $this->set('account_admins', $account_admins);

        $this->set('child_accounts', $child_accounts);

        $sampleHuddles = $this->AccountFolder->find('all', array(
            'conditions' => array(
                'active' => 1,
                'is_sample' => 1,
                'account_id' => $account_id,
                'folder_type' => 1
            )
        ));
        if (empty($sampleHuddles)) {
            $this->set('sample_huddle_not_exists', true);
        } else {
            $this->set('sample_huddle_not_exists', false);
        }

        $this->set('emitplans', array(17, 18, 13, 14, 9, 10));
        $this->render('account_settings_all');
    }

    private function _get_card_informaiton($account_id) {
        $subscription = new Subscription();
        if (isset($account_id) && $account_id != '') {
            $customer_info = array(
                'plan_info' => '',
                'card_info' => ''
            );
            $account = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
            if (isset($account['Account']) && $account['Account']['braintree_customer_id'] != '' && $account['Account']['braintree_subscription_id'] != '' && $account['Account']['deactive_plan'] != '1') {
                $plan_info = '';
                $credit_card_info = '';

                $customer_info = $subscription->get_customer_information($account['Account']['braintree_customer_id']);

                if (empty($customer_info)) {
                    $customer_info = array(
                        'plan_info' => '',
                        'card_info' => ''
                    );
                    return $customer_info;
                }
                $customer_detail = array(
                    'first_name' => $customer_info->firstName,
                    'last_name' => $customer_info->lastName,
                    'email' => $customer_info->email,
                    'company' => $customer_info->company,
                    'id' => $customer_info->id,
                    'address' => $customer_info->creditCards[0]->billingAddress->streetAddress,
                    'city' => $customer_info->creditCards[0]->billingAddress->locality,
                    'state' => $customer_info->creditCards[0]->billingAddress->region,
                    'zip_code' => $customer_info->creditCards[0]->billingAddress->postalCode,
                );


                if (isset($customer_info->creditCards[0]->subscriptions[0]->planId) && $customer_info->creditCards[0]->subscriptions[0]->planId > 0) {
                    //$plan_info = $this->Plans->getSelectedPlan($customer_info->creditCards[0]->subscriptions[0]->planId);
                    $plan_info = $this->Plans->getSelectedPlan($account['Account']['plan_id']);
                }
                if (isset($customer_info->creditCards[0]->subscriptions[0]->transactions[0]->creditCardDetails) && $customer_info->creditCards[0]->subscriptions[0]->transactions[0]->creditCardDetails != '') {
                    $credit_card_info = $customer_info->creditCards[0]->subscriptions[0]->transactions[0]->creditCardDetails;
                } elseif (isset($customer_info->creditCards[0]->maskedNumber) && $customer_info->creditCards[0]->maskedNumber != '' && isset($customer_info->creditCards[0]->token) && $customer_info->creditCards[0]->token != '') {
                    $credit_card_info = $customer_info->creditCards[0];
                }
                $customer_info = array(
                    'plan_info' => $plan_info,
                    'card_info' => $credit_card_info,
                    'customer_info' => $customer_detail
                );
            }

            return $customer_info;
        }
    }

    function fetch_transactions_details($account_id) {
        $account = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $subscription = new Subscription();
        $result = array();

        if (isset($account['Account']) && $account['Account']['braintree_customer_id'] != '') {
            $transactions = $subscription->transactions_detail($account['Account']['braintree_customer_id']);
        }
        if (!empty($transactions)) {
            foreach ($transactions as $transaction) {
                $date = (array) $transaction->createdAt;
                $amount = $transaction->amount;
                $status = $transaction->status;
                $id = $transaction->id;

                $trans_date = strtotime($date['date']);
                $trans_date = date('M-d-Y', $trans_date);


                $result[] = array(
                    'amount' => $amount,
                    'date' => $trans_date,
                    'id' => $id,
                    'status' => $status
                );
            }
        }
        return $result;
    }

    function fetch_subscirption_details($account_id) {
        $account = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $plans = $this->Plans->find("first", array("conditions" => array(
                "id" => $account['Account']['plan_id'],
        )));

        if (!empty($plans)) {
            $yearly_bool = $plans['Plans']['per_year'];
        }
        $subscription = new Subscription();
        $result = array();

        if (isset($account['Account']) && $account['Account']['braintree_customer_id'] != '' && $account['Account']['braintree_subscription_id'] != '' && $account['Account']['deactive_plan'] != '1') {
            $subscriptions = $subscription->subscriptions_detail($account['Account']['braintree_subscription_id']);
        }


        if (!empty($subscriptions)) {

            // print_r((array)$subscriptions['_attributes']);die;
            //$subscriptions = (array)$subscriptions;
            //print_r($subscriptions);die;

            $nextBillingDate = $subscriptions->nextBillingDate;
            $nextBillingDate = (array) $nextBillingDate;

            $billingPeriodStartDate = $subscriptions->billingPeriodStartDate;
            if (!empty($billingPeriodStartDate)) {
                $billingPeriodStartDate = (array) $billingPeriodStartDate;
            } else {
                $billingPeriodStartDate = $nextBillingDate;
            }

            $billingPeriodEndDate = $subscriptions->billingPeriodEndDate;
            if (!empty($billingPeriodEndDate)) {
                $billingPeriodEndDate = (array) $billingPeriodEndDate;
            } else {
                $time = strtotime($nextBillingDate['date']);
                if ($yearly_bool) {
                    $billingPeriodEndDate['date'] = date("Y-m-d", strtotime("+1 year", $time));
                } else {
                    $billingPeriodEndDate['date'] = date("Y-m-d", strtotime("+1 month", $time));
                }
            }


            $billingPeriodStartDate = date('M d,Y', strtotime($billingPeriodStartDate['date']));
            $nextBillingDate = date('M d,Y', strtotime($nextBillingDate['date']));
            $billingPeriodEndDate = date('M d,Y', strtotime($billingPeriodEndDate['date']));

            $result = array(
                'billing_start' => $billingPeriodStartDate,
                'next_billing' => $nextBillingDate,
                'billing_end' => $billingPeriodEndDate
            );
            return $result;
        } else {
            return $result;
        }
    }

    function send_email() {
        $emails = explode(',', $this->request->data['email']);
        $users = $this->Session->read('user_current_account');
        $username = $users['User']['username'];
        $sender_email = $users['User']['email'];
        foreach ($emails as $email) {
            $this->Email->delivery = 'smtp';
            $this->Email->from = $users['User']['email'];
            $this->Email->to = $email;
            $this->Email->subject = $this->request->data['subject'];
            $this->Email->template = 'default';
            $this->Email->sendAs = 'html';

//            $filename = $this->request->data['filename'];
//
//            $Path = APP . 'tmp/PdfReport' . $filename;
//
//            $this->Email->attachments = array($Path);
            $view = new View($this, true);
            $this->AuditEmail->create();
            $message = $this->request->data['message'];
            $html = "<div>Name of User:$email</div>";
            $html .= "<div>Sender's Email:" . $users['User']['first_name'] . " " . $users['User']['last_name'] . "</div>";
            $html .= "<p>$message</p>";
            $auditEmail = array(
                'account_id' => $this->request->data['account_id'],
                'email_from' => $this->Email->from,
                'email_to' => 'hamidbscs8@gmail.com',
                //'email_to' => $email,
                'email_subject' => $this->Email->subject,
                'email_body' => $html,
                'is_html' => true,
                'sent_date' => date("Y-m-d H:i:s")
            );

            if (!empty($this->request->data['email'])) {
                $message = $this->request->data['message'];
                $this->AuditEmail->save($auditEmail, $validation = TRUE);
                $html = "<div>Name of User: " . $users['User']['first_name'] . " " . $users['User']['last_name'] . "</div>";
                $html .= "<div>Sender's Email: " . $sender_email . "</div>";
                $html .= "<p>$message</p>";
                if ($this->Email->send($html)) {
                    echo TRUE;
                } else {
                    return FALSE;
                }
            }
        }
        die;
        return FALSE;
    }

    function find_transaction($transaction_id) {
        $fileName = 'Transaction_Invoice_' . $transaction_id . '_' . date('m-d-Y') . '.pdf';
        $subscription = new Subscription();
        $transaction_data = $this->Transaction->find("first", array("conditions" => array(
                "transaction_id" => $transaction_id,
        )));

        if (!empty($transaction_data)) {
            $plan_id = $transaction_data['Transaction']['student_payment_id'];
        }

        $transactions = $subscription->find_transaction_detail($transaction_id);

        if (empty($transactions->planId) && !empty($transaction_data)) {
            $plans = $this->Plans->find("first", array("conditions" => array(
                    "id" => $plan_id,
            )));
        } else {
            $plans = $this->Plans->find("first", array("conditions" => array(
                    "id" => $transactions->planId,
            )));
        }


        if ($plans['Plans']['per_year']) {
            $billing_cycle = 'Yearly';
        } else {
            $billing_cycle = 'Monthly';
        }
        $plan_category = $plans['Plans']['category'];
        $plan_name = $plans['Plans']['plan_type'];
        $created_at = (array) $transactions->createdAt;
        $created_at = date('M d,Y', strtotime($created_at['date']));

        $params_1 = array(
            'company' => $transactions->customer['company'],
            'firstName' => $transactions->customer['firstName'],
            'lastName' => $transactions->customer['lastName'],
            'email' => $transactions->customer['email'],
            'invoice_number' => $transactions->id,
            'created_at' => $created_at,
            'amount' => $transactions->amount,
            'billing_cycle' => $billing_cycle,
            'plan_category' => $plan_category,
            'plan_name' => $plan_name
        );
        $view = new View($this, true);
        $html = $view->element('pdf/transactions', $params_1);

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(400, 300), true, 'UTF-8', false);

        $pdf->SetTitle("Transaction Invoice");

        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->AddPage();

        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($fileName, "D");
        die;
    }

    function change_framework_names($account_id) {

        if ($this->request->is('post')) {

            $names = $this->request->data['names'];
            $ids = $this->request->data['ids'];


            foreach ($names as $key => $name) {
                $data = array(
                    'tag_title' => "'" . $name . "'"
                );
                $this->AccountTag->updateAll($data, array('account_tag_id' => $ids[$key], 'account_id' => $account_id));
            }
        }
        $this->redirect('/accounts/account_settings_all/' . $account_id . '/1');
    }

    function frameworks($account_id) {

        $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                "tag_type" => '2', "account_id" => $account_id
        )));

        $this->set('frameworks', $frameworks);
        $this->set('account_id', $account_id);
    }

    function deactivated_account_redirect($account_id) {
        $account_owner = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));

        $account_owner_details = $this->User->find('first', array('conditions' => array('id' => $account_owner['UserAccount']['user_id'])));

        $this->set('email', $account_owner_details['User']['email']);
    }

    function add_new_rubric($account_id, $tab) {
        // $this->layout = 'default';
    }

    function save_frame_work_first_level($framework_id = '') {
        $account_id = $this->request->data['account_id'];
        $user_id = $this->request->data['user_id'];

        if (empty($framework_id)) {

            $this->AccountTag->create();
            $save_tags = array(
                "account_id" => $account_id,
                "tag_type" => '2',
                "tag_title" => $this->request->data['rubric_name'],
                "created_by" => $user_id,
                "created_date" => date('Y-m-d H:i:s'),
                "last_edit_by" => $user_id,
                "last_edit_date" => date('Y-m-d H:i:s')
            );
            $this->AccountTag->save($save_tags);

            $account_tag_id = $this->AccountTag->id;

            $this->AccountFrameworkSetting->create();

            $framework_settings = array(
                "account_id" => $account_id,
                "account_tag_id" => $account_tag_id,
                "updated_at" => date('Y-m-d H:i:s'),
                "framework_name" => $this->request->data['rubric_name'],
                "enable_unique_desc" => '0',
                "enable_ascending_order" => '0',
                "enable_performance_level" => '0',
                "tier_level" => $this->request->data['tier_level'],
                "checkbox_level" => $this->request->data['checkbox_level'],
                "framework_sample" => htmlentities($this->request->data['framework_sample']),
                "parent_child_share" => $this->request->data['parent_sharing']
            );
            $this->AccountFrameworkSetting->save($framework_settings);

            $data = array(
                'framework_id' => $account_tag_id
            );
        } else {

            $save_tags = array(
                "account_id" => "'" . $account_id . "'",
                "tag_type" => '2',
                "tag_title" => "'" . $this->request->data['rubric_name'] . "'",
                "created_by" => "'" . $user_id . "'",
                "created_date" => "'" . date('Y-m-d H:i:s') . "'",
                "last_edit_by" => "'" . $user_id . "'",
                "last_edit_date" => "'" . date('Y-m-d H:i:s') . "'"
            );
            $get_framework = $this->AccountFrameworkSetting->find('first', array(
                'conditions' => array(
                    'account_tag_id' => $framework_id
                )
            ));

            $this->AccountTag->updateAll($save_tags, array('account_tag_id' => $framework_id));
            if ($get_framework && $get_framework['AccountFrameworkSetting']['published'] != 1) {
                $this->AccountTag->updateAll(array('standard_level' => $this->request->data['checkbox_level']), array('framework_id' => $framework_id, 'parent_account_tag_id IS NOT NULL'));
            }
            $account_tag_id = $framework_id;
            $framework_settings = array(
                "account_id" => $account_id,
                "updated_at" => "'" . date('Y-m-d H:i:s') . "'",
                "framework_name" => "'" . $this->request->data['rubric_name'] . "'",
                "tier_level" => $this->request->data['tier_level'],
                "checkbox_level" => $this->request->data['checkbox_level'],
                "framework_sample" => "'" . htmlentities($this->request->data['framework_sample']) . "'",
                "parent_child_share" => "'" . $this->request->data['parent_sharing'] . "'"
            );
            $this->AccountFrameworkSetting->updateAll($framework_settings, array('account_tag_id' => $framework_id));

            $data = array(
                'framework_id' => $framework_id
            );
        }

        echo json_encode($data);

        die;
    }

    function check_validation() {

        $title_conents = $this->request->data['title'];
        $framework_id = $this->request->data['framework_id'];
        $error_message = '';
        $titel_message = '';
        $content_message = '';
        $result_index = array();
        $framework_settings_details = $this->AccountFrameworkSetting->find("first", array("conditions" => array(
                "account_tag_id" => $framework_id,
        )));
        for ($i = 0; $i <= count($title_conents); $i++) {
            if (isset($title_conents[$i]) && empty($title_conents[$i])) {
                $titel_message = $this->language_based_messages['performance_levels_title_is_required_msg'];
                $result_index[$i] = $i;
            }
        }


        if (isset($this->request->data['auto_scroll_switch1']) && !isset($this->request->data['auto_scroll_switch2'])) {
            $description = $this->request->data['description'];
            for ($i = 0; $i <= count($description); $i++) {
                if (isset($description[$i]) && empty($description[$i])) {
                    $content_message = $this->language_based_messages['global_description_is_required_msg'];
                    $result_index[$i] = $i;
                }
            }
        }
        //$error_message .= $titel_message;
        //$error_message .= $content_message;

        if ($titel_message != '' || $content_message != '') {
            echo json_encode(
                    array(
                        'status' => false,
                        'title' => $titel_message,
                        'description' => $content_message,
                        'result_array' => $result_index
                    )
            );
        } else {
            echo json_encode(
                    array(
                        'status' => true,
                        'err_message' => ''
                    )
            );
        }
        die;
    }

    function save_frame_work_second_level() {
        $account_id = $this->request->data['account_id'];
        $framework_id = $this->request->data['framework_id'];

        $framework_settings_details = $this->AccountFrameworkSetting->find("first", array("conditions" => array(
                "account_tag_id" => $framework_id,
        )));

        $framework_settings_id = $framework_settings_details['AccountFrameworkSetting']['id'];

        $performace_level_titles = $this->request->data['title'];

        $performace_level_ratings = $this->request->data['ratting_standards'];

        $performace_level_descriptions = $this->request->data['description'];

        if (isset($this->request->data['performance_level_ids'])) {
            $performance_level_ids = $this->request->data['performance_level_ids'];
        }


        foreach ($performace_level_titles as $key => $pl) {

            $account_framework_performance_level = $this->AccountFrameworkSettingPerformanceLevel->find("first", array("conditions" => array(
                    "performance_level_rating" => $performace_level_ratings[$key],
                    "account_framework_setting_id" => $framework_settings_id
            )));

            if (!empty($account_framework_performance_level)) {
                $performace_level_data = array(
                    'account_id' => "'" . $account_id . "'",
                    'account_framework_setting_id' => "'" . $framework_settings_id . "'",
                    'performance_level' => "'" . $pl . "'",
                    'performance_level_rating' => "'" . $performace_level_ratings[$key] . "'"
                );
                if (!isset($this->request->data['auto_scroll_switch2']) && !$this->request->data['auto_scroll_switch2'] == 1) {
                    $performace_level_data['description'] = "'" . Sanitize::escape($performace_level_descriptions[$key]) . "'";
                } else {
                    $performace_level_data['description'] = NULL;
                }
                $this->AccountFrameworkSettingPerformanceLevel->updateAll($performace_level_data, array('performance_level_rating' => $performace_level_ratings[$key], "account_framework_setting_id" => $framework_settings_id));
            } else {
                $this->AccountFrameworkSettingPerformanceLevel->create();

                $performace_level_data = array(
                    'account_id' => $account_id,
                    'account_framework_setting_id' => $framework_settings_id,
                    'performance_level' => $pl,
                    'performance_level_rating' => $performace_level_ratings[$key]
                );
                if (!isset($this->request->data['auto_scroll_switch2']) && !$this->request->data['auto_scroll_switch2'] == 1) {
                    $performace_level_data['description'] = $performace_level_descriptions[$key];
                } else {
                    $performace_level_data['description'] = NULL;
                }

                $this->AccountFrameworkSettingPerformanceLevel->save($performace_level_data);
            }
        }

        if ($framework_settings_details['AccountFrameworkSetting']['published'] != '1') {
            $data = array(
                'enable_unique_desc' => (isset($this->request->data['auto_scroll_switch2']) ? '1' : '0'),
                'enable_ascending_order' => (isset($this->request->data['auto_scroll_switch3']) ? '1' : '0'),
                'enable_performance_level' => (isset($this->request->data['auto_scroll_switch1']) ? '1' : '0'),
            );

            $this->AccountFrameworkSetting->updateAll($data, array('account_tag_id' => $framework_id));
        }

        if (isset($this->request->data['save_button'])) {
            $this->redirect("/accounts/account_settings_all/$account_id/7/1");
        } else {
            $this->redirect("/accounts/account_settings_all/$account_id/7/4/0/$framework_id");
        }
    }

    function mearg_tow_array($arrayData, $current_index) {
        $childarray = array();
        foreach ($arrayData as $key => $row) {
            if ($row['standard_level']) {

            }
        }
    }

    function save_rubrics_last_level_old() {

        if ($this->request->is('post')) {
            //  print_r($this->request->data);die;

            $rsult = array();
            $data = $this->request->data['result'];
//            echo "<pre>";
//            print_r($this->request->data);

            $framework_id = $this->request->data['framework_id'];
            $account_id = $this->request->data['account_id'];
            $user = $this->Session->read('user_current_account');
            $user_id = $user['User']['id'];
            $total_performance_levels = $this->request->data['total_performance_levels'];
            $final_data = array();
            $framework_settings_details = $this->AccountFrameworkSetting->find("first", array("conditions" => array(
                    "account_tag_id" => $framework_id,
            )));

            $standards_data1 = '';
            $checkbox_level = $framework_settings_details['AccountFrameworkSetting']['checkbox_level'];
            foreach ($data as $rows) {
                $standard_level = array();
                $result = array();
                $result2 = array();
                $framework_hierarchy = array();
                $count = 1;


                $parent_account_tag_id = '';
                foreach ($rows as $row) {
//                    echo "<pre>";
//                    print_r($row);
//                    echo "</pre>";
//                echo "$checkbox_level==$count<br/>";
                    if ($count == 1) {
                        $standards_data = array(
                            'account_id' => $account_id,
                            'parent_account_tag_id' => '',
                            'tag_type' => 0,
                            'tag_code' => $row['prefix_level'],
                            'created_by' => $user_id,
                            'created_date' => date('Y-m-d H:i:s'),
                            'last_edit_by' => $user_id,
                            'last_edit_date' => date('Y-m-d H:i:s'),
                            'tads_code' => NULL,
                            'framework_id' => $framework_id,
                            'standard_level' => $row['standard_level']
                        );

                        if (isset($row['account_tag_id'])) {
                            if ($checkbox_level == $count) {
                                if ($row['pl_standard_label'] != '') {
                                    $standards_data['tag_title'] = "'" . $row['pl_standard_label'] . "'";
                                    $explode_data = explode(':', $row['contents']);
                                    if (isset($explode_data[1]) && !empty($explode_data[1])) {
                                        $standards_data['tag_html'] = "'" . $row['pl_standard_label'] . ':' . $explode_data[1] . "'";
                                    } else {
                                        $standards_data['tag_html'] = "'" . $row['pl_standard_label'] . ':' . $row['contents'] . "'";
                                    }
                                } else {
                                    $standards_data['tag_html'] = "'" . $row['contents'] . "'";
                                }
                            } else {
                                $standards_data['tag_html'] = "'" . $row['contents'] . "'";
                            }
                            unset($standards_data['parent_account_tag_id']);
                            unset($standards_data['created_date']);
                            $standards_data['tag_code'] = "'" . $row['prefix_level'] . "'";
                            $standards_data['last_edit_date'] = "'" . date('Y-m-d H:i:s') . "'";

                            $parent_account_tag_id = $row['account_tag_id'];
                            $standards_data1[] = $standards_data;
                            $this->AccountTag->updateAll($standards_data, array('account_tag_id' => $parent_account_tag_id));
                            if (!empty($row['pl_desc_id'][1])) {
                                for ($x = 0; $x < $total_performance_levels; $x++) {
                                    $performance_level_desc_data = array(
                                        'description' => "'" . $row['pl_desc'][$x + 1] . "'"
                                    );
                                    $PerformanceLevelDescription = $this->PerformanceLevelDescription->find("first", array("conditions" => array('performance_level_id' => $row['pl_desc_id'][$x + 1], 'account_tag_id' => $parent_account_tag_id)));

                                    if (!empty($PerformanceLevelDescription)) {
                                        $this->PerformanceLevelDescription->updateAll($performance_level_desc_data, array('performance_level_id' => $row['pl_desc_id'][$x + 1], 'account_tag_id' => $parent_account_tag_id));
                                    } else {

                                        $this->PerformanceLevelDescription->create();
                                        $performance_level_desc_data = array(
                                            'account_tag_id' => $parent_account_tag_id,
                                            'performance_level_id' => $row['pl_desc_id'][$x + 1],
                                            'description' => $row['pl_desc'][$x + 1]
                                        );

                                        $this->PerformanceLevelDescription->save($performance_level_desc_data);
                                    }
                                }
                            }
                        } else {
                            if ($checkbox_level == $count) {
                                if ($row['pl_standard_label'] != '') {
                                    $standards_data['tag_title'] = $row['pl_standard_label'];
                                    $standards_data['tag_html'] = $row['pl_standard_label'] . ':' . $row['contents'];
                                } else {
                                    $standards_data['tag_html'] = $row['contents'];
                                }
                            } else {
                                $standards_data['tag_html'] = $row['contents'];
                            }
                            $standards_data1[] = $standards_data;

                            $this->AccountTag->create();
                            $this->AccountTag->save($standards_data);
                            $parent_account_tag_id = $this->AccountTag->id;
                        }

                        $framework_hierarchy[] = $parent_account_tag_id;
                        $framework_hierarchy_url = '/' . implode('/', $framework_hierarchy) . '/';
                        $data_hrky = array(
                            'framework_hierarchy' => "'" . $framework_hierarchy_url . "'"
                        );
                        if (!isset($row['account_tag_id'])) {
                            if (!empty($row['pl_desc_id'][1])) {
                                for ($x = 0; $x < $total_performance_levels; $x++) {
                                    $this->PerformanceLevelDescription->create();
                                    $performance_level_desc_data = array(
                                        'account_tag_id' => $this->AccountTag->id,
                                        'performance_level_id' => $row['pl_desc_id'][$x + 1],
                                        'description' => $row['pl_desc'][$x + 1]
                                    );

                                    $this->PerformanceLevelDescription->save($performance_level_desc_data);
                                }
                            }
                        }
                        //$standards_data1[] = $data_hrky;

                        $this->AccountTag->updateAll($data_hrky, array('account_tag_id' => $parent_account_tag_id));
                    } else {
                        $standards_data = array(
                            'account_id' => $account_id,
                            'parent_account_tag_id' => '',
                            'tag_type' => 0,
                            'tag_code' => $row['prefix_level'],
                            'created_by' => $user_id,
                            'created_date' => date('Y-m-d H:i:s'),
                            'last_edit_by' => $user_id,
                            'last_edit_date' => date('Y-m-d H:i:s'),
                            'tads_code' => NULL,
                            'framework_id' => $framework_id,
                            'standard_level' => $row['standard_level']
                        );

                        if (isset($row['account_tag_id'])) {
                            unset($standards_data['parent_account_tag_id']);
                            unset($standards_data['created_date']);

                            $standards_data['tag_code'] = "'" . $row['prefix_level'] . "'";
                            $standards_data['tag_title'] = "'" . $row['contents'] . "'";
                            $standards_data['tag_html'] = "'" . $row['contents'] . "'";
                            $standards_data['last_edit_date'] = "'" . date('Y-m-d H:i:s') . "'";
                            if ($checkbox_level == $count) {
                                if ($row['pl_standard_label'] != '') {
                                    $standards_data['tag_title'] = "'" . $row['pl_standard_label'] . "'";
                                    $explode_data = explode(':', $row['contents']);
                                    if (isset($explode_data[1]) && !empty($explode_data[1])) {
                                        $standards_data['tag_html'] = "'" . $row['pl_standard_label'] . ':' . $explode_data[1] . "'";
                                    } else {
                                        $standards_data['tag_html'] = "'" . $row['pl_standard_label'] . ':' . $row['contents'] . "'";
                                    }
                                } else {
                                    $standards_data['tag_html'] = "'" . $row['contents'] . "'";
                                }
                            } else {
                                $standards_data['tag_html'] = "'" . $row['contents'] . "'";
                            }
                            $standards_data1[] = $standards_data;
                            $this->AccountTag->updateAll($standards_data, array('account_tag_id' => $row['account_tag_id']));
                            if (!empty($row['pl_desc_id'][1])) {
                                for ($x = 0; $x < $total_performance_levels; $x++) {

                                    $performance_level_desc_data = array(
                                        'description' => "'" . $row['pl_desc'][$x + 1] . "'"
                                    );

                                    $PerformanceLevelDescription = $this->PerformanceLevelDescription->find("first", array("conditions" => array('performance_level_id' => $row['pl_desc_id'][$x + 1], 'account_tag_id' => $row['account_tag_id'])));

                                    if (!empty($PerformanceLevelDescription)) {
                                        $this->PerformanceLevelDescription->updateAll($performance_level_desc_data, array('performance_level_id' => $row['pl_desc_id'][$x + 1], 'account_tag_id' => $row['account_tag_id']));
                                    } else {

                                        $this->PerformanceLevelDescription->create();
                                        $performance_level_desc_data = array(
                                            'account_tag_id' => $row['account_tag_id'],
                                            'performance_level_id' => $row['pl_desc_id'][$x + 1],
                                            'description' => $row['pl_desc'][$x + 1]
                                        );

                                        $this->PerformanceLevelDescription->save($performance_level_desc_data);
                                    }
                                }
                            }
                            $account_tag_id = $row['account_tag_id'];
                        } else {
                            if ($checkbox_level == $count) {
                                if ($row['pl_standard_label'] != '') {
                                    $standards_data['tag_title'] = $row['pl_standard_label'];
                                    $standards_data['tag_html'] = $row['pl_standard_label'] . ':' . $row['contents'];
                                } else {
                                    $standards_data['tag_html'] = $row['contents'];
                                }
                            } else {
                                $standards_data['tag_html'] = $row['contents'];
                            }
                            $standards_data1[] = $standards_data;
                            $this->AccountTag->create();
                            $this->AccountTag->save($standards_data);
                            $account_tag_id = $this->AccountTag->id;
                        }
                        $standards_data['parent_account_tag_id'] = $parent_account_tag_id;


                        $framework_hierarchy[] = $account_tag_id;
                        $framework_hierarchy_url = '/' . implode('/', $framework_hierarchy);
                        $data_hrky = array(
                            'framework_hierarchy' => '"' . $framework_hierarchy_url . '"'
                        );

                        if (!isset($row['account_tag_id'])) {
                            if (!empty($row['pl_desc_id'][1])) {
                                for ($x = 0; $x < $total_performance_levels; $x++) {
                                    $this->PerformanceLevelDescription->create();
                                    $performance_level_desc_data = array(
                                        'account_tag_id' => $this->AccountTag->id,
                                        'performance_level_id' => $row['pl_desc_id'][$x + 1],
                                        'description' => $row['pl_desc'][$x + 1]
                                    );

                                    $this->PerformanceLevelDescription->save($performance_level_desc_data);
                                }
                            }
                        }


                        $this->AccountTag->updateAll($data_hrky, array('account_tag_id' => $account_tag_id));
                    }
                    if ($framework_settings_details['AccountFrameworkSetting']['tier_level'] == $count) {
                        $count = 1;
                    } else {
                        $count++;
                    }
                }

                $final_data[] = $result;
            }
            //echo "<pre>";
            //print_r($standards_data1);
            //die;
            $this->Session->setFlash($this->language_based_messages['framework_is_saved_msg'], 'default', array('class' => 'message success'));
            $this->redirect("/accounts/account_settings_all/$account_id/7/1");
        }
    }

    function save_rubrics_last_level() {

        if ($this->request->is('post')) {


            $rsult = array();
            $data = $this->request->data['result'];
//            echo "<pre>";
//            print_r($this->request->data);die;

            $framework_id = $this->request->data['framework_id'];
            if (isset($this->request->data['btn']) && $this->request->data['btn'] == 'Publish') {
                $update_data_query = array(
                    'published_at' => "'" . date('Y-m-d H:i:s') . "'",
                    'published' => '1',
                    'updated_at' => "'" . date('Y-m-d H:i:s') . "'"
                );
                $this->AccountFrameworkSetting->updateAll($update_data_query, array('account_tag_id' => $framework_id));
            }
            $account_id = $this->request->data['account_id'];
            $user = $this->Session->read('user_current_account');
            $user_id = $user['User']['id'];
            $total_performance_levels = $this->request->data['total_performance_levels'];
            $final_data = array();
            $framework_settings_details = $this->AccountFrameworkSetting->find("first", array("conditions" => array(
                    "account_tag_id" => $framework_id,
            )));

            $standards_data1 = '';
            $checkbox_level = $framework_settings_details['AccountFrameworkSetting']['checkbox_level'];
            foreach ($data as $row) {
                $standard_level = array();
                $result = array();
                $result2 = array();
                $framework_hierarchy = array();


                $parent_account_tag_id = '';


                $standards_data = array(
                    'account_id' => $account_id,
                    'parent_account_tag_id' => '',
                    'tag_type' => 0,
                    'tag_code' => $row['prefix_level'],
                    'created_by' => $user_id,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $user_id,
                    'last_edit_date' => date('Y-m-d H:i:s'),
                    'tads_code' => NULL,
                    'framework_id' => $framework_id,
                    'standard_level' => $row['standard_level']
                );

                if (isset($row['account_tag_id'])) {
                    unset($standards_data['parent_account_tag_id']);
                    unset($standards_data['created_date']);
                    $row['contents'] = str_ireplace(array('\r', '\n', '\r\n'), array(" ", " ", " "), $row['contents']);
                    $standards_data['tag_code'] = "'" . Sanitize::escape(htmlspecialchars($row['prefix_level'])) . "'";
                    $standards_data['tag_title'] = "'" . Sanitize::escape(htmlspecialchars($row['contents'])) . "'";
                    $standards_data['tag_html'] = "'" . Sanitize::escape(htmlspecialchars($row['contents'])) . "'";
                    $standards_data['last_edit_date'] = "'" . date('Y-m-d H:i:s') . "'";

                    if ($row['pl_standard_label'] != '') {
                        $standards_data['standard_analytics_label'] = "'" . Sanitize::escape(htmlspecialchars($row['pl_standard_label'])) . "'";
                    }


                    $this->AccountTag->updateAll($standards_data, array('account_tag_id' => $row['account_tag_id']));
                    if (!empty($row['pl_desc_id'][1])) {
                        for ($x = 0; $x < $total_performance_levels; $x++) {

                            $performance_level_desc_data = array(
                                'description' => "'" . Sanitize::escape(htmlspecialchars($row['pl_desc'][$x + 1])) . "'"
                            );

                            $PerformanceLevelDescription = $this->PerformanceLevelDescription->find("first", array("conditions" => array('performance_level_id' => $row['pl_desc_id'][$x + 1], 'account_tag_id' => $row['account_tag_id'])));

                            if (!empty($PerformanceLevelDescription)) {
                                $this->PerformanceLevelDescription->updateAll($performance_level_desc_data, array('performance_level_id' => $row['pl_desc_id'][$x + 1], 'account_tag_id' => $row['account_tag_id']));
                            } else {

                                $this->PerformanceLevelDescription->create();
                                $performance_level_desc_data = array(
                                    'account_tag_id' => $row['account_tag_id'],
                                    'performance_level_id' => $row['pl_desc_id'][$x + 1],
                                    'description' => Sanitize::escape(htmlspecialchars($row['pl_desc'][$x + 1]))
                                );

                                $this->PerformanceLevelDescription->save($performance_level_desc_data);
                            }
                        }
                    }
                    $account_tag_id = $row['account_tag_id'];
                } else {

                    if ($row['pl_standard_label'] != '') {
                        $standards_data['standard_analytics_label'] = $row['pl_standard_label'];
                    }
                    $standards_data['tag_title'] = $row['contents'];
                    $standards_data['tag_html'] = $row['contents'];

                    $this->AccountTag->create();
                    $this->AccountTag->save($standards_data);
                    $account_tag_id = $this->AccountTag->id;
                }


                if (!isset($row['account_tag_id'])) {
                    if (!empty($row['pl_desc_id'][1])) {
                        for ($x = 0; $x < $total_performance_levels; $x++) {
                            $this->PerformanceLevelDescription->create();
                            $performance_level_desc_data = array(
                                'account_tag_id' => $this->AccountTag->id,
                                'performance_level_id' => $row['pl_desc_id'][$x + 1],
                                'description' => Sanitize::escape(htmlspecialchars($row['pl_desc'][$x + 1]))
                            );

                            $this->PerformanceLevelDescription->save($performance_level_desc_data);
                        }
                    }
                }


                $final_data[] = $result;
            }
            //echo "<pre>";
            //print_r($standards_data1);
            //die;
            $this->create_churnzero_event('Framework+Added', $account_id, $user['User']['email']);
            $this->Session->setFlash($this->language_based_messages['framework_is_saved_msg'], 'default', array('class' => 'message success'));
            $this->redirect("/accounts/account_settings_all/$account_id/7/1");
        }
    }

    function publish_framework($framework_id, $account_id) {

        $data = array(
            'published_at' => "'" . date('Y-m-d H:i:s') . "'",
            'published' => '1',
            'updated_at' => "'" . date('Y-m-d H:i:s') . "'"
        );

        $framework_settings_details = $this->AccountFrameworkSetting->find("first", array("conditions" => array(
                "account_tag_id" => $framework_id,
        )));

        $framework_name = $framework_settings_details['AccountFrameworkSetting']['framework_name'];

        $this->AccountFrameworkSetting->updateAll($data, array('account_tag_id' => $framework_id));

        $this->Session->setFlash($framework_name . ' - ' . $this->language_based_messages['has_been_published_msg'] , 'default', array('class' => 'message success'));
        $this->redirect("/accounts/account_settings_all/$account_id/7/1");
    }

    function unpublish_framework($framework_id, $account_id) {

        $data = array(
            'published_at' => NULL,
            'published' => '0',
            'updated_at' => "'" . date('Y-m-d H:i:s') . "'"
        );

        $this->AccountFrameworkSetting->updateAll($data, array('account_tag_id' => $framework_id));

        $data = array(
            'video_framework_id' => NULL
            );

        $this->AccountFolderDocument->updateAll($data, array('video_framework_id' => $framework_id));
        
        $data = array(
            'meta_data_value' => NULL
            );
        
        $this->AccountMetaData->updateAll($data, array('meta_data_name' => 'default_framework' , 'account_id' => $account_id ));

        $account_tags_type = $this->AccountTag->find('all', array(
            'conditions' => array(
                'AccountTag.tag_type' => 0,
                'AccountTag.framework_id' => $framework_id,
            ),
            'fields' => array('AccountTag.*')
                )
        );

        foreach ($account_tags_type as $row) {

            $this->AccountCommentTag->deleteAll(array('account_tag_id' => $row['AccountTag']['account_tag_id']), $cascade = true, $callbacks = true);
            $this->DocumentStandardRating->deleteAll(array('standard_id' => $row['AccountTag']['account_tag_id']), $cascade = true, $callbacks = true);
            $this->PerformanceLevelDescription->deleteAll(array('account_tag_id' => $row['AccountTag']['account_tag_id']), $cascade = true, $callbacks = true);
        }

        $framework_settings_details = $this->AccountFrameworkSetting->find("first", array("conditions" => array(
                "account_tag_id" => $framework_id,
        )));

        $framework_name = $framework_settings_details['AccountFrameworkSetting']['framework_name'];

        $this->Session->setFlash($framework_name . ' - ' . $this->language_based_messages['has_been_unpublished_msg'], 'default', array('class' => 'message success'));
        $this->redirect("/accounts/account_settings_all/$account_id/7/1");
    }

    function delete_framework($framework_id, $account_id) {

        $framework_is_present_in_account = $this->AccountMetaData->find("first", array("conditions" => array(
                "meta_data_value" => $framework_id,
        )));

        if (!empty($framework_is_present_in_account)) {
            $this->Session->setFlash($this->language_based_messages['framework_cannot_be_deleted_because_associated_msg'], 'default', array('class' => 'message error'));

            $this->redirect("/accounts/account_settings_all/$account_id/7/1");
        }

        $joins = array(
            'table' => 'account_folders as af',
            'type' => 'inner',
            'conditions' => array('AccountFolderMetaData.account_folder_id = af.account_folder_id')
        );

        $framework_is_present_in_huddle = $this->AccountFolderMetaData->find("first", array('joins' => array($joins), "conditions" => array(
                "meta_data_value" => $framework_id,
                "af.active" => '1'
        )));

        if (!empty($framework_is_present_in_huddle)) {
            $this->Session->setFlash($this->language_based_messages['framework_cannot_be_deleted_because_associated_msg'], 'default', array('class' => 'message error'));

            $this->redirect("/accounts/account_settings_all/$account_id/7/1");
        }
        $framework_is_present_in_video = $this->AccountFolderDocument->find("first", array("conditions" => array(
                "video_framework_id" => $framework_id
        )));

        if (!empty($framework_is_present_in_video)) {
            $this->Session->setFlash($this->language_based_messages['framework_cannot_be_deleted_because_associated_msg'], 'default', array('class' => 'message error'));

            $this->redirect("/accounts/account_settings_all/$account_id/7/1");
        }





        $framework_settings_details = $this->AccountFrameworkSetting->find("first", array("conditions" => array(
                "account_tag_id" => $framework_id,
        )));

        $this->AccountFrameworkSettingPerformanceLevel->deleteAll(array('account_framework_setting_id' => $framework_settings_details['AccountFrameworkSetting']['account_framework_setting_id']), $cascade = true, $callbacks = true);

        $account_tag_details = $this->AccountTag->find("all", array("conditions" => array(
                "framework_id" => $framework_id,
        )));


        foreach ($account_tag_details as $atd) {
            $this->PerformanceLevelDescription->deleteAll(array('account_tag_id' => $atd['AccountTag']['account_tag_id']), $cascade = true, $callbacks = true);
        }

        $this->AccountFrameworkSetting->deleteAll(array('account_tag_id' => $framework_id), $cascade = true, $callbacks = true);

        $this->AccountTag->deleteAll(array('framework_id' => $framework_id), $cascade = true, $callbacks = true);
        $this->AccountTag->deleteAll(array('account_tag_id' => $framework_id), $cascade = true, $callbacks = true);
        $this->Session->setFlash($this->language_based_messages['you_have_sucessfully_deleted_the_framework_msg'], 'default', array('class' => 'message success'));

        $this->redirect("/accounts/account_settings_all/$account_id/7/1");
    }

    function delete_performance_level() {
        $pl_id = $this->request->data['pl_id'];

        $framework_id = $this->request->data['account_tag_id'];
        $performance_level_details = $this->AccountFrameworkSettingPerformanceLevel->find("first", array("conditions" => array(
                "id" => $pl_id,
        )));

        $delete_level = $performance_level_details['AccountFrameworkSettingPerformanceLevel']['performance_level_rating'];


        $this->AccountFrameworkSettingPerformanceLevel->deleteAll(array('id' => $pl_id), $cascade = true, $callbacks = true);

        $this->PerformanceLevelDescription->deleteAll(array('performance_level_id' => $pl_id), $cascade = true, $callbacks = true);


        if ($this->AccountFrameworkSettingPerformanceLevel->getAffectedRows() > 0) {

            $framework_settings_details = $this->AccountFrameworkSetting->find("first", array("conditions" => array(
                    "account_tag_id" => $framework_id,
            )));

            $framework_pl_details = $this->AccountFrameworkSettingPerformanceLevel->find("all", array("conditions" => array(
                    "account_framework_setting_id" => $framework_settings_details['AccountFrameworkSetting']['id'],
            )));


            foreach ($framework_pl_details as $pl) {
                if ($pl['AccountFrameworkSettingPerformanceLevel']['performance_level_rating'] > $delete_level) {
                    $set_level = $pl['AccountFrameworkSettingPerformanceLevel']['performance_level_rating'] - 1;

                    $data = array(
                        'performance_level_rating' => "'" . $set_level . "'"
                    );

                    $this->AccountFrameworkSettingPerformanceLevel->updateAll($data, array('id' => $pl['AccountFrameworkSettingPerformanceLevel']['id']));
                }
            }


            echo json_encode(array('success' => true));
        } else {
            echo json_encode(array('success' => false));
        }
        die;
    }

    function delete_tag_standard() {
        $account_tag_id = $this->request->data['account_tag_id'];

        $this->PerformanceLevelDescription->deleteAll(array('account_tag_id' => $account_tag_id), $cascade = true, $callbacks = true);

        $this->AccountCommentTag->deleteAll(array('account_tag_id' => $account_tag_id), $cascade = true, $callbacks = true);

        $this->DocumentStandardRating->deleteAll(array('standard_id' => $account_tag_id), $cascade = true, $callbacks = true);

        $this->AccountTag->deleteAll(array('account_tag_id' => $account_tag_id), $cascade = true, $callbacks = true);
        if ($this->AccountTag->getAffectedRows() > 0) {
            echo json_encode(array('success' => true));
        } else {
            echo json_encode(array('success' => false));
        }
        die;
    }

    function _get_document_standard_ratings_count($account_id) {
        return $this->DocumentStandardRating->query("SELECT COUNT(id) as ratings_count FROM `document_standard_ratings` WHERE rating_id IN (SELECT account_meta_data_id FROM `account_meta_data` WHERE account_id = " . $account_id . " AND meta_data_name LIKE '%metric_value_%' AND site_id = " . $this->site_id . ")")[0][0]['ratings_count'];
    }

    function global_export($account_id) {
        $data = [];
        $data['account_id'] = $account_id;
        $data['account_owner_email'] = $this->_get_account_owner_email($account_id);
        $data['export_type'] = $this->request->data['export_type'];
        $data['file_type'] = isset($this->request->data['file_type']) && !empty($this->request->data['file_type']) ? $this->request->data['file_type'] : "csv";

        if ($data['export_type'] == "general") {
            $export_fields = $this->request->data['export_fields'];
            $data['export_query'] = $this->_format_general_export_query($export_fields, $account_id);
        } elseif ($data['export_type'] == "performance_level") {
            $data['export_query'] = $this->_format_performance_level_export_query($account_id);
        }

        $this->_create_global_export_request($data);
        $this->Session->setFlash($this->language_based_messages['the_global_export_request'], 'default', array('class' => 'message success'));
        $this->redirect('/accounts/account_settings_all/'.$account_id.'/8');
        // var_dump($data);exit;
    }

    function _create_global_export_request($data){
        App::import("Model", "JobQueue");
        $db = new JobQueue();
        $requestXml = '<GlobalExportJob>
                        <account_id>' . $data['account_id'] . '</account_id>
                        <account_owner_email>' . $data['account_owner_email'] . '</account_owner_email>
                        <file_type>' . $data['file_type'] . '</file_type>
                        <export_type>' . $data['export_type'] . '</export_type>
                        <export_query>' . $data['export_query'] . '</export_query>
                       </GlobalExportJob>';
        $job_data = array(
            'JobId' => '6',
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $requestXml,
            'JobQueueStatusId' => 1,
            'CurrentRetry' => 0,
        );
        $db->create();
        $db->save($job_data);
    }

    function _format_general_export_query($fields=[], $account_id){
        $query = "
        (SELECT 
        accounts.`company_name` AS 'Account Name', 
        accounts.`id` AS 'Accout-GUID', 
        account_folders.`name` AS 'Huddle Name', 
        account_folders.`account_folder_id` AS 'Huddle-GUID', 
        account_folder_documents.`title` AS 'Video Name',
        account_folder_documents.`document_id` AS 'Video-GUID',
        users.`id` AS 'User-GUID', 
        users.`institution_id` AS 'Institution ID',
        users.`username` AS 'Username', 
        users.`first_name` AS 'First Name', 
        users.`last_name` AS 'Last Name', 
         
        (	SELECT 
                ( CASE
                    WHEN account_folders_meta_data.meta_data_value='1' AND role_id=200 THEN 'admin'
                    WHEN account_folders_meta_data.meta_data_value='1' AND role_id=210 THEN 'member'
                    WHEN account_folders_meta_data.meta_data_value='1' AND role_id=220 THEN 'viewer'
                    WHEN account_folders_meta_data.meta_data_value='2' AND role_id=200 THEN 'coach'
                    WHEN account_folders_meta_data.meta_data_value='2' AND role_id=210 THEN 'coachee'
                    WHEN account_folders_meta_data.meta_data_value='2' AND role_id=220 THEN 'coachee'
                    WHEN account_folders_meta_data.meta_data_value='3' AND role_id=200 THEN 'assessor'
                    WHEN account_folders_meta_data.meta_data_value='3' AND role_id=210 THEN 'assessee'
                    WHEN account_folders_meta_data.meta_data_value='3' AND role_id=220 THEN 'assessee'
                END ) AS user_role
            FROM account_folder_users WHERE user_id=comments.`created_by` AND account_folder_id=account_folders.`account_folder_id` LIMIT 1) AS user_type 
        
        ";

        if( in_array("comment", $fields) ){
            $query .= ", comments.`comment` AS 'Comment' ";
        }
        if( in_array("time_stamp", $fields) ){
            $query .= ", (IF(comments.time=0, 'ALL', TIME_FORMAT(SEC_TO_TIME(comments.time),'%H:%i:%s'))) AS 'Time Stamp' ";
        }
        if( in_array("date_stamp", $fields) ){
            $query .= ", comments.`created_date` AS 'Date Stamp' ";
        }
        if( in_array("framework_name", $fields) ){
            $query .= ", (SELECT account_tags.`tag_title` FROM account_tags WHERE account_tags.`account_id`=".$account_id." AND account_tags.`account_tag_id` IN (SELECT account_tags.`framework_id` FROM account_tags WHERE  account_tags.`account_id`=".$account_id." AND account_tags.`account_tag_id` = tbl_act_standards.`account_tag_id` ) ) AS 'Framework Name' ";
        }
        if( in_array("tagged_standards", $fields) ){
            $query .= ", TRIM( CONCAT( COALESCE((SELECT tag_code FROM account_tags WHERE account_tag_id = tbl_act_standards.`account_tag_id` ),''),' ',tbl_act_standards.`tag_title`) ) AS `Tagged Standards` ";
        }
        if( in_array("custom_marker_tags", $fields) ){
            $query .= ", tbl_act_custom_markers.`tag_title` AS `Custom Marker` ";
        }
        if( in_array("attachment_file_names", $fields) ){
            $query .= ", (SELECT GROUP_CONCAT(original_file_name SEPARATOR ', ') FROM `documents` INNER JOIN `comment_attachments` ON documents.`id` = `comment_attachments`.`document_id` WHERE comment_attachments.`comment_id` = comments.`id`) AS 'Attachment File Names' ";
        }

        $query .= "
                FROM
                accounts 
                INNER JOIN account_folders 
                  ON accounts.`id` = account_folders.`account_id` 
                LEFT JOIN account_folder_documents 
                  ON account_folders.`account_folder_id` = account_folder_documents.`account_folder_id` 
                LEFT JOIN account_folders_meta_data 
                  ON account_folders.`account_folder_id` = account_folders_meta_data.`account_folder_id` 
                LEFT JOIN documents 
                  ON account_folder_documents.`document_id` = documents.`id` 
                LEFT JOIN comments 
                  ON documents.`id` = comments.`ref_id` AND comments.ref_type IN (2,3,6) 
                LEFT JOIN users 
                  ON comments.`created_by` = users.`id`
        ";

        $query .= "
            LEFT OUTER JOIN account_comment_tags AS `tbl_act_standards` ON (comments.id=tbl_act_standards.`comment_id` AND tbl_act_standards.`ref_type`=0)
            LEFT OUTER JOIN account_comment_tags AS `tbl_act_custom_markers` ON (comments.id=tbl_act_custom_markers.`comment_id` AND tbl_act_custom_markers.`ref_type`=2)
            LEFT OUTER JOIN account_tags ON (tbl_act_standards.`account_tag_id`=account_tags.`framework_id`)
        ";
/*
        if( in_array("tagged_standards","framework_name", $fields) ) {
            $query .= "
            LEFT OUTER JOIN account_comment_tags AS `tbl_act_standards` ON (comments.id=tbl_act_standards.`comment_id` AND tbl_act_standards.`ref_type`=0)
            ";
        }
        if( in_array("custom_marker_tags", $fields) ) {
            $query .= "
            LEFT OUTER JOIN account_comment_tags AS `tbl_act_custom_markers` ON (comments.id=tbl_act_custom_markers.`comment_id` AND tbl_act_custom_markers.`ref_type`=1)
            ";
        }
        if( in_array("framework_name","tagged_standards", $fields) ) {
            $query .= "
            LEFT OUTER JOIN account_tags ON (tbl_act_standards.`account_tag_id`=account_tags.`framework_id`)
            ";
        }
*/

        $query .= "
        WHERE 
        account_folders.`account_id`=".$account_id." AND folder_type=1 AND 
        account_folders_meta_data.`meta_data_name`='folder_type' AND 
        account_folders.`active`='1'
        AND (documents.`doc_type` IN (1,3) OR documents.`doc_type` IS NULL )
        AND (comments.`ref_type` IN (2,3,6,1,4) OR comments.`ref_type` IS NULL )
        )";
        
        $query .= "UNION ALL ";
        
        
        $query .= "(SELECT 
  accounts.`company_name` AS 'Account Name',
  accounts.`id` AS 'Accout-GUID',
  account_folders.`name` AS 'Huddle Name',
  account_folders.`account_folder_id` AS 'Huddle-GUID',
  NULL AS 'Video Name',
  NULL AS 'Video-GUID',
  users.`id` AS 'User-GUID',
  users.`institution_id` AS 'Institution ID',
  users.`username` AS 'Username',
  users.`first_name` AS 'First Name',
  users.`last_name` AS 'Last Name',
  (SELECT 
    (
      CASE
        WHEN account_folders_meta_data.meta_data_value = '1' 
        AND role_id = 200 
        THEN 'admin' 
        WHEN account_folders_meta_data.meta_data_value = '1' 
        AND role_id = 210 
        THEN 'member' 
        WHEN account_folders_meta_data.meta_data_value = '1' 
        AND role_id = 220 
        THEN 'viewer' 
        WHEN account_folders_meta_data.meta_data_value = '2' 
        AND role_id = 200 
        THEN 'coach' 
        WHEN account_folders_meta_data.meta_data_value = '2' 
        AND role_id = 210 
        THEN 'coachee' 
        WHEN account_folders_meta_data.meta_data_value = '2' 
        AND role_id = 220 
        THEN 'coachee' 
        WHEN account_folders_meta_data.meta_data_value = '3' 
        AND role_id = 200 
        THEN 'assessor' 
        WHEN account_folders_meta_data.meta_data_value = '3' 
        AND role_id = 210 
        THEN 'assessee' 
        WHEN account_folders_meta_data.meta_data_value = '3' 
        AND role_id = 220 
        THEN 'assessee' 
      END
    )) AS user_type ";
        
           if( in_array("comment", $fields) ){
            $query .= ", NULL AS 'Comment' ";
        }
        if( in_array("time_stamp", $fields) ){
            $query .= ", NULL AS 'Time Stamp' ";
        }
        if( in_array("date_stamp", $fields) ){
            $query .= ", NULL AS 'Date Stamp' ";
        }
        if( in_array("framework_name", $fields) ){
            $query .= ", NULL AS 'Framework Name' ";
        }
        if( in_array("tagged_standards", $fields) ){
            $query .= ", NULL AS `Tagged Standards` ";
        }
        if( in_array("custom_marker_tags", $fields) ){
            $query .= ", NULL AS `Custom Marker` ";
        }
        if( in_array("attachment_file_names", $fields) ){
            $query .= ", NULL AS 'Attachment File Names' ";
        }
        
        
        $query .= "
               FROM
            accounts 
            INNER JOIN account_folders 
              ON accounts.`id` = account_folders.`account_id` 
            LEFT JOIN account_folders_meta_data 
              ON account_folders.`account_folder_id` = account_folders_meta_data.`account_folder_id` 
            LEFT JOIN `account_folder_users` 
              ON account_folders.`account_folder_id` = account_folder_users.`account_folder_id` 
            LEFT JOIN users 
              ON account_folder_users.`user_id` = users.`id` 
        ";
        
        
                    $query .= "WHERE account_folders.`account_id` = ".$account_id." 
             AND account_folders.folder_type = 1 
             AND account_folders_meta_data.`meta_data_name` = 'folder_type' 
             AND account_folders.`active` = '1' 
             AND users.id NOT IN 
             (SELECT 
            comments.created_by 
          FROM
            comments 
            JOIN documents 
              ON comments.ref_id = documents.id
            JOIN `account_folder_documents` 
              ON account_folder_documents.document_id = documents.id
            JOIN `account_folders`
              ON account_folders.account_folder_id = account_folder_documents.account_folder_id
          WHERE account_folders.account_id = ".$account_id."  AND comments.ref_type IN (2,3,6) AND account_folders.folder_type = '1')) ";
                    
                    
       $query .= " UNION ALL ";
       
       $query .= "(SELECT 
                    accounts.`company_name` AS 'Account Name',
                    accounts.`id` AS 'Accout-GUID',
                    NULL AS 'Huddle Name',
                    NULL AS 'Huddle-GUID',
                    NULL AS 'Video Name',
                    NULL AS 'Video-GUID',
                    users.`id` AS 'User-GUID',
                    users.`institution_id` AS 'Institution ID',
                    users.`username` AS 'Username',
                    users.`first_name` AS 'First Name',
                    users.`last_name` AS 'Last Name',
                    NULL AS user_type ";
       
          if( in_array("comment", $fields) ){
            $query .= ", NULL AS 'Comment' ";
        }
        if( in_array("time_stamp", $fields) ){
            $query .= ", NULL AS 'Time Stamp' ";
        }
        if( in_array("date_stamp", $fields) ){
            $query .= ", NULL AS 'Date Stamp' ";
        }
        if( in_array("framework_name", $fields) ){
            $query .= ", NULL AS 'Framework Name' ";
        }
        if( in_array("tagged_standards", $fields) ){
            $query .= ", NULL AS `Tagged Standards` ";
        }
        if( in_array("custom_marker_tags", $fields) ){
            $query .= ", NULL AS `Custom Marker` ";
        }
        if( in_array("attachment_file_names", $fields) ){
            $query .= ", NULL AS 'Attachment File Names' ";
        }
        
        
        $query .= " FROM
        accounts 
        LEFT JOIN `users_accounts` 
          ON accounts.`id` = users_accounts.`account_id` 
        LEFT JOIN `users` 
          ON users_accounts.`user_id` = users.`id` ";
        
        
        
        $query .= " WHERE accounts.`id` = ".$account_id." 
        AND users.id NOT IN 
        (SELECT 
          comments.created_by 
        FROM
          comments 
          JOIN documents 
            ON comments.ref_id = documents.id 
        WHERE documents.account_id = ".$account_id.") 
        AND users.id NOT IN 
        (SELECT 
          account_folder_users.`user_id` 
        FROM
          `account_folder_users` 
          JOIN `account_folders` 
            ON account_folder_users.`account_folder_id` = account_folders.`account_folder_id` 
        WHERE account_folders.`account_id` = ".$account_id.")) ";
        
        

        return $query;
    }

    function _format_performance_level_export_query($account_id){
        $query = "SELECT
                        ac.`company_name` AS 'Account Name',
                        ac.`id` AS 'Accout GUID',
                        af.name AS 'Huddle Name',
                        af.account_folder_id AS 'Huddle-GUID',
                        afd.title AS 'Video Name',
                        afd.document_id AS 'Video GUID',
                        (    SELECT
                            `account_folder_users`.`user_id` AS coachee_id
                        FROM account_folder_users WHERE account_folder_users.account_folder_id = af.`account_folder_id`  AND role_id=210 GROUP BY af.`account_folder_id`) AS 'Coachee User GUID',
                        (    SELECT
                            GROUP_CONCAT( CONCAT(users.`first_name`, ' ', users.`last_name`) SEPARATOR ', ') AS coachee_id
                        FROM account_folder_users INNER JOIN users ON (account_folder_users.`user_id`=users.`id`) WHERE account_folder_users.account_folder_id=af.`account_folder_id` AND role_id=200 GROUP BY af.`account_folder_id`) AS 'Coach(s)',
                        act.tag_title AS 'Standard',
                        ( CASE WHEN dsr.`rating_value` IS NULL THEN 0 ELSE dsr.`rating_value` END ) AS 'Rating'
                    FROM
                    `account_comment_tags` act
                    INNER JOIN `account_folder_documents` afd ON afd.`document_id` = act.`ref_id` 
                    LEFT JOIN `document_standard_ratings` dsr ON dsr.`standard_id` = act.`account_tag_id` AND dsr.`document_id` = act.`ref_id` 
                    INNER JOIN `account_folders` af ON af.`account_folder_id` = afd.`account_folder_id` 
                    INNER JOIN `account_folders_meta_data` afmd  ON afmd.`account_folder_id` = afd.`account_folder_id` AND afmd.`meta_data_name` = 'folder_type' 
                    INNER JOIN accounts ac ON ac.`id` = af.`account_id` 
                    WHERE 
                    act.`ref_type` = '0' 
                    AND afmd.`meta_data_value` = '2' 
                    AND af.`account_id` = ".$account_id." 
                    AND af.`active`='1' ";

        return $query;
    }

    function _get_account_owner_email($account_id){
        $joins = array(
            'table' => 'users_accounts',
            'type' => 'inner',
            'conditions' => array('User.id = users_accounts.user_id')
        );
        $result = $this->User->find('first', array(
            'joins' => array($joins),
            'conditions' => array('users_accounts.account_id' => $account_id, 'users_accounts.role_id' => 100),
            'fields' => array('email')
        ));

        if(empty($result['User']['email'])){
            die("Account Owner Email not found @ File: " . __FILE__ . " Line: ".__LINE__);
        }

        return $result['User']['email'];
    }

}

?>
