<?php

class NotificationsController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Notifications';
    var $helpers = array('Custom', 'Browser');
    public $uses = array("Observations", "AccountFolderObservationUsers", "ObservationNoticeLog");
    public $components = array('Email');

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    public function sendNotifications() {
        $this->AccountFolderObservationUsers->create;

        $fields = array(
            'observations.observation_date_time',
            'observations.is_private',
            'observations.location_name',
            'observations.account_folder_observation_id',
            'account_folders.name',
            'account_folders.account_id',
            'AccountFolderObservationUsers.user_id',
            'AccountFolderObservationUsers.role_id',
            'AccountFolderObservationUsers.notify_at',
            'AccountFolderObservationUsers.notify_at_unit',
            'Users.*'
        );

        $joins = array(
            array(
                'table' => 'users as Users',
                'type' => 'LEFT',
                'conditions' => array('AccountFolderObservationUsers.user_id = Users.id')
            ),
            array(
                'table' => 'account_folder_observations as observations',
                'type' => 'LEFT',
                'conditions' => array('AccountFolderObservationUsers.account_folder_observation_id = observations.account_folder_observation_id')
            ),
            array(
                'table' => 'account_folders as account_folders',
                'type' => 'LEFT',
                'conditions' => array('observations.account_folder_id = account_folders.account_folder_id')
            )
        );
        $result = $this->AccountFolderObservationUsers->find('all', array(
            'joins' => $joins,
            'fields' => $fields
        ));
        if (is_array($result) && count($result) > 0) {
            foreach ($result as $obsUserRow):
                //GET NOTIFICAION TIME
                $notifyTimeInput = $obsUserRow['AccountFolderObservationUsers']['notify_at'];
                $observationTime = new DateTime($obsUserRow['observations']['observation_date_time']);
                switch ($obsUserRow['AccountFolderObservationUsers']['notify_at_unit']) {
                    // UNIT IS MINUTES
                    case '1':
                        $notifyMeBeforeMin = $notifyTimeInput;
                        break;
                    // UNIT IS HOURS
                    case '2':
                        $notifyMeBeforeMin = $notifyTimeInput * 60;
                        break;
                    // UNIT IS DAYS
                    case '3':
                        $notifyMeBeforeMin = ($notifyTimeInput * 24) * 60;
                        break;

                    default:
                        break;
                }

                $notificationTime = $observationTime->sub(date_interval_create_from_date_string($notifyMeBeforeMin . ' min'));
                $observationTime = new DateTime($obsUserRow['observations']['observation_date_time']);

                $currentTime = new DateTime();
                $diffrenceOBS_CURR = $currentTime->diff($notificationTime);
                $diffrenceInMIN = $diffrenceOBS_CURR->format('%i');
                $conditions = array('account_folder_observation_id' => $obsUserRow['observations']['account_folder_observation_id'], 'observation_date_time' => $obsUserRow['observations']['observation_date_time'], 'user_id' => $obsUserRow['AccountFolderObservationUsers']['user_id']);

                $logResult = $this->ObservationNoticeLog->find('count', array('conditions' => $conditions));
                if ($logResult < 1) {
                    if ($diffrenceInMIN < '30') {
                        if ($obsUserRow['observations']['is_private'] == '1') {
                            if ($obsUserRow['AccountFolderObservationUsers']['role_id'] != '300') {
                                $sendEmail = false;
                            } else {
                                $sendEmail = true;
                            }
                        } else {
                            $sendEmail = true;
                        }
                        if ($sendEmail) {
                            $this->Email->delivery = 'smtp';
                            $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
                            $this->Email->to = $to_email;
                            $this->Email->subject = "Sibme Observation : " . $obsUserRow['account_folders']['name'] . ", at " . $obsUserRow['observations']['location_name'] . ' on ' . $observationTime->format('D, d M Y h:i a');
                            $this->Email->template = 'default';
                            $this->Email->sendAs = 'html';
                            $view = new View($this, true);
                            $params = array(
                                'data' => $obsUserRow
                            );
                            $html = $view->element('emails/observation_notice', $params);
                            $this->AuditEmail->create();
                            $auditEmail = array(
                                'account_id' => $obsUserRow['account_folders']['account_id'],
                                'email_from' => $this->Email->from,
                                'email_to' => $to_email,
                                'email_subject' => $this->Email->subject,
                                'email_body' => $html,
                                'is_html' => true,
                                'sent_date' => date("Y-m-d H:i:s")
                            );
                            $this->AuditEmail->save($auditEmail, $validation = TRUE);
                            if ($this->Email->send($html)) {
//                            if (print($html)) {
                                $this->ObservationNoticeLog->create();
                                $noticAlertArray = array(
                                    'account_folder_observation_id' => $obsUserRow['observations']['account_folder_observation_id'],
                                    'observation_date_time' => $obsUserRow['observations']['observation_date_time'],
                                    'user_id' => $obsUserRow['AccountFolderObservationUsers']['user_id'],
                                    'is_sent' => '1',
                                    'created_on' => date("Y-m-d H:i:s")
                                );
                                $this->ObservationNoticeLog->save($noticAlertArray);
                            }
                        }
                    }
                }
            endforeach;
        }
        die();
    }

}
