<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class AnalyticalReportsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $uses = array(
        'Account',
        'AccountTag',
        'UsecountFolderUser',
        'AcrsAccount',
        'AccountFolder',
        'AccountFolderUser',
        'AccountFolderMetaData',
        'AccountFolderGroup',
        'UserActivityLog',
        'AccountMetaDatum',
        'User',
        'Group',
        'JobQueue',
        'DocumentFiles',
        'Document',
        'UsersAccount',
        'MailchimpTemplate',
        'AccountsMailchimpCampaign',
        'AccountBraintreeSubscription',
        'AuditEmail',
        'AnalyticsReportLog',
        'UserAccount',
        'EmailUnsubscribers',
        'AccountMetaData'
    );
    public $hmh_base_url = '';
    public $sibme_base_url = '';

    /**
     * index method
     *
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);

        $this->site_id = 1;
        $account_meta_data_2 = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => -1,
                "meta_data_name" => "hmh_base_url"
        )));

        $this->hmh_base_url = $account_meta_data_2['AccountMetaData']['meta_data_value'];

        $account_meta_data_1 = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => -1,
                "meta_data_name" => "sibme_base_url"
        )));
        $this->sibme_base_url = $account_meta_data_1['AccountMetaData']['meta_data_value'];
    }

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(
                'weekly_report', 'add_job_queue_for_cron', 'monthly_report', 'weekly_report', 'send_report_monthly', 'getParentTotalUsersWActivity', 'getTotalUsersWActivity', 'getParentTotalUsers', 'getParentTotalActiveInActiveUsers', 'getTotalActiveInActiveUsers', 'getAccountTotalUsers', 'getParentTotalHuddles', 'getAccountTotalHuddles', 'getParentTotalVideos', 'getAccountTotalVideos', 'getParentTotalVideosViewed', 'getAccountTotalVideosViewed', 'getParentTotalVideosCommentsCount', 'getAccountTotalVideosCommentsCount', 'thousandsCurrencyFormat'
        );
    }

    function sendReportAnotherEmail() {
        @set_time_limit(0);
        if (!empty($this->request->data)) {
            $manual_email = $this->request->data['email'];
            $report_type = $this->request->data['report_type'];
            $user_id = $this->request->data['user_id'];
            $account_id = $this->request->data['account_id'];
            $type = $this->request->data['type'];

            if ($report_type == 'wr') {
                $this->weekly_report($account_id, $user_id, $manual_email);
            } else if ($report_type == 'mr') {
                $this->monthly_report($account_id, $user_id, 0, $manual_email);
            } else if ($report_type == 'mnr') {
                $this->monthly_report($account_id, $user_id, 1, $manual_email);
            }
        }
    }

    function weekly_report($ac_id = '', $us_id = '', $manual_email = '') {

        ini_set('memory_limit', '1024M');
        set_time_limit(0);
        ini_set('max_execution_time', 0); //0 Unlimited

        $view = new View($this, false);

        $accounts = $this->Account->find("all", array("conditions" => array(
                "Account.is_active" => '1',
                "Account.is_suspended" => '0',
            //'Account.id' => array(4322, 4318, 2936, 4031, 4027, 4157)
        )));

        $account_ids = array();
        if ($accounts) {
            foreach ($accounts as $row) {
                $account_ids[] = $row['Account']['id'];
            }
        }

        if (count($account_ids) > 0) {

            //$date_from = date('Y-m-d', strtotime('-7 days')) . " 00:00:00";
            //$date_to = date('Y-m-d') . " 23:59:59";

            $previous_week = strtotime("-1 week +1 day");

            $date_from = strtotime("last sunday midnight", $previous_week);
            $date_to = strtotime("next saturday", $date_from);

            $date_from = date("Y-m-d", $date_from);
            $date_to = date("Y-m-d", $date_to);

            $prev_date_from = '2017-06-29' . " 00:00:00";
            $prev_date_to = '2017-07-06' . " 23:59:59";
            $template_data = $this->MailchimpTemplate->find("first", array("conditions" => array(
                    "MailchimpTemplate.type" => 'parent'
            )));
            $template_contents = '';
            $type = 'Sibme Weekly Summary';

            $template_data = $this->weekly_report_template($ac_id, $us_id, 9);           

            if (!empty($ac_id) && !empty($us_id)) {

                $user_account_details = $this->UserAccount->find("first", array("conditions" => array(
                        "account_id" => $ac_id,
                        "user_id" => $us_id
                )));

                $user_details = $this->User->find("first", array("conditions" => array(
                        "id" => $us_id
                )));


                $l_template_data = $template_data;
                if ($user_details['User']['site_id'] != 1) {
                    $l_template_data = str_replace("{{site_base_url}}", $this->hmh_base_url, $l_template_data);
                } else {
                    $l_template_data = str_replace("{{site_base_url}}", $this->sibme_base_url, $l_template_data);
                }

                $l_template_data = str_replace("{{user_account_unsubscribe}}", $ac_id . '/' . $us_id . '/9', $l_template_data);
                $template_contents = $this->generate_template_weekly($ac_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $l_template_data);
               
                if ($this->check_subscription($us_id, 9, $ac_id)) {

                    $in_trial = $this->Account->find("first", array("conditions" => array(
                            "id" => $ac_id
                    )));

                    if (!empty($manual_email)) {

                        if (($in_trial['Account']['in_trial'] == 0) && !$view->Custom->check_if_parent($ac_id) && ($user_account_details['UserAccount']['role_id'] == 100 || $user_account_details['UserAccount']['role_id'] == 110)) {
                            $this->send_report_weekly($template_contents, $ac_id, $us_id, $manual_email, $type);
                        } else {
                            echo 'Weekly Report will only be sent to Child Account Account Onwer and Super Admin';
                            die;
                        }
                    } else {
                        if (($in_trial['Account']['in_trial'] == 0) && !$view->Custom->check_if_parent($ac_id) && ($user_account_details['UserAccount']['role_id'] == 100 || $user_account_details['UserAccount']['role_id'] == 110)) {
                            $this->send_report_weekly($template_contents, $ac_id, $us_id, $user_details['User']['email'], $type, 1);
                        } else {
                            echo 'Weekly Report will only be sent to Child Account Account Onwer and Super Admin';
                            die;
                        }
                    }
                } else {
                    echo 'This User is not subscribed to Weekly Reports.';
                    die;
                }

                echo 'Report sent out successfully.';
                die;
            }


            foreach ($account_ids as $key => $val) {
                $DateTime = new DateTime();
                $DateTime->modify('-8 hours');
                $account_id = $val;

                $account_res = $this->Account->find('first', array(
                    'conditions' => array(
                        'id' => $account_id
                    ))
                );
                $this->site_id = $account_res['Account']['site_id'];
                $template_contents = $this->generate_template_weekly($account_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $template_data);

                $result = $this->UsersAccount->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'users as u',
                            'conditions' => 'u.id = UsersAccount.user_id',
                            'type' => 'inner'
                        )
                    ),
                    'conditions' => array(
                        'UsersAccount.account_id' => $account_id,
                    //  'em_unsb.email_format_id' => 9
                    ),
                    'fields' => 'u.*,UsersAccount.account_id, UsersAccount.role_id',
                    'group' => 'u.id,UsersAccount.account_id'
                ));

                if (count($result) > 0) {

                    foreach ($result as $user) {


                        $DateTime = new DateTime();
                        $DateTime->modify('-8 hours');

                        $time_from = $DateTime->format("Y-m-d H:i:s");
                        $time_to = date('Y-m-d') . " 23:59:59";
                        $user_account_details = $this->UserAccount->find("first", array("conditions" => array(
                                "account_id" => $user['UsersAccount']['account_id'],
                                "user_id" => $user['u']['id'],
                        )));


                        $user_email = $user['u']['email'];
                        $account_id = $user['UsersAccount']['account_id'];
                        $in_trial = $this->Account->find("first", array("conditions" => array(
                                "id" => $account_id,
                        )));
                        if ($user['u']['site_id'] != 1) {
                            $template_contents = str_replace("{{site_base_url}}", $this->hmh_base_url, $template_contents);
                        } else {
                            $template_contents = str_replace("{{site_base_url}}", $this->sibme_base_url, $template_contents);
                        }

                        $template_contents = str_replace("{{user_account_unsubscribe}}", $account_id . '/' . $user['u']['id'] . '/9', $template_contents);

                        // $template_contents = $this->generate_template_weekly($account_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $template_data);
                        if ($user['u']['id'] == 2423 || $user['u']['id'] == 2427)
                            continue;
                        if ($user['u']['type'] != 'Active')
                            continue;
                        if ($user['u']['is_active'] != '1')
                            continue;
                        if ($this->check_subscription($user['u']['id'], 9, $account_id)) {
                            if (($in_trial['Account']['in_trial'] == 0) && !$view->Custom->check_if_parent($account_id) && ($user_account_details['UserAccount']['role_id'] == 100 || $user_account_details['UserAccount']['role_id'] == 110)) {
                                if (!empty($manual_email)) {
                                    $this->send_report_weekly($template_contents, $account_id, $user['u']['id'], $manual_email, $type, 0);
                                } else {
                                    $this->send_report_weekly($template_contents, $account_id, $user['u']['id'], $user_email, $type, 1);
                                }
                            }
                        }
                    }
                }
            }
            echo 'Report sent out successfully to all via CRON.';
            return true;
        }
    }

    function generate_template_weekly($account_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $template_data, $user_id = '') {

        $parent_account = $this->Account->find('all', array(
            'conditions' => array(
                'Account.id' => $account_id
            )
                )
        );

        $content_html = htmlspecialchars_decode($template_data);

        if (isset($parent_account) && count($parent_account) > 0) {

            $account_name_header = $parent_account[0]['Account']['company_name'];
            $content_html = str_replace("{company_name}", $account_name_header, $content_html);
        }


        $coach_collab_count = $this->UsersAccount->query("SELECT

              COUNT(amd.`account_folder_meta_data_id`) AS coaching_huddles,
              COUNT(amd2.`account_folder_meta_data_id`) AS collab_huddles,
               COUNT(amd3.`account_folder_meta_data_id`) AS eval_huddles
            FROM
              account_folders AS af
              LEFT JOIN accounts AS acc
                ON acc.`id` = af.`account_id`
              LEFT JOIN `account_folders_meta_data` AS amd
                ON (amd.`account_folder_id` = af.`account_folder_id` AND amd.`meta_data_name` ='folder_type' AND amd.`meta_data_value` ='2' )
                LEFT JOIN `account_folders_meta_data` AS amd2
                ON (amd2.`account_folder_id` = af.`account_folder_id` AND amd2.`meta_data_name` ='folder_type' AND amd2.`meta_data_value` ='1' )
                LEFT JOIN `account_folders_meta_data` AS amd3
                ON (amd3.`account_folder_id` = af.`account_folder_id` AND amd3.`meta_data_name` ='folder_type' AND amd3.`meta_data_value` ='3' )
            WHERE af.folder_type = 1
             AND af.site_id = " . $this->site_id . "
              AND  DATE_FORMAT(af.created_date, '%Y-%m-%d') BETWEEN '" . $date_from . "' AND '" . $date_to . "' AND acc.`id` = '" . $account_id . "' ");


        $web_video_uploads = $this->UsersAccount->query("select account_id, a.company_name, 'Web' upload_type, d.created_date, concat(u.`first_name`, ' ', u.`last_name`) uploaded_user from documents d join accounts a on a.id = d.account_id join users u on u.id = d.created_by where recorded_date >= '" . $date_from . "' and recorded_date <= '" . $date_to . "' and doc_type in (3,1) and a.id = '" . $account_id . "'  and d.active = 1 and d.site_id=" . $this->site_id . " and d.published =1
and d.id in (select ref_id from user_activity_logs where type=2 and (environment_type=2 or environment_type is null) ) and a.site_id=" . $this->site_id . " order by d.created_date asc");

        $iOS_video_uploads = $this->UsersAccount->query("select account_id, a.company_name, 'iOS' upload_type, d.created_date, concat(u.`first_name`, ' ', u.`last_name`) uploaded_user  from documents d join accounts a on a.id = d.account_id join users u on u.id = d.created_by where recorded_date >= '" . $date_from . "' and recorded_date <= '" . $date_to . "' and doc_type in (3,1) and d.active = 1 and d.site_id=" . $this->site_id . " and a.id = '" . $account_id . "' and  d.published =1
and d.id in (select ref_id from user_activity_logs where type=2 and environment_type=1) and a.site_id=" . $this->site_id . " order by d.created_date asc");

        $Andriod_video_uploads = $this->UsersAccount->query("select account_id, a.company_name, 'Android/Copy' upload_type, d.created_date, concat(u.`first_name`, ' ', u.`last_name`) uploaded_user from documents d join accounts a on a.id = d.account_id join users u on u.id = d.created_by where recorded_date >= '" . $date_from . "' and recorded_date <= '" . $date_to . "' and doc_type in (3,1) and d.active = 1 and a.id = '" . $account_id . "' and a.site_id=" . $this->site_id . " and d.published =1
and d.id not in (
select id from documents d where recorded_date >= '" . $date_from . "'  and recorded_date <= '" . $date_to . "'  and doc_type in (3,1) and active = 1 and published =1
and id in (select ref_id from user_activity_logs where type=2 and (environment_type=2 or environment_type is null) ) )");

        $videos_uploaded = count($web_video_uploads) + count($iOS_video_uploads) + count($Andriod_video_uploads);

        $comments = $this->UsersAccount->query("SELECT
            d.`account_id`,
            a.`company_name`,
            c.created_date,
            concat(u.`first_name`, ' ', u.`last_name`),
            case when ref_type = 3 then 1 else 0 end as reply
            FROM
              comments c join documents d on c.ref_id = d.id
              join accounts a on a.id = d.account_id
              join users u on u.id = c.created_by
            WHERE ref_type in (2,3)
            AND a.`id` = " . $account_id . "
            AND c.site_id =" . $this->site_id . "
              AND c.created_date >= '" . $date_from . "' and c.created_date <= '" . $date_to . "'
            ");


        $new_users = $this->UsersAccount->query("select u.username,u.email,u.is_active, u.created_date
from users u
left join users_accounts ua on ua.user_id = u.id
where u.created_date >= '" . $date_from . "' and u.created_date <= '" . $date_to . "' and u.is_active=1 and u.site_id =" . $this->site_id . " and ua.account_id = '" . $account_id . "' ");

        $total_users = $this->UsersAccount->query("select u.username,u.email,u.is_active, u.created_date
from users u
left join users_accounts ua on ua.user_id = u.id
where ua.account_id = '" . $account_id . "' and u.site_id =" . $this->site_id);

        $active_users = $this->UsersAccount->query("select u.username,u.email,u.is_active, u.created_date
from users u
left join users_accounts ua on ua.user_id = u.id
where u.is_active=1 and ua.account_id = '" . $account_id . "' and ua.site_id=" . $this->site_id);

        $video_viewed = $this->UsersAccount->query("SELECT * FROM `user_activity_logs` WHERE type = 12 AND account_id = '" . $account_id . "' and date_added >= '" . $date_from . "' and date_added <= '" . $date_to . "' and site_id=" . $this->site_id);
        $mydate = getdate(strtotime($date_to));
        $mydate2 = getdate(strtotime($date_from));
        $result_date = $mydate2['month'] . ' ' . $mydate2['mday'] . ' - ' . $mydate['month'] . ' ' . $mydate['mday'];


        $total_huddes = $coach_collab_count[0][0]['coaching_huddles'] + $coach_collab_count[0][0]['collab_huddles'] + $coach_collab_count[0][0]['eval_huddles'];

        $total_video_watched_where = '';
        if ($date_from != '' && $date_to != '') {
            $total_video_watched_where = "dvh.created_date >= '$date_from' and dvh.created_date <= '$date_to'";
        }
        $total_video_watched = $this->User->find('all', array(
            'joins' => array(
                array(
                    'table' => 'document_viewer_histories as dvh',
                    'conditions' => 'dvh.user_id = User.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'account_folder_documents as afd',
                    'conditions' => 'afd.document_id = dvh.document_id',
                    'type' => 'inner'
                ),
            ),
            'conditions' => array(
                'dvh.account_id' => $account_id,
                $total_video_watched_where
            ),
            'group' => 'dvh.document_id , dvh.user_id',
            'fields' => 'max(dvh.minutes_watched) as minutes_watched'
        ));

        $total_min_watched = 0;
        if ($total_video_watched) {
            foreach ($total_video_watched as $row) {
                $total_min_watched = $total_min_watched + $row[0]['minutes_watched'];
            }
        }
        if ($total_min_watched > 0) {
            $total_min_watched = round(($total_min_watched / 60) / 60, 2);
        } else {
            $total_min_watched = 0;
        }

        $active_users = $this->getAccountTotalUsersWActivity($account_id, $date_from, $date_to);
        $total_huddes = $this->getAccountTotalHuddles($account_id, $date_from, $date_to);
        $videos_uploaded = $this->getAccountTotalVideos($account_id, $date_from, $date_to);
        $comments = $this->getAccountTotalVideosCommentsCount($account_id, $date_from, $date_to);
        $total_users = $this->getTotalActiveInActiveUsers($account_id);

        $content_html = str_replace("{total_huddles}", $total_huddes, $content_html);

        $content_html = str_replace("{video_uploded}", $videos_uploaded, $content_html);
        $content_html = str_replace("{video_comments_added}", $comments, $content_html);
        $content_html = str_replace("{users_added}", count($new_users), $content_html);
        $content_html = str_replace("{total_users}", $total_users, $content_html);
        $content_html = str_replace("{active_users}", $active_users, $content_html);
        $content_html = str_replace("{video_viewed}", $total_min_watched, $content_html);
        $content_html = str_replace("{date_range_header}", $result_date, $content_html);
        return $content_html;
    }

    function send_report_weekly($contents, $account_id, $user_id, $emails, $type, $sent_via_cron = 0) {


        $DateTime = new DateTime();
        $DateTime->modify('-8 hours');
        $Email = new CakeEmail();
        $time_from = $DateTime->format("Y-m-d H:i:s");
        $time_to = date('Y-m-d') . " 23:59:59";


        /* if (empty($this->request->data['email'])) {

          $result = $this->AnalyticsReportLog->find('all', array(
          'conditions' => array(
          "sent_at >= '$time_from' AND sent_at <='$time_to'",
          'sent_to' => $emails,
          'sent_subject' => $type
          )
          ));

          if (count($result) > 0) {
          return true;
          }

          } */

        $Email->delivery = 'smtp';
        $Email->from = "do-not-reply@sibme.com";
        if (Configure::read('analytical_report_test') == 1 && empty($this->request->data['email'])) {
            $Email->to = Configure::read('analytical_report_test_to');
        } else {
            $Email->to = $emails;
        }

        $Email->subject = $type;
        $Email->template = 'default';
        $Email->sendAs = 'html';
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $Email->from,
            'email_to' => $Email->to,
            'email_subject' => $Email->subject,
            'email_body' => $contents,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );


        $this->AuditEmail->save($auditEmail, $validation = TRUE);


        $this->AnalyticsReportLog->create();
        $report_log = array(
            'report_type' => $type,
            'account_id' => $account_id,
            'user_id' => $user_id,
            'sent_to' => $Email->to,
            'sent_from' => $Email->from,
            'sent_subject' => $Email->subject,
            'sent_at' => date("Y-m-d H:i:s"),
            'audit_email_id' => $this->AuditEmail->id,
            'sent_via_cron' => $sent_via_cron
        );

        $this->AnalyticsReportLog->save($report_log, $validation = TRUE);

        $use_job_queue = Configure::read('use_job_queue');

        if ($use_job_queue) {
            $this->add_job_queue_for_cron(1, $Email->to, $Email->subject, $contents, true);
            return TRUE;
        } else {
            if ($Email->send($contents)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function send_report_monthly($contents, $account_id, $user_id, $emails, $type, $sent_via_cron = 0) {
        $Email = new CakeEmail();

        set_time_limit(0);
        ini_set('max_execution_time', 0); //0 Unlimited

        $DateTime = new DateTime();
        $DateTime->modify('-8 hours');

        $time_from = $DateTime->format("Y-m-d H:i:s");
        $time_to = date('Y-m-d') . " 23:59:59";
        /*
          if (empty($this->request->data['email'])) {

          $result = $this->AnalyticsReportLog->find('all', array(
          'conditions' => array(
          "sent_at >= '$time_from' AND sent_at <='$time_to'",
          'sent_to' => $emails,
          'sent_subject' => $type
          )
          ));

          if (count($result) > 0) {
          return true;
          }
          }
         */
        $Email->delivery = 'smtp';
        $Email->from = "do-not-reply@sibme.com";

        if (Configure::read('analytical_report_test') == 1 && empty($this->request->data['email'])) {
            $Email->to = Configure::read('analytical_report_test_to');
        } else {
            $Email->to = $emails;
        }

        $Email->subject = $type;
        $Email->template = 'default';
        $Email->sendAs = 'html';
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $Email->from,
            'email_to' => $Email->to,
            'email_subject' => $Email->subject,
            'email_body' => $contents,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);

        $this->AnalyticsReportLog->create();
        $report_log = array(
            'report_type' => $type,
            'account_id' => $account_id,
            'user_id' => $user_id,
            'sent_to' => $Email->to,
            'sent_from' => $Email->from,
            'sent_subject' => $Email->subject,
            'sent_at' => date("Y-m-d H:i:s"),
            'audit_email_id' => $this->AuditEmail->id,
            'sent_via_cron' => $sent_via_cron
        );

        $this->AnalyticsReportLog->save($report_log, $validation = TRUE);


        $use_job_queue = Configure::read('use_job_queue');

        if ($use_job_queue) {
            $this->add_job_queue_for_cron(1, $Email->to, $Email->subject, $contents, true);
            return TRUE;
        } else {
            if ($Email->send($contents)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function add_job_queue_for_cron($jobid, $to, $subject, $html, $is_html = false, $reply_to = '') {
        if ($is_html)
            $ihtml = 'Y';
        else
            $ihtml = 'N';

        if ($reply_to != '') {
            $reply_to = "<replyto>$reply_to</replyto>";
        }
        $html = '<![CDATA[' . $html . ']]>';
        $noreply = "do-not-reply@sibme.com";
        $fromname = "Sibme";
        $xml = "<mail><to>$to</to>$reply_to <from>$noreply</from><fromname>$fromname</fromname><bcc>khurrams@sibme.com</bcc><cc /><subject><![CDATA[$subject]]></subject><body>$html</body><html>$ihtml</html></mail>";

        /* $xml = "<mail><to>khurram@jjtechnical.com</to>$reply_to <from>info@sibme.com</from><fromname>Sibme</fromname><bcc>khurrams@sibme.com</bcc><cc /><subject><![CDATA[$subject]]></subject><body>$html</body><html>$ihtml</html></mail>"; */

        $this->JobQueue->create();
        $data = array(
            'JobId' => $jobid,
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $xml,
            'JobQueueStatusId' => 1,
            'CurrentRetry' => 0
        );

        $this->JobQueue->save($data);
        return TRUE;
    }

    function monthly_report($ac_id = '', $us_id = '', $bool = 0, $manual_email = '') {

        set_time_limit(0);
        ini_set('max_execution_time', 0); //0 Unlimited

        $accounts = $this->Account->find("all", array("conditions" => array(
                "Account.is_active" => '1',
                "Account.is_suspended" => '0',
            //'Account.id' => array(2936, 4318)
        )));


        $bool = 0;
        $account_ids = array();
        if ($accounts) {
            foreach ($accounts as $row) {
                $account_ids[] = $row['Account']['id'];
            }
        }

        $month_ini = new DateTime("first day of last month");
        $month_end = new DateTime("last day of last month");

        $date_from = $month_ini->format('Y-m-d') . " 00:00:00";
        $date_to = $month_end->format('Y-m-d') . " 23:59:59";

        $prev_date_from = date("Y-m-d", strtotime($date_from . "-30 days")) . " 00:00:00";
        $prev_date_to = date("Y-m-d", strtotime($date_to . "-30 days")) . " 23:59:59";

        if (count($account_ids) > 0 && !empty($ac_id) && !empty($us_id)) {


            //$date_from = date('Y-m-d', strtotime('-30 days')) . " 00:00:00";
            //$date_to = date('Y-m-d') . " 23:59:59";




            $template_data = $this->MailchimpTemplate->find("first", array("conditions" => array(
                    "MailchimpTemplate.type" => 'parent'
            )));


            $template_data = $this->monthly_report_template($ac_id, $us_id, 10);
            $type = 'Sibme Summary Report';


            if ($bool) {
                $template_data = $this->network_report_template();
                $type = 'Sibme Network Summary Report';
            }

            $template_contents = '';


            if (!empty($ac_id) && !empty($us_id)) {
                $user_account_details = $this->UserAccount->find("first", array("conditions" => array(
                        "account_id" => $ac_id,
                        "user_id" => $us_id
                )));

                $user_details = $this->User->find("first", array("conditions" => array(
                        "id" => $us_id
                )));

                if ($user_account_details['UserAccount']['role_id'] == 115) {
                    $bool = 0;
                    $type = 'Sibme Summary Report';
                    $template_data = $this->monthly_report_template($ac_id, $us_id, 10);
                    $view = new View($this, false);
                    $user_ids = $view->Custom->get_users_of_admin_circle($ac_id, $us_id);
                    $huddle_ids = $view->Custom->get_user_huddle_ids($ac_id, $us_id);
                    $all_huddle_ids = $view->Custom->get_user_huddle_ids($ac_id, $us_id, 1);
                    $huddle_ids = implode(',', $huddle_ids);
                    $all_huddle_ids = implode(',', $all_huddle_ids);

                    $template_contents = $this->genrate_template($ac_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $template_data, $user_ids, $bool, $all_huddle_ids, $huddle_ids);
                } else {
                    $view = new View($this, false);
                    if ($view->Custom->check_if_parent($ac_id)) {
                        $bool = 1;
                        $type = 'Sibme Network Summary Report';
                        $template_data = $this->network_report_template();
                    }
                    $template_contents = $this->genrate_template($ac_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $template_data, '', $bool);
                }

                if ($user_details['User']['site_id'] != 1) {
                    $template_contents = str_replace("{{site_base_url}}", $this->hmh_base_url, $template_contents);
                } else {
                    $template_contents = str_replace("{{site_base_url}}", $this->sibme_base_url, $template_contents);
                }

                $template_contents = str_replace("{{user_account_unsubscribe}}", $ac_id . '/' . $us_id . '/10', $template_contents);


                //if($this->check_subscription($us_id, 10,$ac_id)){

                $in_trial = $this->Account->find("first", array("conditions" => array(
                        "id" => $ac_id,
                )));

                if (!empty($manual_email)) {
                    if (($in_trial['Account']['in_trial'] == 0) && $user_account_details['UserAccount']['role_id'] == 100 || $user_account_details['UserAccount']['role_id'] == 110 || $user_account_details['UserAccount']['role_id'] == 115) {
                        $this->send_report_monthly($template_contents, $ac_id, $us_id, $manual_email, $type, 0);
                    }
                } else {
                    if (($in_trial['Account']['in_trial'] == 0) && $user_account_details['UserAccount']['role_id'] == 100 || $user_account_details['UserAccount']['role_id'] == 110 || $user_account_details['UserAccount']['role_id'] == 115) {
                        $this->send_report_monthly($template_contents, $ac_id, $us_id, $user_details['User']['email'], $type, 1);
                    }
                }

                //}
                echo 'Report sent out successfully.';
                die;
            }
        }
        if (count($account_ids) > 0) {
            foreach ($account_ids as $key => $val) {
                $account_id = $val;
                $account_res = $this->Account->find('first', array(
                    'conditions' => array(
                        'id' => $account_id
                    ))
                );
                $this->site_id = $account_res['Account']['site_id'];
                $view = new View($this, false);
                if ($view->Custom->check_if_parent($account_id)) {
                    $bool = 1;
                    $type = 'Sibme Network Summary Report';
                    $template_data = $this->network_report_template();
                } else {
                    $bool = 0;
                    $type = 'Sibme Summary Report';
                    $template_data = $this->monthly_report_template($ac_id, $us_id, 10);
                }
                $template_contents = $this->genrate_template($account_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $template_data, '', $bool);
                $result = $this->UsersAccount->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'users as u',
                            'conditions' => 'u.id = UsersAccount.user_id',
                            'type' => 'inner'
                        )
                    ),
                    'conditions' => array(
                        'UsersAccount.account_id' => $account_id,
                    //  'em_unsb.email_format_id' => 9
                    ),
                    'fields' => 'u.*,UsersAccount.account_id, UsersAccount.role_id',
                    'group' => 'u.id,UsersAccount.account_id'
                ));
                if ($result) {
                    foreach ($result as $user) {
                        $user_account_details = $this->UserAccount->find("first", array("conditions" => array(
                                "account_id" => $user['UsersAccount']['account_id'],
                                "user_id" => $user['u']['id'],
                        )));

                        $user_email = $user['u']['email'];
                        $account_id = $user['UsersAccount']['account_id'];
                        $in_trial = $this->Account->find("first", array("conditions" => array(
                                "id" => $account_id,
                        )));
                        /*
                          if ($user['UsersAccount']['role_id'] == 115) {
                          continue;
                          $bool = 0;
                          //  $type = 'Sibme Summary Report';
                          //$template_data = $this->monthly_report_template();
                          //$view = new View($this, false);
                          //$user_ids = $view->Custom->get_users_of_admin_circle($account_id, $user['UsersAccount']['user_id']);
                          //$huddle_ids = $view->Custom->get_user_huddle_ids($account_id, $user['UsersAccount']['user_id']);
                          //$all_huddle_ids = $view->Custom->get_user_huddle_ids($account_id, $user['UsersAccount']['user_id'], 1);
                          //$huddle_ids = implode(',', $huddle_ids);
                          //$all_huddle_ids = implode(',', $all_huddle_ids);
                          //$template_contents = $this->genrate_template($account_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $template_data, $user_ids, $bool, $all_huddle_ids, $huddle_ids);
                          } else {


                          $DateTime = new DateTime();
                          $DateTime->modify('-8 hours');

                          $time_from = $DateTime->format("Y-m-d H:i:s");
                          $time_to = date('Y-m-d') . " 23:59:59";

                          if (empty($this->request->data['email'])) {

                          // $result_details = $this->AnalyticsReportLog->find('all', array(
                          //     'conditions' => array(
                          //         "sent_at >= '$time_from' AND sent_at <='$time_to'",
                          //         'account_id' => $account_id,
                          //         'user_id' => $user['u']['id'],
                          //         'sent_subject' => $type
                          //     )
                          // ));
                          // if (count($result_details) > 0) {
                          //     continue;
                          // }
                          }


                          //$template_contents = $this->genrate_template($account_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $template_data, '', $bool);
                          }
                         */

                        if ($user['u']['id'] == 2423 || $user['u']['id'] == 2427)
                            continue;
                        if ($user['u']['type'] != 'Active')
                            continue;
                        if ($user['u']['is_active'] != '1')
                            continue;

                        //if($this->check_subscription($user['UsersAccount']['user_id'], 10,$account_id)){

                        $template_contents = str_replace("{{user_account_unsubscribe}}", $account_id . '/' . $user['u']['id'] . '/10', $template_contents);
                        if ($user['u']['site_id'] != 1) {
                            $template_contents = str_replace("{{site_base_url}}", $this->hmh_base_url, $template_contents);
                        } else {
                            $template_contents = str_replace("{{site_base_url}}", $this->sibme_base_url, $template_contents);
                        }
                        if (($in_trial['Account']['in_trial'] == 0) && ($user_account_details['UserAccount']['role_id'] == 100 || $user_account_details['UserAccount']['role_id'] == 110)) {
                            if (!empty($manual_email)) {
                                $this->send_report_monthly($template_contents, $account_id, $user['u']['id'], $user_email, $type, 0);
                            } else {
                                $this->send_report_monthly($template_contents, $account_id, $user['u']['id'], $user_email, $type, 1);
                            }
                        }
                        //}
                    }
                }
            }
            echo 'Report sent out successfully to all via CRON.';
            return true;
        }
    }

    function genrate_template($account_id, $date_from, $date_to, $prev_date_from, $prev_date_to, $template_data, $user_id = '', $bool, $all_huddle_ids = '', $huddle_ids = '') {
        $content_html = htmlspecialchars_decode($template_data);

        //  echo $content_html;die;
        $all_account_array = array($account_id);
        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );

        foreach ($child_accounts as $child_account) {
            $all_account_array[] = $child_account['Account']['id'];
        }


        if (!$bool) {
            $all_account_array = array($account_id);
        }

        $parent_account = $this->Account->find('all', array(
            'conditions' => array(
                'Account.id' => $account_id
            )
                )
        );
        if (isset($parent_account) && count($parent_account) > 0) {

            $total_video_watched_where = '';
            if ($date_from != '' && $date_to != '') {
                $total_video_watched_where = "dvh.created_date >= '$date_from' and dvh.created_date <= '$date_to'";
            }
            $total_video_watched = $this->User->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_viewer_histories as dvh',
                        'conditions' => 'dvh.user_id = User.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'conditions' => 'afd.document_id = dvh.document_id',
                        'type' => 'inner'
                    ),
                ),
                'conditions' => array(
                    'dvh.account_id' => $all_account_array,
                    $total_video_watched_where
                ),
                'group' => 'dvh.document_id , dvh.user_id',
                'fields' => 'max(dvh.minutes_watched) as minutes_watched'
            ));

            $total_min_watched = 0;
            if ($total_video_watched) {
                foreach ($total_video_watched as $row) {
                    $total_min_watched = $total_min_watched + $row[0]['minutes_watched'];
                }
            }
            if ($total_min_watched > 0) {
                $total_min_watched = round(($total_min_watched / 60) / 60, 2);
            } else {
                $total_min_watched = 0;
            }

            $account_name_header = $parent_account[0]['Account']['company_name'];
            $date_range_header = date('F', strtotime($date_from)) . " " . date('d', strtotime($date_from)) . "-" . date('d', strtotime($date_to)) . " , " . date('Y', strtotime($date_from));
            //$parent_account_active_users = $this->getParentTotalUsers($account_id);
            $parent_account_active_users = $this->getParentTotalUsersWActivity($all_account_array, $date_from, $date_to);
            $parent_account_name = $parent_account[0]['Account']['company_name'];
            if (!empty($user_id)) {
                $parent_account_active_huddles = $this->getParentTotalHuddles($account_id, $date_from, $date_to, $user_id);
                $parent_account_active_videos = $this->getParentTotalVideos($account_id, $date_from, $date_to, $user_id);
                $parent_account_active_viewed = $this->getParentTotalVideosViewed($account_id, $date_from, $date_to, $user_id);
                $parent_account_active_comments = $this->getParentTotalVideosCommentsCount($account_id, $date_from, $date_to, $user_id);
            } else {

                $parent_account_active_huddles = $this->getParentTotalHuddles($account_id, $date_from, $date_to);

                $parent_account_active_videos = $this->getParentTotalVideos($account_id, $date_from, $date_to);
                $parent_account_active_viewed = $this->getParentTotalVideosViewed($account_id, $date_from, $date_to);
                $parent_account_active_comments = $this->getParentTotalVideosCommentsCount($account_id, $date_from, $date_to);
            }


            if (!$bool) {

                $parent_account_name = $parent_account[0]['Account']['company_name'];
                if (!empty($user_id)) {
                    $parent_account_active_users = $this->getAccountTotalUsersWActivity($account_id, $date_from, $date_to, $user_id);
                    $total_vide_duration_where = '';
                    if ($date_from != '' && $date_to != '') {
                        $total_vide_duration_where = "Document.created_date >= '$date_from' and Document.created_date <= '$date_to'";
                    }
                    $total_vide_duration = $this->Document->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'document_files as df',
                                'conditions' => 'Document.id = df.document_id',
                                'type' => 'inner'
                            ),
                            array(
                                'table' => '`user_activity_logs` as ua',
                                'conditions' => 'ua.ref_id = df.document_id',
                                'type' => 'inner'
                            ),
                        ),
                        'conditions' => array(
                            'Document.account_id' => $account_id,
                            'Document.created_by' => $user_id,
                            'ua.account_folder_id IN (' . $all_huddle_ids . ')',
                            'ua.type IN (2,4) ',
                            $total_vide_duration_where
                        ),
                        'fields' => 'SUM(df.duration) as total_duration'
                    ));
                    if (isset($total_vide_duration[0][0]['total_duration']) && $total_vide_duration[0][0]['total_duration'] > 0) {
                        $total_duration_hours = round(($total_vide_duration[0][0]['total_duration'] / 60) / 60, 2);
                    } else {
                        $total_duration_hours = 0;
                    }

                    /* ---------------Total Video Duration------------------ */
                    $total_video_watched_where = '';
                    if ($date_from != '' && $date_to != '') {
                        $total_video_watched_where = "dvh.created_date >= '$date_from' and dvh.created_date <= '$date_to'";
                    }
                    $total_video_watched = $this->User->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'document_viewer_histories as dvh',
                                'conditions' => 'dvh.user_id = User.id',
                                'type' => 'left'
                            ),
                            array(
                                'table' => 'account_folder_documents as afd',
                                'conditions' => 'afd.document_id = dvh.document_id',
                                'type' => 'inner'
                            ),
                            array(
                                'table' => '`user_activity_logs` as ua',
                                'conditions' => 'ua.ref_id = dvh.document_id',
                                'type' => 'inner'
                            ),
                        ),
                        'conditions' => array(
                            'dvh.account_id' => $account_id,
                            'dvh.user_id' => $user_id,
                            'ua.account_folder_id IN (' . $all_huddle_ids . ')',
                            $total_video_watched_where
                        ),
                        'group' => 'dvh.document_id',
                        'fields' => 'max(dvh.minutes_watched) as minutes_watched'
                    ));

                    $total_min_watched = 0;
                    if ($total_video_watched) {
                        foreach ($total_video_watched as $row) {
                            $total_min_watched = $total_min_watched + $row[0]['minutes_watched'];
                        }
                    }
                    if ($total_min_watched > 0) {
                        $total_min_watched = round(($total_min_watched / 60) / 60, 2);
                    } else {
                        $total_min_watched = 0;
                    }
                    $parent_account_active_users = $this->getAccountTotalUsersWActivity($account_id, $date_from, $date_to, $user_id);
                    $parent_account_active_huddles = $this->getAccountTotalHuddles($account_id, $date_from, $date_to, $user_id, $huddle_ids);
                    $parent_account_active_videos = $this->getAccountTotalVideos($account_id, $date_from, $date_to, $user_id, $all_huddle_ids);
                    $parent_account_active_viewed = $this->getAccountTotalVideosViewed($account_id, $date_from, $date_to, $user_id, $all_huddle_ids);
                    $parent_account_active_comments = $this->getAccountTotalVideosCommentsCount($account_id, $date_from, $date_to, $user_id, $all_huddle_ids);
                } else {

                    $parent_account_active_huddles = $this->getAccountTotalHuddles($account_id, $date_from, $date_to);
                    $parent_account_active_videos = $this->getAccountTotalVideos($account_id, $date_from, $date_to);
                    $parent_account_active_viewed = $this->getAccountTotalVideosViewed($account_id, $date_from, $date_to);
                    $parent_account_active_comments = $this->getAccountTotalVideosCommentsCount($account_id, $date_from, $date_to);
                }
            }


            $mydate = getdate(strtotime($date_to));
            $mydate2 = getdate(strtotime($date_from));
            $result_date = $mydate2['month'] . ' ' . $mydate2['mday'] . ' - ' . $mydate['month'] . ' ' . $mydate['mday'];


            $content_html = str_replace("{account_name_header}", $account_name_header, $content_html);
            $content_html = str_replace("{date_range_header}", $result_date, $content_html);
            $content_html = str_replace("{parent_account_active_users}", $parent_account_active_users, $content_html);
            $content_html = str_replace("{parent_account_name}", $parent_account_name, $content_html);
            $content_html = str_replace("{parent_account_active_huddles}", $parent_account_active_huddles, $content_html);
            $content_html = str_replace("{parent_account_active_videos}", $parent_account_active_videos, $content_html);
            $content_html = str_replace("{parent_account_active_viewed}", $parent_account_active_viewed, $content_html);
            $content_html = str_replace("{parent_account_active_comments}", $parent_account_active_comments, $content_html);
            $content_html = str_replace("{total_hours_watched}", $total_min_watched, $content_html);
        }


        $account_id_in = $account_id;
        $account_overview_all_child_rows_html = "";

        foreach ($child_accounts as $child_account) {


            $total_video_watched_where = '';
            if ($date_from != '' && $date_to != '') {
                $total_video_watched_where = "dvh.created_date >= '$date_from' and dvh.created_date <= '$date_to'";
            }
            $total_video_watched = $this->User->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_viewer_histories as dvh',
                        'conditions' => 'dvh.user_id = User.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'conditions' => 'afd.document_id = dvh.document_id',
                        'type' => 'inner'
                    ),
                ),
                'conditions' => array(
                    'dvh.account_id' => $child_account['Account']['id'],
                    $total_video_watched_where
                ),
                'group' => 'dvh.document_id , dvh.user_id',
                'fields' => 'max(dvh.minutes_watched) as minutes_watched'
            ));

            $total_min_watched = 0;
            if ($total_video_watched) {
                foreach ($total_video_watched as $row) {
                    $total_min_watched = $total_min_watched + $row[0]['minutes_watched'];
                }
            }
            if ($total_min_watched > 0) {
                $total_min_watched = round(($total_min_watched / 60) / 60, 2);
            } else {
                $total_min_watched = 0;
            }

            $account_id_in .= ", " . $child_account['Account']['id'];
            // $account_active_users = $this->getAccountTotalUsers($child_account['Account']['id']);
            $account_active_users = $this->getAccountTotalUsersWActivity($child_account['Account']['id'], $date_from, $date_to);
            $account_active_huddles = $this->getAccountTotalHuddles($child_account['Account']['id'], $date_from, $date_to);
            $account_active_videos = $this->getAccountTotalVideos($child_account['Account']['id'], $date_from, $date_to);
            $account_active_viewed = $this->getAccountTotalVideosViewed($child_account['Account']['id'], $date_from, $date_to);
            $account_active_comments = $this->getAccountTotalVideosCommentsCount($child_account['Account']['id'], $date_from, $date_to);

            $account_overview_all_child_rows_html .= '   <tr>
                    <td style="padding:5px 0px">
                            <strong> ' . $child_account['Account']['company_name'] . '</strong>
                    </td>
                    <td style="border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#cee3d3;text-align:center">' . $account_active_users . '</td>
                    <td style="border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#cbe7ea;text-align:center">' . $account_active_huddles . '</td>
                    <td style="border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#e9e9e9;text-align:center">' . $account_active_videos . '</td>
                    <td style="border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#e9e9e9;text-align:center">' . $account_active_viewed . '</td>
                    <td style="border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#e9e9e9;text-align:center">' . $total_min_watched . '</td>
                    <td style="border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#e9e9e9;text-align:center">' . $account_active_comments . '</td>
            </tr>';
        }

        $content_html = str_replace("{account_overview_all_child_rows}", $account_overview_all_child_rows_html, $content_html);

        $parent_account_active_users = $this->getParentTotalUsersWActivity($all_account_array, $date_from, $date_to);
        $parent_account_active_users_total = $this->getParentTotalActiveInActiveUsers($all_account_array);

        if (!$bool) {

            $parent_account_active_users = $this->getAccountTotalUsersWActivity($account_id, $date_from, $date_to);
            $parent_account_active_users_total = $this->getTotalActiveInActiveUsers($account_id);

            if (!empty($user_id)) {

                $parent_account_active_users = $this->getAccountTotalUsersWActivity($account_id, $date_from, $date_to, $user_id);
                $parent_account_active_users_total = $this->getTotalActiveInActiveUsers($account_id, $user_id);
            }
        }


        $getParentTotalUsers_getParentTotalActiveInActiveUsers_percentage = round(($parent_account_active_users / $parent_account_active_users_total) * 100);

        //{getParentTotalUsers_getParentTotalActiveInActiveUsers}
        $content_html = str_replace("{getParentTotalUsers_getParentTotalActiveInActiveUsers}", $parent_account_active_users . "/" . $parent_account_active_users_total, $content_html);

        $content_html = str_replace("{getParentTotalUsers_getParentTotalActiveInActiveUsers_percentage}", $parent_account_active_users, $content_html);

        $content_html = str_replace("{users_engagement}", $getParentTotalUsers_getParentTotalActiveInActiveUsers_percentage, $content_html);

        $content_html = str_replace("{all_total_users}", $parent_account_active_users_total, $content_html);


        $results = $this->UsersAccount->query("select company_name,account_id,Videos_Uploaded_Workspace, Videos_Uploaded_Huddle, Huddles_Created, Resources_Uploaded, Videos_Shared_Huddle, Videos_Viewed, Total_Video_Comments, minutes_watched_huddle , total_duration_uploaded, Total_Logins, (
    Videos_Uploaded_Workspace + Videos_Uploaded_Huddle + Huddles_Created + Resources_Uploaded + Videos_Shared_Huddle + Videos_Viewed + Total_Video_Comments + Resources_Viewed + minutes_watched_huddle ) as total_activity
   from (
select
    pivot.`company_name`,
    pivot.account_id,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Workspace' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Workspace,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Huddles_Created' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Huddles_Created,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Uploaded' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Uploaded,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Shared_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Shared_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Video_Comments' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Video_Comments,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Logins' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Logins,
    SUM(
        CASE WHEN pivot.event_type = 'minutes_watched_huddle' THEN
            CASE WHEN pivot.Count_Total > 0 THEN
                pivot.Count_Total/60
            ELSE
                0
            END
        ELSE
            0
        END

    ) as minutes_watched_huddle,
    SUM(
    CASE WHEN pivot.event_type = 'total_duration_uploaded' THEN pivot.Count_Total
    ELSE 0
    END
    ) as total_duration_uploaded

from

(
select
            Concat(u.first_name,' ', u.last_name) as UserFullName,
            u.id,
            u.email,
            a.`id` as account_id,
            a.`company_name`,
            ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
            'Videos_Uploaded_Workspace' as event_type
        from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join
        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle , ua.account_id
            FROM `user_activity_logs` ua LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND  af.active IN ( IF( (af.`archive` = 1)  AND (af.`active` = 0),  0,  1 )) AND af.`folder_type` IN(3)
            AND ua.account_id IN ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' and ua.site_id=" . $this->site_id . " GROUP BY ua.user_id,ua.account_id

        ) d on d.user_id = u.id   and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in)

UNION ALL

        select
                Concat(u.first_name,' ', u.last_name) as UserFullName,
                u.id,
                u.email,
                a.`id` as account_id,
                a.`company_name`,
                ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
                'Videos_Uploaded_Huddle' as event_type
            from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join
        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle, ua.account_id FROM `user_activity_logs` ua LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND  af.`folder_type` IN(1) AND  af.active IN ( IF( (af.`archive` = 1)  AND (af.`active` = 0),  0,  1 )) AND ua.account_id IN ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' GROUP BY ua.user_id ,ua.account_id
        ) d on d.user_id = u.id  and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . "

UNION ALL


        select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.shared_upload_counts,0) as Count_Total,
        'Videos_Shared_Huddle' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT d.created_by AS user_id, COUNT(*) AS shared_upload_counts, d.account_id FROM documents d LEFT JOIN account_folder_documents afd ON d.`id`= afd.`document_id` LEFT JOIN account_folders af ON afd.`account_folder_id`= af.`account_folder_id` LEFT JOIN user_activity_logs ua ON (d.id = ua.ref_id) WHERE d.account_id IN ($account_id_in) AND d.created_date >='$date_from' AND d.created_date <='$date_to' AND af.`folder_type` = 1 and af.site_id=" . $this->site_id . " AND d.doc_type = 1 GROUP BY d.created_by ) d on d.user_id = u.id   and d.account_id = ua.account_id where ua.account_id in ($account_id_in)


UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.videos_viewed_count,0) as Count_Total,
        'Videos_Viewed' as event_type
    from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join (
    SELECT ua.user_id, COUNT(*) AS videos_viewed_count, ua.account_id FROM `user_activity_logs` as ua
    JOIN account_folder_documents afd
      ON afd.document_id = ua.ref_id
     JOIN account_folders af
      ON af.account_folder_id = afd.account_folder_id
    WHERE ua.type=11 AND af.folder_type IN (1, 2, 3) AND ua.account_id IN ($account_id_in) AND ua.date_added >='$date_from' AND ua.date_added <='$date_to' and ua.site_id=" . $this->site_id . " GROUP BY ua.user_id,ua.account_id )
    d on d.user_id = u.id and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . "


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.huddle_created_count,0) as Count_Total,
    'Huddles_Created' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_id,COUNT(*) AS `huddle_created_count`, user_activity_logs.account_id FROM `user_activity_logs` join `account_folders` af ON af.account_folder_id = user_activity_logs.account_folder_id WHERE af.active = 1 and af.site_id=" . $this->site_id . " and `type` = 1 AND user_activity_logs.account_id IN ($account_id_in) and date_added >='$date_from' AND date_added <= '$date_to' GROUP BY user_id,account_id ) d on d.user_id = u.id and d.account_id = ua.account_id where ua.account_id in ($account_id_in)


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.comments_initiated_count,0) as Count_Total,
    'Total_Video_Comments' as event_type
    from users u join `users_accounts` ua on u.id = ua.user_id
    join accounts a on a.id = ua.account_id

    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count, user_activity_logs.account_id FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
    WHERE user_activity_logs.TYPE IN (5,8) AND user_activity_logs.account_id IN ($account_id_in) and user_activity_logs.site_id=" . $this->site_id . " and date_added >='$date_from' AND date_added <= '$date_to' GROUP BY user_activity_logs.user_id,user_activity_logs.account_id )
    d on d.user_id = u.id and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . "


UNION ALL


select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(d.`id`),0) as Count_Total,
    'Resources_Uploaded' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join account_folders af on af.created_by = u.id left join `account_folder_documents` afd on afd.`account_folder_id` = af.`account_folder_id` left join documents d on d.id = afd.document_id and d.account_id in ($account_id_in) and d.doc_type not in (3,1) and d.created_date >= '$date_from' and d.created_date <= '$date_to' and d.site_id=" . $this->site_id . " where ua.account_id in ($account_id_in) and  ua.site_id=" . $this->site_id . " Group by u.email, a.`company_name`

UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(ual.`id`),0) as Count_Total,
    'Resources_Viewed' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id in ($account_id_in) and ual.type=13 and ual.date_added >= '$date_from' and ual.date_added <= '$date_to' and ual.site_id=" . $this->site_id . "  where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . "  Group by u.email, a.`company_name`

UNION ALL


select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.minutes_watched,0) as Count_Total,
        'minutes_watched_huddle' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join (

    SELECT dvh.user_id, MAX(dvh.minutes_watched) AS minutes_watched, dvh.account_id
    FROM document_viewer_histories dvh
    LEFT JOIN account_folder_documents afd ON dvh.`document_id`= afd.`document_id`
    WHERE dvh.account_id IN ($account_id_in) AND dvh.created_date >='$date_from' AND dvh.created_date <='$date_to' and dvh.site_id =" . $this->site_id . "

    GROUP BY dvh.user_id , dvh.document_id


) d on d.user_id = u.id   and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.site_id = " . $this->site_id . "

UNION ALL


select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.total_duration,0) as Count_Total,
        'total_duration_uploaded' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join (

    SELECT d.created_by as user_id, SUM(df.duration) AS total_duration, d.account_id
    FROM document_files df
    JOIN documents d on d.id = df.document_id
    LEFT JOIN user_activity_logs ua ON df.`document_id`= ua.`ref_id`
    WHERE d.account_id IN ($account_id_in) AND d.created_date >='$date_from' AND d.created_date <='$date_to' AND ua.type IN (2,4) and ua.site_id =" . $this->site_id . "
    GROUP BY d.created_by


) d on d.user_id = u.id   and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.site_id = " . $this->site_id . "

UNION ALL


select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(date_added),0) as Count_Total,
    'Total_Logins' as event_type

from user_activity_logs ul join users u on u.id = ul.user_id join users_accounts ua on ua.user_id = u.id join accounts a on a.id = ua.account_id where ul.type=9 and ul.account_id in ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' and a.id in ($account_id_in) and a.site_id =" . $this->site_id . " Group by u.email, a.`company_name`

) as pivot group by pivot.`company_name`
) as a order by (
    Videos_Uploaded_Workspace + Videos_Uploaded_Huddle + Huddles_Created + Resources_Uploaded + Videos_Shared_Huddle + Videos_Viewed + Total_Video_Comments + Resources_Viewed + minutes_watched_huddle) DESC limit 5");


        //percent increase decrease this year

        $result_percent_increase_decrease_this_year = $this->UsersAccount->query("select sum( Videos_Uploaded_Workspace) + sum(Videos_Uploaded_Huddle) as increase_in_video_upload , sum(Huddles_Created) increase_in_huddles_created,  sum(Videos_Viewed) increase_in_videos_viewed , sum(Total_Video_Comments) as increase_in_video_comments
   from (
select
    pivot.`company_name`,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Workspace' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Workspace,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Huddles_Created' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Huddles_Created,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Uploaded' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Uploaded,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Shared_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Shared_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Video_Comments' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Video_Comments,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Logins' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Logins

from

(
select
            Concat(u.first_name,' ', u.last_name) as UserFullName,
            u.id,
            u.email,
            a.`id` as account_id,
            a.`company_name`,
            ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
            'Videos_Uploaded_Workspace' as event_type
        from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join

        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle , ua.account_id
            FROM `user_activity_logs` ua LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
            WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(3)
            AND ua.account_id IN ($account_id_in) and ua.site_id =" . $this->site_id . "  and date_added >= '$date_from' and date_added <= '$date_to' GROUP BY ua.user_id

        ) d on d.user_id = u.id   and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in) and ua.site_id = " . $this->site_id . "

UNION ALL

        select
                Concat(u.first_name,' ', u.last_name) as UserFullName,
                u.id,
                u.email,
                a.`id` as account_id,
                a.`company_name`,
                ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
                'Videos_Uploaded_Huddle' as event_type
            from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join
        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle, ua.account_id FROM `user_activity_logs` ua
            LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1) AND ua.account_id IN ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' and ua.site_id = " . $this->site_id . " GROUP BY ua.user_id
        ) d on d.user_id = u.id  and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in) and ua.site_id = " . $this->site_id . "

UNION ALL


        select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.shared_upload_counts,0) as Count_Total,
        'Videos_Shared_Huddle' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT d.created_by AS user_id, COUNT(*) AS shared_upload_counts, d.account_id FROM documents d LEFT JOIN account_folder_documents afd ON d.`id`= afd.`document_id` LEFT JOIN account_folders af ON afd.`account_folder_id`= af.`account_folder_id` LEFT JOIN user_activity_logs ua ON (d.id = ua.ref_id) WHERE d.account_id IN ($account_id_in) AND d.created_date >='$date_from' AND d.created_date <='$date_to' AND d.site_id=" . $this->site_id . " AND af.`folder_type` = 1 AND d.doc_type = 1 GROUP BY d.created_by ) d on d.user_id = u.id   and d.account_id = ua.account_id where ua.account_id in ($account_id_in)and ua.site_id = " . $this->site_id . "


UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.videos_viewed_count,0) as Count_Total,
        'Videos_Viewed' as event_type
from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_id, COUNT(*) AS videos_viewed_count, account_id FROM `user_activity_logs` WHERE TYPE=11 AND account_id IN ($account_id_in) AND date_added >='$date_from' AND date_added <='$date_to' AND user_activity_logs.site_id = " . $this->site_id . " GROUP BY user_id ) d on d.user_id = u.id and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.site_id =" . $this->site_id . "


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.huddle_created_count,0) as Count_Total,
    'Huddles_Created' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_id,COUNT(*) AS `huddle_created_count`, user_activity_logs.account_id FROM `user_activity_logs` join `account_folders` af ON af.account_folder_id = user_activity_logs.account_folder_id WHERE af.active = 1 and `type` = 1 AND user_activity_logs.account_id IN ($account_id_in) and af.site_id=" . $this->site_id . " and date_added >='$date_from' AND date_added <= '$date_to' GROUP BY user_id ) d on d.user_id = u.id and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.site_id = " . $this->site_id . "


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.comments_initiated_count,0) as Count_Total,
    'Total_Video_Comments' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count, account_id FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE user_activity_logs.TYPE IN (5,8) AND user_activity_logs.account_id IN ($account_id_in) and user_activity_logs.site_id =" . $this->site_id . " and date_added >='$date_from' AND date_added <= '$date_to' GROUP BY user_activity_logs.user_id ) d on d.user_id = u.id and d.account_id = ua.account_id where ua.account_id in ($account_id_in)


UNION ALL


select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(d.`id`),0) as Count_Total,
    'Resources_Uploaded' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join account_folders af on af.created_by = u.id left join `account_folder_documents` afd on afd.`account_folder_id` = af.`account_folder_id` left join documents d on d.id = afd.document_id and d.account_id in ($account_id_in) and d.site_id =" . $this->site_id . " and d.doc_type not in (3,1) and d.created_date >= '$date_from' and d.created_date <= '$date_to' where ua.account_id in ($account_id_in) and ua.site_id =" . $this->site_id . " Group by u.email, a.`company_name`

UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(ual.`id`),0) as Count_Total,
    'Resources_Viewed' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id in ($account_id_in) and ual.site_id =" . $this->site_id . " and ual.type=13 and ual.date_added >= '$date_from' and ual.date_added <= '$date_to' where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . " Group by u.email, a.`company_name`

UNION ALL


select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(date_added),0) as Count_Total,
    'Total_Logins' as event_type

from user_activity_logs ul join users u on u.id = ul.user_id join users_accounts ua on ua.user_id = u.id join accounts a on a.id = ua.account_id where ul.type=9 and ul.account_id in ($account_id_in) and ul.site_id=" . $this->site_id . " and date_added >= '$date_from' and date_added <= '$date_to' and a.id in ($account_id_in) Group by u.email, a.`company_name`

) as pivot group by pivot.`company_name`
) as a ");


//percent increase decrease last year

        $result_percent_increase_decrease_last_year = $this->UsersAccount->query("select sum( Videos_Uploaded_Workspace) + sum(Videos_Uploaded_Huddle) as increase_in_video_upload , sum(Huddles_Created) increase_in_huddles_created,  sum(Videos_Viewed) increase_in_videos_viewed , sum(Total_Video_Comments) as increase_in_video_comments
   from (
select
    pivot.`company_name`,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Workspace' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Workspace,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Huddles_Created' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Huddles_Created,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Uploaded' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Uploaded,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Shared_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Shared_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Video_Comments' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Video_Comments,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Logins' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Logins

from

(
select
            Concat(u.first_name,' ', u.last_name) as UserFullName,
            u.id,
            u.email,
            a.`id` as account_id,
            a.`company_name`,
            ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
            'Videos_Uploaded_Workspace' as event_type
        from users u
        join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id left join

        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle , ua.account_id
            FROM `user_activity_logs` ua
            LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
            WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(3)  AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
            AND ua.account_id IN ($account_id_in) and ua.site_id =" . $this->site_id . " and date_added >= '$prev_date_from' and date_added <= '$prev_date_to' GROUP BY ua.user_id

        ) d on d.user_id = u.id   and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in) and ua.site_id =" . $this->site_id . "

UNION ALL

        select
                Concat(u.first_name,' ', u.last_name) as UserFullName,
                u.id,
                u.email,
                a.`id` as account_id,
                a.`company_name`,
                ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
                'Videos_Uploaded_Huddle' as event_type
            from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join
        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle, ua.account_id FROM `user_activity_logs` ua
            LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
            WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1)  AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))   AND ua.account_id IN ($account_id_in) and ua.site_id = " . $this->site_id . " and date_added >= '$prev_date_from' and date_added <= '$prev_date_to' GROUP BY ua.user_id
        ) d on d.user_id = u.id  and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . "

UNION ALL


        select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.shared_upload_counts,0) as Count_Total,
        'Videos_Shared_Huddle' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT d.created_by AS user_id, COUNT(*) AS shared_upload_counts, d.account_id FROM documents d LEFT JOIN account_folder_documents afd ON d.`id`= afd.`document_id` LEFT JOIN account_folders af ON afd.`account_folder_id`= af.`account_folder_id` LEFT JOIN user_activity_logs ua ON (d.id = ua.ref_id) WHERE d.account_id IN ($account_id_in) AND d.created_date >='$prev_date_from' AND d.created_date <='$prev_date_to' AND af.`folder_type` = 1 AND af.site_id =" . $this->site_id . " AND d.doc_type = 1 GROUP BY d.created_by ) d on d.user_id = u.id   and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . "


UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.videos_viewed_count,0) as Count_Total,
        'Videos_Viewed' as event_type
from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id
left join (
    SELECT ua.user_id, COUNT(*) AS videos_viewed_count, ua.account_id FROM `user_activity_logs` as ua
     JOIN account_folder_documents afd
      ON afd.document_id = ua.ref_id
     JOIN account_folders af
      ON af.account_folder_id = afd.account_folder_id
WHERE ua.type=11  AND af.folder_type IN (1, 2, 3)  AND ua.account_id IN ($account_id_in) AND ua.date_added >='$prev_date_from' AND ua.date_added <='$prev_date_to' AND ua.site_id =" . $this->site_id . " GROUP BY ua.user_id, ua.`account_id`) d on d.user_id = u.id and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . "


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.huddle_created_count,0) as Count_Total,
    'Huddles_Created' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_id,COUNT(*) AS `huddle_created_count`, user_activity_logs.account_id FROM `user_activity_logs` join `account_folders` af ON af.account_folder_id = user_activity_logs.account_folder_id WHERE af.active =1 and `type` = 1 AND user_activity_logs.account_id IN ($account_id_in) and date_added >='$prev_date_from' AND date_added <= '$prev_date_to' GROUP BY user_id ) d on d.user_id = u.id and d.account_id = ua.account_id where ua.account_id in ($account_id_in)


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.comments_initiated_count,0) as Count_Total,
    'Total_Video_Comments' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count, account_id FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE user_activity_logs.TYPE IN (5,8) AND user_activity_logs.account_id IN ($account_id_in) and date_added >='$prev_date_from'  AND date_added <= '$prev_date_to' AND user_activity_logs.site_id=" . $this->site_id . " GROUP BY user_activity_logs.user_id ) d on d.user_id = u.id and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.site_id =" . $this->site_id . "


UNION ALL


select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(d.`id`),0) as Count_Total,
    'Resources_Uploaded' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join account_folders af on af.created_by = u.id left join `account_folder_documents` afd on afd.`account_folder_id` = af.`account_folder_id` left join documents d on d.id = afd.document_id and d.account_id in ($account_id_in) and d.doc_type not in (3,1) and d.created_date >= '$prev_date_from' and d.created_date <= '$prev_date_to' and d.site_id=" . $this->site_id . " where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . " Group by u.email, a.`company_name`

UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(ual.`id`),0) as Count_Total,
    'Resources_Viewed' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id in ($account_id_in) and ual.type=13 and ual.date_added >= '$prev_date_from' and ual.date_added <= '$prev_date_to' and ual.site_id=" . $this->site_id . " where ua.account_id in ($account_id_in) and ua.site_id=" . $this->site_id . " Group by u.email, a.`company_name`

UNION ALL


select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(date_added),0) as Count_Total,
    'Total_Logins' as event_type

from user_activity_logs ul join users u on u.id = ul.user_id join users_accounts ua on ua.user_id = u.id join accounts a on a.id = ua.account_id where ul.type=9 and ul.account_id in ($account_id_in) and date_added >= '$prev_date_from' and date_added <= '$prev_date_to'  and a.id in ($account_id_in) and a.site_id=" . $this->site_id . " Group by u.email, a.`company_name`

) as pivot group by pivot.`company_name`
) as a ");


        //var_dump($child_accounts);
        //{top_engaged_schools}

        $content_html = str_replace("{top_engaged_schools}", (count($results) > 5 ? 5 : count($results)), $content_html);

        $top_engaged_schools_html = "";

        $result_counter = 1;

        $percent_Increase_in_Videos_Uploaded_this_year = (int) $result_percent_increase_decrease_this_year[0][0]['increase_in_video_upload'];
        $percent_Increase_in_Videos_Uploaded_last_year = (int) $result_percent_increase_decrease_last_year[0][0]['increase_in_video_upload'];
        if ($percent_Increase_in_Videos_Uploaded_last_year > 0) {
            $result_Increase_in_Videos_Uploaded = round(($percent_Increase_in_Videos_Uploaded_this_year - $percent_Increase_in_Videos_Uploaded_last_year) / $percent_Increase_in_Videos_Uploaded_last_year, 2) * 100;
        } else {
            $result_Increase_in_Videos_Uploaded = 0;
        }


        if ($result_Increase_in_Videos_Uploaded > 0) {
            $result_Increase_in_Videos_Uploaded = '+' . $result_Increase_in_Videos_Uploaded;
        }
        $content_html = str_replace("{percent_Increase_in_Videos_Uploaded}", $result_Increase_in_Videos_Uploaded, $content_html);


        //percent_Increase_in_Videos_Viewed

        $percent_Increase_in_Videos_Viewed_this_year = (int) $result_percent_increase_decrease_this_year[0][0]['increase_in_videos_viewed'];
        $percent_Increase_in_Videos_Viewed_last_year = (int) $result_percent_increase_decrease_last_year[0][0]['increase_in_videos_viewed'];
        if ($percent_Increase_in_Videos_Viewed_this_year > 0 || $percent_Increase_in_Videos_Viewed_last_year > 0) {
            $result_percent_Increase_in_Videos_Viewed = round(($percent_Increase_in_Videos_Viewed_this_year - $percent_Increase_in_Videos_Viewed_last_year) / $percent_Increase_in_Videos_Viewed_last_year, 2) * 100;
        } else {
            $result_percent_Increase_in_Videos_Viewed = 0;
        }

        if ($result_percent_Increase_in_Videos_Viewed > 0) {
            $result_percent_Increase_in_Videos_Viewed = '+' . $result_percent_Increase_in_Videos_Viewed;
        }
        $content_html = str_replace("{percent_Increase_in_Videos_Viewed}", $result_percent_Increase_in_Videos_Viewed, $content_html);


        //percent_Increase_in_Huddles_Created

        $percent_Increase_in_Huddles_Created_this_year = (int) $result_percent_increase_decrease_this_year[0][0]['increase_in_huddles_created'];

        $percent_Increase_in_Huddles_Created_last_year = (int) $result_percent_increase_decrease_last_year[0][0]['increase_in_huddles_created'];
        if ($percent_Increase_in_Huddles_Created_last_year > 0) {
            $result_percent_Increase_in_Huddles_Created = round(($percent_Increase_in_Huddles_Created_this_year - $percent_Increase_in_Huddles_Created_last_year) / $percent_Increase_in_Huddles_Created_last_year, 2) * 100;
        } else {
            $result_percent_Increase_in_Huddles_Created = 0;
        }

        if ($result_percent_Increase_in_Huddles_Created > 0) {
            $result_percent_Increase_in_Huddles_Created = '+' . $result_percent_Increase_in_Huddles_Created;
        }
        $content_html = str_replace("{percent_Increase_in_Huddles_Created}", $result_percent_Increase_in_Huddles_Created, $content_html);


        //percent_Increase_in_Comments_Added

        $percent_Increase_in_Comments_Added_this_year = (int) $result_percent_increase_decrease_this_year[0][0]['increase_in_video_comments'];
        $percent_Increase_in_Comments_Added_last_year = (int) $result_percent_increase_decrease_last_year[0][0]['increase_in_video_comments'];

        if (
                $percent_Increase_in_Comments_Added_this_year > 0 || $percent_Increase_in_Comments_Added_last_year > 0) {
            $result_percent_Increase_in_Comments_Added = round(($percent_Increase_in_Comments_Added_this_year - $percent_Increase_in_Comments_Added_last_year) / $percent_Increase_in_Comments_Added_last_year, 2) * 100;
        } else {
            $result_percent_Increase_in_Comments_Added = 0;
        }


        if ($result_percent_Increase_in_Comments_Added > 0) {
            $result_percent_Increase_in_Comments_Added = '+' . $result_percent_Increase_in_Comments_Added;
        }
        $content_html = str_replace("{percent_Increase_in_Comments_Added}", $result_percent_Increase_in_Comments_Added, $content_html);

        $users_counter = 0;
        foreach ($results as $result) {
            if ($result[0]['total_activity'] <= 0)
                continue;

            $users_counter++;
        }

        $top_engaged_schools_html = "";

        if ($users_counter > 0) {

            if ($bool) {
                $top_engaged_schools_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ' . $users_counter . ' ENGAGED PROGRAMS

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															Your top ' . $users_counter . ' programs  with the most number of activities:

															</font>
													</td>
											</tr>
                                        </table>';
            } else {
                $top_engaged_schools_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ' . $users_counter . ' ENGAGED PROGRAMS

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															Your top ' . $users_counter . ' programs  with the most number of activities:

															</font>
													</td>
											</tr>
                                        </table>';
            }
        } else {
            if ($bool) {
                $top_engaged_schools_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ENGAGED PROGRAMS

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															NO DATA AVAILABLE

															</font>
													</td>
											</tr>
                                        </table>';
            } else {
                $top_engaged_schools_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ENGAGED PROGRAMS

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															NO DATA AVAILABLE

															</font>
													</td>
											</tr>
                                        </table>';
            }
        }

        $result_counter = 1;
        foreach ($results as $result) {

            if ($result_counter > 5)
                break;


            $total_vide_duration_where = '';
            if ($date_from != '' && $date_to != '') {
                $total_vide_duration_where = "Document.created_date >= '$date_from' and Document.created_date <= '$date_to'";
            }
            $total_vide_duration = $this->Document->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_files as df',
                        'conditions' => 'Document.id = df.document_id',
                        'type' => 'inner'
                    ),
                    array(
                        'table' => '`user_activity_logs` as ua',
                        'conditions' => 'ua.ref_id = df.document_id',
                        'type' => 'inner'
                    ),
                    array(
                        'table' => '`users` as u',
                        'conditions' => 'ua.user_id = u.id',
                        'type' => 'inner'
                    ),
                ),
                'conditions' => array(
                    'Document.account_id' => $result['a']['account_id'],
                    'u.is_active' => 1,
                    'u.type' => 'Active',
                    //    'Document.created_by' => $users['User']['id'],
                    'ua.type IN (2,4) ',
                    $total_vide_duration_where
                ),
                'fields' => 'SUM(df.duration) as total_duration'
            ));
            if (isset($total_vide_duration[0][0]['total_duration']) && $total_vide_duration[0][0]['total_duration'] > 0) {
                $total_duration_hours = round(($total_vide_duration[0][0]['total_duration'] / 60) / 60, 1);
            } else {
                $total_duration_hours = 0;
            }

            /* ---------------Total Video Duration------------------ */
            $total_video_watched_where = '';
            if ($date_from != '' && $date_to != '') {
                $total_video_watched_where = "dvh.created_date >= '$date_from' and dvh.created_date <= '$date_to'";
            }
            $total_video_watched = $this->User->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_viewer_histories as dvh',
                        'conditions' => 'dvh.user_id = User.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'conditions' => 'afd.document_id = dvh.document_id',
                        'type' => 'inner'
                    ),
                ),
                'conditions' => array(
                    'dvh.account_id' => $result['a']['account_id'],
                    $total_video_watched_where
                ),
                'group' => 'dvh.document_id , dvh.user_id',
                'fields' => 'max(dvh.minutes_watched) as minutes_watched'
            ));

            $total_min_watched = 0;
            if ($total_video_watched) {
                foreach ($total_video_watched as $row) {
                    $total_min_watched = $total_min_watched + $row[0]['minutes_watched'];
                }
            }
            if ($total_min_watched > 0) {
                $total_min_watched = round(($total_min_watched) / 60, 1);
            } else {
                $total_min_watched = 0;
            }

            if ($result['a']['minutes_watched_huddle'] > 0) {
                $total_min_watched = round(($result['a']['minutes_watched_huddle']), 1);
            } else {
                $total_min_watched = 0;
            }


            $top_engaged_schools_html .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse">
											<tr>
												<td height="91" valign="middle" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px 20px; border-bottom:1px solid #ccc; line-height:20px;">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:12px;color:#751c1c">
														<tr>
															<td style="padding:15px 0px">
																<font style="color:#0b475b; font-size:16px; font-weight: 700; text-transform:uppercase; line-height:20px;">
																	' . $result_counter . '. ' . $result['a']['company_name'] . '
																</font><br>
																<table cellpadding="0" cellspacing="0" border="0" width="544" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;" class="fullWidth">
																	<tr>
																		<td style="padding:10px 0px 0px 0px">
																			<table cellpadding="0" cellspacing="0" border="0" width="230" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="23" style="color:#111111;vertical-align:middle; text-align:center;padding-bottom: 5px;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-upload.png" height="17" width="19" alt="icon">
																					</td>
																					<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;padding-bottom: 10px;"><span style="color:#0b475b; font-size:14px; font-weight:bold;"> ' . $result['a']['Videos_Uploaded_Workspace'] . ' </span> <span style="color:#232323; font-size:11px;">Videos Uploaded to Workspace</span></td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="14" style="color:#111111;vertical-align:middle; text-align:center;padding-bottom: 5px;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-viewed.png" height="20" width="25" alt="icon">
																					</td>
																					<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;padding-bottom: 10px;">
																							<span style="color:#0b475b; font-size:14px; font-weight:bold;"> ' . $result['a']['Videos_Viewed'] . '  </span>
																							<span style="color:#232323; font-size:11px;">Videos Viewed</span>
																					</td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="14" style="color:#111111;vertical-align:middle; text-align:center;padding-bottom: 5px;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-minutes.png" width="23" alt="icon">
																					</td>
																					<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;padding-bottom: 10px;">
																							<span style="color:#0b475b; font-size:14px; font-weight:bold;"> ' . $total_min_watched . ' </span>
																							<span style="color:#232323; font-size:11px;">Minutes Viewed</span>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>

																	<tr>
																		<td style="padding:0px 0px">
																			<table cellpadding="0" cellspacing="0" border="0" width="230" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="23" style="color:#111111;vertical-align:middle; text-align:center;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-huddles.png" height="23" width="23" alt="icon">
																				</td>
																				<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;">
																						<span style="color:#0b475b; font-size:14px; font-weight:bold;">' . $result['a']['Videos_Uploaded_Huddle'] . '</span>
																						<span style="color:#232323; font-size:11px;">Videos Uploaded to Huddles</span>
																				</td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="13" style="color:#111111;vertical-align:middle; text-align:center;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-comments.png" height="23" width="24" alt="icon">
																				</td>
																				<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;">
																						<span style="color:#0b475b; font-size:14px; font-weight:bold;">' . $result['a']['Total_Video_Comments'] . '</span>
																						<span style="color:#232323; font-size:11px; ">Total Comments</span>
																				</td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="13" style="color:#111111;vertical-align:middle; text-align:center;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-hours.png" width="23" alt="icon">
																				</td>
																				<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;">
																						<span style="color:#0b475b; font-size:14px; font-weight:bold;">' . $total_duration_hours . '</span>
																						<span style="color:#232323; font-size:11px; ">Hours Uploaded</span>
																				</td>
																				</tr>
																			</table>
																		</td>
																	</tr>

																</table>

															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>';

            $result_counter += 1;
        }

        $content_html = str_replace("{top_engaged_schools_html}", $top_engaged_schools_html, $content_html);


        //engaged by user for Parent
        //account_id

        $results = $this->UsersAccount->query("select UserFullName,
    email,company_name,Videos_Uploaded_Workspace, Videos_Uploaded_Huddle, Huddles_Created, Resources_Uploaded, Videos_Shared_Huddle, Videos_Viewed, Resources_Viewed, Total_Logins, Total_Video_Comments, minutes_watched_huddle, total_duration_uploaded , (
    Videos_Uploaded_Workspace + Videos_Uploaded_Huddle + Huddles_Created + Resources_Uploaded + Videos_Shared_Huddle + Videos_Viewed + Total_Video_Comments + Resources_Viewed + minutes_watched_huddle) as total_activity
   from (
select
    pivot.UserFullName,
    pivot.email,
    pivot.`company_name`,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Workspace' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Workspace,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Huddles_Created' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Huddles_Created,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Uploaded' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Uploaded,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Shared_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Shared_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Video_Comments' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Video_Comments,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Logins' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Logins,
    SUM(

        CASE WHEN pivot.event_type = 'minutes_watched_huddle' THEN
            CASE WHEN pivot.Count_Total > 0 THEN
                pivot.Count_Total/60
            ELSE
                0
            END
        ELSE
            0
        END


    ) as minutes_watched_huddle,
    SUM(
    CASE WHEN pivot.event_type = 'total_duration_uploaded' THEN pivot.Count_Total
    ELSE 0
    END
    ) as total_duration_uploaded

from

(
select
            Concat(u.first_name,' ', u.last_name) as UserFullName,
            u.id,
            u.email,
            a.`id` as account_id,
            a.`company_name`,
            ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
            'Videos_Uploaded_Workspace' as event_type
        from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join

        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle  , ua.account_id
            FROM `user_activity_logs` ua LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(3)
            AND ua.account_id IN ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' GROUP BY ua.user_id

        ) d on d.user_id = u.id  and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . "

UNION ALL

        select
                Concat(u.first_name,' ', u.last_name) as UserFullName,
                u.id,
                u.email,
                a.`id` as account_id,
                a.`company_name`,
                ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
                'Videos_Uploaded_Huddle' as event_type
            from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join
        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle , ua.account_id FROM `user_activity_logs` ua LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1) AND ua.account_id IN ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' GROUP BY ua.user_id
        ) d on d.user_id = u.id  and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . "

UNION ALL


        select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.shared_upload_counts,0) as Count_Total,
        'Videos_Shared_Huddle' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT d.created_by AS user_id, COUNT(*) AS shared_upload_counts , d.account_id FROM documents d LEFT JOIN account_folder_documents afd ON d.`id`= afd.`document_id` LEFT JOIN account_folders af ON afd.`account_folder_id`= af.`account_folder_id` LEFT JOIN user_activity_logs ua ON (d.id = ua.ref_id) WHERE d.account_id IN ($account_id_in) AND d.created_date >='$date_from' AND d.created_date <='$date_to' AND af.`folder_type` = 1 AND d.doc_type = 1 GROUP BY d.created_by ) d on d.user_id = u.id  and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . "


UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.videos_viewed_count,0) as Count_Total,
        'Videos_Viewed' as event_type
from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_id, COUNT(*) AS videos_viewed_count , account_id FROM `user_activity_logs` WHERE TYPE=11 AND account_id IN ($account_id_in) AND date_added >='$date_from' AND date_added <='$date_to' GROUP BY user_id ) d on d.user_id = u.id  and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . "


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.huddle_created_count,0) as Count_Total,
    'Huddles_Created' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_id,COUNT(*) AS `huddle_created_count` , user_activity_logs.account_id FROM `user_activity_logs` join `account_folders` af ON af.account_folder_id = user_activity_logs.account_folder_id WHERE af.active = 1 and `type` = 1 AND user_activity_logs.account_id IN ($account_id_in) and date_added >='$date_from' AND date_added <= '$date_to' GROUP BY user_id ) d on d.user_id = u.id  and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . "


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.comments_initiated_count,0) as Count_Total,
    'Total_Video_Comments' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count, account_id FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE user_activity_logs.TYPE IN (5,8) AND user_activity_logs.account_id IN ($account_id_in) and date_added >='$date_from' AND date_added <= '$date_to' GROUP BY user_activity_logs.user_id ) d on d.user_id = u.id  and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . "


UNION ALL


select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(d.`id`),0) as Count_Total,
    'Resources_Uploaded' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join account_folders af on af.created_by = u.id left join `account_folder_documents` afd on afd.`account_folder_id` = af.`account_folder_id` left join documents d on d.id = afd.document_id and d.account_id in ($account_id_in) and d.doc_type not in (3,1) and d.created_date >= '$date_from' and d.created_date <= '$date_to' where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . " Group by u.email, a.`company_name`

UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(ual.`id`),0) as Count_Total,
    'Resources_Viewed' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id in ($account_id_in) and ual.type=13 and ual.date_added >= '$date_from' and ual.date_added <= '$date_to' where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . " Group by u.email, a.`company_name`

UNION ALL

    select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.minutes_watched,0) as Count_Total,
        'minutes_watched_huddle' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join (

    SELECT dvh.user_id, MAX(dvh.minutes_watched) AS minutes_watched, dvh.account_id
    FROM document_viewer_histories dvh
    LEFT JOIN account_folder_documents afd ON dvh.`document_id`= afd.`document_id`
    WHERE dvh.account_id IN ($account_id_in) AND dvh.created_date >='$date_from' AND dvh.created_date <='$date_to' and dvh.site_id= " . $this->site_id . "
    GROUP BY dvh.user_id , dvh.document_id


) d on d.user_id = u.id   and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . "

UNION ALL


select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.total_duration,0) as Count_Total,
        'total_duration_uploaded' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join (

    SELECT d.created_by as user_id, SUM(df.duration) AS total_duration, d.account_id
    FROM document_files df
    JOIN documents d on d.id = df.document_id
    LEFT JOIN user_activity_logs ua ON df.`document_id`= ua.`ref_id`
    WHERE d.account_id IN ($account_id_in) AND d.created_date >='$date_from' AND d.created_date <='$date_to' AND ua.type IN (2,4) and ua.site_id= " . $this->site_id . "
    GROUP BY d.created_by


) d on d.user_id = u.id   and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . "

UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(date_added),0) as Count_Total,
    'Total_Logins' as event_type

from user_activity_logs ul join users u on u.id = ul.user_id join users_accounts ua on ua.user_id = u.id join accounts a on a.id = ua.account_id where ul.type=9 and ul.account_id in ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' and a.id in ($account_id_in) and ua.role_id in (120) and ua.site_id= " . $this->site_id . " Group by u.email, a.`company_name`

) as pivot group by pivot.UserFullName,
    pivot.email,
    pivot.`company_name`
) as a order by (
    Videos_Uploaded_Workspace + Videos_Uploaded_Huddle + Huddles_Created + Resources_Uploaded + Videos_Shared_Huddle + Videos_Viewed + Total_Video_Comments + Resources_Viewed + minutes_watched_huddle) DESC LIMIT 5");

        //var_dump($child_accounts);
        //{top_engaged_schools}

        $users_counter = 0;
        foreach ($results as $result) {
            if ($result[0]['total_activity'] <= 0)
                continue;

            $users_counter++;
        }

        $top_engaged_users_html = "";

        if ($users_counter > 0) {

            if ($bool) {
                $top_engaged_users_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ' . $users_counter . ' ENGAGED USERS IN THE NETWORK

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															Your top ' . $users_counter . ' team members  with the most number of activities:

															</font>
													</td>
											</tr>
                                        </table>';
            } else {
                $top_engaged_users_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ' . $users_counter . ' ENGAGED USERS IN THE NETWORK

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															Your top ' . $users_counter . ' team members  with the most number of activities:

															</font>
													</td>
											</tr>
                                        </table>';
            }
        } else {
            if ($bool) {
                $top_engaged_users_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ENGAGED USERS IN THE NETWORK

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															NO DATA AVAILABLE

															</font>
													</td>
											</tr>
                                        </table>';
            } else {
                $top_engaged_users_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ENGAGED USERS

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															NO DATA AVAILABLE

															</font>
													</td>
											</tr>
                                        </table>';
            }
        }

        $result_counter = 1;

        //  print_r($result);die;

        foreach ($results as $result) {

            if ($result[0]['total_activity'] <= 0)
                continue;


            $users = $this->User->find("first", array("conditions" => array(
                    "User.email" => $result['a']['email'],
            )));


            $total_vide_duration_where = '';
            if ($date_from != '' && $date_to != '') {
                $total_vide_duration_where = "Document.created_date >= '$date_from' and Document.created_date <= '$date_to'";
            }
            $total_vide_duration = $this->Document->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_files as df',
                        'conditions' => 'Document.id = df.document_id',
                        'type' => 'inner'
                    ),
                    array(
                        'table' => '`user_activity_logs` as ua',
                        'conditions' => 'ua.ref_id = df.document_id',
                        'type' => 'inner'
                    ),
                ),
                'conditions' => array(
                    "Document.account_id IN ($account_id_in)",
                    'Document.created_by' => $users['User']['id'],
                    'ua.type IN (2,4) ',
                    $total_vide_duration_where
                ),
                'fields' => 'SUM(df.duration) as total_duration'
            ));
            if (isset($total_vide_duration[0][0]['total_duration']) && $total_vide_duration[0][0]['total_duration'] > 0) {
                $total_duration_hours = round(($total_vide_duration[0][0]['total_duration'] / 60) / 60, 1);
            } else {
                $total_duration_hours = 0;
            }

            /* ---------------Total Video Duration------------------ */
            $total_video_watched_where = '';
            if ($date_from != '' && $date_to != '') {
                $total_video_watched_where = "dvh.created_date >= '$date_from' and dvh.created_date <= '$date_to'";
            }
            $total_video_watched = $this->User->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_viewer_histories as dvh',
                        'conditions' => 'dvh.user_id = User.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'conditions' => 'afd.document_id = dvh.document_id',
                        'type' => 'inner'
                    ),
                ),
                'conditions' => array(
                    "dvh.account_id IN ($account_id_in) ",
                    'dvh.user_id' => $users['User']['id'],
                    $total_video_watched_where
                ),
                'group' => 'dvh.document_id',
                'fields' => 'max(dvh.minutes_watched) as minutes_watched'
            ));

            $total_min_watched = 0;
            if ($total_video_watched) {
                foreach ($total_video_watched as $row) {
                    $total_min_watched = $total_min_watched + $row[0]['minutes_watched'];
                }
            }
            if ($total_min_watched > 0) {
                $total_min_watched = round(($total_min_watched) / 60, 1);
            } else {
                $total_min_watched = 0;
            }

            if ($result['a']['minutes_watched_huddle'] > 0) {
                $total_min_watched = round(($result['a']['minutes_watched_huddle']), 1);
            } else {
                $total_min_watched = 0;
            }


            $top_engaged_users_html .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse">
											<tr>
												<td height="91" valign="middle" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px 20px; border-bottom:1px solid #ccc; line-height:20px;">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:12px;color:#751c1c">
														<tr>
															<td style="padding:15px 0px">
																<font style="color:#0b475b; font-size:16px; font-weight: 700; text-transform:uppercase; line-height:20px;">
																	' . $result_counter . '. ' . $result['a']['company_name'] . ' - ' . $result['a']['UserFullName'] . '
																</font><br>
																<table cellpadding="0" cellspacing="0" border="0" width="544" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;" class="fullWidth">
																	<tr>
																		<td style="padding:10px 0px 0px 0px">
																			<table cellpadding="0" cellspacing="0" border="0" width="230" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="23" style="color:#111111;vertical-align:middle; text-align:center;padding-bottom: 5px;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-upload.png" height="17" width="19" alt="icon">
																					</td>
																					<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;padding-bottom: 10px;"><span style="color:#0b475b; font-size:14px; font-weight:bold;"> ' . $result['a']['Videos_Uploaded_Workspace'] . ' </span> <span style="color:#232323; font-size:11px;">Videos Uploaded to Workspace</span></td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="14" style="color:#111111;vertical-align:middle; text-align:center;padding-bottom: 5px;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-viewed.png" height="20" width="25" alt="icon">
																					</td>
																					<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;padding-bottom: 10px;">
																							<span style="color:#0b475b; font-size:14px; font-weight:bold;"> ' . $result['a']['Videos_Viewed'] . '  </span>
																							<span style="color:#232323; font-size:11px;">Videos Viewed</span>
																					</td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="14" style="color:#111111;vertical-align:middle; text-align:center;padding-bottom: 5px;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-minutes.png" width="23" alt="icon">
																					</td>
																					<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;padding-bottom: 10px;">
																							<span style="color:#0b475b; font-size:14px; font-weight:bold;"> ' . $total_min_watched . ' </span>
																							<span style="color:#232323; font-size:11px;">Minutes Viewed</span>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>

																	<tr>
																		<td style="padding:0px 0px">
																			<table cellpadding="0" cellspacing="0" border="0" width="230" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="23" style="color:#111111;vertical-align:middle; text-align:center;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-huddles.png" height="23" width="23" alt="icon">
																				</td>
																				<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;">
																						<span style="color:#0b475b; font-size:14px; font-weight:bold;">' . $result['a']['Videos_Uploaded_Huddle'] . '</span>
																						<span style="color:#232323; font-size:11px;">Videos Uploaded to Huddles</span>
																				</td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="13" style="color:#111111;vertical-align:middle; text-align:center;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-comments.png" height="23" width="24" alt="icon">
																				</td>
																				<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;">
																						<span style="color:#0b475b; font-size:14px; font-weight:bold;">' . $result['a']['Total_Video_Comments'] . '</span>
																						<span style="color:#232323; font-size:11px; ">Total Comments</span>
																				</td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="13" style="color:#111111;vertical-align:middle; text-align:center;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-hours.png" width="23" alt="icon">
																				</td>
																				<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;">
																						<span style="color:#0b475b; font-size:14px; font-weight:bold;">' . $total_duration_hours . '</span>
																						<span style="color:#232323; font-size:11px; ">Hours Uploaded</span>
																				</td>
																				</tr>
																			</table>
																		</td>
																	</tr>

																</table>

															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>';


            $result_counter += 1;
        }


        //other child accounts for 1 student per


        $content_html = str_replace("{top_engaged_users_html}", $top_engaged_users_html, $content_html);

        $results = array();
        $results = $this->UsersAccount->query("select UserFullName,
    email,company_name,Videos_Uploaded_Workspace, Videos_Uploaded_Huddle, Huddles_Created, Resources_Uploaded, Videos_Shared_Huddle, Videos_Viewed, Resources_Viewed, Total_Logins, Total_Video_Comments, total_duration_uploaded , minutes_watched_huddle, (
    Videos_Uploaded_Workspace + Videos_Uploaded_Huddle + Huddles_Created + Resources_Uploaded + Videos_Shared_Huddle + Videos_Viewed + Total_Video_Comments + Resources_Viewed + minutes_watched_huddle) as total_activity
   from (
select
    pivot.UserFullName,
    pivot.email,
    pivot.`company_name`,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Workspace' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Workspace,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Uploaded_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Uploaded_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Huddles_Created' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Huddles_Created,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Uploaded' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Uploaded,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Shared_Huddle' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Shared_Huddle,
    SUM(
    CASE WHEN pivot.event_type = 'Videos_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Videos_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Resources_Viewed' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Resources_Viewed,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Video_Comments' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Video_Comments,
    SUM(
    CASE WHEN pivot.event_type = 'Total_Logins' THEN pivot.Count_Total
    ELSE 0
    END
    ) as Total_Logins,
    SUM(
    CASE WHEN pivot.event_type = 'minutes_watched_huddle' THEN
        CASE WHEN pivot.Count_Total > 0 THEN
            pivot.Count_Total/60
        ELSE
            0
        END
    ELSE
        0
    END
    ) as minutes_watched_huddle,
    SUM(
    CASE WHEN pivot.event_type = 'total_duration_uploaded' THEN pivot.Count_Total
    ELSE 0
    END
    ) as total_duration_uploaded

from

(
select
            Concat(u.first_name,' ', u.last_name) as UserFullName,
            u.id,
            u.email,
            a.`id` as account_id,
            a.`company_name`,
            ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
            'Videos_Uploaded_Workspace' as event_type
        from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join

        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle  , ua.account_id
            FROM `user_activity_logs` ua LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(3)
            AND ua.account_id IN ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' GROUP BY ua.user_id

        ) d on d.user_id = u.id  and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in) and ua.role_id in (115,110) and ua.site_id= " . $this->site_id . "

UNION ALL

        select
                Concat(u.first_name,' ', u.last_name) as UserFullName,
                u.id,
                u.email,
                a.`id` as account_id,
                a.`company_name`,
                ifnull(d.Videos_Uploaded_Huddle,0) as Count_Total,
                'Videos_Uploaded_Huddle' as event_type
            from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join
        (
            SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle , ua.account_id FROM `user_activity_logs` ua LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1) AND ua.account_id IN ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' GROUP BY ua.user_id
        ) d on d.user_id = u.id  and d.account_id = ua.account_id
            where ua.account_id in ($account_id_in) and ua.role_id in (115,110) and ua.site_id= " . $this->site_id . "

UNION ALL


        select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.shared_upload_counts,0) as Count_Total,
        'Videos_Shared_Huddle' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT d.created_by AS user_id, COUNT(*) AS shared_upload_counts , d.account_id FROM documents d LEFT JOIN account_folder_documents afd ON d.`id`= afd.`document_id` LEFT JOIN account_folders af ON afd.`account_folder_id`= af.`account_folder_id` LEFT JOIN user_activity_logs ua ON (d.id = ua.ref_id) WHERE d.account_id IN ($account_id_in) AND d.created_date >='$date_from' AND d.created_date <='$date_to' AND af.`folder_type` = 1 AND d.doc_type = 1 AND ua.site_id= " . $this->site_id . " GROUP BY d.created_by ) d on d.user_id = u.id  and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (115,110) AND ua.site_id= " . $this->site_id . "


UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.videos_viewed_count,0) as Count_Total,
        'Videos_Viewed' as event_type
from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_id, COUNT(*) AS videos_viewed_count , account_id FROM `user_activity_logs` WHERE TYPE=11 AND account_id IN ($account_id_in) AND date_added >='$date_from' AND date_added <='$date_to'  AND user_activity_logs.site_id= " . $this->site_id . " GROUP BY user_id ) d on d.user_id = u.id  and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (115,110)  AND ua.site_id= " . $this->site_id . "


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.huddle_created_count,0) as Count_Total,
    'Huddles_Created' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_id,COUNT(*) AS `huddle_created_count` , user_activity_logs.account_id FROM `user_activity_logs` join `account_folders` af ON af.account_folder_id = user_activity_logs.account_folder_id WHERE af.active = 1 and `type` = 1 AND user_activity_logs.account_id IN ($account_id_in) and date_added >='$date_from' AND date_added <= '$date_to' GROUP BY user_id ) d on d.user_id = u.id  and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (115,110)


UNION ALL

select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(d.comments_initiated_count,0) as Count_Total,
    'Total_Video_Comments' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join ( SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count, account_id FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE user_activity_logs.TYPE IN (5,8) AND user_activity_logs.account_id IN ($account_id_in) and date_added >='$date_from' AND date_added <= '$date_to' GROUP BY user_activity_logs.user_id ) d on d.user_id = u.id  and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (115,110)  AND ua.site_id= " . $this->site_id . "


UNION ALL


select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(d.`id`),0) as Count_Total,
    'Resources_Uploaded' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join account_folders af on af.created_by = u.id left join `account_folder_documents` afd on afd.`account_folder_id` = af.`account_folder_id` left join documents d on d.id = afd.document_id and d.account_id in ($account_id_in) and d.doc_type not in (3,1) and d.created_date >= '$date_from' and d.created_date <= '$date_to' where ua.account_id in ($account_id_in) and ua.role_id in (115,110) AND ua.site_id= " . $this->site_id . " Group by u.email, a.`company_name`

UNION ALL

    select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(ual.`id`),0) as Count_Total,
    'Resources_Viewed' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id in ($account_id_in) and ual.type=13 and ual.date_added >= '$date_from' and ual.date_added <= '$date_to' where ua.account_id in ($account_id_in) and ua.role_id in (115,110) AND ua.site_id= " . $this->site_id . " Group by u.email, a.`company_name`

UNION ALL

select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.minutes_watched,0) as Count_Total,
        'minutes_watched_huddle' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join (
    SELECT dvh.user_id, MAX(dvh.minutes_watched) AS minutes_watched, dvh.account_id
    FROM document_viewer_histories dvh
    LEFT JOIN account_folder_documents afd ON dvh.`document_id`= afd.`document_id`
    WHERE dvh.account_id IN ($account_id_in) AND dvh.created_date >='$date_from' AND dvh.created_date <='$date_to' AND dvh.site_id= " . $this->site_id . "
    GROUP BY dvh.user_id, dvh.document_id


) d on d.user_id = u.id   and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (115,110) AND ua.site_id= " . $this->site_id . "

UNION ALL


select
        Concat(u.first_name,' ', u.last_name) as UserFullName,
        u.id, u.email,
        a.`id` as account_id,
        a.`company_name`,
        ifnull(d.total_duration,0) as Count_Total,
        'total_duration_uploaded' as event_type

from users u join `users_accounts` ua on u.id = ua.user_id join accounts a on a.id = ua.account_id left join (

    SELECT d.created_by as user_id, SUM(df.duration) AS total_duration, d.account_id
    FROM document_files df
    JOIN documents d on d.id = df.document_id
    LEFT JOIN user_activity_logs ua ON df.`document_id`= ua.`ref_id`
    WHERE d.account_id IN ($account_id_in) AND d.created_date >='$date_from' AND d.created_date <='$date_to' AND ua.type IN (2,4) AND ua.site_id= " . $this->site_id . "
    GROUP BY d.created_by


) d on d.user_id = u.id   and d.account_id = ua.account_id where ua.account_id in ($account_id_in) and ua.role_id in (115,110) AND ua.site_id= " . $this->site_id . "

UNION ALL


select
    Concat(u.first_name,' ', u.last_name) as UserFullName,
    u.id,
    u.email,
    a.`id` as account_id,
    a.`company_name`,
    ifnull(count(date_added),0) as Count_Total,
    'Total_Logins' as event_type

from user_activity_logs ul join users u on u.id = ul.user_id join users_accounts ua on ua.user_id = u.id join accounts a on a.id = ua.account_id where ul.type=9 and ul.account_id in ($account_id_in) and date_added >= '$date_from' and date_added <= '$date_to' and a.id in ($account_id_in) and ua.role_id in (115,110) AND ua.site_id= " . $this->site_id . " Group by u.email, a.`company_name`

) as pivot group by pivot.UserFullName,
    pivot.email,
    pivot.`company_name`
) as a order by (
    Videos_Uploaded_Workspace + Videos_Uploaded_Huddle + Huddles_Created + Resources_Uploaded + Videos_Shared_Huddle + Videos_Viewed + Total_Video_Comments + Resources_Viewed + minutes_watched_huddle) DESC LIMIT 5");

        $users_counter = 0;
        foreach ($results as $result) {
            if ($result[0]['total_activity'] <= 0)
                continue;

            $users_counter++;
        }


        $top_engaged_super_admins_html = "";
        if ($users_counter > 0) {
            if ($bool) {
                $top_engaged_super_admins_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ' . $users_counter . ' ENGAGED SUPER ADMINS AND ADMINS IN THE NETWORK

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															Your top ' . $users_counter . ' Super Admins and Admins based on their account activity:

															</font>
													</td>
											</tr>
                                            </table>';
            } else {
                $top_engaged_super_admins_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ' . $users_counter . ' ENGAGED SUPER ADMINS AND ADMINS

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															Your top ' . $users_counter . ' Super Admins and Admins based on their account activity:

															</font>
													</td>
											</tr>
                                            </table>';
            }
        } else {
            if ($bool) {
                $top_engaged_super_admins_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ENGAGED SUPER ADMINS AND ADMINS IN THE NETWORK

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															NO DATA AVAILABLE

															</font>
													</td>
											</tr>
                                            </table>';
            } else {
                $top_engaged_super_admins_html .= '<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td width="100%" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px;">
															<font style="color: #69bd45; font-size: 20px; text-transform: uppercase; font-weight: 700; line-height: 21px;">

															TOP ENGAGED SUPER ADMINS AND ADMINS

															</font><br><br>
															<font style="font-size:13px; line-height:15px;">

															NO DATA AVAILABLE

															</font>
													</td>
											</tr>
                                            </table>';
            }
        }

        $result_counter = 1;

        foreach ($results as $result) {

            if ($result[0]['total_activity'] <= 0)
                continue;


            $users = $this->User->find("first", array("conditions" => array(
                    "User.email" => $result['a']['email'],
            )));


            $total_vide_duration_where = '';
            if ($date_from != '' && $date_to != '') {
                $total_vide_duration_where = "Document.created_date >= '$date_from' and Document.created_date <= '$date_to'";
            }
            $total_vide_duration = $this->Document->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_files as df',
                        'conditions' => 'Document.id = df.document_id',
                        'type' => 'inner'
                    ),
                    array(
                        'table' => '`user_activity_logs` as ua',
                        'conditions' => 'ua.ref_id = df.document_id',
                        'type' => 'inner'
                    ),
                ),
                'conditions' => array(
                    "Document.account_id IN ($account_id_in)",
                    'Document.created_by' => $users['User']['id'],
                    'ua.type IN (2,4) ',
                    $total_vide_duration_where
                ),
                'fields' => 'SUM(df.duration) as total_duration'
            ));


            if (isset($total_vide_duration[0][0]['total_duration']) && $total_vide_duration[0][0]['total_duration'] > 0) {
                $total_duration_hours = round(($total_vide_duration[0][0]['total_duration'] / 60) / 60, 1);
            } else {
                $total_duration_hours = 0;
            }

            /* ---------------Total Video Duration------------------ */
            $total_video_watched_where = '';
            if ($date_from != '' && $date_to != '') {
                $total_video_watched_where = "dvh.created_date >= '$date_from' and dvh.created_date <= '$date_to'";
            }
            $total_video_watched = $this->User->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_viewer_histories as dvh',
                        'conditions' => 'dvh.user_id = User.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'conditions' => 'afd.document_id = dvh.document_id',
                        'type' => 'inner'
                    ),
                ),
                'conditions' => array(
                    "dvh.account_id IN ($account_id_in) ",
                    'dvh.user_id' => $users['User']['id'],
                    $total_video_watched_where
                ),
                'group' => 'dvh.document_id',
                'fields' => 'max(dvh.minutes_watched) as minutes_watched'
            ));

            $total_min_watched = 0;
            if ($total_video_watched) {
                foreach ($total_video_watched as $row) {
                    $total_min_watched = $total_min_watched + $row[0]['minutes_watched'];
                }
            }
            if ($total_min_watched > 0) {
                $total_min_watched = round(($total_min_watched) / 60, 1);
            } else {
                $total_min_watched = 0;
            }

            if ($result['a']['minutes_watched_huddle'] > 0) {
                $total_min_watched = round(($result['a']['minutes_watched_huddle']), 1);
            } else {
                $total_min_watched = 0;
            }


            $top_engaged_super_admins_html .= '<table cellpadding="0" cellspacing="0" border="0" width="100%" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse">
											<tr>
												<td height="91" valign="middle" style="color:#111111; vertical-align:middle; text-align:left; padding:10px 20px 0px 20px; border-bottom:1px solid #ccc; line-height:20px;">
													<table cellpadding="0" cellspacing="0" border="0" width="100%" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:12px;color:#751c1c">
														<tr>
															<td style="padding:15px 0px">
																<font style="color:#0b475b; font-size:16px; font-weight: 700; text-transform:uppercase; line-height:20px;">
																	' . $result_counter . '. ' . $result['a']['company_name'] . ' - ' . $result['a']['UserFullName'] . '
																</font><br>
																<table cellpadding="0" cellspacing="0" border="0" width="544" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;" class="fullWidth">
																	<tr>
																		<td style="padding:10px 0px 0px 0px">
																			<table cellpadding="0" cellspacing="0" border="0" width="230" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="23" style="color:#111111;vertical-align:middle; text-align:center;padding-bottom: 5px;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-upload.png" height="17" width="19" alt="icon">
																					</td>
																					<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;padding-bottom: 10px;"><span style="color:#0b475b; font-size:14px; font-weight:bold;"> ' . $result['a']['Videos_Uploaded_Workspace'] . ' </span> <span style="color:#232323; font-size:11px;">Videos Uploaded to Workspace</span></td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="14" style="color:#111111;vertical-align:middle; text-align:center;padding-bottom: 5px;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-viewed.png" height="20" width="25" alt="icon">
																					</td>
																					<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;padding-bottom: 10px;">
																							<span style="color:#0b475b; font-size:14px; font-weight:bold;"> ' . $result['a']['Videos_Viewed'] . '  </span>
																							<span style="color:#232323; font-size:11px;">Videos Viewed</span>
																					</td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="14" style="color:#111111;vertical-align:middle; text-align:center;padding-bottom: 5px;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-minutes.png" width="23" alt="icon">
																					</td>
																					<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;padding-bottom: 10px;">
																							<span style="color:#0b475b; font-size:14px; font-weight:bold;"> ' . $total_min_watched . ' </span>
																							<span style="color:#232323; font-size:11px;">Minutes Viewed</span>
																					</td>
																				</tr>
																			</table>
																		</td>
																	</tr>

																	<tr>
																		<td style="padding:0px 0px">
																			<table cellpadding="0" cellspacing="0" border="0" width="230" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="23" style="color:#111111;vertical-align:middle; text-align:center;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-huddles.png" height="23" width="23" alt="icon">
																				</td>
																				<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;">
																						<span style="color:#0b475b; font-size:14px; font-weight:bold;">' . $result['a']['Videos_Uploaded_Huddle'] . '</span>
																						<span style="color:#232323; font-size:11px;">Videos Uploaded to Huddles</span>
																				</td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="13" style="color:#111111;vertical-align:middle; text-align:center;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-comments.png" height="23" width="24" alt="icon">
																				</td>
																				<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;">
																						<span style="color:#0b475b; font-size:14px; font-weight:bold;">' . $result['a']['Total_Video_Comments'] . '</span>
																						<span style="color:#232323; font-size:11px; ">Total Comments</span>
																				</td>
																				</tr>
																			</table>
																			<table cellpadding="0" cellspacing="0" border="0" width="150" align="left" style="mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,"helvetica neue",helvetica,arial,sans-serif;font-size:10px;color:#575656" class="fullWidth">
																				<tr>
																					<td width="13" style="color:#111111;vertical-align:middle; text-align:center;">
																						<img src="https://s3.amazonaws.com/sibme.com/edm_img/icn-hours.png" width="23" alt="icon">
																				</td>
																				<td style="color:#111111; vertical-align:middle; text-align:left; padding-left:10px;">
																						<span style="color:#0b475b; font-size:14px; font-weight:bold;">' . $total_duration_hours . '</span>
																						<span style="color:#232323; font-size:11px; ">Hours Uploaded</span>
																				</td>
																				</tr>
																			</table>
																		</td>
																	</tr>

																</table>

															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>';


            $result_counter += 1;
        }

        $content_html = str_replace("{top_engaged_super_admins_html}", $top_engaged_super_admins_html, $content_html);

        return $content_html;
        //return $content_html = str_replace("{top_engaged_users_html}", $top_engaged_users_html, $content_html);
    }

    private function getParentTotalUsersWActivity($account_id, $date_from, $date_to) {

        $account_id = implode(',', $account_id);

        $results = $this->UsersAccount->query(" select count(*) as total_users from (select distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.is_active=1 AND TYPE='Active' and u.site_id= " . $this->site_id . " and a.id IN ( " . $account_id . " ) and u.id in (select user_id from `user_activity_logs` where account_id IN (" . $account_id . ") and type not in (9,10,12,16) and date_added >= '$date_from' and date_added <= '$date_to'  )) as a");

        $total_users = 0;

        foreach ($results as $result) {

            $total_users += $result[0]['total_users'];
        }

        return $total_users;
    }

    private function getAccountTotalUsersWActivity($account_id, $date_from, $date_to, $user_id = '') {

        if (!empty(trim($user_id))) {
            $user_id = implode(',', $user_id);
            $results = $this->UsersAccount->query(" select count(*) as total_users from (select distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.is_active=1 AND TYPE='Active' and u.site_id= " . $this->site_id . " and a.id=$account_id and u.id in (select user_id from `user_activity_logs` where account_id = $account_id and user_id IN (" . $user_id . ") and type not in (9,10,12,16) and date_added >= '$date_from' and date_added <= '$date_to'  )) as a");
        } else {
            $results = $this->UsersAccount->query(" select count(*) as total_users from (select distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.is_active=1 AND TYPE='Active' and u.site_id= " . $this->site_id . " and a.id=$account_id and u.id in (select user_id from `user_activity_logs` where account_id = $account_id and type not in (9,10,12,16) and date_added >= '$date_from' and date_added <= '$date_to'  )) as a");
        }

        $total_users = 0;

        foreach ($results as $result) {

            $total_users += $result[0]['total_users'];
        }

        return $total_users;
    }

    private function getTotalUsersWActivity($account_id, $date_from, $date_to) {

        $results = $this->UsersAccount->query(" select count(*) as total_users from (select distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.is_active=1 AND TYPE='Active' and u.site_id= " . $this->site_id . " and a.id=$account_id and u.id in (select user_id from `user_activity_logs` where account_id = $account_id and type not in (9,10,12,16) and date_added >= '$date_from' and date_added <= '$date_to'  )) as a ");

        $total_users = 0;

        foreach ($results as $result) {

            $total_users += $result[0]['total_users'];
        }

        return $total_users;
    }

    private function getParentTotalUsers($account_id) {

        $results = $this->UsersAccount->query("

        select count(*) as total_users from (
            select distinct u.id as total_users
  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.is_active=1 and u.site_id= " . $this->site_id . " AND u.id NOT IN (2423,2422,2427) AND TYPE='Active' and a.id=$account_id
  ) as a

  UNION ALL

  select count(*) as total_users from (
   select  distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.is_active=1 AND u.id NOT IN (2423,2422,2427) and u.site_id= " . $this->site_id . " AND TYPE='Active' and a.parent_account_id=$account_id) as a");

        $total_users = 0;

        foreach ($results as $result) {

            $total_users += $result[0]['total_users'];
        }

        return $total_users;
    }

    private function getParentTotalActiveInActiveUsers($account_id) {
        if (!is_array($account_id)) {
            $account_id = array($account_id);
        }
        $account_id = implode(',', $account_id);
        $results = $this->UsersAccount->query(" select count(*) as total_users from (select distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE a.id IN (" . $account_id . ") and ua.site_id= " . $this->site_id . " and ua.role_id NOT IN(125) )  as a");

        $total_users = 0;

        foreach ($results as $result) {

            $total_users += $result[0]['total_users'];
        }

        return $total_users;
    }

    private function getTotalActiveInActiveUsers($account_id, $user_id = '') {

        if (!empty(trim($user_id))) {
            $user_id = implode(',', $user_id);
            $results = $this->UsersAccount->query(" select count(*) as total_users from (select distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.id IN (" . $user_id . ") and u.site_id= " . $this->site_id . " and a.id=$account_id and ua.role_id NOT IN(125) )  as a");
        } else {
            $results = $this->UsersAccount->query(" select count(*) as total_users from (select distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE a.id=$account_id and ua.site_id= " . $this->site_id . " and ua.role_id NOT IN(125) )  as a");
        }

        $total_users = 0;

        foreach ($results as $result) {

            $total_users += $result[0]['total_users'];
        }

        return $total_users;
    }

    private function getAccountTotalUsers($account_id) {

        $results = $this->UsersAccount->query(" select count(u.id) as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.is_active=1 AND a.site_id= " . $this->site_id . " and TYPE='Active' and a.id=$account_id");

        $total_users = 0;

        foreach ($results as $result) {

            $total_users += $result[0]['total_users'];
        }

        return $total_users;
    }

    private function getParentTotalHuddles($account_id, $date_from, $date_to, $user_id = '') {

        $results = $this->UsersAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (

                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=1) AND af.active = 1 AND ua.account_id IN (" . $account_id . ") AND  date_added >= '$date_from' AND date_added <= '$date_to' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.site_id= " . $this->site_id . " and u.type='Active'");

        $huddle_created_count = 0;

        foreach ($results as $result) {

            $huddle_created_count += $result[0]['huddle_created_count'];
        }

        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );

        foreach ($child_accounts as $child_account) {


            $results = $this->UsersAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (

                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=1) AND af.active = 1 AND ua.account_id IN (" . $child_account['Account']['id'] . ") AND  date_added >= '$date_from' AND date_added <= '$date_to' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $child_account['Account']['id'] . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.site_id= " . $this->site_id . " and u.type='Active'");

            foreach ($results as $result) {

                $huddle_created_count += $result[0]['huddle_created_count'];
            }
        }


        return $huddle_created_count;
    }

    private function getAccountTotalHuddles($account_id, $date_from, $date_to, $user_id = '', $huddle_ids = '') {

        if (!empty(trim($user_id))) {
            $user_id = implode(',', $user_id);

            $results = $this->UsersAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (

                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=1) AND af.active = 1 AND ua.account_id IN (" . $account_id . ") AND  date_added >= '$date_from' AND date_added <= '$date_to'  AND ua.account_folder_id IN (" . $huddle_ids . ") AND ua.user_id IN (" . $user_id . ") and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.site_id= " . $this->site_id . " and u.type='Active'");

            /* $results = $this->UsersAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs` join `account_folders` af ON af.account_folder_id = user_activity_logs.account_folder_id  WHERE af.active = 1 and `type` = 1 AND user_activity_logs.account_folder_id IN (" . $huddle_ids . ") AND user_activity_logs.user_id IN (" . $user_id . ")  AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "'"); */
        } else {

            $results = $this->UsersAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (

                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=1) AND af.active = 1 AND ua.account_id IN (" . $account_id . ") AND  date_added >= '$date_from' AND date_added <= '$date_to' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.site_id= " . $this->site_id . " and u.type='Active'");

            /* $results = $this->UsersAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs` join `account_folders` af ON af.account_folder_id = user_activity_logs.account_folder_id  WHERE af.active = 1 and `type` = 1 AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "'"); */
        }

        $huddle_created_count = 0;

        foreach ($results as $result) {

            $huddle_created_count += $result[0]['huddle_created_count'];
        }

        return $huddle_created_count;
    }

    private function getParentTotalVideos($account_id, $date_from, $date_to, $user_id = '') {

        $results = $this->UsersAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
    users u
     join `users_accounts` ua on u.id = ua.user_id
     join accounts a on a.id = ua.account_id
    left join (
    SELECT ua.account_id, ua.user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua
    LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
    WHERE (ua.TYPE=2 OR ua.TYPE=4) and ua.site_id= " . $this->site_id . "  AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND af.`folder_type` IN(1,2,3) AND ua.account_id IN (" . $account_id . ")  and date_added >= '" . $date_from . "' and date_added <= '" . $date_to . "' GROUP BY ua.user_id)
     d on d.user_id = u.id and d.account_id = ua.account_id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id . "");

        $huddle_videos_count = 0;
        foreach ($results as $result) {
            $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
        }

        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
        )));

        foreach ($child_accounts as $child_account) {
            $results = $this->UsersAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle  from
            users u
            join `users_accounts` ua on u.id = ua.user_id
            join accounts a on a.id = ua.account_id
            left join (
            SELECT ua.account_id,ua.user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua
            LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
            WHERE (ua.TYPE=2 OR ua.TYPE=4) AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND af.`folder_type` IN(1,2,3) AND ua.account_id IN (" . $child_account['Account']['id'] . ")  and date_added >= '" . $date_from . "' and date_added <= '" . $date_to . "' GROUP BY ua.user_id)
            d on d.user_id = u.id and d.account_id = ua.account_id where u.is_active=1 and u.site_id= " . $this->site_id . " and  u.type='Active'");
            foreach ($results as $result) {
                $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
            }
        }

        return $huddle_videos_count;
    }

    private function getAccountTotalVideos($account_id, $date_from, $date_to, $user_id = '', $all_huddle_ids = '') {
        if (!empty(trim($user_id))) {
            $user_id = implode(',', $user_id);
            $results = $this->UsersAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
    users u
     join `users_accounts` ua on u.id = ua.user_id
     join accounts a on a.id = ua.account_id
    left join (SELECT ua.account_id,user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua
    LEFT JOIN  account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4)  AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND af.`folder_type` IN(1,2,3) AND af.account_folder_id IN (" . $all_huddle_ids . ")  AND ua.user_id IN (" . $user_id . ")   AND ua.account_id IN (" . $account_id . ")  and date_added >= '" . $date_from . "' and date_added <= '" . $date_to . "' GROUP BY ua.user_id)
    d on d.user_id = u.id  and d.account_id = ua.account_id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);
        } else {
            $results = $this->UsersAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
    users u
    join `users_accounts` ua on u.id = ua.user_id
    join accounts a on a.id = ua.account_id
    left join (SELECT ua.account_id,user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua
    LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
    WHERE (ua.TYPE=2 OR ua.TYPE=4)  AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND af.`folder_type` IN(1,2,3) AND ua.account_id IN (" . $account_id . ")  and date_added >= '" . $date_from . "' and date_added <= '" . $date_to . "' GROUP BY ua.user_id )
    d on d.user_id = u.id and d.account_id = ua.account_id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);
        }

        $huddle_videos_count = 0;

        foreach ($results as $result) {

            $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
        }

        return $huddle_videos_count;
    }

    private function getParentTotalVideosViewed($account_id, $date_from, $date_to, $user_id = '') {

        $results = $this->UsersAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u left join

( SELECT ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua join account_folder_documents afd on afd.document_id = ua.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id WHERE (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $account_id . ") AND date_added >= '" . $date_from . "' AND date_added <= '" . $date_to . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'and u.site_id= " . $this->site_id);


        $huddle_videos_count = 0;

        foreach ($results as $result) {

            $huddle_videos_count += $result[0]['Resources_Viewed'];
        }


        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );

        foreach ($child_accounts as $child_account) {

            $results = $this->UsersAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u left join
( SELECT ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua join account_folder_documents afd on afd.document_id = ua.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id WHERE (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $child_account['Account']['id'] . ") AND date_added >= '" . $date_from . "' AND date_added <= '" . $date_to . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $child_account['Account']['id'] . ")) GROUP BY ua.user_id ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);

            foreach ($results as $result) {

                $huddle_videos_count += $result[0]['Resources_Viewed'];
            }
        }

        return $huddle_videos_count;
    }

    private function getAccountTotalVideosViewed($account_id, $date_from, $date_to, $user_id = '', $all_huddle_ids = false) {
        if (!empty(trim($user_id))) {
            $user_id = implode(',', $user_id);
            $results = $this->UsersAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u
            join `users_accounts` ua on u.id = ua.user_id
            join accounts a on a.id = ua.account_id
            left join
            ( SELECT ua.account_id, ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua
            join account_folder_documents afd on afd.document_id = ua.ref_id
            join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id
            WHERE (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $account_id . ") AND date_added >= '" . $date_from . "' AND date_added <= '" . $date_to . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) and ua.user_id IN (" . $user_id . ") and ua.account_folder_id IN (" . $all_huddle_ids . ") GROUP BY ua.user_id , ua.account_id) d on d.user_id = u.id and d.account_id = ua.account_id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);
        } else {

            $results = $this->UsersAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u
            join `users_accounts` ua on u.id = ua.user_id
            join accounts a on a.id = ua.account_id
            left join
        ( SELECT ua.account_id, ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua join account_folder_documents afd on afd.document_id = ua.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id WHERE (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $account_id . ") AND date_added >= '" . $date_from . "' AND date_added <= '" . $date_to . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id , ua.account_id ) d on d.user_id = u.id and d.account_id = ua.account_id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);
        }

        $huddle_videos_count = 0;

        foreach ($results as $result) {

            $huddle_videos_count += $result[0]['Resources_Viewed'];
        }

        return $huddle_videos_count;
    }

    private function getParentTotalVideosCommentsCount($account_id, $date_from, $date_to, $user_id = '') {
        $results = $this->UsersAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
    WHERE user_activity_logs.TYPE IN (5,8) AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' GROUP BY user_activity_logs.user_id

    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);

        $results_replies = $this->UsersAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    WHERE user_activity_logs.TYPE IN (8) AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' GROUP BY user_activity_logs.user_id
    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);


        $huddle_videos_count = 0;
        $huddle_videos_replies_count = 0;

        foreach ($results as $result) {
            $huddle_videos_count += $result[0]['Total_Video_Comments'];
        }

        foreach ($results_replies as $replies) {
            $huddle_videos_replies_count += $replies[0]['Total_Video_Comments_Replies'];
        }


        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );

        foreach ($child_accounts as $child_account) {


            $results = $this->UsersAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
    WHERE user_activity_logs.TYPE IN (5,8) AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' GROUP BY user_activity_logs.user_id
    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);

            $results_replies = $this->UsersAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    WHERE user_activity_logs.TYPE IN (8) AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' GROUP BY user_activity_logs.user_id
    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);

            foreach ($results as $result) {
                $huddle_videos_count += $result[0]['Total_Video_Comments'];
            }
            foreach ($results_replies as $replies) {
                $huddle_videos_replies_count += $replies[0]['Total_Video_Comments_Replies'];
            }
        }

        return $huddle_videos_count + $huddle_videos_replies_count;
    }

    private function getAccountTotalVideosCommentsCount($account_id, $date_from, $date_to, $user_id = '', $all_huddle_ids = '') {
        if (!empty(trim($user_id))) {
            $user_id = implode(',', $user_id);
            $results = $this->UsersAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
    WHERE user_activity_logs.TYPE IN (5) AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.user_id IN (" . $user_id . ") AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' GROUP BY user_activity_logs.user_id
    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);

            $results_replies = $this->UsersAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    WHERE user_activity_logs.TYPE IN (8) AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.user_id IN (" . $user_id . ") AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' GROUP BY user_activity_logs.user_id
    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);
        } else {

            $results = $this->UsersAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
    WHERE user_activity_logs.type IN (5) AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' GROUP BY user_activity_logs.user_id
    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);

            $results_replies = $this->UsersAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    WHERE user_activity_logs.type IN (8) AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' GROUP BY user_activity_logs.user_id

    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' and u.site_id= " . $this->site_id);
        }

        $huddle_videos_count = 0;
        $huddle_videos_replies_count = 0;
        foreach ($results as $result) {
            $huddle_videos_count += $result[0]['Total_Video_Comments'];
        }
        foreach ($results_replies as $replies) {
            $huddle_videos_replies_count += $replies[0]['Total_Video_Comments_Replies'];
        }

        return $huddle_videos_count + $huddle_videos_replies_count;
    }

    private function thousandsCurrencyFormat($num) {
        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('k', 'm', 'b', 't');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];
        return $x_display;
    }

    function monthly_report_template($ac_id = '', $us_id = '', $template_id = '') {
        $template_data = "&lt;!DOCTYPE html&gt;
&lt;html&gt;
&lt;head&gt;
	&lt;meta charset=&quot;utf-8&quot;&gt;
	&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1.0&quot;&gt;
	&lt;title&gt;SIBME&lt;/title&gt;
	&lt;link href=&quot;https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&quot; rel=&quot;stylesheet&quot;&gt;

&lt;style type=&quot;text/css&quot;&gt;
/*
		font-family DINNextsrc urlhttpstaging.sageninja.comsibmefontsDIN-NextDINNextLTPro-Regular.otf formatopentype{
			font-family:'DINNExt';
			font-weight:bold;
			src:url('http://staging.sageninja.com/sibme/fonts/DIN-Next/DINNextLTPro-Bold.otf') format('opentype');
		}
		font-family DINNExtfont-weight 400src urlhttpstaging.sageninja.comsibmefontsDIN-NextDINNextLTPro-Light.otf formatopentype{
			font-family:'Oswald';
			src:url('http://staging.sageninja.com/sibme/fonts/Oswald/Oswald-Regular.otf') format('opentype');
		}
		font-family Oswaldfont-weight boldsrc urlhttpstaging.sageninja.comsibmefontsOswaldOswald-Bold.otf formatopentypebody{
			font-family:'DINNExt', 'Helvetica', 'sans-serif';
			margin:0;
			padding:0;
		}
		[style*=DINNext]{
			font-family:'DINNext',Verdana,'sans-serif' !important;
		}
		[style*=Oswald]{
			font-family:'Oswald',Verdana,'sans-serif' !important;
		}
*/
	body{
		font-family:roboto,'helvetica neue',helvetica,arial,'sans-serif' !important;
		margin:0;
		padding:0;
	}
	@media only screen and (max-width:480px){
		.fullWidth{
			width:100% !important;
		}
		.text-15 {
			font-size: 15px !important;
		}

		.text-20 {
			font-size: 20px !important;
		}

		.text-28 {
			font-size: 28px !important;
		}

		.text-title {
			font-weight: bold;
			vertical-align: middle;
		}

}	@media only screen and (max-width:480px){
		.halfWidth{
			width:50% !important;
		}

}	@media only screen and (max-width:480px){
		.textCenter{
			text-align:center !important;
		}

}	@media only screen and (max-width:480px){
		.noFloat{
			float:none !important;
			margin:0 auto !important;
		}

}	@media only screen and (max-width:480px){
		.marginBottom10{
			margin-bottom:10px !important;
		}

}	@media only screen and (max-width:480px){
		.paddingTop10{
			padding-top:10px !important;
		}

}	@media only screen and (max-width:480px){
		.noPaddingBottom{
			padding-bottom:0px !important;
		}

}&lt;/style&gt;&lt;/head&gt;
&lt;body style=&quot;margin:0px;padding:0px;-webkit-text-size-adjust:none;background-color:#f1f1f1&quot; yahoo=&quot;fix&quot;&gt;
	&lt;!-- WRAPPER --&gt;

	&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse; margin-top: 50px;&quot;&gt;
		&lt;tr&gt;
			&lt;td align=&quot;center&quot;&gt;

				&lt;!-- START --&gt;
				&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;650&quot; class=&quot;fullWidth&quot;&gt;
					&lt;tr&gt;
						&lt;td&gt;
							&lt;!-- HEADER --&gt;
							&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
								&lt;tr&gt;
									&lt;td background=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-header-bg2.jpg&quot; bgcolor=&quot;#0e3249&quot; height=&quot;237&quot; valign=&quot;middle&quot; style=&quot;background-repeat:no-repeat; border-top: 7px solid #6cbd48; &quot;&gt;
										&lt;!--[if gte mso 9]&gt;
										&lt;v:rect xmlns:v=&quot;urn:schemas-microsoft-com:vml&quot; fill=&quot;true&quot; stroke=&quot;false&quot; style=&quot;width:650px;height:237px;&quot;&gt;
										&lt;v:fill type=&quot;frame&quot; src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-header-bg2.jpg&quot; color=&quot;#0e3249&quot; /&gt;
										&lt;v:textbox inset=&quot;0,0,0,0&quot;&gt;
										&lt;![endif]--&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
											&lt;tr&gt;
												&lt;td height=&quot;237&quot; style=&quot;padding:0px 35px&quot;&gt;

													&lt;!--[if gte mso 9]&gt;&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;left&quot;&gt;&lt;![endif]--&gt;

																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-weight:400;font-size:19px;line-height:21px;color:#ffffff;&quot; class=&quot;fullWidth&quot; align=&quot;left&quot;&gt;
																	&lt;tr&gt;
																		&lt;td valign=&quot;top&quot; class=&quot;textCenter&quot;&gt;
																			&lt;font style=&quot;color:#ffffff; font-size: 31px; line-height: 32px; font-weight: 300; &quot;&gt;

																			{account_name_header}

																			&lt;/font&gt;&lt;br&gt;
																			&lt;font style=&quot;color: #98d5ff; display: block; font-size:19px; line-height: 23px; margin-top: 10px;&quot;&gt;

																			Summary Report&lt;br&gt;
																			{date_range_header}

																			&lt;/font&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;


															&lt;!--[if gte mso 9]&gt;&lt;/td&gt;
															&lt;td align=&quot;right&quot; style=&quot;width:189px&quot;&gt;&lt;![endif]--&gt;

																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;&quot; class=&quot;fullWidth&quot; align=&quot;right&quot;&gt;
																	&lt;tr&gt;
																		&lt;td valign=&quot;top&quot; align=&quot;right&quot; class=&quot;textCenter&quot;&gt;
																		&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-logo.png&quot; alt=&quot;&quot; border=&quot;0&quot; style=&quot;display:block; margin:0px auto&quot; width=&quot;100&quot;&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;

															&lt;!--[if gte mso 9]&gt;&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;&lt;![endif]--&gt;

												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!--[if gte mso 9]&gt;
										&lt;/v:textbox&gt;
										&lt;/v:rect&gt;
										&lt;![endif]--&gt;
									&lt;/td&gt;
								&lt;/tr&gt;
							&lt;/table&gt;
							&lt;!-- /HEADER --&gt;



							&lt;!-- MAIN --&gt;
							&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;background: url(https://s3.amazonaws.com/sibme.com/edm_img/border-rainbow.png) no-repeat bottom center; background-size: 650px 5px; background-color: #f9f9f9;&quot;&gt;
								&lt;tr&gt;
									&lt;td align=&quot;center&quot; style=&quot;padding:28px 30px;&quot;&gt;

										&lt;!-- SECTION 1 --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;background-color:#0c3345;&quot;&gt;
											&lt;tr&gt;
												&lt;td valign=&quot;middle&quot; style=&quot;padding:14px 20px;&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;&quot;&gt;
														&lt;tr&gt;
															&lt;!--&lt;td width=&quot;45&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-network-account-overview.png&quot; alt=&quot;icon&quot; border=&quot;0&quot; style=&quot;margin:0px;display:block&quot; width=&quot;30&quot;&gt;&lt;/td&gt;--&gt;
															&lt;td style=&quot;padding-right:20px;text-transform:uppercase; color:#ffffff; vertical-align:middle;font-size:25px;&quot; class=&quot;text-15&quot;&gt;Account Overview&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;center&quot; style=&quot;padding:5px 20px&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;435&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot;&gt;
																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; align=&quot;left&quot; &gt;
																	&lt;tr&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-huddles.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{parent_account_active_huddles}

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Huddles&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-upload.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{parent_account_active_videos}

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Videos Uploaded&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-viewed.png&quot; alt=&quot;icon&quot; width=&quot;50&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{parent_account_active_viewed}

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Videos Viewed&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
											&lt;tr&gt;
												&lt;td align=&quot;center&quot; style=&quot;padding:5px 20px&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;435&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot;&gt;
																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; align=&quot;left&quot; &gt;
																	&lt;tr&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-hours.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{total_hours_watched}

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Video Hours Viewed&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-comments.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{parent_account_active_comments}

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Video Comments Added&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;

																	&lt;/tr&gt;
																&lt;/table&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;


										&lt;!-- /SECTION 1 --&gt;


&lt;!-- SECTION 2.5 --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;background-color:#0c3345;&quot;&gt;
											&lt;tr&gt;
												&lt;td valign=&quot;middle&quot; style=&quot;padding:14px 20px;&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;&quot;&gt;
														&lt;tr&gt;
															&lt;!--&lt;td width=&quot;45&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-users.png&quot; alt=&quot;icon&quot; width=&quot;30&quot;&gt;&lt;/td&gt;--&gt;
															&lt;td style=&quot;padding-right:20px;text-transform:uppercase; color:#ffffff; vertical-align:middle;font-size:25px;&quot; class=&quot;text-15&quot;&gt;People&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;

										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;center&quot; style=&quot;padding:5px 20px&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;435&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot;&gt;
																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; align=&quot;left&quot; &gt;
																	&lt;tr&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-user-engagement.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{users_engagement}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;User Engagement&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-active-users.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{getParentTotalUsers_getParentTotalActiveInActiveUsers_percentage}

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Active Users&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-total-users.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{all_total_users}

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Total Users&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!-- SECTION 2.5 --&gt;


										&lt;!-- SECTION 2 --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;background-color:#0c3345;&quot;&gt;
											&lt;tr&gt;
												&lt;td valign=&quot;middle&quot; style=&quot;padding:14px 20px;&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;&quot;&gt;
														&lt;tr&gt;
															&lt;!--&lt;td width=&quot;45&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-user-engagement.png&quot; alt=&quot;&quot; border=&quot;0&quot; style=&quot;margin:0px;display:block&quot; width=&quot;30&quot;&gt;&lt;/td&gt;--&gt;
															&lt;td style=&quot;padding-right:20px;text-transform:uppercase; color:#ffffff; vertical-align:middle;font-size:25px;&quot; class=&quot;text-15&quot;&gt;User Engagement Growth&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;

										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;center&quot; style=&quot;padding:5px 20px&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;435&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot;&gt;
																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; align=&quot;left&quot; &gt;
																	&lt;tr&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-upload.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{percent_Increase_in_Videos_Uploaded}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Video Uploads&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-viewed.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{percent_Increase_in_Videos_Viewed}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Videos Viewed&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot;&gt;
																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; align=&quot;left&quot; &gt;
																	&lt;tr&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-huddles.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{percent_Increase_in_Huddles_Created}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Huddles in the Account&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-comments.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-28&quot;&gt;

																									{percent_Increase_in_Comments_Added}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Comments Created&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!-- /SECTION 2 --&gt;






										&lt;!-- SECTION 3 --&gt;


										{top_engaged_users_html}


										&lt;!-- /SECTION 3 --&gt;

&lt;br/&gt;
										&lt;!-- SECTION 3 --&gt;


										{top_engaged_super_admins_html}


										&lt;!-- /SECTION 3 --&gt;




									&lt;/td&gt;
								&lt;/tr&gt;
							&lt;/table&gt;
							&lt;!-- /MAIN --&gt;



							&lt;!-- FOOTER --&gt;
							&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;background-color:#ffffff;&quot;&gt;
								&lt;tr&gt;
									&lt;td valign=&quot;middle&quot; style=&quot;padding:35px&quot;&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;424&quot; align=&quot;left&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; class=&quot;fullWidth&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;left&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;27%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;&quot; align=&quot;left&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;left&quot; class=&quot;textCenter&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-logo2.png&quot; alt=&quot;logo&quot; width=&quot;102&quot; border=&quot;0&quot;&gt;&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;

													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;70%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#232323&quot; align=&quot;right&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt; &lt;td align=&quot;right&quot; height=&quot;54&quot; class=&quot;textCenter&quot;&gt;&lt;a href=&quot;http://www.sibme.com/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Website&lt;/a&gt; &amp;bull; &lt;a href=&quot;http://help.sibme.com/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Help Center&lt;/a&gt; &amp;bull; &lt;a href=&quot;http://help.sibme.com/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Support&lt;/a&gt;
                                                                                                                &amp;bull; &lt;a href=&quot;{{site_base_url}}Users/unsubscribeWeeklyMonthly/{{user_account_unsubscribe}}&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Unsubscribe&lt;/a&gt;&lt;/td&gt;

														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;

										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;145&quot; align=&quot;right&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; class=&quot;noFloat&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://www.facebook.com/sibmeapp/?ref=br_rs&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-facebook.png&quot; height=&quot;23&quot; alt=&quot;social-fb&quot;&gt;&lt;/a&gt;&lt;/td&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://twitter.com/SibmeApp&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-twitter.png&quot; height=&quot;23&quot; alt=&quot;social-twitter&quot;&gt;&lt;/a&gt;&lt;/td&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://plus.google.com/+Sibmeapp&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-googleplus.png&quot; height=&quot;23&quot; alt=&quot;social-googleplus&quot;&gt;&lt;/a&gt;&lt;/td&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://www.linkedin.com/company/sibme/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-linkedin.png&quot; height=&quot;23&quot; alt=&quot;social-linkedin&quot;&gt;&lt;/a&gt;&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
									&lt;/td&gt;
								&lt;/tr&gt;
							&lt;/table&gt;
							&lt;!-- /FOOTER --&gt;



						&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr&gt;
						&lt;td&gt;&lt;center&gt;&lt;p style=&quot;font-size: 14px; color: #aaa; text-transform: uppercase;&quot;&gt;&lt;/p&gt;&lt;/center&gt;&lt;/td&gt;
					&lt;/tr&gt;
				&lt;/table&gt;
				&lt;!-- END --&gt;

			&lt;/td&gt;
		&lt;/tr&gt;
	&lt;/table&gt;
	&lt;!-- /WRAPPER --&gt;


&lt;/body&gt;
&lt;/html&gt;";

        return $template_data;
    }

    function network_report_template() {
        $template_data = "&lt;!DOCTYPE html&gt;
&lt;html&gt;
&lt;head&gt;
	&lt;meta charset=&quot;utf-8&quot;&gt;
	&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1.0&quot;&gt;
	&lt;title&gt;SIBME&lt;/title&gt;
	&lt;link href=&quot;https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&quot; rel=&quot;stylesheet&quot;&gt;

&lt;style type=&quot;text/css&quot;&gt;
/*
		font-family DINNextsrc urlhttpstaging.sageninja.comsibmefontsDIN-NextDINNextLTPro-Regular.otf formatopentype{
			font-family:'DINNExt';
			font-weight:bold;
			src:url('http://staging.sageninja.com/sibme/fonts/DIN-Next/DINNextLTPro-Bold.otf') format('opentype');
		}
		font-family DINNExtfont-weight 400src urlhttpstaging.sageninja.comsibmefontsDIN-NextDINNextLTPro-Light.otf formatopentype{
			font-family:'Oswald';
			src:url('http://staging.sageninja.com/sibme/fonts/Oswald/Oswald-Regular.otf') format('opentype');
		}
		font-family Oswaldfont-weight boldsrc urlhttpstaging.sageninja.comsibmefontsOswaldOswald-Bold.otf formatopentypebody{
			font-family:'DINNExt', 'Helvetica', 'sans-serif';
			margin:0;
			padding:0;
		}
		[style*=DINNext]{
			font-family:'DINNext',Verdana,'sans-serif' !important;
		}
		[style*=Oswald]{
			font-family:'Oswald',Verdana,'sans-serif' !important;
		}
*/
	body{
		font-family:roboto,'helvetica neue',helvetica,arial,'sans-serif' !important;
		margin:0;
		padding:0;
	}
	@media only screen and (max-width:480px){
		.fullWidth{
			width:100% !important;
		}
		.text-15 {
			font-size: 15px !important;
		}

		.text-20 {
			font-size: 20px !important;
		}

		.text-35 {
			font-size: 35px !important;
		}

		.text-title {
			font-weight: bold;
			vertical-align: middle;
		}

}	@media only screen and (max-width:480px){
		.halfWidth{
			width:50% !important;
		}

}	@media only screen and (max-width:480px){
		.textCenter{
			text-align:center !important;
		}

}	@media only screen and (max-width:480px){
		.noFloat{
			float:none !important;
			margin:0 auto !important;
		}

}	@media only screen and (max-width:480px){
		.marginBottom10{
			margin-bottom:10px !important;
		}

}	@media only screen and (max-width:480px){
		.paddingTop10{
			padding-top:10px !important;
		}

}	@media only screen and (max-width:480px){
		.noPaddingBottom{
			padding-bottom:0px !important;
		}

}&lt;/style&gt;&lt;/head&gt;
&lt;body style=&quot;margin:0px;padding:0px;-webkit-text-size-adjust:none;background-color:#f1f1f1&quot; yahoo=&quot;fix&quot;&gt;
	&lt;!-- WRAPPER --&gt;
	&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse; margin-top: 50px;&quot;&gt;
		&lt;tr&gt;
			&lt;td align=&quot;center&quot;&gt;

				&lt;!-- START --&gt;
				&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;650&quot; class=&quot;fullWidth&quot;&gt;
					&lt;tr&gt;
						&lt;td&gt;
							&lt;!-- HEADER --&gt;
							&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
								&lt;tr&gt;
									&lt;td background=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-header-bg2.jpg&quot; bgcolor=&quot;#0e3249&quot; height=&quot;237&quot; valign=&quot;middle&quot; style=&quot;background-repeat:no-repeat; border-top: 7px solid #6cbd48; &quot;&gt;
										&lt;!--[if gte mso 9]&gt;
										&lt;v:rect xmlns:v=&quot;urn:schemas-microsoft-com:vml&quot; fill=&quot;true&quot; stroke=&quot;false&quot; style=&quot;width:650px;height:237px;&quot;&gt;
										&lt;v:fill type=&quot;frame&quot; src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-header-bg2.jpg&quot; color=&quot;#0e3249&quot; /&gt;
										&lt;v:textbox inset=&quot;0,0,0,0&quot;&gt;
										&lt;![endif]--&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
											&lt;tr&gt;
												&lt;td height=&quot;237&quot; style=&quot;padding:0px 35px&quot;&gt;

													&lt;!--[if gte mso 9]&gt;&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;left&quot;&gt;&lt;![endif]--&gt;

																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-weight:400;font-size:19px;line-height:21px;color:#ffffff;&quot; class=&quot;fullWidth&quot; align=&quot;left&quot;&gt;
																	&lt;tr&gt;
																		&lt;td valign=&quot;top&quot; class=&quot;textCenter&quot;&gt;
																			&lt;font style=&quot;color:#ffffff; font-size: 31px; line-height: 32px; font-weight: 300; &quot;&gt;

																			{account_name_header}

																			&lt;/font&gt;&lt;br&gt;
																			&lt;font style=&quot;color: #98d5ff; display: block; font-size:19px; line-height: 23px; margin-top: 10px;&quot;&gt;

																			Network Summary Report&lt;br&gt;
																			{date_range_header}

																			&lt;/font&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;


															&lt;!--[if gte mso 9]&gt;&lt;/td&gt;
															&lt;td align=&quot;right&quot; style=&quot;width:189px&quot;&gt;&lt;![endif]--&gt;

																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;&quot; class=&quot;fullWidth&quot; align=&quot;right&quot;&gt;
																	&lt;tr&gt;
																		&lt;td valign=&quot;top&quot; align=&quot;right&quot; class=&quot;textCenter&quot;&gt;
																		&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-logo.png&quot; alt=&quot;logo&quot; border=&quot;0&quot; style=&quot;display:block; margin:0px auto&quot; width=&quot;100&quot;&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;

															&lt;!--[if gte mso 9]&gt;&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;&lt;![endif]--&gt;

												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!--[if gte mso 9]&gt;
										&lt;/v:textbox&gt;
										&lt;/v:rect&gt;
										&lt;![endif]--&gt;
									&lt;/td&gt;
								&lt;/tr&gt;
							&lt;/table&gt;
							&lt;!-- /HEADER --&gt;



							&lt;!-- MAIN --&gt;
							&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;background: url(https://s3.amazonaws.com/sibme.com/edm_img/border-rainbow.png) no-repeat bottom center; background-size: 650px 5px; background-color: #f9f9f9;&quot;&gt;
								&lt;tr&gt;
									&lt;td align=&quot;center&quot; style=&quot;padding:28px 30px;&quot;&gt;

										&lt;!-- SECTION 1 --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;background-color:#0c3345;&quot;&gt;
											&lt;tr&gt;
												&lt;td valign=&quot;middle&quot; style=&quot;padding:14px 20px;&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;&quot;&gt;
														&lt;tr&gt;
															&lt;!--&lt;td width=&quot;45&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-network-account-overview_1.png&quot; alt=&quot;icon&quot; border=&quot;0&quot; style=&quot;margin:0px; display:block&quot; width=&quot;30&quot;&gt;&lt;/td&gt;--&gt;
															&lt;td style=&quot;padding-right:20px;text-transform:uppercase; color:#ffffff; vertical-align:middle;font-size:25px;&quot; class=&quot;text-15&quot;&gt;Network Account Overview&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;


										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
												&lt;tr&gt;
													&lt;td style=&quot;padding:25px 20px&quot;&gt;
														&lt;table class=&quot;school-table&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-size:12px;color:#575656;&quot;&gt;
															&lt;tr&gt;
																&lt;td valign=&quot;top&quot; width=&quot;125&quot; height=&quot;95&quot; class=&quot;spacer&quot;&gt;&amp;nbsp;

																&lt;/td&gt;
																&lt;td valign=&quot;top&quot; style=&quot;border-left:5px solid #f9f9f9&quot;&gt;
																	&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-size:12px;color:#575656&quot; width=&quot;60&quot; class=&quot;fullWidth&quot;&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-users.png&quot; alt=&quot;&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto 5px;display:block&quot;&gt;&lt;/td&gt;
																		&lt;/tr&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;middle&quot; align=&quot;center&quot; height=&quot;50&quot;&gt;
																				&lt;strong style=&quot;color:#60ae47; font-size: 10px; line-height: 14px; display: block; text-transform: uppercase;&quot;&gt;

																				Active Users

																				&lt;/strong&gt;
																			&lt;/td&gt;
																		&lt;/tr&gt;
																	&lt;/table&gt;
																&lt;/td&gt;
																&lt;td valign=&quot;top&quot; style=&quot;border-left:5px solid #f9f9f9&quot;&gt;
																	&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-size:12px;color:#575656&quot; width=&quot;60&quot; class=&quot;fullWidth&quot;&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-huddles.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																		&lt;/tr&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;middle&quot; align=&quot;center&quot; height=&quot;50&quot;&gt;
																				&lt;strong style=&quot;color:#0e9bc4; font-size: 10px; line-height: 14px; display: block; text-transform: uppercase;&quot;&gt;

																				Huddles

																				&lt;/strong&gt;
																			&lt;/td&gt;
																		&lt;/tr&gt;
																	&lt;/table&gt;
																&lt;/td&gt;
																&lt;td valign=&quot;top&quot; style=&quot;border-left:5px solid #f9f9f9&quot; align=&quot;center&quot;&gt;
																	&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-size:12px;color:#575656&quot; width=&quot;60&quot; class=&quot;fullWidth&quot;&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-upload.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto 5px;display:block&quot;&gt;&lt;/td&gt;
																		&lt;/tr&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;middle&quot; align=&quot;center&quot; height=&quot;50&quot;&gt;
																				&lt;strong style=&quot;color:#0b3e4d; font-size: 10px; line-height: 14px; display: block; text-transform: uppercase;&quot;&gt;

																				Videos Uploaded

																				&lt;/strong&gt;
																			&lt;/td&gt;
																		&lt;/tr&gt;
																	&lt;/table&gt;
																&lt;/td&gt;
																&lt;td valign=&quot;top&quot; style=&quot;border-left:5px solid #f9f9f9;&quot; align=&quot;center&quot;&gt;
																	&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-size:12px;color:#575656&quot; width=&quot;60&quot; class=&quot;fullWidth&quot;&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-viewed.png&quot; alt=&quot;icon&quot; width=&quot;54&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																		&lt;/tr&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;middle&quot; align=&quot;center&quot; height=&quot;50&quot;&gt;
																				&lt;strong style=&quot;color:#0b3e4d; font-size: 10px; line-height: 14px; display: block; text-transform: uppercase;&quot;&gt;

																				Videos Viewed

																				&lt;/strong&gt;
																			&lt;/td&gt;
																		&lt;/tr&gt;
																	&lt;/table&gt;
																&lt;/td&gt;
																&lt;td valign=&quot;top&quot; style=&quot;border-left:5px solid #f9f9f9;&quot; align=&quot;center&quot;&gt;
																	&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-size:12px;color:#575656&quot; width=&quot;60&quot; class=&quot;fullWidth&quot;&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-hours.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																		&lt;/tr&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;middle&quot; align=&quot;center&quot; height=&quot;50&quot;&gt;
																				&lt;strong style=&quot;color:#0b3e4d; font-size: 10px; line-height: 14px; display: block; text-transform: uppercase;&quot;&gt;                                                                         VIDEO HOURS VIEWED &lt;/strong&gt;
																			&lt;/td&gt;
																		&lt;/tr&gt;
																	&lt;/table&gt;
																&lt;/td&gt;
																&lt;td valign=&quot;top&quot; style=&quot;border-left:5px solid #f9f9f9;&quot; align=&quot;center&quot;&gt;
																	&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-size:12px;color:#575656&quot; width=&quot;60&quot; class=&quot;fullWidth&quot;&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-comments.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																		&lt;/tr&gt;
																		&lt;tr&gt;
																			&lt;td valign=&quot;middle&quot; align=&quot;center&quot; height=&quot;50&quot;&gt;
																				&lt;strong style=&quot;color:#0b3e4d; font-size: 10px; line-height: 14px; display: block; text-transform: uppercase;&quot;&gt;

																				Video Comments Added

																				&lt;/strong&gt;
																			&lt;/td&gt;
																		&lt;/tr&gt;
																	&lt;/table&gt;
																&lt;/td&gt;
															&lt;/tr&gt;

															&lt;tr&gt;
																&lt;td align=&quot;left&quot;  height=&quot;50&quot; valign=&quot;bottom&quot; style=&quot;color:#0b3e4d;font-size:14px;padding-top:8px&quot;&gt;
																	&lt;strong&gt;{account_name_header}&lt;/strong&gt;
																&lt;/td&gt;
																&lt;td align=&quot;center&quot; height=&quot;50&quot; valign=&quot;bottom&quot; style=&quot;color:#60ae47;font-size:25px;padding-top:10px;border-left:5px solid #f9f9f9;width:66px;background-color:#cee3d3&quot; class=&quot;text-15 text-title&quot;&gt;{parent_account_active_users}&lt;/td&gt;
																&lt;td align=&quot;center&quot; height=&quot;50&quot; valign=&quot;bottom&quot; style=&quot;color:#0e9bc4;font-size:25px;padding-top:10px;border-left:5px solid #f9f9f9;width:66px;background-color:#cbe7ea&quot; class=&quot;text-15 text-title&quot;&gt;{parent_account_active_huddles}&lt;/td&gt;
																&lt;td align=&quot;center&quot; height=&quot;50&quot; valign=&quot;bottom&quot; style=&quot;color:#0b3e4d;font-size:25px;padding-top:10px;border-left:5px solid #f9f9f9;width:66px;background-color:#e9e9e9&quot; class=&quot;text-15 text-title&quot;&gt;{parent_account_active_videos}&lt;/td&gt;
																&lt;td align=&quot;center&quot; height=&quot;50&quot; valign=&quot;bottom&quot; style=&quot;color:#0b3e4d;font-size:25px;padding-top:10px;border-left:5px solid #f9f9f9;width:66px;background-color:#e9e9e9&quot; class=&quot;text-15 text-title&quot;&gt;{parent_account_active_viewed}&lt;/td&gt;
																&lt;td align=&quot;center&quot; height=&quot;50&quot; valign=&quot;bottom&quot; style=&quot;color:#0b3e4d;font-size:25px;padding-top:10px;border-left:5px solid #f9f9f9;width:66px;background-color:#e9e9e9&quot; class=&quot;text-15 text-title&quot;&gt;{total_hours_watched}&lt;/td&gt;
																&lt;td align=&quot;center&quot; height=&quot;50&quot; valign=&quot;bottom&quot; style=&quot;color:#0b3e4d;font-size:25px;padding-top:10px;border-left:5px solid #f9f9f9;width:66px;background-color:#e9e9e9&quot; class=&quot;text-15 text-title&quot;&gt;{parent_account_active_comments}&lt;/td&gt;
															&lt;/tr&gt;
															&lt;tr&gt;
																&lt;td style=&quot;padding:5px 0px&quot;&gt;&amp;nbsp;&lt;/td&gt;
																&lt;td style=&quot;border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#cee3d3&quot;&gt;&amp;nbsp;&lt;/td&gt;
																&lt;td style=&quot;border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#cbe7ea&quot;&gt;&amp;nbsp;&lt;/td&gt;
																&lt;td style=&quot;border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#e9e9e9&quot;&gt;&amp;nbsp;&lt;/td&gt;
																&lt;td style=&quot;border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#e9e9e9&quot;&gt;&amp;nbsp;&lt;/td&gt;
																&lt;td style=&quot;border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#e9e9e9&quot;&gt;&amp;nbsp;&lt;/td&gt;
																&lt;td style=&quot;border-left:5px solid #f9f9f9;width:66px;padding:5px 0px;background-color:#e9e9e9&quot;&gt;&amp;nbsp;&lt;/td&gt;
															&lt;/tr&gt;
                                                                                                                     {account_overview_all_child_rows}

														&lt;/table&gt;
														&lt;/td&gt;
														&lt;/tr&gt;
														&lt;/table&gt;
										&lt;!-- /SECTION 1 --&gt;





										&lt;!-- SECTION 2 --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;background-color:#0c3345;&quot;&gt;
											&lt;tr&gt;
												&lt;td valign=&quot;middle&quot; style=&quot;padding:14px 20px;&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;&quot;&gt;
														&lt;tr&gt;
															&lt;!--&lt;td width=&quot;45&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-user-engagement_1.png&quot; alt=&quot;icon&quot; border=&quot;0&quot; style=&quot;margin:0px;display:block&quot; width=&quot;30&quot;&gt;&lt;/td&gt;--&gt;
															&lt;td style=&quot;padding-right:20px;text-transform:uppercase; color:#ffffff; vertical-align:middle;font-size:25px;&quot; class=&quot;text-15&quot;&gt;User Engagement Growth&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;

										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;center&quot; style=&quot;padding:5px 20px&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;435&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot;&gt;
																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; align=&quot;left&quot; &gt;
																	&lt;tr&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-upload.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-35&quot;&gt;

																									{percent_Increase_in_Videos_Uploaded}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Video Uploads&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-viewed.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-35&quot;&gt;

																									{percent_Increase_in_Videos_Viewed}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Videos Viewed&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot;&gt;
																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; align=&quot;left&quot; &gt;
																	&lt;tr&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-huddles.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-35&quot;&gt;

																									{percent_Increase_in_Huddles_Created}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Huddles in the Account&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-comments.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-35&quot;&gt;

																									{percent_Increase_in_Comments_Added}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Comments Created&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!-- /SECTION 2 --&gt;


										&lt;!-- SECTION 2.5 --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;background-color:#0c3345;&quot;&gt;
											&lt;tr&gt;
												&lt;td valign=&quot;middle&quot; style=&quot;padding:14px 20px;&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17px;color:#ffffff;&quot;&gt;
														&lt;tr&gt;
															&lt;!--&lt;td width=&quot;45&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-users.png&quot; alt=&quot;&quot; width=&quot;30&quot;&gt;&lt;/td&gt;--&gt;
															&lt;td style=&quot;padding-right:20px;text-transform:uppercase; color:#ffffff; vertical-align:middle;font-size:25px;&quot; class=&quot;text-15&quot;&gt;People&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;

&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;center&quot; style=&quot;padding:5px 20px&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;435&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot;&gt;
																&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; align=&quot;left&quot; &gt;
																	&lt;tr&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-user-engagement.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-35&quot;&gt;

																									{users_engagement}&lt;sup style=&quot;line-height:0;font-size:30px; color: #62ad47;&quot; class=&quot;text-20&quot;&gt;%&lt;/sup&gt;

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;User Engagement&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-active-users.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-35&quot;&gt;

																									{getParentTotalUsers_getParentTotalActiveInActiveUsers_percentage}

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Active Users&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																		&lt;td width=&quot;201&quot; height=&quot;180&quot; style=&quot;background-repeat:no-repeat&quot; valign=&quot;middle&quot; align=&quot;center&quot;&gt;

																			&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#575656;text-transform: uppercase;&quot; width=&quot;100%&quot;&gt;
																				&lt;tr&gt;
																					&lt;td align=&quot;center&quot; valign=&quot;middle&quot; style=&quot;padding:0px 10px&quot; height=&quot;180&quot;&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;65&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td valign=&quot;top&quot; align=&quot;center&quot; height=&quot;35&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-total-users.png&quot; alt=&quot;icon&quot; width=&quot;40&quot; border=&quot;0&quot; style=&quot;margin:0px auto;display:block&quot;&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#0c3345&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;left&quot; style=&quot;font-size:50px;line-height:62px&quot; class=&quot;text-35&quot;&gt;

																									{all_total_users}

																									&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																						&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;color:#62ad47&quot; align=&quot;center&quot;&gt;
																							&lt;tr&gt;
																								&lt;td align=&quot;center&quot; style=&quot;padding-top:0px&quot;&gt;&lt;font style=&quot;color:#0c3345&quot;&gt;Total Users&lt;/font&gt;&lt;/td&gt;
																							&lt;/tr&gt;
																						&lt;/table&gt;
																					&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!-- SECTION 2.5 --&gt;

                                                                                {top_engaged_schools_html}

										&lt;!-- SECTION 3 --&gt;


                                                                                {top_engaged_users_html}
										&lt;!-- /SECTION 3 --&gt;
										&lt;br/&gt;
										&lt;!-- SECTION 3 --&gt;


										{top_engaged_super_admins_html}
										&lt;!-- /SECTION 3 --&gt;



									&lt;/td&gt;
								&lt;/tr&gt;
							&lt;/table&gt;
							&lt;!-- /MAIN --&gt;



							&lt;!-- FOOTER --&gt;
							&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;background-color:#ffffff;&quot;&gt;
								&lt;tr&gt;
									&lt;td valign=&quot;middle&quot; style=&quot;padding:35px&quot;&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;424&quot; align=&quot;left&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; class=&quot;fullWidth&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;left&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;27%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;&quot; align=&quot;left&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;left&quot; class=&quot;textCenter&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-logo2.png&quot; alt=&quot;logo&quot; width=&quot;102&quot; border=&quot;0&quot;&gt;&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;

													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;70%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:12px;color:#232323&quot; align=&quot;right&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;right&quot; height=&quot;54&quot; class=&quot;textCenter&quot;&gt;&lt;a href=&quot;http://www.sibme.com/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Website&lt;/a&gt; &amp;bull; &lt;a href=&quot;http://help.sibme.com/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Help Center&lt;/a&gt; &amp;bull; &lt;a href=&quot;http://help.sibme.com/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Support&lt;/a&gt;
                                                                                                                        &amp;bull; &lt;a href=&quot;{{site_base_url}}Users/unsubscribeWeeklyMonthly/{{user_account_unsubscribe}}&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Unsubscribe&lt;/a&gt;&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;

										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;145&quot; align=&quot;right&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; class=&quot;noFloat&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://www.facebook.com/sibmeapp/?ref=br_rs&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-facebook.png&quot; height=&quot;23&quot; alt=&quot;social-facebook&quot;&gt;&lt;/a&gt;&lt;/td&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://twitter.com/SibmeApp&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-twitter.png&quot; height=&quot;23&quot; alt=&quot;social-twitter&quot;&gt;&lt;/a&gt;&lt;/td&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://plus.google.com/+Sibmeapp&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-googleplus.png&quot; height=&quot;23&quot; alt=&quot;social-googleplus&quot;&gt;&lt;/a&gt;&lt;/td&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://www.linkedin.com/company/sibme/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-linkedin.png&quot; height=&quot;23&quot; alt=&quot;social-linkedin&quot;&gt;&lt;/a&gt;&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
									&lt;/td&gt;
								&lt;/tr&gt;
							&lt;/table&gt;
							&lt;!-- /FOOTER --&gt;



						&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr&gt;
						&lt;td&gt;&lt;center&gt;&lt;p style=&quot;font-size: 14px; color: #aaa; text-transform: uppercase;&quot;&gt;&lt;/p&gt;&lt;/center&gt;&lt;/td&gt;
					&lt;/tr&gt;
				&lt;/table&gt;
				&lt;!-- END --&gt;

			&lt;/td&gt;
		&lt;/tr&gt;
	&lt;/table&gt;
	&lt;!-- /WRAPPER --&gt;


&lt;/body&gt;
&lt;/html&gt;";

        return $template_data;
    }

    function weekly_report_template($ac_id = '', $us_id = '', $template_id = '') {

        $template_data = "&lt;!DOCTYPE html&gt;
&lt;html&gt;
	&lt;head&gt;
		&lt;meta charset=&quot;utf-8&quot;&gt;
		&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1.0&quot;&gt;
		&lt;title&gt;SIBME&lt;/title&gt;
		&lt;link href=&quot;https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&quot; rel=&quot;stylesheet&quot;&gt;
	&lt;style type=&quot;text/css&quot;&gt;
				body{
					font-family:roboto,'helvetica neue',helvetica,arial,'sans-serif' !important;
					margin:0;
					padding:0;
				}
			@media only screen and (max-width:480px){
				.fullWidth{
					width:100% !important;
				}

		}	@media only screen and (max-width:480px){
				.halfWidth{
					width:50% !important;
				}

		}	@media only screen and (max-width:480px){
				.textCenter{
					text-align:center !important;
				}

		}	@media only screen and (max-width:480px){
				.noFloat{
					float:none !important;
					margin:0 auto !important;
				}

		}	@media only screen and (max-width:480px){
				.marginBottom10{
					margin-bottom:10px !important;
				}

		}	@media only screen and (max-width:480px){
				.paddingTop10{
					padding-top:10px !important;
				}

		}	@media only screen and (max-width:480px){
				.noPaddingBottom{
					padding-bottom:0px !important;
				}

		}	@media only screen and (max-width:480px){
				.noPaddingTop{
					padding-top:0px !important;
				}

		}	@media only screen and (max-width:480px){
				.displayBlock{
					display:block !important;
					width:100% !important;
				}

		}	@media only screen and (max-width:480px){
				.hide{
					display:none !important;
				}

		}	@media only screen and (max-width:480px){
				.noBorder{
					border-width:0px !important;
				}

		}
	&lt;/style&gt;
&lt;/head&gt;

&lt;body style=&quot;margin:0px; padding:0px; -webkit-text-size-adjust:none; background-color:#f1f1f1;&quot; yahoo=&quot;fix&quot;&gt;
	&lt;!-- WRAPPER --&gt;
	&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;mso-table-rspace:0pt; mso-table-lspace:0pt; border-collapse:collapse;&quot;&gt;
		&lt;tr&gt;
				&lt;td align=&quot;center&quot;&gt;
				&lt;!-- 650w BOX --&gt;
				&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;650&quot; style=&quot;margin-top: 50px; margin-bottom: 50px;&quot; class=&quot;fullWidth&quot;&gt;
					&lt;tr&gt;
						&lt;td bgcolor=&quot;#0e3249&quot;&gt;
							&lt;!-- HEADER SECTION --&gt;
							&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; height=&quot;237px&quot; style=&quot;border-top: 7px solid #6cbd48;  display: block;&quot; background=&quot;https://s3.amazonaws.com/sibme.com/edm_img/bg-weekly-summary.png&quot;&gt;
								&lt;tr&gt;
									&lt;td align=&quot;center&quot; style=&quot;padding-top: 10px; padding-left: 30px; padding-right: 30px;&quot; width=&quot;650&quot;&gt;
										&lt;!-- TITLE SECTION --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt; mso-table-lspace:0pt; border-collapse:collapse; font-family:roboto,'helvetica neue',helvetica,arial,sans-serif; font-weight:400; line-height:21px;&quot; class=&quot;fullWidth&quot; align=&quot;left&quot;&gt;
											&lt;tr&gt;
												&lt;td valign=&quot;top&quot; class=&quot;textCenter&quot;&gt;
													&lt;br&gt;&lt;font style=&quot;color:#fff; font-size: 31px; line-height: 32px; font-weight: 300; &quot;&gt;

													{company_name}

													&lt;/font&gt;&lt;br&gt;
													&lt;font style=&quot;color: #98d5ff; display: block; font-size:19px; line-height: 23px; margin-top: 10px;&quot;&gt;

													Weekly Summary&lt;br&gt;
													{date_range_header}

													&lt;/font&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!-- /TITLE SECTION --&gt;
									&lt;/td&gt;
								&lt;/tr&gt;
								&lt;tr&gt;
									&lt;td valign=&quot;top&quot; align=&quot;right&quot; class=&quot;textCenter&quot; style=&quot;padding-left: 30px; padding-right: 30px;&quot;&gt;
										&lt;!-- LOGO SECTION --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt; mso-table-lspace:0pt; border-collapse:collapse;&quot; class=&quot;fullWidth&quot; align=&quot;right&quot;&gt;
											&lt;tr&gt;
												&lt;td&gt;
													&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-logo.png&quot; alt=&quot;logo&quot; border=&quot;0&quot; style=&quot;display:block; margin:0px auto&quot; width=&quot;100&quot;&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!-- /LOGO SECTION --&gt;
									&lt;/td&gt;
								&lt;/tr&gt;
							&lt;/table&gt;
							&lt;!-- /HEADER SECTION --&gt;
						&lt;/td&gt;
					&lt;/tr&gt;

					&lt;tr&gt;
						&lt;td  bgcolor=&quot;#f9f9f9&quot;&gt;
							&lt;!-- MID SECTION --&gt;
							&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;display: block; background: url(https://s3.amazonaws.com/sibme.com/edm_img/border-rainbow.png) no-repeat bottom center; background-size: 650px 5px;&quot;&gt;
								&lt;tr&gt;
									&lt;td align=&quot;center&quot; style=&quot;padding-top: 10px; padding-left: 30px; padding-right: 30px;&quot;&gt;
										&lt;!-- TITLE SECTION --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;mso-table-rspace:0pt; mso-table-lspace:0pt; border-collapse:collapse;&quot; class=&quot;fullWidth&quot; align=&quot;left&quot;&gt;
											&lt;tr&gt;
												&lt;td mc:edit=&quot;bodytoptext&quot;&gt;
													&lt;font style=&quot;font-size: 22px; line-height: 32px; font-weight: 400; display: block; margin-top: 20px; margin-bottom: 20px; color: #033c4b;&quot;&gt;

                                                                                                        &lt;center&gt;      Hope you had a great week! Here's a summary of what happened in your Sibme account last week.          &lt;/center&gt;

													&lt;/font&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!-- /TITLE SECTION --&gt;
									&lt;/td&gt;
								&lt;/tr&gt;

								&lt;tr&gt;
									&lt;td align=&quot;center&quot; style=&quot;padding-top: 10px; padding-left: 30px; padding-right: 30px;&quot;&gt;
										&lt;!-- STATS SECTION --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
											&lt;tr&gt;
												&lt;td valign=&quot;top&quot; width=&quot;30%&quot; style=&quot;padding-top:10px;padding-bottom:20px&quot; class=&quot;fullWidth displayBlock noBorder&quot; align=&quot;center&quot;&gt;
													&lt;!-- USERS STATS COL --&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot; class=&quot;textCenter&quot;&gt;
																&lt;!--COL TITLE and IMAGE: USERS--&gt;
																&lt;table width=&quot;115&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:28px;color:#033c4b;&quot; class=&quot;noFloat&quot;&gt;
																	&lt;tr&gt;
																		&lt;td valign=&quot;top&quot; height=&quot;67&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-users.png&quot; width=&quot;60&quot; alt=&quot;icon&quot; border=&quot;0&quot; style=&quot;display:block;margin:0 auto;&quot;&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																	&lt;tr&gt;
																		&lt;td style=&quot;padding-bottom:20px;&quot;&gt;&lt;span style=&quot;font-weight:400&quot;&gt;Users&lt;/span&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
																&lt;!--/COL TITLE and IMAGE: USERS--&gt;

																&lt;!--ADDED--&gt;
																&lt;table width=&quot;115&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:13px;font-weight:500;color:#033c4b;&quot; class=&quot;noFloat&quot;&gt;

																	&lt;tr&gt;
																		&lt;td align=&quot;center&quot;&gt;
																			&lt;table width=&quot;115&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;&quot;&gt;
																				&lt;tr&gt;
																					&lt;td style=&quot;padding:1px 10px&quot; mc:edit=&quot;usersadded&quot;&gt;{total_users}&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																	&lt;tr&gt;
																		&lt;td style=&quot;padding:1px 0px&quot;&gt;&lt;span style=&quot;padding:0px 0px 10px 0px;display:inline-block;  &quot;  &gt;TOTAL&lt;/span&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
																&lt;!--/ADDED--&gt;
																&lt;br&gt;
																&lt;!--ACTIVE--&gt;
																&lt;table width=&quot;115&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:13px;font-weight:500;color:#033c4b;&quot; class=&quot;noFloat&quot;&gt;

																	&lt;tr&gt;
																		&lt;td align=&quot;center&quot;&gt;
																			&lt;table width=&quot;115&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;&quot;&gt;
																				&lt;tr&gt;
																					&lt;td style=&quot;padding:1px 10px&quot; mc:edit=&quot;usersactive&quot;&gt;{active_users}&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																	&lt;tr&gt;
																		&lt;td style=&quot;padding:1px 0px&quot;&gt;&lt;span&gt;ACTIVE USERS&lt;/span&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
																&lt;!--/ACTIVE--&gt;
																&lt;br&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
													&lt;!-- /USERS STATS COL --&gt;
												&lt;/td&gt;
												&lt;td valign=&quot;top&quot; width=&quot;40%&quot; style=&quot;padding-top:10px;padding-bottom:20px; border-left: 1px solid #e1e1e1; border-right: 1px solid #e1e1e1;&quot; class=&quot;fullWidth displayBlock noBorder&quot;&gt;
													&lt;!-- VIDEO STATS COL --&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot;&gt;
																&lt;!--COL TITLE and IMAGE: VIDEO--&gt;
																&lt;table width=&quot;115&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:28px;color:#033c4b;&quot;&gt;
																	&lt;tr&gt;
																		&lt;td valign=&quot;top&quot; height=&quot;67&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-video2.png&quot; width=&quot;61&quot; alt=&quot;icon&quot; border=&quot;0&quot; style=&quot;display:block;margin:0 auto;&quot;&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																	&lt;tr&gt;
																		&lt;td style=&quot;padding-bottom:20px;&quot;&gt;&lt;span style=&quot;font-weight:400&quot;&gt;Videos&lt;/span&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
																&lt;!--/COL TITLE and IMAGE: VIDEO--&gt;

																&lt;!--UPLOADED--&gt;
																&lt;table width=&quot;180&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:13px;font-weight:500;color:#033c4b;&quot;&gt;

																	&lt;tr&gt;
																		&lt;td align=&quot;center&quot;&gt;
																			&lt;table width=&quot;70%&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;&quot;&gt;
																				&lt;tr&gt;
																					&lt;td style=&quot;padding:1px 10px&quot; mc:edit=&quot;videoscomments&quot;&gt;{video_uploded}&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																	&lt;tr&gt;
																		&lt;td style=&quot;padding:1px 0px; text-transform: uppercase;&quot;&gt;&lt;span style=&quot;padding:0px 0px 10px 0px;display:inline-block;  &quot;  &gt;Uploaded&lt;/span&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
																&lt;!--/UPLOADED--&gt;
																&lt;br&gt;
																&lt;!--VIDEO COMMENTS ADDED--&gt;
																&lt;table width=&quot;180&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:13px;font-weight:500;color:#033c4b;&quot;&gt;

																	&lt;tr&gt;
																		&lt;td align=&quot;center&quot;&gt;
																			&lt;table width=&quot;70%&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;&quot;&gt;
																				&lt;tr&gt;
																					&lt;td style=&quot;padding:1px 10px&quot; mc:edit=&quot;videoscomments&quot;&gt;{video_viewed}&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																	&lt;tr&gt;
																		&lt;td style=&quot;padding:1px 0px; text-transform: uppercase;&quot;&gt;&lt;span style=&quot;padding:0px 0px 10px 0px;display:inline-block;  &quot;   &gt;Hours Viewed&lt;/span&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
																&lt;!--/VIDEO COMMENTS ADDED--&gt;
																&lt;br&gt;
																&lt;!--VIDEO COMMENTS ADDED--&gt;
																&lt;table width=&quot;180&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:13px;font-weight:500;color:#033c4b;&quot;&gt;

																	&lt;tr&gt;
																		&lt;td align=&quot;center&quot;&gt;
																			&lt;table width=&quot;70%&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;&quot;&gt;
																				&lt;tr&gt;
																					&lt;td style=&quot;padding:1px 10px&quot; mc:edit=&quot;videoscomments&quot;&gt;{video_comments_added}&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																	&lt;tr&gt;
																		&lt;td style=&quot;padding:1px 0px; text-transform: uppercase;&quot;&gt;&lt;span&gt;Comments&lt;/span&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
																&lt;!--/VIDEO COMMENTS ADDED--&gt;
																&lt;br&gt;
															&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;

												&lt;/td&gt;
												&lt;td valign=&quot;top&quot; width=&quot;30%&quot; style=&quot;padding-top:10px;padding-bottom:20px&quot; class=&quot;fullWidth displayBlock&quot;&gt;
													&lt;!-- HUDDLES STATS COL --&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;center&quot; class=&quot;textCenter&quot;&gt;
																&lt;!--COL TITLE and IMAGE: HUDDLES--&gt;
																&lt;table width=&quot;138&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:28px;color:#033c4b;&quot; class=&quot;noFloat&quot;&gt;
																	&lt;tr&gt;
																		&lt;td valign=&quot;top&quot; height=&quot;67&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-huddles.png&quot; width=&quot;60&quot; alt=&quot;icon&quot; border=&quot;0&quot; style=&quot;display:block;margin:0 auto;&quot;&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																	&lt;tr&gt;
																		&lt;td style=&quot;padding-bottom:20px;&quot;&gt;&lt;span style=&quot;font-weight:400&quot;&gt;Huddles&lt;/span&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
																&lt;!--/COL TITLE and IMAGE: HUDDLES--&gt;

																&lt;!--COACHING--&gt;
																&lt;table width=&quot;138&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:13px;font-weight:500;color:#033c4b;&quot; class=&quot;noFloat&quot;&gt;

																	&lt;tr&gt;
																		&lt;td align=&quot;center&quot;&gt;
																			&lt;table width=&quot;115&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:24px;font-weight:600;color:#033c4b;&quot;&gt;
																				&lt;tr&gt;
																					&lt;td style=&quot;padding:1px 10px&quot; mc:edit=&quot;huddlescoaching&quot;&gt;{total_huddles}&lt;/td&gt;
																				&lt;/tr&gt;
																			&lt;/table&gt;
																		&lt;/td&gt;
																	&lt;/tr&gt;
																	&lt;tr&gt;
																		&lt;td style=&quot;padding:1px 0px&quot;&gt;&lt;span&gt;TOTAL&lt;/span&gt;&lt;/td&gt;
																	&lt;/tr&gt;
																&lt;/table&gt;
																&lt;!--/COACHING--&gt;
																&lt;br&gt;

															&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
													&lt;!-- /HUDDLES STATS COL --&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!-- /STATS SECTION --&gt;
										&lt;!-- LOOKING FOR MORE STATS --&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17.5px;font-weight:500;color:#033c4b;&quot; align=&quot;center&quot;&gt;
											&lt;tr&gt;
												&lt;td&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; style=&quot;text-align:center;font-family:roboto,'helvetica neue',helvetica,arial,sans-serif;font-size:17.5px;font-weight:500;color:#033c4b;&quot; align=&quot;center&quot;&gt;
														&lt;tr&gt;
															&lt;td style=&quot;padding-top:20px&quot; class=&quot;displayBlock noPaddingTop&quot; mc:edit=&quot;textbottom&quot;&gt;Looking for more stats? Check out &lt;a href=&quot;{{site_base_url}}analytics&quot; style=&quot;text-decoration: underline; -webkit-text-decoration-color: #00C4FF; text-decoration-color: #00C4FF; color: #033c4b;&quot;&gt;your team's analytics&lt;/a&gt; page.&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
										&lt;!-- /LOOKING FOR MORE STATS --&gt;
										&lt;br&gt;&lt;br&gt;
									&lt;/td&gt;
								&lt;/tr&gt;
							&lt;/table&gt;
							&lt;!-- /MID SECTION --&gt;
						&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr&gt;
						&lt;td&gt;
							&lt;!-- FOOTER --&gt;
							&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;100%&quot; style=&quot;background-color:#ffffff;&quot;&gt;
								&lt;tr&gt;
									&lt;td valign=&quot;middle&quot; style=&quot;padding:35px&quot;&gt;
										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;424&quot; align=&quot;left&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; class=&quot;fullWidth&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;left&quot;&gt;
													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;27%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;&quot; align=&quot;left&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;left&quot; class=&quot;textCenter&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/sibme-logo2.png&quot; alt=&quot;&quot; width=&quot;102&quot; border=&quot;0&quot;&gt;&lt;/td&gt;
														&lt;/tr&gt;
													&lt;/table&gt;

													&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;70%&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse;font-family:Verdana,'sans-serif','DINNext';font-size:12px;color:#232323&quot; align=&quot;right&quot; class=&quot;fullWidth&quot;&gt;
														&lt;tr&gt;
															&lt;td align=&quot;right&quot; height=&quot;54&quot; class=&quot;textCenter&quot;&gt;&lt;a href=&quot;http://www.sibme.com/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Website&lt;/a&gt; &amp;bull; &lt;a href=&quot;http://help.sibme.com/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Help Center&lt;/a&gt; &amp;bull; &lt;a href=&quot;http://help.sibme.com/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Support&lt;/a&gt;

                                                            &amp;bull; &lt;a href=&quot;{{site_base_url}}Users/unsubscribeWeeklyMonthly/{{user_account_unsubscribe}}&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none;color:#232323&quot;&gt;Unsubscribe&lt;/a&gt;&lt;/td&gt;

														&lt;/tr&gt;
													&lt;/table&gt;
												&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;

										&lt;table cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; border=&quot;0&quot; width=&quot;145&quot; align=&quot;right&quot; style=&quot;mso-table-rspace:0pt;mso-table-lspace:0pt;border-collapse:collapse&quot; class=&quot;noFloat&quot;&gt;
											&lt;tr&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://www.facebook.com/sibmeapp/?ref=br_rs&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-facebook.png&quot; height=&quot;23&quot; alt=&quot;social&quot;&gt;&lt;/a&gt;&lt;/td&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://twitter.com/SibmeApp&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-twitter.png&quot; height=&quot;23&quot; alt=&quot;social&quot;&gt;&lt;/a&gt;&lt;/td&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://plus.google.com/+Sibmeapp&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-googleplus.png&quot; height=&quot;23&quot; alt=&quot;social&quot;&gt;&lt;/a&gt;&lt;/td&gt;
												&lt;td align=&quot;right&quot; height=&quot;54&quot;&gt;&lt;a href=&quot;https://www.linkedin.com/company/sibme/&quot; target=&quot;_blank&quot; style=&quot;text-decoration:none; padding-left:5px;&quot;&gt;&lt;img src=&quot;https://s3.amazonaws.com/sibme.com/edm_img/icn-social-linkedin.png&quot; height=&quot;23&quot; alt=&quot;social&quot;&gt;&lt;/a&gt;&lt;/td&gt;
											&lt;/tr&gt;
										&lt;/table&gt;
									&lt;/td&gt;
								&lt;/tr&gt;
							&lt;/table&gt;
							&lt;!-- /FOOTER --&gt;
						&lt;/td&gt;
					&lt;/tr&gt;
					&lt;tr&gt;
						&lt;td&gt;&lt;center&gt;&lt;p style=&quot;font-size: 14px; color: #aaa; text-transform: uppercase;&quot;&gt;&lt;/p&gt;&lt;/center&gt;&lt;/td&gt;
					&lt;/tr&gt;
				&lt;/table&gt;
				&lt;!-- /650w BOX --&gt;
				&lt;/td&gt;
		&lt;/tr&gt;
	&lt;/table&gt;
	&lt;!-- /WRAPPER --&gt;
&lt;/body&gt;
&lt;/html&gt;
";

        return $template_data;
    }

    function test() {
        return true;
    }

}
