<?php

class VideoLibraryController extends AppController {

    var $helpers = array('Custom');
    public $uses = array('User', 'AccountFolder', 'Group', 'AccountFolderMetaData', 'AccountFolderUser', 'AccountFolderGroup', 'UserAccount', 'Document', 'Comment', 'AccountFolderDocument', 'AccountFolderDocumentAttachment', 'AccountFolderSubject', 'AccountFolderTopic', 'Topic', 'Subject', 'Account');

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    public function beforeFilter() {

        $users = $this->Session->read('user_current_account');
        $totalAccounts = $this->Session->read('totalAccounts');
        $this->set('logged_in_user', $users);
        if ($users['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
            $this->redirect_on_account_settings();
        }
        $this->check_is_account_expired();
        $this->is_account_activated($users['users_accounts']['account_id'],$users['User']['id']);
        parent::beforeFilter();
    }

    function index($accountID = '') {
//        $this->auth_permission('library', 'index');
        return $this->redirect('/home/VideoLibrary' , 301);
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('VideoLibrary'); 
        $users = $this->Session->read('user_current_account');
        $credit_card_status = $this->check_card_expiration_failure($users['accounts']['account_id']);
        $this->set('cardExpired',$credit_card_status['cardExpired']);
        $this->set('cardFailure',$credit_card_status['cardFailure']);
        if($credit_card_status['cardFailure'])
        {
            return $this->redirect('/home/account-settings/plans');
        }
        if (!empty($accountID) && $accountID != $users['accounts']['account_id']) {
            $account_id = $accountID;
            $this->set('selected_account', $accountID);
        } else {
            $account_id = $users['accounts']['account_id'];
            $this->set('selected_account', '');
        }
                
        $view = new View($this, false);
       $parent_library_check = $view->Custom->get_show_parent_video_library($users['accounts']['account_id']);
       
        $account_details = $this->Account->find('first', array(
                        'conditions' => array(
                            'id' => $users['accounts']['account_id']
                        )
                    ));
        
        $this->set('video_upload_button',true);
       
       if($parent_library_check && $account_details['Account']['parent_account_id'] != 0)
       {
            $account_id = $account_details['Account']['parent_account_id'];
            $this->set('selected_account', '');
            $this->set('video_upload_button',false);
       }
        

        $account_info = $this->Account->getMyAccounts($account_id);
        $this->set('account_info', $account_info);

        $have_parent = $this->Account->haveParent($users['accounts']['account_id']);
        if ($have_parent) {
            $accounts = $this->Account->getRelatedAccounts($users['accounts']['account_id'], true);
        } else {
            $accounts = $this->Account->getRelatedAccounts($users['accounts']['account_id']);
        }
        $this->set('have_parent', $have_parent);
        $this->set('user_accounts', $accounts);

        $accounts_ids_arr = array();
        if (!empty($accounts)):
            foreach ($accounts as $account):
                array_push($accounts_ids_arr, $account['Account']['id']);
            endforeach;
        endif;
        if (!empty($accountID) && $accountID != $users['accounts']['account_id']) {
            if (!in_array($accountID, $accounts_ids_arr)) {
                $this->no_permissions();
            }
        }
        
        $this->set('account_id',$account_id);
        $this->set('accounts_ids_arr', $accounts_ids_arr);
        $uncat_videos = $this->AccountFolder->getVideoLibraryCount('uncat', $account_id);
        $this->set('uncat_videos', $uncat_videos);

        $total_videos = $this->AccountFolder->getVideoLibraryCount('all', $account_id);
        $this->set('total_videos', $total_videos);

        $rows = 12;
        $get_videos = $this->AccountFolder->getSubjectTopicVideos($account_id, '-1', 'all', 'n', 0, $rows, '-1');
        //unset($get_videos['total_rec']);
        $this->set('get_videos', $get_videos);
        $this->set('rows', $rows);

        $subject_videos = $this->AccountFolder->getVideoLibraryCount('subjects', $account_id);
        $this->set('subject_videos', $subject_videos);

        $unique_subjects = $this->AccountFolder->getVideoLibraryCount('unique_subjects', $account_id);
        $this->set('unique_subjects', $unique_subjects);

        $unique_topics = $this->AccountFolder->getVideoLibraryCount('unique_topics', $account_id);
        $this->set('unique_topics', $unique_topics);

        $subjects = $this->AccountFolder->getSubjects($account_id);
        $this->set('subjects', $subjects);

        $total_topics = $this->AccountFolder->getVideoLibraryCount('topics', $account_id);
        $this->set('total_topics', $total_topics);

        $topics = $this->AccountFolder->getTopics($account_id);
        $this->set('topics', $topics);

        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $this->set('huddle_permission', $huddle_permission);

        $this->set('user_current_account', $users);

        $this->set('user_permissions', $this->Session->read('user_permissions'));
        $this->set('language_based_content',$language_based_content);
    }


    function view($video_id = '', $accountID = '') {
        $document = $this->AccountFolderDocument->find('first',array('conditions' => array('account_folder_id' => $video_id)));
        return $this->redirect('/home/library_video/home/' . $video_id . '/' . $document['AccountFolderDocument']['document_id'] , 301);
        $this->auth_permission('library', 'view');
        $this->layout = "library";
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('VideoLibrary');
        $users = $this->Session->read('user_current_account');
        $credit_card_status = $this->check_card_expiration_failure($users['accounts']['account_id']);
        $this->set('cardExpired',$credit_card_status['cardExpired']);
        $this->set('cardFailure',$credit_card_status['cardFailure']);
        if($credit_card_status['cardFailure'])
        {
            return $this->redirect('/home/account-settings/plans');
        }
        if (!empty($accountID)) {
            $account_id = $accountID;
            $this->set('selected_account', $accountID);
        } else {
            $account_id = $users['accounts']['account_id'];
            $this->set('selected_account', '');
        }
        if (!empty($accountID) && $accountID != $users['accounts']['account_id']) {
            $account_id = $accountID;
            $this->set('can_download_video', $accountID);
        } else {
            $account_id = $users['accounts']['account_id'];
            $this->set('can_download_video', '');
        }
//        $account_id = $users['accounts']['account_id'];
        $have_parent = $this->Account->haveParent($users['accounts']['account_id']);
        if ($have_parent) {
            $accounts = $this->Account->getRelatedAccounts($users['accounts']['account_id'], true);
        } else {
            $accounts = $this->Account->getRelatedAccounts($users['accounts']['account_id']);
        }
        $accounts_ids_arr = array();
        if (!empty($accounts)):
            foreach ($accounts as $account):
                array_push($accounts_ids_arr, $account['Account']['id']);
            endforeach;
        endif;
        if (!empty($accountID) && $accountID != $users['accounts']['account_id']) {
            if (!in_array($users['accounts']['account_id'], $accounts_ids_arr)) {
                $this->no_permissions();
            }
        }
        $user_id = $users['User']['id'];
        $video_library_video = $this->Document->getVideos($video_id);
        if (count($video_library_video) == 0 || $video_library_video == '') {
            $this->Session->setFlash($this->language_based_messages['no_record_found_msg'], 'default', array('class' => 'message success'));
            $this->redirect('/VideoLibrary');
        }
        $videoLibary = $this->AccountFolder->getVideo($video_id, $account_id);
        $subjects = $this->AccountFolderSubject->get($video_id);
        $topics = $this->AccountFolderTopic->get($video_id);
        $tags = $this->AccountFolder->getVideoTags($video_id, $account_id);
        $supportingDoc = $this->Document->getDocuments($video_id);
        $details = $this->AccountFolderDocument->get_doc_details($video_id);

        $videoCount = $this->AccountFolder->getUserVideoCount($account_id, $videoLibary['AccountFolder']['created_by']);

        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $this->set('huddle_permission', $huddle_permission);
        $this->set('user_permissions', $this->Session->read('user_permissions'));

        $this->set('users', $users);
        $this->set('video_id', $video_id);
        $this->set('accounts', $users['accounts']);
        $this->set('library', $videoLibary);
        $this->set('details', $details);
        $this->set('video', isset($video_library_video[0]) ? $video_library_video[0] : '');
        $this->set('subjects', $subjects);
        $this->set('topics', $topics);
        $this->set('tags', $tags);
        $this->set('documents', $supportingDoc);
        $this->set('videoCount', $videoCount);
        $this->set('language_based_content',$language_based_content);
    }


//    function add() {
//
//        $this->auth_permission('library', 'add');
//        if ($this->request->is('post')) {
//            $users = $this->Session->read('user_current_account');
//            $account_id = $users['accounts']['account_id'];
//            $user_id = $users['User']['id'];
//
//            $subjects = explode(',', $this->request->data['subjects']);
//            $topics = explode(',', $this->request->data['topics']);
//            $tags = explode(',', $this->request->data['tags']);
//
//            $this->AccountFolder->create();
//            $data = array(
//                'account_id' => $account_id,
//                'folder_type' => 2,
//                'name' => $this->request->data['video_title'],
//                'desc' => $this->request->data['video_desc'],
//                'active' => 1,
//                'created_date' => date("Y-m-d H:i:s"),
//                'created_by' => $user_id,
//                'last_edit_date' => date("Y-m-d H:i:s"),
//                'last_edit_by' => $user_id,
//            );
//
//        }
    function add() {
        
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('VideoLibrary'); 

        $this->auth_permission('library', 'add');
        if ($this->request->is('post')) {
            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $user_id = $users['User']['id'];

            $subjects = explode(',', $this->request->data['subjects']);
            $topics = explode(',', $this->request->data['topics']);
            $tags = explode(',', $this->request->data['tags']);

            $this->AccountFolder->create();
            $data = array(
                'account_id' => $account_id,
                'folder_type' => 2,
                'name' => $this->request->data['video_title'],
                'desc' => $this->request->data['video_desc'],
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
            );


            if ($this->AccountFolder->save($data, $validation = TRUE)) {

                $account_folder_id = $this->AccountFolder->id;
                $user_activity_logs = array(
                    'ref_id' => $account_folder_id,
                    'desc' => $this->request->data['video_title'],
                    'url' => $this->base . '/videoLibrary/view/' . $account_folder_id,
                    'account_folder_id' => $account_folder_id,
                    'type' => '4'
                );
                $this->user_activity_logs($user_activity_logs);
                $activity_log_id = '';
                $activity_log_id = $this->UserActivityLog->id;

                for ($i = 0; $i < count($tags); $i++) {
                    $tag = $tags[$i];

                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'tag',
                        'meta_data_value' => $tag,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                }

                for ($i = 0; $i < count($subjects); $i++) {
                    $subject = $subjects[$i];

                    $this->AccountFolderSubject->create();
                    $data = array(
                        'subject_id' => $subject,
                        'account_folder_id' => $account_folder_id,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderSubject->save($data, $validation = TRUE);
                }

                for ($i = 0; $i < count($topics); $i++) {
                    $topic = $topics[$i];

                    $this->AccountFolderTopic->create();
                    $data = array(
                        'topic_id' => $topic,
                        'account_folder_id' => $account_folder_id,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderTopic->save($data, $validation = TRUE);
                }
                
                $user_current_account = $this->Session->read('user_current_account');
                $user_accounts = $this->Account->find('all',
                    array('conditions' => array(
                        'parent_account_id' => $account_id
                    )
                    )
                );
                $total_childs               = count($user_accounts);
                
                if($total_childs > 0)
                {
                $account_details = $this->Account->find('first', array('conditions' => array('id' => $account_id)));    
                $data['company_name'] = $account_details['Account']['company_name'];
                $data['account_id'] = $account_id;
                $child_names[0] =$account_details['Account']['company_name'];
                foreach ($user_accounts as $account):
                    
                 $child_names[] =    $account['Account']['company_name'];
                    
                endforeach;
                $data['child_names'] = $child_names;
                $data['video_name'] = $this->request->data['video_title'];
                $data['huddle_id'] = $account_folder_id;  
                $data['email'] = $users['User']['email'];
                $this->uploadVideos($account_folder_id, $account_id, $user_id, true,true,false,$activity_log_id);   
                if ($this->check_subscription($user_id, '7',$account_id)) {                   
                    $this->sendEmailVideoLibrary($data);  
                    }
                    
                }
                else
                {
                $this->uploadVideos($account_folder_id, $account_id, $user_id, true,false,false,$activity_log_id);
                }

                //Child Accounts
                
                $user_role_id               = $user_current_account['roles']['role_id'];
                $video_library_permission   = $user_current_account['users_accounts']['permission_access_video_library'];
                if($total_childs > 0) {
                    foreach ($user_accounts as $account):
                        $c_account_id = $account['Account']['id'];
                        if($user_role_id == 100 || $user_role_id == 110){
                            $this->AccountFolder->create();
                            $data = array(
                                'account_id' => $c_account_id,
                                'folder_type' => 2,
                                'name' => $this->request->data['video_title'],
                                'desc' => $this->request->data['video_desc'],
                                'active' => 1,
                                'created_date' => date("Y-m-d H:i:s"),
                                'created_by' => $user_id,
                                'last_edit_date' => date("Y-m-d H:i:s"),
                                'last_edit_by' => $user_id,
                            );
                            $this->AccountFolder->save($data);
                            $c_account_folder_id = $this->AccountFolder->id;
                            $user_activity_logs = array(
                                'ref_id' => $account_folder_id,
                                'desc' => $this->request->data['video_title'],
                                'url' => $this->base . '/videoLibrary/view/' . $c_account_folder_id,
                                'account_folder_id' => $c_account_folder_id,
                                'type' => '4'
                            );
                           // $this->user_activity_logs($user_activity_logs);

                            for ($i = 0; $i < count($tags); $i++) {
                                $tag = $tags[$i];

                                $c_account_folders_meta_data = array(
                                    'account_folder_id' => $c_account_folder_id,
                                    'meta_data_name' => 'tag',
                                    'meta_data_value' => $tag,
                                    'created_date' => date("Y-m-d H:i:s"),
                                    'last_edit_date' => date("Y-m-d H:i:s"),
                                    'created_by' => $users['User']['id'],
                                    'last_edit_by' => $users['User']['id']
                                );
                                $this->AccountFolderMetaData->create();
                                $this->AccountFolderMetaData->save($c_account_folders_meta_data);
                            }

                            for ($i = 0; $i < count($subjects); $i++) {
                                $subject = $subjects[$i];

                                $this->AccountFolderSubject->create();
                                $data = array(
                                    'subject_id' => $subject,
                                    'account_folder_id' => $c_account_folder_id,
                                    'created_date' => date("Y-m-d H:i:s"),
                                    'last_edit_date' => date("Y-m-d H:i:s"),
                                    'created_by' => $users['User']['id'],
                                    'last_edit_by' => $users['User']['id']
                                );
                                $this->AccountFolderSubject->save($data, $validation = TRUE);
                            }

                            for ($i = 0; $i < count($topics); $i++) {
                                $topic = $topics[$i];

                                $this->AccountFolderTopic->create();
                                $data = array(
                                    'topic_id' => $topic,
                                    'account_folder_id' => $c_account_folder_id,
                                    'created_date' => date("Y-m-d H:i:s"),
                                    'last_edit_date' => date("Y-m-d H:i:s"),
                                    'created_by' => $users['User']['id'],
                                    'last_edit_by' => $users['User']['id']
                                );
                                $this->AccountFolderTopic->save($data, $validation = TRUE);

                                $this->uploadVideos($c_account_folder_id, $c_account_id, $user_id, true,true);
                            }

                        }else if($video_library_permission == 1){

                            $this->AccountFolder->create();
                            $data = array(
                                'account_id' => $c_account_id,
                                'folder_type' => 2,
                                'name' => $this->request->data['video_title'],
                                'desc' => $this->request->data['video_desc'],
                                'active' => 1,
                                'created_date' => date("Y-m-d H:i:s"),
                                'created_by' => $user_id,
                                'last_edit_date' => date("Y-m-d H:i:s"),
                                'last_edit_by' => $user_id,
                            );
                            $this->AccountFolder->save($data);
                            $c_account_folder_id = $this->AccountFolder->id;
                            $user_activity_logs = array(
                                'ref_id' => $account_folder_id,
                                'desc' => $this->request->data['video_title'],
                                'url' => $this->base . '/videoLibrary/view/' . $c_account_folder_id,
                                'account_folder_id' => $c_account_folder_id,
                                'type' => '4'
                            );
                           // $this->user_activity_logs($user_activity_logs);

                            for ($i = 0; $i < count($tags); $i++) {
                                $tag = $tags[$i];

                                $c_account_folders_meta_data = array(
                                    'account_folder_id' => $c_account_folder_id,
                                    'meta_data_name' => 'tag',
                                    'meta_data_value' => $tag,
                                    'created_date' => date("Y-m-d H:i:s"),
                                    'last_edit_date' => date("Y-m-d H:i:s"),
                                    'created_by' => $users['User']['id'],
                                    'last_edit_by' => $users['User']['id']
                                );
                                $this->AccountFolderMetaData->create();
                                $this->AccountFolderMetaData->save($c_account_folders_meta_data);
                            }

                            for ($i = 0; $i < count($subjects); $i++) {
                                $subject = $subjects[$i];

                                $this->AccountFolderSubject->create();
                                $data = array(
                                    'subject_id' => $subject,
                                    'account_folder_id' => $c_account_folder_id,
                                    'created_date' => date("Y-m-d H:i:s"),
                                    'last_edit_date' => date("Y-m-d H:i:s"),
                                    'created_by' => $users['User']['id'],
                                    'last_edit_by' => $users['User']['id']
                                );
                                $this->AccountFolderSubject->save($data, $validation = TRUE);
                            }

                            for ($i = 0; $i < count($topics); $i++) {
                                $topic = $topics[$i];

                                $this->AccountFolderTopic->create();
                                $data = array(
                                    'topic_id' => $topic,
                                    'account_folder_id' => $c_account_folder_id,
                                    'created_date' => date("Y-m-d H:i:s"),
                                    'last_edit_date' => date("Y-m-d H:i:s"),
                                    'created_by' => $users['User']['id'],
                                    'last_edit_by' => $users['User']['id']
                                );
                                $this->AccountFolderTopic->save($data, $validation = TRUE);

                                $this->uploadVideos($c_account_folder_id, $c_account_id, $user_id, true,true);
                            }
                        }
                    endforeach;
                }
                //End


                echo "Video Upload Successfully.";
            } else {
                $video_library_id = 0;
            }

            $this->layout = null;
            $this->set('ajaxdata', $video_library_id . '-' . $s_url);
            $this->render('/Elements/ajax/ajaxreturn');
        } else {

            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $user_id = $users['User']['id'];

            $this->set('account_id', $account_id);
            $this->set('language_based_content',$language_based_content);
            $this->set('subjects', $this->Subject->getSubjects($account_id));
            $this->set('topics', $this->Topic->getTopics($account_id));
            $this->render('add');
        }
    }

    function addSubject() {

        $this->auth_permission('library', 'addSubject');
        $this->layout = null;

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $this->Subject->create();
        $data = array(
            'account_id' => $account_id,
            'name' => $this->request->data['subject'],
            'created_date' => date("Y-m-d H:i:s"),
            'last_edit_date' => date("Y-m-d h:i:s"),
            'created_by' => $users['User']['id'],
            'last_edit_by' => $users['User']['id']
        );

        $this->Subject->save($data, $validation = TRUE);

        $this->set('subjects', $this->Subject->getSubjects($account_id));
        $this->render('/Elements/ajax/subject-check');
    }

    function addTopic() {
        $this->auth_permission('library', 'addTopic');
        $this->layout = null;

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $this->Topic->create();
        $data = array(
            'account_id' => $account_id,
            'name' => $this->request->data['topic'],
            'created_date' => date("Y-m-d H:i:s"),
            'last_edit_date' => date("Y-m-d h:i:s"),
            'created_by' => $users['User']['id'],
            'last_edit_by' => $users['User']['id']
        );

        $this->Topic->save($data, $validation = TRUE);

        $this->set('topics', $this->Topic->getTopics($account_id));
        $this->render('/Elements/ajax/topic-check');
    }

    function getUncatVideos($subject_id = '', $order = '', $start = '', $rows = '', $topic_id = '', $accountID = '') {
        $this->auth_permission('library', 'getSubjectVideos');
        $users = $this->Session->read('user_current_account');
        if (!empty($accountID)) {
            $account_id = $accountID;
            $this->set('selected_account', $accountID);
        } else {
            $account_id = $users['accounts']['account_id'];
            $this->set('selected_account', '');
        }
//        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $result = $this->AccountFolder->getSubjectTopicVideos($account_id, $subject_id, 'uncat', $order, $start, $rows, $topic_id);
        //echo '<pre>';
        //print_r($result);
        //die;
        $response = array(
            'response' => ''
        );
        $total_records = 0;
        $subjectVideos = '';
        if (is_array($result) && count($result) > 0) {

            $total_records = $result['total'];
            //$total_rec = $result['total_rec'];
            $remaining_rows = $result['total'] - $rows;
            //unset($result['total_rec']);
            unset($result['total']);
            $subjectVideos = $result;
        }

        $params = array(
            'user_id' => $account_id,
            'subject_id' => $subject_id,
            'videos' => $subjectVideos
        );

        $view = new View($this, false);
        $html = $view->element('ajax/vids', $params);
        $contents = array(
            'contents' => $html,
            'remaining' => $remaining_rows,
            //'total_rec' => $total_rec,
            'total_count' => $total_records
        );
        echo json_encode($contents);
        exit;
    }

    function getSubjectVideos($subject_id = '', $order = '', $start = '', $rows = '', $topic_id = '', $accountID = '') {
        $this->auth_permission('library', 'getSubjectVideos');
        $users = $this->Session->read('user_current_account');
        if (!empty($accountID)) {
            $account_id = $accountID;
            $this->set('selected_account', $accountID);
        } else {
            $account_id = $users['accounts']['account_id'];
            $this->set('selected_account', '');
        }
//        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $result = $this->AccountFolder->getSubjectTopicVideos($account_id, $subject_id, 'subject', $order, $start, $rows, $topic_id);
        //echo '<pre>';
        //print_r($result);
        //die;
        $response = array(
            'response' => ''
        );
        $total_records = 0;
        $subjectVideos = '';
        if (is_array($result) && count($result) > 0) {

            $total_records = $result['total'];
            //$total_rec = $result['total_rec'];
            $remaining_rows = $result['total'] - $rows;
            //unset($result['total_rec']);
            unset($result['total']);
            $subjectVideos = $result;
        }

        $params = array(
            'user_id' => $account_id,
            'subject_id' => $subject_id,
            'videos' => $subjectVideos
        );

        $view = new View($this, false);
        $html = $view->element('ajax/vids', $params);
        $contents = array(
            'contents' => $html,
            'remaining' => $remaining_rows,
            //'total_rec' => $total_rec,
            'total_count' => $total_records
        );
        echo json_encode($contents);
        exit;
    }

    function getTopicVideos($topic_id = '', $order = '', $start = '', $rows = '', $subject_id = '', $accountID = '') {
        $this->auth_permission('library', 'getTopicVideos');
        $users = $this->Session->read('user_current_account');
        if (!empty($accountID)) {
            $account_id = $accountID;
            $this->set('selected_account', $accountID);
        } else {
            $account_id = $users['accounts']['account_id'];
            $this->set('selected_account', '');
        }
//        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $result = $this->AccountFolder->getSubjectTopicVideos($account_id, $topic_id, 'topic', $order, $start, $rows, $subject_id);
        //echo '<pre>';
        //print_r($result);
        //die;
        $response = array(
            'response' => ''
        );

        $topicVideos = '';
        $total_count = 0;
        if (is_array($result) && count($result) > 0) {
            $remaining_rows = $result['total'] - $rows;
            //$total_rec = $result['total_rec'];
            $total_count = $result['total'];
            unset($result['total']);
            //unset($result['total_rec']);
            $topicVideos = $result;
        }

        $params = array(
            'user_id' => $account_id,
            'topic_id' => $topic_id,
            'videos' => $topicVideos
        );

        $view = new View($this, false);
        $html = $view->element('ajax/vids', $params);
        $contents = array(
            'contents' => $html,
            'remaining' => $remaining_rows,
            //'total_rec' => $total_rec,
            'total_count' => $total_count
        );
        echo json_encode($contents);
        exit;
    }

    function getAllVideos($start = '', $rows = '', $order = 'n', $accountID = '', $vtype = 'all') {
        $this->auth_permission('library', 'getAllVideos');

        $users = $this->Session->read('user_current_account');
        if (!empty($accountID)) {
            $account_id = $accountID;
            $this->set('selected_account', $accountID);
        } else {
            $account_id = $users['accounts']['account_id'];
            $this->set('selected_account', '');
        }
//        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $result = $this->AccountFolder->getSubjectTopicVideos($account_id, -1, $vtype, $order, $start, $rows, -1);
        $response = array(
            'response' => ''
        );


        $allVideos = '';
        $total_count = 0;

        if (is_array($result) && count($result) > 0) {
            $remaining_rows = $result['total'] - $rows;
            $total_count = $result['total'];
            unset($result['total']);
            $allVideos = $result;
        }


        $params = array(
            'user_id' => $account_id,
            'videos' => $allVideos
        );

        $view = new View($this, false);
        $html = $view->element('ajax/vids', $params);
        $contents = array(
            'contents' => $html,
            'remaining' => $remaining_rows,
            'total_count' => $total_count
        );
        echo json_encode($contents);
        exit;
    }

    function getVideosByTitle($vidTitle = '', $order = '', $start = '', $rows = '', $accountID = '') {
        $this->auth_permission('library', 'getVideosByTitle');
        $users = $this->Session->read('user_current_account');
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('VideoLibrary'); 
        if (!empty($accountID)) {
            $account_id = $accountID;
            $this->set('selected_account', $accountID);
        } else {
            $account_id = $users['accounts']['account_id'];
            $this->set('selected_account', '');
        }
//        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $result = $this->AccountFolder->getVideosByTitle($account_id, $vidTitle, $order, $start, $rows);
        $response = array(
            'response' => ''
        );

        $subjectVideos = '';
        $total_count = 0;

        if (is_array($result) && count($result) > 0) {
            $remaining_rows = $result['total'] - $rows;
            $total_count = $result['total'];
            unset($result['total']);
            $subjectVideos = $result;
        }

        $params = array(
            'user_id' => $account_id,
            'vidTitle' => $vidTitle,
            'videos' => $subjectVideos,
            'language_based_content' => $language_based_content
        );

        $view = new View($this, false);
        $html = $view->element('ajax/vids', $params);
        $contents = array(
            'contents' => $html,
            'remaining' => $remaining_rows,
            'total_count' => $total_count
        );
        echo json_encode($contents);
        exit;
    }

    function update_view_count($document_id = '') {
        $this->auth_permission('library', 'update_view_count');
        $this->layout = null;
        $video = $this->Document->find('first', array('conditions' => array('id' => $document_id)));
        $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $document_id)));
        
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        
        $data = array(
            'view_count' => ((int) $video['Document']['view_count'] + 1)
        );
        $this->Document->updateAll($data, array('id' => $document_id), $validation = TRUE);
        
        $this->create_churnzero_event('Library+Video+Views', $account_id, $users['User']['email']);  
        
        
          if (isset($account_id) && isset($user_id)) {

            $user_activity_logs = array(
                'ref_id' => $document_id,
                'desc' => 'Video Viewed',
                'url' => '/' . $document_id,
                'type' => '11',
                'account_id' => $account_id,
                'account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'],
                'environment_type' => 2,
            );
            $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
        }
        
        

        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function delete($video_id = '') {
        $this->auth_permission('library', 'delete');
        $this->AccountFolder->create();
        $data = array(
            'active' => 0
        );
        $this->AccountFolder->updateAll($data, array('account_folder_id' => $video_id), $validation = TRUE);
        $this->Session->setFlash($this->language_based_messages['video_has_been_deleted_successfully_msg'], 'default', array('class' => 'message success'));
        $this->redirect('/VideoLibrary');
    }

    function edit($video_id = '') {
        $this->auth_permission('library', 'edit');
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('VideoLibrary'); 
        $this->layout = "library";

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        if ($this->request->is('post')) {

            $subjects = explode(',', $this->request->data['subjects']);
            $topics = explode(',', $this->request->data['topics']);
            $tags = explode(',', $this->request->data['tags']);

            $library = $this->AccountFolder->getVideo($video_id, $account_id);
            $video = $this->Document->getVideos($video_id);

            $video = $video[0];

            $data = array(
                'name' => "'" . addslashes($this->request->data['video_title']) . "'",
                'desc' => "'" . addslashes($this->request->data['video_desc']) . "'",
                'last_edit_date' => "'" . addslashes(date("Y-m-d H:i:s")) . "'",
                'last_edit_by' => $user_id
            );

            $this->AccountFolder->updateAll($data, array('account_folder_id' => $video_id), $validation = TRUE);

            $this->AccountFolderMetaData->deleteAll(array('account_folder_id' => $video_id, 'meta_data_name' => 'tag'), $cascade = true, $callbacks = true);
            for ($i = 0; $i < count($tags); $i++) {
                $tag = $tags[$i];

                $account_folders_meta_data = array(
                    'account_folder_id' => $video_id,
                    'meta_data_name' => 'tag',
                    'meta_data_value' => $tag,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d h:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);
            }

            $this->AccountFolderSubject->deleteAll(array('account_folder_id' => $video_id), $cascade = true, $callbacks = true);
            for ($i = 0; $i < count($subjects); $i++) {
                $subject = $subjects[$i];

                $this->AccountFolderSubject->create();
                $data = array(
                    'subject_id' => $subject,
                    'account_folder_id' => $video_id,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d h:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderSubject->save($data, $validation = TRUE);
            }

            $this->AccountFolderTopic->deleteAll(array('account_folder_id' => $video_id), $cascade = true, $callbacks = true);
            for ($i = 0; $i < count($topics); $i++) {
                $topic = $topics[$i];

                $this->AccountFolderTopic->create();
                $data = array(
                    'topic_id' => $topic,
                    'account_folder_id' => $video_id,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d h:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderTopic->save($data, $validation = TRUE);
            }

            if ($video['Document']['file_size'] != $this->request->data['video_file_size'] || $video['Document']['url'] != $this->request->data['video_url']) {

                require('src/services.php');
                //uploaded video

                $path_parts = pathinfo($this->request->data['video_url']);
                $video_file_name = $path_parts['filename'] . "." . $path_parts['extension'];

                $s3_dest_folder_path_only = "$account_id/$video_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                $s3_src_folder_path = $this->request->data['video_url'];

                //echo $uploaded_video_file; die;
                $s3_service = new S3Services(
                        Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                );

                $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);
                $s3_zencoder_id = $s3_service->encode_videos($s_url, "uploads/" . $s3_dest_folder_path_only, "uploads/" . $s3_dest_folder_path);


                $this->Document->create();
                $this->Document->updateAll(
                        array(
                    'zencoder_output_id' => $s3_zencoder_id,
                    'url' => "'$s3_dest_folder_path'",
                    'original_file_name' => "'" . $this->request->data['video_file_name'] . "'",
                    'last_edit_date' => "'" . date("Y-m-d h:i:s") . "'",
                    'last_edit_by' => $user_id,
                    'file_size' => $this->request->data['video_file_size']
                        ), array('id' => $video['Document']['id'])
                );

                $this->AccountFolderDocument->create();
                $data = array(
                    'title' => $this->request->data['video_title'],
                    'desc' => $this->request->data['video_desc']
                );
                $this->AccountFolderDocument->updateAll($data, array('account_folder_id' => $video_id, 'document_id' => $video['Document']['id']));
            }
        } else {


            $view = new View($this, FALSE);

            $subjects = $this->AccountFolderSubject->get($video_id);
            $topics = $this->AccountFolderTopic->get($video_id);
            $tags = $this->AccountFolder->getVideoTags($video_id, $account_id);
            $video_library_video = $this->Document->getVideos($video_id);

            $this->set('library', $this->AccountFolder->getVideo($video_id, $account_id));
            $this->set('video', $video_library_video);
            $this->set('accounts', $users['accounts']);
            $this->set('sel_subjects', $subjects);
            $this->set('sel_topics', $topics);
            $this->set('subjects', $this->Subject->getSubjects($account_id));
            $this->set('topics', $this->Topic->getTopics($account_id));
            $this->set('tags', $tags);
            $this->set('language_based_content', $language_based_content);
            $this->render('edit');
        }
    }

    function updateSubject($subject_id) {

        $this->layout = null;

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $this->Subject->create();
        $data = array(
            'name' => "'" . addslashes($this->request->data['subject']) . "'",
            'last_edit_date' => "'" . date("Y-m-d h:i:s") . "'",
            'last_edit_by' => $users['User']['id']
        );
        $this->Subject->updateAll($data, array('id' => $subject_id, 'account_id', $account_id), $validation = TRUE);

        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function deleteSubject($subject_id) {

        $this->layout = null;

        $this->AccountFolderSubject->deleteAll(array('subject_id' => $subject_id), $cascade = true, $callbacks = true);
        $this->Subject->deleteAll(array('id' => $subject_id), $cascade = true, $callbacks = true);

        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function updateTopic($topic_id) {

        $this->layout = null;

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $this->Topic->create();
        $data = array(
            'name' => "'" . addslashes($this->request->data['topic']) . "'",
            'last_edit_date' => "'" . date("Y-m-d h:i:s") . "'",
            'last_edit_by' => $users['User']['id']
        );
        $this->Topic->updateAll($data, array('id' => $topic_id, 'account_id', $account_id), $validation = TRUE);

        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function deleteTopic($topic_id) {

        $this->layout = null;

        $this->AccountFolderTopic->deleteAll(array('topic_id' => $topic_id), $cascade = true, $callbacks = true);
        $this->Topic->deleteAll(array('id' => $topic_id), $cascade = true, $callbacks = true);

        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function deleteVideoDocument($document_id, $account_folder_id) {

        $account_folder_documents = $this->AccountFolderDocument->find('first', array('conditions' => array(
                'account_folder_id' => $account_folder_id,
                'document_id' => $document_id
            )
        ));

        $this->AccountFolderDocumentAttachment->deleteAll(array(
            'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id']
        ));

        $this->AccountFolderDocument->deleteAll(array(
            'account_folder_id' => $account_folder_id,
            'document_id' => $document_id
        ));

        $this->Document->deleteAll(array(
            'id' => $document_id
        ));

        $this->layout = null;
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }
    
    function session_values_put() {
        
        
        $this->Session->write('key_lib',$this->request->data['key_lib']);
        $this->Session->write('filename_lib',$this->request->data['filename_lib']);
        $this->Session->write('mimetype_lib',$this->request->data['mimetype_lib']);
        $this->Session->write('size_lib',$this->request->data['size_lib']);
        $result = array(
            'success' => true
        );
        echo json_encode($result);
        exit;
        
        
    }

}
