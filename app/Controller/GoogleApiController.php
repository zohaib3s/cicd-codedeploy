<?php

App::import('Vendor', 'Google/autoload');

class GoogleApiController extends AppController {

    public $uses = array('User');
    public $client_id = '';
    public $client_secret = '';
    public $redirect_uri = '';
    public $client = '';
    public $service = '';

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $sibme_base_url = Configure::read('sibme_base_url');
        if (empty($sibme_base_url)) {
            if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
                $redirect = 'https://' . $_SERVER['HTTP_HOST'] . "/";
            } else {
                $redirect = 'http://' . $_SERVER['HTTP_HOST'] . "/";
            }
            $sibme_base_url = $redirect;
        }
        // var_dump(IS_PROD);
        // var_dump($redirect);
        // die;

        if (IS_PROD==false) {
            $this->client_id = '316492945123-p5h575ik19evebh6pcv68lg020r5mmas.apps.googleusercontent.com';
            $this->client_secret = 'dYpyH5CRe5xShiaqjKptXsH9';
        } else {
            //$this->client_id = '799315406812-0m5coft9u3b5v720ing80adltd6q6f9a.apps.googleusercontent.com';
            $this->client_id = '316492945123-p5h575ik19evebh6pcv68lg020r5mmas.apps.googleusercontent.com';
            //$this->client_secret = '316uY6iIFWy8uSARM3lBjRhh';
            $this->client_secret = 'dYpyH5CRe5xShiaqjKptXsH9';
        }
        $this->client = new Google_Client();
        $this->client->setClientId($this->client_id);
        $this->client->setClientSecret($this->client_secret);
        $this->redirect_uri = $sibme_base_url . 'GoogleApi/google_endpoint/';
        $this->client->setRedirectUri($this->redirect_uri);
        $this->client->addScope("email");
        $this->client->addScope("profile");
        $this->client->setAccessType('offline');
        $this->client->setApprovalPrompt('force');
        $this->service = new Google_Service_Oauth2($this->client);
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('google_login', 'google_endpoint'); // Letting users register themselves
    }

    function google_login() {
        $authUrl = $this->client->createAuthUrl();
        return $this->redirect($authUrl);
    }

    function google_endpoint() {
        if (isset($_GET['code']) && $_GET['code'] != '') {
            $this->client->authenticate($_GET['code']);
            $this->Session->write('access_token', $this->client->getAccessToken());



            $this->redirect(filter_var($this->redirect_uri, FILTER_SANITIZE_URL));
        } elseif (isset($_GET['error']) && $_GET['error'] == 'access_denied') {
            $this->Session->delete('access_token');
            $this->Session->delete('last_logged_in_user');
            $this->Session->delete('access_token');
            return $this->redirect('/home/login?error=Access Denied, Please try again.');
        }

        $access_token = $this->Session->read('access_token');

        if (isset($access_token) && $access_token != '') {
            $this->client->setAccessToken($this->Session->read('access_token'));
            $user = $this->service->userinfo->get();

            if ($user && $user->verifiedEmail == 1) {
                $totalac = $this->UserDeviceLog->query("select count(*) as totalaccount from users where email = '". $user->email."' and is_active <> 0");

                if ($totalac[0][0]['totalaccount'] == 2) {
                    $result =$this->UserDeviceLog->query("select * from users where email = '". $user->email."' and is_active <> 0 and site_id = ".$this->site_id);
                }else{
                    $result = $this->UserDeviceLog->query("select * from users where email = '". $user->email."' and is_active <> 0 and site_id = 1");
                    if (empty($result)) {
                        $result =$this->UserDeviceLog->query("select * from users where email = '". $user->email."' and is_active <> 0 and site_id = 2");
                    }
                }
                if($this->site_id == 2 && $result[0]['users']['site_id']==1){
                        $this->Session->delete('last_logged_in_user');
                        $this->Session->delete('access_token');
                        return $this->redirect('/home/login?error=Invalid email or password, try again.');
                }

                //$this->User->find('first', array('conditions' => array('email' => $user->email, 'is_active' <> 0, 'site_id' => $this->site_id)));
                $result['User'] = $result[0]['users'];
                if (!empty($result['User'])) {
                    $resp = $this->redirect_to_hmh($result['User']['username'], $result['User']['email'], $result['User']['password']);
                    if (!$resp) {
                        $this->Session->delete('last_logged_in_user');
                        $this->Session->delete('access_token');
                        return $this->redirect('/home/login?error=Invalid email or password, try again.');
                    }
                }elseif($this->site_id == 2 && !empty($result['User'])){
                    $resp = $this->redirect_to_hmh($result['User']['username'], $result['User']['email'], $result['User']['password']);
                    if (!$resp) {
                        $this->Session->delete('last_logged_in_user');
                        $this->Session->delete('access_token');
                        return $this->redirect('/home/login?error=Invalid email or password, try again.');
                    }
                }else{
                     $this->Session->delete('last_logged_in_user');
                        $this->Session->delete('access_token');
                        return $this->redirect('/home/login?error=Invalid email or password, try again.');
                }
                if ($result) {
                    if ($this->Auth->login($result['User'])) {
                        $this->Cookie->delete('remember_me_cookie');
                        $this->setupPostLogin(true);
                        $redirect_url = $this->Auth->redirect();
                        return $this->redirect($redirect_url);
                    } else {
                        $this->Session->delete('last_logged_in_user');
                        $this->Session->delete('access_token');
                        return $this->redirect('/home/login?error=Invalid email or password, try again.');
                    }
                } else {
                    $this->Session->write('access_token', $this->client->getAccessToken());
                    $this->Session->delete('last_logged_in_user');
                    $this->Session->delete('access_token');
                    return $this->redirect('/home/login?error=User does not exist. Please use different login credentials.');
                }
            } else {
                $this->Session->delete('last_logged_in_user');
                $this->Session->delete('access_token');
                return $this->redirect('/home/login?error=Sorry your email address has been not verified, please try with correct email.');
            }
        } else {
            $this->Session->delete('last_logged_in_user');
            $this->Session->delete('access_token');
            return $this->redirect('/home/login?error=Invalid email or password, try again.');
        }
    }

    public function redirect_to_hmh($username, $email, $password){
        if($this->site_id == 1){
        $hmhdata = $this->UserDeviceLog->query("select * from users where (email = '".$email."' or username = '".$email."') and password = '".$password."' and site_id = 1");
            if (empty($hmhdata)) {
                $hmhdata = $this->UserDeviceLog->query("select * from users where (email = '".$email."' or username = '".$email."') and password = '".$password."' and site_id = 2");
            }
        }
        if($this->site_id == 2){
        $hmhdata = $this->UserDeviceLog->query("select * from users where (email = '".$email."' or username = '".$email."') and password = '".$password."' and site_id = 2");
            if (empty($hmhdata)) {
                $hmhdata = $this->UserDeviceLog->query("select * from users where (email = '".$email."' or username = '".$email."') and password = '".$password."' and site_id = 2");
            }
        }
            if (sizeof($hmhdata) > 0) {
                $sitesdata = $this->UserDeviceLog->query("select * from sites where site_id = ".$hmhdata[0]['users']['site_id']);
                $user_accounts =$this->UserDeviceLog->query("SELECT * FROM users_accounts WHERE user_id = ".$hmhdata[0]['users']['id']." AND is_active = 1 AND site_id =".$hmhdata[0]['users']['site_id']);
                    if (count($user_accounts) > 1) {
                        $this->redirect('https://'.$sitesdata[0]['sites']['site_url'].'/Users/activate_user/'.$hmhdata[0]['users']['authentication_token'].'?stoh=1');
                    }else{
                        $this->redirect('https://'.$sitesdata[0]['sites']['site_url'].'/Users/activate_user/'.$hmhdata[0]['users']['authentication_token']);
                    }
            }else{
                return false;
            }
    }

}
