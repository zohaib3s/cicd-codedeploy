<?php

App::uses('AppController', 'Controller');
App::uses('Subscription', 'Braintree');
App::uses('Sanitize', 'Utility');

class CustomerController extends AppController {

    public $name = 'Customer';
    public $uses = array('User', 'UserAccount', 'Account', 'Plans');

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    public function beforeFilter() {

        $users = $this->Session->read('user_current_account');
        $this->set('logged_in_user', $users);

        parent::beforeFilter();
    }

    function updateBillingInfo($account_id, $tab) {
         $subscription_data = $this->Session->read('subscription_data');
        if ($subscription_data == '') {
            $this->Session->setFlash($this->language_based_messages['please_select_your_plan_before_billing'], 'default', array('class' => 'message error'));
            $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '4');
        }
        $sampleHuddles = $this->AccountFolder->find('all', array(
            'conditions' => array(
                'active' => 1,
                'is_sample' => 1,
                'account_id' => $account_id,
                'folder_type' => 1
            )
        ));
        if (empty($sampleHuddles)) {
            $this->set('sample_huddle_not_exists', true);
        } else {
            $this->set('sample_huddle_not_exists', false);
        }
        $this->set('new_plan', $this->Session->read('subscription_data'));
        $this->set('customer_info', $this->_get_card_informaiton($account_id));
        $this->set('account_id', $account_id);
        $this->set('tab', $tab);
        $this->render('update_billing_info');
    }

    function create_subscription($account_id, $tab) {
        $total_users = $this->User->getTotalUsers($account_id);
        $result = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        if($this->data['quantity'] < $total_users && $result['Account']['in_trial'] == 1  )
        {
         $this->Session->setFlash($this->language_based_messages['you_cannot_purchase_number_of_licences_less_than_your_current_number_of_users'], 'default', array('class' => 'message error'));
         $this->redirect('/accounts/account_settings_all/' . $account_id . "/4");   
        }
        $data = array(
            'is_active' => TRUE,
            'suspended_at' => NULL,
            'in_trial' => '0',
            'is_suspended' => '0',
            'plan_id' => $this->data['plan_id'],
            'updated_at' => "'" . date('Y-m-d h:i:s') . "'",
            'monthly' => $this->data['monthly_yearly'],
            'quantity' => $this->data['quantity']
        );
        $this->Session->write('subscription_data', $data);
        //$this->redirect('/customer/updateBillingInfo/' . $account_id . "/4");
        $this->redirect('/accounts/account_settings_all/' . $account_id . "/5");
    }
    
    function clear_cart($account_id) {
       
        $this->Session->write('subscription_data', '');
        //$this->redirect('/customer/updateBillingInfo/' . $account_id . "/4");
        $this->redirect('/accounts/account_settings_all/' . $account_id . "/5");
    }

    function genrateSubscription($account_id, $tab) {
        $subscription = new Subscription();
        if ($this->request->is('post')) {
            $plan_selected = $this->Session->read('subscription_data');
            if (empty($plan_selected) && $this->data['check_out_type'] != 'update' ) {
            $this->Session->setFlash($this->language_based_messages['please_select_your_plan_before_billing'], 'default', array('class' => 'message error'));
            $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '4');
             }
              $today_date = date('Y-m-d');
                
                $subscription_data_table =  $this->AccountBraintreeSubscription->find("first",array(
          "conditions" => array(
                    "account_id" => $account_id,
                    "Subscription_start_date <=" => $today_date,
                    "Subscription_end_date >=" => $today_date
              
            ),
          "order" => 'id DESC'
          ));
            
            
            if ($this->data['check_out_type'] == 'old') {
                $account = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
                $plan_info = $this->Plans->getSelectedPlan($this->data['planId']);
                $braintree_subscription_id = $account['Account']['braintree_subscription_id'];
                $customer_id = $account['Account']['braintree_customer_id'];
                
              
               $credit_card_number = $this->data['credit_card']['number'];
               $expiration_date = $this->data['card_expiration'];
               $cvv = $this->data['credit_card']['cvv'];
                    if($credit_card_number != '' && $expiration_date != '' && $cvv != ''  )
                    {
                        $subscription_data = array(
                        'firstName' => $this->data['first_name'],
                        'lastName' => $this->data['last_name'],
                        'company' => $this->data['company'],
                        'email' => $this->data['email'],
                        'creditCard' => array(
                            'number' => $this->data['credit_card']['number'],
                          // 'expirationDate' => $this->data['card_expiration_month'] . "/" . $this->data['card_expiration_year'],
                            'expirationDate' => $this->data['card_expiration'],
                            'cvv' => $this->data['credit_card']['cvv'],
                              'billingAddress' => array( 
                            'locality' => $this->data['city'],
                            'postalCode' => $this->data['zip_code'],
                            'region' => $this->data['state'] ,
                            'streetAddress' => $this->data['address']   
                               )
                        )
                    );
                        
                            $plans = $this->Plans->find("first", array("conditions" => array(
                                        "id" => $account['Account']['plan_id'],

                                )));

                        if(!empty($plans))
                        {
                        $yearly_bool = $plans['Plans']['per_year'];
                       
                        
                        }
                        
                        if(empty($subscription_data_table))
                        {
                            $customer = array(
                        'firstName' => $this->data['first_name'],
                        'lastName' => $this->data['last_name'],
                        'company' => $this->data['company_name'],
                        'email' => $this->data['email'],
                        'creditCard' => array(
                            'number' => $this->data['credit_card']['number'],
                          // 'expirationDate' => $this->data['card_expiration_month'] . "/" . $this->data['card_expiration_year'],
                            'expirationDate' => $this->data['card_expiration'],
                            'cvv' => $this->data['credit_card']['cvv'],
                            'billingAddress' => array( 
                            'locality' => $this->data['city'],
                            'postalCode' => $this->data['zip_code'],
                            'region' => $this->data['state'] ,
                            'streetAddress' => $this->data['address']   
                               )
                        )
                    );
                            if($customer_id != NULL)
                            {
                                $result = $subscription->create_subscription_with_existing_customer($this->data['planId'], $customer,$this->data['plan_price'],$plan_selected['monthly'],$braintree_subscription_id,$customer_id);
                            }
                            else{
                            
                            $result = $subscription->create_subscription($this->data['planId'], $customer,$this->data['plan_price'],$plan_selected['monthly'],$braintree_subscription_id);
                            }
                        }
                        
                        else{
                        if($yearly_bool)
                        {
                           $result = $subscription->update_customer_subscription($this->data['planId'], $subscription_data,$this->data['customer_id'],$braintree_subscription_id,$this->data['plan_price'],$plan_selected['monthly'],$yearly_bool,$plan_selected['quantity'],$account['Account']['plan_id'],$account['Account']['plan_qty'],$plans['Plans']['price_yearly']*12*$account['Account']['plan_qty'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_start_date'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_end_date']); 
                        }
                        else{
                            $result = $subscription->update_customer_subscription($this->data['planId'], $subscription_data,$this->data['customer_id'],$braintree_subscription_id,$this->data['plan_price'],$plan_selected['monthly'],$yearly_bool,$plan_selected['quantity'],$account['Account']['plan_id'],$account['Account']['plan_qty'],$plans['Plans']['price']*$account['Account']['plan_qty'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_start_date'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_end_date']);
                        }
                        
                        }
                         
                        
                    }
                    else{
                    $subscription_data = array(
                     'firstName' => $this->data['first_name'],
                        'lastName' => $this->data['last_name'],
                        'company' => $this->data['company'],
                        'email' => $this->data['email'],   
                    'paymentMethodToken' => $this->data['token'],
                        'customer_id' => $this->data['customer_id'],
                        'price' => $this->data['plan_price']
//                    'price' => $plan_info['Plans']['price_yearly']*12,
//                    'planId' => $this->data['planId'],
    
                );
        
                        $plans = $this->Plans->find("first", array("conditions" => array(
                                        "id" => $account['Account']['plan_id'],

                                )));

                        if(!empty($plans))
                        {
                        $yearly_bool = $plans['Plans']['per_year'];
                       
                        }
                        
                        if(empty($subscription_data_table))
                        {
                            $customer = array(
                        'firstName' => $this->data['first_name'],
                        'lastName' => $this->data['last_name'],
                        'company' => $this->data['company_name'],
                        'email' => $this->data['email'],
                        'creditCard' => array(
                            'number' => $this->data['credit_card']['number'],
                          // 'expirationDate' => $this->data['card_expiration_month'] . "/" . $this->data['card_expiration_year'],
                            'expirationDate' => $this->data['card_expiration'],
                            'cvv' => $this->data['credit_card']['cvv'],
                            'billingAddress' => array( 
                            'locality' => $this->data['city'],
                            'postalCode' => $this->data['zip_code'],
                            'region' => $this->data['state'] ,
                            'streetAddress' => $this->data['address']   
                               )
                        )
                    );
                            if($customer_id != NULL)
                            {
                                $result = $subscription->create_subscription_with_existing_customer($this->data['planId'], $customer,$this->data['plan_price'],$plan_selected['monthly'],$braintree_subscription_id,$customer_id);
                            }
                            else{
                            $result = $subscription->create_subscription($this->data['planId'], $customer,$this->data['plan_price'],$plan_selected['monthly'],$braintree_subscription_id);
                            }
                        }
                        
                        else{
                
                        if($yearly_bool)
                        {
                           $result = $subscription->edit_plane($braintree_subscription_id, $subscription_data,$this->data['planId'],$plan_selected['monthly'],$yearly_bool,$plan_selected['quantity'],$account['Account']['plan_id'],$account['Account']['plan_qty'],$plans['Plans']['price_yearly']*12*$account['Account']['plan_qty'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_start_date'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_end_date']); 
                        }
                        else{
                            $result = $subscription->edit_plane($braintree_subscription_id, $subscription_data,$this->data['planId'],$plan_selected['monthly'],$yearly_bool,$plan_selected['quantity'],$account['Account']['plan_id'],$account['Account']['plan_qty'],$plans['Plans']['price']*$account['Account']['plan_qty'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_start_date'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_end_date']);
                        }
                        
                        }
                
                    }
                    
                    if(isset($result->message))
                    {
                     $this->Session->setFlash($result->message, 'default', array('class' => 'message error'));
                    $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                        
                    }
                    
                    
                    
                if (is_array($result) && $result['success']) {
//                    $plane_data = array(
//                        'is_active' => TRUE,
//                        'suspended_at' => NULL,
//                        'in_trial' => '0',
//                        'is_suspended' => '0',
//                        'plan_id' => $this->data['planId'],
//                        'updated_at' => "'" . date('Y-m-d h:i:s') . "'",
//                    );
                    
                     $plane_data = array(
                            'braintree_customer_id' => "'" . $result['braintree_customer_id'] . "'",
                            'braintree_subscription_id' => "'" . $result['braintree_subscription_id'] . "'",
                            'updated_at' => "'" . date('Y-m-d H:i:s') . "'",
                            'is_active' => TRUE,
                            'suspended_at' => NULL,
                            'in_trial' => '0',
                            'is_suspended' => '0',
                            'plan_id' => $result['braintree_plan_id'],
                            'plan_qty' => $this->data['plan_quantity']
                        );
                     
                     $subscription_data = array(
                            
                            'braintree_subscription_id' => Sanitize::escape(htmlspecialchars($result['braintree_subscription_id'])),
                            'Subscription_start_date' => $result['start_date'] ,
                             'Subscription_end_date' => $result['end_date'] ,
                            'number_of_users' => $this->data['plan_quantity'],
                            'plan_id' => $result['braintree_plan_id'],
                            'subscription_type' => $result['subscription_type'],
                            'payment_status' => (empty($result['payment_status']))? 'settled' : $result['payment_status'],
                            'Subscription_Status' => $result['subscription_status'],
                            'account_id' => $account_id
                        );
                     
                     
                     $subscription_data_update = array(
                            
                            'braintree_subscription_id' => "'" . Sanitize::escape(htmlspecialchars($result['braintree_subscription_id']))  . "'",
                            'number_of_users' => $this->data['plan_quantity'],
                            'plan_id' => $result['braintree_plan_id'],
                            'subscription_type' => $result['subscription_type'],
                            'payment_status' => (empty($result['payment_status']))? "'settled'" : "'".$result['payment_status']."'",
                            'Subscription_Status' => "'".$result['subscription_status']. "'",
                            'account_id' => $account_id
                        );
                    
                    
                }
                if($result['db_check'] == 'update')
                {
                    $this->AccountBraintreeSubscription->updateAll($subscription_data_update,array('braintree_subscription_id' => $subscription_data_table['AccountBraintreeSubscription']['braintree_subscription_id'] ));
                    $this->AccountBraintreeSubscription->updateAll($subscription_data_update,array('braintree_subscription_id' => $result['braintree_subscription_id'] ));
                }
                
                else{
                $this->AccountBraintreeSubscription->create();
                $this->AccountBraintreeSubscription->save($subscription_data, $validation = TRUE);
                }
                $this->Account->updateAll($plane_data, array('id' => $account_id));
                if ($this->Account->getAffectedRows() > 0) {
                    if ($this->data['planId'] == 1 || $this->data['planId'] == 5) {
                        $this->team_plan();
                    } elseif ($this->data['planId'] == 2 || $this->data['planId'] == 6) {
                        $this->deprartment_plan();
                    } elseif ($this->data['planId'] == 3 || $this->data['planId'] == 7) {
                        $this->school_plan_account();
                    } elseif ($this->data['planId'] == 4 || $this->data['planId'] == 8) {
                        $this->school_plus_plan();
                    }
                    $this->Session->write('subscription_data', '');
                    $this->Session->setFlash($this->language_based_messages['you_have_successfully_changed_your_information'], 'default', array('class' => 'message success'));
                    $this->setupPostLogin(true);
                    $this->account_meta_data_changes($account_id);
                    $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                } else {
                    $this->Session->setFlash($this->language_based_messages['your_inforamtion_changing_failed_please_try_agian'], 'default', array('class' => 'message error'));
                    $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                }
            } 
            elseif($this->data['check_out_type'] == 'update'){
                
                $account = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
                
                $braintree_subscription_id = $account['Account']['braintree_subscription_id'];
               $credit_card_number = $this->data['credit_card']['number'];
               $expiration_date = $this->data['card_expiration'];
               $cvv = $this->data['credit_card']['cvv'];
                    if($credit_card_number != '' && $expiration_date != '' && $cvv != ''  )
                    {
                        $subscription_data = array(
                        'firstName' => $this->data['first_name'],
                        'lastName' => $this->data['last_name'],
                        'company' => $this->data['company'],
                        'email' => $this->data['email'],
                        'creditCard' => array(
                            'number' => $this->data['credit_card']['number'],
                          // 'expirationDate' => $this->data['card_expiration_month'] . "/" . $this->data['card_expiration_year'],
                            'expirationDate' => $this->data['card_expiration'],
                            'cvv' => $this->data['credit_card']['cvv'],
                              'billingAddress' => array( 
                            'locality' => $this->data['city'],
                            'postalCode' => $this->data['zip_code'],
                            'region' => $this->data['state'] ,
                            'streetAddress' => $this->data['address']   
                               ),
                             'options'=>array(
                           'updateExistingToken' => $this->data['token']
                       )
                        ),
                      
                    );
                         $result = $subscription->update_customer($subscription_data,$this->data['customer_id'],$braintree_subscription_id);
                        
                    }
                    
                    if(isset($result->message))
                    {
                     $this->Session->setFlash($result->message, 'default', array('class' => 'message error'));
                    $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                        
                    }

                if (is_array($result) && $result['success']) {
//                    $plane_data = array(
//                        'is_active' => TRUE,
//                        'suspended_at' => NULL,
//                        'in_trial' => '0',
//                        'is_suspended' => '0',
//                        'plan_id' => $this->data['planId'],
//                        'updated_at' => "'" . date('Y-m-d h:i:s') . "'",
//                    );
                    
                     $plane_data = array(
                            'braintree_customer_id' => "'" . $result['braintree_customer_id'] . "'",
                            'updated_at' => "'" . date('Y-m-d H:i:s') . "'",
                            'is_active' => TRUE,
                            'suspended_at' => NULL,
                            'in_trial' => '0',
                            'is_suspended' => '0',
                        );
                    
                    
                }

                $this->Account->updateAll($plane_data, array('id' => $account_id));
                if ($this->Account->getAffectedRows() > 0) {
                    
                    $this->Session->write('subscription_data', '');
                    $this->Session->setFlash($this->language_based_messages['you_have_successfully_changed_your_information'], 'default', array('class' => 'message success'));
                    $this->setupPostLogin(true);
                    $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                } else {
                    $this->Session->setFlash($this->language_based_messages['your_inforamtion_changing_failed_please_try_agian'], 'default', array('class' => 'message error'));
                    $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                }
                
            }
            
            else {
                $users = $this->Session->read('user_current_account');
                $account = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
                $customer_id = $account['Account']['braintree_customer_id'];
                $subscription_id = $account['Account']['braintree_subscription_id'];
                $account_id = $users['accounts']['account_id'];
                $plan = $this->Account->getMyAccounts($account_id);
                if (!$plan['plans']) {
                    $this->Session->setFlash($this->language_based_messages['please_select_your_plan_before_billing'], array('class' => 'message error'));
                    $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '4');
                } else {
                    $plans = $this->Session->read('subscription_data');
                    $planId = $plans['plan_id'];
//                    $customer = array(
//                        'firstName' => $users['User']['first_name'],
//                        'lastName' => $users['User']['last_name'],
//                        'company' => $users['accounts']['company_name'],
//                        'email' => $users['User']['email'],
//                        'creditCard' => array(
//                            'number' => $this->data['credit_card']['number'],
//                           'expirationDate' => $this->data['card_expiration_month'] . "/" . $this->data['card_expiration_year'],
//                            'expirationDate' => $this->data['card_expiration'],
//                            'cvv' => $this->data['credit_card']['cvv']
//                        )
//                    );
                    
                     $customer = array(
                        'firstName' => $this->data['first_name'],
                        'lastName' => $this->data['last_name'],
                        'company' => $this->data['company_name'],
                        'email' => $this->data['email'],
                        'creditCard' => array(
                            'number' => $this->data['credit_card']['number'],
                          // 'expirationDate' => $this->data['card_expiration_month'] . "/" . $this->data['card_expiration_year'],
                            'expirationDate' => $this->data['card_expiration'],
                            'cvv' => $this->data['credit_card']['cvv'],
                            'billingAddress' => array( 
                            'locality' => $this->data['city'],
                            'postalCode' => $this->data['zip_code'],
                            'region' => $this->data['state'] ,
                            'streetAddress' => $this->data['address']   
                               )
                        )
                    );
                     if($customer_id != NULL && $subscription_id != NULL && !empty($subscription_data_table) )
                     {
                      
                         $plans = $this->Plans->find("first", array("conditions" => array(
                                        "id" => $account['Account']['plan_id'],

                                )));

                        if(!empty($plans))
                        {
                        $yearly_bool = $plans['Plans']['per_year'];
                       
                        
                        }
                       if($yearly_bool){  
                       $result = $subscription->update_customer_subscription($planId, $customer,$customer_id,$subscription_id,$this->data['plan_price'],$plan_selected['monthly'],$yearly_bool,$plan_selected['quantity'],$account['Account']['plan_id'],$account['Account']['plan_qty'],$plans['Plans']['price_yearly']*12*$account['Account']['plan_qty'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_start_date'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_end_date']);
                       }
                       else{
                       $result = $subscription->update_customer_subscription($planId, $customer,$customer_id,$subscription_id,$this->data['plan_price'],$plan_selected['monthly'],$yearly_bool,$plan_selected['quantity'],$account['Account']['plan_id'],$account['Account']['plan_qty'],$plans['Plans']['price']*$account['Account']['plan_qty'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_start_date'],$subscription_data_table['AccountBraintreeSubscription']['Subscription_end_date']);
                       }
                     }
                     else{
                         if($customer_id != NULL)
                         {
                           $result = $subscription->create_subscription_with_existing_customer($planId, $customer,$this->data['plan_price'],$plan_selected['monthly'],$subscription_id,$customer_id);  
                         }
                         else{
                        $result = $subscription->create_subscription($planId, $customer,$this->data['plan_price'],$plan_selected['monthly'],$subscription_id); 
                         }
                     }
                     
                     if(isset($result->message))
                    {
                     $this->Session->setFlash($result->message, 'default', array('class' => 'message error'));
                    $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                        
                    }
                    

                    if (is_array($result) && $result['success']) {
                        $data = '';
                        $data = array(
                            'braintree_customer_id' => "'" . $result['braintree_customer_id'] . "'",
                            'braintree_subscription_id' => "'" . $result['braintree_subscription_id'] . "'",
                            'updated_at' => "'" . date('Y-m-d H:i:s') . "'",
                            'is_active' => TRUE,
                            'suspended_at' => NULL,
                            'in_trial' => '0',
                            'is_suspended' => '0',
                            'plan_id' => $result['braintree_plan_id']
                        );
                        
                        
                        $subscription_data = array(
                            
                            'braintree_subscription_id' => Sanitize::escape(htmlspecialchars($result['braintree_subscription_id'])),
                            'Subscription_start_date' => $result['start_date'] ,
                             'Subscription_end_date' => $result['end_date'] ,
                            'number_of_users' => $this->data['plan_quantity'],
                            'plan_id' => $result['braintree_plan_id'],
                            'subscription_type' => $result['subscription_type'],
                            'payment_status' => (empty($result['payment_status']))? 'settled' : $result['payment_status'],
                            'Subscription_Status' => $result['subscription_status'],
                            'account_id' => $account_id
                        );
                     
                     
                     $subscription_data_update = array(
                            
                            'braintree_subscription_id' => "'" . Sanitize::escape(htmlspecialchars($result['braintree_subscription_id']))  . "'",
                            'number_of_users' => $this->data['plan_quantity'],
                            'plan_id' => $result['braintree_plan_id'],
                            'subscription_type' => $result['subscription_type'],
                            'payment_status' => (empty($result['payment_status']))? "'settled'" : "'".$result['payment_status']."'",
                            'Subscription_Status' => "'".$result['subscription_status']."'",
                            'account_id' => $account_id
                        );
                    
                    
                
                if($result['db_check'] == 'update')
                {
                    $this->AccountBraintreeSubscription->updateAll($subscription_data_update,array('braintree_subscription_id' => $result['braintree_subscription_id'] ));
                }
                
                else{
                $this->AccountBraintreeSubscription->create();
                $this->AccountBraintreeSubscription->save($subscription_data, $validation = TRUE);
                }

                        $result = $this->Account->updateAll($data, array('id' => $account_id));
                        if ($this->Account->getAffectedRows() > 0) {
                            if ($this->data['planId'] == 1 || $this->data['planId'] == 5) {
                                $this->team_plan();
                            } elseif ($this->data['planId'] == 2 || $this->data['planId'] == 6) {
                                $this->deprartment_plan();
                            } elseif ($this->data['planId'] == 3 || $this->data['planId'] == 7) {
                                $this->school_plan_account();
                            } elseif ($this->data['planId'] == 4 || $this->data['planId'] == 8) {
                                $this->school_plus_plan();
                            }
                            $this->Session->write('subscription_data', '');
                            $this->Session->setFlash($this->language_based_messages['you_have_purchased_your_subscription_successfully'], 'default', array('class' => 'message success'));
                        } else {
                            if ($this->data['planId'] == 1 || $this->data['planId'] == 5) {
                                $this->team_plan();
                            } elseif ($this->data['planId'] == 2 || $this->data['planId'] == 6) {
                                $this->deprartment_plan();
                            } elseif ($this->data['planId'] == 3 || $this->data['planId'] == 7) {
                                $this->school_plan_account();
                            } elseif ($this->data['planId'] == 4 || $this->data['planId'] == 8) {
                                $this->school_plus_plan();
                            }
                            $this->Session->setFlash($this->language_based_messages['you_have_purchased_your_subscription_successfully'], 'default', array('class' => 'message success'));
                        }
                        $this->account_meta_data_changes($account_id);
                        $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                    } else if (!$result->success) {
                        $this->Session->setFlash($this->language_based_messages['error_processing_transaction'] . "\n Message : " . $result->message . " \n ", 'default', array('class' => 'message error'));
                        $this->set('new_plan', $this->Session->read('subscription_data'));
                        $this->set('customer_info', $this->_get_card_informaiton($account_id));

                        $this->send_subscription_email($this->language_based_messages['error_processing_transaction'] . "\n Message : " . $result->message . " \n ");

                        $this->set('account_id', $account_id);
                        $this->set('tab', $tab);
                      //  $this->render('update_billing_info');
                        $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                    } else if ($result->transaction) {
                        $this->Session->setFlash($this->language_based_messages['error_processing_transaction']. "\n Message : " . $result->message . " \n Code: " . $result->transaction->processorResponseCode . " \n Text " . $result->transaction->processorResponseText, 'default', array('class' => 'message error'));
                        $this->set('new_plan', $this->Session->read('subscription_data'));
                        $this->set('customer_info', $this->_get_card_informaiton($account_id));
                        $this->set('account_id', $account_id);
                        $this->set('tab', $tab);
                    //    $this->render('update_billing_info');
                        $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                    } else {
                        $this->set('new_plan', $this->Session->read('subscription_data'));
                        $this->set('customer_info', $this->_get_card_informaiton($account_id));
                        $this->Session->setFlash($this->language_based_messages['message'] . $result->message, 'default', array('class' => 'message error'));
                        $this->set('account_id', $account_id);
                        $this->set('tab', $tab);
                     //   $this->render('update_billing_info');
                        $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
                    }
                }
            }
        }
    }

 private function _get_card_informaiton($account_id) {
        $subscription = new Subscription();
        if (isset($account_id) && $account_id != '') {
            $customer_info = array(
                'plan_info' => '',
                'card_info' => ''
            );
            $account = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
            if (isset($account['Account']) && $account['Account']['braintree_customer_id'] != '' && $account['Account']['braintree_subscription_id'] != '') {
                $plan_info = '';
                $credit_card_info = '';

                $customer_info = $subscription->get_customer_information($account['Account']['braintree_customer_id']);

                if (isset($customer_info->creditCards[0]->subscriptions[0]->planId) && $customer_info->creditCards[0]->subscriptions[0]->planId > 0) {
                    $plan_info = $this->Plans->getSelectedPlan($customer_info->creditCards[0]->subscriptions[0]->planId);
                }
                if (isset($customer_info->creditCards[0]->subscriptions[0]->transactions[0]->creditCardDetails) && $customer_info->creditCards[0]->subscriptions[0]->transactions[0]->creditCardDetails != '') {
                    $credit_card_info = $customer_info->creditCards[0]->subscriptions[0]->transactions[0]->creditCardDetails;
                } elseif (isset($customer_info->creditCards[0]->maskedNumber) && $customer_info->creditCards[0]->maskedNumber != '' && isset($customer_info->creditCards[0]->token) && $customer_info->creditCards[0]->token != '') {
                    $credit_card_info = $customer_info->creditCards[0];
                }
                $customer_info = array(
                    'plan_info' => $plan_info,
                    'card_info' => $credit_card_info
                );
            }

            return $customer_info;
        }
    }
    
    function account_meta_data_changes($account_id) {
        
        $account = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        
         $plans = $this->Plans->find("first", array("conditions" => array(
                                        "id" => $account['Account']['plan_id'],

                                )));
         if($plans['Plans']['plan_permission_id'] == 1)
         {
             $data = array (
               'meta_data_value' =>  0 
             );
          $this->AccountMetaData->updateAll($data,
                  array(
                      'meta_data_name' =>array('enabletracking','enable_matric','enable_tracker','enable_video_library','enableanalytics'),
                      'account_id' => $account_id
                      ));
          
          $this->AccountFolderMetaData->updateAll($data,
                  array(
                      'meta_data_name' => 'enable_framework_standard',
                      'account_folder_id' => $account_id
                      ));
          
          
         }
         
         $this->redirect('/accounts/account_settings_all/' . $account_id . '/' . '5');
        
        
    }

}
