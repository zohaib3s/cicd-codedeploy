<?php

class ErrorsController extends AppController {

    public function beforeFilter() {
        $this->Auth->allow('show');
        parent::beforeFilter();
    }

    function show() {
        $this->layout = 'minimal';
        $this->render('show');
    }
    
     function error400() {
         if (!$this->Auth->user()) 
         {
        $this->layout = 'minimal';
        $this->set('message',"Oops! Looks like the page you're looking for was moved or doesn't exist.  Make sure you typed the correct URL or follow a valid link. Or <a href = ".Configure::read('sibme_base_url')." >Click Here</a>");
         }
         else
         {
         $this->set('message',"Oops! Looks like the page you're looking for was moved or doesn't exist. Please click the menu above to go back to the site. Or <a href = ".Configure::read('sibme_base_url')." >Click Here</a>");
         }
        $this->render('error400');
    }

    function view_exception($error_id) {
        $this->set('view_exceptions', $this->AuditErrors->get($error_id));
        $this->render('view_exceptions');
    }

}