<?php

use Aws\ElasticTranscoder\ElasticTranscoderClient;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe\DataMapping\StreamCollection;
use FFMpeg\FFProbe\DataMapping\Stream;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Filters\Video\ResizeFilter;
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

/**
 * User controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::import('Controller', 'Huddles'); // mention at top
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */

class FolderController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Folder';

    /**
     * This controller does not use a model
     *
     * @var array
     */
    var $helpers = array('Custom', 'Browser');
    public $uses = array(
        'User', 'AccountFolder', 'Group', 'UserGroup', 'AccountFolderMetaData', 'AccountFolderUser', 'AccountFolderGroup',
        'UserAccount', 'Document', 'Comment', 'AccountFolderDocument', 'AccountFolderDocumentAttachment', 'CommentUser',
        'CommentAttachment', 'Observations', 'Frameworks', 'AccountTag', 'AccountCommentTag', 'DocumentFiles'
    );
    public $components = array('Session');
    var $user_id = '';
    private $obj_huddles;
    private $video_per_page = 12;
    private $crumbs = array();
    public $reset = false; //reset the $_session variable 
    private $url_count = 10; // number of links allowed on the page

    function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $this->obj_huddles = new HuddlesController();
    }

    public function beforeFilter() {

        $this->set('AccountFolder', $this->AccountFolder);
        $this->set('AccountFolderUser', $this->AccountFolderUser);
        $this->set('Comment', $this->Comment);
        $this->set('Document', $this->Document);
        $this->set('AccountFolderGroup', $this->AccountFolderGroup);
        $this->set('UserGroup', $this->UserGroup);

        $users = $this->Session->read('user_current_account');
        $totalAccounts = $this->Session->read('totalAccounts');
        $this->set('logged_in_user', $users);
        $this->user_id = $users['User']['id'];

        if ($users['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
            $this->redirect_on_account_settings();
        }
        $bcrumb = $this->Session->read('breadcrumb');
        if (!empty($bcrumb)) {
            $this->crumbs = $this->safe_array_merge($this->crumbs, $bcrumb);
        }
        if ($this->reset) {
            $this->unset_variable();
        }

        $this->check_is_account_expired();
        $this->is_account_activated($users['users_accounts']['account_id'],$users['User']['id']);
        parent::beforeFilter();
    }

    public function reset_variable() {

        if ($this->reset) {

            $this->unset_variable();
        }
    }

    public function get_all_parents($huddle_id, $parents) {

        $result = $this->AccountFolder->find("first", array("conditions" => array(
                "account_folder_id" => $huddle_id,
        )));
        if ($result['AccountFolder']['parent_folder_id'] == NULL) {
            return $parents;
        } else {
            $result1 = $this->AccountFolder->find("first", array("conditions" => array(
                    "account_folder_id" => $result['AccountFolder']['parent_folder_id'],
            )));
            $parents[$result1['AccountFolder']['name'] . '_0f'] = $result['AccountFolder']['parent_folder_id'];
            return $this->get_all_parents($result['AccountFolder']['parent_folder_id'], $parents);
        }
    }

    public function add($label, $url) {


        $crumb = array();
        $crumb[$label] = $url;
        $bcrumb = $this->Session->read('breadcrumb');
        $parents = array();
        $bread_crumb_path = ($this->get_all_parents($url, $parents));
        $bread_crumb_path['Home'] = 'huddles';
        $bread_crumb_path = array_reverse($bread_crumb_path);

        if (empty($bcrumb)) {

            if (!isset($bcrumb['Home'])) { //If there's no session data yet, assume a homepage link
                $this->crumbs['Home'] = 'huddles';
                $bcrumb = $this->crumbs;
            }
        }

        $check_duplicate = $this->check_duplicate($crumb, $bread_crumb_path);

        if ($check_duplicate > 0) {

            $break_array = $this->break_array($bread_crumb_path, $crumb);


            $array_merge = $this->safe_array_merge($break_array, $crumb);
            //clear session variable
            //and prepare the session variable to be loaded with the exact information location
            $this->unset_variable();

            $output = $this->output_new_breadcrum($break_array, $label, $url);

            //put the merged array in the session variable
            //this is where the user is at the moment
            $bcrumb = $array_merge;
            $this->Session->write('breadcrumb', $bread_crumb_path);
        } else {

            $output = $this->output_new_breadcrum($bread_crumb_path, $label, $url);

            $bcrumb = $this->safe_array_merge($bread_crumb_path, $crumb);
            $this->Session->write('breadcrumb', $bread_crumb_path);
        }
        //return the output

        return $output;
    }

    private function output($array, $label, $url) {

        $count = 0;

        $link = "<div class='bridcrm'><ul>";

        foreach ($array as $key => $value) {
            $count++;

            if ($count < $this->url_count) {
                if ($key == 'Home') {
                    $link .="<li><a href='" . $this->base . "/Huddles' title='" . $key . "'><img src='/app/img/crhomeicon.png' /></a></li>$this->separator ";
                } else {
                    $k = substr($key, 0, -3);
                    $link .="<li class='foldericon'><a href='" . $this->base . '/Folder/' . $value . "' title='" . $k . "'>" . $k . "</a></li>$this->separator ";
                }
            }
        }
        //attach the last link
        $l = substr($label, 0, -3);
        $link .="<li class='fileicon bridlast'><span>$l</span></li> ";
        $link .="</ul></div>";

        return $link;
    }

     private function output_new_breadcrum($array, $label, $url) {

        $count = 0;

        $link = "";

        foreach ($array as $key => $value) {
            $count++;

            if ($count < $this->url_count) {
                if ($key == 'Home') {
                   
                } else {
                    $k = substr($key, 0, -3);
                  //  $link .="<a href='" . $this->base . '/Folder/' . $value . "'>" . $k . "</a>";
                    $link .= '<a href = "'.$this->base . '/Folder/' . $value .'"   >'.addslashes($k).'</a>';
                }
            }
        }
       
       

        return $link;
    }

    private function check_duplicate($array1, $array2) {


        $array = array_intersect_assoc($array1, $array2);

        //return the number of occurences
        return count($array);
    }

    private function safe_array_merge($array1, $array2) {
        return array_merge($array1, $array2);
    }

    private function break_array($array1, $array2) {

        $count = 0;
        foreach ($array1 as $key => $value) {
            $count++;
            if (($value == @$array2[$key])) {

                $num = $count;
            }
        }

        while ((count($array1) + 1) > ($num)) {

            array_pop($array1); //prune until we reach the $level we've allocated to this page
        }

        return $array1;
    }

    private function unset_variable() {
        $bcrumb = array();
        $this->Session->write('breadcrumb', $bcrumb);
    }

    function index($folder_id = '', $view_mode = '', $sort = '', $searchHuddles = '') {
        
        return $this->redirect('/video_huddles/list/'.$folder_id.'/', 301);

        if (isset($this->request->params['folder_id'])) {
            $folder_id = $this->request->params['folder_id'];
        } else {
            // $this->redirect('/Huddles');
        }
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $this->set('user_id', $users['User']['id']);
        
        $result = $this->AccountFolder->find("first", array("conditions" => array(
                "account_folder_id" => $folder_id,
        )));
        
        if (!($this->folder_has_huddle_participating($folder_id) || $result['AccountFolder']['created_by'] == $user_id)) {
        
            $this->no_permissions();
            
        }

        //$this->unset_variable();
        //die;        

        $folder_label = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $folder_id), 'fields' => array('name')));

        $crumb_output = $this->add($folder_label['AccountFolder']['name'] . '_0f', $folder_id);

        $this->set("breadcrumb", $crumb_output);

        $view_m = $this->Session->read('view_mode');
        if (empty($view_m) && empty($view_mode))
            $this->Session->write('view_mode', 'grid');
        if (empty($view_m) && !empty($view_mode))
            $this->Session->write('view_mode', $view_mode);
        if (!empty($view_m) && empty($view_mode))
            $this->Session->write('view_mode', $view_m);
        if (!empty($view_m) && !empty($view_mode))
            $this->Session->write('view_mode', $view_mode);
        $view_mode = $this->Session->read('view_mode');
        $this->set('view_mode', $view_mode);
        $sort_type = $this->Session->read('sort_type');
        if (empty($sort_type) && empty($sort))
            $this->Session->write('sort_type', 'name');
        if (empty($sort_type) && !empty($sort))
            $this->Session->write('sort_type', $sort);
        if (!empty($sort_type) && empty($sort))
            $this->Session->write('sort_type', $sort_type);
        if (!empty($sort_type) && !empty($sort))
            $this->Session->write('sort_type', $sort);

        $sort = $this->Session->read('sort_type');
        $this->set("sort", $sort);

        $folders_check_dropdown = $this->AccountFolder->getAllAccountFolders($account_id, 'name', FALSE, false, false, $folder_id);
        if ($folders_check_dropdown) {

            $accessible_folders = array();
            foreach ($folders_check_dropdown as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $folders_check_dropdown = $accessible_folders;
        }



        if (empty($folders_check_dropdown) && $sort == 'folders') {
            $sort = 'name';
            $this->Session->write('sort_type', 'name');
            $this->set("sort", $sort);
        }


        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $huddles = $this->AccountFolder->getfolderHuddles($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id);
        $coach_huddles_dropdown = $this->AccountFolder->getfolderHuddles($account_id, 'coaching', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id);

        $collab_huddles_dropdown = $this->AccountFolder->getfolderHuddles($account_id, 'collaboration', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id);
//       echo "<pre>";
//       print_r($collab_huddles_dropdown);
//       die;
        $eval_huddles_dropdown = $this->AccountFolder->getfolderHuddles($account_id, 'evaluation', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id);
        $huddles_count = $this->AccountFolder->getfolderHuddlesCount($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id);
        $item = array();
        $folders = array();
        $other_huddles = array();
        $view = new View($this, false);
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();
//                $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);

                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id) && $users['roles']['role_id'] == 120) {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($participants_ids) && is_array($participants_ids) && in_array($row['Document']['created_by'], $participants_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    } elseif ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id) && ($users['roles']['role_id'] == 110 || $users['roles']['role_id'] == 100 || $users['roles']['role_id'] == 115 )) {
                     //  $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                         $huddles[$i]['AccountFolder']['total_videos'] = $huddles[$i][0]['v_total'];
                    } else {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    }
                } else {
                 //   $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                    $huddles[$i]['AccountFolder']['total_videos'] = $huddles[$i][0]['v_total'];
                }
                
                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    $total_resources = 0;
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $total_participant_videos = $this->Document->countVideosEvaluator_access($huddles[$i]['AccountFolder']['account_folder_id'],$user_id,$evaluators_ids);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {

                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {

                                    $total_count_r = count($this->Document->getVideoDocumentsByVideo($row['Document']['id'], $huddles[$i]['AccountFolder']['account_folder_id']));


                                    $total_resources = $total_resources + $total_count_r;
                                }
                            }
                        }
                        
                        if($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)){
                            $huddles[$i]['AccountFolder']['total_docs'] = $total_resources;
                        }else{
                            $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        }
                    }
                } else {
                  //  $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    if($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)){
                        $huddles[$i]['AccountFolder']['total_docs'] = $huddles[$i][0]['doc_total'];
                    }else{
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    }
                    
                }


                
//                $item = $this->Document->getVideos($huddles[$i]['AccountFolder']['account_folder_id']);
//                $get_comments = 0;
//                if (count($item) > 0) {
//                    for ($j = 0; $j < count($item); $j++) {
//                        $get_comments = $get_comments + $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
//                    }
//                    $huddles[$i]['AccountFolder']['total_comments'] = $get_comments;
//                } else {
//                    $huddles[$i]['AccountFolder']['total_comments'] = $get_comments;
//                }
            }



            if ($huddles) {
                $folder_tree = '';
                foreach ($huddles as $huddle) {
                    if ($huddle['AccountFolder']['folder_type'] == '5') {
                        //$huddle['videos'] = $this->AccountFolder->getFolderVideos($huddle['AccountFolder']['account_folder_id']);
                        //$folders[] = $huddle;
                    } else {
                        $other_huddles[] = $huddle;
                    }
                }
            }
        }

        $folders = $this->AccountFolder->getAllAccountFolders($account_id, $sort, FALSE, false, false, $folder_id);

        if ($folders) {

            $accessible_folders = array();
            foreach ($folders as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $folders = $accessible_folders;
        }



        $total_huddles = $huddles;
        $this->set('video_huddles', $other_huddles);
        if ($sort != 'coaching' && $sort != 'collaboration' && $sort != 'evaluation') {
            $this->set('folders', $folders);
            $total_huddles = $huddles_count;
        }
        if ($sort == 'folders') {
            $this->set('video_huddles', '');
            $this->set('folders', $folders);
            $total_huddles = 0;
            $huddles_count = 0;
        }
        $params = array(
            'sort' => $sort,
            'new_huddle_button' => 'New Huddle',
            'page_title' => 'Huddles',
            'view_mode' => $view_mode,
            'folder_id' => $folder_id,
            'other_folders_info' => $this->AccountFolder->getotherfolders($folder_id, $user_id),
            'total_huddles' => ($total_huddles == false ? "0" : count($total_huddles)),
            'folders_check_dropdown' => $folders_check_dropdown,
            'coach_huddles_dropdown' => $coach_huddles_dropdown,
            'collab_huddles_dropdown' => $collab_huddles_dropdown,
            'eval_huddles_dropdown' => $eval_huddles_dropdown,
        );
        
            $this->set('sort' , $sort);
            $this->set('new_huddle_button' , 'New Huddle');
            $this->set('page_title' , 'Huddles');
            $this->set('view_mode' , $view_mode);
            $this->set('folder_id' , $folder_id);
            $this->set('other_folders_info' , $this->AccountFolder->getotherfolders($folder_id, $user_id));
            $this->set('total_huddles' , ($total_huddles == false ? "0" : count($total_huddles)));
            $this->set('folders_check_dropdown' , $folders_check_dropdown);
            $this->set('coach_huddles_dropdown' , $coach_huddles_dropdown);
            $this->set('collab_huddles_dropdown' , $collab_huddles_dropdown);
            $this->set('eval_huddles_dropdown' , $eval_huddles_dropdown);
        
        
        $this->set('folder_id_edit', $folder_id);
        $this->set('current_folder_name',$folder_label['AccountFolder']['name']);
        $this->layout = 'folder';
        $view = new View($this, false);
      //  $this->set('head', $view->element('folderhuddle', $params));
        $this->render('index');
    }

    function folder_has_huddle_participating($folder_id) {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $this->set('user_id', $users['User']['id']);
         $folder_ids = $this->get_folder_childs($folder_id);
        $flag = false;
        foreach ($folder_ids as $fold_id )
        {
            $has_huddle = $this->AccountFolder->hasAccountHuddlesInFolder($account_id, $user_id, $fold_id);  
            if ($has_huddle == true) {

            $flag = true;
            }
        }
       
        if($flag)
        {
            return true;
        }
        else
        {
        return false;
        }
    }

    function _subFolderExist($account_folder_id) {
        if ($this->AccountFolder->find('count', array('conditions' => array('parent_folder_id' => $account_folder_id, 'folder_type' => 5, 'active' => 1)))) {
            return true;
        } else {
            return false;
        }
    }

    function _getFolderChild($account_folder_id) {
        $result = $this->AccountFolder->find('all', array('conditions' => array('parent_folder_id' => $account_folder_id, 'folder_type' => 5, 'active' => 1)));
        return $result;
    }

    function create($folder_id = '') {
        $this->auth_permission('huddle', 'add');
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $view_m = $this->Session->read('view_mode');

        $user_ids = array();

        $this->set('folder_id', $folder_id);

        if ($this->request->is('post')) {
            $this->Session->write('sort_type', 'date');
            $superUsersIds = isset($this->data['super_admin_ids']) ? $this->data['super_admin_ids'] : '';
            //$groupIds = isset($this->data['group_ids']) ? $this->data['group_ids'] : '';

            if (empty($this->request->data['hname'])) {
                $this->Session->setFlash('Please enter the Huddle name.', 'default', array('class' => 'message error'));
                $this->redirect('/folder/create/' . $this->request->data['folder_id']);
            }
            $name = trim($this->request->data['hname']);
            $this->request->data['created_date'] = date("Y-m-d h:i:s");
            $this->set('user_id', $users['User']['id']);
            $this->AccountFolder->create();

            if (!empty($this->request->data['folder_id'])) {
                $result = $this->AccountFolder->find('first', array('conditions' => array('folder_type' => '5', 'parent_folder_id' => $this->request->data['folder_id'], 'active' => '1', 'name' => $name,'account_id' =>$account_id)));
                if (count($result) > 0) {
                    echo 'folderexists';
                    die();
                }
            } else {

                $result = $this->AccountFolder->find('first', array('conditions' => array('folder_type' => '5', 'parent_folder_id' => NULL, 'active' => '1', 'name' => $name,'account_id' =>$account_id)));
                if (count($result) > 0) {
                    echo 'folderexists';
                    die();
                }
            }

            if (!empty($this->request->data['folder_id'])) {
                $data = array(
                    'name' => trim($this->request->data['hname']),
                    'desc' => $this->request->data['hdescription'],
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id'],
                    'folder_type' => 5,
                    'parent_folder_id' => $this->request->data['folder_id'],
                    'active' => 1,
                    'account_id' => $account_id
                );
            } else {
                $data = array(
                    'name' => trim($this->request->data['hname']),
                    'desc' => $this->request->data['hdescription'],
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id'],
                    'folder_type' => 5,
                    'active' => 1,
                    'account_id' => $account_id
                );
            }

            if ($this->AccountFolder->save($data, $validation = TRUE)) {

                $account_folder_id = $this->AccountFolder->id;

                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'message',
                    'meta_data_value' => (!isset($this->request->data['message']) ? "" : $this->request->data['message']),
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);

                $superUserData = array(
                    'account_folder_id' => $account_folder_id,
                    'user_id' => $users['User']['id'],
                    'role_id' => $this->role_huddle_admin,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );

                $this->AccountFolderUser->create();
                $this->AccountFolderUser->save($superUserData);

                $user_activity_logs = array(
                    'ref_id' => $account_folder_id,
                    'desc' => $this->request->data['hname'],
                    'url' => $this->base . '/Folder/' . $account_folder_id,
                    'account_folder_id' => $account_folder_id,
                    'date_added' => date("Y-m-d H:i:s"),
                    'type' => '1'
                );
                //$this->user_activity_logs($user_activity_logs);

                $search_result = array(
                    'view_mode' => $view_m,
                    'sort' => 'date'
                );
                $view = new View($this, false);
                $html = $view->element('ajax/huddles_sorting', $search_result);
                echo $html;
                die();
            } else {
                $this->Session->setFlash('Folder is not added please try again.', 'default', array('class' => 'message error'));
                $this->set('user_id', $users['id']);
                $this->render('create');
            }
        } else {
            $this->render('create');
        }
        die;
    }

    function edit($huddle_id) {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $result = $this->obj_huddles->get_all_users($account_id);
        $all_users_data = array();
        $name = trim($this->request->data['hname']);
        if (!empty($this->request->data['folder_id'])) {
            $result = $this->AccountFolder->find('first', array('conditions' => array('folder_type' => '5', 'parent_folder_id' => $this->request->data['folder_id'], 'active' => '1', 'name' => $name)));
            if (count($result) > 0) {
                echo 'folderexists';
                die();
            }
        } else {

            $result = $this->AccountFolder->find('first', array('conditions' => array('folder_type' => '5', 'parent_folder_id' => NULL, 'active' => '1', 'name' => $name)));
            if (count($result) > 0) {
                echo 'folderexists';
                die();
            }
        }




        if (count($result) > 0) {
            foreach ($result as $row) {
                if ($row['is_user'] == 'group') {
                    $res = $this->AccountFolderGroup->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'group_id' => $row['id'])));
                    $row['is_coach'] = isset($res['AccountFolderGroup']['is_coach']) ? $res['AccountFolderGroup']['is_coach'] : '0';
                    $row['is_mentee'] = isset($res['AccountFolderGroup']['is_mentee']) ? $res['AccountFolderGroup']['is_mentee'] : '0';
                } elseif ($row['is_user'] == 'member' || $row['is_user'] == 'admin') {
                    $res = $this->AccountFolderUser->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'user_id' => $row['id'])));
                    $row['is_coach'] = isset($res['AccountFolderUser']['is_coach']) ? $res['AccountFolderUser']['is_coach'] : '0';
                    $row['is_mentee'] = isset($res['AccountFolderUser']['is_mentee']) ? $res['AccountFolderUser']['is_mentee'] : '0';
                }
                $all_users_data[] = $row;
            }
        }

        $this->set('users_record', $all_users_data);
        $this->auth_huddle_permission('edit', $huddle_id);
        $huddle = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));
        $user_id = $users['User']['id'];
        $this->set('huddle_id', $huddle_id);
        $this->set('huddles', $huddle);


        $this->set('huddle_message', $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'message'))));
        /*
         * huddle_type 1=Callaboration huddle
         *             2=Coachee huddle
         */
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $this->set('huddle_type', isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1");

        $this->set('huddleUsers', $this->AccountFolderUser->find('all', array('conditions' => array('account_folder_id' => $huddle_id, 'role_id' => '210'))));
        $this->set('huddleViewers', $this->AccountFolderUser->find('all', array('conditions' => array('account_folder_id' => $huddle_id, 'role_id' => '220'))));
        $this->set('supperUsers', $this->AccountFolderUser->find('all', array('conditions' => array('account_folder_id' => $huddle_id, 'role_id' => '200'))));
        $this->set('groups', $this->AccountFolderGroup->find('all', array('conditions' => array('account_folder_id' => $huddle_id))));

        $this->set('users', $this->_getAllUsers($account_id, true));
        $this->set('super_users', $this->_getAllUsers($account_id, true));
        $this->set('users_groups', $this->_groups($account_id));
        $this->set('user_id', $users['User']['id']);
        $created_by = '';
        if (isset($huddle) && $huddle != '') {
            $created_by = $huddle['AccountFolder']['created_by'];
        }

        if ($this->request->is('post')) {
            $superUsersIds = isset($this->data['super_admin_ids']) ? $this->data['super_admin_ids'] : '';
            $groupIds = isset($this->data['group_ids']) ? $this->data['group_ids'] : '';

            if (empty($this->request->data['hname'])) {
                $this->Session->setFlash('Please enter the Huddle name.', 'default', array('class' => 'message error'));
                $this->redirect('/folder/edit/' . $huddle_id);
            }

            if (!isset($this->request->data['message']))
                $this->request->data['message'] = "";

            if (strlen($this->request->data['message']) > 1024) {
                $this->Session->setFlash('Please limit to 1024 characters or less. Currently:' . strlen($this->request->data['message']), 'default', array('class' => 'message error'));
                $this->redirect('/folder/edit/' . $huddle_id);
            }

            $this->request->data['created_date'] = date("Y-m-d H:i:s");
            $this->set('user_id', $users['User']['id']);
            $this->AccountFolder->create();
            $data = array(
                'name' => "'" . addslashes($this->request->data['hname']) . "'",
                'desc' => "'" . addslashes($this->request->data['hdescription']) . "'",
                'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                'last_edit_by' => $users['User']['id']
            );

            $this->AccountFolder->updateAll($data, array('account_folder_id' => $huddle_id));
            if ($this->AccountFolder->getAffectedRows() > 0) {
                $huddle_obj = $this->AccountFolder->find('first', array(
                    'conditions' => array(
                        'account_folder_id' => $huddle_id
                    )
                ));

                $account_folders_meta_data = array(
                    'meta_data_value' => "'" . addslashes($this->request->data['message']) . "'",
                    'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->updateAll($account_folders_meta_data, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'message'));

                $huddle_type_meta = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "folder_type")));
                if (empty($huddle_type_meta)) {

                    $account_folders_meta_data = array(
                        'account_folder_id' => $huddle_id,
                        'meta_data_name' => 'folder_type',
                        'meta_data_value' => (!isset($this->request->data['type']) ? "" : $this->request->data['type']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                } else {
                    $account_folders_meta_data_huddle_type = array(
                        'meta_data_value' => (!isset($this->request->data['type']) ? "" : $this->request->data['type']),
                        'last_edit_date' => '"' . date("Y-m-d H:i:s") . '"',
                        'last_edit_by' => '"' . $users['User']['id'] . '"'
                    );
                    $this->AccountFolderMetaData->updateAll($account_folders_meta_data_huddle_type, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type'));
                }


                /* $tmp_huddle_user_ids = array();
                  $tmp_ex_huddle_user_ids = array();
                  $ex_folder_users = $this->AccountFolderUser->find('all', array(
                  'conditions' => array(
                  'account_folder_id' => $huddle_id
                  )
                  ));

                  if (isset($ex_folder_users) && count($ex_folder_users) > 0) {
                  for ($i = 0; $i < count($ex_folder_users); $i++) {
                  $tmp_ex_huddle_user_ids[] = $ex_folder_users[$i]['AccountFolderUser']['user_id'];
                  }
                  }

                  $this->AccountFolderUser->deleteAll(array('account_folder_id' => $huddle_id));
                  $this->AccountFolderGroup->deleteAll(array('account_folder_id' => $huddle_id));
                  $account_folder_id = $huddle_id;

                  if ($superUsersIds != '') {
                  foreach ($superUsersIds as $supUsers) {
                  $assigned_role = $this->request->data['user_role_' . $supUsers];
                  $assigned_is_coach = isset($this->request->data['is_coach_' . $supUsers]) ? 1 : '0';
                  $assigned_is_mentee = isset($this->request->data['is_mentor_' . $supUsers]) ? 1 : '0';
                  if ($assigned_role == '') {
                  $this->Session->setFlash('Please select user role', 'default', array('class' => array('message error')));
                  $this->redirect('/Huddles/edit/' . $huddle_id);
                  }
                  if ($supUsers <= 0) {
                  //we need to add this user first.
                  $tmp_user_id = $this->_addUser($this->request->data ['super_admin_fullname_' . $supUsers], $this->request->data['super_admin_email_' . $supUsers], $account_folder_id);

                  if ($tmp_user_id <= 0)
                  continue;
                  $supUsers = $tmp_user_id;
                  $tmp_huddle_user_ids[] = $supUsers;
                  }

                  if (!in_array($supUsers, $tmp_ex_huddle_user_ids)) {
                  $tmp_huddle_user_ids[] = $supUsers;
                  }

                  if ($assigned_role == '200')
                  $assigned_is_coach = 1;

                  $superUserData = array(
                  'account_folder_id' => $account_folder_id,
                  'user_id' => $supUsers,
                  'role_id' => $assigned_role,
                  'created_date' => date("Y-m-d H:i:s"),
                  'last_edit_date' => date("Y-m-d H:i:s"),
                  'created_by' => $users['User']['id'],
                  'last_edit_by' => $users['User']['id'],
                  'is_coach' => $assigned_is_coach,
                  'is_mentee' => $assigned_is_mentee,
                  );
                  $this->AccountFolderUser->create();
                  $this->AccountFolderUser->save($superUserData);
                  }
                  }
                  if ($groupIds != '') {
                  foreach ($groupIds as $grp) {
                  $role_id = isset($this->request->data['group_role_' . $grp]) ?
                  $this->request->data['group_role_' . $grp] : 220;
                  if (empty($role_id))
                  $role_id = 220;
                  $groupsData = array(
                  'account_folder_id' => $account_folder_id,
                  'group_id' => $grp,
                  'role_id' => $role_id,
                  'created_date' => date("Y-m-d H:i:s"),
                  'last_edit_date' => date("Y-m-d H:i:s"),
                  'created_by' => $users['User']['id'],
                  'last_edit_by' => $users['User']['id'],
                  'is_coach' => isset($this->request->data['is_coach_' . $grp]) ? $this->request->data['is_coach_' . $grp] : '0',
                  'is_mentee' => isset($this->request->data['is_mentor_' . $grp]) ? $this->request->data['is_mentor_' . $grp] : '0',
                  );
                  $this->AccountFolderGroup->create();
                  $this->AccountFolderGroup->save($groupsData);
                  }
                  }

                  $huddleUsers = $this->AccountFolder->getHuddleUsers($huddle_obj['AccountFolder']['account_folder_id']);
                  $user_ids = implode(',', $tmp_huddle_user_ids);

                  if (!empty($user_ids) && count($user_ids) > 0) {

                  $huddle = $this->AccountFolder->getHuddle($huddle_obj['AccountFolder']['account_folder_id'], $account_id);
                  $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                  if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                  $huddle[0]['participated_user'] = $huddleUsers;
                  foreach ($huddleUserInfo as $row) {
                  $huddle[0]['User']['email'] = $row['User']['email'];
                  if ($this->check_subscription($row['User']['id'], '4')) {
                  $huddle[0]['account_info'] = $users['accounts'];
                  $huddle[0]['huddle_type'] = (!isset($this->request->data['type']) ? "" : $this->request->data['type']);
                  //$this->newHuddleEmail($huddle[0], $row['User']['id']);
                  }
                  }
                  }
                  } */
                return 1;
                $this->Session->setFlash($this->request->data['hname'] . ' has been updated successfully.', 'default', array('class' => 'message success'));
                //$this->redirect('/Huddles/');
            } else {
                $this->Session->setFlash('Video Huddle is not updated please try again.', 'default', array('class' => 'message error'));
                $this->set('user_id', $users['User']['id']);
                $this->render('edit');
            }
        } else {
            $this->render('edit');
        }
    }

    private function _getUsers($account_id, $include_session_users = false) {

        $users = $this->User->getUsersByRole($account_id, $this->role_user);

        if (is_array($users) && count($users) > 0) {
            return $users;
        } else {
            return

                    FALSE;
        }
    }

    private function _getSupperAdmins($account_id, $include_session_users = false) {

        $super_users = $this->User->getUsersByRole($account_id, $this->role_super_user);
        $account_owner_users = $this->User->getUsersByRole($account_id, 100);

        $result = array_merge($account_owner_users, $super_users);

        if (is_array($result) && count($result) > 0) {
            return $result;
        } else {
            return

                    FALSE;
        }
    }

    private function _groups($account_id) {

        $groups = $this->Group->getGroups($account_id);
        if (is_array($groups) && count($groups) > 0) {
            return $groups;
        } else {
            return FALSE;
        }
    }

    function _addUser($full_name, $email, $account_folder_id = '') {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $this->User->create();
        $arr = '';
        $data = '';
        if (isset($full_name)) {
            $name = explode(' ', $full_name);
            $arr['first_name'] = isset($name[0]) ? $name[0] : '';
            $arr['last_name'] = '';
            if (count($name) > 1) {
                array_shift($name);
                $arr['last_name'] = implode(" ", $name);
            }
        }
        if (isset($email)) {

            $arr['created_by'] = $users['User']['id'];
            $arr['created_date'] = date('Y-m-d H:i:s');
            $arr['last_edit_by'] = $users['User']['id'];
            $arr['last_edit_date'] = date('Y-m-d H:i:s');
            $arr['account_id'] = $account_id;
            $arr['email'] = $email;
            $arr['username'] = $email;
            $arr['authentication_token'] = $this->digitalKey(20);
            $arr['is_active'] = false;
            $arr['type'] = 'Invite_Sent';
            if ($arr['first_name'] != '' && $arr['email'] != '') {
                $data[] = $arr;
            }
        }

        $new_user_id = -1;
        $roleData = array();
        foreach ($data as $user) {
            if (isset($user['first_name']) && isset($user['email']) && $user['email']) {
                $exUser = $this->User->find('first', array('conditions' => array('email' => $user['email'])));
                $userAccountCount = 0;
                $this->User->create();
                if (count($exUser) == 0 && $this->User->save($user, $validation = TRUE)) {
                    $user_id = $this->User->id;

                    $this->User->updateAll(array(
                        'authentication_token' => "'" . md5($user_id) . "'"), array('id' => $user_id)
                    );

                    $user['authentication_token'] = md5($user_id);
                } else {
                    $user_id = $exUser['User']['id'];
                }

                if (!empty($user_id)) {

                    $userAccountCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id,
                            'account_id' => $account_id
                        )
                    ));
                    if ($userAccountCount == 0) {

                        $this->UserAccount->create();

                        //Checking default account is exist or not
                        $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id
                        )));
                        $userAccount = array(
                            'account_id' => $account_id,
                            'user_id' => $user_id,
                            'role_id' => '120',
                            'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                            'created_by' => $users['User']['id'],
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'last_edit_by' => $users['User']['id']
                        );

                        $this->UserAccount->save($userAccount, $validation = TRUE);

                        if ($user_id) {
                            $user['user_id'] = $user_id;
                            $user['message'] = $this->data['message'];
                            if (count($exUser) == 0) {
                                $this->sendInvitation($user, $account_folder_id);
                            } else {
                                $this->sendConfirmation($user);
                            }

                            $user_activity_logs = array(
                                'ref_id' => $account_folder_id,
                                'desc' => $user['first_name'] . " " . isset($user['last_name']) ? $user['last_name'] : '',
                                'url' => $this->base . 'Huddles',
                                'account_folder_id' => $account_folder_id,
                                'date_added' => date("Y-m-d H:i:s")
                            );
                            $this->user_activity_logs($user_activity_logs);

                            $new_user_id = $user_id;
                        }
                    }
                }
            }
        }

        return $new_user_id;
    }

    function save_huddle_log($account_folder_id, $user_id) {
        $users = $this->Session->read('user_current_account');
        $user = $this->User->get($user_id);

        $user_activity_logs = array(
            'ref_id' => $user_id,
            'desc' => $users ['User'] ['first_name'] . " " . $users['User']['last_name'],
            'url' => $this->base . '/huddles/view/' . $account_folder_id . '/4',
            'account_folder_id' => $account_folder_id,
            'type' => '7'
        );
        $this->user_activity_logs($user_activity_logs);
    }

    private function _getAllUsers($account_id, $include_session_users = false) {

        $users = $this->User->getUsersByRole($account_id, $this->role_user);
        $super_users = $this->User->getUsersByRole($account_id, $this->role_super_user);
        $account_owner_users = $this->User->getUsersByRole($account_id, 100);

        $result = array_merge($users, $super_users);
        $result = array_merge($result, $account_owner_users);

        if (is_array($result) && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function createhuddle($folder_id, $type = '2') {
        
        
         $user_permissions = $this->Session->read('user_permissions');
        if(!$user_permissions['UserAccount']['manage_collab_huddles'] && !$user_permissions['UserAccount']['manage_coach_huddles'] )
        {
            $type = 3;
        }
        
        elseif(!$user_permissions['UserAccount']['manage_collab_huddles'] && !$user_permissions['UserAccount']['manage_evaluation_huddles'])
        {
            $type = 2;
        }
        
        elseif(!$user_permissions['UserAccount']['manage_coach_huddles'] && !$user_permissions['UserAccount']['manage_evaluation_huddles'])
        {
            $type = 1;
        }
        
        $this->auth_permission('huddle', 'add');
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $default_framework = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "default_framework"
                )));
                if (isset($default_framework['AccountMetaData'])) {
                    $default_framwork_value = $default_framework['AccountMetaData']['meta_data_value'];
                }

        $this->set('default_framework',$default_framwork_value);
        $this->set('users_record', $this->obj_huddles->get_all_users($account_id));
        $this->set('users', $this->_getUsers($account_id, true));
        $this->set('super_users', $this->_getSupperAdmins($account_id, true));
        $this->set('users_groups', $this->_groups($account_id));
        $this->set('user_id', $users['User']['id']);
        $this->set('folder_id', $folder_id);
        $user_ids = array();
        $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                "tag_type" => '2', "account_id" => $account_id
        )));
        if (!empty($frameworks) && count($frameworks) > 0)
            $this->set('frameworks', $frameworks);

        if (!isset($this->request->data['message'])) {
            $this->request->data['message'] = "";
        }

        if ($this->request->is('post')) {
            $superUsersIds = isset($this->data['super_admin_ids']) ? $this->data['super_admin_ids'] : '';
            $groupIds = isset($this->data['group_ids']) ? $this->data['group_ids'] : '';

            if (empty($this->request->data['hname'])) {
                $this->Session->setFlash('Please enter the Huddle name.', 'default', array('class' => 'message error'));
                $this->redirect('/Folder/createhuddle/'.$folder_id.'/'.$type);
            }

            if (!isset($this->request->data['message']))
                $this->request->data['message'] = "";

            if (!isset($this->request->data['chkenableframework']))
                $this->request->data['chkenableframework'] = 0;
            if (!isset($this->request->data['chkenabletags']))
                $this->request->data['chkenabletags'] = 0;

            if (strlen($this->request->data['message']) > 1024) {
                $this->Session->setFlash('Please limit to 1024 characters or less. Currently:' . strlen($this->request->data['message']), 'default', array('class' => 'message error'));
                $this->redirect('/Folder/createhuddle/'.$folder_id.'/'.$type);
            }


            $this->request->data['created_date'] = date("Y-m-d h:i:s");
            $this->set('user_id', $users['User']['id']);
            $this->AccountFolder->create();
            $data = array(
                'name' => trim($this->request->data['hname']),
                'desc' => $this->request->data['hdescription'],
                'created_date' => date("Y-m-d H:i:s"),
                'last_edit_date' => date("Y-m-d H:i:s"),
                'created_by' => $users['User']['id'],
                'last_edit_by' => $users['User']['id'],
                'folder_type' => 1,
                'active' => 1,
                'parent_folder_id' => $folder_id,
                'account_id' => $account_id
            );
            if ($this->AccountFolder->save($data, $validation = TRUE)) {

                $account_folder_id = $this->AccountFolder->id;

                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'message',
                    'meta_data_value' => (!isset($this->request->data['message']) ? "" : $this->request->data['message']),
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);

                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'chk_frameworks',
                    'meta_data_value' => (!isset($this->request->data['chkenableframework']) ? "0" : $this->request->data['chkenableframework']),
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);

                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'chk_tags',
                    'meta_data_value' => (!isset($this->request->data['chkenabletags']) ? "0" : $this->request->data['chkenabletags']),
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);

                if (isset($this->request->data['chkenableframework']) && $this->request->data['chkenableframework'] == 1 && isset($this->request->data['frameworks'])) {
                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'framework_id',
                        'meta_data_value' => $this->request->data['frameworks'],
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                }


                if ($this->request->data['type'] == '2') {
                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'folder_type',
                        'meta_data_value' => (!isset($this->request->data['type']) ? "" : $this->request->data['type']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);

                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'coachee_permission',
                        'meta_data_value' => (!isset($this->request->data['chkcoacheepermissions']) ? "" : $this->request->data['chkcoacheepermissions']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                }
                
                $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'coach_hud_feedback',
                        'meta_data_value' => (!isset($this->request->data['coach_hud_feedback']) ? "" : $this->request->data['coach_hud_feedback']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                
                if ($this->request->data['type'] == '3') {
                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'submission_deadline_date',
                        'meta_data_value' => (!isset($this->request->data['submission_deadline_date']) ? "" : $this->request->data['submission_deadline_date']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'submission_deadline_time',
                        'meta_data_value' => (!isset($this->request->data['submission_deadline_time']) ? "" : $this->request->data['submission_deadline_time']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);

                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'folder_type',
                        'meta_data_value' => (!isset($this->request->data['type']) ? "" : $this->request->data['type']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'coachee_permission',
                        'meta_data_value' => (!isset($this->request->data['chkcoacheepermissions']) ? "" : $this->request->data['chkcoacheepermissions']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                }
                //clear session users.
                $this->Session->write('newly_added_users', "");

                if ($this->request->data['type'] == '2') {

                    $superUserData = array(
                        'account_folder_id' => $account_folder_id,
                        'user_id' => $users['User']['id'],
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id'],
                        'is_coach' => 1,
                        'is_mentee' => 0
                    );
                } elseif ($this->request->data['type'] == '3') {
                    $superUserData = array(
                        'account_folder_id' => $account_folder_id,
                        'user_id' => $users['User']['id'],
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id'],
                        'is_coach' => 1,
                        'is_mentee' => 0
                    );
                }else {

                    $superUserData = array(
                        'account_folder_id' => $account_folder_id,
                        'user_id' => $users['User']['id'],
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                }

                $this->AccountFolderUser->create();
                $this->AccountFolderUser->save($superUserData);

                if ($superUsersIds != '') {
                    foreach ($superUsersIds as $supUsers) {
                        $assigned_role = $this->request->data['user_role_' . $supUsers];
                        $assigned_is_coach = isset($this->request->data['is_coach_' . $supUsers]) ? 1 : '0';
                        $assigned_is_mentee = isset($this->request->data['is_mentor_' . $supUsers]) ? 1 : '0';

                        if ($supUsers <= 0) {
                            //we need to add this user first.
                            $tmp_user_id = $this->_addUser($this->request->data['super_admin_fullname_' . $supUsers], $this->request->data['super_admin_email_' . $supUsers], $account_folder_id);
                            if ($tmp_user_id <= 0)
                                continue;
                            $supUsers = $tmp_user_id;
                        }

                        if ($assigned_role == '200')
                            $assigned_is_coach = 1;

                        $superUserData = array(
                            'account_folder_id' => $account_folder_id,
                            'user_id' => $supUsers,
                            'role_id' => $assigned_role,
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                            'is_coach' => $assigned_is_coach,
                            'is_mentee' => $assigned_is_mentee
                        );
                        $this->save_huddle_log($account_folder_id, $supUsers);
                        $this->AccountFolderUser->create();
                        $this->AccountFolderUser->save($superUserData);
                    }
                }
                if ($groupIds != '') {
                    foreach ($groupIds as $grp) {
                        $groupsData = array(
                            'account_folder_id' => $account_folder_id,
                            'group_id' => $grp,
                            'role_id' => $this->request->data['group_role_' . $grp],
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                            'is_coach' => isset($this->request->data['is_coach_' . $grp]) ? $this->request->data['is_coach_' . $grp] : '0',
                            'is_mentee' => isset($this->request->data['is_mentor_' . $grp]) ? $this->request->data['is_mentor_' . $grp] : '0',
                        );
                        $this->AccountFolderGroup->create();
                        $this->AccountFolderGroup->save($groupsData);
                    }
                }

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                if (count($huddle)) {
                    $huddleUsers = $this->AccountFolder->getHuddleUsers($huddle[0]['AccountFolder']['account_folder_id']);
                    $userGroups = $this->AccountFolderGroup->getHuddleGroups($huddle[0]['AccountFolder']['account_folder_id']);
                    $huddle_user_ids = array();
                    if ($userGroups && count($userGroups) > 0) {
                        foreach ($userGroups as $row) {
                            $huddle_user_ids[] = $row['user_groups']['user_id'];
                        }
                    }
                    if ($huddleUsers && count($huddleUsers) > 0) {
                        foreach ($huddleUsers as $row) {
                            if ($users['User']['id'] == $row['huddle_users']['user_id']) {
                                continue;
                            }
                            $huddle_user_ids[] = $row['huddle_users']['user_id'];
                        }
                    }

                    $user_ids = implode(',', $huddle_user_ids);
                    if (!empty($user_ids) && count($user_ids) > 0) {
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                        if (!empty($huddleUserInfo) && count($huddleUserInfo) > 0) {
                            $huddle[0]['participated_user'] = $huddleUsers;
                            foreach ($huddleUserInfo as $row) {
                                if ($row['User']['is_active'] == 1) {
                                    $huddle[0]['User']['email'] = $row['User']['email'];
                                    if ($this->check_subscription($row['User']['id'], '4',$account_id)) {
                                        $huddle[0]['account_info'] = $users['accounts'];
                                        $huddle[0]['huddle_type'] = (!isset($this->request->data['type']) ? "" : $this->request->data['type']);
                                        //$this->newHuddleEmail($huddle[0], $row['User']['id']);
                                    }
                                }
                            }
                        }
                    }
                }
                $this->Session->setFlash($this->request->data['hname'] . ' has been saved successfully.', 'default', array('class' => 'message success'));
                $user_activity_logs = array(
                    'ref_id' => $account_folder_id,
                    'desc' => $this->request->data['hname'],
                    'url' => $this->base . '/Huddles/view/' . $account_folder_id,
                    'account_folder_id' => $account_folder_id,
                    'date_added' => date("Y-m-d H:i:s"),
                    'type' => '1'
                );
                $this->user_activity_logs($user_activity_logs);
                return $this->redirect('/Folder/' . $folder_id);
            } else {
                $this->Session->setFlash('Huddle is not added please try again.', 'default', array('class' => 'message error'));
                $this->set('user_id', $users['id']);
                $this->set('folder_id', $folder_id);
                if ($type == 1) {
                    $this->render('collab_huddle');
                } elseif ($type == 2) {
                    $this->render('coaching_huddle');
                } elseif ($type == 3) {
                    $this->render('evaluation_huddle');
                }
            }
        } else {
            if ($type == 1) {
                $this->render('collab_huddle');
            } elseif ($type == 2) {
                $this->render('coaching_huddle');
            } elseif ($type == 3) {
                $this->render('evaluation_huddle');
            }

//            $this->render('createhuddle');
        }
    }

    function search($sort = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $this->set('user_id', $users['User']['id']);
        $result = array(
            'status' => FALSE,
            'contents' => '',
            'inHuddles' => False
        );
        $title = $_POST['title'];
        $mode = $_POST['mode'];
        $folder_id = $this->data['folder_id'];

        if (!isset($this->data['folder_id']))
            $folder_id = false;

        if (isset($_POST['observationAjax']) && $_POST['observationAjax'] != '') {
            $observationAjax = true;
        } else {
            $observationAjax = false;
        }

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $folder_label = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $folder_id), 'fields' => array('name')));

        $crumb_output = $this->add($folder_label['AccountFolder']['name'], $folder_id);

        $this->set("breadcrumb", $crumb_output);
        $hasHuddles = false;

        if ($accountFoldereIds != '0')
            $hasHuddles = true;
        $view = new View($this,false);
        $huddles = $this->AccountFolder->searchinfolder($folder_id, $account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $title);
        $folders = array();
        $other_huddles = array();
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();
                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id) && $users['roles']['role_id'] == 120) {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($participants_ids) && is_array($participants_ids) && in_array($row['Document']['created_by'], $participants_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    } elseif ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id) && ($users['roles']['role_id'] == 110 || $users['roles']['role_id'] == 100 || $users['roles']['role_id'] == 115 )) {
                       $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    }
                } else {
                    $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                }
                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    $total_resources = 0;
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $total_participant_videos = $this->Document->countVideosEvaluator_access($huddles[$i]['AccountFolder']['account_folder_id'],$user_id,$evaluators_ids);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {

                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {

                                    $total_count_r = count($this->Document->getVideoDocumentsByVideo($row['Document']['id'], $huddles[$i]['AccountFolder']['account_folder_id']));


                                    $total_resources = $total_resources + $total_count_r;
                                }
                            }
                        }
                        
                        if($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)){
                            $huddles[$i]['AccountFolder']['total_docs'] = $total_resources;
                        }else{
                            $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        }
                    }
                } else {
                    
                    
                     if($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)){
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    }else{
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    }
                }

                $get_comments = 0;
                /* $item = $this->Document->getVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                  if (count($item) > 0) {
                  for ($j = 0; $j < count($item); $j++) {
                  $get_comments = $get_comments + $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
                  }
                  $huddles[$i]['AccountFolder']['total_comments'] = $get_comments;
                  } else {
                  $huddles[$i]['AccountFolder']['total_comments'] = $get_comments;
                  } */
            }
            if ($huddles) {
                foreach ($huddles as $huddle) {

                    if ($huddle['AccountFolder']['folder_type'] == '5') {
                        //$folders[] = $huddle;
                    } else {
                        $other_huddles[] = $huddle;
                    }
                }
            }
        }

        $total_folders_for_users = $this->AccountFolder->getAllAccountFoldersComplete($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        /*if ($total_folders_for_users) {

            $accessible_folders = array();
            foreach ($total_folders_for_users as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $total_folders_for_users = $accessible_folders;
        }*/

        $folders = $this->AccountFolder->getAllAccountFoldersSearch($account_id, $sort, FALSE, false, false, $folder_id, $title);

        if ($folders) {

            $accessible_folders = array();
            foreach ($folders as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $folders = $accessible_folders;
        }

        $view = new View($this, false);
        $search_result = array(
            'video_huddles' => $other_huddles,
            'folders' => $folders,
            'total_folders_for_users' => $total_folders_for_users,
            'view_mode' => $mode,
            'sort' => 'name',
            'total_huddles' => ($huddles == false ? "0" : count($huddles)),
            'inHuddle' => $hasHuddles
        );
        if ($observationAjax) {
            echo json_encode($search_result);
            exit;
        }
        $html = $view->element('folder/ajax/ajax_huddle_search', $search_result);
        if (count($html) > 0) {
            $result = array(
                'status' => true,
                'contents' => $html
            );
        } else {
            $result['contents'] = 'No Huddles Found';
        }
        echo json_encode($result);
        exit;
    }

    function search_textbox($sort = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $this->set('user_id', $users['User']['id']);
        $result = array(
            'status' => FALSE,
            'contents' => '',
            'inHuddles' => False
        );
        $title = $_POST['title'];
        $mode = $_POST['mode'];
        $folder_id = $this->data['folder_id'];

        if (!isset($this->data['folder_id']))
            $folder_id = false;

        if (isset($_POST['observationAjax']) && $_POST['observationAjax'] != '') {
            $observationAjax = true;
        } else {
            $observationAjax = false;
        }

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $folder_label = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $folder_id), 'fields' => array('name')));

        $crumb_output = $this->add($folder_label['AccountFolder']['name'], $folder_id);

        $this->set("breadcrumb", $crumb_output);
        $hasHuddles = false;

        if ($accountFoldereIds != '0')
            $hasHuddles = true;


        $huddles = $this->AccountFolder->searchinfolder_textbox($folder_id, $account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $title);

        $view = new View($this,false);
        $folders = array();
        $other_huddles = array();
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();
                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id) && $users['roles']['role_id'] == 120) {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($participants_ids) && is_array($participants_ids) && in_array($row['Document']['created_by'], $participants_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    } elseif ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id) && ($users['roles']['role_id'] == 110 || $users['roles']['role_id'] == 100 || $users['roles']['role_id'] == 115 )) {
                       $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    }
                } else {
                    $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                }
                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    $total_resources = 0;
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $total_participant_videos = $this->Document->countVideosEvaluator_access($huddles[$i]['AccountFolder']['account_folder_id'],$user_id,$evaluators_ids);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {

                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {

                                    $total_count_r = count($this->Document->getVideoDocumentsByVideo($row['Document']['id'], $huddles[$i]['AccountFolder']['account_folder_id']));


                                    $total_resources = $total_resources + $total_count_r;
                                }
                            }
                        }
                        
                        if($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)){
                            $huddles[$i]['AccountFolder']['total_docs'] = $total_resources;
                        }else{
                            $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        }
                    }
                } else {
                    if($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)){
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    }else{
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    }
                }
                $get_comments = 0;
                /* $item = $this->Document->getVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                  if (count($item) > 0) {
                  for ($j = 0; $j < count($item); $j++) {
                  $get_comments = $get_comments + $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
                  }
                  $huddles[$i]['AccountFolder']['total_comments'] = $get_comments;
                  } else {
                  $huddles[$i]['AccountFolder']['total_comments'] = $get_comments;
                  } */
            }
            if ($huddles) {
                foreach ($huddles as $huddle) {

                    if ($huddle['AccountFolder']['folder_type'] == '5') {
                        //$folders[] = $huddle;
                    } else {
                        $other_huddles[] = $huddle;
                    }
                }
            }
        }

        $folders = $this->AccountFolder->getAllAccountFoldersSearch_textbox($account_id, $sort, FALSE, false, false, $folder_id, $title);

        if ($folders) {

            $accessible_folders = array();
            foreach ($folders as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $folders = $accessible_folders;
        }


        $total_folders_for_users = $this->AccountFolder->getAllAccountFoldersComplete($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);


        /*if ($total_folders_for_users) {

            $accessible_folders = array();
            foreach ($total_folders_for_users as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $total_folders_for_users = $accessible_folders;
        }*/

        $view = new View($this, false);
        $search_result = array(
            'video_huddles' => $other_huddles,
            'folders' => $folders,
            'total_folders_for_users' => $total_folders_for_users,
            'view_mode' => $mode,
            'sort' => 'name',
            'total_huddles' => ($huddles == false ? "0" : count($huddles)),
            'inHuddle' => $hasHuddles
        );
        if ($observationAjax) {
            echo json_encode($search_result);
            exit;
        }
        $html = $view->element('folder/ajax/ajax_huddle_search', $search_result);
        if (count($html) > 0) {
            $result = array(
                'status' => true,
                'contents' => $html
            );
        } else {
            $result['contents'] = 'No Huddles Found';
        }
        echo json_encode($result);
        exit;
    }

    public function movehuddletofolder() {

        $huddles = $this->request->data['huddle'];
        $present_folder_id = $this->request->data['present_id'];

        $move_to_folder = $this->request->data['move_to_folder'];
        if ($move_to_folder == 0)
            $move_to_folder = 'NULL';
        $return = $this->AccountFolder->save_huddle_to_new_folder($huddles, $move_to_folder);
        //$return = $this->get_folder_info($present_folder_id);
        echo $return;
        exit;
    }

    public function refresh_folders_stats($folder_id) {
        $account_id = $this->request->data['account_id'];
        $sort = $this->request->data['sort'];
        $user_id = $this->request->data['user_id'];

        $folders = $this->AccountFolder->getAllAccountFolders($account_id, $sort, FALSE, false, false, '');

        if ($folders) {

            $accessible_folders = array();
            foreach ($folders as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $folders = $accessible_folders;
        }



        $result = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $folder_id)));
        $search_result = array(
            'row' => $result,
            'user_id' => $user_id,
            'folders' => $folders
        );

        $view = new View($this, false);
        $html = $view->element('folder/ajax/refresh_folders_contents', $search_result);
        echo json_encode($html);
        die();
    }

    function get_folder_info($folder_id = 0, $sort = '') {
        $result = array(
            'status' => FALSE,
            'contents' => '',
            'inHuddles' => False
        );
        $title = '';
        $mode = $this->Session->read('view_mode');
        if ($sort == '')
            $sort = $this->Session->read('sort_type');
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $hasHuddles = false;
        $folder_label = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $folder_id), 'fields' => array('name')));
        if (is_array($folder_label) && count($folder_label) > 0) {
            $crumb_output = $this->add($folder_label['AccountFolder']['name'], $folder_id);
            $this->set("breadcrumb", $crumb_output);
        }
        if ($accountFoldereIds != '0')
            $hasHuddles = true;

        $huddles = $this->AccountFolder->searchinfolder($folder_id, $account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $title);
        $folders = array();
        $other_huddles = array();
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();
                $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                $item = $this->Document->getVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                $get_comments = 0;
                if (count($item) > 0) {
                    for ($j = 0; $j < count($item); $j++) {
                        $get_comments = $get_comments + $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
                    }
                    $huddles[$i]['AccountFolder']['total_comments'] = $get_comments;
                } else {
                    $huddles[$i]['AccountFolder']['total_comments'] = $get_comments;
                }
            }
            if ($huddles) {
                foreach ($huddles as $huddle) {
                    if ($huddle['AccountFolder']['folder_type'] == '5') {
                        $folders[] = $huddle;
                    } else {
                        $other_huddles[] = $huddle;
                    }
                }
            }
        }
        $view = new View($this, false);
        $search_result = array(
            'video_huddles' => $other_huddles,
            'folders' => $folders,
            'view_mode' => $mode,
            'sort' => 'name',
            'total_huddles' => ($huddles == false ? "0" : count($huddles)),
            'inHuddle' => $hasHuddles,
            'user_id' => $user_id,
            'account_id' => $account_id
        );
        if (is_array($folder_label) && count($folder_label) > 0) {
            $html = $view->element('folder/ajax/ajax_huddle_search', $search_result);
        } else {
            $html = $view->element('huddles/ajax/ajax_huddle_search', $search_result);
        }

        if (count($html) > 0) {
            $result = array(
                'status' => true,
                'contents' => $html
            );
        } else {
            $result['contents'] = 'No Huddles Found';
        }
        return json_encode($result);
    }

    public function draghuddletofolder() {
        $huddles_id = $this->request->data['huddle_id'];
        $folder_id = $this->request->data['folder_id'];
        $huddles[] = $huddles_id;
        $return = $this->AccountFolder->save_huddle_to_new_folder($huddles, $folder_id);
        echo json_encode($return);
        exit;
    }

    public function treeview_detail($id = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];


        $folders = array();
        $root_ary = array(array('id' => 0, 'text' => 'Huddles', 'parent' => '#'));
        $folders = $this->AccountFolder->find("all", array("conditions" => array(
                'account_id' => $account_id, 'active' => '1', 'folder_type' => '5'
        )));

        $afolder = array();
        if ($folders) {

            $accessible_folders = array();
            foreach ($folders as $folder) {

                //if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                $afolder['AccountFolder']['id'] = $folder['AccountFolder']['account_folder_id'];
                $afolder['AccountFolder']['text'] = $folder['AccountFolder']['name'];
                $afolder['AccountFolder']['parent'] = $folder['AccountFolder']['parent_folder_id'];
                $accessible_folders[] = $afolder;
                //}
            }

            $folders = $accessible_folders;
        }

        $data = Set::extract('/AccountFolder/.', $folders);
        foreach ($data as $key => &$item) {

            if ($item['parent'] == null || empty($item['parent']))
                $item['parent'] = '#';
        }
        if ($id != '') {

            $folder_childs_ids = array();

            /* $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
              'parent_folder_id' => $id, 'active' => '1', 'folder_type' => '5'
              )));


              if (!empty($folder_childs)) {
              foreach ($folder_childs as $folder_child) {
              $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];

              $folder_childs_ids = array_merge($folder_childs_ids, $this->child_finder($folder_child['AccountFolder']['account_folder_id'], $folder_childs_ids));
              }
              }


              //print_r(array_unique($folder_childs_ids));die();

              $folder_childs_ids = array_unique($folder_childs_ids); */



            $new_data = array();
            $array = array('id' => 0, 'text' => 'Huddles', 'parent' => '#');
            $new_data[] = $array;

            foreach ($data as $dat) {
                if ($dat['id'] != $id && !in_array($dat['id'], $folder_childs_ids)) {
                    $new_data[] = $dat;
                }
            }


            //$data1 = array_merge($root_ary, $data);
            echo json_encode($new_data);
            die;
        } else {
            $data1 = array_merge($root_ary, $data);
            echo json_encode($new_data);
            die;
        }
    }

    function child_finder($id, $folder_childs_ids) {

        $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
                'parent_folder_id' => $id, 'active' => '1', 'folder_type' => '5'
        )));


        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];

                $folder_childs_ids = array_merge($folder_childs_ids, $this->child_finder($folder_child['AccountFolder']['account_folder_id'], $folder_childs_ids));
            }
        }

        return $folder_childs_ids;
    }

    function editajax($huddle_id) {
        $huddle = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));
        echo json_encode($huddle['AccountFolder']);
        die;
    }

    function delete($huddle_id) {
        $this->auth_huddle_permission('delete', $huddle_id);
        if ($huddle_id == Configure::read('sample_huddle')) {
            $this->Session->setFlash('Cannot delete sample huddle.', 'default', array('class' => 'message'));
            $this->redirect('/Huddles');
            return;
        }

        $result = $this->AccountFolder->find('all', array('conditions' => array('folder_type' => array('1', '5'), 'parent_folder_id' => $huddle_id, 'active' => '1')));

        if (count($result) > 0) {
            echo 'contentexists';
            die();
        }


        $this->AccountFolder->create();
        $data = array(
            'active' => 0
        );
        $this->AccountFolder->updateAll($data, array('account_folder_id' => $huddle_id), $validation = TRUE);

        /*
        if ($this->AccountFolder->getAffectedRows() > 0) {
            $this->UserActivityLog->deleteAll(array('account_folder_id' => $huddle_id), $cascade = true, $callbacks = true);
            $folderorHuddle = true;
        }
        */

        $this->AccountFolder->create();
        $data = array(
            'active' => 0
        );

        $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
                'parent_folder_id' => $huddle_id
        )));


        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];
                $this->delete_recursive($folder_child['AccountFolder']['account_folder_id']);
            }

            $this->AccountFolder->updateAll($data, array('parent_folder_id' => $huddle_id), $validation = TRUE);
            /*
            if ($this->AccountFolder->getAffectedRows() > 0) {
                foreach ($folder_childs_ids as $folder_childs_id) {
                    $this->UserActivityLog->deleteAll(array('account_folder_id' => $folder_childs_id), $cascade = true, $callbacks = true);
                    $folderorHuddle = true;
                }
            }
            */
        }

        die;

        $this->Session->setFlash('Folder has been deleted successfully', 'default', array('class' => 'message success'));


        // $this->redirect('/Huddles');
    }

    function delete_recursive($huddle_id) {
        $this->auth_huddle_permission('delete', $huddle_id);
        if ($huddle_id == Configure::read('sample_huddle')) {
            $this->Session->setFlash('Cannot delete sample huddle.', 'default', array('class' => 'message'));
            $this->redirect('/Huddles');
            return;
        }
        $this->AccountFolder->create();
        $data = array(
            'active' => 0
        );
        $this->AccountFolder->updateAll($data, array('account_folder_id' => $huddle_id), $validation = TRUE);

        /*
        if ($this->AccountFolder->getAffectedRows() > 0) {
            $this->UserActivityLog->deleteAll(array('account_folder_id' => $huddle_id), $cascade = true, $callbacks = true);
            $folderorHuddle = true;
        }
        */

        $this->AccountFolder->create();
        $data = array(
            'active' => 0
        );

        $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
                'parent_folder_id' => $huddle_id
        )));
        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];
                $this->delete_recursive($folder_child['AccountFolder']['account_folder_id']);
            }

            $this->AccountFolder->updateAll($data, array('parent_folder_id' => $huddle_id), $validation = TRUE);
            /*
            if ($this->AccountFolder->getAffectedRows() > 0) {
                foreach ($folder_childs_ids as $folder_childs_id) {
                    $this->UserActivityLog->deleteAll(array('account_folder_id' => $folder_childs_id), $cascade = true, $callbacks = true);
                    $folderorHuddle = true;
                }
            }
            */
        }
    }
    
    function get_folder_childs($folder_id) {
        $results = $this->AccountFolder->query("SELECT 
         af.`account_folder_id` 
          , af1.`account_folder_id`
           , af2.`account_folder_id` 
           , af3.`account_folder_id` 
            , af4.`account_folder_id` 
             , af5.`account_folder_id`
             , af6.`account_folder_id` 
        FROM
          `account_folders` af  
          LEFT JOIN `account_folders` af1 
            ON (af.`account_folder_id` = af1.`parent_folder_id` AND af1.`folder_type` = 5 AND af1.`active` =1   AND af.`account_id` = af1.`account_id`) 
            LEFT JOIN `account_folders` af2
            ON (af1.`account_folder_id` = af2.`parent_folder_id`  AND af2.`folder_type` = 5 AND af2.`active` =1  AND af2.`active` =1  AND af1.`account_id` = af2.`account_id` )
                LEFT JOIN `account_folders` af3
            ON (af2.`account_folder_id` = af3.`parent_folder_id` AND af3.`folder_type` = 5 AND af3.`active` =1  AND af3.`active` =1  AND af2.`account_id` = af3.`account_id`)
                LEFT JOIN `account_folders` af4
            ON (af3.`account_folder_id` = af4.`parent_folder_id` AND af4.`folder_type` = 5 AND af4.`active` =1 AND af3.`account_id` = af4.`account_id`)
                LEFT JOIN `account_folders` af5
            ON (af4.`account_folder_id` = af5.`parent_folder_id` AND af5.`folder_type` = 5 AND af5.`active` =1  AND af4.`account_id` = af5.`account_id`) 
                LEFT JOIN `account_folders` af6
            ON (af5.`account_folder_id` = af6.`parent_folder_id` AND af5.`folder_type` = 5 AND af5.`active` =1  AND af5.`active` =1  AND af5.`account_id` = af6.`account_id`) 

            WHERE af.`account_folder_id` = ".$folder_id."
                     AND af.`active` = 1
                     AND af.site_id = ".$this->site_id." 
                     AND af.`folder_type` = 5");


                $account_folder_ids = array();

                foreach ($results as $result) {

                    if (isset($result['af']['account_folder_id']) && $result['af']['account_folder_id'] != '') {
                        $account_folder_ids[$result['af']['account_folder_id']] = $result['af']['account_folder_id'];
                    } if (isset($result['af1']['account_folder_id']) && $result['af1']['account_folder_id'] != '') {
                        $account_folder_ids[$result['af1']['account_folder_id']] = $result['af1']['account_folder_id'];
                    } if (isset($result['af2']['account_folder_id']) && $result['af2']['account_folder_id'] != '') {
                        $account_folder_ids[$result['af2']['account_folder_id']] = $result['af2']['account_folder_id'];
                    } if (isset($result['af3']['account_folder_id']) && $result['af3']['account_folder_id'] != '') {
                        $account_folder_ids[$result['af3']['account_folder_id']] = $result['af3']['account_folder_id'];
                    } if (isset($result['af4']['account_folder_id']) && $result['af4']['account_folder_id'] != '') {
                        $account_folder_ids[$result['af4']['account_folder_id']] = $result['af4']['account_folder_id'];
                    } if (isset($result['af5']['account_folder_id']) && $result['af5']['account_folder_id'] != '') {
                        $account_folder_ids[$result['af5']['account_folder_id']] = $result['af5']['account_folder_id'];
                    } if (isset($result['af6']['account_folder_id']) && $result['af6']['account_folder_id'] != '') {
                        $account_folder_ids[$result['af6']['account_folder_id']] = $result['af6']['account_folder_id'];
                    }
                }


                return $account_folder_ids;

            }

}
