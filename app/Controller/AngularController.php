<?php

//App::uses('Subscription', 'Braintree');

class AngularController extends AppController {
    

    public function Index()
    {
        $view = new View($this, false);
      
        $user_current_account = $this->Session->read('user_current_account');
        
        $user_permissions = $this->Session->read('user_permissions');
        
        $lst_login = $this->Session->read('last_logged_in_user');
        
        if(!$this->Auth->loggedIn())
        {
            $result_array = array (
                'status'=> false
                );
            
           // echo json_encode($result_array);die;
            
        }
        else{


        $logo_image = $view->Custom->getSecureSibmeResouceUrl($this->base . '/img/new/logo-3.png');

        $header_background_color = "#336699";
        $nav_bg_color = "#668dab";
        $logo_background_color = "#fff";

        if (!empty($user_current_account['accounts']['header_background_color'])) {
            $header_background_color = "#" . $user_current_account['accounts']['header_background_color'];
        }

        if (!empty($user_current_account['accounts']['nav_bg_color'])) {
            $nav_bg_color = "#" . $user_current_account['accounts']['nav_bg_color'];
        }

        if (!empty($user_current_account['accounts']['usernav_bg_color'])) {
            $logo_background_color = "#" . $user_current_account['accounts']['usernav_bg_color'];
        }

        $logo_path = $view->Custom->getSecureSibmecdnImageUrl("static/companies/" . $user_current_account['accounts']['account_id'] . "/" . $user_current_account['accounts']['image_logo'], $user_current_account['accounts']['image_logo']);

        if (!empty($user_current_account['accounts']['image_logo'])) {
            $logo_image = $logo_path;
        }
        
        $is_change_account_enable = 0;
        
        $allAccounts = $this->Session->read('user_accounts');
        
        if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1 && isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1)
        {
           $is_change_account_enable = 1; 
        }
        $is_impersonate = 0 ;
        
        if (!empty($lst_login['id']))
        {
           $is_impersonate = 1; 
        }
        
        $date = new DateTime($user_current_account['accounts']['created_at']); // Y-m-d
        $date->add(new DateInterval('P30D'));
        $expiry_date = $date->format('m/d/Y');
        
         $user_img = $this->Session->read('user_top_photo');
            if($user_img){
                $user_photo =  $user_img;
                $avatar_path = $view->Custom->getSecureSibmecdnImageUrl("static/users/" . $user_current_account['User']['id'] . "/" . $user_photo, $user_photo);
            }else{
                $user_photo =  $user_current_account['User']['image'];
                $avatar_path = $view->Custom->getSecureSibmecdnImageUrl("static/users/" . $user_current_account['User']['id'] . "/" . $user_photo, $user_photo);
            }
            
            $library_permission = 0;
            if ($user_permissions['UserAccount']['role_id'] == '100' || $user_permissions['UserAccount']['role_id'] == '110' || ($user_permissions['UserAccount']['role_id'] == '120' && $user_permissions['UserAccount']['permission_access_video_library'] == '1') || ($user_permissions['UserAccount']['role_id'] == '115' && $user_permissions['UserAccount']['permission_access_video_library'] == '1') || ($user_permissions['UserAccount']['role_id'] == '125' && $user_permissions['UserAccount']['permission_access_video_library'] == '1') )
            {
                if ($view->Custom->get_account_video_permissions($user_current_account['accounts']['account_id']))
                {
                    $library_permission = 1;
                }
            }
        
        $result_array = array (
            'user_current_account' => $user_current_account,
            'user_permissions' => $user_permissions,
            'logo_image' => $logo_image,
            'header_background_color' => $header_background_color,
            'nav_bg_color' => $nav_bg_color,
            'logo_background_color' => $logo_background_color,
            'is_change_account_enable' => $is_change_account_enable,
            'base_url' =>  isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://'.$_SERVER['HTTP_HOST'],
            'expiry_date' => $expiry_date,
            'status' => true,
            'is_impersonate' => $is_impersonate,
            'avatar_path' => $avatar_path,
            'library_permission' => $library_permission
            
            
            
        );
        if(!empty($lst_login['id']))
        {
            $result_array['last_login_id'] = $lst_login['id'];
        }
        }
        $this->set('settings', $result_array);
        $this->layout = 'angular_layout';
    }


}