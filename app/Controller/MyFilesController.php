<?php

App::uses('Sanitize', 'Utility');

class MyFilesController extends AppController {

    public $name = 'MyFiles';
    var $helpers = array('Custom', 'Browser');
    public $uses = array('AccountTag', 'AccountCommentTag', 'DocumentFiles', 'Comment', 'AssignmentSubmission', 'CommentAttachment');

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $this->layout = 'huddles';
    }

    function beforeFilter() {

        $users = $this->Session->read('user_current_account');
        $this->set('logged_in_user', $users);
        $this->user_id = $users['User']['id'];

        parent::beforeFilter();

        if ($this->Auth->loggedIn()) {
            $this->permissionOnWorkSpace();
        }
        $this->check_is_account_expired();
        $this->is_account_activated($users['users_accounts']['account_id'], $users['User']['id']);

        $this->set('AccountFolder', $this->AccountFolder);
        $this->set('AcccountFolderUser', $this->AcccountFolderUser);
        $this->set('Comment', $this->Comment);
        $this->set('Document', $this->Document);
        $this->set('AccountFolderGroup', $this->AccountFolderGroup);
        $users = $this->Session->read('user_current_account');
        $totalAccounts = $this->Session->read('totalAccounts');
        if ($users['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
            $this->redirect_on_account_settings();
        }
    }

    function permissionOnWorkSpace() {
        $user = $this->Session->read('user_current_account');
        $check_users_permissions = $this->UserAccount->find('first', array('conditions' => array('account_id' => $user['users_accounts']['account_id'], 'user_id' => $user['users_accounts']['user_id'])));

        if ($check_users_permissions['UserAccount']['parmission_access_my_workspace'] == 1 && $check_users_permissions['UserAccount']['role_id'] != '125') {
            return true;
        } else {
           if(isset($this->request->data['req_type']) && $this->request->data['req_type'] =='edtpa'){
            echo json_encode(array('success'=>false,'result'=>''));
            die;
           }
            $this->no_permissions();
        }
    }

    function index($files_mode = '', $tab = 1, $video_id = '') {
        //$this->view(1);
        $view = new View($this, false);
        return $view->Custom->redirectHTTPS('workspace');

        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('MyFiles');
        $this->set('language_based_content', $language_based_content);

        /* if (empty($files_mode) || $files_mode == 'grid') {
          $this->set('files_mode', $files_mode);
          } elseif (!empty($files_mode) && $files_mode == 'list') {
          $this->set('files_mode', $files_mode);
          } */

        $view_m = $this->Session->read('files_mode');
        if (empty($view_m) && empty($files_mode))
            $this->Session->write('files_mode', 'grid');
        if (empty($view_m) && !empty($files_mode))
            $this->Session->write('files_mode', $files_mode);
        if (!empty($view_m) && empty($files_mode))
            $this->Session->write('files_mode', $view_m);
        if (!empty($view_m) && !empty($files_mode))
            $this->Session->write('files_mode', $files_mode);

        $files_mode = $this->Session->read('files_mode');
        $this->set('files_mode', $files_mode);

        $video_doc_id = 0;

        if ($files_mode == 'list') {
            $limit = 10;
        } else {
            $limit = 12;
        }

        $MyFilesVideos = $this->Document->getMyFilesVideos($account_id, $user_id, "", "Document.created_date DESC", $limit, 0, '1, 3');
        $MyFilesDocuments = $this->AccountFolder->getAllMyFiles($account_id, $user_id, '2');

        $changeType = isset($this->data['srtType']) ? $this->data['srtType'] : 1;

        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_current_account = $this->Session->read('user_current_account');
        $huddle_id = $video_id;

        $users = $this->Session->read('user_current_account');

        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);


        $video_details = '';
        $commentsTime = '';
        $vidDocuments = '';
        $videoComments = '';
        $comments_result = '';

        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        //$huddles = $this->AccountFolder->copyAllHuddles($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $huddles = false;
        $export_pdf_url = Configure::read('sibme_base_url') . 'Huddles/print_pdf_comments/' . $video_doc_id . '/' . $changeType . '/2.pdf';
        $export_excel_url = Configure::read('sibme_base_url') . 'Huddles/print_excel_comments/' . $video_doc_id . '/' . $changeType;
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $params_comments = array(
            'user_id' => $user_id,
            'video_id' => $video_doc_id,
            'videoComments' => !empty($videoComments) ? $videoComments : '',
            'huddle_permission' => $huddle_permission,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account,
            'auto_scroll_switch' => $type_pause['UserAccount']['autoscroll_switch'],
            'notifiable' => 0,
            'sortType' => $changeType,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'type' => 'get_video_comments',
            'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                    "account_id" => $account_id,
                    "tag_type" => '1'
        )))
        );

        $view = new View($this, false);
        $html_comments = $view->element('MyFiles/ajax/vidComments', $params_comments);


        $item = $MyFilesVideos;
        $item_count = $this->Document->countMyFilesVideos($account_id, $user_id, "", '1,3');

        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }
        $message = $this->language_based_messages['no_video_have_been_uploaded_to_your_workspace_msg'];
        $element_data = array(
            'files' => $item,
            'total_videos' => $item_count,
            'files_mode' => $files_mode,
            'message' => $message
        );

        $view = new View($this, false);
        $videos_html = $view->element('MyFiles/ajax/videoSearch', $element_data);

        $myFilesData = array(
            'videos' => $MyFilesVideos,
            'user_id' => $user_id,
            'videoDetail' => $video_details,
            'videoComments' => $commentsTime,
            'huddle_id' => $video_id,
            'video_id' => $video_doc_id,
            'vidDocuments' => $vidDocuments,
            'videos_html' => $videos_html,
            'videos_total' => $item_count,
            'show_ff_message' => false,
            'videoCommentsArray' => !empty($videoComments) ? $videoComments : '',
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'html_comments' => $html_comments,
            'language_based_content' => $language_based_content,
            'tab' => $tab,
            'tags' => $this->AccountTag->find("all", array("conditions" => array(
                    "account_id" => $account_id,
                    "tag_type" => '1'))),
        );

        $document_id = '';
        if (isset($video_details['afd']['id']) && $video_details['afd']['id'] != '') {
            $document_id = $video_details['afd']['id'];
        }

        $element_data = array(
            'video_id' => $video_id,
            'user_id' => $user_id,
            'tab' => $tab,
            'videos' => $MyFilesVideos,
        );

        $isFirefoxBrowser = $view->Browser->isFirefox();
        if ($isFirefoxBrowser) {
            $trim_message_displayed = $this->Cookie->read('flash_message_ff_trim');
            if (!isset($trim_message_displayed) || empty($trim_message_displayed)) {
                echo $trim_message_displayed;
                $myFilesData['show_ff_message'] = true;
                $this->Cookie->write('flash_message_ff_trim', "displayed", false, '2 weeks');
            }
        }

        $associatedVideos = $this->Document->getDocumentAssociatedVideosForWorkspace($account_id, $user_id);
        $associatedVideoMap = array();
        foreach ($associatedVideos as $v) {
            $associatedVideoMap[$v['Document']['id']][$v['afda']['attach_id']] = 1;
        }

        $doc_bool = 1;
        if (empty($MyFilesDocuments)) {
            $doc_bool = 0;
        }
        $document_tab_data = array(
            'documents' => $MyFilesDocuments,
            'videos' => $MyFilesVideos,
            'docs_html' => $this->getDocSearchHtml(),
            'associatedVideoMap' => $associatedVideoMap,
            'tab' => $tab,
            'doc_bool' => $doc_bool,
            'language_based_content' => $language_based_content
        );
        $this->set('video_count', $item_count);
        if (!empty($MyFilesDocuments)) {
            $this->set('documents_count', count($MyFilesDocuments));
        } else {
            $this->set('documents_count', '0');
        }
        $model_data = array("present_account_id" => $account_id, "all_accounts" => $this->Session->read('user_accounts'), 'allHuddles' => $huddles, 'tab' => $tab, 'document_id' => $document_id, 'video_id' => $video_id);
        $this->set('video_tab', $view->element('MyFiles/videos', $myFilesData));
        $this->set('documents_tab', $view->element('MyFiles/documents', $document_tab_data));
        $this->set('tab', $tab);
        $this->set('reveal_model', $view->element('MyFiles/models', $model_data));
        $this->set('account_id', $account_id);
        //  $this->set('video_huddle_model', $view->element('MyFilesVideoHolder', $element_data));
        $this->set('live_recording', $this->check_if_live_recording());
        $latest_video_created_date = $this->UserAccount->query("SELECT
              d.`created_date`
            FROM
              `account_folders` AS af
              JOIN `account_folder_documents` AS afd
                ON afd.`account_folder_id` = af.`account_folder_id`
              JOIN `documents` AS d
                ON d.`id` = afd.`document_id`
            WHERE d.`doc_type` IN (1, 3)
              AND af.`folder_type` = 3
              AND af.`account_id` = " . $account_id . "
              AND af.site_id=" . $this->site_id . "
            ORDER BY d.`id` DESC
            LIMIT 1 ");
        $latest_video_created_date = $latest_video_created_date[0]['d']['created_date'];
        $this->set('latest_video_created_date', $latest_video_created_date);
        $this->set('main_workspace', '1');
        $this->render('index');
    }

    function view($tab, $video_id = '', $folder_id = '') {
        if(empty($video_id))
        {
            $this->redirect('/workspace/workspace/home/grid');
        }
        $view = new View($this, TRUE);
        $language_based_content = $view->Custom->get_page_lang_based_content('MyFiles');
        $this->set('language_based_content', $language_based_content);

        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $files_mode = $this->Session->read('files_mode');
        $this->set('files_mode', $files_mode);
        $video_doc_id = 0;

        $MyFilesVideos = $this->AccountFolder->getAllMyFiles($account_id, $user_id, '1,3');
        $MyFilesDocuments = $this->AccountFolder->getAllMyFiles($account_id, $user_id, '2');



        $changeType = isset($this->data['srtType']) ? $this->data['srtType'] : 5;

        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_current_account = $this->Session->read('user_current_account');
        $huddle_id = $video_id;

        $users = $this->Session->read('user_current_account');
        if ($huddle_id != '') {
            $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        } else {
            $huddle = false;
        }

        if (!empty($video_id)) {
            $video_details = $this->AccountFolder->getAllMyFile($account_id, $user_id, $video_id);

            if ($video_details['doc']['doc_type'] == 3 && $video_details['doc']['is_processed'] < 4) {
                // $this->redirect('/workspace_video/video_observation/' . $video_id . '/' . $video_details['doc']['id']);
                $this->redirect('/video_details/scripted_observations/' . $video_id . '/' . $video_details['doc']['id'] . '?workspace=true');
            } elseif (($video_details['doc']['doc_type'] == 3 && ($video_details['doc']['is_processed'] == 4 || $video_details['doc']['is_processed'] == 5))) {
                if($video_details['doc']["upload_progress"] >= 0 &&$video_details['doc']["upload_progress"] < 100 && $video_details['doc']["upload_status"] != 'cancelled' && $video_details['doc']["upload_status"] != 'uploaded')
                {
                    $this->redirect('/workspace_video/video_observation/' . $video_id . '/' . $video_details['doc']['id']);
                }
                else
                {
                    $this->redirect('/workspace_video/home/' . $video_id . '/' . $video_details['doc']['id']);
                }
            }
            else
            {
                $this->redirect('/workspace_video/home/' . $video_id . '/' . $video_details['doc']['id']);
            }
        }

        if(empty($video_id))
        {
            $this->redirect('/workspace/workspace/home/grid');
        }

        if ($video_id != '') {
            $this->Session->write('video_id', $video_id);
            $video_details = $this->AccountFolder->getAllMyFile($account_id, $user_id, $video_id);
            if ($video_details == false) {
                $this->Session->setFlash($this->language_based_messages['video_you_are_trying_to_watch_not_available_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/dashboard/home');
                exit;
            }

            $vidDocuments = $this->Document->getVideoDocumentsByVideo($video_details['doc']['id'], $video_id);

            $srtType = $this->Session->read('srtType_myfiles');
            $commentsTime = $this->Comment->getVideoComments($video_details['doc']['id'], $srtType, '', '', '', 1);
            $comments_result = $this->Comment->getVideoComments($video_details['doc']['id'], $srtType, '', '', '', 1);
            $comments = array();
            if (!empty($comments_result)) {
                foreach ($comments_result as $comment) {
                    $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get standards
                    $comments[] = array_merge($comment, array("default_tags" => $get_tags));
                }
            }
            $videoComments = $comments;
            $video_doc_id = $video_details['doc']['id'];

            $user_activity_logs = array(
                'ref_id' => $video_id,
                'desc' => 'View video',
                'url' => $this->base . '/MyFiles/view/1/' . $video_id,
                'type' => '12',
                'account_folder_id' => '',
                'environment_type' => '2',
            );
            $this->user_activity_logs($user_activity_logs);
        } else {
            $video_details = '';
            $commentsTime = '';
            $vidDocuments = '';
            $comments_result = '';
        }
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        //$huddles = $this->AccountFolder->copyAllHuddles($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $huddles = false;

//        $videoComments = '';
//        if (is_array($comments_result) && count($comments_result) > 0) {
//            $videoComments = $comments_result;
//        } else {
//            $videoComments = '';
//        }




        $export_pdf_url = Configure::read('sibme_base_url') . 'Huddles/print_pdf_comments/' . $video_doc_id . '/' . $changeType . '/2.pdf';
        $export_excel_url = Configure::read('sibme_base_url') . 'Huddles/print_excel_comments/' . $video_doc_id . '/' . $changeType;
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));

        $params_comments = array(
            'videos' => $video_details,
            'user_id' => $user_id,
            'video_id' => $video_doc_id,
            'videoComments' => !empty($videoComments) ? $videoComments : '',
            'huddle_permission' => $huddle_permission,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account,
            'auto_scroll_switch' => $type_pause['UserAccount']['autoscroll_switch'],
            'notifiable' => 0,
            'sortType' => $changeType,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'type' => 'get_video_comments',
            'language_based_content' => $language_based_content,
            'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                    "account_id" => $account_id,
                    "tag_type" => '1'
        )))
        );

        $view = new View($this, false);
        $html_comments = $view->element('MyFiles/ajax/vidComments', $params_comments);

        if ($files_mode == 'list') {
            $limit = 10;
        } else {
            $limit = 12;
        }

        $item = $this->Document->getMyFilesVideos($account_id, $user_id, "", "Document.created_date DESC", $limit, 0, '1,3');

        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }
        $element_data = array(
            'files' => $item,
            'total_videos' => $this->Document->countMyFilesVideos($account_id, $user_id, "", '1,3')
        );
        $total_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $video_details['doc']['id'])));

        $view = new View($this, false);
        $videos_html = $view->element('MyFiles/ajax/videoSearch', $element_data);
        $document_files = $this->DocumentFiles->find('first', array('conditions' => array('document_id' => $video_details['doc']['id'])));

        $myFilesData = array(
            'videos' => $MyFilesVideos,
            'user_id' => $user_id,
            'document_files' => $document_files,
            'videoDetail' => $video_details,
            'videoComments' => $commentsTime,
            'huddle_id' => $video_id,
            'video_id' => $video_doc_id,
            'vidDocuments' => $vidDocuments,
            'videos_html' => $videos_html,
            'videos_total' => $this->Document->countMyFilesVideos($account_id, $user_id, "", '1,3'),
            'show_ff_message' => false,
            'videoCommentsArray' => !empty($videoComments) ? $videoComments : '',
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'html_comments' => $html_comments,
            'tab' => $tab,
            'language_based_content' => $language_based_content,
            'tags' => $this->AccountTag->find("all", array("conditions" => array(
                    "account_id" => $account_id,
                    "tag_type" => '1'))),
        );

        $document_id = '';
        if (isset($video_details['afd']['id']) && $video_details['afd']['id'] != '') {
            $document_id = $video_details['afd']['id'];
        }

        $element_data = array(
            'video_id' => $video_id,
            'user_id' => $user_id,
            'tab' => $tab,
            'videos' => $MyFilesVideos,
        );

        $isFirefoxBrowser = $view->Browser->isFirefox();
        if ($isFirefoxBrowser) {
            $trim_message_displayed = $this->Cookie->read('flash_message_ff_trim');
            if (!isset($trim_message_displayed) || empty($trim_message_displayed)) {
                echo $trim_message_displayed;
                $myFilesData['show_ff_message'] = true;
                $this->Cookie->write('flash_message_ff_trim', "displayed", false, '2 weeks');
            }
        }

        //if($video_id == ''){
        $associatedVideos = $this->Document->getDocumentAssociatedVideosForWorkspace($account_id, $user_id);
        $associatedVideoMap = array();
        foreach ($associatedVideos as $v) {
            $associatedVideoMap[$v['Document']['id']][$v['afda']['attach_id']] = 1;
        }

        $doc_bool = 1;
        if (empty($MyFilesDocuments)) {
            $doc_bool = 0;
        }

        $document_tab_data = array(
            'documents' => $MyFilesDocuments,
            'videos' => $MyFilesVideos,
            'docs_html' => $this->getDocSearchHtml(),
            'associatedVideoMap' => $associatedVideoMap,
            'tab' => $tab,
            'doc_bool' => $doc_bool,
            'language_based_content' => $language_based_content
        );
        $this->set('documents_tab', $view->element('MyFiles/documents', $document_tab_data));
        //}
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));

        $model_data = array("present_account_id" => $account_id, "all_accounts" => $this->Session->read('user_accounts'), 'allHuddles' => $huddles, 'tab' => $tab, 'document_id' => $document_id, 'video_id' => $video_id, 'total_comments' => $total_comments);
        $myFilesData['type_pause'] = $type_pause['UserAccount']['type_pause'];
        $myFilesData['press_enter_to_send'] = $type_pause['UserAccount']['press_enter_to_send'];
        $myFilesData['auto_scroll_switch'] = $type_pause['UserAccount']['autoscroll_switch'];
        $myFilesData['total_comments'] = $total_comments;
        $this->set('video_tab', $view->element('MyFiles/videos', $myFilesData));
        $this->set('tab', $tab);
        $this->set('video_id', $video_id);
        $this->set('reveal_model', $view->element('MyFiles/models', $model_data));
        $this->set('account_id', $account_id);
        //    $this->set('video_huddle_model', $view->element('MyFilesVideoHolder', $element_data));
        $item_count = $this->Document->countMyFilesVideos($account_id, $user_id, "", '1,3');
        if (empty($video_id)) {

            $this->set('video_count', $item_count);
            if (!empty($MyFilesDocuments)) {
                $this->set('documents_count', count($MyFilesDocuments));
            } else {
                $this->set('documents_count', '0');
            }
        } else {
            $this->set('video_count', '');
            $this->set('documents_count', '');
        }
        $this->set('live_recording', $this->check_if_live_recording());
        $this->render('index');
    }

    function get_total_replies($parent_id) {
        $total_replies = 0;
        $result = $this->Comment->commentReplys($parent_id);
        if (count($result) > 0) {
            $total_replies = count($result);
            foreach ($result as $row) {
                if (isset($row['Comment']['id']) && $row['Comment']['id'] != '') {
                    $result2 = $this->Comment->commentReplys($row['Comment']['id']);
                    $total_replies = $total_replies + count($result2);
                }
            }
            return $total_replies;
        } else {
            return 0;
        }
    }

    private function _videoDetails($document_id) {
        return $this->Document->getVideoDetails($document_id);
    }

    function setSession() {
        $margin_left = $_POST['left'];
        $this->Session->write('margin-left', $margin_left);
        echo "added";
        die;
    }

    function add($accountFolerType = '') {
        if ($this->request->is('post')) {
            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $user_id = $users['User']['id'];
            $this->AccountFolder->create();
            $data = array(
                'account_id' => $account_id,
                'folder_type' => 3,
                'name' => $this->data['video_file_name'],
                'desc' => '',
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
            );
            if ($this->AccountFolder->save($data, $validation = TRUE)) {
                $account_folder_id = $this->AccountFolder->id;

                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'folder_type',
                    'meta_data_value' => '5',
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id, //Unique ID to recognise data added by this code
                    'last_edit_by' => $user_id
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);


                if ($accountFolerType == 1) {
                    $this->uploadVideos($account_folder_id, $account_id, $user_id, true, false, true);
                } else {
                    $video_id = isset($this->data['video_id']) ? $this->data['video_id'] : true;
                    $this->uploadDocuments($account_folder_id, $account_id, $user_id, $video_id, $this->data['url'], true);
                }
                echo "Video Upload Successfully.";
                exit;
            } else {
                echo "Video Uploading failed";
                exit;
            }
        }
    }

    function download($id, $tab = '') {
        if ($id) {
            parent::download($id);
        } else {
            $this->Session->setFlash($this->language_based_messages['please_select_at_least_one_file_to_download_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/MyFiles/view/' . $tab);
        }
    }

    function getVideoSearch($title = '') {
        $files_mode = $this->Session->read('files_mode');
        $users = $this->Session->read('user_current_account');
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('MyFiles');
        $this->set('language_based_content', $language_based_content);
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $sort = $this->request->data('sort');
        $limit = $this->request->data('limit');
        $page = $this->request->data('page');
        if ($files_mode == 'list') {
            if ((int) $limit >= 0)
                $limit = 10;
        }else {
            if ((int) $limit >= 0 && (int) $limit == 10) {
                $limit = 12;
            }
        }

        $item = $this->Document->getMyFilesVideos($account_id, $user_id, $title, $sort, $limit, $page, '1,3');
        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }

        if (empty($title)) {
            $message = $language_based_content['no_videos_uploaded_workspace'];
        } else {
            $message = $language_based_content['none_of_your_videos_workspace'];
        }

        $element_data = array(
            'files' => $item,
            'total_videos' => $this->Document->countMyFilesVideos($account_id, $user_id, $title, '1,3'),
            'files_mode' => $files_mode,
            'message' => $message,
            'language_based_content' => $language_based_content
        );
        $view = new View($this, false);
        $html = $view->element('MyFiles/ajax/videoSearch', $element_data);

        $return = array(
            'html' => $html,
            'total' => $this->Document->countMyFilesVideos($account_id, $user_id, $title, '1,3')
        );
        echo json_encode($return);
        exit;
    }

    function getDocSearchHtml($title = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $associatedVideos = $this->Document->getDocumentAssociatedVideosForWorkspace($account_id, $user_id);

        $associatedVideoMap = array();
        foreach ($associatedVideos as $v) {
            $associatedVideoMap[$v['Document']['id']][$v['afda']['attach_id']] = 1;
        }

        $sort = $this->request->data('sort');
        $limit = $this->request->data('limit');
        $page = $this->request->data('page');
        if (0 >= (int) $limit)
            $limit = 10;

        //   $MyFilesVideos = $this->AccountFolder->getAllMyFiles($account_id, $user_id, 1);
        $MyFilesDocuments = $this->Document->getMyFilesVideos($account_id, $user_id, $title, $sort, $limit, $page, 2);

        $document_data = array(
            'documents' => $MyFilesDocuments,
            'associatedVideoMap' => $associatedVideoMap
        );

        $view = new View($this, false);
        $html = $view->element('MyFiles/ajax/docsSearch', $document_data);

        return $html;
    }

    function getDocSearch($title = '') {
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('MyFiles');
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $associatedVideos = $this->Document->getDocumentAssociatedVideosForWorkspace($account_id, $user_id);
        $associatedVideoMap = array();
        foreach ($associatedVideos as $v) {
            $associatedVideoMap[$v['Document']['id']][$v['afda']['attach_id']] = 1;
        }

        $sort = $this->request->data('sort');
        $limit = $this->request->data('limit');
        $page = $this->request->data('page');
        if (0 >= (int) $limit)
            $limit = 10;

        $MyFilesVideos = $this->AccountFolder->getAllMyFiles($account_id, $user_id, '1,3');
        $MyFilesDocuments = $this->Document->getMyFilesVideos($account_id, $user_id, $title, $sort, $limit, $page, 2);
        $document_data = array(
            'documents' => $MyFilesDocuments,
            'videos' => $MyFilesVideos,
            'title' => $title,
            'associatedVideoMap' => $associatedVideoMap,
            'language_based_content' => $language_based_content
        );

        $view = new View($this, false);
        $html = $view->element('MyFiles/ajax/docsSearch', $document_data);

        $return = array(
            'html' => $html,
            'total' => $this->Document->countMyFilesVideos($account_id, $user_id, $title, 2)
        );
        echo json_encode($return);
        exit;
    }

    function deleteFiles($tab = '', $accountFolder = '') {
        if ($this->request->is('post')) {
            $account_folder_ids = $this->request->data['accountFolderIds'];
            if (empty($account_folder_ids))
                $account_folder_ids = $accountFolder;
            if ($account_folder_ids) {
                $document_ids = explode(',', $account_folder_ids);
                $accountFolderIds = array();
                $accountFolderDocIds = array();
                for ($i = 0; $i < count($document_ids); $i++) {
                    $document_id = $document_ids[$i];
                    $documents = $this->AccountFolderDocument->get($document_id);
                    $accountFolderIds[] = $documents['AccountFolderDocument']['account_folder_id'];
                    $accountFolderDocIds[] = $documents['AccountFolderDocument']['id'];
                }
                if (count($accountFolderIds) > 0) {
                    $this->AccountFolderDocumentAttachment->deleteAll(array('account_folder_document_id' => $accountFolderDocIds));
                    $conditions = 'account_folder_id IN(' . implode(',', $accountFolderIds) . ")";
                    $this->AccountFolder->updateAll(array('active' => 0), $conditions);
                    $this->CreateJobQueueArchiveJob($document_id);
                    if ($this->AccountFolder->getAffectedRows() > 0) {

                        if (!empty($tab) && $tab == '1') {
                            $this->Session->setFlash($this->language_based_messages['video_has_been_deleted_successfully'], 'default', array('class' => 'message success'));
                        } else {
                            $this->Session->setFlash($this->language_based_messages['resource_has_been_deleted_successfully_msg'], 'default', array('class' => 'message success'));
                        }

                        $this->redirect('/MyFiles/view/' . $tab);
                    } else {
                        $this->Session->setFlash($this->language_based_messages['your_video_is_not_deleted_try_again_msg'], 'default', array('class' => 'message success'));
                        $this->redirect('/MyFiles/view/' . $tab);
                    }
                } else {
                    $this->Session->setFlash($this->language_based_messages['something_is_wrong_contact_administrator_msg'], 'default', array('class' => 'message success'));
                    $this->redirect('/MyFiles/view/' . $tab);
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['please_delete_corrupt_video_before_downloading_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/MyFiles/view/' . $tab);
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['please_delete_corrupt_video_before_downloading_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/MyFiles/view/' . $tab);
        }
    }

    function deleteFile($document_id = '', $tab = 1) {
        if ($document_id) {
            $documents = $this->AccountFolderDocument->get($document_id);
            if ($documents) {
                //$this->AccountFolder->updateAll(array('active' => 0), array('account_folder_id' => $documents['AccountFolderDocument']['account_folder_id']));
                $this->Document->deleteAll(array('id' => $document_id));
                $this->AccountFolderDocument->deleteAll(array('document_id' => $document_id));
                $this->CreateJobQueueArchiveJob($document_id);
                if ($this->AccountFolder->getAffectedRows() > 0) {
                    $this->Session->setFlash($this->language_based_messages['you_have_successfully_deleted_selected_files_msg'], 'default', array('class' => 'message success'));
                    $this->redirect('/MyFiles/view/' . $tab);
                } else {
                    $this->Session->setFlash($this->language_based_messages['your_video_is_not_deleted_try_again_msg'], 'default', array('class' => 'message success'));
                    $this->redirect('/MyFiles/view/' . $tab);
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['something_is_wrong_contact_administrator_msg'], 'default', array('class' => 'message success'));
                $this->redirect('/MyFiles/view/' . $tab);
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['please_delete_corrupt_video_before_downloading_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/MyFiles/view/' . $tab);
        }
    }

    function copy() {

        $is_copied_video_library = false;
        $document_ids = isset($this->request->data['document_ids']) ? $this->request->data['document_ids'] : '';
        $account_folder_id = isset($this->request->data['account_folder_id']) ? $this->request->data['account_folder_id'] : '';
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $account_id = $users['accounts']['account_id'];

        //$tab = isset($this->request->data['tab']) ? $this->request->data['tab'] : '1';
        if (isset($this->request->data['tab']) && !empty($this->request->data['tab']))
            $tab = $this->request->data['tab'];
        else
            $tab = '1';

        if (empty($account_folder_id)) {
            $this->Session->setFlash($this->language_based_messages['please_select_at_least_one_huddle_to_copy_file_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/MyFiles/view/' . $tab);
        }
        $view = new View($this, false);
        if (!empty($document_ids)) {
            $conditions = 'id IN(' . $document_ids . ")";
            $myfilesRow = $this->AccountFolderDocument->get_row($conditions);

            $account_folder = '';

            if (count($account_folder_id) > 0 && $myfilesRow != '') {
                $copyRecord = array();
                $docs_data = array();
                $copied_video_id = array();
                for ($i = 0; $i < count($account_folder_id); $i++) {
                    foreach ($myfilesRow as $row) {
                        if ((int) $account_folder_id[$i] == -1) {
                            $account_folder = $this->copyToVideoLibrary($row['AccountFolderDocument']['document_id']);
                            $is_copied_video_library = true;
                            continue;
                        }

                        if ($view->Custom->check_if_eval_huddle($account_folder_id[$i]) == 1) {
                            if ($view->Custom->check_if_evaluated_participant($account_folder_id[$i], $user_id) == 1) {
                                $get_eval_participant_videos = $view->Custom->get_eval_participant_videos($account_folder_id[$i], $user_id);
                                $document = $this->Document->get_document_row($row['AccountFolderDocument']['document_id']);


                                $submission_allowed_count = 1;
                                $submission_allowed_count = $view->Custom->get_submission_allowed($account_folder_id[$i]);

                                if ($get_eval_participant_videos >= $submission_allowed_count && $document['Document']['doc_type'] != '2') {
                                    $this->Session->setFlash($this->language_based_messages['you_have_already_submitted_video_msg'], 'default', array('class' => 'message error'));
                                    $this->redirect('/MyFiles/view/' . $tab);
                                }
                            }
                        }
                        $document = $this->Document->get_document_row($row['AccountFolderDocument']['document_id']);
                        if (empty($document))
                            continue;
                        $old_video_id[] = $document['Document']['id'];
                        $new_doc_type = 1;

                        if ($document['Document']['doc_type'] == '1' || $document['Document']['doc_type'] == '3')
                            $new_doc_type = 1;
                        else
                            $new_doc_type = 2;

                        $docs_data[] = array('id' => null, 'created_date' => date("Y-m-d H:i:s"), 'last_edit_date' => date("Y-m-d H:i:s"), 'recorded_date' => date("Y-m-d H:i:s"), 'doc_type' => $new_doc_type) + $document['Document'];
                        $copyRecord[] = array(
                            'account_folder_id' => $account_folder_id[$i],
                            'document_id' => '',
                            'is_viewed' => $row['AccountFolderDocument']['is_viewed'],
                            'title' => $row['AccountFolderDocument']['title'],
                            'desc' => $row['AccountFolderDocument']['desc'],
                            'zencoder_output_id' => $row['AccountFolderDocument']['zencoder_output_id']
                        );
                    }
                    
                        $user_email_info = $this->User->find('first', array('conditions' => array('id' => $user_id)));

                        if($is_copied_video_library)
                        {
                            $this->create_churnzero_event('Videos+Shared+to+Video+Library', $account_id , $user_email_info['User']['email']);
                        }
                        else
                        {
                            $this->create_churnzero_event('Videos+Shared+to+Huddles', $account_id , $user_email_info['User']['email']);   
                        }
                    
                    
                }

                $cnt = 0;
                if (count($docs_data) > 0) {
                    App::import("Model", "JobQueue");
                    $db = new JobQueue();
                    foreach ($docs_data as $row) {
                        $this->Document->create();
                        $row['post_rubric_per_video'] = '1';
                        $this->Document->save($row);
                        $copied_video_id[] = $this->Document->id;
                        if ($row['published'] == 0) {
                            $requestXml = '<TranscodeVideoJob><document_id>' . $this->Document->id . '</document_id><source_file>' . $row['url'] . '</source_file></TranscodeVideoJob>';
                            $video_data = array(
                                'JobId' => '2',
                                'CreateDate' => date("Y-m-d H:i:s"),
                                'RequestXml' => $requestXml,
                                'JobQueueStatusId' => 1,
                                'CurrentRetry' => 0
                            );
                            $db->create();
                            $db->save($video_data);
                        } else {
                            $documentFiles = $this->DocumentFiles->get_document_row($old_video_id[$cnt]);
                            if (!empty($documentFiles)) {
                                $this->create_churnzero_event('Video+Hours+Uploaded', $account_id , $user_email_info['User']['email'],$documentFiles['DocumentFiles']['duration']); 
                                $video_data = array('id' => null, 'document_id' => $this->Document->id,'created_at'=>date('Y-m-d H:i:s'),
                                        'debug_logs'=>"Cake::MyFilesController::copy::line=951::document_id=".$this->Document->id."::status=".$documentFiles['DocumentFiles']['transcoding_status']."::created") + $documentFiles['DocumentFiles'];
                                $this->checkDocumentFilesRecordExist($this->Document->id, $video_data, $this->request->data);
                                $this->DocumentFiles->create();
                                $this->DocumentFiles->save($video_data);
                            }
                        }
                        $cnt++;
                    }
                }

                if (count($copyRecord) > 0 || $is_copied_video_library == true) {
                    $count = 0;
                    foreach ($copyRecord as $data) {
                        $data['document_id'] = $copied_video_id[$count];

                        $this->AccountFolderDocument->create();
                        $this->AccountFolderDocument->save($data);

                        $account_folder_document_id = $this->AccountFolderDocument->id;

                        $document_details = $this->Document->find('first', array('conditions' => array('id' => $data['document_id'])));

                        $document_video_details = $this->Document->query("SELECT * FROM `documents` AS d JOIN `account_folder_documents` AS afd ON d.`id` = afd.`document_id` WHERE d.`created_by` = " . $user_id . " AND afd.site_id=" . $this->site_id . "  AND  afd.`account_folder_id` = " . $data['account_folder_id'] . " AND d.`doc_type` IN (1 ,3)");

                        if ($document_details['Document']['doc_type'] == 2) {
                            if (!empty($document_video_details)) {
                                if ($view->Custom->check_if_eval_huddle($data['account_folder_id']) == 1) {
                                    foreach ($document_video_details as $doc) {
                                        $this->AccountFolderDocumentAttachment->create();
                                        $attachment_data = array(
                                            'account_folder_document_id' => $account_folder_document_id,
                                            'attach_id' => $doc['d']['id']
                                        );
                                        $this->AccountFolderDocumentAttachment->save($attachment_data);
                                    }
                                }
                            }
                        }

                        if (isset($this->request->data['copy_notes']) && $this->request->data['copy_notes'] == 1) {
                            @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 2);
                            //@$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 3);
                            @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 6);
                        }
                        $count++;

                        $user_activity_logs = array(
                            'ref_id' => $data['document_id'],
                            'desc' => 'Video Copied',
                            'url' => $this->base . '/huddles/view/' . $data['account_folder_id'] . "/1/" . $data['document_id'],
                            'type' => '22',
                            'account_id' => $account_id,
                            'account_folder_id' => $data['account_folder_id'],
                            'environment_type' => 2
                        );

                        $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                    }
                    
                        
                    if ($is_copied_video_library == true) {
                        if (count($copyRecord) == 0) {
                            $this->Session->setFlash($this->language_based_messages['you_have_successfully_copied_video_to_library_msg'], 'default', array('class' => 'message success'));
                        } else {
                            if ($tab == 1)
                                $this->EmailCopyVideo($account_folder_id, $data['document_id']);
                            else
                                $this->EmailCopyResource($account_folder_id);
                            $this->Session->setFlash($this->language_based_messages['you_have_successfully_copied_video_to_library_huddle_msg'], 'default', array('class' => 'message success'));
                        }
                        if ($account_folder > 0) {
                            $this->redirect('/videoLibrary/edit/' . $account_folder);
                        } else {
                            $this->Session->setFlash($this->language_based_messages['your_files_are_not_copied_into_selected_huddle_msg'], 'default', array('class' => 'message'));
                        }
                    } else {

                        if ($tab == 1)
                            $this->EmailCopyVideo($account_folder_id, $data['document_id']);
                        else
                            $this->EmailCopyResource($account_folder_id);
                        $this->Session->setFlash($this->language_based_messages['you_have_successfully_copied_your_resource_file_into_seleted_huddle_msg'], 'default', array('class' => 'message success'));
                    }
                } else {
                    $this->Session->setFlash($this->language_based_messages['your_files_are_not_copied_into_selected_huddle_msg'], 'default', array('class' => 'message'));
                }
                $this->redirect('/MyFiles/view/' . $tab);
            } else {
                $this->Session->setFlash($this->language_based_messages['please_select_atleast_one_file_to_copied_into_huddle_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/MyFiles/view/' . $tab);
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['please_select_atleast_one_file_to_copied_into_huddle_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/MyFiles/view/' . $tab);
        }
    }

    function EmailCopyVideo($account_folder_id, $video_id = '') {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        for ($i = 0; $i < count($account_folder_id); $i++) {
            $huddle = $this->AccountFolder->getHuddle($account_folder_id[$i], $account_id);
            $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id[$i]);
            $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id[$i]);
            $huddle_user_ids = array();
            if ($userGroups && count($userGroups) > 0) {
                foreach ($userGroups as $row) {
                    $huddle_user_ids[] = $row['user_groups']['user_id'];
                }
            }
            if ($huddleUsers && count($huddleUsers) > 0) {
                foreach ($huddleUsers as $row) {
                    $huddle_user_ids[] = $row['huddle_users']['user_id'];
                }
            }

            if (count($huddle_user_ids) > 0) {
                $user_ids = implode(',', $huddle_user_ids);
                if (!empty($user_ids))
                    $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
            } else {
                $huddleUserInfo = array();
            }

            if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                $commentsData = array(
                    'account_folder_id' => $account_folder_id[$i],
                    'huddle_name' => $huddle[0]['AccountFolder']['name'],
                    'video_link' => '/Huddles/view/' . $account_folder_id[$i] . '/1/' . $video_id,
                    'participating_users' => $huddleUserInfo
                );
                foreach ($huddleUserInfo as $row) {
                    if ($user_id == $row['User']['id']) {
                        continue;
                    }
                    $view = new View($this, false);
                    if (!$view->Custom->check_if_evalutor($account_folder_id[$i], $row['User']['id']) && $view->Custom->check_if_eval_huddle($account_folder_id[$i])) {
                        continue;
                    }

                    $commentsData ['email'] = $row['User']['email'];
                    if ($this->check_subscription($row['User']['id'], '7', $account_id)) {

                        $this->sendCopyVideoEmail($commentsData, $row['User']['id']);
                    }
                }
            }
        }
    }

    function EmailCopyResource($account_folder_id) {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        for ($i = 0; $i < count($account_folder_id); $i++) {
            $huddle = $this->AccountFolder->getHuddle($account_folder_id[$i], $account_id);
            $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id[$i]);
            $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id[$i]);
            $huddle_user_ids = array();
            if ($userGroups && count($userGroups) > 0) {
                foreach ($userGroups as $row) {
                    $huddle_user_ids[] = $row['user_groups']['user_id'];
                }
            }
            if ($huddleUsers && count($huddleUsers) > 0) {
                foreach ($huddleUsers as $row) {
                    $huddle_user_ids[] = $row['huddle_users']['user_id'];
                }
            }

            if (count($huddle_user_ids) > 0) {
                $user_ids = implode(',', $huddle_user_ids);
                if (!empty($user_ids))
                    $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
            } else {
                $huddleUserInfo = array();
            }

            if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                $commentsData = array(
                    'account_folder_id' => $account_folder_id[$i],
                    'huddle_name' => $huddle[0]['AccountFolder']['name'],
                    'video_link' => '/huddles/view/' . $account_folder_id[$i],
                    'participating_users' => $huddleUserInfo
                );
                foreach ($huddleUserInfo as $row) {
                    if ($user_id == $row['User']['id']) {
                        continue;
                    }
                    $commentsData ['email'] = $row['User']['email'];
                    if ($this->check_subscription($row['User']['id'], '1', $account_id)) {
                        $this->sendCopyResourceEmail($commentsData, $row['User']['id']);
                    }
                }
            }
        }
    }

    function files_copy($doc_id) {

        $is_copied_video_library = false;
        $document_ids = isset($this->request->data['document_ids']) ? $this->request->data['document_ids'] : '';
        $account_folder_id = isset($this->request->data['account_folder_id']) ? $this->request->data['account_folder_id'] : '';
        $tab = isset($this->request->data['tab']) ? $this->request->data['tab'] : '1';

        if (empty($account_folder_id)) {
            $this->Session->setFlash($this->language_based_messages['please_select_at_least_one_huddle_to_copy_file_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/MyFiles/view/' . $tab . '/' . $doc_id);
        }

        if (!empty($document_ids)) {
            $conditions = 'id IN(' . $document_ids . ")";
            $myfilesRow = $this->AccountFolderDocument->get_row($conditions);

            $account_folder = '';
            if (count($account_folder_id) > 0 && $myfilesRow != '') {
                $copyRecord = array();
                $docs_data = array();
                $copied_video_id = array();
                for ($i = 0; $i < count($account_folder_id); $i++) {
                    foreach ($myfilesRow as $row) {
                        if ((int) $account_folder_id[$i] == -1) {
                            $this->copyToVideoLibrary($row['AccountFolderDocument']['document_id']);
                            $is_copied_video_library = true;
                            continue;
                        }
                        $document = $this->Document->get_document_row($row['AccountFolderDocument']['document_id']);
                        if (empty($document))
                            continue;
                        $old_video_id[] = $document['Document']['id'];
                        $docs_data[] = array('id' => null) + $document['Document'];
                        $copyRecord[] = array(
                            'account_folder_id' => $account_folder_id[$i],
                            'document_id' => '',
                            'is_viewed' => $row['AccountFolderDocument']['is_viewed'],
                            'title' => $row['AccountFolderDocument']['title'],
                            'desc' => $row['AccountFolderDocument']['desc'],
                            'zencoder_output_id' => $row['AccountFolderDocument']['zencoder_output_id']
                        );
                    }
                }

                $cnt = 0;
                if (count($docs_data) > 0) {
                    App::import("Model", "JobQueue");
                    $db = new JobQueue();
                    foreach ($docs_data as $row) {
                        $this->Document->create();
                        $row['post_rubric_per_video'] = '1';
                        $this->Document->save($row);
                        $copied_video_id[] = $this->Document->id;
                        if ($row['published'] == 0) {
                            $requestXml = '<TranscodeVideoJob><document_id>' . $this->Document->id . '</document_id><source_file>' . $row['url'] . '</source_file></TranscodeVideoJob>';
                            $video_data = array(
                                'JobId' => '2',
                                'CreateDate' => date("Y-m-d H:i:s"),
                                'RequestXml' => $requestXml,
                                'JobQueueStatusId' => 1,
                                'CurrentRetry' => 0
                            );
                            $db->create();
                            $db->save($video_data);
                        } else {
                            $documentFiles = $this->DocumentFiles->get_document_row($old_video_id[$cnt]);
                            if (!empty($documentFiles)) {
                                $video_data = array('id' => null, 'document_id' => $this->Document->id,'created_at'=>date('Y-m-d H:i:s'),
                                        'debug_logs'=>"Cake::MyFilesController::files_copy::line=1218::document_id=".$this->Document->id."::status=".$documentFiles['DocumentFiles']['transcoding_status']."::created") + $documentFiles['DocumentFiles'];
                                $this->checkDocumentFilesRecordExist($this->Document->id, $video_data, $this->request->data);
                                $this->DocumentFiles->create();
                                $this->DocumentFiles->save($video_data);
                            }
                        }
                        $cnt++;
                    }
                }
                $users = $this->Session->read('user_current_account');
                $user_id = $users['User']['id'];
                if (count($copyRecord) > 0 || $is_copied_video_library == true) {
                    $count = 0;
                    foreach ($copyRecord as $data) {
                        $data['document_id'] = $copied_video_id[$count];
                        $this->AccountFolderDocument->create();
                        $this->AccountFolderDocument->save($data);
                        if (isset($this->request->data['copy_notes']) && $this->request->data['copy_notes'] == 1) {
                            @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 2);
                            // @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 3);
                            @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 6);
                        }
                        $count++;
                    }

                    if ($is_copied_video_library == true) {
                        if (count($copyRecord) == 0) {
                            $this->Session->setFlash($this->language_based_messages['you_have_successfully_copied_video_to_library_msg'], 'default', array('class' => 'message success'));
                        } else {
                            $this->Session->setFlash($this->language_based_messages['you_have_successfully_copied_video_to_library_huddle_msg'], 'default', array('class' => 'message success'));
                        }
                    } else {
                        $this->Session->setFlash($this->language_based_messages['you_have_successfully_copied_video_into_huddle_msg'], 'default', array('class' => 'message success'));
                    }
                } else {
                    $this->Session->setFlash($this->language_based_messages['your_files_are_not_copied_into_selected_huddle_msg'], 'default', array('class' => 'message'));
                }
                $this->redirect('/MyFiles/view/' . $tab . '/' . $doc_id);
            } else {
                $this->Session->setFlash($this->language_based_messages['please_select_atleast_one_file_to_copied_into_huddle_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/MyFiles/view/' . $tab . '/' . $doc_id);
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['please_select_atleast_one_file_to_copied_into_huddle_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/MyFiles/view/' . $tab . '/' . $doc_id);
        }
    }

    function editTitle() {
        if ($this->request->is('post')) {
            $title = $this->request->data['title'];
            $document_id = $this->request->data['document_id'];
            $this->AccountFolderDocument->updateAll(array('title' => '"' . Sanitize::escape(htmlspecialchars($title)) . '"'), array('id' => $document_id));
            if ($this->AccountFolderDocument->getAffectedRows() > 0) {
                echo $title;
            } else {
                echo $title;
            }
            exit;
        }
    }

    function getVideoComments($video_id = '') {

        if (!isset($this->data['srtType'])) {
            $this->Session->write('srtType_myfiles', 5);
        } else {
            $changeType = $this->data['srtType'];
            $this->Session->write('srtType_myfiles', $changeType);
        }
        $searchCmt = isset($this->request->data['searchCmt']) ? $this->request->data['searchCmt'] : '';
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $account_folder_id = $this->Session->read('video_id');
        $changeType = isset($this->data['srtType']) ? $this->data['srtType'] : $this->Session->read('srtType_myfiles');
        $video_details = $this->AccountFolder->getAllMyFile($account_id, $user_id, $account_folder_id);

        $export_pdf_url = Configure::read('sibme_base_url') . 'Huddles/print_pdf_comments/' . $video_id . '/' . $changeType . '/2.pdf';
        $export_excel_url = Configure::read('sibme_base_url') . 'Huddles/print_excel_comments/' . $video_id . '/' . $changeType;
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_current_account = $this->Session->read('user_current_account');
        $huddle_id = $this->Session->read('account_folder_id');



        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);

        $result = $this->Comment->getVideoComments($video_id, $changeType, '', $searchCmt, '', 1);

        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        $comments = array();
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get standards
                $comments[] = array_merge($comment, array("default_tags" => $get_tags));
            }
        }
        $videoComments = $comments;
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $params = array(
            'videos' => $video_details,
            'user_id' => $user_id,
            'video_id' => $video_id,
            'videoComments' => $videoComments,
            'huddle_permission' => $huddle_permission,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account,
            'notifiable' => 0,
            'sortType' => $changeType,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'searchCmt' => $searchCmt,
            'type' => $this->request->data['type'],
            'auto_scroll_switch' => $type_pause['UserAccount']['autoscroll_switch'],
            'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                    "account_id" => $account_id,
                    "tag_type" => '1'
        )))
        );
        $total_comment = count($videoComments);
        $view = new View($this, false);
        $html = $view->element('MyFiles/ajax/vidComments', $params);
        $contents = array(
            'contents' => $html,
            "total_comment" => $total_comment,
            "tag_count" => ''
        );
        echo json_encode($contents);
        exit;
    }

    public function getLTIAttachedDocuments($video_id, $huddle_id = '', $user_id = '') {
        $view = new View($this, false);
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $huddle_type = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $is_coach_enable = $view->Custom->is_enabled_coach_feedback($huddle_id);
        $if_evaluator = $view->Custom->check_if_evalutor($huddle_id, $user_id);
        //echo "huddle type $huddle_type <br/> is coach enabled" . var_dump($is_coach_enable) . " <bris evalutor $if_evaluator";
        $vidDocuments = $this->Document->getVideoDocumentsByVideo_mobile($video_id, $huddle_id, $user_id, $huddle_type, $if_evaluator, $is_coach_enable);
        //$vidDocuments = $this->Document->getVideoDocumentsByVideo($video_id, 0);
        $new_array = array();
        foreach ($vidDocuments as $key => $vid) {
            $new_array[$key]['Document'] = $vid[0];
            $new_array[$key]['afd']['account_folder_id'] = $vid[0]['account_folder_id'];
            $new_array[$key]['afd']['title'] = $vid[0]['title'];
            $new_array[$key]['afd']['desc'] = $vid[0]['desc'];
            $comment_attachments = $this->CommentAttachment->find('first', array(
                'conditions' => array(
                    'document_id' => $vid[0]['id'],
                ),
            ));
            if (!empty($comment_attachments)) {
                $new_array[$key]['comment_id'] = $comment_attachments['CommentAttachment']['comment_id'];
            } else {
                $new_array[$key]['comment_id'] = '';
            }
        }

        $vidDocuments = $new_array;

        $view = new View($this, false);
        $html = $view->element('MyFiles/ajax/vidDocumentsIFrame', array(
            'vidDocuments' => $vidDocuments,
            'videoID' => $video_id,
            'huddle_id' => $huddle_id,
            'videoDetails' => $this->Document->getVideoDetails($video_id)
        ));
        $contents = array(
            'html' => $html
        );
        echo json_encode($contents);
        exit;
    }

    public function getAttachedDocuments($video_id, $huddle_id = '') {

        $vidDocuments = $this->Document->getVideoDocumentsByVideo($video_id, 0);
        $view = new View($this, false);
        $html = $view->element('MyFiles/ajax/vidDocuments', array(
            'vidDocuments' => $vidDocuments,
            'videoID' => $video_id,
            'huddle_id' => $huddle_id,
            'videoDetails' => $this->Document->getVideoDetails($video_id)
        ));
        $contents = array(
            'html' => $html
        );
        echo json_encode($contents);
        exit;
    }

    public function ob_getAttachedDocuments($video_id) {
        $vidDocuments = $this->Document->getVideoDocumentsByObservation($video_id);
//        print_r($vidDocuments);die();
//        $vidDocuments = $this->Document->getDocuments($video_id, $title = '', $sort = '', $doc_type = '');
        $view = new View($this, false);
        $html = $view->element('MyFiles/ajax/ob_vidDocuments', array(
            'vidDocuments' => $vidDocuments,
            'document_id ' => $video_id
        ));
        $contents = array(
            'html' => $html
        );
        echo json_encode($contents);
        exit;
    }

    function copyToVideoLibrary($video_id, $huddle_id = 0) {
        $old_video_id = $video_id;
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $video_details = $this->AccountFolder->getAllMyFileWithDocId($account_id, $user_id, $video_id);

        $this->AccountFolder->create();
        $data_af = array(
            'account_id' => $account_id,
            'folder_type' => 2,
            'name' => $video_details['afd']['title'],
            'desc' => $video_details['AccountFolder']['desc'],
            'active' => $video_details['AccountFolder']['active'],
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
        );
        $this->AccountFolder->save($data_af, $validation = TRUE);

        $account_folder_id = $this->AccountFolder->id;
        $account_folders_meta_data = array(
            'account_folder_id' => $account_folder_id,
            'meta_data_name' => 'tag',
            'meta_data_value' => '',
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
        );
        $this->AccountFolderMetaData->create();
        $this->AccountFolderMetaData->save($account_folders_meta_data);

        $this->Document->create();
        $data_doc = array(
            'account_id' => $video_details['doc']['account_id'],
            'doc_type' => $video_details['doc']['doc_type'],
            'url' => $video_details['doc']['url'],
            'original_file_name' => $video_details['doc']['original_file_name'],
            'active' => $video_details['doc']['active'],
            'view_count' => 0,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'file_size' => $video_details['doc']['file_size'] ? $video_details['doc']['file_size'] : '',
            'last_edit_by' => $user_id,
            'encoder_provider' => $video_details['doc']['encoder_provider'],
            'published' => $video_details['doc']['published'],
            'post_rubric_per_video' => '1'
        );
        $this->Document->save($data_doc, $validation = TRUE);

        $video_id = $this->Document->id;

        App::import("Model", "JobQueue");
        $db = new JobQueue();

        if ($video_details['doc']['published'] == 0) {

            $requestXml = '<TranscodeVideoJob><document_id>' . $video_id . '</document_id><source_file>' . $video_details['doc']['url'] . '</source_file></TranscodeVideoJob>';
            $video_data = array(
                'JobId' => '2',
                'CreateDate' => date("Y-m-d H:i:s"),
                'RequestXml' => $requestXml,
                'JobQueueStatusId' => 1,
                'CurrentRetry' => 0
            );
            $db->create();
            $db->save($video_data);
        } else {
            $documentFiles = $this->DocumentFiles->get_document_row($old_video_id);
            $user_email_info = $this->User->find('first', array('conditions' => array('id' => $user_id)));
            $this->create_churnzero_event('Video+Hours+Uploaded', $account_id , $user_email_info['User']['email'],$documentFiles['DocumentFiles']['duration']); 
            $video_data = array('id' => null, 'document_id' => $video_id,'created_at'=>date('Y-m-d H:i:s'),
                    'debug_logs'=>"Cake::MyFilesController::copyToVideoLibrary::line=1503::document_id=".$video_id."::status=".$documentFiles['DocumentFiles']['transcoding_status']."::created") + $documentFiles['DocumentFiles'];
            $this->checkDocumentFilesRecordExist($video_id, $video_data, $this->request->data);
            $this->DocumentFiles->create();
            $this->DocumentFiles->save($video_data);
        }

        $this->AccountFolderDocument->create();
        $data_afd = array(
            'account_folder_id' => $account_folder_id,
            'document_id' => $video_id,
            'title' => $video_details['afd']['title'],
            'zencoder_output_id' => $video_details['afd']['zencoder_output_id'],
            'desc' => $video_details['afd']['desc']
        );

        $this->AccountFolderDocument->save($data_afd, $validation = TRUE);

        $user_activity_logs = array(
            'ref_id' => $video_id,
            'desc' => 'Video Copied',
            'url' => $this->base . '/videoLibrary/view/' . $account_folder_id . '/' . $account_id,
            'type' => '22',
            'account_id' => $account_id,
            'account_folder_id' => $account_folder_id,
            'environment_type' => 2
        );

        $this->user_activity_logs($user_activity_logs, $account_id, $user_id);


        return $this->AccountFolder->id;
    }

    function trim($video_id, $huddle_id) {
        $this->layout = 'trim';
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $video_details = $this->AccountFolder->getAllMyFile($account_id, $user_id, $huddle_id);
        $vidDocuments = $this->Document->getVideoDocumentsByVideo($video_details['doc']['id'], $huddle_id);
        $commentsTime = $this->Comment->getVideoComments($video_details['doc']['id']);

        $this->set('videoDetail', $video_details);
        $this->set('videoComments', $commentsTime);
        $this->set('huddle_id', $huddle_id);
        $this->set('video_id', $video_id);
        $this->set('vidDocuments', $vidDocuments);
    }

    function copytoaccounts() {
        if ($this->request->is('post')) {
            $account_ids = $this->data['account_ids'];
            $documents = $this->data['document_ids'];
            $tab = isset($this->request->data['tab']) ? $this->request->data['tab'] : '1';
            if (isset($this->request->data['copy_notes']) && $this->request->data['copy_notes'] == 1)
                $copy_notes = 1;
            else
                $copy_notes = 0;

            if ($account_ids) {
                foreach ($account_ids as $account_id) {
                    if ($msg = $this->copyToAccountWorkSpace($account_id, $documents, $tab, $copy_notes)) {
                        if ($tab == 2) {
                            $this->Session->setFlash($this->language_based_messages['you_have_successfully_copied_your_resource_file_msg'], 'default', array('class' => 'message success'));
                        } else {
                            $this->Session->setFlash($this->language_based_messages['you_have_successfully_copied_your_video_file_msg'], 'default', array('class' => 'message success'));
                        }
                    }
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['please_select_atleast_one_account_msg'], 'default', array('class' => 'message error'));
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['your_files_are_not_copied_try_again_msg'], 'default', array('class' => 'message error'));
        }

        $this->redirect('/MyFiles/');
        exit;
    }

    function copyToAccountWorkSpace($account_id, $document_ids, $tab, $copy_notes = 0) {

        $users = $this->Session->read('user_current_account');
        //$account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $doc_ids = explode(",", $document_ids);

        try {
            foreach ($doc_ids as $doc) {
                $conditions = array('id' => $doc);
                $myfilesRow = $this->AccountFolderDocument->get_row($conditions);
                $myfilesRow = $myfilesRow[0];
                //print_r($myfilesRow);
                $this->AccountFolder->create();
                if ($tab == 1)
                    $document = $this->Document->getVideoDetails($myfilesRow['AccountFolderDocument']['document_id'], array(1, 3));
                if ($tab == 2)
                    $document = $this->Document->get_document_row($myfilesRow['AccountFolderDocument']['document_id']);
                //print_r($document);
                //exit;
//                if(!$document){
//                    continue;
//                }
                $data_af = array(
                    'account_id' => $account_id,
                    'folder_type' => 3,
                    'name' => $myfilesRow['AccountFolderDocument']['title'],
                    'desc' => $myfilesRow['AccountFolderDocument']['desc'],
                    'active' => $document['Document']['active'],
                    'created_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'last_edit_by' => $user_id,
                );

                $this->AccountFolder->save($data_af, $validation = TRUE);

                $account_folder_id = $this->AccountFolder->id;

                $this->Document->create();

                $data_doc = array(
                    'account_id' => $account_id,
                    'doc_type' => $document['Document']['doc_type'],
                    'url' => $document['Document']['url'],
                    'original_file_name' => $document['Document']['original_file_name'],
                    'active' => $document['Document']['active'],
                    'view_count' => 0,
                    'created_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'file_size' => $document['Document']['file_size'] ? $document['Document']['file_size'] : '',
                    'last_edit_by' => $user_id,
                    'encoder_provider' => $document['Document']['encoder_provider'],
                    'published' => $document['Document']['published'],
                    'post_rubric_per_video' => '1'
                );


                $this->Document->save($data_doc, $validation = TRUE);

                $video_id = $this->Document->id;

                App::import("Model", "JobQueue");
                $db = new JobQueue();

                if ($document['Document']['published'] == 0) {

                    $requestXml = '<TranscodeVideoJob><document_id>' . $video_id . '</document_id><source_file>' . $document['Document']['url'] . '</source_file></TranscodeVideoJob>';
                    $video_data = array(
                        'JobId' => '2',
                        'CreateDate' => date("Y-m-d H:i:s"),
                        'RequestXml' => $requestXml,
                        'JobQueueStatusId' => 1,
                        'CurrentRetry' => 0
                    );
                    $db->create();
                    $db->save($video_data);
                } else {
                    if ($tab == 1) {
                        $documentFiles = $this->DocumentFiles->get_document_row($myfilesRow['AccountFolderDocument']['document_id']);
                        $video_data = array('id' => null, 'document_id' => $video_id,'created_at'=>date('Y-m-d H:i:s'),
                                'debug_logs'=>"Cake::MyFilesController::copyToAccountWorkSpace::line=1665::document_id=".$video_id."::status=".$documentFiles['DocumentFiles']['transcoding_status']."::created") + $documentFiles['DocumentFiles'];
                        $this->checkDocumentFilesRecordExist($video_id, $video_data, $this->request->data);
                        $this->DocumentFiles->create();
                        $this->DocumentFiles->save($video_data);
                    }
                }

                $this->AccountFolderDocument->create();
                $data_afd = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $video_id,
                    'title' => $myfilesRow['AccountFolderDocument']['title'],
                    'zencoder_output_id' => $myfilesRow['AccountFolderDocument']['zencoder_output_id'],
                    'desc' => $myfilesRow['AccountFolderDocument']['desc']
                );
                $this->AccountFolderDocument->save($data_afd, $validation = TRUE);
                if ($copy_notes == 1)
                    @$this->copyAccountComments($account_id, $myfilesRow['AccountFolderDocument']['document_id'], $video_id, $user_id);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return true;
    }

    function copyComments($sourceVideoId, $destinationVideoId, $sourceUserId, $refType = 2) {

        $comments = $this->Comment->find('all', array(
            'conditions' => array(
                'ref_type' => $refType,
                'ref_id' => $sourceVideoId,
                'active' => 1
            ),
            'order' => 'created_date'
        ));
        if (count($comments) == 0)
            return;

        foreach ($comments as $comment) {
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get tags

            $acc = $this->UserAccount->find('first', array(
                'conditions' => array(
                    'user_id' => $sourceUserId
                )
            ));

            $this->Comment->create();
            $comment_user_id = $comment['Comment']['user_id'];
            if ($refType == 3 || ($refType == 6 && $comment['Comment']['parent_comment_id'] > 0))
                $parentCommentId = $destinationVideoId;
            else
                $parentCommentId = null;
            $comment_data = array(
                'parent_comment_id' => $parentCommentId,
                'ref_type' => $refType,
                'ref_id' => $destinationVideoId,
                'user_id' => $comment_user_id,
                'created_by' => $comment_user_id,
                'last_edit_by' => $comment_user_id,
                    ) + $comment['Comment'];
            unset($comment_data['id']);
            if ($this->Comment->save($comment_data)) {

                $latest_comment_id = $this->Comment->id;

                $replies = $this->Comment->find('all', array(
                    'conditions' => array('site_id' => $this->site_id, 'parent_comment_id' => $comment['Comment']['id'])));

                foreach ($replies as $replies) {
                    $repliesToreplies = $this->Comment->find('all', array(
                        'conditions' => array('site_id' => $this->site_id, 'parent_comment_id' => $replies['Comment']['id'])));

                    $comment_data = array(
                        'parent_comment_id' => $latest_comment_id,
                        'site_id' => $this->site_id,
                        'ref_type' => '3',
                        'ref_id' => $destinationVideoId,
                        'user_id' => $comment_user_id,
                        'created_by' => $comment_user_id,
                        'last_edit_by' => $comment_user_id,
                            ) + $replies['Comment'];
                    unset($comment_data['id']);
                    $this->Comment->create();
                    $this->Comment->save($comment_data);

                    foreach ($repliesToreplies as $replyToreply) {
                        $comment_data = array(
                            'parent_comment_id' => $this->Comment->id,
                            'ref_type' => '3',
                            'site_id' => $this->site_id,
                            'ref_id' => $destinationVideoId,
                            'user_id' => $comment_user_id,
                            'created_by' => $comment_user_id,
                            'last_edit_by' => $comment_user_id,
                                ) + $replyToreply['Comment'];
                        unset($comment_data['id']);
                        $this->Comment->create();
                        $this->Comment->save($comment_data);
                    }
                }



                if (!empty($get_tags)) {
                    foreach ($get_tags as $tag) {
                        $commentTag = $this->AccountTag->find('first', array(
                            'conditions' => array(
                                'account_id' => $acc['UserAccount']['account_id'],
                                'tag_title' => $tag['AccountCommentTag']['tag_title']
                            )
                        ));
                        $this->AccountCommentTag->create();
                        $comment_tag_data = array(
                            'comment_id' => $this->Comment->id,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => 1,
                            'tag_title' => $tag['AccountCommentTag']['tag_title'],
                            'account_tag_id' => $commentTag['AccountTag']['account_tag_id'],
                            'created_by' => $comment_user_id,
                            'last_edit_by' => $comment_user_id,
                        );
                        $this->AccountCommentTag->save($comment_tag_data);
                    }
                }
            }

            //$this->copyComments($comment['Comment']['id'], $this->Comment->id, $sourceUserId, 3);
        }
    }

    function copyAccountComments($destAcc, $sourceVideoId, $destinationVideoId, $sourceUserId, $refType = 2) {
        $comments = $this->Comment->find('all', array(
            'conditions' => array(
                'ref_type' => $refType,
                'ref_id' => $sourceVideoId,
                'active' => 1
            ),
            'order' => 'created_date'
        ));
        if (count($comments) == 0)
            return;
        foreach ($comments as $comment) {
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get tags

            $acc = $this->UserAccount->find('first', array(
                'conditions' => array(
                    'user_id' => $sourceUserId
                )
            ));

            $this->Comment->create();
            $comment_user_id = $comment['Comment']['user_id'];
            $comment_data = array(
                'parent_comment_id' => ($refType == 3) ? $destinationVideoId : null,
                'ref_type' => $refType,
                'ref_id' => $destinationVideoId,
                'user_id' => $comment_user_id,
                'created_by' => $comment_user_id,
                'last_edit_by' => $comment_user_id,
                    ) + $comment['Comment'];
            unset($comment_data['id']);
            if ($this->Comment->save($comment_data)) {
                if (!empty($get_tags)) {
                    foreach ($get_tags as $tag) {
                        $commentTag = $this->AccountTag->find('first', array(
                            'conditions' => array(
                                'account_id' => $acc['UserAccount']['account_id'],
                                'tag_title' => $tag['AccountCommentTag']['tag_title']
                            )
                        ));
                        $this->AccountCommentTag->create();
                        $comment_tag_data = array(
                            'comment_id' => $this->Comment->id,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => 1,
                            'tag_title' => $tag['AccountCommentTag']['tag_title'],
                            'account_tag_id' => $commentTag['AccountTag']['account_tag_id'],
                            'created_by' => $comment_user_id,
                            'last_edit_by' => $comment_user_id,
                        );
                        $this->AccountCommentTag->save($comment_tag_data);
                    }
                }
            }

            $this->copyAccountComments($destAcc, $comment['Comment']['id'], $this->Comment->id, $sourceUserId, 3);
        }
    }

    function getLiveObservations() {

        $result = array();
        $user = $this->Session->read('user_current_account');
        $document_in_review = implode(",", json_decode($_POST['observation_inreview']));

        if (isset($user)) {

            $account_id = $user['accounts']['account_id'];
            $user_id = $user['User']['id'];
            $live_observations = $this->Document->getWorkspaceLiveObservations($user_id);
            if (count($live_observations) > 0) {

                for ($i = 0; $i < count($live_observations); $i++) {

                    $live_observation = $live_observations[$i];
                    $live_observation['doc'] = $live_observation['Document'];
                    $live_observation['doc']['created_date'] = date('M d, Y', strtotime($live_observation['doc']['created_date']));
                    $live_observations[$i] = $live_observation;
                }

                $result['live_observations'] = $live_observations;
            }

            if (!empty($document_in_review)) {

                $live_observations = $this->Document->getWorkspaceLiveObservationsInReview($user_id, $document_in_review);


                if (count($live_observations) > 0) {

                    for ($i = 0; $i < count($live_observations); $i++) {

                        $live_observation = $live_observations[$i];
                        $live_observation['doc'] = $live_observation['Document'];
                        $live_observation['doc']['created_date'] = date('M d, Y', strtotime($live_observation['doc']['created_date']));
                        $live_observations[$i] = $live_observation;
                    }

                    if (isset($result['live_observations']))
                        $result['live_observations'] = array_merge($result['live_observations'], $live_observations);
                    else
                        $result['live_observations'] = $live_observations;
                }
            }
        }

        echo json_encode($result);
        die;
    }

    function check_if_live_recording() {
        $current_user = $this->Session->read('user_current_account');
        $account_id = $current_user['accounts']['account_id'];
        $user_id = $current_user['User']['id'];
        $result = $this->UserActivityLog->find('count', array(
            'conditions' => array(
                'type' => 20,
                'user_id' => $user_id,
                'account_id' => $account_id,
                "ref_id NOT IN ( select ref_id from user_activity_logs where type = 21 and user_id = $user_id and account_id=$account_id)"
            )
        ));
        return $result;
    }

    function getCopyHuddlesList() {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];

        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }
        $view = new View($this, false);

        $allHuddles = $this->AccountFolder->copyAllHuddles($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);

        $view = new View($this, false);
        $li = '';
        if (!empty($allHuddles)) {
            foreach ($allHuddles as $row) {
                $get_eval_participant_videos = $view->Custom->get_eval_participant_video_details($row['AccountFolder']['account_folder_id'], $user_id);
                // if ($view->Custom->check_if_eval_huddle($row['AccountFolder']['account_folder_id']) == 1 && $view->Custom->check_if_evaluated_participant($row['AccountFolder']['account_folder_id'], $user_id) == 1 && empty($get_eval_participant_videos[0]['d'])) {
                //     continue;
                // } elseif ($view->Custom->check_if_eval_huddle($row['AccountFolder']['account_folder_id']) == 1 && !$view->Custom->check_if_evaluated_participant($row['AccountFolder']['account_folder_id'], $user_id)) {
                //     continue;
                // }
                $huddleUsers = $this->AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                $userGroups = $this->AccountFolderGroup->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                $loggedInUserRole = $view->Custom->has_admin_access($huddleUsers, $userGroups, $user_id);

                //if (($loggedInUserRole == 200 || $loggedInUserRole == 210) || ($view->Custom->is_creator($user_id, $row['AccountFolder']['created_by']) )) { //&& $user['UserAccount']['permission_maintain_folders'] == '1')) {
                $li .= '<li>';
                $li .= '<label class="ui-checkbox model">';
                $li .= '<input name="account_folder_id[]" id="account_folder_name_' . $row['AccountFolder']['account_folder_id'] . '" type="checkbox" class="copyFiles_checkbox" value="' . $row['AccountFolder']['account_folder_id'] . '">';
                $li .= '</label>';
                $li .= '<label  for="account_folder_name_' . $row['AccountFolder']['account_folder_id'] . '"><a class="copy_link">';
                $li .= $row['AccountFolder']['name'];
                $li .= '</a></label>';
                $li .= '</li>';
                //}
            }
        }
        $return = array(
            'html' => $li
        );
        echo json_encode($return);
        die;
    }

    function video_details_view($user_id = '', $account_id = '', $course_id = '', $huddle_id_1 = '', $document_id) {

        $account_folder_documents = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $document_id)));

        $video_id = $account_folder_documents['AccountFolderDocument']['account_folder_id'];
        $huddle_id = $video_id;
        $view = new View($this, TRUE);
        $user_accounts = $this->User->get($user_id);
        $user = $user_accounts[0];
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];

        $video_doc_id = 0;
        $MyFilesVideos = $this->AccountFolder->getAllMyFiles($account_id, $user_id, '1,3');
        $MyFilesDocuments = $this->AccountFolder->getAllMyFiles($account_id, $user_id, '2');
        $changeType = isset($this->data['srtType']) ? $this->data['srtType'] : 5;


        $user_current_account = $user_accounts[0];
        $huddle_info = $this->AccountFolder->find('first', array(
            'conditions' => array(
                'account_folder_id' => $huddle_id
            )
        ));

        $submission_data = $this->AssignmentSubmission->query('
                        SELECT
                          hs.`course_id`,
                          hs.`date_added`,
                          hs.`id` AS submission_id,
                          hs.`user_id`,
                          asub.`assignment_id`,
                           DATE_FORMAT(asub.`date_added`,"%M %d , %Y") AS submission_at,
                          asub.`huddle_id`,
                          CONCAT(u.`first_name`,\' \',u.`last_name`) AS student_name,
                          (SELECT COUNT(assignment_submission.student_id) FROM assignment_submission WHERE assignment_submission.student_id = asub.`student_id` )AS total_submissions

                        FROM
                          `huddles_assignments` AS hs
                           INNER JOIN `assignment_submission` AS asub
                              ON asub.`assignment_id` = hs.`id`
                           INNER JOIN users AS u
                              ON u.id = asub.`student_id`
                        WHERE hs.huddle_id = "' . $huddle_id_1 . '"
                          AND hs.course_id = "' . $course_id . '"
                          AND hs.site_id ="' . $this->site_id . '"
                          AND asub.document_id = "' . $document_id . '"
                          GROUP BY asub.`student_id`,asub.`assignment_id`

        ');

        $this->set('submission_data', $submission_data);
        $this->set('huddle_info', $huddle_info);
        $huddle_id = $video_id;
        $users = $user_accounts[0];
        if ($huddle_id != '') {
            $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        } else {
            $huddle = false;
        }

        if ($video_id != '') {

            $this->Session->write('video_id', $video_id);
            $video_details = $this->AccountFolder->getAllMyFile($account_id, $user_id, $video_id);

            if ($video_details == false) {
                echo 'The video you are trying to view is not available.';
                die;
                //$this->Session->setFlash('The video you are trying to view is not available.', 'default', array('class' => 'message error'));
                //$this->redirect('/dashboard/home');
                //exit;
            }

            $vidDocuments = $this->Document->getVideoDocumentsByVideo($video_details['doc']['id'], $video_id);

            $srtType = $this->Session->read('srtType_myfiles');
            $commentsTime = $this->Comment->getVideoComments($video_details['doc']['id'], $srtType, '', '', '', 1);
            $comments_result = $this->Comment->getVideoComments($video_details['doc']['id'], $srtType, '', '', '', 1);
            $comments = array();
            if (!empty($comments_result)) {
                foreach ($comments_result as $comment) {
                    $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get standards
                    $comments[] = array_merge($comment, array("default_tags" => $get_tags));
                }
            }
            $videoComments = $comments;
            $video_doc_id = $video_details['doc']['id'];
        } else {
            $video_details = '';
            $commentsTime = '';
            $vidDocuments = '';
            $comments_result = '';
        }

        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        //$huddles = $this->AccountFolder->copyAllHuddles($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $huddles = false;

//        $videoComments = '';
//        if (is_array($comments_result) && count($comments_result) > 0) {
//            $videoComments = $comments_result;
//        } else {
//            $videoComments = '';
//        }

        $export_pdf_url = Configure::read('sibme_base_url') . 'Huddles/print_pdf_comments/' . $video_doc_id . '/' . $changeType . '/2.pdf';
        $export_excel_url = Configure::read('sibme_base_url') . 'Huddles/print_excel_comments/' . $video_doc_id . '/' . $changeType;
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));

        $params_comments = array(
            'videos' => $video_details,
            'user_id' => $user_id,
            'video_id' => $video_doc_id,
            'videoComments' => !empty($videoComments) ? $videoComments : '',
            'huddle_permission' => false,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account,
            'auto_scroll_switch' => $type_pause['UserAccount']['autoscroll_switch'],
            'notifiable' => 0,
            'sortType' => $changeType,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'type' => 'get_video_comments',
            'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                    "account_id" => $account_id,
                    "tag_type" => '1'
        )))
        );


        $view = new View($this, false);
        $html_comments = $view->element('MyFiles/ajax/vidCommentsFrame', $params_comments);

        $total_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $video_details['doc']['id'])));

        $view = new View($this, false);

        $document_files = $this->DocumentFiles->find('first', array('conditions' => array('document_id' => $video_details['doc']['id'])));

        $myFilesData = array(
            'videos' => $MyFilesVideos,
            'user_id' => $user_id,
            'document_files' => $document_files,
            'videoDetail' => $video_details,
            'videoComments' => $commentsTime,
            'huddle_id' => $video_id,
            'video_id' => $video_doc_id,
            'vidDocuments' => $vidDocuments,
            'videos_total' => $this->Document->countMyFilesVideos($account_id, $user_id, "", '1,3'),
            'show_ff_message' => false,
            'videoCommentsArray' => !empty($videoComments) ? $videoComments : '',
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'html_comments' => $html_comments,
            'tab' => '1',
            'tags' => $this->AccountTag->find("all", array("conditions" => array(
                    "account_id" => $account_id,
                    "tag_type" => '1'))),
        );

        $document_id = '';
        if (isset($video_details['afd']['id']) && $video_details['afd']['id'] != '') {
            $document_id = $video_details['afd']['id'];
        }

        $element_data = array(
            'video_id' => $video_id,
            'user_id' => $user_id,
            'tab' => 1,
            'videos' => $MyFilesVideos,
        );

        $isFirefoxBrowser = $view->Browser->isFirefox();
        if ($isFirefoxBrowser) {
            $trim_message_displayed = $this->Cookie->read('flash_message_ff_trim');
            if (!isset($trim_message_displayed) || empty($trim_message_displayed)) {
                echo $trim_message_displayed;
                $myFilesData['show_ff_message'] = true;
                $this->Cookie->write('flash_message_ff_trim', "displayed", false, '2 weeks');
            }
        }

        //if($video_id == ''){
        $associatedVideos = $this->Document->getDocumentAssociatedVideosForWorkspace($account_id, $user_id);
        $associatedVideoMap = array();
        foreach ($associatedVideos as $v) {
            $associatedVideoMap[$v['Document']['id']][$v['afda']['attach_id']] = 1;
        }

        $doc_bool = 1;
        if (empty($MyFilesDocuments)) {
            $doc_bool = 0;
        }
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $video_title = '';
        if (!empty($video_details['afd']['title'])) {
            $video_title = $video_details['afd']['title'];
        } else {
            $video_title = 'Untitled Video';
        }

        $model_data = array("present_account_id" => $account_id, "all_accounts" => $this->Session->read('user_accounts'), 'allHuddles' => $huddles, 'tab' => $tab, 'document_id' => $document_id, 'video_id' => $video_id, 'total_comments' => $total_comments);
        $myFilesData['type_pause'] = $type_pause['UserAccount']['type_pause'];
        $myFilesData['press_enter_to_send'] = $type_pause['UserAccount']['press_enter_to_send'];
        $myFilesData['auto_scroll_switch'] = $type_pause['UserAccount']['autoscroll_switch'];
        $myFilesData['total_comments'] = $total_comments;
        $this->set('video_tab', $view->element('MyFiles/videosFrame', $myFilesData));
        $this->set('tab', $tab);
        $this->set('video_title', $video_title);
        $this->set('video_id', $video_id);
        //$this->set('reveal_model', $view->element('MyFiles/models', $model_data));
        $this->set('account_id', $account_id);
        //    $this->set('video_huddle_model', $view->element('MyFilesVideoHolder', $element_data));


        $this->set('live_recording', false);
        $this->layout = 'huddles_null';
        $this->render('video_details');
    }

}
