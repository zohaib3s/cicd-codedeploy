<?php

/**
 * User controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('Subscription', 'Braintree');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
App::import('Vendor', 'Google/autoload');

class UsersController extends AppController
{

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Users';

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('AccountUserCode', 'User', 'UserAccount', 'copyUserSettings', 'getAccountSubscribers', 'AccountFolder', 'AccountFolderUser', 'Account', 'Group', 'UserGroup', 'AccountFolderMetaData', "AccountTag", "AccountStudentPayments", "AccountStudentPaymentsLog", "UserDeviceLog", "Hmh_order_licenses","LoginRequest");
    var $dir = 'img/users/';
    var $uploadDir = '';
    var $client = '';
    public $skey  = "E5Iffmmcgn0fowTmoX7JnmbzsLPsLbRA";

    public function __construct($request = null, $response = null)
    {

        $this->uploadDir = WWW_ROOT . $this->dir;

        parent::__construct($request, $response);
        $this->client = new Google_Client();
    }

    public function beforeFilter()
    {

        $users = $this->Session->read('user_current_account');
        $this->set('logged_in_user', $users);
        $this->set('User', $this->User);

        $this->Auth->allow('two_factor_auth', 'login_api', 'forgot_password_api', 'add', 'registration', 'sibme_registration', 'free_sibme_registration', 'unsubscribeWeeklyMonthly', 'successUnsubscribe', 'sso_login_callback', 'check_login_status'); // Letting users register themselves

        parent::beforeFilter();
    }

    public function add()
    {
        $users = $this->Session->read('user_current_account');

        $this->check_is_account_expired();
        $this->is_account_activated($users['users_accounts']['account_id'], $users['User']['id']);

        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__($this->language_based_messages['user_has_been_saved_msg']));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__($this->language_based_messages['user_could_not_be_saved_msg']));
        }
    }

    public function signup()
    {
        if ($this->site_id != 1) {
            $this->redirect('/users/login');
            exit;
        } else {
            $view = new View($this, false);
            return $view->Custom->redirectHTTPS('online_trial_account');
        }

        if ($this->request->is('post')) {


            if (!empty($this->request->data['website'])) {
                $this->Session->setFlash($this->language_based_messages['it_appears_you_are_bot_msg']);
                return $this->redirect('/online_trial_account');
            }


            $errors = false;
            $errors_str = "";
            if (empty($this->request->data['first_name'])) {
                $errors = true;
                $errors_str .= "First Name is required." . "<br>";
            }
            if (empty($this->request->data['last_name'])) {
                $errors = true;
                $errors_str .= "Last Name is required." . "<br>";
            }
            if (empty($this->request->data['email']) || filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL) === false) {
                $errors = true;
                $errors_str .= "Please enter a valid email address." . "<br>";
            }
            if (empty($this->request->data['username'])) {
                $errors = true;
                $errors_str .= "Username is required." . "<br>";
            }
            if (empty($this->request->data['company_name'])) {
                $errors = true;
                $errors_str .= "School/District, Organization, or Institution Name is required." . "<br>";
            }
            if (empty($this->request->data['password'])) {
                $errors = true;
                $errors_str .= "Password is required." . "<br>";
            }
            if (empty($this->request->data['password_confirmation'])) {
                $errors = true;
                $errors_str .= "Confirm Password is required." . "<br>";
            }
            if ($errors) {
                $this->Session->setFlash($errors_str);
                $this->redirect('signup');
            }
            if (strlen($this->request->data['password']) < 6 || strlen($this->request->data['password_confirmation']) < 6) {
                $this->Session->setFlash('For security your password must contain a minimum of 6 characters.', 'default', array('class' => 'message error'));
                $this->redirect('signup');
            }
            if ($this->request->data['password'] != $this->request->data['password_confirmation']) {
                $this->Session->setFlash('Passwords do not match please enter correct passwords.');
                $this->redirect('signup');
            }
            $role_id = '100';
            if ($this->User->isUserExists($this->request->data['username']) > 0) {
                $this->Session->setFlash('Username already exists, please try again.');
                $this->redirect('signup');
            }

            if ($this->User->isUserEmailExists($this->request->data['email']) > 0) {
                $this->Session->setFlash('Email address already exists, please try again.');
                $this->redirect('signup');
            }

            $this->Account->create();
            $data = array(
                'company_name' => $this->request->data['company_name'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'is_active' => '1',
                'in_trial' => '1',
                'has_credit_card' => '0',
                'is_suspended' => '0'
            );
            $this->Account->save($data);
            $account_id = $this->Account->id;

            $this->User->create();
            $verification_code = uniqid();
            $data = array(
                'first_name' => $this->request->data['first_name'],
                'last_name' => $this->request->data['last_name'],
                'email' => $this->request->data['email'],
                'created_date' => date('Y-m-d H:i:s'),
                'username' => $this->request->data['username'],
                'password' => $this->request->data['password'],
                'type' => 'Pending_Activation',
                'is_active' => TRUE,
                'verification_code' => $verification_code
            );

            $this->User->save($data);

            $user_id = $this->User->id;

            $this->User->updateAll(
                array(
                    'authentication_token' => "'" . md5($user_id) . "'"
                ),
                array('id' => $user_id)
            );

            $this->UserAccount->create();
            //Checking default account is exist or not
            $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                'user_id' => $user_id
            )));
            $data = array(
                'user_id' => $user_id,
                'account_id' => $account_id,
                'role_id' => $role_id,
                'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                'permission_maintain_folders' => true,
                'permission_access_video_library' => true,
                'permission_video_library_upload' => true,
                'permission_administrator_user_new_role' => true,
                'parmission_access_my_workspace' => true,
                'folders_check' => true,
                'manage_collab_huddles' => true,
                'manage_coach_huddles' => true,
                'manage_evaluation_huddles' => true,
                'live_recording' => true,
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $user_id,
                'last_edit_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $user_id
            );
            $this->UserAccount->save($data);

            $this->AccountTag->create();
            $save_tags = array(
                "account_id" => $account_id,
                "tag_type" => '1',
                "tag_title" => 'Question',
                "created_by" => $user_id,
                "created_date" => date('Y-m-d H:i:s'),
                "last_edit_by" => $user_id,
                "last_edit_date" => date('Y-m-d H:i:s')
            );
            $this->AccountTag->save($save_tags);

            $this->AccountTag->create();
            $save_tags = array(
                "account_id" => $account_id,
                "tag_type" => '1',
                "tag_title" => 'Idea',
                "created_by" => $user_id,
                "created_date" => date('Y-m-d H:i:s'),
                "last_edit_by" => $user_id,
                "last_edit_date" => date('Y-m-d H:i:s')
            );
            $this->AccountTag->save($save_tags);

            $this->AccountTag->create();
            $save_tags = array(
                "account_id" => $account_id,
                "tag_type" => '1',
                "tag_title" => 'Bright spot',
                "created_by" => $user_id,
                "created_date" => date('Y-m-d H:i:s'),
                "last_edit_by" => $user_id,
                "last_edit_date" => date('Y-m-d H:i:s')
            );
            $this->AccountTag->save($save_tags);

            $this->AccountFolderMetaData->create();
            $data = array(
                'account_folder_id' => $account_id,
                'meta_data_name' => 'enable_tags',
                'meta_data_value' => '1',
                'created_by' => $user_id,
                'created_date' => date('Y-m-d H:i:s'),
            );

            $this->AccountFolderMetaData->save($data, $validation = TRUE);

            $sampleHuddleId = Configure::read('sample_huddle');
            $this->copyHuddleForSample($sampleHuddleId, $account_id, $user_id);

            $this->request->data['id'] = $user_id;
            $this->request->data['account_id'] = $account_id;
            // $this->send_welcome_email($this->request->data, $account_id);
            $this->send_welcome_email_template($this->request->data, $account_id);
            //$this->auto_login($user_id);
            $user_detail = $this->User->findById($user_id);
            $user_detail = $user_detail['User'];

            if ($this->Auth->login($user_detail)) {

                $this->Cookie->delete('remember_me_cookie');
                $this->setupPostLogin(true);
            }
            $data = array(
                'to_email' => $this->request->data['email'],
                'name' => $this->request->data['first_name'] . ' ' . $this->request->data['last_name'],
                'account_name' => $this->request->data['company_name'],
                'user_id' => $user_id,
                'verification_code' => $verification_code
            );


            $this->activation_email($data, $account_id);
            $this->redirect('/users/activation_page/' . $account_id . '/' . $user_id);
        }
    }

    function registration()
    {
        $this->layout = "signup";


        if ($this->request->is('post')) {


            if (!empty($this->request->data['website']) && isset($this->request->data['website'])) {
                $this->Session->setFlash('It appears you are a bot!');
                $this->redirect('registration');
            }

            if (!empty($this->request->data['first_name']) && !empty($this->request->data['last_name'])) {
                $this->request->data['fullname'] = $this->request->data['first_name'] . ' ' . $this->request->data['last_name'];
            }

            $errors = false;
            $errors_str = "";
            if (empty($this->request->data['fullname'])) {
                $errors = true;
                $errors_str .= "Full Name is required." . "<br>";
            }
            if (empty($this->request->data['email']) || filter_var($this->request->data['email'], FILTER_VALIDATE_EMAIL) === false) {
                $errors = true;
                $errors_str .= "Please enter a valid email address." . "<br>";
            }
            if (empty($this->request->data['company_name'])) {
                $errors = true;
                $errors_str .= "School/District, Organization, or Institution Name is required." . "<br>";
            }
            if (empty($this->request->data['password'])) {
                $errors = true;
                $errors_str .= "Password is required." . "<br>";
            }
            if (empty($this->request->data['password_confirmation'])) {
                $errors = true;
                $errors_str .= "Confirm Password is required." . "<br>";
            }


            if (strlen($this->request->data['password']) < 6 || strlen($this->request->data['password_confirmation']) < 6) {
                $errors = true;
                $errors_str .= "For security your password must contain a minimum of 6 characters." . "<br>";
            }


            if ($errors) {
                $this->Session->setFlash($errors_str);
                $this->redirect('registration');
            }
            $full_name = $this->request->data('fullname');

            if (isset($full_name) && $full_name != '') {
                $full_name = explode(' ', $full_name);
                $first_name = isset($full_name[0]) ? $full_name[0] : '';
                $last_name = isset($full_name[1]) ? $full_name[1] : '';
            } else {
                $first_name = $this->request->data['first_name'];
                $last_name = $this->request->data['last_name'];
            }
            $this->request->data['first_name'] = $first_name;
            $this->request->data['last_name'] = $last_name;
            $this->request->data['username'] = $this->request->data['email'];
            $this->Session->write('result', $this->request->data);

            if ($this->request->data['password'] != $this->request->data['password_confirmation']) {
                $this->Session->setFlash('Passwords do not match please enter correct passwords.');
                $this->redirect('registration');
            }
            $role_id = '100';
            if ($this->User->isUserExists($this->request->data['email']) > 0) {
                $this->Session->setFlash('Username already exists, please try again.');
                $this->redirect('registration');
            }
            if ($this->User->isUserEmailExists($this->request->data['email']) > 0) {
                $this->Session->setFlash('Email address already exists, please try again.');
                $this->redirect('registration');
            }

            $this->Account->create();

            $data = array(
                'company_name' => $this->request->data['company_name'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'is_active' => '1',
                'in_trial' => '1',
                'has_credit_card' => '0',
                'is_suspended' => '0'
            );
            $this->Account->save($data);
            $account_id = $this->Account->id;

            $this->User->create();

            $verification_code = uniqid();

            $data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $this->request->data['email'],
                'created_date' => date('Y-m-d H:i:s'),
                'username' => $this->request->data['email'],
                'password' => $this->request->data['password'],
                'type' => 'Pending_Activation',
                'is_active' => TRUE,
                'verification_code' => $verification_code
            );

            $this->User->save($data);

            $user_id = $this->User->id;

            $this->User->updateAll(
                array('authentication_token' => "'" . md5($user_id) . "'"),
                array('id' => $user_id)
            );

            $this->UserAccount->create();
            //Checking default account is exist or not
            $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                'user_id' => $user_id
            )));
            $data = array(
                'user_id' => $user_id,
                'account_id' => $account_id,
                'role_id' => $role_id,
                'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                'permission_maintain_folders' => true,
                'permission_access_video_library' => true,
                'permission_video_library_upload' => true,
                'permission_administrator_user_new_role' => true,
                'parmission_access_my_workspace' => true,
                'folders_check' => true,
                'manage_collab_huddles' => true,
                'manage_coach_huddles' => true,
                'manage_evaluation_huddles' => true,
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $user_id,
                'last_edit_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $user_id
            );

            $this->UserAccount->save($data);

            $this->AccountTag->create();
            $save_tags = array(
                "account_id" => $account_id,
                "tag_type" => '1',
                "tag_title" => 'Question',
                "created_by" => $user_id,
                "created_date" => date('Y-m-d H:i:s'),
                "last_edit_by" => $user_id,
                "last_edit_date" => date('Y-m-d H:i:s')
            );
            $this->AccountTag->save($save_tags);

            $this->AccountTag->create();
            $save_tags = array(
                "account_id" => $account_id,
                "tag_type" => '1',
                "tag_title" => 'Idea',
                "created_by" => $user_id,
                "created_date" => date('Y-m-d H:i:s'),
                "last_edit_by" => $user_id,
                "last_edit_date" => date('Y-m-d H:i:s')
            );
            $this->AccountTag->save($save_tags);

            $this->AccountTag->create();
            $save_tags = array(
                "account_id" => $account_id,
                "tag_type" => '1',
                "tag_title" => 'Bright spot',
                "created_by" => $user_id,
                "created_date" => date('Y-m-d H:i:s'),
                "last_edit_by" => $user_id,
                "last_edit_date" => date('Y-m-d H:i:s')
            );
            $this->AccountTag->save($save_tags);

            $this->AccountFolderMetaData->create();
            $data = array(
                'account_folder_id' => $account_id,
                'meta_data_name' => 'enable_tags',
                'meta_data_value' => '1',
                'created_by' => $user_id,
                'created_date' => date('Y-m-d H:i:s')
            );

            $this->AccountFolderMetaData->save($data, $validation = TRUE);

            $sampleHuddleId = Configure::read('sample_huddle');
            $this->copyHuddleForSample($sampleHuddleId, $account_id, $user_id);

            $this->request->data['id'] = $user_id;
            $this->request->data['account_id'] = $account_id;
            //$this->send_welcome_email($this->request->data, $account_id);
            $this->send_welcome_email_template($this->request->data, $account_id);



            $user_detail = $this->User->findById($user_id);
            $user_detail = $user_detail['User'];

            if ($this->Auth->login($user_detail)) {

                $this->Cookie->delete('remember_me_cookie');
                $this->setupPostLogin(true);
            }



            $data = array(
                'to_email' => $this->request->data['email'],
                'name' => $this->request->data['first_name'] . ' ' . $this->request->data['last_name'],
                'account_name' => $this->request->data['company_name'],
                'user_id' => $user_id,
                'verification_code' => $verification_code
            );


            $this->activation_email($data, $account_id);
            $this->redirect('/users/activation_page/' . $account_id . '/' . $user_id);


            //   $this->auto_login($user_id);
        } else {
            $full_name = $this->request->data('fullname');
            $full_name = explode(' ', $full_name);
            $first_name = isset($full_name[0]) ? $full_name[0] : '';
            $last_name = isset($full_name[1]) ? $full_name[1] : '';
            $this->set('result', $this->request->data);
        }
    }

    public function login_by_token($token_id)
    {


        $final_destination = $this->Auth->redirect();
        $api_user = $this->User->getByHash($token_id);

        if (!$api_user) {

            $this->redirect('/users/login');
            exit;
        } else {

            if ($this->Auth->login($api_user['User'])) {

                $post_login = $this->setupPostLogin(true);
                if (!$post_login) {
                    $this->Session->write('deactivate_user_msg', $this->language_based_messages['user_is_deactivated_in_accounts']);
                }
                $redirect_url = $final_destination;
                $redirect_url = explode('/', $redirect_url);
                $user_current_account = $this->Session->read('user_current_account');
                $totalAccounts = $this->Session->read('totalAccounts');

                if (isset($redirect_url[2]) && $redirect_url[2] == 'change_password') {
                    $this->redirect('/Dashboard/home');
                } elseif (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] == 1) {
                    $this->redirect('/accounts/account_settings_all/' . $user_current_account['accounts']['account_id'] . '/1');
                } else {

                    if ($final_destination == '/' && $totalAccounts > 1) {
                        $this->redirect("/launchpad");
                    } else {
                        $this->redirect($final_destination);
                    }

                    exit;
                }
            } else {

                $this->redirect('/users/login');
                exit;
            }
        }
    }

    /**
     * Displays a view
     *
     * @param mixed What page to display
     * @return void
     */
    public function login($inv_account_id = '', $inv_auth_token = '', $inv_user_id = '')
    {
        $this->redirect('/home/login');
        exit;
    }

//     public function login_api()
//     {
//         if (!$this->request->is('post')) {
//             echo json_encode(["success"=>false, "message"=>"Get Request Not Allowed."]);
//             exit;
//         }

//         $view = new View($this, false);
//         $language_based_content = $view->Custom->get_page_lang_based_content('users/login');

//         $link_url = $this->Auth->redirectUrl();
//         $link_explode = explode('/', $link_url);

//         $new_angular_link_url = '';

//         // $error_message = "Redirected from old login.";

//         if (isset($_SESSION['refurl']) && !empty($_SESSION['refurl'])) {

//             $new_angular_link_url = $_SESSION['refurl'];
//         }


//         if (isset($link_explode) && $link_explode[1] != '') {
//             $this->Session->write('redirect_uri', $link_url);
//         } elseif (empty($link_explode[1]) && isset($_SESSION['refurl']) && !empty($_SESSION['refurl'])) {
//             $this->Session->write('redirect_uri', $_SESSION['refurl']);
//             // @session_destroy($_SESSION['refurl']);
//             unset($_SESSION['refurl']);
//         }

//         $huddle_workspace = 0;
//         $huddle_id = '';
//         $video_id = '';
//         if ($link_url == '/') {
//             $huddle_workspace = 0;
//             $huddle_id = '';
//             $video_id = '';
//         } else {
//             $link_explode = explode('/', $link_url);
//             if (strtolower($link_explode[1]) == 'myfiles') {
//                 $document_details = $this->AccountFolderDocument->find('first', array('conditions' => array(
//                     'account_folder_id' => $link_explode[4]
//                 )));
//                 $huddle_id = $link_explode[4];
//                 $video_id = $document_details['AccountFolderDocument']['document_id'];
//                 $huddle_workspace = 3;
//             }
//             if (strtolower($link_explode[1]) == 'huddles') {
//                 $huddle_id = $link_explode[3];
//                 $video_id = $link_explode[5];
//                 $huddle_workspace = 1;
//             }

//             if (strtolower($link_explode[1]) == 'video_details') {
//                 $huddle_id = $link_explode[3];
//                 $video_id = $link_explode[4];
//                 $huddle_workspace = 1;
//             }

//             if (strtolower($link_explode[1]) == 'workspace_video') {
//                 $huddle_id = $link_explode[3];
//                 $video_id = $link_explode[4];
//                 $huddle_workspace = 3;
//             }
//             if (strtolower($link_explode[3]) == 'video_huddles') {
//                 $huddle_id = $link_explode[6];
//                 $comment_id = $link_explode[9];
//                 $huddle_workspace = 4;
//             }
//         }

//         if (!empty($new_angular_link_url)) {

//             $link_explode = explode('/', $new_angular_link_url);

//             if (strtolower($link_explode[3]) == 'video_details') {
//                 $huddle_id = $link_explode[5];
//                 $video_id = $link_explode[6];
//                 $huddle_workspace = 1;
//             }

//             if (strtolower($link_explode[3]) == 'workspace_video') {
//                 $huddle_id = $link_explode[5];
//                 $video_id = $link_explode[6];
//                 $huddle_workspace = 3;
//             }

//             if (strtolower($link_explode[3]) == 'huddles') {
//                 $huddle_id = $link_explode[5];
//                 $video_id = $link_explode[7];
//                 $huddle_workspace = 1;
//             }
//             if (strtolower($link_explode[3]) == 'video_huddles') {
//                 $huddle_id = $link_explode[6];
//                 $comment_id = $link_explode[9];
//                 $huddle_workspace = 4;
//             }
//         }
//         if(isset($link_explode[4]) && strtolower($link_explode[4])=='goals'){
//             $this->set('is_goal_request',true);
//         }else{
//             $this->set('is_goal_request',false); 
//         }

//         $this->set('video_id', $video_id);
//         $this->set('huddle_id', $huddle_id);
//         $this->set('huddle_workspace', $huddle_workspace);
//         if (!empty($comment_id)) {
//             $this->set('comment_id', $comment_id);
//         } else {
//             $this->set('comment_id', '');
//         }

//         $this->set('language_based_content', $language_based_content);
//         $url_scheme = print_r($link_explode, true);
//         $this->set('url_Scheme', $url_scheme);


//         // $this->layout = 'login';
//         $redir = $this->Session->read('Auth.redirect');

//         if ($redir == '/Users/forgot_password') {
//             $this->Session->write('Auth.redirect', '/');
//         }
// /*
//         //        $this->Session->delete('Message.flash');
//         $view = new View($this, false);
//         $site_title = $view->Custom->get_site_settings('site_title');
//         if (empty($site_title)) {
//             $site_title = 'Sibme';
//         }
// */
//         $error_message = "Uncaught Exception!";
//         if ($this->request->is('post')) {

//             // $final_destination = $this->Auth->redirectUrl();
//             if (!empty($this->Session->read('redirect_uri'))) {
//                 $final_destination = $this->Session->read('redirect_uri');
//             } else {
//                 $final_destination = "/";
//             }

//             if (filter_var($this->request->data['username'], FILTER_VALIDATE_EMAIL)) {

//                 $user = $this->login_by_email();
//                 $user = $user['User'];
//                 if ($this->Auth->login($user)) {
//                     // Commenting following on Asad request because this was creating problem in Push Notifications for Mobile Apps.
//                     // $this->UserDeviceLog->deleteAll(array('user_id' => $user['id']), $cascade = true, $callbacks = true);

//                     if (isset($this->request->data['remember_me']) && $this->request->data['remember_me'] == 1) {
//                         // remove "remember me checkbox"
//                         unset($this->request->data['remember_me']);

//                         // hash the user's password
//                         $this->request->data['password'] = $this->Auth->password($this->request->data['password']);

//                         $user_data = array(
//                             'username' => $user['username'],
//                             'password' => $this->request->data['password']
//                         );

//                         // write the cookie
//                         $this->Cookie->write('remember_me_cookie', $user_data, true, '2 weeks');
//                         $this->redirectJsonData = ['authentication_token'=>$user['authentication_token']];
//                     }

//                     $post_login = $this->setupPostLogin(true);
//                     if (!$post_login) {
//                         $this->Session->write('deactivate_user_msg', $this->language_based_messages['user_is_deactivated_in_accounts']);
//                     }

//                     $redirect_url = $final_destination;
//                     $redirect_url = explode('/', $redirect_url);
//                     $user_current_account = $this->Session->read('user_current_account');
//                     $totalAccounts = $this->Session->read('totalAccounts');
//                     $this->Session->write('user_top_photo', $user_current_account['User']['image']);

//                     if (isset($redirect_url[2]) && $redirect_url[2] == 'change_password') {
//                         // $this->redirect('/Dashboard/home');
//                         echo $this->getRedirectJson('/');
//                         exit;                    
//                     } elseif (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] == 1) {
//                         // $this->redirect('/accounts/account_settings_all/' . $user_current_account['accounts']['account_id'] . '/1');
//                         echo $this->getRedirectJson('accounts/account_settings_all/' . $user_current_account['accounts']['account_id'] . '/1');
//                         exit;                    
//                     } else {
//                         if ($totalAccounts > 1) {
//                             echo $this->getRedirectJson('launchpad');
//                             exit;                    
//                             // $this->redirect("/launchpad");
//                         } else {
//                             $this->Session->write('redirect_uri', "/");
//                             echo $this->getRedirectJson($final_destination, true);
//                             exit;                    
//                             // $this->redirect($final_destination);
//                         }
//                         exit;
//                     }
//                 } else {
//                     $password = sha1(Configure::read('Security.salt') . $this->request->data['password']);
//                     $hmhdata = $this->UserDeviceLog->query("select * from users where email = '" . $this->request->data['username'] . "' and password = '" . $password . "' and site_id = 2");
//                     if (sizeof($hmhdata) > 0) {
//                         $sitesdata = $this->UserDeviceLog->query("select * from sites where site_id = " . $hmhdata[0]['users']['site_id']);
//                         $user_accounts = $this->UserDeviceLog->query("SELECT * FROM users_accounts WHERE user_id = " . $hmhdata[0]['users']['id'] . " AND site_id =" . $hmhdata[0]['users']['site_id']);
//                         if (count($user_accounts) > 1) {
//                             // $this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'] . '?stoh=1');
//                             echo $this->getRedirectJson('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'] . '?stoh=1', true);
//                             exit;                    
//                         } else {
//                             // $this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token']);
//                             echo $this->getRedirectJson('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'], true);
//                             exit;                    
//                         }
//                     }
//                     /*
//                      else {
//                         $this->Session->setFlash(__($language_based_content['invalid_email_login']));
//                     }
//                     */
//                     echo $this->getRedirectJson('login', false, false, __($language_based_content['invalid_email_login']) );
//                     exit;
//                     // $this->Session->setFlash(__($language_based_content['invalid_email_login']));
//                 }
//             } elseif ($this->Auth->login()) {
//                 // did they select the remember me checkbox?


//                 $start_end_date = $this->get_start_end_date($user_current_account['accounts']['account_id']);
//                 $st_date = $start_end_date['start_date'];
//                 $end_date = $start_end_date['end_date'];



//                 if (isset($this->request->data['remember_me']) && $this->request->data['remember_me'] == 1) {
//                     // remove "remember me checkbox"
//                     unset($this->request->data['remember_me']);

//                     // hash the user's password
//                     $this->request->data['password'] = $this->Auth->password($this->request->data['password']);

//                     // write the cookie
//                     $this->Cookie->write('remember_me_cookie', $this->request->data, true, '2 weeks');
//                 }

//                 if (!empty($this->request->data['inv_account_id']) && !empty($this->request->data['inv_auth_token']) && !empty($this->request->data['inv_user_id'])) {
//                     $this->accept_invitation($this->request->data['inv_account_id'], $this->request->data['inv_auth_token'], $this->request->data['inv_user_id']);
//                 }
//                 $post_login = $this->setupPostLogin(true);
//                 if (!$post_login) {
//                     $this->Session->write('deactivate_user_msg', $this->language_based_messages['user_is_deactivated_in_accounts']);
//                 }
//                 $redirect_url = $final_destination;
//                 $redirect_url = explode('/', $redirect_url);
//                 $user_current_account = $this->Session->read('user_current_account');
//                 // Commenting following on Asad request because this was creating problem in Push Notifications for Mobile Apps.
//                 // $this->UserDeviceLog->deleteAll(array('user_id' => $user_current_account['User']['id']), $cascade = true, $callbacks = true);
//                 $user_role_id = $user_current_account['roles']['role_id'];
//                 $totalAccounts = $this->Session->read('totalAccounts');

//                 if (isset($redirect_url[2]) && $redirect_url[2] == 'change_password') {

//                     if ($user_role_id == 120) {
//                         // $this->redirect('/analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
//                         echo $this->getRedirectJson('analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
//                         exit;
//                     } else {
//                         // $this->redirect('/Dashboard/home');
//                         echo $this->getRedirectJson('/');
//                         exit;
//                     }
//                 } elseif (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
//                     // // $this->redirect('/accounts/account_settings_all/' . $user_current_account['accounts']['account_id'] . '/1');
//                     // $this->redirect('/accounts/deactivated_account_redirect/' . $user_current_account['accounts']['account_id']);
//                     echo $this->getRedirectJson('accounts/deactivated_account_redirect/' . $user_current_account['accounts']['account_id']);
//                     exit;
//                 } else {

//                     if ($final_destination == '/' && $totalAccounts > 1) {
//                         if ($user_role_id == 120) {
//                             if ($final_destination == "/") {
//                                 // $this->redirect('/analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
//                                 echo $this->getRedirectJson('analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
//                                 exit;
//                             } else {
//                                 $this->Session->write('redirect_uri', "/");
//                                 // $this->redirect($final_destination);
//                                 echo $this->getRedirectJson($final_destination, true);
//                                 exit;
//                             }
//                         } else {
//                             // $this->redirect("/launchpad");
//                             echo $this->getRedirectJson('launchpad');
//                             exit;
//                         }
//                     } else {

//                         if ($user_role_id == 120) {
//                             if ($final_destination == "/") {
//                                 // $this->redirect('/analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
//                                 echo $this->getRedirectJson('analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
//                                 exit;
//                             } else {
//                                 $this->Session->write('redirect_uri', "/");
//                                 // $this->redirect($final_destination);
//                                 echo $this->getRedirectJson($final_destination, true);
//                                 exit;
//                             }
//                         } else {
//                             // $this->Session->delete('redirect_uri');
//                             // $this->redirect($final_destination);
//                             if ($totalAccounts == 1) {
//                                 // $this->redirect("/");
//                                 echo $this->getRedirectJson('/');
//                                 exit;
//                             } else {
//                                 // $this->redirect("/launchpad");
//                                 echo $this->getRedirectJson('launchpad');
//                                 exit;
//                             }
//                         }
//                     }

//                     exit;
//                 }
//             }
//             $password = sha1(Configure::read('Security.salt') . $this->request->data['password']);
//             $hmhdata = $this->UserDeviceLog->query("select * from users where username = '" . $this->request->data['username'] . "' and password = '" . $password . "' and site_id = 2");
//             if (sizeof($hmhdata) > 0) {
//                 $sitesdata = $this->UserDeviceLog->query("select * from sites where site_id = " . $hmhdata[0]['users']['site_id']);
//                 $user_accounts = $this->UserDeviceLog->query("SELECT * FROM users_accounts WHERE user_id = " . $hmhdata[0]['users']['id'] . " AND site_id =" . $hmhdata[0]['users']['site_id']);
//                 if (count($user_accounts) > 1) {
//                     $this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'] . '?stoh=1');
//                 } else {
//                     $this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token']);
//                 }
//             } else {
//                 $error_message = __($language_based_content['invalid_email_login']);
//             }
//             $error_message = __($language_based_content['invalid_email_login']);
//         }
//         /*
//          else {
//             $this->set('account_id', $inv_account_id);
//             $this->set('auth_token', $inv_auth_token);
//             $this->set('user_id', $inv_user_id);
//             $referer_string = $this->referer(null, true);
//             $referer_array = Router::parse($referer_string);

//             if (!($referer_array['controller'] == 'users' && $referer_array['action'] == 'change_password')) {
//                 CakeSession::delete('Message.flash');
//             }

//             return $this->redirect('/home/login', 301);
//         }
//         */
//         echo $this->getRedirectJson('login', false, false, $error_message);
//         exit;
// // $this->render($this->site_id . '/login');
//     }

    public function login_api()
    {
        if (!$this->request->is('post')) {
            echo json_encode(["success"=>false, "message"=>"Get Request Not Allowed."]);
            exit;
        }

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('users/login');

        $link_url = $this->Auth->redirectUrl();
        $link_explode = explode('/', $link_url);

        $new_angular_link_url = '';

        // $error_message = "Redirected from old login.";

        if (isset($_SESSION['refurl']) && !empty($_SESSION['refurl'])) {

            $new_angular_link_url = $_SESSION['refurl'];
        }


        if (isset($link_explode) && $link_explode[1] != '') {
            $this->Session->write('redirect_uri', $link_url);
        } elseif (empty($link_explode[1]) && isset($_SESSION['refurl']) && !empty($_SESSION['refurl'])) {
            $this->Session->write('redirect_uri', $_SESSION['refurl']);
            // @session_destroy($_SESSION['refurl']);
            unset($_SESSION['refurl']);
        }

        $huddle_workspace = 0;
        $huddle_id = '';
        $video_id = '';
        if ($link_url == '/') {
            $huddle_workspace = 0;
            $huddle_id = '';
            $video_id = '';
        } else {
            $link_explode = explode('/', $link_url);
            if (strtolower($link_explode[1]) == 'myfiles') {
                $document_details = $this->AccountFolderDocument->find('first', array('conditions' => array(
                    'account_folder_id' => $link_explode[4]
                )));
                $huddle_id = $link_explode[4];
                $video_id = $document_details['AccountFolderDocument']['document_id'];
                $huddle_workspace = 3;
            }
            if (strtolower($link_explode[1]) == 'huddles') {
                $huddle_id = $link_explode[3];
                $video_id = $link_explode[5];
                $huddle_workspace = 1;
            }

            if (strtolower($link_explode[1]) == 'video_details') {
                $huddle_id = $link_explode[3];
                $video_id = $link_explode[4];
                $huddle_workspace = 1;
            }

            if (strtolower($link_explode[1]) == 'workspace_video') {
                $huddle_id = $link_explode[3];
                $video_id = $link_explode[4];
                $huddle_workspace = 3;
            }
            if (strtolower($link_explode[3]) == 'video_huddles') {
                $huddle_id = $link_explode[6];
                $comment_id = $link_explode[9];
                $huddle_workspace = 4;
            }
        }

        if (!empty($new_angular_link_url)) {

            $link_explode = explode('/', $new_angular_link_url);

            if (strtolower($link_explode[3]) == 'video_details') {
                $huddle_id = $link_explode[5];
                $video_id = $link_explode[6];
                $huddle_workspace = 1;
            }

            if (strtolower($link_explode[3]) == 'workspace_video') {
                $huddle_id = $link_explode[5];
                $video_id = $link_explode[6];
                $huddle_workspace = 3;
            }

            if (strtolower($link_explode[3]) == 'huddles') {
                $huddle_id = $link_explode[5];
                $video_id = $link_explode[7];
                $huddle_workspace = 1;
            }
            if (strtolower($link_explode[3]) == 'video_huddles') {
                $huddle_id = $link_explode[6];
                $comment_id = $link_explode[9];
                $huddle_workspace = 4;
            }
        }
        if(isset($link_explode[4]) && strtolower($link_explode[4])=='goals'){
            $this->set('is_goal_request',true);
        }else{
            $this->set('is_goal_request',false); 
        }

        $this->set('video_id', $video_id);
        $this->set('huddle_id', $huddle_id);
        $this->set('huddle_workspace', $huddle_workspace);
        if (!empty($comment_id)) {
            $this->set('comment_id', $comment_id);
        } else {
            $this->set('comment_id', '');
        }

        $this->set('language_based_content', $language_based_content);
        $url_scheme = print_r($link_explode, true);
        $this->set('url_Scheme', $url_scheme);


        // $this->layout = 'login';
        $redir = $this->Session->read('Auth.redirect');

        if ($redir == '/Users/forgot_password') {
            $this->Session->write('Auth.redirect', '/');
        }
        $error_message = "Uncaught Exception!";
        if ($this->request->is('post')) {

            // $final_destination = $this->Auth->redirectUrl();
            if (!empty($this->Session->read('redirect_uri'))) {
                $final_destination = $this->Session->read('redirect_uri');
            } else {
                $final_destination = "/";
            }

            if (isset($this->request->data['username']) && !empty($this->request->data['username'])) {
                if (filter_var($this->request->data['username'], FILTER_VALIDATE_EMAIL)) {
                    $user = $this->login_by_email();
                } else {
                    $user = $this->login_by_username();
                }

                $user = $user['User'];
                if ($this->Auth->login($user)) {
                    // Commenting following on Asad request because this was creating problem in Push Notifications for Mobile Apps.
                    // $this->UserDeviceLog->deleteAll(array('user_id' => $user['id']), $cascade = true, $callbacks = true);

                    if (isset($this->request->data['remember_me']) && $this->request->data['remember_me'] == 1) {
                        // remove "remember me checkbox"
                        unset($this->request->data['remember_me']);

                        // hash the user's password
                        $this->request->data['password'] = $this->Auth->password($this->request->data['password']);

                        $user_data = array(
                            'username' => $user['username'],
                            'password' => $this->request->data['password']
                        );

                        // write the cookie
                        $this->Cookie->write('remember_me_cookie', $user_data, true, '2 weeks');
                        $this->redirectJsonData = ['authentication_token'=>$user['authentication_token']];
                    }
                    $post_login = $this->setupPostLogin(true);
                    if(!empty($this->Session->read('idp_link_for_ng'))){
                        // var_dump($this->idp_link_for_ng);exit;
                        $idp_link_for_ng = $this->Session->read('idp_link_for_ng');
                        $this->Session->delete('idp_link_for_ng');
                        echo $this->getRedirectJson($idp_link_for_ng, true);
                        exit;
                    }
                    if (is_array($post_login) && $post_login['is_expired']) {
                        $trial_duration = Configure::read('trial_duration');
                        $this->hard_logout(false);
                        echo $this->getRedirectJson('login', false, false, $view->Custom->parse_translation_params($language_based_content['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]));
                        exit;                
                        // $this->Session->write('deactivate_user_msg', $this->language_based_messages['user_is_deactivated_in_accounts']);
                    }
                    if (!$post_login) {
                        $this->hard_logout(false);
                        echo $this->getRedirectJson('login', false, false, $language_based_content['user_is_deactivated_in_accounts']);
                        exit;                
                        // $this->Session->write('deactivate_user_msg', $this->language_based_messages['user_is_deactivated_in_accounts']);
                    }

                    $redirect_url = $final_destination;
                    $redirect_url = explode('/', $redirect_url);
                    $user_current_account = $this->Session->read('user_current_account');
                    $totalAccounts = $this->Session->read('totalAccounts');
                    $this->Session->write('user_top_photo', $user_current_account['User']['image']);

                    if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] == 1) {
                        // $this->redirect('/accounts/account_settings_all/' . $user_current_account['accounts']['account_id'] . '/1');
                        echo $this->getRedirectJson('accounts/account_settings_all/' . $user_current_account['accounts']['account_id'] . '/1');
                        exit;
                    }elseif(!$this->skip_redirect_urls()){
                        $this->Session->write('redirect_uri', "/");
                        echo $this->getRedirectJson($final_destination, true);
                        exit;                    
                    }else{
                        if ($totalAccounts > 1) {
                            echo $this->getRedirectJson('launchpad');
                            exit;                    
                            // $this->redirect("/launchpad");
                        } else {
                            echo $this->getRedirectJson('profile-page', true);
                            exit;                    
                        }
                    }
                } else {
                    $password = sha1(Configure::read('Security.salt') . $this->request->data['password']);
                    $hmhdata = $this->UserDeviceLog->query("select * from users where email = '" . $this->request->data['username'] . "' and password = '" . $password . "' and site_id = 2");
                    if (sizeof($hmhdata) > 0) {
                        $sitesdata = $this->UserDeviceLog->query("select * from sites where site_id = " . $hmhdata[0]['users']['site_id']);
                        $user_accounts = $this->UserDeviceLog->query("SELECT * FROM users_accounts WHERE user_id = " . $hmhdata[0]['users']['id'] . " AND site_id =" . $hmhdata[0]['users']['site_id']);
                        if (count($user_accounts) > 1) {
                            // $this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'] . '?stoh=1');
                            echo $this->getRedirectJson('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'] . '?stoh=1', true);
                            exit;                    
                        } else {
                            // $this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token']);
                            echo $this->getRedirectJson('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'], true);
                            exit;                    
                        }
                    }
                    echo $this->getRedirectJson('login', false, false, __($language_based_content['invalid_email_login']) );
                    exit;
                    // $this->Session->setFlash(__($language_based_content['invalid_email_login']));
                }
            } elseif ($this->Auth->login()) {
                // did they select the remember me checkbox?


                $start_end_date = $this->get_start_end_date($user_current_account['accounts']['account_id']);
                $st_date = $start_end_date['start_date'];
                $end_date = $start_end_date['end_date'];



                if (isset($this->request->data['remember_me']) && $this->request->data['remember_me'] == 1) {
                    // remove "remember me checkbox"
                    unset($this->request->data['remember_me']);

                    // hash the user's password
                    $this->request->data['password'] = $this->Auth->password($this->request->data['password']);

                    // write the cookie
                    $this->Cookie->write('remember_me_cookie', $this->request->data, true, '2 weeks');
                }

                if (!empty($this->request->data['inv_account_id']) && !empty($this->request->data['inv_auth_token']) && !empty($this->request->data['inv_user_id'])) {
                    $this->accept_invitation($this->request->data['inv_account_id'], $this->request->data['inv_auth_token'], $this->request->data['inv_user_id']);
                }
                $post_login = $this->setupPostLogin(true);
                if (!$post_login) {
                    $this->hard_logout(false);
                    echo $this->getRedirectJson('login', false, false, $language_based_content['user_is_deactivated_in_accounts']);
                    exit;
                    // $this->Session->write('deactivate_user_msg', $this->language_based_messages['user_is_deactivated_in_accounts']);
                }
                $redirect_url = $final_destination;
                $redirect_url = explode('/', $redirect_url);
                $user_current_account = $this->Session->read('user_current_account');
                // Commenting following on Asad request because this was creating problem in Push Notifications for Mobile Apps.
                // $this->UserDeviceLog->deleteAll(array('user_id' => $user_current_account['User']['id']), $cascade = true, $callbacks = true);
                $user_role_id = $user_current_account['roles']['role_id'];
                $totalAccounts = $this->Session->read('totalAccounts');

                if (isset($redirect_url[2]) && $redirect_url[2] == 'change_password') {

                    if ($user_role_id == 120) {
                        // $this->redirect('/analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
                        echo $this->getRedirectJson('analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
                        exit;
                    } else {
                        // $this->redirect('/Dashboard/home');
                        echo $this->getRedirectJson('/');
                        exit;
                    }
                } elseif (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
                    // // $this->redirect('/accounts/account_settings_all/' . $user_current_account['accounts']['account_id'] . '/1');
                    // $this->redirect('/accounts/deactivated_account_redirect/' . $user_current_account['accounts']['account_id']);
                    echo $this->getRedirectJson('accounts/deactivated_account_redirect/' . $user_current_account['accounts']['account_id']);
                    exit;
                } else {

                    if ($final_destination == '/' && $totalAccounts > 1) {
                        if ($user_role_id == 120) {
                            if ($final_destination == "/") {
                                // $this->redirect('/analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
                                echo $this->getRedirectJson('analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
                                exit;
                            } else {
                                $this->Session->write('redirect_uri', "/");
                                // $this->redirect($final_destination);
                                echo $this->getRedirectJson($final_destination, true);
                                exit;
                            }
                        } else {
                            // $this->redirect("/launchpad");
                            echo $this->getRedirectJson('launchpad');
                            exit;
                        }
                    } else {

                        if ($user_role_id == 120) {
                            if ($final_destination == "/") {
                                // $this->redirect('/analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
                                echo $this->getRedirectJson('analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
                                exit;
                            } else {
                                $this->Session->write('redirect_uri', "/");
                                // $this->redirect($final_destination);
                                echo $this->getRedirectJson($final_destination, true);
                                exit;
                            }
                        } else {
                            // $this->Session->delete('redirect_uri');
                            // $this->redirect($final_destination);
                            if ($totalAccounts == 1) {
                                // $this->redirect("/");
                                echo $this->getRedirectJson('/');
                                exit;
                            } else {
                                // $this->redirect("/launchpad");
                                echo $this->getRedirectJson('launchpad');
                                exit;
                            }
                        }
                    }

                    exit;
                }
            }
            $password = sha1(Configure::read('Security.salt') . $this->request->data['password']);
            $hmhdata = $this->UserDeviceLog->query("select * from users where username = '" . $this->request->data['username'] . "' and password = '" . $password . "' and site_id = 2");
            if (sizeof($hmhdata) > 0) {
                $sitesdata = $this->UserDeviceLog->query("select * from sites where site_id = " . $hmhdata[0]['users']['site_id']);
                $user_accounts = $this->UserDeviceLog->query("SELECT * FROM users_accounts WHERE user_id = " . $hmhdata[0]['users']['id'] . " AND site_id =" . $hmhdata[0]['users']['site_id']);
                if (count($user_accounts) > 1) {
                    $this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'] . '?stoh=1');
                } else {
                    $this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token']);
                }
            } else {
                $error_message = __($language_based_content['invalid_email_login']);
            }
            $error_message = __($language_based_content['invalid_email_login']);
        }
        echo $this->getRedirectJson('login', false, false, $error_message);
        exit;
    }

    public function sso_login_callback()
    {
        $email = $this->request->query('email');
        $authentication_token = $this->request->query('authentication_token');
        if (!empty($email) && !empty($authentication_token)) {
            $user = $this->User->query("select * from users where authentication_token = '" . $authentication_token . "'");
            if (empty($user)) {
                // $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">' . $email . ' doesn\'t exist.</div>');
                return $this->redirect('/home/login?error='.$email . ' doesn\'t exist.');
            }
            $user = $user[0]['users'];
            // Create Sibme Session and Redirect
            if ($this->Auth->login($user)) {
                $this->Cookie->delete('remember_me_cookie');
                $this->setupPostLogin(true);
                $redirect_url = $this->Session->read('redirect_uri');
                if (empty($redirect_url) || $this->skip_redirect_urls()) {
                    $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                        'user_id' => $user['id']
                    )));
                    $redirect_url = $CurrentUserAccountsCount > 1 ? "/home/launchpad" : "/";
                    $this->Session->delete('redirect_uri');
                }
                return $this->redirect($redirect_url);
            } else {
                $this->Session->delete('last_logged_in_user');
                $this->Session->delete('access_token');
                // $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">Invalid user, try again.</div>');
                return $this->redirect('/home/login?error=Invalid user, try again.');
            }
        } else {
            // $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">Couldn\'t login with SSO. Please use the login screen below..</div>');
            return $this->redirect('/home/login?error=Couldn\'t login with SSO. Please use the login screen below..');
        }
    }

    public function auto_login($user_id, $account_folder_id = '')
    {

        $user = $this->User->findById($user_id);
        $user = $user['User'];

        if ($this->Auth->login($user)) {

            $this->Cookie->delete('remember_me_cookie');
            $post_login = $this->setupPostLogin(true);
            if (!$post_login) {
                $this->Session->write('deactivate_user_msg', $this->language_based_messages['user_is_deactivated_in_accounts']);
            }
            if ($account_folder_id != '') {
                //$this->Session->setFlash(__('You are not allowed to create new Huddles in this account. To request permission to do so, or if you have questions, please <a id="pop-up-btn" data-original-title="User Permissions" rel="tooltip" data-toggle="modal" data-target="#addPermissionModal" href="#">contact</a> your Account Owner.'));
                $redirect_url = '/Huddles/view/' . $account_folder_id;
            } else {
                $redirect_url = $this->Auth->redirect();
            }
            return $this->redirect($redirect_url);
        }
    }

    public function auto_login_url($user_id, $account_folder_id = '')
    {
        //  echo "<pre>";
        //     var_dump($_GET);
        //     echo "</pre>";
        //     die;

        $user = $this->User->findById_url($user_id);
        $user = $user['User'];
        if ($this->Auth->login($user)) {
             
            $this->Cookie->delete('remember_me_cookie');
            //  echo "<pre>";
            // var_dump($_GET);
            // echo "</pre>";
            // die;
            $post_login = $this->setupPostLogin(true);
           
            if (!$post_login) {
                $this->Session->write('deactivate_user_msg', $this->language_based_messages['user_is_deactivated_in_accounts']);
            }
            if ($account_folder_id != '') {
                //$this->Session->setFlash(__('You are not allowed to create new Huddles in this account. To request permission to do so, or if you have questions, please <a id="pop-up-btn" data-original-title="User Permissions" rel="tooltip" data-toggle="modal" data-target="#addPermissionModal" href="#">contact</a> your Account Owner.'));
                $redirect_url = '/Huddles/view/' . $account_folder_id;
            } else {
                if ($_GET['stoh'] == 1) {
                    $redirect_url = '/home/launchpad';
                    
                }else{
                    $redirect_url = $this->Auth->redirect();
                }
                
            }
            return $this->redirect($redirect_url);
        }
    }

    function login_by_email()
    {
        if ($this->request->is('post')) {
            $email = $this->request->data['username'];
            $password = $this->request->data['password'];
            return $this->User->findByEmail($email, $this->Auth->password($password));
        }
    }

    function login_by_username()
    {
        if ($this->request->is('post')) {
            $username = $this->request->data['username'];
            $password = $this->request->data['password'];
            return $this->User->findByUsername($username, $this->Auth->password($password));
        }
    }

    public function logout()
    {
        $angular_request = $this->request->header('Angular-Request');
        if (!$angular_request) {
            $this->hard_logout(false);
            $this->redirect('/home/login'); 
        } else {
            $this->hard_logout();
        }
    }

    function activation($account_id, $auth_token, $user_id, $account_folder_id = '')
    {
        if ($account_id && $auth_token && $user_id) {
            $result = $this->User->find('count', array('conditions' => array('MD5(id)' => $user_id, 'authentication_token' => $auth_token, 'is_active' => FALSE, 'type' => 'Invite_Sent')));
            if ($result && $result == '') {
                $this->Session->setFlash($this->language_based_messages['your_invitation_has_expired_msg'], 'default', array('message error'));
                $this->redirect('/Users/account_settings');
            } else {
                $this->redirect('/Users/account_settings/' . $account_id . '/' . $auth_token . '/' . $user_id . '/' . $account_folder_id);
            }
        }
    }

    public function accept_invitation($inv_account_id, $inv_auth_token, $inv_user_id)
    {

        $logged_in_user_id = $this->Auth->user('id');
        $logged_in_user = $this->User->findById($logged_in_user_id);

        $account_id = 0;
        $role_id = '';
        $user_account_id = 0;
        $result = $this->User->find('count', array('conditions' => array(
            'MD5(id)' => $inv_user_id,
            'authentication_token' => $inv_auth_token,
            'is_active' => FALSE,
            'type' => 'Invite_Sent'
        )));

        if ($result > 0) {

            $InvitedUserAccount = $this->UserAccount->find('first', array('conditions' => array(
                'MD5(user_id)' => $inv_user_id,
                'MD5(account_id)' => $inv_account_id
            )));

            $account_id = $InvitedUserAccount['UserAccount']['account_id'];
            $role_id = $InvitedUserAccount['UserAccount']['role_id'];
            $user_id = $InvitedUserAccount['UserAccount']['user_id'];
            $user_account_id = $InvitedUserAccount['UserAccount']['id'];
        }


        if (!empty($account_id)) {

            $userAccountCount = $this->UserAccount->find('count', array('conditions' => array(
                'user_id' => $logged_in_user_id,
                'account_id' => $account_id
            )));
            //Checking default account is exist or not
            $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                'user_id' => $logged_in_user_id
            )));

            if ($userAccountCount == 0) {
                $this->UserAccount->create();
                $data = array(
                    'user_id' => $logged_in_user_id,
                    'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1)
                );
                $this->UserAccount->updateAll($data, array('id' => $user_account_id), $validation = TRUE);
            } else {
                //this guy already has this account now check what is the role diff.
                $CurrentUserAccount = $this->UserAccount->find('first', array('conditions' => array(
                    'user_id' => $logged_in_user_id,
                    'account_id' => $account_id
                )));

                if ($CurrentUserAccount['UserAccount']['role_id'] > $role_id) {
                    //drop existing membership
                    $this->UserAccount->deleteAll(array('id' => $CurrentUserAccount['UserAccount']['id']));
                    $this->User->deleteAll(array('id' => $InvitedUserAccount['UserAccount']['user_id']));
                    //choose new membership
                    $this->UserAccount->create();
                    $data = array(
                        'user_id' => $logged_in_user_id,
                        'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                        'role_id' => $role_id
                    );
                    $this->UserAccount->updateAll($data, array('id' => $user_account_id), $validation = TRUE);
                }
            }
        }
    }

    public function account_settings($inv_account_id = '', $inv_auth_token = '', $inv_user_id = '', $account_folder_id = '')
    {
        $users = $this->Session->read('user_current_account');

        $this->check_is_account_expired();
        $this->is_account_activated($users['users_accounts']['account_id'], $users['User']['id']);

        $this->layout = 'null';
        $this->set('account_id', $inv_account_id);
        $this->set('auth_token', $inv_auth_token);
        $this->set('user_id', $inv_user_id);
        $this->set('account_folder_id', $account_folder_id);
        $result = $this->User->find('first', array('conditions' => array(
            'MD5(id)' => $inv_user_id,
            'authentication_token' => $inv_auth_token,
            'is_active' => FALSE,
            'type' => 'Invite_Sent'
        )));
        $this->set('user', $result);

        if ($this->request->is('post')) {

            if (strlen($this->request->data['password']) < 6 || strlen($this->request->data['password_confirmation']) < 6) {
                $this->Session->setFlash('For security purposes, your password must contain a minimum of 6 characters.', 'default', array('class' => 'message error'));
                $this->redirect('/Users/account_settings/' . $inv_account_id . '/' . $inv_auth_token . '/' . $inv_user_id . "/" . $account_folder_id);
            }
            if ($this->request->data['password'] != $this->request->data['password_confirmation']) {
                $this->Session->setFlash('Passwords do not match please enter correct passwords.', 'default', array('class' => 'message error'));
                $this->redirect('/Users/account_settings/' . $inv_account_id . '/' . $inv_auth_token . '/' . $inv_user_id . "/" . $account_folder_id);
            }

            if ((preg_match('/[\'^�$%&*()}{@#~?><>,|=_+�-]/', $this->request->data['first_name'])) || (preg_match('/[\'^�$%&*()}{@#~?><>,|=_+�-]/', $this->request->data['last_name']))) {
                $this->Session->setFlash('First name or Last name cannot have special characters.', 'default', array('class' => 'message error'));
                $this->redirect('/Users/account_settings/' . $inv_account_id . '/' . $inv_auth_token . '/' . $inv_user_id . "/" . $account_folder_id);
            }


            $p_inv_account_id = $this->data['inv_account_id'];
            $p_inv_user_id = $this->data['inv_user_id'];
            $p_inv_token_id = $this->data['inv_auth_token'];
            $account_id = '';
            $role = '100';

            $invited_user = $this->User->find('first', array('conditions' => array(
                'MD5(id)' => $p_inv_user_id,
                'authentication_token' => $p_inv_token_id
            )));

            if ($this->User->isUserExists($this->request->data['username'], $invited_user['User']['id']) > 0) {
                $this->Session->setFlash('Username already exists, please try again.');
                $this->redirect('/Users/account_settings/' . $inv_account_id . '/' . $inv_auth_token . '/' . $inv_user_id . "/" . $account_folder_id);
            }

            if ($this->User->isUserEmailExists($this->request->data['email'], $invited_user['User']['id']) > 0) {
                $this->Session->setFlash('Email address already exists, please try again.');
                $this->redirect('/Users/account_settings/' . $inv_account_id . '/' . $inv_auth_token . '/' . $inv_user_id . "/" . $account_folder_id);
            }

            if ($p_inv_account_id != '' && $p_inv_user_id && $p_inv_token_id) {

                $result = $this->User->find('count', array('conditions' => array(
                    'MD5(id)' => $p_inv_user_id,
                    'authentication_token' => $p_inv_token_id,
                    'is_active' => FALSE,
                    'type' => 'Invite_Sent'
                )));

                if ($result > 0) {
                    $InvitedUserAccount = $this->UserAccount->find('first', array('conditions' => array(
                        'MD5(user_id)' => $p_inv_user_id,
                        'MD5(account_id)' => $p_inv_account_id
                    )));
                    $account_id = $InvitedUserAccount['UserAccount']['account_id'];
                    $role = $InvitedUserAccount['UserAccount']['role_id'];
                    $user_id = $InvitedUserAccount['UserAccount']['user_id'];
                }
            }

            if ($account_id) {

                $verification_code = uniqid();
                $data = array(
                    'first_name' => '"' . $this->request->data['first_name'] . '"',
                    'last_name' => '"' . $this->request->data['last_name'] . '"',
                    'email' => '"' . $this->request->data['email'] . '"',
                    'created_date' => '"' . date('Y-m-d H:i:s') . '"',
                    'username' => '"' . $this->request->data['username'] . '"',
                    'password' => '"' . AuthComponent::password($this->request->data['password']) . '"',
                    'type' => '"' . 'Active' . '"',
                    'authentication_token' => "'" . md5($user_id) . "'",
                    'is_active' => TRUE,
                    'verification_code' => '"' . $verification_code . '"'
                );

                if ($_FILES['myfile'] && $_FILES['myfile']['error'] == 0) {

                    $file_ext = strtolower(pathinfo($_FILES['myfile']['name'], PATHINFO_EXTENSION));
                    $thumb_name = rand() . 'thumb.' . $file_ext;

                    if ($this->upload_user_avatar($_FILES['myfile'], $thumb_name, $user_id)) {

                        $data['image'] = "'" . $thumb_name . "'";
                        $data['file_size'] = $_FILES['myfile']['size'];
                    }
                }

                $this->User->create();
                $this->User->updateAll($data, array('id' => $user_id), $validation = TRUE);
                $this->UserAccount->updateAll(array('is_active' => 1, 'status_type' => "'Active'"), array('user_id' => $user_id, 'account_id' => $account_id), $validation = TRUE);
                if ($user_id) {
                    //$this->Session->setFlash(__('The user has been saved'));
                    if ($role == 100)
                        $this->send_welcome_email_template($this->request->data, $account_id);
                    //$this->send_welcome_email($this->request->data, $account_id);

                    if ($p_inv_account_id != '' && $p_inv_user_id != '' && $p_inv_token_id != '') {

                        $huddle_user = $this->AccountFolderUser->find('first', array('conditions' => array(
                            'user_id' => $user_id
                        )));
                        if ($role == 100) {

                            $this->AccountFolderMetaData->create();
                            $data = array(
                                'account_folder_id' => $account_id,
                                'meta_data_name' => 'enable_tags',
                                'meta_data_value' => '1',
                                'created_by' => $user_id,
                                'created_date' => date('Y-m-d H:i:s')
                            );

                            $this->AccountFolderMetaData->save($data, $validation = TRUE);

                            $sampleHuddleId = Configure::read('sample_huddle');
                            $this->copyHuddleForSample($sampleHuddleId, $account_id, $user_id);
                        }
                        $data['to_email'] = $this->request->data['email'];
                        $invitedAccount = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
                        $data['account_name'] = $invitedAccount['Account']['company_name'];
                        $data['newUsername'] = $this->request->data['username'];
                        $data['name'] = $this->request->data['first_name'] . ' ' . $this->request->data['last_name'];
                        $result = $this->UserAccount->get_account_owner($account_id);
                        $data['owner_email'] = $result['users']['email'];
                        $data['ownder_name'] = $result['users']['first_name'] . ' ' . $result['users']['last_name'];
                        $data['user_id'] = $user_id;
                        $data['account_folder_id'] = $account_folder_id;
                        $data['verification_code'] = $verification_code;

                        $this->request->data['id'] = $user_id;
                        $this->request->data['account_id'] = $account_id;

                        $this->afterCompletion($data, $account_id);
                        //   $this->auto_login($user_id, $account_folder_id);
                        $user_detail = $this->User->findById($user_id);
                        $user_detail = $user_detail['User'];

                        if ($this->Auth->login($user_detail)) {

                            $this->Cookie->delete('remember_me_cookie');
                            $this->setupPostLogin(true);
                        }
                        //    $this->activation_email($data, $account_id);
                        $user_account_data = array('is_active' => '1', 'status_type' => "'Active'");
                        $this->UserAccount->updateAll($user_account_data, array('user_id' => $user_id, 'account_id' => $account_id), $validation = TRUE);
                        // Update activation bit in HMH Licenses as well.
                        $hmh_license = $this->User->query("SELECT `hmh_order_licenses`.`id` FROM `hmh_orders` INNER JOIN `hmh_order_licenses` ON (`hmh_orders`.`id` = `hmh_order_licenses`.`order_id`)
                                            WHERE (`hmh_orders`.`account_id` =" . $account_id . " AND `hmh_order_licenses`.`user_id` =$user_id);");
                        if ($hmh_license) {
                            //                            $this->Hmh_order_licenses->updateAll(['user_activated' => "1", 'updated_at' => '"' . date("Y-m-d H:i:s") . '"' ],["id"=>$hmh_license[0]["hmh_order_licenses"]["id"]]);
                            $this->Hmh_order_licenses->updateAll(['user_activated' => "1", 'updated_at' => '"' . date("Y-m-d H:i:s") . '"'], ["user_id" => $user_id]);
                        }
                        $this->redirect('/users/activation_page/' . $account_id . '/' . $user_id);
                    } else {
                        $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
                        $this->redirect('/account_settings/' . $inv_account_id . '/' . $inv_auth_token . '/' . $inv_user_id . "/" . $account_folder_id);
                    }
                }
            }
        }
    }

    function administrators_groups()
    {
        $this->redirect('/people/home');
        return false;
        $users = $this->Session->read('user_current_account');

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('users/administrators_groups');

        $this->check_is_account_expired();
        $this->is_account_activated($users['users_accounts']['account_id'], $users['User']['id']);

        $this->auth_permission('people', 'administrators_groups');
        if ($this->request->is('post')) {
            if ($this->data['user_type'] == 110) {
                $this->create_churnzero_event('Super+Admin+Added', $users['users_accounts']['account_id'], $users['User']['email']);
                $this->addUsers();
            } elseif ($this->data['user_type'] == 115) {
                $this->create_churnzero_event('Admin+Added', $users['users_accounts']['account_id'], $users['User']['email']);
                $this->addUsers();
            } elseif ($this->data['user_type'] == 120) {
                $this->create_churnzero_event('User+Added', $users['users_accounts']['account_id'], $users['User']['email']);
                $this->addUsers();
            } elseif ($this->data['user_type'] == 125) {
                $this->create_churnzero_event('Viewer+Added', $users['users_accounts']['account_id'], $users['User']['email']);
                $this->addUsers();
            } elseif ($this->data['user_type'] == 'groups') {
                $this->_addGrups();
            }
        }

        $users = $this->Session->read('user_current_account');

        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $view = new View($this, true);
        $accountOwner = array(
            'currentRole' => $users['roles']['role_id'],
            'user_id' => $user_id,
            'accountOwner' => $this->_getAccountOwner($account_id),
            'language_based_content' => $language_based_content
        );
        $supperAdmins = array(
            'user_id' => $user_id,
            'supperAdmins' => $this->_getSupperAdmins($account_id, 24),
            'language_based_content' => $language_based_content
        );
        $admins = array(
            'user_id' => $user_id,
            'admins' => $this->_getAdmins($account_id, 24),
            'language_based_content' => $language_based_content
        );
        $groups = array(
            'userGroups' => $this->_groups($account_id),
            'language_based_content' => $language_based_content
        );

        $this->set('huddles', $this->AccountFolder->getAllHuddlesForPeople($account_id));
        $this->set('newAmins', $this->_getNewAdmins($account_id));
        $this->set('account_owner', $this->_getAccountOwner($account_id));
        $this->set('superAdmins', $this->_getSupperAdmins($account_id));
        $this->set('adminUsers', $this->_getAdmins($account_id));
        $this->set('accountOwner', $view->element('users/account-owner', $accountOwner));
        $this->set('supperAdmins', $view->element('users/super-admins', $supperAdmins));
        $this->set('admins', $view->element('users/admins', $admins));
        //        print_r($groups);
        //        die("abc");
        $this->set('groups', $view->element('users/groups', $groups));
        $this->set('language_based_content', $language_based_content);

        //        echo $view->element('users/groups', $groups);
        //        exit();
        $this->render('manage/index');
    }

    function ajax_super_user()
    {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $keywords = isset($this->request->data['keywords']) ? $this->request->data['keywords'] : '';
        $view = new View($this, true);
        $role_id = 110;
        $supperAdmins = array(
            'user_id' => $user_id,
            'supperAdmins' => $this->_getSupperAdminsAjax($account_id, $role_id, $keywords)
        );
        $html = $view->element('users/ajax_super_users', $supperAdmins);
        echo $html;
        die();
    }

    function ajax_user()
    {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $keywords = isset($this->request->data['keywords']) ? $this->request->data['keywords'] : '';
        $view = new View($this, true);
        $role_id = 120;
        $adminUsers = array(
            'user_id' => $user_id,
            'admins' => $this->_getSupperAdminsAjax($account_id, $role_id, $keywords)
        );
        $html = $view->element('users/ajax_users', $adminUsers);
        echo $html;
        die();
    }

    function ajax_groups()
    {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $keywords = isset($this->request->data['keywords']) ? $this->request->data['keywords'] : '';
        $view = new View($this, true);
        $groups = array(
            'userGroups' => $this->_get_groups_ajax($account_id, $keywords)
        );
        $html = $view->element('users/groups_ajax', $groups);
        echo $html;
        die();
    }

    private function _get_groups_ajax($account_id, $keywords)
    {
        $where = '';
        if ($keywords != '') {
            $keywords = '"' . '%' . $keywords . '%' . '"';
            $where = 'WHERE groups.account_id =' . $account_id . ' AND groups.name LIKE ' . $keywords . ' AND groups.site_id =' . $this->site_id;
        } else {
            $where = 'WHERE groups.account_id =' . $account_id . ' AND groups.site_id =' . $this->site_id;
        }
        $sql = 'select * from groups ' . $where;
        $Groups = $this->User->query($sql);
        $arr = array();
        $i = 0;
        if ($Groups) {
            foreach ($Groups as $res) {
                $arr[$i]['department_name'] = $res['groups']['name'];
                $arr[$i]['group_id'] = $res['groups']['id'];
                $sql = "select ug.*,u.*,ua.role_id as role,ua.account_id as account_id
                        from user_groups ug
                        left join users u on u.id = ug.user_id
                        left join users_accounts ua on ua.user_id = u.id
                        where ug.group_id=" . $res['groups']['id'] . " AND ua.account_id = " . $account_id;

                $userGroups = $this->User->query($sql);


                foreach ($userGroups as $res1) {

                    $arr2['username'] = $res1['u']['username'];
                    $arr2['email'] = $res1['u']['email'];
                    $arr2['created_at'] = $res1['u']['created_date'];
                    $arr2['id'] = $res1['u']['id'];
                    $arr2['first_name'] = $res1['u']['first_name'];
                    $arr2['last_name'] = $res1['u']['last_name'];
                    $arr2['account_id'] = $res1['ua']['account_id'];
                    $arr2['image'] = $res1['u']['image'];
                    $arr2['user_group_id'] = $res1['ug']['id'];
                    $arr2['role'] = $res1['ua']['role'];
                    $arr[$i]['users'][] = $arr2;
                }
                $i++;
            }
            return $arr;
        } else {

            return false;
        }
    }

    private function _getSupperAdminsAjax($account_id, $role_id, $keywords)
    {
        $supperAdmin = $this->User->getUsersByAccountAjax($account_id, $role_id, $keywords);
        if (is_array($supperAdmin) && count($supperAdmin) > 0) {
            return $supperAdmin;
        } else {
            return FALSE;
        }
    }

    function addUsers2()
    {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $this->User->create();
        if ($this->request->is('post')) {

            $arr = '';
            $data = '';

            foreach ($this->request->data['users'] as $row) {
                if (isset($row['name'])) {
                    $name = explode(' ', $row['name']);
                    $arr['first_name'] = isset($name[0]) ? $name[0] : '';
                    $arr['last_name'] = isset($name[1]) ? $name[1] : '';
                }

                if (isset($row['email'])) {

                    $arr['created_by'] = $users['User']['id'];
                    $arr['created_date'] = date('Y-m-d H:i:s');
                    $arr['last_edit_by'] = $users['User']['id'];
                    $arr['last_edit_date'] = date('Y-m-d H:i:s');
                    $arr['account_id'] = $account_id;
                    $arr['email'] = $row['email'];
                    $arr['authentication_token'] = $this->digitalKey(20);
                    $arr['is_active'] = false;
                    $arr['type'] = 'Invite_Sent';
                    $arr['message'] = $this->data['message'];
                    if ($arr['first_name'] != '' && $arr['email'] != '') {
                        $data[] = $arr;
                    }
                }
            }

            $roleData = array();
            foreach ($data as $user) {

                if (isset($user['first_name']) && isset($user['email']) && $user['email']) {

                    $exUser = $this->User->find('first', array('conditions' => array('email' => $user['email'])));
                    $userAccountCount = 0;
                    $this->User->create();

                    if (count($exUser) == 0 && $this->User->save($user, $validation = TRUE)) {
                        $user_id = $this->User->id;

                        $this->User->updateAll(array(
                            'authentication_token' => "'" . md5($user_id) . "'"
                        ), array('id' => $user_id));

                        $user['authentication_token'] = md5($user_id);
                    } else {
                        $user_id = $exUser['User']['id'];
                    }

                    if (!empty($user_id)) {
                        $userAccountCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id,
                            'account_id' => $account_id
                        )));

                        if ($userAccountCount == 0) {
                            $this->UserAccount->create();

                            //Checking default account is exist or not
                            $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                                'user_id' => $user_id
                            )));

                            $userAccount = array(
                                'account_id' => $account_id,
                                'user_id' => $user_id,
                                'role_id' => $this->request->data['user_type'],
                                'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                                'created_by' => $users['User']['id'],
                                'created_date' => date("Y-m-d H:i:s"),
                                'last_edit_date' => date("Y-m-d h:i:s"),
                                'last_edit_by' => $users['User']['id']
                            );

                            $this->UserAccount->save($userAccount, $validation = TRUE);

                            if ($user_id) {
                                $user['user_id'] = $user_id;
                                $this->sendInvitation($user);
                            }

                            if (isset($this->request->data['video_huddle_ids'])) {
                                foreach ($this->request->data['video_huddle_ids'] as $row) {
                                    $required_role = $this->request->data['user_type'];
                                    $huddle_role = '';

                                    if ($required_role == 110) {
                                        $huddle_role = 200;
                                    } else if ($required_role == 120) {
                                        $huddle_role = 210;
                                    }

                                    $huddleUserData = array(
                                        'account_folder_id' => $row,
                                        'user_id' => $user_id,
                                        'role_id' => $huddle_role,
                                        'created_date' => date("Y-m-d H:i:s"),
                                        'last_edit_date' => date("Y-m-d h:i:s"),
                                        'created_by' => $users['User']['id'],
                                        'last_edit_by' => $users['User']['id']
                                    );

                                    $this->AccountFolderUser->create();
                                    $this->AccountFolderUser->save($huddleUserData);
                                }
                            }
                        }
                    }
                }
            }
            $this->Session->setFlash($this->language_based_messages['user_has_been_added_successfully_msg'], 'default', array('class' => 'message success'));
            $this->redirect('/users/administrators_groups');
        } else {
            $this->Session->setFlash($this->language_based_messages['please_provide_name_and_fields_msg']);
            $this->redirect('/users/administrators_groups');
        }
    }

    private function _addGrups()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->user();
            $users = $this->Session->read('user_current_account');

            $groupsData = array(
                'name' => $this->data['group']['name'],
                'created_at' => date('Y-m-d H:i:s'),
                'account_id' => $users['accounts']['account_id']
            );

            $this->Group->save($groupsData);

            $users_groups = array(
                'group_id' => $this->Group->id,
                'user_id' => '',
                'created_at' => date('Y-m-d H:i:s')
            );
            if ($this->Group->id != '') {
                foreach ($this->data['super_admin_ids'] as $key => $val) {
                    $sql = "INSERT INTO user_groups (group_id, user_id, created_date,site_id) VALUES('" . $this->Group->id . "','" . $val . "','" . date('Y-m-d H:i:s') . "','" . $this->site_id . "')";
                    $result = $this->UserGroup->query($sql);
                    $this->Session->setFlash($this->data['group']['name'] . ' ' . $this->language_based_messages['group_has_been_added_sucessfully_msg'], 'default', array('class' => 'message success'));
                }
            }
        }
    }

    function addUsers()
    {
        $this->auth_permission('people', 'addUsers');
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $account_owner = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
        $account_owner_info = $this->User->find('first', array('conditions' => array('id' => $account_owner['UserAccount']['user_id'])));


        $accounts = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $deactive_plan = $this->Account->find('first', array('conditions' => array('id' => $account_id), 'fields' => array('deactive_plan', 'plan_qty')));

        $this->User->create();
        if ($this->request->is('post')) {


            App::import('Vendor', 'MailChimp');
            $MailChimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
            $account_info = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
            $mailchimp_list_id = $account_info['Account']['mailchimp_list_id'];

            $data = '';
            $current_user_count = 0;
            foreach ($this->request->data['users'] as $row) {
                if (isset($row['name']) && !empty($row['name'])) {
                    $current_user_count++;
                }
            }

            foreach ($this->request->data['users'] as $key => $row) {
                $joins = array(
                    'table' => 'plans',
                    'type' => 'left',
                    'conditions' => array('Account.plan_id = plans.id')
                );
                $fields = array('plans.category', 'plans.plan_type', 'plans.storage', 'plans.id', 'plans.users', 'Account.*');
                $result = $this->Account->find('first', array(
                    'joins' => array($joins),
                    'conditions' => array('Account.id' => $account_id),
                    'fields' => $fields
                ));

                $totalUsers = $this->User->getTotalUsers($account_id) + $current_user_count;

                if ($result['Account']['in_trial'] == 1)
                    $plan_users = 40;
                else {
                    if (!empty($result['plans']['category'])) {
                        $plan_users = $deactive_plan['Account']['plan_qty'];
                    } else {
                        $plan_users = $result['plans']['users'];
                    }
                }
                $view = new View($this, false);
                if (($totalUsers > $view->Custom->get_allowed_users($account_id)) && ($this->request->data['user_type'] != 125)) {
                    if ($users['users_accounts']['role_id'] == 100) {
                        if ($result['Account']['deactive_plan'] == 1) {

                            $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_users_msg'], ['allowed_users' => $view->Custom->get_allowed_users($account_id), 'sales_email' => $this->custom->get_site_settings('static_emails')['sales']]), 'default', array('class' => 'message error'));
                        } else {
                            $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_users_upgrade_or_delete_msg'], ['allowed_users' => $view->Custom->get_allowed_users($account_id), 'account_id' => $account_id]), 'default', array('class' => 'message error'));
                        }
                    } else {
                        $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_user_contact_account_owner_msg'], ['allowed_users' => $view->Custom->get_allowed_users($account_id), 'first_name' => $account_owner_info['User']['first_name'], 'last_name' => $account_owner_info['User']['last_name'], 'account_owner_email' => $account_owner_info['User']['email']]), 'default', array('class' => 'message error'));
                    }
                    $this->users_full_email($users['User']['email'], $this->request->data['users'][$key + 1]['email'], $account_id);
                    $this->redirect('/users/administrators_groups');
                }
                if (isset($row['name'])) {
                    $name = explode(' ', $row['name']);
                    $arr['first_name'] = isset($name[0]) ? $name[0] : '';
                    $arr['last_name'] = isset($name[1]) ? $name[1] : '';
                }

                if (isset($row['email'])) {
                    $arr['created_by'] = $users['User']['id'];
                    $arr['created_date'] = date('Y-m-d H:i:s');
                    $arr['last_edit_by'] = $users['User']['id'];
                    $arr['last_edit_date'] = date('Y-m-d H:i:s');
                    $arr['account_id'] = $account_id;
                    $arr['email'] = $row['email'];
                    $arr['authentication_token'] = $this->digitalKey(20);
                    $arr['is_active'] = false;
                    $arr['type'] = 'Invite_Sent';
                    if ($arr['first_name'] != '' && $arr['email'] != '') {
                        $data[] = $arr;
                    }
                }
            }

            $roleData = array();
            foreach ($data as $user) {


                if (!empty($mailchimp_list_id)) {
                    $role = $this->request->data['user_type'];
                    $new_segment_id = '';
                    $segment_details = $MailChimp->get("lists/$mailchimp_list_id/segments");
                    foreach ($segment_details['segments'] as $row) {
                        if ($role == 110 && $row['name'] == 'Super Admins') {
                            $new_segment_id = $row['id'];
                        }

                        if ($role == 115 && $row['name'] == 'Admins') {
                            $new_segment_id = $row['id'];
                        }


                        if ($role == 120 && $row['name'] == 'Users') {
                            $new_segment_id = $row['id'];
                        }



                        if ($role == 125 && $row['name'] == 'Viewers') {
                            $new_segment_id = $row['id'];
                        }
                    }

                    $MailChimp->post('/lists/' . $mailchimp_list_id . '/members', [
                        "status" => "subscribed",
                        "email_address" => $user['email'],
                    ]);

                    $MailChimp->post('lists/' . $mailchimp_list_id . '/segments/' . $new_segment_id . '/members', [
                        //   "status" => "subscribed",
                        "email_address" => $user['email'],
                    ]);
                }



                if (isset($user['first_name']) && isset($user['email']) && $user['email']) {
                    $exUser = $this->User->find('first', array('conditions' => array('email' => $user['email'])));
                    $userAccountCount = 0;
                    $this->User->create();

                    if (count($exUser) == 0 && $this->User->save($user, $validation = TRUE)) {

                        $user_id = $this->User->id;

                        $this->User->updateAll(array(
                            'authentication_token' => "'" . md5($user_id) . "'"
                        ), array('id' => $user_id));

                        $user['authentication_token'] = md5($user_id);
                    } else {
                        $user_id = $exUser['User']['id'];
                    }
                    if (!empty($user_id)) {
                        $userAccountCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id,
                            'account_id' => $account_id
                        )));

                        if ($userAccountCount == 0) {
                            $this->UserAccount->create();
                            //Checking default account is exist or not
                            $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                                'user_id' => $user_id
                            )));
                            $userAccount = array(
                                'account_id' => $account_id,
                                'user_id' => $user_id,
                                'role_id' => $this->request->data['user_type'],
                                'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                                'created_by' => $users['User']['id'],
                                'created_date' => date("Y-m-d H:i:s"),
                                'last_edit_date' => date("Y-m-d h:i:s"),
                                'last_edit_by' => $users['User']['id']
                            );
                            if ($this->request->data['user_type'] == 110) {
                                $userAccount['permission_maintain_folders'] = true;
                                $userAccount['permission_access_video_library'] = true;
                                $userAccount['permission_video_library_upload'] = true;
                                $userAccount['permission_administrator_user_new_role'] = true;
                                $userAccount['parmission_access_my_workspace'] = true;
                                $userAccount['folders_check'] = true;
                                $userAccount['manage_collab_huddles'] = true;
                                $userAccount['manage_coach_huddles'] = true;
                                $userAccount['live_recording'] = true;
                            }
                            if ($this->request->data['user_type'] == 115) {
                                $userAccount['permission_maintain_folders'] = true;
                                $userAccount['permission_access_video_library'] = true;
                                $userAccount['permission_video_library_upload'] = true;
                                $userAccount['permission_administrator_user_new_role'] = true;
                                $userAccount['parmission_access_my_workspace'] = true;
                                $userAccount['folders_check'] = true;
                                $userAccount['manage_collab_huddles'] = true;
                                $userAccount['manage_coach_huddles'] = true;
                            }

                            if ($this->request->data['user_type'] == 125) {
                                $userAccount['permission_maintain_folders'] = false;
                                $userAccount['permission_access_video_library'] = false;
                                $userAccount['permission_video_library_upload'] = false;
                                $userAccount['permission_administrator_user_new_role'] = false;
                                $userAccount['parmission_access_my_workspace'] = false;
                                $userAccount['folders_check'] = false;
                                $userAccount['manage_collab_huddles'] = false;
                                $userAccount['manage_coach_huddles'] = false;
                            }
                            if ($this->request->data['user_type'] == 120) {
                                $userAccount['permission_access_video_library'] = true;
                            }
                            $this->UserAccount->save($userAccount, $validation = TRUE);
                            if ($user) {
                                $first_name = isset($user['first_name']) ? $user['first_name'] : '';
                                $last_name = isset($user['last_name']) ? $user['last_name'] : '';

                                $user['user_id'] = $user_id;
                                $user['message'] = $this->request->data['message'];
                                $user['role_title'] = '';
                                if ($this->request->data['user_type'] == 110) {
                                    $user['role_title'] = 'a Super Admin';
                                } elseif ($this->request->data['user_type'] == 115) {
                                    $user['role_title'] = 'an Admiin';
                                } elseif ($this->request->data['user_type'] == 120) {
                                    $user['role_title'] = 'a User';
                                } elseif ($this->request->data['user_type'] == 125) {
                                    $user['role_title'] = 'a Viewer';
                                } else {
                                    $user['role_title'] = 'a User';
                                }
                                if (count($exUser) == 0) {
                                    $this->sendInvitation($user);
                                } else {
                                    $this->sendConfirmation($user);
                                }
                                $user_activity_logs = array(
                                    'ref_id' => $user_id,
                                    'desc' => $users['accounts']['company_name'],
                                    'url' => $this->base . '/users/administrators_groups/' . $account_id . '/' . $user_id,
                                    'account_folder_id' => '',
                                    'type' => '7'
                                );

                                $this->user_activity_logs($user_activity_logs);
                            }

                            $newly_added_users = $this->Session->read('newly_added_users');
                            if (empty($newly_added_users))
                                $newly_added_users = $user_id;
                            else
                                $newly_added_users .= "," . $user_id;
                            $this->Session->write('newly_added_users', $newly_added_users);
                        }
                    }
                }
            }

            if ($userAccountCount == 0) {
                $this->Session->setFlash(($this->request->data['user_type'] == 110 ? $this->language_based_messages['super_admin_msg'] : $this->language_based_messages['user_msg']) . ' ' . $this->language_based_messages['has_been_added_successfully_msg'], 'default', array('class' => 'message success'));
                $this->redirect('/users/administrators_groups');
            } else {
                $this->Session->setFlash($this->language_based_messages['user_already_exists_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/users/administrators_groups');
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['please_provide_name_and_fields_msg']);
            $this->redirect('/users/administrators_groups');
        }
    }

    function upload($file, $new_file_name)
    {

        if (isset($file['error']) && $file['error'] == 0) {

            $filename = $this->uploadDir . $new_file_name;

            if (!move_uploaded_file($file['tmp_name'], $filename)) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->Session->setFlash(__($this->language_based_messages['please_upload_file_msg'], true));

            return FALSE;
        }
    }

    private function _getAccountOwner($account_id)
    {
        $supperUsers = $this->User->getUsersByAccount($account_id, 100);
        if (is_array($supperUsers) && count($supperUsers) > 0) {
            return $supperUsers[0];
        } else {

            return FALSE;
        }
    }

    private function _getSupperAdmins($account_id, $limit = '')
    {
        $supperAdmin = $this->User->getUsersByAccount($account_id, 110, $limit);

        if (is_array($supperAdmin) && count($supperAdmin) > 0) {
            return $supperAdmin;
        } else {

            return FALSE;
        }
    }

    private function _getSupperAdmins2($account_id)
    {
        $supperAdmin = $this->User->getUsersByAccount2($account_id, 110);

        if (is_array($supperAdmin) && count($supperAdmin) > 0) {
            return $supperAdmin;
        } else {

            return FALSE;
        }
    }

    private function _getAdmins2($account_id)
    {
        $admin = $this->User->getUsersByAccount2($account_id, 120);
        if (is_array($admin) && count($admin) > 0) {
            return $admin;
        } else {
            return FALSE;
        }
    }

    private function _getAdmins($account_id, $limit = '')
    {
        $admin = $this->User->getUsersByAccount($account_id, 120, $limit);
        if (is_array($admin) && count($admin) > 0) {
            return $admin;
        } else {

            return FALSE;
        }
    }

    private function _getNewAdmins($account_id, $limit = '')
    {
        $admin = $this->User->getUsersByAccount($account_id, 115, $limit);
        if (is_array($admin) && count($admin) > 0) {
            return $admin;
        } else {

            return FALSE;
        }
    }

    private function _groups($account_id)
    {
        $sql = "select * from groups where account_id=" . $account_id . " AND site_id =" . $this->site_id;
        $Groups = $this->User->query($sql);
        $arr = array();
        $i = 0;
        if ($Groups) {
            foreach ($Groups as $res) {
                $arr[$i]['department_name'] = $res['groups']['name'];
                $arr[$i]['group_id'] = $res['groups']['id'];
                $sql = "select ug.*,u.*,ua.role_id as role,ua.account_id as account_id
                        from user_groups ug
                        left join users u on u.id = ug.user_id
                        left join users_accounts ua on ua.user_id = u.id
                        where ug.group_id=" . $res['groups']['id'] . " AND ua.account_id = " . $account_id;

                $userGroups = $this->User->query($sql);


                foreach ($userGroups as $res1) {

                    $arr2['username'] = $res1['u']['username'];
                    $arr2['email'] = $res1['u']['email'];
                    $arr2['created_at'] = $res1['u']['created_date'];
                    $arr2['id'] = $res1['u']['id'];
                    $arr2['first_name'] = $res1['u']['first_name'];
                    $arr2['last_name'] = $res1['u']['last_name'];
                    $arr2['account_id'] = $res1['ua']['account_id'];
                    $arr2['image'] = $res1['u']['image'];
                    $arr2['user_group_id'] = $res1['ug']['id'];
                    $arr2['role'] = $res1['ua']['role'];
                    $arr[$i]['users'][] = $arr2;
                }
                $i++;
            }
            return $arr;
        } else {

            return false;
        }
    }

    function editUser($user_id = '', $key = '')
    {
        return $this->redirect('/home/user-settings', 301);

        $this->check_is_account_expired();

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('Users/editUser');
        $this->set('language_based_content', $language_based_content);


        $this->layout = 'library';
        $users = $this->Session->read('user_current_account');
        if ($users['User']['id'] != $user_id) {
            $this->Session->setFlash($language_based_content['no_account_access_us'], 'default', array('class' => 'message success'));
            return $this->redirect('/Dashboard/home');
        }
        $allAccounts = $this->Session->read('user_accounts');

        $account_id = $users['accounts']['account_id'];
        $this->is_account_activated($account_id, $user_id);

        if (empty($user_id)) {
            $user_id = $users['User']['id'];
        }

        if (!empty($this->data)) {
            $password = '';
            $data = array(
                'first_name' => "'" . addslashes($this->data['user']['first_name']) . "'",
                'last_name' => "'" . addslashes($this->data['user']['last_name']) . "'",
                'username' => "'" . addslashes($this->data['user']['username']) . "'",
                'title' => "'" . addslashes($this->data['user']['title']) . "'",
                'email' => "'" . addslashes($this->data['user']['email']) . "'",
                // 'time_zone' => '"' . $this->data['time_zone'] . '"',
                'last_edit_date' => '"' . date('Y-m-d H:i:s') . '"',
                'last_edit_by' => $users['User']['id'],
                'institution_id' => "'" . $this->data['user']['institution_id'] . "'",
            );


            if ($this->User->isUserExists($this->data['user']['username'], $user_id) > 0) {
                $this->Session->setFlash($language_based_content['username_already_exist_us']);
                $this->redirect('/Users/editUser/' . $user_id);
                die;
            }

            if ($this->User->isUserEmailExists($this->data['user']['email'], $user_id) > 0) {
                $this->Session->setFlash($language_based_content['email_already_exist_us']);
                $this->redirect('/Users/editUser/' . $user_id);
                die;
            }

            if (isset($this->request->data['user']['password']) && $this->request->data['user']['password'] != '') {
                if ($this->request->data['user']['password'] == $this->request->data['user']['password_confirmation']) {
                    if (strlen($this->request->data['user']['password']) >= 6) {
                        $data['password'] = '"' . AuthComponent::password($this->request->data['user']['password']) . '"';
                        $this->User->updateAll($data, array('id' => $user_id));

                        if ($this->User->getAffectedRows() > 0) {
                            $this->Session->setFlash($language_based_content['user_profile_updated_us'], 'default', array('class' => 'message success'));
                            $this->redirect('/Users/editUser/' . $user_id);
                        } else {
                            $this->Session->setFlash($language_based_content['user_profile_not_updated_us'], 'default', array('class' => 'message error'));
                            $this->redirect('/Users/editUser/' . $user_id);
                        }
                    } else {
                        $this->Session->setFlash($language_based_content['password_limit_us']);
                        $this->redirect('/Users/editUser/' . $user_id);
                    }
                } else {
                    $this->Session->setFlash($language_based_content['password_not_match_us']);
                    $this->redirect('/Users/editUser/' . $user_id);
                }
            } else {

                $this->User->updateAll($data, array('id' => $user_id));
                if ($this->User->getAffectedRows() > 0) {
                    //  $this->setupPostLogin(true);
                    $this->Session->setFlash($language_based_content['user_profile_updated_us'], 'default', array('class' => 'message success'));
                    $this->redirect('/Users/editUser/' . $user_id);
                } else {
                    $this->Session->setFlash($language_based_content['user_profile_not_updated_us'], 'default', array('class' => 'message error'));
                    $this->redirect('/Users/editUser/' . $user_id);
                }
            }
        } else {
            $permissions_set = $this->set_permissions($user_id);
            $user = $this->User->find('first', array('conditions' => array('id' => $user_id)));
            $user_photo = $this->Session->write('user_top_photo', $user['User']['image']);
            //now find account of current user.

            $editing_user_accounts = $this->UserAccount->find('count', array('conditions' => array(
                'user_id' => $user_id,
                'account_id' => $account_id
            )));

            if ($editing_user_accounts == 0) {
                $this->Session->setFlash($language_based_content['no_account_access_us'], 'default', array('class' => 'message success'));
                return $this->redirect('/Dashboard/home');
            }
            //check User Roles
            $userAccount = $this->UserAccount->get($account_id, $user_id);

            $this->set('site_id', $this->site_id);
            $this->set('user', $user);
            $this->set('current_user', $users);
            $this->set('allAccounts', $allAccounts);
            $this->set('key', $key);
            $this->set('optInIds', $this->EmailUnsubscribers->find('all', array('conditions' => array('user_id' => $user_id, 'account_id' => $account_id))));
            $this->set('emailFormat', $this->EmailFormats->query('select * from email_formats as EmailFormats'));
            $this->set('account_id', $account_id);
            $this->set('permissions', $permissions_set);
            $this->render('edit');
        }
    }

    function uploadImage($user_id)
    {
        $file = $this->data['image'];

        $user = $this->User->find('first', array('conditions' => array('id' => $user_id)));

        $file_ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
        $thumb_name = rand() . 'thumb.' . $file_ext;

        $bytes = $file['size'];
        $bytes = number_format($bytes / 1048576, 2) . ' MB';

        if ($this->upload_user_avatar($file, $thumb_name, $user_id)) {
            $file_name = $thumb_name;
            $imageData = array(
                'image' => "'" . $file_name . "'",
                'file_size' => "'" . $file['size'] . "'"
            );
            $this->User->updateAll($imageData, array('id' => $user_id));
            if ($this->User->getAffectedRows() > 0) {
                $filename = $this->uploadDir . Inflector::slug(pathinfo($user['User']['image'], PATHINFO_FILENAME)) . '.' . pathinfo($user['User']['image'], PATHINFO_EXTENSION);
                @unlink($filename);
                //   $this->setupPostLogin(true);
                $this->Session->setFlash($this->language_based_messages['file_has_been_uploaded_successfully_msg'], 'default', array('class' => 'message success'));
            } else {
                $filename = $this->uploadDir . Inflector::slug(pathinfo($file['name'], PATHINFO_FILENAME)) . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
                @unlink($filename);
                $this->Session->setFlash($this->language_based_messages['image_is_not_uploaded_msg'], 'default', array('class' => 'message error'));
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['image_is_not_uploaded_msg'], 'default', array('class' => 'message error'));
        }
        $this->redirect('/Users/editUser/' . $user_id);
    }

    function deleteAccount($user_id, $logout = 0, $flashRequired = 1)
    {
        App::import('Vendor', 'MailChimp');
        $MailChimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
        $users = $this->Session->read('user_current_account');
        $current_account_id = $users['accounts']['account_id'];

        if ($user_id) {
            //            $user_account = $this->UserAccount->find('first', array('conditions' => array(
            //                    'user_id' => $user_id,
            //                    'account_id' => $current_account_id
            //                ))
            //            );
            //get account owner
            $account_info = $this->Account->find('first', array('conditions' => array('id' => $current_account_id)));
            $user_data = $this->User->find('first', array('conditions' => array('id' => $user_id)));
            $email_id = $user_data['User']['email'];
            $mailchimp_list_id = $account_info['Account']['mailchimp_list_id'];
            $subscriber_hash = $MailChimp->subscriberHash($email_id);
            $MailChimp->delete("lists/$mailchimp_list_id/members/$subscriber_hash");
            //            $MailChimp->success()
            $user_activity_logs = array(
                'ref_id' => $user_id,
                'desc' => $users['accounts']['company_name'],
                'url' => $this->base . '/users/administrators_groups/' . $current_account_id . '/' . $user_id,
                'account_folder_id' => '',
                'type' => '17'
            );

            $this->user_activity_logs($user_activity_logs);

            $current_account_owner = $this->UserAccount->find('first', array('conditions' => array(
                'role_id' => '100',
                'account_id' => $current_account_id
            )));

            $current_account_owner_id = $current_account_owner['UserAccount']['user_id'];

            $account_folder = array(
                'created_by' => $current_account_owner_id,
                'last_edit_by' => $current_account_owner_id
            );

            $account_folder_users = array(
                'user_id' => $current_account_owner_id,
                'created_by' => $current_account_owner_id,
                'last_edit_by' => $current_account_owner_id,
            );

            $result = $this->UserAccount->find('first', array('conditions' => array('user_id' => $user_id, 'account_id' => $current_account_id)));

            $this->AccountFolderUser->updateAll($account_folder_users, array(
                'user_id' => $user_id,
                'account_folder_id IN ( select account_folder_id from account_folders where account_id = ' . $current_account_id . ' )',
                'role_id' => 200
            ), $validation = TRUE);

            $this->AccountFolderUser->deleteAll(array(
                'user_id' => $user_id,
                'account_folder_id IN ( select account_folder_id from account_folders where account_id = ' . $current_account_id . ' )',
                'role_id IN (210, 220)'
            ));

            $this->AccountFolder->updateAll(array('active' => 0), array(
                'account_id' => $current_account_owner['UserAccount']['account_id'],
                'folder_type' => 3,
                'created_by' => $user_id
            ));

            $this->AccountFolder->updateAll(
                $account_folder,
                array(
                    'created_by' => $user_id,
                    'account_id' => $current_account_id,
                    'folder_type' => array(1, 2)
                ),
                $validation = TRUE
            );

            //            $account_huddles = $this->AccountFolderUser->get_all_account_folders_user_by_account($user_id, $current_account_id);
            //            if ($account_huddles) {
            //
            //                for ($i = 0; $i < count($account_huddles); $i++) {
            //                    $this->AccountFolderUser->deleteAll(array('account_folder_user_id' => $account_huddles[$i]['AccountFolderUser']['account_folder_user_id']));
            //                }
            //            }

            $this->UserGroup->deleteAll(array('user_id' => $user_id));
            $this->UserAccount->deleteAll(array('user_id' => $user_id, 'account_id' => $current_account_id));

            $user_accounts = $this->UserAccount->find('count', array('conditions' => array('user_id' => $user_id)));
            if ($user_accounts == 0) {
                //$this->User->deleteAll(array('id' => $user_id));
                //                $user_data = $this->User->find('first', array('conditions' => array('id' => $user_id)));

                $this->User->updateAll(array(
                    'email' => "'" . $user_data['User']['email'] . '-deleted' . "'",
                    'username' => "'" . $user_data['User']['username'] . '-deleted' . "'",
                    'is_active' => '0'
                ), array(
                    'id' => $user_id
                ));
                if ($users['User']['id'] == $user_id)
                    $logout = 1;
                else
                    $logout = 0;
            }
        }


        if ($flashRequired === 1) {
            if ($this->User->getAffectedRows() > 0 && $logout == 1) {
                if ($result['UserAccount']['role_id'] == 110) {
                    $this->Session->setFlash($this->language_based_messages['successfully_deleted_super_admin_msg'], 'default', array('class' => 'message success'));
                } elseif ($result['UserAccount']['role_id'] == 115) {
                    $this->Session->setFlash($this->language_based_messages['successfully_deleted_admin_msg'], 'default', array('class' => 'message success'));
                } elseif ($result['UserAccount']['role_id'] == 120) {
                    $this->Session->setFlash($this->language_based_messages['successfully_deleted_user_msg'], 'default', array('class' => 'message success'));
                } elseif ($result['UserAccount']['role_id'] == 125) {
                    $this->Session->setFlash($this->language_based_messages['successfully_deleted_viewer_msg'], 'default', array('class' => 'message success'));
                }
                $this->redirect('/users/logout');
            } elseif ($this->User->getAffectedRows() > 0) {
                if ($result['UserAccount']['role_id'] == 110) {
                    $this->Session->setFlash($this->language_based_messages['successfully_deleted_super_admin_msg'], 'default', array('class' => 'message success'));
                } elseif ($result['UserAccount']['role_id'] == 115) {
                    $this->Session->setFlash($this->language_based_messages['successfully_deleted_admin_msg'], 'default', array('class' => 'message success'));
                } elseif ($result['UserAccount']['role_id'] == 120) {
                    $this->Session->setFlash($this->language_based_messages['successfully_deleted_user_msg'], 'default', array('class' => 'message success'));
                } elseif ($result['UserAccount']['role_id'] == 125) {
                    $this->Session->setFlash($this->language_based_messages['successfully_deleted_viewer_msg'], 'default', array('class' => 'message success'));
                }
                $this->redirect('/users/administrators_groups');
            } else {
                if ($result['UserAccount']['role_id'] == 110) {
                    $this->Session->setFlash($this->language_based_messages['unable_to_delete_super_admin_msg'], 'default', array('class' => 'message error'));
                } elseif ($result['UserAccount']['role_id'] == 115) {
                    $this->Session->setFlash($this->language_based_messages['unable_to_delete_admin_msg'], 'default', array('class' => 'message error'));
                } elseif ($result['UserAccount']['role_id'] == 120) {
                    $this->Session->setFlash($this->language_based_messages['unable_to_delete_user_msg'], 'default', array('class' => 'message error'));
                } elseif ($result['UserAccount']['role_id'] == 125) {
                    $this->Session->setFlash($this->language_based_messages['unable_to_delete_viewer_msg'], 'default', array('class' => 'message error'));
                }
                $this->redirect('/users/administrators_groups');
            }
        } else {
            if ($this->User->getAffectedRows() > 0) {
                echo "<br>Successfuly Removed " . $email_id;
            }
        }
    }

    function deleteAccountsBatch()
    {
        $csvFile = file('SW-914.csv');
        $data = [];
        foreach ($csvFile as $key => $line) {
            $email = str_getcsv($line)[3];
            $user = $this->User->find('first', array('conditions' => array('email' => $email)));
            $user_id = $user['User']['id'];
            $user_name = $user['User']['username'];
            if (!empty($user_id)) {
                echo "<br>Removing: " . $user_id . " - " . $email;
                $this->deleteAccount($user_id, 0, 0);
            }
        }
        $res = $key + 1;
        echo "<br>Processed " . $res . " Records.";
        exit;
    }

    function send_user_messages()
    {
        if ($this->request->is('post')) {
            $data = array(
                'message' => $this->data['message'],
                'sender_id' => $this->data['sender_id'],
                'recipient_id' => $this->data['recipient_id'],
                'message_type' => $this->data['message_type'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $recipientUser = $this->User->find('first', array('conditions' => array('id' => $data['recipient_id'])));
            if ($this->send_users_message($data, $recipientUser)) {
                $this->Session->setFlash($this->language_based_messages['message_has_been_sent_successfully_msg'], 'default', array('class' => 'message success'));
            } else {
                $this->Session->setFlash($this->language_based_messages['you_are_unable_to_send_your_message_msg']);
            }
            $this->redirect('/users/administrators_groups');
        } else {
            $this->Session->setFlash($this->language_based_messages['function_is_under_development_msg']);
            $this->redirect('/users/administrators_groups');
        }
    }

    function deleteGroups($group_id)
    {
        if (isset($group_id) && $group_id != '') {
            $groupCount = $this->Group->find('count', array('conditions' => array('id' => $group_id)));

            if ($groupCount > 0) {
                $usreGrupCount = $this->UserGroup->find('count', array('conditions' => array('group_id' => $group_id)));

                if ($usreGrupCount > 0) {
                    $result = $this->UserGroup->deleteAll(array('group_id' => $group_id));
                    if ($this->UserGroup->getAffectedRows() > 0) {
                        $result = $this->Group->deleteAll(array('id' => $group_id));
                        if ($this->Group->getAffectedRows() > 0) {
                            $this->Session->setFlash($this->language_based_messages['selected_group_has_been_deleted_successfully_msg'], 'default', array('class' => 'message success'));
                        }
                    } else {
                        $this->Session->setFlash($this->language_based_messages['you_are_unable_to_delete_this_record_msg']);
                    }
                } else {
                    $result = $this->Group->deleteAll(array('id' => $group_id));
                    if ($this->Group->getAffectedRows() > 0) {
                        $this->Session->setFlash($this->language_based_messages['selected_group_has_been_deleted_successfully_msg'], 'default', array('class' => 'message success'));
                    } else {
                        $this->Session->setFlash($this->language_based_messages['you_are_unable_to_delete_this_record_msg']);
                    }
                }
            }
            $this->redirect('/users/administrators_groups');
        } else {
            $this->Session->setFlash($this->language_based_messages['please_provide_valid_credentials_msg']);
            $this->redirect('/users/administrators_groups');
        }
    }

    function deleteGroupUsers($userGroupId)
    {
        if ($userGroupId != '') {
            $usreGrupCount = $this->UserGroup->find('count', array('conditions' => array('id' => $userGroupId)));
            if ($usreGrupCount > 0) {
                $result = $this->UserGroup->deleteAll(array('id' => $userGroupId));
                if ($result) {
                    $this->Session->setFlash($this->language_based_messages['selected_user_has_been_deleted_successfully_msg'], 'default', array('class' => 'message success'));
                } else {
                    $this->Session->setFlash($this->language_based_messages['you_have_enable_to_delete_this_user_msg'], 'default', array('class' => 'message error'));
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['please_provide_valid_credentials_msg'], 'default', array('class' => 'message error'));
            }
            $this->redirect('/users/administrators_groups');
        } else {
            $this->Session->setFlash($this->language_based_messages['please_provide_valid_credentials_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/users/administrators_groups');
        }
    }

    function editGroups($group_id)
    {

        $users = $this->Session->read('user_current_account');
        $view = new View($this, true);
        $language_based_content = $view->Custom->get_page_lang_based_content('users/administrators_groups');
        $account_id = $users['accounts']['account_id'];
        $this->set('accountOwner', $this->_getAccountOwner($account_id));
        $this->set('group_id', $group_id);
        $this->set('userGroup', $this->_get_groups($group_id));
        $this->set('superAdmins', $this->_getSupperAdmins($account_id));
        $this->set('adminUsers', $this->_getAdmins($account_id));
        $this->set('newAdminUsers', $this->_getNewAdmins($account_id));
        $this->set('language_based_content', $language_based_content);
        $this->render('manage/groups/edit');
    }

    private function _get_groups($group_id)
    {
        $Groups = $this->Group->find('first', array('conditions' => array('id' => $group_id)));
        $arr = array();
        $i = 0;
        if ($Groups) {
            $arr['department_name'] = $Groups['Group']['name'];
            $arr['group_id'] = $Groups['Group']['id'];
            $usreGrups = $this->UserGroup->find('all', array('conditions' => array('group_id' => $arr['group_id'])));
            $data = '';
            $groupData = '';
            if (isset($usreGrups) && count($usreGrups) > 0) {

                foreach ($usreGrups as $group) {
                    $groupData[] = $group;
                }
            }
            $usersData = array(
                'name' => $Groups['Group']['name'],
                'id' => $Groups['Group']['id'],
                'groups' => $groupData
            );
            return $usersData;
        } else {

            return false;
        }
    }

    function edit($group_id)
    {
        $users = $this->Session->read('user_current_account');

        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $groupsData = array(
            'name' => '"' . $this->data['group']['name'] . '"',
            'last_edit_date' => '"' . date('Y-m-d H:i:s') . '"',
            'account_id' => '"' . $account_id . '"',
            'last_edit_by' => '"' . $user_id . '"'
        );
        $affected_row = $this->Group->update($group_id, $groupsData);

        $users_groups = array(
            'group_id' => $group_id,
            'user_id' => '',
            'created_at' => date('Y-m-d H:i:s')
        );
        $result = $this->UserGroup->deleteAll(array('group_id' => $group_id));
        $users_groups = array(
            'group_id' => $group_id,
            'user_id' => '',
            'created_at' => date('Y-m-d H:i:s')
        );
        if ($group_id != '' && isset($this->data['super_admin_ids']) && count($this->data['super_admin_ids']) > 0) {
            foreach ($this->data['super_admin_ids'] as $key => $val) {
                $sql = "INSERT INTO user_groups (group_id, user_id, last_edit_date,site_id) VALUES('" . $group_id . "','" . $val . "','" . date('Y-m-d H:i:s') . "','" . $this->site_id . "')";
                $result = $this->UserGroup->query($sql);
                $this->Session->setFlash($this->data['group']['name'] . ' ' . $this->language_based_messages['group_has_been_updated_successfully_msg'], 'default', array('class' => 'success message'));
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['no_record_was_updated_please_try_again_msg'], 'default', array('class' => 'error message'));
        }
        $this->redirect('/users/administrators_groups');
    }

    function resendInvitation($account_id, $user_id)
    {
        $users = $this->User->getUserInformation($account_id, $user_id);

        if ($users) {
            $email = array(
                'first_name' => $users['User']['first_name'],
                'last_name' => $users['User']['last_name'],
                'user_id' => $users['User']['id'],
                'email' => $users['User']['email'],
                'account_id' => $users['accounts']['account_id'],
                'authentication_token' => $users['User']['authentication_token'],
                'message' => ''
            );

            //if ($users['User']['email'] == 'janeroe@sibme.com') {
            //    $sent = true;
            //} else {
            $sent = $this->sendInvitation($email);
            //}

            if ($sent) {
                $this->Session->setFlash($this->language_based_messages['activation_email_sent_successfully_to_msg'] . ' ' . $email['first_name'] . ' ' . $email['last_name'], 'default', array('class' => 'message success'));
                $this->redirect('/permissions/assign_user/' . $account_id . '/' . $user_id);
            } else {
                $this->Session->setFlash($this->language_based_messages['activation_email_failed_msg']);
                $this->redirect('/permissions/assign_user/' . $account_id . '/' . $user_id);
            }
        } else {

            $this->Session->setFlash($this->language_based_messages['user_is_already_activated_msg']);
            $this->redirect('/permissions/assign_user/' . $account_id . '/' . $user_id);
        }
    }

    function forgot_password()
    {
        $this->redirect('/home/forgot_password');
        exit;
    }

    function forgot_password_api()
    {
        // $this->set('title_for_layout', "Forgot Password");
        // $view = new View($this, true);
        // $language_based_content = $view->Custom->get_page_lang_based_content('users/forgot_password');
        // $this->set('language_based_content', $language_based_content);

        if ($this->request->is('post')) {
            $view = new View($this, true);
            $language_based_content = $view->Custom->get_page_lang_based_content('users/forgot_password');

            if (!empty($this->data)) {
                if (empty($this->data['email'])) {
                    // $this->Session->setFlash($language_based_content['please_provide_email_fp'], 'default', array('class' => 'message success'));
                    $message = $language_based_content['please_provide_email_fp'];
                } else {
                    $email = trim($this->data['email']);
                    $user = $this->User->find('first', array('conditions' => array('email' => $email)));
                    $repeated_support = str_replace('{support}', $this->custom->get_site_settings('static_emails')['support'], $language_based_content['repeated_difficulty_fp']);

                    if ($user) {

                        if ($user['User']['is_active']) {

                            $key = Security::hash(String::uuid(), 'sha512', true);
                            //                            $url = Router::url(array('controller' => 'users', 'action' => 'change_password'), true) . '/' . $key;
                            //                            $url = wordwrap($url, 1000);
                            $url = Configure::read('sibme_base_url') . 'public/reset_password?key=' . $key . '&site=' . $this->site_id;
                            $html = '<p><h5>Hi ' . $user['User']['first_name'] . ' ' . $user['User']['last_name'] . '</h5></p>';
                            //$html .='<p><h5>Can`t remember your password? Don`t worry about it - it happens.</h5></p>';
                            $html .= '<p><h5>Your Username is: <a href="mailto:' . $user['User']['email'] . '">' . $user['User']['username'] . '</a></p>';
                            $html .= '<p><strong><a href=' . $url . '>Click this link to reset your password</a> </strong></p>';
                            $html .= '<hr>';
                            $html .= '<p><h4>Didn`t ask to reset your password?</h4></p>';
                            $html .= '<p>If you didn`t ask to reset your password, it`s likely that another user entered your username or email address by mistake while trying to reset their password. If that`s the case, you don`t need to take any other action and can safely disregard this email.</p>';
                            $data = array(
                                'reset_password_token' => "'" . $key . "'"
                            );

                            $this->User->id = $user['User']['id'];
                            $user['User']['reset_password_url'] = $url;
                            $this->User->updateAll($data, array('id' => $this->User->id));
                            if ($this->User->getAffectedRows() > 0) {

                                $this->Email->delivery = 'smtp';

                                $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
                                $this->Email->to = $email;

                                $this->Email->template = 'default';
                                $this->Email->sendAs = 'html';
                                // die($this->Email->to);
                                $result = array(
                                    'data' => $user,
                                    'reset_pass_url' => $url
                                );
                                $lang = $user['User']['lang'];
                                if ($lang == 'en') {
                                    $this->Email->subject = $view->Custom->get_site_settings('email_subject') . '  - Password Reset';
                                } else {
                                    $this->Email->subject = $view->Custom->get_site_settings('email_subject') . '  - Restablecer contraseña';
                                }
                                $key = "forgot_password_" . $this->site_id . "_" . $lang;
                                $forgot_password = $this->get_send_grid_contents($key);
                                $html = $forgot_password->versions[0]->html_content;
                                $html = str_replace('<%body%>', '', $html);
                                $html = str_replace('{sender}', $user['User']['first_name'] . ' ' . $user['User']['last_name'], $html);
                                $html = str_replace('{username}', $user['User']['username'], $html);
                                $html = str_replace('{reset_password_url}', $url, $html);

                                // Now html is loading from SendGrid.
                                //$html = $view->element('users/' . $this->site_id . '/forgot_password', $result);

                                $this->Email->send($html);

                                // $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);

                                // $this->Session->setFlash($language_based_content['check_email_to_reset_fp'], 'default', array('class' => 'message success'));
                                echo $this->getRedirectJson('forgot_password', false, true, $language_based_content['check_email_to_reset_fp']);
                                exit;

                            } else {
                                // $this->Session->setFlash($language_based_content['error_generating_fp'], 'default', array('class' => 'message error'));
                                $message = $language_based_content['error_generating_fp'];
                            }
                        } else {
                            $count = (int) $this->Session->read('forgot-pass-inactive-email');
                            if ($count % 2 == 0) {
                                // $this->Session->setFlash($repeated_support, 'default', array('class' => 'message'));
                                $message = $repeated_support;
                            } else {
                                // $this->Session->setFlash($language_based_content['couldnt_match_fp'], 'default', array('class' => 'message'));
                                $message = $language_based_content['couldnt_match_fp'];
                            }
                            $this->Session->write('forgot-pass-inactive-email', $count + 1);
                        }
                    } else {
                        if ($this->site_id != 1) {
                            $this->Session->setFlash('There is no record of this email address in the HMH system. Try again or please contact ' . $this->custom->get_site_settings('static_emails')['support'], 'default', array('class' => 'message error'));
                            $message = 'There is no record of this email address in the HMH system. Try again or please contact ' . $this->custom->get_site_settings('static_emails')['support'];
                        } else {
                            $repeated_support = str_replace('{support}', $this->custom->get_site_settings('static_emails')['support'], $language_based_content['no_record_of_fp']);
                            // $this->Session->setFlash($repeated_support, 'default', array('class' => 'message error'));
                            $message = $repeated_support;
                        }
                    }
                }
            }
        } else {
            // echo json_encode(["success"=>false, "message"=>"Get Request Not Allowed."]);
            // exit;
            $message = "Get Request Not Allowed.";
        }
        echo $this->getRedirectJson('forgot_password', false, false, $message);
        exit;
    }

    function change_password($key)
    {
        $user = $this->User->find('first', array('conditions' => array('reset_password_token' => $key)));
        if ($user) {
            return $this->redirect('/public/reset_password?key=' . $key . '&site=' . $user['User']['site_id'], 301);
        } else {
            return $this->redirect('/users/login', 301);
        }
        $this->set('title_for_layout', "Change Password");
        $this->Auth->logout();
        $url = Router::url(array('controller' => 'users', 'action' => 'change_password'), true) . '/' . $key;
        $success_url = Router::url(array('controller' => 'users', 'action' => 'login'), true);

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('users/change_password');
        $this->set('language_based_content', $language_based_content);

        if (!empty($this->request->data)) {
            $validation_message = '';
            $password = $this->request->data['password'];
            $conf_password = $this->request->data['confirm_password'];

            if (empty($password)) {
                $validation_message .= '<p>' . $language_based_content['new_password_field_required_cp'] . '</p>';
            }
            if (empty($conf_password)) {
                $validation_message .= '<p>' . $language_based_content['confirm_password_field_required_cp'] . '</p>';
            }
            if (empty($password) || empty($conf_password)) {
                $this->Session->setFlash($validation_message, 'default', array('class' => 'message error'));
                $this->redirect($url);
            }
            $validation_message = '';

            if (strlen($password) < 6) {
                $this->Session->setFlash($language_based_content['password_limit_cp'], 'default', array('class' => 'message error'));
                $this->redirect($url);
            }
            if (!empty($password) && !empty($conf_password) && $password == $conf_password) {
                //add change password logic
                $user = $this->User->find('first', array('conditions' => array('reset_password_token' => $key)));
                if ($user && count($user) > 0) {
                    $data = array(
                        'password' => '"' . AuthComponent::password($password) . '"',
                        'reset_password_token' => NULL,
                    );
                    $this->User->updateAll($data, array('id' => $user['User']['id']));
                    $this->Session->setFlash($language_based_content['password_success_cp'], 'default', array('class' => 'message success'));
                    $this->redirect($success_url);
                } else {
                    $this->Session->setFlash($language_based_content['password_req_expired_cp'], 'default', array('class' => 'message error'));
                    $this->redirect($url);
                }
            } else {
                $this->Session->setFlash($language_based_content['password_dont_match_cp'], 'default', array('class' => 'message error'));
                $this->redirect($url);
            }
        } else {
            $this->set('key', $key);
            $this->render('change_password');
        }
    }

    function change_password_accounts()
    {

        $this->set('title_for_layout', "Change Password");
        //$this->Auth->logout();
        //$url = Router::url(array('controller' => 'users', 'action' => 'change_password'), true) . '/' . $key;
        //$success_url = Router::url(array('controller' => 'users', 'action' => 'login'), true);

        if (!empty($this->request->data)) {
            $validation_message = '';
            $password = $this->request->data['password'];
            $conf_password = $this->request->data['confirm_password'];
            $user_id = $this->request->data['user_id'];

            if (empty($password)) {
                //$validation_message .='<p>New Password field required</p>';
                echo $this->language_based_messages['New_Password_field_required'];
                exit();
            } else if (strlen($password) < 6) {
                echo $this->language_based_messages['password-must-contain-a-minimum-of-6-characters'];
                exit();
            } else if (empty($conf_password)) {
                //$validation_message.='<p>Confirm Password field required.</p>';
                echo $this->language_based_messages['confirm-password-field-required'];
                exit();
            } else if ($password != $conf_password) {
                echo $this->language_based_messages['passwords-do-not-match'];
                exit();
            }

            //$validation_message = '';


            if (!empty($password) && !empty($conf_password) && $password == $conf_password) {
                //add change password logic
                $user = $this->User->find('first', array('conditions' => array('id' => $user_id)));

                if ($user && count($user) > 0) {
                    $data = array(
                        'password' => '"' . AuthComponent::password($password) . '"',
                        'reset_password_token' => NULL,
                    );
                    $this->User->updateAll($data, array('id' => $user['User']['id']));
                    echo "success";
                    exit();
                    //                    $this->Session->setFlash('Password has been successfully changed.', 'default', array('class' => 'message success'));
                    //                    $this->redirect('users/administrators_groups');
                }
            }
        }
    }

    function permission_request()
    {
        if ($this->request->is('post')) {
            $user = $this->Session->read('user_current_account');
            $account_id = $user['accounts']['account_id'];
            $result = $this->UserAccount->get_account_owner($account_id);
            $data['owner_email'] = $result['users']['email'];
            $data['owner_name'] = $result['users']['first_name'] . ' ' . $result['users']['last_name'];
            if (!empty($this->data)) {
                if (empty($this->data['message'])) {
                    $this->Session->setFlash($this->language_based_messages['please_write_the_message_for_AO_msg'], 'default', array('class' => 'message success'));
                } else {
                    $email = $data['owner_email'];
                    //$user = $this->User->find('first', array('conditions' => array('email' => $email)));

                    if ($user) {

                        if ($user['User']['is_active']) {

                            $html = '<p><h5>Hi ' . $data['owner_name'] . '</h5></p>';
                            //$html .='<p><h5>Can`t remember your password? Don`t worry about it - it happens.</h5></p>';
                            $html .= '<p><h5>My Username is: <a href="mailto:' . $user['User']['email'] . '">' . $user['User']['username'] . '</a></p>';

                            $html .= '<p>' . $this->data['message'] . '</p>';

                            $this->Email->delivery = 'smtp';

                            $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
                            $this->Email->to = $email;
                            $this->Email->subject = 'Permission Request';
                            $this->Email->template = 'default';
                            $this->Email->sendAs = 'html';
                            // die($this->Email->to);
                            //$result = $this->Email->send($html);
                            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);

                            $this->Session->setFlash($this->language_based_messages['your_message_has_been_sent_msg'], 'default', array('class' => 'message success'));
                            $this->redirect('/Huddles');
                        } else {

                            $this->Session->setFlash($this->language_based_messages['we_cant_sent_your_message_at_the_moment_msg'], 'default', array('class' => 'message'));
                        }
                    }
                }
            }
        }
    }

    function reset($key = '')
    {
        if ($key) {
            $user = $this->User->find('first', array('conditions' => array('reset_password_token' => $key)));

            if ($user && count($user) > 0) {
                $password = $this->randPass(20, 20);
                $data = array(
                    'password' => '"' . AuthComponent::password($password) . '"',
                    'reset_password_token' => NULL,
                );
                $this->User->updateAll($data, array('id' => $user['User']['id']));

                $html = '<strong>Here is your Account Password</strong><br/><br/>';
                $html .= '<strong>Password :</strong>' . $password . "<br/>";
                $html .= '<strong>Still need help?</strong> <br/>';
                $html .= Router::url('mailto:' . $this->custom->get_site_settings('static_emails')['support'], true);

                $this->Email->delivery = 'smtp';
                $this->Email->template = 'default';
                $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
                $this->Email->to = $user['User']['email'];

                $this->Email->subject = 'Reset Your sibme.com Password';
                $this->Email->sendAs = 'html';
                $result = $this->Email->send($html);
                $this->Session->setFlash($this->language_based_messages['password_has_been_sent_on_your_email_msg'], 'default', array('class' => 'message success'));
                $this->redirect('/users/forgot_password');
            } else {
                $this->Session->setFlash($this->language_based_messages['your_password_request_has_been_expired_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/users/forgot_password');
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['please_provide_correct_token_for_password_recovery_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/users/forgot_password');
        }
    }

    public function update_welcome_screen()
    {


        $user_current_account = $this->Session->read('user_current_account');
        $user_current_account['User']['hide_welcome'] = $this->data['hide_welcome'];
        $this->Session->write('user_current_account', $user_current_account);

        $data = array(
            'hide_welcome' => $this->data['hide_welcome']
        );
        $this->User->updateAll($data, array('id' => $this->data['user_id']));

        $this->layout = null;
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function send_welcome_sibme_emails()
    {

        $result = $this->User->find('all', array(
            'conditions' => array('User.is_active' => '1', 'User.welcome_email_sent' => 0)
        ));

        if ($result) {

            for ($i = 0; $i < count($result); $i++) {

                $user = $result[$i];
                $rand_password = $this->generateRandomString(8);
                $data = array(
                    'password' => '"' . AuthComponent::password($rand_password) . '"'
                );

                $this->User->updateAll($data, array('id' => $user['User']['id']), $validation = TRUE);
                $this->send_sibme_welcome_email($user, $rand_password);

                $data = array(
                    'welcome_email_sent' => '1'
                );

                $this->User->updateAll($data, array('id' => $user['User']['id']), $validation = TRUE);
                die;
            }
        }

        $this->layout = null;
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function generateRandomString($length = 16)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&~';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public function register_changed()
    {

        $this->layout = NULL;
        $result = array(
            'status' => FALSE,
            'message' => '',
        );
        if ($this->request->is('post')) {
            $this->set_validation();
            $role_id = '100';

            $this->Account->create();
            $data = array(
                'company_name' => $this->request->data['company_name'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'is_active' => '1',
                'in_trial' => '1',
                'has_credit_card' => '0',
                'is_suspended' => '0'
            );
            $this->Account->save($data);
            $account_id = $this->Account->id;

            $this->User->create();
            $full_name = $this->request->data('fullname');
            $full_name = explode(' ', $full_name);
            $first_name = isset($full_name[0]) ? $full_name[0] : '';
            $last_name = isset($full_name[1]) ? $full_name[1] : '';
            $data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $this->request->data['email'],
                'username' => $this->request->data['email'],
                'created_date' => date('Y-m-d H:i:s'),
                'password' => $this->request->data['password'],
                'type' => 'Active',
                'is_active' => TRUE
            );

            $this->User->save($data);

            $user_id = $this->User->id;

            $user_data = $data;

            $this->User->updateAll(array(
                'authentication_token' => "'" . md5($user_id) . "'"
            ), array('id' => $user_id));

            $this->UserAccount->create();
            //Checking default account is exist or not
            $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                'user_id' => $user_id
            )));
            $data = array(
                'user_id' => $user_id,
                'account_id' => $account_id,
                'role_id' => $role_id,
                'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                'permission_maintain_folders' => true,
                'permission_access_video_library' => true,
                'permission_video_library_upload' => true,
                'permission_administrator_user_new_role' => true,
                'parmission_access_my_workspace' => true,
                'folders_check' => true,
                'manage_collab_huddles' => true,
                'manage_coach_huddles' => true,
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $user_id,
                'last_edit_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $user_id
            );

            $this->UserAccount->save($data);
            $user_data['id'] = $user_id;
            $user_data['account_id'] = $account_id;

            $this->send_welcome_email($user_data, $account_id);
            if ($user_id) {
                $user = $this->User->findById($user_id);
                $user = $user['User'];
                if ($this->Auth->login($user)) {
                    $this->Cookie->delete('remember_me_cookie');
                    $this->setupPostLogin(true);
                }
                $result['status'] = TRUE;
                $result['message'] = 'You have successfully Signup';
                echo json_encode($result);
                exit;
            } else {
                $result['message'] = 'Your signup process failed.';
                echo json_encode($result);
                exit;
            }
        }
    }

    function set_validation()
    {
        $full_name = $this->request->data('fullname');
        $full_name = explode(' ', $full_name);
        $first_name = isset($full_name[0]) ? $full_name[0] : '';
        $last_name = isset($full_name[1]) ? $full_name[1] : '';
        $validation_errors['status'] = TRUE;
        if ($this->User->isUserEmailExists($this->request->data['email']) > 0) {
            $validation_errors['email'] = 'Email address already exists, please try again.';
            $validation_errors['status'] = FALSE;
        }
        if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $this->request->data['email'])) {
            $validation_errors['email'] = "Invalid email";
            $validation_errors['status'] = FALSE;
        }
        if ($first_name == '') {
            $validation_errors['first_name'] = 'First Name Required';
            $validation_errors['status'] = FALSE;
        }
        if ($last_name == '') {
            $validation_errors['last_name'] = 'Last Name Required';
            $validation_errors['status'] = FALSE;
        }
        if ($this->request->data['company_name'] == '') {
            $validation_errors['company_name'] = 'Company Name Required';
            $validation_errors['status'] = FALSE;
        }
        if ($this->request->data['email'] == '') {
            $validation_errors['email'] = 'Email Required';
            $validation_errors['status'] = FALSE;
        }
        if ($this->request->data['password'] == '') {
            $validation_errors['password'] = 'Password Required';
            $validation_errors['status'] = FALSE;
        }
        if ($this->request->data['confirm'] == '') {
            $validation_errors['forgot_password'] = 'Confirm Password Required';
            $validation_errors['status'] = FALSE;
        }
        if ($this->request->data['password'] != $this->request->data['confirm']) {
            $validation_errors['password_match'] = 'Passwords do not match please enter correct passwords.';
            $validation_errors['status'] = FALSE;
        }

        if ($validation_errors['status'] == TRUE) {
            return TRUE;
        } else {
            echo json_encode($validation_errors);
            exit;
        }
    }

    function subscribe()
    {
        $user_current_account = $this->Session->read('user_current_account');
        $result = array(
            'msg' => '',
            'status' => false
        );

        if ($_POST['type'] == 'all') {
            $arr = $_POST['list_accounts'];
            $arr = json_decode($arr);

            foreach ($arr as $account_id) {

                $data = array(
                    'email_format_id' => $_POST['template_id'],
                    'user_id' => $user_current_account['User']['id'],
                    'account_id' => $account_id,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'created_by' => $user_current_account['User']['id']
                );

                $this->EmailUnsubscribers->deleteAll(array('user_id' => $user_current_account['User']['id'], 'email_format_id' => $_POST['template_id'], 'account_id' => ''));

                if ($this->EmailUnsubscribers->saveAll($data)) {
                    $result['msg'] = 'You have successfully opt out.';
                    $result['status'] = TRUE;
                }
                //            if (!$this->EmailUnsubscribers->find('count', array('conditions' => array('email_format_id' => $_POST['template_id'], 'user_id' => $user_current_account['User']['id'])))) {
                //                if ($this->EmailUnsubscribers->save($data)) {
                //                    $result['msg'] = 'You have successfully opt out.';
                //                    $result['status'] = TRUE;
                //                } else {
                //                    $result['msg'] = 'You are not successfully opt out. please try again.';
                //                    $result['status'] = FALSE;
                //                }
                //            } else {
                //                $result['msg'] = 'You already opt out .';
                //                $result['status'] = FALSE;
                //            }
            }
        } else {
            $data = array(
                'email_format_id' => $_POST['template_id'],
                'user_id' => $user_current_account['User']['id'],
                'account_id' => $_POST['list_accounts'],
                'created_at' => date('Y-m-d H:i:s', time()),
                'created_by' => $user_current_account['User']['id']
            );

            if ($this->EmailUnsubscribers->save($data)) {
                $result['msg'] = 'You have successfully opt out.';
                $result['status'] = TRUE;
            } else {
                $result['msg'] = 'You are not successfully opt out. please try again.';
                $result['status'] = FALSE;
            }
        }

        echo json_encode($result);
        exit;
    }

    function unsubscribe()
    {
        $unsubscription_id = $_POST['template_id'];
        $account_id = $_POST['list_accounts'];
        $user_current_account = $this->Session->read('user_current_account');
        $result = array(
            'msg' => '',
            'status' => false
        );

        if ($_POST['type'] == 'all') {
            $arr = $_POST['list_accounts'];
            $arr = json_decode($arr);

            foreach ($arr as $account_id) {
                if ($this->EmailUnsubscribers->find('count', array('conditions' => array('email_format_id' => $unsubscription_id, 'user_id' => $user_current_account['User']['id'])))) {
                    $this->EmailUnsubscribers->deleteAll(array('email_format_id' => $unsubscription_id, 'user_id' => $user_current_account['User']['id'], 'account_id' => $account_id));
                    if ($this->EmailUnsubscribers->getAffectedRows() > 0) {
                        $result['msg'] = 'You have successfully Unsubscribed';
                        $result['status'] = TRUE;
                    } else {
                        $result['msg'] = 'You have not successfully Unsubscribed please try agian.';
                        $result['status'] = FALSE;
                    }
                } else {
                    $result['msg'] = 'You already unsubscribed.';
                    $result['status'] = FALSE;
                }
            }
        } else {

            $this->EmailUnsubscribers->deleteAll(array('email_format_id' => $unsubscription_id, 'user_id' => $user_current_account['User']['id'], 'account_id' => $account_id));
            $result['msg'] = 'You have successfully Unsubscribed';
            $result['status'] = TRUE;
        }


        echo json_encode($result);
        exit;
    }

    function unsubscribeWeeklyMonthly($account_id, $user_current_account, $unsubscription_id)
    {
        $data = array(
            'email_format_id' => $unsubscription_id,
            'user_id' => $user_current_account,
            'account_id' => $account_id,
            'created_at' => date('Y-m-d H:i:s', time()),
            'created_by' => $user_current_account
        );

        $this->EmailUnsubscribers->saveAll($data);
        $this->redirect($this->base . '/Users/successUnsubscribe/' . $unsubscription_id);
    }

    function successUnsubscribe($template_id)
    {
        $view = new View($this, false);
        $site_title = $view->Custom->get_site_settings('site_title');
        if (empty($site_title)) {
            $site_title = 'Sibme';
        }
        $this->layout = 'minimal';
        $this->set('template_id', $template_id);
        $this->set('title_for_layout', "Unsubscribed from " . $site_title . ".");
    }

    //    function unsubscribe() {
    //        $unsubscription_id = $_POST['template_id'];
    //        $user_current_account = $this->Session->read('user_current_account');
    //        $result = array(
    //            'msg' => '',
    //            'status' => false
    //        );
    //        if ($this->EmailUnsubscribers->find('count', array('conditions' => array('email_format_id' => $unsubscription_id, 'user_id' => $user_current_account['User']['id'])))) {
    //            $this->EmailUnsubscribers->deleteAll(array('email_format_id' => $unsubscription_id, 'user_id' => $user_current_account['User']['id']));
    //            if ($this->EmailUnsubscribers->getAffectedRows() > 0) {
    //                $result['msg'] = 'You have successfully Unsubscribed';
    //                $result['status'] = TRUE;
    //            } else {
    //                $result['msg'] = 'You have not successfully Unsubscribed please try agian.';
    //                $result['status'] = FALSE;
    //            }
    //        } else {
    //            $result['msg'] = 'You already unsubscribed.';
    //            $result['status'] = FALSE;
    //        }
    //        echo json_encode($result);
    //        exit;
    //    }

    function getAjaxUsers()
    {
        $user_current_account = $this->Session->read('user_current_account');
        $account_id = $user_current_account['accounts']['account_id'];
        $user_id = $user_current_account['User']['id'];
        $total_people_count = 1;
        $view = new View($this, true);
        $language_based_content = $view->Custom->get_page_lang_based_content('users/administrators_groups');

        if ($this->request->data['keywords'] == 'Search Super Users...')
            $this->request->data['keywords'] = '';
        if ($this->request->data['keywords'] == 'Search Users...')
            $this->request->data['keywords'] = '';
        if ($this->request->data['keywords'] == 'Search Admins...')
            $this->request->data['keywords'] = '';
        if ($this->request->data['keywords'] == 'Search Viewers...')
            $this->request->data['keywords'] = '';
        $page = isset($this->request->data['page']) ? $this->request->data['page'] : '1';
        $role = isset($this->request->data['role']) ? $this->request->data['role'] : '';
        $keywords = isset($this->request->data['keywords']) ? $this->request->data['keywords'] : '';
        $request_type = isset($this->request->data['request_type']) ? $this->request->data['request_type'] : '';
        $cur_page = $page;
        $page -= 1;
        $per_page = 24;
        $previous_btn = true;
        $next_btn = true;
        $first_btn = true;
        $last_btn = true;
        $start = $page * $per_page;
        $msg = '';

        $all_roles = array(110, 120, 115, 125);

        $count = $this->User->countUsersByAccount($account_id, $role, $keywords);
        $total_people_count = $this->User->countUsersByAccount($account_id, $all_roles, $keywords) + 1;

        $manage_users_permission_arr = $this->UserAccount->find('first', array(
            'conditions' => array(
                'user_id' => $user_id,
                'account_id' => $account_id,
            ),
            'fields' => array('permission_administrator_user_new_role')
        ));

        $manage_users_permission = $manage_users_permission_arr['UserAccount']['permission_administrator_user_new_role'];

        if ($role == 110) {
            $admins = array(
                'user_id' => $user_id,
                'count' => $count,
                'supperAdmins' => $this->User->getUsersByAccount($account_id, $role, $per_page, $start, $keywords),
                'manage_users_permission' => $manage_users_permission,
                'request_type' => $request_type,
                'language_based_content' => $language_based_content
            );
            $msg = $view->element('users/ajax_super_users', $admins);
        } elseif ($role == 120) {
            $supperAdmins = array(
                'user_id' => $user_id,
                'count' => $count,
                'admins' => $this->User->getUsersByAccount($account_id, $role, $per_page, $start, $keywords),
                'manage_users_permission' => $manage_users_permission,
                'request_type' => $request_type,
                'language_based_content' => $language_based_content
            );
            $msg = $view->element('users/adminsAjax', $supperAdmins);
        }

        if ($role == 115) {
            $admins = array(
                'user_id' => $user_id,
                'count' => $count,
                'supperAdmins' => $this->User->getUsersByAccount($account_id, $role, $per_page, $start, $keywords),
                'manage_users_permission' => $manage_users_permission,
                'request_type' => $request_type,
                'language_based_content' => $language_based_content
            );
            $msg = $view->element('users/ajax_new_admins', $admins);
        }

        if ($role == 125) {
            $admins = array(
                'user_id' => $user_id,
                'count' => $count,
                'supperAdmins' => $this->User->getUsersByAccount($account_id, $role, $per_page, $start, $keywords),
                'manage_users_permission' => $manage_users_permission,
                'request_type' => $request_type,
                'language_based_content' => $language_based_content
            );
            $msg = $view->element('users/ajax_viewers', $admins);
        }

        /* --------------------------------------------- */

        if ($count > $per_page) {
            $no_of_paginations = ceil($count / $per_page);

            /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
            if ($cur_page >= 7) {
                $start_loop = $cur_page - 3;
                if ($no_of_paginations > $cur_page + 3)
                    $end_loop = $cur_page + 3;
                else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                    $start_loop = $no_of_paginations - 6;
                    $end_loop = $no_of_paginations;
                } else {
                    $end_loop = $no_of_paginations;
                }
            } else {
                $start_loop = 1;
                if ($no_of_paginations > 7)
                    $end_loop = 7;
                else
                    $end_loop = $no_of_paginations;
            }
            /* ----------------------------------------------------------------------------------------------------------- */
            $msg .= "<div class='pagination'><ul>";

            // FOR ENABLING THE FIRST BUTTON
            if ($first_btn && $cur_page > 1) {
                $msg .= "<li p='1' class='active'>" . $language_based_content['first_pg'] . "</li>";
            } else if ($first_btn) {
                $msg .= "<li p='1' class='inactive'>" . $language_based_content['first_pg'] . "</li>";
            }

            // FOR ENABLING THE PREVIOUS BUTTON
            if ($previous_btn && $cur_page > 1) {
                $pre = $cur_page - 1;
                $msg .= "<li p='$pre' class='active'>" . $language_based_content['previous_pg'] . "</li>";
            } else if ($previous_btn) {
                $msg .= "<li class='inactive'>" . $language_based_content['previous_pg'] . "</li>";
            }

            for ($i = $start_loop; $i <= $end_loop; $i++) {
                if ($cur_page == $i)
                    $msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active'>{$i}</li>";
                else
                    $msg .= "<li p='$i' class='active'>{$i}</li>";
            }

            // TO ENABLE THE NEXT BUTTON
            if ($next_btn && $cur_page < $no_of_paginations) {
                $nex = $cur_page + 1;
                $msg .= "<li p='$nex' class='active'>" . $language_based_content['next_pg'] . "</li>";
            } else if ($next_btn) {
                $msg .= "<li class='inactive'>" . $language_based_content['next_pg'] . "</li>";
            }

            // TO ENABLE THE END BUTTON
            if ($last_btn && $cur_page < $no_of_paginations) {
                $msg .= "<li p='$no_of_paginations' class='active'>" . $language_based_content['last_pg'] . "</li>";
            } else if ($last_btn) {
                $msg .= "<li p='$no_of_paginations' class='inactive'>" . $language_based_content['last_pg'] . "</li>";
            }

            $total_string = "<span class='total' a='$no_of_paginations'>" . $language_based_content['page_pg'] . " <b>" . $cur_page . "</b> " . $language_based_content['of_pg'] . " <b>$no_of_paginations</b></span>";
            $msg = $msg . "</ul>" . $total_string . "</div>";  // Content for pagination
            $msg = $msg . "<hr class='full--slim dashed'>";  // Content for pagination
        }
        $result = array(
            'msg' => $msg,
            'total_people_count' => $total_people_count
        );
        echo json_encode($result);
        exit;
    }

    function student_payment($account_id = '', $amount = false)
    {
        // $this->layout = 'external';
        $this->layout = "payment";
        $account_id = $this->decode_account_id($account_id);
        $allowed_account_id = Configure::read('allowed_account_id');

        $account_ids = explode(',', $allowed_account_id);

        if (!in_array($account_id, $account_ids)) {
            $this->redirect('/');
            exit;
        }
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('student_payment');
        $this->set('language_based_content', $language_based_content);
        if ($amount) {
            $this->set('amount', $amount);
        } else {
            $this->set('amount', '');
        }

        $this->set('account_id', $this->encode_account_id($account_id));
        $huddle_info = $this->Account->find('first', array('conditions' => array('id' => (int) $account_id)));
        $account_owner = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
        $account_owner_details = $this->User->find('first', array('conditions' => array('id' => $account_owner['UserAccount']['user_id'])));
        if ($this->request->data['huddle_id'] != '') {
            $huddle_id = $this->request->data['huddle_id'];
            $huddle_exist = $this->AccountFolder->find("count", array("conditions" => array(
                'account_folder_id' => $huddle_id, 'account_id' => $account_id
            )));
            if ($huddle_exist  == 0) {
                $this->Session->setFlash($this->language_based_messages['incorrect_huddle_info']);
                $this->redirect('student_payment/' . $this->encode_account_id($account_id));
            }
        }

        $company_name = $huddle_info['Account']['company_name'];
        $this->set('company_name', $company_name);
        $student_payment_id = '';
        if ($amount) {
            $account_fees = $this->AccountStudentPayments->find('first', array(
                'conditions' => array(
                    'account_id' => $account_id,
                    'amount' => base64_decode($amount)
                )
            ));
            $student_payment_id = $account_fees['AccountStudentPayments']['id'];
            $this->set('student_fees', $account_fees['AccountStudentPayments']['amount']);
        } else {

            $account_fees = $this->AccountStudentPayments->find('first', array(
                'conditions' => array(
                    'account_id' => $account_id
                ),
                'order' => 'id ASC'
            ));
            $student_payment_id = $account_fees['AccountStudentPayments']['id'];
            $this->set('student_fees', $account_fees['AccountStudentPayments']['amount']);
        }

        if ($huddle_info) {

            $this->set('user_current_account', $huddle_info);
            if ($this->request->is('post')) {

                $posted_amount = $this->request->data['amount'];
                if ($posted_amount == '') {
                    $this->Session->setFlash($this->language_based_messages['there_are_no_paymennts_defined_for_your_account_msg']);
                    //                    $this->Session->setFlash("Please enter a value greater than or equal to $defualt_amount.");
                    $this->redirect('student_payment/' . $this->encode_account_id($account_id));
                }

                if ($this->request->data['password'] != $this->request->data['password_confirmation']) {
                    $this->Session->setFlash($this->language_based_messages['password_do_not_match_please_enter_correct_passwords_msg']);
                    $this->redirect('student_payment/' . $this->encode_account_id($account_id));
                }

                $customer = array(
                    'firstName' => $this->request->data['first_name'],
                    'lastName' => $this->request->data['last_name'],
                    'email' => $this->request->data['email'],
                    'creditCard' => array(
                        'number' => $this->data['card_number'],
                        'expirationDate' => $this->data['expiry_month'] . "/" . $this->data['expiry_year'],
                        'cvv' => $this->data['card_cvv'],
                        'cardholderName' => $this->data['card_holder_name']
                    )
                );

                $expires = DateTime::createFromFormat('mY', $this->data['expiry_month'] . '20' . $this->data['expiry_year']);
                $now     = new DateTime();
                if ($expires < $now) {
                    $this->Session->setFlash(__($this->language_based_messages['credit_card_expired']), 'default', array('class' => 'message error'));
                    $this->redirect('student_payment/' . $this->encode_account_id($account_id) . '/' . $amount);
                }

                $is_allow = false;
                if (!isset($this->request->data['free_payment'])) {
                    $subscription = new Subscription();
                    $result = $subscription->create_transaction($this->request->data['amount'], $customer, $this->request->data['city'], $this->request->data['state'], $this->request->data['zip_code'], $this->request->data['address']);
                    if (isset($result->success) && $result->success == 1) {
                        $is_allow = true;
                        $this->StudentPayment->create();
                        $data = array(
                            'name' => $this->request->data['first_name'],
                            'last_name' => $this->request->data['last_name'],
                            'address' => $this->request->data['address'],
                            'zip_code' => $this->request->data['zip_code'],
                            'state' => $this->request->data['state'],
                            'city' => $this->request->data['city'],
                            'email' => $this->request->data['email'],
                            'amount' => $this->request->data['amount']
                        );

                        $this->StudentPayment->save($data);

                        $last_id = $this->StudentPayment->id;

                        $this->Transaction->create();
                        $transaction = array(
                            'transaction_id' => $result->transaction->id,
                            'transaction_type' => $result->transaction->type,
                            'status' => $result->transaction->status,
                            'student_payment_id' => $last_id,
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $this->Transaction->save($transaction);
                    } else {
                        $is_allow = false;
                    }
                } else {
                    $is_allow = true;
                }

                if ($is_allow) {
                    //$array = $result->transaction->_attributes;
                    if (!$this->User->isUserEmailExists($this->request->data['email']) > 0) {
                        $this->User->create();
                        $role_id = 120;
                        $verification_code = uniqid();
                        $data = array(
                            'first_name' => $this->request->data['first_name'],
                            'last_name' => $this->request->data['last_name'],
                            'email' => $this->request->data['email'],
                            'created_date' => date('Y-m-d H:i:s'),
                            'username' => $this->request->data['email'],
                            'password' => $this->request->data['password'],
                            'institution_id' => $this->request->data['institution_id'],
                            'type' => 'Pending_Activation',
                            'is_active' => TRUE,
                            'verification_code' => $verification_code
                        );

                        $this->User->save($data);
                        $user_id = $this->User->id;
                        $paymentLog = array(
                            'account_id' => $account_id,
                            'user_id' => $user_id,
                            'payment_id' => $student_payment_id,
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $this->AccountStudentPaymentsLog->save($paymentLog);
                        $this->User->updateAll(array(
                            'authentication_token' => "'" . md5($user_id) . "'"
                        ), array('User' . "." . 'id' => $user_id));

                        $this->UserAccount->create();
                        //Checking default account is exist or not
                        $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id
                        )));
                        $data = array(
                            'user_id' => $user_id,
                            'account_id' => $account_id,
                            'role_id' => $role_id,
                            'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                            'permission_maintain_folders' => '1',
                            'permission_access_video_library' => '1',
                            'permission_video_library_upload' => '1',
                            'permission_administrator_user_new_role' => '1',
                            'parmission_access_my_workspace' => '1',
                            'created_date' => date('Y-m-d H:i:s'),
                            'created_by' => $user_id,
                            'last_edit_date' => date('Y-m-d H:i:s'),
                            'last_edit_by' => $user_id,
                            'folders_check' => '1',
                            'manage_coach_huddles' => '1',
                            'manage_collab_huddles' => '1'
                        );

                        if ($role_id == '120') {
                            $data['permission_maintain_folders'] = '0';
                            $data['permission_access_video_library'] = '1';
                            $data['permission_video_library_upload'] = '0';
                            $data['permission_administrator_user_new_role'] = '0';
                            $data['parmission_access_my_workspace'] = '1';
                            $data['folders_check'] = '0';
                            $data['manage_coach_huddles'] = '0';
                            $data['manage_collab_huddles'] = '0';
                        }
                        $this->UserAccount->save($data);
                        $email_data = array(
                            'email' => $this->request->data['email'],
                            'account_id' => $account_id,
                            'company_name' => $company_name,
                            'authentication_token' => md5($user_id),
                            'user_id' => $user_id,
                            'first_name' => $account_owner_details['User']['first_name'],
                            'last_name' => $account_owner_details['User']['last_name'],
                            'account_owner_email' => $account_owner_details['User']['email']
                        );

                        // $this->sendInvitation_student_payment($email_data);
                        //$this->account_invitation_student_payment($email_data);

                        $this->request->data['id'] = $user_id;
                        $this->request->data['account_id'] = $account_id;
                        // $this->send_welcome_email($this->request->data, $account_id);
                        // $this->send_welcome_email_template($this->request->data, $account_id);
                        if ($this->request->data['huddle_id'] != '') {
                            $account_folder_id = $this->request->data['huddle_id'];
                            $assigned_user_id = $user_id;
                            $this->AccountFolderUser->create();
                            $data = array(
                                'account_folder_id' => $account_folder_id,
                                'user_id' => $assigned_user_id,
                                'role_id' => 210,
                                'created_by' => $assigned_user_id,
                                'created_date' => date("Y-m-d H:i:s"),
                                'last_edit_by' => $assigned_user_id,
                                'last_edit_date' => date("Y-m-d H:i:s"),
                                'is_coach' => 0,
                                'is_mentee' => 1
                            );
                            $this->AccountFolderUser->save($data, $validation = TRUE);
                        }

                        $data = array(
                            'to_email' => $this->request->data['email'],
                            'name' => $this->request->data['first_name'] . ' ' . $this->request->data['last_name'],
                            'account_name' => $company_name,
                            'user_id' => $user_id,
                            'verification_code' => $verification_code,
                            'account_id' => $account_id
                        );


                        $this->activation_email_student_payment($data, $account_id);
                    } else {
                        $role_id = 120;
                        $user_details = $this->User->find('first', array('conditions' => array('email' => $this->request->data['email'])));

                        if (!empty($user_details)) {
                            $user_id = $user_details['User']['id'];
                            if ($this->request->data['huddle_id'] != '') {
                                $account_folder_id = $this->request->data['huddle_id'];
                                $assigned_user_id = $user_id;
                                $this->AccountFolderUser->create();
                                $data = array(
                                    'account_folder_id' => $account_folder_id,
                                    'user_id' => $assigned_user_id,
                                    'role_id' => 100,
                                    'created_by' => $assigned_user_id,
                                    'created_date' => date("Y-m-d H:i:s"),
                                    'last_edit_by' => $assigned_user_id,
                                    'last_edit_date' => date("Y-m-d H:i:s"),
                                    'is_coach' => 0,
                                    'is_mentee' => 1
                                );
                                $this->AccountFolderUser->save($data, $validation = TRUE);
                            }

                            $user_account_details = $this->UserAccount->find('first', array('conditions' => array('user_id' => $user_id, 'account_id' => $account_id)));
                            if (empty($user_account_details)) {
                                $this->UserAccount->create();
                                //Checking default account is exist or not
                                $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                                    'user_id' => $user_id
                                )));
                                $data = array(
                                    'user_id' => $user_id,
                                    'account_id' => $account_id,
                                    'role_id' => $role_id,
                                    'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                                    'permission_maintain_folders' => '0',
                                    'permission_access_video_library' => '1',
                                    'permission_video_library_upload' => '0',
                                    'permission_administrator_user_new_role' => '0',
                                    'parmission_access_my_workspace' => '1',
                                    'created_date' => date('Y-m-d H:i:s'),
                                    'created_by' => $user_id,
                                    'last_edit_date' => date('Y-m-d H:i:s'),
                                    'last_edit_by' => $user_id,
                                    'folders_check' => '0',
                                    'manage_coach_huddles' => '0',
                                    'manage_collab_huddles' => '0'
                                );



                                if ($this->UserAccount->save($data)) {

                                    $account_details = $this->Account->find('first', array('conditions' => array('id' => $account_id)));

                                    $account_owner_details = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
                                    $current_user_details = $this->User->find('first', array('conditions' => array('id' => $account_owner_details['UserAccount']['user_id'])));
                                    $data['email'] = $this->request->data['email'];
                                    $data['company_name'] = $account_details['Account']['company_name'];
                                    $data['creator_first_name'] = $current_user_details['User']['first_name'];
                                    $data['creator_last_name'] = $current_user_details['User']['last_name'];
                                    $data['creator_email'] = $current_user_details['User']['email'];

                                    $email_data = array(
                                        'email' => $this->request->data['email'],
                                        'account_id' => $account_id,
                                        'company_name' => $account_details['Account']['company_name'],
                                        'authentication_token' => md5($user_id),
                                        'user_id' => $user_id,
                                        'first_name' => $current_user_details['User']['first_name'],
                                        'last_name' => $current_user_details['User']['last_name'],
                                        'account_owner_email' => $current_user_details['User']['email']
                                    );

                                    $paymentLog = array(
                                        'account_id' => $account_id,
                                        'user_id' => $user_id,
                                        'payment_id' => $student_payment_id,
                                        'created_date' => date('Y-m-d H:i:s')
                                    );
                                    $this->AccountStudentPaymentsLog->save($paymentLog);


                                    $this->account_invitation_student_payment($email_data);


                                    //  $this->send_welcome_to_acc_email($data, $account_id);
                                }
                            } else {
                                $this->Session->setFlash(__($this->language_based_messages['user_is_already_registered']), 'default', array('class' => 'message error'));
                                $this->redirect('student_payment/' . $this->encode_account_id($account_id) . '/' . $amount);
                            }
                        }
                    }

                    $this->AccountFolder->updateAll(array('active' => 0), array('account_id' => $account_id, 'is_sample' => '1'));
                    $this->UserAccount->deleteAll(array('account_id' => $account_id, 'user_id' => array(2423, 242, 242)));
                    $this->Session->setFlash($this->language_based_messages['your_transaction_has_been_completed_successfully_msg'], 'default', array('class' => 'message success'));
                } else {
                    if (is_array($result)) {
                        $this->Session->setFlash(__($result['message']), 'default', array('class' => 'message error'));
                    } else {
                        $this->Session->setFlash(__($result->message), 'default', array('class' => 'message error'));
                    }
                }
            }
        } else {
            echo 'Not Valid';
            exit;
        }
    }

    function free_sibme_registration($account_id = '')
    {

        // $this->layout = 'external';
        if (isset($this->request->query['huddle_id']) && $this->request->query['huddle_id'] != '') {
            $huddle_id = $this->request->query['huddle_id'];
        } else {
            $huddle_id = '';
        }

        if (isset($this->request->query['first_name']) && $this->request->query['first_name'] != '') {
            $first_name = $this->request->query['first_name'];
        } else {
            $first_name = '';
        }

        if (isset($this->request->query['last_name']) && $this->request->query['last_name'] != '') {
            $last_name = $this->request->query['last_name'];
        } else {
            $last_name = '';
        }

        if (isset($this->request->query['email']) && $this->request->query['email'] != '') {
            $email = $this->request->query['email'];
            $email = str_replace(" ", "+", $email);
        } else {
            $email = '';
        }
        $this->layout = "payment_free";
        $account_id = base64_decode($account_id);
        $blocked_account_id = Configure::read('blocked_account_id');
        $account_ids = explode(',', $blocked_account_id);
        if (in_array($account_id, $account_ids)) {
            $this->redirect('/');
            exit;
        }
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('student_payment');
        $this->set('language_based_content', $language_based_content);
        $this->set('huddle_id', $huddle_id);
        $this->set('first_name', $first_name);
        $this->set('last_name', $last_name);
        $this->set('email', $email);
        $this->set('account_id', base64_encode($account_id));
        $huddle_info = $this->Account->find('first', array('conditions' => array('id' => (int) $account_id)));
        $account_owner = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
        $account_owner_details = $this->User->find('first', array('conditions' => array('id' => $account_owner['UserAccount']['user_id'])));
        $company_name = $huddle_info['Account']['company_name'];
        $this->set('company_name', $company_name);
        if ($huddle_info) {
            $this->set('user_current_account', $huddle_info);
            if ($this->request->is('post')) {
                $huddle_id = $this->request->data['huddle_id'];
                if ($huddle_id != '') {
                    $huddle_exist = $this->AccountFolder->find("count", array("conditions" => array(
                        'account_folder_id' => $huddle_id, 'account_id' => $account_id
                    )));
                    if ($huddle_exist  == 0) {
                        $this->Session->setFlash($this->language_based_messages['incorrect_huddle_info']);
                        $this->redirect('free_sibme_registration/' . base64_encode($account_id) . '?first_name=' . $first_name . '&last_name=' . $last_name . '&email=' . $email . '&huddle_id=' . $huddle_id);
                        die;
                    }
                }
                if ($this->request->data['password'] != $this->request->data['password_confirmation']) {
                    $this->Session->setFlash($this->language_based_messages['password_do_not_match_please_enter_correct_passwords_msg']);
                    $this->redirect('free_sibme_registration/' . base64_encode($account_id) . '?first_name=' . $first_name . '&last_name=' . $last_name . '&email=' . $email . '&huddle_id=' . $huddle_id);
                }
                $customer = array(
                    'firstName' => $this->request->data['first_name'],
                    'lastName' => $this->request->data['last_name'],
                    'email' => $this->request->data['email'],
                    'creditCard' => array(
                        'number' => $this->data['card_number'],
                        'expirationDate' => $this->data['expiry_month'] . "/" . $this->data['expiry_year'],
                        'cvv' => $this->data['card_cvv'],
                        'cardholderName' => $this->data['card_holder_name']
                    )
                );
                if (true) {
                    //$array = $result->transaction->_attributes;
                    if (!$this->User->isUserEmailExists($this->request->data['email']) > 0) {
                        $this->User->create();
                        $role_id = 120;
                        $verification_code = uniqid();
                        $data = array(
                            'first_name' => $this->request->data['first_name'],
                            'last_name' => $this->request->data['last_name'],
                            'email' => $this->request->data['email'],
                            'created_date' => date('Y-m-d H:i:s'),
                            'username' => $this->request->data['email'],
                            'password' => $this->request->data['password'],
                            'institution_id' => $this->request->data['institution_id'],
                            'type' => 'Pending_Activation',
                            'is_active' => TRUE,
                            'verification_code' => $verification_code
                        );

                        $this->User->save($data);
                        $user_id = $this->User->id;
                        $paymentLog = array(
                            'account_id' => $account_id,
                            'user_id' => $user_id,
                            'payment_id' => 0,
                            'created_date' => date('Y-m-d H:i:s')
                        );
                        $this->AccountStudentPaymentsLog->save($paymentLog);
                        $this->User->updateAll(array(
                            'authentication_token' => "'" . md5($user_id) . "'"
                        ), array('User' . "." . 'id' => $user_id));

                        $this->UserAccount->create();

                        //Checking default account is exist or not
                        $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id
                        )));
                        $data = array(
                            'user_id' => $user_id,
                            'account_id' => $account_id,
                            'role_id' => $role_id,
                            'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                            'permission_maintain_folders' => '1',
                            'permission_access_video_library' => '1',
                            'permission_video_library_upload' => '1',
                            'permission_administrator_user_new_role' => '1',
                            'parmission_access_my_workspace' => '1',
                            'created_date' => date('Y-m-d H:i:s'),
                            'created_by' => $user_id,
                            'last_edit_date' => date('Y-m-d H:i:s'),
                            'last_edit_by' => $user_id,
                            'folders_check' => '1',
                            'manage_coach_huddles' => '1',
                            'manage_collab_huddles' => '1'
                        );

                        if ($role_id == '120') {
                            $data['permission_maintain_folders'] = '0';
                            $data['permission_access_video_library'] = '1';
                            $data['permission_video_library_upload'] = '0';
                            $data['permission_administrator_user_new_role'] = '0';
                            $data['parmission_access_my_workspace'] = '1';
                            $data['folders_check'] = '0';
                            $data['manage_coach_huddles'] = '0';
                            $data['manage_collab_huddles'] = '0';
                        }
                        $this->UserAccount->save($data);
                        $email_data = array(
                            'email' => $this->request->data['email'],
                            'account_id' => $account_id,
                            'company_name' => $company_name,
                            'authentication_token' => md5($user_id),
                            'user_id' => $user_id,
                            'first_name' => $account_owner_details['User']['first_name'],
                            'last_name' => $account_owner_details['User']['last_name'],
                            'account_owner_email' => $account_owner_details['User']['email']
                        );

                        // $this->sendInvitation_student_payment($email_data);
                        //$this->account_invitation_student_payment($email_data);

                        $this->request->data['id'] = $user_id;
                        $this->request->data['account_id'] = $account_id;
                        // $this->send_welcome_email($this->request->data, $account_id);
                        // $this->send_welcome_email_template($this->request->data, $account_id);
                        if ($this->request->data['huddle_id'] != '') {
                            $account_folder_id = $this->request->data['huddle_id'];
                            $assigned_user_id = $user_id;
                            $this->AccountFolderUser->create();
                            $data = array(
                                'account_folder_id' => $account_folder_id,
                                'user_id' => $assigned_user_id,
                                'role_id' => 210,
                                'created_by' => $assigned_user_id,
                                'created_date' => date("Y-m-d H:i:s"),
                                'last_edit_by' => $assigned_user_id,
                                'last_edit_date' => date("Y-m-d H:i:s"),
                                'is_coach' => 0,
                                'is_mentee' => 1
                            );
                            $this->AccountFolderUser->save($data, $validation = TRUE);
                        }

                        $data = array(
                            'to_email' => $this->request->data['email'],
                            'name' => $this->request->data['first_name'] . ' ' . $this->request->data['last_name'],
                            'account_name' => $company_name,
                            'user_id' => $user_id,
                            'verification_code' => $verification_code,
                            'account_id' => $account_id
                        );


                        $this->activation_email_student_payment($data, $account_id);
                    } else {
                        $role_id = 120;
                        $user_details = $this->User->find('first', array('conditions' => array('email' => $this->request->data['email'])));

                        if (!empty($user_details)) {
                            $user_id = $user_details['User']['id'];
                            if ($this->request->data['huddle_id'] != '') {
                                $account_folder_id = $this->request->data['huddle_id'];
                                $assigned_user_id = $user_id;
                                $this->AccountFolderUser->create();
                                $data = array(
                                    'account_folder_id' => $account_folder_id,
                                    'user_id' => $assigned_user_id,
                                    'role_id' => 100,
                                    'created_by' => $assigned_user_id,
                                    'created_date' => date("Y-m-d H:i:s"),
                                    'last_edit_by' => $assigned_user_id,
                                    'last_edit_date' => date("Y-m-d H:i:s"),
                                    'is_coach' => 0,
                                    'is_mentee' => 1
                                );
                                $this->AccountFolderUser->save($data, $validation = TRUE);
                            }

                            $user_account_details = $this->UserAccount->find('first', array('conditions' => array('user_id' => $user_id, 'account_id' => $account_id)));
                            if (empty($user_account_details)) {
                                $this->UserAccount->create();
                                //Checking default account is exist or not
                                $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                                    'user_id' => $user_id
                                )));
                                $data = array(
                                    'user_id' => $user_id,
                                    'account_id' => $account_id,
                                    'role_id' => $role_id,
                                    'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                                    'permission_maintain_folders' => '0',
                                    'permission_access_video_library' => '1',
                                    'permission_video_library_upload' => '0',
                                    'permission_administrator_user_new_role' => '0',
                                    'parmission_access_my_workspace' => '1',
                                    'created_date' => date('Y-m-d H:i:s'),
                                    'created_by' => $user_id,
                                    'last_edit_date' => date('Y-m-d H:i:s'),
                                    'last_edit_by' => $user_id,
                                    'folders_check' => '0',
                                    'manage_coach_huddles' => '0',
                                    'manage_collab_huddles' => '0'
                                );



                                if ($this->UserAccount->save($data)) {

                                    $account_details = $this->Account->find('first', array('conditions' => array('id' => $account_id)));

                                    $account_owner_details = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
                                    $current_user_details = $this->User->find('first', array('conditions' => array('id' => $account_owner_details['UserAccount']['user_id'])));
                                    $data['email'] = $this->request->data['email'];
                                    $data['company_name'] = $account_details['Account']['company_name'];
                                    $data['creator_first_name'] = $current_user_details['User']['first_name'];
                                    $data['creator_last_name'] = $current_user_details['User']['last_name'];
                                    $data['creator_email'] = $current_user_details['User']['email'];

                                    $email_data = array(
                                        'email' => $this->request->data['email'],
                                        'account_id' => $account_id,
                                        'company_name' => $account_details['Account']['company_name'],
                                        'authentication_token' => md5($user_id),
                                        'user_id' => $user_id,
                                        'first_name' => $current_user_details['User']['first_name'],
                                        'last_name' => $current_user_details['User']['last_name'],
                                        'account_owner_email' => $current_user_details['User']['email']
                                    );


                                    $this->account_invitation_student_payment($email_data);


                                    //  $this->send_welcome_to_acc_email($data, $account_id);
                                }
                            }
                        }
                    }

                    $this->AccountFolder->updateAll(array('active' => 0), array('account_id' => $account_id, 'is_sample' => '1'));
                    $this->UserAccount->deleteAll(array('account_id' => $account_id, 'user_id' => array(2423, 242, 242)));
                    $this->Session->setFlash($this->language_based_messages['your_free_transaction_has_been_completed_successfully_msg'], 'default', array('class' => 'message success'));
                } else {
                    $this->Session->setFlash(__($result['message']), 'default', array('class' => 'message error'));
                }
            }
        } else {
            echo 'Not Valid';
            exit;
        }
    }

    function activation_page($account_id = '', $user_id)
    {
        // $this->layout = 'external';
        $this->layout = "activation";
        $account_details = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $user_details = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        if ($user_details['User']['type'] == 'Active') {
            $this->redirect('/');
        }
        $this->set('company_name', $account_details['Account']['company_name']);
        $this->set('first_name', $user_details['User']['first_name']);
        $this->set('last_name', $user_details['User']['last_name']);
        $this->set('email', $user_details['User']['email']);
        $this->set('verification_code', $user_details['User']['verification_code']);
        $this->set('user_id', $user_id);
        $this->set('account_id', $account_id);
    }

    function activate_user($user_id, $account_folder_id = '')
    {
        $this->User->updateAll(
            array(
                'type' => '"' . 'Active' . '"',
            ),
            array('MD5(id)' => $user_id)
        );

        $this->auto_login_url($user_id, $account_folder_id);
    }

    function activate_user_student_payment($user_id, $account_id, $account_folder_id = '')
    {

        $account_details = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $account_owner = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
        $account_owner_details = $this->User->find('first', array('conditions' => array('id' => $account_owner['UserAccount']['user_id'])));
        $user_details = $this->User->find('first', array('conditions' => array('MD5(id)' => $user_id)));

        $email_data = array(
            'email' => $user_details['User']['email'],
            'company_name' => $account_details['Account']['company_name'],
            'first_name' => $account_owner_details['User']['first_name'],
            'last_name' => $account_owner_details['User']['last_name'],
            'account_owner_email' => $account_owner_details['User']['email']
        );




        $this->account_invitation_student_payment($email_data);


        $this->User->updateAll(
            array(
                'type' => '"' . 'Active' . '"',
            ),
            array('MD5(id)' => $user_id)
        );

        $this->auto_login_url($user_id, $account_folder_id);
    }

    function activate_user_code($account_id, $user_id, $account_folder_id = '')
    {
        $verification_code = $this->request->data['verification_code'];

        $user_details = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        if ($verification_code == $user_details['User']['verification_code']) {
            $this->User->updateAll(
                array(
                    'type' => '"' . 'Active' . '"',
                ),
                array('id' => $user_id)
            );

            $this->auto_login($user_id, $account_folder_id);
        } else {
            $this->Session->setFlash($this->language_based_messages['incorrect_verification_code_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/users/activation_page/' . $account_id . '/' . $user_id);
        }
    }

    function resend_activation_code($account_id, $user_id)
    {

        $account_details = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $user_details = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        $verification_code = uniqid();
        $this->User->updateAll(
            array(
                'verification_code' => "'" . $verification_code . "'"
            ),
            array('id' => $user_id)
        );

        $data = array(
            'to_email' => $user_details['User']['email'],
            'name' => $user_details['User']['first_name'] . ' ' . $user_details['User']['last_name'],
            'account_name' => $account_details['Account']['company_name'],
            'user_id' => $user_id,
            'verification_code' => $verification_code
        );


        $this->activation_email($data, $account_id);
        $this->Session->setFlash($this->language_based_messages['activation_code_email_msg'], 'default', array('class' => 'message error'));
        $this->redirect('/users/activation_page/' . $account_id . '/' . $user_id);
    }

    function activte($account_id, $user_id)
    {
        $user = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        if ($user['User']['is_active'] == 1) {
            $this->User->updateAll(array('is_active' => 0), array('id' => $user_id));
            $this->UserAccount->updateAll(array('role_id' => 125), array('user_id' => $user_id, 'account_id' => $account_id));
            $this->Session->setFlash($user['User']['first_name'] . ' ' . $user['User']['last_name'] . ' ' . $this->language_based_messages['has_been_deactivated_msg'], 'default', array('class' => 'message success'));
            $this->redirect('/permissions/assign_user/' . $account_id . '/' . $user_id);
        } else {
            $view = new View($this, false);

            $allowed_users = $view->Custom->get_allowed_users($account_id);
            $total_users = $this->User->getTotalUsers($account_id);
            if ($total_users < $allowed_users) {
                $role = 120;
                $data = array(
                    'role_id' => "'" . $role . "'",
                    'last_edit_date' => "'" . date("Y-m-d h:i:s") . "'"
                );
                $data['permission_maintain_folders'] = '0';
                $data['permission_access_video_library'] = true;
                $data['permission_video_library_upload'] = '0';
                $data['permission_view_analytics'] = '0';
                $data['permission_administrator_user_new_role'] = '0';
                $data['parmission_access_my_workspace'] = TRUE;
                $data['manage_collab_huddles'] = '0';
                $data['manage_coach_huddles'] = '0';
                $data['folders_check'] = '0';

                $this->UserAccount->updateAll($data, array(
                    'user_id' => $user_id,
                    'account_id' => $account_id
                ));

                $this->User->updateAll(array('is_active' => 1, 'type' => "'Active'"), array('id' => $user_id));
                $this->Session->setFlash($user['User']['first_name'] . ' ' . $user['User']['last_name'] . ' ' . $this->language_based_messages['has_been_activated_msg'], 'default', array('class' => 'message success'));
                $this->redirect('/permissions/assign_user/' . $account_id . '/' . $user_id);
            } else {
                $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_users_msg'], ['allowed_users' => $view->Custom->get_allowed_users($account_id), 'sales_email' => $this->custom->get_site_settings('static_emails')['sales']]), 'default', array('class' => 'message error'));
                $this->redirect('/permissions/assign_user/' . $account_id . '/' . $user_id);
            }
        }
    }

    function getAccountSubscribers()
    {
        $this->layout = 'library';

        $user_current_account = $this->Session->read('user_current_account');
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];

        $account_id = $_POST['account_id'];

        $optInIds = $this->EmailUnsubscribers->find('all', array('conditions' => array('user_id' => $user_id, 'account_id' => $account_id)));
        // $emailFormat = $this->EmailFormats->find('all');
        $emailFormat = $this->EmailFormats->query('select * from email_formats as EmailFormats');

        $optOutIds = array();
        foreach ($optInIds as $optout) {
            $optOutIds[] = $optout['EmailUnsubscribers']['email_format_id'];
        }
?>

        <ul id="list-containers<?php echo $account_id; ?>">
            <?php if ($emailFormat) : ?>
                <?php
                foreach ($emailFormat as $row) :
                    if ($user_current_account['roles']['role_id'] == '120' || $user_current_account['roles']['role_id'] == '125') :
                        if ($row['EmailFormats']['template_name'] == 'monthly_report') {
                            continue;
                        }
                    endif;
                ?>
                    <li>
                        <label id="email-format-id-<?php echo $row['EmailFormats']['id'] ?>" for="email-format-<?php echo $row['EmailFormats']['id'] ?>">
                            <input type="checkbox" <?php echo ((isset($optOutIds) && is_array($optOutIds) && count($optOutIds) > 0 && in_array($row['EmailFormats']['id'], $optOutIds)) ? '' : 'checked="checked"') ?> id="email-format-<?php echo $row['EmailFormats']['id'] ?>" name="super_admin_ids[]" value="<?php echo $row['EmailFormats']['id'] ?>" class="member-user" onchange="test(this);"> <?php echo $row['EmailFormats']['full_name'] ?>
                        </label>
                        <div class="permissions">
                            <label id="template-idz-<?php echo $row['EmailFormats']['id'] ?>" template-id="<?php echo $row['EmailFormats']['id'] ?>" for="subscrib-template-<?php echo $row['EmailFormats']['id'] ?>">
                                <?php echo ((isset($optOutIds) && is_array($optOutIds) && count($optOutIds) > 0 && in_array($row['EmailFormats']['id'], $optOutIds)) ? '<span id="template-lable-' . $row['EmailFormats']['id'] . '">Unsubscribed</span>' : '<span id="template-lable-' . $row['EmailFormats']['id'] . '">Subscribed </span>') ?>
                            </label>
                        </div>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
<?php
        exit();
    }

    // Copy User Settings
    function copyUserSettings()
    {

        //$users =  $this->User->find('all', array('conditions' => array('id' => '5287')));
        //$users =  $this->User->find('all', array('conditions' => array('id' => array(5287, 5263, 5276))));

        $users = $this->User->find('all');
        $data = array();

        foreach ($users as $user) {

            $user_id = $user['User']['id'];
            $user_accounts = $this->UserAccount->find('all', array('conditions' => array('user_id' => $user_id)));

            foreach ($user_accounts as $user_account) {

                if (!empty($user_account)) {

                    $account_id = $user_account['UserAccount']['account_id'];
                    $email_unsubscribers_data = $this->EmailUnsubscribers->find('all', array('conditions' => array('user_id' => $user_id, 'account_id IS NULL')));

                    foreach ($email_unsubscribers_data as $eud) {

                        $this->EmailUnsubscribers->create();

                        $data = array(
                            'email_format_id' => $eud['EmailUnsubscribers']['email_format_id'],
                            'user_id' => $eud['EmailUnsubscribers']['user_id'],
                            'account_id' => $account_id,
                            'created_at' => date('Y-m-d H:i:s', time()),
                            'created_by' => $eud['EmailUnsubscribers']['user_id']
                        );

                        //echo "<pre>"; print_r($data);
                        $this->EmailUnsubscribers->save($data);
                    }
                }
            }

            $this->EmailUnsubscribers->deleteAll(array('user_id' => $user_id, 'account_id IS NULL'));
            $this->EmailUnsubscribers->deleteAll(array('email_format_id' => array(9, 10)));
        }
        echo 'Successfully Copied';
        die();
    }

    function addLangSettings()
    {
        $lang_id = $this->request->data['lang_id'];
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $this->User->updateAll(
            array('lang' => "'" . $lang_id . "'"),
            array('id' => $user_id)
        );

        if ($this->User->getAffectedRows() > 0) {
            echo json_encode(array('status' => 'success'));
        } else {
            echo json_encode(array('status' => 'failed'));
        }
        die;
    }

    function update_user_email_language($user_id)
    {
        $this->User->updateAll(['lang' => "'" . $this->request->data['lang'] . "'"], ['id' => $user_id]);
        echo json_encode(array('message' => true));
        die;
    }

    public function safe_b64encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }
    public function safe_b64decode($string)
    {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
    public function encode_account_id($value)
    {
        if (!$value) {
            return false;
        }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }
    public function decode_account_id($value)
    {
        if (!$value) {
            return false;
        }
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    function sibme_registration($account_id = '')
    {

        // $this->layout = 'external';
        if (isset($this->request->query['code']) && $this->request->query['code'] != '') {
            $code = $this->request->query['code'];
        } else {
            $code = '';
        }

        if (isset($this->request->query['first_name']) && $this->request->query['first_name'] != '') {
            $first_name = $this->request->query['first_name'];
        } else {
            $first_name = '';
        }

        if (isset($this->request->query['last_name']) && $this->request->query['last_name'] != '') {
            $last_name = $this->request->query['last_name'];
        } else {
            $last_name = '';
        }

        if (isset($this->request->query['email']) && $this->request->query['email'] != '') {
            $email = $this->request->query['email'];
            $email = str_replace(" ", "+", $email);
        } else {
            $email = '';
        }
        $this->layout = "payment_free";
        $account_id = base64_decode($account_id);
        $blocked_account_id = Configure::read('blocked_account_id');
        $account_ids = explode(',', $blocked_account_id);
        //        if(in_array($account_id,$account_ids)){
        //            $this->redirect('/');
        //            exit;
        //        }
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('student_payment');
        $this->set('language_based_content', $language_based_content);
        $this->set('code', $code);
        $this->set('first_name', $first_name);
        $this->set('last_name', $last_name);
        $this->set('email', $email);
        $this->set('account_id', base64_encode($account_id));
        $huddle_info = $this->Account->find('first', array('conditions' => array('id' => (int) $account_id)));
        $account_owner = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
        $account_owner_details = $this->User->find('first', array('conditions' => array('id' => $account_owner['UserAccount']['user_id'])));
        $company_name = $huddle_info['Account']['company_name'];
        $this->set('company_name', $company_name);
        $type = '0';
        if ($huddle_info) {
            $this->set('user_current_account', $huddle_info);
            if ($this->request->is('post')) {

                if ($this->request->data['password'] != $this->request->data['password_confirmation']) {
                    $this->Session->setFlash($this->language_based_messages['password_do_not_match_please_enter_correct_passwords_msg']);
                    $this->redirect('sibme_registration/' . base64_encode($account_id) . '?code=' . $code);
                }

                if (isset($this->request->data['auth_code'])) {
                    $auth_code_details = $this->AccountUserCode->find('first', array('conditions' => array('code' => $this->request->data['auth_code'], 'account_id' => $account_id)));
                    if (empty($auth_code_details)) {
                        $this->Session->setFlash('Invalid Code.');
                        $this->redirect('sibme_registration/' . base64_encode($account_id) . '?code=' . $code);
                    } else if ($auth_code_details['AccountUserCode']['is_used'] == '1') {
                        $this->Session->setFlash('Code is already Used.');
                        $this->redirect('sibme_registration/' . base64_encode($account_id) . '?code=' . $code);
                    } else if (date('Y-m-d') > $auth_code_details['AccountUserCode']['expiry_date']) {
                        $this->Session->setFlash('Code is expired.');
                        $this->redirect('sibme_registration/' . base64_encode($account_id) . '?code=' . $code);
                    }
                }

                if (true) {
                    //$array = $result->transaction->_attributes;
                    if (!$this->User->isUserEmailExists($this->request->data['email']) > 0) {
                        $this->User->create();
                        $role_id = 120;
                        $verification_code = uniqid();
                        $data = array(
                            'first_name' => $this->request->data['first_name'],
                            'last_name' => $this->request->data['last_name'],
                            'email' => $this->request->data['email'],
                            'created_date' => date('Y-m-d H:i:s'),
                            'username' => $this->request->data['email'],
                            'password' => $this->request->data['password'],
                            'institution_id' => $this->request->data['institution_id'],
                            'type' => 'Pending_Activation',
                            'is_active' => TRUE,
                            'verification_code' => $verification_code
                        );

                        $this->User->save($data);
                        $user_id = $this->User->id;
                        $this->User->updateAll(array(
                            'authentication_token' => "'" . md5($user_id) . "'"
                        ), array('User' . "." . 'id' => $user_id));

                        $this->UserAccount->create();
                        $data = array(
                            'user_id' => $user_id,
                            'account_id' => $account_id,
                            'role_id' => $role_id,
                            'is_default' => true,
                            'permission_maintain_folders' => '1',
                            'permission_access_video_library' => '1',
                            'permission_video_library_upload' => '1',
                            'permission_administrator_user_new_role' => '1',
                            'parmission_access_my_workspace' => '1',
                            'created_date' => date('Y-m-d H:i:s'),
                            'created_by' => $user_id,
                            'last_edit_date' => date('Y-m-d H:i:s'),
                            'last_edit_by' => $user_id,
                            'folders_check' => '1',
                            'manage_coach_huddles' => '1',
                            'manage_collab_huddles' => '1'
                        );

                        if ($role_id == '120') {
                            $data['permission_maintain_folders'] = '0';
                            $data['permission_access_video_library'] = '1';
                            $data['permission_video_library_upload'] = '0';
                            $data['permission_administrator_user_new_role'] = '0';
                            $data['parmission_access_my_workspace'] = '1';
                            $data['folders_check'] = '0';
                            $data['manage_coach_huddles'] = '0';
                            $data['manage_collab_huddles'] = '0';
                        }
                        $this->UserAccount->save($data);
                        $email_data = array(
                            'email' => $this->request->data['email'],
                            'account_id' => $account_id,
                            'company_name' => $company_name,
                            'authentication_token' => md5($user_id),
                            'user_id' => $user_id,
                            'first_name' => $account_owner_details['User']['first_name'],
                            'last_name' => $account_owner_details['User']['last_name'],
                            'account_owner_email' => $account_owner_details['User']['email']
                        );

                        // $this->sendInvitation_student_payment($email_data);
                        //$this->account_invitation_student_payment($email_data);

                        $this->request->data['id'] = $user_id;
                        $this->request->data['account_id'] = $account_id;


                        $data = array(
                            'to_email' => $this->request->data['email'],
                            'name' => $this->request->data['first_name'] . ' ' . $this->request->data['last_name'],
                            'account_name' => $company_name,
                            'user_id' => $user_id,
                            'verification_code' => $verification_code,
                            'account_id' => $account_id
                        );

                        $type = '1';
                        //$this->activation_email_student_payment($data, $account_id);
                        $this->send_two_factor_auth_token($data, $account_id);
                    } else {
                        $role_id = 120;
                        $user_details = $this->User->find('first', array('conditions' => array('email' => $this->request->data['email'])));

                        if (!empty($user_details)) {
                            $user_id = $user_details['User']['id'];
                            $user_account_details = $this->UserAccount->find('first', array('conditions' => array('user_id' => $user_id, 'account_id' => $account_id)));
                            if (empty($user_account_details)) {
                                $this->UserAccount->create();
                                $data = array(
                                    'user_id' => $user_id,
                                    'account_id' => $account_id,
                                    'role_id' => $role_id,
                                    'is_default' => true,
                                    'permission_maintain_folders' => '0',
                                    'permission_access_video_library' => '1',
                                    'permission_video_library_upload' => '0',
                                    'permission_administrator_user_new_role' => '0',
                                    'parmission_access_my_workspace' => '1',
                                    'created_date' => date('Y-m-d H:i:s'),
                                    'created_by' => $user_id,
                                    'last_edit_date' => date('Y-m-d H:i:s'),
                                    'last_edit_by' => $user_id,
                                    'folders_check' => '0',
                                    'manage_coach_huddles' => '0',
                                    'manage_collab_huddles' => '0'
                                );



                                if ($this->UserAccount->save($data)) {

                                    $account_details = $this->Account->find('first', array('conditions' => array('id' => $account_id)));

                                    $account_owner_details = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
                                    $current_user_details = $this->User->find('first', array('conditions' => array('id' => $account_owner_details['UserAccount']['user_id'])));
                                    $data['email'] = $this->request->data['email'];
                                    $data['company_name'] = $account_details['Account']['company_name'];
                                    $data['creator_first_name'] = $current_user_details['User']['first_name'];
                                    $data['creator_last_name'] = $current_user_details['User']['last_name'];
                                    $data['creator_email'] = $current_user_details['User']['email'];
                                    $verification_code = uniqid();
                                    $data = array(
                                        'to_email' => $this->request->data['email'],
                                        'name' => $this->request->data['first_name'] . ' ' . $this->request->data['last_name'],
                                        'account_name' => $company_name,
                                        'user_id' => $user_id,
                                        'verification_code' => $verification_code,
                                        'account_id' => $account_id
                                    );
                                    $this->User->updateAll(array('verification_code' => "'" . $verification_code . "'"), array('id' => $user_id));
                                    $type = '2';
                                    $this->send_two_factor_auth_token($data, $account_id);
                                    //$this->account_invitation_student_payment($email_data);


                                    //  $this->send_welcome_to_acc_email($data, $account_id);
                                }
                            } else {
                                $this->Session->setFlash(__($this->language_based_messages['user_is_already_registered']), 'default', array('class' => 'message error'));
                                $this->redirect('sibme_registration/' . base64_encode($account_id) . '?code=' . $code);
                            }
                        }
                    }

                    $this->AccountFolder->updateAll(array('active' => 0), array('account_id' => $account_id, 'is_sample' => '1'));
                    //                    $start_yr_date = date('Y') . '-01-01';
                    //                    $end_yr_date = date('Y') . '-07-01';
                    //
                    //                    if( date('Y-m-d') >= $start_yr_date && date('Y-m-d') < $end_yr_date  )
                    //                    {
                    //                        $expiry_date = $end_yr_date;
                    //                    }
                    //                    else if(date('Y-m-d') >= $end_yr_date )
                    //                    { 
                    //                        $expiry_date = date('Y-m-d', strtotime($start_yr_date . ' + 1 year'));
                    //                    }
                    //
                    //                    else if(date('Y-m-d') < $start_yr_date )
                    //                    {
                    //                        $expiry_date = $end_yr_date;
                    //                    }
                    $this->AccountUserCode->updateAll(array('is_used' => '1', 'user_id' => $user_id), array('account_id' => $account_id, 'code' => $this->request->data['auth_code']));
                    $this->UserAccount->deleteAll(array('account_id' => $account_id, 'user_id' => array(2423, 242, 242)));
                    //$this->Session->setFlash($this->language_based_messages['your_free_transaction_has_been_completed_successfully_msg'], 'default', array('class' => 'message success'));
                    $this->redirect('two_factor_auth/' . $account_id . '/' . $user_id . '/' . $type);
                } else {
                    $this->Session->setFlash(__($result['message']), 'default', array('class' => 'message error'));
                }
            }
        } else {
            echo 'Not Valid';
            exit;
        }
    }

    function two_factor_auth($account_id, $user_id, $type = '1')
    {
        $this->layout = "null";
        $this->set('account_id', $account_id);
        $this->set('user_id', $user_id);
        $this->set('type', $type);
        if ($this->request->is('post')) {
            $users = $this->User->find('first', array('conditions' => array(
                'id' => $user_id
            )));
            $accounts = $this->Account->find('first', array('conditions' => array(
                'id' => $account_id
            )));
            if ($users['User']['verification_code'] == $this->request->data['verification_code']) {
                if ($type == '1') {
                    $data = array(
                        'to_email' => $users['User']['email'],
                        'name' => $users['User']['first_name'] . ' ' . $users['User']['last_name'],
                        'account_name' => $accounts['Account']['company_name'],
                        'user_id' => $user_id,
                        'account_id' => $account_id
                    );


                    $this->activation_email_student_payment($data, $account_id);
                    $this->Session->setFlash($this->language_based_messages['your_free_transaction_has_been_completed_successfully_msg'], 'default', array('class' => 'message success'));
                    $this->redirect('/');
                } else {
                    $account_owner_details = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
                    $current_user_details = $this->User->find('first', array('conditions' => array('id' => $account_owner_details['UserAccount']['user_id'])));
                    $email_data = array(
                        'email' => $this->request->data['email'],
                        'account_id' => $account_id,
                        'company_name' => $accounts['Account']['company_name'],
                        'authentication_token' => md5($user_id),
                        'user_id' => $user_id,
                        'first_name' => $current_user_details['User']['first_name'],
                        'last_name' => $current_user_details['User']['last_name'],
                        'account_owner_email' => $current_user_details['User']['email']
                    );


                    $this->account_invitation_student_payment($email_data);
                    $this->Session->setFlash($this->language_based_messages['your_free_transaction_has_been_completed_successfully_msg'], 'default', array('class' => 'message success'));
                    $this->redirect('/');
                }
            } else {


                $account_user_code = $this->AccountUserCode->find('first', array('conditions' => array(
                    'user_id' => $user_id,
                    'account_id' => $account_id
                )));

                if ($account_user_code['AccountUserCode']['num_tries'] == '2') {
                    $this->UserAccount->deleteAll(array('account_id' => $account_id, 'user_id' => $user_id));
                    if ($type == '1') {
                        $user_data = $this->User->find('first', array('conditions' => array(
                            'id' => $user_id,
                        )));
                        $this->User->updateAll(array('User.email' => "'" . $user_data['User']['email'] . "-deleted'", 'User.username' => "'" . $user_data['User']['username'] . "-deleted'", 'is_active' => '0'), array('id' => $user_id));
                    }
                    $this->AccountUserCode->updateAll(array('num_tries' => '0', 'user_id' => NULL, 'is_used' => '0'), array('account_id' => $account_id, 'user_id' => $user_id));
                    $this->Session->setFlash('Two Factor Authentication Failed.', 'default', array('class' => 'message error'));
                    $this->redirect('sibme_registration/' . base64_encode($account_id));
                } else {
                    $num_tries = (int)$account_user_code['AccountUserCode']['num_tries'];
                    $num_tries = $num_tries + 1;
                    $this->AccountUserCode->updateAll(array('num_tries' => $num_tries), array('account_id' => $account_id, 'user_id' => $user_id));
                    $this->Session->setFlash('Error: Code does not match the code emailed. Please try again.', 'default', array('class' => 'message error'));
                    $this->redirect('two_factor_auth/' . $account_id  . '/' . $user_id);
                }
            }
        }
    }

    public function check_login_status(){
        echo json_encode(['status'=>$this->Auth->loggedIn(), 'site_id'=>$this->site_id]);
        exit;
    }
    
    /*LOGIN FROM LTI (CANVAS)*/
    
    public function login_from_lti($email_address, $uid){
     
       if(!empty($email_address) && !empty($uid)){
            $request_details = $this->LoginRequest->find('first', array('conditions' => array(
                    'user_email' => $email_address,
                    'reference_id' => $uid
                )));
            if(!empty($request_details['LoginRequest'])){
                $currentDate = date('Y-m-d H:i:s');
                $request_time = new DateTime($request_details["LoginRequest"]["request_at"]);
                $difference = $request_time->diff(new DateTime($currentDate));
             if($difference->i !=0 && $difference->i > 2){
                    die("Your link is expired. please open the App again from Canvas.");
                }else{                   
                    $user_details = $this->User->query("SELECT * FROM users WHERE username ='$email_address' OR email ='$email_address'");
                    $this->auto_login($user_details[0]['users']['id']);
                }
            }else{
                die("user does not exists in sibme.");
            }
        }else{
            echo 'Invalid Parameters';
        }
        die();
    }
}
