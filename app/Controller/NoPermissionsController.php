<?php

class NoPermissionsController extends AppController {

    function no_permission() {
        $this->layout = 'minimal';
        $this->render('no_permission');
    }
    function inactive_users(){
        $this->layout = 'minimal';
        $this->render('inactive_users');
    }

}

?>
