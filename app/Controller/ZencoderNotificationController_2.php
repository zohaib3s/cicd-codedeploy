<?php

class ZencoderNotificationController extends AppController {

    public $uses = array('Document', 'AccountFolder', 'AccountFolderDocument', 'User');

    function received_notification() {

        $current_user = $this->Session->read('user_current_account');
        $account_id = $current_user['accounts']['account_id'];

        $this->layout = null;
        require_once('src/Services/Zencoder.php');

        $zencoder = new Services_Zencoder('39bdc265c56dc8e09b62b8fd38f475f9');
        // Catch notification
        $notification = $zencoder->notifications->parseIncoming();

        if (isset($notification)) {

            $job_id = $notification->job->id;

            //if ($notification->job->state == 'finished') {

            $is_video_published_now = false;

            $video_old = $this->Document->find('first', array('conditions' => array('zencoder_output_id' => $job_id)));

            if ($video['Document']['published'] != 1) {
                $is_video_published_now = true;
            }

            $this->Document->create();
            $data = array(
                'published' => 1
            );
            $this->Document->updateAll($data, array('zencoder_output_id' => $job_id), $validation = TRUE);

            $video = $this->Document->find('first', array('conditions' => array('zencoder_output_id' => $job_id)));
            $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('zencoder_output_id' => $job_id)));
            $account_folder = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'])));
            $creator = $this->User->find('first', array('conditions' => array('id' => $video['Document']['created_by'])));
            $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_document['AccountFolderDocument']['account_folder_id']);

//            $this->sendVideoPublishedNotification(
//                    $account_folder['AccountFolder']['name']
//                    , $account_folder['AccountFolder']['account_folder_id']
//                    , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $huddleUsers, $creator['User']['email']
//            );


            if ($account_folder['AccountFolder']['folder_type'] != 1) {
                if ($this->check_subscription($creator['User']['id'], '7',$account_id)) {
                    $this->sendVideoPublishedNotification(
                            $account_folder['AccountFolder']['name']
                            , $account_folder['AccountFolder']['account_folder_id']
                            , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $creator, $creator['User']['email'], $creator['User']['id'], $account_folder['AccountFolder']['folder_type']);
                }
            } else {

                if (count($huddleUsers) > 0) {
                    for ($i = 0; $i < count($huddleUsers); $i++) {

                        $huddleUser = $huddleUsers[$i];

                        if ($is_video_published_now) {
                            if ($this->check_subscription($huddleUser['User']['id'], '7',$account_id)) {
                                $this->sendVideoPublishedNotification(
                                        $account_folder['AccountFolder']['name']
                                        , $account_folder['AccountFolder']['account_folder_id']
                                        , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $huddleUsers, $huddleUser['User']['email'], $huddleUser['User']['id'], $account_folder['AccountFolder']['folder_type']);
                            }
                        }
                    }
                }
            }
        }

        $this->set('ajaxdata', "$job_id.$state_job");
        $this->render('/Elements/ajax/ajaxreturn');
    }

}

?>