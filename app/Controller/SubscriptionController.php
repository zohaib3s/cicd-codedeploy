<?php

App::uses('AppController', 'Controller');

class SubscriptionController extends AppController {

    function unsubscirbe_now($user_id, $template_id) {

        $users = $this->Session->read('user_current_account');
        $account_id = $user_current_account['accounts']['account_id'];
        $email_template = '';
        if ($template_id == 1) {
            $email_template = 'Huddle document upload notifications';
        } elseif ($template_id == 2) {
            $email_template = 'Huddle discussion notifications';
        } elseif ($template_id == 4) {
            $email_template = 'Huddle invitation notifications';
        } elseif ($template_id == 5) {
            $email_template = 'Published video notifications';
        } elseif ($template_id == 6) {
            $email_template = 'Huddle video comment notifications';
        } elseif ($template_id == 7) {
            $email_template = 'Huddle video upload notifications';
        }

        $users_accounts = $this->UserAccount->find('all', array(
            'conditions' => array(
                'user_id' => $user_id
            )
        ));

        $this->EmailUnsubscribers->deleteAll(array('user_id' => $user_id, 'email_format_id' => $template_id, 'account_id' => ''));
        $message = '';
        if ($users_accounts) {
            foreach ($users_accounts as $row) {
                $account_id = $row['UserAccount']['account_id'];
                $already_exist = $this->EmailUnsubscribers->find('count', array('conditions' => array('email_format_id' => $template_id, 'user_id' => $user_id, 'account_id' => $account_id)));
                if ($already_exist == 0) {
                    $result_array = array(
                        'email_format_id' => $template_id,
                        'user_id' => $user_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $user_id,
                        'account_id' => $account_id
                    );
                    $this->EmailUnsubscribers->create();
                    $this->EmailUnsubscribers->save($result_array);
                    if ($this->EmailUnsubscribers->getLastInsertId() != '') {
                        $message = 'You have Successfully Unsubscribed ' . $email_template;
                    } else {
                        $message = 'You  are unable to Unsubscribed ' . $email_template . ' please try again.';
                    }
                } else {
                    $message = 'You have Successfully Unsubscribed ' . $email_template;
                }
            }
        } else {
            $message = 'User Account is not exist, please try with correct account.';
        }
        $this->set('message', $message);
        $this->render('unsubscribed_new');
    }

}
