<?php

class EdtpaController extends AppController {

    public $uses = array('User', 'UserAccount', 'Document', 'EdtpaAssessmentsHandbooks', 'EdtpaAssessmentAccounts', 'EdtpaAssessments',
        'EdtpaAssessmentPortfolios', 'EdtpaAssessmentPortfolioNodes', 'EdtpaAssessmentPortfolioCandidateSubmissions', 'EdtpaAssessmentAccountCandidates', 'EdtpaAssessmentAccountCandidateTransfers');

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $view = new View($this, false);
        $this->is_enabled_edtpa();
        $edtpa_result = $view->Custom->get_page_lang_based_content('Api/edTPA');

        $this->Session->write('edtpa_lang', $edtpa_result);
    }

    function is_enabled_edtpa() {
        $user_permissions = $this->Session->read('user_permissions');
        $user = $this->Session->read('user_current_account');

        if ((isset($user_permissions['accounts']['enable_edtpa']) && ($user_permissions['accounts']['enable_edtpa'] == '0' || $user_permissions['accounts']['enable_edtpa'] == false)) || $user['users_accounts']['role_id'] == 125) {
            $this->Session->setFlash($this->language_based_messages['you_dont_have_permission_edtpa'], 'default', array('class' => 'message error'));
            return $this->redirect('/Dashboard/home');
        }
    }

    function edtpa() {
        $view = new View($this, false);
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        if ($view->Custom->is_teacher($account_id, $user_id)) {
            $this->teacher_view();
        } else {
            $this->student_view();
        }
    }

    function student_view() {
        $user_permissions = $this->Session->read('user_permissions');
        if (!isset($user_permissions['accounts']['enable_edtpa']) || !$user_permissions['accounts']['enable_edtpa'] == '1') {
            $this->redirect('/Dashboard/home');
        }

        $this->set('edTPA_submission_summery', $this->edTPA_submission());

        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $result = $this->EdtpaAssessments->getEtpaAssessment($account_id, $user_id);
        if (!empty($result)) {
            foreach ($result as $single_assessment):
                if ($single_assessment['EdtpaAssessmentAccountCandidates']['status'] == 4) {
                    $tranfer_record = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $single_assessment['EdtpaAssessmentAccountCandidates']['id']), 'order' => array('id' => 'DESC')));
                    $tranfer_id = $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['id'];
                    $status_response = $this->Portfolio_Package_Transfer_Status($tranfer_id, true);
                    $status_response_arr = json_decode($status_response);
                    $status_val = '';
                    if (isset($status_response_arr->partnerTransferRequestStatusList['status'])) {
                        if ($status_response_arr->partnerTransferRequestStatusList['status'] == 'IR_PA_Pending') {
                            $status_val = 6;
                        } else if ($status_response_arr->partnerTransferRequestStatusList['status'] == 'IR_Failed') {
                            $status_val = 7;
                        } else if ($status_response_arr->partnerTransferRequestStatusList['status'] == 'IR_Success') {
                            $status_val = 8;
                        }
                        $this->EdtpaAssessmentAccountCandidates->updateAll(array('status' => $status_val), array('id' => $single_assessment['EdtpaAssessmentAccountCandidates']['id']));
                    }
                }
            endforeach;
        }
        $assessment_accounts = $this->EdtpaAssessments->getEtpaAssessment($account_id, $user_id);
        $assessment_accounts_final = array();
        foreach ($assessment_accounts as $assessment_account):
            $tranfer_record = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $assessment_account['EdtpaAssessmentAccountCandidates']['id']), 'order' => array('id' => 'DESC')));
            $tranfer_id = $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['id'];
            $assessment_accounts_inner['EdtpaAssessments']['id'] = $assessment_account['EdtpaAssessments']['id'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_name'] = $assessment_account['EdtpaAssessments']['assessment_name'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_version'] = $assessment_account['EdtpaAssessments']['assessment_version'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_id'] = $assessment_account['EdtpaAssessments']['assessment_id'];
            $assessment_accounts_inner['EdtpaAssessments']['project_id'] = $assessment_account['EdtpaAssessments']['project_id'];
            $assessment_accounts_inner['EdtpaAssessments']['created_at'] = $assessment_account['EdtpaAssessments']['created_at'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_type'] = $assessment_account['EdtpaAssessments']['assessment_type'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_xml'] = $assessment_account['EdtpaAssessments']['assessment_xml'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['user_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'] = $assessment_account['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['status'] = $assessment_account['EdtpaAssessmentAccountCandidates']['status'];
            $assessment_accounts_inner['transfer_id'] = $tranfer_id;
            array_push($assessment_accounts_final, $assessment_accounts_inner);
        endforeach;

        $this->set('assessment_accounts', $assessment_accounts_final);
        $this->render('student_view');
    }

    function fetach_assessments() {
        $edtpa_lang = $this->Session->read('edtpa_lang');
        $this->Session->delete('selected_node');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $page = $this->data['page'];
        $rows = $this->data['rows'];
        $sidx = $this->data['sidx'];
        $sord = $this->data['sord'];
        $offset = $page * $rows;
        $limit = $offset - $rows;

        $total_records = $this->EdtpaAssessments->getEtpaAssessment($account_id, $user_id, false, false);
        $assessment_accounts = $this->EdtpaAssessments->getEtpaAssessment($account_id, $user_id, $limit, $offset);
        $assessment_accounts_final = array();
        $result = array(
            "records" => ceil(count($total_records) / 10),
            "page" => 1,
            "total" => count($total_records),
        );
        $total_submissions = $this->EdtpaAssessmentAccountCandidates->find('count', array(
            'conditions' => array(
                'EdtpaAssessmentAccountCandidates.user_id' => $user_id,
                'EdtpaAssessmentAccountCandidates.account_id' => $account_id
            )
        ));


        $data = array();
        $has_started = false;
        if ($total_submissions > 0) {
            $has_started = true;
        }

        $is_cancel_local = false;
        $is_cancel_local_2 = false;
        foreach ($assessment_accounts as $assessment_account):
            $result = $this->EdtpaAssessmentAccountCandidates->find('first', array(
                'conditions' => array(
                    'EdtpaAssessmentAccountCandidates.user_id' => $user_id,
                    'EdtpaAssessmentAccountCandidates.account_id' => $account_id,
                    'EdtpaAssessmentAccountCandidates.edtpa_assessment_account_id' => $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id']
                )
            ));
            if (isset($result['EdtpaAssessmentAccountCandidates']['id']) && ($result['EdtpaAssessmentAccountCandidates']['status'] == 1 || $result['EdtpaAssessmentAccountCandidates']['status'] == 0)) {

                $submit_status = $edtpa_lang['edtpa_assessment_started'];
                $cancel_btn = true;
                $is_cancel_local = true;
            } else if (isset($result['EdtpaAssessmentAccountCandidates']['id']) && $result['EdtpaAssessmentAccountCandidates']['status'] == 2) {
                $submit_status = $edtpa_lang['edtpa_auth_key_varify'];
                 $cancel_btn = false;
                 $is_cancel_local = false;
                 $is_cancel_local_2 = false;
            } else if (isset($result['EdtpaAssessmentAccountCandidates']['id']) && $result['EdtpaAssessmentAccountCandidates']['status'] == 3) {
                $submit_status = $edtpa_lang['edtpa_transfer_varified'];
                $cancel_btn = true;
                $is_cancel_local = false;
                $is_cancel_local_2 = true;
            } else if (isset($result['EdtpaAssessmentAccountCandidates']['id']) && $result['EdtpaAssessmentAccountCandidates']['status'] == 4) {
                $submit_status = $edtpa_lang['edtpa_transfer_requested'];
                $cancel_btn = true;
                $is_cancel_local = false;
                $is_cancel_local_2 = true;
            } else if (isset($result['EdtpaAssessmentAccountCandidates']['id']) && $result['EdtpaAssessmentAccountCandidates']['status'] == 5) {
                $submit_status = $edtpa_lang['edtpa_transfer_accepted'];
                $cancel_btn = true;
                $is_cancel_local = false;
            } else if (isset($result['EdtpaAssessmentAccountCandidates']['id']) && $result['EdtpaAssessmentAccountCandidates']['status'] == 8) {
                $submit_status = $edtpa_lang['edtpa_success'];
                $cancel_btn = true;
                $is_cancel_local = false;
                $is_cancel_local_2 = true;
            } else if (isset($result['EdtpaAssessmentAccountCandidates']['id']) && $result['EdtpaAssessmentAccountCandidates']['status'] == 9) {
                $submit_status = $edtpa_lang['edtpa_transfer_failed'];
                $cancel_btn = true;
                $is_cancel_local = false;
                $is_cancel_local_2 = true;
            } else {
                $submit_status = 'N/A';
                $cancel_btn = false;
                $is_cancel_local = false;
                $is_cancel_local_2 = true;
            }


            $edtpa_account_candidates = $this->EdtpaAssessmentAccounts->find('first', array(
                'joins' => array(
                    array(
                        'table' => 'edtpa_assessment_account_candidates AS EAAC',
                        'type' => 'left',
                        'conditions' => 'EAAC.edtpa_assessment_account_id = EdtpaAssessmentAccounts.id'
                    )
                ),
                'conditions' => array(
                    'EdtpaAssessmentAccounts.edtpa_assessment_id' => $assessment_account['EdtpaAssessments']['id'],
                    'EAAC.user_id' => $user_id,
                    'EdtpaAssessmentAccounts.account_id' => $account_id
                ),
                'fields' => 'EAAC.*'
            ));


            $tranfer_record = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $assessment_account['EdtpaAssessmentAccountCandidates']['id']), 'order' => array('id' => 'DESC')));

            $class = '';

            if ($has_started) {
                if (count($edtpa_account_candidates) > 0 && $cancel_btn == true) {
                    $class = 'cantEditCls';
                } elseif (count($edtpa_account_candidates) > 0 && $cancel_btn == true) {

                } elseif (count($edtpa_account_candidates) == 0) {
                    $class = 'cantEditCls';
                } else {
                    $class = '';
                }
            } else {
                $class = '';
            }

            $select_btn = '';
            $assessment_name = '';
            if ($has_started) {
                if ($cancel_btn == true) {
                    $href = 'javascript:void(0);';
                    $select_btn = $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id'];
                    if ($_SESSION['LANG'] == 'es') {
                        $assessment_name = "<a class='' href='" . $select_btn . "'>" . $assessment_account['EdtpaAssessments']['assessment_name_es'] . "</a>";
                    } else {
                        $assessment_name = "<a class='' href='" . $select_btn . "'>" . $assessment_account['EdtpaAssessments']['assessment_name'] . "</a>";
                    }
                } elseif (count($edtpa_account_candidates) == 0) {
                    $href = 'javascript:void(0);';
                    $select_btn = $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id'];
                    if ($_SESSION['LANG'] == 'es') {
                        $assessment_name = "<a class='" . $class . "' href='" . $href . "'>" . $assessment_account['EdtpaAssessments']['assessment_name_es'] . "</a>";
                    } else {
                        $assessment_name = "<a class='" . $class . "' href='" . $href . "'>" . $assessment_account['EdtpaAssessments']['assessment_name'] . "</a>";
                    }
                } else {
                    $select_btn = '';
                    $href = $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id'];
                    $assessment_name = "<a class='.$class.' href='" . $href . "'>" . $assessment_account['EdtpaAssessments']['assessment_name'] . "</a>";
                }
            } else {
                $select_btn = '';
                $href = $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id'];
                if ($_SESSION['LANG'] == 'es') {
                    $assessment_name = "<a class='.$class.' href='" . $href . "'>" . $assessment_account['EdtpaAssessments']['assessment_name_es'] . "</a>";
                } else {
                    $assessment_name = "<a class='.$class.' href='" . $href . "'>" . $assessment_account['EdtpaAssessments']['assessment_name'] . "</a>";
                }
            }


            $tranfer_id = $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['id'];
            $action_btn = '';
            if ($cancel_btn) {
                //var_dump($is_cancel_local);
                if ($is_cancel_local == true) {
                    $candidate_id = $result['EdtpaAssessmentAccountCandidates']['id'];
                    $action_btn = '<a href="javascript:void(0);" data-attr-condidate-id="' . $candidate_id . '" class="cancleRequestLocal">' . $edtpa_lang['edtpa_cancel'] . '</a>';
                    if ($select_btn != '') {
                        $action_btn .= ' | <a href="' . $select_btn . '"  class="">' . $edtpa_lang['edtpa_select'] . '</a>';
                    }
                } elseif ($is_cancel_local_2 = true) {
                    $candidate_id = $result['EdtpaAssessmentAccountCandidates']['id'];
                    $action_btn = '<a href="javascript:void(0);" data-attr-condidate-id="' . $tranfer_id . '" class="cancleRequestLocal_2">' . $edtpa_lang['edtpa_cancel'] . '</a>';
                    if ($select_btn != '') {
                        $action_btn .= ' | <a href="' . $select_btn . '"  class="">' . $edtpa_lang['edtpa_select'] . '</a>';
                    }
                } else {
                    $action_btn = '<a href="javascript:void(0);" data-attr-transfer-id="' . $tranfer_id . '" class="cancleRequest" onlclick="cancelRequest(' . $tranfer_id . ')">Cancel</a>';
                    if ($select_btn != '') {
                        $action_btn .= ' | <a href="' . $select_btn . '"  class="">' . $edtpa_lang['edtpa_select'] . '</a>';
                    }
                }
            } else {
                $action_btn = "<a class='" . $class . "' href='" . $href . "'>" . $edtpa_lang['edtpa_select'] . "</a>";
            }
            $assessment_accounts_inner['EdtpaAssessments']['id'] = $assessment_account['EdtpaAssessments']['id'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_name'] = $assessment_account['EdtpaAssessments']['assessment_name'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_version'] = $assessment_account['EdtpaAssessments']['assessment_version'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_id'] = $assessment_account['EdtpaAssessments']['assessment_id'];
            $assessment_accounts_inner['EdtpaAssessments']['project_id'] = $assessment_account['EdtpaAssessments']['project_id'];
            $assessment_accounts_inner['EdtpaAssessments']['created_at'] = $assessment_account['EdtpaAssessments']['created_at'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_type'] = $assessment_account['EdtpaAssessments']['assessment_type'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_xml'] = $assessment_account['EdtpaAssessments']['assessment_xml'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['user_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'] = $assessment_account['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['status'] = $assessment_account['EdtpaAssessmentAccountCandidates']['status'];
            $assessment_accounts_inner['transfer_id'] = $tranfer_id;
            $assessment_accounts_inner['EdtpaAssessments']['status'] = $submit_status;
            $assessment_accounts_inner['EdtpaAssessments']['cancel_btn'] = $cancel_btn;
            $data[] = array(
                'assessment_name' => $assessment_name,
                'assessment_version' => $assessment_account['EdtpaAssessments']['assessment_version'],
                'assessment_id' => $assessment_account['EdtpaAssessments']['assessment_id'],
                'project_id' => $assessment_account['EdtpaAssessments']['project_id'],
                'assessment_xml' => $assessment_account['EdtpaAssessments']['assessment_xml'],
                'transfer_id' => $tranfer_id,
                'status' => $submit_status,
                'cancel_btn' => $cancel_btn,
                'created_date' => $assessment_account['EdtpaAssessments']['created_at'],
                'action' => $action_btn
            );

            array_push($assessment_accounts_final, $assessment_accounts_inner);
        endforeach;
        $result['rows'] = $data;
        echo json_encode($result);
        die;
    }

    function Candidate_change_status($candidate_id) {
        $edtpa_lang = $this->Session->read('edtpa_lang');
        $edtpa_assessment_account_candidate = $this->EdtpaAssessmentAccountCandidates->find('first', array(
            'conditions' => array(                
                'id' => $candidate_id
            )
        ));

        if (count($edtpa_assessment_account_candidate) > 0 && $edtpa_assessment_account_candidate['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'] !='') {
            $edtpa_candidate_auth_key = true;            
        }else{
            $edtpa_candidate_auth_key = false;
        }

       if($edtpa_candidate_auth_key){
            $this->EdtpaAssessmentAccountCandidates->updateAll(array('status' => 2), array('id' => $candidate_id));
       }else{
            $this->EdtpaAssessmentAccountCandidates->updateAll(array('status' => 0), array('id' => $candidate_id));
       }
              
        if ($this->EdtpaAssessmentAccountCandidates->getAffectedRows() > 0) {
            echo json_encode(array('success' => true, 'error' => $edtpa_lang['edtpa_assessment_was_canceld']));
        } else {
            echo json_encode(array('success' => false, 'error' => $edtpa_lang['edtpa_assessment_was_not_canceld']));
        }
        die;
    }

    function Candidate_Transfer_Cancel($candidate_id) {

        $this->EdtpaAssessmentAccountCandidates->deleteAll(array('id' => $candidate_id), false);
        if ($this->EdtpaAssessmentAccountCandidates->getAffectedRows() > 0) {
            $this->EdtpaAssessmentPortfolioCandidateSubmissions->deleteAll(array('edtpa_assessment_account_candidate_id' => $candidate_id), false);
            echo json_encode(array('success' => true));
        } else {
            echo json_encode(array('success' => false));
        }
        die;
    }

    function fetach_teacher_assessments() {

        $view = new View($this, false);
        $user = $this->Session->read('user_current_account');
        $edtpa_lang = $this->Session->read('edtpa_lang');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $student_name = isset($_GET['student_name']) ? $_GET['student_name'] : '';
        $assessment_name = isset($_GET['assessment_name']) ? $_GET['assessment_name'] : '';
        $page = $_GET['page'];
        $rows = $_GET['rows'];
        $sidx = $_GET['sidx'];
        $sord = $_GET['sord'];
        $offset = $page * $rows;
        $limit = $offset - $rows;
        $user_ids = false;
        if (!empty($user) && ($user['users_accounts']['role_id'] == 115)) {
            //get  Coach huddles
            $account_folder_ids = $view->Custom->get_huddle_ids_by_coach($user_id, $account_id);
            // All Coachees huddle

            $coachees = $view->Custom->get_huddle_participants_for_edTPA($account_folder_ids);


            if ($coachees) {
                foreach ($coachees as $row) {
                    $user_ids[] = $row['AccountFolderUser']['user_id'];
                }
            }

            $user_ids = implode(',', $user_ids);
        }


        $total_records = $this->EdtpaAssessments->getEtpaAssessment_for_teacher($account_id, $user_ids, false, false, $sidx, $sord, $student_name, $assessment_name);
        $assessment_accounts = $this->EdtpaAssessments->getEtpaAssessment_for_teacher($account_id, $user_ids, $limit, $offset, $sidx, $sord, $student_name, $assessment_name);
        $assessment_accounts_final = array();
        $result = array(
            "records" => ceil(count($total_records) / 10),
            "page" => 1,
            "total" => count($total_records),
        );
        $data = array();
        foreach ($assessment_accounts as $assessment_account):
            if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && ($assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 1 || $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 0)) {
                $submit_status = $edtpa_lang['edtpa_assessment_started'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 2) {
                $submit_status = $edtpa_lang['edtpa_auth_key_varify'];
                $cancel_btn = false;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 3) {
                $submit_status = $edtpa_lang['edtpa_transfer_varified'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 4) {
                $submit_status = $edtpa_lang['edtpa_transfer_requested'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 5) {
                $submit_status = $edtpa_lang['edtpa_transfer_accepted'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 8) {
                $submit_status = $edtpa_lang['edtpa_success'];
                $cancel_btn = true;
                $is_cancel_local = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 9) {
                $submit_status = $edtpa_lang['edtpa_transfer_failed'];
                $cancel_btn = true;
                $is_cancel_local = true;
            } else {
                $submit_status = 'N/A';
                $cancel_btn = false;
            }

            $tranfer_record = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $assessment_account['EdtpaAssessmentAccountCandidates']['id']), 'order' => array('id' => 'DESC')));

            $class = (isset($cancel_btn) && $cancel_btn == true) ? 'cantEditCls' : '';
            $href = (isset($cancel_btn) && $cancel_btn == true) ? 'javascript:void(0);' : $this->base . '/Edtpa/edtpa_detail_teacher/' . $assessment_account['EdtpaAssessments']['id'] . '/' . $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'] . '/' . $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $href = $this->base . '/Edtpa/edtpa_detail_teacher/' . $assessment_account['EdtpaAssessments']['id'] . '/' . $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'] . '/' . $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            if ($_SESSION['LANG'] == 'es') {
                $assessment_name = "<a class='" . $class . "' href='" . $href . "'>" . $assessment_account['EdtpaAssessments']['assessment_name_es'] . "</a>";
            } else {
                $assessment_name = "<a class='" . $class . "' href='" . $href . "'>" . $assessment_account['EdtpaAssessments']['assessment_name'] . "</a>";
            }
            $tranfer_id = $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['id'];
            $action_btn = '';
            if ($cancel_btn) {
                //$action_btn = '<a href="javascript:void(0);" class="cancleRequest" onlclick="cancelRequest(' . $tranfer_id . ')">Cancel</a>';
            } else {
                //$action_btn = "<a class='" . $class . "' href='". $href . "'>Select </a>";
            }
            $assessment_accounts_inner['EdtpaAssessments']['id'] = $assessment_account['EdtpaAssessments']['id'];
            if ($_SESSION['LANG'] == 'es') {
                $assessment_accounts_inner['EdtpaAssessments']['assessment_name'] = $assessment_account['EdtpaAssessments']['assessment_name_es'];
            } else {
                $assessment_accounts_inner['EdtpaAssessments']['assessment_name'] = $assessment_account['EdtpaAssessments']['assessment_name'];
            }
            $assessment_accounts_inner['EdtpaAssessments']['assessment_version'] = $assessment_account['EdtpaAssessments']['assessment_version'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_id'] = $assessment_account['EdtpaAssessments']['assessment_id'];
            $assessment_accounts_inner['EdtpaAssessments']['project_id'] = $assessment_account['EdtpaAssessments']['project_id'];
            $assessment_accounts_inner['EdtpaAssessments']['created_at'] = $assessment_account['EdtpaAssessments']['created_at'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_type'] = $assessment_account['EdtpaAssessments']['assessment_type'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_xml'] = $assessment_account['EdtpaAssessments']['assessment_xml'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['user_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'] = $assessment_account['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['status'] = $assessment_account['EdtpaAssessmentAccountCandidates']['status'];
            $assessment_accounts_inner['transfer_id'] = $tranfer_id;
            $assessment_accounts_inner['EdtpaAssessments']['status'] = $submit_status;
            $assessment_accounts_inner['EdtpaAssessments']['cancel_btn'] = $cancel_btn;
            $view_assessment_url = 'N/A';
            if ($user['users_accounts']['role_id'] != 115) {

                $view_assessment_url = '<a href=' . $href . '>' . $edtpa_lang['edtpa_view_assessments'] . '</a>';
            } else {
                $view_assessment_url = '<a href="#">' . $edtpa_lang['edtpa_view_assessments'] . '</a>';
            }
            $date_of_transfer = '';
            $time_of_transfer = '';
            if (!empty($tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['file_picked_at'])) {
                $date_of_picked = explode(' ', $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['file_picked_at']);
                $date_of_transfer = !empty($tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['file_picked_at']) ? $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['file_picked_at'] : '00-00-00 00:00:00';
                $time_of_transfer = isset($date_of_picked[1]) ? $date_of_picked[1] : '';
            } else {
                $date_of_transfer = '00-00-00 00:00:00';
                $time_of_transfer = '';
            }
            $data[] = array(
                'assessment_name' => $assessment_name,
                'assessment_version' => $assessment_account['EdtpaAssessments']['assessment_version'],
                'assessment_id' => $assessment_account['EdtpaAssessments']['assessment_id'],
                'project_id' => $assessment_account['EdtpaAssessments']['project_id'],
                'assessment_xml' => $assessment_account['EdtpaAssessments']['assessment_xml'],
                'transfer_id' => $tranfer_id,
                'status' => $submit_status,
                'cancel_btn' => $cancel_btn,
                'created_date' => $assessment_account['EdtpaAssessments']['created_at'],
                'action' => $view_assessment_url,
                'student_name' => $assessment_account['usr']['first_name'] . ' ' . $assessment_account['usr']['last_name'],
                'date_of_transfer' => $date_of_transfer,
                'time_of_transfer' => $time_of_transfer
            );

            array_push($assessment_accounts_final, $assessment_accounts_inner);
        endforeach;
        $result['rows'] = $data;
        echo json_encode($result);
        die;
    }

    function teacher_view() {
        $user_permissions = $this->Session->read('user_permissions');
        if (!isset($user_permissions['accounts']['enable_edtpa']) || !$user_permissions['accounts']['enable_edtpa'] == '1') {
            $this->redirect('/Dashboard/home');
        }

        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $result = $this->EdtpaAssessments->getEtpaAssessment($account_id, $user_id);
        if (!empty($result)) {
            foreach ($result as $single_assessment):
                if ($single_assessment['EdtpaAssessmentAccountCandidates']['status'] == 4) {
                    $tranfer_record = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $single_assessment['EdtpaAssessmentAccountCandidates']['id']), 'order' => array('id' => 'DESC')));
                    $tranfer_id = $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['id'];
                    $status_response = $this->Portfolio_Package_Transfer_Status($tranfer_id, true);
                    $status_response_arr = json_decode($status_response);
                    $status_val = '';
                    if (isset($status_response_arr->partnerTransferRequestStatusList['status'])) {
                        if ($status_response_arr->partnerTransferRequestStatusList['status'] == 'IR_PA_Pending') {
                            $status_val = 6;
                        } else if ($status_response_arr->partnerTransferRequestStatusList['status'] == 'IR_Failed') {
                            $status_val = 7;
                        } else if ($status_response_arr->partnerTransferRequestStatusList['status'] == 'IR_Success') {
                            $status_val = 8;
                        }
                        $this->EdtpaAssessmentAccountCandidates->updateAll(array('status' => $status_val), array('id' => $single_assessment['EdtpaAssessmentAccountCandidates']['id']));
                    }
                }
            endforeach;
        }
        $assessment_accounts = $this->EdtpaAssessments->getEtpaAssessment($account_id, $user_id);
        $assessment_accounts_final = array();
        foreach ($assessment_accounts as $assessment_account):
            $tranfer_record = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $assessment_account['EdtpaAssessmentAccountCandidates']['id']), 'order' => array('id' => 'DESC')));
            $tranfer_id = $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['id'];
            $assessment_accounts_inner['EdtpaAssessments']['id'] = $assessment_account['EdtpaAssessments']['id'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_name'] = $assessment_account['EdtpaAssessments']['assessment_name'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_version'] = $assessment_account['EdtpaAssessments']['assessment_version'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_id'] = $assessment_account['EdtpaAssessments']['assessment_id'];
            $assessment_accounts_inner['EdtpaAssessments']['project_id'] = $assessment_account['EdtpaAssessments']['project_id'];
            $assessment_accounts_inner['EdtpaAssessments']['created_at'] = $assessment_account['EdtpaAssessments']['created_at'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_type'] = $assessment_account['EdtpaAssessments']['assessment_type'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_xml'] = $assessment_account['EdtpaAssessments']['assessment_xml'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['user_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'] = $assessment_account['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['status'] = $assessment_account['EdtpaAssessmentAccountCandidates']['status'];
            $assessment_accounts_inner['transfer_id'] = $tranfer_id;
            array_push($assessment_accounts_final, $assessment_accounts_inner);
        endforeach;

        $this->set('assessment_accounts', $assessment_accounts_final);
        $this->render('teacher_view');
    }

    function ajax_assessment_load($edtpa_assessment_id) {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $assessment_portfolio_tasks = $this->EdtpaAssessmentPortfolios->getAssessmentPortfolioTasks($edtpa_assessment_id, $account_id, $user_id);

        $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
            'conditions' => array(
                'edtpa_assessment_id' => $edtpa_assessment_id,
                'account_id' => $account_id
            )
        ));

        $edtpa_assessment_account_id = $edtpa_assessment_account['EdtpaAssessmentAccounts']['id'];

        $portfolio_nodes_data_arr = array();
        $selected_lang = $_SESSION['LANG'];
        if (!empty($assessment_portfolio_tasks)) {
            foreach ($assessment_portfolio_tasks as $assessment_portfolio_task):

                $attached_documents = $this->EdtpaAssessmentAccountCandidates->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'edtpa_assessment_portfolio_candidate_submissions',
                            'alias' => 'AssessmentAccountCandidateSubmissions',
                            'type' => 'inner',
                            'conditions' => array('EdtpaAssessmentAccountCandidates.id = AssessmentAccountCandidateSubmissions.edtpa_assessment_account_candidate_id')
                        )
                    ),
                    'conditions' => array(
                        'EdtpaAssessmentAccountCandidates.edtpa_assessment_account_id' => $edtpa_assessment_account_id,
                        'EdtpaAssessmentAccountCandidates.user_id' => $user_id,
                        'EdtpaAssessmentAccountCandidates.account_id' => $account_id,
                        'AssessmentAccountCandidateSubmissions.edtpa_assessment_portfolio_node_id' => $assessment_portfolio_task['PortfolioSubNodes']['id']
                    ),
                    'fields' => array(
                        'EdtpaAssessmentAccountCandidates.*',
                        'AssessmentAccountCandidateSubmissions.*'
                    )
                ));

                $document_ids = array();
                if ($attached_documents) {
                    foreach ($attached_documents as $row) {
                        $document_ids[] = $row['AssessmentAccountCandidateSubmissions']['document_id'];
                    }
                }

                $portfolio_nodes_data = array();
                $portfolio_nodes_data['node_id'] = $assessment_portfolio_task['PortfolioSubNodes']['id'];
                $portfolio_nodes_data['assessment_id'] = $assessment_portfolio_task['PortfolioSubNodes']['edtpa_assessment_id'];
                $portfolio_nodes_data['is_unique_label_required'] = $assessment_portfolio_task['PortfolioSubNodes']['is_unique_label_required'];
                $portfolio_nodes_data['is_label_required'] = $assessment_portfolio_task['PortfolioSubNodes']['is_label_required'];
                $portfolio_nodes_data['evidence_type_code'] = $assessment_portfolio_task['PortfolioSubNodes']['evidence_type_code'];
                $portfolio_nodes_data['min_number_of_files'] = $assessment_portfolio_task['PortfolioSubNodes']['min_number_of_files'];
                $portfolio_nodes_data['max_number_of_files'] = $assessment_portfolio_task['PortfolioSubNodes']['max_number_of_files'];
                if ($selected_lang == 'es') {
                    $portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name_es'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name_es'];
                    $portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name_es'];
                    $portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code_es'];
                } else {
                    $portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name'];
                    $portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name'];
                    $portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code'];
                }

                //$portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name'];
                // $portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name'];
                $portfolio_nodes_data['display_order'] = $assessment_portfolio_task['PortfolioSubNodes']['display_order'];
                //$portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code'];
                //$portfolio_nodes_data['documents_arr'] = !empty($document_ids) ? explode(",", $document_ids) : array();
                $portfolio_nodes_data['documents_arr'] = $document_ids;
                $documents = array();
                if (!empty($document_ids)) {
                    foreach ($document_ids as $document_id) {
                        $document_details_arr = $this->Document->getDocumentDetails($document_id);

                        $redirect_url = '';
                        $view = new View($this, false);
                        $document_files_array = $view->Custom->get_document_url($document_details_arr['Document']);

                        if ($document_details_arr['Document']['doc_type'] == '1' || $document_details_arr['Document']['doc_type'] == '3') {
                            if (empty($document_files_array['url'])) {
                                $document_details_arr['Document']['published'] = 0;
                                $document_files_array['url'] = $document_details_arr['Document']['original_file_name'];
                                $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            } else {
                                $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                @$document_details_arr['Document']['duration'] = $document_files_array['duration'];
                            }
                            $thumbnail_image_path = $document_files_array['thumbnail'];
                            if ($document_details_arr['AccountFolders']['folder_type'] == '3') {
                                $redirect_url = $this->base . '/MyFiles/view/1/' . $document_details_arr['AccountFolders']['account_folder_id'];
                            } else {
                                $redirect_url = $this->base . '/Huddles/view/' . $document_details_arr['AccountFolders']['account_folder_id'] . '/1/' . $document_details_arr['Document']['id'];
                            }
                        } else if ($document_details_arr['Document']['doc_type'] == '2') {
                            $view = new View($this, false);
                            $document_icon = $view->Custom->getIcons($document_details_arr['Document']['url']);
                            $thumbnail_image_path = $document_icon;
                            if ($document_details_arr['Document']['stack_url'] != '') {
                                $url_code = explode('/', $document_details_arr['Document']['stack_url']);
                                $url_code = $url_code[3];
                                $redirect_url = $this->base . '/MyFiles/view_document/' . $url_code;
                            } else {
                                $redirect_url = $this->base . '/Huddles/download/' . $document_details_arr['Document']['id'];
                            }
                        }
                        $file_path_info = pathinfo($document_details_arr['Document']['url']);
                        $document_details['document_extension'] = $file_path_info['extension'];
                        $document_details['document_id'] = $document_details_arr['Document']['id'];
                        $document_details['thumbnail'] = $thumbnail_image_path;
                        $document_details['doc_type'] = $document_details_arr['Document']['doc_type']; // 1,3 mean a video and 2 means document
                        $document_details['folder_type'] = $document_details_arr['AccountFolders']['folder_type']; // 1 means a huddle video and 3 means workspace video
                        $document_details['document_title'] = $document_details_arr['afd']['title'];
                        $document_details['document_by'] = $document_details_arr['User']['first_name'] . " " . $document_details_arr['User']['last_name'];
                        $document_details['document_date'] = date('M d, Y', strtotime($document_details_arr['Document']['created_date']));
                        $document_details['redirect_url'] = $redirect_url;
                        array_push($documents, $document_details);
                    }
                }
                $portfolio_nodes_data['documents'] = $documents;
                array_push($portfolio_nodes_data_arr, $portfolio_nodes_data);
            endforeach;
        }

        $result = array(
            'result' => $portfolio_nodes_data_arr
        );
        echo json_encode($result);
        die;
    }

    function edtpa_detail($edtpa_assessment_id) {

        $user_permissions = $this->Session->read('user_permissions');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        if (!isset($user_permissions['accounts']['enable_edtpa']) || !$user_permissions['accounts']['enable_edtpa'] == '1') {
            $this->redirect('/Dashboard/home');
        }
        $added_submission = $this->EdtpaAssessmentAccountCandidates->find('first', array(
            'joins' => array(
                array(
                    'table' => 'edtpa_assessment_accounts as EA',
                    'conditions' => 'EA.id = EdtpaAssessmentAccountCandidates.edtpa_assessment_account_id',
                    'type' => 'inner'
                )
            ),
            'conditions' => array(
                'EdtpaAssessmentAccountCandidates.user_id' => $user_id,
                'EA.account_id' => $account_id,
            ),
            'fields' => array(
                'EA.*'
            ))
        );

        if (count($added_submission) > 0) {
            if ($added_submission['EA']['edtpa_assessment_id'] != $edtpa_assessment_id) {
                $this->Session->setFlash($this->language_based_messages['assessments_cannot_be_deleted_edtpa'], 'default', array('class' => 'message error'));
                $this->redirect('/Edtpa/edtpa');
            }
        }

        $assessment_portfolio_tasks = $this->EdtpaAssessmentPortfolios->getAssessmentPortfolioTasks($edtpa_assessment_id, $account_id, $user_id);

        $edtpa_assessment_handbooks = $this->EdtpaAssessmentsHandbooks->get_assessment_handbook($account_id, $edtpa_assessment_id);


        $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
            'conditions' => array(
                'edtpa_assessment_id' => $edtpa_assessment_id,
                'account_id' => $account_id
            )
        ));


        $this->set('edtpa_assessment_handbooks', $edtpa_assessment_handbooks);

        $this->set('edtpa_assessment_account_id', $edtpa_assessment_account['EdtpaAssessmentAccounts']['id']);
        $edtpa_assessment_account_id = $edtpa_assessment_account['EdtpaAssessmentAccounts']['id'];

        $portfolio_nodes_data_arr = array();
        $selected_lang = $_SESSION['LANG'];
        if (!empty($assessment_portfolio_tasks)) {
            foreach ($assessment_portfolio_tasks as $assessment_portfolio_task):
                $attached_documents = $this->EdtpaAssessmentAccountCandidates->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'edtpa_assessment_portfolio_candidate_submissions',
                            'alias' => 'AssessmentAccountCandidateSubmissions',
                            'type' => 'inner',
                            'conditions' => array(
                                'EdtpaAssessmentAccountCandidates.id = AssessmentAccountCandidateSubmissions.edtpa_assessment_account_candidate_id'
                            )
                        )
                    ),
                    'conditions' => array(
                        'EdtpaAssessmentAccountCandidates.edtpa_assessment_account_id' => $edtpa_assessment_account_id,
                        'EdtpaAssessmentAccountCandidates.user_id' => $user_id,
                        'EdtpaAssessmentAccountCandidates.account_id' => $account_id,
                        'AssessmentAccountCandidateSubmissions.edtpa_assessment_portfolio_node_id' => $assessment_portfolio_task['PortfolioSubNodes']['id']
                    ),
                    'fields' => array(
                        'EdtpaAssessmentAccountCandidates.*',
                        'AssessmentAccountCandidateSubmissions.*'
                    )
                ));
                $document_ids = array();

                if ($attached_documents) {
                    foreach ($attached_documents as $row) {
                        $document_ids[] = $row['AssessmentAccountCandidateSubmissions']['document_id'];
                    }
                }

                $portfolio_nodes_data = array();


                $portfolio_nodes_data['node_id'] = $assessment_portfolio_task['PortfolioSubNodes']['id'];
                $portfolio_nodes_data['assessment_id'] = $assessment_portfolio_task['PortfolioSubNodes']['edtpa_assessment_id'];
                $portfolio_nodes_data['is_unique_label_required'] = $assessment_portfolio_task['PortfolioSubNodes']['is_unique_label_required'];
                $portfolio_nodes_data['is_label_required'] = $assessment_portfolio_task['PortfolioSubNodes']['is_label_required'];
                $portfolio_nodes_data['evidence_type_code'] = $assessment_portfolio_task['PortfolioSubNodes']['evidence_type_code'];
                $portfolio_nodes_data['min_number_of_files'] = $assessment_portfolio_task['PortfolioSubNodes']['min_number_of_files'];
                $portfolio_nodes_data['max_number_of_files'] = $assessment_portfolio_task['PortfolioSubNodes']['max_number_of_files'];
                if ($selected_lang == 'es') {
                    $portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name_es'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name_es'];
                    $portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name_es'];
                    $portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code_es'];
                } else {
                    $portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name'];
                    $portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name'];
                    $portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code'];
                }

                $portfolio_nodes_data['display_order'] = $assessment_portfolio_task['PortfolioSubNodes']['display_order'];
                //$portfolio_nodes_data['documents_arr'] = !empty($document_ids) ? explode(",", $document_ids) : array();
                $portfolio_nodes_data['documents_arr'] = $document_ids;
                $documents = array();

                if (!empty($document_ids)) {
                    //$document_ids_arr = explode(",", $assessment_portfolio_task[0]['documents_ids']);
                    $document_ids_arr = $document_ids;
                    foreach ($document_ids_arr as $document_id) {
                        $document_details_arr = $this->Document->getDocumentDetails($document_id);
                        $redirect_url = '';
                        $view = new View($this, false);
                        $document_files_array = $view->Custom->get_document_url($document_details_arr['Document']);
                        if ($document_details_arr['Document']['doc_type'] == '1' || $document_details_arr['Document']['doc_type'] == '3') {
                            if (empty($document_files_array['url'])) {
                                $document_details_arr['Document']['published'] = 0;
                                $document_files_array['url'] = $document_details_arr['Document']['original_file_name'];
                                $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            } else {
                                $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                @$document_details_arr['Document']['duration'] = $document_files_array['duration'];
                            }
                            $thumbnail_image_path = $document_files_array['thumbnail'];
                            if ($document_details_arr['AccountFolders']['folder_type'] == 3) {

                                $redirect_url = $this->base . '/MyFiles/view/1/' . $document_details_arr['AccountFolders']['account_folder_id'];
                            } else {
                                $redirect_url = $this->base . '/Huddles/view/' . $document_details_arr['AccountFolders']['account_folder_id'] . '/1/' . $document_details_arr['Document']['id'];
                            }
                        } else if ($document_details_arr['Document']['doc_type'] == '2') {
                            $view = new View($this, false);
                            $document_icon = $view->Custom->getIcons($document_details_arr['Document']['url']);
                            $thumbnail_image_path = $document_icon;
                            if ($document_details_arr['Document']['stack_url'] != '') {
                                $url_code = explode('/', $document_details_arr['Document']['stack_url']);
                                $url_code = $url_code[3];
                                $redirect_url = $this->base . '/MyFiles/view_document/' . $url_code;
                            } else {
                                $redirect_url = $this->base . '/Huddles/download/' . $document_details_arr['Document']['id'];
                            }
                        }
                        $view = new View($this, false);
                        $file_path_info = pathinfo($document_details_arr['Document']['url']);
                        $document_details['document_extension'] = $file_path_info['extension'];
                        $document_details['document_id'] = $document_details_arr['Document']['id'];
                        $document_details['thumbnail'] = $thumbnail_image_path;
                        $document_details['doc_type'] = $document_details_arr['Document']['doc_type']; // 1,3 mean a video and 2 means document
                        $document_details['folder_type'] = $document_details_arr['AccountFolders']['folder_type']; // 1 means a huddle video and 3 means workspace video
                        $document_details['document_title'] = $document_details_arr['afd']['title'];
                        $document_details['document_by'] = $document_details_arr['User']['first_name'] . " " . $document_details_arr['User']['last_name'];
                        $document_details['document_date'] = date('M d, Y', strtotime($document_details_arr['Document']['created_date']));
                        $document_details['redirect_url'] = $redirect_url;
                        array_push($documents, $document_details);
                    }
                }
                $portfolio_nodes_data['documents'] = $documents;

                array_push($portfolio_nodes_data_arr, $portfolio_nodes_data);
            endforeach;
        }
        

        $this->set('assessment_portfolio_tasks', $portfolio_nodes_data_arr);
//        $this->set('assessment_portfolio_tasks', $assessment_portfolio_tasks);
        $options = array('conditions' => array('EdtpaAssessments.id' => $edtpa_assessment_id));
        $assessment_details = $this->EdtpaAssessments->find('first', $options);

        $this->set('assessment_details', $assessment_details);
        //Get edtpa_assessment_account_id from edtpa_assessment_accounts table
        //$assessment_account_record = $this->EdtpaAssessmentAccounts->find('first', array('conditions' => array('edtpa_assessment_id' => $edtpa_assessment_id, 'account_id' => $account_id)));
        //if (!empty($assessment_account_record)) {
        //  $assessment_account_id = $assessment_account_record['EdtpaAssessmentAccounts']['id'];
        //}
        $this->set('edtpa_assessment_account_id', $edtpa_assessment_account_id);
        //Workspace videos
//        $MyFilesVideos = $this->Document->getMyFilesVideos($account_id, $user_id, "", "Document.created_date DESC", $limit, 0, '1, 3');
        $MyFilesVideos = $this->Document->getMyFilesVideosEdtpa($account_id, $user_id, "", "Document.created_date DESC", 10, 0, '1, 3');

        $myFiles = false;
        $total_videos_count = $this->Document->getMyFilesVideosEdtpa($account_id, $user_id, "", "Document.created_date DESC", $nolimit, $nopage, '1, 3');
        if ($MyFilesVideos) {
            foreach ($MyFilesVideos as $row) {
                $myFiles[$row['Document']['id']] = $row;
            }
        }

        $this->set('MyFilesVideos', $myFiles);
        $this->set('MyFilesVideosCount', count($total_videos_count));
        //Workspace Documents
//        $MyFilesDocuments = $this->AccountFolder->getAllMyFiles($account_id, $user_id, '2');
        $MyFilesDocuments = $this->Document->getMyFilesVideos($account_id, $user_id, '', '', '', '', 2);


        //echo "<pre>";
        //print_r($myFiles);
        //die;

        $this->set('MyFilesDocuments', $MyFilesDocuments);
        //Huddles Videos and Documents
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }
        $huddles_all = $this->AccountFolder->searchAccountHuddles_textbox($account_id, 'flat_view', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $huddles = $this->AccountFolder->getAllAccountHuddlesEdtpa($account_id, 'AccountFolder.created_date DESC', FALSE, $accountFoldereIds, $accountFolderGroupsIds, false, 0, 10, 0);
        $this->set('HuddlesCount', count($huddles_all));
//        echo '<pre>';
//        print_r($huddles);
//        die;
        $huddles_final_arr = array();
        if (!empty($huddles)) {
            foreach ($huddles as $huddle):
                $huddle_inner_arr = array();
                $huddle_inner_arr['user_id'] = $huddle['User']['id'];
                $huddle_inner_arr['user_name'] = $huddle['User']['username'];
                $huddle_inner_arr['user_email'] = $huddle['User']['email'];
                $huddle_inner_arr['user_first_name'] = $huddle['User']['first_name'];
                $huddle_inner_arr['user_last_name'] = $huddle['User']['last_name'];
                $huddle_inner_arr['AccountFolder_id'] = $huddle['AccountFolder']['account_folder_id'];
                $huddle_inner_arr['AccountFolder_account_id'] = $huddle['AccountFolder']['account_id'];
                $huddle_inner_arr['AccountFolder_folder_type'] = $huddle['AccountFolder']['folder_type'];
                $huddle_inner_arr['AccountFolder_name'] = $huddle['AccountFolder']['name'];
                $huddle_inner_arr['AccountFolder_created_date'] = $huddle['AccountFolder']['created_date'];
                $huddle_inner_arr['AccountFolder_active'] = $huddle['AccountFolder']['active'];
                $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle["AccountFolder"]["account_folder_id"]);

                $huddles_items = $this->Document->getVideos($huddle_id, '', '');

                $huddles_videos_arr = array();

                if (!empty($huddles_items)) {
                    foreach ($huddles_items as $huddles_item):
                        $huddles_videos_inner_arr = array();
                        $view = new View($this, false);
                        $document_files_array = $view->Custom->get_document_url($huddles_item['Document']);

                        if (empty($document_files_array['url'])) {
                            $huddles_item['Document']['published'] = 0;
                            $document_files_array['url'] = $huddles_item['Document']['original_file_name'];
                            $huddles_item['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $huddles_item['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            @$huddles_item['Document']['duration'] = $document_files_array['duration'];
                        }
                        $thumbnail_image_path = $document_files_array['thumbnail'];
                        if ($huddles_item['Document']['doc_type'] == '3') {
                            if ($huddles_item['Document']['published'] == '1' && $huddles_item['Document']['is_processed'] == '4') {
                                $file_path_info = pathinfo($huddles_item['Document']['url']);
                                $huddles_videos_inner_arr['document_extension'] = $file_path_info['extension'];
                                $huddles_videos_inner_arr['video_id'] = $huddles_item['Document']['id'];
                                $huddles_videos_inner_arr['doc_type'] = $huddles_item['Document']['doc_type'];
                                $huddles_videos_inner_arr['video_thumbnail'] = $thumbnail_image_path;
                                $huddles_videos_inner_arr['video_title'] = $huddles_item['afd']['title'];
                                $huddles_videos_inner_arr['video_uploaded_date'] = date('M d, Y', strtotime($huddles_item['Document']['created_date']));
                            }
                        } else {
                            if ($huddles_item['Document']['published'] == '1') {
                                $file_path_info = pathinfo($huddles_item['Document']['url']);
                                $huddles_videos_inner_arr['document_extension'] = $file_path_info['extension'];
                                $huddles_videos_inner_arr['video_id'] = $huddles_item['Document']['id'];
                                $huddles_videos_inner_arr['doc_type'] = $huddles_item['Document']['doc_type'];
                                $huddles_videos_inner_arr['video_thumbnail'] = $thumbnail_image_path;
                                $huddles_videos_inner_arr['video_title'] = $huddles_item['afd']['title'];
                                $huddles_videos_inner_arr['video_uploaded_date'] = date('M d, Y', strtotime($huddles_item['Document']['created_date']));
                            }
                        }
                        if (!empty($huddles_videos_inner_arr))
                            array_push($huddles_videos_arr, $huddles_videos_inner_arr);
                    endforeach;
                }
                $huddle_inner_arr['videos'] = $huddles_videos_arr;
//                if (!empty($huddle_inner_arr['videos']))
                array_push($huddles_final_arr, $huddle_inner_arr);
            endforeach;
        }
    //    echo "<pre>";
    //    print_r($huddles_final_arr);
    //    die;
        $this->set('HuddlesVideos', $huddles_final_arr);


        $huddles_final_arr_docs = array();
        if (!empty($huddles_all)) {
          
            foreach ($huddles_all as $huddle):
                $huddle_inner_arr_docs = array();
                $huddle_inner_arr_docs['user_id'] = $huddle['User']['id'];
                $huddle_inner_arr_docs['user_name'] = $huddle['User']['username'];
                $huddle_inner_arr_docs['user_email'] = $huddle['User']['email'];
                $huddle_inner_arr_docs['user_first_name'] = $huddle['User']['first_name'];
                $huddle_inner_arr_docs['user_last_name'] = $huddle['User']['last_name'];
                $huddle_inner_arr_docs['AccountFolder_id'] = $huddle['AccountFolder']['account_folder_id'];
                $huddle_inner_arr_docs['AccountFolder_account_id'] = $huddle['AccountFolder']['account_id'];
                $huddle_inner_arr_docs['AccountFolder_folder_type'] = $huddle['AccountFolder']['folder_type'];
                $huddle_inner_arr_docs['AccountFolder_name'] = $huddle['AccountFolder']['name'];
                $huddle_inner_arr_docs['AccountFolder_created_date'] = $huddle['AccountFolder']['created_date'];
                $huddle_inner_arr_docs['AccountFolder_active'] = $huddle['AccountFolder']['active'];
                $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle["afmd"]["account_folder_id"]);
                $huddles_documents = $this->Document->getDocuments($huddle_id, '', '', 2);

                $huddles_docs_arr = array();
                if (!empty($huddles_documents)) {
                    foreach ($huddles_documents as $huddles_document):
                        $huddles_docs_inner_arr = array();
                        $file_path_info = pathinfo($huddles_document['Document']['url']);
                        $huddles_docs_inner_arr['document_extension'] = $file_path_info['extension'];
                        $huddles_docs_inner_arr['stack_url'] = $huddles_document['Document']['stack_url'];
                        $huddles_docs_inner_arr['document_id'] = $huddles_document['Document']['id'];
                        $huddles_docs_inner_arr['doc_type'] = $huddles_document['Document']['doc_type'];
                        $huddles_docs_inner_arr['afd_id'] = $huddles_document['afd']['id'];
                        $huddles_docs_inner_arr['afd_title'] = $huddles_document['afd']['title'];
                        $huddles_docs_inner_arr['document_by'] = $huddles_document['User']['first_name'] . " " . $huddles_document['User']['last_name'];
                        $view = new View($this, false);
                        $huddles_docs_inner_arr['document_url'] = $view->Custom->docType($huddles_document['Document']['url']);
                        $huddles_docs_inner_arr['document_icon'] = $view->Custom->getIcons($huddles_document['Document']['url']);
                        $huddles_docs_inner_arr['created_date'] = date('M d, Y', strtotime($huddles_document['Document']['created_date']));
                        if (!empty($huddles_docs_inner_arr))
                            array_push($huddles_docs_arr, $huddles_docs_inner_arr);

                    endforeach;
                }

                $huddle_inner_arr_docs['documents'] = $huddles_docs_arr;
               // if (!empty($huddle_inner_arr_docs['documents']))
                    array_push($huddles_final_arr_docs, $huddle_inner_arr_docs);
            endforeach;
        }
       

        $assessmentID = $assessment_details['EdtpaAssessments']['assessment_id'];
        $assessmentVersion = $assessment_details['EdtpaAssessments']['assessment_version'];
        $edtpa_candidate_auth_key = '';
        $partnerCandidateID = '';


        //$tracker_url = "https://partners.nesinc.com/eportfolio/view/checklist/tracker.seam";
        $tracker_url = "https://cert-edtpa.eportfoliodev.com/eportfolio/view/checklist/tracker.seam";
        $edtpa_provider_name = "Sibme";
        $project_id = "edTPA";
        $edtpa_shared_secret = "z3ol8fvb739u";
        $edtpa_shared_key = "cfb1bb092c907fb8db23456b7c7a5411c4059796f582902e7cd34add57b9064a";
        if (IS_PROD) {
            $tracker_url = "https://edtpa.eportfolio.pearson.com/eportfolio/view/checklist/tracker.seam";
            $edtpa_provider_name = "Sibme";
            $project_id = "edTPA";
            $edtpa_shared_secret = "4*fx1ENebUbf#w6q4nz3TBfRx";
            $edtpa_shared_key = "e557cc4002f5957da1f0e2ed29b4ef12c2497fa721e9ff24b9a225d06d3baf1c";
        }

        //found the assessment
        $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
            'conditions' => array(
                'edtpa_assessment_id' => $edtpa_assessment_id,
                'account_id' => $account_id
            )
        ));

        if (count($edtpa_assessment_account) > 0) {

            $edtpa_assessment_account_candidate = $this->EdtpaAssessmentAccountCandidates->find('first', array(
                'conditions' => array(
                    'user_id' => $user_id,
                    'edtpa_assessment_account_id' => $edtpa_assessment_account['EdtpaAssessmentAccounts']['id']
                )
            ));

            if (count($edtpa_assessment_account_candidate) > 0) {

                $edtpa_candidate_auth_key = $edtpa_assessment_account_candidate['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
                $partnerCandidateID = $edtpa_assessment_account_candidate['EdtpaAssessmentAccountCandidates']['user_id'];
            }
        }

        $epoh_time = strtotime('now');

        $signature = base64_encode(hash_hmac('sha1', utf8_encode($edtpa_provider_name . $project_id . $assessmentID . $assessmentVersion . $partnerCandidateID . $epoh_time), $edtpa_shared_secret, true));

        $tracker_url .= "?partnerID=" . $edtpa_provider_name;
        $tracker_url .= "&assessmentID=" . $assessmentID;
        $tracker_url .= "&projectID=" . $project_id;
        $tracker_url .= "&assessmentVersion=" . $assessmentVersion;

        if (!empty($edtpa_candidate_auth_key))
            $tracker_url .= "&authorizationKey=" . $edtpa_candidate_auth_key;
        $tracker_url .= "&partnerCandidateID=" . $partnerCandidateID;
        $tracker_url .= "&amp;";
        $tracker_url .= "timestamp=" . $epoh_time;
        $tracker_url .= "&signature=" . $signature;


        if (empty($partnerCandidateID))
            $tracker_url = "";
        $user = $this->Session->read('user_current_account');
        $check_users_permissions = $this->UserAccount->find('first', array('conditions' => array('account_id' => $user['users_accounts']['account_id'], 'user_id' => $user['users_accounts']['user_id'])));
    
        $this->set('check_users_permissions',$check_users_permissions);
        $this->set('tracker_url', $tracker_url);
        $this->set('HuddlesDocuments', $huddles_final_arr_docs);
        $this->set('edtpa_assessment_id', $edtpa_assessment_id);
        $view = new View($this, false);
        
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $this->render('edtpa_detail');
    }

    function get_models() {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $MyFilesDocuments = $this->Document->getMyFilesVideos($account_id, $user_id, '', '', '', '', 2);
        $this->set('MyFilesDocuments', $MyFilesDocuments);
        $MyFilesVideos = $this->Document->getMyFilesVideosEdtpa($account_id, $user_id, "", "Document.created_date DESC", 10, 0, '1, 3');
        $total_videos_count = $this->Document->getMyFilesVideosEdtpa($account_id, $user_id, "", "Document.created_date DESC", '', '', '1, 3');
        $this->set('MyFilesVideos', $MyFilesVideos);
        $this->set('MyFilesVideosCount', count($total_videos_count));
        //Huddles Videos and Documents
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }
        $huddles_all = $this->AccountFolder->searchAccountHuddles_textbox($account_id, 'flat_view', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $huddles_final_arr = array();
        if (!empty($huddles)) {
            foreach ($huddles as $huddle):
                $huddle_inner_arr = array();
                $huddle_inner_arr['user_id'] = $huddle['User']['id'];
                $huddle_inner_arr['user_name'] = $huddle['User']['username'];
                $huddle_inner_arr['user_email'] = $huddle['User']['email'];
                $huddle_inner_arr['user_first_name'] = $huddle['User']['first_name'];
                $huddle_inner_arr['user_last_name'] = $huddle['User']['last_name'];
                $huddle_inner_arr['AccountFolder_id'] = $huddle['AccountFolder']['account_folder_id'];
                $huddle_inner_arr['AccountFolder_account_id'] = $huddle['AccountFolder']['account_id'];
                $huddle_inner_arr['AccountFolder_folder_type'] = $huddle['AccountFolder']['folder_type'];
                $huddle_inner_arr['AccountFolder_name'] = $huddle['AccountFolder']['name'];
                $huddle_inner_arr['AccountFolder_created_date'] = $huddle['AccountFolder']['created_date'];
                $huddle_inner_arr['AccountFolder_active'] = $huddle['AccountFolder']['active'];
                $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle["AccountFolder"]["account_folder_id"]);

                $huddles_items = $this->Document->getVideos($huddle_id, '', '');

                $huddles_videos_arr = array();

                if (!empty($huddles_items)) {
                    foreach ($huddles_items as $huddles_item):
                        $huddles_videos_inner_arr = array();
                        $view = new View($this, false);
                        $document_files_array = $view->Custom->get_document_url($huddles_item['Document']);

                        if (empty($document_files_array['url'])) {
                            $huddles_item['Document']['published'] = 0;
                            $document_files_array['url'] = $huddles_item['Document']['original_file_name'];
                            $huddles_item['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $huddles_item['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            @$huddles_item['Document']['duration'] = $document_files_array['duration'];
                        }
                        $thumbnail_image_path = $document_files_array['thumbnail'];
                        if ($huddles_item['Document']['doc_type'] == '3') {
                            if ($huddles_item['Document']['published'] == '1' && $huddles_item['Document']['is_processed'] == '4') {
                                $file_path_info = pathinfo($huddles_item['Document']['url']);
                                $huddles_videos_inner_arr['document_extension'] = $file_path_info['extension'];
                                $huddles_videos_inner_arr['video_id'] = $huddles_item['Document']['id'];
                                $huddles_videos_inner_arr['doc_type'] = $huddles_item['Document']['doc_type'];
                                $huddles_videos_inner_arr['video_thumbnail'] = $thumbnail_image_path;
                                $huddles_videos_inner_arr['video_title'] = $huddles_item['afd']['title'];
                                $huddles_videos_inner_arr['video_uploaded_date'] = date('M d, Y', strtotime($huddles_item['Document']['created_date']));
                            }
                        } else {
                            if ($huddles_item['Document']['published'] == '1') {
                                $file_path_info = pathinfo($huddles_item['Document']['url']);
                                $huddles_videos_inner_arr['document_extension'] = $file_path_info['extension'];
                                $huddles_videos_inner_arr['video_id'] = $huddles_item['Document']['id'];
                                $huddles_videos_inner_arr['doc_type'] = $huddles_item['Document']['doc_type'];
                                $huddles_videos_inner_arr['video_thumbnail'] = $thumbnail_image_path;
                                $huddles_videos_inner_arr['video_title'] = $huddles_item['afd']['title'];
                                $huddles_videos_inner_arr['video_uploaded_date'] = date('M d, Y', strtotime($huddles_item['Document']['created_date']));
                            }
                        }
                        if (!empty($huddles_videos_inner_arr))
                            array_push($huddles_videos_arr, $huddles_videos_inner_arr);
                    endforeach;
                }
                $huddle_inner_arr['videos'] = $huddles_videos_arr;
//                if (!empty($huddle_inner_arr['videos']))
                array_push($huddles_final_arr, $huddle_inner_arr);
            endforeach;
        }
        $this->set('HuddlesVideos', $huddles_final_arr);
        $huddles_final_arr_docs = array();
        if (!empty($huddles_all)) {
            foreach ($huddles_all as $huddle):
                $huddle_inner_arr_docs = array();
                $huddle_inner_arr_docs['user_id'] = $huddle['User']['id'];
                $huddle_inner_arr_docs['user_name'] = $huddle['User']['username'];
                $huddle_inner_arr_docs['user_email'] = $huddle['User']['email'];
                $huddle_inner_arr_docs['user_first_name'] = $huddle['User']['first_name'];
                $huddle_inner_arr_docs['user_last_name'] = $huddle['User']['last_name'];
                $huddle_inner_arr_docs['AccountFolder_id'] = $huddle['AccountFolder']['account_folder_id'];
                $huddle_inner_arr_docs['AccountFolder_account_id'] = $huddle['AccountFolder']['account_id'];
                $huddle_inner_arr_docs['AccountFolder_folder_type'] = $huddle['AccountFolder']['folder_type'];
                $huddle_inner_arr_docs['AccountFolder_name'] = $huddle['AccountFolder']['name'];
                $huddle_inner_arr_docs['AccountFolder_created_date'] = $huddle['AccountFolder']['created_date'];
                $huddle_inner_arr_docs['AccountFolder_active'] = $huddle['AccountFolder']['active'];
                $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle["afmd"]["account_folder_id"]);
                $huddles_documents = $this->Document->getDocuments($huddle_id, '2', '');
                $huddles_docs_arr = array();
                if (!empty($huddles_documents)) {
                    foreach ($huddles_documents as $huddles_document):
                        $huddles_docs_inner_arr = array();
                        $file_path_info = pathinfo($huddles_document['Document']['url']);
                        $huddles_docs_inner_arr['document_extension'] = $file_path_info['extension'];
                        $huddles_docs_inner_arr['stack_url'] = $huddles_document['Document']['stack_url'];
                        $huddles_docs_inner_arr['document_id'] = $huddles_document['Document']['id'];
                        $huddles_docs_inner_arr['doc_type'] = $huddles_document['Document']['doc_type'];
                        $huddles_docs_inner_arr['afd_id'] = $huddles_document['afd']['id'];
                        $huddles_docs_inner_arr['afd_title'] = $huddles_document['afd']['title'];
                        $huddles_docs_inner_arr['document_by'] = $huddles_document['User']['first_name'] . " " . $huddles_document['User']['last_name'];
                        $view = new View($this, false);
                        $huddles_docs_inner_arr['document_url'] = $view->Custom->docType($huddles_document['Document']['url']);
                        $huddles_docs_inner_arr['document_icon'] = $view->Custom->getIcons($huddles_document['Document']['url']);
                        $huddles_docs_inner_arr['created_date'] = date('M d, Y', strtotime($huddles_document['Document']['created_date']));
                        if (!empty($huddles_docs_inner_arr))
                            array_push($huddles_docs_arr, $huddles_docs_inner_arr);

                    endforeach;
                }

                $huddle_inner_arr_docs['documents'] = $huddles_docs_arr;
                if (!empty($huddle_inner_arr_docs['documents']))
                    array_push($huddles_final_arr_docs, $huddle_inner_arr_docs);
            endforeach;
        }
        $this->set('HuddlesDocuments', $huddles_final_arr_docs);
        $view = View($this, false);
        $params = array(
            'HuddlesDocuments' => $huddles_final_arr_docs,
            'HuddlesVideos' => $huddles_final_arr,
            'MyFilesDocuments' => $MyFilesDocuments,
            'MyFilesVideos' => $MyFilesVideos
        );
        $model_html = $view->element('edtpa/models', $params);
    }

    function send_to_edtpa($edtpa_assessment_id) {
        $user_permissions = $this->Session->read('user_permissions');
        if (!isset($user_permissions['accounts']['enable_edtpa']) || !$user_permissions['accounts']['enable_edtpa'] == '1') {
            $this->redirect('/Dashboard/home');
        }
        $total_file_size = 0;
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];

        $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
            'conditions' => array(
                'edtpa_assessment_id' => $edtpa_assessment_id,
                'account_id' => $account_id
            )
        ));

        $edtpa_assessment_account_id = $edtpa_assessment_account['EdtpaAssessmentAccounts']['id'];

        $assessment_portfolio_tasks = $this->EdtpaAssessmentPortfolios->getAssessmentPortfolioTasks($edtpa_assessment_id, $account_id, $user_id);
        $portfolio_nodes_data_arr = array();
        $selected_lang = $_SESSION['LANG'];
        if (!empty($assessment_portfolio_tasks)) {
            foreach ($assessment_portfolio_tasks as $assessment_portfolio_task):
                $portfolio_nodes_data = array();
                $attached_documents = $this->EdtpaAssessmentAccountCandidates->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'edtpa_assessment_portfolio_candidate_submissions',
                            'alias' => 'AssessmentAccountCandidateSubmissions',
                            'type' => 'inner',
                            'conditions' => array('EdtpaAssessmentAccountCandidates.id = AssessmentAccountCandidateSubmissions.edtpa_assessment_account_candidate_id')
                        )
                    ),
                    'conditions' => array(
                        'EdtpaAssessmentAccountCandidates.edtpa_assessment_account_id' => $edtpa_assessment_account_id,
                        'EdtpaAssessmentAccountCandidates.user_id' => $user_id,
                        'EdtpaAssessmentAccountCandidates.account_id' => $account_id,
                        'AssessmentAccountCandidateSubmissions.edtpa_assessment_portfolio_node_id' => $assessment_portfolio_task['PortfolioSubNodes']['id']
                    ),
                    'fields' => array(
                        'EdtpaAssessmentAccountCandidates.*',
                        'AssessmentAccountCandidateSubmissions.*'
                    )
                ));
                $portfolio_nodes_data['node_id'] = $assessment_portfolio_task['PortfolioSubNodes']['id'];
                $portfolio_nodes_data['assessment_id'] = $assessment_portfolio_task['PortfolioSubNodes']['edtpa_assessment_id'];
                $portfolio_nodes_data['is_unique_label_required'] = $assessment_portfolio_task['PortfolioSubNodes']['is_unique_label_required'];
                $portfolio_nodes_data['is_label_required'] = $assessment_portfolio_task['PortfolioSubNodes']['is_label_required'];
                $portfolio_nodes_data['evidence_type_code'] = $assessment_portfolio_task['PortfolioSubNodes']['evidence_type_code'];
                $portfolio_nodes_data['min_number_of_files'] = $assessment_portfolio_task['PortfolioSubNodes']['min_number_of_files'];
                $portfolio_nodes_data['max_number_of_files'] = $assessment_portfolio_task['PortfolioSubNodes']['max_number_of_files'];
                if ($selected_lang == 'es') {
                    $portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name_es'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name_es'];
                    $portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name_es'];
                    $portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code_es'];
                } else {
                    $portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name'];
                    $portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name'];
                    $portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code'];
                }
                //$portfolio_nodes_data['short_name'] = $assessment_portfolio_task['PortfolioSubNodes']['short_name'];
                //$portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name'];
                $portfolio_nodes_data['display_order'] = $assessment_portfolio_task['PortfolioSubNodes']['display_order'];
                //$portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code'];
                $portfolio_nodes_data['documents_arr'] = !empty($assessment_portfolio_task[0]['documents_ids']) ? explode(",", $assessment_portfolio_task[0]['documents_ids']) : array();
                $documents = array();
                $document_ids = array();
                if ($attached_documents) {
                    foreach ($attached_documents as $row) {
                        $document_ids[] = $row['AssessmentAccountCandidateSubmissions']['document_id'];
                    }
                }


                if (!empty($document_ids)) {

                    //  $document_ids_arr = explode(",", $assessment_portfolio_task[0]['documents_ids']);
                    $document_ids_arr = $document_ids;
                    foreach ($document_ids_arr as $document_id) {
                        $document_details_arr = $this->Document->getDocumentDetails($document_id);

                        $view = new View($this, false);
                        $document_files_array = $view->Custom->get_document_url($document_details_arr['Document']);

                        if ($document_details_arr['Document']['doc_type'] == '1' || $document_details_arr['Document']['doc_type'] == '3') {
                            if (empty($document_files_array['url'])) {
                                $document_details_arr['Document']['published'] = 0;
                                $document_files_array['url'] = $document_details_arr['Document']['original_file_name'];
                                $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            } else {
                                $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                @$document_details_arr['Document']['duration'] = $document_files_array['duration'];
                            }
                            $thumbnail_image_path = $document_files_array['thumbnail'];
                        } else if ($document_details_arr['Document']['doc_type'] == '2') {
                            $view = new View($this, false);
                            $document_icon = $view->Custom->getIcons($document_details_arr['Document']['url']);
                            $thumbnail_image_path = $document_icon;
                        }
                        $document_id = $document_details_arr['Document']['id'];
                        $result = $this->DocumentFiles->find("first", array("conditions" => array(
                                "document_id" => $document_id
                            )));
                        
                       if(isset($result['DocumentFiles']['file_size']) && !empty($result['DocumentFiles']['file_size']))
                        {                                
                             $total_file_size = $total_file_size + $result['DocumentFiles']['file_size'];
                        }else{
                            $total_file_size = $total_file_size + $document_details_arr['Document']['file_size'];
                        }                       
                        $file_path_info = pathinfo($document_details_arr['Document']['url']);
                        $document_details['document_extension'] = $file_path_info['extension'];
                        $document_details['document_id'] = $document_details_arr['Document']['id'];
                        $document_details['thumbnail'] = $thumbnail_image_path;
                        $document_details['doc_type'] = $document_details_arr['Document']['doc_type']; // 1,3 mean a video and 2 means document
                        $document_details['folder_type'] = $document_details_arr['AccountFolders']['folder_type']; // 1 means a huddle video and 3 means workspace video
                        $document_details['document_title'] = $document_details_arr['afd']['title'];
                        $document_details['document_by'] = $document_details_arr['User']['first_name'] . " " . $document_details_arr['User']['last_name'];
                        $document_details['document_date'] = date('M d, Y', strtotime($document_details_arr['Document']['created_date']));
                        array_push($documents, $document_details);
                    }
                }
                $portfolio_nodes_data['documents'] = $documents;
                array_push($portfolio_nodes_data_arr, $portfolio_nodes_data);
            endforeach;
        }

        $this->set('assessment_portfolio_tasks', $portfolio_nodes_data_arr);

        $options = array('conditions' => array('EdtpaAssessments.id' => $edtpa_assessment_id));
        $assessment_details = $this->EdtpaAssessments->find('first', $options);
        $this->set('assessment_details', $assessment_details);
        $this->set('edtpa_assessment_id', $edtpa_assessment_id);
        $this->set('user_id', $user_id);
        $this->set('account_id', $account_id);
        $this->set('total_file_size', $total_file_size);
        // $assessment_account_record = $this->EdtpaAssessmentAccounts->find('first', array('conditions' => array('edtpa_assessment_id' => $edtpa_assessment_id, 'account_id' => $account_id)));
        //if (!empty($assessment_account_record)) {
        //  $assessment_account_id = $assessment_account_record['EdtpaAssessmentAccounts']['id'];
        //}
        $this->set('edtpa_assessment_account_id', $edtpa_assessment_account_id);
        $candidate_record = $this->EdtpaAssessmentAccountCandidates->find('first', array('conditions' => array('edtpa_assessment_account_id' => $edtpa_assessment_account_id, 'account_id' => $account_id, 'user_id' => $user_id)));
        if (!empty($candidate_record)) {
            $candidate_id = $candidate_record['EdtpaAssessmentAccountCandidates']['id'];
        }
        $this->set('candidate_id', $candidate_id);
        $assessment_record = $this->EdtpaAssessments->find('first', array('conditions' => array('id' => $edtpa_assessment_id)));
        if (!empty($assessment_record)) {
            $assessment_code = $assessment_record['EdtpaAssessments']['assessment_id'];
            $assessment_version = $assessment_record['EdtpaAssessments']['assessment_version'];
        }


        $assessmentID = $assessment_details['EdtpaAssessments']['assessment_id'];
        $assessmentVersion = $assessment_details['EdtpaAssessments']['assessment_version'];
        $edtpa_candidate_auth_key = '';
        $partnerCandidateID = '';

        //$tracker_url = "https://partners.nesinc.com/eportfolio/view/checklist/tracker.seam";
        $tracker_url = "https://cert-edtpa.eportfoliodev.com/eportfolio/view/checklist/tracker.seam";
        $edtpa_provider_name = "Sibme";
        $project_id = "edTPA";
        $edtpa_shared_secret = "z3ol8fvb739u";
        $edtpa_shared_key = "cfb1bb092c907fb8db23456b7c7a5411c4059796f582902e7cd34add57b9064a";
        if (IS_PROD) {
            $tracker_url = "https://edtpa.eportfolio.pearson.com/eportfolio/view/checklist/tracker.seam";
            $edtpa_provider_name = "Sibme";
            $project_id = "edTPA";
            $edtpa_shared_secret = "4*fx1ENebUbf#w6q4nz3TBfRx";
            $edtpa_shared_key = "e557cc4002f5957da1f0e2ed29b4ef12c2497fa721e9ff24b9a225d06d3baf1c";
        }



        //found the assessment
        $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
            'conditions' => array(
                'edtpa_assessment_id' => $edtpa_assessment_id,
                'account_id' => $account_id
            )
        ));

        if (count($edtpa_assessment_account) > 0) {

            $edtpa_assessment_account_candidate = $this->EdtpaAssessmentAccountCandidates->find('first', array(
                'conditions' => array(
                    'user_id' => $user_id,
                    'edtpa_assessment_account_id' => $edtpa_assessment_account['EdtpaAssessmentAccounts']['id']
                )
            ));

            if (count($edtpa_assessment_account_candidate) > 0) {

                $edtpa_candidate_auth_key = $edtpa_assessment_account_candidate['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
                $partnerCandidateID = $edtpa_assessment_account_candidate['EdtpaAssessmentAccountCandidates']['id'];
            }
        }

        $epoh_time = strtotime('now');

        $signature = base64_encode(hash_hmac('sha1', utf8_encode($edtpa_provider_name . $project_id . $assessmentID . $assessmentVersion . $partnerCandidateID . $epoh_time), $edtpa_shared_secret, true));

        $tracker_url .= "?partnerID=" . $edtpa_provider_name;
        $tracker_url .= "&assessmentID=" . $assessmentID;
        $tracker_url .= "&projectID=" . $project_id;
        $tracker_url .= "&assessmentVersion=" . $assessmentVersion;

        if (!empty($edtpa_candidate_auth_key))
            $tracker_url .= "&authorizationKey=" . $edtpa_candidate_auth_key;

        $tracker_url .= "&partnerCandidateID=" . $partnerCandidateID;
        $tracker_url .= "&amp;";
        $tracker_url .= "timestamp=" . $epoh_time;
        $tracker_url .= "&signature=" . $signature;


        if (empty($partnerCandidateID))
            $tracker_url = "";

        $this->set('tracker_url', $tracker_url);


        $this->set('assessment_code', $assessment_code);
        $this->set('assessment_version', $assessment_version);
        $this->set('candidate_details', $candidate_record);
    }

    function save_selected_files() {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        if ($this->request->is('post')) {
            $assessment_account_id = $this->request->data('assessment_account_id');
            $selected_files = $this->request->data('selected_files');
            $node_id = $this->request->data('node_id');
            $candidate_record = $this->EdtpaAssessmentAccountCandidates->find('first', array('conditions' => array('edtpa_assessment_account_id' => $assessment_account_id, 'account_id' => $account_id, 'user_id' => $user_id)));
            $record_id = '';
            if (empty($candidate_record)) {
                $this->EdtpaAssessmentAccountCandidates->create();
                $data = array(
                    'edtpa_assessment_account_id' => $assessment_account_id,
                    'user_id' => $user_id,
                    'account_id' => $account_id
                );
                $this->EdtpaAssessmentAccountCandidates->save($data);
                $record_id = $this->EdtpaAssessmentAccountCandidates->getLastInsertID();
            } else {
                $record_id = $candidate_record['EdtpaAssessmentAccountCandidates']['id'];
            }

            $this->EdtpaAssessmentPortfolioCandidateSubmissions->deleteAll(array('edtpa_assessment_account_candidate_id' => $record_id, 'edtpa_assessment_portfolio_node_id' => $node_id), false);
            if (!empty($selected_files)) {
                foreach ($selected_files as $selected_file):
                    $check_doc_record = $this->EdtpaAssessmentPortfolioCandidateSubmissions->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $record_id, 'document_id' => $selected_file, 'edtpa_assessment_portfolio_node_id' => $node_id)));
                    if (empty($check_doc_record)) {
                        $this->EdtpaAssessmentPortfolioCandidateSubmissions->create();
                        $data = array(
                            'edtpa_assessment_account_candidate_id' => $record_id,
                            'document_id' => $selected_file,
                            'edtpa_assessment_portfolio_node_id' => $node_id,
                            'created_at' => date('Y-m-d h:i:s'),
                            'created_by' => $user_id
                        );
                        $this->EdtpaAssessmentPortfolioCandidateSubmissions->save($data);
                    }
                endforeach;
            }
            if ($record_id != '') {
                $total_submission_count = $this->EdtpaAssessmentPortfolioCandidateSubmissions->find('count', array(
                    'conditions' => array(
                        'edtpa_assessment_account_candidate_id' => $record_id
                    ))
                );
            } else {
                $total_submission_count = false;
            }

            $result['status'] = true;
            $result['total_submissions'] = $total_submission_count;
            $result['message'] = $this->language_based_messages['changes_saved_successfully_edtpa'];
            echo json_encode($result);
            die;
        }
    }

    public function update_candidate_record() {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        if ($this->request->is('post')) {
            $candidate_id = $this->request->data('candidate_id');
            $auth_key = $this->request->data('auth_key');
            $data = array('edtpa_candidate_auth_key' => "'" . trim($auth_key) . "'", 'auth_key_reg_at' => "'" . date('Y-m-d h:i:s') . "'", 'status' => 2);
            $this->EdtpaAssessmentAccountCandidates->updateAll($data, array('id' => $candidate_id));
            if ($this->EdtpaAssessmentAccountCandidates->getAffectedRows() > 0) {
                $result['status'] = true;
                $result['message'] = 'success';
            } else {
                $result['status'] = false;
                $result['message'] = 'failure';
            }
        } else {
            $result['status'] = false;
            $result['message'] = 'failure';
        }
        echo json_encode($result);
        die;
    }

    public function request_edtpa_submission() {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        if ($this->request->is('post')) {
            $candidate_id = $this->request->data('candidate_id');
            $this->EdtpaAssessmentAccountCandidateTransfers->create();
            $data = array(
                'edtpa_assessment_account_candidate_id' => $candidate_id,
                'created_at' => date('Y-m-d h:i:s'),
                'created_by' => $user_id
            );
            if ($this->EdtpaAssessmentAccountCandidateTransfers->save($data)) {
                $data1 = array('status' => "3");
                $transfer_id = $this->EdtpaAssessmentAccountCandidateTransfers->id;
                $this->EdtpaAssessmentAccountCandidates->updateAll($data1, array('id' => $candidate_id));
                $add_job = $this->job_queue_edtpa_submission_function($transfer_id, '5');
                if ($add_job) {
                    $result['status'] = true;
                    $result['message'] = 'success';
                } else {
                    $result['status'] = false;
                    $result['message'] = 'failure';
                }
            } else {
                $result['status'] = false;
                $result['message'] = 'failure';
            }
        } else {
            $result['status'] = false;
            $result['message'] = 'failure';
        }
        echo json_encode($result);
        die;
    }

    function download($id, $tab = '') {
        if ($id) {
            parent::download($id);
        } else {
            $this->Session->setFlash($this->language_based_messages['please_select_at_least_one_file_edtpa'], 'default', array('class' => 'message error'));
            $this->redirect('/MyFiles/view/' . $tab);
        }
    }

    function loadMoreVideosEdtpMyFiles($title = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $sort = 'Document.created_date DESC';
        $limit = 10;
        $page = $this->request->data('page_num');
        $page = (int) $page;
        $items = $this->Document->getMyFilesVideosEdtpa($account_id, $user_id, $title, $sort, $limit, $page, '1,3');
        $new_page = $page + 1;

        $saved_files_arr = array();
        if ($this->request->is('post')) {
            $node_id = $this->request->data('node_id');
            $assessment_account_id = $this->request->data('assessment_account_id');
            $candidate_record = $this->EdtpaAssessmentAccountCandidates->find('first', array('conditions' => array('edtpa_assessment_account_id' => $assessment_account_id, 'account_id' => $account_id, 'user_id' => $user_id)));
            if (!empty($candidate_record)) {
                $record_id = $candidate_record['EdtpaAssessmentAccountCandidates']['id'];
            }
            $get_saved_records = array();
            if (!empty($record_id)) {
                $get_saved_records = $this->EdtpaAssessmentPortfolioCandidateSubmissions->find('all', array('conditions' => array('edtpa_assessment_account_candidate_id' => $record_id, 'edtpa_assessment_portfolio_node_id' => $node_id)));
            }
            if (!empty($get_saved_records)) {
                foreach ($get_saved_records as $get_saved_record):
                    array_push($saved_files_arr, $get_saved_record['EdtpaAssessmentPortfolioCandidateSubmissions']['document_id']);
                endforeach;
            }
        }
        $html = '';
        if (!empty($items)) {
            foreach ($items as $MyFilesVideo):
                $view = new View($this, false);
                $document_files_array = $view->Custom->get_document_url($MyFilesVideo['Document']);

                if (empty($document_files_array['url'])) {
                    $MyFilesVideo['Document']['published'] = 0;
                    $document_files_array['url'] = $MyFilesVideo['Document']['original_file_name'];
                    $MyFilesVideo['Document']['encoder_status'] = $document_files_array['encoder_status'];
                } else {
                    $MyFilesVideo['Document']['encoder_status'] = $document_files_array['encoder_status'];
                    @$MyFilesVideo['Document']['duration'] = $document_files_array['duration'];
                }
                $video_id = $MyFilesVideo['Document']['id'];
                if (in_array($video_id, $saved_files_arr)) {
                    $checked_value = "checked";
                } else {
                    $checked_value = "";
                }
                $thumbnail_image_path = $document_files_array['thumbnail'];
                $file_path_info = pathinfo($MyFilesVideo['Document']['url']);
                $document_extension = $file_path_info['extension'];
                $html .= '<div class="adt_thumb3">';
                $html .= '<div class="adt_video3" onclick="window.open(' . $this->base . '/MyFiles/view/1/' . $MyFilesVideo['AccountFolder']['account_folder_id'] . ');">';
                $html .= '<img src="' . $thumbnail_image_path . '">';
                $html .= '<div class="play-icon" style="background-size: 50px;height: 50px;margin: 48px 0 0 -30px;position: absolute;left: 11%;width: 50px;"></div>';
                $html .= '</div>';
                $html .= '<div class="adt_desc3" onclick="window.open(' . $this->base . '/MyFiles/view/1/' . $MyFilesVideo['AccountFolder']['account_folder_id'] . ');">';
                $html .= '<h4>' . (strlen($MyFilesVideo['afd']['title']) > 25 ? substr($MyFilesVideo['afd']['title'], 0, 25) . "..." : $MyFilesVideo['afd']['title']) . '</h4>';
                $html .= '<p>By <a href="javascript:void(0);">' . $MyFilesVideo['User']['first_name'] . " " . $MyFilesVideo['User']['last_name'] . '</a></p>';
                $html .= '<p>' . date('M d, Y', strtotime($MyFilesVideo['Document']['created_date'])) . '</p>';
                $html .= '</div>';
                $html .= '<div class="edt_select">';
                $html .= '<span> <input type="checkbox" data-document_extension="' . $document_extension . '" data-thumbnail="' . $thumbnail_image_path . '" data-doc_type="' . $MyFilesVideo['Document']['doc_type'] . '" data-document_by="' . $MyFilesVideo['User']['first_name'] . " " . $MyFilesVideo['User']['last_name'] . '" data-document_date="' . date('M d, Y', strtotime($MyFilesVideo['Document']['created_date'])) . '" data-document_id="' . $MyFilesVideo['Document']['id'] . '" data-document_title="' . (strlen($MyFilesVideo['afd']['title']) > 25 ? substr($MyFilesVideo['afd']['title'], 0, 25) . "..." : $MyFilesVideo['afd']['title']) . '" data-folder_type="' . $MyFilesVideo['AccountFolder']['folder_type'] . '" class="select_checkbox" ' . $checked_value . ' id="select_' . $MyFilesVideo['Document']['id'] . '" name="selected_videos[]" value="' . $MyFilesVideo['Document']['id'] . '"> <label for="select_' . $MyFilesVideo['Document']['id'] . '">Select</label> </span>';
                $html .= '</div>';
                $html .= '<div class="clear"></div>';
                $html .= '</div>';
            endforeach;
        }
        if (count($items) < 10)
            $load_more = false;
        else
            $load_more = true;
        $result['status'] = true;
        $result['new_page_num'] = $new_page;
        $result['load_more'] = $load_more;
        $result['new_count'] = count($items);
        $result['html'] = $html;
        echo json_encode($result);
        exit;
    }

    function loadMoreVideosEdtpHuddles($title = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $sort = 'AccountFolder.created_date DESC';
        $limit = 5;
        $page = $this->request->data('page_num');
        $page = (int) $page;
        $new_page = $page + 1;

        $saved_files_arr = array();
        if ($this->request->is('post')) {
            $node_id = $this->request->data('node_id');
            $assessment_account_id = $this->request->data('assessment_account_id');
            $candidate_record = $this->EdtpaAssessmentAccountCandidates->find('first', array('conditions' => array('edtpa_assessment_account_id' => $assessment_account_id, 'account_id' => $account_id, 'user_id' => $user_id)));
            if (!empty($candidate_record)) {
                $record_id = $candidate_record['EdtpaAssessmentAccountCandidates']['id'];
            }
            $get_saved_records = array();
            if (!empty($record_id)) {
                $get_saved_records = $this->EdtpaAssessmentPortfolioCandidateSubmissions->find('all', array('conditions' => array('edtpa_assessment_account_candidate_id' => $record_id, 'edtpa_assessment_portfolio_node_id' => $node_id)));
            }
            if (!empty($get_saved_records)) {
                foreach ($get_saved_records as $get_saved_record):
                    array_push($saved_files_arr, $get_saved_record['EdtpaAssessmentPortfolioCandidateSubmissions']['document_id']);
                endforeach;
            }
        }
        $html = '';
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }
        $huddles = $this->AccountFolder->getAllAccountHuddlesEdtpa($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, false, 0, $limit, $page);

        $huddles_final_arr = array();
        if (!empty($huddles)) {
            foreach ($huddles as $huddle):

                $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle["AccountFolder"]["account_folder_id"]);
                $html .= '<div class="edtp-heading">' . $huddle['AccountFolder']['name'] . '
                                <div class="arrow-upe"></div>
                                <div class="arrow-downe"></div>
                            </div>';
                $html .= '<div class="edtp-content" style="display:none">
                                <div class="edtp_col">';

                $huddles_items = $this->Document->getVideos($huddle_id, '', '', 10);
                if (!empty($huddles_items)) {
                    foreach ($huddles_items as $huddles_item):
                        $view = new View($this, false);
                        $document_files_array = $view->Custom->get_document_url($huddles_item['Document']);
                        if (empty($document_files_array['url'])) {
                            $huddles_item['Document']['published'] = 0;
                            $document_files_array['url'] = $huddles_item['Document']['original_file_name'];
                            $huddles_item['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $huddles_item['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            @$huddles_item['Document']['duration'] = $document_files_array['duration'];
                        }
                        $thumbnail_image_path = $document_files_array['thumbnail'];
                        if ($huddle['AccountFolder']['folder_type'] == 1) {
                            $url_type_part = 1;
                        } else {
                            $url_type_part = '';
                        }
                        $videoID = $huddles_item['Document']['id'];
                        $huddleID = $huddle['AccountFolder']['account_folder_id'];
                        if (in_array($videoID, $saved_files_arr)) {
                            $checked_value = "checked";
                        } else {
                            $checked_value = "";
                        }
                        $file_path_info = pathinfo($huddles_item['Document']['url']);
                        $document_extension = $file_path_info['extension'];
                        if ($huddles_item['Document']['doc_type'] == '3') {
                            if ($huddles_item['Document']['published'] == '1' && $huddles_item['Document']['is_processed'] == '4') {
                                $html .= '<div class="adt_thumb3">';
                                $html .= '<div class="adt_video3" onclick="window.open(' . $this->base . '/Huddles/view/' . "$huddleID" . '/' . "$url_type_part/$videoID" . ');">';
                                $html .= '<img src="' . $thumbnail_image_path . '">';
                                $html .= '<div class="play-icon" style="background-size: 50px;height: 50px;margin: 48px 0 0 -30px;position: absolute;left: 11%;width: 50px;"></div>';
                                $html .= '</div>';
                                $html .= '<div class="adt_desc3" onclick="window.open(' . $this->base . '/Huddles/view/' . "/$url_type_part/$videoID" . ');">';
                                $html .= '<h4>' . (strlen($huddles_item['afd']['title']) > 25 ? substr($huddles_item['afd']['title'], 0, 25) . "..." : $huddles_item['afd']['title']) . '</h4>';
                                $html .= '<p>By <a href="javascript:void(0);">' . $huddle['User']['first_name'] . " " . $huddle['User']['last_name'] . '</a></p>';
                                $html .= '<p>Uploaded ' . date('M d, Y', strtotime($huddles_item['Document']['created_date'])) . '</p>';
                                $html .= '</div>';
                                $html .= '<div class="edt_select">';
                                $html .= '<span> <input type="checkbox" data-document_extension="' . $document_extension . '" data-thumbnail="' . $thumbnail_image_path . '" data-doc_type="' . $huddles_item['Document']['doc_type'] . '" data-document_by="' . $huddle['User']['first_name'] . " " . $huddle['User']['last_name'] . '" data-document_date="' . date('M d, Y', strtotime($huddles_item['Document']['created_date'])) . '" data-document_id="' . $huddles_item['Document']['id'] . '" data-document_title="' . (strlen($huddles_item['afd']['title']) > 25 ? substr($huddles_item['afd']['title'], 0, 25) . "..." : $huddles_item['afd']['title']) . '" data-folder_type="' . $huddle['AccountFolder']['folder_type'] . '" class="select_checkbox" ' . $checked_value . ' id="select_' . $huddles_item['Document']['id'] . '" name="selected_videos[]" value="' . $huddles_item['Document']['id'] . '"> <label for="select_' . $huddles_item['Document']['id'] . '">Select</label> </span>';
                                $html .= '</div>';
                                $html .= '<div class="clear"></div>';
                                $html .= '</div>';
                            }
                        } else {
                            if ($huddles_item['Document']['published'] == '1') {
                                $html .= '<div class="adt_thumb3">';
                                $html .= '<div class="adt_video3" onclick="window.open(' . $this->base . '/Huddles/view/' . "$huddleID" . '/' . "$url_type_part/$videoID" . ');">';
                                $html .= '<img src="' . $thumbnail_image_path . '">';
                                $html .= '<div class="play-icon" style="background-size: 50px;height: 50px;margin: 48px 0 0 -30px;position: absolute;left: 11%;width: 50px;"></div>';
                                $html .= '</div>';
                                $html .= '<div class="adt_desc3" onclick="window.open(' . $this->base . '/Huddles/view/' . "/$url_type_part/$videoID" . ');">';
                                $html .= '<h4>' . (strlen($huddles_item['afd']['title']) > 25 ? substr($huddles_item['afd']['title'], 0, 25) . "..." : $huddles_item['afd']['title']) . '</h4>';
                                $html .= '<p>By <a href="javascript:void(0);">' . $huddle['User']['first_name'] . " " . $huddle['User']['last_name'] . '</a></p>';
                                $html .= '<p>Uploaded ' . date('M d, Y', strtotime($huddles_item['Document']['created_date'])) . '</p>';
                                $html .= '</div>';
                                $html .= '<div class="edt_select">';
                                $html .= '<span> <input type="checkbox" data-document_extension="' . $document_extension . '"  data-thumbnail="' . $thumbnail_image_path . '" data-doc_type="' . $huddles_item['Document']['doc_type'] . '" data-document_by="' . $huddle['User']['first_name'] . " " . $huddle['User']['last_name'] . '" data-document_date="' . date('M d, Y', strtotime($huddles_item['Document']['created_date'])) . '" data-document_id="' . $huddles_item['Document']['id'] . '" data-document_title="' . (strlen($huddles_item['afd']['title']) > 25 ? substr($huddles_item['afd']['title'], 0, 25) . "..." : $huddles_item['afd']['title']) . '" data-folder_type="' . $huddle['AccountFolder']['folder_type'] . '" class="select_checkbox" id="select_' . $huddles_item['Document']['id'] . '" name="selected_videos[]" value="' . $huddles_item['Document']['id'] . '"> <label for="select_' . $huddles_item['Document']['id'] . '">Select</label> </span>';
                                $html .= '</div>';
                                $html .= '<div class="clear"></div>';
                                $html .= '</div>';
                            }
                        }
                    endforeach;
                } else {
                    $html .= '<p class="noDataCls">There are no videos in this huddle.</p>';
                }
                $html .= '</div>
                                </div>';
            endforeach;
        }
        if (count($huddles) < 5)
            $load_more = false;
        else
            $load_more = true;
        $result['status'] = true;
        $result['new_page_num'] = $new_page;
        $result['load_more'] = $load_more;
        $result['html'] = $html;
        echo json_encode($result);
        exit;
    }

    function refresh_tab() {
        $this->Session->write('selected_node', $this->request->data('node_id'));
        die;
    }

    function edtpa_detail_teacher($edtpa_assessment_id, $account_id, $student_id) {
        $user_permissions = $this->Session->read('user_permissions');
        if (!isset($user_permissions['accounts']['enable_edtpa']) || !$user_permissions['accounts']['enable_edtpa'] == '1') {
            $this->redirect('/Dashboard/home');
        }
        $user = $this->Session->read('user_current_account');
        //$account_id = $user['accounts']['account_id'];
        $user_id = $student_id;

        $role_id = $user['users_accounts']['role_id'];
        $assessment_portfolio_tasks = $this->EdtpaAssessmentPortfolios->getAssessmentPortfolioTasks($edtpa_assessment_id, $account_id, $user_id);
        $edtpa_assessment_handbooks = $this->EdtpaAssessmentsHandbooks->get_assessment_handbook($account_id, $edtpa_assessment_id);
        $this->set('edtpa_assessment_handbooks', $edtpa_assessment_handbooks);

        $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
            'conditions' => array(
                'edtpa_assessment_id' => $edtpa_assessment_id,
                'account_id' => $account_id
            )
        ));

        $edtpa_assessment_account_id = $edtpa_assessment_account['EdtpaAssessmentAccounts']['id'];


        $portfolio_nodes_data_arr = array();
        $documents = array();
        $selected_lang = $_SESSION['LANG'];
        if (!empty($assessment_portfolio_tasks)) {
            foreach ($assessment_portfolio_tasks as $assessment_portfolio_task):
                $portfolio_nodes_data = array();
                $attached_documents = $this->EdtpaAssessmentAccountCandidates->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'edtpa_assessment_portfolio_candidate_submissions',
                            'alias' => 'AssessmentAccountCandidateSubmissions',
                            'type' => 'inner',
                            'conditions' => array('EdtpaAssessmentAccountCandidates.id = AssessmentAccountCandidateSubmissions.edtpa_assessment_account_candidate_id')
                        )
                    ),
                    'conditions' => array(
                        'EdtpaAssessmentAccountCandidates.edtpa_assessment_account_id' => $edtpa_assessment_account_id,
                        'EdtpaAssessmentAccountCandidates.user_id' => $user_id,
                        'EdtpaAssessmentAccountCandidates.account_id' => $account_id,
                        'AssessmentAccountCandidateSubmissions.edtpa_assessment_portfolio_node_id' => $assessment_portfolio_task['PortfolioSubNodes']['id']
                    ),
                    'fields' => array(
                        'EdtpaAssessmentAccountCandidates.*',
                        'AssessmentAccountCandidateSubmissions.*'
                    )
                ));
                $document_ids = array();
                if ($attached_documents) {
                    foreach ($attached_documents as $row) {
                        $document_ids[] = $row['AssessmentAccountCandidateSubmissions']['document_id'];
                    }
                }
				$portfolio_nodes_data = array();
                $portfolio_nodes_data['node_id'] = $assessment_portfolio_task['PortfolioSubNodes']['id'];
                $portfolio_nodes_data['assessment_id'] = $assessment_portfolio_task['PortfolioSubNodes']['edtpa_assessment_id'];
                $portfolio_nodes_data['is_unique_label_required'] = $assessment_portfolio_task['PortfolioSubNodes']['is_unique_label_required'];
                $portfolio_nodes_data['is_label_required'] = $assessment_portfolio_task['PortfolioSubNodes']['is_label_required'];
                $portfolio_nodes_data['evidence_type_code'] = $assessment_portfolio_task['PortfolioSubNodes']['evidence_type_code'];
                $portfolio_nodes_data['min_number_of_files'] = $assessment_portfolio_task['PortfolioSubNodes']['min_number_of_files'];
                $portfolio_nodes_data['max_number_of_files'] = $assessment_portfolio_task['PortfolioSubNodes']['max_number_of_files'];
                if ($selected_lang == 'es') {
                    $portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name_es'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name_es'];
                    $portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name_es'];
                    $portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code_es'];
                } else {
                    $portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name'];
                    $portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name'];
                    $portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code'];
                }
                //$portfolio_nodes_data['short_name'] = $assessment_portfolio_task['EdtpaAssessmentPortfolios']['short_name'] . ' - ' . $assessment_portfolio_task['PortfolioSubNodes']['short_name'];
                //$portfolio_nodes_data['long_name'] = $assessment_portfolio_task['PortfolioSubNodes']['long_name'];
                $portfolio_nodes_data['display_order'] = $assessment_portfolio_task['PortfolioSubNodes']['display_order'];
                //$portfolio_nodes_data['code'] = $assessment_portfolio_task['PortfolioSubNodes']['code'];
                //$portfolio_nodes_data['documents_arr'] = !empty($document_ids) ? $document_ids : array();
				$portfolio_nodes_data['documents_arr'] = $document_ids;
                $documents = array();
                if (!empty($document_ids)) {

                    $document_ids_arr = $document_ids;
                    foreach ($document_ids_arr as $document_id) {
                        $document_details_arr = $this->Document->getDocumentDetails($document_id);
                        $view = new View($this, false);
                        $document_files_array = $view->Custom->get_document_url($document_details_arr['Document']);
                        if (empty($document_files_array['url'])) {
                            $document_details_arr['Document']['published'] = 0;
                            $document_files_array['url'] = $videoDetail['Document']['original_file_name'];
                            $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        }


                        $videoFilePath = pathinfo($document_files_array['url']);
                        $videoFileName = $videoFilePath['filename'];
                        $thumbnail_image_path = $document_files_array['thumbnail'];
                        $video_path = $document_files_array['url'];

                        $redirect_url = '';
                        if ($document_details_arr['Document']['doc_type'] == '1' || $document_details_arr['Document']['doc_type'] == '3') {

                            if (empty($document_files_array['url'])) {
                                $document_details_arr['Document']['published'] = 0;
                                $document_files_array['url'] = $document_details_arr['Document']['original_file_name'];
                                $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            } else {
                                $document_details_arr['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                @$document_details_arr['Document']['duration'] = $document_files_array['duration'];
                            }
                            $thumbnail_image_path = $document_files_array['thumbnail'];

                            if ($document_details_arr['AccountFolders']['folder_type'] == 1) {
                                $redirect_url = $this->base . '/Huddles/view/' . $document_details_arr['AccountFolders']['account_folder_id'] . '/1/' . $document_details_arr['Document']['id'];
                            } else {

                                $redirect_url = $this->base . '/MyFiles/view/1/' . $document_details_arr['AccountFolders']['account_folder_id'];
                            }
                        } else if ($document_details_arr['Document']['doc_type'] == '2') {
                            $view = new View($this, false);
                            $document_icon = $view->Custom->getIcons($document_details_arr['Document']['url']);
                            $thumbnail_image_path = $document_icon;
                            if ($document_details_arr['Document']['stack_url'] != '') {
                                $url_code = explode('/', $document_details_arr['Document']['stack_url']);
                                $url_code = $url_code[3];
                                if ($role_id == '115') {
                                    $redirect_url = false;
                                } else {
                                    $redirect_url = $this->base . '/MyFiles/view_document/' . $url_code;
                                }
                            } else {
                                if ($role_id == '115') {
                                    $redirect_url = false;
                                } else #
                                    $redirect_url = $this->base . '/Huddles/download/' . $document_details_arr['Document']['id'];
                            }
                        }
                    
                    $file_path_info = pathinfo($document_details_arr['Document']['url']);
                    $document_details['document_extension'] = $file_path_info['extension'];
                    $document_details['document_id'] = $document_details_arr['Document']['id'];
                    $document_details['thumbnail'] = $thumbnail_image_path;
                    $document_details['doc_type'] = $document_details_arr['Document']['doc_type']; // 1,3 mean a video and 2 means document
                    $document_details['folder_type'] = $document_details_arr['AccountFolders']['folder_type']; // 1 means a huddle video and 3 means workspace video
                    $document_details['document_title'] = $document_details_arr['afd']['title'];
                    $document_details['document_by'] = $document_details_arr['User']['first_name'] . " " . $document_details_arr['User']['last_name'];
                    $document_details['document_date'] = date('M d, Y', strtotime($document_details_arr['Document']['created_date']));
                    $document_details['redirect_url'] = $redirect_url;
                    if ($role_id == '115') {
                        $document_details['video_url'] = false;
                    } else {
                        $document_details['video_url'] = $video_path;
                    }
                    //$documents[] = $document_details;
                     array_push($documents, $document_details);
					}
				}
                $portfolio_nodes_data['documents'] = $documents;
                array_push($portfolio_nodes_data_arr, $portfolio_nodes_data);
            endforeach;
        }		
       
        $this->set('assessment_portfolio_tasks', $portfolio_nodes_data_arr);
//        $this->set('assessment_portfolio_tasks', $assessment_portfolio_tasks);
        $options = array('conditions' => array('EdtpaAssessments.id' => $edtpa_assessment_id));
        $assessment_details = $this->EdtpaAssessments->find('first', $options);
        $this->set('assessment_details', $assessment_details);
        //Get edtpa_assessment_account_id from edtpa_assessment_accounts table
        //$assessment_account_record = $this->EdtpaAssessmentAccounts->find('first', array('conditions' => array('edtpa_assessment_id' => $edtpa_assessment_id, 'account_id' => $account_id)));
        //if (!empty($assessment_account_record)) {
        //  $assessment_account_id = $assessment_account_record['EdtpaAssessmentAccounts']['id'];
        //}
        $this->set('edtpa_assessment_account_id', $edtpa_assessment_account_id);
        //Workspace videos
//        $MyFilesVideos = $this->Document->getMyFilesVideos($account_id, $user_id, "", "Document.created_date DESC", $limit, 0, '1, 3');
        $MyFilesVideos = $this->Document->getMyFilesVideosEdtpa($account_id, $user_id, "", "Document.created_date DESC", 10, 0, '1, 3');
        $total_videos_count = $this->Document->getMyFilesVideosEdtpa($account_id, $user_id, "", "Document.created_date DESC", $nolimit, $nopage, '1, 3');
        $this->set('MyFilesVideos', $MyFilesVideos);
        $this->set('MyFilesVideosCount', count($total_videos_count));
        //Workspace Documents
//        $MyFilesDocuments = $this->AccountFolder->getAllMyFiles($account_id, $user_id, '2');
        $MyFilesDocuments = $this->Document->getMyFilesVideos($account_id, $user_id, '', $sort, $limit, $page, 2);
        $this->set('MyFilesDocuments', $MyFilesDocuments);
        //Huddles Videos and Documents
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }
        $huddles_all = $this->AccountFolder->searchAccountHuddles_textbox($account_id, 'flat_view', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $huddles = $this->AccountFolder->getAllAccountHuddlesEdtpa($account_id, 'AccountFolder.created_date DESC', FALSE, $accountFoldereIds, $accountFolderGroupsIds, false, 0, 10, 0);
        $this->set('HuddlesCount', count($huddles_all));
//        echo '<pre>';
//        print_r($huddles);
//        die;
        $huddles_final_arr = array();
        if (!empty($huddles)) {
            foreach ($huddles as $huddle):
                $huddle_inner_arr = array();
                $huddle_inner_arr['user_id'] = $huddle['User']['id'];
                $huddle_inner_arr['user_name'] = $huddle['User']['username'];
                $huddle_inner_arr['user_email'] = $huddle['User']['email'];
                $huddle_inner_arr['user_first_name'] = $huddle['User']['first_name'];
                $huddle_inner_arr['user_last_name'] = $huddle['User']['last_name'];
                $huddle_inner_arr['AccountFolder_id'] = $huddle['AccountFolder']['account_folder_id'];
                $huddle_inner_arr['AccountFolder_account_id'] = $huddle['AccountFolder']['account_id'];
                $huddle_inner_arr['AccountFolder_folder_type'] = $huddle['AccountFolder']['folder_type'];
                $huddle_inner_arr['AccountFolder_name'] = $huddle['AccountFolder']['name'];
                $huddle_inner_arr['AccountFolder_created_date'] = $huddle['AccountFolder']['created_date'];
                $huddle_inner_arr['AccountFolder_active'] = $huddle['AccountFolder']['active'];
                $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle["AccountFolder"]["account_folder_id"]);

                $huddles_items = $this->Document->getVideos($huddle_id, '', '');

                $huddles_videos_arr = array();

                if (!empty($huddles_items)) {
                    foreach ($huddles_items as $huddles_item):
                        $huddles_videos_inner_arr = array();
                        $view = new View($this, false);
                        $document_files_array = $view->Custom->get_document_url($huddles_item['Document']);

                        if (empty($document_files_array['url'])) {
                            $huddles_item['Document']['published'] = 0;
                            $document_files_array['url'] = $huddles_item['Document']['original_file_name'];
                            $huddles_item['Document']['encoder_status'] = $document_files_array['encoder_status'];
                        } else {
                            $huddles_item['Document']['encoder_status'] = $document_files_array['encoder_status'];
                            @$huddles_item['Document']['duration'] = $document_files_array['duration'];
                        }
                        $thumbnail_image_path = $document_files_array['thumbnail'];
                        if ($huddles_item['Document']['doc_type'] == '3') {
                            if ($huddles_item['Document']['published'] == '1' && $huddles_item['Document']['is_processed'] == '4') {
                                $file_path_info = pathinfo($huddles_item['Document']['url']);
                                $huddles_videos_inner_arr['document_extension'] = $file_path_info['extension'];
                                $huddles_videos_inner_arr['video_id'] = $huddles_item['Document']['id'];
                                $huddles_videos_inner_arr['doc_type'] = $huddles_item['Document']['doc_type'];
                                $huddles_videos_inner_arr['video_thumbnail'] = $thumbnail_image_path;
                                $huddles_videos_inner_arr['video_title'] = $huddles_item['afd']['title'];
                                $huddles_videos_inner_arr['video_uploaded_date'] = date('M d, Y', strtotime($huddles_item['Document']['created_date']));
                            }
                        } else {
                            if ($huddles_item['Document']['published'] == '1') {
                                $file_path_info = pathinfo($huddles_item['Document']['url']);
                                $huddles_videos_inner_arr['document_extension'] = $file_path_info['extension'];
                                $huddles_videos_inner_arr['video_id'] = $huddles_item['Document']['id'];
                                $huddles_videos_inner_arr['doc_type'] = $huddles_item['Document']['doc_type'];
                                $huddles_videos_inner_arr['video_thumbnail'] = $thumbnail_image_path;
                                $huddles_videos_inner_arr['video_title'] = $huddles_item['afd']['title'];
                                $huddles_videos_inner_arr['video_uploaded_date'] = date('M d, Y', strtotime($huddles_item['Document']['created_date']));
                            }
                        }
                        if (!empty($huddles_videos_inner_arr))
                            array_push($huddles_videos_arr, $huddles_videos_inner_arr);
                    endforeach;
                }
                $huddle_inner_arr['videos'] = $huddles_videos_arr;
//                if (!empty($huddle_inner_arr['videos']))
                array_push($huddles_final_arr, $huddle_inner_arr);
            endforeach;
        }
        $this->set('HuddlesVideos', $huddles_final_arr);


        $huddles_final_arr_docs = array();
        if (!empty($huddles_all)) {
            foreach ($huddles_all as $huddle):
                $huddle_inner_arr_docs = array();
                $huddle_inner_arr_docs['user_id'] = $huddle['User']['id'];
                $huddle_inner_arr_docs['user_name'] = $huddle['User']['username'];
                $huddle_inner_arr_docs['user_email'] = $huddle['User']['email'];
                $huddle_inner_arr_docs['user_first_name'] = $huddle['User']['first_name'];
                $huddle_inner_arr_docs['user_last_name'] = $huddle['User']['last_name'];
                $huddle_inner_arr_docs['AccountFolder_id'] = $huddle['AccountFolder']['account_folder_id'];
                $huddle_inner_arr_docs['AccountFolder_account_id'] = $huddle['AccountFolder']['account_id'];
                $huddle_inner_arr_docs['AccountFolder_folder_type'] = $huddle['AccountFolder']['folder_type'];
                $huddle_inner_arr_docs['AccountFolder_name'] = $huddle['AccountFolder']['name'];
                $huddle_inner_arr_docs['AccountFolder_created_date'] = $huddle['AccountFolder']['created_date'];
                $huddle_inner_arr_docs['AccountFolder_active'] = $huddle['AccountFolder']['active'];
                $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle["afmd"]["account_folder_id"]);
                $huddles_documents = $this->Document->getDocuments($huddle_id, '2', '');
                $huddles_docs_arr = array();
                if (!empty($huddles_documents)) {
                    foreach ($huddles_documents as $huddles_document):
                        $huddles_docs_inner_arr = array();
                        $file_path_info = pathinfo($huddles_document['Document']['url']);
                        $huddles_docs_inner_arr['document_extension'] = $file_path_info['extension'];
                        $huddles_docs_inner_arr['stack_url'] = $huddles_document['Document']['stack_url'];
                        $huddles_docs_inner_arr['document_id'] = $huddles_document['Document']['id'];
                        $huddles_docs_inner_arr['doc_type'] = $huddles_document['Document']['doc_type'];
                        $huddles_docs_inner_arr['afd_id'] = $huddles_document['afd']['id'];
                        $huddles_docs_inner_arr['afd_title'] = $huddles_document['afd']['title'];
                        $huddles_docs_inner_arr['document_by'] = $huddles_document['User']['first_name'] . " " . $huddles_document['User']['last_name'];
                        $view = new View($this, false);
                        $huddles_docs_inner_arr['document_url'] = $view->Custom->docType($huddles_document['Document']['url']);
                        $huddles_docs_inner_arr['document_icon'] = $view->Custom->getIcons($huddles_document['Document']['url']);
                        $huddles_docs_inner_arr['created_date'] = date('M d, Y', strtotime($huddles_document['Document']['created_date']));
                        if (!empty($huddles_docs_inner_arr))
                            array_push($huddles_docs_arr, $huddles_docs_inner_arr);

                    endforeach;
                }

                $huddle_inner_arr_docs['documents'] = $huddles_docs_arr;
                if (!empty($huddle_inner_arr_docs['documents']))
                    array_push($huddles_final_arr_docs, $huddle_inner_arr_docs);
            endforeach;
        }
        $this->set('HuddlesDocuments', $huddles_final_arr_docs);
        $this->set('edtpa_assessment_id', $edtpa_assessment_id);
        $view = new View($this, false);
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        if ($view->Custom->is_teacher($account_id, $user_id)) {
            $this->render('edtpa_detail_for_teacher');
        } else {
            $this->render('edtpa_detail');
        }
    }

    function edTPA_submission() {
        $user = $this->Session->read('user_current_account');
        $edtpa_lang = $this->Session->read('edtpa_lang');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $assessment_accounts = $this->EdtpaAssessments->getEtpaAssessment_for_summery($account_id, $user_id);

        $data = array();
        foreach ($assessment_accounts as $assessment_account):

            if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && ($assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 1 || $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 0)) {
                $submit_status = $edtpa_lang['edtpa_assessment_started'];
                $cancel_btn = false;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 2) {
                $submit_status = $edtpa_lang['edtpa_auth_key_varify'];
                $cancel_btn = false;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 3) {
                $submit_status = $edtpa_lang['edtpa_transfer_varified'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 4) {
                $submit_status = $edtpa_lang['edtpa_transfer_requested'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 5) {
                $submit_status = $edtpa_lang['edtpa_transfer_accepted'];
                $cancel_btn = false;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 8) {
                $submit_status = 'Success';
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 9) {
                $submit_status = $edtpa_lang['edtpa_transfer_failed'];
                $cancel_btn = true;
            } else {
                $submit_status = 'N/A';
                $cancel_btn = false;
            }

            $tranfer_record = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $assessment_account['EdtpaAssessmentAccountCandidates']['id']), 'order' => array('id' => 'DESC')));

            $class = (isset($cancel_btn) && $cancel_btn == true) ? 'cantEditCls' : '';
            //$href = (isset($cancel_btn) && $cancel_btn == true) ? 'javascript:void(0);' : $this->base . '/Edtpa/edtpa_detail_teacher/'.$assessment_account['EdtpaAssessments']['id'].'/'. $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'].'/'. $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $href = 'javascript:void(0);';
            $class = '';
            if ($_SESSION['LANG'] == 'es') {
                $assessment_name = "<a class='" . $class . "' href='" . $href . "'>" . $assessment_account['EdtpaAssessments']['assessment_name_es'] . "</a>";
            } else {
                $assessment_name = "<a class='" . $class . "' href='" . $href . "'>" . $assessment_account['EdtpaAssessments']['assessment_name'] . "</a>";
            }


            $tranfer_id = $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['id'];
            $action_btn = '';
            if ($cancel_btn) {
                $action_btn = '<a href="javascript:void(0);" class="cancleRequest" onlclick="cancelRequest(' . $tranfer_id . ')">' . $edtpa_lang['edtpa_cancel'] . '</a>';
            } else {
                $action_btn = "<a class='" . $class . "' href='" . $href . "'>" . $edtpa_lang['edtpa_select'] . "</a>";
            }
            $assessment_accounts_inner['EdtpaAssessments']['id'] = $assessment_account['EdtpaAssessments']['id'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_name'] = $assessment_account['EdtpaAssessments']['assessment_name'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_version'] = $assessment_account['EdtpaAssessments']['assessment_version'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_id'] = $assessment_account['EdtpaAssessments']['assessment_id'];
            $assessment_accounts_inner['EdtpaAssessments']['project_id'] = $assessment_account['EdtpaAssessments']['project_id'];
            $assessment_accounts_inner['EdtpaAssessments']['created_at'] = $assessment_account['EdtpaAssessments']['created_at'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_type'] = $assessment_account['EdtpaAssessments']['assessment_type'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_xml'] = $assessment_account['EdtpaAssessments']['assessment_xml'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['user_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'] = $assessment_account['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['status'] = $assessment_account['EdtpaAssessmentAccountCandidates']['status'];
            $assessment_accounts_inner['transfer_id'] = $tranfer_id;
            $assessment_accounts_inner['EdtpaAssessments']['status'] = $submit_status;
            $assessment_accounts_inner['EdtpaAssessments']['cancel_btn'] = $cancel_btn;
            $data[] = array(
                'assessment_name' => $assessment_name,
                'assessment_version' => $assessment_account['EdtpaAssessments']['assessment_version'],
                'assessment_id' => $assessment_account['EdtpaAssessments']['assessment_id'],
                'project_id' => $assessment_account['EdtpaAssessments']['project_id'],
                'assessment_xml' => $assessment_account['EdtpaAssessments']['assessment_xml'],
                'transfer_id' => $tranfer_id,
                'status' => $submit_status,
                'submission_status' => $submit_status,
                'cancel_btn' => $cancel_btn,
                'created_date' => $assessment_account['EdtpaAssessments']['created_at'],
                'action' => $action_btn,
                'student_name' => $assessment_account['usr']['first_name'] . ' ' . $assessment_account['usr']['last_name']
            );
        endforeach;
        return $data;
    }

    function export_to_excel() {
        $view = new View($this, false);
        $user = $this->Session->read('user_current_account');
        $edtpa_lang =  $this->Session->read('edtpa_lang');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $student_name = isset($_GET['student_name']) ? $_GET['student_name'] : '';
        $assessment_name = isset($_GET['assessment_name']) ? $_GET['assessment_name'] : '';
        $page = $_GET['page'];
        $rows = $_GET['rows'];
        $sidx = $_GET['sidx'];
        $sord = $_GET['sord'];
        $offset = $page * $rows;
        $limit = $offset - $rows;
        $user_ids = false;
        if (!empty($user) && ($user['users_accounts']['role_id'] == 115)) {
            //get  Coach huddles
            $account_folder_ids = $view->Custom->get_huddle_ids_by_coach($user_id, $account_id);
            // All Coachees huddle

            $coachees = $view->Custom->get_huddle_participants_for_edTPA($account_folder_ids);


            if ($coachees) {
                foreach ($coachees as $row) {
                    $user_ids[] = $row['AccountFolderUser']['user_id'];
                }
            }

            $user_ids = implode(',', $user_ids);
        }


        $total_records = $this->EdtpaAssessments->getEtpaAssessment_for_teacher($account_id, $user_ids, false, false, $sidx, $sord, $student_name, $assessment_name);
        $assessment_accounts = $this->EdtpaAssessments->getEtpaAssessment_for_teacher($account_id, $user_ids, $limit, $offset, $sidx, $sord, $student_name, $assessment_name);
        $assessment_accounts_final = array();
        $result = array(
            "records" => ceil(count($total_records) / 10),
            "page" => 1,
            "total" => count($total_records),
        );
        $data = array();
        foreach ($assessment_accounts as $assessment_account):
           if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && ($assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 1 || $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 0)) {
                $submit_status = $edtpa_lang['edtpa_assessment_started'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 2) {
                $submit_status = $edtpa_lang['edtpa_auth_key_varify'];
                $cancel_btn = false;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 3) {
                $submit_status = $edtpa_lang['edtpa_transfer_varified'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 4) {
                $submit_status = $edtpa_lang['edtpa_transfer_requested'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 5) {
                $submit_status = $edtpa_lang['edtpa_transfer_accepted'];
                $cancel_btn = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 8) {
                $submit_status = $edtpa_lang['edtpa_success'];
                $cancel_btn = true;
                $is_cancel_local = true;
            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 9) {
                $submit_status = $edtpa_lang['edtpa_transfer_failed'];
                $cancel_btn = true;
                $is_cancel_local = true;
            } else {
                $submit_status = 'N/A';
                $cancel_btn = false;
            }

            $tranfer_record = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $assessment_account['EdtpaAssessmentAccountCandidates']['id']), 'order' => array('id' => 'DESC')));

            $class = (isset($cancel_btn) && $cancel_btn == true) ? 'cantEditCls' : '';
            $href = (isset($cancel_btn) && $cancel_btn == true) ? 'javascript:void(0);' : $this->base . '/Edtpa/edtpa_detail_teacher/' . $assessment_account['EdtpaAssessments']['id'] . '/' . $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'] . '/' . $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $href = $this->base . '/Edtpa/edtpa_detail_teacher/' . $assessment_account['EdtpaAssessments']['id'] . '/' . $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'] . '/' . $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $assessment_name = $assessment_account['EdtpaAssessments']['assessment_name'];
            $tranfer_id = $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['id'];
            $action_btn = '';
            if ($cancel_btn) {
                //$action_btn = '<a href="javascript:void(0);" class="cancleRequest" onlclick="cancelRequest(' . $tranfer_id . ')">Cancel</a>';
            } else {
                //$action_btn = "<a class='" . $class . "' href='". $href . "'>Select </a>";
            }
            $assessment_accounts_inner['EdtpaAssessments']['id'] = $assessment_account['EdtpaAssessments']['id'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_name'] = $assessment_account['EdtpaAssessments']['assessment_name'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_version'] = $assessment_account['EdtpaAssessments']['assessment_version'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_id'] = $assessment_account['EdtpaAssessments']['assessment_id'];
            $assessment_accounts_inner['EdtpaAssessments']['project_id'] = $assessment_account['EdtpaAssessments']['project_id'];
            $assessment_accounts_inner['EdtpaAssessments']['created_at'] = $assessment_account['EdtpaAssessments']['created_at'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_type'] = $assessment_account['EdtpaAssessments']['assessment_type'];
            $assessment_accounts_inner['EdtpaAssessments']['assessment_xml'] = $assessment_account['EdtpaAssessments']['assessment_xml'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['user_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['user_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['account_id'] = $assessment_account['EdtpaAssessmentAccountCandidates']['account_id'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'] = $assessment_account['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'] = $assessment_account['EdtpaAssessmentAccountCandidates']['auth_key_reg_at'];
            $assessment_accounts_inner['EdtpaAssessmentAccountCandidates']['status'] = $assessment_account['EdtpaAssessmentAccountCandidates']['status'];
            $assessment_accounts_inner['transfer_id'] = $tranfer_id;
            $assessment_accounts_inner['EdtpaAssessments']['status'] = $submit_status;
            $assessment_accounts_inner['EdtpaAssessments']['cancel_btn'] = $cancel_btn;
            $view_assessment_url = 'N/A';
            if ($user['users_accounts']['role_id'] != 115) {
                $view_assessment_url = '<a href=' . $href . '>View Assessment </a>';
            } else {
                $view_assessment_url = '<a href="#">View Assessment </a>';
            }
            $date_of_transfer = '';
            $time_of_transfer = '';
            if (!empty($tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['file_picked_at'])) {
                $date_of_picked = explode(' ', $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['file_picked_at']);
                $date_of_transfer = !empty($tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['file_picked_at']) ? $tranfer_record['EdtpaAssessmentAccountCandidateTransfers']['file_picked_at'] : '00-00-00 00:00:00';
                $time_of_transfer = isset($date_of_picked[1]) ? $date_of_picked[1] : '';
            } else {
                $date_of_transfer = '00-00-00 00:00:00';
                $time_of_transfer = '';
            }
            $data[] = array(
                'assessment_name' => $assessment_name,
                'assessment_version' => $assessment_account['EdtpaAssessments']['assessment_version'],
                'assessment_id' => $assessment_account['EdtpaAssessments']['assessment_id'],
                'project_id' => $assessment_account['EdtpaAssessments']['project_id'],
                //'assessment_xml' => $assessment_account['EdtpaAssessments']['assessment_xml'],
                'transfer_id' => $tranfer_id,
                'status' => $submit_status,
                'cancel_btn' => $cancel_btn,
                'created_date' => $assessment_account['EdtpaAssessments']['created_at'],
                'action' => $view_assessment_url,
                'student_name' => $assessment_account['usr']['first_name'] . ' ' . $assessment_account['usr']['last_name'],
                'date_of_transfer' => $date_of_transfer,
                'time_of_transfer' => $time_of_transfer
            );

            array_push($assessment_accounts_final, $assessment_accounts_inner);
        endforeach;
        $export_excel = '';
        $export_excel = '<table border="1">';
        $export_excel .='<thead valign="top">';
        $export_excel .= '<tr>';
        $export_excel .= '<th>'.$edtpa_lang['edtpa_placehoder_name'].'</th>';
        $export_excel .= '<th>'.$edtpa_lang['edtpa_assessment_cap'].'</th>';
        $export_excel .= '<th>'.$edtpa_lang['edtpa_date_of_transfer'].'</th>';
        $export_excel .= '<th>'.$edtpa_lang['edtpa_time_of_transfer'].'</th>';
        $export_excel .= '<th>'.$edtpa_lang['edtpa_status'].'</th>';
        $export_excel .= '</tr>';
        $export_excel .= '</thead>';
        $export_excel .= '<tbody>';
        if ($data) {
            foreach ($data as $row) {
                $export_excel .= '<tr>';
                $export_excel .= '<td>' . $row['student_name'] . '</td>';
                $export_excel .= '<td>' . $row['assessment_name'] . '</td>';
                $export_excel .= '<td>' . $row['date_of_transfer'] . '</td>';
                $export_excel .= '<td>' . $row['time_of_transfer'] . '</td>';
                $export_excel .= '<td>' . $row['status'] . '</td>';
                $export_excel .= '</tr>';
            }
        } else {
            $export_excel .= '<tr>';
            $export_excel .= '<td colspan="5">No Assessment Found!<td>';
            $export_excel .= '<tr>';
        }
        $export_excel .= '</tbody>';
        $export_excel .= '</table>';
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Type: application/octet-stream");
        header("Content-Transfer-Encoding: binary");         
        header("Content-Disposition: attachment;filename=Assesments.xls");
        echo $export_excel;
        die;
    }

}
