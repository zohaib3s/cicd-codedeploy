<?php

use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Aws3\MediaConvert\MediaConvertClient;  

class S3Services {

    public $amazon_base_url = '';
    public $bucket_name = '';
    public $access_key_id = '';
    public $secret_access_key = '';

    function __construct($p_amazon_base_url, $p_bucket_name, $p_access_key_id, $p_secret_access_key) {

        $this->amazon_base_url = $p_amazon_base_url;
        $this->bucket_name = $p_bucket_name;
        $this->access_key_id = $p_access_key_id;
        $this->secret_access_key = $p_secret_access_key;
    }

    function publish_to_s3($file_path, $s3_folder_path, $del_file = true) {

        //include the S3 class
        if (!class_exists('S3'))
            require_once('S3.php');

        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class
        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        $fileName = basename($file_path);

        //move the file
        if ($s3->putObjectFile($file_path, $this->bucket_name, $s3_folder_path . $fileName, S3::ACL_AUTHENTICATED_READ)) {
            if ($del_file)
                $this->deleteDir($file_path);
            return $this->amazon_base_url . "$s3_folder_path" . $fileName;
        } else {
            return "";
        }
    }

    function get_object_info_s3($bucket, $uri) {

        //include the S3 class
        if (!class_exists('S3'))
            require_once('S3.php');

        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class
        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        return $s3->getObjectInfo($this->bucket_name, $uri);
    }

    function copy_from_s3($src_folder_path, $dest_folder_path) {

        //include the S3 class
        if (!class_exists('S3'))
            require_once('S3.php');

        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class
        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        //move the file
        if ($s3->copyObject($this->bucket_name, $src_folder_path, $this->bucket_name, $dest_folder_path, S3::ACL_AUTHENTICATED_READ)) {
            //$s3->deleteObject($this->bucket_name, $src_folder_path);
            return $this->amazon_base_url . "$dest_folder_path";
        } else {
            return "";
        }
    }

    function copy_from_s3_wcontent($src_folder_path, $dest_folder_path, $content_type) {

        //include the S3 class
        if (!class_exists('S3'))
            require_once('S3.php');

        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class
        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        //move the file
        if ($s3->copyObject($this->bucket_name, $src_folder_path, $this->bucket_name, $dest_folder_path, S3::ACL_AUTHENTICATED_READ, array(), array("Content-Type" => $content_type))) {
            //$s3->deleteObject($this->bucket_name, $src_folder_path);
            return $this->amazon_base_url . "$dest_folder_path";
        } else {
            return "";
        }
    }

    function set_acl_permission($src_folder_path) {

        //include the S3 class
        if (!class_exists('S3'))
            require_once('S3.php');

        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class

        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        $acp = $s3->getAccessControlPolicy($this->bucket_name, $src_folder_path);
        $acp['acl'][] = array(
            'type' => 'Group'
            , 'uri' => 'http://acs.amazonaws.com/groups/global/AuthenticatedUsers'
            , 'permission' => 'FULL_CONTROL'
        );


        return $s3->setAccessControlPolicy($this->bucket_name, $src_folder_path, $acp);
    }

    function deleteDir($dirPath) {
        if (!is_dir($dirPath)) {
            $dirPath = dirname($dirPath);
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    function get_job_id() {

        $client = ElasticTranscoderClient::factory(array(
                    'key' => $this->access_key_id,
                    'region' => 'us-east-1',
                    'secret' => $this->secret_access_key
        ));

        $result = $client->listPipelines(array());

        if ($this->bucket_name == 'sibme-production')
            $pipelineId = $result['Pipelines'][1]['Id'];
        else
            $pipelineId = $result['Pipelines'][0]['Id'];
    }

    function encode_videos($s_url, $s3_relative_path, $file_name, $transcoder_type = 1) {

        if ($transcoder_type == 1) {

            try {

                $path_parts = pathinfo($file_name);
                $base_file_name = $path_parts['filename'];
                require_once('Services/Zencoder.php');

                $s_url = str_replace("#", urlencode("#"), $s_url);
                $s_url = str_replace("?", urlencode("?"), $s_url);

                $zencoder = new Services_Zencoder('39bdc265c56dc8e09b62b8fd38f475f9');

                $encoding_job = $zencoder->jobs->create(
                        array(
                            "input" => $s_url,
                            "outputs" => array(
                                array(
                                    "label" => "mp4",
                                    "base_url" => $this->amazon_base_url . "$s3_relative_path",
                                    "filename" => $base_file_name . "_enc.mp4",
                                    "public" => false,
                                    "thumbnails" => array(
                                        "base_url" => $this->amazon_base_url . "$s3_relative_path",
                                        "size" => "320x180",
                                        "filename" => $base_file_name . "_thumb"
                                    ),
                                    "notifications" => array("https://app.sibme.com/ZencoderNotification/received_notification")
                                )
                            )
                        )
                );

                // Success if we got here
                return $encoding_job->id;
            } catch (ErrorException $e) {
                return FALSE;
            }
        } else {

            //amazon

            $src_relative_path = str_replace($this->amazon_base_url, "", $s_url);

            $path_parts = pathinfo($file_name);
            $base_file_name = $path_parts['filename'];

            $client = ElasticTranscoderClient::factory(array(
                        'key' => $this->access_key_id,
                        'region' => 'us-east-1',
                        'secret' => $this->secret_access_key
            ));

            $result = $client->listPipelines(array());

            if ($this->bucket_name == 'sibme-production')
                $pipelineId = $result['Pipelines'][1]['Id'];
            else
                $pipelineId = '1444071830576-8h6mzh';
//'PresetId' => '1425215270054-j95h0f'
            $job = $client->createJob(array(
                'PipelineId' => $pipelineId,
                'Input' => array(
                    'Key' => "$src_relative_path",
                    'FrameRate' => 'auto',
                    'Resolution' => 'auto',
                    'AspectRatio' => 'auto',
                    'Interlaced' => 'auto',
                    'Container' => 'auto',
                ),
                'Output' => array(
                    'Key' => "$s3_relative_path/$base_file_name" . "_720_enc.mp4",
                    'ThumbnailPattern' => "$s3_relative_path/$base_file_name" . "_720_enc" . '_thumb_{count}',
                    'Rotate' => 'auto',
                    /* 'Composition' => array(
                      array(
                      'TimeSpan' => array(
                      'StartTime' => '00:00:03.000',
                      'Duration' => '00:00:01.000'
                      )
                      )
                      ), */
                    'PresetId' => '1443730387055-5y6awl'
                )
            ));

            return $job['Job']['Id'];
        }
    }

    function trim_videos($s_url, $s3_relative_path, $file_name, $startTime, $endTime,$folder_path = 'uploads',$aws_transcoder_type=1,$name_modifier ='') {
        //amazon

        $src_relative_path = str_replace($this->amazon_base_url, "", $s_url);
        $url_parsing = pathinfo($s_url);
        $path_parts = pathinfo($file_name);
        $base_file_name = $path_parts['filename'];

        $timestamp = "_" . strtotime('now');

        $base_file_name_output = $base_file_name . $timestamp;

        //  $src_relative_path_output = str_replace("_enc.mp4" , $timestamp."_enc.mp4", $src_relative_path);
        $audio_extension = array('mp3','wma','aac','wav');
        if(in_array($url_parsing['extension'],$audio_extension)){
            $src_relative_path_output = str_replace(".mp3", $timestamp . ".mp3", $src_relative_path);
        }else{
            $src_relative_path_output = str_replace(".mp4", $timestamp . ".mp4", $src_relative_path);
        }        

        if ($aws_transcoder_type==2) {
            // $src_relative_path = str_replace($this->amazon_base_url, "", $s_url);
            $input_file_w_bucket = "s3://".$this->bucket_name."/".$file_name; 
            $job_id = $this->encode_with_elemental_mediaconvert($input_file_w_bucket, $startTime, $endTime,null,$name_modifier);
            return $job_id;
        }else{
        $client = ElasticTranscoderClient::factory(array(
                    'key' => $this->access_key_id,
                    'region' => 'us-east-1',
                    'secret' => $this->secret_access_key
        ));

        $result = $client->listPipelines(array());
        if ($this->bucket_name == 'sibme-production')
            $pipelineId = $result['Pipelines'][1]['Id'];
        else
            $pipelineId = $result['Pipelines'][0]['Id'];

        $thumnail_pattern_path = pathinfo($src_relative_path_output);

        $job = $client->createJob(array(
            'PipelineId' => $pipelineId,
            'Input' => array(
                'Key' => "$src_relative_path",
                'FrameRate' => 'auto',
                'Resolution' => 'auto',
                'AspectRatio' => 'auto',
                'Interlaced' => 'auto',
                'Container' => 'auto',
            ),
            'Output' => array(
                'Key' => "$src_relative_path_output",
                'ThumbnailPattern' => $thumnail_pattern_path['dirname'] . "/" . $thumnail_pattern_path['filename'] . '_thumb_{count}',
                //'Rotate' => 'auto',
                'Composition' => array(
                    array(
                        'TimeSpan' => array(
                            'StartTime' => $startTime,
                            'Duration' => $endTime
                        )
                    )
                ),
                //'PresetId' => '1425215270054-j95h0f'
                'PresetId' => '1443730387055-5y6awl'
            )
        ));

        return $job['Job']['Id'] . "||" . str_replace($folder_path . '/', '', $src_relative_path_output);
       }
    }
    
    public function encode_with_elemental_mediaconvert($input_file_w_path, $startTimecode=null, $endTimecode=null, $millicast_preset = false,$name_modifier=null) {
        $file_parts = pathinfo($input_file_w_path);
        $dir_name = $file_parts['dirname'] . "/";
        if(!strpos($dir_name,"transcoded")){
            $dir_name .= "transcoded/";
        }
        $mediaConvertClient = new MediaConvertClient( [
            'version' => '2017-08-29',
            'region' => 'us-east-1',
            'endpoint' => 'https://q25wbt2lc.mediaconvert.us-east-1.amazonaws.com',
            'credentials' => [
                // 'key'    => 'AKIAIXPDDXXGZVUBMUFA',
                // 'secret' => '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',
                'key'    => 'AKIAJ4ZWDR5X5JKB7CZQ',
                'secret' => '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',
            ]
        ]);
        if($millicast_preset)
        {
            /*$preset = "Live Stream Millicast Preset"; // This is creating very poor quality video with no audio
            $preset_thumbnail = "Custom_Thumbnail_Millicast";*/ // This preset is Wrong need to delete.
            //Old Preset -> System-Generic_Sd_Mp4_Avc_Aac_16x9_Sdr_640x360p_30Hz_0.8Mbps_Qvbr_Vq7
            $preset = "Custom_elemental"; // This preset create good video but no audio.
            $preset_thumbnail = "Custom Thumbnail";//For live streams it is always creating a black thumbnail
        }
        else
        {
            $preset = "Custom_elemental";
            $preset_thumbnail = "Custom Thumbnail";
        }
        $jobSetting = [
            "OutputGroups" => [
                [
                    "CustomName" => "Video",
                    "Name" => "File Group",
                    "Outputs" => [
                        [
                            "Preset" => $preset,
                            // "Preset" => "Custom-Generic_Sd_Mp4_Avc_Aac_16x9_Sdr_1280x720p_24Hz_0.15Mbps_Qvbr_Vq4",
                        ]
                    ],
                    "OutputGroupSettings" => [
                        "Type" => "FILE_GROUP_SETTINGS",
                        "FileGroupSettings" => [
                            "Destination" => $dir_name
                            // "Destination" => "s3://sibme-production/elemental_mediaconvert/"
                        ]
                    ],
                ],
                [
                    "CustomName" => "Thumbnail",
                    "Name" => "File Group",
                    "Outputs" => [
                        [
                            "Preset" => $preset_thumbnail,
                            "Extension" => "png",
                            "NameModifier" => "_thumbnail"
                        ]
                    ],
                    "OutputGroupSettings" => [
                        "Type" => "FILE_GROUP_SETTINGS",
                        "FileGroupSettings" => [
                            "Destination" => $dir_name 
                            // "Destination" => "s3://sibme-production/elemental_mediaconvert/"
                        ]
                    ],
                ],
            ],
            "AdAvailOffset" => 0,
            "Inputs" => [
                [
                    "AudioSelectors" => [
                        "Audio Selector 1" => [
                            "Offset" => 0,
                            "DefaultSelection" => "DEFAULT",
                            "ProgramSelection" => 1,
                        ]
                    ],
                    "VideoSelector" => [
                        "ColorSpace" => "FOLLOW"
                    ],
                    "FilterEnable" => "AUTO",
                    "PsiControl" => "USE_PSI",
                    "FilterStrength" => 0,
                    "DeblockFilter" => "DISABLED",
                    "DenoiseFilter" => "DISABLED",
                    "TimecodeSource" => "ZEROBASED",
                    // "FileInput" => "s3://sibme-production/ksaleem_recent.mov"
                    "FileInput" => $input_file_w_path
                ]
            ],
            "TimecodeConfig" => [
                "Source" => "EMBEDDED"
            ]
        ];

        if(!empty($name_modifier)){
            $jobSetting['OutputGroups'][0]['Outputs'][0]["NameModifier"] = $name_modifier;
            $jobSetting['OutputGroups'][1]['Outputs'][0]["NameModifier"] = $name_modifier."_thumbnail";
        }
        if(!empty($startTimecode) && !empty($endTimecode)){
            $jobSetting['OutputGroups'][0]['OutputGroupSettings']["FileGroupSettings"]["Destination"] = $dir_name;
            $jobSetting['OutputGroups'][1]['OutputGroupSettings']["FileGroupSettings"]["Destination"] = $dir_name;
            $frame = ":05";
            $jobSetting['Inputs'][0]['InputClippings'] = [
                                                            [
                                                                'StartTimecode' => $startTimecode . $frame,
                                                                'EndTimecode' => $endTimecode . $frame,
                                                            ]
                                                         ];
        }
        try {
            $free_queue = $this->qetAvailableQueueName($mediaConvertClient);
            $result = $mediaConvertClient->createJob([
                "Role" => "arn:aws:iam::614596103109:role/MediaConvert_Transcoder_Default_Role",
                "Settings" => $jobSetting, //JobSettings structure
                // "JobTemplate" => "arn:aws:mediaconvert:us-east-1:614596103109:jobTemplates/Custom-Generic_Mp4_Hev1_Avc_Aac_Sdr_Qvbr",
                "JobTemplate" => "Custom-Generic_Mp4_Hev1_Avc_Aac_Sdr_Qvbr",
                "Queue" => $free_queue,
                "StatusUpdateInterval" => "SECONDS_30",
                "Priority" => 0
            ]);
            return $result['Job']['Id'];
        } catch (ErrorException $e) {
                return FALSE;
            }

    }
    
       public function qetAvailableQueueName($mediaConvertClient = "")
    {
        if(empty($mediaConvertClient))
        {
            $mediaConvertClient = new MediaConvertClient( [
                'version' => '2017-08-29',
                'region' => 'us-east-1',
                'endpoint' => 'https://q25wbt2lc.mediaconvert.us-east-1.amazonaws.com',
                'credentials' => [
                    // 'key'    => 'AKIAIXPDDXXGZVUBMUFA',
                    // 'secret' => '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',
                    'key'    => 'AKIAJ4ZWDR5X5JKB7CZQ',
                    'secret' => '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',
                ]
            ]);
        }

        $results = $mediaConvertClient->listQueues([]);
        $final_queue = "";
        if(!empty($results["Queues"]))
        {
            foreach ($results["Queues"] as $queue)
            {
                if($queue["PricingPlan"] == "RESERVED" && $queue["ProgressingJobsCount"] < 2 && $queue["Status"] == "ACTIVE" && $queue["SubmittedJobsCount"] < 2)
                {
                    $final_queue = $queue["Arn"];
                    break;//as soon as we get a free Reserved Queue loop should exit.
                }
                else if($queue["PricingPlan"] == "ON_DEMAND" && $queue["ProgressingJobsCount"] < 2 && $queue["Status"] == "ACTIVE" && $queue["SubmittedJobsCount"] < 2)
                {
                    $final_queue = $queue["Arn"];
                }
            }
        }
        if(empty($final_queue))
        {
            return "arn:aws:mediaconvert:us-east-1:614596103109:queues/Default";
        }
        return $final_queue;
    }


}

?>
