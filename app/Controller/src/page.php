<?php

//imports new pricing table for frames
ini_set("display_errors", 1);
error_reporting(E_ALL);

define('NO_SESSION', true);

ini_set("max_execution_time", "40000");
ini_set('memory_limit', '8024M');

set_time_limit(0);
ignore_user_abort();

function opendb() {
    $dbc = mysqli_connect("127.0.0.1", "root", "root", "sibme_production");
    if (!$dbc) {
        echo('Could not connect to MySQL: ' . mysqli_connect_error());
    }
    return $dbc;
}

function closedb($dbc) {
    mysqli_close($dbc);
}

function get_unpublished_files() {

    $dbc = opendb();
    $items = array();

    $getStates = mysqli_query($dbc, "select * from videos where is_webservice =1 and processed=0");

    if (intval(mysqli_num_rows($getStates)) > 0) {

        while ($row = mysqli_fetch_array($getStates)) {
            $items[] = $row;
        }
    }

    return $items;

    closedb($dbc);
}

function publish_video($id, $zencoder_output_id) {

    $dbc = opendb();
    $items = array();

    mysqli_query($dbc, "update videos set zencoder_output_id = $zencoder_output_id, processed =1 WHERE id=$id");

    closedb($dbc);
}

function get_user_data($user_id) {

    $dbc = opendb();
    $items = array();

    $getStates = mysqli_query($dbc, "select * from users where id= $user_id");

    if (intval(mysqli_num_rows($getStates)) > 0) {

        while ($row = mysqli_fetch_array($getStates)) {
            return $row;
        }
    }

    return null;

    closedb($dbc);
}

function get_video_huddle($video_huddle_id) {

    $dbc = opendb();
    $items = array();

    $getStates = mysqli_query($dbc, "select * from video_huddles where id= $video_huddle_id");

    if (intval(mysqli_num_rows($getStates)) > 0) {

        while ($row = mysqli_fetch_array($getStates)) {
            return $row;
        }
    }

    return null;

    closedb($dbc);
}

function deleteDir($dirPath) {
    if (!is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

function publish_to_s3($file_name, $file_path, $video_huddle_id) {

    //include the S3 class
    if (!class_exists('S3'))
        require_once('S3.php');

    //AWS access info
    if (!defined('awsAccessKey'))
        define('awsAccessKey', 'AKIAJLZK6AH42MMIBMAA');
    if (!defined('awsSecretKey'))
        define('awsSecretKey', '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');

    //instantiate the class
    $s3 = new S3(awsAccessKey, awsSecretKey);

    //retreive post variables
    $fileName = $file_name;
    $fileTempName = $file_path . $fileName;

    //create a new bucket
    //$s3->putBucket("yourbucket", S3::ACL_PUBLIC_READ);
    //move the file
    if ($s3->putObjectFile($fileTempName, "sibme-production", "uploads/video/video/$video_huddle_id/" . $fileName, S3::ACL_PUBLIC_READ)) {
        return "http://sibme-production.s3.amazonaws.com/uploads/video/video/$video_huddle_id/" . $fileName;
    } else {
        return "";
    }
}

require_once('Services/Zencoder.php');
$unpublished_videos = get_unpublished_files();

for ($i = 0; $i < count($unpublished_videos); $i++) {

    $video = $unpublished_videos[$i];


    $path_parts = pathinfo($video['video']);
    $base_file_name = $path_parts['filename'];



    $s_url = publish_to_s3($video['video'], $video['raw_path'], $video['video_huddle_id']);
    $video_id = $video['id'];
    $video_huddle_id = $video['video_huddle_id'];
    // Initialize the Services_Zencoder class
    $zencoder = new Services_Zencoder('39bdc265c56dc8e09b62b8fd38f475f9');

    //echo $s_url;
    //die;

    try {

        // New Encoding Job
        $encoding_job = $zencoder->jobs->create(
                array(
                    "input" => $s_url,
                    "outputs" => array(
                        array(
                            "label" => "mp4",
                            "base_url" => "http://sibme-production.s3.amazonaws.com/uploads/video/video/$video_huddle_id/",
                            "filename" => $base_file_name . "_enc.mp4",
                            "public" => true,
                            "thumbnails" => array(
                                "base_url" => "http://sibme-production.s3.amazonaws.com/uploads/video/video/$video_huddle_id/",
                                "size" => "320x180",
                                "filename" => $base_file_name . "_thumb"
                            )
                        ),
                        array(
                            "label" => "webm",
                            "base_url" => "http://sibme-production.s3.amazonaws.com/uploads/video/video/$video_huddle_id/",
                            "filename" => $base_file_name . "_enc.webm",
                            "public" => true
                        )
                    )
                )
        );

        // Success if we got here
        echo "w00t! \n\n";
        echo "Job ID: " . $encoding_job->id . "\n";
        echo "Output ID: " . $encoding_job->outputs['web']->id . "\n";

        publish_video($video_id, $encoding_job->id);

        $user_data = get_user_data($video['user_id']);
        $authentication_token = $user_data['authentication_token'];
        $video_huddle_data = get_video_huddle($video_huddle_id);
        $video_huddle_name = $video_huddle_data['name'];

        require_once('mail/dmailer.inc.php');
        $mail = new dMailer();

        $email_html = "";
        $email_html .= "<h2 style='color:#3d9bc2;' xmlns='http://www.w3.org/1999/html'>$video_huddle_name</h2>
			<br/>
			<p>The following video has been uploaded and processed and is now ready for viewing.</p>
			<br/>
			<p>Follow this link to view:
			  <br />
			   <a href='http://app.sibme.com/video_huddles/$video_huddle_id?auth_token=$authentication_token&video_id=$video_id'>Click to View</a>
			  <p>
			<br/>
			<a href='http://app.sibme.com'>app.sibme.com</a>
			<p>Need help?</p>
			<p>email ".$this->custom->get_site_settings('static_emails')['support']."</p>";


        $mail->dMailer($this->custom->get_site_settings('static_emails')['noreply'], $user_data['email'], 'Your video is ready to be viewed!', $email_html, 1);

        echo $email_html;

        deleteDir($video['raw_path']);
    } catch (Services_Zencoder_Exception $e) {
        // If were here, an error occured
        echo "Fail :(\n\n";
        echo "Errors:\n";
        foreach ($e->getErrors() as $error)
            echo $error . "\n";
        echo "Full exception dump:\n\n";
        print_r($e);
    }
}
?>