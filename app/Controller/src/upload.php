<?php

/**
 * upload.php
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */
// HTTP headers for no cache etc

function opendb() {
    $dbc = mysqli_connect("127.0.0.1", "root", "root", "sibme_production");
    if (!$dbc) {
        echo('Could not connect to MySQL: ' . mysqli_connect_error());
    }
    return $dbc;
}

function closedb($dbc) {
    mysqli_close($dbc);
}

function cleanFileName($string) {
    $string = str_replace(" ", "-", $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function insert_video_to_huddle($file_name, $folder_loc) {

    $dbc = opendb();

    $title = addslashes($_POST['title']);

    //new filename

    $new_file_name = str_replace(basename($file_name), cleanFileName($title) . ".MOV", $file_name);
    rename($file_name, $new_file_name);

    $video = addslashes(basename($new_file_name));
    $video_huddle_id = (int) $_POST['video_huddle_id'];
    $user_id = (int) $_POST['user_id'];
    $date_now = date('Y-m-d H:i:s', strtotime('now'));
    $query = "INSERT INTO videos (title, video, created_at, updated_at, video_huddle_id, user_id, processed, is_webservice, raw_path) VALUES (
		'$title',
		'$video',
		'$date_now',
		'$date_now',
		'$video_huddle_id',
		$user_id,
		'0',
		'1',
		'$folder_loc'
	)";

    //echo $query;
    mysqli_query($dbc, $query);

    closedb($dbc);
}

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-type: application/json');

// Settings
$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";


$base = '/';
$relative_path_thumb = $base . 'tmp';
$root_path = $_SERVER['DOCUMENT_ROOT'] . $relative_path_thumb;
$targetDir = $root_path;

$targetDir .= DIRECTORY_SEPARATOR;

// Create target dir
if (!file_exists($targetDir))
    @mkdir($targetDir);

$key = "";

if (empty($_POST['id'])) {
    $key = strtotime('now');
} else {
    $key = $_POST['id'];
}

$targetDir .= "$key" . DIRECTORY_SEPARATOR;

$cleanupTargetDir = true; // Remove old files
$maxFileAge = 5 * 3600; // Temp file age in seconds
// 5 minutes execution time
@set_time_limit(5 * 60);

// Uncomment this one to fake upload time
// usleep(5000);
// Get parameters
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

// Clean the fileName for security reasons
$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

// Make sure the fileName is unique but only if chunking is disabled
if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
    $ext = strrpos($fileName, '.');
    $fileName_a = substr($fileName, 0, $ext);
    $fileName_b = substr($fileName, $ext);

    $count = 1;
    while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
        $count++;

    $fileName = $fileName_a . '_' . $count . $fileName_b;
}

$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

// Create target dir
if (!file_exists($targetDir))
    @mkdir($targetDir);

// Remove old temp files	
if ($cleanupTargetDir) {
    if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
        while (($file = readdir($dir)) !== false) {
            $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

            // Remove temp file if it is older than the max age and is not the current file
            if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                @unlink($tmpfilePath);
            }
        }
        closedir($dir);
    } else {
        die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
    }
}

// Look for the content type header
if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
    $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

if (isset($_SERVER["CONTENT_TYPE"]))
    $contentType = $_SERVER["CONTENT_TYPE"];

// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
if (strpos($contentType, "multipart") !== false) {
    if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
        // Open temp file
        $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
        if ($out) {
            // Read binary input stream and append it to temp file
            $in = @fopen($_FILES['file']['tmp_name'], "rb");

            if ($in) {
                while ($buff = fread($in, 4096))
                    fwrite($out, $buff);
            }
            else
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            @fclose($in);
            @fclose($out);
            @unlink($_FILES['file']['tmp_name']);
        }
        else
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
    }
    else
        die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."' . $targetDir . '-' . $_FILES['file']['tmp_name'] . '}, "id" : "id"}');
} else {
    // Open temp file
    $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
    if ($out) {
        // Read binary input stream and append it to temp file
        $in = @fopen("php://input", "rb");

        if ($in) {
            while ($buff = fread($in, 4096))
                fwrite($out, $buff);
        }
        else
            die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

        @fclose($in);
        @fclose($out);
    }
    else
        die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
}

// Check if file has been uploaded
if (!$chunks || $chunk == $chunks - 1) {
    // Strip the temp .part suffix off

    $old_path_parts = pathinfo($filePath);

    $old_file_name_only = $old_path_parts['filename'];
    $old_file_ext_only = $old_path_parts['extension'];
    $new_file_name = $old_path_parts['dirname'] . '/' . cleanFileName($old_file_name_only) . '.' . $old_file_ext_only;
    rename("{$filePath}.part", $new_file_name);
    insert_video_to_huddle($new_file_name, $targetDir);
}

die('{"jsonrpc" : "2.0", "result" : null, "id" : "' . $key . '"}');
