<?php
require_once('class.phpmailer.php');

class dMailer extends PHPMailer
{
	var $priority = 3;
	var $to_name;
	var $to_email;
	var $From = null;
	var $FromName = null;
	var $Sender = null;
	
	function dMailer($from=$this->custom->get_site_settings('static_emails')['noreply'], $to='', $subject='', $body='', $html='', $replyto='', $bcc='')
	{
		if (!isset($from)) return;
		if (empty($to)) return;
		
		
		$account = $this->getFromMailSettings($from);
		
		if (isset($account)) {
			
			$this->SMTPAuth = true;
			$this->IsSMTP(); // enable SMTP
			$this->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
			$this->SMTPAuth = true;  // authentication enabled
			$this->SMTPSecure = $account['protocol']; // secure transfer enabled REQUIRED for Gthis
			$this->Host = $account['smtp'];
			$this->Port = $account['port']; 
			$this->Username = $account['from'];  
			$this->Password = $account['password'];           
			$this->SetFrom($from, $account['from_name'], 0);
			$this->Subject = $subject;
			$this->Body = $body;
			$this->IsHTML($html);
			
			if (empty($replyto)) $replyto = $from;
			$this->AddReplyTo($replyto, $replyto);
			
			if (!empty($bcc)) {
				$this->AddBCC($bcc, $bcc);
			}
			
			$to_array = array();
			
			if (is_array($to)) {
				$to_array = $to;
			} else {
				$to_array = explode(',', $to);
			}
			
			for ($i=0; $i < count($to_array); $i++) {
				$this->AddAddress($to_array[$i]);
			}
			
			$this->Send();
			
			$this->ClearAddresses();
			$this->ClearAttachments();
		}
	}
	
	function dMailerWAttachment($from, $to, $subject, $body, $html, $replyto='', $bcc='', $attachments = array())
	{
		
		$account = $this->getFromMailSettings($from);
		
		if (isset($account)) {
			
			$this->SMTPAuth = true;
			$this->IsSMTP(); // enable SMTP
			$this->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
			$this->SMTPAuth = true;  // authentication enabled
			$this->SMTPSecure = $account['protocol']; // secure transfer enabled REQUIRED for Gthis
			$this->Host = $account['smtp'];
			$this->Port = $account['port']; 
			$this->Username = $account['from'];  
			$this->Password = $account['password'];           
			$this->SetFrom($from, $from);
			$this->Subject = $subject;
			$this->Body = $body;
			$this->IsHTML($html);
			
			if (empty($replyto)) $replyto = $from;
			$this->AddReplyTo($replyto, $replyto);
			
			if (!empty($bcc)) {
				$this->AddBCC($bcc, $bcc);
			}
			
			if (count($attachments) >0) {				
				for ($i=0; $i < count($attachments); $i++) {
					
					$attachment = $attachments[$i];
					$this->AddAttachment($attachment['file_path'], $attachment['file_name']);					
				}				
			}			
			
			$this->AddAddress($to);
			$this->Send();
			
			$this->ClearAddresses();
			$this->ClearAttachments();
		}
	}

	
	function getFromMailSettings($from) {
		
		
		return array(
			'from' => 'davew@sibme.com',
			'smtp' => 'smtp.sendgrid.net',
			'port' => '587',
			'password' => 'Er9m2hSdfc',
			'protocol' => 'tls',
			'from_name' => $this->custom->get_site_settings('static_emails')['noreply'],
		);
	}
	
}


?> 