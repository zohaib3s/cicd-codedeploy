<?php

class ContactController extends AppController {

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $this->layout = 'minimal';
    }

    function public_contact_us() {

        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->clean_string($this->data['email']);
        $this->Email->to = Configure::read('to');
        $this->Email->subject = $this->getSubject($_POST['routing']);
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $email_message = "Below you'll find all the email addresses and telephone numbers you need to contact. \n\n";
        $email_message .= "Name: " . $this->clean_string($this->data['name']) . "\n";
        $email_message .= "Email: " . $this->clean_string($this->data['email']) . "\n";
        $email_message .= "Phone: " . $this->clean_string($this->data['phone']) . "\n";
        $email_message .= "Message\n: " . $this->clean_string($this->data['message']) . "\n";

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => '',
            'email_from' => $this->data['email'],
            'email_to' => Configure::read('to'),
            'email_subject' => $this->getSubject($_POST['routing']),
            'email_body' => $this->data['message'],
            'is_html' => false,
            'sent_date' => date("Y-m-d H:i:s")
        );
        $this->AuditEmail->save($auditEmail, $validation = TRUE);

        if ($this->Email->send($email_message) > 0) {
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }

    function success() {
        $this->render('success');
    }

    function getSubject($key) {
        switch ($key) {
            case 1:
                return 'General Questions';
                break;
            case 2:
                return 'Technology Support';
                break;
            case 3:
                return 'Request Demo';
                break;
        }
    }

    function clean_string($string) {
        $bad = array("content-type", "bcc:", "to:", "cc:", "href");
        return str_replace($bad, "", $string);
    }

}
