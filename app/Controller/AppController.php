<?php

use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Aws\ElasticTranscoder\ElasticTranscoderClient;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe\DataMapping\StreamCollection;
use FFMpeg\FFProbe\DataMapping\Stream;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Filters\Video\ResizeFilter;
// use Intercom\IntercomClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
App::uses('Subscription', 'Braintree');

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('Helper', 'Custom');
App::uses('Validation', 'Utility');
App::import('Vendor', 'PHPExcel');
App::uses('Sanitize', 'Utility');
App::import('Vendor', 'MailChimp');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package     app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    var $helpers = array('Minify.Minify', 'Custom');
    var $MailChimp = '';
    public $uses = array('User', 'Account', 'Comment', 'UserAccount', 'AuditEmail', 'AuditErrors',
        'AccountFolder', 'AccountFolderGroup', 'UserGroup', 'AccountFolderUser', 'Document','LiveClusters','ClusterStreamSlots',
        'AccountFolderDocument', 'AccountFolderDocumentAttachment', 'UserActivityLog', 'Plans',
        'AccountFolderMetaData', 'EmailUnsubscribers', 'EmailFormats', 'CommentAttachment',
        'AccountFolderSubject', 'AccountFolderTopic', 'Group', 'JobQueue', 'DocumentFiles', 'Subject', 'Topic',
        'AccountCommentTag', 'AccountTag', 'AccountMetaData', 'StudentPayment', 'Transaction', 'AccountBraintreeSubscription',
        'EdtpaAssessments', 'EdtpaAssessmentAccounts', 'EdtpaAssessmentAccountCandidates', 'EdtpaAssessmentPortfolioCandidateSubmissions',
        'EdtpaAssessmentPortfolioNodes', 'EdtpaAssessmentPortfolios', 'EdtpaAssessmentAccountCandidateTransfers', 'DocumentViewerHistory', 'DocumentStandardRating',
        'check_subscription','AnalyticsLoginSummary','AnalyticsHuddlesSummary', 'AccountFrameworkSetting', 'AccountFrameworkSettingPerformanceLevel', 'PerformanceLevelDescription', 'Sites', 'UserDeviceLog', 'SendgridTemplates');
    public $components = array(
        'Session',
        'Security',
        'Email',
        'Cookie',
        'PhpExcel',
        'RequestHandler',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'Dashboard', 'action' => 'home'),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
            'unauthorizedRedirect' => '/users/login',
        )
    );
    public $role_account_owner = '100';
    public $role_super_user = '110';
    public $role_user = '120';
    public $role_viewer = '125';
    public $role_admin = '115';
    public $role_huddle_admin = '200';
    public $role_huddle_user = '210';
    public $role_huddle_viewer = '220';
    var $dir = 'files/tempupload';
    var $uploadDir = '';
    var $site_id = '';
    public $custom = '';
    var $subject_title = '';
    public $template_name_postfix = '';
    public $website_url = 'www.sibme.com';
    public $language_based_messages = array();
    public $idp_link_for_ng = '';

    public $thinkific_api_url = "https://api.thinkific.com/api/public/v1";
    public $thinkific_api_key = "96d633126b778e4e959e44c25c5b8030";
    public $thinkific_sub_domain = "sibmeresourcecenter";
    public $activities = [
        '2-4-1' => 'video_upload_counts',
        '2-4-3' => 'workspace_upload_counts',
        '2-4-2' => 'library_upload_counts',
        '22-1' => 'shared_upload_counts',
        '22-2' => 'library_shared_upload_counts',
        '11-1' => 'videos_viewed_count',
        '11-3' => 'workspace_videos_viewed_count',
        '11-2' => 'library_videos_viewed_count',
        '1' => 'huddle_created_count',
        '5-1' => 'comments_initiated_count',
        '5-3' => 'workspace_comments_initiated_count',
        '8' => 'replies_initiated_count',
        '3-29-1' => 'documents_uploaded_count',
        '13-1' => 'documents_viewed_count',
        '23' => 'scripted_observations',
        '20' => 'scripted_video_observations',
        'total_hours_uploaded' => 'total_hours_uploaded',
        'total_hours_viewed' => 'total_hours_viewed',
        '3-29-3' => 'workspace_resources_uploaded',
        '13-3' => 'workspace_resources_viewed',
        '6' => 'huddle_discussion_created',
        '8' => 'huddle_discussion_posts',
        '26' => 'scripted_notes_shared_upload_counts',
        '24' => 'live_sessions_count',
    ];
    public $redirectJsonData = [];
    public function __construct($request = null, $response = null) {

        $this->uploadDir = WWW_ROOT . $this->dir;
        parent::__construct($request, $response);
    }

    public function beforeFilter() {
        ob_start();
        if ($_SESSION['LANG'] == 'es') {
            @setlocale(LC_TIME, "es_ES");
        } else {
            setlocale(LC_TIME, "US");
        }
        $this->Security->unlockedFields = array();
        $this->Security->validatePost = false;
        $this->Security->csrfCheck = false;
        $this->check_configuration();
        $view = new View($this, false);
        $this->MailChimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
        $this->custom = $view->Custom;

        $site_url = $view->Custom->get_site();
        $this->language_based_messages = $view->Custom->get_page_lang_based_content('flash_messages');
        /*
        if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
            $redirect = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        } else {
            $redirect = 'https://' . $_SERVER['HTTP_HOST'] . '/';
        }
        */
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . '/';

        @Configure::write('sibme_base_url', $redirect);

        if ($this->params['controller'] != 'Api') {
            $use_ssl = Configure::read('use_ssl');
            if ($use_ssl == '1') {
                $this->Security->blackHoleCallback = 'forceSSL';
                $this->Security->requireSecure();
            }
        }
        /*
        // This code is incorrect because it always sets site_id to 1. The reason is that there's no place where site_id is being set in session. Instead of that We should set site_id based on the domain.
        if (!empty($this->Session->read('site_id')) && $this->Session->read('site_id') != 1) {
            $this->site_id = $this->Session->read('site_id');
        } else {
            $this->site_id = 1;
        }
        */
        $this->site_id = 1;
        if(stristr($_SERVER['HTTP_HOST'], "hmh")){
            $this->site_id = 2;
        }

        /* if ($this->params['controller'] == 'Folder') {
          //echo 'new';
          //$bcrumb = array();
          //$this->Session->write('breadcrumb', $bcrumb);
          }else{
          $ctrlName = $this->params['controller'];
          if($ctrlName == 'Folder'){
          echo 'test';
          $bcrumb = array();
          $this->Session->write('breadcrumb', $bcrumb);
          }
          //$bcrumb = array();
          //$this->Session->write('breadcrumb', $bcrumb);
          } */

        ini_set('memory_limit', '-1');

        $this->Auth->allow(
                'sysRefreash','get_language_based_content_for_js','view_qr','comments_ajax', 'register', 'login_activate_link', 'student_payment', 'getVideoDuration', 'set_validation', 'login', 'reset', 'change_password', 'no_permission', 'account_settings', 'forgot_password', 'received_notification', 'activation', 'signup', 'public_contact_us', 'success', 'send_welcome_sibme_emails', 'amazon_jnlp_signature', 'unsubscirbe_now', 'amazon_jnlp_success', 'check_is_account_expired', 'uploadVideos', 'uploadDocuments', 'removeTempFolder', 'anyvideos', 'externaluploadVideos', 'get_folder_size', 'login_by_token', 'assessment_zipfile_maker', 'Portfolio_Package_Retrieval', 'Portfolio_Package_Transfer_Status', 'Portfolio_Package_Transfer_Cancel', 'activate_user', 'update_password',
                'error400', 'signup_google', 'account_settings_all', 'activate_user_student_payment', 'monthly_report', 'weekly_report', 'sendReportAnotherEmail', 'live_stream_force_stop', 'syncWithMailchimpGlobal', 'angular_header_api', 'video_details', 'add_viewer_history', 'getLTIAttachedDocuments', 'edTPA_assessments_transfer', 'Portfolio_Package_Retrieval_Notification', 'insert_collaboration_huddle_data', 'add_video_duration_cron', 'get_sendgrid_all_templates', 'get_sendgrid_templates', 'migrate_accounts', 'language_update_in_cookie_session', 'session_update_lumen_side','get_counts_of_comment_tags','async_create_churnzero_attribute','insert_workspace_old_videos_data','syncUsersWithHubspot','update_user_image_in_session','update_session_from_lumen','mobile_trimming',
                'restore_sample_huddles','cancel_account_api','fetch_account_plan_details_angular', 'cron_for_stoping_live_videos' , 'check_card_expiration_failure', 'create_live_recording', 'logout', 'checkDocumentFilesRecordExist','submit_login_request','login_from_lti','isUserExist'
        );
        // set cookie options
        $this->Cookie->key = 'qSI232qs*&sXOw!adre@34SAv!@*(XSL#$%)asGb$@11~_+!@#HKis~#^';
        $this->Cookie->httpOnly = true;

        if ($this->_validate_request()) {
            $this->Auth->allow($this->params['action']);
        }
        if ($this->Auth->loggedIn())
        {
            $this->switch_account_if_required();
        }

        $isNotLoginPageRequest = !($this->params['controller'] == 'Users' && $this->params['action'] == 'login');
        if ($this->Auth->loggedIn()) {
            if ($isNotLoginPageRequest) {
                $current_user = $this->Session->read('user_current_account');
                if (!isset($current_user)){
                    $this->redirect($this->Auth->logout());
                }

                $result = $this->User->get($current_user['User']['id']);
                $this->Session->write('user_accounts', $result);
                
                $this->validate_with_sso_if_required();
            } else {
                // $this->redirect($this->Auth->logout());
                // This block executes if user is already logged in but users/login route is hit.
                if($this->hasCurrentUserMultipleAccounts()){
                    $this->redirect("/launchpad");
                } else {
                    $this->redirect("/home/profile-page");
                }
            }
        } else {
            $cookie = $this->Cookie->read('remember_me_cookie');
            if ($cookie && $isNotLoginPageRequest) {
                $user = $this->User->find('first', array(
                    'conditions' => array(
                        'User.username' => $cookie['username'],
                        'User.password' => $cookie['password'],
                        'User.is_active' => '1'
                    )
                ));

                if ($user && !$this->Auth->login($user)) {
                    // destroy session & cookie
                    $this->hard_logout();
                    // $this->redirect('/Users/logout');
                }
            }

            /**/ 
            // New Remember Me Authentication Logic is Below
            $authentication_token = $this->request->header('Authentication-Token');
            if (!empty($authentication_token) && $isNotLoginPageRequest) {
                $user = $this->User->find('first', array(
                    'conditions' => array(
                        'User.authentication_token' => $authentication_token,
                        'User.is_active' => '1'
                    )
                ));
                $user = $user["User"];
                if ($user && !$this->Auth->login($user)) {
                    // destroy session & cookie
                    $this->hard_logout();
                    // $this->redirect('/Users/logout');
                }
                $this->setupPostLogin(true);
                // return $this->redirect('/home/launchpad');
            }
            /**/
        }

        $cookieName = Configure::read("Session.cookie");
        if ($this->Auth->loggedIn() && isset($_COOKIE[$cookieName])) {
            $session_delay = Configure::read("Session.timeout") * 60 * 2;
            setcookie($cookieName, $_COOKIE[$cookieName], time() + $session_delay, "/");
        }

        

        $site_title = $view->Custom->get_site_settings('site_title');
        if (empty($site_title)) {
            $site_title = 'Sibme';
        }
        $this->set('title_for_layout', $site_title);
        $this->subject_title = $this->custom->get_site_settings('email_subject');
        $this->template_name_postfix = $this->site_id . '_EN';
        if ($this->params['action'] != 'session_update_lumen_side' && $this->params['action'] != 'get_translation_settings') {
            if (isset($_COOKIE['LANG']) && !empty($_COOKIE['LANG'])) {
                $lang = $_COOKIE['LANG'];
                $this->Session->write('LANG', $_COOKIE['LANG']);
            } else {
                setcookie('LANG', '', time() - 3600, '/');
                setcookie('LANG', $this->getPreferredLanguage(), time() + 2678400, '/');
                $lang = $this->getPreferredLanguage();
                $this->Session->write('LANG', $this->getPreferredLanguage());
            }
        }
        if ($this->site_id == '2') {
            setcookie('LANG', '', time() - 3600, '/');
            setcookie('LANG', 'en', time() + 2678400, '/');
            $this->Session->write('LANG', 'en');
        }
        $this->is_account_expired();
        // if a user is logged in
        $this->_get_permissions();
        $params = array();
        $this->set('sibme_learning_center_modal', $view->element('sibme_learning_center_modal', $params));
        ob_end_flush();
    }

    public function beforeRender() {
        if ($this->params['controller'] == 'Folder') {
            //echo 'new';
            //$bcrumb = array();
            //$this->Session->write('breadcrumb', $bcrumb);
        } else {
            $bcrumb = array();
            $this->Session->write('breadcrumb', $bcrumb);
        }
    }

    public function validate_with_sso_if_required($account_id=null){
        if($this->skip_urls_from_sso()){
            return;
        }
        $current_user = $this->Session->read('user_current_account');
        $account_id = !empty($account_id) ? $account_id : $current_user['accounts']['account_id'];

        $user_accounts = $this->Session->read('user_accounts');
        $account_ids = [];
        foreach($user_accounts as $account){
            $account_ids [] = $account["users_accounts"]["account_id"];
        }

        if (!empty($account_id) && in_array($account_id, $account_ids)) {
            $this->validate_with_sso($account_id, $account_ids);
        }
    }

    public function skip_urls_from_sso(){
        /**
         * Check if Current URL is mandatory for SSO Auth, otherwise check if it needs to be skipped.
         */
        if($this->mandatory_urls_for_sso()){
            return false;
        }

        $current_url = $this->custom->getCurrentURL();

        $urls_parts = ["/launchpad","Errors/view_exception", "app/impersonate_user", "Huddles/", "api/", "get_language_based_content_for_js", "logout", "Users/activation", "update_session_from_lumen"];
        $skip_me = false;
        foreach($urls_parts as $value){
            if(stristr($current_url,$value)){
                $skip_me = true;
            }
        }
        return $skip_me;
    }

    public function mandatory_urls_for_sso(){
        $current_url = $this->custom->getCurrentURL();
        $mandatory_urls = ["api/angular_header_api"];
        $mandatory = false;
        foreach($mandatory_urls as $value){
            if(stristr($current_url,$value)){
                $mandatory = true;
            }
        }
        return $mandatory;
    }

    public function forceSSL() {
        $this->redirect('https://' . env('SERVER_NAME') . $this->here);
        return;
    }

    public function redirect_on_account_settings() {
        $user_current_account = $this->Session->read('user_current_account');
        //  $this->redirect('/accounts/account_settings_all/' . $user_current_account['accounts']['account_id'] . '/1');
        // $this->redirect('/accounts/deactivated_account_redirect/' . $user_current_account['accounts']['account_id']);
        // exit;
        echo $this->getRedirectJson('/accounts/deactivated_account_redirect/' . $user_current_account['accounts']['account_id'], true);
        exit;
    }

    function _get_permissions() {
        $current_user = $this->Session->read('user_current_account');

        $account_id = $current_user['accounts']['account_id'];
        $user_id = $current_user['User']['id'];
        $permissions = $this->UserAccount->get($account_id, $user_id);

        $this->Session->write('user_permissions', $permissions);
    }

    function get_huddle_level_permissions($account_folder_id) {
        $current_user = $this->Session->read('user_current_account');
        $user_id = $current_user['User']['id'];
        $permissions = $this->AccountFolderUser->getUserAccount($account_folder_id, $user_id);

        if (isset($permissions) && isset($permissions['AccountFolderUser'])) {
            $this->Session->write('user_huddle_level_permissions', $permissions['AccountFolderUser']['role_id']);
        } else {
            $permissions = $this->UserGroup->get_acount_folder_user_group($account_folder_id, $user_id);
            if (isset($permissions) && isset($permissions['account_folder_groups'])) {
                $this->Session->write('user_huddle_level_permissions', $permissions['account_folder_groups']['role_id']);
            } else {
                $this->Session->write('user_huddle_level_permissions', "220");
            }
        }
    }

    public function sysRefreash() {
        //echo "1";
        $huddle_id = $this->Session->read('account_folder_id');
        $created_date = $this->request->data('created_date');
        $current_user = $this->Session->read('user_current_account');
        if(empty($current_user))
        {
            $result = ["status"=>false,"message"=>"Please login to continue","ok"=>0];
            echo json_encode($result);
            exit;

            /*
            if(!empty($this->request->query('json_response')))
            {
                $result = ["status"=>false,"message"=>"Please login to continue","ok"=>0];
                echo json_encode($result);
                exit;
            }
            else
            {
                $this->redirect('/');
            }
            */
        }
        $account_id = $current_user['accounts']['account_id'];
        $processing_video_ids = $this->request->data('processing_video_ids');
        $video_envo = $this->request->data('video_envo');
        $user_id = $current_user['User']['id'];

        if (empty($account_id))
            $account_id = 0;
        if (empty($created_date))
            $created_date = 0;

        $list = $this->Document->find('all', array(
            'conditions' => array(
                'id' => $processing_video_ids,
                'published' => 1
            ),
            'fields' => array('id', 'url', 'thumbnail_number', 'encoder_provider', 'published', 'encoder_status')
        ));

        $list_live_videos = $this->Document->find('first', array(
            'conditions' => array(
                'account_id' => $account_id,
                'doc_type' => 4,
                'is_processed' => array(5, 6)
            ),
            'fields' => array('id', 'url', 'thumbnail_number', 'encoder_provider', 'published', 'encoder_status'),
            'order' => array('id' => 'DESC')
        ));




        $view = new View($this, false);

        $send_push = '';
        $send_push = $view->Custom->check_to_send_live_push($list_live_videos['Document']['id'], $user_id);

        $data = array();


        foreach ($list as $item) {

            $doc = $item['Document'];
            $document_files_array = $this->get_document_url($doc);

            if (empty($document_files_array['url'])) {
                $document_files_array['url'] = $doc['url'];
                $doc['encoder_status'] = $document_files_array['encoder_status'];
            } else {
                $doc['encoder_status'] = $document_files_array['encoder_status'];
            }

            $data[$doc['id']] = $view->Html->image($document_files_array['thumbnail']);
        }

        $result = array('ok' => 1);

        if ($video_envo == '1') {
            if (!empty($huddle_id)) {
                $latest_video_created_date = $this->UserAccount->query("SELECT
              d.`created_date`
            FROM
              `account_folders` AS af
              #join `account_folder_users` as afu
                #on af.`account_folder_id` = afu.`account_folder_id`
              JOIN `account_folder_documents` AS afd
                ON afd.`account_folder_id` = af.`account_folder_id`
              JOIN `documents` AS d
                ON d.`id` = afd.`document_id`
            WHERE d.`doc_type` IN (1, 3)
            AND af.`folder_type` = 1
              AND af.`account_id` = " . $account_id . "
              AND af.`account_folder_id`= " . $huddle_id . "
            AND d.`created_date` > '" . $created_date . "'
            AND d.site_id = " . $this->site_id . "
            ORDER BY d.`id` DESC
            LIMIT 1 ");
            } else {
                $latest_video_created_date = false;
            }
        } else {
            $latest_video_created_date = $this->UserAccount->query("SELECT
              d.`created_date`
            FROM
              `account_folders` AS af
              #join `account_folder_users` as afu
                #on af.`account_folder_id` = afu.`account_folder_id`
              JOIN `account_folder_documents` AS afd
                ON afd.`account_folder_id` = af.`account_folder_id`
              JOIN `documents` AS d
                ON d.`id` = afd.`document_id`
            WHERE d.`doc_type` IN (1, 3)
              AND af.`folder_type` = 3
              AND af.`account_id` = " . $account_id . "
              AND d.`created_date` > '" . $created_date . "'
              AND af.site_id = " . $this->site_id . "
            ORDER BY d.`id` DESC
            LIMIT 1 ");
        }
        $result['latest_video_created_date'] = $latest_video_created_date[0]['d']['created_date'];
        if (!empty($data)) {
            $result['data'] = $data;
        }
        if (!empty($send_push)) {
            $huddle_details = $this->AccountFolder->find('first', array(
                'conditions' => array(
                    'account_folder_id' => $send_push['huddle_id']
            )));

            $document_details = $this->Document->find('first', array(
                'conditions' => array(
                    'id' => $send_push['video_id']
            )));

            $user_details = $this->User->find('first', array(
                'conditions' => array(
                    'id' => $document_details['Document']['created_by']
            )));

            $result['send_push'] = true;
            $result['video_id'] = $send_push['video_id'];
            $result['huddle_id'] = $send_push['huddle_id'];
            $result['huddle_name'] = $huddle_details['AccountFolder']['name'];
            $result['user_name'] = $user_details['User']['first_name'] . ' ' . $user_details['User']['last_name'];
        }

        echo json_encode($result);

        exit;
    }

    function _validate_request() {
        if (!empty($_REQUEST['auth_token'])) {
            $auth_token = $_REQUEST['auth_token'];
            $this->api_user = $this->User->getByHash($auth_token);

            if (!$this->api_user) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    public function setupPostLogin($switch_account = false, $environment_type = '2',$account_id = '') {

        $logged_in_user = $this->Auth->user('id');
         
        if(!empty($account_id))
        {
            $session_account_id = $account_id;
        }
        $user_accounts_result = $this->User->get($logged_in_user);

        if($user_accounts_result[0]['User']['is_active'] == '0' || $user_accounts_result[0]['User']['type'] != 'Active' )
        {
            return false;
        }
       
        $user_accounts =array();
        if($user_accounts_result ){
            foreach($user_accounts_result as $row){
                if( $row['users_accounts']['is_active'] == 0){
                    continue;
                }
                $user_accounts[] = $row;
            }
        }
        if(count($user_accounts) == 0){
            return false;
        }
        
       

        foreach ($user_accounts as $user_account) {

            if ($user_account['roles']['role_id'] == 100 || $user_account['roles']['role_id'] == 110) {
                $update_array = array(
                    'permission_maintain_folders' => 1,
                    'permission_access_video_library' => 1,
                    'permission_video_library_upload' => 1,
                    'permission_administrator_user_new_role' => 1,
                    'parmission_access_my_workspace' => 1,
                    'manage_collab_huddles' => 1,
                    'manage_coach_huddles' => 1,
                    'manage_evaluation_huddles' => 1,
                    'folders_check' => 1,
                );

                $this->UserAccount->updateAll($update_array, array('account_id' => $user_account['users_accounts']['account_id'], 'user_id' => $user_account['User']['id']), $validation = TRUE);
            }

            if ($user_account['accounts']['in_trial'] == 1) {
                //$this->AccountFolderMetaData->updateAll(array('meta_data_value' => 0), array('account_folder_id' => $user_account['users_accounts']['account_id'], 'meta_data_name' => 'enable_tags'), $validation = TRUE);
            }
        }

        $user_default_account = $this->User->getDefaultAccount($logged_in_user);
        //$user_accounts[0]['User']['is_active'] == 0 ||

        if ($user_accounts[0]['users_accounts']['is_active'] == 0)
            $this->isAuthorized();
        if (isset($user_default_account) && isset($user_default_account['accounts']) && $environment_type == 2) {

            //100 Or 110 (Owner OR SuperUser)

            $user_activity_logs = array(
                'ref_id' => $logged_in_user,
                'desc' => 'Logged in',
                'url' => $this->base . '/users/login',
                'type' => '9',
                'account_id' => $user_default_account['accounts']['account_id'],
                'account_folder_id' => '',
                'environment_type' => $environment_type,
            );
            $account_id = $user_default_account['accounts']['account_id'];
            $user_id = $user_account['User']['id'];
            $this->saveRecord($account_id, $user_id);
            $this->user_activity_logs($user_activity_logs, $user_default_account['accounts']['account_id'], $logged_in_user);
            $this->create_churnzero_event('User+Login', $user_default_account['accounts']['account_id'], $user_default_account['User']['email']);

        }

       

        foreach ($user_accounts as $user_account) {

            if ($user_account['users_accounts']['account_id'] != $user_default_account['accounts']['account_id']) {

                $user_activity_logs = array(
                    'ref_id' => $logged_in_user,
                    'desc' => 'Logged in',
                    'url' => $this->base . '/users/login',
                    'type' => '9',
                    'account_id' => $user_account['users_accounts']['account_id'],
                    'account_folder_id' => '',
                    'environment_type' => $environment_type,
                );
                $account_id = $user_account['users_accounts']['account_id'];
                $user_id = $user_default_account['User']['id'];
                $this->saveRecord($account_id, $user_id);
                $this->user_activity_logs($user_activity_logs, $user_account['users_accounts']['account_id'], $logged_in_user);
                $this->create_churnzero_event('User+Login', $user_account['users_accounts']['account_id'] , $user_default_account['User']['email']);
            }
        }

        $this->User->updateAll(["last_sign_in_at" => '"' .date('Y-m-d H:i:s').'"' ], array('id' => $logged_in_user));

        $this->Session->write('user_accounts', $user_accounts);
        $this->Session->write('user_default_account', $user_default_account);
        $this->Session->write('totalAccounts', count($user_accounts));
        $this->Session->write('user_current_account', $user_accounts);
        
        if (isset($user_default_account) && isset($user_default_account['accounts'])) {
            if(isset($session_account_id) && !empty($session_account_id))
            {
                $default_account_id = $session_account_id;
            }
            else
            {
                $default_account_id = $user_default_account['accounts']['account_id'];
            }
        }
        if (isset($default_account_id) && $this->Account->is_account_expired($default_account_id)) {
            // return false;
            return ['success'=>false, 'is_expired'=>true, 'message'=>'Account ID '.$default_account_id.' is Expired'];
        }
        if ($switch_account == true && isset($default_account_id)) {
            $this->switch_account($default_account_id);
        }
        return true;
    }

    function digitalKey($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }

    function switch_account($account_id) {
        
        $current_user = $this->Session->read('user_current_account');
        $current_user_account_id = $current_user['accounts']['account_id'];
        $user_accounts = $this->Session->read('user_accounts');
        $account_ids = [];
        foreach($user_accounts as $account){
            $account_ids [] = $account["users_accounts"]["account_id"];
        }

        if (!empty($account_id) && in_array($account_id, $account_ids)) {
            if(!empty($current_user_account_id) && $current_user_account_id != $account_id){
                // User wants to switch his account so we'd need to remove his current sso validity.
                $this->Session->delete('sso_authenticated');
            }
            // $this->validate_with_sso($account_id, $account_ids);

            // echo "<pre>";
            // echo "here";
            // echo "</pre>";
            // die;
            if (!$_GET['stoh'] == 1) {
                //$this->redirect('/launchpad');
                $this->validate_with_sso_if_required($account_id);
            }
            

            if(!empty($this->idp_link_for_ng)){
                // Break execution here because we first need authentication from IDP
                return;
            }
            for ($i = 0; $i < count($user_accounts); $i++) {
                $user_account = $user_accounts[$i];
                if ($user_account['accounts']['account_id'] == $account_id) {

                    $this->Session->write('user_current_account', $user_account);
                    $this->_get_permissions();

//                    if ($user_account['roles']['name'] == 'Account Owner') {
//                        $this->Session->setFlash('You are logged in as an <span style="color:#000"> ' . $user_account['roles']['name'] . '</span>', 'default', array('class' => 'success'));
//                    } else {
//                        $this->Session->setFlash('You are logged in as a <span style="color:#000"> ' . $user_account['roles']['name'] . '</span>', 'default', array('class' => 'success'));
//                    }


                    $this->Session->write('user_current_account_id', $account_id);
                    $this->Session->write('role', $user_account['roles']['role_id']);
                }
            }
            if ($_GET['stoh'] == 1) {
                $this->redirect('/home/launchpad');
            }
        } else {
            $cType = $_SERVER['HTTP_ACCEPT'];
            if (strpos($cType,'application/json') !== false) {
                //this is from angular if we execute $this->redirect angular stucks and keep loading
            }
            else
            {
                // $this->Session->setFlash($this->language_based_messages['please_choose_an_account_from_launchpad_msg'], 'default', array('class' => 'message error'));
                $this->redirect('/home/launchpad?error='.$this->language_based_messages['please_choose_an_account_from_launchpad_msg']);
            }
        }
    }

    public function validate_with_sso($account_id, $account_ids){
        // 1. (if) Check if Force SSL is enabled for $user_account_id and is_post is FALSE.
        // -- If yes then create a curl call to get/sso-link. 
        // -- If get a correct link then redirect to IDP for auth.
        // -- IDP redirects user back to the current url.
        // 2. (elseif) Check if Force SSL is enabled for $user_account_id and is_post is TRUE and authentication_token is not empty.
        // -- If all of above is true then match the authentication_token with current logged in user_id
        // -- If above is not true then redirect to "/launchpad" with appropreate message.
        // 3. (else) If force SSL is not enabled then continue to current code.
        $view = new View($this, false);
        $current_url = $view->Custom->getCurrentURL();

        // $sso_auth = $this->Session->read('sso_auth_'.$account_id);
        $sso_auth = $this->Session->read('sso_authenticated');

        if( !empty($sso_auth) ){
            // ignore sso auth for these calls.
            return;
        }

        $result = $this->Account->find('first', array(
            'conditions' => ['Account.id' => $account_id],
            'fields' => ['is_force_sso_enabled']
        ));
        $email = $this->request->query('email');
        $authentication_token = $this->request->query('authentication_token');
        $referer = urldecode($_SERVER['HTTP_REFERER']);
        if(isset($referer) && !empty($referer) && stristr($referer, 'authentication_token') && empty($email) && empty($authentication_token)){
            $email = $this->custom->getParamFromUrl($referer,'email');
            $authentication_token = $this->custom->getParamFromUrl($referer,'authentication_token');
        }

        if( $result && current($result)['is_force_sso_enabled'] && empty($email) && empty($authentication_token) ) {
            if( !empty($account_id) && count($account_ids) > 1 && in_array($account_id, $account_ids) && stristr($current_url,"login") ) {
                // Return if user has multiple accounts and he is logging in using Regular Sibme Login. He should be redirected to Launchpad instead of IDP.
                return;
            }
            $idp_link = $this->getIDPLink($account_id, $current_url);
            if(stristr($current_url,"api/angular_header_api") || stristr($current_url,"users/login_api")){
                // This is from Angular, so we have to return IDP Link to NG so they can perform a redirect.
                // $this->idp_link_for_ng = $idp_link;
                $this->Session->write('idp_link_for_ng',$idp_link);
            } else {
                // var_dump("Line: ".__LINE__, $idp_link, $current_url);exit;
                $this->redirect($idp_link);
            }
        } elseif( $result && current($result)['is_force_sso_enabled'] && !empty($email) && !empty($authentication_token) ) {
            $user = $this->User->query("select * from users where authentication_token = '" . $authentication_token . "'");
            // $this->idp_link_for_ng = "";
            
            $this->Session->delete('idp_link_for_ng');
            $current_user = $this->Session->read('user_current_account');
            //$current_authentication_token = $current_user['User']['authentication_token'];
            //above line was not getting the data as the it was array in side [0]
            if (isset($current_user['User'])){
                $current_authentication_token = $current_user['User']['authentication_token'];
                $current_authentication_email = $current_user['User']['email'];
            }else{
                $current_authentication_token = $current_user[0]['User']['authentication_token'];
                $current_authentication_email = $current_user[0]['User']['email'];
            }
            

            // echo "<pre>";
            // var_dump($current_user);
            // echo "</pre>";
            // die;

            if(empty($user) || (!empty($current_authentication_token) && $current_authentication_token != $authentication_token )){
            //if(empty($user) || (!empty($current_authentication_email) && $current_authentication_email != $email )){
                // $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">' . $email . ' doesn\'t exist.</div>');
                $error = 'Error signing in with ' . $email . '. Please login with correct email.';
                if(stristr($current_url,"api/angular_header_api")){
                    // This is from Angular, so we have to return Launchpad Link to NG so they can perform a redirect.
                    $this->Session->setFlash($error, 'default', array('class' => 'message error'));
                    $this->Session->write('idp_link_for_ng','/launchpad');
                } else {
                    if(count($current_user) > 1){
                        $this->Session->setFlash($error, 'default', array('class' => 'message error'));
                        $this->Session->write('idp_link_for_ng','/launchpad');
                    }else{
                    $this->Session->delete('sso_authenticated');
                    $this->Session->setFlash($error, 'default', array('class' => 'message error'));
                    //$this->redirect('/launchpad');
                    
                    $this->Session->delete('last_logged_in_user');
                    $this->Session->delete('access_token');
                    $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">' .$error . '</div>');
                    return $this->redirect('/home/login?error='.$error);
                    }        
                }
                $this->Session->delete('sso_authenticated');
            } else {
                // check if the current session user is the same returning from one login
                if($current_authentication_token == $authentication_token ){
                //if($current_authentication_email == $email ){
                    // $this->Session->write('sso_auth_'.$account_id, true);
                    $this->Session->write('sso_authenticated', true);
                }else{
                    $error = 'Error signing in with ' . $email  . '. Please login with correct email.';
                    if(count($current_user) > 1){
                        $this->Session->setFlash($error, 'default', array('class' => 'message error'));
                        $this->Session->write('idp_link_for_ng','/launchpad');
                    }else{
                    if(stristr($current_url,"api/angular_header_api")){
                        // This is from Angular, so we have to return Launchpad Link to NG so they can perform a redirect.
                        
                        $this->Session->setFlash($error, 'default', array('class' => 'message error'));
                        $this->Session->write('idp_link_for_ng','/launchpad');
                    } else {
                        
                        $this->Session->delete('sso_authenticated');
                        //$this->Session->setFlash($error, 'default', array('class' => 'message error'));
                        
                        //$this->redirect('/launchpad');
                        $this->Session->delete('last_logged_in_user');
                        $this->Session->delete('access_token');
                        $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">' .$error . '</div>');
                        return $this->redirect('/home/login?error='.$error);
                        //header('location:' . Configure::read('sibme_base_url') . 'users/login');    
                    }
                }
                    $this->Session->delete('sso_authenticated');
                }

                
            }
        }
    }

    public function getIDPLink($account_id, $current_url){
        if(stristr($current_url,"api/angular_header_api")){
            $redirect_to = $_SERVER["HTTP_REFERER"];
        }elseif(stristr($current_url,"login") || stristr($current_url,"Users/account_settings") || stristr($current_url,"Users/activation") ){
            $redirect_to = Configure::read('sibme_base_url') . 'users/sso_login_callback';
        }else{
            $redirect_to = $current_url;
        }
        if($this->Session->read('is_account_switched')){
            $redirect_to = $this->custom->addParamToURL('is_account_switched',$this->Session->read('is_account_switched'),$redirect_to);
        }
        $url = Configure::read('sibme_api_url') . 'get/sso-link';
        $data = ['account_id' => $account_id, 'redirect_to' => $redirect_to];
        $postdata = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = json_decode(curl_exec($ch));
        curl_close($ch);
        
        if(isset($result->success) && !$result->success){
            
        // if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        //     $result_array = array(
        //         'status' => false,
        //     );

        //     echo json_encode($result_array);
        //     die;
        // }

            $this->Session->setFlash($result->message, 'default', array('class' => 'message error'));

            if(stristr($current_url,"api/angular_header_api")){
                echo json_encode($result);
                die;
            }else{
                $this->redirect($current_url.'?error='.$result->message);
            }

            
        }
        
        return $result->idp_login_url;
    }

    public function checkDocumentFilesRecordExist($document_id, $new, $request)
    {
        $existing = $this->DocumentFiles->find('first', array('conditions' => array('document_id' => $document_id)));
        $existing = $existing['DocumentFiles'];
        if(!empty($existing))
        {
            $existing = json_encode($existing);
            $new = json_encode($new);
            $request = json_encode($request);
            $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 3);
            $backtrace = json_encode($backtrace);
            $message = "<b>Duplicate document is being inserted for document = $document_id</b>. 
                    <b>Existing Record:</b><br>
                    $existing <br><br>
                    <hr>
                    <br>
                    <b>New Record:</b><br>
                    $new
                    <br>
                    <br>
                    <hr>
                    <b>Request Dump:</b><br>
                    $request
                    <br>
                    <br>
                    <hr>
                    <b>Called from Function:</b><br>
                    $backtrace
                    <br>
        ";
            $data = [
                'message' => $message,
                'email' => "testinglandingpage9@gmail.com",
                'subject' => "Alert Duplicate document is being inserted",

            ];
            $this->sendAlertEmail($data);
            return json_decode($existing, true);
        }
       return false;
    }

    public function sendAlertEmail($data)
    {
        $view = new View($this, true);
        $site_title = $view->custom->get_site_settings('site_title');
        $this->Email->delivery = 'smtp';
        $this->Email->from ='do-not-reply@sibme.com';
        $this->Email->to = $data['email'];
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $this->Email->subject = $this->subject_title . " - " . $data['subject'];
        $html = $data['message'];

        if (!empty($data['email'])) {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }

        return FALSE;
    }

    function sendInvitation($data, $account_folder = '') {
        $view = new View($this, true);
        $users = $this->Session->read('user_current_account');
        $site_title = $view->custom->get_site_settings('site_title');
        if (empty($users)) {
            $users = $this->User->find('first', array('conditions' => array('id' => $data['user_id'])));
        }
        $this->Email->delivery = 'smtp';
        $this->Email->from = $site_title . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];
        //$this->Email->subject = "You've been invited to join " . $users['accounts']['company_name'] . "'s Sibme Account!";

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $params = array(
            'data' => $data,
            'account_folder_id' => $account_folder
        );

        $site_id = $this->site_id;
        $user_id = $users['User']['id'];
        $users_r = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        $lang = $users_r['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->subject_title . " - Account Invitation";
        } else {
            $this->Email->subject = $this->subject_title . " - Invitación a Cuenta";
        }

        $key = "account_invitation_" . $this->site_id . "_" . $lang;
        $sibme_base_url = config('s3.sibme_base_url');
        $activation_url = $this->get_host_url() . '/Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']) . "/" . $account_folder_id;
        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $users['User']['first_name'] . ' ' . $users['User']['last_name'], $html);
        $html = str_replace('{company_name}', $users['accounts']['company_name'], $html);
        $html = str_replace('{redirect}', $activation_url, $html);
        $html = str_replace('{site_title}', $view->Custom->get_site_settings('site_title'), $html);
        $html = str_replace('{unsubscribe}', '#', $html);
        $html = str_replace('{site_url}', $this->website_url, $html);

        //$html = $view->element("emails/users/$site_id/new_admin_role", $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {

            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            //$use_job_queue = Configure::read('use_job_queue');
            //if($use_job_queue){
            //    $this->add_job_queue(1,$this->Email->to,$this->Email->subject,$html,true);
            //    return TRUE;
            //}else{
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
//}
        }

        return FALSE;
    }

    function sendInvitation_student_payment($data, $account_folder = '') {


        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $data['email'];
        $this->Email->subject = "You've been invited to join " . $data['company_name'] . "'s Sibme Account!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'data' => $data,
            'account_folder_id' => $account_folder
        );

        $html = $view->element('emails/users/new_admin_role_student_payment', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {

            $this->AuditEmail->save($auditEmail, $validation = TRUE);
//$use_job_queue = Configure::read('use_job_queue');
//if($use_job_queue){
//    $this->add_job_queue(1,$this->Email->to,$this->Email->subject,$html,true);
//    return TRUE;
//}else{
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
//}
        }

        return FALSE;
    }

    function account_invitation_student_payment($data, $account_folder = '') {


        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $data['email'];
        $this->Email->subject = "You've been added to " . $data['company_name'] . "'s Sibme Account Successfully!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'data' => $data,
            'account_folder_id' => $account_folder
        );

        $html = $view->element('emails/users/account_invitation_student_payment', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {

            $this->AuditEmail->save($auditEmail, $validation = TRUE);
//$use_job_queue = Configure::read('use_job_queue');
//if($use_job_queue){
//    $this->add_job_queue(1,$this->Email->to,$this->Email->subject,$html,true);
//    return TRUE;
//}else{


            try {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } catch (Exception $e) {
                return false;
            }
//}
        }

        return FALSE;
    }

    function sendInvitation_mobile($data, $account_folder = '') {

        $site_title = $this->custom->get_site_settings('site_title');
        $users = $this->Session->read('user_current_account');
        $view = new View($this, true);
        if (empty($users)) {
            $users = $this->User->find('first', array('conditions' => array('id' => $data['user_id'])));
        }
        $this->Email->delivery = 'smtp';
        $this->Email->from = $site_title . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $params = array(
            'data' => $data,
            'account_folder_id' => $account_folder
        );
        $site_id = $this->site_id;
        $lang = $data['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $view->custom->get_site_settings('email_subject') . " - Account Invitation";
        } else {
            $this->Email->subject = $view->custom->get_site_settings('email_subject') . " - Invitación a Cuenta";
        }
        $user_id = $users['User']['id'];
        $key = "new_admin_role_mobile_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{site_title}', $view->Custom->get_site_settings('site_title'), $html);
        $html = str_replace('{sender}', $data['creator_first_name'] . ' ' . $data['creator_last_name'], $html);
        $html = str_replace('{company_name}', $data['company_name'], $html);
        $html = str_replace('{redirect}', $this->get_host_url() . '/Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']) . "/" . $account_folder, $html);
        $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $user_id . '/6', $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
// $html = $view->element("emails/users/$site_id/new_admin_role_mobile", $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {

            $this->AuditEmail->save($auditEmail, $validation = TRUE);
//$use_job_queue = Configure::read('use_job_queue');
//if($use_job_queue){
//    $this->add_job_queue(1,$this->Email->to,$this->Email->subject,$html,true);
//    return TRUE;
//}else{
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
//}
        }

        return FALSE;
    }

    function users_full_email($current_user_email, $user_email, $account_id) {

        $users = $this->Session->read('user_current_account');
        $site_title = $this->custom->get_site_settings('site_title');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $site_title . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $current_user_email;

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $result = $this->UserAccount->find('first', array(
            'conditions' => array(
                'UserAccount.account_id' => $account_id,
                'UserAccount.role_id' => 100
            ),
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'u',
                    'type' => 'inner',
                    'conditions' => 'u.id= UserAccount.user_id',
                ),
            ),
            'fields' => array(
                'u.*'
            )
        ));
        $params = array(
            'current_user_email' => $current_user_email,
            'account_id' => $account_id,
            'user_email' => $user_email,
            'account_owner_email' => $result['u']['email']
        );
        $total_allowed_user = $view->Custom->get_allowed_users($account_id);
        $account_owner_email = $result['u']['email'];
        $site_id = $this->site_id;
        $lang = $result['u']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->subject_title . " - Account User Limit Reached";
        } else {
            $this->Email->subject = $this->subject_title . " - Límite de Usuarios de Cuenta";
        }
        $key = "users_full_" . $this->site_id . "_" . $lang;
        $results = $this->get_send_grid_contents($key);
        $html = $results->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{account_owner_email}', $account_owner_email, $html);
        $html = str_replace('{total_allowed_user}', $total_allowed_user, $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
//$html = $view->element("emails/$site_id/users_full", $params);
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $current_user_email,
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );
        $data['email'] = 'sibmeqa@gmail.com';

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
//$use_job_queue = Configure::read('use_job_queue');
//if($use_job_queue){
//    $this->add_job_queue(1,$this->Email->to,$this->Email->subject,$html,true);
//    return TRUE;
//}else{
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
//}
        }

        return FALSE;
    }

    function storage_full_email($current_user_email, $account_id) {

// $users = $this->Session->read('user_current_account');
        $site_title = $this->custom->get_site_settings('site_title');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $site_title . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $current_user_email;

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);

        $site_id = $this->site_id;
        $result = $this->UserAccount->find('first', array(
            'conditions' => array(
                'UserAccount.account_id' => $account_id,
                'UserAccount.role_id' => 100
            ),
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'u',
                    'type' => 'inner',
                    'conditions' => 'u.id= UserAccount.user_id',
                ),
            ),
            'fields' => array(
                'u.*'
            )
        ));
        $params = array(
            'current_user_email' => $current_user_email,
            'account_id' => $account_id,
            'user_email' => $current_user_email,
            'account_owner_email' => $result['u']['email']
        );
        $total_allowed_size = $view->Custom->get_allowed_storage($account_id);
        $lang = $result['u']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->subject_title . " - Account Storage Limit Reached";
        } else {
            $this->Email->subject = $this->subject_title . " - Límite de Almacenamiento de Cuenta";
        }
        $key = "storage_full_" . $this->site_id . "_" . $lang;
        $results = $this->get_send_grid_contents($key);
        $html = $results->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);

        $html = str_replace('{current_user_email}', $current_user_email, $html);
        $html = str_replace('{total_allowed_size}', $total_allowed_size, $html);
        $html = str_replace('{account_owner_email}', $result['u']['email'], $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
//$html = $view->element("emails/$site_id/storage_full", $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $current_user_email,
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );
        $data['email'] = 'sibmeqa@gmail.com';

        if (!empty($data['email'])) {

            $this->AuditEmail->save($auditEmail, $validation = TRUE);
//$use_job_queue = Configure::read('use_job_queue');
//if($use_job_queue){
//    $this->add_job_queue(1,$this->Email->to,$this->Email->subject,$html,true);
//    return TRUE;
//}else{
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
//}
        }

        return FALSE;
    }

    function afterCompletion($data, $account_id) {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $data['to_email'];
        $this->Email->subject = "You've joined  " . $users['accounts']['company_name'] . "'s " . $this->custom->get_site_settings('email_subject') . " Account!";
        if(isset($this->site_id) && $this->site_id=='2'){
            $this->Email->subject = "You've Joined the HMH Coaching Studio";
        }
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $data['base_url'] = Configure::read('sibme_base_url');
        $params = array(
            'data' => $data
        );

        $html = $view->element('emails/users/complete_invitations', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $data['to_email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );


        if (!empty($data['to_email'])) {

            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['to_email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }

    function activation_email($data, $account_id) {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $data['to_email'];
        $this->Email->subject = $this->custom->get_site_settings('email_subject') . " Activation Email!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'data' => $data
        );

        $html = $view->element('emails/users/activation_email', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $data['to_email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );


        if (!empty($data['to_email'])) {

            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            $use_job_queue = false;
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['to_email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }

    function activation_email_student_payment($data, $account_id) {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $data['to_email'];
        $this->Email->subject = "Sibme Activation Email!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        /*$view = new View($this, true);
        $params = array(
            'data' => $data
        );

        $html = $view->element('emails/users/activation_email_student_payment', $params);*/

        $sibme_base_url = Configure::read('sibme_base_url');
        if(isset($account_folder_id))
        {
            $link = $this->get_host_url() . '/Users/activate_user_student_payment/' . md5($data['user_id']) . '/' . $data['account_id'] . '/' .$account_folder_id;
        }
        else
        {
            $link = $this->get_host_url() . '/Users/activate_user_student_payment/' . md5($data['user_id']). '/' . $data['account_id'];
        }
        $key = "activation_email_student_payment_en";
        $results = $this->get_send_grid_contents($key);
        $html = $results->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{USER_NAME}', $data['name'], $html);
        $html = str_replace('{ACCOUNT_NAME}', $data['account_name'] , $html);
        $html = str_replace('{redirect}', $link, $html);
        $html = str_replace('{site_url}', $this->website_url, $html);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $data['to_email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );


        if (!empty($data['to_email'])) {

            $this->AuditEmail->save($auditEmail, $validation = TRUE);
//    $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['to_email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                try {
                    if ($this->Email->send($html)) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                } catch (Exception $e) {
                    return false;
                }
            }
        }

        return FALSE;
    }
    
    function send_two_factor_auth_token($data, $account_id) {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $data['to_email'];
        $this->Email->subject = "Sibme - Two Factor Authentication Code";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        /*$view = new View($this, true);
        $params = array(
            'data' => $data
        );

        $html = $view->element('emails/users/two_factor_auth_code', $params);*/

        $key = "two_factor_varification_en";
        $results = $this->get_send_grid_contents($key);
        $html = $results->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{USER_NAME}', $data['name'], $html);
        $html = str_replace('{VERIFICATION_CODE}', $data['verification_code'] , $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $data['to_email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );


        if (!empty($data['to_email'])) {

            $this->AuditEmail->save($auditEmail, $validation = TRUE);
//    $use_job_queue = Configure::read('use_job_queue');
            if (false) {
                $this->add_job_queue(1, $data['to_email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                try {
                    if ($this->Email->send($html)) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                } catch (Exception $e) {
                    return false;
                }
            }
        }

        return FALSE;
    }

    function sendConfirmation($data) {
        if (!Validation::email($data['email']))
            return FALSE;
        $users = $this->Session->read('user_current_account');
        $site_title = $this->custom->get_site_settings('site_title');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $site_title . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];
//        $this->Email->subject = "You've been invited to join " . $users['accounts']['company_name'] . "'s Sibme Account!";

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'data' => $data
        );
        $site_id = $this->site_id;
        $user_id = $users['User']['id'];
        $users_r = $this->User->find('first', array('conditions' => array('id' => $user_id)));

        $lang = $users_r['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->subject_title . " - Account Invitation";
        } else {
            $this->Email->subject = $this->subject_title . " -  Invitación a Cuenta";
        }
        $key = "account_invitation_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $users['User']['first_name'] . ' ' . $users['User']['last_name'], $html);
        $html = str_replace('{site_title}', $view->Custom->get_site_settings('site_title'), $html);
        $html = str_replace('{company_name}', $users['accounts']['company_name'], $html);
        $html = str_replace('{redirect}', $this->get_host_url() . '/Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']), $html);
        $html = str_replace('{unsubscribe}', '#', $html);
        $html = str_replace('{site_title}', $view->Custom->get_site_settings('site_title'), $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
//$html = $view->element("emails/users/$site_id/account_invitation", $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }


        return FALSE;
    }

    function sendConfirmation_mobile($data) {
        if (!Validation::email($data['email']))
            return FALSE;
        $view = new View($this, true);
        $users = $this->Session->read('user_current_account');
        if (empty($users)) {
            $users = $this->User->find('first', array('conditions' => array('id' => $data['user_id'])));
        }
        $this->Email->delivery = 'smtp';
        $this->Email->from = $view->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $data['email'];

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $params = array(
            'data' => $data
        );

        $site_id = $this->site_id;
//$html = $view->element("emails/users/$site_id/account_invitation_mobile", $params);
        $lang = $users['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $view->custom->get_site_settings('email_subject') . " - Account Invitation";
        } else {
            $this->Email->subject = $view->custom->get_site_settings('email_subject') . " - Invitación a Cuenta";
        }
        $user_id = $users['User']['id'];
        $key = "account_invitation_mobile_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $users['User']['first_name'] . ' ' . $users['User']['last_name'], $html);
        $html = str_replace('{site_title}', $view->Custom->get_site_settings('site_title'), $html);
        $html = str_replace('{company_name}', $users['accounts']['company_name'], $html);
        $html = str_replace('{redirect}', $this->get_host_url() . '/Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']), $html);
        $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $user_id . '/6', $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }


        return FALSE;
    }

    function sendCopyVideoEmail($data, $user_id = '') {

        if (!Validation::email($data['email']))
            return FALSE;
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . '  ' . $users['accounts']['company_name'] . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $data['account_folder_id'], 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $params = array(
            'data' => $data,
            'user_id' => $user_id,
            'htype' => $htype,
            'users' => $users
        );
        $site_id = $this->site_id;
        $lang = $users['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->subject_title . " - Video Added";
        } else {
            $this->Email->subject = $this->subject_title . " - Video Añadido";
        }
        $key = "video_copy_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $users['User']['first_name'] . ' ' . $users['User']['last_name'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect}', $this->get_host_url() . '/' . $data['video_link'], $html);
        $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $user_id . '/7', $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
//$html = $view->element("emails/$site_id/video_copy", $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }
    
    
    function sendCopyVideoEmailMobile($data, $user_id = '',$logged_in_user_id,$account_id) {

        if (!Validation::email($data['email']))
            return FALSE;
        $user_detail = $this->User->find('first', array('conditions' => array('id' => $logged_in_user_id )));
        $reciever_detail = $this->User->find('first', array('conditions' => array('id' => $user_id )));
        $account_detail = $this->Account->find('first', array('conditions' => array('id' => $account_id )));
        $this->Email->delivery = 'smtp';
        $this->Email->from = $user_detail['User']['first_name'] . " " . $user_detail['User']['last_name'] . '  ' . $account_detail['Account']['company_name'] . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $data['account_folder_id'], 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $params = array(
            'data' => $data,
            'user_id' => $user_id,
            'htype' => $htype,
            'users' => $users
        );
        $site_id = $this->site_id;
        $lang = $reciever_detail['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->subject_title . " - Video Added";
        } else {
            $this->Email->subject = $this->subject_title . " - Video Añadido";
        }
        $key = "video_copy_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $user_detail['User']['first_name'] . ' ' . $user_detail['User']['last_name'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect}', $this->get_host_url() . '/' . $data['video_link'], $html);
        $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $user_id . '/7', $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
//$html = $view->element("emails/$site_id/video_copy", $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }

    function sendCopyResourceEmail($data, $user_id = '') {
        if (!Validation::email($data['email']))
            return FALSE;
        $users = $this->Session->read('user_current_account');
        if (empty($users)) {
            $users = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        }

        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . '  ' . $users['accounts']['company_name'] . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'data' => $data,
            'user_id' => $user_id,
            'users' => $users
        );
        $site_id = $this->site_id;
        $lang = $users['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->subject_title . " - Resource Added";
        } else {
            $this->Email->subject = $this->subject_title . " - Recurso Añadido";
        }
        $key = "resource_copy_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $users['User']['first_name'] . " " . $users['User']['last_name'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect}', $this->get_host_url() . $data['video_link'], $html);
        $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $user_id . '/7', $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
//$html = $view->element("emails/$site_id/resource_copy", $params);
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }

    function sendVideoComments($data, $user_id = '', $mobile = false) {
        $to_user_id = $data['user_id'];
        if (!Validation::email($data['email']))
            return FALSE;
        $users = $this->Session->read('user_current_account');
        if (empty($users)) {
            $users = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        }

        if ($mobile) {
            $users = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        }

        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . '  ' . $users['accounts']['company_name'] . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $this->Email->replyto = 'comment_' . $data['comment_id'] . Configure::read('reply_to_email');
        $view = new View($this, true);
        $params = array(
            'data' => $data,
            'user_id' => $user_id
        );
        $site_id = $this->site_id;
        $to_person = $this->User->find('first', array('conditions' => array('id' => $data['user_id'])));
//$html = $view->element("emails/$site_id/video_replies", $params);
        $lang = $to_person['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->custom->get_site_settings('email_subject') . " - Comment Added";
        } else {
            $this->Email->subject = $this->custom->get_site_settings('email_subject') . " - Comentario añadido";
        }
        $key = "video_comments_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);

        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $users['User']['first_name'] . ' ' . $users['User']['last_name'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect}', $this->get_host_url() . $data['video_link'], $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
        $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $to_user_id . '/6', $html);
        $reply_to = 'comment_' . $data['comment_id'] . Configure::read('reply_to_email');
//$html = $view->element("emails/$site_id/video_comments", $params);
//$reply_to = 'comment_' . $data['comment_id'] . Configure::read('reply_to_email');

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );
        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true, $reply_to);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }

    function sendVideoReplies($data, $user_id = '') {
        if (!Validation::email($data['email']))
            return FALSE;
        $users = $this->Session->read('user_current_account');
        if (empty($users)) {
            $users = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        }

        $view = new View($this, true);
        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . '  ' . $users['accounts']['company_name'] . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $this->Email->replyto = 'comment_' . $data['comment_id'] . Configure::read('reply_to_email');

        $params = array(
            'data' => $data,
            'user_id' => $user_id
        );
        $site_id = $this->site_id;

//$html = $view->element("emails/$site_id/video_replies", $params);
        $lang = $users['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $view->custom->get_site_settings('email_subject') . " - Comment Reply";
        } else {
            $this->Email->subject = $view->custom->get_site_settings('email_subject') . " - Respuesta a comentario";
        }
        $key = "video_replies_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);

        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $users['User']['first_name'] . ' ' . $users['User']['last_name'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect}', $this->get_host_url() . $data['video_link'], $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
        $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $data['user_id'] . '/6', $html);

        $reply_to = 'comment_' . $data['comment_id'] . Configure::read('reply_to_email');

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true, $reply_to);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }

    function sendDocumentEmail($data) {
        if (!Validation::email($data['email']))
            return FALSE;

        $users = $this->Session->read('user_current_account');
        if (empty($users)) {
            $users = $this->User->find('first', array('conditions' => array('id' => $data['user_id'])));
        }
        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . '  ' . $users['accounts']['company_name'] . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'data' => $data
        );
//$html = $view->element('emails/' . $this->site_id . '/document_uploaded', $params);
        $lang = $users['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->subject_title . " - Resource Added";
        } else {
            $this->Email->subject = $this->subject_title . " - Recurso Añadido";
        }
        $key = "document_uploaded_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $users['User']['first_name'] . "  " . $users['User']['last_name'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect}', $this->get_host_url() . $data['video_link'], $html);
        $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $data['user_id'] . '/1', $html);
        $html = str_replace('{site_url}', $this->website_url, $html);
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $result = false;
            try {
                $use_job_queue = Configure::read('use_job_queue');
                if ($use_job_queue) {
                    $result = $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true);
                } else {
                    $result = $this->Email->send($html);
                }
            } catch (Exception $ex) {

            }
            return $result;
        }

        return FALSE;
    }

    function sendVideoPublishedNotification($huddle_name, $huddle_id, $video_id, $creator, $account_id, $huddleUsers, $recipient_email, $huddle_user_id, $huddle_type, $email_type = '') {
        if (!Validation::email($recipient_email) || !isset($this->Session))
            return FALSE;
        $users = $this->Session->read('user_current_account');
        //if (empty($users)) {
        $users = $this->User->find('first', array('conditions' => array('id' => $creator['User']['id'])));
        //}

        $account_info = $this->Account->find('first', array('conditions' => array('id' => $account_id)));

        $sibme_base_url = Configure::read('sibme_base_url');
        $auth_token = md5($creator['User']['id']);
        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . " " . $users['accounts']['company_name'] . " <" . $this->custom->get_site_settings('static_emails')['noreply'] . ">";
        $this->Email->to = $recipient_email;

//        if ($huddle_type == '2') {
//            $this->Email->subject = $account_info['Account']['company_name'] . " Video Library.";
//        } else {
//            $this->Email->subject = "Re: " . $huddle_name . " You've got a new video to view. ";
//        }
        $lang = $creator['User']['lang'];
        $key = "video_publish_huddle_" . $this->site_id . "_" . $lang;

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $lang_id = $users['User']['lang'];
        if ($lang_id == 'en') {
            $this->Email->subject = $this->subject_title . ' - Video Added';
        } else {
            $this->Email->subject = $this->subject_title . ' - Video Añadido';
        }

        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $view = new View($this, true);
        if ($huddle_type == '1') {
            $huddle_name = $huddle_name;
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id;
        } elseif ($huddle_type == '2') {
            $huddle_name = $huddle_name;
            $url = $sibme_base_url . "VideoLibrary/view/" . $huddle_id;
        } elseif ($huddle_type == '3') {
            $huddle_name = $huddle_name;
            $url = $sibme_base_url . "MyFiles/view/1/" . $huddle_id;
        } else {
            $huddle_type = '1';
            $huddle_name = $huddle_name;
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id;
        }

        $params = array(
            'huddle_name' => $huddle_name,
            'account_name' => $account_info['Account']['company_name'],
            'huddle_id' => $huddle_id,
            'video_id' => $video_id,
            'creator' => $creator,
            'huddle_type' => $huddle_type,
            'participated_user' => $huddleUsers,
            'authentication_token' => $auth_token,
            'url' => $url,
            'htype' => $htype,
            'huddle_user_id' => $huddle_user_id,
            'email_type' => $email_type
        );
        $site_id = $this->site_id;
        $html = '';
        $key = '';
        if ($huddle_type == 3) {
            $lang = $users['User']['lang'];
            $key = "video_publish_workspace_" . $this->site_id . "_" . $lang;
            $result = $this->get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{redirect}', $url, $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
            $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $huddle_user_id . "/" . $email_type, $html);
        } elseif ($huddle_type == 2) {
            $lang = $users['User']['lang'];
            $key = "video_publish_lib_" . $this->site_id . "_" . $lang;
            $result = $this->get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{account_name}', $account_info['Account']['company_name'], $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect}', $url, $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
            $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $huddle_user_id . "/" . $email_type, $html);
        } else {
            $lang = $users['User']['lang'];
            $key = "video_publish_huddle_" . $this->site_id . "_" . $lang;
            $result = $this->get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $creator['User']['first_name'] . " " . $creator['User']['last_name'], $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect}', $url, $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
            $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $huddle_user_id . "/" . $email_type, $html);
        }



//$html = $view->element("emails/$site_id/video_publish", $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $creator['User']['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );


        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function sendVideoPublishedNotification_AfterPublish($huddle_name, $huddle_id, $video_id, $creator, $account_id, $huddleUsers, $recipient_email, $huddle_user_id, $huddle_type, $email_type = '', $url_scripted = '') {
        if (!Validation::email($recipient_email))
            return FALSE;

        $users = $this->Session->read('user_current_account');
        $account_info = $this->Account->find('first', array('conditions' => array('id' => $account_id)));

        $sibme_base_url = Configure::read('sibme_base_url');
        $auth_token = md5($creator['User']['id']);
        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . " " . $users['accounts']['company_name'] . " <" . $this->custom->get_site_settings('static_emails')['noreply'] . ">";
        $this->Email->to = $recipient_email;

        if ($huddle_type == '2') {
            $this->Email->subject = $account_info['Account']['company_name'] . " Video Library.";
        } else {
            $this->Email->subject = "Re: " . $huddle_name . " You've got a new video to view. ";
        }

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $view = new View($this, true);
        if ($huddle_type == '1') {
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id;
        } elseif ($huddle_type == '2') {
            $url = $sibme_base_url . "VideoLibrary/view/" . $huddle_id;
        } elseif ($huddle_type == '3') {
            $url = $sibme_base_url . "MyFiles/view/1/" . $huddle_id;
        } else {
            $huddle_type = '1';
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id;
        }

        if (!empty($url_scripted)) {
            $url = $url_scripted;
// $this->Email->subject = "Re: " . $huddle_name . " You've got a new scripted observation to view. ";
        }

        $params = array(
            'huddle_name' => $huddle_name,
            'account_name' => $account_info['Account']['company_name'],
            'huddle_id' => $huddle_id,
            'video_id' => $video_id,
            'creator' => $creator,
            'huddle_type' => $huddle_type,
            'participated_user' => $huddleUsers,
            'authentication_token' => $auth_token,
            'url' => $url,
            'htype' => $htype,
            'huddle_user_id' => $huddle_user_id,
            'email_type' => $email_type
        );

        $site_id = $this->site_id;
        if (!empty($url_scripted)) {

//$html = $view->element("emails/$site_id/after_scriptedob_publish_email", $params);
            $redirect_url = $this->get_host_url() . "/huddles/observation_details_1/" . $huddle_id . "/1/" . $video_id;
            $lang = $creator['User']['lang'];
            if ($lang == 'en') {
                $this->Email->subject = $this->subject_title . " - Scripted Observation Published";
            } else {
                $this->Email->subject = $this->subject_title . " - Observación con Guión Publicados";
            }
            $key = "after_scriptedob_publish_email_" . $this->site_id . "_" . $lang;
            $result = $this->get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $creator['User']['first_name'] . " " . $creator['User']['last_name'], $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect_url}', $redirect_url, $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
            $html = str_replace('{unsubcribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $creator['User']['id'] . '/6', $html);
        } else {

//$html = $view->element("emails/$site_id/after_video_publish_email", $params);
            $redirect_url = $this->get_host_url() . "/huddles/view/" . $huddle_id . "/1/" . $video_id;
            $lang = $creator['User']['lang'];
            if ($lang == 'en') {
                $this->Email->subject = $this->subject_title . " - Video Observation Published";
            } else {
                $this->Email->subject = $this->subject_title . " - Video Observación Publicados";
            }

            $key = "after_video_publish_email_" . $this->site_id . "_" . $lang;
            $result = $this->get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $creator['User']['first_name'] . " " . $creator['User']['last_name'], $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect_url}', $redirect_url, $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
            $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $creator['User']['id'] . '/6', $html);
        }
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $creator['User']['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function sendVideoReceivedNotification($huddle_name, $huddle_id, $video_id, $creator, $account_id, $huddleUsers, $recipient_email, $huddle_user_id, $huddle_type, $email_type = '') {
        if (!Validation::email($recipient_email))
            return FALSE;

        $users = $this->Session->read('user_current_account');

        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . " " . $users['accounts']['company_name'] . " <" . $this->custom->get_site_settings('static_emails')['noreply'] . ">";
        $this->Email->to = $recipient_email;
        $this->Email->subject = "Re: " . $huddle_name . " A new video is posted. ";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';


        $html = "<p>Your video is currently processing. Depending on the size of your video it could take some time. We will notify you when your video is ready to be viewed.</p><br/><br/>The Sibme Team.";

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $creator['User']['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function SendDiscussionEmail($account_id, $User, $attachment_path, $comment, $data = '', $reply = '') {
        if (!Validation::email($User['User']['email']))
            return FALSE;

        $users = $this->Session->read('user_current_account');


// if ($reply != '') {
//     $reply = 'Re:';
// } else {
//     $reply = '';
// }

        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . " " . $users['accounts']['company_name'] . " <" . $this->custom->get_site_settings('static_emails')['noreply'] . ">";
        $this->Email->to = $User['User']['first_name'] . ' ' . $User['User']['last_name'] . '<' . $User['User']['email'] . '>';

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        if ($attachment_path != '') {
            $this->Email->attachments = array($attachment_path);
        }
        $params = array(
            'data' => $data,
            'user_id' => $User['User']['id']
        );
        $view = new View($this, true);
        $html = '';
        $site_id = $this->site_id;
        if ($reply != '') {

            $lang = $User['User']['lang'];
            if ($lang == 'en') {
                $this->Email->subject = $this->subject_title . " - New Discussion Message Posted";
            } else {
                $this->Email->subject = $this->subject_title . " - Publicación de discusión";
            }
            $key = "huddle_discussions_reply_" . $this->site_id . "_" . $lang;
            $results = $this->get_send_grid_contents($key);
            $html = $results->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $data['created_by'], $html);
            $html = str_replace('{discussion_topic}', $data['discussion_topic'], $html);
            $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
            $html = str_replace('{redirect}', $data['discussion_link'], $html);
            $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $User['User']['id'] . '/3', $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
//$html = $view->element("emails/$site_id/huddle_discussions_reply", $params);
        } else {

            $lang = $User['User']['lang'];
            if ($lang == 'en') {
                $this->Email->subject = $this->subject_title . " - New Discussion Added";
            } else {
                $this->Email->subject = $this->subject_title . " -  Nueva discusión";
            }
            $key = "huddle_discussion_" . $this->site_id . "_" . $lang;

            $results = $this->get_send_grid_contents($key);
            $html = $results->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $data['created_by'], $html);
            $html = str_replace('{discussion_topic}', $data['discussion_topic'], $html);
            $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
            $html = str_replace('{redirect}', $data['discussion_link'], $html);
            $html = str_replace('{unsubscribe}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $User['User']['id'] . '/2', $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
//$html = $view->element("emails/$site_id/huddle_discussions", $params);
        }
// $this->Email->htmlMessage = $comment;

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $User['User']['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $comment,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $User['User']['email'], $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function newHuddleEmail($data, $user_id = '') {
        if (!Validation::email($data['User']['email']))
            return FALSE;
        $view = new View($this, false);
        $users = $this->Session->read('user_current_account');
        if (empty($users)) {
            $users = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        }
        $site_id = $this->site_id;
        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . ' ' . $users['accounts']['company_name'] . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['User']['email'];

        $site_title = $view->Custom->get_site_settings('site_title');
        if (empty($site_title)) {
            $site_title = 'Sibme';
        }


        /*
          if ($data["huddle_type"] == '2') {
          $this->Email->subject = $users['User']['first_name'] . " " . $users['User']['last_name'] . " created a new Coaching Huddle";
          } else if ($data["huddle_type"] == '3') {
          $this->Email->subject = $users['User']['first_name'] . " " . $users['User']['last_name'] . " created a new Assessment Huddle";
          } else {
          $this->Email->subject = $users['User']['first_name'] . " " . $users['User']['last_name'] . " created a new Collaboration Huddle";
          }
         */
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $message_detail = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $data['AccountFolder']['account_folder_id'], 'meta_data_name' => 'message')));
        $params = array(
            'data' => $data,
            'user_id' => $user_id,
            'message' => $message_detail['AccountFolderMetaData']['meta_data_value']
        );
        $unsubscribe = $this->get_host_url() . '/subscription/unsubscirbe_now/' . $user_id . '/4';

        $lang = $users['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $view->Custom->get_site_settings('email_subject') . " - Huddle Invitation";
        } else {
            $this->Email->subject = $view->Custom->get_site_settings('email_subject') . " - Invitación a Equipo";
        }
        $key = "new_huddle_email_collab_" . $this->site_id . "_" . $lang;
        $html = '';
        if ($data['huddle_type'] == '1') {
            $key = "new_huddle_email_collab_" . $this->site_id . "_" . $lang;
            $results = $this->get_send_grid_contents($key);
            $html = $results->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $data['User']['first_name'] . " " . $data['User']['last_name'], $html);
            $html = str_replace('{huddle_name}', $data['AccountFolder']['name'], $html);
            $html = str_replace('{contact_email}', $users['User']['email'], $html);
            $html = str_replace('{contact_name}', $users['User']['first_name'] . " " . $users['User']['last_name'], $html);
            $html = str_replace('{redirect}', $this->get_host_url() . '/Huddles/view/' . $data['AccountFolder']['account_folder_id'], $html);
            $html = str_replace('{unsubscribe}', $unsubscribe, $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
        } elseif ($data['huddle_type'] == '3') {
            $key = "new_huddle_email_eval_" . $this->site_id . "_" . $lang;
            $results = $this->get_send_grid_contents($key);
            $html = $results->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $data['User']['first_name'] . " " . $data['User']['last_name'], $html);
            $html = str_replace('{huddle_name}', $data['AccountFolder']['name'], $html);
            $html = str_replace('{submission_date}', $data['submission_deadline_date'], $html);
            $html = str_replace('{submission_time}', $data['submission_deadline_time'], $html);
            $html = str_replace('{contact_email}', $users['User']['email'], $html);
            $html = str_replace('{contact_name}', $users['User']['first_name'] . " " . $users['User']['last_name'], $html);
            $html = str_replace('{redirect}', $this->get_host_url() . '/Huddles/view/' . $data['AccountFolder']['account_folder_id'], $html);
            $html = str_replace('{unsubscribe}', $unsubscribe, $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
        } else {
            $key = "new_huddle_email_coaching_" . $this->site_id . "_" . $lang;
            $results = $this->get_send_grid_contents($key);
            $html = $results->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $data['User']['first_name'] . " " . $data['User']['last_name'], $html);
            $html = str_replace('{huddle_name}', $data['AccountFolder']['name'], $html);
            $html = str_replace('{contact_email}', $users['User']['email'], $html);
            $html = str_replace('{contact_name}', $users['User']['first_name'] . " " . $users['User']['last_name'], $html);
            $html = str_replace('{redirect}', $this->get_host_url() . '/Huddles/view/' . $data['AccountFolder']['account_folder_id'], $html);
            $html = str_replace('{unsubscribe}', $unsubscribe, $html);
            $html = str_replace('{site_url}', $this->website_url, $html);
        }
//$html = view($this->site_id . '.newhuddleemail', $params);
//$html = $view->element("emails/$site_id/new_huddle", $params);


        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['AccountFolder']['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['User']['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function HuddleInviteEmail($data, $to_email) {

        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $to_email;
        $this->Email->subject = $data['User']['first_name'] . " " . $data['User']['last_name'] . " invited you to a Sibme Huddle: " . $data['AccountFolder']['name'];
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'data' => $data
        );
        $html = $view->element('emails/new_huddle', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['AccountFolder']['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['User']['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function send_users_message($data, $recipientUser) {
        $senderUser = $this->Session->read('user_current_account');

        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $senderUser['User']['first_name'] . ' ' . $senderUser['User']['last_name'] . '<' . $senderUser['User']['email'] . '>';
        $this->Email->to = $recipientUser['User']['first_name'] . ' ' . $recipientUser['User']['last_name'] . '<' . $recipientUser['User']['email'] . '>';
        $this->Email->subject = 'Message from (' . $senderUser['User']['first_name'] . ' ' . $senderUser['User']['last_name'] . ') To ' . $recipientUser['User']['first_name'] . ' ' . $recipientUser['User']['last_name'] . '(' . $senderUser['accounts']['company_name'] . ')';
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'data' => $data
        );
        $html = $view->element('emails/users/user_message', $params);
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $users['accounts']['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $this->Email->to,
            'email_subject' => $this->Email->subject,
            'email_body' => $this->data['message'],
            'is_html' => FALSE,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $recipientUser['User']['email'], $this->Email->subject, $html, true, $senderUser['User']['email']);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function send_welcome_email($data, $account_id) {

        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $data['email'];
        $this->Email->subject = "Welcome to Sibme!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $params = array(
            'user' => $data
        );
        $view = new View($this, true);
        $html = $view->element('emails/users/signup', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function send_welcome_to_acc_email($data, $account_id) {

        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $data['email'];
        $this->Email->subject = "Welcome to Sibme!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $params = array(
            'data' => $data
        );


        $view = new View($this, true);
        $html = $view->element('emails/users/account_invitation_mobile', $params);


        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function send_welcome_email_template($data, $account_id) {

        $this->Email->delivery = 'smtp';
        $this->Email->from ='do-not-reply@sibme.com';
        $this->Email->to = $data['email'];
        $this->Email->subject = "Welcome to Sibme!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $params = array(
            'user' => $data
        );
        $view = new View($this, true);
        $use_template = Configure::read('day1_all');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sendgrid.com/v3/templates/$use_template",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic ZGF2ZXdAc2libWUuY29tOmM5ODhZWXdzIUA=",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $json = json_decode($response);
        }
        $html = str_replace('<%body%>', '', $json->versions[0]->html_content);
        $user_details = $this->User->find('first', array('conditions' => array('email' => $data['email'])));
        $html = str_replace('[Insert the User Name Here]', $user_details['User']['first_name'] . ' ' . $user_details['User']['last_name'], $html);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
//  $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {

            try {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } catch (Exception $e) {
                return false;
            }
        }
    }

    function send_sibme_welcome_email($user, $temp_password) {

        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = "mdonaho@techwerksinc.com";
//$this->Email->to = "khurri.saleem@gmail.com";
        $this->Email->subject = "Welcome to Sibme 2.0!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $params = array(
            'user_id' => $user['User']['id'],
            'user_name' => $user['User']['username'],
            'temp_password' => $temp_password
        );
        $view = new View($this, true);
        $html = $view->element('emails/welcome_sibme', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => -1,
            'email_from' => $this->Email->from,
            'email_to' => $this->Email->to,
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function tempupload() {
        $this->layout = null;
        require('VideoUploadHandler.php');
        $upload_handler = new UploadHandler($this->request->data['rand_folder']);
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function uploadDocuments($account_folder_id, $account_id, $user_id, $video_id = '', $url_stack = '', $workspace = false) {
        $view = new View($this, false);
        if ($this->request->is('post')) {


            if ($workspace) {
                $loggedInUser = $this->Session->read('user_current_account');
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = $view->Custom->check_if_account_in_trial_intercom($account_id);
//if (IS_QA):

                $meta_data = array(
                    'workspace_resource' => 'true',
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );

                // $this->create_intercom_event('workspace-resource-uploaded', $meta_data, $loggedInUser['User']['email']);
                $this->create_churnzero_event('Workspace+Resource+Uploaded', $account_id, $loggedInUser['User']['email']);
//endif;
            } else {
                $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
                $huddle_detail = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $account_folder_id)));
                $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
                if ($htype == 1) {
                    $huddle_type = 'Collaboration Huddle';
                } elseif ($htype == 2) {
                    $huddle_type = 'Coaching Huddle';
                } else {
                    $huddle_type = 'Assessment Huddle';
                }
                $loggedInUser = $this->Session->read('user_current_account');
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = $view->Custom->check_if_account_in_trial_intercom($account_id);
// if (IS_QA):

                $meta_data = array(
                    'huddle_resource' => 'true',
                    'huddle_type' => $huddle_type,
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );
                
                if(isset($huddle_detail['AccountFolder']['folder_type']) && $huddle_detail['AccountFolder']['folder_type'] == '2' )
                {
                    $this->create_churnzero_event('Library+Resource+Uploaded', $account_id, $loggedInUser['User']['email']);
                    
                }
                
                else {

                // $this->create_intercom_event('huddle-resource-uploaded', $meta_data, $loggedInUser['User']['email']);
                
                $this->create_churnzero_event('Huddle+Resource+Uploaded', $account_id, $loggedInUser['User']['email']);
                
                }
// endif;
            }


            if ($url_stack == 1) {
                $url_stack = $this->request->data['url'];
            }
            $previous_storage_used = $this->Account->getPreviousStorageUsed($account_id);
            $url = "$account_id/$account_folder_id/" . date('Y/m/d') . "/" . $this->request->data['video_url'];

            $path_parts = pathinfo($this->request->data['video_url']);
            $video_file_name = $path_parts['filename'] . "." . $path_parts['extension'];

            $path_parts_orig = pathinfo($this->request->data['video_file_name']);
            $request_dump = addslashes(print_r($_POST, true));
//$url_stack = explode('/',$url_stack );
            $this->Document->create();
            $data = array(
                'account_id' => $account_id,
                'doc_type' => '2',
                'url' => $url,
                'original_file_name' => $this->request->data['video_file_name'],
                'active' => 1,
                'created_date' => date("Y-m-d h:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d h:i:s"),
                'last_edit_by' => $user_id,
                'published' => '1',
                'stack_url' => $url_stack,
                'post_rubric_per_video' => '1'
            );


            if ($this->Document->save($data, $validation = TRUE)) {

                $document_id = $this->Document->id;

                $folder_type = $this->get_folder_type($account_folder_id);
                if ($folder_type == '1') {
                    $url = $this->base . '/Huddles/view/' . $account_folder_id . '/2';
                } elseif ($folder_type == '3') {
                    $url = $this->base . '/MyFiles/view/2';
                } else {
                    $url = $this->base . '/videoLibrary/view/' . $account_folder_id;
                }
                if ($view->Custom->check_if_eval_huddle($account_folder_id)) {
                    $url = $this->base . '/Huddles/view/' . $account_folder_id . '/1/' . $video_id;
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $url,
                        'type' => '3',
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                } else {
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $url,
                        'type' => '3',
// 'account_folder_id' => ($video_id != '' && $video_id != 'undefined') ? $video_id : $account_folder_id,
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                }


                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

                require('src/services.php');
//uploaded video
                $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                $s3_src_folder_path = $this->request->data['video_url'];

//echo $uploaded_video_file; die;
                $s3_service = new S3Services(
                        Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                );
                $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);

                $this->Document->create();
                $this->Document->updateAll(
                        array(
                    'zencoder_output_id' => 0,
                    'url' => "'" . addslashes($s3_dest_folder_path) . "'",
                    'file_size' => (int) $this->request->data['video_file_size'],
                    'request_dump' => "'" . $request_dump . "'"
                        ), array(
                    'id' => $document_id
                        )
                );

                if ($previous_storage_used['Account']['storage_used'] != '') {
                    $storage_in_used = $this->request->data['video_file_size'] + $previous_storage_used['Account']['storage_used'];
                } else {
                    $storage_in_used = $this->request->data['video_file_size'];
                }

                $this->Account->updateAll(array('storage_used' => $storage_in_used), array('id' => $account_id));
                $this->job_queue_storage_function($account_id, '4');
                $this->AccountFolderDocument->create();
                $data = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $document_id,
                    'title' => $path_parts_orig['filename'],
                    'desc' => $this->request->data['video_desc']
                );
                $this->AccountFolderDocument->save($data, $validation = TRUE);

                if (!empty($video_id) && $video_id != "undefined") {
                    $account_folder_document_id = $this->AccountFolderDocument->id;
                    $this->AccountFolderDocumentAttachment->create();
                    $data = array(
                        'account_folder_document_id' => $account_folder_document_id,
                        'attach_id' => "$video_id"
                    );
                    $this->AccountFolderDocumentAttachment->save($data, $validation = TRUE);
                }

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
                if ($view->Custom->check_if_eval_huddle($account_folder_id)) {
                    $documentData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'video_link' => $this->base . '/Huddles/view/' . $account_folder_id . '/1/' . $video_id,
                        'participating_users' => $huddleUsers
                    );
                } else {
                    $documentData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'video_link' => '/Huddles/view/' . $account_folder_id . '/2/',
                        'participating_users' => $huddleUsers
                    );
                }

                if ($view->Custom->check_if_eval_huddle($account_folder_id) == 1) {
                    $all_participants = array();
                    if ($huddleUsers) {
                        foreach ($huddleUsers as $usre) {
                            if ($usre['huddle_users']['role_id'] == 210) {
                                $all_participants[] = $usre['huddle_users']['user_id'];
                            }
                        }
                    }
                }

                $video_details = $view->Custom->get_video_details($video_id);
                $vide_created_by = $video_details['Document']['created_by'];

                foreach ($huddleUsers as $row) {
                    if ($row['User']['id'] == $user_id) {
                        continue;
                    }
                    if ($view->Custom->check_if_eval_huddle($account_folder_id) == 1) {
                        if ($view->Custom->check_if_evaluated_participant($account_folder_id, $user_id)) {
                            if (isset($all_participants) && is_array($all_participants) && in_array($row['User']['id'], $all_participants)) {
                                continue;
                            }
                        } else {

                            if ($vide_created_by != $row['User']['id']) {
                                continue;
                            }
                        }
                        $documentData['huddle_type'] = 3;
                    } else {
                        $documentData['huddle_type'] = '';
                    }
                    $documentData['email'] = $row['User']['email'];
                    $documentData['user_id'] = $row['User']['id'];
                    if ($this->check_subscription($row['User']['id'], '1', $account_id)) {
                        $this->sendDocumentEmail($documentData);
                    }
                }
            } else {
                $document_id = 0;
                $s_url = '';
            }

            $this->layout = null;
            $res = array(
                'document_id' => $document_id,
                'url' => $s_url
            );
            $this->set('ajaxdata', json_encode($res));
            $this->render('/Elements/ajax/ajaxreturn');
        } else {
            $this->set('huddle_id', $account_folder_id);
            $this->set('upload_type', 'doc');
            $this->layout = 'fileupload';
            $this->render('uploadVideos');
        }
    }

    function tempdocupload() {
        $this->layout = null;
        require('VideoUploadHandler.php');
        $upload_handler = new UploadHandler($this->request->data['rand_folder'], 'doc');
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function resubmit_to_zencoder($video_id, $account_id, $account_folder_id) {

        $this->layout = null;

        $video_file = $this->Document->find('first', array('conditions' => array('id' => $video_id)));

        require('src/services.php');
//uploaded video
        $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
        $s3_dest_folder_path = "$s3_dest_folder_path_only/" . $video_file['Document']['original_file_name'];
        $s3_src_folder_path = $video_file['Document']['url'];


//echo $uploaded_video_file; die;
        $s3_service = new S3Services(
                Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
        );


        $s3_service->set_acl_permission($s3_src_folder_path);
        die;

        $s_url = Configure::read('amazon_base_url') . $s3_src_folder_path;
        $s3_zencoder_id = $s3_service->encode_videos($s_url, "uploads/" . $s3_dest_folder_path_only, "uploads/" . $s3_dest_folder_path);
        if ($s3_zencoder_id == FALSE) {

//$this->AccountFolder->updateAll(array('active' => '0'), array('account_folder_id' => $account_folder_id));
//$this->sendVideoUploadingFailed();

            $this->Document->create();
            $this->Document->updateAll(
                    array(
                'zencoder_output_id' => -1,
                'url' => "'$s3_dest_folder_path'",
                'file_size' => (int) $this->request->data['video_file_size']
                    ), array('id' => $video_id)
            );

            $this->AccountFolderDocument->create();
            $data = array(
                'account_folder_id' => $account_folder_id,
                'document_id' => $video_id,
                'title' => $this->request->data['video_title'],
                'zencoder_output_id' => -1,
                'desc' => $this->request->data['video_desc']
            );
            $this->AccountFolderDocument->save($data, $validation = TRUE);


//exit;
        } else {
            $this->Document->create();
            $this->Document->updateAll(
                    array(
                'zencoder_output_id' => $s3_zencoder_id,
                'url' => "'$s3_dest_folder_path'",
                'file_size' => (int) $this->request->data['video_file_size']
                    ), array('id' => $video_id)
            );

            $this->AccountFolderDocument->create();
            $data = array(
                'account_folder_id' => $account_folder_id,
                'document_id' => $video_id,
                'title' => $this->request->data['video_title'],
                'zencoder_output_id' => $s3_zencoder_id,
                'desc' => $this->request->data['video_desc']
            );
            $this->AccountFolderDocument->save($data, $validation = TRUE);
        }

        $this->set('ajaxdata', "submitted");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function uploadVideos($account_folder_id, $account_id, $user_id, $suppress_render = false, $suppress_success_email = false, $workspace = false, $activity_log_id = '') {
        
        $view = new View($this, false);
        if ($this->request->is('post')) {

            if ($workspace) {
                $loggedInUser = $this->Session->read('user_current_account');
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = $view->Custom->check_if_account_in_trial_intercom($account_id);
//      if (IS_QA):

                $meta_data = array(
                    'workspace_video' => 'true',
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );


                // $this->create_intercom_event('workspace-video-uploaded', $meta_data, $loggedInUser['User']['email']);
                $this->create_churnzero_event('Workspace+Video+Uploaded', $account_id, $loggedInUser['User']['email']);
                

//    endif;
            } else {
                $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
                $huddle_detail = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $account_folder_id)));
                $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
                if ($htype == 1) {
                    $huddle_type = 'Collaboration Huddle';
                } elseif ($htype == 2) {
                    $huddle_type = 'Coaching Huddle';
                } else {
                    $huddle_type = 'Assessment Huddle';
                }
                $loggedInUser = $this->Session->read('user_current_account');
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = $view->Custom->check_if_account_in_trial_intercom($account_id);
// if (IS_QA):

                $meta_data = array(
                    'huddle_video' => 'true',
                    'huddle_type' => $huddle_type,
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );
                $view = new View($this, false);
                
                if(isset($huddle_detail['AccountFolder']['folder_type']) && $huddle_detail['AccountFolder']['folder_type'] == '2' )
                {
                    $this->create_churnzero_event('Library+Video+Uploaded', $account_id, $loggedInUser['User']['email']);
                    
                }
                else
                {

                        if ($huddle_type == 'Assessment Huddle' && !$view->Custom->check_if_evalutor($account_folder_id, $loggedInUser['User']['id'])) {

                            // $this->create_intercom_event('huddle-video-submitted-for-assessment', $meta_data, $loggedInUser['User']['email']);
                            $this->create_churnzero_event('Huddle+Video+Uploaded', $account_id, $loggedInUser['User']['email']);
                        } else {

                            // $this->create_intercom_event('huddle-video-uploaded', $meta_data, $loggedInUser['User']['email']);
                            $this->create_churnzero_event('Huddle+Video+Uploaded', $account_id, $loggedInUser['User']['email']);
                        }
                
                }

//  endif;
            }

            $previous_storage_used = $this->Account->getPreviousStorageUsed($account_id);

            $current_user = $this->User->find('first', array('conditions' => array('id' => $user_id)));
            $current_account_folder = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $account_folder_id)));

            $account_folder_type_string = "Workspace";
            $video_title = $this->request->data['video_title'];

            if ($current_account_folder['AccountFolder']['folder_type'] == 1) {
                $account_folder_type_string = $current_account_folder['AccountFolder']['name'];
                if (empty($video_title)) {
                    $video_title = $account_folder_type_string;
                }
            }

            $path_parts = pathinfo($this->request->data['video_url']);
            $video_file_name = $this->cleanFileName($path_parts['filename']) . "." . $path_parts['extension'];
            $video_file_name_only = $this->cleanFileName($path_parts['filename']);

            $request_dump = addslashes(print_r($_POST, true));
            $parent_folder_id = isset($this->request->data['parent_folder_id']) ? $this->request->data['parent_folder_id'] : 0;

            $this->Document->create();
            $data = array(
                'account_id' => $account_id,
                'doc_type' => '1',
                'url' => $this->request->data['video_url'],
                'original_file_name' => $this->request->data['video_file_name'],
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'recorded_date' => date("Y-m-d H:i:s"),
                'file_size' => (int) $this->request->data['video_file_size'],
                'last_edit_by' => $user_id,
                'published' => '0',
                'parent_folder_id' => $parent_folder_id,
                'post_rubric_per_video' => '1'
            );

            if ($this->Document->save($data, $validation = TRUE)) {
                $video_id = $this->Document->id;

                if (!empty($activity_log_id)) {
                    $this->UserActivityLog->updateAll(array('ref_id' => $video_id), array('id' => $activity_log_id));
                }

                if ($current_account_folder['AccountFolder']['folder_type'] == 1) {

                    $user_activity_logs = array(
                        'ref_id' => $video_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $this->base . '/Huddles/view/' . $account_folder_id . "/1/" . $video_id,
                        'account_folder_id' => $account_folder_id,
                        'type' => '2',
                        'environment_type' => 2
                    );
                    $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                } elseif ($current_account_folder['AccountFolder']['folder_type'] == 3) {
                    $user_activity_logs = array(
                        'ref_id' => $video_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $this->base . '/MyFiles/view/1/' . $account_folder_id,
                        'account_folder_id' => $account_folder_id,
                        'type' => '2',
                        'environment_type' => 2
                    );
                    $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                }


                include_once "src/services.php";

//require('src/services.php');
                $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                $s3_src_folder_path = $this->request->data['video_url'];

                $s3_service = new S3Services(
                        Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                );

                $s3_service->set_acl_permission($s3_src_folder_path);
// $video_file_size_in_mbs = isset($this->request->data['video_file_size']) && !empty($this->request->data['video_file_size']) ? number_format($this->request->data['video_file_size'] / 1048576, 2) : 0;
                $video_file_size_in_int = isset($this->request->data['video_file_size']) ? (int) $this->request->data['video_file_size'] : 0;
                $video_file_size_in_mbs = !empty($video_file_size_in_int) ? $video_file_size_in_int / 1048576 : 0;

                if ($video_file_size_in_mbs > 800 || $video_file_size_in_mbs === 0) {
                    $this->request->data['direct_publish'] = 0;
                }

                if ($this->request->data['direct_publish'] == 0) {

                    if ($suppress_success_email == false) {
                        $s3_zencoder_id = $this->CreateJobQueueTranscodingJob($video_id, $s3_src_folder_path);
                    } else {
                        $s3_zencoder_id = $this->CreateJobQueueTranscodingJob($video_id, $s3_src_folder_path, $suppress_success_email);
                    }
                }

                if ($this->request->data['direct_publish'] == 1) {
                    try {
                        $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name_only" . "_enc.mp4";
                        $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);
                        $s3_zencoder_id = $s3_service->encode_videos($s_url, "uploads/" . $s3_dest_folder_path_only, "uploads/" . $s3_dest_folder_path, Configure::read('encoder_provider'));
                    } catch (Exception $e) {
                        $user_activity_logs = array(
                            'ref_id' => $user_id,
                            'desc' => 'Error @ Line:' . __LINE__ . ' ' . $e->getMessage(),
                            'url' => 'uploadVideos',
                            'type' => '313',
                            'account_id' => $account_id,
                            'account_folder_id' => $account_folder_id,
                            'environment_type' => '2',
                        );
                        $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

                        var_dump("Line: " . __LINE__, http_response_code($e->getMessage()));
                        echo "<hr>";
                        var_dump($e->getMessage());
                        exit;
                    }
//revert back to actual destination.

                    $s3_dest_folder_path = "uploads/" . $s3_dest_folder_path;

                    $this->Document->updateAll(
                            array(
                        'zencoder_output_id' => "'$s3_zencoder_id'",
                        'url' => "'$s3_dest_folder_path'",
                        'encoder_provider' => Configure::read('encoder_provider'),
                        'file_size' => (!isset($this->request->data['video_file_size']) ? 0 : $this->request->data['video_file_size']),
                        'published' => 1
                            ), array('id' => $video_id)
                    );

                    $video_data = array(
                        'document_id' => $video_id,
                        'url' => $s3_dest_folder_path,
                        'duration' => 0,
                        'file_size' => 0,
                        'transcoding_job_id' => $s3_zencoder_id,
                        'transcoding_status' => 2,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'debug_logs'=>"Cake::AppController::uploadVideos::line=3498::document_id=$video_id::status=2::created"
                    );
                    $this->checkDocumentFilesRecordExist($video_id, $video_data, $this->request->data);
                    $this->DocumentFiles->create();
                    $this->DocumentFiles->save($video_data);
                } else {
                    $this->Document->create();
                    $this->Document->updateAll(
                            array(
                        'zencoder_output_id' => $s3_zencoder_id,
                        'encoder_provider' => Configure::read('encoder_provider'),
                        'file_size' => (int) $this->request->data['video_file_size'],
                        'request_dump' => "'$request_dump'"
                            ), array('id' => $video_id)
                    );
                }

                if ($previous_storage_used['Account']['storage_used'] != '') {
                    $new_storage_used = $this->request->data['video_file_size'] + $previous_storage_used['Account']['storage_used'];
                } else {
                    $new_storage_used = $this->request->data['video_file_size'];
                }
                $this->Account->updateAll(array('storage_used' => $new_storage_used), array('id' => $account_id));
                $this->job_queue_storage_function($account_id, '4');
                $this->AccountFolderDocument->create();
                $data = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $video_id,
                    'title' => $this->request->data['video_title'],
                    'zencoder_output_id' => "$s3_zencoder_id",
                    'encoder_provider' => Configure::read('encoder_provider'),
                    'desc' => $this->request->data['video_desc']
                );
                $this->AccountFolderDocument->save($data, $validation = TRUE);
                $loggedInUser = $this->Session->read('user_current_account');
                $Videoduration = $this->getVideoDurationProduction($video_id);
                $this->create_churnzero_event('Video+Hours+Uploaded', $account_id, $loggedInUser['User']['email'],$Videoduration); 
            } else {
                $video_id = 0;
            }

            if ($suppress_render == true) {

                return $video_id;
            } else {

                $this->layout = null;
                $res = array(
                    'video_id' => $video_id,
//'url' => $s_url
                    'url' => ''
                );
//$this->set('ajaxdata', json_encode($res));
//$this->render('/Elements/ajax/ajaxreturn');
                echo json_encode($res);
                die;
                return true;
            }
        } else {
            $this->set('huddle_id', $account_folder_id);
            $this->set('upload_type', 'video');
            $this->layout = 'fileupload';
            $this->render('uploadVideos');
        }
    }

    function video_success_message($document_id, $account_folder_id) {

        $current_user = $this->Session->read('user_current_account');
        $account_id = $current_user['accounts']['account_id'];

        $is_video_published_now = false;

        $video_old = $this->Document->find('first', array('conditions' => array(
                'id' => $document_id)
        ));


        if (isset($video_old) && !empty($video_old)) {


            $is_video_published_now = true;


            $video = $this->Document->find('first', array('conditions' => array('id' => $document_id)));
            $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $document_id)));
            $account_folder = $this->AccountFolder->find('first', array('conditions' => array(
                    'account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'])
            ));
            $creator = $this->User->find('first', array('conditions' => array('id' => $video['Document']['created_by'])));
            $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_document['AccountFolderDocument']['account_folder_id']);


            if ($account_folder['AccountFolder']['folder_type'] != 1) {
                if ($this->check_subscription($creator['User']['id'], '7', $account_id)) {

                    $video_name = $account_folder_document['AccountFolderDocument']['title'];

                    if (empty($video_name))
                        $video_name = $account_folder['AccountFolder']['name'];

                    var_dump($creator);
                    $this->sendVideoPublishedNotification(
                            $video_name
                            , $account_folder['AccountFolder']['account_folder_id']
                            , $video['Document']['id']
                            , $creator
                            , $account_folder['AccountFolder']['account_id']
                            , $creator, $creator['User']['email']
                            , $creator['User']['id']
                            , $account_folder['AccountFolder']['folder_type']
                            , '7'
                    );
                }
            } else {

                if (count($huddleUsers) > 0) {
                    for ($i = 0; $i < count($huddleUsers); $i++) {

                        $huddleUser = $huddleUsers[$i];

                        if ($is_video_published_now) {
                            if ($this->check_subscription($huddleUser['User']['id'], '8', $account_id)) {
                                $this->sendVideoPublishedNotification(
                                        $account_folder['AccountFolder']['name']
                                        , $account_folder['AccountFolder']['account_folder_id']
                                        , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $huddleUsers, $huddleUser['User']['email'], $huddleUser['User']['id'], $account_folder['AccountFolder']['folder_type'], '8');
                            }
                        }
                    }
                }
            }
        }
    }

    function sendVideoUploadingFailed($creator_email = '', $video_type = '', $video_title = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];

        if (empty($creator_email)) {
            $this->Email->to = Configure::read('email_address');
            $this->Email->bcc = array('khurri.saleem@gmail.com', 'mdonaho@techwerksinc.com');
        } else {
            $this->Email->to = $creator_email;
            $this->Email->bcc = array('khurri.saleem@gmail.com', 'mdonaho@techwerksinc.com');
        }

        $view = new View($this, false);
        $this->Email->subject = "Video Uploading Failed";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $params = array(
            'video_type' => $video_type,
            'video_title' => $video_title
        );

        $view = new View($this, true);
        $lang = $users['User']['lang'];
        $key = "video_uploading_failed_" . $this->site_id . "_" . $lang;
        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{video_title}', $video_title, $html);
        $html = str_replace('{support_email}', $view->custom->get_site_settings('static_emails')['support'], $html);
        $html = str_replace('{video_type}', $video_type, $html);
        $html = str_replace('{contact_email}', $view->custom->get_site_settings('static_emails')['info'], $html);
//$html = $view->element('emails/video_uploading_failed', $params);
        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $this->Email->to,
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => false,
            'sent_date' => date("Y-m-d H:i:s")
        );
        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function uploadAttachementToS3($data, $account_id, $account_folder_id) {

        if (!empty($data['tmp_name'])) {

            if (!is_uploaded_file($data['tmp_name'])) {
                return FALSE;
            }

            $users = $this->Session->read('user_current_account');
            $user_id = $users['User']['id'];

            $s3_folder_path = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
            require_once('src/services.php');
//uploaded video

            $uploaded_video_file = realpath(dirname(__FILE__) . '/../..') . "/app/webroot/files/tempupload/" . $data['name'];

            if (file_exists($uploaded_video_file))
                unlink($uploaded_video_file);

            if (move_uploaded_file($data['tmp_name'], $uploaded_video_file)) {

                $path_parts = pathinfo($uploaded_video_file);

                $new_uploaded_video_file = str_replace(basename($uploaded_video_file), $this->cleanFileName($path_parts['filename']) . "." . $path_parts['extension'], $uploaded_video_file);
                rename($uploaded_video_file, $new_uploaded_video_file);

                $uploaded_video_file = $new_uploaded_video_file;

                $path_parts = pathinfo($uploaded_video_file);
                $video_file_name = $path_parts['filename'] . "." . $path_parts['extension'];

                $s3_service = new S3Services(
                        Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                );
                $s_url = $s3_service->publish_to_s3($uploaded_video_file, "uploads/" . $s3_folder_path . "/", false);

                $this->Document->create();
                $data = array(
                    'account_id' => $account_id,
                    'doc_type' => 2,
                    'url' => $s3_folder_path . "/" . $video_file_name,
                    'original_file_name' => $video_file_name,
                    'active' => 1,
                    'created_date' => date("Y-m-d h:i:s"),
                    'created_by' => $user_id,
                    'last_edit_date' => date("Y-m-d h:i:s"),
                    'last_edit_by' => $user_id,
                    'published' => '1',
                    'post_rubric_per_video' => '1'
                );

                $this->Document->save($data, $validation = TRUE);

                return $this->Document->id;
            } else {
                return false;
            }
        } else {
            return FALSE;
        }
    }

    function uploadPhysicalVideoToS3($temp_path, $account_id, $account_folder_id, $user_id) {

        if (!empty($temp_path)) {

            $s3_folder_path = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
            require('src/services.php');
//uploaded video

            $uploaded_video_file = $temp_path;
            $path_parts = pathinfo($uploaded_video_file);
            $video_file_name = $path_parts['filename'] . "." . $path_parts['extension'];

            if (file_exists($uploaded_video_file)) {

                $s3_service = new S3Services(
                        Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                );

                $s_url = $s3_service->publish_to_s3($uploaded_video_file, "uploads/" . $s3_folder_path . "/", false);
                $s3_zencoder_id = $s3_service->encode_videos($s_url, "uploads/" . $s3_folder_path, "uploads/" . $s3_folder_path . "/$video_file_name");
                if ($s3_zencoder_id == FALSE) {

//$this->AccountFolder->updateAll(array('activ' => '0'), array('account_folder_id' => $account_folder_id));
//$this->sendVideoUploadingFailed();

                    $this->Document->create();
                    $data = array(
                        'account_id' => $account_id,
                        'doc_type' => '1',
                        'url' => $s3_folder_path . "/" . $video_file_name,
                        'original_file_name' => $video_file_name,
                        'active' => 1,
                        'created_date' => date("Y-m-d h:i:s"),
                        'created_by' => $user_id,
                        'last_edit_date' => date("Y-m-d h:i:s"),
                        'last_edit_by' => $user_id,
                        'published' => '0',
                        'zencoder_output_id' => -1,
                        'file_size' => filesize($uploaded_video_file),
                        'post_rubric_per_video' => '1'
                    );

                    $this->Document->save($data, $validation = TRUE);
                    unlink($uploaded_video_file);

                    $video_id = $this->Document->id;

                    $this->AccountFolderDocument->create();
                    $data = array(
                        'account_folder_id' => $account_folder_id,
                        'document_id' => $video_id,
                        'title' => $_POST['title'],
                        'zencoder_output_id' => -1,
                        'desc' => $_POST['title']
                    );
                    $this->AccountFolderDocument->save($data, $validation = TRUE);
                } else {
                    $this->Document->create();
                    $data = array(
                        'account_id' => $account_id,
                        'doc_type' => '1',
                        'url' => $s3_folder_path . "/" . $video_file_name,
                        'original_file_name' => $video_file_name,
                        'active' => 1,
                        'created_date' => date("Y-m-d h:i:s"),
                        'created_by' => $user_id,
                        'last_edit_date' => date("Y-m-d h:i:s"),
                        'last_edit_by' => $user_id,
                        'published' => '0',
                        'zencoder_output_id' => $s3_zencoder_id,
                        'file_size' => filesize($uploaded_video_file),
                        'post_rubric_per_video' => '1'
                    );

                    $this->Document->save($data, $validation = TRUE);
                    unlink($uploaded_video_file);

                    $video_id = $this->Document->id;

                    $this->AccountFolderDocument->create();
                    $data = array(
                        'account_folder_id' => $account_folder_id,
                        'document_id' => $video_id,
                        'title' => $_POST['title'],
                        'zencoder_output_id' => $s3_zencoder_id,
                        'desc' => $_POST['title']
                    );
                    $this->AccountFolderDocument->save($data, $validation = TRUE);

                    return $video_id;
                }
            } else {
                return false;
            }
        } else {
            return FALSE;
        }
    }

    public function downloadZip() {
// $this->auth_huddle_permission('downloadZip', $this->Session->read('account_folder_id'));
        $this->layout = null;

        $document_ids = $this->request->data['download_as_zip_docs'];
        $documents_array = explode(',', $document_ids);

        if (count($documents_array) == 1) {
            $this->download($document_ids);
        }

        $temp_dir = WWW_ROOT . 'files/tempupload/' . strtotime('now');
        if (!is_dir($temp_dir)) {
            mkdir($temp_dir);
        }

        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name');
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));
        $client->registerStreamWrapper();

        $zip_file_name = $temp_dir . "/" . rand() . ".zip";
        $zip = new ZipArchive();
        $zip->open($zip_file_name, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);

        $countFiles = array();
        for ($i = 0; $i < count($documents_array); $i++) {
            $id = trim($documents_array[$i]);

            $file = $this->Document->findById($id);
            $original_file_name = $file['Document']['original_file_name'];
            $renamed_title = $this->AccountFolderDocument->get_document_name($id);

            $documentFilePath = pathinfo($file['Document']['url']);

            $document_files_array = $this->get_document_url($file['Document']);
            $document_url = $document_files_array['relative_url'];


            if ($file['Document']['doc_type'] == '2') {
                $document_url = "uploads/" . $file['Document']['url'];
            }

            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $aws_bucket = Configure::read('bucket_name');
            $expires = '+60 minutes';

// Create an Amazon S3 client object
            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            $result = $client->getObjectUrl(
                    $aws_bucket, $document_url, $expires
            );

            $result = str_replace("https://", "http://", "$result");
            $uri = $result;
            $destination = $temp_dir . '/' . $documentFilePath['basename'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $uri);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSLVERSION, 3);
            $data = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);

            $file = fopen($destination, "w+");
            fputs($file, $data);
            fclose($file);

            $ext = "";
            if (!empty($renamed_title)) {
                if (empty(pathinfo($renamed_title)['extension'])) {
                    $ext = "." . $documentFilePath['extension'];
                }
            }
            $filenameInZip = !empty($renamed_title) ? $renamed_title . $ext : $original_file_name;

            $zip->addFile($destination, $filenameInZip);
        }

        if (0 == count($documents_array)) {
            $destination = $temp_dir . '/NoFile';
            $file = fopen($destination, 'w+');
            fwrite($file, 'NO FILE DOWNLOAD');
            fclose($file);
            $zip->addFile($destination, 'NoFile');
        }
        $zip->close();

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename = "documents.zip"');
        header('Content-Length: ' . filesize($zip_file_name));
        header("Content-Transfer-Encoding: binary");
        session_write_close();
        readfile($zip_file_name);
        unlink($zip_file_name);

        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    public function render_jnlp() {

        $this->layout = null;
        $root_url = Configure::read('sibme_base_url');

        $users = $this->Session->read('user_current_account');
        $authentication_token = $users['User']['id'];
        $account_id = $users['accounts']['account_id'];

        $jnlp = '<?xml version = "1.0" encoding = "UTF-8"
?>
<jnlp spec="1.0+" codebase="https://app.sibme.com/uploadclient">
    <information>
        <title>Sibme Video Uploader</title>
        <vendor>Sibme</vendor>
    </information>
    <resources>
        <!-- Application Resources -->
        <j2se version="1.6+" href="http://java.sun.com/products/autodl/j2se"/>
        <jar href="SibmeUploadClient-2013-01-19.jar" main="true" />
        <property name="authenticity_token" value="' . $authentication_token . '" />
        <property name="sibme.endpoint" value="' . $root_url . 'Huddles/amazon_jnlp_signature" />
        <property name="sibme.upload.signatures" value="' . $root_url . 'Huddles/amazon_jnlp_signature" />
        <property name="jnlp.sibme.upload.success" value="https://app.sibme.com/Huddles/amazon_jnlp_success/' . $authentication_token . '/' . $account_id . '" />
        <property name="sibme.upload.failure" value="' . $root_url . 'Huddles/amazon_jnlp_failure" />
    </resources>
    <application-desc
        name="Sibme Video Upload Client"
        main-class="org.eclipse.jdt.internal.jarinjarloader.JarRsrcLoader">
        <argument>jnlp.sibme.upload.success=https://app.sibme.com/Huddles/amazon_jnlp_success/' . $authentication_token . '</argument>
    </application-desc>
    <update check="always" policy="always"/>
    <security>
        <all-permissions/>
    </security>
</jnlp>';

        header('Content-Type: application/x-java-jnlp-file');
        header('Content-Disposition: attachment; filename=sibme-' . $authentication_token . '.jnlp');

        $this->set('ajaxdata', "$jnlp");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    public function download($id) {

        $this->layout = null;

        $file = $this->Document->findById($id);
//$document_url = $file['Document']['url'];

        if ($file['Document']['doc_type'] == 1) {

            $document_files_array = $this->get_document_url($file['Document']);
            if (empty($document_files_array['url'])) {
                $file['Document']['published'] = 0;
                $document_files_array['url'] = $file['Document']['original_file_name'];
                $document_files_array['file_path'] = $file['Document']['url'];
                $file['Document']['encoder_status'] = $document_files_array['encoder_status'];
            } else {
                $file['Document']['encoder_status'] = $document_files_array['encoder_status'];
            }

            $document_url = $document_files_array['relative_url'];

            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $aws_bucket = Configure::read('bucket_name');
            $expires = '+20 minutes';

// Create an Amazon S3 client object
            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));



            if (!empty($document_url)) {
                $response = $client->doesObjectExist($aws_bucket, $document_url);
                if ($response) {

                } else {

                    if (!empty($file['Document']['url']) && isset($file['Document']['url'])) {
                        $document_url = $file['Document']['url'];
                        $response = $client->doesObjectExist($aws_bucket, $document_url);
                        if ($response) {

                        } else {
                            $this->Session->setFlash($this->language_based_messages['this_video_not_available_for_download_msg'], 'default', array('class' => 'message'));
                            $this->redirect($this->referer());
                        }
                    } else {
                        $this->Session->setFlash($this->language_based_messages['this_video_not_available_for_download_msg'], 'default', array('class' => 'message'));
                        $this->redirect($this->referer());
                    }
                }
            } else {

                if (!empty($file['Document']['url']) && isset($file['Document']['url'])) {
                    $document_url = $file['Document']['url'];
                    $response = $client->doesObjectExist($aws_bucket, $document_url);
                    if ($response) {

                    } else {
                        $this->Session->setFlash($this->language_based_messages['this_video_not_available_for_download_msg'], 'default', array('class' => 'message'));
                        $this->redirect($this->referer());
                    }
                } else {
                    $this->Session->setFlash($this->language_based_messages['this_video_not_available_for_download_msg'], 'default', array('class' => 'message'));
                    $this->redirect($this->referer());
                }
            }


            $file['Document']['original_file_name'] = str_replace(',', '', $file['Document']['original_file_name']);

            $renamed_title = $this->AccountFolderDocument->get_document_name($id);
            $result_data = $this->AccountFolderDocument->get_account_folder_document($id);
            $account_folder_id = $result_data['AccountFolderDocument']['account_folder_id'];
            $ext = "";
            if (!empty($renamed_title)) {
                if (empty(pathinfo($renamed_title)['extension'])) {
                    $ext = "." . pathinfo($file['Document']['original_file_name'])['extension'];
                }
                $filename = $renamed_title . $ext;
            } else {
                $filename = $file['Document']['original_file_name'];
            }

            $result = $client->getObjectUrl(
                    $aws_bucket, $document_url, $expires, array(
                'ResponseContentType' => 'video/mp4',
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . rawurlencode($filename)
                    )
            );

            $user_activity_logs = array(
                'ref_id' => $id,
                'desc' => 'Download Resource',
                'url' => $this->base . '/Huddles/download/' . $id,
                'type' => '13',
                'account_folder_id' => $account_folder_id,
                'environment_type' => '2',
            );
            $this->user_activity_logs($user_activity_logs);
            
            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $this->create_churnzero_event('Resource+Viewed', $account_id, $users['User']['email']);
            

            header('Location:' . $result);
            die;
        } else {

// $document_url = "uploads/" . $file['Document']['url'];

            $document_files_array = $this->get_document_url($file['Document']);

//$document_url = $document_files_array['relative_url'];
// $document_url = "uploads/" . $file['Document']['url'];    //BEFORE
            $document_url = $file['Document']['url'];

            if ($file['Document']['doc_type'] == 2) {
                $document_url = "uploads/" . utf8_encode($file['Document']['url']);
            }

            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $aws_bucket = Configure::read('bucket_name');
            $expires = '+20 minutes';

// Create an Amazon S3 client object
            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            $renamed_title = $this->AccountFolderDocument->get_document_name($id);
            $ext = "";
            if (!empty($renamed_title)) {
                if (empty(pathinfo($renamed_title)['extension'])) {
                    $ext = "." . pathinfo($file['Document']['original_file_name'])['extension'];
                }
                $filename = $renamed_title . $ext;
            } else {
                $filename = $file['Document']['original_file_name'];
            }

            $result = $client->getObjectUrl(
                    $aws_bucket, $document_url, $expires, array(
//'ResponseContentType' => 'video/mp4',
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' .rawurlencode($filename)
                    )
            );
            $account_folder_id = '';
            $result_data = $this->AccountFolderDocument->get_account_folder_document($id);
            if($result_data ){
                $account_folder_id = $result_data['AccountFolderDocument']['account_folder_id'];
            }

            $user_activity_logs = array(
                'ref_id' => $id,
                'desc' => 'Download Resource',
                'url' => $this->base . '/Huddles/download/' . $id,
                'type' => '13',
                'account_folder_id' => $account_folder_id ,
                'environment_type' => '2',
            );
            $this->user_activity_logs($user_activity_logs);
            
            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $this->create_churnzero_event('Resource+Viewed', $account_id, $users['User']['email']);

            header('Location:' . $result);
            die;
        }
    }

    public function download_zipfolder_make($id, $targetfolder) {

        ini_set('memory_limit', '-1');
        $this->layout = null;

        $file = $this->Document->findById($id);
//$document_url = $file['Document']['url'];

        if ($file['Document']['doc_type'] == 1 || $file['Document']['doc_type'] == 3) {

            $document_files_array = $this->get_document_url($file['Document']);
            if (empty($document_files_array['url'])) {
                $file['Document']['published'] = 0;
                $document_files_array['url'] = $file['Document']['original_file_name'];
                $document_files_array['file_path'] = $file['Document']['url'];
                $file['Document']['encoder_status'] = $document_files_array['encoder_status'];
            } else {
                $file['Document']['encoder_status'] = $document_files_array['encoder_status'];
            }

            $document_url = $document_files_array['relative_url'];
            $document_url_path_info = pathinfo($document_url);

            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $aws_bucket = Configure::read('bucket_name');
            $expires = '+20 minutes';

// Create an Amazon S3 client object
            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            $document_name_user_defined = $document_url_path_info['filename'];

            $account_folder_document = $this->AccountFolderDocument->get($file['Document']['id']);
            if (!empty($account_folder_document)) {

                $account_folder_document_file_name = $account_folder_document['AccountFolderDocument']['title'];
                $account_folder_document_file_name = str_replace(".", " ", $account_folder_document_file_name);
                $document_title_path_info = pathinfo($account_folder_document_file_name);
                $document_name_user_defined = $document_title_path_info['filename'];
            }

            $document_name_user_defined = preg_replace('/\\.[^.\\s]{3,4}$/', '', $document_name_user_defined);

            $result = $client->getObjectUrl(
                    $aws_bucket, $document_url, $expires, array(
                'ResponseContentType' => 'video/mp4',
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . rawurlencode($document_name_user_defined) . "." . $document_url_path_info['extension']
                    )
            );


            file_put_contents($targetfolder . '/' . ($document_name_user_defined) . "." . $document_url_path_info['extension'], file_get_contents($result));
//return 1;
//  header('Location:' . $result);
        } else {

// $document_url = "uploads/" . $file['Document']['url'];

            $document_files_array = $this->get_document_url($file['Document']);

            $document_url = $document_files_array['relative_url'];

            $document_url_doc = $file['Document']['url'];
            $document_url_path_info = pathinfo($document_url_doc);

            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $aws_bucket = Configure::read('bucket_name');
            $expires = '+20 minutes';

// Create an Amazon S3 client object
            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            if ($file['Document']['doc_type'] == '2') {
                $document_url = "uploads/" . $file['Document']['url'];
            }

            $document_name_user_defined = $document_url_path_info['filename'];

            $account_folder_document = $this->AccountFolderDocument->get($file['Document']['id']);
            if (!empty($account_folder_document)) {
                $account_folder_document_file_name = $account_folder_document['AccountFolderDocument']['title'];
                $account_folder_document_file_name = str_replace(".", " ", $account_folder_document_file_name);
                $document_title_path_info = pathinfo($account_folder_document_file_name);
                $document_name_user_defined = $document_title_path_info['filename'];
            }

            $document_name_user_defined = preg_replace('/\\.[^.\\s]{3,4}$/', '', $document_name_user_defined);

            $result = $client->getObjectUrl(
                    $aws_bucket, $document_url, $expires, array(
//'ResponseContentType' => 'video/mp4',
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . rawurlencode($document_name_user_defined) . "." . $document_url_path_info['extension']
                    )
            );

            file_put_contents($targetfolder . '/' . ($document_name_user_defined) . "." . $document_url_path_info['extension'], file_get_contents($result));
//return 1;
// header('Location:' . $result);
        }
    }

    function preview($id) {
//$this->layout = null;

        $file = $this->Document->findById($id);
//$document_url = $file['Document']['url'];
        if ($file['Document']['doc_type'] == 2) {

            $document_url = "uploads/" . $file['Document']['url'];

            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $aws_bucket = Configure::read('bucket_name');
            $expires = '+20 minutes';

// Create an Amazon S3 client object
            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            $result = $client->getObjectUrl(
                    $aws_bucket, $document_url, $expires, array(
//'ResponseContentType' => 'video/mp4',
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . rawurlencode($file['Document']['original_file_name'])
                    )
            );
//       print_r($result);
            return $this->set('web_url', $result);
            die;
        }
    }

    function downloadFileByStreamWrapper($original_file_name, $document_url) {
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name');

        $uri = "s3://{$aws_bucket}/{$document_url}";

// Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

// Register the stream wrapper from a client object
        $client->registerStreamWrapper();

        if (!file_exists($uri)) {
            $this->Session->setFlash($this->language_based_messages['no_file_exist_directory_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/home/profile-page');
        }

        if (!($stream = fopen($uri, 'r'))) {
            $this->Session->setFlash($this->language_based_messages['could_not_open_stream_msg'], 'default', array('class' => 'message error'));
            $this->redirect('/home/profile-page');
        }

        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header('Content-Type: application/octet-stream');
        header('Content-Length: ' . filesize($uri));
        header('Content-Disposition: attachment; filename="' . rawurlencode($original_file_name) . '"');
        header("Content-Transfer-Encoding: binary");
        session_write_close();

        while (!feof($stream)) {
// Read 4096 bytes from the stream
            echo fread($stream, 4096);
            ob_flush();  // flush output
            flush();
        }
// Be sure to close the stream resource when you're done with it
        fclose($stream);
    }

    function cleanFileName($string) {
        $string = str_replace(" ", "-", $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    function user_activity_logs($data, $l_account_id = '', $l_user_id = '') {

        if (empty($l_account_id) || empty($l_user_id)) {

            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $user_id = $users['User']['id'];

            $data['account_id'] = $account_id;
            $data['user_id'] = $user_id;
        } else {

            $data['account_id'] = $l_account_id;
            $data['user_id'] = $l_user_id;
        }


        $data['date_added'] = date('Y-m-d H:i:s');
        $data['app_name'] = $this->get_app_name();
        $data['site_id'] = $this->site_id;

        $this->UserActivityLog->create();
        $result = $this->UserActivityLog->save($data);

        if ($result) {
            $account_id = $data['account_id'];
            $user_id = $data['user_id'];
            $type = $data['type'];
            $huddle_id = $data['account_folder_id'];
            $this->saveRecordHuddle($account_id,$huddle_id,$user_id,$type);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_app_name() {
        if (isset($_GET['app_name']) && !empty($_GET['app_name'])) {
            return $_GET['app_name'];
        }
        if (isset($_POST['app_name']) && !empty($_POST['app_name'])) {
            return $_POST['app_name'];
        }
        $app_name = $_SERVER['HTTP_HOST'];
        if (!empty($app_name)) {
            return $app_name;
        } else {
            return "NA";
        }
    }

    function get_huddle_name($folder_id) {
        return $this->AccountFolder->getHuddleName($folder_id);
    }

    function get_folder_type($account_folder_id) {
        return $this->AccountFolder->getHuddleType($account_folder_id);
    }

    function randPass($length, $strength = 8) {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength >= 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength >= 2) {
            $vowels .= "AEUY";
        }
        if ($strength >= 4) {
            $consonants .= '23456789';
        }
        if ($strength >= 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }

    function set_permissions($user_id) {
        $not_logged_in_user = $this->User->get($user_id);
        $logged_in_user = $this->Session->read('user_current_account');

        $logged_in_role = $logged_in_user['roles']['role_id'];
        $not_logged_in_role = $not_logged_in_user[0]['roles']['role_id'];

        $permission = '';
        if ($logged_in_role == $not_logged_in_role) {
            $permission = 'allow';
        } elseif ($logged_in_role < $not_logged_in_role) {
            $permission = "allow";
        } elseif ($logged_in_role > $not_logged_in_role) {
            $permission = "allow";
        }
        return $permission;
    }

    function set_promotions($user_id) {
        $not_logged_in_user = $this->User->get($user_id);
        $logged_in_user = $this->Session->read('user_current_account');

        $logged_in_role = $logged_in_user['roles']['role_id'];
        $not_logged_in_role = $not_logged_in_user[0]['roles']['role_id'];

        $permotions = '';
        if ($logged_in_role == $not_logged_in_role) {
            $permotions = 'allow';
        } elseif ($logged_in_role < $not_logged_in_role) {
            $permotions = "allow";
        } elseif ($logged_in_role > $not_logged_in_role) {
            $permotions = "not_allow";
        }
        return $permotions;
    }

    function auth_permission($module = '', $function = '', $current_user_role = '', $created_by_id = '', $current_user_id = '') {
        $logged_in_user = $this->Session->read('user_current_account');
        $user = $this->User->getUserInformation($logged_in_user['users_accounts']['account_id'], $logged_in_user['User']['id']);

        if ($module && $user && count($user) > 0) {
            switch ($module) {
                case 'library':
                    return $this->_library_access($function, $logged_in_user);
                    break;
                case 'people':
                    return $this->_people_access($function, $user['users_accounts']);
                    break;
                case 'permissions':
                    return $this->_permission_access($function, $user['users_accounts']);
                    break;
                case 'huddle':
                    return $this->_permissions_on_huddle($function, $user, $current_user_role, $created_by_id, $current_user_id);
                    break;
            }
        }
    }

    private function _library_access($function, $user_account) {
        
        if ($function == 'index' || $function == 'view' || $function == 'update_view_count' || $function == 'getAllVideos' || $function == 'getVideosByTitle' || $function == 'getSubjectVideos' || $function == 'getTopicVideos' || $function == 'addSubject' || $function == 'addTopic' || $function == 'delete' && $user_account) {
            if ($user_account['users_accounts']['permission_access_video_library'] == TRUE) {
                return TRUE;
            } else {
                $this->no_permissions();
            }
        } elseif ($function == 'add' || $function == 'edit' && $user_account) {

            if ($user_account['roles']['role_id'] == '110' || $user_account['roles']['role_id'] == '100' || $user_account['users_accounts']['permission_video_library_upload'] == TRUE) {
                return TRUE;
            } else {
                $this->no_permissions();
            }
        }
    }

    private function _people_access($function, $user_account) {

        if ($function == 'administrators_groups' || $function == 'addUsers' && $user_account) {
            if ($user_account['permission_administrator_user_new_role'] == '1') {
                return TRUE;
            } else {
                $this->no_permissions();
            }
        }
    }

    private function _permission_access($function, $user_account) {

        if ($function == 'all' && $user_account) {
            if ($user_account['permission_administrator_user_new_role'] == '1') {
                return TRUE;
            } else {
                $this->no_permissions();
            }
        }
    }

    private function _permissions_on_huddle($function, $user, $current_user_role, $created_by_id, $current_user_id) {

        if ($function == 'add' && $user) {
            if ($user['roles']['role_id'] != '120' || $user['users_accounts']['folders_check'] == '1' || $user['users_accounts']['manage_collab_huddles'] == '1' || $user['users_accounts']['manage_coach_huddles'] == '1' || $user['users_accounts']['manage_evaluation_huddles'] == '1') {
                return TRUE;
            } else {
                $this->no_permissions();
            }
        } elseif ($function == 'edit' || $function == 'download' || $function == 'downloadZip' || $function == 'upload' || $function == 'delete' || $function == 'changeTitle' || $function == 'deleteHuddleVideo' || $function == 'deleteComment' || $function == 'editComment' || $current_user_role || $created_by_id || $current_user_id) {

            if ($current_user_role == 200 || ($this->is_creator($current_user_id, $created_by_id) && $user['users_accounts']['permission_maintain_folders'] == '1')) {
                return TRUE;
            } else if ($current_user_role == 210 && $this->is_creator($current_user_id, $created_by_id)) {
                return TRUE;
            } else {
                $this->no_permissions();
            }
        } elseif ($function == 'comments' && $user || $current_user_role || $created_by_id || $current_user_id) {
            if ($current_user_role == 200 || $current_user_role == 210 || ($this->is_creator($current_user_id, $created_by_id) && $user['users_accounts']['permission_maintain_folders'] == '1')) {
                return TRUE;
            } else {
                $this->no_permissions();
            }
        }
    }

    function has_admin_access($huddleUsers, $account_folder_groups, $user_id) {

        $huddle_role = FALSE;
        if ($huddleUsers && count($huddleUsers) > 0 || $account_folder_groups && count($account_folder_groups) > 0 && $user_id != '') {
            foreach ($huddleUsers as $huddle_user) {
                if ($huddle_user['huddle_users']['user_id'] == $user_id) {
                    $huddle_role = $huddle_user['huddle_users']['role_id'];
                    return $huddle_role;
                } else {
                    $huddle_role = FALSE;
                }
            }
            if ($huddle_role != '') {
                return $huddle_role;
            } else {
                if ($account_folder_groups && count($account_folder_groups) > 0) {
                    foreach ($account_folder_groups as $groups) {

                        if ($groups['user_groups']['user_id'] == $user_id) {
                            $huddle_role = $groups['AccountFolderGroup']['role_id'];
                            return $huddle_role;
                        } else {
                            $huddle_role = FALSE;
                        }
                    }
                } else {
                    return FALSE;
                }
            }
        } else {

            return FALSE;
        }
    }

    function is_creator($created_by_id, $user_id) {
        if ($created_by_id == $user_id) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function auth_huddle_permission($function, $huddle_id, $video_id = '') {
        $huddle = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));
        $created_by = '';
        if (isset($huddle['AccountFolder']['created_by'])) {
            $created_by = $huddle['AccountFolder']['created_by'];
        }

        if (!empty($video_id) && $function == "deleteHuddleVideo") {
            $video = $this->Document->getVideoDetails($video_id);

            if (isset($video)) {
                $created_by = $video['Document']['created_by'];
            }
        }

        $huddle_inforation = $this->AccountFolder->getHuddleUsers($huddle_id);
        $account_folder_groups = $this->AccountFolderGroup->getHuddleGroups($huddle_id);
        $current_user_role = $this->has_admin_access($huddle_inforation, $account_folder_groups, $this->user_id);
        $this->auth_permission('huddle', $function, $current_user_role, $created_by, $this->user_id);
    }

    function no_permissions() {
        echo $this->getRedirectJson(Configure::read('sibme_base_url').'NoPermissions/no_permission', true);
        exit;
        // $this->redirect('/NoPermissions/no_permission');
    }

    function hmacsha1($key, $data) {
        $blocksize = 64;
        $hashfunc = 'sha1';
        if (strlen($key) > $blocksize)
            $key = pack('H*', $hashfunc($key));
        $key = str_pad($key, $blocksize, chr(0x00));
        $ipad = str_repeat(chr(0x36), $blocksize);
        $opad = str_repeat(chr(0x5c), $blocksize);
        $hmac = pack(
                'H*', $hashfunc(
                        ($key ^ $opad) . pack(
                                'H*', $hashfunc(
                                        ($key ^ $ipad) . $data
                                )
                        )
                )
        );
        return bin2hex($hmac);
    }

    /*
     * Used to encode a field for Amazon Auth
     * (taken from the Amazon S3 PHP example library)
     */

    function hex2b64($str) {
        $raw = '';
        for ($i = 0; $i < strlen($str); $i += 2) {
            $raw .= chr(hexdec(substr($str, $i, 2)));
        }
        return base64_encode($raw);
    }

    function get_secure_amazon_url($filePath, $name) {
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name');
        $expires = '+20 minutes';

// Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

        $url = $client->getObjectUrl($aws_bucket, $filePath, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : rawurlencode(basename($filePath)) ),
        ));

        return $url;
    }

    function is_account_expired() {
        $user_current_account = $this->Session->read('user_current_account');
        if (isset($user_current_account) && $user_current_account['accounts']['account_id'] != '') {
            $result = $this->Account->find('first', array('conditions' => array('id' => $user_current_account['accounts']['account_id'], 'in_trial' => '1')));
            if ($result) {
                $trial_duration = Configure::read('trial_duration');
                $account_create_date = $result['Account']['created_at'];
                $this->Session->write('account_created_at', $account_create_date);
                $account_end_date = date('Y-m-d H:i:s', strtotime("+{$trial_duration} day", strtotime($account_create_date)));
                $current_date = date('Y-m-d', time());
                $expiry_date = date('Y-m-d', strtotime($account_end_date));
                $current_date = strtotime($current_date);
                $expiry_date = strtotime($expiry_date);
                $view = new View($this, true);
                if ($current_date > $expiry_date) {
                    if ($user_current_account['roles']['role_id'] == 100) {
                        $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_in_order_msg'], ['trial_duration' => $trial_duration]), 'default', array('class' => 'message error'));
                    } else {
                        $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]), 'default', array('class' => 'message error'));
                    }
                } else {
                    return true;
                }
            }
        }
    }

    function send_subscription_email($error_message) {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $users['User']['email'];
        $this->Email->subject = "Credit Card Notification";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $params = array(
            'error_message' => $error_message,
            'user_id' => $users['User']['id']
        );

        $view = new View($this, true);
        $html = $view->element('emails/send_email_notification', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $this->Email->to,
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => false,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function check_subscription($user_id, $email_id, $account_id) {
        $result = $this->EmailUnsubscribers->find('count', array('conditions' => array('user_id' => $user_id, 'email_format_id' => $email_id, 'account_id' => $account_id)));
        if ($result == 0) {
            $users_accounts = $this->UserAccount->find('first', array('conditions' => array(
                'user_id' => $user_id,
                'account_id' =>  $account_id
            )));

            if($users_accounts){
                return TRUE;
            }else{
                return FALSE;
            }
           
        } else {
            return FALSE;
        }
    }

    function one_week_left($user) {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = "One week left of your trial";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'first_name' => $users['User']['first_name'],
            'last_name' => $users['User']['last_name'],
            'account_id' => $user['Account']['id'],
            'trial_end_date' => $user['User']['trial_end_date']
        );

        $html = $view->element('emails/one_week_left', $params);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function two_week_left($user) {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = "Two weeks into your trial";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'first_name' => $users['User']['first_name'],
            'last_name' => $users['User']['last_name'],
            'account_id' => $user['Account']['id'],
            'trial_end_date' => $user['User']['trial_end_date']
        );

        $html = $view->element('emails/two_weeks_left', $params);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function last_day($user) {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $user['User']['email'];
        $this->Email->subject = "Last day of your trial";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'first_name' => $users['User']['first_name'],
            'last_name' => $users['User']['last_name'],
            'account_id' => $user['Account']['id'],
            'trial_end_date' => $user['User']['trial_end_date']
        );

        $html = $view->element('emails/last_day', $params);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function team_plan() {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $users['User']['email'];
        $this->Email->subject = "Thanks for becoming a Team Plan subscriber!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'first_name' => $users['User']['first_name'],
            'last_name' => $users['User']['last_name'],
        );

        $html = $view->element('emails/team_plan', $params);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function deprartment_plan() {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $users['User']['email'];
        $this->Email->subject = "Thanks for becoming a Department Plan subscriber!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'first_name' => $users['User']['first_name'],
            'last_name' => $users['User']['last_name']
        );

        $html = $view->element('emails/department_plan', $params);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function school_plan_account() {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $users['User']['email'];
        $this->Email->subject = "Thanks for becoming a School Plan subscriber!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'first_name' => $users['User']['first_name'],
            'last_name' => $users['User']['last_name']
        );

        $html = $view->element('emails/school_plan_account', $params);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function school_plus_plan() {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $users['User']['email'];
        $this->Email->subject = "Thanks for becoming a School Plus subscriber!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'first_name' => $users['User']['first_name'],
            'last_name' => $users['User']['last_name']
        );

        $html = $view->element('emails/school_plus_plan', $params);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function cancel_account() {
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $users['User']['email'];
        $this->Email->subject = "We will miss you!";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'first_name' => $users['User']['first_name'],
            'last_name' => $users['User']['last_name']
        );

        $html = $view->element('emails/cancel_account', $params);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $this->Email->to, $this->Email->subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function CreateJobQueueTranscodingJob($document_id, $temp_upload_path, $suppress_success_message = false) {
        if ($suppress_success_message == false) {
            $jobQueueString = "<TranscodeVideoJob><document_id>$document_id</document_id><source_file><![CDATA[$temp_upload_path]]></source_file></TranscodeVideoJob>";
        } else {
            $jobQueueString = "<TranscodeVideoJob><document_id>$document_id</document_id><source_file><![CDATA[$temp_upload_path]]></source_file><send_success_message>0</send_success_message></TranscodeVideoJob>";
        }

        $this->JobQueue->create();
        $data = array(
            'JobId' => '2',
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $jobQueueString,
            'JobQueueStatusId' => 1,
            'CurrentRetry' => 0
        );


        if ($this->JobQueue->save($data, $validation = TRUE)) {

            return $this->JobQueue->id;
        } else {
            return false;
        }
    }

    function CreateJobQueueEmail($to = "", $cc = "", $bcc = "", $subject = "", $body = "", $isHtml = "n") {
        $jobQueueString = "<emailJob>";

        if ($to == null || $to == "") {
            $jobQueueString = $jobQueueString . "<to>" . $to . "</to>";
        }

        if ($cc == null || $cc == "") {
            $jobQueueString = $jobQueueString . "<cc>" . $cc . "</cc>";
        }

        if ($bcc == null || $bcc == "") {
            $jobQueueString = $jobQueueString . "<bcc>" . $bcc . "</bcc>";
        }

        if ($subject == null || $subject == "") {
            $jobQueueString = $jobQueueString . "<subject>" . $subject . "</subject>";
        }

        if ($body == null || $body == "") {
            $jobQueueString = $jobQueueString . "<body>" . $body . "</body>";
        }

        if ($isHtml == true || $isHtml == false) {
            if ($isHtml == true) {
                $jobQueueString = $jobQueueString . "<html>Y</html>";
            } else {
                $jobQueueString = $jobQueueString . "<html>N</html>";
            }
        } else {
            if ($isHtml == 'y' || $isHtml == 'Y' || $isHtml == 'n' || $isHtml == 'N') {
                $jobQueueString = $jobQueueString . "<html>" . $isHtml . "</html>";
            } else {
                return false;
            }
        }

        $jobQueueString = $jobQueueString . "</emailJob>";

        $this->JobTask->create();

        $emailJob = array(
            'JobTypeId' => '1',
            'RequestXML' => $jobQueueString,
            'JobStatus' => '0',
            'SubmitDate' => date("Y-m-d H:i:s")
        );


        if ($this->JobTask->save($emailJob, $validation = TRUE)) {
            return true;
        } else {
            return false;
        }
    }

    function check_is_account_expired() {
        $user_current_account = $this->Session->read('user_current_account');

        if (isset($user_current_account) && $user_current_account['accounts']['account_id'] != '') {
            $result = $this->Account->find('first', array('conditions' => array('id' => $user_current_account['accounts']['account_id'], 'in_trial' => '1')));
            if ($result) {
                $trial_duration = Configure::read('trial_duration');
                $account_create_date = $result['Account']['created_at'];
                $this->Session->write('account_created_at', $account_create_date);
                $account_end_date = date('Y-m-d H:i:s', strtotime("+{$trial_duration} day", strtotime($account_create_date)));
                $current_date = date('Y-m-d', time());
                $expiry_date = date('Y-m-d', strtotime($account_end_date));
                $current_date = strtotime($current_date);
                $expiry_date = strtotime($expiry_date);
                $view = new View($this, true);

                if ($current_date > $expiry_date) {

                    if ($user_current_account['roles']['role_id'] == 100) {
                        $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_in_order_msg'], ['trial_duration' => $trial_duration]), 'default', array('class' => 'message error'));
                        header('location:' . Configure::read('sibme_base_url') . 'accounts/account_settings_all/' . $user_current_account['users_accounts']['account_id'] . '/4');
                        exit;
                    } elseif ($user_current_account['roles']['role_id'] == 110) {
                        $allAccounts = $this->Session->read('user_accounts');
                        if (isset($allAccounts) && count($allAccounts) > 1) {
                            $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]), 'default', array('class' => 'message error'));
                            header('location:' . Configure::read('sibme_base_url') . 'launchpad');
                            exit;
                        } else {
                            $this->Session->destroy();
                            $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]), 'default', array('class' => 'message error'));
                            header('location:' . Configure::read('sibme_base_url') . 'users/login');
                            exit;
                        }
                    } elseif ($user_current_account['roles']['role_id'] == 120) {
                        $allAccounts = $this->Session->read('user_accounts');
                        if (isset($allAccounts) && count($allAccounts) > 1) {
                            $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]), 'default', array('class' => 'message error'));
                            header('location:' . Configure::read('sibme_base_url') . 'launchpad');
                            exit;
                        } else {
                            $this->Session->destroy();
                            $this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]), 'default', array('class' => 'message error'));
                            header('location:' . Configure::read('sibme_base_url') . 'users/login');
                            exit;
                        }
                    }
                } else {
                    return true;
                }
            }
        }
    }
    
    
    function check_is_account_expired_for_lumen_header($user_current_account = null) {
        if(empty($user_current_account))
        {
            $user_current_account = $this->Session->read('user_current_account');
        }

        if (isset($user_current_account) && $user_current_account['accounts']['account_id'] != '') {
            $result = $this->Account->find('first', array('conditions' => array('id' => $user_current_account['accounts']['account_id'], 'in_trial' => '1')));
            if ($result) {
                $trial_duration = Configure::read('trial_duration');
                $account_create_date = $result['Account']['created_at'];
                $this->Session->write('account_created_at', $account_create_date);
                $account_end_date = date('Y-m-d H:i:s', strtotime("+{$trial_duration} day", strtotime($account_create_date)));
                $current_date = date('Y-m-d', time());
                $expiry_date = date('Y-m-d', strtotime($account_end_date));
                $current_date = strtotime($current_date);
                $expiry_date = strtotime($expiry_date);
                $view = new View($this, true);

                if ($current_date > $expiry_date) {

                    if ($user_current_account['roles']['role_id'] == 100) {
                        
                        $result_array = array(
                            'message' => $view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_in_order_msg'], ['trial_duration' => $trial_duration]),
                            'url' => Configure::read('sibme_base_url') . 'accounts/account_settings_all/' . $user_current_account['users_accounts']['account_id'] . '/4',
                            'status' => false
                        );
                        
                        return $result_array;
                    } elseif ($user_current_account['roles']['role_id'] == 110) {
                        $allAccounts = $this->Session->read('user_accounts');
                        if (isset($allAccounts) && count($allAccounts) > 1) {
                            $result_array = array(
                            'message' => $view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]),
                            'url' => Configure::read('sibme_base_url') . 'launchpad',
                            'status' => false
                        );
                            return $result_array;
                        } else {
                            $this->Session->destroy();
                            
                            $result_array = array(
                               'message' => $view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]),
                               'url' => Configure::read('sibme_base_url') . 'users/login',
                               'status' => false
                           );
                            return $result_array;
                        }
                    } elseif ($user_current_account['roles']['role_id'] == 120) {
                        $allAccounts = $this->Session->read('user_accounts');
                        if (isset($allAccounts) && count($allAccounts) > 1) {
                           $result_array = array(
                               'message' => $view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]),
                               'url' => Configure::read('sibme_base_url') . 'users/login',
                               'status' => false
                           );
                            return $result_array;
                        } else {
                            $this->Session->destroy();
                            $result_array = array(
                               'message' => $view->Custom->parse_translation_params($this->language_based_messages['day_trial_has_expired_contact_account_owner_msg'], ['trial_duration' => $trial_duration]),
                               'url' => Configure::read('sibme_base_url') . 'users/login',
                                'status' => false
                           );
                            return $result_array;
                        }
                    }
                } else {
                         $result_array = array(
                               'message' => '',
                               'url' => '',
                                'status' => true
                           );
                            return $result_array;
                }
            }
            
            else
            {
                 $result_array = array(
                               'message' => '',
                               'url' => '',
                                'status' => true
                           );
                            return $result_array;
                
            }
            
        }
    }


    function check_if_storage_or_users_full() {
        $user_current_account = $this->Session->read('user_current_account');
        $view = new View($this, false);
        $allowed_storage = $view->Custom->get_allowed_storage($user_current_account['accounts']['account_id']);
        $consumed_storage = $view->Custom->get_consumed_storage($user_current_account['accounts']['account_id']);
        if ($consumed_storage > $allowed_storage) {

            if ($user_current_account['roles']['role_id'] == 100) {
                $this->Session->setFlash($this->language_based_messages['storage_limit_of_your_account_in_order_msg'], 'default', array('class' => 'message error'));
                header('location:' . Configure::read('sibme_base_url') . 'accounts/account_settings_all/' . $user_current_account['users_accounts']['account_id'] . '/4');
                exit;
            } elseif ($user_current_account['roles']['role_id'] == 110) {
                $allAccounts = $this->Session->read('user_accounts');
                if (isset($allAccounts) && count($allAccounts) > 1) {
                    $this->Session->setFlash($this->language_based_messages['storage_limit_of_your_account_contact_account_owner_msg'], 'default', array('class' => 'message error'));
                    header('location:' . Configure::read('sibme_base_url') . 'launchpad');
                    exit;
                } else {
                    $this->Session->destroy();
                    $this->Session->setFlash($this->language_based_messages['storage_limit_of_your_account_contact_account_owner_msg'], 'default', array('class' => 'message error'));
                    header('location:' . Configure::read('sibme_base_url') . 'users/login');
                    exit;
                }
            } elseif ($user_current_account['roles']['role_id'] == 120) {
                $allAccounts = $this->Session->read('user_accounts');
                if (isset($allAccounts) && count($allAccounts) > 1) {
                    $this->Session->setFlash($this->language_based_messages['storage_limit_of_your_account_contact_account_owner_msg'], 'default', array('class' => 'message error'));
                    header('location:' . Configure::read('sibme_base_url') . 'launchpad');
                    exit;
                } else {
                    $this->Session->destroy();
                    $this->Session->setFlash($this->language_based_messages['storage_limit_of_your_account_contact_account_owner_msg'], 'default', array('class' => 'message error'));
                    header('location:' . Configure::read('sibme_base_url') . 'users/login');
                    exit;
                }
            }
        } else {
            return true;
        }
    }

    /**
     * copy huddle of an account to another account
     * @param $sourceHuddleId   source huddle id
     * @param $destinationAccountId  destination account id
     * @param $destinationUserId     user who having destination account id
     */
    protected function copyHuddleForSample($sourceHuddleIds, $destinationAccountId, $destinationUserId) {

        $sampleHuddles = explode(',', $sourceHuddleIds);
        $sourceAccountId = 0;
        $sourceUserId = 0;

        $huddle_user = $this->AccountFolderUser->find('first', array('conditions' => array(
                'user_id' => $destinationUserId
        )));
//if (count($huddle_user) === 0) {
        foreach ($sampleHuddles as $sourceHuddleId) {

            $huddle = $this->AccountFolder->get($sourceHuddleId);
            $account = $this->UserAccount->get($destinationAccountId, $destinationUserId);

            if (empty($huddle) || empty($account))
                return;

            $sourceAccountId = $huddle['AccountFolder']['account_id'];
            $sourceUserId = $huddle['AccountFolder']['created_by'];

            if ($sourceAccountId == $destinationAccountId || $sourceUserId == $destinationUserId)
                return;

            $this->AccountFolder->create();
            $account_folder_data = array(
                'account_id' => (int) $destinationAccountId,
                'folder_type' => 1,
                'name' => $huddle['AccountFolder']['name'],
                'desc' => $huddle['AccountFolder']['desc'],
                'created_by' => $destinationUserId,
                'created_date' => date('Y-m-d H:i:s'),
                'active' => 1,
                'is_sample' => 1
            );

            if ($this->AccountFolder->save($account_folder_data, true)) {
                $destinationHuddleId = $this->AccountFolder->id;
                $huddle_type = $this->AccountFolderMetaData->find('first', array('conditions' => array(
                        'account_folder_id' => $sourceHuddleId,
                        'meta_data_name' => 'folder_type',
                        'meta_data_value' => 2
                )));
                if (count($huddle_type) > 0 && $huddle_type['AccountFolderMetaData']['meta_data_name'] == 'folder_type' && $huddle_type['AccountFolderMetaData']['meta_data_value'] == 2) {
                    $account_fold_meta_data = array(
                        'account_folder_id' => $destinationHuddleId,
                        'meta_data_name' => $huddle_type['AccountFolderMetaData']['meta_data_name'],
                        'meta_data_value' => $huddle_type['AccountFolderMetaData']['meta_data_value'],
                        'created_date' => date('Y-m-d H:i:s'),
                        'created_by' => $destinationUserId,
                        'last_edit_date' => date('Y-m-d H:i:s'),
                        'last_edit_by' => $destinationUserId
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_fold_meta_data);
                    $account_fold_tag_meta_data = array(
                        'account_folder_id' => $destinationHuddleId,
                        'meta_data_name' => 'chk_tags',
                        'meta_data_value' => 1,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_fold_tag_meta_data);
                } else {
//$this->AccountFolderMetaData->save($account_fold_meta_data);
                    $account_fold_tag_meta_data = array(
                        'account_folder_id' => $destinationHuddleId,
                        'meta_data_name' => 'chk_tags',
                        'meta_data_value' => 0,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_fold_tag_meta_data);
                }
                $account_folders_meta_data = array(
                    'account_folder_id' => $destinationHuddleId,
                    'meta_data_name' => 'message',
                    'meta_data_value' => '',
                    'created_date' => date('Y-m-d H:i:s'),
                    'created_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);
                if (count($huddle_type) > 0 && $huddle_type['AccountFolderMetaData']['meta_data_name'] == 'folder_type' && $huddle_type['AccountFolderMetaData']['meta_data_value'] == 2) {
                    $superUserData = array(
                        'account_folder_id' => $destinationHuddleId,
                        'user_id' => $destinationUserId,
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId,
                        'is_coach' => 1,
                        'is_mentee' => 0
                    );
                } else {
                    $superUserData = array(
                        'account_folder_id' => $destinationHuddleId,
                        'user_id' => $destinationUserId,
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId
                    );
                }
                $this->AccountFolderUser->create();
                $this->AccountFolderUser->save($superUserData);

                $InvitedUserAccount = $this->UserAccount->find('first', array('conditions' => array(
                        'user_id' => $destinationUserId,
                        'account_id' => $destinationAccountId
                )));

                $role = $InvitedUserAccount['UserAccount']['role_id'];

                $this->copyAccountFolderDocument($sourceHuddleId, $sourceUserId, $destinationAccountId, $destinationHuddleId, $destinationUserId);
                $this->copyAccountFolderDiscussion($sourceHuddleId, $destinationAccountId, $destinationHuddleId, $destinationUserId, $sourceUserId);
                if ($role == 100) {
                    $this->copyUsersToSampleAccount($huddle['AccountFolder']['account_id'], $destinationAccountId);
                    $this->copyGroupForSampleAccount($huddle['AccountFolder']['account_id'], $destinationAccountId, $sourceUserId, $destinationUserId);
                }
                $this->copyUsersToSampleHuddle($sourceHuddleId, $destinationHuddleId);
            }
        }
//}
        if ($sourceAccountId > 0 && $sourceUserId > 0) {
            $InvitedUserAccount = $this->UserAccount->find('first', array('conditions' => array(
                    'user_id' => $destinationUserId,
                    'account_id' => $destinationAccountId
            )));

            $role = $InvitedUserAccount['UserAccount']['role_id'];

            $this->copyMyFilesForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId);
            if ($role == 100) {
                $this->copyVideoLibraryForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId);
            }
        } else {
            $sampleHuddleId = Configure::read('sample_huddle');
            $sampleHuddles = explode(',', $sourceHuddleIds);
            $huddle = $this->AccountFolder->get($sampleHuddles[0]);
            $sourceAccountId = $huddle['AccountFolder']['account_id'];
            $sourceUserId = $huddle['AccountFolder']['created_by'];
            $this->copyMyFilesForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId);
        }
    }

    /**
     *
     * @param $sourceUserId
     * @param $sourceHuddleId
     * @param $destinationAccountId
     * @param $destinationHuddleId
     * @param $destinationUserId
     */
    protected function copyAccountFolderDocument($sourceHuddleId, $sourceUserId, $destinationAccountId, $destinationHuddleId, $destinationUserId) {
// copy document
        $documents = $this->AccountFolderDocument->find('all', array('conditions' => array(
                'account_folder_id' => $sourceHuddleId
        )));
        if (empty($documents))
            return;

        $doc_maps = $folder_doc_maps = array();
        foreach ($documents as $document) {
            $doc = $this->Document->get_document_row($document['AccountFolderDocument']['document_id']);
            if (!$doc)
                continue;

            $det_doc_id = $doc['Document']['id'];
            $det_doc_zencoder_output_id = $doc['Document']['zencoder_output_id'];

            $this->Document->create();
            $doc_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'last_edit_by' => $destinationUserId,
                'post_rubric_per_video' => '1'
                    ) + $doc['Document'];
            unset($doc_data['id']);
            $this->Document->save($doc_data);


            $check_data = $this->DocumentFiles->find("first", array("conditions" => array(
                    "document_id" => $det_doc_id,
                    "default_web" => true
            )));

            if (isset($check_data) && isset($check_data['DocumentFiles'])) {

                $this->DocumentFiles->create();
                $data = array(
                    'document_id' => $this->Document->id,
                    'url' => $check_data['DocumentFiles']['url'],
                    'resolution' => 1080,
                    'duration' => $check_data['DocumentFiles']['duration'],
                    'file_size' => $check_data['DocumentFiles']['file_size'],
                    'default_web' => 1,
                    'transcoding_job_id' => $det_doc_zencoder_output_id,
                    'transcoding_status' => 3,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'debug_logs'=>"Cake::AppController::copyAccountFolderDocument::line=5526::document_id=".$this->Document->id."::status=3::created"
                );
                $this->checkDocumentFilesRecordExist($this->Document->id, $data, $this->request->data);
                $this->DocumentFiles->save($data, $validation = TRUE);
            }


            $doc_maps[$doc['Document']['id']] = $this->Document->id;

            if ($doc['Document']['doc_type'] == 1) {
                $this->copyVideoComment($doc['Document']['id'], $this->Document->id, $destinationUserId, $sourceUserId);
            }

            $doc = $document['AccountFolderDocument'];
            $this->AccountFolderDocument->create();
            $account_folder_document_data = array(
                'account_folder_id' => $destinationHuddleId,
                'document_id' => $this->Document->id
                    ) + $doc;
            unset($account_folder_document_data['id']);
            $this->AccountFolderDocument->save($account_folder_document_data);
            $folder_doc_maps[$doc['id']] = $this->AccountFolderDocument->id;
        }

        $attachments = $this->AccountFolderDocumentAttachment->get_by_huddle_id($sourceHuddleId);
        if (count($attachments) > 0) {
            foreach ($attachments as $att) {
                $this->AccountFolderDocumentAttachment->create();
                $this->AccountFolderDocumentAttachment->save(array(
                    'account_folder_document_id' => $folder_doc_maps[$att['account_folderdocument_attachments']['account_folder_document_id']],
                    'attach_id' => $doc_maps[$att['account_folderdocument_attachments']['attach_id']]
                ));
            }
        }
    }

    /**
     * @param $sourceVideoId
     * @param $destinationVideoId
     * @param $destinationUserId
     * @param $sourceUserId
     * @param int $refType
     */
    protected function copyVideoComment($sourceVideoId, $destinationVideoId, $destinationUserId, $sourceUserId, $refType = 2) {

        $comments = $this->Comment->find('all', array(
            'conditions' => array(
                'ref_type' => $refType,
                'ref_id' => $sourceVideoId,
                'active' => 1
            ),
            'order' => 'created_date'
        ));
        if (count($comments) == 0)
            return;

        foreach ($comments as $comment) {
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get tags

            $acc = $this->UserAccount->find('first', array(
                'conditions' => array(
                    'user_id' => $destinationUserId
                )
            ));

            $this->Comment->create();
            $comment_user_id = $comment['Comment']['user_id'] == $sourceUserId ? $destinationUserId : $comment['Comment']['user_id'];
            $comment_data = array(
                'parent_comment_id' => ($refType == 3) ? $destinationVideoId : null,
                'ref_type' => $refType,
                'ref_id' => $destinationVideoId,
                'user_id' => $comment_user_id,
                'created_by' => $comment_user_id,
                'last_edit_by' => $comment_user_id,
                    ) + $comment['Comment'];
            unset($comment_data['id']);
            if ($this->Comment->save($comment_data)) {
                if (!empty($get_tags)) {
                    foreach ($get_tags as $tag) {
                        $commentTag = $this->AccountTag->find('first', array(
                            'conditions' => array(
                                'account_id' => $acc['UserAccount']['account_id'],
                                'tag_title' => $tag['AccountCommentTag']['tag_title']
                            )
                        ));
                        $this->AccountCommentTag->create();
                        $comment_tag_data = array(
                            'comment_id' => $this->Comment->id,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => 1,
                            'tag_title' => $tag['AccountCommentTag']['tag_title'],
                            'account_tag_id' => $commentTag['AccountTag']['account_tag_id'],
                            'created_by' => $comment_user_id,
                            'last_edit_by' => $comment_user_id,
                        );
                        $this->AccountCommentTag->save($comment_tag_data);
                    }
                }
            }

            $this->copyVideoComment($comment['Comment']['id'], $this->Comment->id, $destinationUserId, $sourceUserId, 3);
        }
    }

    protected function copyAccountFolderDiscussion($sourceHuddleId, $destinationAccountId, $destinationHuddleId, $destinationUserId, $sourceUserId) {
        $comments = $this->Comment->find('all', array('conditions' => array(
                'ref_type' => 1,
                'ref_id' => $sourceHuddleId,
                'active' => 1
        )));
        if (count($comments) == 0)
            return;

        $comment_maps = array();
        foreach ($comments as $comment) {
            $this->Comment->create();
            $comment_user_id = $comment['Comment']['user_id'] == $sourceUserId ? $destinationUserId : $comment['Comment']['user_id'];
            $comment_data = array(
                'parent_comment_id' => isset($comment_maps[$comment['Comment']['parent_comment_id']]) ?
                        $comment_maps[$comment['Comment']['parent_comment_id']] : null,
                'ref_type' => 1,
                'ref_id' => $destinationHuddleId,
                'user_id' => $comment_user_id,
                'active' => 1,
                'created_by' => $comment_user_id,
                'last_edit_by' => $comment_user_id
                    ) + $comment['Comment'];
            unset($comment_data['id']);
            $this->Comment->save($comment_data);
            $comment_maps[$comment['Comment']['id']] = $this->Comment->id;

            $attachs = $this->CommentAttachment->find('all', array(
                'conditions' => array(
                    'CommentAttachment.comment_id' => $comment['Comment']['id']
                ),
                'joins' => array(
                    array(
                        'table' => 'documents as Document',
                        'type' => 'inner',
                        'conditions' => array(
                            'CommentAttachment.document_id = Document.id'
                        )
                    )
                ),
                'fields' => array('CommentAttachment.comment_id', 'Document.*')
            ));
            if (count($attachs) == 0)
                continue;
            foreach ($attachs as $att) {
                $document_data = array(
                    'account_id' => $destinationAccountId,
                    'doc_type' => 2,
                    'created_by' => $comment_user_id,
                    'last_edit_by' => $comment_user_id,
                    'post_rubric_per_video' => '1'
                        ) + $att['Document'];
                unset($document_data['id']);
                $this->Document->create();
                $this->Document->save($document_data);

                $att_data = array(
                    'comment_id' => $comment_maps[$att['CommentAttachment']['comment_id']],
                    'document_id' => $this->Document->id
                );
                $this->CommentAttachment->create();
                $this->CommentAttachment->save($att_data);
            }
        }
    }

    /**
     * @param $sourceAccountId
     * @param $destinationAccountId
     */
    protected function copyUsersToSampleAccount($sourceAccountId, $destinationAccountId) {
        if ($sourceAccountId == $destinationAccountId)
            return;

        $userAccounts = $this->UserAccount->find('all', array('conditions' => array(
                'account_id' => $sourceAccountId
        )));
        if (empty($userAccounts))
            return;
        foreach ($userAccounts as $userAccount) {
//ignore account owner
            if ($userAccount['UserAccount']['role_id'] == 100)
                continue;
            $userId = $userAccount['UserAccount']['user_id'];
            $userInAccount = $this->UserAccount->find('first', array('conditions' => array(
                    'account_id' => $destinationAccountId,
                    'user_id' => $userId
            )));
            if (empty($userInAccount)) {
                $userAccountData = array(
                    'account_id' => $destinationAccountId,
                    'user_id' => $userId,
                        ) + $userAccount['UserAccount'];
                unset($userAccountData['id']);

                $this->UserAccount->create();
                $this->UserAccount->save($userAccountData);
            }
        }
    }

    /**
     * @param $sourceHuddleId
     * @param $destinationHuddleId
     */
    protected function copyUsersToSampleHuddle($sourceHuddleId, $destinationHuddleId) {
        if ($sourceHuddleId == $destinationHuddleId)
            return;

        $accountFolderUsers = $this->AccountFolderUser->find('all', array('conditions' => array(
                'account_folder_id' => $sourceHuddleId
        )));
        if (empty($accountFolderUsers))
            return;

        foreach ($accountFolderUsers as $accountFolderUser) {
// ignore admin
            if ($accountFolderUser['AccountFolderUser']['role_id'] == 200)
                continue;
            $userInHuddle = $this->AccountFolderUser->find('first', array('conditions' => array(
                    'account_folder_id' => $destinationHuddleId,
                    'user_id' => $accountFolderUser['AccountFolderUser']['user_id']
            )));
            if (empty($userInHuddle)) {
                $accountFolderUserData = array(
                    'account_folder_id' => $destinationHuddleId,
                        ) + $accountFolderUser['AccountFolderUser'];

                unset($accountFolderUserData['account_folder_user_id']);
                $this->AccountFolderUser->create();
                $this->AccountFolderUser->save($accountFolderUserData);
            }
        }
    }

    /**
     * @param $accountId
     * @param $userId
     * @param $sampleHuddleId
     */
    protected function restoreSampleHuddle($accountId, $userId, $sampleHuddleId) {
        $huddles = $this->AccountFolder->find('all', array('conditions' => array(
                'account_id' => $accountId,
                'is_sample' => 1
        )));
        if (!empty($huddles)) {
            foreach ($huddles as $huddle) {
                $this->deleteSampleHuddle($huddle['AccountFolder']['account_folder_id']);
            }
        }
        $this->copyHuddleForSample($sampleHuddleId, $accountId, $userId);
    }

    /**
     * @param $huddleId
     */
    protected function deleteSampleHuddle($huddleId) {
// delete video & comments
        $this->deleteSampleHuddleDocument($huddleId);
// delete discussion
        $this->deleteSampleHuddleDiscussion($huddleId);
// delete users
        $this->deleteSampleHuddleUsers($huddleId);
// delete huddle
        $this->AccountFolder->deleteAll(array('account_folder_id' => $huddleId), false);

// delete user activites log
//$this->UserActivityLog->deleteAll(array('account_folder_id' => $huddleId));
    }

    /**
     * @param $huddleId
     */
    protected function deleteSampleHuddleDocument($huddleId) {
        $documents = $this->AccountFolderDocument->find('all', array(
            'conditions' => array(
                'account_folder_id' => $huddleId
            ),
            'joins' => array(
                array(
                    'table' => 'documents as Document',
                    'type' => 'inner',
                    'conditions' => array(
                        'AccountFolderDocument.document_id = Document.id'
                    )
                )
            ),
            'fields' => array('AccountFolderDocument.id', 'Document.id', 'Document.doc_type')
        ));
        if (empty($documents))
            return;

        $docIds = array();
        foreach ($documents as $doc) {
            if ($doc['Document']['doc_type'] == 1) {
                $this->deleteSampleVideoComment($doc['Document']['id']);
            }
            $docIds[] = $doc['Document']['id'];
        }
        $this->Document->deleteAll(array('id' => $docIds));
        $this->AccountFolderDocument->deleteAll(array('account_folder_id' => $huddleId));
    }

    /**
     * @param $videoId
     * @param int $ref_type
     */
    protected function deleteSampleVideoComment($videoId, $ref_type = 2) {
        $commentIds = $this->Comment->find('all', array(
            'conditions' => array(
                'ref_type' => $ref_type,
                'ref_id' => $videoId
            ),
            'fields' => array('id')
        ));
        if (count($commentIds) == 0)
            return;

        foreach ($commentIds as $commentId) {
            $this->deleteSampleVideoComment($commentId['Comment']['id'], 3);
        }
        $this->Comment->deleteAll(array(
            'ref_type' => $ref_type,
            'ref_id' => $videoId
        ));
    }

    /**
     * @param $huddleId
     */
    protected function deleteSampleHuddleDiscussion($huddleId) {
        $comments = $this->Comment->find('all', array(
            'conditions' => array(
                'ref_type' => 1,
                'ref_id' => $huddleId
            ),
            'order' => array('Comment.parent_comment_id desc')
        ));
        if (count($comments) == 0)
            return;
        foreach ($comments as $comment) {
            $attachs = $this->CommentAttachment->find('all', array(
                'conditions' => array(
                    'CommentAttachment.comment_id' => $comment['Comment']['id']
                ),
                'joins' => array(
                    array(
                        'table' => 'documents as Document',
                        'type' => 'inner',
                        'conditions' => array(
                            'CommentAttachment.document_id = Document.id'
                        )
                    )
                ),
                'fields' => array('CommentAttachment.id', 'CommentAttachment.comment_id', 'Document.*')
            ));
            if (empty($attachs))
                continue;
            $comAttIds = $docIds = array();
            foreach ($attachs as $att) {
                $comAttIds[] = $att['CommentAttachment']['id'];
                $docIds[] = $att['Document']['id'];
            }
            $this->Document->deleteAll(array('id' => $docIds));
            $this->CommentAttachment->deleteAll(array('id' => $comAttIds));
        }
        $this->Comment->deleteAll(array(
            'ref_type' => 1,
            'ref_id' => $huddleId
        ));
    }

    /**
     * @param $huddleId
     */
    protected function deleteSampleHuddleUsers($huddleId) {
        $this->AccountFolderUser->deleteAll(array(
            'account_folder_id' => $huddleId
        ));
    }

    protected function copyMyFilesForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId) {
        $InvitedUserAccount = $this->UserAccount->find('first', array('conditions' => array(
                'user_id' => $destinationUserId,
                'account_id' => $destinationAccountId
        )));

//echo "<pre>"; print_r($InvitedUserAccount); die;

        $role = $InvitedUserAccount['UserAccount']['role_id'];
        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id=AccountFolder.account_folder_id',
            ),
            array(
                'table' => 'documents as doc',
                'type' => 'inner',
                'conditions' => 'afd.document_id=doc.id'
            )
        );

        $params = array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_id' => $destinationAccountId,
                'AccountFolder.created_by' => $destinationUserId,
                'AccountFolder.folder_type' => 3,
                'AccountFolder.active' => 1,
                'AccountFolder.is_sample' => 1
            ),
            'order' => 'AccountFolder.created_date DESC',
        );

        $result = $this->AccountFolder->find('all', $params);
        if (!empty($result)) {
            $ids = array();
            foreach ($result as $item) {
                $ids[] = $item['AccountFolder']['account_folder_id'];
            }
            $this->AccountFolder->updateAll(array('active' => 0), array('account_folder_id' => $ids));
        }

        $params = array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_id' => $sourceAccountId,
                'AccountFolder.created_by' => $sourceUserId,
                'AccountFolder.folder_type' => 3,
                'AccountFolder.active' => 1,
                'AccountFolder.is_sample' => 0
            ),
            'order' => 'AccountFolder.created_date DESC',
            'fields' => array('AccountFolder.*', 'afd.*', 'doc.*')
        );

        $result = $this->AccountFolder->find('all', $params);
        if (empty($result))
            return;

        foreach ($result as $item) {
            $account_folder_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'last_edit_by' => $destinationUserId,
                'is_sample' => 1
                    ) + $item['AccountFolder'];
            unset($account_folder_data['account_folder_id']);
            $this->AccountFolder->create();
            $this->AccountFolder->save($account_folder_data);

            $det_doc_id = $item['doc']['id'];
            $det_doc_zencoder_output_id = $item['doc']['zencoder_output_id'];
            if ($role == '120' && ($item['afd']['title'] == 'Sibme Quick Start Guide 2015-16  Super User and Account Owner' || $item['doc']['original_file_name'] == 'Sibme - User Management-HD.mp4' || $item['doc']['original_file_name'] == 'Sibme - Creating Huddles-HD.mp4')) {
                continue;
            } else {
                $document_data = array(
                    'account_id' => $destinationAccountId,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s'),
                    'post_rubric_per_video' => '1'
                        ) + $item['doc'];
                unset($document_data['id']);
                $this->Document->create();
                $this->Document->save($document_data);
            }
            $check_data = $this->DocumentFiles->find("first", array("conditions" => array(
                    "document_id" => $det_doc_id,
                    "default_web" => true
            )));

            if (isset($check_data) && isset($check_data['DocumentFiles'])) {

                $this->DocumentFiles->create();
                $data = array(
                    'document_id' => $this->Document->id,
                    'url' => $check_data['DocumentFiles']['url'],
                    'resolution' => 1080,
                    'duration' => $check_data['DocumentFiles']['duration'],
                    'file_size' => $check_data['DocumentFiles']['file_size'],
                    'default_web' => 1,
                    'transcoding_job_id' => $det_doc_zencoder_output_id,
                    'transcoding_status' => 3,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'debug_logs'=>"Cake::AppController::copyMyFilesForSampleAccount::line=6017::document_id=".$this->Document->id."::status=3::created"
                );
                $this->checkDocumentFilesRecordExist($this->Document->id, $data, $this->request->data);
                $this->DocumentFiles->save($data, $validation = TRUE);
            }

            $account_folder_document_data = array(
                'account_folder_id' => $this->AccountFolder->id,
                'document_id' => $this->Document->id
                    ) + $item['afd'];
            unset($account_folder_document_data['id']);
            $this->AccountFolderDocument->create();
            $this->AccountFolderDocument->save($account_folder_document_data);

            if ($document_data['doc_type'] == 1) {

                $this->copyVideoComment($item['doc']['id'], $this->Document->id, $destinationUserId, $sourceUserId);
            }
        }
    }

    protected function copyVideoLibraryForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId) {
        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id=AccountFolder.account_folder_id',
            ),
            array(
                'table' => 'documents as doc',
                'type' => 'inner',
                'conditions' => 'afd.document_id=doc.id'
            )
        );

        $params = array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_id' => $destinationAccountId,
                'AccountFolder.created_by' => $destinationUserId,
                'AccountFolder.folder_type' => 2,
                'AccountFolder.active' => 1,
                'AccountFolder.is_sample' => 1
            ),
            'order' => 'AccountFolder.created_date DESC',
        );

        $result = $this->AccountFolder->find('all', $params);

        if (!empty($result)) {
            $ids = array();
            foreach ($result as $item) {
                $ids[] = $item['AccountFolder']['account_folder_id'];
            }
            $this->AccountFolder->updateAll(array('active' => 0), array('account_folder_id' => $ids));
        }

        $params = array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolder.account_id' => $sourceAccountId,
                'AccountFolder.created_by' => $sourceUserId,
                'AccountFolder.folder_type' => 2,
                'AccountFolder.active' => 1,
                'AccountFolder.is_sample' => 0,
                'doc.doc_type' => 1
            ),
            'order' => 'AccountFolder.created_date DESC',
            'fields' => array('DISTINCT AccountFolder.*', 'afd.*', 'doc.*')
        );

        $result = $this->AccountFolder->find('all', $params);
        if (empty($result))
            return;


        foreach ($result as $item) {
            $account_folder_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'last_edit_by' => $destinationUserId,
                'is_sample' => 1
                    ) + $item['AccountFolder'];
            unset($account_folder_data['account_folder_id']);
            $this->AccountFolder->create();
            $this->AccountFolder->save($account_folder_data);

            $det_doc_id = $item['doc']['id'];
            $det_doc_zencoder_output_id = $item['doc']['zencoder_output_id'];

            $document_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'last_edit_by' => $destinationUserId,
                'post_rubric_per_video' => '1'
                    ) + $item['doc'];
            unset($document_data['id']);
            $this->Document->create();
            $this->Document->save($document_data);

            $check_data = $this->DocumentFiles->find("first", array("conditions" => array(
                    "document_id" => $det_doc_id,
                    "default_web" => true
            )));

            if (isset($check_data) && isset($check_data['DocumentFiles'])) {

                $this->DocumentFiles->create();
                $data = array(
                    'document_id' => $this->Document->id,
                    'url' => $check_data['DocumentFiles']['url'],
                    'resolution' => 1080,
                    'duration' => $check_data['DocumentFiles']['duration'],
                    'file_size' => $check_data['DocumentFiles']['file_size'],
                    'default_web' => 1,
                    'transcoding_job_id' => $det_doc_zencoder_output_id,
                    'transcoding_status' => -1,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'debug_logs'=>"Cake::AppController::copyVideoLibraryForSampleAccount::line=6135::document_id=".$this->Document->id."::status=-1::created"
                );
                $this->checkDocumentFilesRecordExist($this->Document->id, $data, $this->request->data);
                $this->DocumentFiles->save($data, $validation = TRUE);
            }

            $account_folder_document_data = array(
                'account_folder_id' => $this->AccountFolder->id,
                'document_id' => $this->Document->id
                    ) + $item['afd'];
            unset($account_folder_document_data['id']);
            $this->AccountFolderDocument->create();
            $this->AccountFolderDocument->save($account_folder_document_data);

// copy subjects
            $subjects = $this->AccountFolderSubject->find('all', array(
                'conditions' => array(
                    'account_folder_id' => $item['AccountFolder']['account_folder_id']
                )
            ));
            foreach ($subjects as $subject) {
                $sub = $this->Subject->find('first', array(
                    'conditions' => array(
                        'id' => $subject['AccountFolderSubject']['subject_id']
                    )
                ));

                $sub_data = array(
                    'name' => $sub['Subject']['name'],
                    'account_id' => $destinationAccountId,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s')
                );
                $this->Subject->create();
                $this->Subject->save($sub_data);
                $subject_data = array(
                    'account_folder_id' => $this->AccountFolder->id,
                    'subject_id' => $this->Subject->id,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s')
                );
                $this->AccountFolderSubject->create();
                $this->AccountFolderSubject->save($subject_data);
            }
// copy topics
            $topics = $this->AccountFolderTopic->find('all', array(
                'conditions' => array(
                    'account_folder_id' => $item['AccountFolder']['account_folder_id']
                )
            ));
            foreach ($topics as $topic) {
                $top = $this->Topic->find('first', array(
                    'conditions' => array(
                        'id' => $topic['AccountFolderTopic']['topic_id']
                    )
                ));

                $top_data = array(
                    'name' => $top['Topic']['name'],
                    'account_id' => $destinationAccountId,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s')
                );
                $this->Topic->create();
                $this->Topic->save($top_data);
                $topic_data = array(
                    'account_folder_id' => $this->AccountFolder->id,
                    'topic_id' => $this->Topic->id,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s')
                );
                $this->AccountFolderTopic->create();
                $this->AccountFolderTopic->save($topic_data);
            }
// copy video tags
            $tags = $this->AccountFolder->getVideoTags($item['AccountFolder']['account_folder_id'], $sourceAccountId);
            if (!empty($tags)) {
                foreach ($tags as $tag) {
                    $tag_data = array(
                        'account_folder_id' => $this->AccountFolder->id,
                        'meta_data_name' => $tag['account_folders_meta_data']['meta_data_name'],
                        'meta_data_value' => $tag['account_folders_meta_data']['meta_data_value'],
                        'created_by' => $destinationUserId,
                        'created_date' => date('Y-m-d H:i:s'),
                        'last_edit_by' => $destinationUserId,
                        'last_edit_date' => date('Y-m-d H:i:s')
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($tag_data);
                }
            }
// copy documents
            $this->copyDocumentForSampleVideoLibrary(
                    $item['AccountFolder']['account_folder_id'], $this->AccountFolder->id, $destinationAccountId, $destinationUserId
            );
        }
    }

    protected function copyDocumentForSampleVideoLibrary($account_folder_id, $new_account_folder_id, $destinationAccountId, $destinationUserId) {
        $documents = $this->AccountFolderDocument->find('all', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'doc_type' => 2,
                'Document.published' => 1
            ),
            'joins' => array(
                array(
                    'table' => 'documents as Document',
                    'type' => 'inner',
                    'conditions' => 'AccountFolderDocument.document_id = Document.id'
                )
            ),
            'fields' => array('AccountFolderDocument.*', 'Document.*')
        ));
        if (empty($documents))
            return;
        foreach ($documents as $document) {
            $doc_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'created_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $destinationUserId,
                'last_edut_date' => date('Y-m-d H:i:s'),
                'post_rubric_per_video' => '1'
                    ) + $document['Document'];
            unset($doc_data['id']);
            $this->Document->create();
            $this->Document->save($doc_data);

            $account_folder_data = array(
                'account_folder_id' => $new_account_folder_id,
                'document_id' => $this->Document->id
                    ) + $document['AccountFolderDocument'];
            unset($account_folder_data['id']);
            $this->AccountFolderDocument->create();
            $this->AccountFolderDocument->save($account_folder_data);
        }
    }

    protected function copyGroupForSampleAccount($sourceAccountId, $destinationAccountId, $sourceUserId, $destinationUserId) {
// ignore if source and destination account is one
        if ($sourceAccountId == $destinationAccountId)
            return;
// delete old sample group
        $oldGroups = $this->Group->find('all', array('conditions' => array('account_id' => $destinationAccountId, 'is_sample' => 1)));
        if (!empty($oldGroups)) {
            $oldGroupIds = array();
            foreach ($oldGroups as $group) {
                $oldGroupIds[] = $group['Group']['id'];
            }
            $this->UserGroup->deleteAll(array('group_id' => $oldGroupIds));
            $this->Group->deleteAll(array('account_id' => $destinationAccountId, 'is_sample' => 1));
        }
// copy sample group to destination account
        $groups = $this->Group->find('all', array('conditions' => array('account_id' => $sourceAccountId)));
        if (empty($groups))
            return;

        foreach ($groups as $group) {
            $group_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'created_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $destinationUserId,
                'last_edit_date' => date('Y-m-d H:i:s'),
                'name' => $group['Group']['name'],
                'is_sample' => 1
            );
            $this->Group->create();
            $this->Group->save($group_data);

            $user_groups = $this->UserGroup->find('all', array('conditions' => array('group_id' => $group['Group']['id'])));
            if (empty($user_groups))
                continue;

            $group_id = $this->Group->id;
            foreach ($user_groups as $user_group) {
                if ($user_group['UserGroup']['user_id'] == $sourceUserId) {
                    $group_user_id = $destinationUserId;
                } else {
                    $group_user_id = $user_group['UserGroup']['user_id'];
                }
                $user_group_data = array(
                    'group_id' => $group_id,
                    'user_id' => $group_user_id,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s')
                );
                $this->UserGroup->create();
                $this->UserGroup->save($user_group_data);
            }
        }
    }

    function logFilePickerError() {

        $this->layout = null;

        $request_dump = addslashes(print_r($_POST, true));
        $this->AuditErrors->create();
        $data = array(
            'form_fields' => $request_dump,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $_POST['current_user_id'],
            'error_detail' => $request_dump,
            'path' => $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
        );

        $this->AuditErrors->save($data, $validation = TRUE);

        $this->set('ajaxdata', "1");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    /**
     *  cron job to remove temporary files
     *
     */
    public function removeTempFolder() {
        $dir = realpath(dirname(__FILE__) . '/../..') . "/app/webroot/files/tempupload";
        $it = new RecursiveDirectoryIterator($dir);
        $it = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($it as $file) {
            if ('.' === $file->getBasename() || '..' === $file->getBasename())
                continue;
            if ($file->isDir())
                rmdir($file->getPathname());
            else
                unlink($file->getPathname());
        }
        die;
    }

    function getSecureAmazonCloudFrontUrl($resource, $name = '', $timeout = 600) {


//This comes from key pair you generated for cloudfront
        $keyPairId = Configure::read('cloudfront_keypair');

//Read Cloudfront Private Key Pair
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => realpath(dirname(__FILE__) . '/..') . "/Config/cloudfront_key.pem",
                    'key_pair_id' => $keyPairId,
        ));

        $streamHostUrl = Configure::read('cloudfront_host_url');
        $resourceKey = $resource;
        $expires = time() + 21600;

//Construct the URL
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));

        return $signedUrlCannedPolicy;
    }

    public function regenerate_thumbnail($video_id, $huddle_id) {

        $this->layout = null;
        $bucket_name = Configure::read('bucket_name');

        $video_document = $this->Document->find('first', array('conditions' => array(
                'id' => $video_id
            )
                )
        );

        $videoFilePath = pathinfo($video_document['Document']['url']);
        $videoFileName = $videoFilePath['filename'];

        $thumbnail_image_path = "uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName .
                (empty($video_document['Document']['thumbnail_number']) ?
                        '_thumb_00001.png' :
                        '_thumb_' . sprintf('%05d', $video_document['Document']['thumbnail_number']) . '.png'
                );

        $ThumbnailFilePath = pathinfo($thumbnail_image_path);

        $video_url = $this->getSecureAmazonCloudFrontHttpUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4");

        $dir = realpath(dirname(__FILE__) . '/../..') . "/app/webroot/files/tempupload/";

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];


        $use_ffmpeg_path = Configure::read('use_ffmpeg_path');
        $ffmpeg_binaries = Configure::read('ffmpeg_binaries');
        $ffprobe_binaries = Configure::read('ffprobe_binaries');

        if ($use_ffmpeg_path == 0) {

            $ffmpeg = FFMpeg::create();
        } else {

            $ffmpeg = FFMpeg::create(array(
                        'ffmpeg.binaries' => $ffmpeg_binaries,
                        'ffprobe.binaries' => $ffprobe_binaries,
            ));
        }

        $video = $ffmpeg->open($video_url);
        $video
                ->filters()
                ->resize(new Dimension(320, 240))
                ->synchronize();


        $video
                ->frame(TimeCode::fromSeconds(2))
                ->save($dir . $ThumbnailFilePath['filename'] . ".jpg");


        $width = 320;
        $height = 180;

        $images_orig = ImageCreateFromJPEG($dir . $ThumbnailFilePath['filename'] . ".jpg");
        $photoX = ImagesX($images_orig);
        $photoY = ImagesY($images_orig);
        $images_fin = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
        ImageJPEG($images_fin, $dir . $ThumbnailFilePath['filename'] . ".png");
        ImageDestroy($images_orig);
        ImageDestroy($images_fin);

        $keyname = $thumbnail_image_path;
// $filepath should be absolute path to a file on disk
        $filepath = $dir . $ThumbnailFilePath['filename'] . ".png";

// Instantiate the client.
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $distribution_id = Configure::read('distribution_id');

        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

// Upload a file.
        $result = $client->putObject(array(
            'Bucket' => $bucket_name,
            'Key' => $keyname,
            'SourceFile' => $filepath,
            'ACL' => 'public-read'
        ));

//This comes from key pair you generated for cloudfront
        $keyPairId = Configure::read('cloudfront_keypair');

//Read Cloudfront Private Key Pair
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => realpath(dirname(__FILE__) . '/..') . "/Config/cloudfront_key.pem",
                    'key_pair_id' => $keyPairId,
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

        $result = $cloudFront->createInvalidation(array(
// DistributionId is required
            'DistributionId' => $distribution_id,
            'Paths' => array(
// Quantity is required
                'Quantity' => 1,
                'Items' => array("/" . $keyname)
            ),
            // CallerReference is required
            'CallerReference' => $bucket_name . strtotime('now'),
        ));

        $this->set('ajaxdata', "true");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    public function generate_stats($account_id, $doc_type) {

        $this->layout = null;

        $list = $this->Document->find('all', array(
            'conditions' => array(
                'account_id' => $account_id,
                'published' => 1,
                'doc_type' => $doc_type
            ),
            'fields' => array('id', 'url', 'original_file_name', 'created_by')
        ));

        require('src/services.php');
        $s3_service = new S3Services(
                Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
        );


        $html = "";

        $html .= '<table class="analysisData">
                    <thead>
                        <tr style="background-color:#D6D3D6;">
                            <td>Document ID</td>
                            <td>Url</td>
                            <td>File Name</td>
                            <td>File Size</td>
                            <td>Created By</td>
                            <td>Number Of Comments</td>
                        </tr>
                    </thead><tbody>';

        for ($i = 0; $i < count($list); $i++) {

            $item = $list[$i];

            $videoFilePath = pathinfo($item['Document']['url']);
            $videoFileName = $videoFilePath['filename'];

            $User_Created = $this->User->find('first', array(
                'conditions' => array(
                    'id' => $item['Document']['created_by']
                ),
                'fields' => array('username')
            ));

            $User_Comments_Count = 0;

            /* $AccountFolderDocument = $this->AccountFolderDocument->find('first', array(
              'conditions' => array(
              'document_id' => $item['Document']['id']
              ),
              'fields' => array('account_folder_id')
              )); */


            $User_Comments = $this->Comment->find('all', array('conditions' => array(
                    'ref_id' => $item['Document']['id']
            )));

            $User_Comments_Count = count($User_Comments);

            $item_size = $s3_service->get_object_info_s3(Configure::read('bucket_name'), "uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4");
            $item_size_mb = $item_size['size'];

            if ($item_size['size'] > 0) {

                $item_size_mb = round(($item_size['size'] / (1024)) / 1024, 2);
            }

            $html .= '
                <tr>
                    <td>' . $item['Document']['id'] . '</td>
                    <td>' . $item['Document']['url'] . '</td>
                    <td>' . $item['Document']['original_file_name'] . '</td>
                    <td>' . $item_size_mb . '</td>
                    <td>' . $User_Created['User']['username'] . '</td>
                    <td>' . $User_Comments_Count . '</td>
                </tr>
            ';
        }

        header("Content-disposition: attachment; filename=download.xls");
        header("Content-type: application/octet-stream");
        $html .= '</tbody></table>';
        echo $html;

        die();

//var_dump($list[count($list)-1]);

        $this->set('ajaxdata', "true");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function getSecureAmazonCloudFrontHttpUrl($resource, $name = '', $timeout = 600) {


//This comes from key pair you generated for cloudfront
        $keyPairId = Configure::read('cloudfront_keypair');

//Read Cloudfront Private Key Pair
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => realpath(dirname(__FILE__) . '/..') . "/Config/cloudfront_key.pem",
                    'key_pair_id' => $keyPairId,
        ));

        $streamHostUrl = Configure::read('cloudfront_host_http_url');
        $resourceKey = $resource;
        $expires = time() + 21600;

//Construct the URL
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));

        return $signedUrlCannedPolicy;
    }

    function getVideoDuration($document_id) {

        $this->layout = null;

        try {

            $videoDetail = $this->Document->find('first', array('conditions' => array('id' => $document_id)));

            if (isset($videoDetail) && isset($videoDetail['Document'])) {

                $videoID = $videoDetail['Document']['id'];
                $videoFilePath = pathinfo($videoDetail['Document']['url']);
                $videoFileName = $videoFilePath['filename'];

                $use_ffmpeg_path = Configure::read('use_ffmpeg_path');
                $ffmpeg_binaries = Configure::read('ffmpeg_binaries');
                $ffprobe_binaries = Configure::read('ffprobe_binaries');

                if ($use_ffmpeg_path == 0) {

                    $ffmpeg = FFMpeg::create();
                } else {

                    $ffmpeg = FFMpeg::create(array(
                                'ffmpeg.binaries' => $ffmpeg_binaries,
                                'ffprobe.binaries' => $ffprobe_binaries,
                    ));
                }
//echo "<pre>"; print_r($ffmpeg);
//die;
                $lvideo_path = '';

//$video_path = $this->getSecureAmazonCloudFrontHttpUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4");
                $duration = 0;
                $document_files_array = $this->get_document_url($videoDetail['Document']);

                $lvideo_path = $this->getSecureAmazonCloudFrontHttpUrl(urlencode($videoDetail['Document']['url']));


                $ffmpeg_streams = $ffmpeg->getFFProbe()->streams($lvideo_path);
                if (isset($ffmpeg_streams)) {
                    $ffmpeg_streams_videos = $ffmpeg_streams->videos();
                    $ffmpeg_streams_audios = $ffmpeg_streams->audios();
                    if (isset($ffmpeg_streams_videos)) {
                        $ffmpeg_streams_videos_first = $ffmpeg_streams_videos->first();

                        if (isset($ffmpeg_streams_videos_first)) {
                            $duration = $ffmpeg_streams_videos->first()->get('duration');
                        } else {
                            $duration = $ffmpeg_streams_audios->first()->get('duration');
                        }
                    }
                }

                $this->set('ajaxdata', $duration);
            } else {

                $this->set('ajaxdata', -1);
            }
        } catch (Exception $ex) {

            $this->set('ajaxdata', -1);
        }

        $this->render('/Elements/ajax/ajaxreturn');
    }

    function upload_company_logo($file, $account_id) {

        if (isset($file['error']) && $file['error']['image_logo'] == 0) {
            $maxDim = 250;
            list($width, $height, $type, $attr) = getimagesize($file['tmp_name']['image_logo']);
            if ($width > $maxDim || $height > $maxDim) {
                $target_filename = $file['tmp_name']['image_logo'];
                $fn = $file['tmp_name']['image_logo'];
                $size = getimagesize($fn);
                $ratio = $size[0] / $size[1]; // width/height
                if ($ratio > 1) {
                    $width = $maxDim;
                    $height = $maxDim / $ratio;
                } else {
                    $width = $maxDim * $ratio;
                    $height = $maxDim;
                }
                $src = imagecreatefromstring(file_get_contents($fn));
                $dst = imagecreatetruecolor($width, $height);
                imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
                imagedestroy($src);
                imagepng($dst, $target_filename); // adjust format as needed
                imagedestroy($dst);
            }
            $fileName = $file['name']['image_logo'];
            $fileTempName = $file['tmp_name']['image_logo'];
            $fileType = $file['type']['image_logo'];

            $bucket_name = Configure::read('bucket_name_cdn');

            $image_path = "static/companies/" . $account_id . "/" . $fileName;
            $keyname = $image_path;

// Instantiate the client.
            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $distribution_id = Configure::read('distribution_id');

            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

// Upload a file.
            $result = $client->putObject(array(
                'Bucket' => $bucket_name,
                'Key' => $keyname,
                'SourceFile' => $fileTempName,
                'ContentType' => $fileType,
                'ACL' => 'public-read'
            ));

//This comes from key pair you generated for cloudfront
            $keyPairId = Configure::read('cloudfront_keypair');

//Read Cloudfront Private Key Pair
            $cloudFront = CloudFrontClient::factory(array(
                        'private_key' => realpath(dirname(__FILE__) . '/..') . "/Config/cloudfront_key.pem",
                        'key_pair_id' => $keyPairId,
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            /* $result = $cloudFront->createInvalidation(array(
              // DistributionId is required
              'DistributionId' => $distribution_id,
              'Paths' => array(
              // Quantity is required
              'Quantity' => 1,
              'Items' => array("/".$keyname)
              ),
              // CallerReference is required
              'CallerReference' => $bucket_name.strtotime('now'),
              )); */

            return $fileName;
        } else {
            $this->Session->setFlash(__($this->language_based_messages['please_upload_file_msg'], true));
            return FALSE;
        }
    }

    function upload_user_avatar($file, $thumb_name, $user_id) {

        if (isset($file['error']) && $file['error'] == 0) {

            $fileName = $thumb_name;
            $maxDim = 85;
            list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);
            if ($width > $maxDim || $height > $maxDim) {
                $target_filename = $file['tmp_name'];
                $fn = $file['tmp_name'];
                $size = getimagesize($fn);
                $ratio = $size[0] / $size[1]; // width/height
                if ($ratio > 1) {
                    $width = $maxDim;
                    $height = $maxDim / $ratio;
                } else {
                    $width = $maxDim * $ratio;
                    $height = $maxDim;
                }
                $src = imagecreatefromstring(file_get_contents($fn));
                $dst = imagecreatetruecolor($width, $height);
                imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
                imagedestroy($src);
                imagepng($dst, $target_filename); // adjust format as needed
                imagedestroy($dst);
            }
            $fileTempName = $file['tmp_name'];
            $fileType = $file['type'];

            $bucket_name = Configure::read('bucket_name_cdn');

            $image_path = "static/users/" . $user_id . "/" . $thumb_name;
            $keyname = $image_path;

// Instantiate the client.
            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $distribution_id = Configure::read('distribution_id');

            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

// Upload a file.
            $result = $client->putObject(array(
                'Bucket' => $bucket_name,
                'Key' => $keyname,
                'SourceFile' => $fileTempName,
                'ContentType' => $fileType,
                'ACL' => 'public-read'
            ));

//This comes from key pair you generated for cloudfront
            $keyPairId = Configure::read('cloudfront_keypair');

//Read Cloudfront Private Key Pair
            $cloudFront = CloudFrontClient::factory(array(
                        'private_key' => realpath(dirname(__FILE__) . '/..') . "/Config/cloudfront_key.pem",
                        'key_pair_id' => $keyPairId,
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            $result = $cloudFront->createInvalidation(array(
// DistributionId is required
                'DistributionId' => $distribution_id,
                'Paths' => array(
// Quantity is required
                    'Quantity' => 1,
                    'Items' => array("/" . $keyname)
                ),
                // CallerReference is required
                'CallerReference' => time(),
            ));

            return TRUE;
        } else {
            $this->Session->setFlash(__($this->language_based_messages['please_upload_file_msg'], true));
            return FALSE;
        }
    }

    function upload_audio($file, $thumb_name, $user_id) {

        if (isset($file['error']) && $file['error'] == 0) {

            $fileTempName = $file['tmp_name'];
            $fileType = $file['type'];

            $bucket_name = Configure::read('bucket_name');

            $image_path = $thumb_name;
            $keyname = $image_path;

// Instantiate the client.
            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $distribution_id = Configure::read('distribution_id');

            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

// Upload a file.
            $result = $client->putObject(array(
                'Bucket' => $bucket_name,
                'Key' => $keyname,
                'SourceFile' => $fileTempName,
                'ContentType' => $fileType,
                'ACL' => 'public-read'
            ));

//This comes from key pair you generated for cloudfront
            $keyPairId = Configure::read('cloudfront_keypair');

//Read Cloudfront Private Key Pair
            $cloudFront = CloudFrontClient::factory(array(
                        'private_key' => realpath(dirname(__FILE__) . '/..') . "/Config/cloudfront_key.pem",
                        'key_pair_id' => $keyPairId,
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            $result = $cloudFront->createInvalidation(array(
// DistributionId is required
                'DistributionId' => $distribution_id,
                'Paths' => array(
// Quantity is required
                    'Quantity' => 1,
                    'Items' => array("/" . $keyname)
                ),
                // CallerReference is required
                'CallerReference' => $bucket_name . strtotime('now'),
            ));

            return TRUE;
        } else {
            $this->Session->setFlash(__($this->language_based_messages['please_upload_file_msg'], true));
            return FALSE;
        }
    }

    function add_job_queue($jobid, $to, $subject, $html, $is_html = false, $reply_to = '') {
        if ($is_html)
            $ihtml = 'Y';
        else
            $ihtml = 'N';

        if ($reply_to != '') {
            $reply_to = "<replyto>$reply_to</replyto>";
        }
        $html = '<![CDATA[' . $html . ']]>';
        $noreply = $this->custom->get_site_settings('static_emails')['noreply'];
        $fromname = $this->custom->get_site_settings('site_title');
        $xml = "<mail><to>$to</to>$reply_to <from>$noreply</from><fromname>$fromname</fromname><bcc /><cc /><subject><![CDATA[$subject]]></subject><body>$html</body><html>$ihtml</html></mail>";
        $this->JobQueue->create();
        $data = array(
            'JobId' => $jobid,
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $xml,
            'JobQueueStatusId' => 1,
            'CurrentRetry' => 0
        );

        $this->JobQueue->save($data);
        return TRUE;
    }

    function get_document_url($document_video) {

        App::import("Model", "DocumentFiles");
        $db = new DocumentFiles();
        $videoFilePath = pathinfo($document_video['url']);
        $videoFileExtension = $videoFilePath['extension'];
        if ($document_video['published'] == 0) {

            return array(
                'url' => $document_video['url'],
                'thumbnail' => '',
                'encoder_status' => $document_video['encoder_status']
            );
        }

        $check_data = $db->find("first", array("conditions" => array(
                "document_id" => $document_video['id'],
                "default_web" => true
        )));

        if (isset($check_data) && isset($check_data['DocumentFiles']['url'])) {

            $url = $check_data['DocumentFiles']['url'];
            $duration = $check_data['DocumentFiles']['duration'];
            $thumbnail = '';
            $videoFilePath = pathinfo($url);
            $videoFileName = $videoFilePath['filename'];
//these are old files before JobQueue
            if ($check_data['DocumentFiles']['transcoding_status'] == '-1') {

                $videoFilePathThumb = pathinfo($document_video['url']);
                $videoFileNameThumb = $videoFilePathThumb['filename'];
            } else {
                $videoFileNameThumb = $videoFileName;
            }


            $poster_end_extension = "_thumb.png";
            if ($document_video['encoder_provider'] == '2') {
                $poster_end_extension = empty($document_video['thumbnail_number']) ?
                        "_thumb_00001.png" :
                        '_thumb_' . sprintf('%05d', $document_video['thumbnail_number']) . '.png';
            }

            $thumbnail_image_path = '';
            if (Configure::read('use_cloudfront') == true) {
                if ($check_data['DocumentFiles']['transcoding_status'] == '2') {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail.png');
                } else {
                    if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                        $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/audio-thumbnail.png');
                    } else {
                        if ($videoFilePath['dirname'] == ".")
                            $thumbnail_image_path = $this->getSecureAmazonUrl(($videoFileNameThumb) . "$poster_end_extension");
                        else
                            $thumbnail_image_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                    }
                }
            } else {
                if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/audio-thumbnail.png');
                } else {
                    if ($videoFilePath['dirname'] == ".")
                        $thumbnail_image_path = $this->getSecureAmazonUrl(($videoFileNameThumb) . "$poster_end_extension");
                    else
                        $thumbnail_image_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                }
            }

            if ($thumbnail_image_path == '')
                $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail.png');

            $video_path = '';
            $relative_video_path = '';

            if (Configure::read('use_cloudfront') == true) {

                if ($videoFilePath['dirname'] == ".") {
                   if($videoFilePath['extension'] !=''){
                    $video_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . ".".$videoFilePath['extension']);
                    $relative_video_path = $videoFileName . ".".$videoFilePath['extension'];
                   }else{
                    $video_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . ".mp4");
                    $relative_video_path = $videoFileName . ".mp4";
                   }
                    
                } else {
                    if($videoFilePath['extension'] !=''){
                        $video_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".".$videoFilePath['extension']);
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".".$videoFilePath['extension'];
                    }else{
                        $video_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".mp4");
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                    }
                    
                }
            } else {

                if ($videoFilePath['dirname'] == ".") {
                    if($videoFilePath['extension'] !=''){
                        $video_path = $this->getSecureAmazonUrl(($videoFileName) . ".".$videoFilePath['extension']);
                        $relative_video_path = $videoFileName . ".".$videoFilePath['extension'];
                    }else{
                        $video_path = $this->getSecureAmazonUrl(($videoFileName) . ".mp4");
                        $relative_video_path = $videoFileName . ".mp4";
                    }
                   
                } else {
                    if($videoFilePath['extension'] !=''){
                        $video_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".".$videoFilePath['extension']);
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".".$videoFilePath['extension'];
                    }else{
                        $video_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp4");
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                    }
                   
                }
            }

            return array(
                'url' => $video_path,
                'relative_url' => $relative_video_path,
                'thumbnail' => $thumbnail_image_path,
                'duration' => $duration,
                'encoder_status' => (isset($document_video['encoder_status']) ? $document_video['encoder_status'] : '')
            );
        } else {
//todo
//through the exception here
            $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail.png');
            return array(
                'url' => '',
                'relative_url' => $thumbnail_image_path,
                'thumbnail' => '',
                'duration' => '',
                'encoder_status' => 'Error'
            );
        }
    }
    
    public function impersonate_user($user_id) {
        $view = new View($this, false);
        $current_user = $this->Session->read('user_current_account');
        $user_ids_for_impersonate = $view->Custom->get_impersonation_ids();
        $user_id_imp = explode(',', $user_ids_for_impersonate);
        if (in_array($current_user['User']['id'], $user_id_imp)) {
            $user = $this->User->findById($user_id);
            $user = $user['User']; 
            if($user['is_active']==0){
                 return $this->redirect('/NoPermissions/inactive_users');

            }    
            $this->Session->delete('last_logged_in_user');
            $this->Session->delete('user_top_photo');
            $last_loggedin_user = $this->Session->read('user_current_account');
            $lst_usr = $last_loggedin_user['User'];
            $this->Session->write('last_logged_in_user', $lst_usr);
            $lstuser = $this->Session->read('last_logged_in_user');    
            if ($this->Auth->login($user)) {
                $this->Cookie->delete('remember_me_cookie');
                $this->setupPostLogin(true);
                $redirect_url = $this->Auth->redirect();
                return $this->redirect($redirect_url);
            }
        } else {
        
            $redirect_url = $this->Auth->redirect();
            return $this->redirect($redirect_url);
        }
    }

    public function impersonate_back($user_id) {
        $this->Session->delete('last_logged_in_user');
        $user = $this->User->findById($user_id);
        $user = $user['User'];

        if ($this->Auth->login($user)) {
            $this->Cookie->delete('remember_me_cookie');
            $this->setupPostLogin(true);
            $redirect_url = $this->Auth->redirect();

            return $this->redirect($redirect_url);
        }
    }

    function getSecureSibmeResouceUrl($url) {
        if (Configure::read('use_local_file_store') == false) {
//This comes from key pair you generated for cloudfront
            $keyPairId = Configure::read('cloudfront_keypair');

//Read Cloudfront Private Key Pair
            $cloudFront = CloudFrontClient::factory(array(
                        'private_key' => realpath(dirname(__FILE__) . '/../..') . "/Config/cloudfront_key.pem",
                        'key_pair_id' => $keyPairId,
            ));

            $streamHostUrl = Configure::read('amazon_assets_url');
            $resourceKey = $url;
            $expires = time() + 21600;

//Construct the URL
            $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
                'url' => $streamHostUrl . 'app' . $resourceKey,
                'expires' => $expires,
            ));

            return $signedUrlCannedPolicy;
        } else {
            return $url;
        }
    }

    function externaluploadVideos($account_id, $user_id, $uploader_email, $uploader_firstname, $uploader_lastname) {

//$observer_users = "621,622,1163";
        $conditions = array(
            "account_id" => $account_id,
            "meta_data_name" => "web_uploader_users"
        );
        $observer_users = $this->AccountMetaData->find("first", array(
            "conditions" => $conditions,
            "fields" => array("meta_data_value")
        ));
        if (!empty($observer_users)) {
            $observer_users = $observer_users['AccountMetaData']['meta_data_value'];
        } else {
            $observer_users = $user_id;
        }

        $huddle_name = date("Y F") . " Application Videos";

        $account_users = $this->UserAccount->find("list", array(
            "conditions" => array("account_id" => $account_id),
            "fields" => array("user_id")
        ));
        $joins = array(
            array(
                'table' => 'account_folders_meta_data as afmd',
                'type' => 'left',
                'conditions' => 'AccountFolder.account_folder_id = afmd.account_folder_id'
            )
        );
        $check_huddle = $this->AccountFolder->find("first", array(
            "conditions" => array("name" => $huddle_name, "folder_type" => "1", "account_id" => $account_id, "afmd.meta_data_name" => 'autocreated', "afmd.meta_data_value" => '1'),
            "joins" => $joins
        ));

        /* $user_id = $this->UserAccount->find("first", array(
          "conditions" => array("account_id" => $account_id, "role_id" => "100"),
          "fields" => array("user_id")
          )); */

        if (strpos($observer_users, $user_id) === false) {
            $observer_users = $observer_users . ',' . $user_id;
        }

        if (empty($check_huddle)) {
            $account_folder_id = $this->save_huddle($account_id, $huddle_name, $user_id, $observer_users, $account_users);
        } else {

            $account_folder_id = $check_huddle['AccountFolder']['account_folder_id'];
            $check_auto_created = $this->AccountFolderMetaData->find("first", array(
                "conditions" => array(
                    "account_folder_id" => $account_folder_id,
                    "meta_data_name" => 'autocreated',
                    "meta_data_value" => '1'
                )
            ));

            if (!$check_auto_created) {
                $account_folder_id = $this->save_huddle($account_id, $huddle_name, $user_id, $observer_users, $account_users);
            }
        }
        if (!empty($account_folder_id)) {
            $document_id = $this->externalsavedocument($account_folder_id, $account_id, $user_id, true);
            $this->save_uploader_email($document_id, $user_id, $uploader_email, $uploader_firstname, $uploader_lastname);
//$this->update_user_subscrition($user_id,$account_folder_id);
            $return = array("code" => "200", "msg" => "File Uploaded.");
        } else {
            $return = array("code" => "400", "msg" => "File do not uploaded.Please Try again");
        }
        echo json_encode($return);
        exit;
    }

    function save_huddle($account_id, $huddle_name, $user_id, $observer_users, $account_users) {
        $data = array(
            'account_id' => $account_id,
            'folder_type' => 1,
            'name' => $huddle_name,
            'desc' => '',
            'active' => '1',
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
        );
        $this->AccountFolder->create();
        $this->AccountFolder->save($data);

        $account_folder_id = $this->AccountFolder->id;

        $account_folders_meta_data = array(
            'account_folder_id' => $account_folder_id,
            'meta_data_name' => 'message',
            'meta_data_value' => '',
            'created_date' => date("Y-m-d H:i:s"),
            'last_edit_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_by' => $user_id
        );
        $this->AccountFolderMetaData->create();
        $this->AccountFolderMetaData->save($account_folders_meta_data);

        $account_folder_meta_data = array(
            'account_folder_id' => $account_folder_id,
            'meta_data_name' => 'autocreated',
            'meta_data_value' => "1",
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
        );
        $this->AccountFolderMetaData->create();
        $this->AccountFolderMetaData->save($account_folder_meta_data);
        $huddle_users = explode(",", $observer_users);
        foreach ($huddle_users as $huddle_user) {
            if (in_array($huddle_user, $account_users)) {
                $superUserData = array(
                    'account_folder_id' => $account_folder_id,
                    'user_id' => $huddle_user,
                    'role_id' => ($huddle_user == $user_id) ? '200' : '210',
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_by' => $user_id
                );
                $this->AccountFolderUser->create();
                $this->AccountFolderUser->save($superUserData);
            }
        }
        return $account_folder_id;
    }

    function save_uploader_email($document_id, $owner_id, $uploader_email, $uploader_firstname, $uploader_lastname) {
        $account_folder_meta_data = array(
            'account_folder_id' => $document_id,
            'meta_data_name' => 'web_uploader_email',
            'meta_data_value' => $uploader_email,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $owner_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $owner_id,
        );
        $this->AccountFolderMetaData->create();
        $this->AccountFolderMetaData->save($account_folder_meta_data);
        $account_folder_meta_data = array(
            'account_folder_id' => $document_id,
            'meta_data_name' => 'web_uploader_name',
            'meta_data_value' => $uploader_firstname . ' ' . $uploader_lastname,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $owner_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $owner_id,
        );
        $this->AccountFolderMetaData->create();
        $this->AccountFolderMetaData->save($account_folder_meta_data);
    }

    function externalsavedocument($account_folder_id, $account_id, $user_id, $suppress_render = false) {
        $this->layout = false;
        if ($this->request->is('post')) {
            $previous_storage_used = $this->Account->getPreviousStorageUsed($account_id);

            $current_user = $this->User->find('first', array('conditions' => array('id' => $user_id)));
            $current_account_folder = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $account_folder_id)));

            $account_folder_type_string = "Workspace";
            $video_title = $this->request->data['video_title'];

            if ($current_account_folder['AccountFolder']['folder_type'] == 1) {
                $account_folder_type_string = $current_account_folder['AccountFolder']['name'];
                if (empty($video_title)) {
                    $video_title = $account_folder_type_string;
                }
            }

            $path_parts = pathinfo($this->request->data['video_url']);
            $video_file_name = $this->cleanFileName($path_parts['filename']) . "." . $path_parts['extension'];
            $video_file_name_only = $this->cleanFileName($path_parts['filename']);

            $request_dump = addslashes(print_r($_POST, true));

            $this->Document->create();
            $data = array(
                'account_id' => $account_id,
                'doc_type' => '1',
                'url' => $this->request->data['video_url'],
                'original_file_name' => $this->request->data['video_file_name'],
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'recorded_date' => date("Y-m-d H:i:s"),
                'file_size' => (int) $this->request->data['video_file_size'],
                'last_edit_by' => $user_id,
                'published' => '0',
                'post_rubric_per_video' => '1'
            );
            if ($this->Document->save($data, $validation = TRUE)) {

                $video_id = $this->Document->id;
                $user_activity_logs = array(
                    'ref_id' => $video_id,
                    'desc' => $this->request->data['video_file_name'],
                    'url' => $this->base . '/Huddles/view/' . $account_folder_id . "/1/" . $video_id,
                    'account_folder_id' => $account_folder_id,
                    'type' => '2'
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

                require('src/services.php');
                $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                $s3_src_folder_path = $this->request->data['video_url'];

                $s3_service = new S3Services(
                        Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                );
                $s3_service->set_acl_permission($s3_src_folder_path);

                $s3_zencoder_id = $this->CreateJobQueueTranscodingJob($video_id, $s3_src_folder_path);

                $this->Document->create();
                $this->Document->updateAll(
                        array(
                    'zencoder_output_id' => $s3_zencoder_id,
                    'encoder_provider' => Configure::read('encoder_provider'),
                    'file_size' => (int) $this->request->data['video_file_size'],
                    'request_dump' => "'$request_dump'"
                        ), array('id' => $video_id)
                );
                if ($previous_storage_used['Account']['storage_used'] != '') {
                    $new_storage_used = $this->request->data['video_file_size'] + $previous_storage_used['Account']['storage_used'];
                } else {
                    $new_storage_used = $this->request->data['video_file_size'];
                }

                $this->Account->updateAll(array('storage_used' => $new_storage_used), array('id' => $account_id));
                $this->AccountFolderDocument->create();
                $data = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $video_id,
                    'title' => $this->request->data['video_title'],
                    'zencoder_output_id' => "$s3_zencoder_id",
                    'encoder_provider' => Configure::read('encoder_provider'),
                    'desc' => $this->request->data['video_desc']
                );
                $this->AccountFolderDocument->save($data, $validation = TRUE);
            } else {
                $video_id = 0;
            }

//            if ($suppress_render == true) {

            return $video_id;

//            } else {
//
//                $this->layout = null;
//                $res = array(
//                    'video_id' => $video_id,
//                    //'url' => $s_url
//                    'url' => ''
//                );
//                $this->set('ajaxdata', json_encode($res));
//                $this->render('/Elements/ajax/ajaxreturn');
//            }
        } else {
//            $this->set('huddle_id', $account_folder_id);
//            $this->set('upload_type', 'video');
//            $this->layout = 'fileupload';
//            $this->render('uploadVideos');
        }
    }

    function update_user_subscrition($owner_id, $account_folder_id) {
        $current_user = $this->Session->read('user_current_account');
        $account_id = $current_user['accounts']['account_id'];

        $huddle_users = $this->AccountFolderUser->find("list", array(
            "conditions" => array("account_folder_id" => $account_folder_id, 'NOT' => array('role_id' => '200')),
            "fields" => array("user_id")
        ));
        foreach ($huddle_users as $huddle_user) {
            $chk_sub = $this->check_subscription($huddle_user, '7', $account_id);
            if ($chk_sub) {
                $data = array(
                    "email_format_id" => '7',
                    "user_id" => $huddle_user,
                    "created_at" => date("Y-m-d H:i:s"),
                    "created_by" => $owner_id
                );
                $this->EmailUnsubscribers->create();
                $this->EmailUnsubscribers->save($data);
            }
        }
        return true;
    }

    function view_document($url) {       
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $account_folder_id = '';
        $documents = $this->Document->find('first', array('conditions' => array('stack_url LIKE' => '%'.$url.'%')));
        $result_data = $this->AccountFolderDocument->get_account_folder_document($documents['Document']['id']);

        if($result_data ){
            $account_folder_id = $result_data['AccountFolderDocument']['account_folder_id'];
        }

        $user_activity_logs = array(
                'ref_id' => $documents['Document']['id'],
                'desc' => 'Download Resource',
                'url' => $this->base . '/Huddles/download/' . $documents['Document']['id'],
                'type' => '13',
                'account_folder_id' => $account_folder_id ,
                'environment_type' => '2',
            );
        $this->user_activity_logs($user_activity_logs);
        $this->create_churnzero_event('Resource+Viewed', $account_id, $users['User']['email']);
        $this->layout = '';
        $this->set('url_code', $url);
        $this->render('/Elements/view_documents');
    }

    function export_in_phpExcel($html, $file_name) {
        $tmpfile = time() . '.html';
        $dir = realpath(dirname(__FILE__) . '/../..') . "/app/webroot/files/tempupload";
        file_put_contents($dir . "/$tmpfile", $html);
        $reader = new PHPExcel_Reader_HTML;
        $content = $reader->load($dir . "/$tmpfile");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=" . rawurlencode($file_name));
        $writer = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
        $writer->save('php://output');
        unlink($dir . "/$tmpfile");
        die;
    }

    function get_live_video_observations() {
        $current_user = $this->Session->read('user_current_account');
        $account_id = $current_user['accounts']['account_id'];
        $user_id = $current_user['User']['id'];
        $view = new View($this, false);
        $alert_messages = $view->Custom->get_page_lang_based_content('alert_messages');
        $result = $this->UserActivityLog->find('all', array(
            'conditions' => array(
                'type' => 20,
                'user_id' => $user_id,
                'account_id' => $account_id,
                " ref_id NOT IN ( select ref_id from user_activity_logs where type = 21 and user_id = $user_id and account_id=$account_id)"
            )
        ));
        $view = new View($this, false);
        if (count($result) > 0) {
            for ($i = 0; $i < count($result); $i++) {
                $live_observation = $result[$i];
                $account_folder_id = $live_observation['UserActivityLog']['account_folder_id'];
                $video_id = $live_observation['UserActivityLog']['ref_id'];
                $url = '';

                if ($view->Custom->workspace_video($account_folder_id) == 0) {
                    $live_observation['UserActivityLog']['default_text'] = $alert_messages['started_a_new_video_observation'] . ':';
                    $live_observation['UserActivityLog']['redirect_link'] = '/Huddles/observation_details_2/' . $account_folder_id . '/' . $video_id;
                    $live_observation['UserActivityLog']['title'] = ' Observation -' . date('m:d:Y h:i A', strtotime($live_observation['UserActivityLog']['date_added'])) . '-' . 'Observation_' . date('m:d:Y', strtotime($live_observation['UserActivityLog']['date_added']));
                } else {
                    $live_observation['UserActivityLog']['default_text'] = $alert_messages['started_synced_video_notes'] . ':';
                    $live_observation['UserActivityLog']['redirect_link'] = '/MyFiles/view/1/' . $account_folder_id;
                    $live_observation['UserActivityLog']['title'] = ' Synced_notes -' . date('m:d:Y h:i A', strtotime($live_observation['UserActivityLog']['date_added'])) . '-' . 'Synced_notes_' . date('m:d:Y', strtotime($live_observation['UserActivityLog']['date_added']));
                }
                $live_observation['UserActivityLog']['date_added_day_month'] = date('M d', strtotime($live_observation['UserActivityLog']['date_added']));
                $live_observation['UserActivityLog']['date_added_time'] = date('h:i A', strtotime($live_observation['UserActivityLog']['date_added']));

                $live_observations[$i] = $live_observation;
            }
            $result['live_observations'] = $live_observations;
        }
        echo json_encode($result);
        die;
    }

    function get_live_stream_video() {
        $current_user = $this->Session->read('user_current_account');
        $account_id = $current_user['accounts']['account_id'];
        $user_id = $current_user['User']['id'];
        $result = $this->UserActivityLog->find('all', array(
            'conditions' => array(
                'type' => 24,
                'user_id' => $user_id,
                'account_id' => $account_id,
                " ref_id NOT IN ( select ref_id from user_activity_logs where type = 25 and user_id = $user_id and account_id=$account_id)"
            )
        ));
        $view = new View($this, false);
        if (count($result) > 0) {
            for ($i = 0; $i < count($result); $i++) {
                $live_observation = $result[$i];
                $account_folder_id = $live_observation['UserActivityLog']['account_folder_id'];
                $video_id = $live_observation['UserActivityLog']['ref_id'];
                $url = '';
                $live_observation['UserActivityLog']['default_text'] = 'You started a new Live Stream Video:';
                $live_observation['UserActivityLog']['redirect_link'] = '/Huddles/view_live/' . $account_folder_id . '/1/' . $video_id;
                $live_observation['UserActivityLog']['title'] = ' Live Stream -' . date('m:d:Y h:i A', strtotime($live_observation['UserActivityLog']['date_added'])) . '-' . 'Live_Stream_' . date('m:d:Y', strtotime($live_observation['UserActivityLog']['date_added']));

                $live_observation['UserActivityLog']['date_added_day_month'] = date('M d', strtotime($live_observation['UserActivityLog']['date_added']));
                $live_observation['UserActivityLog']['date_added_time'] = date('h:i A', strtotime($live_observation['UserActivityLog']['date_added']));

                $live_observations[$i] = $live_observation;
            }
            $result['live_stream_video'] = $live_observations;
        }
        echo json_encode($result);
        die;
    }

    function sendEmailFeedBackVideo($data, $user_id = '') {
        if (!Validation::email($data['email']))
            return FALSE;
        $users = $this->Session->read('user_current_account');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $users['User']['first_name'] . " " . $users['User']['last_name'] . '  ' . $users['accounts']['company_name'] . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];
        $this->Email->subject = "Re:[Huddle] " . $data['huddle_name'] . ": Feedback is ready for you to view";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $data['account_folder_id'], 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $params = array(
            'data' => $data,
            'user_id' => $user_id,
            'htype' => $htype,
        );
        $html = $view->element('emails/feedback_email', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }

    function sendEmailVideoLibrary($data, $user_id = '') {
        if (!Validation::email($data['email']))
            return FALSE;

        $users = $this->Session->read('user_current_account');
        $lib_user_id = $users['User']['id'];
        $users = $this->User->find('first', array('conditions' => array('id' => $lib_user_id)));

        $site_title = $this->custom->get_site_settings('site_title');
        $this->Email->delivery = 'smtp';
        $this->Email->from = $site_title . '<' . $this->custom->get_site_settings('static_emails')['noreply'] . '>';
        $this->Email->to = $data['email'];
        //$this->Email->subject = $data['company_name'] . " Video Library.";
        //$this->Email->subject = $this->subject_title . " Video Added";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $view = new View($this, true);
        $params = array(
            'data' => $data,
        );
        //$html = $view->element('emails/video_library_email', $params);
        $lang = $users['User']['lang'];
        if ($lang == 'en') {
            $this->Email->subject = $this->subject_title . " - Video Added to Libraries";
        } else {
            $this->Email->subject = $this->subject_title . " - Video Añadido a Biblioteca";
        }
        $key = "video_library_email_" . $this->site_id . "_" . $lang;

        $result = $this->get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;

        $html = str_replace('<%body%>', '', $html);
        //$html = str_replace('{sender}', $users['User']['first_name'] . "  " . $users['User']['last_name'], $html);
        $html = str_replace('{title}', $data['video_name'], $html);
        if (isset($data['child_names']) && count($data['child_names']) > 0) {
            $html = str_replace('{accounts}', implode(", ", $data['child_names']), $html);
        }
        $html = str_replace('{redirect_url}', $this->get_host_url() . '/VideoLibrary/view/' . $data['huddle_id'], $html);
        $html = str_replace('{unsubscribe_url}', $this->get_host_url() . '/subscription/unsubscirbe_now/' . $users['User']['id'] . '/7', $html);
        $html = str_replace('{site_url}', $this->website_url, $html);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $data['account_id'],
            'email_from' => $this->Email->from,
            'email_to' => $data['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            $this->AuditEmail->save($auditEmail, $validation = TRUE);
            $use_job_queue = Configure::read('use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $this->Email->subject, $html, true);
                return TRUE;
            } else {
                if ($this->Email->send($html)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }

        return FALSE;
    }

    function job_queue_storage_function($account_id, $jobid) {


        if (!empty($account_id) && $account_id > 0) {

            $xml = "<StorageCalculationJob><accounts_id>$account_id</accounts_id></StorageCalculationJob>";
            $this->JobQueue->create();
            $data = array(
                'JobId' => $jobid,
                'CreateDate' => date("Y-m-d H:i:s"),
                'RequestXml' => $xml,
                'JobQueueStatusId' => 1,
                'CurrentRetry' => 0
            );

            $this->JobQueue->save($data);
        }

        return TRUE;
    }

    function is_account_activated($account_id, $user_id) {
        $result = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        if ($result['User']['type'] == 'Pending_Activation') {
            $this->redirect('/users/activation_page/' . $account_id . '/' . $user_id);
        }
    }

    function edtpa_encrypt($plaintext, $key) {
        $plaintext = $this->edtpa_pkcs5_pad($plaintext, 16);
        return bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, hex2bin($key), $plaintext, MCRYPT_MODE_ECB));
    }

    function edtpa_decrypt($encrypted, $key) {
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, hex2bin($key), hex2bin($encrypted), MCRYPT_MODE_ECB);
        $padSize = ord(substr($decrypted, -1));
        return substr($decrypted, 0, $padSize * -1);
    }

    function edtpa_pkcs5_pad($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

    function get_edtpa_validated_xml($assessment_id, $assessment_version, $user_id, $account_id, $transfer_id, $encrypted_string) {

        $client = S3Client::factory(array(
                    'key' => Configure::read('access_key_id'),
                    'secret' => Configure::read('secret_access_key')
        ));

        $edtpa_base_url = Configure::read('edtpa_base_url');
        $edtpa_provider_name = Configure::read('edtpa_provider_name');
        $edtpa_shared_secret = Configure::read('edtpa_shared_secret');
        $edtpa_shared_key = Configure::read('edtpa_shared_key');
        $project_id = "edTPA";

        $edtpa_base_url = "https://partners.nesinc.com/ws/seam/resource/v1/Import/";
        $edtpa_base_url = "https://cert-ppp.eportfoliodev.com/ws/seam/resource/v1/Import/";
        $edtpa_provider_name = "Sibme";
        $edtpa_shared_secret = "z3ol8fvb739u";
        $edtpa_shared_key = "cfb1bb092c907fb8db23456b7c7a5411c4059796f582902e7cd34add57b9064a";
        $project_id = "edTPA";
        if (IS_PROD) {
            $edtpa_base_url = "https://ppp.eportfolio.pearson.com/ws/seam/resource/v1/Import/";
            $edtpa_provider_name = "Sibme";
            $edtpa_shared_secret = "4*fx1ENebUbf#w6q4nz3TBfRx";
            $edtpa_shared_key = "e557cc4002f5957da1f0e2ed29b4ef12c2497fa721e9ff24b9a225d06d3baf1c";
            $project_id = "edTPA";
        }


        $edtpa_assessment = $this->EdtpaAssessments->find('first', array(
            'conditions' => array(
                'assessment_id' => $assessment_id,
                'assessment_version' => $assessment_version,
                'project_id' => $project_id
            )
        ));

        if (count($edtpa_assessment) == 0) {

            $current_date = date("Y-m-d\TH:i:s.000\Z", strtotime("now"));
            $text = '<?xml version="1.0" encoding="UTF-8"?>
            <espm:portfolioManifest created="' . $current_date . '" schemaVersion="2.0" xmlns:espm="http://www.pearson.com/ESPortfolioManifest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.pearson.com/ESPortfolioManifest http://partners.nesinc.com/ws/schemas/ESPortfolioManifest_v2.xsd">
                <espm:transferRequestInformation partnerTransferRequestID="' . $transfer_id . '" partnerID="Sibme" submittedAt="' . $current_date . '">
                    <espm:partnerCandidateID>' . $user_id . '</espm:partnerCandidateID>
                    <espm:partnerPortfolioID>SibmePortfolio_' . md5($transfer_id) . '</espm:partnerPortfolioID>
                </espm:transferRequestInformation>';
            $text .= '</espm:portfolioManifest>';
            return $text;
        } else {


//found the assessment
            $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
                'conditions' => array(
                    'edtpa_assessment_id' => $edtpa_assessment['EdtpaAssessments']['id'],
                    'account_id' => $account_id
                )
            ));

            if (count($edtpa_assessment_account) > 0) {

                $edtpa_assessment_account_candidate = $this->EdtpaAssessmentAccountCandidates->find('first', array(
                    'conditions' => array(
                        'user_id' => $user_id,
                        'edtpa_assessment_account_id' => $edtpa_assessment_account['EdtpaAssessmentAccounts']['id']
                    )
                ));

                if (count($edtpa_assessment_account_candidate) > 0) {


                    $current_date = date("Y-m-d\TH:i:s.000\Z", strtotime("now"));
                    $key = pack('H*', "$edtpa_shared_key");

                    $text = '<?xml version="1.0" encoding="UTF-8"?>
                <espm:portfolioManifest created="' . $current_date . '" schemaVersion="2.0" xmlns:espm="http://www.pearson.com/ESPortfolioManifest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.pearson.com/ESPortfolioManifest http://partners.nesinc.com/ws/schemas/ESPortfolioManifest_v2.xsd">
                    <espm:transferRequestInformation partnerTransferRequestID="' . $transfer_id . '" partnerID="Sibme" submittedAt="' . $current_date . '">
                        <espm:partnerCandidateID>' . $user_id . '</espm:partnerCandidateID>
                        <espm:partnerPortfolioID>SibmePortfolio_' . md5($transfer_id) . '</espm:partnerPortfolioID>
                    </espm:transferRequestInformation>';

                    $text .= '<espm:assessment projectID="' . $project_id . '" assessmentID="' . $assessment_id . '" assessmentVersion="' . $assessment_version . '" >';

                    $edtpa_assessment_portfolios = $this->EdtpaAssessmentPortfolios->find('all', array(
                        'conditions' => array(
                            'edtpa_assessment_id' => $edtpa_assessment['EdtpaAssessments']['id']
                        )
                    ));


                    foreach ($edtpa_assessment_portfolios as $edtpa_assessment_portfolio) {

                        $text .= '<espm:portfolioNode code="' . $edtpa_assessment_portfolio['EdtpaAssessmentPortfolios']['code'] . '">';

                        $edtpa_assessment_portfolio_nodes = $this->EdtpaAssessmentPortfolioNodes->find('all', array(
                            'conditions' => array(
                                'edtpa_assessment_portfolio_id' => $edtpa_assessment_portfolio['EdtpaAssessmentPortfolios']['id'],
                                'edtpa_assessment_id' => $edtpa_assessment['EdtpaAssessments']['id']
                            )
                        ));

                        foreach ($edtpa_assessment_portfolio_nodes as $edtpa_assessment_portfolio_node) {

                            $text .= '<espm:portfolioNode code="' . $edtpa_assessment_portfolio_node['EdtpaAssessmentPortfolioNodes']['code'] . '">';

                            $edtpa_assessment_portfolio_candidate_submissions = $this->EdtpaAssessmentPortfolioCandidateSubmissions->find('all', array(
                                'joins' => array(
                                    array(
                                        'table' => 'documents as Document',
                                        'type' => 'left',
                                        'conditions' => 'Document.id = EdtpaAssessmentPortfolioCandidateSubmissions.document_id'
                                    )
                                ),
                                'fields' => array(
                                    'EdtpaAssessmentPortfolioCandidateSubmissions.*',
                                    'Document.*'
                                ),
                                'conditions' => array(
                                    'EdtpaAssessmentPortfolioCandidateSubmissions.edtpa_assessment_account_candidate_id' => $edtpa_assessment_account_candidate['EdtpaAssessmentAccountCandidates']['id'],
                                    'EdtpaAssessmentPortfolioCandidateSubmissions.edtpa_assessment_portfolio_node_id' => $edtpa_assessment_portfolio_node['EdtpaAssessmentPortfolioNodes']['id']
                                ),
                                'order' => 'edtpa_assessment_portfolio_node_id,EdtpaAssessmentPortfolioCandidateSubmissions.id'
                            ));

                            if (count($edtpa_assessment_portfolio_candidate_submissions) > 0) {

                                $submission_counter = 1;
                                foreach ($edtpa_assessment_portfolio_candidate_submissions as $edtpa_assessment_portfolio_candidate_submission) {

                                    if ($edtpa_assessment_portfolio_candidate_submission['Document']['doc_type'] == 2) {

                                        $document_url = $edtpa_assessment_portfolio_candidate_submission['Document']['url'];
                                        $document_url_path_info = pathinfo($document_url);

                                        $response_file = $client->doesObjectExist(Configure::read("bucket_name"), "uploads/" . $document_url_path_info['dirname'] . "/" . ($document_url_path_info['filename']) . "." . $document_url_path_info['extension']);

                                        $document_name_user_defined = $document_url_path_info['filename'];
                                        $account_folder_document = $this->AccountFolderDocument->get($edtpa_assessment_portfolio_candidate_submission['Document']['id']);
                                        if (!empty($account_folder_document)) {

                                            $account_folder_document_file_name = $account_folder_document['AccountFolderDocument']['title'];

                                            $account_folder_document_file_name = str_replace(".", " ", $account_folder_document_file_name);
                                            $document_title_path_info = pathinfo($account_folder_document_file_name);
                                            $document_name_user_defined = $document_title_path_info['filename'];
                                        }

                                        if ($response_file) {

                                            $document_name_user_defined = preg_replace('/\\.[^.\\s]{3,4}$/', '', $document_name_user_defined);

                                            $text .= '<espm:file sequence="' . $submission_counter . '">
                                                    <espm:name>' . ($document_name_user_defined) . "." . $document_url_path_info['extension'] . '</espm:name>
                                                    <espm:sizeinbytes>0</espm:sizeinbytes>
                                                </espm:file>';
                                        }
                                    } else {

                                        $document_url_obj = $this->get_document_url($edtpa_assessment_portfolio_candidate_submission['Document']);

                                        if ($document_url_obj['encoder_status'] == 'Complete' || $document_url_obj['encoder_status'] == 'complete' || $document_url_obj['encoder_status'] == '') {

                                            $document_url = $document_url_obj['relative_url'];
                                            $document_url_path_info = pathinfo($document_url);

                                            $response_file = $client->doesObjectExist(Configure::read("bucket_name"), $document_url_obj['relative_url']);

                                            $document_name_user_defined = $document_url_path_info['filename'];
                                            $account_folder_document = $this->AccountFolderDocument->get($edtpa_assessment_portfolio_candidate_submission['Document']['id']);
                                            if (!empty($account_folder_document)) {

                                                $account_folder_document_file_name = $account_folder_document['AccountFolderDocument']['title'];

                                                $account_folder_document_file_name = str_replace(".", " ", $account_folder_document_file_name);
                                                $document_title_path_info = pathinfo($account_folder_document_file_name);
                                                $document_name_user_defined = $document_title_path_info['filename'];
                                            }

                                            if ($response_file) {

                                                $document_name_user_defined = preg_replace('/\\.[^.\\s]{3,4}$/', '', $document_name_user_defined);

                                                $text .= '<espm:file sequence="' . $submission_counter . '">
                                                        <espm:name>' . ($document_name_user_defined) . "." . $document_url_path_info['extension'] . '</espm:name>
                                                        <espm:sizeinbytes>0</espm:sizeinbytes>
                                                    </espm:file>';
                                            }
                                        }
                                    }


                                    $submission_counter += 1;
                                }
                            }


                            $text .= '</espm:portfolioNode>';
                        }

                        $text .= '</espm:portfolioNode>';
                    }

                    $text .= '</espm:assessment>';
                    $text .= '</espm:portfolioManifest>';

                    if ($encrypted_string) {
                        return $this->edtpa_encrypt($text, $edtpa_shared_key);
                    } else {
                        return $text;
                    }
                } else {

                    $current_date = date("Y-m-d\TH:i:s.000\Z", strtotime("now"));
                    $text = '<?xml version="1.0" encoding="UTF-8"?>
                    <espm:portfolioManifest created="' . $current_date . '" schemaVersion="2.0" xmlns:espm="http://www.pearson.com/ESPortfolioManifest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.pearson.com/ESPortfolioManifest http://partners.nesinc.com/ws/schemas/ESPortfolioManifest_v2.xsd">
                        <espm:transferRequestInformation partnerTransferRequestID="' . $transfer_id . '" partnerID="Sibme" submittedAt="' . $current_date . '">
                            <espm:partnerCandidateID>' . $user_id . '</espm:partnerCandidateID>
                            <espm:partnerPortfolioID>SibmePortfolio_' . md5($transfer_id) . '</espm:partnerPortfolioID>
                        </espm:transferRequestInformation>';
                    $text .= '</espm:portfolioManifest>';

                    return $text;
                }
            }
        } //end of else for Assessment count
    }

    function assessment_zipfile_maker($transfer_id) {

        $transfer_details = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array('conditions' => array('id' => $transfer_id)));

        if (!empty($transfer_details)) {
            $assessment_candidate_id = $transfer_details['EdtpaAssessmentAccountCandidateTransfers']['edtpa_assessment_account_candidate_id'];
        } else {
            $output['success'] = false;
            $output['error'] = 'Candidate key not found';
            return json_encode($output);
            die;
        }

        $edtpa_account_candidate_details = $this->EdtpaAssessmentAccountCandidates->find('first', array('conditions' => array('id' => $assessment_candidate_id)));

        if (!empty($edtpa_account_candidate_details)) {

            $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
                'conditions' => array(
                    'id' => $edtpa_account_candidate_details['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id']
                )
            ));

            if (!empty($edtpa_assessment_account)) {

                $edtpa_assessment_detail = $this->EdtpaAssessments->find('first', array('conditions' => array('id' => $edtpa_assessment_account['EdtpaAssessmentAccounts']['edtpa_assessment_id'])));
            } else {

                $output['success'] = false;
                $output['error'] = 'edTPA Assessment ID not associated with Account';
                return json_encode($output);
                die;
            }
        } else {
            $output['success'] = false;
            $output['error'] = 'edTPA Assessment Candidate not found';
            return json_encode($output);
            die;
        }

        $user_id = $edtpa_account_candidate_details['EdtpaAssessmentAccountCandidates']['user_id'];
        $account_id = $edtpa_account_candidate_details['EdtpaAssessmentAccountCandidates']['account_id'];

        if (!empty($edtpa_assessment_detail)) {
            $xmlEncryptedString = $this->get_edtpa_validated_xml($edtpa_assessment_detail['EdtpaAssessments']['assessment_id'], $edtpa_assessment_detail['EdtpaAssessments']['assessment_version'], $user_id, $account_id, $transfer_id, true);
            $xmlString = $this->get_edtpa_validated_xml($edtpa_assessment_detail['EdtpaAssessments']['assessment_id'], $edtpa_assessment_detail['EdtpaAssessments']['assessment_version'], $user_id, $account_id, $transfer_id, false);
        } else {
            $output['success'] = false;
            $output['error'] = 'Assessment not found';
            return json_encode($output);
            die;
        }

        $targetfolder_root = getcwd();
        $targetfolder = "EdtpaZipFiles";
        $random_folder = $user_id . '_' . date('Y-m-d') . '_' . strtotime('now');

        if (!file_exists($targetfolder)) {
            mkdir($targetfolder, 0777, true);
        }
        $targetfolder = "EdtpaZipFiles/" . $random_folder;

        if (!file_exists($targetfolder)) {
            mkdir($targetfolder, 0777, true);
        }


        $file = fopen($targetfolder . '/portfolioManifest.xml', "w");
        fwrite($file, $xmlEncryptedString);
        fclose($file);


        $doc_video_details = $this->EdtpaAssessmentPortfolioCandidateSubmissions->find('all', array('conditions' => array('edtpa_assessment_account_candidate_id' => $assessment_candidate_id),
            'order' => 'edtpa_assessment_portfolio_node_id,id'
                )
        );

        $file_counter = 1;
        foreach ($doc_video_details as $row) {
            $edtpa_assessment_portfolio_node_details = $this->EdtpaAssessmentPortfolioNodes->find('first', array('conditions' => array('id' => $row['EdtpaAssessmentPortfolioCandidateSubmissions']['edtpa_assessment_portfolio_node_id'])));

            $edtpa_assessment_portfolio_details = $this->EdtpaAssessmentPortfolios->find('first', array('conditions' => array('id' => $edtpa_assessment_portfolio_node_details['EdtpaAssessmentPortfolioNodes']['edtpa_assessment_portfolio_id'])));

            $new_folder = $targetfolder . '/' . $edtpa_assessment_portfolio_details['EdtpaAssessmentPortfolios']['code'];
            if (!file_exists($new_folder)) {
                mkdir($new_folder, 0777, true);
            }

            $new_folder_2 = $new_folder . '/' . $edtpa_assessment_portfolio_node_details['EdtpaAssessmentPortfolioNodes']['code'];

            if (!file_exists($new_folder_2)) {
                mkdir($new_folder_2, 0777, true);
                $file_counter = 1;
            }

            $new_folder_3 = $new_folder_2 . '/FILE' . $file_counter;

            if (!file_exists($new_folder_3)) {
                mkdir($new_folder_3, 0777, true);
            }

            $this->download_zipfolder_make($row['EdtpaAssessmentPortfolioCandidateSubmissions']['document_id'], $new_folder_3);

            $file_counter += 1;
        }


//  print_r($doc_video_details);die;
        $xmlArray = Xml::toArray(Xml::build($xmlString));

        $portfolio_nodes = $xmlArray['Assessment']['PortfolioNode'];

        foreach ($portfolio_nodes as $portfolio_node) {
            $new_folder = $targetfolder . '/' . $portfolio_node['@code'];
            if (!file_exists($new_folder)) {
                mkdir($new_folder, 0777, true);
            }

            foreach ($portfolio_node['PortfolioNode'] as $row) {

                $new_folder_2 = $new_folder . '/' . $row['@code'];

                if (!file_exists($new_folder_2)) {
                    mkdir($new_folder_2, 0777, true);
                }
            }
        }
        $the_folder = $targetfolder;
        $zip_file_name = $targetfolder . '/' . $user_id . '_' . date('Y-m-d') . '_' . strtotime('now') . '.zip';
        $zip = new ZipArchive;
        $res = $zip->open($zip_file_name, ZipArchive::CREATE);
        if ($res === TRUE) {

// Create recursive directory iterator
            /** @var SplFileInfo[] $files */
            $files = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($the_folder), RecursiveIteratorIterator::LEAVES_ONLY
            );

            foreach ($files as $name => $file) {
// Skip directories (they would be added automatically)
                if (!$file->isDir()) {
// Get real and relative path for current file
                    echo $relativePath;
                    echo "<br/>";
                    echo $random_folder;
                    echo "<br/>";
                    echo $the_folder;
                    echo "<br/>";

                    $filePath = $file->getRealPath();

                    echo "filepath:" . $filePath;
                    echo "<br/>";

                    if (IS_PROD) {
//$relativePath = substr($filePath, strlen($the_folder));
                        $relativePath = substr($filePath, strlen("$targetfolder_root/" . $the_folder) + 1);
                    } else {
//$relativePath = substr($filePath, strlen($the_folder) + 1);
                        $relativePath = substr($filePath, strlen($the_folder));
                    }


                    $relativePath = str_replace("$random_folder/", "", $relativePath);

                    $filename_parts = explode('/', $filePath);
// Add current file to archive
                    echo "final:" . $relativePath;
                    echo "<br/>";

                    $zip->addFile($filePath, $relativePath);
                }
            }

// Zip archive will be created only after closing object
            $zip->close();
        } else {
            $output['success'] = false;
            $output['error'] = 'Unable to create zip file';
            return json_encode($output);
            die;
        }
        $update_data = array(
            'zip_path' => "'" . $zip_file_name . "'",
            'zip_generated' => 1
        );

        $this->save_to_s3($zip_file_name);

        $assessment_id = $edtpa_assessment_detail['EdtpaAssessments']['assessment_id'];
        $assessment_version = $edtpa_assessment_detail['EdtpaAssessments']['assessment_version'];
        $candidate_key = $edtpa_account_candidate_details['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
        $partnerTransferRequestID = $transfer_id;

        $edtpa_base_url = Configure::read('edtpa_base_url');
        $edtpa_provider_name = Configure::read('edtpa_provider_name');
        $edtpa_shared_secret = Configure::read('edtpa_shared_secret');
        $edtpa_shared_key = Configure::read('edtpa_shared_key');
        $project_id = Configure::read('project_id');

        //$edtpa_base_url = "https://partners.nesinc.com/ws/seam/resource/v1/Import/";
        $edtpa_base_url = "https://cert-ppp.eportfoliodev.com/ws/seam/resource/v1/Import/";
        $edtpa_provider_name = "Sibme";
        $edtpa_shared_secret = "z3ol8fvb739u";
        $edtpa_shared_key = "cfb1bb092c907fb8db23456b7c7a5411c4059796f582902e7cd34add57b9064a";
        $project_id = "edTPA";
        if (IS_PROD) {
            $edtpa_base_url = "https://ppp.eportfolio.pearson.com/ws/seam/resource/v1/Import/";
            $edtpa_provider_name = "Sibme";
            $edtpa_shared_secret = "4*fx1ENebUbf#w6q4nz3TBfRx";
            $edtpa_shared_key = "e557cc4002f5957da1f0e2ed29b4ef12c2497fa721e9ff24b9a225d06d3baf1c";
            $project_id = "edTPA";
        }

        $epoh_time = strtotime('now');

        $signature = base64_encode(hash_hmac('sha1', utf8_encode($edtpa_provider_name . $candidate_key . $project_id . $assessment_id . $assessment_version . $partnerTransferRequestID .
                                $epoh_time . $user_id), $edtpa_shared_secret, true));


        $url = "$edtpa_base_url" . "scheduleTransferRequest/";
        $fields = array(
            'partnerID' => urlencode($edtpa_provider_name),
            'projectID' => urlencode($project_id),
            'assessmentID' => urlencode($assessment_id),
            'assessmentVersion' => urlencode($assessment_version),
            'authorizationKey' => urlencode($candidate_key),
            'partnerCandidateID' => urlencode($user_id),
            'partnerTransferRequestID' => $partnerTransferRequestID,
            'timestamp' => urlencode($epoh_time),
            'signature' => urlencode($signature)
        );


//url-ify the data for the POST
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

//execute post
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//close connection
        curl_close($ch);




        if ($result == true && $httpcode == 200) {
            $this->EdtpaAssessmentAccountCandidates->updateAll(array('status' => 4), array('id' => $assessment_candidate_id));
        }

        $this->EdtpaAssessmentAccountCandidateTransfers->updateAll($update_data, array('id' => $transfer_id));
        $output['success'] = true;
        $output['error'] = '';
        return $output;
// echo json_encode($output);
//die;
    }

    function Portfolio_Package_Transfer_Cancel($transfer_id) {

        $edtpa_lang = $this->Session->read('edtpa_lang');
        $transfer_details = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array(
            'conditions' => array(
                'id' => $transfer_id
            ),
            'order' => array('id' => 'ASC')
        ));

        if (!empty($transfer_details)) {
            $assessment_candidate_id = $transfer_details['EdtpaAssessmentAccountCandidateTransfers']['edtpa_assessment_account_candidate_id'];
        } else {
            $output['success'] = false;
            $output['error'] = 'Candidate key not found';
            echo json_encode($output);
            die;
        }

        $edtpa_account_candidate_details = $this->EdtpaAssessmentAccountCandidates->find('first', array('conditions' => array('id' => $assessment_candidate_id)));

        if (!empty($edtpa_account_candidate_details)) {

            $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
                'conditions' => array(
                    'id' => $edtpa_account_candidate_details['EdtpaAssessmentAccountCandidates']['edtpa_assessment_account_id']
                )
            ));

            if (!empty($edtpa_assessment_account)) {

                $edtpa_assessment_detail = $this->EdtpaAssessments->find('first', array('conditions' => array('id' => $edtpa_assessment_account['EdtpaAssessmentAccounts']['edtpa_assessment_id'])));
            } else {

                $output['success'] = false;
                $output['error'] = 'edTPA Assessment ID not associated with Account';
                echo json_encode($output);
                die;
            }
        } else {
            $output['success'] = false;
            $output['error'] = 'edTPA Assessment Candidate not found';
            echo json_encode($output);
            die;
        }

        $user_id = $edtpa_account_candidate_details['EdtpaAssessmentAccountCandidates']['user_id'];
        $account_id = $edtpa_account_candidate_details['EdtpaAssessmentAccountCandidates']['account_id'];

        if (!empty($edtpa_assessment_detail)) {
            $xmlEncryptedString = $this->get_edtpa_validated_xml($edtpa_assessment_detail['EdtpaAssessments']['assessment_id'], $edtpa_assessment_detail['EdtpaAssessments']['assessment_version'], $user_id, $account_id, $transfer_id, true);
            $xmlString = $this->get_edtpa_validated_xml($edtpa_assessment_detail['EdtpaAssessments']['assessment_id'], $edtpa_assessment_detail['EdtpaAssessments']['assessment_version'], $user_id, $account_id, $transfer_id, false);
        } else {
            $output['success'] = false;
            $output['error'] = 'Assessment not found';
            echo json_encode($output);
            die;
        }

        $assessment_id = $edtpa_assessment_detail['EdtpaAssessments']['assessment_id'];
        $assessment_version = $edtpa_assessment_detail['EdtpaAssessments']['assessment_version'];
        $candidate_key = $edtpa_account_candidate_details['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key'];
        $partnerTransferRequestID = $transfer_id;

        $edtpa_base_url = Configure::read('edtpa_base_url');
        $edtpa_provider_name = Configure::read('edtpa_provider_name');
        $edtpa_shared_secret = Configure::read('edtpa_shared_secret');
        $edtpa_shared_key = Configure::read('edtpa_shared_key');
        $project_id = "edTPA";

        //$edtpa_base_url = "https://partners.nesinc.com/ws/seam/resource/v1/Import/";
        $edtpa_base_url = "https://cert-ppp.eportfoliodev.com/ws/seam/resource/v1/Import/";
        $edtpa_provider_name = "Sibme";
        $edtpa_shared_secret = "z3ol8fvb739u";
        $edtpa_shared_key = "cfb1bb092c907fb8db23456b7c7a5411c4059796f582902e7cd34add57b9064a";
        $project_id = "edTPA";
        if (IS_PROD) {
            $edtpa_base_url = "https://ppp.eportfolio.pearson.com/ws/seam/resource/v1/Import/";
            $edtpa_provider_name = "Sibme";
            $edtpa_shared_secret = "4*fx1ENebUbf#w6q4nz3TBfRx";
            $edtpa_shared_key = "e557cc4002f5957da1f0e2ed29b4ef12c2497fa721e9ff24b9a225d06d3baf1c";
            $project_id = "edTPA";
        }

        $epoh_time = strtotime('now');

        $signature = base64_encode(hash_hmac('sha1', utf8_encode($edtpa_provider_name . $project_id . $assessment_id . $assessment_version . $candidate_key . $user_id .
                                $epoh_time), $edtpa_shared_secret, true));


        $url = "$edtpa_base_url" . "cancelTransferRequest/";
        $fields = array(
            'partnerID' => urlencode($edtpa_provider_name),
            'projectID' => urlencode($project_id),
            'assessmentID' => urlencode($assessment_id),
            'assessmentVersion' => urlencode($assessment_version),
            'authorizationKey' => urlencode($candidate_key),
            'partnerCandidateID' => urlencode($user_id),
            'timestamp' => urlencode($epoh_time),
            'signature' => urlencode($signature)
        );


//url-ify the data for the POST
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//execute post
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode == 200) {
            $this->EdtpaAssessmentAccountCandidates->updateAll(array('status' => 2), array('id' => $assessment_candidate_id));
        }
//close connection
        curl_close($ch);
        if ($httpcode == 200) {
            $output['success'] = true;
            $output['error'] = $edtpa_lang['edtpa_assessment_was_canceld'];
            echo json_encode($output);
            die;
        } else {
            $result = json_decode($result);
            $output['success'] = true;
            $output['error'] = $result->DisplayMessage;
            echo json_encode($output);
            die;
        }
    }

    function Portfolio_Package_Transfer_Status($partnerTransferRequestID, $rVal = false) {

        $output = array();
        $edtpa_base_url = Configure::read('edtpa_base_url');
        $edtpa_provider_name = Configure::read('edtpa_provider_name');
        $edtpa_shared_secret = Configure::read('edtpa_shared_secret');
        $edtpa_shared_key = Configure::read('edtpa_shared_key');
        $project_id = "edTPA";

        $partnerTransferRequestIDList = '["' . $partnerTransferRequestID . '"]';
        $epoh_time = strtotime('now');

        $signature = base64_encode(hash_hmac('sha1', utf8_encode($edtpa_provider_name . $partnerTransferRequestIDList . $epoh_time), $edtpa_shared_secret, true));


        $url = "$edtpa_base_url" . "getTransferRequestStatus/";
        $fields = array(
            'partnerID' => urlencode($edtpa_provider_name),
            'partnerTransferRequestIDList' => $partnerTransferRequestIDList,
            'timeStamp' => urlencode($epoh_time),
            'signature' => urlencode($signature)
        );


//url-ify the data for the POST
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

//open connection
        $ch = curl_init();

//set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//execute post
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//close connection
        curl_close($ch);

        if ($httpcode == 200) {

            if (!$rVal) {

                echo $result;
                die;
            } else {

                return json_decode($result);
            }
        } else {

            if (!$rVal) {

                $output['success'] = false;
                $output['error'] = 'Error getting the status';
                echo json_encode($output);
                die;
            } else {

                $output['success'] = false;
                $output['error'] = 'Error getting the status';
                return $output;
            }
        }
    }

    function Portfolio_Package_Retrieval() {
        $this->Email->delivery = 'smtp';
        $this->Email->from = "do-not-reply@sibme.com";
        //$this->Email->to = "tahir@jjtestsite.us";
        $this->Email->to = "khurri.saleem@gmail.com";
        $this->Email->subject = "Package here";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';

        $html = addslashes(print_r($_REQUEST, true));

        $this->Email->send($html);

        $partnerTransferRequestID = isset($_REQUEST['partnerTransferRequestid'])?$_REQUEST['partnerTransferRequestid']:'';
        $partnerTransferRequest = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array(
            'conditions' => array(
                'id' => $partnerTransferRequestID
            ))
        );
//var_dump($partnerTransferRequest);

        if (!empty($partnerTransferRequest)) {
            $update_data = array(
                'file_picked_at' => "'" . date('Y-m-d H:i:s') . "'",
                'zip_picked_up' => 1
            );

            $this->EdtpaAssessmentAccountCandidateTransfers->updateAll($update_data, array('id' => $partnerTransferRequestID));

            $data = array(
                'status' => '5'
            );

            $this->EdtpaAssessmentAccountCandidates->updateAll($data, array(
                'id' => $partnerTransferRequest['EdtpaAssessmentAccountCandidateTransfers']['edtpa_assessment_account_candidate_id']
                    ), $validation = TRUE);

            $zip_path = $partnerTransferRequest['EdtpaAssessmentAccountCandidateTransfers']['zip_path'];
            $this->retrieve_from_s3($zip_path);

            die;
            $zip_file_name = realpath(dirname(__FILE__) . '/../..') . "/app/webroot/$zip_path";
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . filesize($zip_file_name));
            header('Content-Disposition: attachment; filename="documents.zip"');
            header("Content-Transfer-Encoding: binary");
            session_write_close();
            readfile($zip_file_name);
            $this->set('ajaxdata', "");
            $this->render('/Elements/ajax/ajaxreturn');
        } else {
            echo "someting went wrong";
            die;
           // throw new NotFoundException();
        }
    }

    function addDir($location, $name, $za) {
        $za->addEmptyDir($name);
        $this->addDirDo($location, $name, $za);
    }

// EO addDir;

    /**  Add Files & Dirs to archive;;;; @param string $location Real Location;  @param string $name Name in Archive;;;;;; @author Nicolas Heimann * @access private   * */
    function addDirDo($location, $name, $za) {
        $name .= '/';
        $location .= '/';
// Read all Files in Dir
        $dir = opendir($location);
        while ($file = readdir($dir)) {
            if ($file == '.' || $file == '..')
                continue;
// Rekursiv, If dir: FlxZipArchive::addDir(), else ::File();
            $do = (filetype($location . $file) == 'dir') ? 'addDir' : 'addFile';
            if (filetype($location . $file) == 'dir') {
                $this->$do($location . $file, $name . $file, $za);
            } else {
                $za->addFile($location . $file, $name . $file);
            }
        }
    }

    function getSecureAmazonUrl($file, $name = null) {
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name');
        $expires = '+240 minutes';

// Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

        $url = $client->getObjectUrl($aws_bucket, $file, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : rawurlencode(basename($file)) ),
        ));

        return $url;
    }

    function getSecureAmazonSibmeUrl($file, $name = null) {
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name_cdn');
        $expires = '+240 minutes';

// Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

        $url = $client->getObjectUrl($aws_bucket, $file, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : rawurlencode(basename($file)) ),
        ));

        return $url;
    }

    function job_queue_edtpa_submission_function($transfer_id, $jobid) {


        return TRUE;
//        $xml = "<StorageCalculationJob><accounts_id>$account_id</accounts_id></StorageCalculationJob>";
        $xml = "<edTPAPrepareSubmission><edtpa_assessment_account_candidate_transfer_id>$transfer_id</edtpa_assessment_account_candidate_transfer_id></edTPAPrepareSubmission>";
        $this->JobQueue->create();
        $data = array(
            'JobId' => $jobid,
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $xml,
            'JobQueueStatusId' => 1,
            'CurrentRetry' => 0
        );

//$this->JobQueue->save($data);
    }

    function add_viewer_history() {

        if ($this->request->is('post')) {

            $response = array();
            $document_id = $this->request->data['document_id'];
            $minutes_watched = $this->request->data['minutes_watched'];
            $playback_rate = $this->request->data['playback_rate'];
            $user_current_account = $this->Session->read('user_current_account');
            $account_id = $user_current_account['accounts']['account_id'];
            $user_id = $user_current_account['User']['id'];

            $documentViewerHistory = $this->DocumentViewerHistory->query("SELECT
                                                                                `document_viewer_histories`.`id`
                                                                                , `document_viewer_histories`.`minutes_watched`
                                                                                , `document_files`.`duration`
                                                                            FROM
                                                                                `document_viewer_histories`
                                                                                INNER JOIN `document_files` 
                                                                                    ON (`document_viewer_histories`.`document_id` = `document_files`.`document_id`)
                                                                            WHERE (`document_viewer_histories`.`document_id` = ".$document_id.")
                                                                            ORDER BY minutes_watched DESC
                                                                            LIMIT 1");
            $result = false;
            //if (empty($documentViewerHistory)) {
            if (true){
                $data = array(
                    'document_id' => $document_id,
                    'minutes_watched' => $minutes_watched,
                    'playback_rate' => $playback_rate ? $playback_rate : 1,
                    'user_id' => $user_id,
                    'account_id' => $account_id,
                    'created_date' => date('Y-m-d H:i:s'),
                    'modified_date' => date('Y-m-d H:i:s'),
                );
                $result = $this->DocumentViewerHistory->save($data);
            } else {
                $dvh_id = $documentViewerHistory[0]['document_viewer_histories']['id'];
                $existing_minutes_watched = $documentViewerHistory[0]['document_viewer_histories']['minutes_watched'];
                $duration = $documentViewerHistory[0]['document_files']['duration'];
                $data['modified_date'] = "'".date('Y-m-d H:i:s')."'";
                if($minutes_watched > $existing_minutes_watched && $minutes_watched <= $duration){
                    $data['minutes_watched'] = $minutes_watched;
                } elseif($minutes_watched > $existing_minutes_watched && $minutes_watched > $duration) {
                    $data['minutes_watched'] = $duration;
                }
                $minutes_watched = $data['minutes_watched'];
                $result = $this->DocumentViewerHistory->updateAll($data, array('id' => $dvh_id));
            }

//echo "<pre>"; print_r($data); die;
            if ($result) {
                $response = array(
                    'status' => true,
                    'data1'=> $data,
                );
                
             $this->create_churnzero_event('Video+Hours+Viewed', $account_id, $user_current_account['User']['email'],$minutes_watched,'&cf_VideoId='.$document_id);   
            } else {
                $response = array(
                    'status' => false,
                    'data'=> $data,
                );
            }
            echo json_encode($response);
            die;
        }
    }

    function autoscroll_switch() {
        $user_id = $this->request->data['user_id'];
        $account_id = $this->request->data['account_id'];
        $value = $this->request->data['value'];
        $data = array(
            'autoscroll_switch' => $value,
        );
        $this->UserAccount->updateAll($data, array('user_id' => $user_id, 'account_id' => $account_id));
        die();
    }

    function get_start_end_date($account_id) {

        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));

        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];

        if (date('m') >= $acct_duration_start) {
            $yr = date('Y');
            $dd = $yr . '-' . $acct_duration_start . '-01';
        } else {
            $yr = date('Y') - 1;
            $dd = $yr . '-' . $acct_duration_start . '-01';
        }
        $stDate = date_create($dd);
        $st_date = date_format($stDate, 'Y-m-d');


        $d = date('Y-m-d', strtotime($st_date . "+11 month"));
        $endDate = date_create($d);
        $end_date = date_format($endDate, 'Y-m-t');

        $data_array = array(
            'start_date' => $st_date,
            'end_date' => $end_date
        );

        return $data_array;
    }

    function isAuthorized() {
        if ($this->Auth->user('is_active') == 0) {
            $this->Cookie->delete('remember_me_cookie');
            $this->Auth->logout();
            $this->Session->delete('last_logged_in_user');
            $this->Session->delete('access_token');
            if (isset($this->client))
                $this->client->revokeToken();
            $this->Session->setFlash($this->language_based_messages['you_have_been_deactivated_msg']);
            return $this->redirect('/');
        }
        return true;
    }

    function CreateJobQueueArchiveJob($document_id) {

        $jobQueueString = "<ArchiveJob><DocumentID>$document_id</DocumentID></ArchiveJob>";

        $this->JobQueue->create();
        $data = array(
            'JobId' => '6',
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $jobQueueString,
            'JobQueueStatusId' => 1,
            'CurrentRetry' => 0
        );


        if ($this->JobQueue->save($data, $validation = TRUE)) {

            return $this->JobQueue->id;
        } else {
            return false;
        }
    }

    function addResources($accountFolerType = '') {
        if ($this->request->is('post')) {
            $this->Session->write('selected_node', $this->request->data('node_id'));
            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $user_id = $users['User']['id'];
            $this->AccountFolder->create();
            $data = array(
                'account_id' => $account_id,
                'folder_type' => 3,
                'name' => $this->data['video_file_name'],
                'desc' => '',
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
            );
            if ($this->AccountFolder->save($data, $validation = TRUE)) {
                $account_folder_id = $this->AccountFolder->id;
                if ($accountFolerType == 1) {
                    $this->uploadVideos($account_folder_id, $account_id, $user_id, true);
                } else {
                    $video_id = isset($this->data['video_id']) ? $this->data['video_id'] : true;
                    $result = $this->uploadDocumentseDTPA($account_folder_id, $account_id, $user_id, $video_id, $this->data['url']);
//                    echo "<pre>";
//                    print_r($result);
//                    die;
                    $this->save_edTPA($result);
                }

                echo "Video Upload Successfully.";
                exit;
            } else {
                echo "Video Uploading failed";
                exit;
            }
        }
    }

    function save_edTPA($data) {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        if ($this->request->is('post')) {
            $assessment_account_id = $this->request->data('assessment_account_id');
            $selected_files[] = $data['document_id'];
            $node_id = $this->request->data('node_id');
            $candidate_record = $this->EdtpaAssessmentAccountCandidates->find('first', array('conditions' => array('edtpa_assessment_account_id' => $assessment_account_id, 'account_id' => $account_id, 'user_id' => $user_id)));
            $record_id = '';

            if (empty($candidate_record)) {
                $this->EdtpaAssessmentAccountCandidates->create();
                $data = array(
                    'edtpa_assessment_account_id' => $assessment_account_id,
                    'user_id' => $user_id,
                    'account_id' => $account_id
                );
                $this->EdtpaAssessmentAccountCandidates->save($data);
                $record_id = $this->EdtpaAssessmentAccountCandidates->getLastInsertID();
            } else {
                $record_id = $candidate_record['EdtpaAssessmentAccountCandidates']['id'];
            }

//  $this->EdtpaAssessmentPortfolioCandidateSubmissions->deleteAll(array('edtpa_assessment_account_candidate_id' => $record_id, 'edtpa_assessment_portfolio_node_id' => $node_id), false);
            if (!empty($selected_files)) {
                foreach ($selected_files as $selected_file):
                    $check_doc_record = $this->EdtpaAssessmentPortfolioCandidateSubmissions->find('first', array('conditions' => array('edtpa_assessment_account_candidate_id' => $record_id, 'document_id' => $selected_file, 'created_by' => $user_id, 'edtpa_assessment_portfolio_node_id' => $node_id)));

                    if (empty($check_doc_record)) {
                        $this->EdtpaAssessmentPortfolioCandidateSubmissions->create();
                        $data = array(
                            'edtpa_assessment_account_candidate_id' => $record_id,
                            'document_id' => $selected_file,
                            'edtpa_assessment_portfolio_node_id' => $node_id,
                            'created_at' => date('Y-m-d h:i:s'),
                            'created_by' => $user_id
                        );
                        $this->EdtpaAssessmentPortfolioCandidateSubmissions->save($data);
                    }
                endforeach;
            }
            if ($record_id != '') {
                $total_submission_count = $this->EdtpaAssessmentPortfolioCandidateSubmissions->find('count', array(
                    'conditions' => array(
                        'edtpa_assessment_account_candidate_id' => $record_id
                    ))
                );
            } else {
                $total_submission_count = false;
            }

            $result['status'] = true;
            $result['result'] = $data;
            $result['total_submissions'] = $total_submission_count;

            echo json_encode($result);
            die;
        }
    }

    function uploadDocumentseDTPA($account_folder_id, $account_id, $user_id, $video_id = '', $url_stack = '', $workspace = false) {
        $view = new View($this, false);
        if ($this->request->is('post')) {
            if ($workspace) {
                $loggedInUser = $this->Session->read('user_current_account');
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = $view->Custom->check_if_account_in_trial_intercom($account_id);
// if (IS_QA):

                $meta_data = array(
                    'workspace_resource' => 'true',
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );

                // $this->create_intercom_event('workspace-resource-uploaded', $meta_data, $loggedInUser['User']['email']);
                
                $this->create_churnzero_event('Workspace+Resource+Uploaded', $account_id, $loggedInUser['User']['email']);
//    endif;
            } else {
                $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
                $huddle_detail = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $account_folder_id)));
                $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
                if ($htype == 1) {
                    $huddle_type = 'Collaboration Huddle';
                } elseif ($htype == 2) {
                    $huddle_type = 'Coaching Huddle';
                } else {
                    $huddle_type = 'Assessment Huddle';
                }
                $loggedInUser = $this->Session->read('user_current_account');
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = $view->Custom->check_if_account_in_trial_intercom($account_id);
//   if (IS_QA):

                $meta_data = array(
                    'huddle_resource' => 'true',
                    'huddle_type' => $huddle_type,
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );
                
                if(isset($huddle_detail['AccountFolder']['folder_type']) && $huddle_detail['AccountFolder']['folder_type'] == '2' )
                {
                    $this->create_churnzero_event('Library+Resource+Uploaded', $account_id, $loggedInUser['User']['email']);
                    
                }
                else {
                    
                    // $this->create_intercom_event('huddle-resource-uploaded', $meta_data, $loggedInUser['User']['email']);
                    $this->create_churnzero_event('Huddle+Resource+Uploaded', $account_id, $loggedInUser['User']['email']);
                    
                }

                
//    endif;
            }


            if ($url_stack == 1) {
                $url_stack = $this->request->data['url'];
            }
            $previous_storage_used = $this->Account->getPreviousStorageUsed($account_id);
            $url = "$account_id/$account_folder_id/" . date('Y/m/d') . "/" . $this->request->data['video_url'];

            $path_parts = pathinfo($this->request->data['video_url']);
            $video_file_name = $path_parts['filename'] . "." . $path_parts['extension'];

            $path_parts_orig = pathinfo($this->request->data['video_file_name']);
            $request_dump = addslashes(print_r($_POST, true));
//$url_stack = explode('/',$url_stack );
            $this->Document->create();
            $data = array(
                'account_id' => $account_id,
                'doc_type' => '2',
                'url' => $url,
                'original_file_name' => $this->request->data['video_file_name'],
                'active' => 1,
                'created_date' => date("Y-m-d h:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d h:i:s"),
                'last_edit_by' => $user_id,
                'published' => '1',
                'stack_url' => $url_stack
            );


            if ($this->Document->save($data, $validation = TRUE)) {

                $document_id = $this->Document->id;

                $folder_type = $this->get_folder_type($account_folder_id);
                if ($folder_type == '1') {
                    $url = $this->base . '/Huddles/view/' . $account_folder_id . '/2';
                } elseif ($folder_type == '3') {
                    $url = $this->base . '/MyFiles/view/2';
                } else {
                    $url = $this->base . '/videoLibrary/view/' . $account_folder_id;
                }
                if ($view->Custom->check_if_eval_huddle($account_folder_id)) {
                    $url = $this->base . '/Huddles/view/' . $account_folder_id . '/1/' . $video_id;
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $url,
                        'type' => '3',
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                } else {
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $url,
                        'type' => '3',
                        // 'account_folder_id' => ($video_id != '' && $video_id != 'undefined') ? $video_id : $account_folder_id,
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                }


                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

                require('src/services.php');
//uploaded video
                $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                $s3_src_folder_path = $this->request->data['video_url'];

//echo $uploaded_video_file; die;
                $s3_service = new S3Services(
                        Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                );
                $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);

                $this->Document->create();
                $this->Document->updateAll(
                        array(
                    'zencoder_output_id' => 0,
                    'url' => "'" . addslashes($s3_dest_folder_path) . "'",
                    'file_size' => (int) $this->request->data['video_file_size'],
                    'request_dump' => "'" . $request_dump . "'"
                        ), array(
                    'id' => $document_id
                        )
                );

                if ($previous_storage_used['Account']['storage_used'] != '') {
                    $storage_in_used = $this->request->data['video_file_size'] + $previous_storage_used['Account']['storage_used'];
                } else {
                    $storage_in_used = $this->request->data['video_file_size'];
                }

                $this->Account->updateAll(array('storage_used' => $storage_in_used), array('id' => $account_id));
                $this->job_queue_storage_function($account_id, '4');
                $this->AccountFolderDocument->create();
                $data = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $document_id,
                    'title' => $path_parts_orig['filename'],
                    'desc' => $this->request->data['video_desc']
                );
                $this->AccountFolderDocument->save($data, $validation = TRUE);

                if (!empty($video_id) && $video_id != "undefined") {
                    $account_folder_document_id = $this->AccountFolderDocument->id;
                    $this->AccountFolderDocumentAttachment->create();
                    $data = array(
                        'account_folder_document_id' => $account_folder_document_id,
                        'attach_id' => "$video_id"
                    );
                    $this->AccountFolderDocumentAttachment->save($data, $validation = TRUE);
                }

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
                if ($view->Custom->check_if_eval_huddle($account_folder_id)) {
                    $documentData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'video_link' => $this->base . '/Huddles/view/' . $account_folder_id . '/1/' . $video_id,
                        'participating_users' => $huddleUsers
                    );
                } else {
                    $documentData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'video_link' => '/Huddles/view/' . $account_folder_id . '/2/',
                        'participating_users' => $huddleUsers
                    );
                }

                if ($view->Custom->check_if_eval_huddle($account_folder_id) == 1) {
                    $all_participants = array();
                    if ($huddleUsers) {
                        foreach ($huddleUsers as $usre) {
                            if ($usre['huddle_users']['role_id'] == 210) {
                                $all_participants[] = $usre['huddle_users']['user_id'];
                            }
                        }
                    }
                }

                $video_details = $view->Custom->get_video_details($video_id);
                $vide_created_by = $video_details['Document']['created_by'];

                foreach ($huddleUsers as $row) {
                    if ($row['User']['id'] == $user_id) {
                        continue;
                    }
                    if ($view->Custom->check_if_eval_huddle($account_folder_id) == 1) {
                        if ($view->Custom->check_if_evaluated_participant($account_folder_id, $user_id)) {
                            if (isset($all_participants) && is_array($all_participants) && in_array($row['User']['id'], $all_participants)) {
                                continue;
                            }
                        } else {

                            if ($vide_created_by != $row['User']['id']) {
                                continue;
                            }
                        }
                        $documentData['huddle_type'] = 3;
                    } else {
                        $documentData['huddle_type'] = '';
                    }
                    $documentData['email'] = $row['User']['email'];
                    $documentData['user_id'] = $row['User']['id'];
                    if ($this->check_subscription($row['User']['id'], '1', $account_id)) {
                        $this->sendDocumentEmail($documentData);
                    }
                }
            } else {
                $document_id = 0;
                $s_url = '';
            }

            $this->layout = null;
            $res = array(
                'document_id' => $document_id,
                'url' => $s_url
            );
            return $res;
//$this->set('ajaxdata', json_encode($res));
//$this->render('/Elements/ajax/ajaxreturn');
        } else {
            $this->set('huddle_id', $account_folder_id);
            $this->set('upload_type', 'doc');
            $this->layout = 'fileupload';
            $this->render('uploadVideos');
        }
    }

    function test_auto_stop() {
        $results = $this->UserActivityLog->find('all', array(
            'conditions' => array(
                'type' => 24,
                "ref_id NOT IN ( select ref_id from user_activity_logs where type = 25)",
                "TIMESTAMPDIFF(HOUR, date_added, now()) > 23"
            )
        ));

        if (isset($results) && count($results) > 0) {

            foreach ($results as $result) {

                $document_details = $this->Document->find('first', array(
                    'conditions' => array(
                        'id' => $result['UserActivityLog']['ref_id'],
                    )
                ));

                $dateFromDatabase = strtotime($document_details['Document']['last_edit_date']);
                $dateFiveMinutesAgo = strtotime("-5 minutes");

                $start_date = new DateTime($document_details['Document']['last_edit_date']);

                $start_date_end = new DateTime();
                $start_date_end->setTimestamp(strtotime('now'));

                $since_start = $start_date->diff($start_date_end);

                if ($since_start->i >= 5) {
                    $user_activity_logs = array(
                        'ref_id' => $result['UserActivityLog']['ref_id'],
                        'account_id' => $result['UserActivityLog']['account_id'],
                        'user_id' => $result['UserActivityLog']['user_id'],
                        'desc' => 'Live_Recording_Stopped',
                        'url' => $result['UserActivityLog']['url'],
                        'account_folder_id' => $result['UserActivityLog']['account_folder_id'],
                        'type' => '25',
                        'environment_type' => $result['UserActivityLog']['environment_type']
                    );
                    $this->user_activity_logs($user_activity_logs, $result['UserActivityLog']['account_id'], $result['UserActivityLog']['user_id']);

                    $update_array = array(
                        'is_processed' => 4,
                        'upload_status' => "'" . 'cancelled' . "'"
                    );

                    $this->Document->updateAll($update_array, array('id' => $result['UserActivityLog']['ref_id']), $validation = TRUE);
                }
            }
        }
        echo 'test';
        die;
    }

    function archive_check_for_amazon() {
        $arr = [
            'version' => 'latest',
            'region' => 'us-east-1',
            'credentials' => [
                'key' => 'AKIAJ4ZWDR5X5JKB7CZQ',
                'secret' => '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',
            ],
            'http' => [
                'verify' => false
            ]
        ];
        $s3Client = S3Client::factory($arr);
//print_r($s3newClient); die;
        $result = $s3Client->listBuckets();
//echo '<pre>'; print_r($s3Client->listObjects()); die;

        $newarr = array(
            'Bucket' => 'sibme-development',
            'Prefix' => 'uploads/',
            'Delimiter' => "/",
        );
//$objects = $s3Client->getListObjectsIterator();

        $objects = $s3Client->listObjects($newarr);

//print_r($objects); die;

        $accounts = array();


        foreach ($objects['CommonPrefixes'] as $object) {
            echo '<pre>';
            print_r($object['Prefix']);

            $accounts[] = $object['Prefix'];
        }


        $account_huddles = array();

        foreach ($accounts as $account) {
            $newarr = array(
                'Bucket' => 'sibme-development',
                'Prefix' => $account,
                'Delimiter' => "/",
            );

            $result = $this->Document->query("SELECT * from documents where url like '%" . $account . "%'");

//echo mysqli_num_rows($result);
            if (count($result) > 0) {

            } else {
                $xml = "<ArchiveExFolder><path>" . $account . "</path></ArchiveExFolder>";
                $sql = $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '" . date("Y-m-d H:i:s") . "', NULL, NULL, '" . $xml . "', '1', '0', NULL)");
            }

            $objects = $s3Client->listObjects($newarr);


            if (!empty($objects['CommonPrefixes'])) {
                foreach ($objects['CommonPrefixes'] as $object) {
                    echo '<pre>';
                    print_r($object['Prefix']);

                    $account_huddles[] = $object['Prefix'];
                }
            }
        }

        $with_years = array();

        foreach ($account_huddles as $account_huddle) {
            $newarr = array(
                'Bucket' => 'sibme-development',
                'Prefix' => $account_huddle,
                'Delimiter' => "/",
            );


            $result = $this->Document->query("SELECT * from documents where url like '%" . $account_huddle . "%'");
//echo mysqli_num_rows($result);
            if (count($result) > 0) {

            } else {
                $xml = "<ArchiveExFolder><path>" . $account_huddle . "</path></ArchiveExFolder>";
                $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '" . date("Y-m-d H:i:s") . "', NULL, NULL, '" . $xml . "', '1', '0', NULL)");
            }

            $objects = $s3Client->listObjects($newarr);


            if (!empty($objects['CommonPrefixes'])) {
                foreach ($objects['CommonPrefixes'] as $object) {
                    echo '<pre>';
                    print_r($object['Prefix']);

                    $with_years[] = $object['Prefix'];
                }
            }
        }

        $with_months = array();

        foreach ($with_years as $with_year) {
            $newarr = array(
                'Bucket' => 'sibme-development',
                'Prefix' => $with_year,
                'Delimiter' => "/",
            );

            $result = $this->Document->query("SELECT * from documents where url like '%" . $with_year . "%' AND site_id=" . $this->site_id);
//echo mysqli_num_rows($result);
            if (count($result) > 0) {

            } else {
                $xml = "<ArchiveExFolder><path>" . $with_year . "</path></ArchiveExFolder>";
                $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '" . date("Y-m-d H:i:s") . "', NULL, NULL, '" . $xml . "', '1', '0', NULL)");
            }


            $objects = $s3Client->listObjects($newarr);


            if (!empty($objects['CommonPrefixes'])) {
                foreach ($objects['CommonPrefixes'] as $object) {
                    echo '<pre>';
                    print_r($object['Prefix']);

                    $with_months[] = $object['Prefix'];
                }
            }
        }



        $with_dates = array();

        foreach ($with_months as $with_month) {
            $newarr = array(
                'Bucket' => 'sibme-development',
                'Prefix' => $with_month,
                'Delimiter' => "/",
            );

            $result = $this->Document->query("SELECT * from documents where url like '%" . $with_month . "%' AND site_id=" . $this->site_id);
//echo mysqli_num_rows($result);
            if (count($result) > 0) {

            } else {
                $xml = "<ArchiveExFolder><path>" . $with_month . "</path></ArchiveExFolder>";
                $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '" . date("Y-m-d H:i:s") . "', NULL, NULL, '" . $xml . "', '1', '0', NULL)");
            }

            $objects = $s3Client->listObjects($newarr);


            if (!empty($objects['CommonPrefixes'])) {
                foreach ($objects['CommonPrefixes'] as $object) {
                    echo '<pre>';
                    print_r($object['Prefix']);

                    $with_dates[] = $object['Prefix'];
                }
            }
        }


        foreach ($with_dates as $with_date) {

            $result = $this->Document->query("SELECT * from documents where url like '%" . $with_date . "%' AND site_id=" . $this->site_id);
//echo mysqli_num_rows($result);
            if (count($result) > 0) {

            } else {
                $xml = "<ArchiveExFolder><path>" . $with_date . "</path></ArchiveExFolder>";
                $this->Document->query("INSERT INTO `JobQueue` (`JobQueueId`, `JobId`, `CreateDate`, `RunStart`, `RunEnd`, `RequestXml`, `JobQueueStatusId`, `CurrentRetry`, `AutoRetryCount`) VALUES (NULL, '6', '" . date("Y-m-d H:i:s") . "', NULL, NULL, '" . $xml . "', '1', '0', NULL)");
            }
        }


        die;
    }

    function add_account_framework_settings($account_id) {
        $messages = array();
        $view = new View($this, false);
        if (!empty($account_id)):
            $account_tags_type_2 = $this->AccountTag->find('all', array(
                'conditions' => array(
                    'AccountTag.tag_type' => 2,
                    'AccountTag.account_id' => $account_id,
                ),
                'fields' => array('AccountTag.*')
                    )
            );
            if (!empty($account_tags_type_2)):
                foreach ($account_tags_type_2 as $account_tag):
                    $account_tag_id_exists = $this->AccountFrameworkSetting->find('first', array(
                        'conditions' => array(
                            'AccountFrameworkSetting.account_tag_id' => $account_tag['AccountTag']['account_tag_id'],
                        ),
                        'fields' => array('AccountFrameworkSetting.*')
                            )
                    );
                    if (empty($account_tag_id_exists)) {
                        $account_tags_type_0 = $this->AccountTag->find('all', array(
                            'conditions' => array(
                                'AccountTag.tag_type' => 0,
                                'AccountTag.framework_id' => $account_tag['AccountTag']['account_tag_id'],
                            ),
                            'fields' => array('AccountTag.*')
                                )
                        );
                        $is_not_null = false;
                        if (!empty($account_tags_type_0)):
                            foreach ($account_tags_type_0 as $account_tag_0):
                                if (!empty($account_tag_0['AccountTag']['parent_account_tag_id'])):
                                    $is_not_null = true;
                                    if (!empty($account_tag_0['AccountTag']['tag_title'])) {
                                        $this->AccountTag->updateAll(
                                                array(
                                            'tag_code' => NULL,
                                            'tag_html' => "'" . Sanitize::escape(strip_tags($account_tag_0['AccountTag']['tag_code'] . ' ' . $account_tag_0['AccountTag']['tag_html'])) . "'",
                                            'tag_title' => "'" . Sanitize::escape(strip_tags($account_tag_0['AccountTag']['tag_code'] . ' ' . $account_tag_0['AccountTag']['tag_title'])) . "'",
                                            'standard_level' => "2",
                                            'framework_hierarchy' => "'" . "/" . $account_tag_0['AccountTag']['parent_account_tag_id'] . "/" . $account_tag_0['AccountTag']['account_tag_id'] . "'"), array('account_tag_id' => $account_tag_0['AccountTag']['account_tag_id'])
                                        );
                                    } else {
                                        $this->AccountTag->updateAll(
                                                array(
                                            'tag_code' => NULL,
                                            'tag_html' => "'" . Sanitize::escape(strip_tags($account_tag_0['AccountTag']['tag_code'] . ' ' . $account_tag_0['AccountTag']['tag_html'])) . "'",
                                            'standard_level' => "2",
                                            'framework_hierarchy' => "'" . "/" . $account_tag_0['AccountTag']['parent_account_tag_id'] . "/" . $account_tag_0['AccountTag']['account_tag_id'] . "'"), array('account_tag_id' => $account_tag_0['AccountTag']['account_tag_id'])
                                        );
                                    }
                                else:
                                    $this->AccountTag->updateAll(
                                            array(
                                        'tag_code' => NULL,
                                        'tag_title' => "'" . Sanitize::escape(strip_tags($account_tag_0['AccountTag']['tag_code'] . ' ' . $account_tag_0['AccountTag']['tag_title'])) . "'",
                                        'standard_level' => "1",
                                        'framework_hierarchy' => "'" . "/" . $account_tag_0['AccountTag']['parent_account_tag_id'] . "/" . $account_tag_0['AccountTag']['account_tag_id'] . "'"), array('account_tag_id' => $account_tag_0['AccountTag']['account_tag_id'])
                                    );

                                endif;
                            endforeach;
                        endif;
                        $this->AccountFrameworkSetting->create();
                        $data = array(
                            'account_id' => $account_id,
                            'account_tag_id' => $account_tag['AccountTag']['account_tag_id'],
                            'framework_name' => $account_tag['AccountTag']['tag_title'],
                            'tier_level' => $is_not_null ? "2" : "1",
                            'checkbox_level' => $is_not_null ? "2" : "1",
                            'published_at' => date("Y-m-d h:i:s"),
                            'updated_at' => date("Y-m-d h:i:s"),
                            'published' => '1'
                        );

                        if ($is_not_null) {
                            $data['framework_sample'] = htmlentities('<div class="f-group L1" data-level="1"><div class="f-item pre">1.1</div><div class="f-item content">new item</div></div><div class="f-group L2" data-level="2"><input type="checkbox" class="form-check-input cbox"><div class="f-item pre">1.2</div><div class="f-item content">new item</div></div>');
                        } else {
                            $data['framework_sample'] = htmlentities('<div class="f-group L1" data-level="1"><input type="checkbox" class="form-check-input cbox"><div class="f-item pre">1.1</div><div class="f-item content">new item</div></div>');
                        }


                        if ($this->AccountFrameworkSetting->save($data)) {
                            array_push($messages, $view->Custom->parse_translation_params($this->language_based_messages['settings_for_account_tag_are_saved_msg'], ['account_tag_id' => $account_tag['AccountTag']['account_tag_id']]));
                        } else {
                            array_push($messages, $view->Custom->parse_translation_params($this->language_based_messages['settings_for_account_tag_failed_msg'], ['account_tag_id' => $account_tag['AccountTag']['account_tag_id']]));
                        }
                    }
                endforeach;
            else:
                array_push($messages, $this->language_based_messages['nothing_found_against_id_msg']);
            endif;
        else:
            array_push($messages, $this->language_based_messages['please_provide_valid_account_id_msg']);
        endif;
        return true;
    }

    function get_framework_settings($account_tag_id) {
        if (!empty($account_tag_id)):
            $framework_settings = array();
            $acc_tag_type_0 = $this->AccountTag->find('all', array(
                'conditions' => array(
                    'AccountTag.framework_id' => $account_tag_id,
                    'AccountTag.tag_type' => 0
                ),
                'fields' => array('AccountTag.*')
                    )
            );
            $framework_settings['account_tag_type_0'] = $acc_tag_type_0;
            $account_framework_settings = $this->AccountFrameworkSetting->find('first', array(
                'conditions' => array(
                    'AccountFrameworkSetting.account_tag_id' => $account_tag_id,
                ),
                'fields' => array('AccountFrameworkSetting.*')
                    )
            );
            $framework_settings['account_framework_settings'] = $account_framework_settings;
            if (!empty($account_framework_settings)) {
                $account_framework_settings_performance_levels = $this->AccountFrameworkSettingPerformanceLevel->find('all', array(
                    'conditions' => array(
                        'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $account_framework_settings['AccountFrameworkSetting']['id'],
                    ),
                    'fields' => array('AccountFrameworkSettingPerformanceLevel.*')
                        )
                );
                if (!empty($framework_settings['account_tag_type_0'])) {
                    $single_type_0_array = array();
                    foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                        if ($single_acc_tag_type_0['AccountTag']['standard_level'] == $account_framework_settings['AccountFrameworkSetting']['checkbox_level']) {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                        } else {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = array();
                        }
                        $single_type_0_array[] = $single_acc_tag_type_0;
                    endforeach;
                    $framework_settings['account_tag_type_0'] = $single_type_0_array;
                }
//                $framework_settings['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                if ($account_framework_settings['AccountFrameworkSetting']['enable_unique_desc'] == 1) {
                    $acc_tag_type_0_tag_ids = array();
                    if (!empty($acc_tag_type_0)) {
                        foreach ($acc_tag_type_0 as $acc_tag_type_0_single):
                            array_push($acc_tag_type_0_tag_ids, $acc_tag_type_0_single['AccountTag']['account_tag_id']);
                        endforeach;
                    }
                    if (!empty($acc_tag_type_0_tag_ids)) {
                        $account_framework_settings_performance_levels_ids = array();
                        if (!empty($account_framework_settings_performance_levels)) {
                            foreach ($account_framework_settings_performance_levels as $single_performance_level):
                                array_push($account_framework_settings_performance_levels_ids, $single_performance_level['AccountFrameworkSettingPerformanceLevel']['id']);
                            endforeach;
                        }
                        $performance_level_descriptions = $this->PerformanceLevelDescription->find('all', array(
                            'conditions' => array(
                                'PerformanceLevelDescription.performance_level_id' => $account_framework_settings_performance_levels_ids,
                                'PerformanceLevelDescription.account_tag_id' => $acc_tag_type_0_tag_ids,
                            ),
                            'fields' => array('PerformanceLevelDescription.*')
                                )
                        );
                        if (!empty($framework_settings['account_tag_type_0']) && !empty($performance_level_descriptions)) {
                            $single_type_0_array = array();
                            foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                                $performance_level_descriptions_arr = array();
                                foreach ($performance_level_descriptions as $single_performance_level_description):
                                    if ($single_acc_tag_type_0['AccountTag']['standard_level'] == $account_framework_settings['AccountFrameworkSetting']['checkbox_level'] && $single_acc_tag_type_0['AccountTag']['account_tag_id'] == $single_performance_level_description['PerformanceLevelDescription']['account_tag_id']) {
                                        array_push($performance_level_descriptions_arr, $single_performance_level_description);
                                    }
                                endforeach;
                                $single_acc_tag_type_0['performance_level_descriptions'] = $performance_level_descriptions_arr;
                                $single_type_0_array[] = $single_acc_tag_type_0;
                            endforeach;
                            $framework_settings['account_tag_type_0'] = $single_type_0_array;
                        }
//                        $framework_settings['performance_level_descriptions'] = $performance_level_descriptions;
                    }
                }
            }
            if (!empty($account_framework_settings)) {
                $result['status'] = true;
                $result['msg'] = "Account framework settings fetched successfully.";
                $result['data'] = $account_framework_settings;
            } else {
                $result['status'] = true;
                $result['msg'] = "Nothing found for your request.";
            }
        else:
            $result['status'] = false;
            $result['msg'] = "Please provide a valid account tag id.";
        endif;
        echo json_encode($result);
        die;
    }

    function uploadDocumentsMobile($account_folder_id, $account_id, $user_id, $video_id = '', $url_stack = '', $workspace = false) {
        $view = new View($this, false);
        if ($this->request->is('post')) {





            if ($url_stack == 1) {
                $url_stack = $this->request->data['url'];
            }
            $previous_storage_used = $this->Account->getPreviousStorageUsed($account_id);
            $url = "$account_id/$account_folder_id/" . date('Y/m/d') . "/" . $this->request->data['video_url'];

            $path_parts = pathinfo($this->request->data['video_url']);
            $video_file_name = $path_parts['filename'] . "." . $path_parts['extension'];

            $path_parts_orig = pathinfo($this->request->data['video_file_name']);
            $request_dump = addslashes(print_r($_POST, true));
//$url_stack = explode('/',$url_stack );
            $this->Document->create();
            $data = array(
                'account_id' => $account_id,
                'doc_type' => '2',
                'url' => $url,
                'original_file_name' => $this->request->data['video_file_name'],
                'active' => 1,
                'created_date' => date("Y-m-d h:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d h:i:s"),
                'last_edit_by' => $user_id,
                'published' => '1',
                'stack_url' => $url_stack,
                'post_rubric_per_video' => '1'
            );


            if ($this->Document->save($data, $validation = TRUE)) {

                $document_id = $this->Document->id;

                $folder_type = $this->get_folder_type($account_folder_id);
                if ($folder_type == '1') {
                    $url = $this->base . '/Huddles/view/' . $account_folder_id . '/2';
                } elseif ($folder_type == '3') {
                    $url = $this->base . '/MyFiles/view/2';
                } else {
                    $url = $this->base . '/videoLibrary/view/' . $account_folder_id;
                }
                if ($view->Custom->check_if_eval_huddle($account_folder_id)) {
                    $url = $this->base . '/Huddles/view/' . $account_folder_id . '/1/' . $video_id;
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $url,
                        'type' => '3',
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                } else {
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $url,
                        'type' => '3',
                        // 'account_folder_id' => ($video_id != '' && $video_id != 'undefined') ? $video_id : $account_folder_id,
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                }


                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

                require('src/services.php');
//uploaded video
                $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                $s3_src_folder_path = $this->request->data['video_url'];

//echo $uploaded_video_file; die;
                $s3_service = new S3Services(
                        Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                );
                $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);

                $this->Document->create();
                $this->Document->updateAll(
                        array(
                    'zencoder_output_id' => 0,
                    'url' => "'" . addslashes($s3_dest_folder_path) . "'",
                    'file_size' => (int) $this->request->data['video_file_size'],
                    'request_dump' => "'" . $request_dump . "'"
                        ), array(
                    'id' => $document_id
                        )
                );

                if ($previous_storage_used['Account']['storage_used'] != '') {
                    $storage_in_used = $this->request->data['video_file_size'] + $previous_storage_used['Account']['storage_used'];
                } else {
                    $storage_in_used = $this->request->data['video_file_size'];
                }

                $this->Account->updateAll(array('storage_used' => $storage_in_used), array('id' => $account_id));
                $this->job_queue_storage_function($account_id, '4');
                $this->AccountFolderDocument->create();
                $data = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $document_id,
                    'title' => $path_parts_orig['filename'],
                    'desc' => $this->request->data['video_desc']
                );
                $this->AccountFolderDocument->save($data, $validation = TRUE);

                if (!empty($video_id) && $video_id != "undefined") {
                    $account_folder_document_id = $this->AccountFolderDocument->id;
                    $this->AccountFolderDocumentAttachment->create();
                    $data = array(
                        'account_folder_document_id' => $account_folder_document_id,
                        'attach_id' => "$video_id"
                    );
                    $this->AccountFolderDocumentAttachment->save($data, $validation = TRUE);
                }

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
                if ($view->Custom->check_if_eval_huddle($account_folder_id)) {
                    $documentData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'video_link' => $this->base . '/Huddles/view/' . $account_folder_id . '/1/' . $video_id,
                        'participating_users' => $huddleUsers
                    );
                } else {
                    $documentData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'video_link' => '/Huddles/view/' . $account_folder_id . '/2/',
                        'participating_users' => $huddleUsers
                    );
                }

                if ($view->Custom->check_if_eval_huddle($account_folder_id) == 1) {
                    $all_participants = array();
                    if ($huddleUsers) {
                        foreach ($huddleUsers as $usre) {
                            if ($usre['huddle_users']['role_id'] == 210) {
                                $all_participants[] = $usre['huddle_users']['user_id'];
                            }
                        }
                    }
                }

                $video_details = $view->Custom->get_video_details($video_id);
                $vide_created_by = $video_details['Document']['created_by'];

                foreach ($huddleUsers as $row) {
                    if ($row['User']['id'] == $user_id) {
                        continue;
                    }
                    if ($view->Custom->check_if_eval_huddle($account_folder_id) == 1) {
                        if ($view->Custom->check_if_evaluated_participant($account_folder_id, $user_id)) {
                            if (isset($all_participants) && is_array($all_participants) && in_array($row['User']['id'], $all_participants)) {
                                continue;
                            }
                        } else {

                            if ($vide_created_by != $row['User']['id']) {
                                continue;
                            }
                        }
                        $documentData['huddle_type'] = 3;
                    } else {
                        $documentData['huddle_type'] = '';
                    }
                    $documentData['email'] = $row['User']['email'];
                    $documentData['user_id'] = $row['User']['id'];
                    if ($this->check_subscription($row['User']['id'], '1', $account_id)) {
                        $this->sendDocumentEmail($documentData);
                    }
                }
            } else {
                $document_id = 0;
                $s_url = '';
            }


            $res = array(
                'document_id' => $document_id,
                'url' => $s_url
            );

            return $res;
        } else {
            $this->set('huddle_id', $account_folder_id);
            $this->set('upload_type', 'doc');
            $this->layout = 'fileupload';
            $this->render('uploadVideos');
        }
    }

    function multipleuploadDocumentsMobile($account_folder_id, $account_id, $user_id, $video_id = '', $url_stack = '', $workspace = false, $video_url, $video_file_name, $video_desc, $video_file_size,$discussion_bool = true,$assessment_sample = '0',$slot_index = '0') {
        $view = new View($this, false);
        $this->request->data['video_url'] = $video_url;
        $this->request->data['video_file_name'] = $video_file_name;
        $this->request->data['video_file_size'] = $video_file_size;
        $this->request->data['video_desc'] = $video_desc;
        $parent_folder_id = isset($this->request->data['parent_folder_id']) ? $this->request->data['parent_folder_id'] : 0;


        if ($this->request->is('post')) {

            if ($url_stack == 1) {
                $url_stack = $this->request->data['url'];
            }
            $previous_storage_used = $this->Account->getPreviousStorageUsed($account_id);
            $url = "$account_id/$account_folder_id/" . date('Y/m/d') . "/" . $this->request->data['video_url'];

            $path_parts = pathinfo($this->request->data['video_url']);
            $video_file_name = $path_parts['filename'] . "." . $path_parts['extension'];

            $path_parts_orig = pathinfo($this->request->data['video_file_name']);
            $request_dump = addslashes(print_r($_POST, true));
//$url_stack = explode('/',$url_stack );
            $this->Document->create();
            $data = array(
                'account_id' => $account_id,
                'doc_type' => '2',
                'url' => $url,
                'original_file_name' => $this->request->data['video_file_name'],
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
                'published' => '1',
                'stack_url' => $url_stack,
                'parent_folder_id' => $parent_folder_id,
                'post_rubric_per_video' => '1'
            );


            if ($this->Document->save($data, $validation = TRUE)) {
                $document_id = $this->Document->id;
                /**generate resources thumbnails using files stack start (irfan shah)*/
                $stack_array = explode('/',$url_stack );
                $filestack_id = end($stack_array);
                $extension = pathinfo($this->request->data['video_file_name'], PATHINFO_EXTENSION);
                $s3_dest_folder_path_only = "uploads/" .$account_id."/".$account_folder_id."/" . date('Y') . "/" . date('m') . "/" . date('d');

                $data = $view->Custom->sendHttpRequest("generate_resources_thumbnail_cake", ["doc_type" => 2,"file_Stack_id"=>$filestack_id,'extension' => $extension,'s3_path'=>$s3_dest_folder_path_only]);
                $data = json_decode($data);
                if($data->success!==false)
                {
                    $image = $filestack_id.".jpg";
                    $thumbnail_url = "'$s3_dest_folder_path_only/$image'";
                    $this->Document->create();
                    $this->Document->updateAll(['s3_thumbnail_url' => $thumbnail_url],['id' => $document_id]);
                }
                /*End resources thumbnails*/
                if($account_folder_id)
                {
                    $huddle_update_data = array(
                        'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                        'last_edit_by' => $user_id,
                    );
                    $this->AccountFolder->updateAll($huddle_update_data, array('account_folder_id' => $account_folder_id), $validation = true);
                }
                $folder_type = $this->get_folder_type($account_folder_id);
                if ($folder_type == '1') {
                    $url = $this->base . '/Huddles/view/' . $account_folder_id . '/2';
                    $user_email_info = $this->User->find('first', array('conditions' => array('id' => $user_id)));
                    $this->create_churnzero_event('Huddle+Resource+Uploaded', $account_id, $user_email_info['User']['email']); 
                } elseif ($folder_type == '3') {
                    $url = $this->base . '/MyFiles/view/2';
                    $user_email_info = $this->User->find('first', array('conditions' => array('id' => $user_id)));
                    $this->create_churnzero_event('Workspace+Resource+Uploaded', $account_id, $user_email_info['User']['email']);
                } else {
                    $url = $this->base . '/videoLibrary/view/' . $account_folder_id;
                }
                if ($view->Custom->check_if_eval_huddle($account_folder_id)) {
                    $url = $this->base . '/Huddles/view/' . $account_folder_id . '/1/' . $video_id;
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $url,
                        'type' => '3',
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                } else {
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $this->request->data['video_file_name'],
                        'url' => $url,
                        'type' => '3',
                        // 'account_folder_id' => ($video_id != '' && $video_id != 'undefined') ? $video_id : $account_folder_id,
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                }


                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

                require_once('src/services.php');
//uploaded video
                $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                $s3_src_folder_path = $this->request->data['video_url'];

//echo $uploaded_video_file; die;
                $s3_service = new S3Services(
                        Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                );
                $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);

                $this->Document->create();
                $this->Document->updateAll(
                        array(
                    'zencoder_output_id' => 0,
                    'url' => "'" . addslashes($s3_dest_folder_path) . "'",
                    'file_size' => (int) $this->request->data['video_file_size'],
                    'request_dump' => "'" . $request_dump . "'"
                        ), array(
                    'id' => $document_id
                        )
                );

                if ($previous_storage_used['Account']['storage_used'] != '') {
                    $storage_in_used = $this->request->data['video_file_size'] + $previous_storage_used['Account']['storage_used'];
                } else {
                    $storage_in_used = $this->request->data['video_file_size'];
                }

                $this->Account->updateAll(array('storage_used' => $storage_in_used), array('id' => $account_id));
                $this->job_queue_storage_function($account_id, '4');
                $this->AccountFolderDocument->create();
                $data = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $document_id,
                    'title' => $path_parts_orig['filename'],
                    'desc' => $this->request->data['video_desc'],
                    'assessment_sample' => $assessment_sample,
                    'slot_index' => $slot_index
                );
                if($discussion_bool)
                {
                $this->AccountFolderDocument->save($data, $validation = TRUE);
                }
                if (!empty($video_id) && $video_id != "undefined") {
                    $account_folder_document_id = $this->AccountFolderDocument->id;
                    $this->AccountFolderDocumentAttachment->create();
                    $data = array(
                        'account_folder_document_id' => $account_folder_document_id,
                        'attach_id' => "$video_id"
                    );
                    $this->AccountFolderDocumentAttachment->save($data, $validation = TRUE);
                }



                /////////////////////////////////

                // $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                // $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);


                // if ($view->Custom->check_if_eval_huddle($account_folder_id)) {
                //     $documentData = array(
                //         'account_folder_id' => $account_folder_id,
                //         'huddle_name' => $huddle[0]['AccountFolder']['name'],
                //         'video_link' => $this->base . '/Huddles/view/' . $account_folder_id . '/1/' . $video_id,
                //         'participating_users' => $huddleUsers
                //     );
                // } else {
                //     $documentData = array(
                //         'account_folder_id' => $account_folder_id,
                //         'huddle_name' => $huddle[0]['AccountFolder']['name'],
                //         'video_link' => '/Huddles/view/' . $account_folder_id . '/2/',
                //         'participating_users' => $huddleUsers
                //     );
                // }

                // if ($view->Custom->check_if_eval_huddle($account_folder_id) == 1) {
                //     $all_participants = array();
                //     if ($huddleUsers) {
                //         foreach ($huddleUsers as $usre) {
                //             if ($usre['huddle_users']['role_id'] == 210) {
                //                 $all_participants[] = $usre['huddle_users']['user_id'];
                //             }
                //         }
                //     }
                // }

                // $video_details = $view->Custom->get_video_details($video_id);
                // $vide_created_by = $video_details['Document']['created_by'];



                //send email job to queues thoug lumen

                //QueuesManager::sendToEmailQueue(__CLASS__, "send_emails_to_queue", $huddleUsers, $user_id, $account_id, $account_folder_id, $site_id, $all_participants, $vide_created_by, $documentData);
                //'huddleUsers' => $huddleUsers,'all_participants' => $all_participants, 'vide_created_by' => $vide_created_by, 'documentData' =>$documentData
                $view->Custom->sendHttpRequest("cake_multiple_upload_documents_mobile_emails_to_queues", [ 'user_id' => $user_id, 'account_id' => $account_id, 'account_folder_id' => $account_folder_id, 'video_id'=> $video_id , 'site_id' => $this->site_id]);
                // print_r($response);
                // die;

                // foreach ($huddleUsers as $row) {
                //     if ($row['User']['id'] == $user_id) {
                //         continue;
                //     }
                //     if ($view->Custom->check_if_eval_huddle($account_folder_id) == 1) {
                //         if ($view->Custom->check_if_evaluated_participant($account_folder_id, $user_id)) {
                //             if (isset($all_participants) && is_array($all_participants) && in_array($row['User']['id'], $all_participants)) {
                //                 continue;
                //             }
                //         } else {

                //             if ($vide_created_by != $row['User']['id']) {
                //                 continue;
                //             }
                //         }
                //         $documentData['huddle_type'] = 3;
                //     } else {
                //         $documentData['huddle_type'] = '';
                //     }
                //     $documentData['email'] = $row['User']['email'];
                //     $documentData['user_id'] = $row['User']['id'];
                //     if ($this->check_subscription($row['User']['id'], '1', $account_id)) {
                //       //  $this->sendDocumentEmail($documentData);
                //     }
                // }


            } else {
                $document_id = 0;
                $s_url = '';
            }


            $res = array(
                'document_id' => $document_id,
                'url' => $s_url
            );

            return $res;
        } else {
            $this->set('huddle_id', $account_folder_id);
            $this->set('upload_type', 'doc');
            $this->layout = 'fileupload';
            $this->render('uploadVideos');
        }
    }

    function account_frameworks_settings() {
        $account_tags = $this->AccountTag->query('select * from accounts where is_active =1 and site_id=' . $this->site_id);
        if ($account_tags) {
            foreach ($account_tags as $row) {
                $account_id = $row['accounts']['id'];
                $this->add_account_framework_settings($account_id);
            }
            echo "Total migrated Accounts " . count($account_tags);
            die;
        } else {
            echo "All Accounts Migrated successfully!";
            die;
        }
    }

    function get_document_url_for_lti($document_video) {

        App::import("Model", "DocumentFiles");
        $db = new DocumentFiles();
        $videoFilePath = pathinfo($document_video['url']);
        $videoFileExtension = $videoFilePath['extension'];
        if ($document_video['published'] == 0) {

            return array(
                'url' => $document_video['url'],
                'thumbnail' => '',
                'encoder_status' => $document_video['encoder_status']
            );
        }

        $check_data = $db->find("first", array("conditions" => array(
                "document_id" => $document_video['id'],
                "default_web" => true
        )));

        if (isset($check_data) && isset($check_data['DocumentFiles']['url'])) {

            $url = $check_data['DocumentFiles']['url'];
            $duration = $check_data['DocumentFiles']['duration'];
            $thumbnail = '';
            $videoFilePath = pathinfo($url);
            $videoFileName = $videoFilePath['filename'];
//these are old files before JobQueue
            if ($check_data['DocumentFiles']['transcoding_status'] == '-1') {

                $videoFilePathThumb = pathinfo($document_video['url']);
                $videoFileNameThumb = $videoFilePathThumb['filename'];
            } else {
                $videoFileNameThumb = $videoFileName;
            }


            $poster_end_extension = "_thumb.png";
            if ($document_video['encoder_provider'] == '2') {
                $poster_end_extension = empty($document_video['thumbnail_number']) ?
                        "_thumb_00001.png" :
                        '_thumb_' . sprintf('%05d', $document_video['thumbnail_number']) . '.png';
            }

            $thumbnail_image_path = '';
            if (Configure::read('use_cloudfront') == true) {
                if ($check_data['DocumentFiles']['transcoding_status'] == '2') {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrlForLti('app/img/video-thumbnail.png');
                } else {
                    if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                        $thumbnail_image_path = $this->getSecureAmazonSibmeUrlForLti('app/img/audio-thumbnail.png');
                    } else {
                        if ($videoFilePath['dirname'] == ".")
                            $thumbnail_image_path = $this->getSecureAmazonUrlForLti(($videoFileNameThumb) . "$poster_end_extension");
                        else
                            $thumbnail_image_path = $this->getSecureAmazonUrlForLti($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                    }
                }
            } else {
                if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrlForLti('app/img/audio-thumbnail.png');
                } else {
                    if ($videoFilePath['dirname'] == ".")
                        $thumbnail_image_path = $this->getSecureAmazonUrlForLti(($videoFileNameThumb) . "$poster_end_extension");
                    else
                        $thumbnail_image_path = $this->getSecureAmazonUrlForLti($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                }
            }

            if ($thumbnail_image_path == '')
                $thumbnail_image_path = $this->getSecureAmazonSibmeUrlForLti('app/img/video-thumbnail.png');

            $video_path = '';
            $relative_video_path = '';

            if (Configure::read('use_cloudfront') == true) {

                if ($videoFilePath['dirname'] == ".") {

                    $video_path = $this->getSecureAmazonCloudFrontUrlForLti(urlencode($videoFileName) . ".mp4");
                    $relative_video_path = $videoFileName . ".mp4";
                } else {

                    $video_path = $this->getSecureAmazonCloudFrontUrlForLti($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".mp4");
                    $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                }
            } else {

                if ($videoFilePath['dirname'] == ".") {

                    $video_path = $this->getSecureAmazonUrlForLti(($videoFileName) . ".mp4");
                    $relative_video_path = $videoFileName . ".mp4";
                } else {

                    $video_path = $this->getSecureAmazonUrlForLti($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp4");
                    $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                }
            }

            return array(
                'url' => $video_path,
                'relative_url' => $relative_video_path,
                'thumbnail' => $thumbnail_image_path,
                'duration' => $duration,
                'encoder_status' => (isset($document_video['encoder_status']) ? $document_video['encoder_status'] : '')
            );
        } else {
//todo
//through the exception here
            return array(
                'url' => '',
                'relative_url' => '',
                'thumbnail' => '',
                'duration' => '',
                'encoder_status' => 'Error'
            );
        }
    }

    function getSecureAmazonSibmeUrlForLti($file, $name = null) {
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name_cdn');
        $expires = '+525600 minutes';

// Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

        $url = $client->getObjectUrl($aws_bucket, $file, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file)),
        ));

        return $url;
    }

    function getSecureAmazonUrlForLti($file, $name = null) {
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name');
        $expires = '+525600 minutes';

// Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

        $url = $client->getObjectUrl($aws_bucket, $file, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file)),
        ));

        return $url;
    }

    function getSecureAmazonCloudFrontUrlForLti($resource, $name = '', $timeout = 600) {

//This comes from key pair you generated for cloudfront
        $keyPairId = Configure::read('cloudfront_keypair');

//Read Cloudfront Private Key Pair
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => realpath(dirname(__FILE__) . '/..') . "/Config/cloudfront_key.pem",
                    'key_pair_id' => $keyPairId,
        ));

        $streamHostUrl = Configure::read('cloudfront_host_url');
        $resourceKey = $resource;
        $expires = time() + 525600;

//Construct the URL
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));

        return $signedUrlCannedPolicy;
    }

    function save_to_s3($filename) {
        $path_parts = pathinfo($filename);
        $bucket_name = Configure::read('bucket_name');
        $new_destination = $path_parts['dirname'] . '/' . $path_parts['filename'] . '.' . $path_parts['extension'];
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
//        $arr = [
//            'version' => 'latest',
//            'region' => 'us-east-1',
//            'credentials' => [
//                'key' => 'AKIAJ4ZWDR5X5JKB7CZQ',
//                'secret' => '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',
//            ],
//            'http' => [
//                'verify' => false
//            ]
//        ];
//        $s3Client = S3Client::factory($arr);
//    $s3Client->putObject(array(
//    'Bucket'     => 'sibme-development',
//    'Key'        => 'uploads/EdtpaZipFiles/5819_2018-03-14_1521064636',
//    'SourceFile' => 'EdtpaZipFiles/5819_2018-03-14_1521064630/5819_2018-03-14_1521064636.zip'
//));
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));
        $client->putObject(array(
            'Bucket' => $bucket_name,
            'Key' => 'uploads/' . $new_destination,
            'SourceFile' => $filename
        ));
    }

    function retrieve_from_s3($filename) {

        $path_parts = pathinfo($filename);
        $bucket_name = Configure::read('bucket_name');
        $key = $path_parts['dirname'] . '/' . $path_parts['filename'] . '.' . $path_parts['extension'];

// Instantiate the client.
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');


        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));


        $zip_file_name = $client->getObject(array(
            'Bucket' => $bucket_name,
            'Key' => 'uploads/' . $filename
        ));


        header('Content-disposition: filename="documents.zip"');
        header("Content-Type: {$zip_file_name['ContentType']}");
        echo $zip_file_name['Body'];

        die;
    }

    function edTPA_assessments_transfer() {
        $result = $this->EdtpaAssessmentAccountCandidates->find('all', array(
            'conditions' => array(
                'EdtpaAssessmentAccountCandidates.status' => 3
        )));

        if ($result) {
            foreach ($result as $row) {
                $candidate_id = $row['EdtpaAssessmentAccountCandidates']['id'];
                $resut_tramsfer = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array(
                    'conditions' => array(
                        'EdtpaAssessmentAccountCandidateTransfers.zip_picked_up' => 0,
                        'EdtpaAssessmentAccountCandidateTransfers.edtpa_assessment_account_candidate_id' => $candidate_id
                    ),
                    'order' => 'EdtpaAssessmentAccountCandidateTransfers.id DESC'
                ));

                $transfer_id = $resut_tramsfer['EdtpaAssessmentAccountCandidateTransfers']['id'];

                if (!empty($transfer_id)) {
                    $this->assessment_zipfile_maker($transfer_id);
                }
            }
        }
        die;
    }

    function Portfolio_Package_Retrieval_Notification() {
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = "tahir@jjtestsite.us";
        //$this->Email->to = "khurri.saleem@gmail.com";
        $this->Email->subject = "Package here from notification";
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $html = addslashes(print_r($_REQUEST, true));
        $notification_data = json_decode($_REQUEST['notifications']);
        $this->Email->send($html);
        if (!empty($_REQUEST) && isset($notification_data->partnerTransferRequestStatusList)) {
            foreach ($notification_data->partnerTransferRequestStatusList as $row) {
                $partnerTransferRequestID = $row->partnerTransferRequestID;
                $ir_status = $row->status;

                $partnerTransferRequest = $this->EdtpaAssessmentAccountCandidateTransfers->find('first', array(
                    'conditions' => array(
                        'id' => $partnerTransferRequestID
                    ))
                );
                if (!empty($partnerTransferRequest)) {
                    if ($ir_status == 'IR_Success') {
                        $data = array(
                            'status' => '8'
                        );
                        $this->EdtpaAssessmentAccountCandidates->updateAll($data, array(
                            'id' => $partnerTransferRequest['EdtpaAssessmentAccountCandidateTransfers']['edtpa_assessment_account_candidate_id']
                                ), $validation = TRUE);
                    } elseif ($ir_status == 'IR_PA_Pending') {
                        $data = array(
                            'status' => '9'
                        );
                        $this->EdtpaAssessmentAccountCandidates->updateAll($data, array(
                            'id' => $partnerTransferRequest['EdtpaAssessmentAccountCandidateTransfers']['edtpa_assessment_account_candidate_id']
                                ), $validation = TRUE);
                    }
                }
            }
        }else{
            echo "something went wrong with url";
        }
        die;
    }

    function create_intercom_event($event_name, $meta_data, $loggedInUserEmail) {        
        try {
            if($this->site_id == '2')
            {
              $client = new IntercomClient(Configure::read('intercom_access_token_hmh'), null); 
            }
            else
            {
              $client = new IntercomClient(Configure::read('intercom_access_token'), null);  
            }
            

            $client->events->create([
                "event_name" => $event_name,
                "created_at" => time(),
                "email" => $loggedInUserEmail,
                "metadata" => $meta_data
            ]);
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $status_code = $response->getStatusCode();
            return $status_code;
        }
    }

    function check_configuration($return_site_id = false) {
        $view = new View($this, false);
        $site_url = $view->Custom->get_site();
        $result = $this->UserAccount->query("select * from sites where site_url = '$site_url'");
        if($return_site_id)
        {
            return isset($result[0]['sites']['site_id'])?$result[0]['sites']['site_id']:1;
        }
        if (!empty($result[0]['sites']['site_id'])) {
            $this->Session->write('site_id', $result[0]['sites']['site_id']);
        } else {
            if(strpos($site_url, 'hmh')) {
                $this->Session->write('site_id', 2);
            } else {
                $this->Session->write('site_id', 1);                
            }
        }
        return true;
    }

    function insert_workspace_huddle_data() {

        //$fields = array('AccountFolder.account_folder_id');
        $workspace_ids = $this->AccountFolder->query("SELECT
              af.`account_folder_id`
            FROM
              `account_folders` af
              JOIN `account_folder_documents` afd
                ON af.`account_folder_id` = afd.`account_folder_id`
                JOIN documents d ON d.`id` = afd.`document_id`
            WHERE af.folder_type = '3' AND d.`doc_type` IN (1,3) AND af.`active` = '1'
            ");

        foreach ($workspace_ids as $row) {
            $folder_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $row['af']['account_folder_id'], 'meta_data_name' => 'folder_type')));

            if (empty($folder_type)) {
                $account_folders_meta_data = array(
                    'account_folder_id' => $row['af']['account_folder_id'],
                    'meta_data_name' => 'folder_type',
                    'meta_data_value' => '5',
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => '-313', //Unique ID to recognise data added by this code
                    'last_edit_by' => '-313'
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);
                echo 'Data Added for Workspace ID : ' . $row['af']['account_folder_id'] . '<br>';
            }
        }

        die;
    }

    function insert_collaboration_huddle_data() {

        $fields = array('AccountFolder.account_folder_id');
        $huddle_details = $this->AccountFolder->find('all', array(
            'conditions' => array(
                'folder_type' => '1',
                'active' => '1'
            ),
            'fields' => $fields
        ));

        foreach ($huddle_details as $row) {
            $folder_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $row['AccountFolder']['account_folder_id'], 'meta_data_name' => 'folder_type')));

            if (empty($folder_type)) {
                $account_folders_meta_data = array(
                    'account_folder_id' => $row['AccountFolder']['account_folder_id'],
                    'meta_data_name' => 'folder_type',
                    'meta_data_value' => '1',
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => '313', //Unique ID to recognise data added by this code
                    'last_edit_by' => '313'
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);
                echo 'Data Added for Huddle ID : ' . $row['AccountFolder']['account_folder_id'] . '<br>';
            }
        }

        die;
    }
    
    function insert_workspace_old_videos_data() {

       
        $workspace_ids = $this->AccountFolder->query("SELECT
              af.`account_folder_id`
            FROM
              `account_folders` af
              JOIN `account_folder_documents` afd
                ON af.`account_folder_id` = afd.`account_folder_id`
                JOIN documents d ON d.`id` = afd.`document_id`
            WHERE af.folder_type = '3' AND d.`doc_type` IN (1,3) AND af.`active` = '1'
        ");

        foreach ($workspace_ids as $row) {
            $doc_type= '0';
            $document_data = array();
            $account_folder_document_data = array();
            $folder_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $row['af']['account_folder_id'], 'meta_data_name' => 'folder_type')));
            
            $account_folder_document_data = $this->AccountFolderDocument->find('first', array('conditions' => array('account_folder_id' => $row['af']['account_folder_id'])));
            
            if(!empty($account_folder_document_data) && isset($account_folder_document_data['AccountFolderDocument']['document_id']) )
            {
                $document_data = $this->Document->find('first', array('conditions' => array('id' => $account_folder_document_data['AccountFolderDocument']['document_id'])));  
            }
            
            if(!empty($document_data) && isset($document_data['Document']['doc_type']) )
            {
                $doc_type = $document_data['Document']['doc_type'];
            }
            
            if (empty($folder_type) && ($doc_type == '1' || $doc_type == '3' ) ) {
                $account_folders_meta_data = array(
                    'account_folder_id' => $row['af']['account_folder_id'],
                    'meta_data_name' => 'folder_type',
                    'meta_data_value' => '5',
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => '314', //Unique ID to recognise data added by this code
                    'last_edit_by' => '314'
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);
                echo 'Data Added for Workspace ID : ' . $row['af']['account_folder_id'] . '<br>';
            }
        }

        die;
    }

    function add_video_duration_cron() {
        $joins = array(
            array(
                'table' => 'document_files as df',
                'type' => 'left',
                'conditions' => 'Document.id = df.document_id'
            )
        );

        $fields = ('Document.id');

        $doc_type = array(1, 3);
        $videos = $this->Document->find("all", array(
            "conditions" => array("Document.published" => '1', "df.duration" => '0', 'Document.active' => '1', 'Document.doc_type' => $doc_type),
            "joins" => $joins,
            'fields' => $fields
        ));

        foreach ($videos as $key => $video) {

            $duration = '0';

            $ch = curl_init();

            $data = array();

            $sibme_base_url = 'https://app.sibme.com/';
            curl_setopt($ch, CURLOPT_URL, $sibme_base_url . 'app/getVideoDuration/' . $video['Document']['id']);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $duration = (int) $result;


            if ($duration > 0) {

                $updated_data_array = array(
                    'duration' => $duration
                );

                $this->DocumentFiles->updateAll($updated_data_array, array('document_id' => $video['Document']['id']));

                echo 'Duration of Video with ID ' . $video['Document']['id'] . 'is updated' . PHP_EOL;
            }

            echo 'Serial #' . $key . ' Document ID : ' . $video['Document']['id'] . ' Duration : ' . $duration . PHP_EOL;
        }

        die;
    }

    function get_template_contents($key) {
        $result = $this->SendgridTemplates->query("select * from sendgrid_templates where slug = '$key'");
        if (count($result[0]) > 0) {
            $use_template = $result[0]['sendgrid_templates']['send_grid_id'];
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.sendgrid.com/v3/templates/$use_template",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                //CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Basic ZGF2ZXdAc2libWUuY29tOmM5ODhZWXdzIUA=",
                    "content-type: application/json"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                return $this->get_send_grid_contents($key);
            } else {
                return json_decode($response);
            }
        } else {
            return false;
        }
    }

    function get_send_grid_contents($key) {
        $result = $this->SendgridTemplates->query("select * from sendgrid_templates where slug = '$key'");
        $temp_content = (object) array(
                    'versions' => array(
                        0 => (object) array(
                            'html_content' => !empty($result[0]['sendgrid_templates']['contents']) ? $result[0]['sendgrid_templates']['contents'] : '',
                        )
                    )
        );
        return $temp_content;
    }

    function get_host_url() {
        $redirect = '';
        if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
            $redirect = 'https://' . $_SERVER['HTTP_HOST'];
        } else {
            $redirect = 'http://' . $_SERVER['HTTP_HOST'];
        }
        return $redirect;
    }

    function get_sendgrid_all_templates() {
        $curl = curl_init();
        $result = $this->SendgridTemplates->query("select * from sendgrid_templates");
        if ($result) {
            foreach ($result as $row) {
                $key = $row['sendgrid_templates']['send_grid_id'];
                $result = $this->get_sendgrid_templates($key);
                if (isset($result->versions[0]->html_content) || empty($result->versions[0]->html_content)) {
                    $data = array(
                        'contents' => "'" . Sanitize::escape($result->versions[0]->html_content) . "'"
                    );
                    $this->SendgridTemplates->updateAll($data, array('send_grid_id' => $key));
                }
            }
        } else {
            return false;
        }
        die;
    }

    function get_sendgrid_templates($key) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sendgrid.com/v3/templates/$key",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            //CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic ZGF2ZXdAc2libWUuY29tOmM5ODhZWXdzIUA=",
                "content-type: application/json"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return json_decode($response);
        }
    }

    function getPreferredLanguage() {

        $langs = array();
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            // break up string into pieces (languages and q factors)
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
            if (count($lang_parse[1])) {
                // create a list like "en" => 0.8
                $langs = array_combine($lang_parse[1], $lang_parse[4]);
                // set default to 1 for any without q factor
                foreach ($langs as $lang => $val) {
                    if ($val === '')
                        $langs[$lang] = 1;
                }
                // sort list based on value
                arsort($langs, SORT_NUMERIC);
            }
        }
        //extract most important (first)
        foreach ($langs as $lang => $val) {
            break;
        }
        //if complex language simplify it
        if (stristr($lang, "-")) {
            $tmp = explode("-", $lang);
            $lang = $tmp[0];
        }
        return $lang;
    }

    function language_update_in_cookie_session($lang = '') {

        if (empty($lang)) {
            $language = $this->request->data['lang'];
        } else {
            $language = $lang;
        }
        unset($_SESSION['LANG']);
        setcookie('LANG', '', time() - 3600, '/');
        setcookie('LANG', $language, time() + 2678400, '/');
        $_COOKIE['LANG'] = $language;
        $this->Session->write('LANG', $language);
        $_SESSION['LANG'] = $language;
        if (empty($lang)) {
            echo json_encode(array('success' => true));
            die;
        } else {
            $this->redirect($this->referer());
        }
    }

    function get_language_based_content_for_js() {
        $url = $this->request->data['url'];
        $view = new View($this, false);
        $language_content = $view->Custom->get_page_lang_based_content($url);

        echo json_encode($language_content);
        die;
    }

    function session_update_lumen_side() {

        $lang = $this->request->data['lang'];
        if ($lang != 'es' && $lang != 'en') {
            $lang = 'en';
        }

        if (isset($_COOKIE['LANG']) && !empty($_COOKIE['LANG'])) {
            $lang = $_COOKIE['LANG'];
            $this->Session->write('LANG', $_COOKIE['LANG']);
        } else {
            setcookie('LANG', '', time() - 3600, '/');
            setcookie('LANG', $lang, time() + 2678400, '/');
            $this->Session->write('LANG', $lang);
        }
        die;
    }

    function modify_new_document_websocket($doc_id, $users, $title, $desc, $account_folder_doc_id, $duration, $account_folder_id) {
        $doc_object = $this->Document->find('first', array(
            'conditions' => array(
                'id' => $doc_id
            )
        ));

        $doc_file_object = $this->DocumentFiles->find('first', array(
            'conditions' => array(
                'document_id' => $doc_id
            )
        ));
        $from_workspace = false;
        $huddle_detail = $this->AccountFolder->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
            ),
        ));
        if($huddle_detail['AccountFolder']['folder_type'] == 3)
        {
            $from_workspace = true;
        }
        $view = new View($this, false);
        $original_extension = pathinfo($doc_object['Document']['url']);
        $doc_object['Document'] = $view->Custom->getThumbnailUrl($doc_object['Document'], $from_workspace);
        $doc_object = (object) $doc_object['Document'];
        $doc_object->first_name = $users['User']["first_name"];
        $doc_object->last_name = $users['User']["last_name"];
        $doc_object->email = $users['User']["email"];
        $doc_object->user_id = $users['User']["id"];
        $doc_object->title = $title;
        $doc_object->from_workspace = $from_workspace;
        $doc_object->doc_type = (int) $doc_object->doc_type;
        $doc_object->desc = $desc;
        $doc_object->document_id = (int) $doc_object->id;
        $doc_object->doc_id = (int) $doc_object->id;
        $doc_object->id = (int) $account_folder_doc_id;
        $doc_object->video_duration = empty($duration) ? (!empty($doc_file_object['DocumentFiles']['duration']) ? $doc_file_object['DocumentFiles']['duration'] : $doc_object->video_duration) : $duration;
        $doc_object->account_folder_id = (int) $account_folder_id;
        $doc_object->total_comments = $view->Custom->get_video_comment_numbers($doc_object->doc_id, $doc_object->account_folder_id, $users['User']['id']);
        $doc_object->total_attachment = $view->Custom->get_video_attachment_numbers_for_lti($doc_object->doc_id, $account_folder_id, $users['User']["id"]);
        //$document_url = $this->get_document_url($doc_object['Document']);
        //$doc_object->thubnail_url = $document_url['thumbnail'];
        $doc_object->file_type = $original_extension['extension'];
        if(empty($doc_object->thubnail_url) && $doc_object->published)
        {
            if($this->site_id == 1)
            {
                $doc_object->thubnail_url = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail-sibme.svg');
            }
            else
            {
                $doc_object->thubnail_url = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail-2.png');
            }
        }
        $doc_object->updated_by =  $users['User']["id"];
        return $doc_object;
    }
    
    
      function create_churnzero_event($event_name,$account_id,$user_email,$quantity=1,$custom_fields='')
    {
        if(strpos($user_email,"@sibme.com") || strpos($user_email,"@jjtestsite.us")   )
        {
              return true;
        }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://analytics.churnzero.net/i?appKey=invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg&accountExternalId='.$account_id.'&contactExternalId='.$user_email.'&action=trackEvent&eventName='.$event_name.'&description='.$event_name.'&quantity='.$quantity.$custom_fields );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            return true;
        
    }
    
    
    function getVideoDurationProduction($document_id)
    {
        
            $duration = '0';

            $ch = curl_init();

            $data = array();

            $sibme_base_url = Configure::read('sibme_base_url');//'https://qa.sibme.com/';
            curl_setopt($ch, CURLOPT_URL, $sibme_base_url . 'app/getVideoDuration/' . $document_id);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $duration = (int) $result;
            
            return $duration;
            
    }

    function switch_account_if_required()
    {
        $ref = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : '/';
        $request_url = $_SERVER['REQUEST_URI'];
        $user_current_account = $this->Session->read('user_current_account');
        $huddle_id = false;
        $account_switched = false;
        if(strpos($ref,'/huddle/details/') !== false)
        {
            $ref_array = explode("/",$ref);
            if(isset($ref_array[7]) &&!empty($ref_array[7]))
            {
                $huddle_id = $ref_array[7];
            }
        }

        if(strpos($ref,'/video_huddles/list/') !== false || strpos($ref,'/video_details/live-streaming/') !== false || strpos($ref,'/video_huddles/assessment/') !== false || strpos($ref,'/video_details/scripted_observations/') !== false || strpos($ref,'/video_details/home/') !== false || strpos($ref,'/home/workspace_video') !== false || strpos($ref,'/MyFiles/view/') !== false)
        {
            $ref_array = explode("/",$ref);
            if(isset($ref_array[6]) &&!empty($ref_array[6]))
            {
                $huddle_id = $ref_array[6];
            }
        }
        $direct_link = 0;
        if(strpos($request_url,'/MyFiles/view/') !== false)
        {
            $ref_array = explode("/",$request_url);
            if(isset($ref_array[4]) &&!empty($ref_array[4]))
            {
                $direct_link = 1;
                $huddle_id = $ref_array[4];
            }
        }
        if(!empty($huddle_id))
        {
            $huddle = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));
            if($huddle && $huddle["AccountFolder"]["account_id"] != $user_current_account['accounts']['account_id'])
            {
                $account_switched = true;
                if($direct_link)
                {
                    setcookie("is_account_switched", 1, time() + 12, "/");
                }
                // We need to create session at this point as well because in SSO case user is redirected before we set the session which causes an issue with switching account pop up alert.
                $this->Session->write('is_account_switched', $account_switched);
                $this->switch_account($huddle["AccountFolder"]["account_id"]);
            }
        }
        $this->Session->write('is_account_switched', $account_switched);
    }

    /**
     * Thinkific Implementation Starts Here
     */
    function sibme_learning_center($returnToBundles=false) {
        // Check if this email exists in Thinkific then login to thinkific (learn.sibme.com) and redirect to thinkific (learn.sibme.com) site.
        // if account doesn't exist then create an account in Thinkific as a basic learner (public signup) then login to thinkific and redirect to learn.sibme.com site. 
        $current_user = $this->Session->read('user_current_account');

        if(empty($current_user['User']['email']) || empty($current_user['User']['first_name']) || empty($current_user['User']['last_name'])){
            $response = ["success"=>false, "show_popup"=>false, "message"=>"Cannot access sibme learning center because your Email, First Name or Last Name is missing!"];
        }

        $user = $this->check_if_user_exists_in_thinkific($current_user['User']['email']);
        if($user) {
            // Login to Thinkific and Redirect
            $returnToBundles = isset($returnToBundles) && !empty($returnToBundles) ? true : false;
            $response = $this->login_to_thinkific($current_user['User']['email'], $user->first_name, $user->last_name, $returnToBundles, $user->company);
        } else {
            // Create a new user with default role in thinkific and then Login to Thinkific and Redirect.
            $response = ['show_popup'=>true];
        }
        echo json_encode($response);
        die;
    }

    function check_if_user_exists_in_thinkific($current_user_email){
        $endpoint = "/users";
        $query = ['limit'=>1, 'query[email]'=>$current_user_email];
        
        $response = $this->thinkific_get($endpoint, $query);
        if(empty($response->items)){
            return false;
        } else {
            return $response->items[0];
        }
    }

    function login_to_thinkific($email, $first_name, $last_name, $returnToBundles=false, $company=""){
        // Create token header as a JSON string
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

        // Create token payload as a JSON string
        $payload = json_encode([
            'email'=>$email,
            'first_name'=>$first_name,
            'last_name' => $last_name,
            'company' => $company,
            'iat' => time(),
        ]);

        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        // Create Signature Hash
        $key = $this->thinkific_api_key;
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $key, true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        $baseUrl = "https://".$this->thinkific_sub_domain.".thinkific.com/api/sso/v2/sso/jwt?jwt=";
        $returnTo = !empty($returnToBundles) ? urlencode('https://learn.sibme.com/bundles/getting-started-bundle') : urlencode("https://".$this->thinkific_sub_domain.".thinkific.com");
        $errorUrl = urlencode("https://".$this->thinkific_sub_domain.".thinkific.com");
        $url = $baseUrl . $jwt . "&return_to=" . $returnTo . "&error_url=" . $errorUrl;

        return ["success"=>true, "show_popup"=>false, "learning_center_login_url"=>$url];
    }

    // function create_new_thinkific_account($email, $first_name, $last_name, $account_name){
    function create_new_thinkific_account(){
        $current_user = $this->Session->read('user_current_account');
        $email = $current_user['User']['email']; 
        $first_name = $current_user['User']['first_name']; 
        $last_name = $current_user['User']['last_name']; 
        $account_name = $current_user['accounts']['company_name'];
        $password = $this->request->data['password'];
        $returnToBundles = isset($this->request->data['returnToBundles']) && !empty($this->request->data['returnToBundles']) ? true : false;

        $endpoint = "/users";
        $post_params = [
                            'email'=>$email,
                            'first_name'=>$first_name,
                            'last_name'=>$last_name,
                            'password' => $password,
                            'custom_profile_fields' => [
                                [
                                    "value" => $account_name,
                                    "custom_profile_field_definition_id" => 15687 // This ID is custom created in Learn.Sibme.com if they delete or change it. The code should be revised accordingly.
                                ]
                            ],
                            'send_welcome_email' => true,
                        ];

        $response = $this->thinkific_post($endpoint, $post_params);
        if(isset($response->errors)){
            $message = "";
            foreach($response->errors as $error_key=>$error){
                foreach ($error as $value) {
                    $message .= $error_key ." ". $value;
                }
            }
            $response = ["success"=>false, "message"=>$message];
        } else {
            $response = $this->login_to_thinkific($email, $first_name, $last_name, $returnToBundles);
        }
        echo json_encode($response);
        die;
    }

    function thinkific_get($endpoint, $query=[]) {
        $query = !empty($query) ? "?".http_build_query($query) : "";

        $ch = curl_init();
        $headers = array(
            'Content-Type:application/json',
            'X-Auth-API-Key:'.$this->thinkific_api_key,
            'X-Auth-Subdomain:'.$this->thinkific_sub_domain,
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->thinkific_api_url . $endpoint . $query);        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    function thinkific_post($endpoint, $post_params=[]) {
        $ch = curl_init();
        $headers = array(
            'Content-Type:application/json',
            'X-Auth-API-Key:'.$this->thinkific_api_key,
            'X-Auth-Subdomain:'.$this->thinkific_sub_domain,
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $this->thinkific_api_url . $endpoint); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_params));

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    /**
     * End of Thinkific Implementation
     */
    
    public function syncUsersWithHubspot()
    {
        $account_query = "SELECT * FROM accounts WHERE is_active='1' AND is_suspended='0' ";
        $accounts = $this->Account->query($account_query);
        
        foreach ($accounts as $account)
        {
            $this->syncUsersWithHubspotforAccount($account['accounts']['id']);
        }
        
        echo "All Accounts Done"; die;
        
    }
    
    function syncUsersWithHubspotforAccount($account_id = '') {
        
        echo "Account ID " . $account_id . " is Started \n";
        
        $results = $this->UserAccount->find('all', array(
            'conditions' => array(
                'UserAccount.account_id' => $account_id
            ),
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'u',
                    'type' => 'inner',
                    'conditions' => 'u.id= UserAccount.user_id',
                ),
            ),
            'fields' => array(
                'u.*',
                'UserAccount.*'
            )
        ));
        
        $account_details = $this->Account->find('first', array(
            'conditions' => array(
                'id' => $account_id
            )));
        
        
     foreach ($results as $result)
     {
        
        $vid = '';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.hubapi.com/contacts/v1/contact/email/'.$result['u']['email'].'/profile?hapikey=a27f2344-00a7-4f6f-a70e-a5616ea61fe4' );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($output,true);
        if(isset($output['vid']))
        {
           $vid = $output['vid']; 
        }
        else
        {
            $body_post = '{
                "properties": [
                  {
                    "property": "email",
                    "value": "'.$result['u']['email'].'"
                  },
                  {
                    "property": "firstname",
                    "value": "'.$result['u']['first_name'].'"
                  },
                  {
                    "property": "lastname",
                    "value": "'.$result['u']['last_name'].'"
                  },
                  {
                    "property": "website",
                    "value": ""
                  },
                  {
                    "property": "company",
                    "value": "'.$account_details['Account']['company_name'].'"
                  },
                  {
                    "property": "phone",
                    "value": ""
                  },
                  {
                    "property": "address",
                    "value": ""
                  },
                  {
                    "property": "city",
                    "value": ""
                  },
                  {
                    "property": "state",
                    "value": ""
                  },
                  {
                    "property": "zip",
                    "value": ""
                  }
                ]
              }';
            
           // $body_post = json_decode($body_post, true);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.hubapi.com/contacts/v1/contact/?hapikey=a27f2344-00a7-4f6f-a70e-a5616ea61fe4' );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST,           1 );
            curl_setopt($ch, CURLOPT_POSTFIELDS,  $body_post );
            $output = curl_exec($ch);
            curl_close($ch);
            $output = json_decode($output,true);
            
            if(isset($output['vid']))
            {
                $vid = $output['vid'];
            }
             
            
            
        }
       // echo $vid . '     ' . $result['UserAccount']['role_id'] . '    ' . $result['UserAccount']['user_id'] . '<br>';
//        if($vid == '5872701')
//        {
//            echo $result['UserAccount']['role_id'] . '<br>';
//        }
        
        if(!empty($vid))
        {
            if($result['UserAccount']['role_id'] == '100')
            {
                
                $this->curl_post_request($vid,'221','add');
                
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'110'))
                {
                    $this->curl_post_request($vid,'222','remove');
                }
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'115'))
                {
                    $this->curl_post_request($vid,'223','remove');
                }
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'120'))
                {
                    $this->curl_post_request($vid,'224','remove');
                }
            }
            
            if($result['UserAccount']['role_id'] == '110')
            {
               
                $this->curl_post_request($vid,'222','add');
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'100'))
                {
                    $this->curl_post_request($vid,'221','remove');
                }
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'115'))
                {
                    $this->curl_post_request($vid,'223','remove');
                }
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'120'))
                {
                    $this->curl_post_request($vid,'224','remove');
                }
               
                
            }
            
            if($result['UserAccount']['role_id'] == '115')
            {
                $this->curl_post_request($vid,'223','add');
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'100'))
                {
                    $this->curl_post_request($vid,'221','remove');
                }
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'110'))
                {
                    $this->curl_post_request($vid,'222','remove');
                }
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'120'))
                {
                    $this->curl_post_request($vid,'224','remove');
                }
                
            }
            
            if($result['UserAccount']['role_id'] == '120')
            {
                $this->curl_post_request($vid,'224','add');
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'100'))
                {
                    $this->curl_post_request($vid,'221','remove');
                }
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'110'))
                {
                    $this->curl_post_request($vid,'222','remove');
                }
                if($this->check_if_this_role_not_exists($result['UserAccount']['role_id'],'115'))
                {
                    $this->curl_post_request($vid,'223','remove');
                }
                
            }
            
            
            
            echo $result['u']['id'] . ' ' . $result['u']['first_name'] . ' ' . $result['u']['last_name'] . ' Served \n \n';
            
            
        }
        
        
     }
        
        
        
        
        
        

    }
    
    
    function curl_post_request($vid,$list_id,$action)
    {
  //      echo $url;
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, $url );
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_POST,           1 );
//            curl_setopt($ch, CURLOPT_POSTFIELDS,  $body_post );
//            $output = curl_exec($ch);
//            curl_close($ch);
//            return $output;
//        $contact_list_url_stored = 'https://api.hubapi.com/contacts/v1/lists';
//        $api_key = 'a27f2344-00a7-4f6f-a70e-a5616ea61fe4';
//        $contact_list_url = $contact_list_url_stored . '/221/add?hapikey=' . $api_key;
//        $client = new \GuzzleHttp\Client(['http_errors' => false]);
//        
//        $response = $client->post($contact_list_url, [
//            'debug' => true,
//            'body' => $body_post,
//            'headers' => [
//                'Content-Type' => 'application/json',
//            ]
//        ]);
//        $resp = json_decode($response->getBody()->getContents(), true);
//        print_r($resp);die;
        
        if($action == 'add')
        {
            $function_name = 'add_to_contact_list_for_cake';
        }
        else
        {
            $function_name = 'remove_from_contact_list_for_cake';
        }
        
        $view = new View($this, false);
        $base_url = $view->Custom->getDomain(true);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $base_url.$function_name.'/'.$vid. '/'.$list_id );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
        
        
        
        
    }
    
    function check_if_this_role_not_exists($user_id,$role_id)
    {
        $user_accounts =  $this->UserAccount->find('first', array(
               'conditions' => array(
                   'user_id' => $user_id ,
                   'role_id' => $role_id
               )));
        
        if(empty($user_accounts))
        {
            return true;
        }
        else
        {
            return false;
        }
        
        
    }

    function check_card_expiration_failure($account_id)
    {
        $account_detail = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $subscription = new Subscription();
        $result = array(
            'cardExpired' => false,
            'cardFailure' => false
        );
        if(!empty($account_detail['Account']['braintree_subscription_id']) && !empty($account_detail['Account']['braintree_customer_id']) && $account_detail['Account']['deactive_plan'] == '0'  )
        {

            $customer_info = $subscription->get_customer_information($account_detail['Account']['braintree_customer_id']);
            $subscription_detail = $subscription->subscriptions_detail($account_detail['Account']['braintree_subscription_id']);
            $subscription_detail = json_decode(json_encode($subscription_detail), true);
            $nextBillingDate = $subscription_detail['nextBillingDate']['date'];
            $nextBillingDate = date('Y-m-d', strtotime($nextBillingDate));
            $expires = DateTime::createFromFormat('mY', $customer_info->creditCards[0]->expirationMonth . $customer_info->creditCards[0]->expirationYear);
            $now     = new DateTime();
            if ($expires < $now) {
                $today_date = date('Y-m-d');
                $days_difference = $this->dateDiffInDays($nextBillingDate,$today_date);
                if($days_difference < 31)
                {
                    $result['cardExpired'] = true;
                }
                else
                {
                    $result['cardExpired'] = false;
                }
            }
            else
            {
                $result['cardExpired'] = false;
            }

        }

        if(!empty($account_detail['Account']['braintree_subscription_id']) && $account_detail['Account']['deactive_plan'] == '0'  )
        {
            $subscription_detail = $subscription->subscriptions_detail($account_detail['Account']['braintree_subscription_id']);

            if($subscription_detail->status == 'PastDue')
            {
                $result['cardFailure'] = true;

            }
            else
            {
                $result['cardFailure'] = false;
            }


        }


        return $result;
        echo json_encode($result);die;


    }
function dateDiffInDays($date1, $date2)
    {
        $diff = strtotime($date2) - strtotime($date1);
        return abs(round($diff / 86400));
    }


    public function getHashKey($account_id, $user_id, $date){
        $dt = DateTime::createFromFormat("Y-m-d", $date);
        if($dt==false){
            return false;
        }
      return md5($account_id.$user_id.$date);
    }
    public function getHashKeyHuddle($account_id, $huddle_id, $user_id, $date){
        $dt = DateTime::createFromFormat("Y-m-d", $date);
        if($dt==false){
            return false;
        }
        return md5($account_id.$huddle_id.$user_id.$date);
    }

    public function saveRecord($account_id, $user_id){
        // $hash_key = $this->getHashKey($account_id, $user_id, date("Y-m-d"));
        $hash_key = $this->getHashKey($account_id, $user_id,date("Y-m-d"));
        if($hash_key==false){
            return false;
        }
        // App::uses('AnalyticsController', 'Controller');
        // $user_type  = AnalyticsController::get_user_type($account_id,$user_id);
        // $usersAccount = $this->UserAccount->get($account_id, $user_id);
        // $user_role_id = $usersAccount['UserAccount']['role_id'];
        $item = $this->AnalyticsLoginSummary->find('first', array(
            'conditions' => array(
                'hash_key' => $hash_key
             ),
             'fields'=>array(
                 'web_login_counts',
                 'id'
             )
            ));
        if($item){
            // Update
            $total_count = $item['AnalyticsLoginSummary']['web_login_counts'] + 1;
            $update_array = array(
                'web_login_counts'=>$total_count,
                'updated_at'=>"'".date("Y-m-d")."'",
            );

            $this->AnalyticsLoginSummary->updateAll($update_array, array('hash_key' => $hash_key ), $validation = TRUE);
           // return $total_count;
        } else {
            $usersAccount = $this->UserAccount->get($account_id, $user_id);
            $parent_account_id = '';
            if($usersAccount['accounts']['parent_account_id'] =='-1' || $usersAccount['accounts']['parent_account_id'] ==-1){
                $parent_account_id = 0;
            }else{
                $parent_account_id = $usersAccount['accounts']['parent_account_id'] ;
            }
            /*
            // Insert
            $user_role ='';
            if($user_role_id==100){
                $user_role ='Account Owner';
            }elseif($user_role_id==110){
                $user_role ='Super Admin';
            }elseif($user_role_id==115){
                $user_role ='Admin';
            }elseif($user_role_id==120){
                $user_role ='User';
            }elseif($user_role_id==125){
                $user_role ='Viewer';
            }
            $data = array(
                'hash_key'=>$hash_key,
                'user_id'=> $user_id,
                'is_active'=>1,
                'account_id'=>$account_id,
                'created_at'=>date("Y-m-d"),
                'updated_at'=>date("Y-m-d"),
                'web_login_counts'=>1,
                'parent_account_id'=>$parent_account_id ,
                'is_account_active'=>$usersAccount['accounts']['is_active'],
                'company_name'=>$usersAccount['accounts']['company_name'],
                'is_user_active'=>$usersAccount['users']['is_active'],
                'user_role_id'=>$user_role_id,
                'user_role'=>$user_role,
                'user_type'=> $user_type,
                'username'=>$usersAccount['users']['username'],
                'email'=>$usersAccount['users']['email'],
                'full_name'=>$usersAccount['users']['first_name'].' '.$usersAccount['users']['last_name']

            );
            */
            $data = array(
                'hash_key'=>$hash_key,
                'user_id'=> $user_id,
                'is_active'=>1,
                'account_id'=>$account_id,
                'created_at'=>date("Y-m-d"),
                'updated_at'=>date("Y-m-d"),
                'web_login_counts'=>1,
                'parent_account_id'=>$parent_account_id ,
                'is_account_active'=>$usersAccount['accounts']['is_active'],
                'company_name'=>$usersAccount['accounts']['company_name'],
                'is_user_active'=>$usersAccount['users']['is_active'],
                'site_id'=>$this->site_id,
                // 'username'=>$usersAccount['users']['username'],
                // 'email'=>$usersAccount['users']['email'],
                // 'full_name'=>$usersAccount['users']['first_name'].' '.$usersAccount['users']['last_name']

            );
            $this->AnalyticsLoginSummary->create();
            $this->AnalyticsLoginSummary->save($data,$validation = TRUE);
            //return true;
        }

    }

    public function saveRecordHuddle($account_id, $huddle_id, $user_id, $activity_type, $activity_value=1, $decrement=false){

        if( empty($account_id) || empty($huddle_id) || empty($user_id) || !in_array($activity_type,[2,4,22,11,1,5,8,3,29,13,23,20,6,8,24,26,'total_hours_uploaded','total_hours_viewed']) ){
            return false;
        }
        $hash_key = $this->getHashKeyHuddle($account_id, $huddle_id, $user_id, date("Y-m-d"));
        $activity = $this->getActivityName($activity_type, $huddle_id);
        if(!$activity)
        {
            return false;
        }
        $item = $this->AnalyticsHuddlesSummary->find('first', array(
            'conditions' => array(
                'hash_key' => $hash_key
             )));
        //$item = $this->where("hash_key",$hash_key)->first();
        if($item){
            // Update
            $update_array =array();
            if($activity=="total_hours_uploaded" || $activity=="total_hours_viewed"){
                $update_array = array(
                    $activity=> $item['AnalyticsHuddlesSummary'][$activity] += $activity_value,
                    'updated_at'=>"'".date("Y-m-d")."'"
                );

            } else {
                if($decrement){
                    $update_array = array(
                        $activity=> $item['AnalyticsHuddlesSummary'][$activity] -= 1,
                        'updated_at'=>"'".date("Y-m-d")."'"
                    );

                } else {
                    $update_array = array(
                        $activity=>$item['AnalyticsHuddlesSummary'][$activity] += 1,
                        'updated_at'=>"'".date("Y-m-d")."'"
                    );

                }
            }
            $this->AnalyticsHuddlesSummary->updateAll($update_array, array('hash_key' => $hash_key ), $validation = TRUE);
            //return $hash_key;
            //die;
        } else {
            // Insert
            $data = $this->newItem($hash_key, $account_id, $huddle_id, $user_id, $activity, $activity_value);
            $this->AnalyticsHuddlesSummary->create();
            $this->AnalyticsHuddlesSummary->save($data,$validation = TRUE);
           return  $this->AnalyticsHuddlesSummary->id;
            die;
        }

    }


    protected function newItem($hash_key, $account_id, $huddle_id, $user_id, $activity, $activity_value){
        $item = array();
        //$account = Account::find($account_id);
        $account = $this->Account->find('first', array('conditions' => array('id' => $account_id )));
        $huddle = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));
       // $huddle = AccountFolder::where("account_folder_id", $huddle_id)->first();
        //$user = User::find($user_id);
        $user = $this->User->get($user_id);
        $item['hash_key'] = $hash_key;
        $item['created_at'] = date("Y-m-d");
        $item['updated_at'] =  date("Y-m-d");
        $item['account_id'] = $account_id;
        $item['parent_account_id'] = $account['Account']['parent_account_id'];
        $item['company_name'] = $account['Account']['company_name'];
        $item['is_account_active'] = 1;
        $item['huddle_id'] = $huddle_id;
        $item['is_huddle_active'] = 1;
        $item['created_by'] = $huddle['AccountFolder']['created_by'];
        $item['folder_type'] = $huddle['AccountFolder']['folder_type'];
        $item['user_id'] = $user_id;
        $item['username'] = $user['User']['username'];
        $item['email'] = $user['User']['email'];
        $item['is_user_active'] = 1;
        $usersAccount = $this->UserAccount->get($account_id, $user_id);
        $item['user_role_id'] = $usersAccount['UserAccount']['role_id'];
        $item['site_id'] = $this->site_id;
        return $this->addUserDefaultMetrics($item, $activity, $activity_value);
    }

    protected function addUserDefaultMetrics(&$item, $for_activity, $for_activity_value){
        foreach ($this->activities as $activity) {
            if($activity==$for_activity && ($activity=="total_hours_uploaded" || $activity=="total_hours_viewed")){
                $item[$activity] = $for_activity_value;
            } else if($activity==$for_activity){
                $item[$activity] = 1;
            } else {
                $item[$activity] = 0;
            }
        }
        return $item;
    }

    protected function getActivityName($activity_type_id, $huddle_id){
        $key = $activity_type_id;
        if(in_array($activity_type_id, [2,4,22,11,5,3,29,13])){
            $folder_type = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));
            $folder_type = $folder_type['AccountFolder']['folder_type'];
            if(empty($folder_type) && $huddle_id < 0)
            {
                $folder_type = 3;//workspace
            }
            //$folder_type = AccountFolder::where('account_folder_id' , $huddle_id)->value('folder_type');
            if($activity_type_id==2 || $activity_type_id==4){
                $key = "2-4-".$folder_type;
            } elseif($activity_type_id==3 || $activity_type_id==29){
                $key = "3-29-".$folder_type;
            } else {
                $key = $activity_type_id ."-". $folder_type;
            }
        }
        return isset($this->activities[$key]) ? $this->activities[$key] : false;
    }

    protected function getRedirectJson($redirect_to, $hard_redirect=false, $success=true, $message=""){
        $res = ["redirect_to"=>$redirect_to, "hard_redirect"=>$hard_redirect, "success"=>$success, "message"=>$message];
        if(!empty($this->redirectJsonData)){
            $res['data'] = $this->redirectJsonData;
            $this->redirectJsonData = []; // reset variable for next use.
        }
        return json_encode($res);
    }

    protected function hasCurrentUserMultipleAccounts(){
        $user_accounts = $this->Session->read('user_accounts');
        if(empty($user_accounts)){
            return false;
        }
        $account_ids = [];
        foreach($user_accounts as $account){
            $account_ids [] = $account["users_accounts"]["account_id"];
        }
        return count($account_ids) > 1;
    }

    public function skip_redirect_urls(){
        /**
         * Check if given URL is one of following then skip it.
         */

        $session_redirect_url = $this->Session->read('redirect_uri');
        if(empty($session_redirect_url)){
            return true;
        }
        $urls_parts = ["/", "Errors/view_exception", "app/impersonate_user", "Huddles/", "api/", "get_language_based_content_for_js", "logout", "Users/activation", "update_session_from_lumen", "login", "launchpad", "change_password"];
        $skip_me = false;
        foreach($urls_parts as $value){
            if(stristr($session_redirect_url,$value)){
                $skip_me = true;
            }
        }
        return $skip_me;
    }

    public function hard_logout($need_redirection=true){
        // clear the cookie (if it exists) when logging out
        $this->Cookie->delete('remember_me_cookie');
        $this->Auth->logout();

        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $account_id = $users['accounts']['account_id'];

        $user_activity_logs = array(
            'ref_id' => $user_id,
            'desc' => 'Logged out',
            'url' => $this->custom->getCurrentURL(),
            'type' => '10',
            'account_folder_id' => '',
            'environment_type' => '2',
        );
        if (!empty($user_id) && !empty($account_id) && $account_id != NULL && $user_id != NULL) {
            $this->user_activity_logs($user_activity_logs);
        }
        $this->Session->delete('last_logged_in_user');
        $this->Session->delete('redirect_uri');
        $this->Session->delete('access_token');
        $this->Session->delete('filters_data');

        $this->client->revokeToken();
        //his->Session->delete('view_mode');
        //his->Session->delete('sort_type');
        // @session_destroy($_SESSION['refurl']);


        $this->Session->delete('user_accounts');
        $this->Session->delete('user_default_account');
        $this->Session->delete('totalAccounts');
        $this->Session->delete('user_current_account');

        $this->Session->delete('sso_authenticated');
        $this->Session->destroy();

        unset($_SESSION['refurl']);
        // return $this->redirect('/');
        if($need_redirection){
            echo $this->getRedirectJson('login', true);
            exit;
        }
    }

}
