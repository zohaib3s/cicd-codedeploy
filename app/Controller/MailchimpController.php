<?php

App::uses('AppController', 'Controller');

class MailchimpController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $uses = array(
        'Account',
        'AccountTag',
        'UsecountFolderUser',
        'AcrsAccount',
        'AccountFolder',
        'AccountFolderUser',
        'AccountFolderMetaData',
        'AccountFolderGroup',
        'UserActivityLog',
        'AccountMetaDatum',
        'User',
        'Group',
        'JobQueue',
        'DocumentFiles',
        'Document',
        'UsersAccount',
        'MailchimpTemplate',
        'AccountsMailchimpCampaign',
        'AccountBraintreeSubscription',
        'AuditEmail',
        'AnalyticsReportLog'
    );

    /**
     * index method
     *
     * @return void
     */
    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(
               'syncWithMailchimpGlobal','syncWithMailchimp'
        );
    }
    
    
    
    public function syncWithMailchimpGlobal() {
//        $accounts = $this->Account->find("all", array("conditions" => array(
//                "Account.is_active" => '1',
//                "Account.is_suspended" => '0',
//                "Account.mailchimp_list_id !=" => '',
//        )));
        ini_set('max_execution_time', 300);
        $account_query = "SELECT * FROM accounts WHERE is_active='1' AND is_suspended='0' AND mailchimp_list_id !='' ";
        $accounts = $this->Account->query($account_query);
        $messages_arr = array();
        if (!empty($accounts)) {
            foreach ($accounts as $account):
                if (!empty($account['accounts']['mailchimp_list_id'])) {
                    $messages_arr[] = "Account ID : " . $account['accounts']['id'] . "<br>" . $this->syncWithMailchimp($account['accounts']['id'], $account['accounts']['mailchimp_list_id'], true);
                }
            endforeach;
        }

        $messages = '';
        if (!empty($messages_arr)) {
            foreach ($messages_arr as $message):
                $messages.=$message . "<br>";
            endforeach;
        }
        if (!empty($messages)) {
            echo $messages;
            return true;
           // $this->redirect('index');
        } else {
          // $this->redirect('index');
            return true;
        }
    }
    
    
    
    public function syncWithMailchimp($account_id = '', $mailchimp_list_id = '', $index = false) {
        ini_set('max_execution_time', 300);
        App::import('Vendor', 'MailChimp');
        $MailChimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
        if (!empty($account_id) && !empty($mailchimp_list_id)) {
            $errors_arr = array();
            $return_messages = '';
            $result_list_info = $MailChimp->get('lists/' . $mailchimp_list_id);
            if ($MailChimp->success()) {
                //Lists exists
                $result_list_segments = $MailChimp->get('lists/' . $mailchimp_list_id . '/segments');
                $segments_titles_arr = array();
                if ($MailChimp->success()) {
                    if (!empty($result_list_segments['segments'])):
                        foreach ($result_list_segments['segments'] as $single_segment):
                            $segments_titles_arr[$single_segment['id']] = $single_segment['name'];
                        endforeach;
                    endif;
                    //check and create Account Owners segment
                    if (in_array('Account Owners', $segments_titles_arr)) {
                        $segment_id_account_ownser = array_search('Account Owners', $segments_titles_arr);
                    } else {
                        $result_create_segment_AO = $MailChimp->post('lists/' . $mailchimp_list_id . '/segments', [
                            "name" => 'Account Owners',
                            "static_segment" => array(),
                        ]);
                        if ($MailChimp->success()) {
                            $segment_id_account_ownser = $result_create_segment_AO['id'];
                        } else {
                            $segment_id_account_ownser = '';
                            $errors_arr[] = 'Creating Account Owners Segment : ' . $MailChimp->getLastError();
                        }
                    }


                    //check and create Super Users segment
                    if (in_array('Super Users - Account Owners', $segments_titles_arr)) {
                        $segment_id_super_users_account_owners = array_search('Super Users - Account Owners', $segments_titles_arr);
                        $MailChimp->delete('/lists/' . $mailchimp_list_id . '/segments/' . $segment_id_super_users_account_owners);
                    }

//                    if (in_array('Super Admins - Account Owners', $segments_titles_arr)) {
//                        $segment_id_super_users_account_owners = array_search('Super Admins - Account Owners', $segments_titles_arr);
//                    } else {
//                        $result_create_segment_SU_AU = $MailChimp->post('lists/' . $mailchimp_list_id . '/segments', [
//                            "name" => 'Super Admins - Account Owners',
//                            "static_segment" => array(),
//                        ]);
//                        if ($MailChimp->success()) {
//                            $segment_id_super_users_account_owners = $result_create_segment_SU_AU['id'];
//                        } else {
//                            $segment_id_super_users_account_owners = '';
//                            $errors_arr[] = 'Creating Super Admins - Account Owners Segment : ' . $MailChimp->getLastError();
//                        }
//                    }


                    //check and create Super Users segment
                    if (in_array('Super Users', $segments_titles_arr)) {
                        $segment_id_super_users = array_search('Super Users', $segments_titles_arr);
                        $MailChimp->delete('/lists/' . $mailchimp_list_id . '/segments/' . $segment_id_super_users);
                    }

                    if (in_array('Super Admins', $segments_titles_arr)) {
                        $segment_id_super_users = array_search('Super Admins', $segments_titles_arr);
                    } else {
                        $result_create_segment_SU = $MailChimp->post('lists/' . $mailchimp_list_id . '/segments', [
                            "name" => 'Super Admins',
                            "static_segment" => array(),
                        ]);
                        if ($MailChimp->success()) {
                            $segment_id_super_users = $result_create_segment_SU['id'];
                        } else {
                            $segment_id_super_users = '';
                            $errors_arr[] = 'Creating Super Admins Segment : ' . $MailChimp->getLastError();
                        }
                    }
                    //check and create Users segment
                    if (in_array('Users', $segments_titles_arr)) {
                        $segment_id_users = array_search('Users', $segments_titles_arr);
                    } else {
                        $result_create_segment_U = $MailChimp->post('lists/' . $mailchimp_list_id . '/segments', [
                            "name" => 'Users',
                            "static_segment" => array(),
                        ]);
                        if ($MailChimp->success()) {
                            $segment_id_users = $result_create_segment_U['id'];
                        } else {
                            $segment_id_users = '';
                            $errors_arr[] = 'Creating Users Segment : ' . $MailChimp->getLastError();
                        }
                    }


                    //check and create Admins segment
                    if (in_array('Admins', $segments_titles_arr)) {
                        $segment_id_admins = array_search('Admins', $segments_titles_arr);
                    } else {
                        $result_create_segment_U = $MailChimp->post('lists/' . $mailchimp_list_id . '/segments', [
                            "name" => 'Admins',
                            "static_segment" => array(),
                        ]);
                        if ($MailChimp->success()) {
                            $segment_id_admins = $result_create_segment_U['id'];
                        } else {
                            $segment_id_admins = '';
                            $errors_arr[] = 'Creating Admins Segment : ' . $MailChimp->getLastError();
                        }
                    }

                    //check and create Viewers segment
                    if (in_array('Viewers', $segments_titles_arr)) {
                        $segment_id_viewers = array_search('Viewers', $segments_titles_arr);
                    } else {
                        $result_create_segment_U = $MailChimp->post('lists/' . $mailchimp_list_id . '/segments', [
                            "name" => 'Viewers',
                            "static_segment" => array(),
                        ]);
                        if ($MailChimp->success()) {
                            $segment_id_viewers = $result_create_segment_U['id'];
                        } else {
                            $segment_id_viewers = '';
                            $errors_arr[] = 'Creating Viewers Segment : ' . $MailChimp->getLastError();
                        }
                    }


                    //All active accounts in our system against this account
                    $accounts = $this->UsersAccount->find("all", array("conditions" => array(
                        "UsersAccount.account_id" => $account_id,
                        "Account.is_active" => '1',
                        "Account.is_suspended" => '0',
                        "User.is_active" => '1',
                    )));

                    if (!empty($accounts)):
                        $account_owners_emails = array();
                        $super_users_emails = array();
                        $super_users_account_owner_emails = array();
                        $users_emails = array();
                        $admins_emails = array();
                        $viewers_emails = array();
                        foreach ($accounts as $account):
                            $check_subscriber_email = $MailChimp->get('/lists/' . $mailchimp_list_id . '/members/' . md5($account['User']['email']));
                            if ($MailChimp->success()) {
                                //subscriber already exists
                                if ($account['UsersAccount']['role_id'] == '100') {// account owner
                                    array_push($account_owners_emails, $account['User']['email']);
                                    //  array_push($super_users_account_owner_emails, $account['User']['email']);
                                } else if ($account['UsersAccount']['role_id'] == '110') {// super user
                                    array_push($super_users_emails, $account['User']['email']);
                                    //   array_push($super_users_account_owner_emails, $account['User']['email']);
                                } else if ($account['UsersAccount']['role_id'] == '120') {// user
                                    array_push($users_emails, $account['User']['email']);
                                } else if ($account['UsersAccount']['role_id'] == '115') {// user
                                    array_push($admins_emails, $account['User']['email']);
                                } else if ($account['UsersAccount']['role_id'] == '125') {// user
                                    array_push($viewers_emails, $account['User']['email']);
                                }
                            } else {
                                //subscriber does not exist add one
                                $MailChimp->post('/lists/' . $mailchimp_list_id . '/members', [
                                    "status" => "subscribed",
                                    "email_address" => $account['User']['email'],
                                ]);
                                if ($MailChimp->success()) {
                                    if ($account['UsersAccount']['role_id'] == '100') {// account owner
                                        array_push($account_owners_emails, $account['User']['email']);
                                        //    array_push($super_users_account_owner_emails, $account['User']['email']);
                                    } else if ($account['UsersAccount']['role_id'] == '110') {// super user
                                        array_push($super_users_emails, $account['User']['email']);
                                        //   array_push($super_users_account_owner_emails, $account['User']['email']);
                                    } else if ($account['UsersAccount']['role_id'] == '120') {// user
                                        array_push($users_emails, $account['User']['email']);
                                    } else if ($account['UsersAccount']['role_id'] == '115') {// user
                                        array_push($admins_emails, $account['User']['email']);
                                    } else if ($account['UsersAccount']['role_id'] == '125') {// user
                                        array_push($viewers_emails, $account['User']['email']);
                                    }
                                } else {
                                    $errors_arr[] = 'Adding Subscriber ' . $account['User']['email'] . ' to list : ' . $MailChimp->getLastError();
                                }
                            }
                        endforeach;
                        //Add account owners to Account Owners segment
                        if (!empty($account_owners_emails) && !empty($segment_id_account_ownser)):
                            $MailChimp->post('/lists/' . $mailchimp_list_id . '/segments/' . $segment_id_account_ownser, [
                                "members_to_add" => $account_owners_emails
                            ]);
                            if ($MailChimp->success()) {

                            } else {
                                $errors_arr[] = 'Adding Account Owners to Account Owners segment : ' . $MailChimp->getLastError();
                            }
                        endif;


                        //Add super users to Super Users segment
//                        if (!empty($super_users_account_owner_emails) && !empty($segment_id_super_users_account_owners)):
//                            $MailChimp->post('/lists/' . $mailchimp_list_id . '/segments/' . $segment_id_super_users_account_owners, [
//                                "members_to_add" => $super_users_account_owner_emails
//                            ]);
//                            if ($MailChimp->success()) {
//
//                            } else {
//                                $errors_arr[] = 'Adding Super Users to Super Users segment : ' . $MailChimp->getLastError();
//                            }
//                        endif;

                        //Add super users to Super Users segment
                        if (!empty($super_users_emails) && !empty($segment_id_super_users)):
                            $MailChimp->post('/lists/' . $mailchimp_list_id . '/segments/' . $segment_id_super_users, [
                                "members_to_add" => $super_users_emails
                            ]);
                            if ($MailChimp->success()) {

                            } else {
                                $errors_arr[] = 'Adding Super Users to Super Users segment : ' . $MailChimp->getLastError();
                            }
                        endif;
                        //Add users to Users segment
                        if (!empty($users_emails) && !empty($segment_id_users)):
                            $MailChimp->post('/lists/' . $mailchimp_list_id . '/segments/' . $segment_id_users, [
                                "members_to_add" => $users_emails
                            ]);
                            if ($MailChimp->success()) {

                            } else {
                                $errors_arr[] = 'Adding Users to Super segment : ' . $MailChimp->getLastError();
                            }
                        endif;

                        //Add admins to Admins segment
                        if (!empty($admins_emails) && !empty($segment_id_admins)):
                            $MailChimp->post('/lists/' . $mailchimp_list_id . '/segments/' . $segment_id_admins, [
                                "members_to_add" => $admins_emails
                            ]);
                            if ($MailChimp->success()) {

                            } else {
                                $errors_arr[] = 'Adding Users to Super segment : ' . $MailChimp->getLastError();
                            }
                        endif;

                        //Add viewers to Viewers segment
                        if (!empty($viewers_emails) && !empty($segment_id_viewers)):
                            $MailChimp->post('/lists/' . $mailchimp_list_id . '/segments/' . $segment_id_viewers, [
                                "members_to_add" => $viewers_emails
                            ]);
                            if ($MailChimp->success()) {

                            } else {
                                $errors_arr[] = 'Adding Users to Super segment : ' . $MailChimp->getLastError();
                            }
                        endif;


                    endif;
                } else {
                    $errors_arr[] = 'Getting list segments : ' . $MailChimp->getLastError();
                }
            } else {
                $errors_arr[] = 'Getting list information : ' . $MailChimp->getLastError();
            }
            if (!empty($errors_arr)) {
                $errors = 'Sync with mailchimp is completed with followings Exceptions OR errors :' . "<br>";
                foreach ($errors_arr as $single_error):
                    $errors.=$single_error . "<br>";
                endforeach;
                echo $errors;
                die;
            } else {
                $return_messages .= 'Sync with mailchimp is completed successfully!';
                echo $return_messages;
                die;

            }
        }

    }
    
    function test()
    {
        return true;
    }
    
    

    
    
    
}