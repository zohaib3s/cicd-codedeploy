<?php
App::uses('AppController', 'Controller');
class CronController extends AppController 
{
    
    public $uses = array('Document', 'AccountFolder', 'AccountFolderDocument', 'User','OnboardingEmailLog','UserActivityLog','UsersAccount');

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    
    function beforeFilter() {
        $this->Auth->allow('check_trial');
        parent::beforeFilter();
    }


    function check_trial() 
    {
        $users = $this->Account->getAllTrialAccounts();
        if ($users) {
            foreach ($users as $user) {
                $trial_duration = Configure::read('trial_duration');
                $account_create_date = $user['Account']['created_at'];
                $currentDate = date('Y-m-d H:i:s', strtotime('+' . $trial_duration . ' day', strtotime($account_create_date)));
                $trial_end_date = new DateTime($currentDate);
                $user['User']['trial_end_date'] = date('m-d-Y H:i:s', strtotime("+{$trial_duration} day", strtotime($account_create_date)));

                $start_date = new DateTime(date('Y-m-d H:i:s'));
                $since_start = $trial_end_date->diff($start_date);
                $trialDaysLeft = '0';
                if ($since_start->days > 0) {
                    $trialDaysLeft = $since_start->days;
                }

                echo "<div> Account Owner Name " . $user['User']['first_name'] . ' ' . $user['User']['email'] .  $since_start->days.' weeks </div>';

                if ($trialDaysLeft <= $trial_duration) {
                    if ($trialDaysLeft == 7) {
                        $this->one_week_left($user);
                    }
                    elseif ($trialDaysLeft == 14) {
                        $this->two_week_left($user);
                    }
                    elseif ($trialDaysLeft == 1) {
                        $this->last_day($user);
                    }
                } else {
                    echo "<div> Account Owner Name " . $user['User']['first_name'] . ' ' . $user['User']['last_name'] . ' Trial Expired </div>';
                }
            }
            die;
        }
        die;
    }
    
    
    function live_stream_force_stop()
    {
        $results = $this->UserActivityLog->find('all', array(
                'conditions' => array(
                    'type' => 24,
                    "ref_id NOT IN ( select ref_id from user_activity_logs where type = 25)",
                    "TIMESTAMPDIFF(HOUR, date_added, now()) > 23"
                )
            ));

                if (isset($results) && count($results) >0) {

                    foreach ($results as $result) {

                    $document_details = $this->Document->find('first', array(
                        'conditions' => array(
                            'id' => $result['UserActivityLog']['ref_id'],
                        )
                    ));
                    
                    $dateFromDatabase = strtotime($document_details['Document']['last_edit_date']);
                    $dateFiveMinutesAgo = strtotime("-1 minutes");

                    $start_date = new DateTime($document_details['Document']['last_edit_date']);
                    
                    $start_date_end = new DateTime();
                    $start_date_end->setTimestamp(strtotime('now'));

                    $since_start = $start_date->diff($start_date_end);

                    if ($since_start->i >= 1) {
                            $user_activity_logs = array(
                            'ref_id' => $result['UserActivityLog']['ref_id'],
                            'account_id' => $result['UserActivityLog']['account_id'],
                            'user_id' => $result['UserActivityLog']['user_id'],
                            'desc' => 'Live_Recording_Stopped',
                            'url' => $result['UserActivityLog']['url'],
                            'account_folder_id' => $result['UserActivityLog']['account_folder_id'],
                            'type' => '25',
                            'environment_type' => $result['UserActivityLog']['environment_type']
                        );
                        $this->user_activity_logs($user_activity_logs, $result['UserActivityLog']['account_id'], $result['UserActivityLog']['user_id']);
                        
                          $update_array = array(
                            'is_processed' => 4,
                            'upload_status' => "'" . 'cancelled' . "'"
                          );

                         $this->Document->updateAll($update_array, array('id' => $result['UserActivityLog']['ref_id'] ) , $validation = TRUE);
                        
                    }
                       
                    }

                }
                
               return true;
                
    }
    
    function observations_force_stop()
    {
        $results = $this->UserActivityLog->find('all', array(
                'conditions' => array(
                    'type' => 20,
                    "ref_id NOT IN ( select ref_id from user_activity_logs where type = 21)",
                    "TIMESTAMPDIFF(SECOND, date_added, now()) > 60"
                )
            ));

                if (isset($results) && count($results) >0) {

                    foreach ($results as $result) {

                    $document_details = $this->Document->find('first', array(
                        'conditions' => array(
                            'id' => $result['UserActivityLog']['ref_id'],
                        )
                    ));
                    
                    
                if(empty($document_details)){
                   
                  
                            $user_activity_logs = array(
                            'ref_id' => $result['UserActivityLog']['ref_id'],
                            'account_id' => $result['UserActivityLog']['account_id'],
                            'user_id' => $result['UserActivityLog']['user_id'],
                            'desc' => 'Observation_Stopped',
                            'url' => $result['UserActivityLog']['url'],
                            'account_folder_id' => $result['UserActivityLog']['account_folder_id'],
                            'type' => '21',
                            'environment_type' => $result['UserActivityLog']['environment_type']
                        );
                        $this->user_activity_logs($user_activity_logs, $result['UserActivityLog']['account_id'], $result['UserActivityLog']['user_id']);
                        
                    
                    
                    }
                       
                    }

                }
                
               return true;
                
    }
    
     function test()
    {
        return true;
    }
    

}

?>
