<?php

use Aws\ElasticTranscoder\ElasticTranscoderClient;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe\DataMapping\StreamCollection;
use FFMpeg\FFProbe\DataMapping\Stream;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Filters\Video\ResizeFilter;
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Intercom\IntercomClient;
use GuzzleHttp\Exception\ClientException;

App::import('Vendor', 'PushNotifications');

/**
 * User controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class HuddlesController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Huddles';

    /**
     * This controller does not use a model
     *
     * @var array
     */
    var $helpers = array('Custom', 'Browser');
    public $uses = array(
        'User', 'AccountFolder', 'Group', 'UserGroup', 'AccountFolderMetaData', 'AccountFolderUser', 'AccountFolderGroup',
        'UserAccount', 'Document', 'Comment', 'AccountFolderDocument', 'AccountFolderDocumentAttachment', 'CommentUser', 'AssignmentSubmission',
        'CommentAttachment', 'Observations', 'Frameworks', 'AccountTag', 'AccountCommentTag', 'DocumentFiles', 'EdtpaAssessmentAccounts', 'EdtpaAssessments',
        'EdtpaAssessmentPortfolios', 'EdtpaAssessmentPortfolioNodes', 'EdtpaAssessmentPortfolioCandidateSubmissions', 'EdtpaAssessmentAccountCandidates', 'DocumentStandardRating', 'LiveStreamLog', 'LiveStreamLogDetail', 'UserDeviceLog'
    );
    var $user_id = '';
    private $video_per_page = 12;

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    public function generate_copy_account_scripts($from_account_id, $to_account_id, $assigned_user_id) {

        $script_sql = "";
        $this->layout = null;

        $from_account_huddles = $this->AccountFolder->find('all', array('conditions' => array('account_id IN(' . $from_account_id . ')')));
        if ($from_account_huddles && count($from_account_huddles) > 0) {
            foreach ($from_account_huddles as $from_huddle) {

                $this->AccountFolder->create();
                $data = array(
                    'name' => $from_huddle['AccountFolder']['name'],
                    'desc' => $from_huddle['AccountFolder']['desc'],
                    'created_date' => $from_huddle['AccountFolder']['created_date'],
                    'last_edit_date' => $from_huddle['AccountFolder']['last_edit_date'],
                    'created_by' => $assigned_user_id,
                    'last_edit_by' => $assigned_user_id,
                    'folder_type' => 1,
                    'active' => 1,
                    'account_id' => $to_account_id
                );

                if ($this->AccountFolder->save($data, $validation = TRUE)) {

                    $account_folder_id = $this->AccountFolder->id;

                    $from_huddle_documents = $this->AccountFolderDocument->find('all', array('conditions' => array('account_folder_id IN(' . $from_huddle['AccountFolder']['account_folder_id'] . ')')));

                    foreach ($from_huddle_documents as $from_huddle_document) {

                        $this->AccountFolderDocument->create();
                        $data = array(
                            'account_folder_id' => $account_folder_id,
                            'document_id' => $from_huddle_document['AccountFolderDocument']['document_id'],
                            'is_viewed' => $from_huddle_document['AccountFolderDocument']['is_viewed'],
                            'title' => $from_huddle_document['AccountFolderDocument']['title'],
                            'desc' => $from_huddle_document['AccountFolderDocument']['desc'],
                            'zencoder_output_id' => $from_huddle_document['AccountFolderDocument']['zencoder_output_id'],
                        );
                        $this->AccountFolderDocument->save($data, $validation = TRUE);
                    }


                    $this->AccountFolderUser->create();
                    $data = array(
                        'account_folder_id' => $account_folder_id,
                        'user_id' => $assigned_user_id,
                        'role_id' => 200,
                        'created_by' => $assigned_user_id,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_by' => $assigned_user_id,
                        'last_edit_date' => date("Y-m-d H:i:s"),
                    );
                    $this->AccountFolderUser->save($data, $validation = TRUE);
                }
            }
        }

        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    public function beforeFilter() {

        $this->set('AccountFolder', $this->AccountFolder);
        $this->set('AccountFolderUser', $this->AccountFolderUser);
        $this->set('Comment', $this->Comment);
        $this->set('Document', $this->Document);
        $this->set('AccountFolderGroup', $this->AccountFolderGroup);
        $this->set('UserGroup', $this->UserGroup);

        $users = $this->Session->read('user_current_account');
        $totalAccounts = $this->Session->read('totalAccounts');
        $this->set('logged_in_user', $users);
        $this->user_id = $users['User']['id'];

        if ($users['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
            $this->redirect_on_account_settings();
        }

        $this->check_is_account_expired();
        $this->is_account_activated($users['users_accounts']['account_id'], $users['User']['id']);
        parent::beforeFilter();
    }

    function index($view_mode = '', $sort = '', $searchHuddles = '') {
        //Redirect to new huddle page
        $view = new View($this, false);
        return $view->Custom->redirectHTTPS('video_huddles/list');

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $user_role_id = $users['roles']['role_id'];
        $this->set('user_id', $users['User']['id']);

        $view_m = $this->Session->read('view_mode');
        if (empty($view_m) && empty($view_mode))
            $this->Session->write('view_mode', 'grid');
        if (empty($view_m) && !empty($view_mode))
            $this->Session->write('view_mode', $view_mode);
        if (!empty($view_m) && empty($view_mode))
            $this->Session->write('view_mode', $view_m);
        if (!empty($view_m) && !empty($view_mode))
            $this->Session->write('view_mode', $view_mode);

        $view_mode = $this->Session->read('view_mode');
        $this->set('view_mode', $view_mode);

        $sort_type = $this->Session->read('sort_type');
        if (empty($sort_type) && empty($sort))
            $this->Session->write('sort_type', 'name');
        if (empty($sort_type) && !empty($sort))
            $this->Session->write('sort_type', $sort);
        if (!empty($sort_type) && empty($sort))
            $this->Session->write('sort_type', $sort_type);
        if (!empty($sort_type) && !empty($sort))
            $this->Session->write('sort_type', $sort);

        $sort = $this->Session->read('sort_type');

        $this->set("sort", $sort);
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $folders = array();
        if ($sort == 'flat_view') {
            $huddles = $this->AccountFolder->searchAccountHuddles_textbox($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        } else {
            $huddles = $this->AccountFolder->getAllAccountHuddles($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        }
        if ($sort != 'coaching' && $sort != 'collaboration' && $sort != 'evaluation' && $sort != 'flat_view') {
            $folders = $this->AccountFolder->getAllAccountFolders($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        }

        $folders_check_dropdown = $this->AccountFolder->getAllAccountFolders($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $collab_huddles_dropdown = $this->AccountFolder->getAllAccountHuddles($account_id, 'collaboration', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $coach_huddles_dropdown = $this->AccountFolder->getAllAccountHuddles($account_id, 'coaching', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $eval_huddles_dropdown = $this->AccountFolder->getAllAccountHuddles($account_id, 'evaluation', FALSE, $accountFoldereIds, $accountFolderGroupsIds);


        if ($folders_check_dropdown) {

            $accessible_folders = array();
            foreach ($folders_check_dropdown as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $folders_check_dropdown = $accessible_folders;
        }

        $total_huddles_count = $this->AccountFolder->getAllHuddlesCount($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $huddles_count = array();
        $view = new View($this, false);
        if ($total_huddles_count) {
            foreach ($total_huddles_count as $row) {
                //  if ($view->Custom->check_if_eval_huddle_active($account_id) == false) {
//                    if ($row[0]['folderType'] == 'evaluation') {
//                        continue;
//                    }
                //        }
                $huddles_count[] = $row;
            }
        }
        $view = new View($this, false);
        $item = array();
        $other_huddles = array();

        if (is_array($huddles) && count($huddles) > 0) {

            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();
                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id) && $users['roles']['role_id'] == 120) {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($participants_ids) && is_array($participants_ids) && in_array($row['Document']['created_by'], $participants_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    } elseif ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id) && ($users['roles']['role_id'] == 110 || $users['roles']['role_id'] == 100 || $users['roles']['role_id'] == 115)) {
                        // $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                        $huddles[$i]['AccountFolder']['total_videos'] = $huddles[$i][0]['v_total'];
                    } else {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    }
                } else {

                    //  $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                    $huddles[$i]['AccountFolder']['total_videos'] = $huddles[$i][0]['v_total'];
                }
                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    $total_resources = 0;
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        //  $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        $huddles[$i]['AccountFolder']['total_docs'] = $huddles[$i][0]['doc_total'];
                    } else {

                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $total_participant_videos = $this->Document->countVideosEvaluator_access($huddles[$i]['AccountFolder']['account_folder_id'], $user_id, $evaluators_ids);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {

                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {

                                    $total_count_r = count($this->Document->getVideoDocumentsByVideo($row['Document']['id'], $huddles[$i]['AccountFolder']['account_folder_id']));


                                    $total_resources = $total_resources + $total_count_r;
                                }
                            }
                        }
                        if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                            $huddles[$i]['AccountFolder']['total_docs'] = $total_resources;
                        } else {
                            $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        }
                    }
                } else {
                    //  $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_docs'] = $huddles[$i][0]['doc_total'];
                    } else {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    }
                }


                $get_comments = 0;
            }
        }
        if ($folders) {

            $accessible_folders = array();
            foreach ($folders as $folder) {
                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {
                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $folders = $accessible_folders;
        }
        $all_huddles = array();
        if ($huddles && $user_role_id == 125) {
            foreach ($huddles as $row) {
                if ($row[0]['folderType'] == 'collaboration' || $row[0]['folderType'] == '') {
                    $all_huddles[] = $row;
                }
            }
        } else {
            $all_huddles = $huddles;
        }
//        echo "<pre>";
//        echo count($all_huddles);
//        print_r($all_huddles);
//        echo "</pre>";

        $this->set('video_huddles', $all_huddles);
        if ($sort != 'coaching' && $sort != 'collaboration' && $sort != 'evaluation' && $user_role_id != 125) {
            $this->set('folders', $folders);
            $total_huddles = $huddles_count;
        } elseif ($user_role_id == 125) {
            $total_huddles = $all_huddles;
        } elseif ($sort == 'coaching' || $sort == 'collaboration' || $sort == 'evaluation') {
            $total_huddles = $all_huddles;
        }
        if ($sort == 'folders') {
            $this->set('video_huddles', '');
            $this->set('folders', $folders);
            $total_huddles = 0;
            $huddles_count = 0;
        }

        $params = array(
            'sort' => $sort,
            'new_huddle_button' => 'New Huddle',
            'page_title' => 'Huddles',
            'view_mode' => $view_mode,
            'total_huddles' => ($total_huddles == false ? "0" : count($total_huddles)),
            'folders_check_dropdown' => $folders_check_dropdown,
            'collab_huddles_dropdown' => $collab_huddles_dropdown,
            'coach_huddles_dropdown' => $coach_huddles_dropdown,
            'eval_huddles_dropdown' => $eval_huddles_dropdown,
        );

        $this->set('sort', $sort);
        $this->set('new_huddle_button', 'New Huddle');
        $this->set('page_title', 'Huddles');
        $this->set('view_mode', $view_mode);
        $this->set('total_huddles', ($total_huddles == false ? "0" : count($total_huddles)));
        $this->set('folders_check_dropdown', $folders_check_dropdown);
        $this->set('collab_huddles_dropdown', $collab_huddles_dropdown);
        $this->set('coach_huddles_dropdown', $coach_huddles_dropdown);
        $this->set('eval_huddles_dropdown', $eval_huddles_dropdown);

        $this->set('folders_in_account', $folders_check_dropdown);

        $view = new View($this, false);
        //  $this->set('head', $view->element('videoHuddles', $params));

        $this->render('index');
    }

    function archive_contents($check = 0, $ajax = 0, $view_mode = '', $sort = '', $searchHuddles = '') {

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('huddles/archive_contents');
        $this->set('language_based_content', $language_based_content);

        $page = !empty($this->request->data['page']) ? (int) $this->request->data['page'] : 1;
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $this->set('user_id', $users['User']['id']);

        $view_m = $this->Session->read('view_mode');
        if (empty($view_m) && empty($view_mode))
            $this->Session->write('view_mode', 'grid');
        if (empty($view_m) && !empty($view_mode))
            $this->Session->write('view_mode', $view_mode);
        if (!empty($view_m) && empty($view_mode))
            $this->Session->write('view_mode', $view_m);
        if (!empty($view_m) && !empty($view_mode))
            $this->Session->write('view_mode', $view_mode);

        $view_mode = $this->Session->read('view_mode');
        $this->set('view_mode', $view_mode);

        $sort_type = $this->Session->read('sort_type');
        if (empty($sort_type) && empty($sort))
            $this->Session->write('sort_type', 'name');
        if (empty($sort_type) && !empty($sort))
            $this->Session->write('sort_type', $sort);
        if (!empty($sort_type) && empty($sort))
            $this->Session->write('sort_type', $sort_type);
        if (!empty($sort_type) && !empty($sort))
            $this->Session->write('sort_type', $sort);

        $sort = $this->Session->read('sort_type');

        $this->set("sort", $sort);
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $folders = array();

        $huddles = $this->AccountFolder->getAllAccountHuddles($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, false, 1, 20, $page - 1);
        $huddles_counter = $this->AccountFolder->getAllAccountHuddles($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, false, 1);

        if ($sort != 'coaching' && $sort != 'collaboration') {
            $folders = $this->AccountFolder->getAllAccountFolders($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, false, '/root/', 1);
        }
        $huddles_count = $this->AccountFolder->getAllHuddlesCount($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        $item = array();
        $other_huddles = array();
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();
                $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);

                $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                $get_comments = 0;
            }
        }
//        if ($folders) {
//
//            $accessible_folders = array();
//            foreach ($folders as $folder) {
//
//                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id'])  || $folder['AccountFolder']['created_by'] == $user_id) {
//
//                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
//                    $accessible_folders[] = $folder;
//                }
//            }
//
//           $folders = $accessible_folders;
//
//        }
        $total_huddles = $huddles;
        $this->set('video_huddles', $huddles);
        if ($sort != 'coaching' && $sort != 'collaboration') {
            $this->set('folders', $folders);
            $total_huddles = $huddles_count;
        }
        if ($sort == 'folders') {
            $this->set('video_huddles', '');
            $this->set('folders', $folders);
            $total_huddles = 0;
            $huddles_count = 0;
        }


        if (empty($huddles_counter)) {
            $count_archive_unarchive = 0;
        } else {
            $count_archive_unarchive = count($huddles_counter);
        }

        if (empty($folders)) {
            $folder_count_archive_unarchive = 0;
        } else {
            $folder_count_archive_unarchive = count($folders);
        }

        if ($ajax) {
            $element_data = array(
                'video_huddles' => $huddles,
                'sort' => $sort,
                'view_mode' => $view_mode,
                'user_id' => $users['User']['id'],
                'current_page' => $page,
                'total_huddles' => count($huddles_counter),
            );
            $view = new View($this, false);
            $html = $view->element('huddles/ajax/archive_loadmore', $element_data);
            echo json_encode($html);
            die();
        } else {
            $params = array(
                'sort' => $sort,
                'new_huddle_button' => 'New Huddle',
                'page_title' => 'Huddles',
                'view_mode' => $view_mode,
                'total_huddles' => ($total_huddles == false ? "0" : count($total_huddles)),
                'huddleorfolder' => $check,
                'count_folders' => $folder_count_archive_unarchive,
                'count_huddles' => $count_archive_unarchive,
                'language_based_content' => $language_based_content
            );

            $this->set('total_huddles', count($huddles_counter));
            $this->set('current_page', $page);


            $view = new View($this, false);
            $this->set('head', $view->element('archive_header', $params));
            if ($check == 1) {
                $this->render('archive_huddles');
            } else {
                $this->render('archive_folders_huddles');
            }
        }
    }

    function archive($check) {
        $ids = $this->request->data['selector'];
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->archive_folder_update($id);
            }
        }
        $this->redirect('/Huddles/archive_contents/' . $check);
    }

    function archive_folder_update($huddle_id) {
//        $this->auth_huddle_permission('delete', $huddle_id);
//        if ($huddle_id == Configure::read('sample_huddle')) {
//            $this->Session->setFlash('Cannot delete sample huddle.', 'default', array('class' => 'message'));
//            $this->redirect('/Huddles');
//            return;
//        }
//
        $result = $this->AccountFolder->find('all', array('conditions' => array('folder_type' => array('1', '5'), 'parent_folder_id' => $huddle_id, 'active' => '1')));

        $this->AccountFolder->create();
        $data = array(
            'active' => 0,
            'archive' => 1
        );
        $this->AccountFolder->updateAll($data, array('account_folder_id' => $huddle_id), $validation = TRUE);

        $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
                'parent_folder_id' => $huddle_id, 'active' => 1
        )));


        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];
                $this->archive_folder_update_recursive($folder_child['AccountFolder']['account_folder_id']);
            }
        }


        // $this->redirect('/Huddles');
    }

    function archive_folder_update_recursive($huddle_id) {
//        $this->auth_huddle_permission('delete', $huddle_id);
//        if ($huddle_id == Configure::read('sample_huddle')) {
//            $this->Session->setFlash('Cannot delete sample huddle.', 'default', array('class' => 'message'));
//            $this->redirect('/Huddles');
//            return;
//        }
        $this->AccountFolder->create();
        $data = array(
            'active' => 0,
            'archive' => 1
        );
        $this->AccountFolder->updateAll($data, array('account_folder_id' => $huddle_id), $validation = TRUE);


        $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
                'parent_folder_id' => $huddle_id, 'active' => 1
        )));
        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];
                $this->archive_folder_update_recursive($folder_child['AccountFolder']['account_folder_id']);
            }
        }
    }

    function unarchive_contents($check = 0, $ajax = 0, $view_mode = '', $sort = '', $searchHuddles = '') {

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('huddles/archive_contents');
        $this->set('language_based_content', $language_based_content);

        $page = !empty($this->request->data['page']) ? (int) $this->request->data['page'] : 1;
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $this->set('user_id', $users['User']['id']);

        $view_m = $this->Session->read('view_mode');
        if (empty($view_m) && empty($view_mode))
            $this->Session->write('view_mode', 'grid');
        if (empty($view_m) && !empty($view_mode))
            $this->Session->write('view_mode', $view_mode);
        if (!empty($view_m) && empty($view_mode))
            $this->Session->write('view_mode', $view_m);
        if (!empty($view_m) && !empty($view_mode))
            $this->Session->write('view_mode', $view_mode);

        $view_mode = $this->Session->read('view_mode');
        $this->set('view_mode', $view_mode);

        $sort_type = $this->Session->read('sort_type');
        if (empty($sort_type) && empty($sort))
            $this->Session->write('sort_type', 'name');
        if (empty($sort_type) && !empty($sort))
            $this->Session->write('sort_type', $sort);
        if (!empty($sort_type) && empty($sort))
            $this->Session->write('sort_type', $sort_type);
        if (!empty($sort_type) && !empty($sort))
            $this->Session->write('sort_type', $sort);

        $sort = $this->Session->read('sort_type');

        $this->set("sort", $sort);
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $folders = array();

        $huddles = $this->AccountFolder->getAllAccountHuddlesArchive($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, false, 20, $page - 1);
        $huddles_counter = $this->AccountFolder->getAllAccountHuddlesArchive($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        if ($sort != 'coaching' && $sort != 'collaboration') {
            $folders = $this->AccountFolder->getAllAccountFoldersArchive($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        }
        $huddles_count = $this->AccountFolder->getAllHuddlesCount($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);

        $item = array();
        $other_huddles = array();
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {

                $item = array();
                $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                $get_comments = 0;
            }
        }
//        if ($folders) {
//
//            $accessible_folders = array();
//            foreach ($folders as $folder) {
//
//                if ($this->folder_has_huddle_participating_archive($folder['AccountFolder']['account_folder_id'])  || $folder['AccountFolder']['created_by'] == $user_id) {
//
//                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
//                    $accessible_folders[] = $folder;
//                }
//            }
//
//           $folders = $accessible_folders;
//
//        }
        $total_huddles = $huddles;
        $this->set('video_huddles', $huddles);
        if ($sort != 'coaching' && $sort != 'collaboration') {
            $this->set('folders', $folders);
            $total_huddles = $huddles_count;
        }
        if ($sort == 'folders') {
            $this->set('video_huddles', '');
            $this->set('folders', $folders);
            $total_huddles = 0;
            $huddles_count = 0;
        }

        if (empty($huddles_counter)) {
            $count_archive_unarchive = 0;
        } else {
            $count_archive_unarchive = count($huddles_counter);
        }


        if (empty($folders)) {
            $folder_count_archive_unarchive = 0;
        } else {
            $folder_count_archive_unarchive = count($folders);
        }

        if ($ajax) {
            $element_data = array(
                'video_huddles' => $huddles,
                'sort' => $sort,
                'view_mode' => $view_mode,
                'user_id' => $users['User']['id'],
                'current_page' => $page,
                'total_huddles' => $count_archive_unarchive
            );
            $view = new View($this, false);
            $html = $view->element('huddles/ajax/archive_loadmore', $element_data);
            echo json_encode($html);
            die();
        } else {

            $params = array(
                'sort' => $sort,
                'new_huddle_button' => 'New Huddle',
                'page_title' => 'Huddles',
                'view_mode' => $view_mode,
                'total_huddles' => ($total_huddles == false ? "0" : count($total_huddles)),
                'huddleorfolder' => $check,
                'unarchive' => 1,
                'count_folders' => $folder_count_archive_unarchive,
                'count_huddles' => $count_archive_unarchive,
                'language_based_content' => $language_based_content
            );
            $this->set('total_huddles', count($huddles_counter));
            $this->set('current_page', $page);

            $view = new View($this, false);
            $this->set('head', $view->element('archive_header', $params));
            if ($check == 1) {
                $this->render('unarchive_huddles');
            } else {
                $this->render('unarchive_folders_huddles');
            }
        }
    }

    function unarchive($check) {
        $ids = $this->request->data['selector'];
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->unarchive_folder_update($id);
            }
        }
        $this->redirect('/Huddles/unarchive_contents/' . $check);
    }

    function unarchive_folder_update($huddle_id) {
//        $this->auth_huddle_permission('delete', $huddle_id);
//        if ($huddle_id == Configure::read('sample_huddle')) {
//            $this->Session->setFlash('Cannot delete sample huddle.', 'default', array('class' => 'message'));
//            $this->redirect('/Huddles');
//            return;
//        }
//
        $result = $this->AccountFolder->find('all', array('conditions' => array('folder_type' => array('1', '5'), 'parent_folder_id' => $huddle_id, 'active' => '1')));

        $this->AccountFolder->create();
        $data = array(
            'active' => 1,
            'archive' => 0
        );
        $this->AccountFolder->updateAll($data, array('account_folder_id' => $huddle_id), $validation = TRUE);

        $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
                'parent_folder_id' => $huddle_id, 'active' => 0, 'archive' => 1
        )));


        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];
                $this->unarchive_folder_update_recursive($folder_child['AccountFolder']['account_folder_id']);
            }
        }


        // $this->redirect('/Huddles');
    }

    function unarchive_folder_update_recursive($huddle_id) {
//        $this->auth_huddle_permission('delete', $huddle_id);
//        if ($huddle_id == Configure::read('sample_huddle')) {
//            $this->Session->setFlash('Cannot delete sample huddle.', 'default', array('class' => 'message'));
//            $this->redirect('/Huddles');
//            return;
//        }
        $this->AccountFolder->create();
        $data = array(
            'active' => 1,
            'archive' => 0
        );
        $this->AccountFolder->updateAll($data, array('account_folder_id' => $huddle_id), $validation = TRUE);


        $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
                'parent_folder_id' => $huddle_id, 'active' => 0, 'archive' => 1
        )));
        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];
                $this->unarchive_folder_update_recursive($folder_child['AccountFolder']['account_folder_id']);
            }
        }
    }

    function folder_has_huddle_participating($folder_id) {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $this->set('user_id', $users['User']['id']);

        $folder_ids = $this->get_folder_childs($folder_id);
        $flag = false;
        foreach ($folder_ids as $fold_id) {
            $has_huddle = $this->AccountFolder->hasAccountHuddlesInFolder($account_id, $user_id, $fold_id);
            if ($has_huddle == true) {

                $flag = true;
            }
        }

        if ($flag) {
            return true;
        } else {
            return false;
        }
    }

    function folder_has_huddle_participating_archive($folder_id) {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $this->set('user_id', $users['User']['id']);


        $has_huddle = $this->AccountFolder->hasAccountHuddlesInFolder($account_id, $user_id, $folder_id);

        if ($has_huddle == true) {

            return true;
        } else {

            $folders = $this->AccountFolder->getAllAccountFoldersArchive($account_id, '', FALSE, false, false, $folder_id);
            if (!empty($folders))
                foreach ($folders as $folder) {

                    return $this->folder_has_huddle_participating_archive($folder['AccountFolder']['account_folder_id']);
                }
        }

        return false;
    }

    function search($sort = '') {
        $result = array(
            'status' => FALSE,
            'contents' => '',
            'inHuddles' => False
        );
        $title = $_POST['title'];
        $mode = $_POST['mode'];
        if (isset($_POST['observationAjax']) && $_POST['observationAjax'] != '') {
            $observationAjax = true;
        } else {
            $observationAjax = false;
        }

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $hasHuddles = false;
        $folders = array();
        if ($accountFoldereIds != '0')
            $hasHuddles = true;
        if ($sort == '')
            $sort = $this->Session->read('sort_type');
        if ($sort != 'folders') {
            if ($sort == 'flat_view') {
                $huddles = $this->AccountFolder->searchAccountHuddles_textbox($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);
            } else {
                $huddles = $this->AccountFolder->searchAccountHuddles($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $title);
            }
        }
        if ($sort != 'coaching' && $sort != 'collaboration' && $sort != 'evaluation' && $sort != 'flat_view') {
            $folders = $this->AccountFolder->getAllAccountFoldersSearch($account_id, $sort, FALSE, false, false, false, $title);
        }
        $huddles_count = 0;
        if (empty($title) && $sort != 'folders')
            $total_huddles_count = $this->AccountFolder->getAllHuddlesCount($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);


        $view = new View($this, false);
        if ($total_huddles_count) {
            foreach ($total_huddles_count as $row) {
                if ($view->Custom->check_if_eval_huddle_active($account_id) == false) {
//                    if ($row[0]['folderType'] == 'evaluation') {
//                        continue;
//                    }
                }
                $huddles_count[] = $row;
            }
        }

        $view = new View($this, false);
        $other_huddles = array();
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();
                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    }
                } else {
                    $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                }

                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    $total_resources = 0;
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $total_participant_videos = $this->Document->countVideosEvaluator_access($huddles[$i]['AccountFolder']['account_folder_id'], $user_id, $evaluators_ids);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {

                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {

                                    $total_count_r = count($this->Document->getVideoDocumentsByVideo($row['Document']['id'], $huddles[$i]['AccountFolder']['account_folder_id']));


                                    $total_resources = $total_resources + $total_count_r;
                                }
                            }
                        }

                        if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                            $huddles[$i]['AccountFolder']['total_docs'] = $total_resources;
                        } else {
                            $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        }
                    }
                } else {
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    }
                }
                $get_comments = 0;
            }
            /* if ($huddles) {
              foreach ($huddles as $huddle) {
              if ($huddle['AccountFolder']['folder_type'] == '5') {
              $huddle['videos'] = $this->AccountFolder->getFolderVideos($huddle['AccountFolder']['account_folder_id']);
              $folders[] = $huddle;
              } else {
              $other_huddles[] = $huddle;
              }
              }
              } */
        }

        $total_folders_for_users = $this->AccountFolder->getAllAccountFolders($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        if ($total_folders_for_users) {

            $accessible_folders = array();
            foreach ($total_folders_for_users as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $total_folders_for_users = $accessible_folders;
        }


        if ($folders) {

            $accessible_folders = array();
            foreach ($folders as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $folders = $accessible_folders;
        }
        $all_huddles = array();
        if ($huddles) {
            foreach ($huddles as $row) {
                if ($view->Custom->check_if_eval_huddle_active($account_id) == false) {
//                    if ($row[0]['folderType'] == 'evaluation') {
//                        continue;
//                    }
                }
                $all_huddles[] = $row;
            }
        }

        $total_huddles = $all_huddles;
        if ($sort != 'coaching' && $sort != 'collaboration' && $sort != 'evaluation') {
            // $total_huddles = $huddles_count;
            $total_huddles = $total_huddles_count;
        }
        if ($sort == 'folders') {
            $total_huddles = 0;
            $huddles_count = 0;
        }
//        if ($huddles_count == 0)
//            $total_huddles = $all_huddles;

        $view = new View($this, false);
        $search_result = array(
            'video_huddles' => $huddles,
            'folders' => $folders,
            'total_folders_for_users' => $total_folders_for_users,
            'view_mode' => $mode,
            'sort' => 'name',
            //'total_huddles' => ($huddles == false ? "0" : count($huddles)),
            'total_huddles' => ($total_huddles == false ? "0" : count($total_huddles)),
            'inHuddle' => $hasHuddles
        );
        if ($observationAjax) {
            echo json_encode($search_result);
            exit;
        }
        $html = $view->element('huddles/ajax/ajax_huddle_search', $search_result);
        if (count($html) > 0) {
            $result = array(
                'status' => true,
                'contents' => $html
            );
        } else {
            $result['contents'] = 'No Huddles Found';
        }
        echo json_encode($result);
        exit;
    }

    function search_textbox($sort = '') {
        $result = array(
            'status' => FALSE,
            'contents' => '',
            'inHuddles' => False
        );
        $title = $_POST['title'];
        $mode = $_POST['mode'];
        if (isset($_POST['observationAjax']) && $_POST['observationAjax'] != '') {
            $observationAjax = true;
        } else {
            $observationAjax = false;
        }

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $hasHuddles = false;
        $folders = array();
        if ($accountFoldereIds != '0')
            $hasHuddles = true;
        if ($sort == '')
            $sort = $this->Session->read('sort_type');

        if ($sort != 'folders')
            $huddles = $this->AccountFolder->searchAccountHuddles_textbox($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $title);
        if ($sort != 'coaching' && $sort != 'collaboration' && $sort != 'evaluation' && $sort != 'flat_view') {
            $folders = $this->AccountFolder->getAllAccountFoldersSearch_textbox($account_id, $sort, FALSE, false, false, false, $title);
        }
        $huddles_count = 0;
        if (empty($title) && $sort != 'folders')
            $total_huddles_count = $this->AccountFolder->getAllHuddlesCount($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);

        $view = new View($this, false);
        if ($total_huddles_count) {
            foreach ($total_huddles_count as $row) {
                if ($view->Custom->check_if_eval_huddle_active($account_id) == false) {
//                    if ($row[0]['folderType'] == 'evaluation') {
//                        continue;
//                    }
                }
                $huddles_count[] = $row;
            }
        }

        $view = new View($this, false);
        $other_huddles = array();
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();

                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $total_videos = 0;
                        $total_participant_videos = $this->Document->countVideosEvaluator($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {
                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {
                                    $total_videos++;
                                }
                            }
                        }
                        $huddles[$i]['AccountFolder']['total_videos'] = $total_videos;
                    }
                } else {
                    $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                }
                if ($huddles[$i][0]['folderType'] == 'evaluation') {
                    $total_resources = 0;
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $participants_ids = $view->Custom->get_participants_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $evaluators_ids = $view->Custom->get_evaluator_ids($huddles[$i]['AccountFolder']['account_folder_id']);
                        $total_participant_videos = $this->Document->countVideosEvaluator_access($huddles[$i]['AccountFolder']['account_folder_id'], $user_id, $evaluators_ids);
                        if ($total_participant_videos) {
                            foreach ($total_participant_videos as $row) {

                                if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {

                                    $total_count_r = count($this->Document->getVideoDocumentsByVideo($row['Document']['id'], $huddles[$i]['AccountFolder']['account_folder_id']));


                                    $total_resources = $total_resources + $total_count_r;
                                }
                            }
                        }

                        if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                            $huddles[$i]['AccountFolder']['total_docs'] = $total_resources;
                        } else {
                            $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        }
                    }
                } else {
                    if ($view->Custom->check_if_evalutor($huddles[$i]['AccountFolder']['account_folder_id'], $user_id)) {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    } else {
                        $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    }
                }
                $get_comments = 0;
            }
            /* if ($huddles) {
              foreach ($huddles as $huddle) {
              if ($huddle['AccountFolder']['folder_type'] == '5') {
              $huddle['videos'] = $this->AccountFolder->getFolderVideos($huddle['AccountFolder']['account_folder_id']);
              $folders[] = $huddle;
              } else {
              $other_huddles[] = $huddle;
              }
              }
              } */
        }

        $total_folders_for_users = $this->AccountFolder->getAllAccountFolders($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
        if ($total_folders_for_users) {

            $accessible_folders = array();
            foreach ($total_folders_for_users as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $total_folders_for_users = $accessible_folders;
        }


        if ($folders) {

            $accessible_folders = array();
            foreach ($folders as $folder) {

                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {

                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                }
            }

            $folders = $accessible_folders;
        }
        $all_huddles = array();
        if ($huddles) {
            foreach ($huddles as $row) {
                if ($view->Custom->check_if_eval_huddle_active($account_id) == false) {
//                    if ($row[0]['folderType'] == 'evaluation') {
//                        continue;
//                    }
                }
                $all_huddles[] = $row;
            }
        }
        $total_huddles = $all_huddles;

        if ($sort != 'coaching' && $sort != 'collaboration' && $sort != 'evaluation') {
            $total_huddles = $huddles_count;
        }
        if ($sort == 'folders') {
            $total_huddles = 0;
            $huddles_count = 0;
        }
        if ($huddles_count == 0)
            $total_huddles = $all_huddles;

        $view = new View($this, false);
        $search_result = array(
            'video_huddles' => $huddles,
            'folders' => $folders,
            'total_folders_for_users' => $total_folders_for_users,
            'view_mode' => $mode,
            'sort' => 'name',
            //'total_huddles' => ($huddles == false ? "0" : count($huddles)),
            'total_huddles' => ($total_huddles == false ? "0" : count($total_huddles)),
            'inHuddle' => $hasHuddles
        );
        if ($observationAjax) {
            echo json_encode($search_result);
            exit;
        }
        $html = $view->element('huddles/ajax/ajax_huddle_search', $search_result);
        if (count($html) > 0) {
            $result = array(
                'status' => true,
                'contents' => $html
            );
        } else {
            $result['contents'] = 'No Huddles Found';
        }
        echo json_encode($result);
        exit;
    }

    function search_archive($type, $sort = '') {
        $result = array(
            'status' => FALSE,
            'contents' => '',
            'inHuddles' => False
        );
        $title = $_POST['title'];
        $mode = $_POST['mode'];
        if (isset($_POST['observationAjax']) && $_POST['observationAjax'] != '') {
            $observationAjax = true;
        } else {
            $observationAjax = false;
        }

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $hasHuddles = false;
        $folders = array();
        if ($accountFoldereIds != '0')
            $hasHuddles = true;
        if ($sort == '')
            $sort = $this->Session->read('sort_type');
        if ($sort != 'folders')
            $huddles = $this->AccountFolder->searchAccountHuddles($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $title, 1);
        if ($sort != 'coaching' && $sort != 'collaboration') {
            $folders = $this->AccountFolder->getAllAccountFoldersSearch($account_id, $sort, FALSE, false, false, false, $title, '/root/', 1);
        }
        $huddles_count = 0;
        if (empty($title) && $sort != 'folders')
            $huddles_count = $this->AccountFolder->getAllHuddlesCount($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);


        $other_huddles = array();
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();
                $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                $get_comments = 0;
            }
            /* if ($huddles) {
              foreach ($huddles as $huddle) {
              if ($huddle['AccountFolder']['folder_type'] == '5') {
              $huddle['videos'] = $this->AccountFolder->getFolderVideos($huddle['AccountFolder']['account_folder_id']);
              $folders[] = $huddle;
              } else {
              $other_huddles[] = $huddle;
              }
              }
              } */
        }


//        if ($folders) {
//
//            $accessible_folders = array();
//            foreach ($folders as $folder) {
//
//                if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id ) {
//
//                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
//                    $accessible_folders[] = $folder;
//                }
//            }
//
//           $folders = $accessible_folders;
//
//        }

        $total_huddles = $huddles;
        if ($sort != 'coaching' && $sort != 'collaboration') {
            $total_huddles = $huddles_count;
        }
        if ($sort == 'folders') {
            $total_huddles = 0;
            $huddles_count = 0;
        }
        if ($huddles_count == 0)
            $total_huddles = $huddles;

        $view = new View($this, false);
        $search_result = array(
            'video_huddles' => $huddles,
            'folders' => $folders,
            'view_mode' => $mode,
            'sort' => 'name',
            //'total_huddles' => ($huddles == false ? "0" : count($huddles)),
            'total_huddles' => ($total_huddles == false ? "0" : count($total_huddles)),
            'inHuddle' => $hasHuddles
        );
        if ($observationAjax) {
            echo json_encode($search_result);
            exit;
        }
        if ($type == 1) {
            $html = $view->element('huddles/ajax/archive_search_huddles', $search_result);
        } else {
            $html = $view->element('huddles/ajax/archive_search', $search_result);
        }
        if (count($html) > 0) {
            $result = array(
                'status' => true,
                'contents' => $html,
                'huddles_count' => count($huddles),
                'folders_count' => count($folders)
            );
        } else {
            $result['contents'] = 'No Huddles Found';
        }
        echo json_encode($result);
        exit;
    }

    function search_unarchive($type, $sort = '') {
        $result = array(
            'status' => FALSE,
            'contents' => '',
            'inHuddles' => False
        );
        $title = $_POST['title'];
        $mode = $_POST['mode'];
        if (isset($_POST['observationAjax']) && $_POST['observationAjax'] != '') {
            $observationAjax = true;
        } else {
            $observationAjax = false;
        }

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);

        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $hasHuddles = false;
        $folders = array();
        if ($accountFoldereIds != '0')
            $hasHuddles = true;
        if ($sort == '')
            $sort = $this->Session->read('sort_type');
        if ($sort != 'folders')
            $huddles = $this->AccountFolder->searchAccountHuddles_archive($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $title);
        if ($sort != 'coaching' && $sort != 'collaboration') {
            $folders = $this->AccountFolder->getAllAccountFoldersSearch_archive($account_id, $sort, FALSE, false, false, false, $title);
        }
        $huddles_count = 0;
        if (empty($title) && $sort != 'folders')
            $huddles_count = $this->AccountFolder->getAllHuddlesCount($account_id, $sort, FALSE, $accountFoldereIds, $accountFolderGroupsIds);


        $other_huddles = array();
        if (is_array($huddles) && count($huddles) > 0) {
            for ($i = 0; $i < count($huddles); $i++) {
                $item = array();
                $huddles[$i]['AccountFolder']['total_videos'] = $this->Document->countVideos($huddles[$i]['AccountFolder']['account_folder_id']);
                $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                $get_comments = 0;
            }
            /* if ($huddles) {
              foreach ($huddles as $huddle) {
              if ($huddle['AccountFolder']['folder_type'] == '5') {
              $huddle['videos'] = $this->AccountFolder->getFolderVideos($huddle['AccountFolder']['account_folder_id']);
              $folders[] = $huddle;
              } else {
              $other_huddles[] = $huddle;
              }
              }
              } */
        }


//        if ($folders) {
//
//            $accessible_folders = array();
//            foreach ($folders as $folder) {
//
//                if ($this->folder_has_huddle_participating_archive($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id ) {
//
//                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
//                    $accessible_folders[] = $folder;
//                }
//            }
//
//           $folders = $accessible_folders;
//
//        }

        $total_huddles = $huddles;
        if ($sort != 'coaching' && $sort != 'collaboration') {
            $total_huddles = $huddles_count;
        }
        if ($sort == 'folders') {
            $total_huddles = 0;
            $huddles_count = 0;
        }
        if ($huddles_count == 0)
            $total_huddles = $huddles;

        $view = new View($this, false);
        $search_result = array(
            'video_huddles' => $huddles,
            'folders' => $folders,
            'view_mode' => $mode,
            'sort' => 'name',
            //'total_huddles' => ($huddles == false ? "0" : count($huddles)),
            'total_huddles' => ($total_huddles == false ? "0" : count($total_huddles)),
            'inHuddle' => $hasHuddles
        );
        if ($observationAjax) {
            echo json_encode($search_result);
            exit;
        }
        if ($type == 1) {
            $html = $view->element('huddles/ajax/unarchive_search_huddles', $search_result);
        } else {
            $html = $view->element('huddles/ajax/unarchive_search', $search_result);
        }
        if (count($html) > 0) {
            $result = array(
                'status' => true,
                'contents' => $html,
                'huddles_count' => count($huddles),
                'folders_count' => count($folders)
            );
        } else {
            $result['contents'] = 'No Huddles Found';
        }
        echo json_encode($result);
        exit;
    }

    function get_all_users($account_id, $huddle_type = '') {
        $arr1 = $this->_getUsers($account_id, true);
        $arr2 = $this->_getSupperAdmins($account_id, true);
        $arr3 = $this->_groups($account_id);
        $arr5 = $this->_getAdmin($account_id);
        $view = new View($this, false);


        $result = array();
        if ($huddle_type == 1) {
            $arr4 = $this->_getViewer($account_id);
            if ($arr4) {
                foreach ($arr4 as $row) {
                    $row['User']['role'] = $view->Custom->get_user_role_name($row['roles']['role_id']);
                    $row['User']['is_user'] = 'viewer';
                    $row['User']['name'] = trim($row['User']['last_name'] . " " . $row['User']['first_name']);
                    $result[$row['User']['id']] = $row['User'];
                }
            }
        }

        if ($arr5) {
            foreach ($arr5 as $row) {
                $row['User']['role'] = $view->Custom->get_user_role_name($row['roles']['role_id']);
                $row['User']['is_user'] = 'member';
                $row['User']['name'] = trim($row['User']['last_name'] . " " . $row['User']['first_name']);
                $result[$row['User']['id']] = $row['User'];
            }
        }
        if ($arr1) {
            foreach ($arr1 as $row) {
                $row['User']['role'] = $view->Custom->get_user_role_name($row['roles']['role_id']);
                $row['User']['is_user'] = 'member';
                $row['User']['name'] = trim($row['User']['last_name'] . " " . $row['User']['first_name']);
                $result[$row['User']['id']] = $row['User'];
            }
        }
        if ($arr2) {
            foreach ($arr2 as $row) {
                $row['User']['role'] = $view->Custom->get_user_role_name($row['roles']['role_id']);
                $row['User']['is_user'] = 'admin';
                $row['User']['name'] = trim($row['User']['last_name'] . " " . $row['User']['first_name']);
                $result[$row['User']['id']] = $row['User'];
            }
        }
        if ($arr3) {
            foreach ($arr3 as $row) {
                $row['Group']['is_user'] = 'group';
                $result[] = $row['Group'];
            }
        }

        usort($result, array($this, 'cal_array_sort_name'));
        return $result;
    }

    function cal_array_sort_name($a, $b) {
        return strcasecmp($a['name'], $b['name']);
    }

    function add() {
        $this->auth_permission('huddle', 'add');

        $this->redirect('/Huddles/create');

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $this->set('users_record', $this->get_all_users($account_id));
        $this->set('users', $this->_getUsers($account_id, true));
        $this->set('super_users', $this->_getSupperAdmins($account_id, true));
        $this->set('users_groups', $this->_groups($account_id));
        $this->set('user_id', $users['User']['id']);
        $user_ids = array();

        if (!isset($this->request->data['message'])) {
            $this->request->data['message'] = "";
        }

        if ($this->request->is('post')) {
            $superUsersIds = isset($this->data['super_admin_ids']) ? $this->data['super_admin_ids'] : '';
            $groupIds = isset($this->data['group_ids']) ? $this->data['group_ids'] : '';
            $submission_allowed = $this->data['submission_allowed'];
            if (empty($this->request->data['name'])) {
                $this->Session->setFlash($this->language_based_messages['please_enter_the_huddle_name'], 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/add');
            }

            if (!isset($this->request->data['message']))
                $this->request->data['message'] = "";

            if (strlen($this->request->data['message']) > 1024) {
                $this->Session->setFlash($this->language_based_messages['please_limit_to_1024_characters_or_less_currently'] . strlen($this->request->data['message']), 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/add');
            }


            $this->request->data['created_date'] = date("Y-m-d h:i:s");
            $this->set('user_id', $users['User']['id']);
            $this->AccountFolder->create();
            $data = array(
                'name' => $this->request->data['name'],
                'desc' => $this->request->data['description'],
                'created_date' => date("Y-m-d H:i:s"),
                'last_edit_date' => date("Y-m-d H:i:s"),
                'created_by' => $users['User']['id'],
                'last_edit_by' => $users['User']['id'],
                'folder_type' => 1,
                'active' => 1,
                'account_id' => $account_id
            );
            if ($this->AccountFolder->save($data, $validation = TRUE)) {

                $account_folder_id = $this->AccountFolder->id;
                //Add Allowed video permissions limit.
                $submission_limit_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'submission_allowed',
                    'meta_data_value' => $submission_allowed,
                    'created_by' => $users['User']['id'],
                    'created_date' => date('Y-m-d H:i:s')
                );
                $this->AccountFolderMetaData->save($submission_limit_data);
                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'message',
                    'meta_data_value' => (!isset($this->request->data['message']) ? "" : $this->request->data['message']),
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);

                //clear session users.
                $this->Session->write('newly_added_users', "");


                $superUserData = array(
                    'account_folder_id' => $account_folder_id,
                    'user_id' => $users['User']['id'],
                    'role_id' => $this->role_huddle_admin,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );

                $this->AccountFolderUser->create();
                $this->AccountFolderUser->save($superUserData);

                if ($superUsersIds != '') {
                    foreach ($superUsersIds as $supUsers) {
                        $assigned_role = $this->request->data['user_role_' . $supUsers];
                        $assigned_is_coach = isset($this->request->data['is_coach_' . $supUsers]) ? 1 : '0';
                        $assigned_is_mentee = isset($this->request->data['is_mentor_' . $supUsers]) ? 1 : '0';

                        if ($supUsers <= 0) {
                            //we need to add this user first.
                            $tmp_user_id = $this->_addUser($this->request->data['super_admin_fullname_' . $supUsers], $this->request->data['super_admin_email_' . $supUsers], $account_folder_id);
                            if ($tmp_user_id <= 0)
                                continue;
                            $supUsers = $tmp_user_id;
                        }

                        if ($assigned_role == '200')
                            $assigned_is_coach = 1;

                        $superUserData = array(
                            'account_folder_id' => $account_folder_id,
                            'user_id' => $supUsers,
                            'role_id' => $assigned_role,
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                            'is_coach' => $assigned_is_coach,
                            'is_mentee' => $assigned_is_mentee
                        );

                        $this->save_huddle_log($account_folder_id, $supUsers);

                        $this->AccountFolderUser->create();
                        $this->AccountFolderUser->save($superUserData);
                        // echo $this->AccountFolderUser->getLastQuery();
                    }
                }
                if ($groupIds != '') {
                    foreach ($groupIds as $grp) {
                        $groupsData = array(
                            'account_folder_id' => $account_folder_id,
                            'group_id' => $grp,
                            'role_id' => $this->request->data['group_role_' . $grp],
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                            'is_coach' => isset($this->request->data['is_coach_' . $grp]) ? $this->request->data['is_coach_' . $grp] : '0',
                            'is_mentee' => isset($this->request->data['is_mentor_' . $grp]) ? $this->request->data['is_mentor_' . $grp] : '0',
                        );
                        $this->AccountFolderGroup->create();
                        $this->AccountFolderGroup->save($groupsData);
                    }
                }

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                if (count($huddle)) {
                    $huddleUsers = $this->AccountFolder->getHuddleUsers($huddle[0]['AccountFolder']['account_folder_id']);
                    $userGroups = $this->AccountFolderGroup->getHuddleGroups($huddle[0]['AccountFolder']['account_folder_id']);
                    $huddle_user_ids = array();
                    if ($userGroups && count($userGroups) > 0) {
                        foreach ($userGroups as $row) {
                            $huddle_user_ids[] = $row['user_groups']['user_id'];
                        }
                    }
                    if ($huddleUsers && count($huddleUsers) > 0) {
                        foreach ($huddleUsers as $row) {
                            if ($users['User']['id'] == $row['huddle_users']['user_id']) {
                                continue;
                            }
                            $huddle_user_ids[] = $row['huddle_users']['user_id'];
                        }
                    }

                    $user_ids = implode(',', $huddle_user_ids);
                    if (!empty($user_ids) && count($user_ids) > 0) {
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                        if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                            $huddle[0]['participated_user'] = $huddleUsers;
                            foreach ($huddleUserInfo as $row) {
                                if ($row['User']['is_active'] == 1) {
                                    $huddle[0]['User']['email'] = $row['User']['email'];
                                    if ($this->check_subscription($row['User']['id'], '4', $account_id)) {
                                        $this->newHuddleEmail($huddle[0], $row['User']['id']);
                                    }
                                }
                            }
                        }
                    }
                }

                $this->Session->setFlash($this->request->data['name'] . ' ' . $this->language_based_messages['been_saved_successfully'], 'default', array('class' => 'message success'));
                $user_activity_logs = array(
                    'ref_id' => $account_folder_id,
                    'desc' => $this->request->data['name'],
                    'url' => $this->base . '/Huddles/view/' . $account_folder_id,
                    'account_folder_id' => $account_folder_id,
                    'date_added' => date("Y-m-d H:i:s"),
                    'type' => '1'
                );
                $this->user_activity_logs($user_activity_logs);
                return $this->redirect('/Huddles');
            } else {
                $this->Session->setFlash($this->language_based_messages['huddle_is_not_added_please_try_again'], 'default', array('class' => 'message error'));
                $this->set('user_id', $users['id']);
                $this->render('add');
            }
        } else {
            $this->render('add');
        }
    }

    function get_people_ajax() {
        $people_ids = isset($this->request->data['participants']) ? $this->request->data['participants'] : exit();
        $huddle_type = $this->request->data['type'];
        $user = $this->Session->read('user_current_account');
        $participants = array();
        $roles = array();
        $h_type = array();
        $par = array();
        $huddle_particpaint_users = array();
        foreach ($people_ids as $p) {
            $peoples = explode('||', $p);
            $participants[] = $peoples[0];
            $roles[] = $peoples[1];
            $h_type[] = $peoples[2];
            $par[] = $peoples;
            $new_user = array();

            if (isset($peoples) && count($peoples) > 0 && $peoples[0] < 0) {
                $new_user['id'] = $peoples[0];
                $new_user['user_role'] = $peoples[1];
                $new_user['h_type'] = $peoples[2];
                $new_user['name'] = $peoples[3];
                $huddle_particpaint_users[] = $new_user;
            }
        }
        $account_id = $user['accounts']['account_id'];
        $users_record = $this->get_all_users($account_id, $huddle_type);
        $grp = '';
        if ($huddle_type != 3) {
            $grp = $this->_groups($account_id);
        }


        $huddle_particpaint_grp = array();
        foreach ($users_record as $row) {
            if (in_array($row['id'], $participants)) {
                foreach ($par as $p) {
                    if ($row['id'] == $p[0]) {
                        $row['user_role'] = $p[1];
                        $row['h_type'] = $p[2];
                        $huddle_particpaint_users[] = $row;
                    }
                }
            }
        }

        if (isset($grp) && !empty($grp)) {

            foreach ($grp as $row) {

                if (in_array($row['Group']['id'], $participants)) {
                    foreach ($par as $p) {
                        if ($row['Group']['id'] == $p[0]) {
                            $row['Group']['user_role'] = $p[1];
                            $row['Group']['h_type'] = $p[2];
                            $huddle_particpaint_grp[] = $row['Group'];
                        }
                    }
                }
            }
        }
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('Huddles/view');
        $peopleData = array(
            'people' => $huddle_particpaint_users,
            'group' => $huddle_particpaint_grp,
            'huddle_type' => $huddle_type,
            'language_based_content' => $language_based_content
        );
        $view = new View($this, false);
        $people = $view->element('huddles/tabs/participants', $peopleData);

        echo $people;
        exit;
    }

    function create($type = '2') {

        return $this->redirect('/add_huddle_angular/home', 301);

        $user_permissions = $this->Session->read('user_permissions');
        if (!$user_permissions['UserAccount']['manage_collab_huddles'] && !$user_permissions['UserAccount']['manage_coach_huddles']) {
            $type = 3;
        } elseif (!$user_permissions['UserAccount']['manage_collab_huddles'] && !$user_permissions['UserAccount']['manage_evaluation_huddles']) {
            $type = 2;
        } elseif (!$user_permissions['UserAccount']['manage_coach_huddles'] && !$user_permissions['UserAccount']['manage_evaluation_huddles']) {
            $type = 1;
        }
        $this->auth_permission('huddle', 'add');
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

//        $viewers = $this->_getViewer($account_id);
        $default_framwork_value = '';

        $default_framework = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "default_framework"
        )));
        if (isset($default_framework['AccountMetaData'])) {
            $default_framwork_value_match = $default_framework['AccountMetaData']['meta_data_value'];
        }

        $this->set('default_framework', $default_framwork_value_match);
        $this->set('viewers', $this->_getViewer($account_id));
        $this->set('admins', $this->_getAdmin($account_id));
        $this->set('users_record', $this->get_all_users($account_id, $type));
        $this->set('users', $this->_getUsers($account_id, true));
        $this->set('super_users', $this->_getSupperAdmins($account_id, true));
        $this->set('users_groups', $this->_groups($account_id));
        $this->set('user_id', $users['User']['id']);

        $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                "tag_type" => '2', "account_id" => $account_id
        )));
        if (!empty($frameworks) && count($frameworks) > 0)
            $this->set('frameworks', $frameworks);

        $user_ids = array();

        if (!isset($this->request->data['message'])) {
            $this->request->data['message'] = "";
        }

        if ($this->request->is('post')) {
//            echo "<pre>";
//            print_r($this->request->data);
//            die;
            $superUsersIds = isset($this->data['super_admin_ids']) ? $this->data['super_admin_ids'] : '';
            $groupIds = isset($this->data['group_ids']) ? $this->data['group_ids'] : '';

            if (empty($this->request->data['hname'])) {
                $this->Session->setFlash($this->language_based_messages['please_enter_the_huddle_name'], 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/create');
            }

            if (!isset($this->request->data['message']))
                $this->request->data['message'] = "";

            if (!isset($this->request->data['chkenableframework']))
                $this->request->data['chkenableframework'] = 0;
            if (!isset($this->request->data['chkenabletags']))
                $this->request->data['chkenabletags'] = 0;

            if (strlen($this->request->data['message']) > 1024) {
                $this->Session->setFlash($this->language_based_messages['please_limit_to_1024_characters_or_less_currently'] . strlen($this->request->data['message']), 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/create');
            }


            $this->request->data['created_date'] = date("Y-m-d h:i:s");
            $this->set('user_id', $users['User']['id']);
            $this->AccountFolder->create();
            $data = array(
                'name' => trim($this->request->data['hname']),
                'desc' => $this->request->data['hdescription'],
                'created_date' => date("Y-m-d H:i:s"),
                'last_edit_date' => date("Y-m-d H:i:s"),
                'created_by' => $users['User']['id'],
                'last_edit_by' => $users['User']['id'],
                'folder_type' => 1,
                'active' => 1,
                'account_id' => $account_id
            );
            if ($this->AccountFolder->save($data, $validation = TRUE)) {
                $submission_allowed = isset($this->request->data['submission_allowed']) ? $this->request->data['submission_allowed'] : 1;
                $account_folder_id = $this->AccountFolder->id;
                //Add Allowed video permissions limit.
                $submission_limit_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'submission_allowed',
                    'meta_data_value' => $submission_allowed,
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id'],
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                );
                $this->AccountFolderMetaData->save($submission_limit_data);
                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'message',
                    'meta_data_value' => (!isset($this->request->data['message']) ? "" : $this->request->data['message']),
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);

                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'chk_frameworks',
                    'meta_data_value' => (!isset($this->request->data['chkenableframework']) ? "0" : $this->request->data['chkenableframework']),
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);

                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'chk_tags',
                    'meta_data_value' => (!isset($this->request->data['chkenabletags']) ? "0" : $this->request->data['chkenabletags']),
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);
//                $default_framework = $this->AccountMetaData->find("first", array("conditions" => array(
//                        "account_id" => $account_id,
//                        "meta_data_name" => "default_framework"
//                )));
//                if (isset($default_framework['AccountMetaData'])) {
//                    $default_framwork_value = $default_framework['AccountMetaData']['meta_data_value'];
//                }
                if (isset($this->request->data['chkenableframework']) && $this->request->data['chkenableframework'] == 1 && isset($this->request->data['frameworks'])) {

                    if (isset($default_framwork_value)) {
                        $account_folders_meta_data = array(
                            'account_folder_id' => $account_folder_id,
                            'meta_data_name' => 'framework_id',
                            'meta_data_value' => (empty($default_framwork_value) ? $this->request->data['frameworks'] : $default_framwork_value),
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id']
                        );
                    } else {
                        $account_folders_meta_data = array(
                            'account_folder_id' => $account_folder_id,
                            'meta_data_name' => 'framework_id',
                            'meta_data_value' => $this->request->data['frameworks'],
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id']
                        );
                    }
                    if (!empty($this->request->data['frameworks']) && isset($this->request->data['frameworks'])) {
                        $loggedInUser = $this->Session->read('user_current_account');
                        $view = new View($this, false);
                        $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                        if (IS_QA):
                        /* $client = new IntercomClient('dG9rOjg4MjgyOGJmX2Q2MWRfNDM3OF9hNWNjXzQzODU2ZjliMDZlYjoxOjA=', null);
                          $meta_data = array(
                          'framework_added_in_huddle' => 'true',
                          'user_role' => $user_role
                          );
                          $client->events->create([
                          "event_name" => "framework-added-in-huddle",
                          "created_at" => time(),
                          "email" => $loggedInUser['User']['email'],
                          "metadata" => $meta_data
                          ]); */
                        endif;
                    }

                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                }


                if ($this->request->data['type'] == '2') {
                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'folder_type',
                        'meta_data_value' => (!isset($this->request->data['type']) ? "" : $this->request->data['type']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);


                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'coachee_permission',
                        'meta_data_value' => (!isset($this->request->data['chkcoacheepermissions']) ? "" : $this->request->data['chkcoacheepermissions']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                }


                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'coach_hud_feedback',
                    'meta_data_value' => (!isset($this->request->data['coach_hud_feedback']) ? "" : $this->request->data['coach_hud_feedback']),
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);


                if ($this->request->data['type'] == '3') {
                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'submission_deadline_date',
                        'meta_data_value' => (!isset($this->request->data['submission_deadline_date']) ? "" : $this->request->data['submission_deadline_date']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'submission_deadline_time',
                        'meta_data_value' => (!isset($this->request->data['submission_deadline_time']) ? "" : $this->request->data['submission_deadline_time']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);

                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'folder_type',
                        'meta_data_value' => (!isset($this->request->data['type']) ? "" : $this->request->data['type']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                    $account_folders_meta_data = array(
                        'account_folder_id' => $account_folder_id,
                        'meta_data_name' => 'coachee_permission',
                        'meta_data_value' => (!isset($this->request->data['chkcoacheepermissions']) ? "" : $this->request->data['chkcoacheepermissions']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                }
                //clear session users.
                $this->Session->write('newly_added_users', "");

                if ($this->request->data['type'] == '2') {
                    $superUserData = array(
                        'account_folder_id' => $account_folder_id,
                        'user_id' => $users['User']['id'],
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id'],
                        'is_coach' => 1,
                        'is_mentee' => 0
                    );
                } elseif ($this->request->data['type'] == '3') {
                    $superUserData = array(
                        'account_folder_id' => $account_folder_id,
                        'user_id' => $users['User']['id'],
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id'],
                        'is_coach' => 1,
                        'is_mentee' => 0
                    );
                } else {

                    $superUserData = array(
                        'account_folder_id' => $account_folder_id,
                        'user_id' => $users['User']['id'],
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                }

                $this->AccountFolderUser->create();
                $this->AccountFolderUser->save($superUserData);

                if ($superUsersIds != '' && $this->request->data['type'] != '3') {
                    foreach ($superUsersIds as $supUsers) {
                        $assigned_role = $this->request->data['user_role_' . $supUsers];
                        $assigned_is_coach = isset($this->request->data['is_coach_' . $supUsers]) ? 1 : '0';
                        $assigned_is_mentee = isset($this->request->data['is_mentor_' . $supUsers]) ? 1 : '0';

                        if ($supUsers <= 0) {
                            //we need to add this user first.
                            $tmp_user_id = $this->_addUser($this->request->data['super_admin_fullname_' . $supUsers], $this->request->data['super_admin_email_' . $supUsers], $account_folder_id);
                            if ($tmp_user_id <= 0)
                                continue;
                            $supUsers = $tmp_user_id;
                        }

                        if ($assigned_role == '200')
                            $assigned_is_coach = 1;

                        $superUserData = array(
                            'account_folder_id' => $account_folder_id,
                            'user_id' => $supUsers,
                            'role_id' => $assigned_role,
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                            'is_coach' => $assigned_is_coach,
                            'is_mentee' => $assigned_is_mentee
                        );
                        $this->save_huddle_log($account_folder_id, $supUsers);
                        $this->AccountFolderUser->create();
                        $this->AccountFolderUser->save($superUserData);
                    }
                }
                if ($superUsersIds != '' && $this->request->data['type'] == '3') {
                    foreach ($superUsersIds as $supUsers) {
                        $assigned_role = $this->request->data['user_role_' . $supUsers];
                        $assigned_is_evaluator = isset($this->request->data['is_evaluator_' . $supUsers]) ? 1 : '0';
                        $assigned_is_eval_participant = isset($this->request->data['is_eval_participant_' . $supUsers]) ? 1 : '0';

                        if ($supUsers <= 0) {
                            //we need to add this user first.
                            $tmp_user_id = $this->_addUser($this->request->data['super_admin_fullname_' . $supUsers], $this->request->data['super_admin_email_' . $supUsers], $account_folder_id);
                            if ($tmp_user_id <= 0)
                                continue;
                            $supUsers = $tmp_user_id;
                        }

                        if ($assigned_role == '200')
                            $assigned_is_coach = 1;

                        $superUserData = array(
                            'account_folder_id' => $account_folder_id,
                            'user_id' => $supUsers,
                            'role_id' => $assigned_role,
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                            'is_coach' => $assigned_is_evaluator,
                            'is_mentee' => $assigned_is_eval_participant
                        );
                        $this->save_huddle_log($account_folder_id, $supUsers);
                        $this->AccountFolderUser->create();
                        $this->AccountFolderUser->save($superUserData);
                    }
                } elseif ($groupIds != '' && $this->request->data['type'] == '3') {
                    foreach ($groupIds as $grp) {
                        $groupsData = array(
                            'account_folder_id' => $account_folder_id,
                            'group_id' => $grp,
                            'role_id' => $this->request->data['group_role_' . $grp],
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                            'is_coach' => isset($this->request->data['is_evaluator_' . $grp]) ? $this->request->data['is_evaluator_' . $grp] : '0',
                            'is_mentee' => isset($this->request->data['is_eval_participant_' . $grp]) ? $this->request->data['is_eval_participant_' . $grp] : '0',
                        );
                        $this->AccountFolderGroup->create();
                        $this->AccountFolderGroup->save($groupsData);
                    }
                } else {
                    if (isset($groupIds) && $groupIds != '') {
                        foreach ($groupIds as $grp) {
                            $groupsData = array(
                                'account_folder_id' => $account_folder_id,
                                'group_id' => $grp,
                                'role_id' => $this->request->data['group_role_' . $grp],
                                'created_date' => date("Y-m-d H:i:s"),
                                'last_edit_date' => date("Y-m-d H:i:s"),
                                'created_by' => $users['User']['id'],
                                'last_edit_by' => $users['User']['id'],
                                'is_coach' => isset($this->request->data['is_coach_' . $grp]) ? $this->request->data['is_evaluator_' . $grp] : '0',
                                'is_mentee' => isset($this->request->data['is_mentee_' . $grp]) ? $this->request->data['is_eval_participant_' . $grp] : '0',
                            );
                            $this->AccountFolderGroup->create();
                            $this->AccountFolderGroup->save($groupsData);
                        }
                    }
                }

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                if (count($huddle)) {
                    $huddleUsers = $this->AccountFolder->getHuddleUsers($huddle[0]['AccountFolder']['account_folder_id']);
                    $userGroups = $this->AccountFolderGroup->getHuddleGroups($huddle[0]['AccountFolder']['account_folder_id']);
                    $huddle_user_ids = array();
                    if ($userGroups && count($userGroups) > 0) {
                        foreach ($userGroups as $row) {
                            $huddle_user_ids[] = $row['user_groups']['user_id'];
                        }
                    }
                    if ($huddleUsers && count($huddleUsers) > 0) {
                        foreach ($huddleUsers as $row) {
                            if ($users['User']['id'] == $row['huddle_users']['user_id']) {
                                continue;
                            }
                            $huddle_user_ids[] = $row['huddle_users']['user_id'];
                        }
                    }

                    $user_ids = implode(',', $huddle_user_ids);
                    if (!empty($user_ids) && count($user_ids) > 0) {
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                        if (!empty($huddleUserInfo) && count($huddleUserInfo) > 0) {
                            $huddle[0]['participated_user'] = $huddleUsers;
                            foreach ($huddleUserInfo as $row) {
                                if ($row['User']['is_active'] == 1) {
                                    $huddle[0]['User']['email'] = $row['User']['email'];
                                    if ($this->check_subscription($row['User']['id'], '4', $account_id)) {
                                        $huddle[0]['account_info'] = $users['accounts'];
                                        $huddle[0]['huddle_type'] = (!isset($this->request->data['type']) ? "" : $this->request->data['type']);
                                        if ($this->request->data['type'] == 3) {
                                            $submission_date = explode('-', $this->request->data['submission_deadline_date']);
                                            $submission_time = (!isset($this->request->data['submission_deadline_time']) ? "" : date("h:i a", strtotime($this->request->data['submission_deadline_time'])) . ' CST');
                                            $huddle[0]['submission_time'] = date('F j, Y', strtotime($submission_date[1] . '-' . $submission_date[0] . '-' . $submission_date[2])) . ' at ' . $submission_time;
                                        }
                                        $this->newHuddleEmail($huddle[0], $row['User']['id']);
                                    }
                                }
                            }
                        }
                    }
                }
                $this->Session->setFlash($this->request->data['hname'] . ' ' . $this->language_based_messages['been_saved_successfully'], 'default', array('class' => 'message success'));
                $user_activity_logs = array(
                    'ref_id' => $account_folder_id,
                    'desc' => $this->request->data['hname'],
                    'url' => $this->base . '/Huddles/view/' . $account_folder_id,
                    'account_folder_id' => $account_folder_id,
                    'date_added' => date("Y-m-d H:i:s"),
                    'type' => '1',
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs);
                return $this->redirect('/Huddles');
            } else {
                $this->Session->setFlash($this->language_based_messages['huddle_is_not_added_please_try_again'], 'default', array('class' => 'message error'));
                $this->set('user_id', $users['id']);
                $this->render('create');
            }
        } else {
            if ($type == 1) {
                $this->render('collab_huddle');
            } elseif ($type == 2) {
                $this->render('coaching_huddle');
            } elseif ($type == 3) {
                $this->render('evaluation_huddle');
            }
        }
    }

    function save_huddle_log($account_folder_id, $user_id) {
        $users = $this->Session->read('user_current_account');
        $user = $this->User->get($user_id);

        $user_activity_logs = array(
            'ref_id' => $user_id,
            'desc' => $users ['User'] ['first_name'] . " " . $users['User']['last_name'],
            'url' => $this->base . '/huddles/view/' . $account_folder_id . '/4',
            'account_folder_id' => $account_folder_id,
            'type' => '7'
        );
        $this->user_activity_logs($user_activity_logs);
    }

    function delete($huddle_id) {
        $this->auth_huddle_permission('delete', $huddle_id);
        if ($huddle_id == Configure::read('sample_huddle')) {
            $this->Session->setFlash($this->language_based_messages['cannot_delete_sample_huddle'], 'default', array('class' => 'message'));
            $this->redirect('/Huddles');
            return;
        }
        $this->AccountFolder->create();
        $data = array(
            'active' => 0
        );
        $this->AccountFolder->updateAll($data, array('account_folder_id' => $huddle_id), $validation = TRUE);
        /*
          if ($this->AccountFolder->getAffectedRows() > 0) {
          $this->UserActivityLog->deleteAll(array('account_folder_id' => $huddle_id), $cascade = true, $callbacks = true);
          $folderorHuddle = true;
          }
         */
        $this->Session->setFlash($this->language_based_messages['huddle_has_been_deleted_successfully'], 'default', array('class' => 'message success'));
        $this->redirect('/Huddles');
    }

    function performace_level_update() {

        $huddle_id = $this->request->data['huddle_id'];
        $account_id = $this->request->data['account_id'];
        $video_id = $this->request->data['video_id'];

        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            //$framework_id =  $id_framework['AccountMetaData']['meta_data_value'];
            $framework_id = isset($id_framework['AccountMetaData']['meta_data_value']) ? $id_framework['AccountMetaData']['meta_data_value'] : "0";
        }
        $standardsL2 = array();
        $standards = array();
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }

        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));

        $standard_name_details = '';
        if ($framework_id != -1 && $framework_id != 0) {
            $standard_name_details = $this->AccountTag->find("first", array("conditions" => array(
                    "account_tag_id" => $framework_id, "account_id" => $account_id
            )));
        }
        $standard_name = '';
        if (!empty($standard_name_details)) {
            $standard_name = $standard_name_details['AccountTag']['tag_title'];
        }


        $params_comments = array(
            'standards' => $standards,
            'standardsL2' => $standardsL2,
            'ratings' => $metric_old,
            'video_id' => $video_id,
            'huddle_id' => $huddle_id,
            'account_id' => $account_id,
            'standard_name' => $standard_name
        );

        $view = new View($this, false);
        $html_comments = $view->element('huddles/ajax/performance_level', $params_comments);

        echo($html_comments);
        die;
    }

    function load_perfomance_level_comments() {
        $huddle_id = $this->request->data['huddle_id'];
        $account_id = $this->request->data['account_id'];
        $video_id = $this->request->data['video_id'];
        $account_tag_id = $this->request->data['account_tag_id'];
        $detail_id = $video_id;
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        $srtType = $this->Session->read('srtType');
        $user_current_account = $this->Session->read('user_current_account');

        $changeType = $srtType != '' ? $srtType : 5;
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $export_pdf_url = Configure::read('sibme_base_url') . 'Huddles/print_pdf_comments/' . $detail_id . '/' . $changeType . '/2.pdf';
        $export_excel_url = Configure::read('sibme_base_url') . 'Huddles/print_excel_comments/' . $detail_id . '/' . $changeType;


        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            //$framework_id =  $id_framework['AccountMetaData']['meta_data_value'];
            $framework_id = isset($id_framework['AccountMetaData']['meta_data_value']) ? $id_framework['AccountMetaData']['meta_data_value'] : "0";
        }
        $standardsL2 = array();
        $standards = array();
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }

        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));

        $comments_result = array();

        if (!empty($detail_id)) {
            $comments_result = $this->Comment->getVideoComments_perfomance_level($detail_id, $account_tag_id, $changeType, '', '', '', 1);
        }


        $comments = array();
        foreach ($comments_result as $comment) {
            $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array('1', '2')); //get tags
            if ($htype == 3) {
                $view = new View($this, false);
                if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                    $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                } else {
                    if ($comment['Comment']['active'] == 1) {
                        $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                    }
                }
            } else {
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
            }
        }

        $comments_result = $comments;

        $videoComments = '';
        if (is_array($comments_result) && count($comments_result) > 0) {
            $videoComments = $comments_result;
        } else {
            $videoComments = '';
        }


        $params_comments = array(
            'comments_action' => 0,
            'user_id' => $user_id,
            'video_id' => $detail_id,
            'videoComments' => $videoComments,
            'huddle_permission' => $huddle_permission,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account,
            'sortType' => $changeType,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'type' => 'get_video_comments',
            'ratings' => $metric_old,
            'huddle_type' => $htype,
            'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $detail_id),
            'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                    "account_id" => $account_id,
                    "tag_type" => '1'
        ))),
            'standards' => $standards,
            'standardsL2' => $standardsL2
        );

        $view = new View($this, false);
        $html_comments = $view->element('ajax/vidComments', $params_comments);

        echo $html_comments;
        die;
    }

    function add_standard_ratings() {
        $huddle_id = $this->request->data['huddle_id'];
        $video_id = $this->request->data['video_id'];
        $standard_id = $this->request->data['standard_id'];
        $rating_id = $this->request->data['rating_id'];
        $rating_value = $this->request->data['rating_value'];
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $this->Document->updateAll(array('not_observed' => 1), array('id' => $video_id));

        if (empty($rating_id) && empty($rating_value)) {
            $this->DocumentStandardRating->deleteAll(array('account_folder_id' => $huddle_id, "account_id" => $account_id, 'document_id' => $video_id, 'standard_id' => $standard_id), $cascade = true, $callbacks = true);
        } else {


            $result = $this->DocumentStandardRating->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'document_id' => $video_id, 'account_id' => $account_id, 'standard_id' => $standard_id)));
            if (empty($result)) {
                $this->DocumentStandardRating->create();

                $data = array(
                    'document_id' => $video_id,
                    'account_folder_id' => $huddle_id,
                    'standard_id' => $standard_id,
                    'rating_id' => $rating_id,
                    'rating_value' => $rating_value,
                    'account_id' => $account_id,
                    'user_id' => $user_id,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                );

                $this->DocumentStandardRating->save($data);
            } else {
                $this->DocumentStandardRating->updateAll(array('rating_id' => $rating_id, 'rating_value' => $rating_value, 'created_at' => "'" . date("Y-m-d H:i:s") . "'", 'updated_at' => "'" . date("Y-m-d H:i:s") . "'"), array('id' => $result['DocumentStandardRating']['id']));
            }
        }


        $results = $this->DocumentStandardRating->find('all', array('conditions' => array('document_id' => $video_id, 'account_id' => $account_id, 'account_folder_id' => $huddle_id)));

        if (!empty($results)) {
            $avg = 0;
            foreach ($results as $result) {
                $avg = $avg + (int) $result['DocumentStandardRating']['rating_value'];
            }

            echo round($avg / count($results));
        } else {
            echo 'N/O';
        }

        exit;
    }

    function reset_document_ratings() {

        $huddle_id = $this->request->data['huddle_id'];
        $video_id = $this->request->data['video_id'];
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $this->Document->updateAll(array('not_observed' => 1), array('id' => $video_id));

        $this->DocumentStandardRating->deleteAll(array('account_folder_id' => $huddle_id, "account_id" => $account_id, 'document_id' => $video_id), $cascade = true, $callbacks = true);
        $results = $this->DocumentStandardRating->find('all', array('conditions' => array('document_id' => $video_id, 'account_id' => $account_id, 'account_folder_id' => $huddle_id)));

        if (!empty($results)) {
            $avg = 0;
            foreach ($results as $result) {
                $avg = $avg + (int) $result['DocumentStandardRating']['rating_value'];
            }

            echo round($avg / count($results));
        } else {
            echo 'N/O';
        }
        exit;
    }

    function view($huddle_id, $tab = '', $detail_id = '') {
        //Redirect to new huddle detail page
        $view = new View($this, false);
        
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        
        if($htype == 3 && empty($detail_id))
        {
          return $this->redirect('/home/video_huddles/assessment/'.$huddle_id, 301);  
        }

        if (!empty($detail_id) && $tab == '1') {
            return $this->redirect('/video_details/home/' . $huddle_id . '/' . $detail_id, 301);
        } else if (!empty($detail_id) && $tab == '3') {
            return $this->redirect('video_huddles/huddle/details/' . $huddle_id . '/discussions/details/' . $detail_id, 301);
        } else {
            return $view->Custom->redirectHTTPS('video_huddles/huddle/details/' . $huddle_id);
        }

        $user_current_account = $this->Session->read('user_current_account');
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle_id);


        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('Huddles/view');
        $this->set('language_based_content', $language_based_content);

        if (!empty($detail_id) && $tab == '1') {
            return $this->redirect('/video_details/home/' . $huddle_id . '/' . $detail_id, 301);
        }

        $tab = (int) $tab;
        $detail_id = ($detail_id != '' && $detail_id != 'add' && $detail_id != '') ? (int) preg_replace('/[^0-9]/', '', $detail_id) : $detail_id;

        $huddle_info = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));

        if ($huddle_info['AccountFolder']['parent_folder_id'] != '0' && !empty($huddle_info['AccountFolder']['parent_folder_id'])) {
            $folder_details = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_info['AccountFolder']['parent_folder_id'])));
            $this->set('parent_folder_name', $folder_details);
            $folder_label = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $folder_details['AccountFolder']['account_folder_id']), 'fields' => array('name')));

            $crumb_output = $this->add_new_folder_breadcrum($folder_label['AccountFolder']['name'] . '_0f', $folder_details['AccountFolder']['account_folder_id']);

            $this->set("breadcrumb", $crumb_output);
        }
        $this->set('current_huddle_info', $huddle_info);

        if ($huddle_info['AccountFolder']['active'] == 0) {
            $this->Session->setFlash($this->language_based_messages['invalid_huddle'], 'default', array('class' => 'message error'));
            $this->redirect('/Huddles');
        }

        $this->Session->write('account_folder_id', $huddle_id);
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');


        $view = new View($this, FALSE);
        $this->layout = 'huddles';
        $srtType = $this->Session->read('srtType');

        $changeType = $srtType != '' ? $srtType : 5;
        $export_pdf_url = Configure::read('sibme_base_url') . 'Huddles/print_pdf_comments/' . $detail_id . '/' . $changeType . '/2.pdf';
        $export_excel_url = Configure::read('sibme_base_url') . 'Huddles/print_excel_comments/' . $detail_id . '/' . $changeType;
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        if ($htype == 3) {
            $account_id = $user_current_account['accounts']['account_id'];
            if ($view->Custom->check_if_eval_huddle_active($account_id) == false) {
                $this->Session->setFlash($this->language_based_messages["you_dont_have_access_to_this_huddle"], 'default', array('class' => 'message error'));
                return $this->redirect('/Huddles');
            }
        }

        $comments_result = array();

        if (!empty($detail_id)) {
            $comments_result = $this->Comment->getVideoComments($detail_id, $changeType, '', '', '', 1);
        }
        $user_id = $user_current_account['User']['id'];
        $comments = array();
        foreach ($comments_result as $comment) {
            $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array('1', '2')); //get tags
            $view = new View($this, false);
            if ($htype == 3 || ($htype == 2 && $view->Custom->is_enabled_coach_feedback($huddle_id))) {    //coaching huddle feedback
                $view = new View($this, false);
                if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                    $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                } else {
                    if ($comment['Comment']['active'] == 1) {
                        $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                    }
                }
            } else {
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
            }
        }

        $comments_result = $comments;


        $p_huddle_users = $this->AccountFolder->getHuddleUsers($huddle_id);
        $p_huddle_group_users = $this->AccountFolder->getHuddleGroupsWithUsers($huddle_id);
        $has_access_to_huddle = false;

        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        //echo $framework_check;
        //echo $framework_id;
        $coachee_permission = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "coachee_permission")));
        $coachee_permissions = isset($coachee_permission[0]['AccountFolderMetaData']['meta_data_value']) ? $coachee_permission[0]['AccountFolderMetaData']['meta_data_value'] : "0";

        if (empty($detail_id) && isset($huddle)) {

            if (isset($account_id) && isset($user_id)) {

                $user_activity_logs = array(
                    'ref_id' => $huddle[0]['AccountFolder']['account_folder_id'],
                    'desc' => $huddle[0]['AccountFolder']['name'],
                    'url' => '/Huddles/view/' . $huddle[0]['AccountFolder']['account_folder_id'],
                    'type' => '12',
                    'account_id' => $account_id,
                    'account_folder_id' => $huddle[0]['AccountFolder']['account_folder_id'],
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }

        if (count($p_huddle_users) > 0) {

            for ($i = 0; $i < count($p_huddle_users); $i++) {
                $p_huddle_user = $p_huddle_users[$i];
                if ($p_huddle_user['huddle_users']['user_id'] == $user_id) {
                    $has_access_to_huddle = true;
                }
            }
        }
        if (count($p_huddle_group_users) > 0) {
            for ($i = 0; $i < count($p_huddle_group_users); $i++) {
                $p_huddle_group_user = $p_huddle_group_users[$i];
                if (isset($p_huddle_group_user['User']['id']) && $p_huddle_group_user['User']['id'] == $user_id)
                    $has_access_to_huddle = true;
            }
        }
        if (!$has_access_to_huddle) {
            $this->Session->setFlash($this->language_based_messages['you_dont_have_access_to_this_huddle'], 'default', array('class' => 'message success'));
            return $this->redirect('/Huddles');
        }

        $this->get_huddle_level_permissions($huddle_id);

        if ($detail_id != '' && ($tab == '1' || $tab == '5')) {
            if ($tab == '5') {
                $document_detail = $this->AccountFolderDocument->ob_get_doc_details($detail_id);
                if (count($document_detail) > 0) {
                    $document_id = $document_detail['AccountFolderDocument']['document_id'];
                    $videoDetail2 = $this->_videoDetails($document_id);
                    $videoDetail = '';
                    $vidDocuments = '';
                    $commentsTime = '';
                } else {
                    $videoDetail2 = '';
                    $commentsTime = '';
                    $videoDetail = '';
                    $vidDocuments = '';
                }
            } else {

                $videoDetail = $this->_videoDetails($detail_id);


                $vidDocuments = $this->Document->getVideoDocumentsByVideo($detail_id, $huddle_id);
                $commentsTime = $this->Comment->getVideoComments($detail_id, $changeType, '', '', '', 1);
            }
        } else {
            $commentsTime = '';
            $videoDetail = '';
            $vidDocuments = '';
        }

        $addDiscussionsData = array(
            'adminUsers' => $this->User->getUsersByRole($account_id, array('110', '100'), $user_id),
            'otherUsers' => $this->User->getUsersByRole($account_id, '120', $user_id),
            'huddle_id' => $huddle_id,
            'huddle' => $this->AccountFolder->getHuddle($huddle_id, $account_id),
            'user_id' => $user_id,
            'detail_id' => $detail_id,
            'huddles_users' => $this->AccountFolder->getHuddleUsers($huddle_id),
            'videoHuddleGroups' => $this->AccountFolder->getHuddleGroupsWithUsers($huddle_id),
            'discussionReply' => '',
            'tab' => 'add',
            'language_based_content' => $language_based_content
        );
        if ($detail_id != 'add' && $detail_id != '' && $tab == '3') {
            $replyDiscussions = $this->Comment->commentReplys($detail_id);
            $addDiscussionsData['discussionReply'] = $replyDiscussions;
            $user_permissions = $this->Session->read('user_permissions');
            $replyForm = '';
            if ($huddle_permission == '200' || $huddle_permission == '210' || ($this->is_creator($user_current_account['User']['id'], $huddle_info['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')) {

                $replyForm = $view->element('huddles/tabs/addDiscussionsReply', $addDiscussionsData);
            }

            $replayData = array(
                'discussionReply' => $replyDiscussions,
                'huddle_id' => $huddle_id,
                'user_id' => $user_id,
                'detail_id' => $detail_id,
                'replayForm' => $replyForm,
                'huddle' => $this->AccountFolder->getHuddle($huddle_id, $account_id),
                'language_based_content' => $language_based_content
            );
            $discussionsReply = $view->element('huddles/tabs/discussions_reply', $replayData);
        }
        if ($detail_id == 'add' && $tab == '3') {
            $addForm = $view->element('huddles/tabs/addDiscussions', $addDiscussionsData);
        }

        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);

        $accountFolderIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFolderIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFolderIds = "'" . implode("','", $accountFolderIds) . "'";
        } else {
            $accountFolderIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $all_huddles = $this->AccountFolder->copyAllHuddles($account_id, 'name', FALSE, $accountFolderIds, $accountFolderGroupsIds);
        $this->set('tab', $tab);


        $view = new View($this, false);
        $evaluator = $view->Custom->check_if_evalutor($huddle_id, $user_id);

        if (!$evaluator && $htype == 3) {
            $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_id);
            array_push($evaluator_ids, $user_id);
            $totalVideos = $this->Document->countVideos($huddle_id, '', $evaluator_ids);
        } else {
            $totalVideos = $this->Document->countVideos($huddle_id);
        }

//Start framework
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            //$framework_id =  $id_framework['AccountMetaData']['meta_data_value'];
            $framework_id = isset($id_framework['AccountMetaData']['meta_data_value']) ? $id_framework['AccountMetaData']['meta_data_value'] : "0";
        }
        $standards_new = $this->get_framework_settings($framework_id);

        $standardsL2 = array();
        $standards = array();
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }

            $standards = $this->get_framework_settings($framework_id);
        }

        //End framework

        if (!empty($detail_id)) {
            $view = new View($this, false);
            if ($htype == 3 || ($htype == 2 && $view->Custom->is_enabled_coach_feedback($huddle_id))) {      //coaching huddle feedback
                $view = new View($this, false);
                if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                    $total_incative_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $detail_id, 'active' => array('', '0'))));
                } else {
                    $total_incative_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $detail_id, 'user_id' => $user_id, 'active' => 1)));
                }
            } else {
                $total_incative_comments = 0;
            }

            $videoComments = '';
            if (is_array($comments_result) && count($comments_result) > 0) {
                $videoComments = $comments_result;
            } else {
                $videoComments = '';
            }
            $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
            $params_comments = array(
                'user_id' => $user_id,
                'video_id' => $detail_id,
                'videoComments' => $videoComments,
                'huddle_permission' => $huddle_permission,
                'huddle' => $huddle,
                'user_current_account' => $user_current_account,
                'sortType' => $changeType,
                'export_pdf_url' => $export_pdf_url,
                'export_excel_url' => $export_excel_url,
                'type' => 'get_video_comments',
                'ratings' => $metric_old,
                'huddle_type' => $htype,
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $detail_id),
                'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                        "account_id" => $account_id,
                        "tag_type" => '1'
            ))),
                'standards' => $standards,
                'standardsL2' => $standardsL2,
                'framework_data' => $standards_new
            );

            $view = new View($this, false);
            $html_comments = $view->element('ajax/vidComments', $params_comments);
        }
        $item = array();


        $evaluator = $view->Custom->check_if_evalutor($huddle_id, $user_id);

        if (!$evaluator && $htype == 3) {
            $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_id);
            array_push($evaluator_ids, $user_id);
            $all_videos = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page, 0, $evaluator_ids);
        } else {
            $all_videos = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page);
        }


        if ($all_videos) {
            foreach ($all_videos as $row) {
                $created_by = $row['Document']['created_by'];
                if ($this->_check_evaluator_permissions($huddle_id, $created_by) == false) {
                    continue;
                } else {
                    $item[] = $row;
                }
            }
        }
        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }
        if (!empty($videoDetail)) {
            $videoDetail['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $videoDetail['Document']['id'])));
        }


        if ($htype == 3 && $detail_id != '') {
            $isEditable = $this->_check_evaluator_permissions($huddle_id, $videoDetail['Document']['created_by']);
            if ($isEditable == false) {
                $this->Session->setFlash($this->language_based_messages["you_dont_have_access_to_this_video"], 'default', array('class' => 'message error'));
                return $this->redirect('/Huddles/view/' . $huddle_id . '/' . $tab);
            }

            if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                if ($videoDetail['Document']['created_by'] != $user_id) {
                    $coaches = $this->AccountFolderUser->getUsernameEvaluator($huddle_id, $user_id);
                    $mente = $this->AccountFolderUser->getParticipants($huddle_id);
                    $all_meente = array();
                    foreach ($mente as $row) {
                        if ($videoDetail['Document']['created_by'] == $row['users']['id']) {
                            $participant = $row[0]['user_name'];
                        }
                    }
                    $mente = $participant;
                }
            } else {
                $coaches = $this->AccountFolderUser->getUserNameCoaches($huddle_id);
                $mente = $this->AccountFolderUser->getParticipants($huddle_id);
                $participant = '';
                foreach ($mente as $row) {
                    if ($user_id == $row['users']['id']) {
                        $participant = $row[0]['user_name'];
                    }
                }
                $mente = $participant;
            }
        } else {
            $coaches = $this->AccountFolderUser->getUserNameCoaches($huddle_id);
            $mente = $this->AccountFolderUser->getUserNameMente($huddle_id);
        }


        //Set Video Tabs Data.
        $videosData = array(
            'huddle' => $this->AccountFolder->getHuddle($huddle_id, $account_id),
            'huddle_users' => $this->AccountFolder->getHuddleUsers($huddle_id),
            'all_huddle' => $all_huddles,
            'videos' => $item,
            'coachee_permissions' => $coachee_permissions,
            'totalVideos' => $totalVideos,
            'video_per_page' => $this->video_per_page,
            'current_page' => 1,
            'videoDetail' => $videoDetail,
            'vidDocuments' => $vidDocuments,
            'videoComments' => $commentsTime,
            'videoCommentsArray' => (!isset($videoComments) ? array() : $videoComments),
            'huddle_id' => $huddle_id,
            'user_id' => $user_id,
            'tab' => $tab,
            'type' => 'get_video_comments',
            'huddle_des' => $this->AccountFolder->get($huddle_id),
            'show_ff_message' => false,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'video_id' => $detail_id,
            'html_comments' => (!isset($html_comments) ? "" : $html_comments),
            'coaches' => $coaches,
            'mente' => $mente,
            'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $detail_id),
            'ratings' => $metric_old,
            'standards' => $standards,
            'standardsL2' => $standardsL2,
            'framework_data' => $standards_new
        );

        if (!empty($detail_id)) {
            $this->set('is_video_page', '1');
            $this->set('video_total_info', $videoDetail);
        }


        $isFirefoxBrowser = $view->Browser->isFirefox();
        if ($isFirefoxBrowser) {
            $trim_message_displayed = $this->Cookie->read('flash_message_ff_trim');

            if (!isset($trim_message_displayed) || empty($trim_message_displayed)) {
                echo $trim_message_displayed;
                $videosData['show_ff_message'] = true;
                $this->Cookie->write('flash_message_ff_trim', "displayed", false, '2 weeks');
            }
        }

        //Set Documents Tab data.
        $documentData = array(
            'huddle_id' => $huddle_id,
            'huddle' => $huddle_info,
            'tab' => $tab,
            'language_based_content' => $language_based_content
        );

        //Set Discussions Tab Data.
        $discussionsData = array(
            'discussions' => $this->Comment->getCommentsSearch($huddle_id),
            'adminUsers' => $this->User->getUsersByRole($account_id, array('110', '100')),
            'huddle_id' => $huddle_id,
            'huddle' => $huddle_info,
            'huddles_users' => $this->AccountFolder->getHuddleUsers($huddle_id),
            'videoHuddleGroups' => $this->AccountFolder->getHuddleGroupsWithUsers($huddle_id),
            'user_id' => $user_id,
            'replys' => isset($discussionsReply) ? $discussionsReply : '',
            'addForm' => isset($addForm) ? $addForm : '',
            'key' => $detail_id,
            'tab' => $tab,
            'language_based_content' => $language_based_content
        );

        //Set People Tab Data

        $peopleData = array(
            'people' => $this->get_people_data($huddle_id, $huddle_info['AccountFolder']['created_by']),
            'huddles_users' => $this->AccountFolder->getHuddleUsers($huddle_id),
            'videoHuddleGroups' => $this->AccountFolder->getHuddleGroups($huddle_id),
            'huddle_id' => $huddle_id,
            'huddle' => $huddle_info,
            'tab' => $tab,
            'user_id' => $user_id,
            'huddle_type' => $htype,
            'language_based_content' => $language_based_content
        );
        //$ob_item = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page, '', '', 3);
        $ob_item = $this->Document->getObVideos($huddle_id, '', 'Document.created_date DESC', 10, '', '', 3);
        if (count($ob_item) > 0) {
            for ($j = 0; $j < count($ob_item); $j++) {
                $ob_item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $ob_item[$j]['Document']['id'])));
            }
        }
        $totalObVideos = $this->Document->countVideos($huddle_id, '', '', 3);
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $observationsData = array(
            'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $detail_id),
            'account_folder_id' => $huddle_id,
            'huddle' => $huddle_info,
            'account_id' => $account_id,
            'press_enter_to_send' => $type_pause['UserAccount']['press_enter_to_send'],
            'huddle_type' => $htype,
            'videos' => $ob_item,
            'huddle' => $this->AccountFolder->getHuddle($huddle_id, $account_id),
            'all_huddle' => $all_huddles,
            'totalVideos' => $totalObVideos,
            'video_per_page' => $this->video_per_page,
            'current_page' => 1,
            'videoDetail' => $videoDetail,
            'vidDocuments' => $vidDocuments,
            'videoComments' => $commentsTime,
            'videoCommentsArray' => (!isset($videoComments) ? array() : $videoComments),
            'huddle_id' => $huddle_id,
            'user_id' => $user_id,
            'tab' => $tab,
            'type' => 'get_video_comments',
            'huddle_des' => $this->AccountFolder->get($huddle_id),
            'show_ff_message' => false,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'video_id' => $detail_id,
            'html_comments' => (!isset($html_comments) ? "" : $html_comments),
            'coaches' => $this->AccountFolderUser->getUserNameCoaches($huddle_id),
            'mente' => $this->AccountFolderUser->getUserNameMente($huddle_id),
            'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $detail_id),
            'standards' => $standards,
            'standardsL2' => $standardsL2,
            'ratings' => $metric_old,
            'language_based_content' => $language_based_content
                //'associate_video_model' => $this->load_associate_model($huddle_id)
        );
        $isFirefoxBrowser = $view->Browser->isFirefox();
        if ($isFirefoxBrowser) {
            $trim_message_displayed = $this->Cookie->read('flash_message_ff_trim');

            if (!isset($trim_message_displayed) || empty($trim_message_displayed)) {
                echo $trim_message_displayed;
                $obser_data['show_ff_message'] = true;
                $this->Cookie->write('flash_message_ff_trim', "displayed", false, '2 weeks');
            }
        }

        $matric_enable = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'enable_matric')));
        $enable_matric = isset($matric_enable['AccountMetaData']['meta_data_value']) ? $matric_enable['AccountMetaData']['meta_data_value'] : "0";

        $tracker_enable = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'enable_tracker')));
        $enable_tracker = isset($tracker_enable['AccountMetaData']['meta_data_value']) ? $tracker_enable['AccountMetaData']['meta_data_value'] : "0";


        $user_huddle_level_permissions = $this->Session->read('user_huddle_level_permissions');
        $this->set('user_huddle_level_permissions', $user_huddle_level_permissions);
        $this->set('huddle_id', $huddle_id);
        $this->set('detail_id', $detail_id);
        $this->set('account_id', $account_id);
        $videosData['huddle_type'] = $htype;
        $this->set('enable_matric', $enable_matric);
        $this->set('enable_tracker', $enable_tracker);
        $this->set('video_data', $videosData);
        $this->set('videoCommentsArray', (!isset($videoComments) ? array() : $videoComments));
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $videosData['auto_scroll_switch'] = $type_pause['UserAccount']['autoscroll_switch'];
        $videosData['type_pause'] = $type_pause['UserAccount']['type_pause'];
        $videosData['press_enter_to_send'] = $type_pause['UserAccount']['press_enter_to_send'];
        $videosData["present_account_id"] = $account_id;
        $videosData["all_accounts"] = $this->Session->read('user_accounts');
        $videosData["language_based_content"] = $language_based_content;

        $total_commnets_count = 0;
        if (!empty($detail_id)) {

            $comments_result_for_count = $this->Comment->getVideoComments($detail_id, $changeType, '', '', '', 1, 1, true);

            $total_comments = 0;
            if (count($videoComments) > 0) {
                $total_comments = count($videoComments);
                foreach ($videoComments as $cmnt) {

                    $total_comments = $total_comments + $this->get_total_replies($cmnt['Comment']['id']);
                }
            }


//            for ($i = 0; $i < count($comments_result_for_count); $i++) {
//
//                if (isset($comments_result_for_count[$i]['Comment'])) {
//                    $all_replies = array();
//                    $item_comment_reply = $this->Comment->commentReplys($comments_result_for_count[$i]['Comment']['id']);
//                    if (isset($item_comment_reply) && count($item_comment_reply) > 0) {
//                        array_push($comments_result_for_count_loop, $item_comment_reply);
//                        $total_commnets_count +=1;
//                    }
//                    $total_commnets_count +=1;
//                }
//            }
        }
        // $videosData["comments_count"] = $total_commnets_count;
        $videosData["comments_count"] = $total_comments;


        $this->set('total_incative_comments', (!isset($total_incative_comments) ? 0 : $total_incative_comments));
        $this->set('videosTab', $view->element('huddles/tabs/videos', $videosData));

        $this->set('documentsTab', $view->element('huddles/tabs/documents', $documentData));

        $this->set('discussionsTab', $view->element('huddles/tabs/discussions', $discussionsData));

        $this->set('peopleTab', $view->element('huddles/tabs/people', $peopleData));

        $this->set('observations', $view->element('huddles/tabs/observations', $observationsData));


        /*         * **     Recent Activities     **** */


        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }

        $account_id = $user_current_account['accounts']['account_id'];
        $user_id = $user_current_account['User']['id'];

        $UserActivityLogs_model = $this->UserActivityLog->get_huddle_activity($user_current_account['accounts']['account_id'], $huddle_id);
        $myHuddles = $this->AccountFolder->getAllHuddlesDashboard($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);

        $this->set('UserActivityLogs', $UserActivityLogs_model);
        $this->set('user_current_account', $user_current_account);
        $this->set('user_id', $user_id);
        $this->set('account_id', $account_id);
        $this->set('myHuddles', $myHuddles);

        $UserActivityLogs['tab'] = $tab;

        $UserActivityLogs['myHuddles'] = $myHuddles;
        $UserActivityLogs['user_current_account'] = $user_current_account;
        $UserActivityLogs['AccountFolder'] = $this->AccountFolder;
        $UserActivityLogs['AccountFolderUser'] = $this->AccountFolderUser;
        $UserActivityLogs['AccountFolderGroup'] = $this->AccountFolderGroup;
        $UserActivityLogs['AccountFolderDocument'] = $this->AccountFolderDocument;
        $UserActivityLogs['Comment'] = $this->Comment;
        $UserActivityLogs['User'] = $this->User;
        $UserActivityLogs['UserActivityLogs'] = $UserActivityLogs_model;


        $this->set('activities', $view->element('huddles/tabs/activities', $UserActivityLogs));


        /*         * **     End of Recent Activities     **** */

        $latest_video_created_date = $this->UserAccount->query("SELECT
              d.`created_date`
            FROM
              `account_folders` AS af
              #join `account_folder_users` as afu
                #on af.`account_folder_id` = afu.`account_folder_id`
              JOIN `account_folder_documents` AS afd
                ON afd.`account_folder_id` = af.`account_folder_id`
              JOIN `documents` AS d
                ON d.`id` = afd.`document_id`
            WHERE d.`doc_type` IN (1, 3)
            AND af.`folder_type` = 1
              AND af.`account_id` = " . $account_id . "
              AND af.`account_folder_id`= " . $huddle_id . "
              AND af.site_id = " . $this->site_id . "
            ORDER BY d.`id` DESC
            LIMIT 1 ");
        $latest_video_created_date = $latest_video_created_date[0]['d']['created_date'];
        $this->set('latest_video_created_date', $latest_video_created_date);
        $this->set('standards', $standards);
        $this->set('standardsL2', $standardsL2);
        $this->set('ratings', $metric_old);
        $this->set('video_id', $detail_id);


        if (!empty($videoDetail)) {
            $this->set('isVideoPage', '1');
        } else {
            $this->set('isVideoPage', '0');
        }
    }

    function get_framework_settings($account_tag_id) {
        if (!empty($account_tag_id)):
            $framework_settings = array();
            $acc_tag_type_0 = $this->AccountTag->find('all', array(
                'conditions' => array(
                    'AccountTag.framework_id' => $account_tag_id,
                    'AccountTag.tag_type' => 0
                ),
                'fields' => array('AccountTag.*')
                    )
            );
            $framework_settings['account_tag_type_0'] = $acc_tag_type_0;
            $account_framework_settings = $this->AccountFrameworkSetting->find('first', array(
                'conditions' => array(
                    'AccountFrameworkSetting.account_tag_id' => $account_tag_id,
                ),
                'fields' => array('AccountFrameworkSetting.*')
                    )
            );
            $framework_settings['account_framework_settings'] = $account_framework_settings;
            if (!empty($account_framework_settings)) {
                $account_framework_settings_performance_levels = $this->AccountFrameworkSettingPerformanceLevel->find('all', array(
                    'conditions' => array(
                        'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $account_framework_settings['AccountFrameworkSetting']['id'],
                    ),
                    'fields' => array('AccountFrameworkSettingPerformanceLevel.*')
                        )
                );
                if (!empty($framework_settings['account_tag_type_0'])) {
                    $single_type_0_array = array();
                    foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                        if ($single_acc_tag_type_0['AccountTag']['standard_level'] == $account_framework_settings['AccountFrameworkSetting']['checkbox_level']) {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                        } else {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = array();
                        }
                        $single_type_0_array[] = $single_acc_tag_type_0;
                    endforeach;
                    $framework_settings['account_tag_type_0'] = $single_type_0_array;
                }
//                $framework_settings['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                if ($account_framework_settings['AccountFrameworkSetting']['enable_unique_desc'] == 1) {
                    $acc_tag_type_0_tag_ids = array();
                    if (!empty($acc_tag_type_0)) {
                        foreach ($acc_tag_type_0 as $acc_tag_type_0_single):
                            array_push($acc_tag_type_0_tag_ids, $acc_tag_type_0_single['AccountTag']['account_tag_id']);
                        endforeach;
                    }
                    if (!empty($acc_tag_type_0_tag_ids)) {
                        $account_framework_settings_performance_levels_ids = array();
                        if (!empty($account_framework_settings_performance_levels)) {
                            foreach ($account_framework_settings_performance_levels as $single_performance_level):
                                array_push($account_framework_settings_performance_levels_ids, $single_performance_level['AccountFrameworkSettingPerformanceLevel']['id']);
                            endforeach;
                        }
                        $performance_level_descriptions = $this->PerformanceLevelDescription->find('all', array(
                            'conditions' => array(
                                'PerformanceLevelDescription.performance_level_id' => $account_framework_settings_performance_levels_ids,
                                'PerformanceLevelDescription.account_tag_id' => $acc_tag_type_0_tag_ids,
                            ),
                            'fields' => array('PerformanceLevelDescription.*')
                                )
                        );
                        if (!empty($framework_settings['account_tag_type_0']) && !empty($performance_level_descriptions)) {
                            $single_type_0_array = array();
                            foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                                $performance_level_descriptions_arr = array();
                                foreach ($performance_level_descriptions as $single_performance_level_description):
                                    if ($single_acc_tag_type_0['AccountTag']['standard_level'] == $account_framework_settings['AccountFrameworkSetting']['checkbox_level'] && $single_acc_tag_type_0['AccountTag']['account_tag_id'] == $single_performance_level_description['PerformanceLevelDescription']['account_tag_id']) {
                                        array_push($performance_level_descriptions_arr, $single_performance_level_description);
                                    }
                                endforeach;
                                $single_acc_tag_type_0['performance_level_descriptions'] = $performance_level_descriptions_arr;
                                $single_type_0_array[] = $single_acc_tag_type_0;
                            endforeach;
                            $framework_settings['account_tag_type_0'] = $single_type_0_array;
                        }
//                        $framework_settings['performance_level_descriptions'] = $performance_level_descriptions;
                    }
                }
            }
            if (!empty($framework_settings)) {
                // $result['status'] = true;
                //$result['msg'] = "Account framework settings fetched successfully.";
                return $framework_settings;
            } else {
                //$result['status'] = true;
                //$result['msg'] = "Nothing found for your request.";
                return false;
            }
        endif;
    }

    function _check_evaluator_permissions($huddle_id, $created_by) {
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $view = new View($this);
        if (!$view->Custom->check_if_eval_huddle($huddle_id)) {
            return true;
        }
        $is_avaluator = $view->Custom->check_if_evalutor($huddle_id, $user_id);
        $evaluators_ids = $view->Custom->get_evaluator_ids($huddle_id);
        $participants_ids = $view->Custom->get_participants_ids($huddle_id);
        $isEditable = '';
        if ($is_avaluator && $users['roles']['role_id'] == 120) {
            $isEditable = ($user_id == $created_by || (is_array($participants_ids) && in_array($created_by, $participants_ids)));
        } elseif ($is_avaluator && ($users['roles']['role_id'] == 110 || $users['roles']['role_id'] == 100 || $users['roles']['role_id'] == 115 )) {
            $isEditable = ($is_avaluator || ($user_id == $created_by || (is_array($evaluators_ids) && in_array($created_by, $evaluators_ids))));
        } else {
            $isEditable = (($user_id == $created_by || (is_array($evaluators_ids) && in_array($created_by, $evaluators_ids))));
        }
        return $isEditable;
    }

    function get_total_replies($parent_id) {
        $total_replies = 0;
        $result = $this->Comment->commentReplys($parent_id);
        if (count($result) > 0) {
            $total_replies = count($result);
            foreach ($result as $row) {
                if (isset($row['Comment']['id']) && $row['Comment']['id'] != '') {
                    $result2 = $this->Comment->commentReplys($row['Comment']['id']);
                    $total_replies = $total_replies + count($result2);
                }
            }
            return $total_replies;
        } else {
            return 0;
        }
    }

    function getDiscussionSearch($huddle_id, $title = '') {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('Huddles/view');

        $tab = 3;

        $huddle_info = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));

        //Set Discussions Tab Data.
        $discussionsData = array(
            'discussions' => $this->Comment->getCommentsSearch($huddle_id, $title, $_POST['sort']),
            'adminUsers' => $this->User->getUsersByRole($account_id, array('110', '100')),
            'huddle_id' => $huddle_id,
            'huddle' => $huddle_info,
            'user_id' => $user_id,
            'replys' => isset($discussionsReply) ? $discussionsReply : '',
            'addForm' => isset($addForm) ? $addForm : '',
            'key' => null,
            'tab' => $tab,
            'language_based_content' => $language_based_content
        );


        $view = new View($this, false);
        $html = $view->element('huddles/ajax/ajax_discussion_search', $discussionsData);
        echo $html;
        exit;
    }

    function get_people_data($huddle_id, $huddle_creator_id) {

        $huddle_uses = $this->AccountFolder->getHuddleUsers($huddle_id);
        $huddle_groups = $this->AccountFolder->getHuddleGroupsWithUsers($huddle_id);

        $peple_data = array(
            'admin' => array(),
            'member' => array(),
            'viewer' => array()
        );
        if (count($huddle_uses) > 0) {
            foreach ($huddle_uses as $row) {
                if ($row['huddle_users']['is_coach'] == 1)
                    $row['User']['is_coach'] = 1;
                else
                    $row['User']['is_coach'] = 0;
                if ($row['huddle_users']['is_mentee'] == 1)
                    $row['User']['is_coachee'] = 1;
                else
                    $row['User']['is_coachee'] = 0;
                if ($row['huddle_users']['role_id'] == 200 || $row['User']['id'] == $huddle_creator_id) {
                    $peple_data['admin']['users'][] = $row['User'];
                } elseif ($row['huddle_users']['role_id'] == 210) {
                    $peple_data['member']['users'][] = $row['User'];
                } elseif ($row['huddle_users']['role_id'] == 220) {
                    $peple_data['viewer']['users'][] = $row['User'];
                }
            }
        }
        if (count($huddle_groups) > 0) {
            foreach ($huddle_groups as $row) {
                if ($row['huddle_groups']['role_id'] == 200) {
                    $peple_data['admin']['group'][] = $row['Group'];
                } elseif ($row['huddle_groups']['role_id'] == 210) {
                    $peple_data['member']['group'][] = $row['Group'];
                } elseif ($row['huddle_groups']['role_id'] == 220) {
                    $peple_data['viewer']['group'][] = $row['Group'];
                }
            }
        }

        return $peple_data;
    }

    function _addUser($full_name, $email, $account_folder_id = '') {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $this->User->create();
        $arr = '';
        $data = '';
        if (isset($full_name)) {
            $name = explode(' ', $full_name);
            $arr['first_name'] = isset($name[0]) ? $name[0] : '';
            $arr['last_name'] = '';
            if (count($name) > 1) {
                array_shift($name);
                $arr['last_name'] = implode(" ", $name);
            }
        }
        if (isset($email)) {

            $arr['created_by'] = $users['User']['id'];
            $arr['created_date'] = date('Y-m-d H:i:s');
            $arr['last_edit_by'] = $users['User']['id'];
            $arr['last_edit_date'] = date('Y-m-d H:i:s');
            $arr['account_id'] = $account_id;
            $arr['email'] = $email;
            $arr['username'] = $email;
            $arr['authentication_token'] = $this->digitalKey(20);
            $arr['is_active'] = false;
            $arr['type'] = 'Invite_Sent';
            if ($arr['first_name'] != '' && $arr['email'] != '') {
                $data[] = $arr;
            }
        }

        $new_user_id = -1;
        $roleData = array();
        foreach ($data as $user) {
            if (isset($user['first_name']) && isset($user['email']) && $user['email']) {
                $exUser = $this->User->find('first', array('conditions' => array('email' => $user['email'])));
                $userAccountCount = 0;
                $this->User->create();
                if (count($exUser) == 0 && $this->User->save($user, $validation = TRUE)) {
                    $user_id = $this->User->id;

                    $this->User->updateAll(array(
                        'authentication_token' => "'" . md5($user_id) . "'"), array('id' => $user_id)
                    );

                    $user['authentication_token'] = md5($user_id);
                } else {
                    $user_id = $exUser['User']['id'];
                }

                if (!empty($user_id)) {

                    $userAccountCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id,
                            'account_id' => $account_id
                        )
                    ));
                    if ($userAccountCount == 0) {

                        $this->UserAccount->create();
                        //Checking default account is exist or not
                        $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id
                        )));
                        $userAccount = array(
                            'account_id' => $account_id,
                            'user_id' => $user_id,
                            'role_id' => '120',
                            'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                            'created_by' => $users['User']['id'],
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'last_edit_by' => $users['User']['id']
                        );

                        $this->UserAccount->save($userAccount, $validation = TRUE);

                        if ($user_id) {
                            $user['user_id'] = $user_id;
                            $user['message'] = $this->data['message'];
                            if (count($exUser) == 0) {
                                $this->sendInvitation($user, $account_folder_id);
                            } else {
                                $this->sendConfirmation($user);
                            }

                            $user_activity_logs = array(
                                'ref_id' => $account_folder_id,
                                'desc' => $user['first_name'] . " " . isset($user['last_name']) ? $user['last_name'] : '',
                                'url' => $this->base . 'Huddles',
                                'account_folder_id' => $account_folder_id,
                                'date_added' => date("Y-m-d H:i:s")
                            );
                            $this->user_activity_logs($user_activity_logs);

                            $new_user_id = $user_id;
                        }
                    }
                }
            }
        }

        return $new_user_id;
    }

    private function _getAllUsers($account_id, $include_session_users = false) {

        $users = $this->User->getUsersByRole($account_id, $this->role_user);
        $super_users = $this->User->getUsersByRole($account_id, $this->role_super_user);
        $account_owner_users = $this->User->getUsersByRole($account_id, 100);

        $result = array_merge($users, $super_users);
        $result = array_merge($result, $account_owner_users);

        if (is_array($result) && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    private function _getUsers($account_id, $include_session_users = false) {

        $users = $this->User->getUsersByRole($account_id, $this->role_user);

        if (is_array($users) && count($users) > 0) {
            return $users;
        } else {
            return

                    FALSE;
        }
    }

    private function _getSupperAdmins($account_id, $include_session_users = false) {

        $super_users = $this->User->getUsersByRole($account_id, $this->role_super_user);
        $account_owner_users = $this->User->getUsersByRole($account_id, 100);

        $result = array_merge($account_owner_users, $super_users);

        if (is_array($result) && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    private function _getViewer($account_id) {
        $viewers = $this->User->getUsersByRole($account_id, $this->role_viewer);
        if (is_array($viewers) && count($viewers) > 0) {
            return $viewers;
        } else {
            return FALSE;
        }
    }

    private function _getAdmin($account_id) {
        $super_admins = $this->User->getUsersByRole($account_id, $this->role_admin);
        if (is_array($super_admins) && count($super_admins) > 0) {
            return $super_admins;
        } else {
            return FALSE;
        }
    }

    private function _groups($account_id) {

        $groups = $this->Group->getGroups($account_id);
        if (is_array($groups) && count($groups) > 0) {
            return $groups;
        } else {
            return FALSE;
        }
    }

    public function _getHuddleUsers($account_folder_id) {
        return $this->AccountFolder->getHuddleUsers($account_folder_id);
    }

    private function _videoDetails($document_id) {
        return $this->Document->getVideoDetails($document_id);
    }

    private function _videoLiveDetails($document_id) {
        return $this->Document->getLiveVideoDetails($document_id);
    }

    function getVideoSearch($huddle_id, $title = '') {

        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $load_more = isset($this->request->data['load_more']) ? (int) $this->request->data['load_more'] : 0;
        $page = !empty($this->request->data['page']) ? (int) $this->request->data['page'] : 1;
        $sort_param = !isset($this->request->data['sort']) ? '' : $this->request->data['sort'];
        $title = Sanitize::escape(htmlspecialchars($title));

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('Huddles/view');
        $this->set('language_based_content', $language_based_content);

        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $view = new View($this, false);
        $evaluator = $view->Custom->check_if_evalutor($huddle_id, $user_id);

        if (!$evaluator && $htype == 3) {
            $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_id);
            array_push($evaluator_ids, $user_id);
            $item = $this->_getVideoList($huddle_id, $title, $sort_param, $this->video_per_page, $page - 1, $evaluator_ids);
        } else {
            $item = $this->_getVideoList($huddle_id, $title, $sort_param, $this->video_per_page, $page - 1);
        }

        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }


        $coachee_permission = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "coachee_permission")));
        $coachee_permissions = isset($coachee_permission[0]['AccountFolderMetaData']['meta_data_value']) ? $coachee_permission[0]['AccountFolderMetaData']['meta_data_value'] : "0";


        if (!$evaluator && $htype == 3) {
            $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_id);
            array_push($evaluator_ids, $user_id);
            $totalVideos = $this->Document->countVideos($huddle_id, '', $evaluator_ids);

            $element_data = array(
                'huddle_id' => $huddle_id,
                'videos' => $item,
                'coachee_permissions' => $coachee_permissions,
                'huddle_type' => $htype,
                'totalVideos' => $this->Document->countVideos($huddle_id, $title, $evaluator_ids),
                'video_per_page' => $this->video_per_page,
                'current_page' => $page,
                'language_based_content' => $language_based_content
            );
        } else {
            $totalVideos = $this->Document->countVideos($huddle_id);
            $element_data = array(
                'huddle_id' => $huddle_id,
                'videos' => $item,
                'coachee_permissions' => $coachee_permissions,
                'huddle_type' => $htype,
                'totalVideos' => $this->Document->countVideos($huddle_id, $title),
                'video_per_page' => $this->video_per_page,
                'current_page' => $page,
                'language_based_content' => $language_based_content
            );
        }


        $view = new View($this, false);
        $html = $view->element('ajax/videoSearch', $element_data);

        if (!$load_more) {
            $html = '<ul class="videos-list">' . $html . '</ul>';
            $html .= $view->element('load_more', array(
                'total_items' => $element_data['totalVideos'],
                'count_items' => count($element_data['videos']),
                'current_page' => $page,
                'number_per_page' => $this->video_per_page
            ));
            $html .= '<script>page = 2</script>';
        } elseif ($this->video_per_page * $page >= $element_data['totalVideos']) {
            $html .= '<script>hideLoadMore();</script>';
        }

        echo $html;
        exit;
    }

    private function _getVideoList($huddle_id, $title = '', $sort = '', $limit = 0, $page = 0, $evaluator_ids = 0) {
        if ($evaluator_ids) {
            $result = $this->Document->getVideos($huddle_id, $title, $sort, $limit, $page, $evaluator_ids);
        } else {
            $result = $this->Document->getVideos($huddle_id, $title, $sort, $limit, $page);
        }
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function changeTitle($account_folder_id, $document_id, $title = 'Untitled Video') {
//        $this->auth_huddle_permission('changeTitle', $account_folder_id);
//        $this->AccountFolderDocument->updateAll(array('title' => "'" . addslashes($title) . "'"), array('account_folder_id' => $account_folder_id, 'document_id' => $document_id));
//        if ($this->AccountFolderDocument->getAffectedRows()) {
//            echo (strlen($title) > 75 ? "<a title='" . $title . "'>" . substr($title, 0, 75) . "...</a>" : $title);
//        } else {
//            echo (strlen($title) > 75 ? "<a title='" . $title . "'>" . substr($title, 0, 75) . "...</a>" : $title);
//        }
//        exit;
        if (isset($this->request->data['title']) && $this->request->data['title'] != '') {
            $title = $this->request->data['title'];
        } else {
            $title = $title;
        }

        $this->AccountFolderDocument->updateAll(array('title' => '"' . Sanitize::escape(htmlspecialchars($title)) . '"'), array('account_folder_id' => $account_folder_id, 'document_id' => $document_id));
        if ($this->AccountFolderDocument->getAffectedRows() > 0) {
            echo(strlen($title) > 75 ? "<a title='" . $title . "'>" . substr($title, 0, 75) . "...</a>" : $title);
        } else {
            echo(strlen($title) > 75 ? "<a title='" . $title . "'>" . substr($title, 0, 75) . "...</a>" : $title);
        }
        exit;
    }

    function changeDate($account_folder_id, $document_id) {
        if ($this->request->data['recorded_date'] != '') {
            $this->Document->updateAll(array('recorded_date' => "'" . date('Y-m-d H:i:s', strtotime($this->request->data['recorded_date'])) . "'"), array('id' => $document_id));
            if ($this->Document->getAffectedRows()) {
                echo "<a title='" . date('M d, Y  H:i', strtotime($this->request->data['recorded_date'])) . "'>" . date('M d, Y  H:i', strtotime($this->request->data['recorded_date'])) . "</a>";
            } else {
                echo "<a title='" . date('M d, Y  H:i', strtotime($this->request->data['recorded_date'])) . "'>" . date('M d, Y  H:i', strtotime($this->request->data['recorded_date'])) . "</a>";
            }
        } else {
            echo "<a title='Select Session date'>No Session date</a>";
        }

        exit;
    }

    function update_view_count($document_id) {

        $this->layout = null;

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $video = $this->Document->find('first', array('conditions' => array('id' => $document_id)));
        $view_count = (int) $video['Document']['view_count'];
        $this->Document->updateAll(array('view_count' => ($view_count + 1)), array('id' => $document_id));
        $result_data = $this->AccountFolderDocument->get_account_folder_document($id);
        $account_folder_id = '';
        if( $result_data ){
            $account_folder_id = $result_data['AccountFolderDocument']['account_folder_id'];
        }
        
        if (isset($account_id) && isset($user_id)) {

            $user_activity_logs = array(
                'ref_id' => $document_id,
                'desc' => 'Video Viewed',
                'url' => '/' . $document_id,
                'type' => '11',
                'account_id' => $account_id,
                'account_folder_id' =>$account_folder_id,
                'environment_type' => 2,
            );
            $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
        }


        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function getVideoComments($video_id) {
        $searchCmt = isset($this->request->data['searchCmt']) ? $this->request->data['searchCmt'] : '';
        $tags = isset($this->request->data['tags']) ? substr($this->request->data['tags'], 0, -1) : '';
        $changeType = isset($this->data['srtType']) ? $this->data['srtType'] : 5;
        $this->Session->write('srtType', $changeType);
        $export_pdf_url = Configure::read('sibme_base_url') . 'Huddles/print_pdf_comments/' . $video_id . '/' . $changeType . '/2.pdf';

        $export_excel_url = Configure::read('sibme_base_url') . 'Huddles/print_excel_comments/' . $video_id . '/' . $changeType;
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_current_account = $this->Session->read('user_current_account');
        $huddle_id = $this->Session->read('account_folder_id');

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        $user_id = $users['User']['id'];
        if (isset($this->request->data['type']) && $this->request->data['type'] == '') {
            $result = $this->Comment->getVideoComments($video_id, $changeType, $huddle_id, $searchCmt, $tags, 1);
        } else {
            $result = $this->Comment->getVideoComments($video_id, $changeType, '', $searchCmt, $tags, 1);
        }
        $comments = array();
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        foreach ($result as $comment) {
            $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array('1', '2')); //get tags
            $view = new View($this, false);
            if ($htype == 3 || ($htype == 2 && $view->Custom->is_enabled_coach_feedback($huddle_id))) {    //coaching huddle feedback
                $view = new View($this, false);
                if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                    $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                } else {
                    if ($comment['Comment']['active'] == 1) {
                        $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                    }
                }
            } else {
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
            }
        }
        $result = $comments;


        $tagsArray = "";
        if ($this->request->data['type'] == 'get_video_comments_obs') {
            foreach ($result as $key => $comment):
                $commentText = $comment['Comment']['comment'];
                $commentFixed = $comment['Comment']['comment'];
                if ($commentText != '') {
                    foreach (explode(' ', $commentText) as $part) {
                        if (strstr(trim($part), '#')) {
                            $tempPart = str_replace('#', '', $part);
                            if ($this->Frameworks->findIfExixts($tempPart) > 0) {
                                $commentFixed = str_replace($part, '<a class="hashTagClickAble" href="javascript:openTagsDialog(\'' . $tempPart . '\')">' . $part . '</a>', $commentFixed);
                            }
                        }
                    }
                    $result[$key]['Comment']['comment'] = $commentFixed;
                }
            endforeach;

            if (isset($this->request->data['tagsArray']) && !$this->request->data['tagsArray'] == '') {
                $tagsArray = $this->request->data['tagsArray'];
                $keyToKeep = array();
                foreach ($result as $key => $comment):
                    $commentText = $comment['Comment']['comment'];
                    if ($commentText != '') {
                        foreach ($tagsArray as $part) {
                            if (strstr(trim($commentText), $part) === false) {
                                $keyToKeep[] = $key;
                            }
                        }
                    }
                endforeach;
                if (count($keyToKeep) > 0) {
                    $tagsArray = $keyToKeep;
                    foreach ($keyToKeep as $toKeep) {
                        unset($result[$toKeep]);
                    }
                } else {
                    $tagsArray = 'YYY';
                }
            } else {
                $tagsArray = "";
            }
        }
        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        $comments = array();
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array('1', '2')); //get tags
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
            }
        }
        $videoComments = $comments;
        $get_tags = $this->AccountTag->find("all", array("conditions" => array(
                "account_id" => $account_id,
                "tag_type" => '1'
        )));
        $total_tags = array();
        $count_tag = 0;

        $videoCommentsLoop = $videoComments;
        $event_type = isset($_POST['event_type']) ? $_POST['event_type'] : '';

        if (isset($event_type) && $event_type == 'group_button' || $event_type == 'search' || $event_type == 'clear' || $event_type == 'sorting') {

            $videoCommentsLoop = $this->Comment->getVideoComments($video_id, $changeType, $huddle_id, $searchCmt, '', 1);

            $comments = array();
            if (!empty($videoCommentsLoop)) {
                foreach ($videoCommentsLoop as $comment) {
                    $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                    $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array('1', '2')); //get tags
                    $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                }
            }

            $get_tags = $this->AccountTag->find("all", array("conditions" => array(
                    "account_id" => $account_id,
                    "tag_type" => '1'
            )));

            $videoCommentsLoop = $comments;
        }

        foreach ($get_tags as $tag) {
            $total_tag = 0;
            foreach ($videoCommentsLoop as $videos) {
                foreach ($videos['default_tags'] as $default_tags) {
                    if ($tag['AccountTag']['account_tag_id'] == $default_tags['AccountCommentTag']['account_tag_id']) {
                        $total_tag++;
                    }
                }
            }
            $total_tags[] = array("account_tag_id" => $tag['AccountTag']['account_tag_id'], "total" => $total_tag);
            $count_tag++;
        }

        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        $standardsL2 = array();
        $standards = array();
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }
        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $this->set('huddle_type', isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1");
        $standards_new = $this->get_framework_settings($framework_id);
        $params = array(
            'user_id' => $user_id,
            'video_id' => $video_id,
            'videoComments' => $videoComments,
            'huddle_permission' => $huddle_permission,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account,
            'sortType' => $changeType,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'type' => $this->request->data['type'],
            'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
            'defaulttags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
            "standards" => $standards,
            'standardsL2' => $standardsL2,
            'searchCmt' => $searchCmt,
            'ratings' => $metric_old,
            'framework_data' => $standards_new
        );

        $total_commnets_count = 0;
        if (count($videoComments) > 0) {

            for ($i = 0; $i < count($videoComments); $i++) {

                if (isset($videoComments[$i]['Comment'])) {

                    $item_comment_reply = $this->Comment->commentReplys($videoComments[$i]['Comment']['id']);
                    if (isset($item_comment_reply) && count($item_comment_reply) > 0) {
                        $total_commnets_count += 1;
                    }
                    $total_commnets_count += 1;
                }
            }
        }
        $total_comment = $total_commnets_count;
        $view = new View($this, false);

        $html = $view->element('ajax/vidComments', $params);

        $contents = array(
            'contents' => $html,
            "total_comment" => $total_comment,
            "tag_count" => $total_tags
        );
        echo json_encode($contents);
        exit;
    }

    function addComments() {
//        if($this->request->data['txtVideoTags']!='' && $this->data['assessment_value']!='')
//        {
//        $this->request->data['txtVideoTags'] = $this->request->data['txtVideoTags'].','.$this->data['assessment_value'];
//        }
//        elseif($this->request->data['txtVideoTags']=='' && $this->data['assessment_value']!='' ){
//            $this->request->data['txtVideoTags'] = $this->data['assessment_value'];
//        }
        if (empty($this->data['videoId']))
            die('miss videoId');

        $account_folder_document = $this->AccountFolderDocument->get($this->data['videoId']);

        if (empty($account_folder_document))
            die('invalid videoId');

        $account_folder_id = $account_folder_document['AccountFolderDocument']['account_folder_id'];
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle = $this->AccountFolder->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'account_id' => $account_id
            )
        ));
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_permissions = $this->Session->read('user_permissions');

        $isCreator = ($users['User']['id'] == $huddle['AccountFolder']['created_by']);
        $canComment = $isCreator || in_array($huddle_permission, array('200', '210')) ||
                $user_permissions['UserAccount']['permission_maintain_folders'] == 1;
        if (!$canComment) {
            die('do not have permission to comment');
        }

        $time = '';
        $accessLevel = '';
        if ($this->data['for'] == 'synchro_time' && $this->data['synchro_time'] != '') {
            $time = $this->data['synchro_time'];
        } else {
            $accessLevel = 'All';
        }
        if ($this->data['comment']['audio_comments'] == '') {
            $data = array(
                'comment' => $this->data['comment']['comment'],
                'time' => $time,
                'access_level' => $accessLevel,
                'ref_id' => $this->data['videoId'],
                'ref_type' => '2',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        } else {

            $data = array(
                'comment' => $this->data['comment']['audio_comments'],
                'time' => $time,
                'access_level' => $accessLevel,
                'ref_id' => $this->data['videoId'],
                'ref_type' => '6',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        }

        if ($this->Comment->save($data)) {
            if ($huddle['AccountFolder']['folder_type'] == 1) {
                $user_activity_logs = array(
                    'ref_id' => $this->Comment->id,
                    'desc' => $this->data['comment']['comment'],
                    'url' => $this->base . '/huddles/view/' . $account_folder_id . '/1/' . $this->data['videoId'],
                    'account_folder_id' => $account_folder_id,
                    'type' => '5',
                    'environment_type' => 2
                );
            } else {
                $user_activity_logs = array(
                    'ref_id' => $this->Comment->id,
                    'desc' => $this->data['comment']['comment'],
                    'url' => $this->base . '/MyFiles/view/1/' . $account_folder_id,
                    'account_folder_id' => $account_folder_id,
                    'type' => '5',
                    'environment_type' => 2
                );
            }

            $standards = isset($this->request->data['txtVideostandard']) ? $this->request->data['txtVideostandard'] : '';
            if (!empty($standards)) {
                $this->save_standard_tag($standards, $this->Comment->id, $this->data['videoId']);
            }
            $tags = isset($this->request->data['txtVideoTags']) ? $this->request->data['txtVideoTags'] : '';
            if (!empty($tags)) {
                $this->save_tag($tags, $this->Comment->id, $this->data['videoId']);
            }
            $tags_1 = isset($this->data['assessment_value']) ? $this->data['assessment_value'] : '';
            if (!empty($tags_1)) {
                $this->save_tag_1($tags_1, $this->Comment->id, $this->data['videoId']);
            }

            $notifiable = isset($this->data['notifiable']) ? (int) $this->data['notifiable'] : 1;

            if ($notifiable && $account_folder_id != '') {

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
                $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id);

                $huddle_user_ids = array();
                if ($userGroups && count($userGroups) > 0) {
                    foreach ($userGroups as $row) {
                        $huddle_user_ids[] = $row['user_groups']['user_id'];
                    }
                }
                if ($huddleUsers && count($huddleUsers) > 0) {
                    foreach ($huddleUsers as $row) {
                        $huddle_user_ids[] = $row['huddle_users']['user_id'];
                    }
                }

                if (count($huddle_user_ids) > 0) {
                    $user_ids = implode(',', $huddle_user_ids);
                    if (!empty($user_ids))
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                } else {
                    $huddleUserInfo = array();
                }

                if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                    $commentsData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'comment' => $this->data['comment']['comment'],
                        'video_link' => '/huddles/view/' . $account_folder_id . '/1/' . $this->data['videoId'],
                        'participating_users' => $huddleUserInfo
                    );

                    foreach ($huddleUserInfo as $row) {
                        if ($user_id == $row['User']['id']) {
                            continue;
                        }
                        $commentsData ['email'] = $row['User']['email'];
                        if ($this->check_subscription($row['User']['id'], '6', $account_id)) {
                            $commentsData['comment_id'] = $this->Comment->id;
                            $this->sendVideoComments($commentsData, $row['User']['id']);
                        }
                    }
                }
            }

            $this->user_activity_logs($user_activity_logs);
            echo "success";
            exit;
        } else {
            echo "failed";
            exit;
        }
    }

    function addLiveComments() {
//        if($this->request->data['txtVideoTags']!='' && $this->data['assessment_value']!='')
//        {
//        $this->request->data['txtVideoTags'] = $this->request->data['txtVideoTags'].','.$this->data['assessment_value'];
//        }
//        elseif($this->request->data['txtVideoTags']=='' && $this->data['assessment_value']!='' ){
//            $this->request->data['txtVideoTags'] = $this->data['assessment_value'];
//        }
        if (empty($this->data['videoId']))
            die('miss videoId');

        $account_folder_document = $this->AccountFolderDocument->get($this->data['videoId']);

        if (empty($account_folder_document))
            die('invalid videoId');

        $account_folder_id = $account_folder_document['AccountFolderDocument']['account_folder_id'];
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle = $this->AccountFolder->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'account_id' => $account_id
            )
        ));
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_permissions = $this->Session->read('user_permissions');

        $isCreator = ($users['User']['id'] == $huddle['AccountFolder']['created_by']);
        $canComment = $isCreator || in_array($huddle_permission, array('200', '210')) ||
                $user_permissions['UserAccount']['permission_maintain_folders'] == 1;
        if (!$canComment) {
            die('do not have permission to comment');
        }

        $time = '';
        $accessLevel = '';
        if ($this->data['for'] == 'synchro_time' && $this->data['synchro_time'] != '') {
            $time = $this->data['synchro_time'];
        } else {
            $accessLevel = 'All';
        }
        if ($this->data['comment']['audio_comments'] == '') {
            $data = array(
                'comment' => $this->data['comment']['comment'],
                'time' => $time,
                'access_level' => $accessLevel,
                'ref_id' => $this->data['videoId'],
                'ref_type' => '2',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        } else {

            $data = array(
                'comment' => $this->data['comment']['audio_comments'],
                'time' => $time,
                'access_level' => $accessLevel,
                'ref_id' => $this->data['videoId'],
                'ref_type' => '6',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        }

        if ($this->Comment->save($data)) {


            $token_ids = $this->get_device_token_ids($account_folder_id, $user_id);
            $user_data = $this->User->find('first', array(
                'conditions' => array(
                    'id' => $user_id
                )
            ));
            foreach ($token_ids as $token_id) {
                $title = 'Video Comment is added in Live Video';
                $desc = 'Video Comment is added in Live Video';
                $title = '';
                $desc = '';
                $data_array = array(
                    'video_id' => $this->data['videoId'],
                    'huddle_id' => $account_folder_id,
                    'notification_type' => '5',
                    'comment' => $this->data['comment']['comment'],
                    'user' => $user_data['User']
                );
                $this->push_notification_test($token_id['apns_token'], $title, $desc, $data_array, $token_id['device_type']);
            }


            if ($huddle['AccountFolder']['folder_type'] == 1) {
                $user_activity_logs = array(
                    'ref_id' => $this->Comment->id,
                    'desc' => $this->data['comment']['comment'],
                    'url' => $this->base . '/huddles/view/' . $account_folder_id . '/1/' . $this->data['videoId'],
                    'account_folder_id' => $account_folder_id,
                    'type' => '5',
                    'environment_type' => 2
                );
            } else {
                $user_activity_logs = array(
                    'ref_id' => $this->Comment->id,
                    'desc' => $this->data['comment']['comment'],
                    'url' => $this->base . '/MyFiles/view/1/' . $account_folder_id,
                    'account_folder_id' => $account_folder_id,
                    'type' => '5',
                    'environment_type' => 2
                );
            }

            $standards = isset($this->request->data['txtVideostandard']) ? $this->request->data['txtVideostandard'] : '';
            if (!empty($standards)) {
                $this->save_standard_tag($standards, $this->Comment->id, $this->data['videoId']);
            }
            $tags = isset($this->request->data['txtVideoTags']) ? $this->request->data['txtVideoTags'] : '';
            if (!empty($tags)) {
                $this->save_tag($tags, $this->Comment->id, $this->data['videoId']);
            }
            $tags_1 = isset($this->data['assessment_value']) ? $this->data['assessment_value'] : '';
            if (!empty($tags_1)) {
                $this->save_tag_1($tags_1, $this->Comment->id, $this->data['videoId']);
            }

            $notifiable = isset($this->data['notifiable']) ? (int) $this->data['notifiable'] : 1;

            if ($notifiable && $account_folder_id != '') {

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
                $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id);

                $huddle_user_ids = array();
                if ($userGroups && count($userGroups) > 0) {
                    foreach ($userGroups as $row) {
                        $huddle_user_ids[] = $row['user_groups']['user_id'];
                    }
                }
                if ($huddleUsers && count($huddleUsers) > 0) {
                    foreach ($huddleUsers as $row) {
                        $huddle_user_ids[] = $row['huddle_users']['user_id'];
                    }
                }

                if (count($huddle_user_ids) > 0) {
                    $user_ids = implode(',', $huddle_user_ids);
                    if (!empty($user_ids))
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                } else {
                    $huddleUserInfo = array();
                }

                if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                    $commentsData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'comment' => $this->data['comment']['comment'],
                        'video_link' => '/huddles/view/' . $account_folder_id . '/1/' . $this->data['videoId'],
                        'participating_users' => $huddleUserInfo
                    );

                    foreach ($huddleUserInfo as $row) {
                        if ($user_id == $row['User']['id']) {
                            continue;
                        }
                        $commentsData ['email'] = $row['User']['email'];
                        if ($this->check_subscription($row['User']['id'], '6', $account_id)) {
                            $commentsData['comment_id'] = $this->Comment->id;
                            $this->sendVideoComments($commentsData, $row['User']['id']);
                        }
                    }
                }
            }

            $this->user_activity_logs($user_activity_logs);
            echo "success";
            exit;
        } else {
            echo "failed";
            exit;
        }
    }

    function get_device_token_ids($huddle_id, $user_id) {
        $token_ids = array();
        $view = new View($this, false);
        $participant_ids = $view->Custom->get_all_participant_ids($huddle_id, $user_id);
        $count = 0;
        foreach ($participant_ids as $participant_id) {

            $user_device_log = $this->UserDeviceLog->find('first', array(
                'conditions' => array(
                    'user_id' => $participant_id
                )
            ));

            if (!empty($user_device_log)) {
                $token_ids[$count]['apns_token'] = $user_device_log['UserDeviceLog']['apns_token'];
                $token_ids[$count]['device_type'] = $user_device_log['UserDeviceLog']['device_type'];
                $count++;
            }
        }

        return $token_ids;
    }

    function push_notification_test($deviceToken, $title, $desc, $data_array, $device_type) {
        $object = new PushNotifications();
        $msg_payload = array(
            'mtitle' => $title,
            'mdesc' => $desc,
            'required_data' => $data_array
        );

        if ($device_type == '1') {
            return $object->iOS($msg_payload, $deviceToken);
        } else {
            return $object->android($msg_payload, $deviceToken);
        }
    }

    function ob_addComments() {
        if (empty($this->data['videoId']))
            die('miss videoId');
        if (empty($this->data['detail_id']))
            die('Detail ID Missing');
        // $account_folder_document = $this->AccountFolderDocument->get($this->data['videoId']);

        if (empty($this->request->data['huddle_id']))
            die('invalid videoId');
        $account_folder_id = $this->request->data['huddle_id']; //$account_folder_document['AccountFolderDocument']['account_folder_id'];
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle = $this->AccountFolder->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'account_id' => $account_id
            )
        ));

        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_permissions = $this->Session->read('user_permissions');

        $isCreator = ($users['User']['id'] == $huddle['AccountFolder']['created_by']);
        $canComment = $isCreator || in_array($huddle_permission, array('200', '210')) ||
                $user_permissions['UserAccount']['permission_maintain_folders'] == 1;
        if (!$canComment) {
            die('do not have permission to comment');
        }

        $time = '';
        $accessLevel = '';
        if ($this->data['for'] == 'synchro_time' && $this->data['synchro_time'] != '') {
            $time = $this->data['synchro_time'];
        } else {
            $accessLevel = 'All';
        }

        $data = array(
            'comment' => $this->data['comment']['comment'],
            'time' => $time,
            'access_level' => $accessLevel,
            'ref_id' => $this->data['detail_id'],
            'ref_type' => '2',
            'created_date' => date('y-m-d H:i:s', time()),
            'created_by' => $user_id,
            'last_edit_date' => date('y-m-d H:i:s', time()),
            'last_edit_by' => $user_id,
            'user_id' => $user_id
        );

        if ($this->Comment->save($data)) {
            $user_activity_logs = array(
                'ref_id' => $this->Comment->id,
                'desc' => $this->data['comment']['comment'],
                'url' => $this->base . '/huddles/view/' . $account_folder_id . '/5/' . $this->data['detail_id'],
                'account_folder_id' => $account_folder_id,
                'type' => '5'
            );

            $notifiable = isset($this->data['notifiable']) ? (int) $this->data['notifiable'] : 1;

            if ($notifiable && $account_folder_id != '') {
                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
                $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id);
                $huddle_user_ids = array();
                if ($userGroups && count($userGroups) > 0) {
                    foreach ($userGroups as $row) {
                        $huddle_user_ids[] = $row['user_groups']['user_id'];
                    }
                }
                if ($huddleUsers && count($huddleUsers) > 0) {
                    foreach ($huddleUsers as $row) {
                        $huddle_user_ids[] = $row['huddle_users']['user_id'];
                    }
                }

                if (count($huddle_user_ids) > 0) {
                    $user_ids = implode(',', $huddle_user_ids);
                    $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                } else {
                    $huddleUserInfo = array();
                }

                if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                    $commentsData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'comment' => $this->data['comment']['comment'],
                        'video_link' => '/huddles/view/' . $account_folder_id . '/5/' . $this->data['detail_id'],
                        'participating_users' => $huddleUserInfo
                    );
                    foreach ($huddleUserInfo as $row) {
                        if ($user_id == $row['User']['id']) {
                            continue;
                        }
                        $commentsData ['email'] = $row['User']['email'];
                        if ($this->check_subscription($row['User']['id'], '6', $account_id)) {
                            $commentsData['comment_id'] = $this->Comment->id;
                            $this->sendVideoComments($commentsData, $row['User']['id']);
                        }
                    }
                }
            }
            $this->user_activity_logs($user_activity_logs);
            echo "success";
            exit;
        } else {
            echo "failed";
            exit;
        }
    }

    function editComments() {
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $comment_data = $this->Comment->find('first', array('conditions' => array('id' => $this->data['comment_id'])));
        if (isset($this->request->data['time_hours']) && isset($this->request->data['time_minutes']) && isset($this->request->data['time_seconds'])) {
            $observations_time = $this->request->data['time_hours'] . ':' . $this->request->data['time_minutes'] . ':' . $this->request->data['time_seconds'];
            $time = $this->time_to_decimal($observations_time);
        } else {
            $time = '';
        }
        if ($comment_data['Comment']['ref_type'] == '6') {
            $data = array(
                'last_edit_date' => "'" . date('y-m-d H:i:s', time()) . "'",
                'last_edit_by' => $user_id
            );
        } else {
            if ($time != '') {
                $data = array(
                    'comment' => "'" . Sanitize::escape($this->data['comment']['comment']) . "'",
                    'last_edit_date' => "'" . date('y-m-d H:i:s', time()) . "'",
                    'last_edit_by' => $user_id,
                    'time' => $time
                );
            } else {
                $data = array(
                    'comment' => "'" . Sanitize::escape($this->data['comment']['comment']) . "'",
                    'last_edit_date' => "'" . date('y-m-d H:i:s', time()) . "'",
                    'last_edit_by' => $user_id,
                );
            }
        }
//        echo "<pre>";
//        print_r($this->data);
//        die;

        if ($this->Comment->updateAll($data, array('id' => $this->data['comment_id']))) {
            $txtVidTag = 'txtVideoTags1' . $this->data['comment_id'];
            $txtVidstandard = 'txtVideostandard1' . $this->data['comment_id'];

            $tags_1 = isset($this->data['assessment_value']) ? $this->data['assessment_value'] : '';
            if (!empty($tags_1)) {
                $this->save_tag_2($tags_1, $this->data['comment_id'], $this->data['videoId']);
            }
            $tags = isset($this->request->data[$txtVidTag]) ? $this->request->data[$txtVidTag] : '';
            $standards = isset($this->request->data[$txtVidstandard]) ? $this->request->data[$txtVidstandard] : '';

            if (!empty($tags)) {
                $this->update_tag($tags, $this->data['comment_id'], $this->data['videoId']);
            } else {
                $this->AccountCommentTag->deleteAll(array('comment_id' => $this->data['comment_id'], "ref_type" => "1"), $cascade = true, $callbacks = true);
            }
            if (!empty($standards)) {
                $this->AccountCommentTag->deleteAll(array('comment_id' => $this->data['comment_id'], "ref_type" => "0"), $cascade = true, $callbacks = true);
                $this->save_standard_tag($standards, $this->data['comment_id'], $this->data['videoId']);
            } else {
                $this->AccountCommentTag->deleteAll(array('comment_id' => $this->data['comment_id'], "ref_type" => "0"), $cascade = true, $callbacks = true);
            }
            echo "success";
            exit;
        } else {
            echo "failed";
            exit;
        }
    }

    function addReply($parentId, $comment_type = '') {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $commentDocId = $this->Comment->find('first', array('conditions' => array('id' => $parentId)));
        $user_id = $users['User']['id'];
        if ($this->data['comment']['audio_comments'] == '') {
            $data = array(
                'comment' => $this->data['comment']['comment'],
                'access_level' => $this->data['comment']['access_level'],
                //'ref_id' => $parentId,
                'ref_id' => $commentDocId['Comment']['ref_id'],
                'parent_comment_id' => $parentId,
                'ref_type' => '3',
                'created_date' => date('y-m-d h:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d h:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        } else {
            $data = array(
                'comment' => $this->data['comment']['audio_comment'],
                'access_level' => $this->data['comment']['access_level'],
                //'ref_id' => $parentId,
                'ref_id' => $commentDocId['Comment']['ref_id'],
                'parent_comment_id' => $parentId,
                'ref_type' => '6',
                'created_date' => date('y-m-d h:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d h:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        }
        if ($this->Comment->save($data)) {
            $user_activity_logs = array(
                'desc' => $this->data['comment']['comment'],
                'url' => $this->base . 'comments/view/' . $this->Comment->id,
                'account_folder_id' => $parentId,
                'ref_id' => $this->Comment->id,
                'type' => 8,
                'environment_type' => 2
            );

            $account_folder_id = $this->Session->read('account_folder_id');
            $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
            $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
            $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id);

            $huddle_user_ids = array();
            if ($userGroups && count($userGroups) > 0) {
                foreach ($userGroups as $row) {
                    $huddle_user_ids[] = $row['user_groups']['user_id'];
                }
            }
            if ($huddleUsers && count($huddleUsers) > 0) {
                foreach ($huddleUsers as $row) {
                    $huddle_user_ids[] = $row['huddle_users']['user_id'];
                }
            }

            if (count($huddle_user_ids) > 0) {
                $user_ids = implode(',', $huddle_user_ids);
                if (!empty($user_ids))
                    $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
            } else {
                $huddleUserInfo = array();
            }

            $reciver_detail = $this->User->find('first', array('conditions' => array('id' => $commentDocId['Comment']['user_id'])));

            $commentsData = array(
                'account_folder_id' => $account_folder_id,
                'huddle_name' => $huddle[0]['AccountFolder']['name'],
                'comment' => $this->data['comment']['comment'],
                'video_link' => '/huddles/view/' . $account_folder_id . '/1/' . $commentDocId['Comment']['ref_id'],
                'participating_users' => $huddleUserInfo,
                'email' => $reciver_detail['User']['email'],
                'comment_id' => $parentId,
            );
            if ($this->check_subscription($commentDocId['Comment']['user_id'], '6', $account_id)) {
                $this->sendVideoReplies($commentsData, $commentDocId['Comment']['user_id']);
            }

            $this->user_activity_logs($user_activity_logs);
            echo $this->Comment->id;
            exit;
        } else {
            echo "faild";

            exit;
        }
    }

    function deleteVideoComments($videoId, $current_video_id = '') {
        $this->Comment->deleteAll(array('id' => $videoId), $cascade = true, $callbacks = true);
        $comment_tags = $this->AccountCommentTag->find('all', array('conditions' => array('comment_id' => $videoId)));

        if (!empty($comment_tags)) {
            foreach ($comment_tags as $comment_tag) {
                $this->AccountCommentTag->deleteAll(array('account_comment_tag_id' => $comment_tag['AccountCommentTag']['account_comment_tag_id']), $cascade = true, $callbacks = true);

                $bool = 1;

                $comments_video_related = $this->Comment->find('all', array('conditions' => array('ref_id' => $current_video_id)));

                foreach ($comments_video_related as $row) {
                    $comments_related_tags = $this->AccountCommentTag->find('all', array('conditions' => array('comment_id' => $row['Comment']['id'], 'account_tag_id' => $comment_tag['AccountCommentTag']['account_tag_id'])));
                    if (count($comments_related_tags) > 0) {
                        $bool = 0;
                    }
                }
                if ($bool) {
                    $this->DocumentStandardRating->deleteAll(array(
                        "standard_id" => $comment_tag['AccountCommentTag']['account_tag_id'],
                        "document_id" => $current_video_id,
                    ));
                }
            }
        }
        $is_deleted = $this->Comment->find('count', array('conditions' => array('id' => $videoId)));

        if ($is_deleted == 0) {

            if (isset($this->request->data['internal_comment_id'])) {

                $video_id = $this->request->data['video_id'];
                $comment_id = $this->request->data['comment_id'];

                $result = array(
                    'document_id' => $video_id,
                    'status' => 'success',
                    'internal_comment_id' => $this->request->data['internal_comment_id']
                );

                echo json_encode($result);
            } else {
                echo "success";
            }
        } else {
            echo "failed";
        }
        exit;
    }

    function deleteCommentTag($tagId) {
        $this->AccountCommentTag->deleteAll(array('account_comment_tag_id' => $tagId), $cascade = true, $callbacks = true);
        //$this->AccountCommentTag->deleteAll(array('account_comment_tag_id' => $tagId));
        if ($this->AccountCommentTag->getAffectedRows()) {
            if (isset($this->request->data['internal_comment_id'])) {
                $video_id = $this->request->data['video_id'];
                $comment_id = $this->request->data['comment_id'];

                $result = array(
                    'document_id' => $video_id,
                    'status' => 'success',
                    'internal_comment_id' => $this->request->data['internal_comment_id'],
                    'comments' => $this->getObComment($video_id, $comment_id)
                );

                echo json_encode($result);
            } else {
                echo "success";
            }
        } else {
            echo "failed";
        }
        exit;
    }

    function associateVideoDocuments($document_id, $account_folder_id) {
        $video_ids = $this->request->data['associated_videos'];
        $video_ids_array = explode(',', $video_ids);

        $account_folder_documents = $this->AccountFolderDocument->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'document_id' => $document_id
            )
        ));

        $this->AccountFolderDocumentAttachment->deleteAll(array(
            'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id']
        ));

        for ($i = 0; $i < count($video_ids_array); $i++) {
            $video_id = $video_ids_array[$i];
            if (!empty($video_id)) {
                $this->AccountFolderDocumentAttachment->create();
                $data = array(
                    'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id'],
                    'attach_id' => $video_id
                );
                $this->AccountFolderDocumentAttachment->save($data, $validation = TRUE);
            }
        }

        $this->layout = null;
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function getDocumentSearch($huddle_id, $title = '') {
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];

        $huddle_info = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));
        if ($huddle_info['AccountFolder']['active'] == 0) {
            die('');
        }

        $totalVideos = $this->Document->countVideos($huddle_id);
        // prevent performance when associated a mass of videos
        // @todo: update new for decrease performance impact
        if ($totalVideos > 200) {
            $documentVideos = $this->Document->getVideos($huddle_id, '', '', 200);
        } else {
            $documentVideos = $this->Document->getVideos($huddle_id);
        }

        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";

        $documents = array();

        $view = new View($this, false);

        if (!$view->Custom->check_if_evalutor($huddle_id, $user_id) && $htype == '2') {

            $coachee_documents = $this->Document->getCoacheeDocuments($huddle_id, $title, $this->request->data('sort'));

            foreach ($coachee_documents as $document) {
                $temp_array = array();
                $temp_array['Document'] = $document[0];
                $temp_array['afd']['title'] = $document[0]['title'];
                $temp_array['afd']['desc'] = $document[0]['desc'];
                $temp_array['afd']['id'] = $document[0]['afdid'];
                $temp_array['User']['first_name'] = $document[0]['first_name'];
                $temp_array['User']['last_name'] = $document[0]['last_name'];
                $temp_array['User']['email'] = $document[0]['email'];

                $documents[] = $temp_array;
            }
        } else {
            $documents = $this->Document->getDocuments($huddle_id, $title, $this->request->data('sort'));
        }

        $documentData = array(
            'documents' => $documents,
            'associatedVideos' => $this->Document->getDocumentAssociatedVideos($huddle_id),
            'videos' => $documentVideos,
            'huddle_id' => $huddle_id,
            'huddle' => $huddle_info,
            'user_id' => $user_id,
        );

        $view = new View($this, false);
        $html = $view->element('ajax/docSearch', $documentData);
        echo $html;
        exit;
    }

    public function restore() {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $user_account = $this->UserAccount->find('first', array(
            'conditions' => array(
                'account_id' => $account_id,
                'user_id' => $user_id
            )
        ));

        if (empty($user_account) || $user_account['UserAccount']['role_id'] != $this->role_account_owner)
            $this->redirect('/');

        $sample_huddle_id = Configure::read('sample_huddle');

        $this->restoreSampleHuddle(
                $account_id, $user_id, $sample_huddle_id
        );

        $this->Session->setFlash($this->language_based_messages['sample_huddle_has_been_restored_successfully'], 'default', array('class' => 'message success'));
        $this->redirect('/Huddles');
    }

    function edit($huddle_id) {
        return $this->redirect('/add_huddle_angular/edit/' . $huddle_id, 301);
        $this->layout = 'huddles';
        $huddle_info = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));

        if ($huddle_info['AccountFolder']['parent_folder_id'] != '0' && !empty($huddle_info['AccountFolder']['parent_folder_id'])) {
            $folder_details = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_info['AccountFolder']['parent_folder_id'])));
            $this->set('parent_folder_name', $folder_details);
            $folder_label = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $folder_details['AccountFolder']['account_folder_id']), 'fields' => array('name')));

            $crumb_output = $this->add_new_folder_breadcrum($folder_label['AccountFolder']['name'] . '_0f', $folder_details['AccountFolder']['account_folder_id']);

            $this->set("breadcrumb", $crumb_output);
        }
        $this->set('current_huddle_info', $huddle_info);

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                "tag_type" => '2', "account_id" => $account_id
        )));
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
        }

        $correct_folder_users = $this->AccountFolderUser->find('all', array(
            'conditions' => array(
                'account_folder_id' => $huddle_id
            )
        ));
        $huddle_data = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));

        $folder_type = $huddle_data['AccountFolder']['folder_type'];
        $result = $this->get_all_users($account_id, $folder_type);

        if (!empty($correct_folder_users)) {
            foreach ($correct_folder_users as $correct_user) {
                if ($correct_user['AccountFolderUser']['role_id'] == '200') {
                    $data = array('is_coach' => 1, 'is_mentee' => 0);
                    $this->AccountFolderUser->updateAll($data, array('account_folder_user_id' => $correct_user['AccountFolderUser']['account_folder_user_id']));
                } elseif ($correct_user['AccountFolderUser']['role_id'] == '210') {
                    $data = array('is_coach' => 0, 'is_mentee' => 1);
                    $this->AccountFolderUser->updateAll($data, array('account_folder_user_id' => $correct_user['AccountFolderUser']['account_folder_user_id']));
                }
            }
        }


        //for getting current framework_id

        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);

        $framework_id = '';
        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $this->set('framework_id', $framework_id);
        //end here

        $all_users_data = array();
        if (count($result) > 0) {
            foreach ($result as $row) {
                if ($row['is_user'] == 'group') {
                    $res = $this->AccountFolderGroup->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'group_id' => $row['id'])));
                    $row['is_coach'] = isset($res['AccountFolderGroup']['is_coach']) ? $res['AccountFolderGroup']['is_coach'] : '0';
                    $row['is_mentee'] = isset($res['AccountFolderGroup']['is_mentee']) ? $res['AccountFolderGroup']['is_mentee'] : '0';
                } elseif ($row['is_user'] == 'member' || $row['is_user'] == 'admin') {
                    $res = $this->AccountFolderUser->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'user_id' => $row['id'])));
                    $row['is_coach'] = isset($res['AccountFolderUser']['is_coach']) ? $res['AccountFolderUser']['is_coach'] : '0';
                    $row['is_mentee'] = isset($res['AccountFolderUser']['is_mentee']) ? $res['AccountFolderUser']['is_mentee'] : '0';
                }
                $all_users_data[] = $row;
            }
        }

        $this->set('users_record', $all_users_data);
        $this->auth_huddle_permission('edit', $huddle_id);
        $huddle = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));
        $user_id = $users['User']['id'];
        $this->set('huddle_id', $huddle_id);
        $this->set('huddles', $huddle);
        $chk_frameworks = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "chk_frameworks")));
        $this->set('chk_frameworks', isset($chk_frameworks[0]['AccountFolderMetaData']['meta_data_value']) ? $chk_frameworks[0]['AccountFolderMetaData']['meta_data_value'] : "0");

        $coachee_permission = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "coachee_permission")));
        $this->set('coachee_permission', isset($coachee_permission[0]['AccountFolderMetaData']['meta_data_value']) ? $coachee_permission[0]['AccountFolderMetaData']['meta_data_value'] : "0");


        $coach_hud_feedback = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "coach_hud_feedback")));
        $this->set('coach_hud_feedback', isset($coach_hud_feedback[0]['AccountFolderMetaData']['meta_data_value']) ? $coach_hud_feedback[0]['AccountFolderMetaData']['meta_data_value'] : "0");

        $chk_tags = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "chk_tags")));
        $this->set('chk_tags', isset($chk_tags[0]['AccountFolderMetaData']['meta_data_value']) ? $chk_tags[0]['AccountFolderMetaData']['meta_data_value'] : "0");

        $chk_folder_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "chk_frameworks")));
        $this->set('huddle_message', $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'message'))));
        /*
         * huddle_type 1=Callaboration huddle
         *             2=Coachee huddle
         */
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $this->set('huddle_type', isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1");
        if (isset($h_type['AccountFolderMetaData']['meta_data_value']) && $h_type['AccountFolderMetaData']['meta_data_value'] == 3) {
            $submission_deadline_time = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'submission_deadline_time')));
            $this->set('submission_deadline_time', isset($submission_deadline_time['AccountFolderMetaData']['meta_data_value']) ? $submission_deadline_time['AccountFolderMetaData']['meta_data_value'] : date('H:i'));
            $submission_deadline_date = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'submission_deadline_date')));
            $this->set('submission_deadline_date', isset($submission_deadline_date['AccountFolderMetaData']['meta_data_value']) ? $submission_deadline_date['AccountFolderMetaData']['meta_data_value'] : date('m-d-Y'));
            $submission_allowed = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'submission_allowed')));
            $this->set('submission_allowed', isset($submission_allowed['AccountFolderMetaData']['meta_data_value']) ? $submission_allowed['AccountFolderMetaData']['meta_data_value'] : 1);
        }

        $this->set('huddleUsers', $this->AccountFolderUser->find('all', array('conditions' => array('account_folder_id' => $huddle_id, 'role_id' => '210'))));
        $this->set('huddleViewers', $this->AccountFolderUser->find('all', array('conditions' => array('account_folder_id' => $huddle_id, 'role_id' => '220'))));
        $this->set('supperUsers', $this->AccountFolderUser->find('all', array('conditions' => array('account_folder_id' => $huddle_id, 'role_id' => '200'))));
        $this->set('groups', $this->AccountFolderGroup->find('all', array('conditions' => array('account_folder_id' => $huddle_id))));

        $this->set('users', $this->_getAllUsers($account_id, true));
        $this->set('super_users', $this->_getAllUsers($account_id, true));
        $this->set('users_groups', $this->_groups($account_id));
        $this->set('user_id', $users['User']['id']);
        $this->set('account_id', $account_id);
        $created_by = '';
        if (isset($huddle) && $huddle != '') {
            $created_by = $huddle['AccountFolder']['created_by'];
        }

        if ($this->request->is('post')) {

            $default_framework = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "default_framework"
            )));
            if (isset($default_framework['AccountMetaData'])) {
                $default_framwork_value = $default_framework['AccountMetaData']['meta_data_value'];
            }

            if (isset($this->request->data['frameworks']) && $this->request->data['frameworks'] != $framework_id) {

                $result = $this->AccountFolderDocument->find("all", array("conditions" => array(
                        "account_folder_id" => $huddle_id
                )));
                //print_r($result);
                $document_ids = array();
                foreach ($result as $res) {
                    $document_ids[] = $res['AccountFolderDocument']['document_id'];
                }
                foreach ($document_ids as $document_id) {
                    $this->AccountCommentTag->deleteAll(array(
                        "ref_id" => $document_id, "ref_type" => 0
                    ));
                }
                if ($framework_id == 0 || $framework_id == -1) {
                    $framework_all_standards = $this->AccountTag->find("all", array("conditions" => array(
                            "framework_id is NULL",
                            "account_id" => $account_id,
                            "tag_type" => 0
                    )));
                } else {
                    $framework_all_standards = $this->AccountTag->find("all", array("conditions" => array(
                            "framework_id" => $framework_id,
                            "account_id" => $account_id,
                            "tag_type" => 0
                    )));
                }

                foreach ($framework_all_standards as $row) {
                    $this->DocumentStandardRating->deleteAll(array(
                        "standard_id" => $row['AccountTag']['account_tag_id'],
                        "account_id" => $account_id,
                        "account_folder_id" => $huddle_id
                    ));
                }
            }
            $superUsersIds = isset($this->data['super_admin_ids']) ? $this->data['super_admin_ids'] : '';
            $groupIds = isset($this->data['group_ids']) ? $this->data['group_ids'] : '';

            if (empty($this->request->data['name'])) {
                $this->Session->setFlash($this->language_based_messages['please_enter_the_huddle_name'], 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/edit/' . $huddle_id);
            }

            if (!isset($this->request->data['message']))
                $this->request->data['message'] = "";

            if (strlen($this->request->data['message']) > 1024) {
                $this->Session->setFlash($this->language_based_messages['please_limit_to_1024_characters_or_less_currently'] . strlen($this->request->data['message']), 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/edit/' . $huddle_id);
            }

            $this->request->data['created_date'] = date("Y-m-d H:i:s");
            $this->set('user_id', $users['User']['id']);
            $this->AccountFolder->create();
            $data = array(
                'name' => "'" . addslashes($this->request->data['name']) . "'",
                'desc' => "'" . addslashes($this->request->data['description']) . "'",
                'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                'last_edit_by' => $users['User']['id']
            );

            $this->AccountFolder->updateAll($data, array('account_folder_id' => $huddle_id));
            if ($this->AccountFolder->getAffectedRows() > 0) {
                $huddle_obj = $this->AccountFolder->find('first', array(
                    'conditions' => array(
                        'account_folder_id' => $huddle_id
                    )
                ));

                $account_folders_meta_data = array(
                    'meta_data_value' => "'" . addslashes($this->request->data['message']) . "'",
                    'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                    'last_edit_by' => $users['User']['id']
                );

                $this->AccountFolderMetaData->updateAll($account_folders_meta_data, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'message'));

                if ($submission_allowed['AccountFolderMetaData']['meta_data_value'] > 0) {
                    $submission_allowed_data = array(
                        'meta_data_value' => addslashes($this->request->data['submission_allowed']),
                        'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->updateAll($submission_allowed_data, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'submission_allowed'));
                } else {
                    $submission_allowed = isset($this->request->data['submission_allowed']) ? $this->request->data['submission_allowed'] : 1;

                    //Add Allowed video permissions limit.
                    $submission_limit_data = array(
                        'account_folder_id' => $huddle_id,
                        'meta_data_name' => 'submission_allowed',
                        'meta_data_value' => $submission_allowed,
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id'],
                        'created_date' => date('Y-m-d H:i:s'),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($submission_limit_data);
                }

                $huddle_type_meta = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "folder_type")));
                if (empty($huddle_type_meta)) {
                    $account_folders_meta_data = array(
                        'account_folder_id' => $huddle_id,
                        'meta_data_name' => 'folder_type',
                        'meta_data_value' => (!isset($this->request->data['type']) ? "" : $this->request->data['type']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_data);
                } else {
                    $account_folders_meta_data_huddle_type = array(
                        'meta_data_value' => (!isset($this->request->data['type']) ? "" : $this->request->data['type']),
                        'last_edit_date' => '"' . date("Y-m-d H:i:s") . '"',
                        'last_edit_by' => '"' . $users['User']['id'] . '"'
                    );
                    $this->AccountFolderMetaData->updateAll($account_folders_meta_data_huddle_type, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type'));
                }

                /*
                 * Standards and tags save and update
                 */
                $chk_frameworks = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "chk_frameworks")));

                if (empty($chk_frameworks)) {
                    $account_folders_meta_framework = array(
                        'account_folder_id' => $huddle_id,
                        'meta_data_name' => 'chk_frameworks',
                        'meta_data_value' => (!isset($this->request->data['chkenableframework']) ? "0" : $this->request->data['chkenableframework']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_framework);
                } else {
                    $account_folders_meta_framework = array(
                        'meta_data_value' => (!isset($this->request->data['chkenableframework']) ? "0" : $this->request->data['chkenableframework']),
                        'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->updateAll($account_folders_meta_framework, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks'));
                }


                $coachee_permission = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "coachee_permission")));

                if (empty($coachee_permission)) {
                    $account_folders_meta_framework = array(
                        'account_folder_id' => $huddle_id,
                        'meta_data_name' => 'coachee_permission',
                        'meta_data_value' => (!isset($this->request->data['chkcoacheepermissions']) ? "0" : $this->request->data['chkcoacheepermissions']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_framework);
                } else {
                    $account_folders_meta_framework = array(
                        'meta_data_value' => (!isset($this->request->data['chkcoacheepermissions']) ? "0" : $this->request->data['chkcoacheepermissions']),
                        'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->updateAll($account_folders_meta_framework, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'coachee_permission'));
                }


                $coach_hud_feedback = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "coach_hud_feedback")));

                if (empty($coach_hud_feedback)) {
                    $account_folders_meta_framework = array(
                        'account_folder_id' => $huddle_id,
                        'meta_data_name' => 'coach_hud_feedback',
                        'meta_data_value' => (!isset($this->request->data['coach_hud_feedback']) ? "0" : $this->request->data['coach_hud_feedback']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_meta_framework);
                } else {
                    $account_folders_meta_framework = array(
                        'meta_data_value' => (!isset($this->request->data['coach_hud_feedback']) ? "0" : $this->request->data['coach_hud_feedback']),
                        'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->updateAll($account_folders_meta_framework, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'coach_hud_feedback'));
                }


                $chk_tags = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "chk_tags")));

                if (empty($chk_tags)) {
                    $account_folders_tags = array(
                        'account_folder_id' => $huddle_id,
                        'meta_data_name' => 'chk_tags',
                        'meta_data_value' => (!isset($this->request->data['chkenabletags']) ? "0" : $this->request->data['chkenabletags']),
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->create();
                    $this->AccountFolderMetaData->save($account_folders_tags);
                } else {
                    $account_folders_tags = array(
                        'meta_data_value' => (!isset($this->request->data['chkenabletags']) ? "0" : $this->request->data['chkenabletags']),
                        'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->updateAll($account_folders_tags, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_tags'));
                }

                if (isset($this->request->data['chkenableframework']) && $this->request->data['chkenableframework'] == 1 && isset($this->request->data['frameworks'])) {
                    $framework_id = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "framework_id")));

                    if (empty($framework_id)) {
                        $account_folders_tags = array(
                            'account_folder_id' => $huddle_id,
                            'meta_data_name' => 'framework_id',
                            'meta_data_value' => $this->request->data['frameworks'],
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id']
                        );
                        $this->AccountFolderMetaData->create();
                        $this->AccountFolderMetaData->save($account_folders_tags);
                    } else {
                        if ($this->request->data['frameworks'] == '') {

                            if (isset($default_framwork_value)) {
                                $account_folders_tags = array(
                                    'meta_data_value' => (empty($default_framwork_value) ? -1 : $default_framwork_value),
                                    'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                                    'last_edit_by' => $users['User']['id']
                                );
                            } else {
                                $account_folders_tags = array(
                                    'meta_data_value' => -1,
                                    'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                                    'last_edit_by' => $users['User']['id']
                                );
                            }
                        } else {
                            $account_folders_tags = array(
                                'meta_data_value' => $this->request->data['frameworks'],
                                'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                                'last_edit_by' => $users['User']['id']
                            );
                        }
                        $this->AccountFolderMetaData->updateAll($account_folders_tags, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id'));
                    }
                }


                if ($this->request->data['type'] == '3') {
                    $account_folders_meta_data = array(
                        'meta_data_name' => "'submission_deadline_date'",
                        'meta_data_value' => (!isset($this->request->data['submission_deadline_date']) ? "" : "'" . $this->request->data['submission_deadline_date'] . "'"),
                        'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                        'last_edit_by' => $users['User']['id']
                    );

                    $this->AccountFolderMetaData->updateAll($account_folders_meta_data, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'submission_deadline_date'));

                    $account_folders_meta_data = array(
                        'meta_data_name' => "'submission_deadline_time'",
                        'meta_data_value' => (!isset($this->request->data['submission_deadline_time']) ? "" : "'" . $this->request->data['submission_deadline_time'] . "'"),
                        'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                        'last_edit_by' => $users['User']['id']
                    );
                    $this->AccountFolderMetaData->updateAll($account_folders_meta_data, array('account_folder_id' => $huddle_id, 'meta_data_name' => 'submission_deadline_time'));
                }

                /*
                 * end
                 */
                $tmp_huddle_user_ids = array();
                $tmp_ex_huddle_user_ids = array();
                $ex_folder_users = $this->AccountFolderUser->find('all', array(
                    'conditions' => array(
                        'account_folder_id' => $huddle_id
                    )
                ));

                if (isset($ex_folder_users) && count($ex_folder_users) > 0) {
                    for ($i = 0; $i < count($ex_folder_users); $i++) {
                        $tmp_ex_huddle_user_ids[] = $ex_folder_users[$i]['AccountFolderUser']['user_id'];
                    }
                }

                $this->AccountFolderUser->deleteAll(array('account_folder_id' => $huddle_id));
                $this->AccountFolderGroup->deleteAll(array('account_folder_id' => $huddle_id));
                $account_folder_id = $huddle_id;

                if (!in_array($huddle_obj['AccountFolder']['created_by'], $superUsersIds)) {
                    array_push($superUsersIds, $huddle_obj['AccountFolder']['created_by']);
                }
                if ($superUsersIds != '') {
                    foreach ($superUsersIds as $supUsers) {
                        $assigned_role = $this->request->data['user_role_' . $supUsers];
                        $assigned_is_coach = isset($this->request->data['is_coach_' . $supUsers]) ? 1 : '0';
                        $assigned_is_mentee = isset($this->request->data['is_mentor_' . $supUsers]) ? 1 : '0';

                        if ($assigned_role == '') {

                            if ($assigned_is_coach == 1)
                                $assigned_role = 200;
                            if ($assigned_is_mentee == 1)
                                $assigned_role = 210;

                            if ($assigned_is_coach == 0 && $assigned_is_mentee == 0)
                                $assigned_role = 220;
                        }

                        if ($supUsers <= 0) {
                            //we need to add this user first.
                            $tmp_user_id = $this->_addUser($this->request->data ['super_admin_fullname_' . $supUsers], $this->request->data['super_admin_email_' . $supUsers], $account_folder_id);

                            if ($tmp_user_id <= 0)
                                continue;
                            $supUsers = $tmp_user_id;
                            $tmp_huddle_user_ids[] = $supUsers;
                        }

                        if (!in_array($supUsers, $tmp_ex_huddle_user_ids)) {
                            $tmp_huddle_user_ids[] = $supUsers;
                        }

                        if ($assigned_role == '200')
                            $assigned_is_coach = 1;


                        if ($this->request->data['type'] == 3) {
                            if ($supUsers == $huddle_obj['AccountFolder']['created_by']) {
                                $assigned_is_coach = 1;
                                $assigned_is_mentee = 0;
                                $assigned_role = 200;
                            }
                        }


                        $superUserData = array(
                            'account_folder_id' => $account_folder_id,
                            'user_id' => $supUsers,
                            'role_id' => $assigned_role,
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                            'is_coach' => $assigned_is_coach,
                            'is_mentee' => $assigned_is_mentee,
                        );
                        $this->AccountFolderUser->create();
                        $this->AccountFolderUser->save($superUserData);
                    }
                }
                if ($groupIds != '') {
                    foreach ($groupIds as $grp) {
                        $role_id = isset($this->request->data['group_role_' . $grp]) ?
                                $this->request->data['group_role_' . $grp] : 220;
                        if (empty($role_id))
                            $role_id = 220;
                        $groupsData = array(
                            'account_folder_id' => $account_folder_id,
                            'group_id' => $grp,
                            'role_id' => $role_id,
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                            'is_coach' => isset($this->request->data['is_coach_' . $grp]) ? $this->request->data['is_coach_' . $grp] : '0',
                            'is_mentee' => isset($this->request->data['is_mentor_' . $grp]) ? $this->request->data['is_mentor_' . $grp] : '0',
                        );
                        $this->AccountFolderGroup->create();
                        $this->AccountFolderGroup->save($groupsData);
                    }
                }

                $huddleUsers = $this->AccountFolder->getHuddleUsers($huddle_obj['AccountFolder']['account_folder_id']);
                $user_ids = implode(',', $tmp_huddle_user_ids);

                if (!empty($user_ids) && count($user_ids) > 0) {

                    $huddle = $this->AccountFolder->getHuddle($huddle_obj['AccountFolder']['account_folder_id'], $account_id);
                    $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                    if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                        $huddle[0]['participated_user'] = $huddleUsers;
                        foreach ($huddleUserInfo as $row) {
                            $huddle[0]['User']['email'] = $row['User']['email'];
                            if ($this->check_subscription($row['User']['id'], '4', $account_id)) {
                                $huddle[0]['account_info'] = $users['accounts'];

                                $huddle[0]['huddle_type'] = (!isset($this->request->data['type']) ? "" : $this->request->data['type']);
                                if ($this->request->data['type'] == 3) {
                                    $submission_date = explode('-', $this->request->data['submission_deadline_date']);
                                    $submission_time = (!isset($this->request->data['submission_deadline_time']) ? "" : date("h:i a", strtotime($this->request->data['submission_deadline_time'])) . ' CST');
                                    $huddle[0]['submission_time'] = date('F j, Y', strtotime($submission_date[1] . '-' . $submission_date[0] . '-' . $submission_date[2])) . ' at ' . $submission_time;
                                }

                                $this->newHuddleEmail($huddle[0], $row['User']['id']);
                            }
                        }
                    }
                }

                $before_edit_users = array();
                if (!empty($correct_folder_users)) {
                    foreach ($correct_folder_users as $row) {
                        $before_edit_users[] = $row['AccountFolderUser']['user_id'];
                    }
                }

                $after_edit_users = $this->data['super_admin_ids'];
//                echo 'User Before edit';
//                print_r($before_edit_users);
//                echo 'User After edit';
//                print_r($after_edit_users);


                $users_removed = array();
                foreach ($before_edit_users as $row) {
                    if (!in_array($row, $after_edit_users)) {
                        $users_removed[] = $row;
                    }
                }
                $users_added = array();
                foreach ($after_edit_users as $row) {
                    if (!in_array($row, $before_edit_users)) {
                        $users_added[] = $row;
                    }
                }


                foreach ($users_added as $row) {
                    $user_activity_logs = array(
                        'ref_id' => $row,
                        'desc' => 'User Added in Huddle',
                        'url' => $this->base . '/huddles/view/' . $huddle_id,
                        'account_folder_id' => $huddle_id,
                        'type' => '18'
                    );
                    $this->user_activity_logs($user_activity_logs);
                }

                foreach ($users_removed as $row) {
                    $user_activity_logs = array(
                        'ref_id' => $row,
                        'desc' => 'User Added in Huddle',
                        'url' => $this->base . '/huddles/view/' . $huddle_id,
                        'account_folder_id' => $huddle_id,
                        'type' => '19'
                    );
                    $this->user_activity_logs($user_activity_logs);
                }
                $this->Session->setFlash($this->request->data['name'] . ' ' . $this->language_based_messages['has_been_updated_successfully'], 'default', array('class' => 'message success'));
                $this->redirect('/Huddles/view/' . $huddle_id);
            } else {
                $this->Session->setFlash($this->language_based_messages['video_huddle_is_not_updated_please_try_again'], 'default', array('class' => 'message error'));
                $this->set('user_id', $users['User']['id']);
                $this->render('edit');
            }
        } else {
            $this->render('edit');
        }
    }

    public function add_new_folder_breadcrum($label, $url) {


        $crumb = array();
        $crumb[$label] = $url;
        $bcrumb = $this->Session->read('breadcrumb');
        $parents = array();
        $bread_crumb_path = ($this->get_all_parents($url, $parents));
        $bread_crumb_path['Home'] = 'huddles';
        $bread_crumb_path = array_reverse($bread_crumb_path);

        if (empty($bcrumb)) {

            if (!isset($bcrumb['Home'])) { //If there's no session data yet, assume a homepage link
                $this->crumbs['Home'] = 'huddles';
                $bcrumb = $this->crumbs;
            }
        }

        $check_duplicate = $this->check_duplicate($crumb, $bread_crumb_path);

        if ($check_duplicate > 0) {

            $break_array = $this->break_array($bread_crumb_path, $crumb);


            $array_merge = $this->safe_array_merge($break_array, $crumb);
            //clear session variable
            //and prepare the session variable to be loaded with the exact information location
            $this->unset_variable();

            $output = $this->output_new_breadcrum($break_array, $label, $url);

            //put the merged array in the session variable
            //this is where the user is at the moment
            $bcrumb = $array_merge;
            $this->Session->write('breadcrumb', $bread_crumb_path);
        } else {

            $output = $this->output_new_breadcrum($bread_crumb_path, $label, $url);

            $bcrumb = $this->safe_array_merge($bread_crumb_path, $crumb);
            $this->Session->write('breadcrumb', $bread_crumb_path);
        }
        //return the output

        return $output;
    }

    private function output_new_breadcrum($array, $label, $url) {

        $count = 0;
        $link = "";

        foreach ($array as $key => $value) {
            //    $count++;
            //    if ($count < $this->url_count) {
            if ($key == 'Home') {

            } else {
                $k = substr($key, 0, -3);
                //  $link .="<a href='" . $this->base . '/Folder/' . $value . "'>" . $k . "</a>";
                $link .= '<a href = "' . $this->base . '/video_huddles/list/' . $value . '"   >' . addslashes($k) . '</a>';
            }
            //  }
        }

        return $link;
    }

    public function get_all_parents($huddle_id, $parents) {

        $result = $this->AccountFolder->find("first", array("conditions" => array(
                "account_folder_id" => $huddle_id,
        )));
        if ($result['AccountFolder']['parent_folder_id'] == NULL) {
            return $parents;
        } else {
            $result1 = $this->AccountFolder->find("first", array("conditions" => array(
                    "account_folder_id" => $result['AccountFolder']['parent_folder_id'],
            )));
            $parents[$result1['AccountFolder']['name'] . '_0f'] = $result['AccountFolder']['parent_folder_id'];
            return $this->get_all_parents($result['AccountFolder']['parent_folder_id'], $parents);
        }
    }

    private function check_duplicate($array1, $array2) {


        $array = array_intersect_assoc($array1, $array2);

        //return the number of occurences
        return count($array);
    }

    private function safe_array_merge($array1, $array2) {
        return array_merge($array1, $array2);
    }

    private function break_array($array1, $array2) {

        $count = 0;
        foreach ($array1 as $key => $value) {
            $count++;
            if (($value == @$array2[$key])) {

                $num = $count;
            }
        }

        while ((count($array1) + 1) > ($num)) {

            array_pop($array1); //prune until we reach the $level we've allocated to this page
        }

        return $array1;
    }

    public function ajax_get_uninvited_users($huddle_id) {
        $keywords = $this->request->data('keywords');
        $limit = (int) $this->request->data('limit');
        if (empty($limit))
            $limit = 10;

        $page = (int) $this->request->data('page');
        $offset = $page * $limit;

        $ignoreList = $this->request->data('ignore');
        if (!empty($ignoreList)) {
            $ignoreList = explode(',', $ignoreList);
        } else {
            $ignoreList = array();
        }

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $result = $this->User->getUninvitedUsers($keywords, $account_id, $huddle_id, $limit, $offset, $ignoreList);

        $view = new View($this, false);
        $html = $view->element('ajax/uninvited_user_list', $result);

        $data = array(
            'total' => $result['count'],
            'html' => $html
        );
        echo json_encode($data);
        exit;
    }

    public function ajax_get_uninvited_groups($huddle_id) {
        $keywords = $this->request->data('keywords');
        $limit = (int) $this->request->data('limit');
        if (empty($limit))
            $limit = 10;

        $page = (int) $this->request->data('page');
        $offset = $page * $limit;

        $ignoreList = $this->request->data('ignore');
        if (!empty($ignoreList)) {
            $ignoreList = explode(',', $ignoreList);
        } else {
            $ignoreList = array();
        }

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $result = $this->Group->getUninvitedGroups($keywords, $account_id, $huddle_id, $limit, $offset, $ignoreList);

        $view = new View($this, false);
        $html = $view->element('ajax/uninvited_group_list', $result);

        $data = array(
            'total' => $result['count'],
            'html' => $html
        );
        echo json_encode($data);
        exit;
    }

    function ajax_people_filter($huddle_id = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $keywords = $this->request->data['keywords'];
        $params = array(
            'users_record' => $this->get_all_users_ajax($account_id, $keywords),
            'huddles' => isset($huddle_id) ? $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id))) : '',
            'huddle_id' => isset($huddle_id) ? $huddle_id : '',
            'huddleUsers' => isset($huddle_id) ? $this->AccountFolderUser->find('all', array('conditions' => array('account_folder_id' => $huddle_id, 'role_id' => '210'))) : '',
            'huddleViewers' => isset($huddle_id) ? $this->AccountFolderUser->find('all', array('conditions' => array('account_folder_id' => $huddle_id, 'role_id' => '220'))) : '',
            'supperUsers' => isset($huddle_id) ? $this->AccountFolderUser->find('all', array('conditions' => array('account_folder_id' => $huddle_id, 'role_id' => '200'))) : '',
            'groups' => isset($huddle_id) ? $this->AccountFolderGroup->find('all', array('conditions' => array('account_folder_id' => $huddle_id))) : '',
            'users' => $this->_get_all_filterd_users($account_id, $keywords),
            'super_users' => $this->_get_all_filterd_users($account_id, $keywords),
            'users_groups' => $this->_ajax_grups($account_id, $keywords),
            'user_id' => $users['User']['id']
        );

        $view = new View($this, FALSE);
        $html = $view->element('huddles/ajax/ajax_people_filter', $params);
        echo $html;
        die();
    }

    function _get_all_filterd_users($account_id, $keywords) {
        $users = $this->User->getUsersByRole_ajax($account_id, $this->role_user, $keywords);
        $super_users = $this->User->getUsersByRole_ajax($account_id, $this->role_super_user, $keywords);
        $account_owner_users = $this->User->getUsersByRole_ajax($account_id, 100, $keywords);

        $result = array_merge($users, $super_users);
        $result = array_merge($result, $account_owner_users);

        if (is_array($result) && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function _ajax_grups($account_id, $keywords) {
        $groups = $this->Group->ajax_getGroups($account_id, $keywords);
        if (is_array($groups) && count($groups) > 0) {
            return $groups;
        } else {
            return FALSE;
        }
    }

    function get_all_users_ajax($account_id, $keywords) {
        $arr1 = $this->_get_all_filterd_users($account_id, $keywords);
        $arr2 = $this->_getSupperAdminsAjax($account_id, $keywords);
        $arr3 = $this->_ajax_grups($account_id, $keywords);
        $result = array();
        $view = new View($this, false);
        if ($arr1) {
            foreach ($arr1 as $row) {
                $row['User']['role'] = $view->Custom->get_user_role_name($row['roles']['role_id']);
                $row['User']['is_user'] = 'member';
                $row['User']['name'] = $row['User']['first_name'] . " " . $row['User']['last_name'];
                $result[] = $row['User'];
            }
        }
        if ($arr2) {
            foreach ($arr2 as $row) {
                $row['User']['role'] = $view->Custom->get_user_role_name($row['roles']['role_id']);
                $row['User']['is_user'] = 'admin';
                $row['User']['name'] = $row['User']['first_name'] . " " . $row['User']['last_name'];
                $result[] = $row['User'];
            }
        }
        if ($arr3) {
            foreach ($arr3 as $row) {
                $row['Group']['is_user'] = 'group';
                $result[] = $row['Group'];
            }
        }
        $outputArray = array();
        //remove dupplication
        foreach ($result as $innerArray) {
            $outputArray[$innerArray['id']] = $innerArray;
        }
        usort($outputArray, array($this, 'cal_array_sort_name'));
        return $outputArray;
    }

    private function _getSupperAdminsAjax($account_id, $keywords) {

        $super_users = $this->User->getUsersByRole_ajax($account_id, $this->role_super_user, $keywords);
        $account_owner_users = $this->User->getUsersByRole_ajax($account_id, 100, $keywords);
        $result = array_merge($account_owner_users, $super_users);
        if (is_array($result) && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function deleteVideoDocument($document_id, $video_id, $account_folder_id) {
        $account_folder_documents = $this->AccountFolderDocument->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'document_id' => $document_id
            )
        ));

        if (!empty($account_folder_documents)) {
            $attachmentCount = $this->AccountFolderDocumentAttachment->find('count', array(
                'conditions' => array(
                    'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id']
                )
            ));

            if ($attachmentCount > 1) {
                $this->AccountFolderDocumentAttachment->deleteAll(array(
                    'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id'],
                    'attach_id' => (int) $video_id
                ));
            } else {
                $this->AccountFolderDocumentAttachment->deleteAll(array(
                    'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id']
                ));

                $this->AccountFolderDocument->deleteAll(array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $document_id
                ));

                $this->Document->deleteAll(array(
                    'id' => $document_id
                ));
            }
        }

        $this->layout = null;
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function deleteOservationDocument($document_id, $account_folder_id) {
        $account_folder_documents = $this->AccountFolderDocument->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'document_id' => $document_id
            )
        ));

        if (!empty($account_folder_documents)) {
            $attachmentCount = $this->AccountFolderDocumentAttachment->find('count', array(
                'conditions' => array(
                    'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id']
                )
            ));

            if ($attachmentCount > 1) {
                $this->AccountFolderDocumentAttachment->deleteAll(array(
                    'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id'],
                    'attach_id' => (int) $document_id
                ));
            } else {
                $this->AccountFolderDocumentAttachment->deleteAll(array(
                    'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id']
                ));

                $this->AccountFolderDocument->deleteAll(array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $document_id
                ));

                $this->Document->deleteAll(array(
                    'id' => $document_id
                ));
            }
        }

        $this->layout = null;
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function deleteDocument($document_id, $account_folder_id) {
        $this->auth_huddle_permission('deleteDocument', $account_folder_id);

        $account_folder_documents = $this->AccountFolderDocument->find('first', array('conditions' => array(
                'account_folder_id' => $account_folder_id,
                'document_id' => $document_id
            )
        ));

        $this->AccountFolderDocumentAttachment->deleteAll(array(
            'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id']
        ));

        $this->AccountFolderDocument->deleteAll(array(
            'account_folder_id' => $account_folder_id,
            'document_id' => $document_id
        ));

        $this->Document->deleteAll(array('id' => $document_id));

        $this->layout = null;
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function delDocument($document_id, $account_folder_id) {
        $documentResult = $this->Document->get_document_row($document_id);
        $user_current_account = $this->Session->read('user_current_account');
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        if ($huddle_permission == '200' || ($user_current_account['User']['id'] == $documentResult['Document']['created_by'])) {
            $account_folder_documents = $this->AccountFolderDocument->find('first', array('conditions' => array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $document_id
                )
            ));

            $this->AccountFolderDocumentAttachment->deleteAll(array(
                'account_folder_document_id' => $account_folder_documents['AccountFolderDocument']['id']
            ));

            $this->AccountFolderDocument->deleteAll(array(
                'account_folder_id' => $account_folder_id,
                'document_id' => $document_id
            ));

            $this->Document->deleteAll(array('id' => $document_id));
            $this->CreateJobQueueArchiveJob($document_id);
            if ($this->Document->getAffectedRows() > 0) {
                $this->Session->setFlash($this->language_based_messages['you_have_successfully_deleted_selected_document'], 'default', array('class' => 'message success'));
                $this->redirect('/Huddles/view/' . $account_folder_id . '/2');
            } else {
                $this->Session->setFlash($this->language_based_messages['you_selected_document_is_not_deleted_yet_please_try_again'], 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/view/' . $account_folder_id . '/2');
            }
        } else {
            $this->redirect('/NoPermissions/no_permission');
        }
    }

    function deleteDocs() {
        if ($this->request->is('post')) {
            $document_ids = $this->request->data['document_ids'];
            $account_folder_id = $this->request->data['account_folder_id'];
            $account_f_id = 'account_folder_id =' . $account_folder_id;
            $document_id = "document_id IN (" . $document_ids . ")";
            $document_id2 = "id IN (" . $document_ids . ")";
            $this->auth_huddle_permission('deleteDocument', $account_folder_id);
            $account_folder_documents = $this->AccountFolderDocument->find('all', array('conditions' => array($account_f_id, $document_id)
            ));
            if (isset($account_folder_documents) && count($account_folder_documents) > 0) {
                $afd_ids = '';
                foreach ($account_folder_documents as $row) {
                    $afd_ids[] = $row['AccountFolderDocument']['id'];
                }
                $this->AccountFolderDocumentAttachment->deleteAll(array(
                    'account_folder_document_id IN (' . implode(',', $afd_ids) . ")"
                ));
                $this->AccountFolderDocument->deleteAll(array($account_f_id, $document_id));
                $this->Document->deleteAll(array($document_id2));
                $this->Session->setFlash($this->language_based_messages['resource_deleted_successfully_msg'], 'default', array('class' => 'message success'));
                $this->redirect('/Huddles/view/' . $account_folder_id . '/2');
            } else {
                $this->Session->setFlash($this->language_based_messages['you_selected_documents_are_not_deleted_yet_please_try_agian'], 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/view/' . $account_folder_id . '/2');
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['please_select_atleast_one_document_to_delete'], 'default', array('class' => 'message error'));
            $this->redirect('/Huddles/view/' . $account_folder_id . '/2');
        }
    }

    function comments($huddle_id, $tab = '', $commentable_id = '') {
        //$this->auth_huddle_permission('comments', $huddle_id);
        if ($this->request->is('post')) {
            $notificationIds = '';
            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $user_id = $users['User']['id'];
            $users_ids = explode(',', $this->request->data['notification_user_ids'][0]);
            $counter = 0;
            foreach ($users_ids as $key => $val) {
                if (isset($users_ids[$counter]) && $users_ids[$counter] != 0) {
                    $notificationIds[] = $users_ids[$counter];
                }
                $counter++;
            }

            $this->Comment->create();
            $data = array(
                'title' => isset($this->data['title']) ? htmlspecialchars($this->data['title']) : '',
                'comment' => !empty($this->data['comment']) ? strip_tags(html_entity_decode($this->data['comment'])) : '',
                'access_level' => $this->data['access_level'],
                'ref_id' => $huddle_id,
                'parent_comment_id' => (empty($this->data['commentable_id']) ? null : $this->data['commentable_id']),
                'ref_type' => '1',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );

            if ($this->Comment->save($data)) {
                $comment_id = $this->Comment->id;
                if ($commentable_id == '') {
                    $user_activity_logs = array(
                        'ref_id' => $comment_id,
                        'desc' => $this->data ['comment'],
                        'url' => $this->base . '/Huddles/view/' . $huddle_id . "/" . $tab . "/" . $comment_id,
                        'account_folder_id' => $huddle_id,
                        'type' => '6'
                    );
                } else {
                    $user_activity_logs = array(
                        'ref_id' => $comment_id,
                        'desc' => $this->data ['comment'],
                        'url' => $this->base . '/Huddles/view/' . $huddle_id . "/" . $tab . "/" . $comment_id,
                        'account_folder_id' => $huddle_id,
                        'type' => '8'
                    );
                }
                $this->user_activity_logs($user_activity_logs);

                if (isset($this->request->data['attachment'])) {
                    $attachments = $this->request->data['attachment'];
                    if (isset($attachments['error'])) {
                        $attachments = array($attachments);
                    }
                    foreach ($attachments as $att) {
                        if ($att['error'] != 0)
                            continue;
                        $document_id = $this->uploadAttachementToS3($att, $account_id, $huddle_id);
                        if ($document_id != false) {
                            $this->CommentAttachment->create();
                            $data = array(
                                'comment_id' => $comment_id,
                                'document_id' => $document_id
                            );
                            $this->CommentAttachment->save($data);
                        }
                    }
                }

                if ($notificationIds) {
                    $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
                    $huddleUsers = $this->AccountFolder->getHuddleUsers($huddle_id);
                    $userGroups = $this->AccountFolderGroup->getHuddleGroups($huddle_id);
                    $huddle_user_ids = array();
                    if ($userGroups && count($userGroups) > 0) {
                        foreach ($userGroups as $row) {
                            $huddle_user_ids[] = $row['user_groups']['user_id'];
                        }
                    }
                    if ($huddleUsers && count($huddleUsers) > 0) {
                        foreach ($huddleUsers as $row) {
                            $huddle_user_ids[] = $row['huddle_users']['user_id'];
                        }
                    }

                    $user_ids = implode(',', $huddle_user_ids);
                    $huddleUserInfo = '';
                    if ($user_ids != '') {
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                    }

                    $sibme_base_url = Configure::read('sibme_base_url');
                    for ($i = 0; $i < count($notificationIds); $i++) {
                        $notifier_id = $notificationIds[$i];
                        $notifier = $this->User->find('first', array('conditions' => array('id' => $notifier_id)));
                        if ($notifier ['User']['is_active'] == 1) {

                            $this->CommentUser->create();
                            $data = array(
                                'comment_id' => $comment_id,
                                'user_id' => $notifier_id,
                                'comment_user_type' => 1
                            );

                            $this->CommentUser->save($data);
                            if ($commentable_id != '') {
                                $orignal_comment = $this->Comment->get_parent_discussion($commentable_id);
                                $huddle_topic_name = $orignal_comment['Comment']['title'];
                                $discussion_link = $sibme_base_url . 'Huddles/view/' . $huddle_id . "/" . $tab . "/" . $commentable_id;
                            } else {
                                $huddle_topic_name = $this->data ['title'];
                                $discussion_link = $sibme_base_url . 'Huddles/view/' . $huddle_id . "/" . $tab . "/" . $comment_id;
                            }
                            $discussions = array(
                                'huddle_name' => $huddle[0]['AccountFolder']['name'],
                                'discussion_topic' => $huddle_topic_name,
                                'created_by' => $users['User']['first_name'] . " " . $users['User']['last_name'],
                                'message' => $this->data ['comment'],
                                'discussion_link' => $discussion_link,
                                'participting_users' => $huddleUserInfo
                            );
                            if ($this->check_subscription($notifier_id, '2', $account_id) && $commentable_id == '') {
                                $this->SendDiscussionEmail($account_id, $notifier, '', $this->data['comment'], $discussions, $commentable_id);
                            } elseif ($this->check_subscription($notifier_id, '2', $account_id) && $commentable_id != '') {
                                $this->SendDiscussionEmail($account_id, $notifier, '', $this->data['comment'], $discussions, $commentable_id);
                            }
                        }
                    }
                }

//                $replyDiscussions = $this->Comment->commentReplys_single($this->data['commentable_id'],$comment_id);
//
//                $params = array(
//                    'row' => $replyDiscussions,
//                    'huddle_id' => $huddle_id
//                );

                $this->Session->setFlash($this->language_based_messages['discussion_has_been_started_successfully'], 'default', array('class' => 'message success'));

//                 $view = new View($this, false);
//                $html = $view->element('huddles/ajax/huddles_discussion',$params);
//                echo $html;
//                die;

                $loggedInUser = $this->Session->read('user_current_account');
                $view = new View($this, false);
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = $view->Custom->check_if_account_in_trial_intercom($account_id);
                //    if (IS_QA):

                $meta_data = array(
                    'discussion-comment-added' => 'true',
                    'comment' => $this->data ['comment'],
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );

                $this->create_intercom_event('discussion-added', $meta_data, $loggedInUser['User']['email']);
                //     endif;


                if (isset($this->request->data['send_email']) && ($this->request->data['send_email'] == '1' || $this->request->data['send_email'] == '2' )) {
                    //  if (IS_QA):

                    $meta_data = array(
                        'included_in_discussion' => 'true',
                        'comment' => $this->data ['comment'],
                        'user_role' => $user_role,
                        'is_in_trial' => $in_trial_intercom,
                        'Platform' => 'Web'
                    );

                    $this->create_intercom_event('included-in-discussion', $meta_data, $loggedInUser['User']['email']);
                    // endif;
                }


                if ($commentable_id) {
                    $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab . '/' . $commentable_id);
                } else {
                    $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab);
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['comments_are_not_saved_please_try_again'], 'default', array('class' => 'message errror'));
                if ($commentable_id) {
                    $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab . '/' . $commentable_id);
                } else {
                    $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab);
                }
            }
        } else {
            $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab);
        }
    }

    function comments_ajax($huddle_id, $tab = '', $commentable_id = '') {
        //$this->auth_huddle_permission('comments', $huddle_id);
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('Huddles/view');

        if ($this->request->is('post')) {

            $notificationIds = '';
            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $user_id = $users['User']['id'];
            $users_ids = explode(',', $this->request->data['notification_user_ids'][0]);
            $counter = 0;
            foreach ($users_ids as $key => $val) {
                if (isset($users_ids[$counter]) && $users_ids[$counter] != 0) {
                    $notificationIds[] = $users_ids[$counter];
                }
                $counter++;
            }

            $this->Comment->create();
            $data = array(
                'title' => isset($this->data['title']) ? htmlspecialchars($this->data['title']) : '',
                'comment' => strip_tags(html_entity_decode($this->data['comment'])),
                'access_level' => $this->data['access_level'],
                'ref_id' => $huddle_id,
                'parent_comment_id' => (empty($this->data['commentable_id']) ? null : $this->data['commentable_id']),
                'ref_type' => '1',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );

            if ($this->Comment->save($data)) {
                $comment_id = $this->Comment->id;
                if ($commentable_id == '') {
                    $user_activity_logs = array(
                        'ref_id' => $comment_id,
                        'desc' => $this->data ['comment'],
                        'url' => $this->base . '/Huddles/view/' . $huddle_id . "/" . $tab . "/" . $comment_id,
                        'account_folder_id' => $huddle_id,
                        'type' => '6'
                    );
                } else {
                    $user_activity_logs = array(
                        'ref_id' => $comment_id,
                        'desc' => $this->data ['comment'],
                        'url' => $this->base . '/Huddles/view/' . $huddle_id . "/" . $tab . "/" . $comment_id,
                        'account_folder_id' => $huddle_id,
                        'type' => '8'
                    );
                }
                $this->user_activity_logs($user_activity_logs);

                if (isset($this->request->data['attachment'])) {
                    $attachments = $this->request->data['attachment'];
                    if (isset($attachments['error'])) {
                        $attachments = array($attachments);
                    }
                    foreach ($attachments as $att) {
                        if ($att['error'] != 0)
                            continue;
                        $document_id = $this->uploadAttachementToS3($att, $account_id, $huddle_id);
                        if ($document_id != false) {
                            $this->CommentAttachment->create();
                            $data = array(
                                'comment_id' => $comment_id,
                                'document_id' => $document_id
                            );
                            $this->CommentAttachment->save($data);
                        }
                    }
                }

                if ($notificationIds) {
                    $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
                    $huddleUsers = $this->AccountFolder->getHuddleUsers($huddle_id);
                    $userGroups = $this->AccountFolderGroup->getHuddleGroups($huddle_id);
                    $huddle_user_ids = array();
                    if ($userGroups && count($userGroups) > 0) {
                        foreach ($userGroups as $row) {
                            $huddle_user_ids[] = $row['user_groups']['user_id'];
                        }
                    }
                    if ($huddleUsers && count($huddleUsers) > 0) {
                        foreach ($huddleUsers as $row) {
                            $huddle_user_ids[] = $row['huddle_users']['user_id'];
                        }
                    }

                    $user_ids = implode(',', $huddle_user_ids);
                    $huddleUserInfo = '';
                    if ($user_ids != '') {
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                    }

                    $sibme_base_url = Configure::read('sibme_base_url');
                    for ($i = 0; $i < count($notificationIds); $i++) {
                        $notifier_id = $notificationIds[$i];
                        $notifier = $this->User->find('first', array('conditions' => array('id' => $notifier_id)));
                        if ($notifier ['User']['is_active'] == 1) {

                            $this->CommentUser->create();
                            $data = array(
                                'comment_id' => $comment_id,
                                'user_id' => $notifier_id,
                                'comment_user_type' => 1
                            );

                            $this->CommentUser->save($data);
                            if ($commentable_id != '') {
                                $orignal_comment = $this->Comment->get_parent_discussion($commentable_id);
                                $huddle_topic_name = $orignal_comment['Comment']['title'];
                                $discussion_link = $sibme_base_url . 'Huddles/view/' . $huddle_id . "/" . $tab . "/" . $commentable_id;
                            } else {
                                $huddle_topic_name = $this->data ['title'];
                                $discussion_link = $sibme_base_url . 'Huddles/view/' . $huddle_id . "/" . $tab . "/" . $comment_id;
                            }
                            $discussions = array(
                                'huddle_name' => $huddle[0]['AccountFolder']['name'],
                                'discussion_topic' => $huddle_topic_name,
                                'created_by' => $users['User']['first_name'] . " " . $users['User']['last_name'],
                                'message' => $this->data ['comment'],
                                'discussion_link' => $discussion_link,
                                'participting_users' => $huddleUserInfo
                            );
                            if ($this->check_subscription($notifier_id, '2', $account_id) && $commentable_id == '') {
                                $this->SendDiscussionEmail($account_id, $notifier, '', $this->data['comment'], $discussions, $commentable_id);
                            } elseif ($this->check_subscription($notifier_id, '2', $account_id) && $commentable_id != '') {
                                $this->SendDiscussionEmail($account_id, $notifier, '', $this->data['comment'], $discussions, $commentable_id);
                            }
                        }
                    }
                }


                $loggedInUser = $this->Session->read('user_current_account');
                $view = new View($this, false);
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = $view->Custom->check_if_account_in_trial_intercom($account_id);
                // if (IS_QA):

                $meta_data = array(
                    'discussion-comment-added' => 'true',
                    'comment' => $this->data ['comment'],
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );

                $this->create_intercom_event('discussion-added', $meta_data, $loggedInUser['User']['email']);
                //  endif;


                if (isset($this->request->data['send_email']) && ($this->request->data['send_email'] == '1' || $this->request->data['send_email'] == '2' )) {
                    //   if (IS_QA):

                    $meta_data = array(
                        'included_in_discussion' => 'true',
                        'comment' => $this->data ['comment'],
                        'user_role' => $user_role,
                        'is_in_trial' => $in_trial_intercom,
                        'Platform' => 'Web'
                    );

                    $this->create_intercom_event('included-in-discussion', $meta_data, $loggedInUser['User']['email']);
                    // endif;
                }



                $replyDiscussions = $this->Comment->commentReplys_single($this->data['commentable_id'], $comment_id);

                $params = array(
                    'row' => $replyDiscussions,
                    'huddle_id' => $huddle_id,
                    'language_based_content' => $language_based_content
                );

                //  $this->Session->setFlash('Comment has been saved successfully.', 'default', array('class' => 'message success'));

                $view = new View($this, false);
                $html = $view->element('huddles/ajax/huddles_discussion', $params);
                echo $html;
                die;


                if ($commentable_id) {
                    $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab . '/' . $commentable_id);
                } else {
                    $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab);
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['comments_are_not_saved_please_try_again'], 'default', array('class' => 'message errror'));
                if ($commentable_id) {
                    $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab . '/' . $commentable_id);
                } else {
                    $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab);
                }
            }
        } else {
            $this->redirect('/Huddles/view/' . $huddle_id . "/" . $tab);
        }
    }

    function upload_comment_file($huddle_id, $video_id, $account_id) {

        $attachments = $this->request->data['comment_attachment'];

        $account_folder_id = $huddle_id;


        $document_id = $this->uploadAttachementToS3($attachments, $account_id, $huddle_id);


        $this->AccountFolderDocument->create();
        $data = array(
            'account_folder_id' => $account_folder_id,
            'document_id' => $document_id,
            'title' => $attachments['name'],
                //    'desc' => $this->request->data['video_desc']
        );
        $this->AccountFolderDocument->save($data, $validation = TRUE);

        if (!empty($video_id) && $video_id != "undefined") {
            $account_folder_document_id = $this->AccountFolderDocument->id;
            $this->AccountFolderDocumentAttachment->create();
            $data = array(
                'account_folder_document_id' => $account_folder_document_id,
                'attach_id' => "$video_id"
            );
            $this->AccountFolderDocumentAttachment->save($data, $validation = TRUE);
        }


        $result = array(
            'document_id' => $document_id,
            'file_name' => $attachments['name'],
        );
        echo json_encode($result);

        die;
    }

    function update_stack_url($document_id, $video_id) {

        $stack_key = $this->request->data['stack_url'];
        $scripted_current_duration = $this->request->data['timestamp_duration'];
        $data_title = array(
            'stack_url' => "'" . $stack_key . "'",
            'scripted_current_duration' => $scripted_current_duration
        );
        if (!empty($document_id) && $document_id) {
            $this->Document->updateAll($data_title, array('id' => $document_id));
        }
        $view = new View($this, false);
        $result_count = $view->Custom->get_video_attachment_numbers($video_id);
        echo $result_count;
        die;
    }

    function editComment($comment_id) {

        //$this->auth_huddle_permission('editComment', $this->Session->read('account_folder_id'));
        $dataReturn = array('ok' => 0);

        $comment = $this->Comment->get($comment_id);
        if (empty($comment)) {
            echo json_encode($dataReturn);
            return;
        }

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $this->layout = null;
        $this->Comment->create();
        $parent_id = $this->request->data['discussion_parent_id'];
        $data_title = array(
            'title' => '"' . htmlspecialchars(addslashes($this->request->data['title'])) . '"'
        );
        $data = array(
            'comment' => '"' . addslashes(html_entity_decode(Sanitize::escape($this->request->data['comment']))) . '"'
        );
        $this->Comment->updateAll($data_title, array('id' => $parent_id));
        $this->Comment->create();
        $this->Comment->updateAll($data, array('id' => $comment_id));

        if (!empty($_FILES['attachment'])) {
            $attachments = $_FILES['attachment'];
            $n = count($attachments['name']);
            for ($i = 0; $i < $n; $i++) {
                $att = array(
                    'name' => $attachments['name'][$i],
                    'type' => $attachments['type'][$i],
                    'tmp_name' => $attachments['tmp_name'][$i],
                    'error' => $attachments['error'][$i],
                    'size' => $attachments['size'][$i]
                );
                if ($att['error'] != 0)
                    continue;
                $document_id = $this->uploadAttachementToS3($att, $account_id, $comment['Comment']['ref_id']);
                if ($document_id != false) {
                    $this->CommentAttachment->create();
                    $data = array(
                        'comment_id' => $comment_id,
                        'document_id' => $document_id
                    );
                    $this->CommentAttachment->save($data);
                }
            }
        }

        if (isset($this->request->data['remove_attachment'])) {
            $removeAttachment = $this->request->data['remove_attachment'];
            if (!empty($removeAttachment)) {
                $removeAttachmentIds = explode(',', $removeAttachment);
                $this->CommentAttachment->deleteAll(array('document_id' => $removeAttachmentIds));
                $this->Document->updateAll(array('published' => 0), array('id' => $removeAttachmentIds));
            }
        }

        $dataReturn['ok'] = 1;
        $dataReturn['content'] = stripslashes(html_entity_decode(Sanitize::escape($this->request->data['comment'])));
        $dataReturn['documents'] = array();

        $documents = $this->Document->getByCommentId($comment_id);
        foreach ($documents as $doc) {
            $dataReturn['documents'][] = array(
                'id' => $doc['Document']['id'],
                'name' => $doc['Document']['original_file_name']
            );
        }

        $this->set('ajaxdata', json_encode($dataReturn));
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function _make_url_clickable_cb($matches) {
        $ret = '';
        $url = $matches[2];

        if (empty($url))
            return $matches[0];
        // removed trailing [.,;:] from URL
        if (in_array(substr($url, -1), array('.', ',', ';', ':')) === true) {
            $ret = substr($url, -1);
            $url = substr($url, 0, strlen($url) - 1);
        }
        return $matches[1] . "<a href=\"$url\" target=\"_blank\" rel=\"nofollow\">$url</a>" . $ret;
    }

    function _make_web_ftp_clickable_cb($matches) {
        $ret = '';
        $dest = $matches[2];
        $dest = 'http://' . $dest;

        if (empty($dest))
            return $matches[0];
        // removed trailing [,;:] from URL
        if (in_array(substr($dest, -1), array('.', ',', ';', ':')) === true) {
            $ret = substr($dest, -1);
            $dest = substr($dest, 0, strlen($dest) - 1);
        }
        return $matches[1] . "<a href=\"$dest\" target=\"_blank\" rel=\"nofollow\">$dest</a>" . $ret;
    }

    function _make_email_clickable_cb($matches) {
        $email = $matches[2] . '@' . $matches[3];
        return $matches[1] . "<a href=\"mailto:$email\" target=\"_blank\">$email</a>";
    }

    function make_clickable($ret) {

        $ret = str_replace("&nbsp;", " ", $ret);

        $ret = ' ' . $ret;
        // in testing, using arrays here was found to be faster
        $ret = preg_replace_callback('#([\s>])([\w]+?://[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', "self::_make_url_clickable_cb", $ret);
        $ret = preg_replace_callback('#([\s>])((www|ftp)\.[\w\\x80-\\xff\#$%&~/.\-;:=,?@\[\]+]*)#is', "self::_make_web_ftp_clickable_cb", $ret);
        $ret = preg_replace_callback('#([\s>])([.0-9a-z_+-]+)@(([0-9a-z-]+\.)+[0-9a-z]{2,})#i', "self::_make_email_clickable_cb", $ret);

        // this one is not in an array because we need it to run last, for cleanup of accidental links within links
        $ret = preg_replace("#(<a( [^>]+?>|>))<a [^>]+?>([^>]+?)</a></a>#i", "$1$3</a>", $ret);
        $ret = trim($ret);
        return $ret;
    }

    function deleteComment($comment_id) {
        //$this->auth_huddle_permission('deleteComment', $this->Session->read('account_folder_id'));
        $this->layout = null;
        $this->Comment->updateAll(array('active' => '0'), array('id' => $comment_id));
        /*
          if ($this->Comment->getAffectedRows() > 0) {
          $get_activity_log = $this->UserActivityLog->find('first', array('conditions' => array('ref_id' => $comment_id)));

          if (isset($get_activity_log['UserActivityLog']) && $get_activity_log['UserActivityLog']['id'] != '') {
          $this->UserActivityLog->deleteAll(array('id' => $get_activity_log['UserActivityLog']['id']), $cascade = true, $callbacks = true);
          }
          }
         */
        $this->set('ajaxdata', "$comment_id");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function deleteHuddleVideo($huddleId, $tab, $videoId) {
        $view = new View($this, false);
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddleId, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        if ($htype == 3 && $view->Custom->check_if_evaluated_participant($huddleId, $user_id)) {
            $submission_date = $view->Custom->get_submission_date($huddleId);
            if ($submission_date != '') {
                $submission_string = strtotime($submission_date);
                $current_time = strtotime(date('Y-m-d h:i:s a', time()));
                if ($submission_string < $current_time) {
                    $this->Session->setFlash($this->language_based_messages['you_cannot_delete_your_video_because_your_submission_date_has_expired'], 'default', array('class' => 'message error'));
                    $this->redirect('/Huddles/view/' . $huddleId . '/' . $tab);
                }
            }
        }


        if ($htype != 2)
            $this->auth_huddle_permission('deleteHuddleVideo', $huddleId, $videoId);


        if ($videoId != '') {
            $this->layout = null;

            if ($this->AccountFolderDocument->deleteAll(array('document_id' => $videoId), $cascade = true, $callbacks = true)) {

                $video_comments = $this->Comment->find('all', array('conditions' => array('ref_id' => $videoId)));
                if (!empty($video_comments)) {
                    foreach ($video_comments as $video_comment) {
                        $this->Comment->deleteAll(array('id' => $video_comment['Comment']['id']), $cascade = true, $callbacks = true);
                        $comment_tags = $this->AccountCommentTag->find('all', array('conditions' => array('comment_id' => $video_comment['Comment']['id'])));
                        if (!empty($comment_tags)) {
                            foreach ($comment_tags as $comment_tag) {
                                $this->AccountCommentTag->deleteAll(array('account_comment_tag_id' => $comment_tag['AccountCommentTag']['account_comment_tag_id']), $cascade = true, $callbacks = true);
                            }
                        }
                    }
                }


                $this->Document->deleteAll(array('id' => $videoId), $cascade = true, $callbacks = true);
                $this->DocumentStandardRating->deleteAll(array('document_id' => $videoId), $cascade = true, $callbacks = true);
                $this->delete_video_attachments($videoId);
                $this->CreateJobQueueArchiveJob($videoId);
                $account_folder_observation = $this->Document->find('first', array('conditions' => array(
                        'doc_type' => 3,
                        'is_associated' => $videoId
                )));
                if (is_array($account_folder_observation) && count($account_folder_observation) > 0) {
                    $this->AccountFolderDocument->deleteAll(array(
                        'account_folder_id' => $huddleId,
                        'document_id' => $account_folder_observation['Document']['id']
                    ));

                    $this->Document->deleteAll(array('id' => $account_folder_observation['Document']['id']), $cascade = true, $callbacks = true);
                }
                $this->Session->setFlash($this->language_based_messages['video_has_been_deleted_successfully'], 'default', array('class' => 'message success'));
                $this->redirect('/Huddles/view/' . $huddleId);
            } else {
                $this->Session->setFlash($this->language_based_messages['video_is_not_deleted_please_try_again'], 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/view/' . $huddleId . '/', $tab . '/' . $videoId);
            }
        }
    }

    function insert_dummy_message_meta() {

        $result = $this->AccountFolder->find('all', array(
            'conditions' => array('folder_type' => '1')
        ));

        if ($result) {

            for ($i = 0; $i < count($result); $i++) {

                $AccountFolder = $result[$i];
                $account_folders_meta_data = array(
                    'account_folder_id' => $AccountFolder['AccountFolder']['account_folder_id'],
                    'meta_data_name' => 'message',
                    'meta_data_value' => "",
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $AccountFolder['AccountFolder']['created_by'],
                    'last_edit_by' => $AccountFolder['AccountFolder']['created_by']
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);
            }
        }

        $this->layout = null;
        $this->set('ajaxdata', "");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function verifyNewUsers() {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $account_owner = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
        $account_owner_info = $this->User->find('first', array('conditions' => array('id' => $account_owner['UserAccount']['user_id'])));
        $accounts = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $this->layout = null;
        $errorMessages = '';
        $user_data = $_POST['user_data'];
        for ($i = 0; $i < count($user_data); $i++) {
            $posted_user = $user_data[$i];
            if ($posted_user[0] == '')
                $puser = $posted_user[2];
            else
                $puser = $posted_user[1];
            if ($this->User->isUserEmailExistsInAccount($puser, $account_id) > 0) {
                if (!empty($errorMessages))
                    $errorMessages .= "<br/>";
                $errorMessages .= $puser . " email already exists.";
            }
            $joins = array(
                'table' => 'plans',
                'type' => 'left',
                'conditions' => array('Account.plan_id = plans.id')
            );
            $fields = array('plans.plan_type', 'plans.storage', 'plans.id', 'plans.users', 'Account.*');
            $result = $this->Account->find('first', array(
                'joins' => array($joins),
                'conditions' => array('Account.id' => $account_id),
                'fields' => $fields
            ));

            $totalUsers = $this->User->getTotalUsers($account_id);

            if ($result['Account']['in_trial'] == 1)
                $plan_users = 40;
            else
                $plan_users = $result['plans']['users'];
            $deactive_plan = $this->Account->find('first', array('conditions' => array('id' => $account_id), 'fields' => array('deactive_plan')));
//            if ($totalUsers >= $plan_users && $deactive_plan['Account']['deactive_plan'] != '1') {
            $view = new View($this, false);
            if ($view->Custom->get_users_added($account_id) >= $view->Custom->get_allowed_users($account_id)) {
                if (!empty($errorMessages))
                    $errorMessages .= "<br/>";
                if ($users['users_accounts']['role_id'] == 100) {
                    if ($result['Account']['deactive_plan'] == 1) {
                        $errorMessages .= "Your account has reached the maximum allowed user limit of " . $view->Custom->get_allowed_users($account_id) . " users. Please <a href = mailto:" . $this->custom->get_site_settings('static_emails')['sales'] . " >contact us</a> to upgrade your account.";
                    } else {

                        $errorMessages .= "Your account has reached the maximum allowed user limit of " . $view->Custom->get_allowed_users($account_id) . " users. Please <a href=/accounts/account_settings_all/$account_id/4>upgrade your plan</a> or delete existing users.";
                    }
                } else {
                    $errorMessages .= "Your account has reached the maximum allowed user limit of " . $view->Custom->get_allowed_users($account_id) . " users. Please contact your account owner " . $account_owner_info['User']['first_name'] . ' ' . $account_owner_info['User']['last_name'] . " at " . $account_owner_info['User']['email'] . ".";
                }
                $this->users_full_email($users['User']['email'], $user_data[$i][2], $account_id);
            }
        }
        $this->set('ajaxdata', "$errorMessages");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function check_storage_full() {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $view = new View($this, false);
        $account_owner = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
        $account_owner_info = $this->User->find('first', array('conditions' => array('id' => $account_owner['UserAccount']['user_id'])));

        $result = $this->Account->find('first', array(
            'conditions' => array('id' => $account_id)));

        if ($view->Custom->get_consumed_storage($account_id) > $view->Custom->get_allowed_storage($account_id)) {
            if (!empty($errorMessages))
                $errorMessages .= "<br/>";
            if ($users['users_accounts']['role_id'] == 100) {
                if ($result['Account']['deactive_plan'] == 1) {
                    $errorMessages .= "Your account has reached the maximum allowed storage limit of " . $view->Custom->get_allowed_storage($account_id) . " GB(s). Please <a href = mailto:" . $this->custom->get_site_settings('static_emails')['sales'] . " >contact us</a> to upgrade your account.";
                } else {
                    $errorMessages .= "Your account has reached the maximum allowed storage limit of " . $view->Custom->get_allowed_storage($account_id) . " GB(s). Please <a href=/accounts/account_settings_all/$account_id/4>upgrade your plan,</a> contact us, or delete videos and resources to free up storage space.";
                }
            } else {
                $errorMessages .= "Your account has reached the maximum allowed storage limit of " . $view->Custom->get_allowed_storage($account_id) . " GB(s). Please contact your account owner " . $account_owner_info['User']['first_name'] . ' ' . $account_owner_info['User']['last_name'] . " at " . $account_owner_info['User']['email'] . ".";
            }
            $this->storage_full_email($users['User']['email'], $account_id);
        }
        $this->set('ajaxdata', "$errorMessages");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function generate_s3_uri($object_name, $items) {

        $bucket = Configure::read('bucket_name');

        $request_uri = "/" . $bucket;
        $request_uri .= '/' . urlencode($object_name);

        $query = "";

        if ($items) {
            foreach ($items as $key => $value) {
                if (!empty($query))
                    $query .= "&";
                if (empty($value)) {
                    $query .= $key;
                } else {
                    $query .= $key . "=" . urlencode($value);
                }
            }
        }

        $request_uri .= "?" . (!empty($query) ? $query : "");
        return parse_url($request_uri);
    }

    function generate_rest_signature($method, $uri, $headers, $amz_headers) {

        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name');
        $downloadAudioTimeout = '+60 minutes';

        $http_verb = $method;
        $content_md5 = "";
        $content_type = "";
        $expires = strtotime($downloadAudioTimeout);
        $canonicalizedAmzHeaders = "";
        $canonicalizedResource = $uri;

        //$stringToSign = $http_verb . "\n" . $content_md5 . "\n" . $content_type . "\n" . $expires . "\n" . $canonicalizedAmzHeaders . $canonicalizedResource;
        //$signature = $this->hex2b64($this->hmacsha1($aws_secret_key, utf8_encode($stringToSign)));
        $signature = "";

        return $signature;
    }

    function amazon_jnlp_signature() {

        $this->layout = null;

        CakeLog::write('debug', 'Something did not work');

        $xml = '<?xml version="1.0" encoding="UTF-8"?><UploadClient>';


        $request_body = @file_get_contents('php://input');
        $xmlDoc = simplexml_load_string($request_body);

        $parts = array();

        $multiUploads = $xmlDoc->xpath("/UploadClient/MultipartStartUpload");

        for ($i = 0; $i < count($multiUploads); $i++) {

            $multiUpload = $multiUploads[$i];
            $dt = gmdate("D, d M Y H:i:s T");

            $key = $multiUpload['key'];
            $uri = $this->generate_s3_uri($key, array('uploads' => ''));
            $headers = array();
            $amz_headers = array('x-amz-acl' => 'public-read');
            $signature = $this->generate_rest_signature('POST', $uri, $headers, $amz_headers);
            // @parts << { :key => key, :date => headers['Date'], :signature => signature }

            $part = array(
                'key' => $multiUpload['key'],
                'date' => $dt,
                'signature' => $signature
            );

            $parts[] = $part;
            //CakeLog::write('debug', var_export($part, true));
        }


        if (count($parts) > 0) {
            $xml .= '<Parts>';
        }

        for ($i = 0; $i < count($parts); $i++) {

            $part = $parts[$i];
            $xml .= '<Part key="temp/accounts/2/' . $part['key'] . '" signature="' . $part['signature'] . '" date="' . $part['date'] . '"></Part>';
        }

        if (count($parts) > 0) {
            $xml .= '</Parts>';
        }

        $xml .= '</UploadClient>';

        $this->set('ajaxdata', "$xml");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function amazon_jnlp_success($user_id, $account_id) {

        $request_body = @file_get_contents('php://input');
        $file_name = $request_body;


        $this->request->data['video_file_name'] = $file_name;
        $this->request->data['video_url'] = $file_name;

        $this->AccountFolder->create();
        $data = array(
            'account_id' => $account_id,
            'folder_type' => 3,
            'name' => $this->data['video_file_name'],
            'desc' => '',
            'active' => 1,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
        );

        $this->AccountFolder->save($data, $validation = TRUE);
        $account_folder_id = $this->AccountFolder->id;


        $path_parts = pathinfo($this->request->data['video_url']);
        $video_file_name = $this->cleanFileName($path_parts['filename']) . "." . $path_parts['extension'];

        $this->Document->create();
        $data = array(
            'account_id' => $account_id,
            'doc_type' => '1',
            'url' => $this->request->data['video_url'],
            'original_file_name' => $this->request->data['video_file_name'],
            'active' => 1,
            'created_date' => date("Y-m-d h:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d h:i:s"),
            'last_edit_by' => $user_id,
            'published' => '0',
            'post_rubric_per_video' => '1'
        );

        if ($this->Document->save($data, $validation = TRUE)) {

            $video_id = $this->Document->id;


            require('src/services.php');
            //uploaded video
            $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
            $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
            $s3_src_folder_path = $this->request->data['video_url'];


            //echo $uploaded_video_file; die;
            $s3_service = new S3Services(
                    Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
            );

            $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);
            $s3_zencoder_id = $s3_service->encode_videos($s_url, "uploads/" . $s3_dest_folder_path_only, "uploads/" . $s3_dest_folder_path);
            if ($s3_zencoder_id == FALSE) {
                $this->AccountFolder->updateAll(array('active' => '0'), array('account_folder_id' => $account_folder_id));
                $this->sendVideoUploadingFailed();
                exit;
            } else {
                $this->Document->create();
                $this->Document->updateAll(
                        array(
                    'zencoder_output_id' => $s3_zencoder_id,
                    'url' => "'$s3_dest_folder_path'",
                    'file_size' => $this->request->data['video_file_size']
                        ), array('id' => $video_id)
                );

                $this->AccountFolderDocument->create();
                $data = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $video_id,
                    'title' => $file_name,
                    'zencoder_output_id' => $s3_zencoder_id,
                    'desc' => $this->request->data['video_desc']
                );
                $this->AccountFolderDocument->save($data, $validation = TRUE);
            }
        }


        $this->layout = null;
        $this->set('ajaxdata', "success");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function amazon_jnlp_failure() {

        $this->layout = null;
        $this->set('ajaxdata', "success");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function amazon_signature() {

        $this->layout = null;


        /*  */

        $aws_access_key_id = 'AKIAJLZK6AH42MMIBMAA';
        $aws_secret_key = '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';
        $aws_bucket = Configure::read('bucket_name');
        $downloadAudioTimeout = '+20 minutes';
        $file_path = 'kupload.mov';

        $http_verb = "GET";
        $content_md5 = "";
        $content_type = "";
        $expires = strtotime($downloadAudioTimeout);
        $canonicalizedAmzHeaders = "";
        $canonicalizedResource = '/' . $aws_bucket . '/' . $file_path;

        $stringToSign = $http_verb . "\n" . $content_md5 . "\n" . $content_type . "\n" . $expires . "\n" . $canonicalizedAmzHeaders . $canonicalizedResource;

        $signature = urlencode($this->hex2b64($this->hmacsha1($aws_secret_key, utf8_encode($stringToSign))));

        $url = "https://$aws_bucket.s3.amazonaws.com/$file_path?AWSAccessKeyId=$aws_access_key_id&Signature=$signature&Expires=$expires";


        $this->set('ajaxdata', "$url");
        $this->render('/Elements/ajax/ajaxreturn');
    }

    function copy() {
        $document_id = $this->request->data['document_id'];
        $account_folder_id = isset($this->request->data['account_folder_id']) ? $this->request->data['account_folder_id'] : '';
        $current_account_folder_id = isset($this->request->data['current_huddle_id']) ? $this->request->data['current_huddle_id'] : '';
        $result = array(
            'status' => FALSE,
            'message' => ''
        );
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $view = new View($this, false);
        if (!empty($document_id)) {
            $conditions = array('document_id' => $document_id, 'account_folder_id' => $current_account_folder_id);
            $myfilesRow = $this->AccountFolderDocument->get_row($conditions);

            if (count($account_folder_id) > 0 && $myfilesRow != '') {
                $copyRecord = array();
                $docs_data = array();
                $is_copied_video_library = false;
                $copied_video_id = array();
                for ($i = 0; $i < count($account_folder_id); $i++) {
                    foreach ($myfilesRow as $row) {
                        if ((int) $account_folder_id[$i] == -1) {
                            $this->copyToVideoLibrary($myfilesRow);
                            $is_copied_video_library = true;
                            continue;
                        }
                        if ($view->Custom->check_if_eval_huddle($account_folder_id[$i]) == 1) {
                            if ($view->Custom->check_if_evaluated_participant($account_folder_id[$i], $user_id) == 1) {
                                $get_eval_participant_videos = $view->Custom->get_eval_participant_videos($account_folder_id[$i], $user_id);

                                $submission_allowed_count = 1;
                                $submission_allowed_count = $view->Custom->get_submission_allowed($account_folder_id[$i]);

                                if ($get_eval_participant_videos >= $submission_allowed_count) {
                                    $result['message'] = 'You have already submitted a video to be evaluated.  You must delete your submission before resubmitting.';
                                    echo json_encode($result);
                                    die;
                                }
                            }
                        }
                        $document = $this->Document->get_document_row($row['AccountFolderDocument']['document_id']);
                        if (empty($document))
                            continue;
                        $old_video_id[] = $document['Document']['id'];
                        $docs_data[] = array('id' => null, 'created_date' => date("Y-m-d H:i:s"), 'last_edit_date' => date("Y-m-d H:i:s"), 'recorded_date' => date("Y-m-d H:i:s"), 'created_by' => $user_id, 'last_edit_by' => $user_id) + $document['Document'];
                        if (!empty($account_folder_id[$i])) {
                            $copyRecord[] = array(
                                'account_folder_id' => $account_folder_id[$i],
                                'document_id' => '',
                                'is_viewed' => $row['AccountFolderDocument']['is_viewed'],
                                'title' => $row['AccountFolderDocument']['title'],
                                'desc' => $row['AccountFolderDocument']['desc'],
                                'zencoder_output_id' => $row['AccountFolderDocument']['zencoder_output_id']
                            );
                        }
                    }
                    
                        $user_email_info = $this->User->find('first', array('conditions' => array('id' => $user_id)));

                        if($is_copied_video_library)
                        {
                            $this->create_churnzero_event('Videos+Shared+to+Video+Library', $account_id , $user_email_info['User']['email']);
                        }
                        else
                        {
                            $this->create_churnzero_event('Videos+Shared+to+Huddles', $account_id , $user_email_info['User']['email']);   
                        }
                    
                    
                    
                }

                $cnt = 0;
                if (count($docs_data) > 0) {
                    App::import("Model", "JobQueue");
                    $db = new JobQueue();
                    foreach ($docs_data as $row) {
                        $this->Document->create();
                        $row['post_rubric_per_video'] = '1';
                        $this->Document->save($row);
                        $copied_video_id[] = $this->Document->id;
                        if ($row['published'] == 0) {
                            $requestXml = '<TranscodeVideoJob><document_id>' . $this->Document->id . '</document_id><source_file>' . $row['url'] . '</source_file></TranscodeVideoJob>';
                            $video_data = array(
                                'JobId' => '2',
                                'CreateDate' => date("Y-m-d H:i:s"),
                                'RequestXml' => $requestXml,
                                'JobQueueStatusId' => 1,
                                'CurrentRetry' => 0
                            );
                            $db->create();
                            $db->save($video_data);
                        } else {
                            $documentFiles = $this->DocumentFiles->get_document_row($old_video_id[$cnt]);
                            if (!empty($documentFiles)) {
                                $this->create_churnzero_event('Video+Hours+Uploaded', $account_id , $user_email_info['User']['email'],$documentFiles['DocumentFiles']['duration']); 
                                $video_data = array('id' => null, 'document_id' => $this->Document->id,'created_at'=>date('Y-m-d H:i:s'),
                                        'debug_logs'=>"Cake::HuddlesController::copy::line=7578::document_id=".$this->Document->id."::status=".$documentFiles['DocumentFiles']['transcoding_status']."::created") + $documentFiles['DocumentFiles'];
                                $this->checkDocumentFilesRecordExist($this->Document->id, $video_data, $this->request->data);
                                $this->DocumentFiles->create();
                                $this->DocumentFiles->save($video_data);
                            }
                        }
                        $cnt++;
                    }
                }
                $result_ids = array();

                $count = 0;
                foreach ($copyRecord as $data) {
                    $data['document_id'] = $copied_video_id[$count];

                    $this->AccountFolderDocument->create();
                    $this->AccountFolderDocument->save($data);
                    $result_ids[] = $this->AccountFolderDocument->id;
                    if (isset($this->request->data['copy_notes']) && $this->request->data['copy_notes'] == "true") {
                        @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 2);
                        //  @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 3);
                        @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 6);
                    }
                    $count++;

                    $user_activity_logs = array(
                        'ref_id' => $data['document_id'],
                        'desc' => 'Video Copied',
                        'url' => $this->base . '/huddles/view/' . $data['account_folder_id'] . "/1/" . $data['document_id'],
                        'type' => '22',
                        'account_id' => $account_id,
                        'account_folder_id' => $data['account_folder_id'],
                        'environment_type' => 2
                    );

                    $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                }
                
                
                
                if (count($result_ids) > 0 || $is_copied_video_library == TRUE) {
                    if ($is_copied_video_library == true && count($result_ids) > 0) {
                        $this->EmailCopyVideo($account_folder_id, $data['document_id']);
                        $result['message'] = 'You have successfully copied your video file(s) into selected huddle and Account video library.';
                    } elseif ($is_copied_video_library == true && count($result_ids) <= 0) {
                        $result['message'] = 'You have successfully copied your video file(s) to your Account video library.';
                    } else {
                        $this->EmailCopyVideo($account_folder_id, $data['document_id']);
                        $result['message'] = 'You have successfully copied your video file(s) into selected huddle.';
                    }

                    $result['status'] = TRUE;
                } else {
                    $result['message'] = 'Video is not Copied into selected huddle.';
                }
            } else {
                $result['message'] = 'Please select at least one video to copied into huddle';
            }
        } else {
            $result['message'] = 'Please select at least one video to copied into huddle';
        }
        echo json_encode($result);
        exit;
    }

    function EmailCopyVideo($account_folder_id) {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        for ($i = 0; $i < count($account_folder_id); $i++) {
            $huddle = $this->AccountFolder->getHuddle($account_folder_id[$i], $account_id);
            $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id[$i]);
            $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id[$i]);
            $huddle_user_ids = array();
            if ($userGroups && count($userGroups) > 0) {
                foreach ($userGroups as $row) {
                    $huddle_user_ids[] = $row['user_groups']['user_id'];
                }
            }
            if ($huddleUsers && count($huddleUsers) > 0) {
                foreach ($huddleUsers as $row) {
                    $huddle_user_ids[] = $row['huddle_users']['user_id'];
                }
            }

            if (count($huddle_user_ids) > 0) {
                $user_ids = implode(',', $huddle_user_ids);
                if (!empty($user_ids))
                    $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
            } else {
                $huddleUserInfo = array();
            }

            if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                $commentsData = array(
                    'account_folder_id' => $account_folder_id[$i],
                    'huddle_name' => $huddle[0]['AccountFolder']['name'],
                    'video_link' => '/Huddles/view/' . $account_folder_id[$i] . '/1/' . $video_id,
                    'participating_users' => $huddleUserInfo
                );
                foreach ($huddleUserInfo as $row) {
                    if ($user_id == $row['User']['id']) {
                        continue;
                    }
                    $commentsData ['email'] = $row['User']['email'];
                    if ($this->check_subscription($row['User']['id'], '7', $account_id)) {
                        $this->sendCopyVideoEmail($commentsData, $row['User']['id']);
                    }
                }
            }
        }
    }

    function copyToVideoLibrary($myfilesRow) {

        $myfilesRow = $myfilesRow[0];

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $this->AccountFolder->create();
        $document = $this->Document->getVideoDetails($myfilesRow['AccountFolderDocument']['document_id']);

        $data_af = array(
            'account_id' => $account_id,
            'folder_type' => 2,
            'name' => $myfilesRow['AccountFolderDocument']['title'],
            'desc' => $myfilesRow['AccountFolderDocument']['desc'],
            'active' => $document['Document']['active'],
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
        );

        $this->AccountFolder->save($data_af, $validation = TRUE);

        $account_folder_id = $this->AccountFolder->id;
        $account_folders_meta_data = array(
            'account_folder_id' => $account_folder_id,
            'meta_data_name' => 'tag',
            'meta_data_value' => '',
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
        );

        $this->AccountFolderMetaData->create();
        $this->AccountFolderMetaData->save($account_folders_meta_data);

        $this->Document->create();

        $data_doc = array(
            'account_id' => $document['Document']['account_id'],
            'doc_type' => $document['Document']['doc_type'],
            'url' => $document['Document']['url'],
            'original_file_name' => $document['Document']['original_file_name'],
            'active' => $document['Document']['active'],
            'view_count' => 0,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'file_size' => $document['Document']['file_size'] ? $document['Document']['file_size'] : '',
            'last_edit_by' => $user_id,
            'encoder_provider' => $document['Document']['encoder_provider'],
            'published' => $document['Document']['published'],
            'post_rubric_per_video' => '1'
        );


        $this->Document->save($data_doc, $validation = TRUE);

        $video_id = $this->Document->id;

        App::import("Model", "JobQueue");
        $db = new JobQueue();

        if ($document['Document']['published'] == 0) {

            $requestXml = '<TranscodeVideoJob><document_id>' . $video_id . '</document_id><source_file>' . $document['Document']['url'] . '</source_file></TranscodeVideoJob>';
            $video_data = array(
                'JobId' => '2',
                'CreateDate' => date("Y-m-d H:i:s"),
                'RequestXml' => $requestXml,
                'JobQueueStatusId' => 1,
                'CurrentRetry' => 0
            );
            $db->create();
            $db->save($video_data);
        } else {
            $documentFiles = $this->DocumentFiles->get_document_row($myfilesRow['AccountFolderDocument']['document_id']);
            $user_email_info = $this->User->find('first', array('conditions' => array('id' => $user_id)));
            $this->create_churnzero_event('Video+Hours+Uploaded', $account_id , $user_email_info['User']['email'],$documentFiles['DocumentFiles']['duration']);
            $video_data = array('id' => null, 'document_id' => $video_id,'created_at'=>date('Y-m-d H:i:s'),
                    'debug_logs'=>"Cake::HuddlesController::copyToVideoLibrary::line=7773::document_id=".$video_id."::status=".$documentFiles['DocumentFiles']['transcoding_status']."::created") + $documentFiles['DocumentFiles'];
            $this->checkDocumentFilesRecordExist($video_id, $video_data, $this->request->data);
            $this->DocumentFiles->create();
            $this->DocumentFiles->save($video_data);
        }

        $this->AccountFolderDocument->create();
        $data_afd = array(
            'account_folder_id' => $account_folder_id,
            'document_id' => $video_id,
            'title' => $myfilesRow['AccountFolderDocument']['title'],
            'zencoder_output_id' => $myfilesRow['AccountFolderDocument']['zencoder_output_id'],
            'desc' => $myfilesRow['AccountFolderDocument']['desc']
        );
        $this->AccountFolderDocument->save($data_afd, $validation = TRUE);
        $user_activity_logs = array(
            'ref_id' => $video_id,
            'desc' => 'Video Copied',
            'url' => $this->base . '/videoLibrary/view/' . $account_folder_id . '/' . $account_id,
            'type' => '22',
            'account_id' => $account_id,
            'account_folder_id' => $account_folder_id,
            'environment_type' => 2
        );

        $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
        return true;
    }

    function print_pdf_comments($video_id, $changeType, $file_name) {

        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_current_account = $this->Session->read('user_current_account');
        $huddle_id = $this->Session->read('account_folder_id');

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);

        $user_id = $users['User']['id'];

        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $h_type = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $view = new View($this, false);
        if ((!$view->Custom->check_if_evalutor($huddle_id, $user_id)) && (($h_type == '2' && $view->Custom->is_enabled_coach_feedback($huddle_id)) || $h_type == '3')) {

            $result = $this->Comment->getActiveVideoComments($video_id, $changeType, '', '', '', 1);
        } else {
            $result = $this->Comment->getVideoComments($video_id, $changeType, '', '', '', 1);
        }


        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        $params = array(
            'user_id' => $user_id,
            'video_id' => $video_id,
            'videoComments' => $videoComments,
            'huddle_permission' => $huddle_permission,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account
        );

        $comments = array();
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array(1, 2)); //get tags
                $comments[] = array_merge($comment, array("tags" => array_merge($get_standard, $get_tags)));
            }
        }

        $this->set('video_id', $video_id);
        $this->set('user_id', $user_id);
        $this->set('videoComments', $comments);
        $this->set('huddle_permission', $huddle_permission);
        $this->set('huddle', $huddle);
        $this->set('user_current_account', $user_current_account);

        $this->layout = 'pdf';

        echo $this->render();
        die;
    }

    function print_pdf_comments_1($video_id) {

        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_current_account = $this->Session->read('user_current_account');
        $huddle_id = $this->Session->read('account_folder_id');

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);

        $user_id = $users['User']['id'];

        $result = $this->Comment->getVideoComments($video_id, 2, '', '', '', 1);

        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        $params = array(
            'user_id' => $user_id,
            'video_id' => $video_id,
            'videoComments' => $videoComments,
            'huddle_permission' => $huddle_permission,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account
        );

        $comments = array();
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get tags
                $comments[] = array_merge($comment, array("tags" => array_merge($get_standard, $get_tags)));
            }
        }

        $this->set('video_id', $video_id);
        $this->set('user_id', $user_id);
        $this->set('videoComments', $comments);
        $this->set('huddle_permission', $huddle_permission);
        $this->set('huddle', $huddle);
        $this->set('user_current_account', $user_current_account);

        $params_1 = array(
            'video_id' => $video_id,
            'user_id' => $user_id,
            'videoComments' => $comments,
            'huddle_permission' => $huddle_permission,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account
        );

        $view = new View($this, true);
        $html = $view->element('pdf/print_pdf_comments', $params_1);

        header("Content-type: application/pdf");
        require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php');
        spl_autoload_register('DOMPDF_autoload');
        $filename_email = $huddle_id . date("Ymd") . date("His") . '.pdf';
        $filename = APP . 'tmp/PdfReport' . $filename_email;
        $dompdf = new DOMPDF();
        $dompdf->set_paper = 'A4';
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents($filename, $output);

        echo $filename_email;
        die;
    }

    function print_excel_comments($video_id, $changeType) {

        $this->layout = null;
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_current_account = $this->Session->read('user_current_account');
        $huddle_id = $this->Session->read('account_folder_id');

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);

        $user_id = $users['User']['id'];

        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $h_type = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $view = new View($this, false);
        if ((!$view->Custom->check_if_evalutor($huddle_id, $user_id)) && (($h_type == '2' && $view->Custom->is_enabled_coach_feedback($huddle_id)) || $h_type == '3')) {

            $result = $this->Comment->getActiveVideoComments($video_id, $changeType, '', '', '', 1);
        } else {
            $result = $this->Comment->getVideoComments($video_id, $changeType, '', '', '', 1);
        }

        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array(1, 2)); //get tags
                $comments[] = array_merge($comment, array("tags" => array_merge($get_standard, $get_tags)));
            }
        }
        $params = array(
            'user_id' => $user_id,
            'video_id' => $video_id,
            'videoComments' => $comments,
            'huddle_permission' => $huddle_permission,
            'huddle' => $huddle,
            'user_current_account' => $user_current_account
        );
        $this->set('video_id', $video_id);
        $this->set('user_id', $user_id);
        $this->set('videoComments', $comments);
        $this->set('huddle_permission', $huddle_permission);
        $this->set('huddle', $huddle);
        $this->set('user_current_account', $user_current_account);
        $html = "<html><body>";
        $html .= $this->render('/Huddles/pdf/print_excel_comments');
        $html .= "</body></html>";
        $this->export_in_phpExcel($html, 'comments.xlsx');
        die;
    }

    function excel_sheet_test() {
        $html = "<html><body><table>
                <thead> <tr> <td colspan='2'> <h1> Main Heading </h1> <td> </tr> </thead>
                <tbody>
                <tr>
                  <th style='background:#ccc; color:red; font-size:15px'> Name <th>
                  <th style='background:#ccc; color:red; font-size:15px'> Class <th>
                </tr>
                <tr>
                  <td style='background:#fff; color:green; font-size:13px'> Jhon <th>
                  <td style='background:#fff; color:gree; font-size:13px'> 9th <th>
                </tr>
                </tbody>

                </table></body></html>";

        // Load the table view into a variable
//        $html = $this->load->view('table_view', $data, true);
        App::import('Vendor', 'PHPExcel');
// Put the html into a temporary file
        $tmpfile = time() . '.html';
        file_put_contents($tmpfile, $html);

// Read the contents of the file into PHPExcel Reader class
        $reader = new PHPExcel_Reader_HTML;
        $content = $reader->load($tmpfile);

// Pass to writer and output as needed
        $objWriter = PHPExcel_IOFactory::createWriter($content, 'Excel2007');
//        echo "<pre>";
//        print_r($objWriter);
//        die;
        $objWriter->save('excelfile.xlsx');

// Delete temporary file
//        unlink($tmpfile);

        die;
//        $this->PhpExcel->createWorksheet()
//                ->setDefaultFont('Calibri', 12);
//
//
//        $this->PhpExcel->loadWorksheet($html)
//                ->output();
    }

    public function testxls() {
//        echo "<pre>";
//        print_r($_SERVER['DOCUMENT_ROOT'].'/'.;);
//        die;
        $folderToSaveXls = $_SERVER['DOCUMENT_ROOT'];
        $objPHPExcel = new PHPExcel();
        $this->PHPExcel->getProperties()->setCreator("Maarten Balliauw")
                ->setLastModifiedBy("Maarten Balliauw")
                ->setTitle("PHPExcel Test Document")
                ->setSubject("PHPExcel Test Document")
                ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                ->setKeywords("office PHPExcel php")
                ->setCategory("Test result file");

        $this->PHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Hello')
                ->setCellValue('B2', 'world!')
                ->setCellValue('C1', 'Hello')
                ->setCellValue('D2', 'world!');

        $objWriter = $this->PhpExcel->createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($folderToSaveXls . '/test.xls');
    }

    function save_css_attributes() {
        $left = $_POST['left'];
        $this->Session->write('left', $left);
        echo "done";
        die;
    }

    function check_amazon_transcoding() {

        $this->layout = null;

        $client = ElasticTranscoderClient::factory(array(
                    'key' => 'AKIAJLZK6AH42MMIBMAA',
                    'region' => 'us-east-1',
                    'secret' => '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',
        ));

        $result = $client->listPipelines(array());

        var_dump($result['Pipelines'][1]['Id']);
        die;
        $pipelineId = $result['Pipelines'][1]['Id'];
        $file_name = 'app-sibme-25-03-2014-07-24-23-comp.MOV';
        $file_name_explode = explode('.', $file_name);

        $job = $client->createJob(array(
            'PipelineId' => $pipelineId,
            'Input' => array(
                'Key' => 'app-sibme-25-03-2014-07-24-23-comp.MOV',
                'FrameRate' => 'auto',
                'Resolution' => 'auto',
                'AspectRatio' => 'auto',
                'Interlaced' => 'auto',
                'Container' => 'auto',
            ),
            'Output' => array(
                'Key' => 'app-sibme-25-03-2014-07-24-23-comp.mp4',
                'ThumbnailPattern' => $file_name_explode[0] . '_thumb_{count}',
                'Rotate' => '0',
                'PresetId' => '1425215270054-j95h0f'
            )
        ));

        echo "<pre>";
        var_dump($job['Job']['Id']);
        echo "</pre>";

        die;
    }

    function submit_trim_request($video_id, $huddle_id) {

        $this->layout = null;
        $view = new View($this, false);
        $alert_messages = $view->Custom->get_page_lang_based_content('alert_messages');
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $workspace = isset($this->request->data['workspace']) ? 1 : 0;
        $library = isset($this->request->data['library']) ? 1 : 0;

        if(isset($this->request->data['copy_comments']) && $this->request->data['copy_comments'] )
        {
            $trim_push = false;
        }
        else if(isset($this->request->data['copy_comments']) && !$this->request->data['copy_comments'])
        {
            $trim_push = true;
        }
        else
        {
            $trim_push = false;
        }
        $transcoder_type = $this->getTranscoderType($account_id);
        require('src/services.php');
        $s3_service = new S3Services(
                Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
        );

        $video = $this->Document->find('first', array('conditions' => array('id' => $video_id)));
        $document_files_data = $this->DocumentFiles->find('first', array('conditions' => array('document_id' => $video_id)));
        $huddle = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));
        if(isset($huddle['AccountFolder']['folder_type']) && $huddle['AccountFolder']['folder_type'] == 2)
        {
            $library = 1;
        }
        if($library)
        {
            $parent_library_check = $view->Custom->get_show_parent_video_library($users['accounts']['account_id']);
            $account_details = $this->Account->find('first', array(
                'conditions' => array(
                    'id' => $users['accounts']['account_id']
                )
            ));

            if($parent_library_check && $account_details['Account']['parent_account_id'] != 0)
            {
                $account_id = $account_details['Account']['parent_account_id'];
            }
        }
        $huddle_document = $this->AccountFolderDocument->find('first', array(
            'conditions' => array(
                'account_folder_id' => $huddle_id,
                'document_id' => $video_id
            )
        ));
        $account_folder = null;
        if ($huddle['AccountFolder']['folder_type'] != 1) {
            $this->AccountFolder->create();
            $data = array(
                'account_id' => $account_id,
                'folder_type' => $workspace ? 3 : 2,
                'name' => $huddle['AccountFolder']['name'],
                'desc' => $huddle['AccountFolder']['desc'],
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
            );
            $saveStatus = $this->AccountFolder->save($data, $validation = TRUE);
            $account_folder_id = $saveStatus ? $this->AccountFolder->id : 0;
            if($workspace)
            {
                $account_folders_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'folder_type',
                    'meta_data_value' => '5',
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_by' => $user_id
                );
                $this->AccountFolderMetaData->create();
                $this->AccountFolderMetaData->save($account_folders_meta_data);
            }
            
        } else {
            $saveStatus = true;
            $account_folder_id = $huddle['AccountFolder']['account_folder_id'];
        }

        if ($saveStatus) {

            $document_files = $this->get_document_url($video['Document']);
            if (empty($document_files['url'])) {

                echo "No Record found";
                die;
            } else {
                
                if(strpos($document_files['relative_url'], 'temp/') !== false)
                {
                    $folder_path = 'temp';
                }
                elseif (strpos($document_files['relative_url'], 'tempupload/') !== false ) {
                    $folder_path = 'tempupload';
                }
                else
                {
                    $folder_path = 'uploads';
                }

                $streamHostUrl = Configure::read('cloudfront_host_url');
                $original_extension = pathinfo($document_files_data['DocumentFiles']['url']);
                $video['Document']['url'] = str_replace("$folder_path/", "", $document_files['relative_url']);
                $path_parts = pathinfo($video['Document']['url']);
                $startTime = explode(".", $this->request->data['startVideo']);
                $endTime = explode(".", $this->request->data['endVideo']);

                if ((int) $startTime[1] < 0)
                    $startTime[1] = 0;

                $timeDiffSecs = (int) ((float) $endTime[0] - (float) $startTime[0]);
                $timeDiffMilliSecs = (int) ((float) $endTime[1] - (float) $startTime[1]);

                if ((int) $timeDiffMilliSecs < 0)
                    $timeDiffMilliSecs = 0;
                $extension = $original_extension['extension'];
                if($transcoder_type == 2){
                    $startCode =gmdate("H:i:s",$startTime[0]);
                    $endCode =gmdate("H:i:s", $endTime[0]);
                    $name_modifier = "_trimmed_".date("YmdHim");
                    $s3_zencoder_output = $s3_service->trim_videos(
                        Configure::read('amazon_base_url') . "$folder_path/" . $path_parts['dirname'] . "/" . $path_parts['filename'] . "." . $extension, $document_files['relative_url'], $document_files['relative_url'], $startCode, $endCode,$folder_path,$transcoder_type,$name_modifier
                           );
                      $trim_url = $folder_path.'/'.str_replace("/transcoded", "",$path_parts["dirname"]) .'/transcoded/'. $path_parts["filename"] . $name_modifier .".". $path_parts["extension"];
                  }
                  else
                  {
                    $startCode =$startTime[0] . "." . sprintf('%03d', $startTime[1]);
                    $endCode =$timeDiffSecs . "." . sprintf('%03d', $timeDiffMilliSecs);
                     $s3_zencoder_output = $s3_service->trim_videos(
                        Configure::read('amazon_base_url') . "$folder_path/" . $path_parts['dirname'] . "/" . $path_parts['filename'] . "." . $extension, "$folder_path/" . $path_parts['dirname'], "$folder_path/" . $path_parts['dirname'] . "/" . $path_parts['filename'], $startCode, $endCode,$folder_path,$transcoder_type
                     );
                }
                
                $zencoder_trim_output_array = explode('||', $s3_zencoder_output);
                $zencoder_trim_output_id = $zencoder_trim_output_array[0];
                $url_trim = $zencoder_trim_output_array[1];
                
                if ($original_extension['extension'] == 'mp3' || $original_extension['extension'] == 'wav') {
                    $url_trim_parts = pathinfo($url_trim);
                    $url_trim = $url_trim_parts['filename'] . '.' . $original_extension['extension'];
                    $original_file_name = $path_parts['filename'] . '.' . $original_extension['extension'];
                    $url_trim = $folder_path."/" . $path_parts['dirname'] . "/" .$url_trim;
                } else {
                    $original_file_name = $path_parts['filename'] . "." . $path_parts['extension'];
                    $url_trim = $folder_path."/".$url_trim;
                }

                $parent_folder_id = isset($this->request->data['parent_folder_id']) ? $this->request->data['parent_folder_id'] : 0;
                $this->Document->create();
                $data = array(
                    'account_id' => $account_id,
                    'doc_type' => '1',
                    'url' => $url_trim,
                    'original_file_name' => $original_file_name,
                    'active' => 1,
                    'created_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'file_size' => 0,
                    'last_edit_by' => $user_id,
                    'zencoder_output_id' => $zencoder_trim_output_id,
                    'published' => '0', // Changed to 1 because 0 was keeping trimmed video as always "in processing..."
                    'recorded_date' => date("Y-m-d H:i:s"),
                    'encoder_provider' => Configure::read('encoder_provider'),
                    'post_rubric_per_video' => '1',
                    'source_document_id' => $video_id,
                    'source_type' => '2',
                    'parent_folder_id' => $parent_folder_id,
                    'aws_transcoder_type' => $transcoder_type
                );

                $this->Document->save($data, $validation = TRUE);
                $document_id = $this->Document->id;

                $this->DocumentFiles->create();
                $data = array(
                    'document_id' => $document_id,
                    'url' => $url_trim,
                    'resolution' => 1080,
                    'duration' => $timeDiffSecs,
                    'file_size' => 0,
                    'default_web' => 1,
                    'transcoding_job_id' => $zencoder_trim_output_id,
                    'transcoding_status' => -1,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'debug_logs'=>"Cake::HuddlesController::submit_trim_request::line=8307::document_id=$document_id::status=-1::created"
                );
                $this->checkDocumentFilesRecordExist($document_id, $data, $this->request->data);
                $this->DocumentFiles->save($data, $validation = TRUE);
                
                $trimmed_file_name = $this->_pathinfo($huddle_document['AccountFolderDocument']['title']);

                $this->AccountFolderDocument->create();
                $title = $trimmed_file_name['filename'] . " " . $alert_messages['trimmed_append'];
                $desc = $huddle['AccountFolder']['desc'];
                $data = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $document_id,
                    'title' => $trimmed_file_name['filename'] . " " . $alert_messages['trimmed_append'],
                    'zencoder_output_id' => $zencoder_trim_output_id,
                    'encoder_provider' => Configure::read('encoder_provider'),
                    'desc' => $desc,
                    'video_framework_id' => $huddle_document['AccountFolderDocument']['video_framework_id']
                );
                $this->AccountFolderDocument->save($data, $validation = TRUE);
                /*save the transcribe status 1 if allow on account level*/
                $this->custom->CheckAllowTrancribe($document_id,$account_id);
                $account_folder_doc = $this->AccountFolderDocument->get();
                $account_folder_doc = (object) $account_folder_doc["AccountFolderDocument"];
            }

            $this->Session->setFlash($this->language_based_messages['the_trimming_request_has_been_successfully_submitted_an_email_notification_will_be_sent_when_the_video_clip_is_ready_to_be_viewed'], 'default', array('class' => 'message success'));
            $doc_id = $this->Document->id;
            $doc_object = self::modify_new_document_websocket($doc_id, $users, $title, $desc, $account_folder_doc->id, $timeDiffSecs, $account_folder_id);
            $channel = 'huddle-details-' . $huddle_id;
            $subjects = [];
            $uncat_videos = [];
            if ($workspace) {
                $channel = "workspace-" . $account_id . "-" . $user_id;
            }
            else if ($library)
            {
                $channel = "library-" . $account_id;
                $lumen_data = $view->Custom->sendHttpRequest("get_categories", ["account_id" => $account_id, "uncat_counts" => 1]);
                $subjects = $lumen_data["data"];
                $uncat_videos = $lumen_data["uncat_counts"];
            }
            $doc_object->thubnail_url = '';
            $channel_data = array(
                'item_id' => $doc_object->doc_id,
                'document_id' => $doc_object->doc_id,
                'reference_id' => $doc_object->doc_id,
                'is_video_crop' => 1,
                'data' => $doc_object,
                'user_id' => $user_id,
                'huddle_id' => $huddle_id,
                'channel' => $channel,
                'video_categories' => [],
                'sidebar_categories' => $subjects,
                'uncat_videos' => $uncat_videos,
                'event' => "resource_added"
            );
            $view = new View($this, false);
          //  $view->Custom->BroadcastEvent($channel_data);
            if($trim_push)
            {   
              if($view->Custom->is_push_notification_allowed($account_id))
              {  
                  $view->Custom->BroadcastEvent($channel_data);
              }
              else
              {
                  $view->Custom->BroadcastEvent($channel_data,false);
              }
            }
            $doc_object->id = $doc_object->doc_id;
            echo json_encode(array('document' => $doc_object));

            //$this->redirect('/MyFiles');
            die;
        }
    }

    public function trim($video_id, $huddle_id) {
        $this->layout = 'trim';
//        $users = $this->Session->read('user_current_account');
//        $account_id = $users['accounts']['account_id'];
//        $user_id = $users['User']['id'];
//        $video_details = $this->AccountFolder->getAllMyFile($account_id, $user_id, $huddle_id);
        $video_details = $this->Document->find('first', array('conditions' => array('id' => $video_id)));
        $vidDocuments = $this->Document->getVideoDocumentsByVideo($video_details['Document']['id'], $huddle_id);
        $commentsTime = $this->Comment->getVideoComments($video_details['Document']['id']);

        $this->set('videoDetail', $video_details);
        $this->set('videoComments', $commentsTime);
        $this->set('huddle_id', $huddle_id);
        $this->set('video_id', $video_id);
        $this->set('vidDocuments', $vidDocuments);
    }

    function test_video() {

        $this->layout = 'test';
    }

    function get_comments_count($video_id) {
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $account_id = $users['accounts']['account_id'];
        $this->layout = null;
        $type = $this->request->data['type'];
        $result = $this->Comment->getVideoComments($video_id, $type, '', '', '', 1);

        $account_folder_details = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $video_id)));
        $account_folder_id = $account_folder_details['AccountFolderDocument']['account_folder_id'];
        $total_comments_reply = 0;

        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
        $h_type = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $view = new View($this, false);
        if ((!$view->Custom->check_if_evalutor($account_folder_id, $user_id)) && (($h_type == '2' && $view->Custom->is_enabled_coach_feedback($account_folder_id)) || $h_type == '3')) {
            $comment_result_count = $this->Comment->find('all', array(
                'conditions' => array(
                    'ref_id' => $video_id,
                    'active' => 1
                )
            ));
        } else {
            $comment_result_count = $this->Comment->find('all', array(
                'conditions' => array(
                    'ref_id' => $video_id,
                )
            ));
        }

        $total_comments_reply = count($comment_result_count);

//        if (count($result) > 0) {
//            $total_comments_reply = count($result);
//            foreach ($result as $cmnt) {
//
//                $total_comments_reply = $total_comments_reply + $this->get_total_replies($cmnt['Comment']['id']);
//            }
//        }


        $comments_ids_arr = array();
        $is_feedback_published = false;
        if (!empty($result)) {
            foreach ($result as $single_result):
                if ($single_result['Comment']['active'] == '1') {
                    $is_feedback_published = true;
                }
            endforeach;
        } else {
            $is_feedback_published = true;
        }

        $total_comments = '';
        $response = array();

        $total_commnets_count = 0;
//        if (count($result) > 0) {
//
//            for ($i = 0; $i < count($result); $i++) {
//
//                if (isset($result[$i]['Comment'])) {
//
//                    $item_comment_reply = $this->Comment->commentReplys($result[$i]['Comment']['id']);
//                    if (isset($item_comment_reply) && count($item_comment_reply) > 0) {
//                        $total_commnets_count +=1;
//                    }
//                    $total_commnets_count +=1;
//                }
//            }
//        }

        if (count($comment_result_count) > 0) {
            $response['total_comments'] = $total_comments_reply;
            $response['tag_count'] = $this->AccountTag->gettagscountonly($account_id, '1', $video_id);
        } else {
            $response['total_comments'] = 0;
            $response['tag_count'] = array();
        }
        if ($is_feedback_published) {
            $response['is_feedback_published'] = true;
        } else {
            $response['is_feedback_published'] = false;
        }
        echo json_encode($response);
        exit;
    }

    function save_standard_tag($standard_tag, $comment_id, $ref_id) {

        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $account_id = $users['accounts']['account_id'];
        $huddle_id = $this->Session->read('account_folder_id');
        $view = new View($this, false);
        $framework_id = $view->Custom->get_framework_id($account_id, $huddle_id);
        $standards = str_replace('# ', '', $standard_tag);
        $standardtags = explode(',', $standards);
        $standards_acc_tags = $this->request->data['observations_standard_acc_tag_id'];
        $standardtags_acc_tags = explode(',', $standards_acc_tags);
        foreach ($standardtags_acc_tags as $standardtag) {
            $tagcode = explode('-', $standardtag);
//            $code_standard = $this->AccountTag->getstandardbycode($tagcode[0], $account_id, $framework_id);
            $code_standard = $this->AccountTag->getstandardbycodeweb($standardtag, $account_id, $framework_id);
            $save_cmt_tag[] = array(
                "comment_id" => $comment_id,
                "ref_type" => "0",
                "ref_id" => $ref_id,
                "tag_title" => $code_standard['AccountTag']['tag_title'],
                "account_tag_id" => $code_standard['AccountTag']['account_tag_id'],
                "created_by" => $user_id,
                "created_date" => date('y-m-d H:i:s', time())
            );

            $user_activity_logs = array(
                'ref_id' => $comment_id,
                'desc' => $code_standard['AccountTag']['tag_title'],
                'url' => '/' . $comment_id,
                'type' => '15',
                'account_id' => $account_id,
                'account_folder_id' => $code_standard['AccountTag']['account_tag_id'],
                'environment_type' => 2
            );
            $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
        }
        if ($this->AccountCommentTag->saveMany($save_cmt_tag)) {
            return true;
        } else {
            return false;
        }
    }

    function save_tag($tags, $comment_id, $ref_id) {

        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $account_id = $users['accounts']['account_id'];
        $tag = str_replace('# ', '', $tags);
        $taginfos = explode(',', $tag);
        foreach ($taginfos as $info) {

            $get_tag_info = '';

            if(!isset($_POST['associate_default_tags'])) {
                $get_tag_info = $this->AccountTag->gettagbytitle($info, $account_id);
            }

            if(!empty($get_tag_info)) {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "1",
                    "ref_id" => $ref_id,
                    "tag_title" => $get_tag_info['AccountTag']['tag_title'],
                    "account_tag_id" => $get_tag_info['AccountTag']['account_tag_id'],
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $get_tag_info['AccountTag']['tag_title'],
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => $get_tag_info['AccountTag']['account_tag_id'],
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            } else {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "1",
                    "ref_id" => $ref_id,
                    "tag_title" => $info,
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $info,
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => '',
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }
        if ($this->AccountCommentTag->saveMany($save_cmt_tag)) {
            return true;
        } else {
            return false;
        }
    }

    function save_tag_1($tags, $comment_id, $ref_id) {

        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $account_id = $users['accounts']['account_id'];
        $tag = str_replace('# ', '', $tags);
        $taginfos = explode(',', $tag);
        foreach ($taginfos as $info) {

            $get_tag_info = '';

            if (!isset($_POST['associate_default_tags'])) {
                $get_tag_info = $this->AccountTag->gettagbytitle($info, $account_id);
            }

            if (!empty($get_tag_info)) {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "2",
                    "ref_id" => $ref_id,
                    "tag_title" => $get_tag_info['AccountTag']['tag_title'],
                    "account_tag_id" => $get_tag_info['AccountTag']['account_tag_id'],
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $get_tag_info['AccountTag']['tag_title'],
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => $get_tag_info['AccountTag']['account_tag_id'],
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            } else {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "2",
                    "ref_id" => $ref_id,
                    "tag_title" => $info,
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $info,
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => '',
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }
        if ($this->AccountCommentTag->saveMany($save_cmt_tag)) {
            return true;
        } else {
            return false;
        }
    }

    function save_tag_2($tags, $comment_id, $ref_id) {

        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $account_id = $users['accounts']['account_id'];
        $tag = str_replace('# ', '', $tags);
        $taginfos = explode(',', $tag);
        $this->AccountCommentTag->deleteAll(array('comment_id' => $comment_id, "ref_type" => "2"), $cascade = true, $callbacks = true);
        foreach ($taginfos as $info) {

            $get_tag_info = '';

            if (!isset($_POST['associate_default_tags'])) {
                $get_tag_info = $this->AccountTag->gettagbytitle($info, $account_id);
            }

            if (!empty($get_tag_info)) {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "2",
                    "ref_id" => $ref_id,
                    "tag_title" => $get_tag_info['AccountTag']['tag_title'],
                    "account_tag_id" => $get_tag_info['AccountTag']['account_tag_id'],
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $get_tag_info['AccountTag']['tag_title'],
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => $get_tag_info['AccountTag']['account_tag_id'],
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            } else {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "2",
                    "ref_id" => $ref_id,
                    "tag_title" => $info,
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $info,
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => '',
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }
        if ($this->AccountCommentTag->saveMany($save_cmt_tag)) {
            return true;
        } else {
            return false;
        }
    }

    function update_tag($tags, $comment_id, $ref_id) {

        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $account_id = $users['accounts']['account_id'];
        $tag = str_replace('# ', '', $tags);
        $taginfos = explode(',', $tag);
        foreach ($taginfos as $info) {

            $get_tag_info = '';
            $this->AccountCommentTag->deleteAll(array('comment_id' => $comment_id, "ref_type" => "1"), $cascade = true, $callbacks = true);
            if (!isset($_POST['associate_default_tags'])) {
                $get_tag_info = $this->AccountTag->gettagbytitle($info, $account_id);
            }
            if (!empty($get_tag_info)) {
                $tag_exist = $this->AccountCommentTag->find('count', array('conditions' => array('account_tag_id' => $get_tag_info['AccountTag']['account_tag_id'], 'comment_id' => $comment_id)));
                if ($tag_exist == 0) {
                    $save_cmt_tag[] = array(
                        "comment_id" => $comment_id,
                        "ref_type" => "1",
                        "ref_id" => $ref_id,
                        "tag_title" => $get_tag_info['AccountTag']['tag_title'],
                        "account_tag_id" => $get_tag_info['AccountTag']['account_tag_id'],
                        "created_by" => $user_id,
                        "created_date" => date('y-m-d H:i:s', time())
                    );

                    $user_activity_logs = array(
                        'ref_id' => $comment_id,
                        'desc' => $get_tag_info['AccountTag']['tag_title'],
                        'url' => '/' . $comment_id,
                        'type' => '14',
                        'account_id' => $account_id,
                        'account_folder_id' => $get_tag_info['AccountTag']['account_tag_id'],
                        'environment_type' => 2
                    );
                    $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                }
            } else {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "1",
                    "ref_id" => $ref_id,
                    "tag_title" => $info,
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $info,
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => '',
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }
        if ($this->AccountCommentTag->saveMany($save_cmt_tag)) {
            return true;
        } else {
            return false;
        }
    }

    function deleteaccounttag($accounttagid) {
        $this->AccountTag->deleteAll(array('account_tag_id' => $accounttagid), $cascade = true, $callbacks = true);
        //$this->AccountCommentTag->deleteAll(array('account_comment_tag_id' => $tagId));
        if ($this->AccountTag->getAffectedRows()) {
            echo "sucess";
        } else {
            echo "failed";
        }
        exit;
    }

    function copytoaccounts() {
        if ($this->request->is('post')) {
            $account_ids = $this->data['account_ids'];
            $documents = $this->data['document_id'];
            $huddle_id = $this->data['huddle_id'];
            if (isset($this->request->data['copy_notes']) && $this->request->data['copy_notes'] == 1)
                $copy_notes = 1;
            else
                $copy_notes = 0;
            if ($account_ids) {
                foreach ($account_ids as $account_id) {
                    if ($msg = $this->copyToAccountWorkSpace($account_id, $documents, $copy_notes)) {
                        $this->Session->setFlash($this->language_based_messages['video_copied_successfully'], 'default', array('class' => 'message success'));
                    }
                }
            } else {
                $this->Session->setFlash($this->language_based_messages['please_select_at_least_one_account'], 'default', array('class' => 'message error'));
            }
        } else {
            $this->Session->setFlash($this->language_based_messages['video_is_not_copied_into_selected_account'], 'default', array('class' => 'message error'));
        }

        $this->redirect('/Huddles/view/' . $huddle_id);
        exit;
    }

    function copyToAccountWorkSpace($account_id, $document_ids, $copy_notes = 0) {

        $users = $this->Session->read('user_current_account');
        //$account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $doc_ids = explode(",", $document_ids);

        try {
            foreach ($doc_ids as $doc) {
                $conditions = array('document_id' => $doc);
                $myfilesRow = $this->AccountFolderDocument->get_row($conditions);
                $myfilesRow = $myfilesRow[0];
                $this->AccountFolder->create();
                $document = $this->Document->getVideoDetails($myfilesRow['AccountFolderDocument']['document_id']);

                if (!$document) {
                    continue;
                }
                $data_af = array(
                    'account_id' => $account_id,
                    'folder_type' => 3,
                    'name' => $myfilesRow['AccountFolderDocument']['title'],
                    'desc' => $myfilesRow['AccountFolderDocument']['desc'],
                    'active' => $document['Document']['active'],
                    'created_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'last_edit_by' => $user_id,
                );

                $this->AccountFolder->save($data_af, $validation = TRUE);

                $account_folder_id = $this->AccountFolder->id;

                $this->Document->create();

                $data_doc = array(
                    'account_id' => $account_id,
                    'doc_type' => $document['Document']['doc_type'],
                    'url' => $document['Document']['url'],
                    'original_file_name' => $document['Document']['original_file_name'],
                    'active' => $document['Document']['active'],
                    'view_count' => 0,
                    'created_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'file_size' => $document['Document']['file_size'] ? $document['Document']['file_size'] : '',
                    'last_edit_by' => $user_id,
                    'encoder_provider' => $document['Document']['encoder_provider'],
                    'published' => $document['Document']['published'],
                    'post_rubric_per_video' => '1'
                );


                $this->Document->save($data_doc, $validation = TRUE);

                $video_id = $this->Document->id;

                App::import("Model", "JobQueue");
                $db = new JobQueue();

                if ($document['Document']['published'] == 0) {

                    $requestXml = '<TranscodeVideoJob><document_id>' . $video_id . '</document_id><source_file>' . $document['Document']['url'] . '</source_file></TranscodeVideoJob>';
                    $video_data = array(
                        'JobId' => '2',
                        'CreateDate' => date("Y-m-d H:i:s"),
                        'RequestXml' => $requestXml,
                        'JobQueueStatusId' => 1,
                        'CurrentRetry' => 0
                    );
                    $db->create();
                    $db->save($video_data);
                } else {
                    $documentFiles = $this->DocumentFiles->get_document_row($myfilesRow['AccountFolderDocument']['document_id']);
                    $video_data = array('id' => null, 'document_id' => $video_id,'created_at'=>date('Y-m-d H:i:s'),
                            'debug_logs'=>"Cake::HuddlesController::copyToAccountWorkSpace::line=8915::document_id=$video_id::status=".$documentFiles['DocumentFiles']['transcoding_status']."::created") + $documentFiles['DocumentFiles'];
                    $this->checkDocumentFilesRecordExist($video_id, $video_data, $this->request->data);
                    $this->DocumentFiles->create();
                    $this->DocumentFiles->save($video_data);
                }

                $this->AccountFolderDocument->create();
                $data_afd = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $video_id,
                    'title' => $myfilesRow['AccountFolderDocument']['title'],
                    'zencoder_output_id' => $myfilesRow['AccountFolderDocument']['zencoder_output_id'],
                    'desc' => $myfilesRow['AccountFolderDocument']['desc']
                );
                $this->AccountFolderDocument->save($data_afd, $validation = TRUE);
                if ($copy_notes == 1)
                    @$this->copyAccountComments($account_id, $myfilesRow['AccountFolderDocument']['document_id'], $video_id, $user_id);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return true;
    }

    function copyComments($sourceVideoId, $destinationVideoId, $sourceUserId, $refType = 2) {

        $comments = $this->Comment->find('all', array(
            'conditions' => array(
                'ref_type' => $refType,
                'ref_id' => $sourceVideoId,
                'active' => 1
            ),
            'order' => 'created_date'
        ));
        if (count($comments) == 0)
            return;

        foreach ($comments as $comment) {
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get tags
            $get_standards = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards

            $acc = $this->UserAccount->find('first', array(
                'conditions' => array(
                    'user_id' => $sourceUserId
                )
            ));

            $this->Comment->create();
            $comment_user_id = $comment['Comment']['user_id'];
            if ($refType == 3 || ($refType == 6 && $comment['Comment']['parent_comment_id'] > 0))
                $parentCommentId = $destinationVideoId;
            else
                $parentCommentId = null;
            $comment_data = array(
                'parent_comment_id' => $parentCommentId,
                'ref_type' => $refType,
                'ref_id' => $destinationVideoId,
                'user_id' => $comment_user_id,
                'created_by' => $comment_user_id,
                'last_edit_by' => $comment_user_id,
                    ) + $comment['Comment'];
            unset($comment_data['id']);
            if ($this->Comment->save($comment_data)) {

                $latest_comment_id = $this->Comment->id;

                $replies = $this->Comment->find('all', array(
                    'conditions' => array('site_id' => $this->site_id, 'parent_comment_id' => $comment['Comment']['id'])));

                foreach ($replies as $replies) {
                    $repliesToreplies = $this->Comment->find('all', array(
                        'conditions' => array('site_id' => $this->site_id, 'parent_comment_id' => $replies['Comment']['id'])));

                    $comment_data = array(
                        'parent_comment_id' => $latest_comment_id,
                        'site_id' => $this->site_id,
                        'ref_type' => '3',
                        'ref_id' => $destinationVideoId,
                        'user_id' => $comment_user_id,
                        'created_by' => $comment_user_id,
                        'last_edit_by' => $comment_user_id,
                            ) + $replies['Comment'];
                    unset($comment_data['id']);
                    $this->Comment->create();
                    $this->Comment->save($comment_data);

                    foreach ($repliesToreplies as $replyToreply) {
                        $comment_data = array(
                            'parent_comment_id' => $this->Comment->id,
                            'ref_type' => '3',
                            'site_id' => $this->site_id,
                            'ref_id' => $destinationVideoId,
                            'user_id' => $comment_user_id,
                            'created_by' => $comment_user_id,
                            'last_edit_by' => $comment_user_id,
                                ) + $replyToreply['Comment'];
                        unset($comment_data['id']);
                        $this->Comment->create();
                        $this->Comment->save($comment_data);
                    }
                }








                if (!empty($get_tags)) {
                    foreach ($get_tags as $tag) {
                        $commentTag = $this->AccountTag->find('first', array(
                            'conditions' => array(
                                'account_id' => $acc['UserAccount']['account_id'],
                                'tag_title' => $tag['AccountCommentTag']['tag_title']
                            )
                        ));
                        $this->AccountCommentTag->create();
                        $comment_tag_data = array(
                            'comment_id' => $this->Comment->id,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => 1,
                            'tag_title' => $tag['AccountCommentTag']['tag_title'],
                            'account_tag_id' => $commentTag['AccountTag']['account_tag_id'],
                            'created_by' => $comment_user_id,
                            'last_edit_by' => $comment_user_id,
                        );
                        $this->AccountCommentTag->save($comment_tag_data);
                    }
                }
                if (!empty($get_standards)) {
                    foreach ($get_standards as $tag) {
                        $commentTag = $this->AccountTag->find('first', array(
                            'conditions' => array(
                                'account_id' => $acc['UserAccount']['account_id'],
                                'tag_title' => $tag['AccountCommentTag']['tag_title']
                            )
                        ));
                        $this->AccountCommentTag->create();
                        $comment_tag_data = array(
                            'comment_id' => $this->Comment->id,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => 0,
                            'tag_title' => $tag['AccountCommentTag']['tag_title'],
                            'account_tag_id' => $tag['AccountCommentTag']['account_tag_id'],
                            'created_by' => $comment_user_id,
                            'last_edit_by' => $comment_user_id,
                        );
                        $this->AccountCommentTag->save($comment_tag_data);
                    }
                }
            }

            //$this->copyComments($comment['Comment']['id'], $this->Comment->id, $sourceUserId, 3);
        }
    }

    function copyAccountComments($destAcc, $sourceVideoId, $destinationVideoId, $sourceUserId, $refType = 2) {
        $comments = $this->Comment->find('all', array(
            'conditions' => array(
                'ref_type' => $refType,
                'ref_id' => $sourceVideoId,
                'active' => 1
            ),
            'order' => 'created_date'
        ));
        if (count($comments) == 0)
            return;
        foreach ($comments as $comment) {
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get tags

            $acc = $this->UserAccount->find('first', array(
                'conditions' => array(
                    'user_id' => $sourceUserId
                )
            ));

            $this->Comment->create();
            $comment_user_id = $comment['Comment']['user_id'];
            $comment_data = array(
                'parent_comment_id' => ($refType == 3) ? $destinationVideoId : null,
                'ref_type' => $refType,
                'ref_id' => $destinationVideoId,
                'user_id' => $comment_user_id,
                'created_by' => $comment_user_id,
                'last_edit_by' => $comment_user_id,
                    ) + $comment['Comment'];
            unset($comment_data['id']);
            if ($this->Comment->save($comment_data)) {
                if (!empty($get_tags)) {
                    foreach ($get_tags as $tag) {
                        $commentTag = $this->AccountTag->find('first', array(
                            'conditions' => array(
                                'account_id' => $acc['UserAccount']['account_id'],
                                'tag_title' => $tag['AccountCommentTag']['tag_title']
                            )
                        ));
                        $this->AccountCommentTag->create();
                        $comment_tag_data = array(
                            'comment_id' => $this->Comment->id,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => 1,
                            'tag_title' => $tag['AccountCommentTag']['tag_title'],
                            'account_tag_id' => $commentTag['AccountTag']['account_tag_id'],
                            'created_by' => $comment_user_id,
                            'last_edit_by' => $comment_user_id,
                        );
                        $this->AccountCommentTag->save($comment_tag_data);
                    }
                }
            }

            $this->copyAccountComments($destAcc, $comment['Comment']['id'], $this->Comment->id, $sourceUserId, 3);
        }
    }

    public function treeview_detail($id = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $folders = array();
        $folders = $this->AccountFolder->find("all", array("conditions" => array(
                'account_id' => $account_id, 'active' => '1', 'folder_type' => '5'
        )));
        $afolder = array();
        if ($folders) {
            $ids_arr = array();
            foreach ($folders as $folder) {
                //if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {
                array_push($ids_arr, $folder['AccountFolder']['account_folder_id']);
                //}
            }
            $accessible_folders = array();
            foreach ($folders as $folder) {

                //if ($this->folder_has_huddle_participating($folder['AccountFolder']['account_folder_id']) || $folder['AccountFolder']['created_by'] == $user_id) {
                if (in_array($folder['AccountFolder']['parent_folder_id'], $ids_arr)) {
                    $afolder['AccountFolder']['id'] = $folder['AccountFolder']['account_folder_id'];
                    $afolder['AccountFolder']['text'] = $folder['AccountFolder']['name'];
                    $afolder['AccountFolder']['parent'] = $folder['AccountFolder']['parent_folder_id'];
                    $accessible_folders[] = $afolder;
                } else {
                    $afolder['AccountFolder']['id'] = $folder['AccountFolder']['account_folder_id'];
                    $afolder['AccountFolder']['text'] = $folder['AccountFolder']['name'];
                    $afolder['AccountFolder']['parent'] = '#';
                    $accessible_folders[] = $afolder;
                }
                //}
            }

            $folders = $accessible_folders;
        }

        $data = Set::extract('/AccountFolder/.', $folders);
        foreach ($data as $key => &$item) {

            if ($item['parent'] == null || empty($item['parent']))
                $item['parent'] = '#';
        }
        if ($id != '') {

            $folder_childs_ids = array();

            /* $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
              'parent_folder_id' => $id, 'active' => '1', 'folder_type' => '5'
              )));


              if (!empty($folder_childs)) {
              foreach ($folder_childs as $folder_child) {
              $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];

              $folder_childs_ids = array_merge($folder_childs_ids, $this->child_finder($folder_child['AccountFolder']['account_folder_id'], $folder_childs_ids));
              }
              }


              //print_r(array_unique($folder_childs_ids));die();

              $folder_childs_ids = array_unique($folder_childs_ids); */


            $new_data = array();
            // $array = array('id'=>0,'text'=>'Huddles','parent'=>'#');
            //  $new_data[] = $array;

            foreach ($data as $dat) {
                if ($dat['id'] != $id && !in_array($dat['id'], $folder_childs_ids)) {
                    $new_data[] = $dat;
                }
            }

            echo json_encode($new_data);
            die;
        } else {
            echo json_encode($data);
            die;
        }
    }

    function child_finder($id, $folder_childs_ids) {

        $folder_childs = $this->AccountFolder->find("all", array("conditions" => array(
                'parent_folder_id' => $id, 'active' => '1', 'folder_type' => '5'
        )));


        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['AccountFolder']['account_folder_id'];

                $folder_childs_ids = array_merge($folder_childs_ids, $this->child_finder($folder_child['AccountFolder']['account_folder_id'], $folder_childs_ids));
            }
        }

        return $folder_childs_ids;
    }

    function time_to_decimal($time) {
        $timeArr = explode(':', $time);
        $decTime = ($timeArr[0] * 60) + ($timeArr[1]) + ($timeArr[2] / 60);
        return $decTime * 60;
    }

    function add_call_notes() {
        
        if ($this->request->is('post')) {
            $document_id = $this->request->data['video_id'] != '' ? $this->request->data['video_id'] : '';
            //echo $document_id;
            if ($document_id != '') {
                $this->addObComments($document_id);
            } else {
                $this->addObDocuments();
            }
        } else {
            die('invalid request!');
        }
    }

    function add_video_comments($audio_comment = 0) {
        if ($this->request->is('post')) {
            $document_id = $this->request->data['video_id'];
            $audio_comment_path = $this->request->data['audio_path'];
            $this->addVdoComments($document_id, $audio_comment, $audio_comment_path);
        } else {
            die('invalid request!');
        }
    }

    function add_live_video_comments($audio_comment = 0) {
        if ($this->request->is('post')) {
            $document_id = $this->request->data['video_id'];
            $audio_comment_path = $this->request->data['audio_path'];
            $this->addLiveVdoComments($document_id, $audio_comment, $audio_comment_path);
        } else {
            die('invalid request!');
        }
    }

    function addVdoComments($video_id, $audio_comment, $audio_comment_path = '') {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];

        /* $account_id = $this->request->data['account_id'];
          $user_id = $this->request->data['user_id']; */
        $observations_comments = $this->request->data['observations_comments'];
        $observation_title = $this->request->data['observation_title'];
        $observations_standards = $this->request->data['observations_standards'];
        $observations_tags = $this->request->data['observations_tags'];
        $observations_time = $this->request->data['observations_time'];
        $huddle_id = $this->request->data['huddle_id'];
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $time = '0';
        if (!empty($observations_time)) {
            $time = $this->time_to_decimal($observations_time);
        }
        if (empty($video_id)) {
            die('miss videoId');
        }
        $account_folder_document = $this->AccountFolderDocument->get($video_id);
        if (empty($account_folder_document))
            die('invalid videoId');

        $account_folder_id = $account_folder_document['AccountFolderDocument']['account_folder_id'];
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle = $this->AccountFolder->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'account_id' => $account_id
            )
        ));
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_permissions = $this->Session->read('user_permissions');
        $isCreator = ($users['User']['id'] == $huddle['AccountFolder']['created_by']);

        $data = array();
        if (!$audio_comment) {
            $data = array(
                'comment' => $observations_comments,
                'time' => $time,
                'access_level' => '',
                'ref_id' => $video_id,
                'ref_type' => '2',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        } else {
            $data = array(
                'comment' => $audio_comment_path,
                'time' => $time,
                'access_level' => '',
                'ref_id' => $video_id,
                'ref_type' => '6',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        }
        $view = new View($this, false);
        if ($htype == 3 || ($htype == 2 && $view->Custom->is_enabled_coach_feedback($huddle_id) && $view->Custom->check_if_evalutor($account_folder_id, $user_id))) {   //coaching huddle feedback
            $is_feedback_published = false;
            $video_comments_list = $this->Comment->getVideoComments($video_id, '', '', '', '', 1, 0, true);
            if (!empty($video_comments_list)):
                foreach ($video_comments_list as $single_video_comment):
                    if ($single_video_comment['Comment']['active'] == '1') {
                        $is_feedback_published = true;
                    }
                endforeach;
            endif;
            if ($is_feedback_published) {
                $data['active'] = '1';
            } else {
                $data['active'] = '0';
            }
        }

        if ($this->Comment->save($data)) {
            $comment_id = $this->Comment->id;
            $user_activity_logs = array(
                'ref_id' => $this->Comment->id,
                'desc' => $observations_comments,
                'url' => $this->base . '/huddles/view/' . $account_folder_id . '/1/' . $video_id,
                'account_folder_id' => $account_folder_id,
                'type' => '5',
                'environment_type' => 2
            );

            $standards = isset($observations_standards) ? $observations_standards : '';
            if (!empty($standards)) {
                $loggedInUser = $this->Session->read('user_current_account');
                $view = new View($this, false);
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                if (IS_QA):
                /* $client = new IntercomClient('dG9rOjg4MjgyOGJmX2Q2MWRfNDM3OF9hNWNjXzQzODU2ZjliMDZlYjoxOjA=', null);
                  $meta_data = array(
                  'framework_added_in_comment' => 'true',
                  'user_role' => $user_role
                  );
                  $client->events->create([
                  "event_name" => "framework-added-in-comment",
                  "created_at" => time(),
                  "email" => $loggedInUser['User']['email'],
                  "metadata" => $meta_data
                  ]); */
                endif;
                $this->save_standard_tag($standards, $this->Comment->id, $video_id);
            }
            $tags = isset($observations_tags) ? $observations_tags : '';
            if (!empty($tags)) {
                $loggedInUser = $this->Session->read('user_current_account');
                $view = new View($this, false);
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                if (IS_QA):
                /* $client = new IntercomClient('dG9rOjg4MjgyOGJmX2Q2MWRfNDM3OF9hNWNjXzQzODU2ZjliMDZlYjoxOjA=', null);
                  $meta_data = array(
                  'custom_marker_tags_added' => 'true',
                  'user_role' => $user_role
                  );
                  $client->events->create([
                  "event_name" => "custom-marker-tags-added",
                  "created_at" => time(),
                  "email" => $loggedInUser['User']['email'],
                  "metadata" => $meta_data
                  ]); */
                endif;
                $this->save_tag($tags, $this->Comment->id, $video_id);
            }

            $tags_1 = isset($this->data['assessment_value']) ? $this->data['assessment_value'] : '';
            if (!empty($tags_1)) {
                $this->save_tag_1($tags_1, $this->Comment->id, $video_id);
            }

            if ($account_folder_id != '') {

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
                $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id);

                $huddle_user_ids = array();
                if ($userGroups && count($userGroups) > 0) {
                    foreach ($userGroups as $row) {
                        $huddle_user_ids[] = $row['user_groups']['user_id'];
                    }
                }
                if ($huddleUsers && count($huddleUsers) > 0) {
                    foreach ($huddleUsers as $row) {
                        $huddle_user_ids[] = $row['huddle_users']['user_id'];
                    }
                }

                if (count($huddle_user_ids) > 0) {
                    $user_ids = implode(',', $huddle_user_ids);
                    if (!empty($user_ids))
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                } else {
                    $huddleUserInfo = array();
                }


                if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                    $commentsData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'comment' => $this->data['observations_comments'],
                        'video_link' => '/huddles/view/' . $account_folder_id . '/1/' . $this->data['video_id'],
                        'participating_users' => $huddleUserInfo
                    );

                    foreach ($huddleUserInfo as $row) {
                        if ($user_id == $row['User']['id']) {
                            continue;
                        }
                        $commentsData ['email'] = $row['User']['email'];
                        if ($htype != 3 && (!$view->Custom->is_enabled_coach_feedback($huddle_id) || $is_feedback_published)) {
                            if ($this->check_subscription($row['User']['id'], '6', $account_id)) {
                                $commentsData['comment_id'] = $this->Comment->id;
                                $this->sendVideoComments($commentsData, $row['User']['id']);
                            }
                        }
                    }
                }
            }
            $this->user_activity_logs($user_activity_logs);

            $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
            $h_type = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
            $view = new View($this, false);
            if ((!$view->Custom->check_if_evalutor($account_folder_id, $user_id)) && $h_type == '2' && $view->Custom->is_enabled_coach_feedback($account_folder_id)) {
                $comment_result_count = $this->Comment->find('all', array(
                    'conditions' => array(
                        'ref_id' => $video_id,
                        'active' => 1
                    )
                ));
            } else {
                $comment_result_count = $this->Comment->find('all', array(
                    'conditions' => array(
                        'ref_id' => $video_id,
                    )
                ));
            }

            if (isset($this->request->data['comments_optimized']) && $this->request->data['comments_optimized'] == 1) {
                $result = array(
                    'document_id' => $video_id,
                    'status' => 'success',
                    'total_comments' => count($comment_result_count),
                    'internal_comment_id' => $this->request->data['internal_comment_id'],
                    'comments' => $this->getVdoComment($video_id, $comment_id)
                );
            } else {

                $result = array(
                    'document_id' => $video_id,
                    'status' => 'success',
                    'comments' => $this->getObComments($video_id)
                );
            }

            echo json_encode($result);

            exit;
        }
    }

    function addLiveVdoComments($video_id, $audio_comment, $audio_comment_path = '') {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $document_details = $this->Document->get_document_row($this->data['video_id']);

        /* $account_id = $this->request->data['account_id'];
          $user_id = $this->request->data['user_id']; */
        $observations_comments = $this->request->data['observations_comments'];
        $observation_title = $this->request->data['observation_title'];
        $observations_standards = $this->request->data['observations_standards'];
        $observations_tags = $this->request->data['observations_tags'];
        $observations_time = $this->request->data['observations_time'];
        $huddle_id = $this->request->data['huddle_id'];
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $time = '0';
        if (!empty($document_details['Document']['current_duration'])) {
            $time = $document_details['Document']['current_duration'];
            //$time = $this->time_to_decimal($observations_time);
        }
        if (empty($video_id)) {
            die('miss videoId');
        }
        $account_folder_document = $this->AccountFolderDocument->get($video_id);
        if (empty($account_folder_document))
            die('invalid videoId');

        $account_folder_id = $account_folder_document['AccountFolderDocument']['account_folder_id'];
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle = $this->AccountFolder->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'account_id' => $account_id
            )
        ));
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_permissions = $this->Session->read('user_permissions');
        $isCreator = ($users['User']['id'] == $huddle['AccountFolder']['created_by']);

        $data = array();
        if (!$audio_comment) {
            $data = array(
                'comment' => $observations_comments,
                'time' => $time,
                'access_level' => '',
                'ref_id' => $video_id,
                'ref_type' => '2',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        } else {
            $data = array(
                'comment' => $audio_comment_path,
                'time' => $time,
                'access_level' => '',
                'ref_id' => $video_id,
                'ref_type' => '6',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );
        }
        $view = new View($this, false);
        if ($htype == 3 || ($htype == 2 && $view->Custom->is_enabled_coach_feedback($huddle_id) && $view->Custom->check_if_evalutor($account_folder_id, $user_id))) {   //coaching huddle feedback
            $is_feedback_published = false;
            $video_comments_list = $this->Comment->getVideoComments($video_id, '', '', '', '', 1, 0, true);
            if (!empty($video_comments_list)):
                foreach ($video_comments_list as $single_video_comment):
                    if ($single_video_comment['Comment']['active'] == '1') {
                        $is_feedback_published = true;
                    }
                endforeach;
            endif;
            if ($is_feedback_published) {
                $data['active'] = '1';
            } else {
                $data['active'] = '0';
            }
        }

        if ($this->Comment->save($data)) {
            $comment_id = $this->Comment->id;
            $user_activity_logs = array(
                'ref_id' => $this->Comment->id,
                'desc' => $observations_comments,
                'url' => $this->base . '/huddles/view/' . $account_folder_id . '/1/' . $video_id,
                'account_folder_id' => $account_folder_id,
                'type' => '5',
                'environment_type' => 2
            );


            $token_ids = $this->get_device_token_ids($account_folder_id, $user_id);
            $user_data = $this->User->find('first', array(
                'conditions' => array(
                    'id' => $user_id
                )
            ));
            foreach ($token_ids as $token_id) {
                $title = 'Video Comment is added in Live Video';
                $desc = 'Video Comment is added in Live Video';
                $title = '';
                $desc = '';
                $data_array = array(
                    'video_id' => $video_id,
                    'huddle_id' => $account_folder_id,
                    'notification_type' => '5',
                    'comment' => $observations_comments,
                    'user' => $user_data['User']
                );
                $this->push_notification_test($token_id['apns_token'], $title, $desc, $data_array, $token_id['device_type']);
            }

            $standards = isset($observations_standards) ? $observations_standards : '';
            if (!empty($standards)) {
                $loggedInUser = $this->Session->read('user_current_account');
                $view = new View($this, false);
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                if (IS_QA):
                /* $client = new IntercomClient('dG9rOjg4MjgyOGJmX2Q2MWRfNDM3OF9hNWNjXzQzODU2ZjliMDZlYjoxOjA=', null);
                  $meta_data = array(
                  'framework_added_in_comment' => 'true',
                  'user_role' => $user_role
                  );
                  $client->events->create([
                  "event_name" => "framework-added-in-comment",
                  "created_at" => time(),
                  "email" => $loggedInUser['User']['email'],
                  "metadata" => $meta_data
                  ]); */
                endif;
                $this->save_standard_tag($standards, $this->Comment->id, $video_id);
            }
            $tags = isset($observations_tags) ? $observations_tags : '';
            if (!empty($tags)) {
                $loggedInUser = $this->Session->read('user_current_account');
                $view = new View($this, false);
                $user_role = $view->Custom->get_user_role_name($loggedInUser['users_accounts']['role_id']);
                if (IS_QA):
                /* $client = new IntercomClient('dG9rOjg4MjgyOGJmX2Q2MWRfNDM3OF9hNWNjXzQzODU2ZjliMDZlYjoxOjA=', null);
                  $meta_data = array(
                  'custom_marker_tags_added' => 'true',
                  'user_role' => $user_role
                  );
                  $client->events->create([
                  "event_name" => "custom-marker-tags-added",
                  "created_at" => time(),
                  "email" => $loggedInUser['User']['email'],
                  "metadata" => $meta_data
                  ]); */
                endif;
                $this->save_tag($tags, $this->Comment->id, $video_id);
            }

            $tags_1 = isset($this->data['assessment_value']) ? $this->data['assessment_value'] : '';
            if (!empty($tags_1)) {
                $this->save_tag_1($tags_1, $this->Comment->id, $video_id);
            }

            if ($account_folder_id != '') {

                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
                $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id);

                $huddle_user_ids = array();
                if ($userGroups && count($userGroups) > 0) {
                    foreach ($userGroups as $row) {
                        $huddle_user_ids[] = $row['user_groups']['user_id'];
                    }
                }
                if ($huddleUsers && count($huddleUsers) > 0) {
                    foreach ($huddleUsers as $row) {
                        $huddle_user_ids[] = $row['huddle_users']['user_id'];
                    }
                }

                if (count($huddle_user_ids) > 0) {
                    $user_ids = implode(',', $huddle_user_ids);
                    if (!empty($user_ids))
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
                } else {
                    $huddleUserInfo = array();
                }


                if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                    $commentsData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['AccountFolder']['name'],
                        'comment' => $this->data['observations_comments'],
                        'video_link' => '/huddles/view/' . $account_folder_id . '/1/' . $this->data['video_id'],
                        'participating_users' => $huddleUserInfo
                    );

                    foreach ($huddleUserInfo as $row) {
                        if ($user_id == $row['User']['id']) {
                            continue;
                        }
                        $commentsData ['email'] = $row['User']['email'];
                        if ($htype != 3 && (!$view->Custom->is_enabled_coach_feedback($huddle_id) || $is_feedback_published)) {
                            if ($this->check_subscription($row['User']['id'], '6', $account_id)) {
                                $commentsData['comment_id'] = $this->Comment->id;
                                $this->sendVideoComments($commentsData, $row['User']['id']);
                            }
                        }
                    }
                }
            }
            $this->user_activity_logs($user_activity_logs);

            $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
            $h_type = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
            $view = new View($this, false);
            if ((!$view->Custom->check_if_evalutor($account_folder_id, $user_id)) && $h_type == '2' && $view->Custom->is_enabled_coach_feedback($account_folder_id)) {
                $comment_result_count = $this->Comment->find('all', array(
                    'conditions' => array(
                        'ref_id' => $video_id,
                        'active' => 1
                    )
                ));
            } else {
                $comment_result_count = $this->Comment->find('all', array(
                    'conditions' => array(
                        'ref_id' => $video_id,
                    )
                ));
            }

            if (isset($this->request->data['comments_optimized']) && $this->request->data['comments_optimized'] == 1) {
                $result = array(
                    'document_id' => $video_id,
                    'status' => 'success',
                    'total_comments' => count($comment_result_count),
                    'internal_comment_id' => $this->request->data['internal_comment_id'],
                    'comments' => $this->getVdoComment($video_id, $comment_id)
                );
            } else {

                $result = array(
                    'document_id' => $video_id,
                    'status' => 'success',
                    'comments' => $this->getObComments($video_id)
                );
            }

            echo json_encode($result);

            exit;
        }
    }

    function getVdoComment($video_id, $comment_id) {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $internal_comment_id = $this->request->data['internal_comment_id'];

        $account_id = isset($this->request->data['account_id']) ? $this->request->data['account_id'] : $account_id;
        $huddle_id = isset($this->request->data['huddle_id']) ? $this->request->data['huddle_id'] : $huddle_id;
        $user_id = isset($this->request->data['user_id']) ? $this->request->data['user_id'] : $user_id;
        $result = $this->Comment->getVideoComment($video_id, $comment_id);
        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";
        $standardsL2 = array();
        $standards = array();
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }


        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array(1, 2)); //get tags
                $get_tags_noratings = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], 1);
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags), array("default_tags_noratings" => $get_tags_noratings));
            }
            $videoComments = $comments;

            $params = array(
                'vidComments' => $videoComments,
                'huddle' => $huddle,
                'huddle_id' => $huddle_id,
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
                'standards' => $standards,
                'standardsL2' => $standardsL2,
                'video_id' => $video_id,
                'user_id' => $user_id,
                'internal_comment_id' => $internal_comment_id,
                'type' => 'get_video_comments_obs',
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
                'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                        "account_id" => $account_id,
                        "tag_type" => '1'
            )))
            );
            $view = new View($this, false);
            $html = $view->element('ajax/vdoComment', $params);
            return $html;
        }
    }

    function addObDocuments( ) {

        $view = new View($this,true);
        $translation= $view->Custom->get_page_lang_based_content('Api/workspace');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $observations_comments = isset($this->request->data['observations_comments']) ? $this->request->data['observations_comments'] : '';
        $observation_title = $translation['workspace_scriptednotes'].'_' . date('m-d-Y');
               
        $observations_standards = isset($this->request->data['observations_standards']) ? $this->request->data['observations_standards'] : '';
        $observations_tags = isset($this->request->data['observations_tags']) ? $this->request->data['observations_tags'] : '';
        $observations_time = isset($this->request->data['observations_time']) ? $this->request->data['observations_time'] : '';
        $parent_folder_id = isset($this->request->data['parent_folder_id']) ? $this->request->data['parent_folder_id'] : 0;
        //$huddle_id = isset($this->request->data['huddle_id']) ? $this->request->data['huddle_id'] : '';//As we have moved scripted notes to workspace

        $this->AccountFolder->create();
        $data = array(
            'name' => $observation_title,
            'desc' => $observation_title,
            'created_date' => date("Y-m-d H:i:s"),
            'last_edit_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_by' => $user_id,
            'folder_type' => 3,
            'active' => 1,
            'account_id' => $account_id,
        );

        $this->AccountFolder->save($data, $validation = true);
        $huddle_id = $this->AccountFolder->id;
        $time = '0';
        if (!empty($observations_time)) {
            $time = $this->time_to_decimal($observations_time);
        }
        $this->Document->create();
        $data = array(
            'account_id' => $account_id,
            'doc_type' => '3',
            'url' => str_replace(' ', '_', $observation_title) . '.mp4',
            'original_file_name' => str_replace(' ', '_', $observation_title) . '.mp4',
            'active' => 1,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'recorded_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
            'published' => '0',
            'post_rubric_per_video' => '1',
            'parent_folder_id' => $parent_folder_id,
            'is_scripted_note_start' => '0'
        );
        if ($this->Document->save($data)) {
            $this->create_churnzero_event('Scripted+Observation', $account_id , $user['User']['email']);
            $video_id = $this->Document->id;
            $this->Session->write('ob_video_id', $video_id);
            
            $data = array(
                'account_folder_id' => $huddle_id,
                'meta_data_name' => 'folder_type',
                'meta_data_value' => '5',
                'created_by' => $user_id,
                'created_date' => date('Y-m-d H:i:s'),
            );

            $this->AccountFolderMetaData->save($data, $validation = true);
            $video_data = array(
                'document_id' => $video_id,
                'url' => "",
                'resolution' => 1080,
                'duration' => 0,
                'file_size' => 0,
                'default_web' => 1,
                'transcoding_job_id' => '1',
                'transcoding_status' => '0',
                'created_at'=>date('Y-m-d H:i:s'),
                'debug_logs'=>"Cake::HuddlesController::addObDocuments::line=9983::document_id=$video_id::status=0::created"
            );
            $this->checkDocumentFilesRecordExist($video_id, $video_data, $this->request->data);
            $this->DocumentFiles->create();
            $this->DocumentFiles->save($video_data, $validation = TRUE);
            $this->AccountFolderDocument->create();
            if ($this->AccountFolderDocument->save(array(
                        'account_folder_id' => $huddle_id,
                        'document_id' => $video_id,
                        'title' => $observation_title,
                        'zencoder_output_id' => 0,
                        'desc' => 'observation call notes'
                            ), $validation = TRUE)) {
                if (empty($video_id)) {
                    die('miss videoId');
                }
                $account_folder_document = $this->AccountFolderDocument->get($video_id);
                if (empty($account_folder_document))
                    die('invalid videoId');

                $account_folder_id = $account_folder_document['AccountFolderDocument']['account_folder_id'];
                $users = $this->Session->read('user_current_account');
                $account_id = $users['accounts']['account_id'];
                $user_id = $users['User']['id'];
                $huddle = $this->AccountFolder->find('first', array(
                    'conditions' => array(
                        'account_folder_id' => $account_folder_id,
                        'account_id' => $account_id
                    )
                ));
                $huddle_permission = $this->Session->read('user_huddle_level_permissions');
                $user_permissions = $this->Session->read('user_permissions');

                if ($observations_comments != '') {
                    $data = array(
                        'comment' => $observations_comments,
                        'time' => $time,
                        'access_level' => '',
                        'ref_id' => $video_id,
                        'ref_type' => '2',
                        'created_date' => date('y-m-d H:i:s', time()),
                        'created_by' => $user_id,
                        'last_edit_date' => date('y-m-d H:i:s', time()),
                        'last_edit_by' => $user_id,
                        'user_id' => $user_id
                    );
                    if ($this->Comment->save($data)) {
                        $user_activity_logs = array(
                            'ref_id' => $this->Comment->id,
                            'desc' => $observations_comments,
                            'url' => $this->base . '/huddles/view/' . $account_folder_id . '/1/' . $video_id,
                            'account_folder_id' => $account_folder_id,
                            'type' => '5'
                        );

                        $standards = isset($observations_standards) ? $observations_standards : '';
                        if (!empty($standards)) {
                            $this->save_standard_tag($standards, $this->Comment->id, $video_id);
                        }
                        $tags = isset($observations_tags) ? $observations_tags : '';
                        if (!empty($tags)) {
                            $this->save_tag($tags, $this->Comment->id, $video_id);
                        }
                        $this->user_activity_logs($user_activity_logs);

                        $result = array(
                            'document_id' => $video_id,
                            'status' => 'success',
                            'comments' => $this->getObComments($video_id)
                        );

                        echo json_encode($result);
                        exit;
                    }
                } else {
                    $result = array(
                        'document_id' => $video_id,
                        'status' => 'success',
                        'huddle_id' => $huddle_id,
                        'comments' => $this->getObComments($video_id)
                    );
                    echo json_encode($result);
                    exit;
                }
            } else {
                $result = array(
                    'document_id' => '',
                    'status' => 'failed'
                );
                echo json_encode($result);
                exit;
            }
        }
    }

    function addObComments($video_id) {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        /* $account_id = $this->request->data['account_id'];
          $user_id = $this->request->data['user_id']; */
        $observations_comments = $this->request->data['observations_comments'];
        $observation_title = $this->request->data['observation_title'];
        $observations_standards = $this->request->data['observations_standards'];
        $observations_tags = $this->request->data['observations_tags'];
        $observations_time = $this->request->data['observations_time'];
        $huddle_id = $this->request->data['huddle_id'];
        $time = '0';
        if (!empty($observations_time)) {
            $time = $this->time_to_decimal($observations_time);
        }
        if (empty($video_id)) {
            die('miss videoId');
        }
        $account_folder_document = $this->AccountFolderDocument->get($video_id);
        if (empty($account_folder_document))
            die('invalid videoId');

        $account_folder_id = $account_folder_document['AccountFolderDocument']['account_folder_id'];
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle = $this->AccountFolder->find('first', array(
            'conditions' => array(
                'account_folder_id' => $account_folder_id,
                'account_id' => $account_id
            )
        ));
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_permissions = $this->Session->read('user_permissions');
        $isCreator = ($users['User']['id'] == $huddle['AccountFolder']['created_by']);

        $data = array();
        $data = array(
            'comment' => $observations_comments,
            'time' => $time,
            'access_level' => '',
            'ref_id' => $video_id,
            'ref_type' => '2',
            'created_date' => date('y-m-d h:i:s', time()),
            'created_by' => $user_id,
            'last_edit_date' => date('y-m-d h:i:s', time()),
            'last_edit_by' => $user_id,
            'user_id' => $user_id
        );
        if ($this->Comment->save($data)) {
            $comment_id = $this->Comment->id;
            $user_activity_logs = array(
                'ref_id' => $this->Comment->id,
                'desc' => $observations_comments,
                'url' => $this->base . '/huddles/view/' . $account_folder_id . '/1/' . $video_id,
                'account_folder_id' => $account_folder_id,
                'type' => '5'
            );

            $standards = isset($observations_standards) ? $observations_standards : '';
            if (!empty($standards)) {
                $this->save_standard_tag($standards, $this->Comment->id, $video_id);
            }
            $tags = isset($observations_tags) ? $observations_tags : '';
            if (!empty($tags)) {
                $this->save_tag($tags, $this->Comment->id, $video_id);
            }

            $tags_1 = isset($this->data['assessment_value']) ? $this->data['assessment_value'] : '';
            if (!empty($tags_1)) {
                $this->save_tag_1($tags_1, $this->Comment->id, $video_id);
            }


            //   $this->user_activity_logs($user_activity_logs);

            if (isset($this->request->data['comments_optimized']) && $this->request->data['comments_optimized'] == 1) {

                $result = array(
                    'document_id' => $video_id,
                    'status' => 'success',
                    'total_comments' => $this->Comment->find('count', array('conditions' => array('ref_id' => $video_id))),
                    'internal_comment_id' => $this->request->data['internal_comment_id'],
                    'comments' => $this->getObComment($video_id, $comment_id)
                );
            } else {

                $result = array(
                    'document_id' => $video_id,
                    'status' => 'success',
                    'comments' => $this->getObComments($video_id)
                );
            }

            echo json_encode($result);

            exit;
        }
    }

    function getObComment($video_id, $comment_id) {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $internal_comment_id = $this->request->data['internal_comment_id'];

        $account_id = isset($this->request->data['account_id']) ? $this->request->data['account_id'] : $account_id;
        $huddle_id = isset($this->request->data['huddle_id']) ? $this->request->data['huddle_id'] : $huddle_id;
        $user_id = isset($this->request->data['user_id']) ? $this->request->data['user_id'] : $user_id;
        $result = $this->Comment->getVideoComment($video_id, $comment_id);
        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";
        $standardsL2 = array();
        $standards = array();
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }


        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array(1, 2)); //get tags
                $get_tags_noratings = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], 1);
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags), array("default_tags_noratings" => $get_tags_noratings));
            }
            $videoComments = $comments;

            $document1 = $this->Document->getVideoDetails($video_id, 3);
            $params = array(
                'vidComments' => $videoComments,
                'huddle' => $huddle,
                'huddle_id' => $huddle_id,
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
                'standards' => $standards,
                'standardsL2' => $standardsL2,
                'video_id' => $video_id,
                'user_id' => $user_id,
                'internal_comment_id' => $internal_comment_id,
                'type' => 'get_video_comments_obs',
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
                'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                        "account_id" => $account_id,
                        "tag_type" => '1'
            ))),
                'videos' => $document1,
            );
            $view = new View($this, false);
            $html = $view->element('ajax/obComment', $params);
            return $html;
        }
    }

    function getObComments($video_id, $huddle_id = '') {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];

        $account_id = isset($this->request->data['account_id']) ? $this->request->data['account_id'] : $account_id;
        $huddle_id = isset($this->request->data['huddle_id']) ? $this->request->data['huddle_id'] : $huddle_id;
        $user_id = isset($this->request->data['user_id']) ? $this->request->data['user_id'] : $user_id;
        $result = $this->Comment->getVideoComments($video_id, '', '', '', '', 1);
        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";
        $standardsL2 = array();
        $standards = array();
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }


        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array(1, 2)); //get tags
                $get_tags_noratings = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], 1);
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags), array("default_tags_noratings" => $get_tags_noratings));
            }
            $videoComments = $comments;

            $document1 = $this->Document->getVideoDetails($video_id, 3);
//            $this->set('videos', $document1);
            $standards_new = $this->get_framework_settings($framework_id);
            $params = array(
                'vidComments' => $videoComments,
                'huddle' => $huddle,
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
                'standards' => $standards,
                'standardsL2' => $standardsL2,
                'framework_data' => $standards_new,
                'video_id' => $video_id,
                'user_id' => $user_id,
                'type' => 'get_video_comments_obs',
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
                'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                        "account_id" => $account_id,
                        "tag_type" => '1'
            ))),
                'videos' => $document1,
            );
            $view = new View($this, false);
            $html = $view->element('ajax/obComments', $params);
            return $html;
        }
    }

    function getObComments_modified($video_id, $huddle_id = '') {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];

        $account_id = isset($this->request->data['account_id']) ? $this->request->data['account_id'] : $account_id;
        $huddle_id = isset($this->request->data['huddle_id']) ? $this->request->data['huddle_id'] : $huddle_id;
        $user_id = isset($this->request->data['user_id']) ? $this->request->data['user_id'] : $user_id;
        $result = $this->Comment->getVideoComments($video_id, '', '', '', '', 1);
        $video_name = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $video_id)));

        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";
        $standardsL2 = array();
        $standards = array();
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }


        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array(1, 2)); //get tags
                $get_tags_noratings = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], 1);
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags), array("default_tags_noratings" => $get_tags_noratings));
            }
            $videoComments = $comments;
            $document1 = $this->Document->getVideoDetails($video_id, 3);
            $params = array(
                'vidComments' => $videoComments,
                'huddle' => $huddle,
                'video_name' => $video_name,
                'huddle_id' => $huddle_id,
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
                'standards' => $standards,
                'standardsL2' => $standardsL2,
                'video_id' => $video_id,
                'user_id' => $user_id,
                'type' => 'get_video_comments_obs',
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
                'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                        "account_id" => $account_id,
                        "tag_type" => '1'
            ))),
                'videos' => $document1
            );
            $view = new View($this, false);
            $html = $view->element('ajax/obComments_modified', $params);
            return $html;
        } else {

            return '<ul class="comments comments_huddles" id="normal_comment" style="overflow-x: hidden;margin-top: 34px;text-align: center;"><li>No Observation notes added.</li></ul>';
        }
    }

    function get_huddle_document($huddle_id = '', $retrun_type = false, $video_id = '') {
        $document = $this->Document->getVideoDetails($video_id, 3);
        $processed_check = $document['Document']['is_processed'];
        $huddle_id = isset($this->request->data['huddle_id']) ? $this->request->data['huddle_id'] : $huddle_id;
        $is_stop = isset($this->request->data['is_stop']) ? $this->request->data['is_stop'] : false;
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $item = $this->Document->getVideos($huddle_id);
        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }
//        // Load Workspace
        $work_space_item = $this->Document->getMyFilesVideos($account_id, $user_id, "", "Document.created_date DESC", 20, 0, 1);
        if (count($work_space_item) > 0) {
            for ($j = 0; $j < count($work_space_item); $j++) {
                $work_space_item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $work_space_item[$j]['Document']['id'])));
            }
        }
        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";


        $standardsL2 = array();
        $standards = array();
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }
        $comments_html = '';
        //if ($is_stop) {
        $comments_html = $this->getObComments_modified($video_id, $huddle_id);
        //  }


        $params = array(
            'files' => $work_space_item,
            'huddle_videos' => $item,
            'huddle_id' => $huddle_id,
            'account_folder_id' => $huddle_id,
            'account_id' => $account_id,
            'standards' => $standards,
            'standardsL2' => $standardsL2,
            'comments_html' => $comments_html,
            'processed_check' => $processed_check,
            'video_id' => $video_id
        );

        $view = new View($this, false);
        $html = $view->element('huddles/ajax/ajax_ob_associate', $params);
        if ($huddle_id != '' && $retrun_type) {
            return $html;
        }
        echo json_encode(array('model' => $html));
        die;
    }

    function add_observation_video() {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $video_id = $this->request->data['video_id'];
        $load_video_id = $this->request->data['loaded_video_id'];
        $account_folder_id = $this->request->data['huddle_id'];
        //$account_id = $this->request->data['account_id'];
        $account_folder_document = $this->AccountFolderDocument->get($load_video_id);
        $document = $this->Document->getVideoDetails($load_video_id);
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $document_data_files = $this->DocumentFiles->find("first", array("conditions" => array(
                "document_id" => $document['Document']['id'],
                "default_web" => true
        )));
        $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL"
        )));
        $standardsL2 = array();
        if ($standards_l2 > 0) {
            $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL"
            )));
            $standards = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL"
            )));
        } else {
            $standards = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id
            )));
        }

        if ($document) {
            $data = array(
                'url' => "'" . $document['Document']['url'] . "'",
                'original_file_name' => "'" . $document['Document']['original_file_name'] . "'",
                'file_size' => "'" . $document['Document']['file_size'] . "'",
                'zencoder_output_id' => "'" . $document['Document']['zencoder_output_id'] . "'",
                'encoder_provider' => "'" . $document['Document']['encoder_provider'] . "'",
                'thumbnail_number' => "'" . $document['Document']['thumbnail_number'] . "'",
                'active' => 1,
                'published' => 0,
                'is_associated' => 1,
            );
            $this->Document->updateAll($data, array('id' => $video_id));
            $this->DocumentFiles->updateAll(array(
                'document_id' => $video_id,
                'url' => "'" . $document_data_files['DocumentFiles']['url'] . "'",
                'resolution' => "'" . $document_data_files['DocumentFiles']['resolution'] . "'",
                'duration' => "'" . $document_data_files['DocumentFiles']['duration'] . "'",
                'file_size' => "'" . $document_data_files['DocumentFiles']['file_size'] . "'",
                'default_web' => "'" . $document_data_files['DocumentFiles']['default_web'] . "'",
                'transcoding_job_id' => "'" . $document_data_files['DocumentFiles']['transcoding_job_id'] . "'",
                'transcoding_status' => "'" . $document_data_files['DocumentFiles']['transcoding_status'] . "'",
                'transcoding_status_details' => "'" . $document_data_files['DocumentFiles']['transcoding_status_details'] . "'",
                'created_at'=>"'".date('Y-m-d H:i:s')."'",
                'debug_logs'=>"'Cake::HuddlesController::add_observation_video::line=10784::document_id=$video_id::status=".$document_data_files['DocumentFiles']['transcoding_status']."::updated'"
                    ), array('document_id' => $video_id));

            $get_ob_comments = $this->getObComments($video_id);
            $document1 = $this->Document->getVideoDetails($video_id, 3);
            $commentsTime = $this->Comment->getVideoComments($video_id, '', '', '', '', 1);
            $comments_result = array();

            $changeType = '';
            $comments_result = $this->Comment->getVideoComments($video_id, $changeType, '', '', '', 1);
            $comments = array();
            foreach ($comments_result as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get tags
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
            }
            $comments_result = $comments;
            $videoComments = '';
            if (is_array($comments_result) && count($comments_result) > 0) {
                $videoComments = $comments_result;
            } else {
                $videoComments = '';
            }

            $params = array(
                'video_id' => $video_id,
                'huddle_id' => $account_folder_id,
                'comments' => $get_ob_comments,
                'videoComments' => $commentsTime,
                'videoCommentsArray' => (!isset($videoComments) ? array() : $videoComments),
                'videos' => $document1,
                'account_folder_id' => $account_folder_id,
                'account_id' => $account_id,
                'standardsL2' => $standardsL2,
                'standards' => $standards,
                'huddle_type' => $htype,
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
            );

            $view = new View($this, false);
            $html = $view->element('huddles/ajax/ajax_ob_publish', $params);
            echo json_encode(array('html' => $html));
            die;
        }
    }

    function publish_observation() {
        $video_id = $this->request->data['video_id'];
        $account_id = $this->request->data['account_id'];
        $huddle_id = $this->request->data['huddle_id'];
        $document = $this->Document->getVideoDetails($video_id, 3);
        $account_folder_document = $this->AccountFolderDocument->get($video_id);
        $document_data_files = $this->DocumentFiles->find("first", array("conditions" => array(
                "document_id" => $video_id,
                "default_web" => true
        )));
        if (empty($document) || empty($account_folder_document) || empty($document_data_files)) {
            echo json_encode(array('status' => 'false'));
            die;
        }
        $this->Document->updateAll(array('is_associated' => 1), array('id' => $video_id));


        $comments = $this->Comment->getVideoComments($video_id, '', '', '', '', 1);
        $this->Document->create();
        $data = array(
            'account_id' => $account_id,
            'doc_type' => '1',
            'url' => $document['Document']['url'],
            'original_file_name' => $document['Document']['original_file_name'],
            'file_size' => $document['Document']['file_size'],
            'view_count' => $document['Document']['view_count'],
            'content_type' => $document['Document']['content_type'],
            'published' => 1,
            'is_associated' => 1,
            'job_id' => $document['Document']['job_id'],
            'zencoder_output_id' => $document['Document']['zencoder_output_id'],
            'active' => $document['Document']['active'],
            'created_date' => date('Y-m-d H:i:s', time()),
            'created_by' => $document['Document']['created_by'],
            'last_edit_date' => date('Y-m-d H:i:s', time()),
            'last_edit_by' => $document['Document']['last_edit_by'],
            'zencoder_output' => $document['Document']['zencoder_output'],
            'encoder_provider' => $document['Document']['encoder_provider'],
            'encoder_status' => $document['Document']['encoder_status'],
            'thumbnail_number' => $document['Document']['thumbnail_number'],
            'request_dump' => $document['Document']['request_dump'],
            'recorded_date' => date('Y-m-d H:i:s', time()),
            'post_rubric_per_video' => '1'
        );
        if ($this->Document->save($data)) {
            $document_id = $this->Document->id;


            $this->DocumentFiles->create();
            $data2 = array(
                'document_id' => $document_id,
                'url' => $document_data_files['DocumentFiles']['url'],
                'resolution' => $document_data_files['DocumentFiles']['resolution'],
                'duration' => $document_data_files['DocumentFiles']['duration'],
                'file_size' => $document_data_files['DocumentFiles']['file_size'],
                'default_web' => $document_data_files['DocumentFiles']['default_web'],
                'transcoding_job_id' => $document_data_files['DocumentFiles']['transcoding_job_id'],
                'transcoding_status' => $document_data_files['DocumentFiles']['transcoding_status'],
                'created_at'=>date('Y-m-d H:i:s'),
                'debug_logs'=>"Cake::HuddlesController::publish_observation::line=10889::document_id=$document_id::status=".$document_data_files['DocumentFiles']['transcoding_status']."::created"
            );
            $this->checkDocumentFilesRecordExist($document_id, $data2, $this->request->data);
            $this->DocumentFiles->save($data2, $validation = TRUE);
            $data3 = array(
                'account_folder_id' => $huddle_id,
                'document_id' => $document_id,
                'title' => $account_folder_document['AccountFolderDocument']['title'],
                'zencoder_output_id' => $account_folder_document['AccountFolderDocument']['zencoder_output_id'],
                'desc' => $account_folder_document['AccountFolderDocument']['desc'],
                'is_viewed' => $account_folder_document['AccountFolderDocument']['is_viewed']
            );
            $this->AccountFolderDocument->create();
            $this->AccountFolderDocument->save($data3, $validation = TRUE);
            if (count($comments) > 0) {
                foreach ($comments as $row) {
                    $data4 = array(
                        'parent_comment_id' => $row['Comment']['parent_comment_id'],
                        'title' => htmlspecialchars($row['Comment']['title']),
                        'comment' => $row['Comment']['comment'],
                        'ref_type' => $row['Comment']['ref_type'],
                        'ref_id' => $document_id,
                        'user_id' => $row['Comment']['user_id'],
                        'time' => $row['Comment']['time'],
                        'restrict_to_users' => $row['Comment']['restrict_to_users'],
                        'created_date' => date('Y-m-d H:i:s', time()),
                        'last_edit_by' => $row['Comment']['last_edit_by'],
                        'created_by' => $row['Comment']['created_by'],
                        'last_edit_date' => date('Y-m-d H:i:s', time()),
                        'active' => $row['Comment']['active'],
                    );
                    $this->Comment->create();
                    $this->Comment->save($data4);
                    $comment_id = $this->Comment->id;
                    $get_standard = $this->AccountCommentTag->gettagsbycommentid($row['Comment']['id'], '0'); //get standards
                    $get_tags = $this->AccountCommentTag->gettagsbycommentid($row['Comment']['id'], '1'); //get tags
//                    echo "<pre>";
//                    print_r($get_tags);
                    // $standards = isset($observations_standards) ? $observations_standards : '';
                    if (!empty($get_standard)) {
                        foreach ($get_standard as $row) {
                            $this->AccountCommentTag->create();
                            $data = array(
                                // 'account_comment_tag_id1' => $row['AccountCommentTag']['account_comment_tag_id'],
                                'comment_id' => $comment_id,
                                'ref_id' => $document_id,
                                'ref_type' => $row['AccountCommentTag']['ref_type'],
                                'tag_title' => $row['AccountCommentTag']['tag_title'],
                                'account_tag_id' => $row['AccountCommentTag']['account_tag_id'],
                                'created_by' => $row['AccountCommentTag']['created_by'],
                                'created_date' => $row['AccountCommentTag']['created_date'],
                            );
                            $this->AccountCommentTag->save($data);
                        }
                    }
                    //$tags = isset($observations_tags) ? $observations_tags : '';
                    if (!empty($get_tags)) {
                        foreach ($get_tags as $row) {
                            $this->AccountCommentTag->create();
                            $data = array(
                                // 'account_comment_tag_id' => $row['AccountCommentTag']['account_comment_tag_id'],
                                'comment_id' => $comment_id,
                                'ref_id' => $document_id,
                                'ref_type' => $row['AccountCommentTag']['ref_type'],
                                'tag_title' => $row['AccountCommentTag']['tag_title'],
                                'account_tag_id' => $row['AccountCommentTag']['account_tag_id'],
                                'created_by' => $row['AccountCommentTag']['created_by'],
                                'created_date' => $row['AccountCommentTag']['created_date'],
                            );
                            $this->AccountCommentTag->save($data);
                        }
                    }
                }
            }
            $this->after_publish_observation_email($document_id);
        }
        $this->Document->updateAll(array('is_associated' => $document_id), array('id' => $video_id));
        echo json_encode(array('status' => 'true', 'document_id' => $document_id));
        die;
    }

    function observation_detail($account_folder_id, $video_id) {

        $document = $this->Document->getVideoDetails($video_id, 3);
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        if ($document) {
            $document_data_files = $this->DocumentFiles->find("first", array("conditions" => array(
                    "document_id" => $document['Document']['id'],
                    "default_web" => true
            )));
            $framework_id = '';
            $framework_check = '';

            $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'chk_frameworks')));
            $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
            $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

            $standardsL2 = array();
            $standards = array();
            if ($framework_id == -1 || $framework_id == 0) {
                $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
                $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
            }
            if ($framework_id == 0 && $framework_check == 1) {
                $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
                $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
                if (!empty($fw_id)) {
                    $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));

                    if ($standards_l2 > 0) {
                        $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                                "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                        )));
                        $standards = $this->AccountTag->find("all", array("conditions" => array(
                                "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                        )));
                    } else {
                        $standards = $this->AccountTag->find("all", array("conditions" => array(
                                "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                        )));
                    }
                } else {
                    $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                    $standardsL2 = array();
                    if ($standards_l2 > 0) {
                        $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                                "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                        )));
                        $standards = $this->AccountTag->find("all", array("conditions" => array(
                                "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                        )));
                    } else {
                        $standards = $this->AccountTag->find("all", array("conditions" => array(
                                "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                        )));
                    }
                }
            } elseif ($framework_id > 0 && $framework_check == 1) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
                )));
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }

            $get_ob_comments = $this->getObComments($video_id, $account_folder_id);
            $document1 = $this->Document->getVideoDetails($video_id, 3);
            $commentsTime = $this->Comment->getVideoComments($video_id, '', '', '', '', 1);
            $comments_result = array();
            $changeType = '';
            $comments_result = $this->Comment->getVideoComments($video_id, $changeType, '', '', '', 1);
            $comments = array();
            foreach ($comments_result as $comment) {
                $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
                $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '1'); //get tags
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
            }
            $comments_result = $comments;
            $videoComments = '';
            if (is_array($comments_result) && count($comments_result) > 0) {
                $videoComments = $comments_result;
            } else {
                $videoComments = '';
            }
            $item = $this->Document->getVideos($account_folder_id);
            if (count($item) > 0) {
                for ($j = 0; $j < count($item); $j++) {
                    $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
                }
            }
            //        // Load Workspace
            $work_space_item = $this->Document->getMyFilesVideos($account_id, $user_id, "", "Document.created_date DESC", 20, 0, 1);
            if (count($work_space_item) > 0) {
                for ($j = 0; $j < count($work_space_item); $j++) {
                    $work_space_item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $work_space_item[$j]['Document']['id'])));
                }
            }

            $params = array(
                'video_id' => $video_id,
                'files' => $work_space_item,
                'huddle_videos' => $item,
                'huddle_id' => $account_folder_id,
                'comments' => $get_ob_comments,
                'videoComments' => $commentsTime,
                'videoCommentsArray' => (!isset($videoComments) ? array() : $videoComments),
                'videos' => $document1,
                'account_folder_id' => $account_folder_id,
                'account_id' => $account_id,
                'standardsL2' => $standardsL2,
                'standards' => $standards,
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $video_id),
            );
            $view = new View($this, FALSE);
            if ($document['Document']['is_processed'] > 0) {
                return $view->element('huddles/ajax/ajax_ob_v2_details', $params);
            } else {
                return $view->element('huddles/ajax/ajax_ob_details', $params);
            }
        }
    }

    function observation_details_2($account_folder_id, $video_id) {

        return $this->redirect('/video_details/video_observation/' . $account_folder_id . '/' . $video_id, 301);

        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $this->layout = 'huddles';
        $document = $this->Document->getVideoDetails($video_id, 3);
        $video_name = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $video_id)));
        $processed_check = $document['Document']['is_processed'];
        $framework_id = '';
        $framework_check = '';
        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $standardsL2 = array();
        $standards = array();
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }

        $hmtl = '';

        $comments_html = '';
        //if ($is_stop) {
        $comments_html = $this->getObComments($video_id, $account_folder_id);
        //  }
        $count_comments = $this->Comment->find('all', array('conditions' => array('ref_id' => $video_id)));
        $count_comments = count($count_comments);
        $name = $this->AccountFolder->get($account_folder_id);
        $video = $this->Document->find('first', array('conditions' => array('id' => $video_id)));
        $tags = $this->AccountTag->gettagswithcount($account_id, '1', $video_id);
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";

        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $this->set('tags', $tags);
        $this->set('video_data', $name);
        $this->set('video', $video);
        $this->set('count_comments', $count_comments);
        $this->set('comments_html', $comments_html);
        $this->set('standardsL2', $standardsL2);
        $this->set('standards', $standards);
        $this->set('account_folder_id', $account_folder_id);
        $this->set('processed_check', $processed_check);
        $this->set('details_html', $hmtl);
        $this->set('video_id', $video_id);
        $this->set('ratings', $metric_old);
        $this->set('ratings', $metric_old);
        $this->set('video_name', $video_name);
        $this->set('account_id', $account_id);
        $this->set('press_enter_to_send', $type_pause['UserAccount']['press_enter_to_send']);
        $this->set('huddle_type', $htype);


        $this->set('huddle_id', $account_folder_id);
        $this->set('huddle_des', $this->AccountFolder->get($account_folder_id));
//        $this->render('observations_details');
    }

    function observation_details_3($account_folder_id, $video_id) {
        return $this->redirect('/video_details/video_observation/' . $account_folder_id . '/' . $video_id, 301);
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $view = new View($this, false);
        if ($view->Custom->check_if_evaluated_participant($account_folder_id, $user_id)) {
            $this->no_permissions();
        }


        $this->layout = 'huddles';
        $document = $this->Document->getVideoDetails($video_id, 3);
        $processed_check = $document['Document']['is_processed'];
        $framework_id = '';
        $framework_check = '';
        $video_name = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $video_id)));

        $count_comments = $this->Comment->find('all', array('conditions' => array('ref_id' => $video_id, 'ref_type' => 2)));
        $count_comments = count($count_comments);

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";
        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";
        $standards_new = $this->get_framework_settings($framework_id);
        $standardsL2 = array();
        $standards = array();
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));

        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";

        $hmtl = '';

        $comments_html = '';
        //if ($is_stop) {
        $comments_html = $this->getObComments($video_id, $account_folder_id);
        $document1 = $this->Document->getVideoDetails($video_id, 3);
        //  }
        $comments = $this->Comment->find("all", array("conditions" => array(
                "ref_id" => $video_id)));
        $tags = $this->AccountTag->gettagswithcount($account_id, '1', $video_id);

        $name = $this->AccountFolder->get($account_folder_id);
        $this->set('tags', $tags);
        $this->set('count_comments', $count_comments);
        $this->set('video_data', $name);
        $this->set('videos', $document1);
        $this->set('comments_html', $comments_html);
        $this->set('standardsL2', $standardsL2);
        $this->set('standards', $standards);
        $this->set('framework_data', $standards_new);
        $this->set('account_folder_id', $account_folder_id);
        $this->set('processed_check', $processed_check);
        $this->set('details_html', $hmtl);
        $this->set('video_id', $video_id);
        $this->set('ratings', $metric_old);
        $this->set('videoCommentsArray', $comments);
        $this->set('type_pause', $type_pause['UserAccount']['type_pause']);
        $this->set('press_enter_to_send', $type_pause['UserAccount']['press_enter_to_send']);
        $this->set('account_id', $account_id);
        $this->set('huddle_type', $htype);
        $this->set('video_name', $video_name);

        $this->set('huddle_id', $account_folder_id);
        $this->set('huddle_des', $this->AccountFolder->get($account_folder_id));
//        $this->render('observations_details');
    }

    function observation_details_4($account_folder_id, $video_id) {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $this->layout = 'huddles';
        $document = $this->Document->getVideoDetails($video_id, 3);
        $processed_check = $document['Document']['is_processed'];
        $framework_id = '';
        $framework_check = '';

        $count_comments = $this->Comment->find('all', array('conditions' => array('ref_id' => $video_id)));
        $count_comments = count($count_comments);

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";
        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $standardsL2 = array();
        $standards = array();
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }

        $hmtl = '';

        $comments_html = '';
        //if ($is_stop) {
        $comments_html = $this->getObComments($video_id, $account_folder_id);
        $document1 = $this->Document->getVideoDetails($video_id, 3);
        //  }
        $comments = $this->Comment->find("all", array("conditions" => array(
                "ref_id" => $video_id)));
        $tags = $this->AccountTag->gettagswithcount($account_id, '1', $video_id);

        $name = $this->AccountFolder->get($account_folder_id);
        $this->set('count_comments', $count_comments);
        $this->set('tags', $tags);
        $this->set('video_data', $name);
        $this->set('videos', $document1);
        $this->set('comments_html', $comments_html);
        $this->set('standardsL2', $standardsL2);
        $this->set('standards', $standards);
        $this->set('account_folder_id', $account_folder_id);
        $this->set('processed_check', $processed_check);
        $this->set('details_html', $hmtl);
        $this->set('video_id', $video_id);
        $this->set('ratings', $metric_old);
        $this->set('videoCommentsArray', $comments);
        $this->set('account_id', $account_id);

        $this->set('huddle_id', $account_folder_id);
        $this->set('huddle_des', $this->AccountFolder->get($account_folder_id));
//        $this->render('observations_details');
    }

    function observation_details_1($account_folder_id, $video_id, $bool = 0) {
        return $this->redirect('/video_details/scripted_observations/' . $account_folder_id . '/' . $video_id, 301);
        $coachee_emails = $this->AccountFolder->getHuddleUsers($account_folder_id);
        foreach ($coachee_emails as $email) {
            if ($email['huddle_users']['role_id'] == 210) {
                $coachee_email = $email['User']['email'];
                $coachee_name = $email['User']['first_name'] . ' ' . $email['User']['last_name'];
            }
        }

        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $this->layout = 'huddles';
        $document = $this->Document->getVideoDetails($video_id, 3);
        $processed_check = $document['Document']['is_processed'];
        $video_name = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $video_id)));
        $video_details = $this->Document->find('first', array('conditions' => array('id' => $video_id)));
        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $standardsL2 = array();
        $standards = array();
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
        }
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }

        $hmtl = '';

        $comments_html = '';
        //if ($is_stop) {
        $comments_html = $this->getObComments_modified($video_id, $account_folder_id);
        $document1 = $this->Document->getVideoDetails($video_id, 3);
        //  }
        $tags = $this->AccountTag->gettagswithcount($account_id, '1', $video_id);
        if ($bool) {
            $user_activity_logs = array(
                'ref_id' => $video_id,
                'desc' => 'Observation_Completed',
                'url' => $this->base . '/huddles/view/' . $account_folder_id . '/1/' . $video_id,
                'account_folder_id' => $account_folder_id,
                'type' => '2'
            );
            //  $this->user_activity_logs($user_activity_logs);
        }

        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";

        $this->set('tags', $tags);
        $this->set('h_type', $htype);
        $this->set('video_details', $video_details);
        $this->set('videos', $document1);
        $this->set('video_name', $video_name);
        $this->set('comments_html', $comments_html);
        $this->set('standardsL2', $standardsL2);
        $this->set('standards', $standards);
        $this->set('account_folder_id', $account_folder_id);
        $this->set('processed_check', $processed_check);
        $this->set('details_html', $hmtl);
        $this->set('video_id', $video_id);
        $this->set('coachee_email', $coachee_email);
        $this->set('coachee_name', $coachee_name);


        $this->set('huddle_id', $account_folder_id);
        $this->set('huddle_des', $this->AccountFolder->get($account_folder_id));
//        $this->render('observations_details');
    }

    function observation_details($account_folder_id, $video_id) {
        $this->layout = 'huddles';
        $document = $this->Document->getVideoDetails($video_id, 3);
        $processed_check = $document['Document']['is_processed'];
        if ($document['Document']['published'] == 0 && $document['Document']['is_associated'] == '0') {
            $hmtl = $this->get_huddle_document($account_folder_id, true, $video_id);
        } elseif ($document['Document']['is_processed'] > 0) {
            $hmtl = $this->observation_detail($account_folder_id, $video_id);
            $this->observation_v2_details($account_folder_id, $video_id, $hmtl);
        } else {
            $hmtl = $this->observation_detail($account_folder_id, $video_id);
        }
        $this->set('processed_check', $processed_check);
        $this->set('details_html', $hmtl);
        $this->set('video_id', $video_id);
        $this->set('huddle_id', $account_folder_id);
        $this->set('huddle_des', $this->AccountFolder->get($account_folder_id));
        $this->render('observations_details');
    }

    function observation_v2_details($account_folder_id, $video_id, $html) {
        $this->set('details_html', $html);
        $this->set('video_id', $video_id);
        $this->set('huddle_id', $account_folder_id);
        $this->set('huddle_des', $this->AccountFolder->get($account_folder_id));
        $this->render('observation_v2_details');
    }

    function load_observation_tab() {
        $huddle_id = isset($this->request->data['huddle_id']) ? $this->request->data['huddle_id'] : '2735';
        $this->Session->write('account_folder_id', $huddle_id);
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $user_current_account = $this->Session->read('user_current_account');
        $view = new View($this, FALSE);
        $this->layout = 'huddles';
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        $totalVideos = $this->Document->countVideos($huddle_id);
        //$ob_item = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page, '', '', 3);
        $ob_item = $this->Document->getVideos($huddle_id, '', '', '', '', '', 3);
        if (count($ob_item) > 0) {
            for ($j = 0; $j < count($ob_item); $j++) {
                $ob_item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $ob_item[$j]['Document']['id'])));
            }
        }
        $totalObVideos = $this->Document->countVideos($huddle_id, '', '', 3);
        $observationsData = array(
            'account_folder_id' => $huddle_id,
            'account_id' => $account_id,
            'videos' => $ob_item,
            'huddle' => $this->AccountFolder->getHuddle($huddle_id, $account_id),
            'totalVideos' => $totalObVideos,
            'video_per_page' => $this->video_per_page,
            'current_page' => 1,
            'huddle_id' => $huddle_id,
            'user_id' => $user_id,
            'huddle_permission' => $this->Session->read('user_huddle_level_permissions'),
            'type' => 'get_video_comments',
            'huddle_des' => $this->AccountFolder->get($huddle_id),
                //'associate_video_model' => $this->load_associate_model($huddle_id)
        );
        $view = new View($this, false);
        $html = $view->element('huddles/ajax/ajax_search_observation', $observationsData);
        echo json_encode(array('html' => $html));
        die;
    }

    function getObVideoSearch($huddle_id, $title = '') {
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('huddles/archive_contents');
        $this->set('language_based_content', $language_based_content);

        $load_more = isset($this->request->data['load_more']) ? (int) $this->request->data['load_more'] : 0;
        $page = !empty($this->request->data['page']) ? (int) $this->request->data['page'] : 1;
        $sort_param = !isset($this->request->data['sort']) ? '' : $this->request->data['sort'];
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('Huddles/view');

        $item = $this->_getObVideoList($huddle_id, $title, $sort_param, 10, $page - 1);
        if (count($item) > 0 && $item != '') {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }
        $view = new View($this, false);
        $videos_count = $this->Document->countObservations($huddle_id, $title);
        $total_count = $view->Custom->chck_publish_count($item, $huddle_permission);
        $element_data = array(
            'huddle_id' => $huddle_id,
            'videos' => $item,
            'totalVideos' => $total_count,
            'video_per_page' => 10,
            'current_page' => $page,
            'language_based_content' => $language_based_content
        );


        $html = $view->element('ajax/ajaxSearchObservation', $element_data);

        if (!$load_more) {

        } elseif (10 * $page >= $element_data['totalVideos']) {
            $html .= '<script>hideLoadMore();</script>';
        }
        echo json_encode(array('html' => $html, 'count' => $total_count));
        exit;
    }

    private function _getObVideoList($huddle_id, $title = '', $sort = '', $limit = 0, $page = 0) {
        $result = $this->Document->getVideos($huddle_id, $title, $sort, $limit, $page, '', 3);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    function get_all_ob_comments($video_id) {
        $html = $this->getObComments($video_id);
        echo json_encode(array('comments' => $html));
        die;
    }

    function edit_observations($comment_id) {
        $observations_time_hours = $this->request->data['time_hours'];
        $observations_time_hours = $this->request->data['time_minutes'];
        $observations_time_hours = $this->request->data['time_seconds'];
        $observations_time = $this->request->data['time_hours'] . ':' . $this->request->data['time_minutes'] . ':' . $this->request->data['time_seconds'];

        $time = $this->time_to_decimal($observations_time);

        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $data = array(
            'comment' => "'" . Sanitize::escape($this->data['comment']['comment']) . "'",
            'last_edit_date' => "'" . date('y-m-d H:i:s', time()) . "'",
            'last_edit_by' => $user_id,
            'time' => $time
        );

        if ($this->Comment->updateAll($data, array('id' => $this->data['comment_id']))) {
            $txtVidTag = 'txtVideoTags2' . $this->data['comment_id'];
            $txtVidstandard = 'txtVideostandard2' . $this->data['comment_id'];
            $tags = isset($this->request->data[$txtVidTag]) ? $this->request->data[$txtVidTag] : '';
            $standards = isset($this->request->data[$txtVidstandard]) ? $this->request->data[$txtVidstandard] : '';

            if (!empty($tags)) {
                $this->update_tag($tags, $this->data['comment_id'], $this->data['videoId']);
            } else {
                $this->AccountCommentTag->deleteAll(array('comment_id' => $this->data['comment_id'], "ref_type" => "1"), $cascade = true, $callbacks = true);
            }
            if (!empty($standards)) {
                $this->AccountCommentTag->deleteAll(array('comment_id' => $this->data['comment_id'], "ref_type" => "0"), $cascade = true, $callbacks = true);
                $this->save_standard_tag($standards, $this->data['comment_id'], $this->data['videoId']);
            } else {
                $this->AccountCommentTag->deleteAll(array('comment_id' => $this->data['comment_id'], "ref_type" => "0"), $cascade = true, $callbacks = true);
            }

            $result = array(
                'status' => 'success',
                'internal_comment_id' => $this->request->data['internal_comment_id'],
                'comments' => $this->getObComment($this->data['videoId'], $comment_id)
            );

            echo json_encode($result);
            exit;
        } else {
            echo "failed";
            exit;
        }
    }

    function load_associate_model($huddle_id) {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $item = $this->Document->getVideos($huddle_id);
        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }
        // Load Workspace
        $work_space_item = $this->Document->getMyFilesVideos($account_id, $user_id, "", "Document.created_date DESC", 20, 0, 1);
        if (count($work_space_item) > 0) {
            for ($j = 0; $j < count($work_space_item); $j++) {
                $work_space_item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $work_space_item[$j]['Document']['id'])));
            }
        }
        $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL"
        )));
        $standardsL2 = array();
        if ($standards_l2 > 0) {
            $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL"
            )));
            $standards = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL"
            )));
        } else {
            $standards = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id
            )));
        }

        $params = array(
            'files' => $work_space_item,
            'huddle_videos' => $item,
            'huddle_id' => $huddle_id
        );

        $view = new View($this, false);
        return $html = $view->element('huddles/ajax/associate_model', $params);
    }

    function mobile_video($video_id) {

        $video = $this->Document->find('first', array('conditions' => array('id' => $video_id)));
        $video = json_encode($video);
        echo $video;
        die();
    }

    function update_note() {
        $title = $this->data['title'];
        $data = array(
            'title' => '"' . Sanitize::escape(htmlspecialchars($title)) . '"'
        );
        $this->AccountFolderDocument->updateAll($data, array('document_id' => $this->data['video_id']));
        echo $this->AccountFolderDocument->getAffectedRows();
        die();
    }

    function send_email() {
        $emails = explode(',', $this->request->data['email']);

        foreach ($emails as $email) {

            $this->Email->delivery = 'smtp';
            $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
            $this->Email->to = $email;
            $this->Email->subject = $this->request->data['subject'];
            $this->Email->template = 'default';
            $this->Email->sendAs = 'html';

            $filename = $this->request->data['filename'];

            $Path = APP . 'tmp/PdfReport' . $filename;

            $this->Email->attachments = array($Path);
            $view = new View($this, true);
            $this->AuditEmail->create();
            $auditEmail = array(
                'account_id' => $this->request->data['account_id'],
                'email_from' => $this->Email->from,
                'email_to' => $email,
                'email_subject' => $this->Email->subject,
                'email_body' => $this->request->data['message'],
                'is_html' => true,
                'sent_date' => date("Y-m-d H:i:s")
            );

            if (!empty($this->request->data['email'])) {

                $this->AuditEmail->save($auditEmail, $validation = TRUE);

                if ($this->Email->send($this->request->data['message'])) {
                    echo TRUE;
                } else {
                    return FALSE;
                }
            }
        }
        die;
        return FALSE;
    }

    function observations_load() {
        $huddle_id = $this->request->data['huddle_id'];
        $account_id = $this->request->data['account_id'];
        $ob_count = $this->request->data['ob_count'];
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $is_coach = '';
        if ($huddle_permission == 210) {
            $is_coach = false;
            $compare_count = $this->Document->countObservations($huddle_id, '', 0, 3, $is_coach);
        } else {
            $compare_count = $this->Document->countObservations($huddle_id, '');
        }


        if ($ob_count != $compare_count) {
            echo json_encode(array('status' => 1, 'compare_count' => $compare_count));
            die;
        } else {
            echo json_encode(array('status' => 0, 'compare_count' => $compare_count));
            die;
        }
    }

    function type_pause() {
        $user_id = $this->request->data['user_id'];
        $account_id = $this->request->data['account_id'];
        $value = $this->request->data['value'];

        $data = array(
            'type_pause' => $value,
        );
        $this->UserAccount->updateAll($data, array('user_id' => $user_id, 'account_id' => $account_id));
        die();
    }

    function press_enter_to_send() {
        $user_id = $this->request->data['user_id'];
        $account_id = $this->request->data['account_id'];
        $value = $this->request->data['value'];

        $data = array(
            'press_enter_to_send' => $value,
        );
        $this->UserAccount->updateAll($data, array('user_id' => $user_id, 'account_id' => $account_id));
        die();
    }

    function upload_audio_comment() {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        if (isset($_FILES['file'])) {
            $fileTempName = $_FILES['file']['tmp_name'];
            $fileType = $_FILES['file']['type'];

            $bucket_name = Configure::read('bucket_name');

            $image_path = "uploads/" . $account_id . "/mp3/" . date("Y") . "/" . date("m") . "/" . date("d") . "/audio" . $account_id . "_" . $user_id . "_" . time() . ".wav";
            $keyname = $image_path;

            // Instantiate the client.
            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $distribution_id = Configure::read('distribution_id');

            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            // Upload a file.
            $result = $client->putObject(array(
                'Bucket' => $bucket_name,
                'Key' => $keyname,
                'SourceFile' => $fileTempName,
                'ContentType' => $fileType,
                'ACL' => 'public-read'
            ));

            //This comes from key pair you generated for cloudfront
            $keyPairId = Configure::read('cloudfront_keypair');

            //Read Cloudfront Private Key Pair
            $cloudFront = CloudFrontClient::factory(array(
                        'private_key' => realpath(dirname(__FILE__) . '/..') . "/Config/cloudfront_key.pem",
                        'key_pair_id' => $keyPairId,
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            $result = $cloudFront->createInvalidation(array(
                // DistributionId is required
                'DistributionId' => $distribution_id,
                'Paths' => array(
                    // Quantity is required
                    'Quantity' => 1,
                    'Items' => array("/" . $keyname)
                ),
                // CallerReference is required
                'CallerReference' => $bucket_name . strtotime('now'),
            ));
        }
        //echo json_encode(array('status' => 1, 'path' => $image_path));
        //var_dump($result);
        echo $image_path;
        die;
    }

    function delete_document_file($document_id) {

        $this->Document->deleteAll(array('id' => $document_id), $cascade = true, $callbacks = true);
        die;
    }

    function view_resource($id) {
    $account_folder_id = '';
    $result_data = $this->AccountFolderDocument->get_account_folder_document($id);
    if($result_data){
            $account_folder_id = $result_data['AccountFolderDocument']['account_folder_id'];
        }
            $user_activity_logs = array(
            'ref_id' => $id,
            'desc' => 'View Resource',
            'url' => $this->base . '/Huddles/download/' . $id,
            'type' => '13',
            'account_folder_id' => $account_folder_id ,
            'environment_type' => '2',
        );
        $this->user_activity_logs($user_activity_logs);
        die;
    }

    function after_publish_observation_email($document_id) {


        $is_video_published_now = false;

        $video_old = $this->Document->find('first', array('conditions' => array('id' => $document_id)));


        if (isset($video_old) && !empty($video_old)) {

            $current_user = $this->Session->read('user_current_account');
            $account_id = $current_user['accounts']['account_id'];

            $is_video_published_now = true;


            $video = $this->Document->find('first', array('conditions' => array('id' => $document_id)));
            $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $document_id)));
            $account_folder = $this->AccountFolder->find('first', array('conditions' => array(
                    'account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'])
            ));
            $creator = $this->User->find('first', array('conditions' => array('id' => $video['Document']['created_by'])));
            $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_document['AccountFolderDocument']['account_folder_id']);


            if ($account_folder['AccountFolder']['folder_type'] != 1) {
                if ($this->check_subscription($creator['User']['id'], '7', $account_id)) {

                    $video_name = $account_folder_document['AccountFolderDocument']['title'];

                    if (empty($video_name))
                        $video_name = $account_folder['AccountFolder']['name'];

                    //var_dump($creator);
                    $this->sendVideoPublishedNotification_AfterPublish(
                            $video_name
                            , $account_folder['AccountFolder']['account_folder_id']
                            , $video['Document']['id']
                            , $creator
                            , $account_folder['AccountFolder']['account_id']
                            , $creator, $creator['User']['email']
                            , $creator['User']['id']
                            , $account_folder['AccountFolder']['folder_type']
                            , '7'
                    );
                }
            } else {

                if (count($huddleUsers) > 0) {
                    for ($i = 0; $i < count($huddleUsers); $i++) {

                        $huddleUser = $huddleUsers[$i];

                        if ($is_video_published_now && $huddleUser['huddle_users']['role_id'] == 210) {
                            if ($this->check_subscription($huddleUser['User']['id'], '8', $account_id)) {
                                $this->sendVideoPublishedNotification_AfterPublish(
                                        $account_folder['AccountFolder']['name']
                                        , $account_folder['AccountFolder']['account_folder_id']
                                        , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $huddleUsers, $huddleUser['User']['email'], $huddleUser['User']['id'], $account_folder['AccountFolder']['folder_type'], '8');
                            }
                        }
                    }
                }
            }
        }
    }

    function feedback_publish($huddle_id, $video_id) {
        $user = $this->Session->read('user_current_account');
        $user_id = $user['User']['id'];
        $data = array(
            'active' => 1,
            'published_by' => $user_id
        );
        $this->Comment->updateAll($data, array('ref_id' => $video_id, 'user_id' => $user_id));
        if ($this->Comment->getAffectedRows() > 0) {
            $this->EmailFeedbackVideo($huddle_id, $video_id);
            echo json_encode(array('message' => 'Comments have been posted.', 'status' => true));
            die;
        } else {
            echo json_encode(array('message' => 'Comments are not posted successfully, please try agian', 'status' => false));
            die;
        }
    }

    function get_inactive_comments($video_id) {
        $user = $this->Session->read('user_current_account');
        $user_id = $user['User']['id'];
        $this->layout = null;
        $result = $this->Comment->find('all', array(
            'conditions' => array(
                'ref_id' => $video_id,
                'user_id' => $user_id,
                'active' => array('', '0')
            )
        ));
        $response = array();
        if (count($result) > 0) {
            $response['total_comments'] = count($result);
        } else {
            $response['total_comments'] = 0;
        }
        echo json_encode($response);
        exit;
    }

    function sent_feedback_email() {
        $attachment_1 = '';
        $attachment_2 = '';
        $errors = array();
        $maxsize = 204800;
        $acceptable = array(
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/msword',
            'application/vnd.ms-excel',
            'application/xls',
            'image/jpeg',
            'image/jpg',
            'image/gif',
            'image/png'
        );

        $huddle_id = $this->request->data['huddle_id'];
        $video_id = $this->request->data['video_id'];
        if (isset($_FILES['additional_attachemnt']['name']) && $_FILES['additional_attachemnt']['name'] != '') {
            $additional_attachment = $_FILES['additional_attachemnt']['tmp_name'];
            if (($_FILES['additional_attachemnt'] ['size'] >= $maxsize) || ($_FILES["additional_attachemnt"]["size"] == 0)) {
                $errors[] = $this->language_based_messages['file_too_large_file_must_be_less_than_2_megabytes'];
            }
            if (!in_array($_FILES['additional_attachemnt']['type'], $acceptable) && (!empty($_FILES["additional_attachemnt"]["type"]))) {
                $errors[] = $this->language_based_messages['invalid_file_type_only_PDF_XLXS_XLS_JPG_GIF_and_PNG_types_are_accepted'];
            }

            if (count($errors) === 0) {
                $path = APP . 'tmp/PdfReport_' . $_FILES['additional_attachemnt']['name'];
                move_uploaded_file($additional_attachment, $path);
                $attachment_1 = $path;
            } else {
                $error_message = '';
                if (isset($errors[0])) {
                    $error_message .= $errors[0] . '<br/>';
                } elseif (isset($errors[1])) {
                    $error_message .= $errors[0];
                }

                $this->Session->setFlash($error_message, 'default', array('class' => 'message error'));
                $this->redirect('/Huddles/view/' . $huddle_id . '/1/' . $video_id);
            }
        }

        if (isset($this->request->data['filename_email']) && $this->request->data['filename_email'] != '') {
            $filename = $this->request->data['filename_email'];
            $attachment_2 = APP . 'tmp/PdfReport' . $filename;
        }
//        $emails_list = $this->request->data['email'];
        $emails = explode(',', $this->request->data['email']);
        $this->Email->delivery = 'smtp';
        $this->Email->from = $this->custom->get_site_settings('static_emails')['noreply'];
        $this->Email->to = $emails;
        $this->Email->subject = $this->request->data['subject'];
        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
//      $this->Email->attachments = array($attachment_1, $attachment_2);
        $this->Email->attachments = array($attachment_1);

        $view = new View($this, true);

        foreach ($emails as $email_single) {
            $this->AuditEmail->create();
            $auditEmail = array(
                'account_id' => $this->request->data['account_id'],
                'email_from' => $this->Email->from,
                'email_to' => $email_single,
                'email_subject' => $this->Email->subject,
                'email_body' => $this->request->data['message'],
                'is_html' => true,
                'sent_date' => date("Y-m-d H:i:s")
            );
            if (!empty($email_single)) {
                $this->AuditEmail->save($auditEmail, $validation = TRUE);
            }
        }
        if ($this->Email->send($this->request->data['message'])) {
            $this->Session->setFlash($this->language_based_messages['email_sent_successfully'], 'default', array('class' => 'message success'));
            $this->redirect('/Huddles/view/' . $huddle_id . '/1/' . $video_id);
        } else {
            $this->Session->setFlash($this->language_based_messages['sorry_your_email_could_not_be_sent_right_now_please_try_again'], 'default', array('class' => 'message error'));
            $this->redirect('/Huddles/view/' . $huddle_id . '/1/' . $video_id);
        }

        die;
        return FALSE;
    }

    function EmailFeedbackVideo($account_folder_id, $video_id) {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $video_data = $this->Document->find('first', array('conditions' => array(
                'id' => $video_id
        )));

        $video_data_name = $this->AccountFolderDocument->find('first', array('conditions' => array(
                'document_id' => $video_id
        )));

        $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
        $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_id);
        $userGroups = $this->AccountFolderGroup->getHuddleGroups($account_folder_id);
        $view = new View($this, false);
        $coach_ids = $view->Custom->get_evaluator_ids($account_folder_id);
        $huddle_user_ids = array();
        if ($userGroups && count($userGroups) > 0) {
            foreach ($userGroups as $row) {
                $huddle_user_ids[] = $row['user_groups']['user_id'];
            }
        }
        if ($huddleUsers && count($huddleUsers) > 0) {
            foreach ($huddleUsers as $row) {
                $huddle_user_ids[] = $row['huddle_users']['user_id'];
            }
        }

        if (count($huddle_user_ids) > 0) {
            $user_ids = implode(',', $huddle_user_ids);
            if (!empty($user_ids))
                $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_ids . ')')));
        } else {
            $huddleUserInfo = array();
        }

        $coach_details = $this->User->find('all', array('conditions' => array('id' => $coach_ids)));

        $view = new View($this, false);
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'folder_type')));
        $h_type = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        if ($huddleUserInfo && count($huddleUserInfo) > 0) {
            $commentsData = array(
                'account_folder_id' => $account_folder_id,
                'huddle_name' => $huddle[0]['AccountFolder']['name'],
                'video_link' => '/huddles/view/' . $account_folder_id . '/1/' . $video_id,
                'participating_users' => $huddleUserInfo,
                'video_title' => $video_data_name['AccountFolderDocument']['title'],
                'coach_details' => $coach_details
            );
            foreach ($huddleUserInfo as $row) {
                if (($h_type == '3' && $video_data['Document']['created_by'] == $row['User']['id']) || ($h_type == '2' && !$view->Custom->check_if_evalutor($account_folder_id, $row['User']['id']))) {


                    $commentsData ['email'] = $row['User']['email'];
                    if ($this->check_subscription($row['User']['id'], '7', $account_id)) {
                        $this->sendEmailFeedBackVideo($commentsData, $row['User']['id']);
                    }
                }
            }
        }
    }

    function check_videos_count() {
        $huddle_id = $this->request->data['huddle_id'];
        $user_id = $this->request->data['user_id'];
        $view = new View($this, false);
        $check_if_evaluated_participant = $view->Custom->check_if_evaluated_participant($huddle_id, $user_id);
        $get_eval_participant_videos = $view->Custom->get_eval_participant_videos($huddle_id, $user_id);
        $submission_allowed = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'submission_allowed')));
        $vide_max_limit = 1;
        if ($submission_allowed['AccountFolderMetaData']['meta_data_value'] != '') {
            $vide_max_limit = $submission_allowed['AccountFolderMetaData']['meta_data_value'];
        }




//        echo "<pre>";
//        var_dump($check_if_evaluated_participant);
//        print_r($get_eval_participant_videos);
//        print_r($submission_allowed);
//
//        echo "$check_if_evaluated_participant && $get_eval_participant_videos >= $vide_max_limit";
//        die;

        if ($check_if_evaluated_participant) {
            $submission_date = $view->Custom->get_submission_date($huddle_id);
            if ($submission_date != '') {
                $submission_string = strtotime($submission_date);
                $current_time = strtotime(date('Y-m-d h:i:s a', time()));
                if ($submission_string < $current_time) {
                    echo json_encode(
                            array(
                                'message' => 'You cannot upload a video because your submission date has expired.',
                                'can_upload' => 'no'
                    ));
                    die;
                }
            }
        }


        if ($check_if_evaluated_participant && $get_eval_participant_videos >= $vide_max_limit) {
            echo json_encode(
                    array(
                        'message' => 'You have already submitted a video to be evaluated.  You must delete your submission before resubmitting.',
                        'can_upload' => 'no'
            ));
            die;
        } else {
            echo json_encode(array('message' => '', 'can_upload' => 'yes'));
            die;
        }
    }

    function publish_scripted_observation($huddle_id, $video_id) {

        $current_user = $this->Session->read('user_current_account');
        $account_id = $current_user['accounts']['account_id'];

        $this->Document->updateAll(array('is_associated' => 1, 'published' => 1), array('id' => $video_id));
        $user_activity_logs = array(
            'ref_id' => $video_id,
            'desc' => 'Scripted Notes',
            'url' => $this->base . '/Huddles/observation_details_1/' . $huddle_id . '/' . $video_id,
            'account_folder_id' => $huddle_id,
            'date_added' => date("Y-m-d H:i:s"),
            'type' => '23',
            'environment_type' => '2'
        );
        $this->user_activity_logs($user_activity_logs);

        $url = $this->base . '/Huddles/observation_details_1/' . $huddle_id . '/' . $video_id;
        $video = $this->Document->find('first', array('conditions' => array('id' => $video_id)));
        $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $video_id)));
        $account_folder = $this->AccountFolder->find('first', array('conditions' => array(
                'account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'])
        ));
        $creator = $this->User->find('first', array('conditions' => array('id' => $video['Document']['created_by'])));
        $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_document['AccountFolderDocument']['account_folder_id']);
        $is_video_published_now = true;


        if ($account_folder['AccountFolder']['folder_type'] != 1) {
            if ($this->check_subscription($creator['User']['id'], '7', $account_id)) {

                $video_name = $account_folder_document['AccountFolderDocument']['title'];

                if (empty($video_name))
                    $video_name = $account_folder['AccountFolder']['name'];

                //var_dump($creator);
                $this->sendVideoPublishedNotification_AfterPublish(
                        $video_name
                        , $account_folder['AccountFolder']['account_folder_id']
                        , $video['Document']['id']
                        , $creator
                        , $account_folder['AccountFolder']['account_id']
                        , $creator, $creator['User']['email']
                        , $creator['User']['id']
                        , $account_folder['AccountFolder']['folder_type']
                        , '7'
                        , $url
                );
            }
        } else {

            if (count($huddleUsers) > 0) {
                for ($i = 0; $i < count($huddleUsers); $i++) {

                    $huddleUser = $huddleUsers[$i];

                    if ($is_video_published_now && $huddleUser['huddle_users']['role_id'] == 210) {
                        if ($this->check_subscription($huddleUser['User']['id'], '8', $account_id)) {
                            $this->sendVideoPublishedNotification_AfterPublish(
                                    $account_folder['AccountFolder']['name']
                                    , $account_folder['AccountFolder']['account_folder_id']
                                    , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $huddleUsers, $huddleUser['User']['email'], $huddleUser['User']['id'], $account_folder['AccountFolder']['folder_type'], '8', $url);
                        }
                    }
                }
            }
        }


        $this->redirect('/Huddles/observation_details_1/' . $huddle_id . '/' . $video_id);
    }

    function edtpa_validate_xml($assessment_id, $assessment_version) {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $client = S3Client::factory(array(
                    'key' => Configure::read('access_key_id'),
                    'secret' => Configure::read('secret_access_key')
        ));

        $edtpa_base_url = Configure::read('edtpa_base_url');
        $edtpa_provider_name = Configure::read('edtpa_provider_name');
        $edtpa_shared_secret = Configure::read('edtpa_shared_secret');
        $edtpa_shared_key = Configure::read('edtpa_shared_key');
        $project_id = "edTPA";

        //$edtpa_base_url = "https://partners.nesinc.com/ws/seam/resource/v1/Import/";
        $edtpa_base_url = "https://cert-ppp.eportfoliodev.com/ws/seam/resource/v1/Import/";
        $edtpa_provider_name = "Sibme";
        $edtpa_shared_secret = "z3ol8fvb739u";
        $edtpa_shared_key = "cfb1bb092c907fb8db23456b7c7a5411c4059796f582902e7cd34add57b9064a";
        $project_id = "edTPA";
        if (IS_PROD) {
            $edtpa_base_url = "https://ppp.eportfolio.pearson.com/ws/seam/resource/v1/Import/";
            $edtpa_provider_name = "Sibme";
            $edtpa_shared_secret = "4*fx1ENebUbf#w6q4nz3TBfRx";
            $edtpa_shared_key = "e557cc4002f5957da1f0e2ed29b4ef12c2497fa721e9ff24b9a225d06d3baf1c";
            $project_id = "edTPA";
        }

        $edtpa_assessment = $this->EdtpaAssessments->find('first', array(
            'conditions' => array(
                'assessment_id' => $assessment_id,
                'assessment_version' => $assessment_version,
                'project_id' => $project_id
            )
        ));

        if (count($edtpa_assessment) == 0) {

            echo json_encode(array('success' => 'false', 'result' => 'No Assessment found.'));
            die;
        } else {

            //found the assessment
            $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
                'conditions' => array(
                    'edtpa_assessment_id' => $edtpa_assessment['EdtpaAssessments']['id'],
                    'account_id' => $account_id
                )
            ));

            if (count($edtpa_assessment_account) > 0) {

                $edtpa_assessment_account_candidate = $this->EdtpaAssessmentAccountCandidates->find('first', array(
                    'conditions' => array(
                        'user_id' => $user_id,
                        'edtpa_assessment_account_id' => $edtpa_assessment_account['EdtpaAssessmentAccounts']['id']
                    )
                ));

                if (count($edtpa_assessment_account_candidate) > 0) {


                    $current_date = date("Y-m-d\TH:i:s.000\Z", strtotime("now"));
                    $key = pack('H*', "$edtpa_shared_key");

                    $text = '<?xml version="1.0" encoding="UTF-8"?>
            <espm:portfolioManifest created="' . $current_date . '" schemaVersion="2.0" xmlns:espm="http://www.pearson.com/ESPortfolioManifest" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.pearson.com/ESPortfolioManifest http://partners.nesinc.com/ws/schemas/ESPortfolioManifest_v2.xsd">
                <espm:transferRequestInformation partnerTransferRequestID="SAMPLEELEM_LIT3" partnerID="Sibme" submittedAt="' . $current_date . '">
                    <espm:partnerCandidateID>' . $user_id . '</espm:partnerCandidateID>
                    <espm:partnerPortfolioID>SibmePortfolio_' . md5($transfer_id) . '</espm:partnerPortfolioID>
                </espm:transferRequestInformation>';

                    $text .= '<espm:assessment projectID="' . $project_id . '" assessmentID="' . $assessment_id . '" assessmentVersion="' . $assessment_version . '" >';

                    $edtpa_assessment_portfolios = $this->EdtpaAssessmentPortfolios->find('all', array(
                        'conditions' => array(
                            'edtpa_assessment_id' => $edtpa_assessment['EdtpaAssessments']['id']
                        )
                    ));


                    foreach ($edtpa_assessment_portfolios as $edtpa_assessment_portfolio) {

                        $text .= '<espm:portfolioNode code="' . $edtpa_assessment_portfolio['EdtpaAssessmentPortfolios']['code'] . '">';

                        $edtpa_assessment_portfolio_nodes = $this->EdtpaAssessmentPortfolioNodes->find('all', array(
                            'conditions' => array(
                                'edtpa_assessment_portfolio_id' => $edtpa_assessment_portfolio['EdtpaAssessmentPortfolios']['id'],
                                'edtpa_assessment_id' => $edtpa_assessment['EdtpaAssessments']['id']
                            )
                        ));

                        foreach ($edtpa_assessment_portfolio_nodes as $edtpa_assessment_portfolio_node) {

                            $text .= '<espm:portfolioNode code="' . $edtpa_assessment_portfolio_node['EdtpaAssessmentPortfolioNodes']['code'] . '">';

                            $edtpa_assessment_portfolio_candidate_submissions = $this->EdtpaAssessmentPortfolioCandidateSubmissions->find('all', array(
                                'joins' => array(
                                    array(
                                        'table' => 'documents as Document',
                                        'type' => 'left',
                                        'conditions' => 'Document.id = EdtpaAssessmentPortfolioCandidateSubmissions.document_id'
                                    )
                                ),
                                'fields' => array(
                                    'EdtpaAssessmentPortfolioCandidateSubmissions.*',
                                    'Document.*'
                                ),
                                'conditions' => array(
                                    'EdtpaAssessmentPortfolioCandidateSubmissions.edtpa_assessment_account_candidate_id' => $edtpa_assessment_account_candidate['EdtpaAssessmentAccountCandidates']['id'],
                                    'EdtpaAssessmentPortfolioCandidateSubmissions.edtpa_assessment_portfolio_node_id' => $edtpa_assessment_portfolio_node['EdtpaAssessmentPortfolioNodes']['id']
                                )
                            ));

                            if (count($edtpa_assessment_portfolio_candidate_submissions) > 0) {

                                $submission_counter = 1;
                                foreach ($edtpa_assessment_portfolio_candidate_submissions as $edtpa_assessment_portfolio_candidate_submission) {

                                    if ($edtpa_assessment_portfolio_candidate_submission['Document']['doc_type'] == 2) {

                                        $document_url = $edtpa_assessment_portfolio_candidate_submission['Document']['url'];
                                        $document_url_path_info = pathinfo($document_url);

                                        $response_file = $client->doesObjectExist(Configure::read("bucket_name"), "uploads/" . $document_url_path_info['dirname'] . "/" . ($document_url_path_info['filename']) . "." . $document_url_path_info['extension']);
                                        if ($response_file) {

                                            $text .= '<espm:file sequence="' . $submission_counter . '">
                                                <espm:name>' . ($document_url_path_info['filename']) . "." . $document_url_path_info['extension'] . '</espm:name>
                                                <espm:sizeinbytes>0</espm:sizeinbytes>
                                            </espm:file>';
                                        }
                                    } else {

                                        $document_url_obj = $this->get_document_url($edtpa_assessment_portfolio_candidate_submission['Document']);

                                        if ($document_url_obj['encoder_status'] == 'Complete' || $document_url_obj['encoder_status'] == 'complete' || $document_url_obj['encoder_status'] == '') {

                                            $document_url = $document_url_obj['relative_url'];
                                            $document_url_path_info = pathinfo($document_url);

                                            $response_file = $client->doesObjectExist(Configure::read("bucket_name"), $document_url_obj['relative_url']);
                                            if ($response_file) {

                                                $text .= '<espm:file sequence="' . $submission_counter . '">
                                                    <espm:name>' . ($document_url_path_info['filename']) . "." . $document_url_path_info['extension'] . '</espm:name>
                                                    <espm:sizeinbytes>0</espm:sizeinbytes>
                                                </espm:file>';
                                            }
                                        }
                                    }


                                    $submission_counter += 1;
                                }
                            }


                            $text .= '</espm:portfolioNode>';
                        }

                        $text .= '</espm:portfolioNode>';
                    }

                    $text .= '</espm:assessment>';
                    $text .= '</espm:portfolioManifest>';

                    /*
                      <espm:assessment projectID="edTPA" assessmentID="PHYS_ED" assessmentVersion="3" >
                      <espm:portfolioNode code="TASK1">
                      <espm:portfolioNode code="TASK1_PARTA">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_information_1.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK1_PARTB">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_information_2.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK1_PARTC">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_information_3.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK1_PARTD">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_information_4.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK1_PARTE">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_information_5.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK2">
                      <espm:portfolioNode code="TASK2_PARTA">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_information_1.mov</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      <espm:file sequence="2">
                      <espm:name>elem_lit_context_for_learning_information_2.mov</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK2_PARTB">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_informationTASK2_PARTB_1.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK3">
                      <espm:portfolioNode code="TASK3_PARTA">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_informationTASK3_PARTA_1.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      <espm:file sequence="2">
                      <espm:name>elem_lit_context_for_learning_informationTASK3_PARTA_2.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      <espm:file sequence="3">
                      <espm:name>elem_lit_context_for_learning_informationTASK3_PARTA_3.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      <espm:file sequence="4">
                      <espm:name>elem_lit_context_for_learning_informationTASK3_PARTA_4.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      <espm:file sequence="5">
                      <espm:name>elem_lit_context_for_learning_informationTASK3_PARTA_5.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      <espm:file sequence="6">
                      <espm:name>elem_lit_context_for_learning_informationTASK3_PARTA_6.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK3_PARTB">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_informationTASK3_PARTB_1.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK3_PARTC">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_informationTASK3_PARTC_1.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      <espm:portfolioNode code="TASK3_PARTD">
                      <espm:file sequence="1">
                      <espm:name>elem_lit_context_for_learning_informationTASK3_PARTC_1.pdf</espm:name>
                      <espm:sizeinbytes>0</espm:sizeinbytes>
                      </espm:file>
                      </espm:portfolioNode>
                      </espm:portfolioNode>
                      </espm:assessment>

                     */


                    $url = "$edtpa_base_url" . "validatePortfolioManifest/";


                    $fields = array(
                        'partnerID' => 'Sibme',
                        'portfolioManifestXML' => $this->edtpa_encrypt($text, $edtpa_shared_key)
                    );


                    //url-ify the data for the POST
                    $fields_string = '';
                    foreach ($fields as $key => $value) {
                        $fields_string .= $key . '=' . $value . '&';
                    }
                    rtrim($fields_string, '&');

                    //open connection
                    $ch = curl_init();

                    //set the url, number of POST vars, POST data
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, count($fields));
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

                    //execute post
                    $result = curl_exec($ch);

                    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    //close connection
                    curl_close($ch);


                    if ($httpcode == 200) {

                        echo json_encode(array('success' => 'true', 'result' => $result));
                        die;
                    } elseif ($httpcode == 400) {

                        echo json_encode(array('success' => 'false', 'result' => json_decode($result), 'xml' => json_encode($text)));
                        die;
                    } else {

                        echo json_encode(array('success' => 'false', 'result' => 'This assessment has been submitted successfully.'));
                        die;
                    }
                } else {

                    echo json_encode(array('success' => 'false', 'result' => 'This assessment is already submitted!'));
                    die;
                }
            } else {

                echo json_encode(array('success' => 'false', 'result' => 'This assessment is not available in this account.'));
                die;
            }
        }
    }

    function edtpa_reserve_authorization_key($candidate_key, $assessment_id, $assessment_version) {

        $candidate_key = trim($candidate_key);
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
       
       // $edtpa_base_url = "https://partners.nesinc.com/ws/seam/resource/v1/Import/";
        $edtpa_base_url = "https://cert-ppp.eportfoliodev.com/ws/seam/resource/v1/Import/";
        $edtpa_provider_name = "Sibme";
        $edtpa_shared_secret = "z3ol8fvb739u";
        $edtpa_shared_key = "cfb1bb092c907fb8db23456b7c7a5411c4059796f582902e7cd34add57b9064a";
        $project_id = "edTPA";
        if (IS_PROD) {
            $edtpa_base_url = "https://ppp.eportfolio.pearson.com/ws/seam/resource/v1/Import/";
            $edtpa_provider_name = "Sibme";
            $edtpa_shared_secret = "4*fx1ENebUbf#w6q4nz3TBfRx";
            $edtpa_shared_key = "e557cc4002f5957da1f0e2ed29b4ef12c2497fa721e9ff24b9a225d06d3baf1c";
            $project_id = "edTPA";
        }

        $epoh_time = strtotime('now');
        $signature = base64_encode(hash_hmac('sha1', utf8_encode($edtpa_provider_name . $project_id . $assessment_id . $assessment_version .
                                $candidate_key . $user_id . $epoh_time), $edtpa_shared_secret, true));
        ;

        $url = "$edtpa_base_url" . "reserveAuthorizationKey";
        $fields = array(
            'partnerID' => urlencode($edtpa_provider_name),
            'projectID' => urlencode($project_id),
            'assessmentID' => urlencode($assessment_id),
            'assessmentVersion' => urlencode($assessment_version),
            'authorizationKey' => urlencode($candidate_key),
            'partnerCandidateID' => urlencode($user_id),
            'timestamp' => urlencode($epoh_time),
            'signature' => urlencode($signature)
        );


        //url-ify the data for the POST
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //execute post
        $result = curl_exec($ch);


        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // var_dump($result);
        // var_dump($httpcode);
        // die;
        //close connection
        curl_close($ch);

        if ($httpcode == 200) {

            //check if assessment is valid and in the Account

            $edtpa_assessment = $this->EdtpaAssessments->find('first', array(
                'conditions' => array(
                    'assessment_id' => $assessment_id,
                    'assessment_version' => $assessment_version,
                    'project_id' => $project_id
                )
            ));

            if (count($edtpa_assessment) == 0) {
                echo json_encode(array('success' => 'false'));
            } else {

                //found the assessment
                $edtpa_assessment_account = $this->EdtpaAssessmentAccounts->find('first', array(
                    'conditions' => array(
                        'edtpa_assessment_id' => $edtpa_assessment['EdtpaAssessments']['id'],
                        'account_id' => $account_id
                    )
                ));

                if (count($edtpa_assessment_account) > 0) {

                    $edtpa_assessment_account_candidate = $this->EdtpaAssessmentAccountCandidates->find('first', array(
                        'conditions' => array(
                            'edtpa_candidate_auth_key' => $candidate_key
                        )
                    ));


                    if (count($edtpa_assessment_account_candidate) > 0) {

                        if ($edtpa_assessment_account_candidate['EdtpaAssessmentAccountCandidates']['user_id'] != $user_id) {
                            echo json_encode(array('success' => 'false'));
                        } else {

                            echo json_encode(array('success' => 'true'));
                        }
                    } else {

                        //open for everyone

                        $edtpa_assessment_account_candidate_ex = $this->EdtpaAssessmentAccountCandidates->find('first', array(
                            'conditions' => array(
                                'edtpa_assessment_account_id' => $edtpa_assessment_account['EdtpaAssessmentAccounts']['id'],
                                'user_id' => $user_id,
                                'account_id' => $account_id
                            )
                        ));


                        if (count($edtpa_assessment_account_candidate_ex) > 0) {

                            //update
                            $this->EdtpaAssessmentAccountCandidates->create();
                            $data = array(
                                'edtpa_candidate_auth_key' => "'" . addslashes($candidate_key) . "'",
                                'auth_key_reg_at' => "'" . date("Y-m-d H:i:s") . "'",
                                'status' => '1'
                            );
                            $this->EdtpaAssessmentAccountCandidates->updateAll($data, array(
                                'edtpa_assessment_account_id' => $edtpa_assessment_account['EdtpaAssessmentAccounts']['id'],
                                'user_id' => $user_id,
                                'account_id' => $account_id,
                                    ), $validation = TRUE);
                        } else {

                            $this->EdtpaAssessmentAccountCandidates->create();
                            $data = array(
                                'edtpa_assessment_account_id' => $edtpa_assessment_account['EdtpaAssessmentAccounts']['id'],
                                'user_id' => $user_id,
                                'account_id' => $account_id,
                                'edtpa_candidate_auth_key' => $candidate_key,
                                'auth_key_reg_at' => date("Y-m-d H:i:s"),
                                'status' => '1'
                            );

                            $this->EdtpaAssessmentAccountCandidates->save($data, $validation = TRUE);
                        }


                        if (1 == 1) {
                            echo json_encode(array('success' => 'true'));
                        } else {
                            echo json_encode(array('success' => 'false'));
                        }
                    }
                } else {

                    echo json_encode(array('success' => 'false'));
                }
            }
        } else {

            echo json_encode(array('success' => 'false'));
        }

        die;
    }

    public function generate_url($id) {

        $this->layout = null;

        $file = $this->Document->findById($id);
        //$document_url = $file['Document']['url'];

        if ($file['Document']['doc_type'] == 1) {

            $document_files_array = $this->get_document_url($file['Document']);
            if (empty($document_files_array['url'])) {
                $file['Document']['published'] = 0;
                $document_files_array['url'] = $file['Document']['original_file_name'];
                $document_files_array['file_path'] = $file['Document']['url'];
                $file['Document']['encoder_status'] = $document_files_array['encoder_status'];
            } else {
                $file['Document']['encoder_status'] = $document_files_array['encoder_status'];
            }

            $document_url = $document_files_array['relative_url'];

            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $aws_bucket = Configure::read('bucket_name');
            $expires = '+20 minutes';

            // Create an Amazon S3 client object
            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            $result = $client->getObjectUrl(
                    $aws_bucket, $document_url, $expires, array(
                'ResponseContentType' => 'video/mp4',
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . $file['Document']['original_file_name']
                    )
            );
            $account_folder_id = '';
            $result_data = $this->AccountFolderDocument->get_account_folder_document($id);
            if($result_data){
                $account_folder_id = $result_data['AccountFolderDocument']['account_folder_id'];
            }
           

            $user_activity_logs = array(
                'ref_id' => $id,
                'desc' => 'Download Resource',
                'url' => $this->base . '/Huddles/download/' . $id,
                'type' => '13',
                'account_folder_id' =>  $account_folder_id,
                'environment_type' => '2',
            );
            $this->user_activity_logs($user_activity_logs);
            
            $users = $this->Session->read('user_current_account');
            $account_id = $users['accounts']['account_id'];
            $this->create_churnzero_event('Resource+Viewed', $account_id, $users['User']['email']);

            header('Location:' . $result);
            die;
        } else {

            // $document_url = "uploads/" . $file['Document']['url'];

            $document_files_array = $this->get_document_url($file['Document']);

            //$document_url = $document_files_array['relative_url'];
            $document_url = "uploads/" . $file['Document']['url'];

            $aws_access_key_id = Configure::read('access_key_id');
            $aws_secret_key = Configure::read('secret_access_key');
            $aws_bucket = Configure::read('bucket_name');
            $expires = '+20 minutes';

            // Create an Amazon S3 client object
            $client = S3Client::factory(array(
                        'key' => $aws_access_key_id,
                        'secret' => $aws_secret_key
            ));

            $result = $client->getObjectUrl(
                    $aws_bucket, $document_url, $expires, array(
                //'ResponseContentType' => 'video/mp4',
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . $file['Document']['original_file_name']
                    )
            );

            echo $result;
            die;
        }
    }

    public function update_scripted_observation_duration($video_id) {
        $data = array(
            'scripted_current_duration' => $this->request->data['duration'],
            'current_duration' => NULL,
        );
        $this->Document->updateAll($data, array(
            'id' => $video_id,
                ), $validation = TRUE);

        die;
    }

    function add_multiple_standard_ratings() {


        $standard_ids = $this->request->data['standard_ids'];
        $huddle_id = $this->request->data['huddle_id'];
        $video_id = $this->request->data['video_id'];
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];


        foreach ($standard_ids as $standard_id) {


            $rating_id = $this->request->data['rating_id_' . $standard_id][0];
            $rating_value_details = $this->AccountMetaData->find('first', array('conditions' => array('account_meta_data_id' => $rating_id)));


            $rating_value = $rating_value_details['AccountMetaData']['meta_data_value'];


            $this->Document->updateAll(array('not_observed' => 1), array('id' => $video_id));

            if (empty($rating_id) && empty($rating_value) || $rating_id == 0) {
                $this->DocumentStandardRating->deleteAll(array('account_folder_id' => $huddle_id, "account_id" => $account_id, 'document_id' => $video_id, 'standard_id' => $standard_id), $cascade = true, $callbacks = true);
            } else {


                $result = $this->DocumentStandardRating->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'document_id' => $video_id, 'account_id' => $account_id, 'standard_id' => $standard_id)));
                if (empty($result)) {
                    $this->DocumentStandardRating->create();

                    $data = array(
                        'document_id' => $video_id,
                        'account_folder_id' => $huddle_id,
                        'standard_id' => $standard_id,
                        'rating_id' => $rating_id,
                        'rating_value' => $rating_value,
                        'account_id' => $account_id,
                        'user_id' => $user_id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    );

                    $this->DocumentStandardRating->save($data);
                } else {
                    $this->DocumentStandardRating->updateAll(array('rating_id' => $rating_id, 'rating_value' => $rating_value, 'created_at' => "'" . date("Y-m-d H:i:s") . "'", 'updated_at' => "'" . date("Y-m-d H:i:s") . "'"), array('id' => $result['DocumentStandardRating']['id']));
                }
            }
        }

        $results = $this->DocumentStandardRating->find('all', array('conditions' => array('document_id' => $video_id, 'account_id' => $account_id, 'account_folder_id' => $huddle_id)));

        if (!empty($results)) {
            $avg = 0;
            foreach ($results as $result) {
                $avg = $avg + (int) $result['DocumentStandardRating']['rating_value'];
            }
            $view = new View($this, false);
            $rating_name = $view->Custom->get_rating_name($view->Custom->get_average_video_rating($video_id, $account_id, $huddle_id), $account_id);

            $data = array('rating_score' => round($avg / count($results)),
                'rating_name' => $rating_name
            );

            echo json_encode($data);
        } else {
            $data = array('rating_score' => 'N/O',
                'rating_name' => 'No Rating'
            );

            echo json_encode($data);
        }

        exit;
    }

    function get_folder_childs($folder_id) {
        $results = $this->AccountFolder->query("SELECT
         af.`account_folder_id`
          , af1.`account_folder_id`
           , af2.`account_folder_id`
           , af3.`account_folder_id`
            , af4.`account_folder_id`
             , af5.`account_folder_id`
             , af6.`account_folder_id`
        FROM
          `account_folders` af
          LEFT JOIN `account_folders` af1
            ON (af.`account_folder_id` = af1.`parent_folder_id` AND af1.`folder_type` = 5 AND af1.`active` =1  AND af.`account_id` = af1.`account_id`)
            LEFT JOIN `account_folders` af2
            ON (af1.`account_folder_id` = af2.`parent_folder_id`  AND af2.`folder_type` = 5 AND af2.`active` =1  AND af1.`account_id` = af2.`account_id` )
                LEFT JOIN `account_folders` af3
            ON (af2.`account_folder_id` = af3.`parent_folder_id` AND af3.`folder_type` = 5 AND af3.`active` =1  AND af2.`account_id` = af3.`account_id` )
                LEFT JOIN `account_folders` af4
            ON (af3.`account_folder_id` = af4.`parent_folder_id` AND af4.`folder_type` = 5 AND af4.`active` =1   AND af3.`account_id` = af4.`account_id`)
                LEFT JOIN `account_folders` af5
            ON (af4.`account_folder_id` = af5.`parent_folder_id` AND af5.`folder_type` = 5 AND af5.`active` =1  AND af4.`account_id` = af5.`account_id`)
                LEFT JOIN `account_folders` af6
            ON (af5.`account_folder_id` = af6.`parent_folder_id` AND af5.`folder_type` = 5 AND af5.`active` =1  AND af5.`account_id` = af6.`account_id`)

            WHERE af.`account_folder_id` = " . $folder_id . "
            AND af.`active` = 1
            AND af.site_id =" . $this->site_id . "
            AND af.`folder_type` = 5");


        $account_folder_ids = array();

        foreach ($results as $result) {

            if (isset($result['af']['account_folder_id']) && $result['af']['account_folder_id'] != '') {
                $account_folder_ids[$result['af']['account_folder_id']] = $result['af']['account_folder_id'];
            }
            if (isset($result['af1']['account_folder_id']) && $result['af1']['account_folder_id'] != '') {
                $account_folder_ids[$result['af1']['account_folder_id']] = $result['af1']['account_folder_id'];
            }
            if (isset($result['af2']['account_folder_id']) && $result['af2']['account_folder_id'] != '') {
                $account_folder_ids[$result['af2']['account_folder_id']] = $result['af2']['account_folder_id'];
            }
            if (isset($result['af3']['account_folder_id']) && $result['af3']['account_folder_id'] != '') {
                $account_folder_ids[$result['af3']['account_folder_id']] = $result['af3']['account_folder_id'];
            }
            if (isset($result['af4']['account_folder_id']) && $result['af4']['account_folder_id'] != '') {
                $account_folder_ids[$result['af4']['account_folder_id']] = $result['af4']['account_folder_id'];
            }
            if (isset($result['af5']['account_folder_id']) && $result['af5']['account_folder_id'] != '') {
                $account_folder_ids[$result['af5']['account_folder_id']] = $result['af5']['account_folder_id'];
            }
            if (isset($result['af6']['account_folder_id']) && $result['af6']['account_folder_id'] != '') {
                $account_folder_ids[$result['af6']['account_folder_id']] = $result['af6']['account_folder_id'];
            }
        }


        return $account_folder_ids;
    }

    public function survey_check() {
        $user_id = $this->request->data['user_id'];
        $data = array(
            'survey_check' => '1',
        );
        $this->User->updateAll($data, array(
            'id' => $user_id,
                ), $validation = TRUE);

        die;
    }

    function view_live($huddle_id, $tab = '1', $detail_id = '', $viewer_entry = '') {

        $user_current_account = $this->Session->read('user_current_account');
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle_id);


        $tab = (int) $tab;
        $detail_id = ($detail_id != '' && $detail_id != 'add' && $detail_id != '') ? (int) preg_replace('/[^0-9]/', '', $detail_id) : $detail_id;

        $live_stream_log = $this->LiveStreamLog->find('first', array('conditions' => array('document_id' => $detail_id)));

        $live_stream_log_detail = '';

        if (!empty($live_stream_log)) {
            $live_stream_log_detail = $this->LiveStreamLogDetail->find("first", array("conditions" => array(
                    "user_id" => $user_id,
                    "live_stream_log_id" => $live_stream_log['LiveStreamLog']['id']
            )));
        }

        if (empty($live_stream_log_detail) && !empty($live_stream_log)) {

            $this->LiveStreamLogDetail->create();
            $data = array(
                'live_stream_log_id' => $live_stream_log['LiveStreamLog']['id'],
                'user_id' => $user_id,
                'participant_type' => '2',
                'joined_at' => date("Y-m-d H:i:s"),
            );
            $this->LiveStreamLogDetail->save($data);
        }


        $huddle_info = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));

        if ($huddle_info['AccountFolder']['active'] == 0) {
            $this->Session->setFlash($this->language_based_messages['invalid_huddle'], 'default', array('class' => 'message error'));
            $this->redirect('/Huddles');
        }

        $this->Session->write('account_folder_id', $huddle_id);
        $huddle_permission = $this->Session->read('user_huddle_level_permissions');


        $view = new View($this, FALSE);
        $this->layout = 'huddles';
        $srtType = $this->Session->read('srtType');

        $changeType = $srtType != '' ? $srtType : 5;
        $export_pdf_url = Configure::read('sibme_base_url') . 'Huddles/print_pdf_comments/' . $detail_id . '/' . $changeType . '/2.pdf';
        $export_excel_url = Configure::read('sibme_base_url') . 'Huddles/print_excel_comments/' . $detail_id . '/' . $changeType;
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        if ($htype == 3) {
            $account_id = $user_current_account['accounts']['account_id'];
            if ($view->Custom->check_if_eval_huddle_active($account_id) == false) {
                $this->Session->setFlash($this->language_based_messages['you_dont_have_access_to_this_huddle'], 'default', array('class' => 'message error'));
                return $this->redirect('/Huddles');
            }
        }

        $comments_result = array();

        if (!empty($detail_id)) {
            $comments_result = $this->Comment->getVideoComments($detail_id, $changeType, '', '', '', 1);
        }
        $user_id = $user_current_account['User']['id'];
        $comments = array();
        foreach ($comments_result as $comment) {
            $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array('1', '2')); //get tags
            $view = new View($this, false);
            if ($htype == 3 || ($htype == 2 && $view->Custom->is_enabled_coach_feedback($huddle_id))) {    //coaching huddle feedback
                $view = new View($this, false);
                if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                    $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                } else {
                    if ($comment['Comment']['active'] == 1) {
                        $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                    }
                }
            } else {
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
            }
        }

        $comments_result = $comments;


        $p_huddle_users = $this->AccountFolder->getHuddleUsers($huddle_id);
        $p_huddle_group_users = $this->AccountFolder->getHuddleGroupsWithUsers($huddle_id);
        $has_access_to_huddle = false;

        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        //echo $framework_check;
        //echo $framework_id;
        $coachee_permission = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "coachee_permission")));
        $coachee_permissions = isset($coachee_permission[0]['AccountFolderMetaData']['meta_data_value']) ? $coachee_permission[0]['AccountFolderMetaData']['meta_data_value'] : "0";

        if (empty($detail_id) && isset($huddle)) {

            if (isset($account_id) && isset($user_id)) {

                $user_activity_logs = array(
                    'ref_id' => $huddle[0]['AccountFolder']['account_folder_id'],
                    'desc' => $huddle[0]['AccountFolder']['name'],
                    'url' => '/Huddles/view/' . $huddle[0]['AccountFolder']['account_folder_id'],
                    'type' => '12',
                    'account_id' => $account_id,
                    'account_folder_id' => $huddle[0]['AccountFolder']['account_folder_id'],
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }

        if (count($p_huddle_users) > 0) {

            for ($i = 0; $i < count($p_huddle_users); $i++) {
                $p_huddle_user = $p_huddle_users[$i];
                if ($p_huddle_user['huddle_users']['user_id'] == $user_id) {
                    $has_access_to_huddle = true;
                }
            }
        }
        if (count($p_huddle_group_users) > 0) {
            for ($i = 0; $i < count($p_huddle_group_users); $i++) {
                $p_huddle_group_user = $p_huddle_group_users[$i];
                if (isset($p_huddle_group_user['User']['id']) && $p_huddle_group_user['User']['id'] == $user_id)
                    $has_access_to_huddle = true;
            }
        }
        if (!$has_access_to_huddle) {
            $this->Session->setFlash($this->language_based_messages['you_dont_have_access_to_this_huddle'], 'default', array('class' => 'message success'));
            return $this->redirect('/Huddles');
        }


        $this->get_huddle_level_permissions($huddle_id);

        $videoDetail = $this->_videoLiveDetails($detail_id);
        if (empty($videoDetail)) {
            $this->Session->setFlash($this->language_based_messages["this_live_video_recording_doesnt_exist"], 'default', array('class' => 'message success'));
            return $this->redirect('/Huddles/view/' . $huddle_id);
        }
        $vidDocuments = $this->Document->getVideoDocumentsByVideo($detail_id, $huddle_id);
        $commentsTime = $this->Comment->getVideoComments($detail_id, $changeType, '', '', '', 1);


        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);

        $accountFolderIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFolderIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFolderIds = "'" . implode("','", $accountFolderIds) . "'";
        } else {
            $accountFolderIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $all_huddles = $this->AccountFolder->copyAllHuddles($account_id, 'name', FALSE, $accountFolderIds, $accountFolderGroupsIds);
        $this->set('tab', $tab);


        $view = new View($this, false);
        $evaluator = $view->Custom->check_if_evalutor($huddle_id, $user_id);

        if (!$evaluator && $htype == 3) {
            $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_id);
            array_push($evaluator_ids, $user_id);
            $totalVideos = $this->Document->countVideos($huddle_id, '', $evaluator_ids);
        } else {
            $totalVideos = $this->Document->countVideos($huddle_id);
        }

        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            //$framework_id =  $id_framework['AccountMetaData']['meta_data_value'];
            $framework_id = isset($id_framework['AccountMetaData']['meta_data_value']) ? $id_framework['AccountMetaData']['meta_data_value'] : "0";
        }
        $standardsL2 = array();
        $standards = array();
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }
        }

        if (!empty($detail_id)) {
            $view = new View($this, false);
            if ($htype == 3 || ($htype == 2 && $view->Custom->is_enabled_coach_feedback($huddle_id))) {      //coaching huddle feedback
                $view = new View($this, false);
                if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                    $total_incative_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $detail_id, 'active' => array('', '0'))));
                } else {
                    $total_incative_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $detail_id, 'user_id' => $user_id, 'active' => 1)));
                }
            } else {
                $total_incative_comments = 0;
            }

            $videoComments = '';
            if (is_array($comments_result) && count($comments_result) > 0) {
                $videoComments = $comments_result;
            } else {
                $videoComments = '';
            }
            $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
            $params_comments = array(
                'user_id' => $user_id,
                'video_id' => $detail_id,
                'videoComments' => $videoComments,
                'huddle_permission' => $huddle_permission,
                'huddle' => $huddle,
                'user_current_account' => $user_current_account,
                'sortType' => $changeType,
                'export_pdf_url' => $export_pdf_url,
                'export_excel_url' => $export_excel_url,
                'type' => 'get_video_comments',
                'ratings' => $metric_old,
                'huddle_type' => $htype,
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $detail_id),
                'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                        "account_id" => $account_id,
                        "tag_type" => '1'
            ))),
                'standards' => $standards,
                'standardsL2' => $standardsL2
            );

            $view = new View($this, false);
            $html_comments = $view->element('ajax/vidComments', $params_comments);
        }
        $item = array();


        $evaluator = $view->Custom->check_if_evalutor($huddle_id, $user_id);

        if (!$evaluator && $htype == 3) {
            $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_id);
            array_push($evaluator_ids, $user_id);
            $all_videos = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page, 0, $evaluator_ids);
        } else {
            $all_videos = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page);
        }


        if ($all_videos) {
            foreach ($all_videos as $row) {
                $created_by = $row['Document']['created_by'];
                if ($this->_check_evaluator_permissions($huddle_id, $created_by) == false) {
                    continue;
                } else {
                    $item[] = $row;
                }
            }
        }
        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }
        if (!empty($videoDetail)) {
            $videoDetail['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $videoDetail['Document']['id'])));
        }


        if ($htype == 3 && $detail_id != '') {
            $isEditable = $this->_check_evaluator_permissions($huddle_id, $videoDetail['Document']['created_by']);
            if ($isEditable == false) {
                $this->Session->setFlash($this->language_based_messages['you_dont_have_access_to_this_video'], 'default', array('class' => 'message error'));
                return $this->redirect('/Huddles/view/' . $huddle_id . '/' . $tab);
            }

            if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                if ($videoDetail['Document']['created_by'] != $user_id) {
                    $coaches = $this->AccountFolderUser->getUsernameEvaluator($huddle_id, $user_id);
                    $mente = $this->AccountFolderUser->getParticipants($huddle_id);
                    $all_meente = array();
                    foreach ($mente as $row) {
                        if ($videoDetail['Document']['created_by'] == $row['users']['id']) {
                            $participant = $row[0]['user_name'];
                        }
                    }
                    $mente = $participant;
                }
            } else {
                $coaches = $this->AccountFolderUser->getUserNameCoaches($huddle_id);
                $mente = $this->AccountFolderUser->getParticipants($huddle_id);
                $participant = '';
                foreach ($mente as $row) {
                    if ($user_id == $row['users']['id']) {
                        $participant = $row[0]['user_name'];
                    }
                }
                $mente = $participant;
            }
        } else {
            $coaches = $this->AccountFolderUser->getUserNameCoaches($huddle_id);
            $mente = $this->AccountFolderUser->getUserNameMente($huddle_id);
        }


        //Set Video Tabs Data.
        $videosData = array(
            'huddle' => $this->AccountFolder->getHuddle($huddle_id, $account_id),
            'huddle_users' => $this->AccountFolder->getHuddleUsers($huddle_id),
            'all_huddle' => $all_huddles,
            'videos' => $item,
            'coachee_permissions' => $coachee_permissions,
            'totalVideos' => $totalVideos,
            'video_per_page' => $this->video_per_page,
            'current_page' => 1,
            'videoDetail' => $videoDetail,
            'vidDocuments' => $vidDocuments,
            'videoComments' => $commentsTime,
            'videoCommentsArray' => (!isset($videoComments) ? array() : $videoComments),
            'huddle_id' => $huddle_id,
            'user_id' => $user_id,
            'tab' => $tab,
            'type' => 'get_video_comments',
            'huddle_des' => $this->AccountFolder->get($huddle_id),
            'show_ff_message' => false,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'video_id' => $detail_id,
            'html_comments' => (!isset($html_comments) ? "" : $html_comments),
            'coaches' => $coaches,
            'mente' => $mente,
            'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $detail_id),
            'ratings' => $metric_old,
            'standards' => $standards,
            'standardsL2' => $standardsL2
        );


        $isFirefoxBrowser = $view->Browser->isFirefox();
        if ($isFirefoxBrowser) {
            $trim_message_displayed = $this->Cookie->read('flash_message_ff_trim');

            if (!isset($trim_message_displayed) || empty($trim_message_displayed)) {
                echo $trim_message_displayed;
                $videosData['show_ff_message'] = true;
                $this->Cookie->write('flash_message_ff_trim', "displayed", false, '2 weeks');
            }
        }

        //$ob_item = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page, '', '', 3);
        $ob_item = $this->Document->getObVideos($huddle_id, '', 'Document.created_date DESC', 10, '', '', 3);
        if (count($ob_item) > 0) {
            for ($j = 0; $j < count($ob_item); $j++) {
                $ob_item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $ob_item[$j]['Document']['id'])));
            }
        }
        $totalObVideos = $this->Document->countVideos($huddle_id, '', '', 3);
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $isFirefoxBrowser = $view->Browser->isFirefox();
        if ($isFirefoxBrowser) {
            $trim_message_displayed = $this->Cookie->read('flash_message_ff_trim');

            if (!isset($trim_message_displayed) || empty($trim_message_displayed)) {
                echo $trim_message_displayed;
                $obser_data['show_ff_message'] = true;
                $this->Cookie->write('flash_message_ff_trim', "displayed", false, '2 weeks');
            }
        }

        $matric_enable = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'enable_matric')));
        $enable_matric = isset($matric_enable['AccountMetaData']['meta_data_value']) ? $matric_enable['AccountMetaData']['meta_data_value'] : "0";

        $tracker_enable = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'enable_tracker')));
        $enable_tracker = isset($tracker_enable['AccountMetaData']['meta_data_value']) ? $tracker_enable['AccountMetaData']['meta_data_value'] : "0";


        $user_huddle_level_permissions = $this->Session->read('user_huddle_level_permissions');
        $this->set('user_huddle_level_permissions', $user_huddle_level_permissions);
        $this->set('huddle_id', $huddle_id);
        $this->set('detail_id', $detail_id);
        $this->set('account_id', $account_id);
        $videosData['huddle_type'] = $htype;
        $this->set('enable_matric', $enable_matric);
        $this->set('enable_tracker', $enable_tracker);
        $this->set('video_data', $videosData);
        $this->set('videoCommentsArray', (!isset($videoComments) ? array() : $videoComments));
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $videosData['auto_scroll_switch'] = $type_pause['UserAccount']['autoscroll_switch'];
        $videosData['type_pause'] = $type_pause['UserAccount']['type_pause'];
        $videosData['press_enter_to_send'] = $type_pause['UserAccount']['press_enter_to_send'];
        $videosData["present_account_id"] = $account_id;
        $videosData["all_accounts"] = $this->Session->read('user_accounts');

        $total_commnets_count = 0;
        if (!empty($detail_id)) {

            $comments_result_for_count = $this->Comment->getVideoComments($detail_id, $changeType, '', '', '', 1, 1, true);

            $total_comments = 0;
            if (count($videoComments) > 0) {
                $total_comments = count($videoComments);
                foreach ($videoComments as $cmnt) {

                    $total_comments = $total_comments + $this->get_total_replies($cmnt['Comment']['id']);
                }
            }


//            for ($i = 0; $i < count($comments_result_for_count); $i++) {
//
//                if (isset($comments_result_for_count[$i]['Comment'])) {
//                    $all_replies = array();
//                    $item_comment_reply = $this->Comment->commentReplys($comments_result_for_count[$i]['Comment']['id']);
//                    if (isset($item_comment_reply) && count($item_comment_reply) > 0) {
//                        array_push($comments_result_for_count_loop, $item_comment_reply);
//                        $total_commnets_count +=1;
//                    }
//                    $total_commnets_count +=1;
//                }
//            }
        }
        // $videosData["comments_count"] = $total_commnets_count;
        $videosData["comments_count"] = $total_comments;

        $this->set('total_incative_comments', (!isset($total_incative_comments) ? 0 : $total_incative_comments));
        $this->set('videosTab', $view->element('huddles/tabs/videos_live', $videosData));

        /*         * **     Recent Activities     **** */


        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }

        $account_id = $user_current_account['accounts']['account_id'];
        $user_id = $user_current_account['User']['id'];

        $UserActivityLogs_model = $this->UserActivityLog->get_huddle_activity($user_current_account['accounts']['account_id'], $huddle_id);
        $myHuddles = $this->AccountFolder->getAllHuddlesDashboard($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);

        $this->set('UserActivityLogs', $UserActivityLogs_model);
        $this->set('user_current_account', $user_current_account);
        $this->set('user_id', $user_id);
        $this->set('account_id', $account_id);
        $this->set('myHuddles', $myHuddles);

        /*         * **     End of Recent Activities     **** */

        $latest_video_created_date = $this->UserAccount->query("SELECT
              d.`created_date`
            FROM
              `account_folders` AS af
              #join `account_folder_users` as afu
                #on af.`account_folder_id` = afu.`account_folder_id`
              JOIN `account_folder_documents` AS afd
                ON afd.`account_folder_id` = af.`account_folder_id`
              JOIN `documents` AS d
                ON d.`id` = afd.`document_id`
            WHERE d.`doc_type` IN (1, 3)
            AND af.`folder_type` = 1
              AND af.`account_id` = " . $account_id . "
              AND af.`account_folder_id`= " . $huddle_id . "
              AND af.site_id =" . $this->site_id . "
            ORDER BY d.`id` DESC
            LIMIT 1 ");
        $latest_video_created_date = $latest_video_created_date[0]['d']['created_date'];
        $this->set('latest_video_created_date', $latest_video_created_date);
        $this->set('standards', $standards);
        $this->set('standardsL2', $standardsL2);
        $this->set('ratings', $metric_old);
        $this->set('video_id', $detail_id);


        if (!empty($videoDetail)) {
            $this->set('isVideoPage', '1');
        } else {
            $this->set('isVideoPage', '0');
        }
    }

    function live_video_append() {
        $result = array();
        $user = $this->Session->read('user_current_account');
        $document_in_review = implode(",", json_decode($_POST['observation_inreview']));
        $huddle_id = $_POST['huddle_id'];
        if (isset($user)) {
            $account_id = $user['accounts']['account_id'];
            $user_id = $user['User']['id'];
            $live_observations = $this->Document->getHuddleLiveVideos($user_id, $huddle_id);
            if (count($live_observations) > 0) {
                for ($i = 0; $i < count($live_observations); $i++) {
                    $live_observation = $live_observations[$i];
                    $live_observation['doc']['created_date'] = date('M d, Y', strtotime($live_observation['doc']['created_date']));
                    $live_observations[$i] = $live_observation;
                }
                $result['live_observations'] = $live_observations;
            }
            if (!empty($document_in_review)) {
                $live_observations = $this->Document->getHuddleLiveVideosInReview($user_id, $document_in_review);
                if (count($live_observations) > 0) {
                    for ($i = 0; $i < count($live_observations); $i++) {
                        $live_observation = $live_observations[$i];
                        $live_observation['doc']['created_date'] = date('M d, Y', strtotime($live_observation['doc']['created_date']));
                        $live_observations[$i] = $live_observation;
                    }
                    if (isset($result['live_observations']))
                        $result['live_observations'] = array_merge($result['live_observations'], $live_observations);
                    else
                        $result['live_observations'] = $live_observations;
                }
            }
        }

        echo json_encode($result);
        die;
    }

    function check_if_video_saved_or_not() {

        $video_id = $this->request->data['videoID'];
        $huddle_id = $this->request->data['huddleID'];

        $video_details = $this->Document->find('first', array(
            'conditions' => array(
                'id' => $video_id
            ))
        );

        if ($video_details['Document']['published'] == '1' && $video_details['Document']['is_processed'] == '4' && $video_details['Document']['video_is_saved'] == '1') {
            $output = array(
                'success' => '1',
                'message' => 'Video is stopped and saved to huddle'
            );
            echo json_encode($output);
            die;
        } elseif ($video_details['Document']['is_processed'] == '4' && $video_details['Document']['video_is_saved'] == '0') {
            $output = array(
                'success' => '2',
                'message' => 'Video is stopped and not saved to huddle'
            );
            echo json_encode($output);
            die;
        } elseif ($video_details['Document']['doc_type'] == '4' && ($video_details['Document']['is_processed'] == '6' || $video_details['Document']['is_processed'] == '5')) {
            $output = array(
                'success' => '3',
                'message' => 'Video is not stopped'
            );
            echo json_encode($output);
            die;
        } else {
            $output = array(
                'success' => '4',
                'message' => 'Video does not exist'
            );
            echo json_encode($output);
            die;
        }
    }

    function test($account_id, $tab, $framework_id = 0) {
        if ($this->request->is('post') && $tab == 1) {
            $user = $this->Session->read('user_current_account');
            $user_id = $user['User']['id'];
            $standards_data = array();
            $data = $this->request->data;
            if (count($data) > 0) {
                foreach ($data['prefix'] as $keys => $row) {
                    $contents = $data['contents'][$keys];
                    $standard_level = $data['standard_level'][$keys];
                    foreach ($row as $key => $rows) {
                        $standards_data[$keys][] = array(
                            'account_id' => $account_id,
                            'parent_account_tag_id' => '',
                            'tag_type' => 0,
                            'tag_code' => $rows,
                            'tag_title' => $contents[$key],
                            'tag_html' => $contents[$key],
                            'created_by' => $user_id,
                            'created_date' => date('Y-m-d H:i:s'),
                            'last_edit_by' => $user_id,
                            'last_edit_date' => date('Y-m-d H:i:s'),
                            'tads_code' => NULL,
                            'framework_id' => $framework_id,
                            'standard_level' => $standard_level[$key]
                        );
                    }
                }
                if (count($standards_data) > 0) {
                    for ($count = 0; $count <= count($standards_data); $count++) {
                        $parent_account_tag_id = '';
                        foreach ($standards_data[$count] as $key => $rows) {
                            if ($key == 0) {
                                $standard_level = $rows['standard_level'];
                                $this->AccountTag->create();
                                unset($rows['standard_level']);
                                $this->AccountTag->save($rows);
                                $parent_account_tag_id = $this->AccountTag->id;
                                $data_hrky = array(
                                    'framework_hierarchy' => '"' . $parent_account_tag_id . '/' . $standard_level . '"'
                                );
                                $this->AccountTag->updateAll($data_hrky, array('account_tag_id' => $parent_account_tag_id));
                            } else {
                                $standard_level = $rows['standard_level'];
                                $this->AccountTag->create();
                                $rows['parent_account_tag_id'] = $parent_account_tag_id;
                                unset($rows['standard_level']);
                                $this->AccountTag->save($rows);
                                $account_tag_id = $this->AccountTag->id;
                                $data_hrky = array(
                                    'framework_hierarchy' => '"' . $parent_account_tag_id . '/' . $account_tag_id . '/' . $standard_level . '"'
                                );
                                $this->AccountTag->updateAll($data_hrky, array('account_tag_id' => $account_tag_id));
                            }
                        }
                    }
                }
                die;
            }
        }
    }

    function video_details($huddle_id, $detail_id, $user_id, $course_id) {
        $tab = 1;
        $users = $this->User->get($user_id);
        $user_current_account = $users[0];
        $user = $users[0];
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $huddle_id = (int) preg_replace('/[^0-9]/', '', $huddle_id);

        $tab = (int) $tab;
        $detail_id = ($detail_id != '' && $detail_id != 'add' && $detail_id != '') ? (int) preg_replace('/[^0-9]/', '', $detail_id) : $detail_id;

        $huddle_info = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_id)));

        if ($huddle_info['AccountFolder']['parent_folder_id'] != '0' && !empty($huddle_info['AccountFolder']['parent_folder_id'])) {
            $folder_details = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $huddle_info['AccountFolder']['parent_folder_id'])));
            $this->set('parent_folder_name', $folder_details);
            $folder_label = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $folder_details['AccountFolder']['account_folder_id']), 'fields' => array('name')));

            $crumb_output = $this->add_new_folder_breadcrum($folder_label['AccountFolder']['name'] . '_0f', $folder_details['AccountFolder']['account_folder_id']);

            $this->set("breadcrumb", $crumb_output);
        }
        $this->set('current_huddle_info', $huddle_info);

        if ($huddle_info['AccountFolder']['active'] == 0) {

        }

        //$this->Session->write('account_folder_id', $huddle_id);
        $huddle_permission = '';


        $view = new View($this, FALSE);
        $this->layout = 'huddles';
        $srtType = $this->Session->read('srtType');

        $changeType = $srtType != '' ? $srtType : 5;
        $export_pdf_url = Configure::read('sibme_base_url') . 'Huddles/print_pdf_comments/' . $detail_id . '/' . $changeType . '/2.pdf';
        $export_excel_url = Configure::read('sibme_base_url') . 'Huddles/print_excel_comments/' . $detail_id . '/' . $changeType;
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        if ($htype == 3) {
            $account_id = $user_current_account['accounts']['account_id'];

            if ($view->Custom->check_if_eval_huddle_active($account_id) == false) {

            }
        }

        $comments_result = array();

        if (!empty($detail_id)) {
            $comments_result = $this->Comment->getVideoComments($detail_id, $changeType, '', '', '', 1);
        }
        $user_id = $user_current_account['User']['id'];
        $comments = array();
        foreach ($comments_result as $comment) {
            $get_standard = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], '0'); //get standards
            $get_tags = $this->AccountCommentTag->gettagsbycommentid($comment['Comment']['id'], array('1', '2')); //get tags
            $view = new View($this, false);
            if ($htype == 3 || ($htype == 2 && $view->Custom->is_enabled_coach_feedback($huddle_id))) {    //coaching huddle feedback
                $view = new View($this, false);
                if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                    $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                } else {
                    if ($comment['Comment']['active'] == 1) {
                        $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                    }
                }
            } else {
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
            }
        }

        $comments_result = $comments;


        $p_huddle_users = $this->AccountFolder->getHuddleUsers($huddle_id);
        $p_huddle_group_users = $this->AccountFolder->getHuddleGroupsWithUsers($huddle_id);
        $has_access_to_huddle = false;

        $huddle = $this->AccountFolder->getHuddle($huddle_id, $account_id);
        $framework_id = '';
        $framework_check = '';

        $c_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_frameworks')));
        $framework_check = isset($c_framework['AccountFolderMetaData']['meta_data_value']) ? $c_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        //echo $framework_check;
        //echo $framework_id;
        $coachee_permission = $this->AccountFolderMetaData->find('all', array('conditions' => array('account_folder_id' => $huddle_id, "meta_data_name" => "coachee_permission")));
        $coachee_permissions = isset($coachee_permission[0]['AccountFolderMetaData']['meta_data_value']) ? $coachee_permission[0]['AccountFolderMetaData']['meta_data_value'] : "0";

        if (empty($detail_id) && isset($huddle)) {

            if (isset($account_id) && isset($user_id)) {

                $user_activity_logs = array(
                    'ref_id' => $huddle[0]['AccountFolder']['account_folder_id'],
                    'desc' => $huddle[0]['AccountFolder']['name'],
                    'url' => '/Huddles/view/' . $huddle[0]['AccountFolder']['account_folder_id'],
                    'type' => '12',
                    'account_id' => $account_id,
                    'account_folder_id' => $huddle[0]['AccountFolder']['account_folder_id'],
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }

        if (count($p_huddle_users) > 0) {

            for ($i = 0; $i < count($p_huddle_users); $i++) {
                $p_huddle_user = $p_huddle_users[$i];
                if ($p_huddle_user['huddle_users']['user_id'] == $user_id) {
                    $has_access_to_huddle = true;
                }
            }
        }
        if (count($p_huddle_group_users) > 0) {
            for ($i = 0; $i < count($p_huddle_group_users); $i++) {
                $p_huddle_group_user = $p_huddle_group_users[$i];
                if (isset($p_huddle_group_user['User']['id']) && $p_huddle_group_user['User']['id'] == $user_id)
                    $has_access_to_huddle = true;
            }
        }
        if (!$has_access_to_huddle) {

        }

        $this->get_huddle_level_permissions($huddle_id);

        if ($detail_id != '' && ($tab == '1' || $tab == '5')) {
            if ($tab == '5') {
                $document_detail = $this->AccountFolderDocument->ob_get_doc_details($detail_id);
                if (count($document_detail) > 0) {
                    $document_id = $document_detail['AccountFolderDocument']['document_id'];
                    $videoDetail2 = $this->_videoDetails($document_id);
                    $videoDetail = '';
                    $vidDocuments = '';
                    $commentsTime = '';
                } else {
                    $videoDetail2 = '';
                    $commentsTime = '';
                    $videoDetail = '';
                    $vidDocuments = '';
                }
            } else {

                $videoDetail = $this->_videoDetails($detail_id);


                $vidDocuments = $this->Document->getVideoDocumentsByVideo($detail_id, $huddle_id);
                $commentsTime = $this->Comment->getVideoComments($detail_id, $changeType, '', '', '', 1);
            }
        } else {
            $commentsTime = '';
            $videoDetail = '';
            $vidDocuments = '';
        }

        $addDiscussionsData = array(
            'adminUsers' => $this->User->getUsersByRole($account_id, array('110', '100'), $user_id),
            'otherUsers' => $this->User->getUsersByRole($account_id, '120', $user_id),
            'huddle_id' => $huddle_id,
            'huddle' => $this->AccountFolder->getHuddle($huddle_id, $account_id),
            'user_id' => $user_id,
            'detail_id' => $detail_id,
            'huddles_users' => $this->AccountFolder->getHuddleUsers($huddle_id),
            'videoHuddleGroups' => $this->AccountFolder->getHuddleGroupsWithUsers($huddle_id),
            'discussionReply' => '',
            'tab' => 'add'
        );



        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);

        $accountFolderIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFolderIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFolderIds = "'" . implode("','", $accountFolderIds) . "'";
        } else {
            $accountFolderIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $all_huddles = $this->AccountFolder->copyAllHuddles($account_id, 'name', FALSE, $accountFolderIds, $accountFolderGroupsIds);
        $this->set('tab', $tab);


        $view = new View($this, false);
        $evaluator = $view->Custom->check_if_evalutor($huddle_id, $user_id);

        if (!$evaluator && $htype == 3) {
            $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_id);
            array_push($evaluator_ids, $user_id);
            $totalVideos = $this->Document->countVideos($huddle_id, '', $evaluator_ids);
        } else {
            $totalVideos = $this->Document->countVideos($huddle_id);
        }

//Start framework
        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            //$framework_id =  $id_framework['AccountMetaData']['meta_data_value'];
            $framework_id = isset($id_framework['AccountMetaData']['meta_data_value']) ? $id_framework['AccountMetaData']['meta_data_value'] : "0";
        }
        $standards_new = $this->get_framework_settings($framework_id);

        $standardsL2 = array();
        $standards = array();
        if ($framework_id == 0 && $framework_check == 1) {
            $id_framework = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
            $fw_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "";
            if (!empty($fw_id)) {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));

                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                    )));
                }
            } else {
                $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                )));
                $standardsL2 = array();
                if ($standards_l2 > 0) {
                    $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                } else {
                    $standards = $this->AccountTag->find("all", array("conditions" => array(
                            "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL" // -> framework_id IS NULL change added
                    )));
                }
            }
        } elseif ($framework_id > 0 && $framework_check == 1) {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id" => $framework_id
            )));
            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id" => $framework_id
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id, "parent_account_tag_id IS NOT NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id" => $framework_id
                )));
            }
        } else {
            $standards_l2 = $this->AccountTag->find("count", array("conditions" => array(
                    "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
            )));

            if ($standards_l2 > 0) {
                $standardsL2 = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NULL", "framework_id IS NULL"
                )));
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "parent_account_tag_id IS NOT NULL", "framework_id IS NULL"
                )));
            } else {
                $standards = $this->AccountTag->find("all", array("conditions" => array(
                        "tag_type" => '0', "account_id" => $account_id, "framework_id IS NULL"
                )));
            }

            $standards = $this->get_framework_settings($framework_id);
        }

        //End framework

        if (!empty($detail_id)) {
            $view = new View($this, false);
            if ($htype == 3 || ($htype == 2 && $view->Custom->is_enabled_coach_feedback($huddle_id))) {      //coaching huddle feedback
                $view = new View($this, false);
                if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                    $total_incative_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $detail_id, 'active' => array('', '0'))));
                } else {
                    $total_incative_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $detail_id, 'user_id' => $user_id, 'active' => 1)));
                }
            } else {
                $total_incative_comments = 0;
            }

            $videoComments = '';
            if (is_array($comments_result) && count($comments_result) > 0) {
                $videoComments = $comments_result;
            } else {
                $videoComments = '';
            }
            $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
            $params_comments = array(
                'user_id' => $user_id,
                'video_id' => $detail_id,
                'videoComments' => $videoComments,
                'huddle_permission' => $huddle_permission,
                'huddle' => $huddle,
                'user_current_account' => $user_current_account,
                'sortType' => $changeType,
                'export_pdf_url' => $export_pdf_url,
                'export_excel_url' => $export_excel_url,
                'type' => 'get_video_comments',
                'ratings' => $metric_old,
                'huddle_type' => $htype,
                'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $detail_id),
                'defaulttags' => $this->AccountTag->find("all", array("conditions" => array(
                        "account_id" => $account_id,
                        "tag_type" => '1'
            ))),
                'standards' => $standards,
                'standardsL2' => $standardsL2,
                'framework_data' => $standards_new
            );

            $view = new View($this, false);
            $html_comments = $view->element('ajax/vidCommentsFrame', $params_comments);
        }
        $item = array();


        $evaluator = $view->Custom->check_if_evalutor($huddle_id, $user_id);

        if (!$evaluator && $htype == 3) {
            $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_id);
            array_push($evaluator_ids, $user_id);
            $all_videos = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page, 0, $evaluator_ids);
        } else {
            $all_videos = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page);
        }


        if ($all_videos) {
            foreach ($all_videos as $row) {
                $created_by = $row['Document']['created_by'];
                if ($this->_check_evaluator_permissions($huddle_id, $created_by) == false) {
                    continue;
                } else {
                    $item[] = $row;
                }
            }
        }
        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $item[$j]['Document']['id'])));
            }
        }
        if (!empty($videoDetail)) {
            $videoDetail['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $videoDetail['Document']['id'])));
        }


        if ($htype == 3 && $detail_id != '') {
            $isEditable = $this->_check_evaluator_permissions($huddle_id, $videoDetail['Document']['created_by']);
            if ($view->Custom->check_if_evalutor($huddle_id, $user_id)) {
                if ($videoDetail['Document']['created_by'] != $user_id) {
                    $coaches = $this->AccountFolderUser->getUsernameEvaluator($huddle_id, $user_id);
                    $mente = $this->AccountFolderUser->getParticipants($huddle_id);
                    $all_meente = array();
                    foreach ($mente as $row) {
                        if ($videoDetail['Document']['created_by'] == $row['users']['id']) {
                            $participant = $row[0]['user_name'];
                        }
                    }
                    $mente = $participant;
                }
            } else {
                $coaches = $this->AccountFolderUser->getUserNameCoaches($huddle_id);
                $mente = $this->AccountFolderUser->getParticipants($huddle_id);
                $participant = '';
                foreach ($mente as $row) {
                    if ($user_id == $row['users']['id']) {
                        $participant = $row[0]['user_name'];
                    }
                }
                $mente = $participant;
            }
        } else {
            $coaches = $this->AccountFolderUser->getUserNameCoaches($huddle_id);
            $mente = $this->AccountFolderUser->getUserNameMente($huddle_id);
        }


        $created_by = $videoDetail['Document']['created_by'];

        $user_data = $this->User->find('first', array(
            'conditions' => array(
                'id' => $created_by
            )
        ));

        //Set Video Tabs Data.
        $videosData = array(
            'huddle' => $this->AccountFolder->getHuddle($huddle_id, $account_id),
            'huddle_users' => $this->AccountFolder->getHuddleUsers($huddle_id),
            'all_huddle' => $all_huddles,
            'videos' => $item,
            'coachee_permissions' => $coachee_permissions,
            'totalVideos' => $totalVideos,
            'video_per_page' => $this->video_per_page,
            'current_page' => 1,
            'created_by' => $user_data,
            'videoDetail' => $videoDetail,
            'vidDocuments' => $vidDocuments,
            'videoComments' => $commentsTime,
            'videoCommentsArray' => (!isset($videoComments) ? array() : $videoComments),
            'huddle_id' => $huddle_id,
            'user_id' => $user_id,
            'tab' => $tab,
            'type' => 'get_video_comments',
            'huddle_des' => $this->AccountFolder->get($huddle_id),
            'show_ff_message' => false,
            'export_pdf_url' => $export_pdf_url,
            'export_excel_url' => $export_excel_url,
            'video_id' => $detail_id,
            'html_comments' => (!isset($html_comments) ? "" : $html_comments),
            'coaches' => $coaches,
            'mente' => $mente,
            'tags' => $this->AccountTag->gettagswithcount($account_id, '1', $detail_id),
            'ratings' => $metric_old,
            'standards' => $standards,
            'standardsL2' => $standardsL2,
            'framework_data' => $standards_new
        );

        if (!empty($detail_id)) {
            $this->set('is_video_page', '1');
            $this->set('video_total_info', $videoDetail);
        }


        $isFirefoxBrowser = $view->Browser->isFirefox();
        if ($isFirefoxBrowser) {
            $trim_message_displayed = $this->Cookie->read('flash_message_ff_trim');

            if (!isset($trim_message_displayed) || empty($trim_message_displayed)) {
                echo $trim_message_displayed;
                $videosData['show_ff_message'] = true;
                $this->Cookie->write('flash_message_ff_trim', "displayed", false, '2 weeks');
            }
        }

        //Set Documents Tab data.
        $documentData = array(
            'huddle_id' => $huddle_id,
            'huddle' => $huddle_info,
            'tab' => $tab,
        );

        //Set People Tab Data
        //$ob_item = $this->Document->getVideos($huddle_id, '', '', $this->video_per_page, '', '', 3);
        $ob_item = $this->Document->getObVideos($huddle_id, '', 'Document.created_date DESC', 10, '', '', 3);
        if (count($ob_item) > 0) {
            for ($j = 0; $j < count($ob_item); $j++) {
                $ob_item[$j]['Document']['total_comments'] = $this->Comment->find('count', array('conditions' => array('ref_id' => $ob_item[$j]['Document']['id'])));
            }
        }
        $totalObVideos = $this->Document->countVideos($huddle_id, '', '', 3);
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));

        $isFirefoxBrowser = $view->Browser->isFirefox();
        if ($isFirefoxBrowser) {
            $trim_message_displayed = $this->Cookie->read('flash_message_ff_trim');

            if (!isset($trim_message_displayed) || empty($trim_message_displayed)) {
                echo $trim_message_displayed;
                $obser_data['show_ff_message'] = true;
                $this->Cookie->write('flash_message_ff_trim', "displayed", false, '2 weeks');
            }
        }

        $matric_enable = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'enable_matric')));
        $enable_matric = isset($matric_enable['AccountMetaData']['meta_data_value']) ? $matric_enable['AccountMetaData']['meta_data_value'] : "0";

        $tracker_enable = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'enable_tracker')));
        $enable_tracker = isset($tracker_enable['AccountMetaData']['meta_data_value']) ? $tracker_enable['AccountMetaData']['meta_data_value'] : "0";


        $user_huddle_level_permissions = $this->Session->read('user_huddle_level_permissions');
        $this->set('user_huddle_level_permissions', $user_huddle_level_permissions);
        $this->set('huddle_id', $huddle_id);
        $this->set('detail_id', $detail_id);
        $this->set('account_id', $account_id);
        $videosData['huddle_type'] = $htype;
        $this->set('enable_matric', $enable_matric);
        $this->set('enable_tracker', $enable_tracker);
        $this->set('video_data', $videosData);
        $this->set('videoCommentsArray', (!isset($videoComments) ? array() : $videoComments));
        $type_pause = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $videosData['auto_scroll_switch'] = $type_pause['UserAccount']['autoscroll_switch'];
        $videosData['type_pause'] = $type_pause['UserAccount']['type_pause'];
        $videosData['press_enter_to_send'] = $type_pause['UserAccount']['press_enter_to_send'];
        $videosData["present_account_id"] = $account_id;
        $videosData["all_accounts"] = $this->Session->read('user_accounts');

        $total_commnets_count = 0;
        if (!empty($detail_id)) {

            $comments_result_for_count = $this->Comment->getVideoComments($detail_id, $changeType, '', '', '', 1, 1, true);

            $total_comments = 0;
            if (count($videoComments) > 0) {
                $total_comments = count($videoComments);
                foreach ($videoComments as $cmnt) {

                    $total_comments = $total_comments + $this->get_total_replies($cmnt['Comment']['id']);
                }
            }
        }
        // $videosData["comments_count"] = $total_commnets_count;
        $videosData["comments_count"] = $total_comments;
        $this->set('total_incative_comments', (!isset($total_incative_comments) ? 0 : $total_incative_comments));
        $this->set('videosTab', $view->element('huddles/tabs/videosFrame', $videosData));

        /*         * **     Recent Activities     **** */


        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }

        $account_id = $user_current_account['accounts']['account_id'];
        $user_id = $user_current_account['User']['id'];

        $UserActivityLogs_model = $this->UserActivityLog->get_huddle_activity($user_current_account['accounts']['account_id'], $huddle_id);
        $myHuddles = $this->AccountFolder->getAllHuddlesDashboard($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);

        $this->set('UserActivityLogs', $UserActivityLogs_model);
        $this->set('user_current_account', $user_current_account);
        $this->set('user_id', $user_id);
        $this->set('account_id', $account_id);
        $this->set('myHuddles', $myHuddles);

        $UserActivityLogs['tab'] = $tab;

        $UserActivityLogs['myHuddles'] = $myHuddles;
        $UserActivityLogs['user_current_account'] = $user_current_account;
        $UserActivityLogs['AccountFolder'] = $this->AccountFolder;
        $UserActivityLogs['AccountFolderUser'] = $this->AccountFolderUser;
        $UserActivityLogs['AccountFolderGroup'] = $this->AccountFolderGroup;
        $UserActivityLogs['AccountFolderDocument'] = $this->AccountFolderDocument;
        $UserActivityLogs['Comment'] = $this->Comment;
        $UserActivityLogs['User'] = $this->User;
        $UserActivityLogs['UserActivityLogs'] = $UserActivityLogs_model;


        /*         * **     End of Recent Activities     **** */

        $latest_video_created_date = $this->UserAccount->query("SELECT
              d.`created_date`
            FROM
              `account_folders` AS af
              #join `account_folder_users` as afu
                #on af.`account_folder_id` = afu.`account_folder_id`
              JOIN `account_folder_documents` AS afd
                ON afd.`account_folder_id` = af.`account_folder_id`
              JOIN `documents` AS d
                ON d.`id` = afd.`document_id`
            WHERE d.`doc_type` IN (1, 3)
            AND af.`folder_type` = 1
              AND af.`account_id` = " . $account_id . "
              AND af.site_id =" . $this->site_id . "
              AND af.`account_folder_id`= " . $huddle_id . "
            ORDER BY d.`id` DESC
            LIMIT 1 ");

        $latest_video_created_date = $latest_video_created_date[0]['d']['created_date'];
        $this->set('latest_video_created_date', $latest_video_created_date);
        $this->set('standards', $standards);
        $this->set('standardsL2', $standardsL2);
        $this->set('ratings', $metric_old);
        $this->set('video_id', $detail_id);
        $this->layout = 'huddles_null';
        $this->render('video_details');
        if (!empty($videoDetail)) {
            $this->set('isVideoPage', '1');
        } else {
            $this->set('isVideoPage', '0');
        }
    }

    function delete_video_attachments($video_id) {


        $video_attachments = $this->AccountFolderDocumentAttachment->find('all', array('conditions' => array('attach_id' => $video_id)));



        foreach ($video_attachments as $video_attachment) {
            $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array(
                    'id' => $video_attachment['AccountFolderDocumentAttachment']['account_folder_document_id']
            )));



            $this->Document->deleteAll(array('id' => $account_folder_document['AccountFolderDocument']['document_id']), $cascade = true, $callbacks = true);
            $this->AccountFolderDocument->deleteAll(array('document_id' => $account_folder_document['AccountFolderDocument']['document_id']), $cascade = true, $callbacks = true);
        }

        $this->AccountFolderDocumentAttachment->deleteAll(array('attach_id' => $video_id), $cascade = true, $callbacks = true);

        return true;
    }

    function create_intercom_event($event_name, $meta_data, $loggedInUserEmail) {
        try {
            $client = new IntercomClient(Configure::read('intercom_access_token'), null);

            $client->events->create([
                "event_name" => $event_name,
                "created_at" => time(),
                "email" => $loggedInUserEmail,
                "metadata" => $meta_data
            ]);
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $status_code = $response->getStatusCode();
            return $status_code;
        }
    }

    function get_group_details($group_id) {

        $joins = array(
            'table' => 'users',
            'type' => 'inner',
            'conditions' => array('users.id = UserGroup.user_id')
        );


        $group_details = $this->Group->find('first', array('conditions' => array(
                "id" => $group_id
        )));


        $group_name = $group_details['Group']['name'];

        $group_user_details = $this->UserGroup->find('all', array('conditions' => array("group_id" => $group_id), 'joins' => array($joins), 'fields' => array(
                'users.first_name',
                'users.last_name',
                'users.email',
                'users.image',
                'users.id'
        )));

        $html = '';

        foreach ($group_user_details as $user) {
            if (isset($user['users']['image']) && $user['users']['id'] != '') {
                $html .= '<tr><td ><img width="20" src="https://s3.amazonaws.com/sibme.com/static/users/' . $user['users']['id'] . '/' . $user['users']['image'] . '"> ' . $user['users']['first_name'] . ' ' . $user['users']['last_name'] . ' </td><td>' . $user['users']['email'] . '</td></tr>';
            } else {
                $html .= '<tr><td ><img width="20" src="' . $this->base . '/img/home/photo-default.png' . '"> ' . $user['users']['first_name'] . ' ' . $user['users']['last_name'] . ' </td><td>' . $user['users']['email'] . '</td></tr>';
            }
        }

        $result = array(
            'html' => $html,
            'group_name' => $group_name
        );

        echo json_encode($result);
        die;
    }

    function _pathinfo($path, $options = null)
    {
        $path = urlencode($path);
        $parts = null === $options ? pathinfo($path) : pathinfo($path, $options);
        foreach ($parts as $field => $value) {
            $parts[$field] = urldecode($value);
        }
        return $parts;
    }
    
    /*get trascoder type set on account level*/
    function getTranscoderType($account_id){
       $transcoder = $this->Account->find('first', array('conditions' => array(
                    'id' => $account_id
            ),'fields' => array('active_transcoder_type')));
           
       return $transcoder['Account']['active_transcoder_type'];
        
    }
}
