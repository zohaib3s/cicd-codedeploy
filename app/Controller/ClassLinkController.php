<?php



class ClassLinkController extends AppController
{

    public $uses = array('User');
    public $client_id = '';
    public $client_secret = '';
    public $redirect_uri = '';
    public $authorize_url = '';
    public $auth_url = '';
    public $user_info_url = '';

    public function __construct($request = null, $response = null)
    {
        parent::__construct($request, $response);
        if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
            $redirect = 'https://' . $_SERVER['HTTP_HOST'] . "/";
        } else {
            $redirect = 'http://' . $_SERVER['HTTP_HOST'] . "/";
        }
        // var_dump(IS_PROD);
        // var_dump($redirect);
        // die;

        @Configure::write('sibme_base_url', $redirect);
        if (IS_PROD == false) {
            $this->client_id = 'c160189873061691ac79af4e5cbe7a27ba3be1fb7597';
            $this->client_secret = 'f360072b0afabb36011b010b31640bff';
        } else {
            //$this->client_id = '799315406812-0m5coft9u3b5v720ing80adltd6q6f9a.apps.googleusercontent.com';
            $this->client_id = 'c1601898730616387fc42eab4ee2f0e8a155ff5f0b32';
            //$this->client_secret = '316uY6iIFWy8uSARM3lBjRhh';
            $this->client_secret = 'aff0e18b4121ff15497d42c49a2d0150';
        }
        //$this->client = new Google_Client();
        //$this->client->setClientId($this->client_id);
        //$this->client->setClientSecret($this->client_secret);
        $this->redirect_uri = Configure::read('sibme_base_url') . 'ClassLink/classlink_endpoint/';
        //$this->client->setRedirectUri($this->redirect_uri);
        //$this->client->addScope("email");
        //$this->client->addScope("profile");
        //$this->client->setAccessType('offline');
        //$this->client->setApprovalPrompt('force');
        //$this->service = new Google_Service_Oauth2($this->client);

        $this->authorize_url =  "https://launchpad.classlink.com/oauth2/v2/auth";
        $this->auth_url = "https://launchpad.classlink.com/oauth2/v2/token";
        $this->user_info_url = "https://nodeapi.classlink.com/v2/my/info";
        //$this->Session->write('state', bin2hex(random_bytes(5)));
        //$this->Session->write('state', "123");
    }

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Session->write('state',  substr(md5(microtime()), rand(0, 26), 5));
        $this->Auth->allow('classlink_login', 'classlink_endpoint'); // Letting users register themselves
    }

    function classlink_login()
    {

        $authorize_url = $this->authorize_url . '?' . http_build_query([

            'client_id' =>  $this->client_id,

            'redirect_uri' =>  $this->redirect_uri,

            'response_type' => 'code',

            'state' => $this->Session->read('state'),

            'scope' => 'openid',

        ]);

        // $authUrl = $this->client->createAuthUrl();
        return $this->redirect($authorize_url);
    }

    function classlink_endpoint()
    {


        if (isset($_GET['code']) && $_GET['code'] != '') {

            $code = $_GET['code'];
            $params = [
                'client_id' =>  $this->client_id,
                'client_secret' =>  $this->client_secret,
                'code' => $code,
            ];

            $response = $this->http($this->auth_url, $params, true);

            if (isset($response->access_token)) {

                $user = $this->http($this->user_info_url, false, false, $response->access_token);

                if ($user && $user->Email) {
                    $totalac = $this->UserDeviceLog->query("select count(*) as totalaccount from users where email = '" . $user->Email . "' and is_active <> 0");

                    if ($totalac[0][0]['totalaccount'] == 2) {
                        $result = $this->UserDeviceLog->query("select * from users where email = '" . $user->Email . "' and is_active <> 0 and site_id = " . $this->site_id);
                    } else {
                        $result = $this->UserDeviceLog->query("select * from users where email = '" . $user->Email . "' and is_active <> 0 and site_id = 1");
                        if (empty($result)) {
                            $result = $this->UserDeviceLog->query("select * from users where email = '" . $user->Email . "' and is_active <> 0 and site_id = 2");
                        }
                    }
                    if ($this->site_id == 2 && $result[0]['users']['site_id'] == 1) {
                        $this->Session->delete('last_logged_in_user');
                        $this->Session->delete('access_token');
                        $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">Invalid email or password, try again.</div>');
                        return $this->redirect('/users/login');
                    }

                    //$this->User->find('first', array('conditions' => array('email' => $user->Email, 'is_active' <> 0, 'site_id' => $this->site_id)));
                    $result['User'] = $result[0]['users'];
                    if (!empty($result['User'])) {
                        $resp = $this->redirect_to_hmh($result['User']['username'], $result['User']['email'], $result['User']['password']);
                        if (!$resp) {
                            $this->Session->delete('last_logged_in_user');
                            $this->Session->delete('access_token');
                            $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">Invalid email or password, try again.</div>');
                            return $this->redirect('/users/login');
                        }
                    } elseif ($this->site_id == 2 && !empty($result['User'])) {
                        $resp = $this->redirect_to_hmh($result['User']['username'], $result['User']['email'], $result['User']['password']);
                        if (!$resp) {
                            $this->Session->delete('last_logged_in_user');
                            $this->Session->delete('access_token');
                            $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">Invalid email or password, try again.</div>');
                            return $this->redirect('/users/login');
                        }
                    } else {
                        $this->Session->delete('last_logged_in_user');
                        $this->Session->delete('access_token');
                        $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">Invalid email or password, try again.</div>');
                        return $this->redirect('/users/login');
                    }
                    if ($result) {
                        if ($this->Auth->login($result['User'])) {
                            $this->Cookie->delete('remember_me_cookie');
                            $this->setupPostLogin(true);
                            $redirect_url = $this->Auth->redirect();
                            return $this->redirect($redirect_url);
                        } else {
                            $this->Session->delete('last_logged_in_user');
                            $this->Session->delete('access_token');
                            $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">Invalid email or password, try again.</div>');
                            return $this->redirect('/users/login');
                        }
                    } else {
                        $this->Session->write('access_token', $this->client->getAccessToken());
                        $this->Session->delete('last_logged_in_user');
                        $this->Session->delete('access_token');
                        $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">User does not exist. Please use different login credentials. </div>');
                        return $this->redirect('/users/login');
                    }
                } else {
                    $this->Session->delete('last_logged_in_user');
                    $this->Session->delete('access_token');
                    $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">sorry your email address has been not verified, please try with correct email. </div>');
                    return $this->redirect('/users/login');
                }
            } else {

                $this->Session->delete('access_token');
                $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">Access Denied, Please try again. </div>');
                $this->Session->delete('last_logged_in_user');
                $this->Session->delete('access_token');
                return $this->redirect('/users/login');
                // echo $response->error;
                // echo "<br/>";
                // var_dump($response);
                // die();
            }

            //$this->client->authenticate($_GET['code']);


            // $this->Session->write('access_token', $this->client->getAccessToken());



            // $this->redirect(filter_var($this->redirect_uri, FILTER_SANITIZE_URL));
        } elseif (isset($_GET['error']) && $_GET['error'] == 'access_denied') {
            $this->Session->delete('access_token');
            $this->Session->write('error', '<div id="flashMessage" class="g-error-msg" style="cursor: pointer;">Access Denied, Please try again. </div>');
            $this->Session->delete('last_logged_in_user');
            $this->Session->delete('access_token');
            return $this->redirect('/users/login');
        }
    }

    public function redirect_to_hmh($username, $email, $password)
    {
        if ($this->site_id == 1) {
            $hmhdata = $this->UserDeviceLog->query("select * from users where (email = '" . $email . "' or username = '" . $email . "') and password = '" . $password . "' and site_id = 1");
            if (empty($hmhdata)) {
                $hmhdata = $this->UserDeviceLog->query("select * from users where (email = '" . $email . "' or username = '" . $email . "') and password = '" . $password . "' and site_id = 2");
            }
        }
        if ($this->site_id == 2) {
            $hmhdata = $this->UserDeviceLog->query("select * from users where (email = '" . $email . "' or username = '" . $email . "') and password = '" . $password . "' and site_id = 2");
            if (empty($hmhdata)) {
                $hmhdata = $this->UserDeviceLog->query("select * from users where (email = '" . $email . "' or username = '" . $email . "') and password = '" . $password . "' and site_id = 2");
            }
        }
        if (sizeof($hmhdata) > 0) {
            $sitesdata = $this->UserDeviceLog->query("select * from sites where site_id = " . $hmhdata[0]['users']['site_id']);
            $user_accounts = $this->UserDeviceLog->query("SELECT * FROM users_accounts WHERE user_id = " . $hmhdata[0]['users']['id'] . " AND is_active = 1 AND site_id =" . $hmhdata[0]['users']['site_id']);
            
            // echo "<pre>";
            // var_dump($user_accounts);
            // echo "</pre>";
            // die;
            if (count($user_accounts) > 1) {
                //$this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'] . '?stoh=1');
                
                $this->redirect('/Users/activate_user/' . $hmhdata[0]['users']['authentication_token'] . '?stoh=1');
                
            } else {
                //$this->redirect('https://' . $sitesdata[0]['sites']['site_url'] . '/Users/activate_user/' . $hmhdata[0]['users']['authentication_token']);
                $this->redirect('/Users/activate_user/' . $hmhdata[0]['users']['authentication_token']);
            }
        } else {
            return false;
        }
    }

    private function http($url, $params = false, $post = false, $token = false)
    {

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($post)
            curl_setopt($ch, CURLOPT_POST, 1);

        if ($token)
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Authorization: Bearer " . $token
            ));


        if ($params)
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

        $response = curl_exec($ch);

        curl_close($ch);

        return json_decode($response);
    }
}
