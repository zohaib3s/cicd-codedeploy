<?php

App::uses('AppController', 'Controller');

class AnalyticsController extends AppController {

    public $view = '';
    public $name = 'Analytics';
    public $use = array('User', 'UserActivityLog', 'Document', 'AccountTag', 'AccountCommentTag', 'DocumentStandardRating', 'UserAccount', 'AccountFolder', 'AccountFolderUser', 'UserGroup');

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
        $this->view = new View($this, false);
    }

    function index_2(){


        $this->layout = "layout_angular_apps";
        $this->render('index_2');

    }
    
    function index() {
        return $this->redirect('/analytics_angular/home', 301);  
        $user_current_account = $this->Session->read('user_current_account');
        $start_end_date = $this->get_start_end_date($user_current_account['accounts']['account_id']);
        $st_date = $start_end_date['start_date'];
        $end_date = $start_end_date['end_date'];
        $user_role_id = $user_current_account['roles']['role_id'];
        if ($user_role_id == 120) {

            $this->redirect('/analytics/play_card/' . $user_current_account['accounts']['account_id'] . '/' . $user_current_account['User']['id'] . '/' . $st_date . '/' . $end_date . '/2');
        }
        $user_permissions = $this->Session->read('user_permissions');
        $analytics_permission = $user_permissions['UserAccount']['permission_view_analytics'];


        if ($analytics_permission == 0 && $this->Session->read('role') == '120') {
            $this->no_permissions();
        }


        $frequency = '4';
        $filter_type = 'default';
        $start_date = '';
        $end_date = '';
        $account_id = $user_current_account['accounts']['account_id'];
        $filter_data = $this->Session->read('filters_data');
        $account_id_for_filter = '';
        if (isset($filter_data['subAccount']) && $filter_data['subAccount'] != '') {
            $account_id_for_filter = $filter_data['subAccount'];
        } else {
            $account_id_for_filter = $user_current_account['accounts']['account_id'];
        }

        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id_for_filter, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));

        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id_for_filter,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];
        if (date('m') >= $acct_duration_start) {
            $yr = date('Y');
            $dd = $yr . '-' . $acct_duration_start . '-01';
        } else {
            $yr = date('Y') - 1;
            $dd = $yr . '-' . $acct_duration_start . '-01';
        }
        $stDate = date_create($dd);
        $year = date_format($stDate, 'Y');
        $month = date_format($stDate, 'm');

        $account_sub_users = $this->UserAccount->find('all', array(
            'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'UserAccount.account_id = a.id'
                )
            ),
            'conditions' => array(
                'a.parent_account_id' => $account_id
            ),
            'fields' => array(
                'a.id',
                'a.company_name'
            ),
            'group' => array('a.id')
        ));
        $frameworks = $this->AccountTag->find("all", array(
            "conditions" => array(
                "tag_type" => '2', "account_id" => $account_id_for_filter
            ),
            'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'a.id = AccountTag.account_id'
                )
            ),
            'fields' => array(
                'AccountTag.*',
                'a.company_name'
            )
        ));
        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {

            $this->set('frameworks', $frameworks);
            $frm_wrk = array('framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
        }

        $standards = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id_for_filter,
                "tag_type" => 0,
                $frm_wrk
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code", "tag_html")
        ));

        $standard_array = array();
        if ($standards) {
            foreach ($standards as $st) {
                if (!empty($st['AccountTag']['tag_html'])) {
                    if (empty($st['AccountTag']['tads_code']))
                        $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                    else
                        $ac_tag_title = $st['AccountTag']['tads_code'];
                    $standard_array[] = array(
                        "value" => $st['AccountTag']['account_tag_id'],
                        "label" => $ac_tag_title
                    );
                }
            }
        }
        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id_for_filter, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $mt_arr = array();
        $count = 1;
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = $count . '-' . substr($mt_old['AccountMetaData']['meta_data_name'], 13);
            $count++;
        }
        $mt_arr = array();
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = substr($mt_old['AccountMetaData']['meta_data_name'], 13);
        }
        $assessments = array(0 => 'No Assessment');
        $arr_all = array_merge($assessments, $mt_arr);

        $folder_type = 2;

        $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        $d = date('Y-m-d', strtotime($st_date . "+11 month"));
        $endDate = date_create($d);
        $endDate = date_format($endDate, 'Y-m-t') . ' 23:59:59';

        $total_huddles = $this->get_total_huddles($account_id_for_filter, $folder_type, $st_date, $endDate);
        $request_type = false;
        $this->set('usersHtml', $this->accountUsersAnalytics($request_type));
        $this->set('frequency_tagged_standards', $this->frequency_of_tagged_standars($folder_type));
        $this->set('standards', json_encode($standard_array));

        $this->set('month', $month);
        $this->set('selected_year', $year);
        $this->set('selected_frequency', $frequency);
        $this->set('selected_account', $account_id_for_filter);
        $this->set('start_date', $start_date);
        $this->set('end_date', $end_date);
        $this->set('ratings', $mt_arr);
        $this->set('assessment_array', $arr_all);
        $this->set('folder_type', $folder_type);
        $this->set('filter_type', $filter_type);
        $this->set('account_overview', $this->account_overview($account_id));
        $this->set('account_sub_users', $account_sub_users);
        $this->set('total_huddles', $total_huddles);
        $this->render('index');
    }

    function frequency_of_tagged_standars() {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_role_id = $user['roles']['role_id'];
        $user_id = $user['User']['id'];

        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric['AccountMetaData']['meta_data_name'], 13)), 'value' => $metric['AccountMetaData']['meta_data_value']);
        }

        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();
        $mATCnd_custom_markers = array();
        if (!empty($this->request->data['folder_type'])) {
            $folder_type = $this->request->data['folder_type'];
        } else {
            $folder_type = 2;
        }
        $account_ids = array();
        if (!empty($this->data['subAccount'])) {
            $account_ids[] = $this->data['subAccount'];
        } else {
            $account_ov_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'id' => $account_id
                ),
                'fields' => array('id')
            ));

            $account_overview_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'parent_account_id' => $account_id
                ),
                'fields' => array('id')
            ));
            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $account_ids[] = $all_ac['Account']['id'];
            }
        }

        $acc_ids = implode(',', $account_ids);
        $search_by_standard = '';
        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (!empty($this->data['duration'])) {
            $durationGrph = $this->data['duration'];
            $search_by_standard = $this->data['search_by_standards'];
        } else {
            $durationGrph = 2;
        }


        if (!empty($this->data['framework_id'])) {
            $frameworks = $this->AccountTag->find("all", array(
                "conditions" => array(
                    "tag_type" => '2',
                    "account_id" => $account_id,
                    'account_tag_id' => $this->data['framework_id']
                ),
                'joins' => array(
                    array(
                        'table' => 'accounts as a',
                        'type' => 'inner',
                        'conditions' => 'a.id = AccountTag.account_id'
                    )
                ),
                'fields' => array(
                    'AccountTag.*',
                    'a.company_name'
                )
            ));
//            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
//                    "tag_type" => '2', "account_id" => $account_id, 'account_tag_id' => $this->data['framework_id']
//            )));
        } else {
//            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
//                    "tag_type" => '2', "account_id" => $account_id
//            )));

            $account_id_for_filter = '';

            if (!empty($this->request->data['subAccount'])) {
                $account_id_for_filter = $this->request->data['subAccount'];
            } else {
                $filter_data = $this->Session->read('filters_data');
                if (isset($filter_data['subAccount']) && $filter_data['subAccount'] != '') {
                    $account_id_for_filter = $filter_data['subAccount'];
                } else {
                    $account_id_for_filter = $account_id;
                }
            }

            $frameworks = $this->AccountTag->find("all", array(
                "conditions" => array(
                    "tag_type" => '2',
                    "account_id" => $account_id_for_filter
                ),
                'joins' => array(
                    array(
                        'table' => 'accounts as a',
                        'type' => 'inner',
                        'conditions' => 'a.id = AccountTag.account_id'
                    )
                ),
                'fields' => array(
                    'AccountTag.*',
                    'a.company_name'
                )
            ));
        }

        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
        }
        if (!empty($this->data['framework_id'])) {

            $frm_wrk = array('framework_id' => $this->data['framework_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $this->data['framework_id']);
        } else {

            $frm_wrk = array('framework_id' => isset($frameworks[0]['AccountTag']['account_tag_id']) ? $frameworks[0]['AccountTag']['account_tag_id'] : '');
            $act_frm_wrk = array('AccountTag.framework_id' => isset($frameworks[0]['AccountTag']['account_tag_id']) ? $frameworks[0]['AccountTag']['account_tag_id'] : '');
        }


        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];

        if (!empty($this->data['startDate']) && $this->data['filter_type'] == 'default') {
            $st_date = $this->data['startDate'] . '-01 00:00:00';
        } elseif (!empty($this->request->data['start_date']) && $this->request->data['filter_type'] == 'custom') {
            $start_date = $this->request->data['start_date'];
            $stDate = date_create($start_date);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        } else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }

        if (!empty($this->data['startDate']) && $this->data['filter_type'] == 'default') {
            $start_date = $this->data['startDate'] . '-01';
            if ($this->data['duration'] == 1) {
                $qu = 2;
            } elseif ($this->data['duration'] == 2) {
                $qu = 5;
            } elseif ($this->data['duration'] == 3) {
                $qu = 8;
            } elseif ($this->data['duration'] == 4) {
                $qu = 11;
            }
            $st_date = $this->data['startDate'] . '-01 00:00:00';
            $end_date = date('Y-m-d', strtotime($start_date . "+$qu month"));
            $end_date = date_create($end_date);
            $end_date = date_format($end_date, 'Y-m-t') . ' 23:59:59';
        } elseif (!empty($this->request->data['end_date']) && $this->request->data['filter_type'] == 'custom') {
            $end_date = $this->request->data['end_date'];
            $endDate = date_create($end_date);
            $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
        } else {
            $d = date('Y-m-d', strtotime($st_date . "+11 month"));
            $endDate = date_create($d);
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        }
//        Standard off
//        echo $st_date . '<br/>' . $end_date;
        $view = new View($this, false);
        if ($view->Custom->is_enabled_framework_and_standards($account_id)) {
            $mAUCnd[] = array('UserAccount.created_date >= ' => $st_date);
            $mAHCnd[] = array('AccountFolder.created_date >= ' => $st_date);
            $mAVCnd[] = array('d.created_date >= ' => $st_date);
            $mAVWCnd[] = array('UserActivityLog.date_added >= ' => $st_date);
            $mATCnd[] = array('act.created_date >= ' => $st_date);
            $mATCnd_custom_markers[] = array('act.created_date >= ' => $st_date);
            if (!empty($search_by_standard)) {
                $mATCnd[] = array('act.account_tag_id IN( ' . $search_by_standard . ')');
            }

            $mAUCnd[] = array('UserAccount.created_date <= ' => $end_date);
            $mAHCnd[] = array('AccountFolder.created_date <= ' => $end_date);
            $mAVCnd[] = array('d.created_date <= ' => $end_date);
            $mAVWCnd[] = array('UserActivityLog.date_added <= ' => $end_date);
            $mATCnd[] = array('act.created_date <= ' => $end_date);
            $mATCnd_custom_markers[] = array('act.created_date <= ' => $end_date);

            $start = (new DateTime($st_date))->modify('first day of this month');
            $end = (new DateTime($end_date))->modify('last day of this month');

            $interval = DateInterval::createFromDateString('1 month');
            $period = new DatePeriod($start, $interval, $end);

            $this->AccountTag->virtualFields['tags_date'] = 0;
            $this->AccountTag->virtualFields['total_tags'] = 0;
            $addtional_join = '';
            $additionalCon = '';
            if ($user_role_id == 115) {
                $addtional_join = array(
                    'table' => 'account_folder_users as afu',
                    'type' => 'inner',
                    'conditions' => 'afd.account_folder_id = afd.account_folder_id'
                );
                $additionalCon = array(
                    'afu.role_id' => 200,
                    'afu.user_id' => $user_id
                );
            }


            $joins = array(
                array(
                    'table' => 'account_comment_tags as act',
                    'type' => 'inner',
                    'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                ),
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'inner',
                    'conditions' => 'act.ref_id = afd.document_id'
                ),
//                array(
//                    'table' => 'account_folders as af',
//                    'type' => 'inner',
//                    'conditions' => 'af.account_folder_id = afd.account_folder_id'
//                ),
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'inner',
                    'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                ),
                $addtional_join
            );
            $conditions = array(
                'AccountTag.tag_type' => 0,
                'AccountTag.account_id' => $account_ids,
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => $folder_type,
                $mATCnd,
                $act_frm_wrk,
                $additionalCon
            );
            $fields = array(
                'afmd.meta_data_name',
                'afmd.meta_data_value',
                'AccountTag.tag_title',
                'AccountTag.tads_code',
                'AccountTag.account_tag_id',
                'act.created_date AS AccountTag__tags_date',
                'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
            );
            if (empty($search_by_standard)) {
                $get_value = array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => array('AccountTag.account_tag_id'),
                    'order' => 'AccountTag__total_tags DESC',
                );
            } else {
                $get_value = array(
                    'joins' => $joins,
                    'conditions' => $conditions,
                    'fields' => $fields,
                    'group' => array('AccountTag.account_tag_id'),
                    'order' => 'AccountTag__total_tags DESC'
                );
            }

            $account_tags_analytics1 = $this->AccountTag->find('all', $get_value);

            $account_tags_analytics = Set::extract('/AccountTag/.', $account_tags_analytics1);
            $response = array();
            $all_account_ids = array();


            $colors = array('#85c4e3', '#fff568', '#90ec7d', '#f7a35b', '#ee1c25', '#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#f1c40f', '#e67e22', '#ecf0f1', '#95a5a6', '#5A530D', '#FF0000', '#850CE8', '#0D74FF', '#75810C', '#0D2B5A');

            if ($account_tags_analytics) {
                $count = 0;
                for ($i = 0; $i <= 4; $i++) {
                    if ($account_tags_analytics[$i]['tag_title'] != '') {
                        $response[] = array(
                            'country' => $account_tags_analytics[$i]['tag_title'],
                            'tads_code' => $account_tags_analytics[$i]['tads_code'],
                            'litres' => $account_tags_analytics[$i]['total_tags'],
                            'color' => $colors[$count],
                            'account_tag_id' => $account_tags_analytics[$i]['account_tag_id'],
                            'AccountTag__tags_date' => $account_tags_analytics[$i]['tags_date'],
                            'AccountTag__total_tags' => $account_tags_analytics[$i]['total_tags']
                        );
                        $count ++;
                    }
                }

                $all_at_ids = array();
                foreach ($account_tags_analytics as $row) {
                    if ($row['tag_title'] != '') {
                        $all_account_ids[$row['account_tag_id']] = array(
                            'account_tag_id' => $row['account_tag_id'],
                            'tads_code' => $row['tads_code'],
                            'country' => $row['tag_title'],
                        );

                        $all_at_ids[] = $row['account_tag_id'];
                    }
                }
            }

            $result = array();
            $data['standarads_tag'] = json_encode($response);
            $frequency_of_tagged_standars_chart = $view->element('analytics/frequency_of_tagged_standards', $data, false);
            $result['frequency_of_tagged_standars_chart'] = $frequency_of_tagged_standars_chart;
            //Load Empty Records
            if ($search_by_standard == '') {
                $account_empty_tags_standers = $this->AccountTag->find('all', array(
                    'joins' => array(array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                        $addtional_join
                    ),
                    'fields' => '',
                    'conditions' => array(
                        'tag_type' => '0',
                        'account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $act_frm_wrk,
                        "tag_html <>  ''",
                        $additionalCon
                    ),
                ));

                if ($account_empty_tags_standers && count($account_tags_analytics) > 0) {
                    foreach ($account_empty_tags_standers as $row) {
                        if (isset($row['AccountTag']['account_tag_id']) && in_array($row['AccountTag']['account_tag_id'], $all_at_ids)) {
                            continue;
                        } else {
                            if ($row['AccountTag']['tag_title'] != '') {
                                $all_account_ids[$row['AccountTag']['account_tag_id']] = array(
                                    'account_tag_id' => $row['AccountTag']['account_tag_id'],
                                    'tads_code' => $row['AccountTag']['tads_code'],
                                    'country' => $row['AccountTag']['tag_title'],
                                );
                            }
                        }
                    }
                }
            }

            //Load Serial Charts

            $count = 0;
            $acc_tags = array();
            $total_standards = '0';
            foreach ($all_account_ids as $actag) {
                $acc_tags[] = $actag['account_tag_id'];
                $standard_title = '';
                if (empty($actag['tads_code'])) {
                    $standard_title = $actag['country'];
                } else {
                    $standard_title = $actag['tads_code'];
                }
                $graph_data_all[] = array();
                $graph_data = array();

                foreach ($period as $dt) {
                    $g_data = $dt->format("Y-m-d");
                    $duration = explode('-', $dt->format("Y-m-d"));
                    $dCnd = array();
                    $accountTagCnd = array();
                    $docCnd = array();
                    $graph_data['date'] = $dt->format("M-y");
                    $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                    $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                    $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                    $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        )
                    );
                    $conditions = array(
                        'AccountTag.account_tag_id' => $actag['account_tag_id'],
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $dCnd
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => array('AccountTag__total_tags' => 'desc')
                        );
                    }

                    $account_tags_analytics = $this->AccountTag->find('first', $get_value);

                    $standard_ratings = $this->DocumentStandardRating->find("all", array("conditions" => array(
                            'standard_id' => $actag['account_tag_id'], 'account_id' => $account_ids, 'created_at >= "' . $g_data . '"', 'created_at <= "' . $g_data . '" + INTERVAL 1 MONTH'
                    )));

                    $standard_avg_sum = 0;
                    $standard_count = 0;
                    $standard_avg = 'No Ratings';
                    $average_rating_name = 'No Ratings';
                    if ($standard_ratings) {
                        foreach ($standard_ratings as $row) {
                            $standard_avg_sum = $standard_avg_sum + $row['DocumentStandardRating']['rating_value'];
                            $standard_count++;
                        }
                        if (count($standard_avg_sum) > 0) {
                            $standard_avg = round($standard_avg_sum / $standard_count);
                        } else {
                            $standard_avg = 0;
                        }

                        $average_rating_name_details = $this->AccountMetaData->find('first', array("conditions" => array(
                                'account_id' => $account_id, 'meta_data_value' => $standard_avg, 'meta_data_name like "metric_value_%"'
                        )));
                        if (!empty($average_rating_name_details)) {
                            $average_rating_name = substr($average_rating_name_details['AccountMetaData']['meta_data_name'], 13);
                        } else {
                            $average_rating_name = 'No Ratings';
                        }
                    }
                    $graph_data['color'] = $colors[$count];
                    $graph_data['standard_rating_avg'] = $standard_avg;
                    $graph_data['average_rating_name'] = $average_rating_name;
                    $graph_data['color_rating'] = '#000';
                    $graph_data['total_tags'] = isset($account_tags_analytics['AccountTag']['total_tags']) ? $account_tags_analytics['AccountTag']['total_tags'] : '0';
                    $graph_data_all[$actag['account_tag_id'] . '-' . $standard_title . '-' . $colors[$count]][] = $graph_data;
                    unset($graph_data);
                }
                $count++;
            }

            $data_provider = array();
            $data_charts = array();
            $title = '';
            $mt_arr = array();
            foreach ($metric_old as $mt_old) {
                $mt_arr[] = substr($mt_old['AccountMetaData']['meta_data_name'], 13);
            }
            $assessments = array(0 => 'No Assessment');
            $arr_all = array_merge($assessments, $mt_arr);

            $role_id = $user['users_accounts']['role_id'];
            foreach ($graph_data_all as $key => $row) {
                $total_standards = 0;
                $total_avg_rattings = 0;
                $avg_counter = 0;
                if (count($row) > 0) {
                    $title = explode('-', $key);
                    if ($title[1] == '') {
                        continue;
                    }
                    foreach ($row as $r) {
                        $total_standards = $total_standards + $r['total_tags'];
                        $total_avg_rattings = $total_avg_rattings + $r['standard_rating_avg'];
                        if ($r['standard_rating_avg'] != 0) {
                            $avg_counter ++;
                        }
                    }
                    $total_avg_rattings = round($total_avg_rattings / $avg_counter);
                    if ($total_avg_rattings > 0) {
                        $final_average_rating_name = '';
                        foreach ($metric_new as $metric) {
                            if ($total_avg_rattings == $metric['value']) {
                                $final_average_rating_name = $metric['name'];
                            }
                        }
                    }

                    if ($total_avg_rattings == 0) {
                        $final_average_rating_name = 'No Rating';
                    }

                    $data_provider = array(
                        'title' => $title[1],
                        'color' => isset($title[2]) ? $title[2] : '',
                        'color_rating' => '#000',
                        'ratting_title' => 'Average Performance Level',
                        'count' => $count,
                        'data_provider' => json_encode($row),
                        'assessment_array' => $arr_all,
                        'folder_type' => $folder_type,
                        'total_tagged_standards' => $total_standards,
                        'total_avg' => ($role_id == 100) ? $final_average_rating_name : $total_avg_rattings,
                    );

                    $html = $view->element('analytics/frequency_serial_charts', $data_provider);
                    $data_charts[] = $html;
                }
                $count++;
            }

            $result['serial_charts'] = $data_charts;

        } else {
            //When Standard off
            $mATCnd_1[] = array('AccountCommentTag.created_date >= ' => $st_date);
            $mATCnd_1[] = array('AccountCommentTag.created_date <= ' => $end_date);
            $start = (new DateTime($st_date))->modify('first day of this month');
            $end = (new DateTime($end_date))->modify('last day of this month');

            $interval = DateInterval::createFromDateString('1 month');
            $period = new DatePeriod($start, $interval, $end);

            $this->AccountCommentTag->virtualFields['tags_date'] = 0;
            $this->AccountCommentTag->virtualFields['total_tags'] = 0;
            $addtional_join = '';
            $additionalCon = '';
            if ($user_role_id == 115) {
                $addtional_join = array(
                    'table' => 'account_folder_users as afu',
                    'type' => 'inner',
                    'conditions' => 'afd.account_folder_id = afd.account_folder_id'
                );
                $additionalCon = array(
                    'afu.role_id' => 200,
                    'afu.user_id' => $user_id
                );
            }
            $account_tags_analytics = $this->AccountCommentTag->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'AccountCommentTag.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders as af',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = af.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'af.account_folder_id = afmd.account_folder_id'
                    ),
                    $addtional_join
                ),
                'conditions' => array(
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'AccountCommentTag.ref_type' => 1,
                    'af.account_id' => $account_ids,
                    'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                    $mATCnd_1,
                    $additionalCon
                ),
                'fields' => array(
                    'AccountCommentTag.tag_title',
                    'AccountCommentTag.created_date AS AccountTag__tags_date',
                    'AccountCommentTag.account_comment_tag_id',
                    'AccountCommentTag.account_tag_id',
                    '(SELECT COUNT(tag_title) FROM account_comment_tags WHERE account_comment_tags.tag_title = AccountCommentTag.tag_title) AS total_tags'
                ),
                'group' => 'tag_title ORDER BY total_tags DESC',
                'limit' => 5
            ));
            $response = array();
            $colors = array('#85c4e3', '#fff568', '#90ec7d', '#f7a35b', '#ee1c25');

            if ($account_tags_analytics) {
                $count = 0;
                foreach ($account_tags_analytics as $row) {
                    $response[] = array(
                        'country' => $row['AccountCommentTag']['tag_title'],
                        'litres' => $row[0]['total_tags'],
                        'color' => $colors[$count],
                        'account_comment_tag_id' => $row['AccountCommentTag']['account_comment_tag_id'],
                        'AccountTag__tags_date' => $row[0]['AccountTag__tags_date'],
                        'AccountTag__total_tags' => $row[0]['total_tags'],
                    );
                    $count ++;
                }
            }

            $data['standarads_tag'] = json_encode($response);
            $view = new View($this, false);
            $frequency_of_tagged_standars_chart = $view->element('analytics/frequency_of_tagged_standards', $data, false);
            $result['frequency_of_tagged_standars_chart'] = $frequency_of_tagged_standars_chart;

            //Load Serial Charts
            $count = 0;
            foreach ($response as $actag) {
                $acc_tag_title = $actag['country'];
                $standard_title = '';
                if (empty($actag['tads_code'])) {
                    $standard_title = $actag['country'];
                } else {
                    $standard_title = $actag['tads_code'];
                }

                $graph_data_all[] = array();
                $graph_data = array();

                foreach ($period as $dt) {
                    $g_data = $dt->format("Y-m-d");
                    $duration = explode('-', $dt->format("Y-m-d"));
                    $dCnd_1 = array();
                    $accountTagCnd = array();
                    $docCnd = array();
                    $graph_data['date'] = $dt->format("M-y");
                    $dCnd_1[] = array('AccountCommentTag.created_date >= "' . $g_data . '"');
                    $dCnd_1[] = array('AccountCommentTag.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                    $account_tags_analytics = $this->AccountCommentTag->find('first', array(
                        'conditions' => array(
                            'AccountCommentTag.tag_title' => $acc_tag_title,
                            $dCnd_1
                        ),
                        'fields' => array(
                            'AccountCommentTag.tag_title',
                            'AccountCommentTag.created_date AS AccountTag__tags_date',
                            'AccountCommentTag.account_comment_tag_id',
                            'AccountCommentTag.account_tag_id',
                            '(SELECT COUNT(tag_title) FROM account_comment_tags WHERE account_comment_tags.tag_title = AccountCommentTag.tag_title) AS total_tags'
                        ),
                        'group' => 'tag_title ORDER BY total_tags DESC',
                    ));

                    $graph_data['color'] = $colors[$count];
                    $graph_data['total_tags'] = isset($account_tags_analytics[0]['total_tags']) ? $account_tags_analytics[0]['total_tags'] : '0';
                    $graph_data_all[$actag['account_comment_tag_id'] . '-' . $standard_title . '-' . $colors[$count]][] = $graph_data;
                    unset($graph_data);
                }
                $count++;
            }

            $data_provider = array();
            $data_charts = array();
            $title = '';
            if (count($graph_data_all) > 0) {
                foreach ($graph_data_all as $key => $row) {
                    $total_standards = 0;
                    $total_avg_rattings = 0;
                    $avg_counter = 0;
                    if (count($row) > 0) {
                        $title = explode('-', $key);
                        foreach ($row as $r) {
                            $total_standards = $total_standards + $r['total_tags'];
                            $total_avg_rattings = $total_avg_rattings + $r['standard_rating_avg'];
                            if ($r['standard_rating_avg'] != 0) {
                                $avg_counter ++;
                            }
                        }

                        $total_avg_rattings = round($total_avg_rattings / $avg_counter);


                        if ($total_avg_rattings > 0) {
                            $final_average_rating_name = '';
                            foreach ($metric_new as $metric) {
                                if ($total_avg_rattings == $metric['value']) {
                                    $final_average_rating_name = $metric['name'];
                                }
                            }
                        }

                        if ($total_avg_rattings == 0) {
                            $final_average_rating_name = 'No Rating';
                        }

                        $data_provider = array(
                            'title' => $title[1],
                            'color' => isset($title[2]) ? $title[2] : '',
                            'ratting_title' => 'Average Performance Level',
                            'count' => $count,
                            'data_provider' => json_encode($row),
                            'total_tagged_standards' => $total_standards,
                            'total_avg' => ($role_id == 100) ? $final_average_rating_name : $total_avg_rattings,
                        );
                        $html = $view->element('analytics/frequency_serial_charts', $data_provider);
                        $data_charts[] = $html;
                    }
                    $count++;
                }
            }


            $result['serial_charts'] = $data_charts;
        }

        //Load Custom Markers

        $joins_custom = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'act.ref_id = afd.document_id'
            ),
//            array(
//                'table' => 'account_folders_meta_data as afmd',
//                'type' => 'inner',
//                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
//            ),
        );
        $all_accounts = implode(',', $account_ids);

        $conditions_custom = array(
            'AccountTag.tag_type' => 1,
            'AccountTag.account_id' => $account_ids,
            //   'afmd.meta_data_name' => 'folder_type',
            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.')) ',
            //  'afmd.meta_data_value' => $folder_type,
            $mATCnd_custom_markers,
        );
        $fields_custom = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );

        $get_value_custom = array(
            'joins' => $joins_custom,
            'conditions' => $conditions_custom,
            'fields' => $fields_custom,
            'group' => array('AccountTag.account_tag_id'),
            //   'order' => 'AccountTag__total_tags DESC',
            'limit' => 4
        );

        $account_custom_tags_analytics1 = $this->AccountTag->find('all', $get_value_custom);

        $response_custom = array();
        $colors = array('#7fc44f', '#326291', '#ff5900', '#1ebdb0');
        if ($account_custom_tags_analytics1) {
            $count = 0;
            foreach ($account_custom_tags_analytics1 as $row) {
                $response_custom[] = array(
                    'country' => $row['AccountTag']['tag_title'],
                    'tads_code' => $row['AccountTag']['tads_code'],
                    'litres' => $row['AccountTag']['total_tags'],
                    'color' => $colors[$count],
                    'account_tag_id' => $row['AccountTag']['account_tag_id'],
                    'AccountTag__tags_date' => $row['AccountTag']['total_tags'],
                    'AccountTag__total_tags' => $row['AccountTag']['total_tags'],
                );
                $count ++;
            }
        }
        $total_huddles = $this->get_total_huddles($acc_ids, $folder_type, $st_date, $end_date);

        $data_1['custom_markers_tag'] = json_encode($response_custom);
        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
        $result['custom_markers_chart'] = $custom_markers_chart;
        $result['total_huddles'] = $total_huddles;
        $st_date = date_create($st_date);
        $end_date = date_create($end_date);
        $result['filter_date'] = date_format($st_date, 'F d, Y') . ' - ' . date_format($end_date, 'F d, Y');
        if ($this->request->is('post')) {
            return $result;
        } else {
            return $result;
        }
    }

    function account_overview() {

        $user = $this->Session->read('user_current_account');
        $user_check = '';
        $user_check1 = '';
        $user_check2 = '';
        $user_check3 = '';
        $user_check4 = '';
        $user_check5 = '';
        $user_check6 = '';
        $user_check7 = '';
        $user_role = $user['users_accounts']['role_id'];

        if ($user_role == 115) {
            $view = new View($this, false);
            $user_ids = $view->Custom->get_users_of_admin_circle($user['accounts']['account_id'], $user['users_accounts']['user_id']);
            $all_huddle_ids = $view->Custom->get_user_huddle_ids($user['accounts']['account_id'], $user['users_accounts']['user_id']);
            $huddle_ids = $view->Custom->get_user_huddle_ids($user['accounts']['account_id'], $user['users_accounts']['user_id'], 1);
            $all_huddle_ids = implode(',', $all_huddle_ids);
            $huddle_ids = implode(',', $huddle_ids);
            $user_ids = implode(',', $user_ids);
            $user_check = "u.id IN (" . $user_ids . ") AND ";
            $user_check1 = "ua.user_id IN (" . $user_ids . ") AND ";
            $user_check2 = "user_id IN (" . $user_ids . ") AND ";
            $user_check3 = "ua.user_id IN (" . $user_ids . ") AND ";
            $user_check4 = "ual.user_id IN (" . $user_ids . ") AND ";
            $user_check5 = "user_activity_logs.user_id IN (" . $user_ids . ") AND ";
            $user_check6 = "dvh.user_id IN (" . $user_ids . ")";
            $user_check7 = "ua.user_id IN (" . $user_ids . ")";
        }

        $owner_account_id = $user['accounts']['account_id'];

        $bool = 0;
        if (!empty($this->request->is('post')) && $this->request->data['subAccount'] != '') {
            $account_id = $this->request->data['subAccount'];
            $bool = 1;
        } else {
            $account_id = $user['accounts']['account_id'];
        }

        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $owner_account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];

        if ($this->request->is('post')) {
            if ($this->request->data['startDate'] && $this->request->data['filter_type'] == 'default') {
                $start_date = $this->data['startDate'] . '-01';
                if ($this->data['duration'] == 1) {
                    $qu = 2;
                } elseif ($this->data['duration'] == 2) {
                    $qu = 5;
                } elseif ($this->data['duration'] == 3) {
                    $qu = 8;
                } elseif ($this->data['duration'] == 4) {
                    $qu = 11;
                }
                $start_date = $this->data['startDate'] . '-01 00:00:00';
                $end_date = date('Y-m-d', strtotime($start_date . "+$qu month"));
                $end_date = date_create($end_date);
                $end_date = date_format($end_date, 'Y-m-t') . ' 23:59:59';
            } elseif (!empty($this->request->data['start_date']) && !empty($this->request->data['end_date'])) {
                $start_date = $this->request->data['start_date'];
                $stDate = date_create($start_date);
                $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

                $end_date = $this->request->data['end_date'];
                $endDate = date_create($end_date);
                $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
            } else {
                if (date('m') >= $acct_duration_start) {
                    $yr = date('Y');
                    $dd = $yr . '-' . $acct_duration_start . '-01';
                } else {
                    $yr = date('Y') - 1;
                    $dd = $yr . '-' . $acct_duration_start . '-01';
                }
                $stDate = date_create($dd);
                $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

                $endDate = date_create(date('Y-m-d'));
                $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
            }
        } else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

            $d = date('Y-m-d', strtotime($start_date . "+11 month"));
            $endDate = date_create($d);
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        }



        $account_ov_analytics = $this->Account->find('all', array(
            'conditions' => array(
                'Account.is_active' => 1,
                'id' => $account_id
            ),
            'fields' => array('id')
        ));

        $account_overview_analytics = $this->Account->find('all', array(
            'conditions' => array(
                'Account.is_active' => 1,
                'parent_account_id' => $account_id
            ),
            'fields' => array('id')
        ));
        $all_account_ids = array();
        if ($bool == 1) {
            $all_account_ids = array($account_id);
        } else {
            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $all_account_ids[] = $all_ac['Account']['id'];
            }
        }

        $all_acc = implode(',', $all_account_ids);

        $all_users = array();
        $account_user_analytics_results = $this->UserAccount->query("
                select * from (
                    select u.id as total_users,
                    ua.account_id
          from users u join users_accounts ua on u.id = ua.user_id
          join accounts a on a.id = ua.account_id
          WHERE " . $user_check . "   a.id IN ( $all_acc ) and ua.role_id NOT IN(125) GROUP BY u.id,ua.`account_id`
          ) as a");


        if (count($account_user_analytics_results) > 0) {

            $account_user_analytics = 0;

            foreach ($account_user_analytics_results as $account_user_analytics_result) {
                $all_users[$account_user_analytics_result['a']['account_id'] . '-' . $account_user_analytics_result['a']['total_users']] = $account_user_analytics_result['a']['total_users'];
//                $account_user_analytics += $account_user_analytics_result[0]['total_users'];
            }
        }
//        die;
//        $child_accounts = $this->Account->find('all', array(
//            'conditions' => array(
//                'parent_account_id' => $account_id
//            )
//                )
//        );
//
//        foreach ($child_accounts as $child_account) {
//            $account_user_analytics_results = $this->UserAccount->query("
//                select * from (
//                    select distinct u.id as total_users
//          from users u join users_accounts ua on u.id = ua.user_id
//          join accounts a on a.id = ua.account_id
//          WHERE  a.id= " . $child_account['Account']['id'] . " and ua.role_id NOT IN(125)
//          ) as a");
//
//            if (count($account_user_analytics_results) > 0) {
//
//                foreach ($account_user_analytics_results as $account_user_analytics_result) {
//                    $all_users[$account_user_analytics_result['a']['total_users']] = $account_user_analytics_result['a']['total_users'];
////                    $account_user_analytics += $account_user_analytics_result[0]['total_users'];
//                }
//            }
//        }


        if ($bool == 1) {
            $account_user_analytics_results = $this->UserAccount->query("
                    select * from (
                        select u.id as total_users,
                        ua.account_id
              from users u join users_accounts ua on u.id = ua.user_id
              join accounts a on a.id = ua.account_id
              WHERE " . $user_check . "  a.id=$account_id and ua.role_id NOT IN(125)GROUP BY u.id,ua.`account_id`
              ) as a");



            if (count($account_user_analytics_results) > 0) {
                $account_user_analytics = 0;
                foreach ($account_user_analytics_results as $account_user_analytics_result) {
                    $all_users[$account_user_analytics_result['a']['account_id'] . '-' . $account_user_analytics_result['a']['total_users']] = $account_user_analytics_result['a']['total_users'];
//                    $account_user_analytics += $account_user_analytics_result[0]['total_users'];
                }
            }
        }

        if (count($all_users) > 0) {
            $account_user_analytics = count($all_users);
        } else {
            $account_user_analytics = 0;
        }


        if ($bool == 1) {

            $total_huddles_results = $this->UserAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (

                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check2 . "  (ua.TYPE=1) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $account_id . ") AND  date_added >= '$start_date' AND date_added <= '$end_date' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");

            if ($user_role == 115) {

                $total_huddles_results = $this->UserAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (

                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check3 . "  (ua.TYPE=1) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $account_id . ") AND ua.account_folder_id IN (" . $huddle_ids . ") AND  date_added >= '$start_date' AND date_added <= '$end_date' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");
            }

            $huddle_created_count = 0;

            foreach ($total_huddles_results as $result) {

                $huddle_created_count += $result[0]['huddle_created_count'];
            }
            $account_huddle_analytics = $huddle_created_count;
        } else {


            $total_huddles_results = $this->UserAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (

                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check2 . "  (ua.TYPE=1) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $account_id . ") AND  date_added >= '$start_date' AND date_added <= '$end_date' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");

            if ($user_role == 115) {


                $total_huddles_results = $this->UserAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (
                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check3 . "  (ua.TYPE=1) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $account_id . ") AND ua.account_folder_id IN (" . $huddle_ids . ") AND  date_added >= '$start_date' AND date_added <= '$end_date' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");
            }

            $huddle_created_count = 0;

            foreach ($total_huddles_results as $result) {

                $huddle_created_count += $result[0]['huddle_created_count'];
            }

            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                )
                    )
            );
            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {
                    $total_huddles_results = $this->UserAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (
                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check2 . "  (ua.TYPE=1) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $child_account['Account']['id'] . ") AND  date_added >= '$start_date' AND date_added <= '$end_date' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $child_account['Account']['id'] . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");

                    if ($user_role == 115) {
                        $total_huddles_results = $this->UserAccount->query("
                    select sum(IFNULL(d.huddle_created_count, 0)) huddle_created_count from
                    users u
                    left join (
                        SELECT user_id, COUNT(*) AS huddle_created_count FROM `user_activity_logs` ua LEFT JOIN
                        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check3 . "  (ua.TYPE=1) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $child_account['Account']['id'] . ") AND ua.account_folder_id IN (" . $huddle_ids . ") AND  date_added >= '$start_date' AND date_added <= '$end_date' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $child_account['Account']['id'] . ")) GROUP BY ua.user_id
                    ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");
                    }



                    foreach ($total_huddles_results as $result) {

                        $huddle_created_count += $result[0]['huddle_created_count'];
                    }
                }
            }


            $account_huddle_analytics = $huddle_created_count;
        }

        if ($bool == 1) {
            $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
        users u
        left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
        account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
        WHERE " . $user_check3 . " (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1,2,3) AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $account_id . ")  AND  date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");

            if ($user_role == 115) {
                $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
        users u
        left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check3 . " (ua.TYPE=2 OR ua.TYPE=4) AND af.account_folder_id IN (" . $all_huddle_ids . ") AND af.`folder_type` IN(1,2,3)  AND   af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $account_id . ")  AND  date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");
            }

            $huddle_videos_count = 0;
            foreach ($total_videos_results as $result) {
                $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
            }
            $account_video_analytics = $huddle_videos_count;
        } else {
            $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
    users u
    left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check3 . " (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1,2,3) AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND  ua.date_added >='" . $start_date . "' AND ua.date_added <= '" . $end_date . "' AND ua.account_id IN (" . $account_id . ")  and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");


            if ($user_role == 115) {
                $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
        users u
        left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check3 . " (ua.TYPE=2 OR ua.TYPE=4) AND af.account_folder_id IN (" . $all_huddle_ids . ") AND af.`folder_type` IN(1,2,3) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $account_id . ")  AND  date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");
            }

            $huddle_videos_count = 0;

            foreach ($total_videos_results as $result) {

                $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
            }
            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                )
                    )
            );
            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {


                    $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle  from
            users u
            left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
            account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check3 . " (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1,2,3) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND  ua.date_added >='" . $start_date . "' AND ua.date_added <= '" . $end_date . "' AND ua.account_id IN (" . $child_account['Account']['id'] . ") and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $child_account['Account']['id'] . ")) GROUP BY ua.user_id) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");

                    if ($user_role == 115) {
                        $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
        users u
        left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE " . $user_check3 . " (ua.TYPE=2 OR ua.TYPE=4) AND af.account_folder_id IN (" . $all_huddle_ids . ") AND af.`folder_type` IN(1,2,3) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $child_account['Account']['id'] . ")  AND  date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $child_account['Account']['id'] . "))  GROUP BY ua.user_id ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");
                    }

                    foreach ($total_videos_results as $result) {

                        $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
                    }
                }
            }


            $account_video_analytics = $huddle_videos_count;
        }

        if ($bool == 1) {
            $total_videos_viewed_results = $this->UserAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u left join
( SELECT ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua join account_folder_documents afd on afd.document_id = ua.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id WHERE " . $user_check3 . " (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $account_id . ") AND date_added >= '" . $start_date . "' AND date_added <= '" . $end_date . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");

            if ($user_role == 115) {

                $total_videos_viewed_results = $this->UserAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u left join
( SELECT ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua join account_folder_documents afd on afd.document_id = ua.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id WHERE " . $user_check3 . " (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $account_id . ") AND date_added >= '" . $start_date . "' AND date_added <= '" . $end_date . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) AND ua.account_folder_id IN (" . $all_huddle_ids . ") GROUP BY ua.user_id ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");
            }


            $huddle_videos_count = 0;
            foreach ($total_videos_viewed_results as $result) {
                $huddle_videos_count += $result[0]['Resources_Viewed'];
            }
            $account_video_viewed_analytics = $huddle_videos_count;
        } else {

            $total_videos_viewed_results = $this->UserAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u left join
( SELECT ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua join account_folder_documents afd on afd.document_id = ua.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id WHERE " . $user_check3 . " (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $account_id . ") AND date_added >= '" . $start_date . "' AND date_added <= '" . $end_date . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) GROUP BY ua.user_id ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");

            if ($user_role == 115) {

                $total_videos_viewed_results = $this->UserAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u left join
( SELECT ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua join account_folder_documents afd on afd.document_id = ua.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id WHERE " . $user_check3 . " (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $account_id . ") AND date_added >= '" . $start_date . "' AND date_added <= '" . $end_date . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $account_id . ")) AND ua.account_folder_id IN (" . $all_huddle_ids . ") GROUP BY ua.user_id ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");
            }

            $huddle_videos_count = 0;

            foreach ($total_videos_viewed_results as $result) {

                $huddle_videos_count += $result[0]['Resources_Viewed'];
            }




            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                )
                    )
            );
            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {
                    $total_videos_viewed_results = $this->UserAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u left join
( SELECT ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua join account_folder_documents afd on afd.document_id = ua.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id WHERE " . $user_check3 . " (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $child_account['Account']['id'] . ") AND date_added >= '" . $start_date . "' AND date_added <= '" . $end_date . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $child_account['Account']['id'] . ")) GROUP BY ua.user_id ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");
                    if ($user_role == 115) {
                        $total_videos_viewed_results = $this->UserAccount->query("select sum(IFNULL(d.Resources_Viewed, 0)) Resources_Viewed
            from users u left join
( SELECT ua.user_id, COUNT(*) AS Resources_Viewed FROM `user_activity_logs` ua join account_folder_documents afd on afd.document_id = ua.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id join `users_accounts` uact on uact.user_id=ua.user_id and uact.account_id=ua.account_id WHERE " . $user_check3 . " (ua.TYPE=11) AND af.folder_type IN (1,2,3) AND ua.account_id IN (" . $child_account['Account']['id'] . ") AND date_added >= '" . $start_date . "' AND date_added <= '" . $end_date . "' and ua.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $child_account['Account']['id'] . ")) AND ua.account_folder_id IN (" . $all_huddle_ids . ") GROUP BY ua.user_id ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");
                    }

                    foreach ($total_videos_viewed_results as $result) {
                        $huddle_videos_count += $result[0]['Resources_Viewed'];
                    }
                }
            }



            $account_video_viewed_analytics = $huddle_videos_count;
        }



        if ($bool == 1) {

            $results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
                users u
                left join (
                SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                join comments on user_activity_logs.ref_id = comments.id
                join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
                WHERE " . $user_check5 . " TYPE IN (5) AND account_folders.folder_type IN(1,3)  AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");

            $results_replies = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
                users u
                left join (
                SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                join comments on user_activity_logs.ref_id = comments.id
                WHERE " . $user_check5 . " TYPE IN (8)  AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                ) d on d.user_id = u.id where u.is_active=1 and u.type='Active'");


            if ($user_role == 115) {
                $results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
                users u
                left join (
                SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                join comments on user_activity_logs.ref_id = comments.id
                join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
                WHERE " . $user_check5 . " TYPE IN (5) AND account_folders.folder_type IN(1,3) AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");

                $results_replies = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
                users u
                left join (
                SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                join comments on user_activity_logs.ref_id = comments.id
                WHERE " . $user_check5 . " TYPE IN (8) AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");
            }

            $huddle_videos_count = 0;
            $huddle_videos_replies_count = 0;
            foreach ($results as $result) {
                $huddle_videos_count += $result[0]['Total_Video_Comments'];
            }

            foreach ($results_replies as $replies) {
                $huddle_videos_replies_count += $replies[0]['Total_Video_Comments_Replies'];
            }
            $account_comments_analytics = $huddle_videos_count + $huddle_videos_replies_count;
        } else {

            $results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
    WHERE " . $user_check5 . " TYPE IN (5) AND account_folders.folder_type IN(1,3) AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
    ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");


            $results_replies = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    WHERE " . $user_check5 . " TYPE IN (8) AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
    ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");




            if ($user_role == 115) {
                $results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
    users u
    left join (

    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
    WHERE " . $user_check5 . " TYPE IN (5) AND account_folders.folder_type IN(1,3) AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "' GROUP BY user_activity_logs.user_id

    ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");
                $results_replies = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
    users u
    left join (
    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
    join comments on user_activity_logs.ref_id = comments.id
    WHERE " . $user_check5 . " TYPE IN (8) AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "' GROUP BY user_activity_logs.user_id

    ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");
            }


            $huddle_videos_count = 0;
            $huddle_videos_replies_count = 0;

            foreach ($results as $result) {

                $huddle_videos_count += $result[0]['Total_Video_Comments'];
            }
            foreach ($results_replies as $replies) {

                $huddle_videos_replies_count += $replies[0]['Total_Video_Comments_Replies'];
            }


            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
            )));
            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {
                    $results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
                    users u
                    left join (
                    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                    join comments on user_activity_logs.ref_id = comments.id
                    join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
                    WHERE " . $user_check5 . " TYPE IN (5) AND account_folders.folder_type IN(1,3) AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                    ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");
                    $results_replies = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
                    users u
                    left join (
                    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                    join comments on user_activity_logs.ref_id = comments.id
                    WHERE " . $user_check5 . " TYPE IN (8) AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                    ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");

                    if ($user_role == 115) {
                        $results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
                    users u
                    left join (
                    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                    join comments on user_activity_logs.ref_id = comments.id
                    join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
                    WHERE " . $user_check5 . " TYPE IN (5) AND account_folders.folder_type IN(1,3) AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                    ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");
                        $results_replies = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
                    users u
                    left join (
                    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                    join comments on user_activity_logs.ref_id = comments.id
                    WHERE " . $user_check5 . " TYPE IN (8) AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                    ) d on d.user_id = u.id  where u.is_active=1 and u.type='Active'");
                    }

                    foreach ($results as $result) {
                        $huddle_videos_count += $result[0]['Total_Video_Comments'];
                    }
                    foreach ($results_replies as $replies) {
                        $huddle_videos_replies_count += $replies[0]['Total_Video_Comments_Replies'];
                    }
                }
            }




            $account_comments_analytics = $huddle_videos_count + $huddle_videos_replies_count;
//            echo "Comments " . $account_comments_analytics;
//            die;
        }
        //total video watched
        $total_vide_duration_where = '';
        if ($start_date != '' && $end_date != '') {
            $total_vide_duration_where = "Document.created_date >= '$start_date' and Document.created_date <= '$end_date'";
        }
        $total_vide_duration = $this->Document->find('all', array(
            'joins' => array(
                array(
                    'table' => 'document_files as df',
                    'conditions' => 'Document.id = df.document_id',
                    'type' => 'inner'
                ),
                array(
                    'table' => '`user_activity_logs` as ua',
                    'conditions' => 'ua.ref_id = df.document_id',
                    'type' => 'inner'
                ),
                array(
                    'table' => '`users` as u',
                    'conditions' => 'ua.user_id = u.id',
                    'type' => 'inner'
                ),
            ),
            'conditions' => array(
                'Document.account_id' => $all_account_ids,
                'u.is_active' => 1,
                'u.type' => 'Active',
                'ua.type IN (2,4) ',
                $user_check7,
                $total_vide_duration_where
            ),
            'fields' => 'round(SUM(`df`.`duration`)/60/60,2) as total_duration',
            'group' => array('Document.created_by')
        ));
        $total_vide_duration_hours = 0;
        if ($total_vide_duration) {
            foreach ($total_vide_duration as $hours) {
                $total_vide_duration_hours = $total_vide_duration_hours + $hours[0]['total_duration'];
            }
        }


        if ($user_role == 115) {
            $total_vide_duration = $this->Document->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_files as df',
                        'conditions' => 'Document.id = df.document_id',
                        'type' => 'inner'
                    ),
                    array(
                        'table' => '`user_activity_logs` as ua',
                        'conditions' => 'ua.ref_id = df.document_id',
                        'type' => 'inner'
                    ),
                    array(
                        'table' => '`users` as u',
                        'conditions' => 'ua.user_id = u.id',
                        'type' => 'left'
                    ),
                ),
                'conditions' => array(
                    'Document.account_id' => $all_account_ids,
                    'u.is_active' => 1,
                    'u.type' => 'Active',
                    'ua.type IN (2,4) ',
                    'ua.account_folder_id IN (' . $all_huddle_ids . ')',
                    $user_check7,
                    $total_vide_duration_where
                ),
                'fields' => 'SUM(df.duration) as total_duration',
            ));

            $total_vide_duration_hours = 0;
            if ($total_vide_duration) {
                foreach ($total_vide_duration as $hours) {
                    $total_vide_duration_hours = $total_vide_duration_hours + $hours[0]['total_duration'];
                }
            }
        }

        if ($total_vide_duration_hours > 0) {
            $total_duration_hours = $total_vide_duration_hours;
        } else {
            $total_duration_hours = 0;
        }

        /* ---------------Total Video Duration------------------ */
        $total_video_watched_where = '';
        if ($start_date != '' && $end_date != '') {
            $total_video_watched_where = "dvh.created_date >= '$start_date' and dvh.created_date <= '$end_date'";
        }
        $total_video_watched = $this->User->find('all', array(
            'joins' => array(
                array(
                    'table' => 'document_viewer_histories as dvh',
                    'conditions' => 'dvh.user_id = User.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'account_folder_documents as afd',
                    'conditions' => 'afd.document_id = dvh.document_id',
                    'type' => 'inner'
                ),
            ),
            'conditions' => array(
                'dvh.account_id' => $all_account_ids,
                $user_check6,
                $total_video_watched_where
            ),
            'group' => array('dvh.document_id', 'dvh.user_id'),
            'fields' => 'max(dvh.minutes_watched) as minutes_watched'
        ));


        if ($user_role == 115) {

            $total_video_watched = $this->User->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'document_viewer_histories as dvh',
                        'conditions' => 'dvh.user_id = User.id',
                        'type' => 'left'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'conditions' => 'afd.document_id = dvh.document_id',
                        'type' => 'inner'
                    ),
                    array(
                        'table' => '`user_activity_logs` as ua',
                        'conditions' => 'ua.ref_id = dvh.document_id',
                        'type' => 'inner'
                    ),
                ),
                'conditions' => array(
                    'dvh.account_id' => $all_account_ids,
                    'ua.account_folder_id IN (' . $all_huddle_ids . ')',
                    $user_check6,
                    $total_video_watched_where
                ),
                'group' => array('dvh.document_id', 'dvh.user_id'),
                'fields' => 'max(dvh.minutes_watched) as minutes_watched'
            ));
        }

        $total_min_watched = 0;
        if ($total_video_watched) {
            foreach ($total_video_watched as $row) {
                $total_min_watched = $total_min_watched + $row[0]['minutes_watched'];
            }
        }
        if ($total_min_watched > 0) {
            $total_min_watched = round(($total_min_watched / 60) / 60, 2);
        } else {
            $total_min_watched = 0;
        }


//        $active_users = $this->UserAccount->query("select u.first_name from users u join users_accounts ua on u.id = ua.user_id
//  join accounts a on a.id = ua.account_id join user_activity_logs ul on  ul.type=9 and ul.user_id=u.id
//  WHERE u.is_active=1 AND u.TYPE='Active' AND ul.date_added >= '".$start_date."' and ul.date_added <= '".$end_date."' AND a.id = ".$account_id." group by ua.account_id, a.`company_name`,u.email, u.username, u.is_active");


        if ($bool == 1) {

            $active_users = $this->UserAccount->query(" select count(*) as total_users from (select distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.is_active=1 AND TYPE='Active' and a.id=$account_id and u.id in (select user_id from `user_activity_logs` where " . $user_check2 . " account_id = $account_id and type not in (9,10,12,16) and date_added >= '$start_date' and date_added <= '$end_date'  )) as a ");
            $active_total_users = 0;
            foreach ($active_users as $result) {

                $active_total_users += $result[0]['total_users'];
            }
        } else {
            $active_users = $this->UserAccount->query(" select count(*) as total_users from (select distinct u.id as total_users

  from users u join users_accounts ua on u.id = ua.user_id
  join accounts a on a.id = ua.account_id
  WHERE u.is_active=1 AND TYPE='Active' and a.id IN (" . $all_acc . ") and u.id in (select user_id from `user_activity_logs` where " . $user_check2 . "  account_id IN (" . $all_acc . ") and type not in (9,10,12,16) and date_added >= '$start_date' and date_added <= '$end_date'  )) as a ");

            $active_total_users = 0;

            foreach ($active_users as $result) {

                $active_total_users += $result[0]['total_users'];
            }

//            foreach ($child_accounts as $child_account) {
//                $active_users = $this->UserAccount->query(" select count(*) as total_users from (select distinct u.id as total_users
//
//  from users u join users_accounts ua on u.id = ua.user_id
//  join accounts a on a.id = ua.account_id
//  WHERE u.is_active=1 AND TYPE='Active' and a.id= " . $child_account['Account']['id'] . " and u.id in (select user_id from `user_activity_logs` where ". $user_check2 ." account_id = " . $child_account['Account']['id'] . " and type not in (9,10,12,16) and date_added >= '$start_date' and date_added <= '$end_date'  )) as a ");
//
//                foreach ($active_users as $result) {
//
//                    $active_total_users += $result[0]['total_users'];
//                }
//            }
        }


        $view = new View($this, false);
        $stDate = date_create($start_date);
        $endDate = date_create($end_date);
        $analytics_result = array(
            'total_account_users' => $account_user_analytics,
            'total_account_huddles' => $account_huddle_analytics,
            'total_account_videos' => $account_video_analytics,
            'total_viewed_videos' => $account_video_viewed_analytics,
            'total_comments_added' => $account_comments_analytics,
            'total_video_duration' => $total_duration_hours,
            'total_min_watched' => $total_min_watched,
            'active_users' => $active_total_users,
            'filter_date' => date_format($stDate, 'F d, Y') . ' - ' . date_format($endDate, 'F d, Y')
        );
        $account_overview_elem = $view->element('analytics/account_overview', $analytics_result, false);
        if ($this->request->is('post')) {
            return array('account_overview_elem' => $account_overview_elem);
        } else {
            return $account_overview_elem;
        }
    }

    function accountUsersAjax() {
        $start_limit = 0;
        $end_limit = 10;
        $page = $this->data['page'];
        $rows = $this->data['rows'];
        $sidx = $this->data['sidx'];
        $sord = $this->data['sord'];
        $filters = json_decode($this->data['filters']);


        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $view = new View($this, false);
        $user_ids = $view->Custom->get_users_of_admin_circle($account_id, $user_id);

        $all_huddle_ids = $view->Custom->get_user_huddle_ids($account_id, $user_id);
        $huddle_ids = $view->Custom->get_user_huddle_ids($account_id, $user_id, 1);
        $all_huddle_ids = implode(',', $all_huddle_ids);
        $huddle_ids = implode(',', $huddle_ids);
        $user_ids = implode(',', $user_ids);

        $mAUCndS = '';
        $mAUCndE = '';
        $account_ids = array();
        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];

        if (!empty($this->data['subAccount'])) {
            $account_ids[] = $this->data['subAccount'];
        } else {
            $account_ov_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'id' => $account_id
                ),
                'fields' => array('id')
            ));

            $account_overview_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'parent_account_id' => $account_id
                ),
                'fields' => array('id')
            ));
            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $account_ids[] = $all_ac['Account']['id'];
            }
        }

        $acc_ids = implode(',', $account_ids);

        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];
        $st_date = '';
        $end_date = '';



        if ($this->request->is('post') != '') {
            if (!empty($this->request->data['startDate']) && $this->request->data['filter_type'] == 'default') {
                $start_date = $this->data['startDate'] . '-01';
                if ($this->data['duration'] == 1) {
                    $qu = 2;
                } elseif ($this->data['duration'] == 2) {
                    $qu = 5;
                } elseif ($this->data['duration'] == 3) {
                    $qu = 8;
                } elseif ($this->data['duration'] == 4) {
                    $qu = 11;
                }
                $start_date = $this->data['startDate'] . '-01 00:00:00';
                $st_date = $start_date;
                $end_date = date('Y-m-d', strtotime($start_date . "+$qu month"));
                $end_date = date_create($end_date);
                $end_date = date_format($end_date, 'Y-m-t') . ' 23:59:59';
            } elseif (!empty($this->request->data['start_date']) && !empty($this->request->data['end_date']) && $this->request->data['filter_type'] == 'custom') {
                $start_date = $this->request->data['start_date'];
                $stDate = date_create($start_date);
                $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

                $end_date = $this->request->data['end_date'];
                $endDate = date_create($end_date);
                $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
            } else {
                if (date('m') >= $acct_duration_start) {
                    $yr = date('Y');
                    $dd = $yr . '-' . $acct_duration_start . '-01';
                } else {
                    $yr = date('Y') - 1;
                    $dd = $yr . '-' . $acct_duration_start . '-01';
                }
                $stDate = date_create($dd);
                $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

                $d = date('Y-m-d', strtotime($start_date . "+11 month"));
                $endDate = date_create($d);
                $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
            }
        } else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';


            $d = date('Y-m-d', strtotime($st_date . "+11 month"));
            $endDate = date_create($d);
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        }

        $folder_type = 2;
        if (!empty($this->request->data['folder_type'])) {
            $folder_type = $this->request->data['folder_type'];
        }




        $s_date = $st_date;
        $e_date = $end_date;

        $mAUCndS = ' AND au.created_date >="' . $st_date . '"';
        $mAUCndSD = ' AND d.created_date >="' . $st_date . '"';
        $mAUCndSLog = ' AND date_added >="' . $st_date . '"';

        $mAUCndE = ' AND au.created_date <="' . $end_date . '"';
        $mAUCndED = ' AND d.created_date <="' . $end_date . '"';
        $mAUCndELog = ' AND date_added <="' . $end_date . '"';
        $user_role_id = $user['roles']['role_id'];

        if ($user_role_id == 115) {
            $account_folder_id_cond = ' AND ua.account_folder_id IN( ' . $all_huddle_ids . ' ) ';
        } else {
            $account_folder_id_cond = '';
        }
        if ($user_role_id == 115) {
            $account_folder_id_dvh = ' AND afd.account_folder_id IN( ' . $all_huddle_ids . ' ) ';
        } else {
            $account_folder_id_dvh = '';
        }

        $additionalCond = '';
        if ($user_role_id == 115) {
            $user_ids = implode(',', $user_ids);
            $additionalCond = " AND u.id IN (SELECT user_id FROM account_folder_users WHERE account_folder_id IN(
                            SELECT
                              afu.account_folder_id
                            FROM
                              account_folders AS af
                              LEFT JOIN `account_folder_users` AS afu
                                ON af.`account_folder_id` = afu.`account_folder_id`
                                LEFT JOIN users as u
                                ON u.id = afu.user_id
                            WHERE af.`account_id` IN (" . $acc_ids . ")
                              AND af.folder_type IN(1)
                              AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                              AND u.is_active = 1
                              AND afu.`user_id` IN (" . $user_id . ")
                              ))";
        }
        $filterCond = '';
        if (isset($filters->rules[0]->data) && $filters->rules[0]->data != '') {
            $search_string = $filters->rules[0]->data;
            $filterCond = " AND CONCAT(u.`first_name`,' ',u.`last_name`) LIKE '%$search_string%' ";
            $rows = 1;
        }
        $all_users = $this->UserAccount->query("SELECT count(DISTINCT u.id) AS total_users FROM users u
                                                    LEFT JOIN users_accounts ua ON u.id = ua.user_id
                                                    LEFT JOIN accounts a ON ua.account_id = a.id
                                                    WHERE u.is_active=1 AND u.id NOT IN  (2423,2422,2427)
                                                    and ua.role_id NOT IN(125)  AND u.type='Active' $filterCond AND ua.account_id IN (" . $acc_ids . ") $additionalCond ");

        $total_users = $all_users[0][0]['total_users'];

        $limit = '';
        if ($rows != 'export' && $rows >= 1) {
            $end_limit = $page * $rows;
            $start_limit = $end_limit - $rows;
            $limit = " LIMIT $start_limit , $end_limit";
        } elseif ($rows == 'export') {
            $limit = "";
        }
//        echo $limit;
//        die;
//        $offset = ($page - 1) * $items_per_page;


        if ($user_role_id == 115) {


            $all_users = $this->UserAccount->query("SELECT DISTINCT usr.user_id,usr.account_id,usr.username as username, usr.email,usr.company_name,CONCAT(usr.first_name,' ',usr.last_name) AS account_name,
                                                    web_login_counts.login_counts AS web_login_counts,
                                                    video_upload_counts.video_upload_counts AS video_upload_counts,
                                                    workspace_upload_counts.workspace_upload_counts AS workspace_upload_counts,
                                                    library_upload_counts.library_upload_counts AS library_upload_counts,
                                                    shared_upload_counts.shared_upload_counts AS shared_upload_counts,
                                                    library_shared_upload_counts.library_shared_upload_counts AS library_shared_upload_counts,
                                                    huddle_created_count.huddle_created_count AS huddle_created_count,
                                                    comments_initiated_count.comments_initiated_count AS comments_initiated_count,
                                                    workspace_comments_initiated_count.workspace_comments_initiated_count AS workspace_comments_initiated_count,
                                                    replies_initiated_count.replies_initiated_count AS replies_initiated_count,
                                                    videos_viewed_count.videos_viewed_count AS videos_viewed_count,
                                                    workspace_videos_viewed_count.workspace_videos_viewed_count AS workspace_videos_viewed_count,
                                                    library_videos_viewed_count.library_videos_viewed_count AS library_videos_viewed_count,
                                                    documents_uploaded_count.documents_uploaded_count AS documents_uploaded_count,
                                                    documents_viewed_count.documents_viewed_count AS documents_viewed_count,
                                                    scripted_observations.scripted_observations AS scripted_observations,
                                                    scripted_video_observations.scripted_video_observations AS scripted_video_observations,
                                                     ROUND(document_durations.total_duration/60/60,2) AS total_document_durations,
                                                    dv_histories.minutes_watched,
                                                    assessment_huddle.total_assess_huddle,
                                                    coaching_huddle.total_coaching_huddle,
                                                    collab_huddles.total_collab_huddle,
                                                    is_user_coache_details.is_coachee,
                                                    user_accounts.role_id,
                                                    user_accounts.manage_evaluation_huddles,
                                                    user_accounts.manage_coach_huddles,
                                                    user_accounts.manage_collab_huddles

                                                    FROM (SELECT u.id AS user_id,u.username, u.email,u.first_name,u.last_name,a.company_name as company_name,a.id as account_id FROM users u
                                                    LEFT JOIN users_accounts ua ON u.id = ua.user_id
                                                    LEFT JOIN accounts a ON ua.account_id = a.id
                                                    WHERE u.is_active=1 AND u.id NOT IN  (2423,2422,2427) and ua.role_id NOT IN(125)  AND u.type='Active' $filterCond AND ua.account_id IN (" . $acc_ids . ") $additionalCond   GROUP BY u.id, ua.account_id ORDER BY CONCAT(u.`first_name`,' ',u.`last_name`)  $sord ) AS usr

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS login_counts FROM `user_activity_logs` WHERE TYPE=9 AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS web_login_counts

                                                    ON usr.user_id = web_login_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS video_upload_counts FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1) AND  af.`active` = 1 AND af.account_folder_id IN (" . $all_huddle_ids . ") AND ua.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY ua.user_id) AS video_upload_counts
                                                    ON usr.user_id = video_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS workspace_upload_counts FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(3) AND  af.`active` = 1 AND af.account_folder_id IN (" . $all_huddle_ids . ") AND ua.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY ua.user_id) AS workspace_upload_counts
                                                    ON usr.user_id = workspace_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS library_upload_counts FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(2) AND  af.`active` = 1 AND af.account_folder_id IN (" . $all_huddle_ids . ") AND ua.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY ua.user_id) AS library_upload_counts
                                                    ON usr.user_id = library_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT d.created_by AS user_id, COUNT(*) AS shared_upload_counts
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.account_folder_id IN (" . $all_huddle_ids . ") AND af.`folder_type` = 1 AND ua.`type` = 22 AND d.doc_type = 1  GROUP BY d.created_by) AS shared_upload_counts
                                                    ON usr.user_id = shared_upload_counts.user_id

                                                    LEFT JOIN

                                                      (SELECT d.created_by AS user_id, COUNT(*) AS library_shared_upload_counts
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 2 AND af.account_folder_id IN (" . $all_huddle_ids . ") AND ua.`type` = 22 AND d.doc_type = 1  GROUP BY d.created_by) AS library_shared_upload_counts
                                                    ON usr.user_id = library_shared_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id,COUNT(*) AS `huddle_created_count` FROM `user_activity_logs` join account_folders af ON af.account_folder_id = user_activity_logs.account_folder_id   WHERE af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) and `type` = 1 AND user_activity_logs.account_folder_id IN (" . $huddle_ids . ")  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS huddle_created_count
                                                    ON usr.user_id = huddle_created_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id join account_folders on user_activity_logs.account_folder_id = account_folders.account_folder_id   WHERE TYPE=5 AND account_folders.folder_type = 1 AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ") AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . "  group by user_activity_logs.user_id) AS comments_initiated_count

                                                    ON usr.user_id = comments_initiated_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS workspace_comments_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id join account_folders on user_activity_logs.account_folder_id = account_folders.account_folder_id   WHERE TYPE=5 AND account_folders.folder_type = 3 AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ") AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . "  group by user_activity_logs.user_id ) AS workspace_comments_initiated_count

                                                    ON usr.user_id = workspace_comments_initiated_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS replies_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE TYPE=8 AND user_activity_logs.account_folder_id IN (" . $huddle_ids . ") AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . "  group by user_activity_logs.user_id) AS replies_initiated_count

                                                    ON usr.user_id = replies_initiated_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS documents_uploaded_count FROM `user_activity_logs` WHERE TYPE=3 AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS documents_uploaded_count

                                                    ON usr.user_id = documents_uploaded_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS documents_viewed_count FROM `user_activity_logs` WHERE TYPE=13 AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS documents_viewed_count

                                                    ON usr.user_id = documents_viewed_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS videos_viewed_count
                                                        FROM `user_activity_logs`
                                                        join account_folder_documents on user_activity_logs.ref_id = account_folder_documents.document_id
                                                        join account_folders on account_folders.account_folder_id = account_folder_documents.account_folder_id
                                                        WHERE TYPE=11 AND account_folders.folder_type = 1 AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS videos_viewed_count
                                                    ON usr.user_id = videos_viewed_count.`user_id`


                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS workspace_videos_viewed_count
                                                        FROM `user_activity_logs`
                                                        join account_folder_documents on user_activity_logs.ref_id = account_folder_documents.document_id
                                                        join account_folders on account_folders.account_folder_id = account_folder_documents.account_folder_id

                                                        WHERE TYPE=11 AND account_folders.folder_type = 3 AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS workspace_videos_viewed_count

                                                    ON usr.user_id = workspace_videos_viewed_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS library_videos_viewed_count
                                                        FROM `user_activity_logs`
                                                        join account_folder_documents on user_activity_logs.ref_id = account_folder_documents.document_id
                                                        join account_folders on account_folders.account_folder_id = account_folder_documents.account_folder_id

                                                        WHERE TYPE=11 AND account_folders.folder_type = 2 AND user_activity_logs.account_folder_id IN (" . $all_huddle_ids . ")  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS library_videos_viewed_count

                                                    ON usr.user_id = library_videos_viewed_count.`user_id`


                                                      LEFT JOIN

                                                    (SELECT d.created_by AS user_id, COUNT(*) AS scripted_observations
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 1 AND ua.`type` = 23 AND d.is_associated = 1  AND d.current_duration is NULL AND d.published = 1 AND d.doc_type = 3  GROUP BY d.created_by) AS scripted_observations
                                                    ON usr.user_id = scripted_observations.user_id


                                                    LEFT JOIN

                                                    (SELECT d.created_by AS user_id, COUNT(*) AS scripted_video_observations
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 1 AND ua.`type` = 20 AND d.is_associated > 0  AND d.current_duration is NOT NULL AND d.published = 1 AND d.doc_type = 3  GROUP BY d.created_by) AS scripted_video_observations
                                                    ON usr.user_id = scripted_video_observations.user_id


                                                     LEFT JOIN
                                                    (
                                                       SELECT
                                                       d.created_by,
                                                       SUM(df.duration) AS total_duration
                                                       FROM documents AS d
                                                       INNER JOIN document_files AS df
                                                         ON df.`document_id` = d.`id`
                                                       INNER JOIN user_activity_logs AS  ua
                                                         ON ua.ref_id = df.document_id
                                                       WHERE
                                                        d.account_id IN (" . $acc_ids . ")
                                                       AND d.created_date >='$s_date'
                                                       AND d.created_date <= '$e_date'
                                                       AND ua.type IN (2,4)
                                                       $account_folder_id_cond
                                                    ) AS document_durations
                                                   ON  usr.user_id = document_durations.created_by

                                                   LEFT JOIN
                                                    (SELECT
                                                    dvh_history.user_id,
                                                    ROUND(SUM(minutes_watched) / 60 / 60, 2) AS minutes_watched
                                                  FROM
                                                    (SELECT
                                                      dvh.`document_id`,
                                                      dvh.user_id,
                                                      MAX(dvh.minutes_watched) AS minutes_watched
                                                    FROM
                                                      document_viewer_histories AS dvh
                                                      INNER JOIN account_folder_documents AS afd
                                                        ON afd.document_id = dvh.document_id
                                                    WHERE dvh.account_id IN (" . $acc_ids . ")
                                                      AND dvh.created_date >= '$s_date'
                                                      AND dvh.created_date <= '$e_date'
                                                      $account_folder_id_dvh
                                                    GROUP BY dvh.`document_id`, dvh.`user_id`) AS dvh_history
                                                    GROUP BY dvh_history.user_id
                                                    ) AS dv_histories
                                                  ON usr.user_id = dv_histories.user_id

                                                  LEFT JOIN
                                                    (
                                                          SELECT
                                                           afu.`user_id`,
                                                           CASE WHEN COUNT(af.account_folder_id) > 0  THEN 1 ELSE 0 END AS total_assess_huddle
                                                           FROM account_folders AS af
                                                           JOIN `account_folders_meta_data` AS afmd
                                                              ON afmd.`account_folder_id` = af.`account_folder_id`
                                                              JOIN `account_folder_users` AS afu
                                                           ON afu.`account_folder_id` = af.`account_folder_id`
                                                            WHERE afmd.`meta_data_name` = 'folder_type'
                                                              AND afmd.`meta_data_value` = 3
                                                              AND af.`account_id` IN (" . $acc_ids . ")
                                                              AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                                                            GROUP BY afu.user_id

                                                    )AS assessment_huddle
                                                    ON usr.user_id = assessment_huddle.user_id

                                                    LEFT JOIN
                                                    (
                                                          SELECT
                                                           afu.`user_id`,
                                                           CASE WHEN COUNT(af.account_folder_id) > 0  THEN 1 ELSE 0 END AS total_coaching_huddle
                                                           FROM account_folders AS af
                                                           JOIN `account_folders_meta_data` AS afmd
                                                              ON afmd.`account_folder_id` = af.`account_folder_id`
                                                              JOIN `account_folder_users` AS afu
                                                           ON afu.`account_folder_id` = af.`account_folder_id`
                                                            WHERE afmd.`meta_data_name` = 'folder_type'
                                                              AND afmd.`meta_data_value` = 2
                                                              AND af.`account_id` IN (" . $acc_ids . ")
                                                              AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                                                            GROUP BY afu.user_id

                                                    )AS coaching_huddle
                                                    ON usr.user_id = coaching_huddle.user_id

                                                  LEFT JOIN (
                                                    SELECT
                                                     afu.`user_id`,
                                                     CASE WHEN COUNT(af.account_folder_id) > 0  THEN 1 ELSE 0 END AS total_collab_huddle
                                                     FROM account_folders AS af
                                                     JOIN `account_folders_meta_data` AS afmd
                                                        ON afmd.`account_folder_id` = af.`account_folder_id`
                                                        JOIN `account_folder_users` AS afu
                                                     ON afu.`account_folder_id` = af.`account_folder_id`
                                                      WHERE afmd.`meta_data_name` = 'folder_type'
                                                        AND afmd.`meta_data_value` = 1
                                                        AND af.`account_id` IN(" . $acc_ids . ")
                                                        AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))

                                                        GROUP BY afu.user_id

                                              ) AS collab_huddles
                                              ON usr.user_id = collab_huddles.user_id
                                              LEFT JOIN(
                                                    SELECT
                                                     afu.`user_id`,
                                                     CASE WHEN COUNT(af.account_folder_id) > 0  THEN 1 ELSE 0 END AS is_coachee
                                                    FROM
                                                    account_folders AS af
                                                    INNER JOIN account_folder_users afu
                                                      ON af.`account_folder_id` = afu.`account_folder_id`
                                                      INNER JOIN `account_folders_meta_data` afmd ON afmd.`account_folder_id` = af.`account_folder_id`
                                                    WHERE af.`account_id` IN(" . $acc_ids . ")
                                                    AND afu.`role_id` = '210'
                                                    AND afmd.`meta_data_name` = 'folder_type'
                                                    AND afmd.`meta_data_value` = '2'
                                                    AND afu.`is_mentee` = 1
                                                     GROUP BY afu.user_id
                                              ) AS is_user_coache_details
                                             ON usr.user_id = is_user_coache_details.user_id

                                             LEFT JOIN (
                                                        SELECT
                                                        *
                                                        FROM
                                                        users_accounts
                                                        GROUP BY account_id, user_id
                                                )AS user_accounts
                                                ON (usr.user_id = user_accounts.user_id  AND usr.account_id = user_accounts.account_id)
                                                    group by usr.`user_id`,usr.account_id
                                                    ORDER BY CONCAT(usr.`first_name`,' ',usr.`last_name`)  $sord
                                                    $limit
                                                ");
        } else {
            $all_users = $this->UserAccount->query("SELECT DISTINCT usr.user_id,usr.account_id,usr.username as username, usr.email,usr.company_name,CONCAT(usr.first_name,' ',usr.last_name) AS account_name,
                                                    web_login_counts.login_counts AS web_login_counts,
                                                    video_upload_counts.video_upload_counts AS video_upload_counts,
                                                    workspace_upload_counts.workspace_upload_counts AS workspace_upload_counts,
                                                    library_upload_counts.library_upload_counts AS library_upload_counts,
                                                    shared_upload_counts.shared_upload_counts AS shared_upload_counts,
                                                    library_shared_upload_counts.library_shared_upload_counts AS library_shared_upload_counts,
                                                    huddle_created_count.huddle_created_count AS huddle_created_count,
                                                    comments_initiated_count.comments_initiated_count AS comments_initiated_count,
                                                    workspace_comments_initiated_count.workspace_comments_initiated_count AS workspace_comments_initiated_count,
                                                    replies_initiated_count.replies_initiated_count AS replies_initiated_count,
                                                    videos_viewed_count.videos_viewed_count AS videos_viewed_count,
                                                    workspace_videos_viewed_count.workspace_videos_viewed_count AS workspace_videos_viewed_count,
                                                    library_videos_viewed_count.library_videos_viewed_count AS library_videos_viewed_count,
                                                    documents_uploaded_count.documents_uploaded_count AS documents_uploaded_count,
                                                    documents_viewed_count.documents_viewed_count AS documents_viewed_count,
                                                    scripted_observations.scripted_observations AS scripted_observations,
                                                    scripted_video_observations.scripted_video_observations AS scripted_video_observations,
                                                    ROUND(document_durations.total_duration/60/60,2) AS total_document_durations,
                                                    dv_histories.minutes_watched,
                                                    assessment_huddle.total_assess_huddle,
                                                    coaching_huddle.total_coaching_huddle,
                                                    collab_huddles.total_collab_huddle,
                                                    is_user_coache_details.is_coachee,
                                                    user_accounts.role_id,
                                                    user_accounts.manage_evaluation_huddles,
                                                    user_accounts.manage_coach_huddles,
                                                    user_accounts.manage_collab_huddles

                                                    FROM (SELECT u.id AS user_id,u.username, u.email,u.first_name,u.last_name,a.company_name as company_name,a.id as account_id FROM users u
                                                    LEFT JOIN users_accounts ua ON u.id = ua.user_id
                                                    LEFT JOIN accounts a ON ua.account_id = a.id
                                                    WHERE u.is_active=1 AND u.id NOT IN (2423,2422,2427) and ua.role_id NOT IN(125)  AND u.type='Active' $filterCond AND ua.account_id IN (" . $acc_ids . ") $additionalCond   GROUP BY u.id, ua.account_id ORDER BY CONCAT(u.`first_name`,' ',u.`last_name`)  $sord) AS usr

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS login_counts FROM `user_activity_logs` WHERE TYPE=9 AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS web_login_counts

                                                    ON usr.user_id = web_login_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS video_upload_counts FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1) AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY ua.user_id ) AS video_upload_counts
                                                    ON usr.user_id = video_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS workspace_upload_counts FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(3) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY ua.user_id) AS workspace_upload_counts
                                                    ON usr.user_id = workspace_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS library_upload_counts FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(2) AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY ua.user_id) AS library_upload_counts
                                                    ON usr.user_id = library_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT d.created_by AS user_id, COUNT(*) AS shared_upload_counts
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 1 AND ua.`type` = 22 AND d.doc_type = 1  GROUP BY d.created_by) AS shared_upload_counts
                                                    ON usr.user_id = shared_upload_counts.user_id

                                                    LEFT JOIN

                                                      (SELECT d.created_by AS user_id, COUNT(*) AS library_shared_upload_counts
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 2 AND ua.`type` = 22 AND d.doc_type = 1  GROUP BY d.created_by) AS library_shared_upload_counts
                                                    ON usr.user_id = library_shared_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id,COUNT(*) AS `huddle_created_count`
                                                    FROM `user_activity_logs`
                                                    join account_folders af ON af.account_folder_id = user_activity_logs.account_folder_id
                                                    join `users_accounts` uact on uact.user_id=user_activity_logs.user_id and uact.account_id=user_activity_logs.account_id
                                                    WHERE af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                                                    AND `type` = 1
                                                    AND user_activity_logs.user_id in (select user_id from users_accounts where role_id <125 and account_id IN (" . $acc_ids . "))
                                                    AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . "
                                                    GROUP BY user_activity_logs.user_id) AS huddle_created_count
                                                    ON usr.user_id = huddle_created_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                                                    join comments on user_activity_logs.ref_id = comments.id
                                                    join account_folders on user_activity_logs.account_folder_id = account_folders.account_folder_id
                                                    WHERE TYPE=5 AND account_folders.folder_type = 1 AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . "  group by user_activity_logs.user_id ) AS comments_initiated_count

                                                    ON usr.user_id = comments_initiated_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS workspace_comments_initiated_count FROM `user_activity_logs`
                                                    join comments on user_activity_logs.ref_id = comments.id
                                                    join account_folders on user_activity_logs.account_folder_id = account_folders.account_folder_id
                                                    WHERE 
                                                    TYPE=5 AND account_folders.folder_type = 3 
                                                    AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . "
                                                    group by user_activity_logs.user_id 
                                                     ) AS workspace_comments_initiated_count

                                                    ON usr.user_id = workspace_comments_initiated_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS replies_initiated_count FROM `user_activity_logs`
                                                    join comments on user_activity_logs.ref_id = comments.id
                                                    WHERE TYPE=8 AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . "  group by user_activity_logs.user_id  ) AS replies_initiated_count

                                                    ON usr.user_id = replies_initiated_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS documents_uploaded_count FROM `user_activity_logs` WHERE TYPE=3 AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS documents_uploaded_count

                                                    ON usr.user_id = documents_uploaded_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS documents_viewed_count FROM `user_activity_logs` WHERE TYPE=13 AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS documents_viewed_count

                                                    ON usr.user_id = documents_viewed_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS videos_viewed_count
                                                        FROM `user_activity_logs`
                                                        join account_folder_documents on user_activity_logs.ref_id = account_folder_documents.document_id
                                                        join account_folders on account_folders.account_folder_id = account_folder_documents.account_folder_id
                                                        join `users_accounts` uact on uact.user_id=user_activity_logs.user_id and uact.account_id=user_activity_logs.account_id
                                                        WHERE TYPE=11 AND account_folders.folder_type = 1  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_activity_logs.user_id) AS videos_viewed_count
                                                    ON usr.user_id = videos_viewed_count.`user_id`


                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS workspace_videos_viewed_count
                                                        FROM `user_activity_logs`
                                                        join account_folder_documents on user_activity_logs.ref_id = account_folder_documents.document_id
                                                        join account_folders on account_folders.account_folder_id = account_folder_documents.account_folder_id
                                                        join `users_accounts` uact on uact.user_id=user_activity_logs.user_id and uact.account_id=user_activity_logs.account_id
                                                        WHERE TYPE=11 AND account_folders.folder_type = 3  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_activity_logs.user_id) AS workspace_videos_viewed_count

                                                    ON usr.user_id = workspace_videos_viewed_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS library_videos_viewed_count
                                                        FROM `user_activity_logs`
                                                        join account_folder_documents on user_activity_logs.ref_id = account_folder_documents.document_id
                                                        join account_folders on account_folders.account_folder_id = account_folder_documents.account_folder_id
                                                        join `users_accounts` uact on uact.user_id=user_activity_logs.user_id and uact.account_id=user_activity_logs.account_id
                                                        WHERE TYPE=11 AND account_folders.folder_type = 2  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_activity_logs.user_id) AS library_videos_viewed_count

                                                    ON usr.user_id = library_videos_viewed_count.`user_id`


                                                      LEFT JOIN

                                                    (SELECT d.created_by AS user_id, COUNT(*) AS scripted_observations
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 1 AND ua.`type` = 23 AND d.is_associated = 1  AND d.current_duration is NULL AND d.published = 1 AND d.doc_type = 3  GROUP BY d.created_by) AS scripted_observations
                                                    ON usr.user_id = scripted_observations.user_id


                                                    LEFT JOIN

                                                    (SELECT d.created_by AS user_id, COUNT(*) AS scripted_video_observations
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 1 AND ua.`type` = 20 AND d.is_associated > 0  AND d.current_duration is NOT NULL AND d.published = 1 AND d.doc_type = 3  GROUP BY d.created_by) AS scripted_video_observations
                                                    ON usr.user_id = scripted_video_observations.user_id

                                                    LEFT JOIN
                                                    (
                                                       SELECT
                                                       d.created_by,
                                                       SUM(df.duration) AS total_duration
                                                       FROM documents AS d
                                                       INNER JOIN document_files AS df
                                                         ON df.`document_id` = d.`id`
                                                       INNER JOIN user_activity_logs AS  ua
                                                         ON ua.ref_id = df.document_id
                                                       WHERE
                                                        d.account_id IN (" . $acc_ids . ")
                                                       AND d.created_date >='$s_date'
                                                       AND d.created_date <= '$e_date'
                                                       AND ua.type IN (2,4)
                                                       $account_folder_id_cond
                                                      group by d.created_by
                                                    ) AS document_durations
                                                   ON  usr.user_id = document_durations.created_by

                                                   LEFT JOIN
                                                    (SELECT
                                                    dvh_history.user_id,
                                                    ROUND(SUM(minutes_watched) / 60 / 60, 2) AS minutes_watched
                                                  FROM
                                                    (SELECT
                                                      dvh.`document_id`,
                                                      dvh.user_id,
                                                      MAX(dvh.minutes_watched) AS minutes_watched
                                                    FROM
                                                      document_viewer_histories AS dvh
                                                      INNER JOIN account_folder_documents AS afd
                                                        ON afd.document_id = dvh.document_id
                                                    WHERE dvh.account_id IN (" . $acc_ids . ")
                                                      AND dvh.created_date >= '$s_date'
                                                      AND dvh.created_date <= '$e_date'
                                                      $account_folder_id_dvh
                                                    GROUP BY dvh.`document_id`, dvh.`user_id`) AS dvh_history
                                                    GROUP BY dvh_history.user_id
                                                    ) AS dv_histories
                                                  ON usr.user_id = dv_histories.user_id

                                                  LEFT JOIN
                                                    (
                                                          SELECT
                                                           afu.`user_id`,
                                                           CASE WHEN COUNT(af.account_folder_id) > 0  THEN 1 ELSE 0 END AS total_assess_huddle
                                                           FROM account_folders AS af
                                                           JOIN `account_folders_meta_data` AS afmd
                                                              ON afmd.`account_folder_id` = af.`account_folder_id`
                                                              JOIN `account_folder_users` AS afu
                                                           ON afu.`account_folder_id` = af.`account_folder_id`
                                                            WHERE afmd.`meta_data_name` = 'folder_type'
                                                              AND afmd.`meta_data_value` = 3
                                                              AND af.`account_id` IN (" . $acc_ids . ")
                                                              AND af.created_date >= '$s_date'
                                                              AND af.created_date <= '$e_date'
                                                              AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                                                            GROUP BY afu.user_id

                                                    )AS assessment_huddle
                                                    ON usr.user_id = assessment_huddle.user_id

                                                    LEFT JOIN
                                                    (
                                                          SELECT
                                                           afu.`user_id`,
                                                           CASE WHEN COUNT(af.account_folder_id) > 0  THEN 1 ELSE 0 END AS total_coaching_huddle
                                                           FROM account_folders AS af
                                                           JOIN `account_folders_meta_data` AS afmd
                                                              ON afmd.`account_folder_id` = af.`account_folder_id`
                                                              JOIN `account_folder_users` AS afu
                                                           ON afu.`account_folder_id` = af.`account_folder_id`
                                                            WHERE afmd.`meta_data_name` = 'folder_type'
                                                              AND afmd.`meta_data_value` = 2
                                                              AND af.`account_id` IN (" . $acc_ids . ")
                                                              AND af.created_date >= '$s_date'
                                                              AND af.created_date <= '$e_date'
                                                              AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                                                            GROUP BY afu.user_id

                                                    )AS coaching_huddle
                                                    ON usr.user_id = coaching_huddle.user_id

                                                  LEFT JOIN (
                                                    SELECT
                                                     afu.`user_id`,
                                                     CASE WHEN COUNT(af.account_folder_id) > 0  THEN 1 ELSE 0 END AS total_collab_huddle
                                                     FROM account_folders AS af
                                                     JOIN `account_folders_meta_data` AS afmd
                                                        ON afmd.`account_folder_id` = af.`account_folder_id`
                                                        JOIN `account_folder_users` AS afu
                                                     ON afu.`account_folder_id` = af.`account_folder_id`
                                                      WHERE afmd.`meta_data_name` = 'folder_type'
                                                        AND afmd.`meta_data_value` = 1
                                                        AND af.`account_id` IN(" . $acc_ids . ")
                                                        AND af.created_date >= '$s_date'
                                                        AND af.created_date <= '$e_date'
                                                        AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                                                        GROUP BY afu.user_id

                                              ) AS collab_huddles
                                              ON usr.user_id = collab_huddles.user_id
                                              LEFT JOIN(
                                                    SELECT
                                                     afu.`user_id`,
                                                     CASE WHEN COUNT(af.account_folder_id) > 0  THEN 1 ELSE 0 END AS is_coachee
                                                    FROM
                                                    account_folders AS af
                                                    INNER JOIN account_folder_users afu
                                                      ON af.`account_folder_id` = afu.`account_folder_id`
                                                      INNER JOIN `account_folders_meta_data` afmd ON afmd.`account_folder_id` = af.`account_folder_id`
                                                    WHERE af.`account_id` IN(" . $acc_ids . ")
                                                    AND afu.`role_id` = '210'
                                                    AND afmd.`meta_data_name` = 'folder_type'
                                                    AND afmd.`meta_data_value` = '2'
                                                    AND afu.`is_mentee` = 1
                                                     GROUP BY afu.user_id
                                              ) AS is_user_coache_details
                                             ON usr.user_id = is_user_coache_details.user_id

                                             LEFT JOIN (
                                                        SELECT
                                                        *
                                                        FROM
                                                        users_accounts
                                                        GROUP BY account_id, user_id
                                                )AS user_accounts
                                                ON (usr.user_id = user_accounts.user_id  AND usr.account_id = user_accounts.account_id)
                                                 group by usr.user_id
                                                ORDER BY CONCAT(usr.`first_name`,' ',usr.`last_name`)  $sord
                                                $limit
                                                ");
        }






        $view = new View($this, false);
        $eval_active = $view->Custom->check_if_eval_huddle_active($account_id);




        $user_data = array();
        $cnt = 0;
        $st_date = explode(' ', $st_date);

        $date_explode = explode(' ', $end_date);
        $start_date = $st_date[0];
        $total_commments_adde = 0;

        foreach ($all_users as $user):
            $user_data[$cnt]['account_id'] = $user['usr']['account_id'];
            $user_data[$cnt]['company_name'] = $user['usr']['company_name'];
            $user_data[$cnt]['user_id'] = $user['usr']['user_id'];
            $user_data[$cnt]['account_name'] = '<a href="' . Configure::read('sibme_base_url') . 'analytics/play_card/' . $user['usr']['account_id'] . '/' . $user['usr']['user_id'] . '/' . $start_date . '/' . $date_explode[0] . '/' . $folder_type . '">' . utf8_encode($user[0]['account_name']) . '</a>';
            $user_data[$cnt]['user_name'] = utf8_encode($user[0]['account_name']);
            $user_data[$cnt]['email'] = $user['usr']['email'];
            $user_data[$cnt]['video_upload_counts'] = !empty($user['video_upload_counts']['video_upload_counts']) ? $user['video_upload_counts']['video_upload_counts'] : 0;
            $user_data[$cnt]['workspace_upload_counts'] = !empty($user['workspace_upload_counts']['workspace_upload_counts']) ? $user['workspace_upload_counts']['workspace_upload_counts'] : 0;
            $user_data[$cnt]['library_upload_counts'] = !empty($user['library_upload_counts']['library_upload_counts']) ? $user['library_upload_counts']['library_upload_counts'] : 0;
            $user_data[$cnt]['shared_upload_counts'] = !empty($user['shared_upload_counts']['shared_upload_counts']) ? $user['shared_upload_counts']['shared_upload_counts'] : 0;
            $user_data[$cnt]['library_shared_upload_counts'] = !empty($user['library_shared_upload_counts']['library_shared_upload_counts']) ? $user['library_shared_upload_counts']['library_shared_upload_counts'] : 0;
            $user_data[$cnt]['videos_viewed_count'] = !empty($user['videos_viewed_count']['videos_viewed_count']) ? $user['videos_viewed_count']['videos_viewed_count'] : 0;
            $user_data[$cnt]['workspace_videos_viewed_count'] = !empty($user['workspace_videos_viewed_count']['workspace_videos_viewed_count']) ? $user['workspace_videos_viewed_count']['workspace_videos_viewed_count'] : 0;
            $user_data[$cnt]['library_videos_viewed_count'] = !empty($user['library_videos_viewed_count']['library_videos_viewed_count']) ? $user['library_videos_viewed_count']['library_videos_viewed_count'] : 0;
            $user_data[$cnt]['huddle_created_count'] = !empty($user['huddle_created_count']['huddle_created_count']) ? $user['huddle_created_count']['huddle_created_count'] : 0;
            $user_data[$cnt]['comments_initiated_count'] = !empty($user['comments_initiated_count']['comments_initiated_count']) ? $user['comments_initiated_count']['comments_initiated_count'] : 0;
            $user_data[$cnt]['workspace_comments_initiated_count'] = !empty($user['workspace_comments_initiated_count']['workspace_comments_initiated_count']) ? $user['workspace_comments_initiated_count']['workspace_comments_initiated_count'] : 0;
            $user_data[$cnt]['replies_initiated_count'] = !empty($user['replies_initiated_count']['replies_initiated_count']) ? $user['replies_initiated_count']['replies_initiated_count'] : 0;
            $user_data[$cnt]['documents_uploaded_count'] = !empty($user['documents_uploaded_count']['documents_uploaded_count']) ? $user['documents_uploaded_count']['documents_uploaded_count'] : 0;
            $user_data[$cnt]['documents_viewed_count'] = !empty($user['documents_viewed_count']['documents_viewed_count']) ? $user['documents_viewed_count']['documents_viewed_count'] : 0;
            $user_data[$cnt]['web_login_counts'] = !empty($user['web_login_counts']['web_login_counts']) ? $user['web_login_counts']['web_login_counts'] : 0;
            $user_data[$cnt]['scripted_observations'] = !empty($user['scriptede_observations']['scripted_observations']) ? $user['scripted_observations']['scripted_observations'] : 0;
            $user_data[$cnt]['scripted_video_observations'] = !empty($user['scripted_video_observations']['scripted_video_observations']) ? $user['scripted_video_observations']['scripted_video_observations'] : 0;
            $user_data[$cnt]['total_hours_uploaded'] = !empty($user[0]['total_document_durations']) ? $user[0]['total_document_durations'] : 0;
            $user_data[$cnt]['total_hours_viewed'] = !empty($user['dv_histories']['minutes_watched']) ? $user['dv_histories']['minutes_watched'] : 0;
//            $total_commments_adde = $total_commments_adde + ($user_data[$cnt]['comments_initiated_count'] + $usr_data[$cnt]['workspace_comments_initiated_count'] + $user_data[$cnt]['replies_initiated_count']);
            $user_role_details = $this->UserAccount->find('first', array('conditions' => array('account_id' => $user['usr']['account_id'], 'user_id' => $user['usr']['user_id'])));

            $assessment_huddles = !empty($user['assessment_huddle']['total_assess_huddle']) ? $user['assessment_huddle']['total_assess_huddle'] : 0;
            $coaching_huddles = !empty($user['coaching_huddle']['total_coaching_huddle']) ? $user['coaching_huddle']['total_coaching_huddle'] : 0;
            $collaboration_huddles = !empty($user['collab_huddles']['total_collab_huddle']) ? $user['collab_huddles']['total_collab_huddle'] : 0;
            $assessment_permission = $user['user_accounts']['manage_evaluation_huddles'];
            $coaching_permission = $user['user_accounts']['manage_coach_huddles'];
            $collaboration_permission = $user['user_accounts']['manage_collab_huddles'];
            $is_user_coachee = !empty($user['is_user_coache_details']['is_coachee']) ? $user['is_user_coache_details']['is_coachee'] : 0;

            if ($user['user_accounts']['role_id'] == 100) {
                $user_role = 'Account Owner';
            } elseif ($user['user_accounts']['role_id'] == 110) {
                $user_role = 'Super Admin';
            } elseif ($user['user_accounts']['role_id'] == 120) {
                $user_role = 'User';
            } elseif ($user['user_accounts']['role_id'] == 115) {
                $user_role = 'Admin';
            } elseif ($user['user_accounts']['role_id'] == 125) {
                $user_role = 'Viewer';
            } else {
                $user_role = '';
            }
            $user_data[$cnt]['user_role'] = !empty($user_role) ? $user_role : '';
            //echo $user[0]['account_name'] . ":" . $user_role_details['UserAccount']['role_id'] . "<br/>";

            $user_type = 'Participant';
            if ($collaboration_permission && !$coaching_permission && !$assessment_permission && ($user['user_accounts']['role_id'] == 120 || $user['user_accounts']['role_id'] == 115)) {
                $user_type = 'Collaborator';
            } if ($coaching_permission && !$assessment_permission && ($user['user_accounts']['role_id'] == 120 || $user['user_accounts']['role_id'] == 115)) {
                $user_type = 'Coach';
            } if (!$coaching_permission && $assessment_permission && ($user['user_accounts']['role_id'] == 120 || $user['user_accounts']['role_id'] == 115)) {
                $user_type = 'Assessor';
            } if ($coaching_permission && $assessment_permission && ($user['user_accounts']['role_id'] == 120 || $user['user_accounts']['role_id'] == 115)) {
                $user_type = 'Coach/Assessor';
            } if (!$collaboration_permission && !$coaching_permission && !$assessment_permission && ($user['user_accounts']['role_id'] == 120 || $user['user_accounts']['role_id'] == 115)) {
                $user_type = 'Participant';
            } if ($coaching_huddles && !$collaboration_permission && !$coaching_permission && !$assessment_permission && ($user['user_accounts']['role_id'] == 120 || $user['user_accounts']['role_id'] == 115)) {
                $user_type = 'Coachee';
            } if ($assessment_huddles && !$collaboration_permission && !$coaching_permission && !$assessment_permission && ($user['user_accounts']['role_id'] == 120 || $user['user_accounts']['role_id'] == 115)) {
                $user_type = 'Coachee/Assessed Participant';
            } if ($eval_active && ($user['user_accounts']['role_id'] == 100 || $user['user_accounts']['role_id'] == 110)) {
                $user_type = 'Coach/Assessor';
            } if (!$eval_active && ($user['user_accounts']['role_id'] == 100 || $user['user_accounts']['role_id'] == 110)) {
                $user_type = 'Coach';
            }


            $user_data[$cnt]['user_type'] = !empty($user_type) ? $user_type : '';

            $user_data[$cnt]['detail'] = 'detail';
            $cnt++;
        endforeach;

//        echo "Total Comments " . $total_commments_adde;
        $user_data_frameworks = array();
//        $frameworks = $this->AccountTag->find("all", array("conditions" => array(
//                "tag_type" => array(0, 2), "account_id" => $account_id
//        )));
        $frameworks = $this->AccountTag->find("all", array(
            "conditions" => array(
                "tag_type" => array(0, 2),
                "account_id" => $account_id,
            ),
            'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'a.id = AccountTag.account_id'
                )
            ),
            'fields' => array(
                'AccountTag.*',
                'a.company_name'
            )
        ));
        if (!empty($frameworks) && count($frameworks) > 0) {
            $user_data_frameworks['frameworks'] = $frameworks;
        }
        $user_data_frameworks['account_id'] = $account_id;
        $result['total'] = ceil($total_users / 10);
        $result['page'] = $page;
        $result['records'] = $total_users;
        $result['rows'] = $user_data;
        echo json_encode($result);
        die;
    }

    function accountUsersAnalytics($request_type = false) {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $view = new View($this, false);
        $account_ids = array();
        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];

        if (!empty($this->data['subAccount'])) {
            $account_ids[] = $this->data['subAccount'];
        } else {
            $account_ov_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'id' => $account_id
                ),
                'fields' => array('id')
            ));

            $account_overview_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'parent_account_id' => $account_id
                ),
                'fields' => array('id')
            ));
            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $account_ids[] = $all_ac['Account']['id'];
            }
        }

        $st_date = '';
        $end_date = '';
        if ($this->request->is('post')) {
            if (!empty($this->request->data['startDate']) && $this->request->data['filter_type'] == 'default') {
                $start_date = $this->data['startDate'] . '-01';
                if ($this->data['duration'] == 1) {
                    $qu = 2;
                } elseif ($this->data['duration'] == 2) {
                    $qu = 5;
                } elseif ($this->data['duration'] == 3) {
                    $qu = 8;
                } elseif ($this->data['duration'] == 4) {
                    $qu = 11;
                }
                $start_date = $this->data['startDate'] . '-01 00:00:00';
                $st_date = $start_date;
                $end_date = date('Y-m-d', strtotime($start_date . "+$qu month"));
                $end_date = date_create($end_date);
                $end_date = date_format($end_date, 'Y-m-t') . ' 23:59:59';
            } elseif (!empty($this->request->data['start_date']) && !empty($this->request->data['end_date']) && $this->request->data['filter_type'] == 'custom') {
                $start_date = $this->request->data['start_date'];
                $stDate = date_create($start_date);
                $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

                $end_date = $this->request->data['end_date'];
                $endDate = date_create($end_date);
                $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
            } else {
                if (date('m') >= $acct_duration_start) {
                    $yr = date('Y');
                    $dd = $yr . '-' . $acct_duration_start . '-01';
                } else {
                    $yr = date('Y') - 1;
                    $dd = $yr . '-' . $acct_duration_start . '-01';
                }
                $stDate = date_create($dd);
                $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
                $d = date('Y-m-d', strtotime($start_date . "+11 month"));
                $endDate = date_create($d);
                $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
            }
        } else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

            $d = date('Y-m-d', strtotime($st_date . "+11 month"));
            $endDate = date_create($d);
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        }

        $user_data_frameworks = array();
        $frameworks = $this->AccountTag->find("all", array(
            "conditions" => array(
                "tag_type" => array(0, 2),
                "account_id" => $account_id,
            ),
            'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'a.id = AccountTag.account_id'
                )
            ),
            'fields' => array(
                'AccountTag.*',
                'a.company_name'
            )
        ));

        if (!empty($frameworks) && count($frameworks) > 0) {
            $user_data_frameworks['frameworks'] = $frameworks;
        }
        $user_data_frameworks['account_id'] = $account_id;
        $st_date = explode(' ', $start_date);
        $end_date = explode(' ', $end_date);
        $date = new DateTime($st_date[0]);
        $st_date = $date->format('Y-m-d');
        $endDate = new DateTime($end_date[0]);
        $end_date = $endDate->format('Y-m-d');
        $analytics_result = array(
            'user_data' => '',
            'user_data_frameworks' => $user_data_frameworks,
            'start_date' => $st_date,
            'end_dates' => $end_date,
            'request_type' => $request_type
        );

        $html = $view->element('analytics/accountUsersAnalytics', $analytics_result);
        return $html;
    }

    public function get_user_type($account_id, $user_id) {
        $view = new View($this, false);
        $eval_active = $view->Custom->check_if_eval_huddle_active($account_id);
        $assessment_huddles_details = $this->UserAccount->query("SELECT
                                                                    *
                                                                  FROM
                                                                    account_folders AS af
                                                                    JOIN `account_folders_meta_data` AS afmd
                                                                      ON afmd.`account_folder_id` = af.`account_folder_id`
                                                                      JOIN `account_folder_users` AS afu
                                                                      ON afu.`account_folder_id` = af.`account_folder_id`
                                                                  WHERE afmd.`meta_data_name` = 'folder_type'
                                                                    AND afmd.`meta_data_value` = 3
                                                                    AND af.`account_id` = " . $account_id . "
                                                                    AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                                                                    AND afu.`user_id` = " . $user_id);
        $coaching_huddles_details = $this->UserAccount->query("SELECT
                                                                    *
                                                                  FROM
                                                                    account_folders AS af
                                                                    JOIN `account_folders_meta_data` AS afmd
                                                                      ON afmd.`account_folder_id` = af.`account_folder_id`
                                                                      JOIN `account_folder_users` AS afu
                                                                      ON afu.`account_folder_id` = af.`account_folder_id`
                                                                  WHERE afmd.`meta_data_name` = 'folder_type'
                                                                    AND afmd.`meta_data_value` = 2
                                                                    AND af.`account_id` = " . $account_id . "
                                                                    AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                                                                    AND afu.`user_id` = " . $user_id);
        $collaboration_huddles_details = $this->UserAccount->query("SELECT
                                                                    *
                                                                  FROM
                                                                    account_folders AS af
                                                                    JOIN `account_folders_meta_data` AS afmd
                                                                      ON afmd.`account_folder_id` = af.`account_folder_id`
                                                                      JOIN `account_folder_users` AS afu
                                                                      ON afu.`account_folder_id` = af.`account_folder_id`
                                                                  WHERE afmd.`meta_data_name` = 'folder_type'
                                                                    AND afmd.`meta_data_value` = 1
                                                                    AND af.`account_id` = " . $account_id . "
                                                                    AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
                                                                    AND afu.`user_id` = " . $user_id);

        if (!empty($assessment_huddles_details)) {
            $assessment_huddles = 1;
        } else {
            $assessment_huddles = 0;
        }

        if (!empty($coaching_huddles_details)) {
            $coaching_huddles = 1;
        } else {
            $coaching_huddles = 0;
        }

        if (!empty($collaboration_huddles_details)) {
            $collaboration_huddles = 1;
        } else {
            $collaboration_huddles = 0;
        }


        $user_role_details = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));

        $assessment_permission = isset($user_role_details['UserAccount']['manage_evaluation_huddles']) ? $user_role_details['UserAccount']['manage_evaluation_huddles'] : false;
        $coaching_permission = isset($user_role_details['UserAccount']['manage_coach_huddles']) ? $user_role_details['UserAccount']['manage_coach_huddles'] : false;
        $collaboration_permission = isset($user_role_details['UserAccount']['manage_collab_huddles']) ? $user_role_details['UserAccount']['manage_collab_huddles'] : false;

        $is_user_coachee_details = $this->UserAccount->query("SELECT
                        *
                        FROM
                        account_folders AS af
                        INNER JOIN account_folder_users afu
                          ON af.`account_folder_id` = afu.`account_folder_id`
                          INNER JOIN `account_folders_meta_data` afmd ON afmd.`account_folder_id` = af.`account_folder_id`
                        WHERE af.`account_id` = " . $account_id . "
                        AND afu.`role_id` = '210'
                        AND afmd.`meta_data_name` = 'folder_type'
                        AND afmd.`meta_data_value` = '2'
                        AND afu.`is_mentee` = 1
                        AND afu.`user_id` = " . $user_id);

        if (!empty($is_user_coachee_details)) {
            $is_user_coachee = 1;
        } else {
            $is_user_coachee = 0;
        }
        $user_type = 'Participant';
        if ($collaboration_permission && !$coaching_permission && !$assessment_permission && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $user_type = 'Collaborator';
        } if ($coaching_permission && !$assessment_permission && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $user_type = 'Coach';
        } if (!$coaching_permission && $assessment_permission && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $user_type = 'Assessor';
        } if ($coaching_permission && $assessment_permission && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $user_type = 'Coach/Assessor';
        } if (!$collaboration_permission && !$coaching_permission && !$assessment_permission && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $user_type = 'Participant';
        } if ($coaching_huddles && !$collaboration_permission && !$coaching_permission && !$assessment_permission && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $user_type = 'Coachee';
        } if ($assessment_huddles && !$collaboration_permission && !$coaching_permission && !$assessment_permission && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $user_type = 'Coachee/Assessed Participant';
        } if ($eval_active && ($user_role_details['UserAccount']['role_id'] == 100 || $user_role_details['UserAccount']['role_id'] == 110)) {
            $user_type = 'Coach/Assessor';
        } if (!$eval_active && ($user_role_details['UserAccount']['role_id'] == 100 || $user_role_details['UserAccount']['role_id'] == 110)) {
            $user_type = 'Coach';
        }
        return $user_type;
    }

    function getFramework($account_id = false) {
        $user = $this->Session->read('user_current_account');
        $user_id = $user['User']['id'];
        if (isset($this->data['framework_id']) && $this->data['framework_id'] != '') {
            $account_id =$this->data['account_id'];
            $framework_id =$this->data['framework_id'] ;
        } elseif ($account_id == false) {
            $account_id = $user['accounts']['account_id'];
        }
        $standards = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 0,
                "framework_id" => $framework_id
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code")
        ));



        $standards_array = array();
        if ($standards) {
            foreach ($standards as $st) {
                if (empty($st['AccountTag']['tads_code']))
                    $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                else
                    $ac_tag_title = $st['AccountTag']['tads_code'];
                $standard_array[] = array(
                    "value" => $st['AccountTag']['account_tag_id'],
                    "label" => $ac_tag_title
                );
            }
        }

        echo json_encode($standard_array);
        exit;
    }

    function play_card($account_id = '', $user_id = '', $start_date = '', $end_date = '', $folder_type = 2) {
        return $this->redirect('/analytics_angular/playcard/'.$account_id.'/'.$user_id.'/'.$start_date.'/'.$end_date.'/'.$folder_type , 301);
        $current_logged_in_account_id = $account_id;
        $this->Session->write('account_id', $current_logged_in_account_id);

        $user_permissions = $this->Session->read('user_permissions');
        $analytics_permission = $user_permissions['UserAccount']['permission_view_analytics'];


        if ($analytics_permission == 0 && $this->Session->read('role') == '120') {
// $this->no_permissions();
            $this->redirect('/dashboard/home/');
        }

        if ($account_id == '' || $user_id == '' || $start_date == '' || $end_date == '') {
            $this->Session->setFlash('Please select correct filters. ', 'default', array('class' => 'message error'));
            $this->redirect('/analytics');
        }
        $user_current_account = $this->Session->read('user_current_account');
        if ($user_id == '') {
            $user_id = $user_current_account['User']['id'];
        }
        if ($account_id == '') {
            $account_id = $user_current_account['accounts']['account_id'];
        }


        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));

        $user_result = $this->User->find('first', array(
            'joins' => array(
                array(
                    'table' => 'users_accounts as ua',
                    'conditions' => 'ua.user_id = User.id',
                    'type' => 'inner',
                ),
                array(
                    'table' => 'roles',
                    'conditions' => 'ua.role_id = roles.role_id',
                    'type' => 'inner'
                )
            ),
            'conditions' => array(
                'User.id' => $user_id,
                'ua.account_id' => $account_id
            ),
            'fields' => array('User.*', 'ua.*', 'roles.*')
        ));

        $last_login = $this->UserActivityLog->find('first', array(
            'conditions' => array(
                'UserActivityLog.user_id' => $user_id,
                'UserActivityLog.account_id' => $account_id,
                'UserActivityLog.type' => 9
            ),
            'fields' => 'MAX(date_added) AS last_login'
        ));

        $last_login = date_create($last_login[0]['last_login']);
//        $frameworks = $this->AccountTag->find("all", array("conditions" => array(
//                "tag_type" => '2', "account_id" => $account_id
//        )));

        $account_id_for_filter = '';
        $filter_data = $this->Session->read('filters_data');
        if (isset($filter_data['subAccount']) && $filter_data['subAccount'] != '') {
            $account_id_for_filter = $filter_data['subAccount'];
        } else {
            $user_current_account = $this->Session->read('user_current_account');
            $parent_account_id = $user_current_account['accounts']['account_id'];
            $account_ov_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'id' => $account_id
                ),
                'fields' => array('id')
            ));
            $account_overview_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'parent_account_id' => $parent_account_id
                ),
                'fields' => array('id')
            ));


            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $account_id_for_filter[] = $all_ac['Account']['id'];
            }
        }


        $frameworks = $this->AccountTag->find("all", array(
            "conditions" => array(
                "tag_type" => '2',
                "account_id" => $account_id_for_filter,
            ),
            'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'a.id = AccountTag.account_id'
                )
            ),
            'fields' => array(
                'AccountTag.*',
                'a.company_name'
            )
        ));


        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
            $frm_wrk = array('framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
        }

        $standards = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 0,
                $frm_wrk
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code", "tag_html")
        ));

        $standard_array = array();
        if ($standards) {
            foreach ($standards as $st) {
                if (!empty($st['AccountTag']['tag_html'])) {
                    if (empty($st['AccountTag']['tads_code']))
                        $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                    else
                        $ac_tag_title = $st['AccountTag']['tads_code'];
                    $standard_array[] = array(
                        "value" => $st['AccountTag']['account_tag_id'],
                        "label" => $ac_tag_title
                    );
                }
            }
        }
        $mt_arr = array();
        $count = 1;

        foreach ($metric_old as $mt_old) {
            $mt_arr[] = $count . '-' . substr($mt_old['AccountMetaData']['meta_data_name'], 13);
            $count++;
        }
        $huddle_ids = array();

        $view = new View($this, false);
        $user_type = $this->get_user_type($account_id, $user_id);
        $drop_down = array();
        $view_dropdown = array();
        $user_role_details = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $user_ids = array();
        if ($user_type == 'Collaborator' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
            );
            $user_ids = array($user_id);
            $folder_type = 1;
        } elseif ($user_type == 'Coach' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
            );
            $view_dropdown = array(
                '1' => 'Coach Perspective',
                '2' => 'Coachee Perspective'
            );
            $folder_type = 2;
            if ($folder_type == 1) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 2) {
                $user_ids = array($user_id);
            }
        } elseif ($user_type == 'Assessor' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '3' => 'Assessment Huddles',
            );
            $folder_type = 1;
            $user_ids = array($user_id);
        } elseif ($user_type == 'Coach/Assessor' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
                '3' => 'Assessment Huddles',
            );
            $view_dropdown = array(
                '1' => 'Coach Perspective',
                '2' => 'Coachee Perspective'
            );
            $folder_type = 2;
            $user_ids = array($user_id);
        } elseif ($user_type == 'Participant' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
            );
            $user_ids = array($user_id);
            $folder_type = 1;
        } elseif ($user_type == 'Coachee' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
            );
            $folder_type = 2;
            $user_ids = array($user_id);
        } elseif ($user_type == 'Coachee/Assessed Participant' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
                '3' => 'Assessment Huddles',
            );
            $folder_type = 2;
            $user_ids = array($user_id);
        } elseif ($user_type == 'Coach/Assessor' && ($user_role_details['UserAccount']['role_id'] == 100 || $user_role_details['UserAccount']['role_id'] == 110)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
                '3' => 'Assessment Huddles',
            );
            $view_dropdown = array(
                '1' => 'Coach Perspective',
                '2' => 'Coachee Perspective'
            );
            $folder_type = 2;
            $user_ids = array($user_id);
        } elseif ($user_type == 'Coach' && ($user_role_details['UserAccount']['role_id'] == 100 || $user_role_details['UserAccount']['role_id'] == 110)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
            );
            $view_dropdown = array(
                '1' => 'Coach Perspective',
                '2' => 'Coachee Perspective'
            );
            $folder_type = 2;
            $user_ids = array($user_id);
        }
        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $current_logged_in_account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $mt_arr = array();
        $count = 1;
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = $count . '-' . substr($mt_old['AccountMetaData']['meta_data_name'], 13);
            $count++;
        }
        $mt_arr = array();
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = substr($mt_old['AccountMetaData']['meta_data_name'], 13);
        }
        $assessments = array(0 => 'No Assessment');
        $arr_all = array_merge($assessments, $mt_arr);

        $total_huddles = $this->get_total_huddles_by_user_id($account_id, $user_ids, $folder_type, $start_date, $end_date);

        $frequency_of_tagged_standars = $this->frequency_of_tagged_standars_2($account_id, $user_ids, $start_date, $end_date, $user_type, $user_role_details['UserAccount']['role_id'], $folder_type, $huddle_ids);
        $account_overview = $this->account_overview_by_user($account_id, $user_id, $start_date, $end_date, $folder_type);
        $this->set('frequency_tagged_standards', $frequency_of_tagged_standars);
        $this->set('user_type', $this->get_user_type($account_id, $user_id));
        $this->set('standards', json_encode($standard_array));
        $this->set('selected_account', $account_id);
        $this->set('start_date', $start_date);
        $this->set('end_date', $end_date);
        $this->set('current_logged_in_account_id', $current_logged_in_account_id);
        $this->set('user_id', $user_id);
        $this->set('last_login', $last_login);
        $this->set('folder_type', $folder_type);
        $this->set('user', $user_result);
        $this->set('ratings', $mt_arr);
        $this->set('total_huddles', $total_huddles);
        $this->set('account_overview', $account_overview);
        $this->set('filter_drodown', $drop_down);
        $this->set('view_dropdown', $view_dropdown);
        $this->set('assessment_array', $arr_all);
        $this->render('play_card');
    }

    function account_overview_by_user($account_id, $user_id, $start_date = '', $end_date = '', $folder_type) {
        $user_permissions = $this->Session->read('user_permissions');
        $collab_permission = $user_permissions['UserAccount']['manage_collab_huddles'];
        $coach_permission = $user_permissions['UserAccount']['manage_coach_huddles'];
        $eval_permission = $user_permissions['UserAccount']['manage_evaluation_huddles'];
        if ($start_date == '' && $end_date == '') {
            $start_date = date('Y-m-01', strtotime('-3 months'));
            $stDate = date_create($start_date);
            $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
            $endDate = date_create(date('Y-m-d'));
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        } else {
            $start_date = $start_date . ' 00:00:00';
            $end_date = $end_date . ' 23:59:59';
        }

        if (!empty($this->request->data['folder_type'])) {
            $folder_type = $this->request->data['folder_type'];
        }


        if ($collab_permission == 1 && $coach_permission == 0 && $eval_permission == 0 && $this->Session->read('role') == '120') {
            $folder_type = '1';
        }

        if ($collab_permission == 0 && $coach_permission == 1 && $eval_permission == 0 && $this->Session->read('role') == '120') {
            $folder_type = '2';
        }

        if ($collab_permission == 0 && $coach_permission == 0 && $eval_permission == 1 && $this->Session->read('role') == '120') {
            $folder_type = '3';
        }

        if ($collab_permission == 1 && $coach_permission == 1 && $eval_permission == 0 && $this->Session->read('role') == '120') {
            $folder_type = '2';
        }

        if ($collab_permission == 1 && $coach_permission == 0 && $eval_permission == 1 && $this->Session->read('role') == '120') {
            $folder_type = '1';
        }

        if ($collab_permission == 0 && $coach_permission == 1 && $eval_permission == 1 && $this->Session->read('role') == '120') {
            $folder_type = '2';
        }

        /* --------Total Huddles-------- */
        $user = $this->Session->read('user_current_account');
        $bool = 0;
        $filter_data = $this->Session->read('filters_data');
        if (isset($filter_data['subAccount']) && $filter_data['subAccount'] != '') {
            $account_id = $filter_data['subAccount'];
            $bool = 0;
        } else {
            $account_id = $user['accounts']['account_id'];
            $bool = 1;

        }
        $all_account_ids = '';

        if (isset($filter_data['subAccount']) && $filter_data['subAccount'] != '') {
            $all_account_ids = $filter_data['subAccount'];
        } else {
            $user_current_account = $this->Session->read('user_current_account');
            $parent_account_id = $user_current_account['accounts']['account_id'];
            $account_ov_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'id' => $parent_account_id
                ),
                'fields' => array('id')
            ));
            $account_overview_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'parent_account_id' => $parent_account_id
                ),
                'fields' => array('id')
            ));


            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $all_account_ids[] = $all_ac['Account']['id'];
            }
        }

        $total_huddles_results = $this->UserAccount->query(""
                . "SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs` join account_folders af ON af.account_folder_id = user_activity_logs.account_folder_id   "
                . "WHERE af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) and `type` = 1 AND date_added >= '$start_date' AND date_added <= '$end_date' AND user_id = $user_id AND user_activity_logs.account_id IN (" . $account_id . ")");

        $huddle_created_count = 0;

        foreach ($total_huddles_results as $result) {

            $huddle_created_count += $result[0]['huddle_created_count'];
        }

//        $child_accounts = $this->Account->find('all', array(
//            'conditions' => array(
//                'parent_account_id' => $account_id
//            )  )
//        );
//        if ($child_accounts) {
//            foreach ($child_accounts as $child_account) {
//                $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs` join account_folders af ON af.account_folder_id = user_activity_logs.account_folder_id   WHERE af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) and `type` = 1 AND user_id = $user_id AND  date_added >= '$start_date' AND date_added <= '$end_date' AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ")");
//
//                foreach ($total_huddles_results as $result) {
//
//                    $huddle_created_count += $result[0]['huddle_created_count'];
//                }
//            }
//        }


        $account_huddle_analytics = $huddle_created_count;
        if ($bool == 1) {
            $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs` join account_folders af ON af.account_folder_id = user_activity_logs.account_folder_id  WHERE af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) and `type` = 1 AND user_id = $user_id  AND user_activity_logs.account_id IN (" . $account_id . ")  and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'");
            $huddle_created_count = 0;
            foreach ($total_huddles_results as $result) {

                $huddle_created_count += $result[0]['huddle_created_count'];
            }

            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                ))
            );
            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {
                    $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs` join account_folders af ON af.account_folder_id = user_activity_logs.account_folder_id  WHERE af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) and `type` = 1 AND user_id = $user_id AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ")  and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'");
                    foreach ($total_huddles_results as $result) {
                        $huddle_created_count += $result[0]['huddle_created_count'];
                    }
                }
            }

            $account_huddle_analytics = $huddle_created_count;
        }


        $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
    users u join `users_accounts` ua on u.id = ua.user_id
    join accounts a on a.id = ua.account_id
    left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
    WHERE (ua.TYPE=2 OR ua.TYPE=4) AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND af.`folder_type` IN(1,3,2) AND ua.user_id = $user_id AND  ua.date_added >='" . $start_date . "' AND ua.date_added <= '" . $end_date . "' AND ua.account_id IN (" . $account_id . ") GROUP BY ua.user_id) d on d.user_id = u.id where ua.account_id=" . $account_id);

        $huddle_videos_count = 0;

        foreach ($total_videos_results as $result) {

            $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
        }
//        $child_accounts = $this->Account->find('all', array(
//            'conditions' => array(
//                'parent_account_id' => $account_id
//            )
//                )
//        );
//        if ($child_accounts) {
//            foreach ($child_accounts as $child_account) {
//
//                $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle  from
//            users u join `users_accounts` ua on u.id = ua.user_id
//            join accounts a on a.id = ua.account_id
//            left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua
//            LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
//            WHERE (ua.TYPE=2 OR ua.TYPE=4) AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND af.`folder_type` IN(1,3,2) AND ua.user_id = $user_id AND  ua.date_added >='" . $start_date . "' AND ua.date_added <= '" . $end_date . "' AND ua.account_id IN (" . $child_account['Account']['id'] . ") GROUP BY ua.user_id) d on d.user_id = u.id where ua.account_id=" . $child_account['Account']['id']);
//
//                foreach ($total_videos_results as $result) {
//
//                    $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
//                }
//            }
//        }


        $account_video_analytics = $huddle_videos_count;

        if ($bool == 1) {



            $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua
        LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
        WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1,3,2) AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.user_id = $user_id AND ua.account_id IN (" . $account_id . ")  AND  date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "' GROUP BY ua.user_id) d on d.user_id = u.id where ua.account_id=" . $account_id);

            $huddle_videos_count = 0;

            foreach ($total_videos_results as $result) {

                $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
            }




            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                )
                    )
            );
            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {


                    $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle  from
                users u join `users_accounts` ua on u.id = ua.user_id
                join accounts a on a.id = ua.account_id
                left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua
                LEFT JOIN account_folders af ON ua.`account_folder_id` = af.`account_folder_id`
                WHERE (ua.TYPE=2 OR ua.TYPE=4) AND  af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND af.`folder_type` IN(1,3,2) AND ua.user_id = $user_id AND ua.account_id IN (" . $child_account['Account']['id'] . ")  AND  ua.date_added >='" . $start_date . "' AND ua.date_added <= '" . $end_date . "' GROUP BY ua.user_id) d on d.user_id = u.id where ua.account_id=" . $child_account['Account']['id']);

                    foreach ($total_videos_results as $result) {

                        $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
                    }
                }
            }


            $account_video_analytics = $huddle_videos_count;
        }



        $total_videos_viewed_results = $this->UserAccount->query("select distinct count(ual.id) as 'Resources_Viewed' from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id = " . $account_id . "  and ual.type=11
        join account_folder_documents afd on afd.document_id = ual.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id
        where ua.user_id = $user_id  and af.folder_type IN (1,2,3) and   ual.date_added >='" . $start_date . "' AND ual.date_added <= '" . $end_date . "' AND ua.account_id = " . $account_id);
        $huddle_videos_count = 0;

        foreach ($total_videos_viewed_results as $result) {

            $huddle_videos_count += $result[0]['Resources_Viewed'];
        }

//        $child_accounts = $this->Account->find('all', array(
//            'conditions' => array(
//                'parent_account_id' => $account_id
//            )
//                )
//        );
//        if ($child_accounts) {
//            foreach ($child_accounts as $child_account) {
//                $total_videos_viewed_results = $this->UserAccount->query("select distinct count(ual.id) as 'Resources_Viewed' from
//        users u join `users_accounts` ua on u.id = ua.user_id
//        join accounts a on a.id = ua.account_id
//        left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id = " . $child_account['Account']['id'] . "  and ual.type=11
//        join account_folder_documents afd on afd.document_id = ual.ref_id
//        join account_folders af on af.account_folder_id = afd.account_folder_id
//        where ua.user_id = $user_id and af.folder_type IN (1,2,3) and ual.date_added >='" . $start_date . "' AND ual.date_added <= '" . $end_date . "' AND ua.account_id = " . $child_account['Account']['id']);
//
//                foreach ($total_videos_viewed_results as $result) {
//
//                    $huddle_videos_count += $result[0]['Resources_Viewed'];
//                }
//            }
//        }
        $account_video_viewed_analytics = $huddle_videos_count;

        if ($bool == 1) {

            $total_videos_viewed_results = $this->UserAccount->query("select distinct count(ual.id) as 'Resources_Viewed' from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id = " . $account_id . "  and ual.type=11
        join account_folder_documents afd on afd.document_id = ual.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id
        where  ua.user_id = $user_id and af.folder_type IN (1,2,3) and ual.date_added >='" . $start_date . "' AND ual.date_added <= '" . $end_date . "' AND ua.account_id = " . $account_id);

            $huddle_videos_count = 0;

            foreach ($total_videos_viewed_results as $result) {

                $huddle_videos_count += $result[0]['Resources_Viewed'];
            }




            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                )
                    )
            );
            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {


                    $total_videos_viewed_results = $this->UserAccount->query("select distinct count(ual.id) as 'Resources_Viewed' from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id = " . $child_account['Account']['id'] . "  and ual.type=11
        join account_folder_documents afd on afd.document_id = ual.ref_id
        join account_folders af on af.account_folder_id = afd.account_folder_id
         where ua.user_id = $user_id and af.folder_type IN (1,2,3) and ual.date_added >='" . $start_date . "' AND ual.date_added <= '" . $end_date . "' AND ua.account_id = " . $child_account['Account']['id']);

                    foreach ($total_videos_viewed_results as $result) {

                        $huddle_videos_count += $result[0]['Resources_Viewed'];
                    }
                }
            }


            $account_video_viewed_analytics = $huddle_videos_count;
        }



        if ($bool == 0) {

            $results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
                users u
                left join (
                SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                join comments on user_activity_logs.ref_id = comments.id
                join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
                WHERE TYPE IN (5) AND account_folders.folder_type IN(1,3) AND user_activity_logs.user_id = $user_id  AND  user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                ) d on d.user_id = u.id where u.is_active=1 AND u.id = $user_id  and u.type='Active'");

            $results_replies = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
                users u
                left join (
                SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                join comments on user_activity_logs.ref_id = comments.id
                WHERE  TYPE IN (8)   AND user_activity_logs.account_id IN (" . $account_id . ") AND user_activity_logs.user_id = $user_id and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                ) d on d.user_id = u.id where u.is_active=1 AND u.id = $user_id  and u.type='Active'");

            $huddle_videos_count = 0;
            $huddle_videos_replies_count = 0;
            foreach ($results as $result) {

                $huddle_videos_count += $result[0]['Total_Video_Comments'];
            }

            foreach ($results_replies as $replies) {
                $huddle_videos_replies_count += $replies[0]['Total_Video_Comments_Replies'];
            }
            $account_comments_analytics = $huddle_videos_count + $huddle_videos_replies_count;
        } else {

            $results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
            users u
            left join (
            SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
            join comments on user_activity_logs.ref_id = comments.id
            join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
            WHERE TYPE IN (5) AND account_folders.folder_type IN(1,3) AND user_activity_logs.user_id = $user_id   AND  user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
            ) d on d.user_id = u.id where u.is_active=1 AND u.id = $user_id  and u.type='Active'");

            $results_replies = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
            users u
            left join (
            SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
            join comments on user_activity_logs.ref_id = comments.id
            WHERE  TYPE IN (8) AND user_activity_logs.user_id = $user_id  AND   user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
            ) d on d.user_id = u.id  where u.is_active=1 AND u.id = $user_id  and u.type='Active' ");

            $huddle_videos_count = 0;
            $huddle_videos_replies_count = 0;
            foreach ($results as $result) {
                $huddle_videos_count += $result[0]['Total_Video_Comments'];
            }
            foreach ($results_replies as $replies) {
                $huddle_videos_replies_count += $replies[0]['Total_Video_Comments_Replies'];
            }

            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                ))
            );


            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {
                    $results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
                users u
                left join (
                SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM
                `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id
                join account_folders ON account_folders.account_folder_id = user_activity_logs.account_folder_id
                WHERE TYPE IN (5) AND user_activity_logs.user_id = $user_id AND account_folders.folder_type IN(1,3) AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                ) d on d.user_id = u.id where u.is_active=1 and u.type='Active' AND u.id = $user_id");
                    $results_replies = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments_Replies from
                    users u
                    left join (
                    SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
                    join comments on user_activity_logs.ref_id = comments.id
                    WHERE TYPE IN (8) AND user_activity_logs.user_id = $user_id  AND   user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ") and date_added >='" . $start_date . "' AND date_added <= '" . $end_date . "'
                    ) d on d.user_id = u.id  where u.is_active=1 AND u.id = $user_id  and u.type='Active'");

                    foreach ($results as $result) {
                        $huddle_videos_count += $result[0]['Total_Video_Comments'];
                    }
                    foreach ($results_replies as $replies) {
                        $huddle_videos_replies_count += $replies[0]['Total_Video_Comments_Replies'];
                    }
                }
            }




            $account_comments_analytics = $huddle_videos_count + $huddle_videos_replies_count;
        }
//total video watched
        $total_vide_duration_where = '';
        if ($start_date != '' && $end_date != '') {
            $total_vide_duration_where = "Document.created_date >= '$start_date' and Document.created_date <= '$end_date'";
        }
        $total_vide_duration = $this->Document->find('all', array(
            'joins' => array(
                array(
                    'table' => 'document_files as df',
                    'conditions' => 'Document.id = df.document_id',
                    'type' => 'inner'
                ),
                array(
                    'table' => '`user_activity_logs` as ua',
                    'conditions' => 'ua.ref_id = df.document_id',
                    'type' => 'inner'
                ),
                array(
                    'table' => '`users` as u',
                    'conditions' => 'ua.user_id = u.id',
                    'type' => 'left'
                ),
            ),
            'conditions' => array(
                'Document.account_id' => $all_account_ids,
                'Document.created_by' => $user_id,
                'ua.type IN (2,4) ',
                'u.is_active' => 1,
                'u.type' => 'Active',
                'ua.type IN (2,4) ',
                $total_vide_duration_where
            ),
            'fields' => 'SUM(df.duration) as total_duration'
        ));

        if (isset($total_vide_duration[0][0]['total_duration']) && $total_vide_duration[0][0]['total_duration'] > 0) {
            $total_duration_hours = round(($total_vide_duration[0][0]['total_duration'] / 60 ) / 60, 2);
        } else {
            $total_duration_hours = 0;
        }

        /* ---------------Total Video Duration------------------ */
        $total_video_watched_where = '';
        if ($start_date != '' && $end_date != '') {
            $total_video_watched_where = "dvh.created_date >= '$start_date' and dvh.created_date <= '$end_date'";
        }
        $total_video_watched = $this->User->find('all', array(
            'joins' => array(
                array(
                    'table' => 'document_viewer_histories as dvh',
                    'conditions' => 'dvh.user_id = User.id',
                    'type' => 'left'
                ),
                array(
                    'table' => 'account_folder_documents as afd',
                    'conditions' => 'afd.document_id = dvh.document_id',
                    'type' => 'inner'
                ),
            ),
            'conditions' => array(
                'dvh.account_id' => $all_account_ids,
                'dvh.user_id' => $user_id,
                $total_video_watched_where
            ),
            'group' => 'dvh.document_id',
            'fields' => 'max(dvh.minutes_watched) as minutes_watched'
        ));

        $total_min_watched = 0;
        if ($total_video_watched) {
            foreach ($total_video_watched as $row) {
                $total_min_watched = $total_min_watched + $row[0]['minutes_watched'];
            }
        }
        if ($total_min_watched > 0) {
            $total_min_watched = round(($total_min_watched / 60 ) / 60, 2);
        } else {
            $total_min_watched = 0;
        }


        $view = new View($this, false);
        $stDate = date_create($start_date);
        $endDate = date_create($end_date);

        $analytics_result = array(
            'total_account_huddles' => $account_huddle_analytics,
            'total_account_videos' => $account_video_analytics,
            'total_viewed_videos' => $account_video_viewed_analytics,
            'total_comments_added' => $account_comments_analytics,
            'total_video_duration' => $total_duration_hours,
            'total_min_watched' => $total_min_watched,
            'filter_date' => date_format($stDate, 'F d, Y') . ' - ' . date_format($endDate, 'F d, Y'),
            'is_single' => true,
            'folder_type' => $folder_type,
            'start_date' => date_format($stDate, 'Y-m-d'),
            'end_dates' => date_format($endDate, 'Y-m-d')
        );

        $account_overview_elem = $view->element('analytics/account_overview', $analytics_result, false);
        if ($this->request->is('post')) {
            echo json_encode(array('account_overview_elem' => $account_overview_elem));
            die;
        } else {
            return $account_overview_elem;
        }
    }

    function get_total_huddles_by_user_id($account_id, $user_id, $folder_type = '', $start_date = '', $end_date = '') {
        $created_by_conditions = '';

        if (count($user_id) > 0) {
            $user_id = "'" . implode(',', $user_id) . "'";
            $created_by_conditions = " AND  afu.user_id IN($user_id)";
        }


        if ($folder_type == 2 || $folder_type == 3) {
              $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs`"
                    . " inner join account_folders_meta_data as afmd on user_activity_logs.account_folder_id = afmd.account_folder_id"
                    . " INNER JOIN `account_folder_users` AS afu ON afu.`account_folder_id` = user_activity_logs.`account_folder_id`"
                    . "  WHERE `type` = 1 AND date_added >= '$start_date' AND date_added <= '$end_date' AND user_activity_logs.account_id IN (" . $account_id . ") $created_by_conditions ");

            $huddle_created_count = 0;

            foreach ($total_huddles_results as $result) {
                $huddle_created_count += $result[0]['huddle_created_count'];
            }

            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                ))
            );
            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {
                    $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs`  "
                            . " inner join account_folders_meta_data as afmd on user_activity_logs.account_folder_id = afmd.account_folder_id "
                            . " INNER JOIN `account_folder_users` AS afu ON afu.`account_folder_id` = user_activity_logs.`account_folder_id`"
                            . "WHERE `type` = 1 AND  date_added >= '$start_date' AND date_added <= '$end_date' $created_by_conditions AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ")  $created_by_conditions;");

                    foreach ($total_huddles_results as $result) {

                        $huddle_created_count += $result[0]['huddle_created_count'];
                    }
                }
            }

            return $huddle_created_count;
        } elseif ($folder_type == 1) {


            $total_huddles_results = $this->UserAccount->query("SELECT  
                CASE
                  WHEN meta_data_value = '2' 
                  THEN 'coaching' 
                  WHEN meta_data_value = '3' 
                  THEN 'evaluation' 
                  ELSE NULL                   
                END 
              FROM `user_activity_logs`
              INNER JOIN `account_folder_users` AS afu ON afu.`account_folder_id` = user_activity_logs.`account_folder_id`
              INNER JOIN account_folders_meta_data ON  account_folders_meta_data.account_folder_id = user_activity_logs.`account_folder_id` 
              WHERE `type` = 1 AND date_added >= '$start_date' AND date_added <= '$end_date' AND user_activity_logs.account_id IN (" . $account_id . ") $created_by_conditions ");

            $huddle_created_count = 0;
            foreach ($total_huddles_results as $result) {
                if ($result[0]['folderType'] != 'coaching' && $result[0]['folderType'] != 'evaluation') {
                    $huddle_created_count = $huddle_created_count + 1;
                }
            }
            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                ))
            );
            if ($child_accounts) {
                foreach ($child_accounts as $child_account) {
                    $total_huddles_results = $this->UserAccount->query(" 
              SELECT  
                CASE
                  WHEN meta_data_value = '2' 
                  THEN 'coaching' 
                  WHEN meta_data_value = '3' 
                  THEN 'evaluation' 
                  ELSE NULL                   
                END 
                FROM `user_activity_logs`
                 INNER JOIN `account_folder_users` AS afu ON afu.`account_folder_id` = user_activity_logs.`account_folder_id`
                  INNER JOIN account_folders_meta_data ON  account_folders_meta_data.account_folder_id = user_activity_logs.`account_folder_id`
                   WHERE `type` = 1 AND  date_added >= '$start_date' AND date_added <= '$end_date' $created_by_conditions AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ")");
                    foreach ($total_huddles_results as $result) {
                        if ($result[0]['folderType'] != 'coaching' && $result[0]['folderType'] != 'evaluation') {
                            $huddle_created_count = $huddle_created_count + 1;
                        }
                    }
                }
            }



            return $huddle_created_count;
        }
    }

    function get_total_huddles($account_id, $folder_type = '', $start_date = '', $end_date = '') {
        if ($folder_type != '') {
            $folder_type = " AND afmd.meta_data_name ='folder_type' AND  afmd.meta_data_value = $folder_type";
        } else {
            $folder_type = " AND afmd.meta_data_name ='folder_type' AND  afmd.meta_data_value = 2";
        }

        $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs`"
                . " inner join account_folders_meta_data as afmd on user_activity_logs.account_folder_id = afmd.account_folder_id"
                . "  WHERE `type` = 1 AND date_added >= '$start_date' AND date_added <= '$end_date' AND account_id IN (" . $account_id . ") $folder_type");

        $huddle_created_count = 0;

        foreach ($total_huddles_results as $result) {

            $huddle_created_count += $result[0]['huddle_created_count'];
        }

        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );
        if ($child_accounts) {
            foreach ($child_accounts as $child_account) {

                $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs`  inner join account_folders_meta_data as afmd on user_activity_logs.account_folder_id = afmd.account_folder_id "
                        . "WHERE `type` = 1 AND  date_added >= '$start_date' AND date_added <= '$end_date' AND account_id IN (" . $child_account['Account']['id'] . ") $folder_type");

                foreach ($total_huddles_results as $result) {

                    $huddle_created_count += $result[0]['huddle_created_count'];
                }
            }
        }


        return $huddle_created_count;
    }

    function ajax_filter() {
        if ($this->request->data['bool'] == 2) {
            $frequency_standards = $this->frequency_of_tagged_standars();
            $account_user_analytics = $this->accountUsersAnalytics(true);
            $frequency_standards['account_user_analytics'] = $account_user_analytics;
            $this->Session->write('filters_data', $this->request->data);
        } else {
            $frequency_standards = $this->frequency_of_tagged_standars();
            $account_user_analytics = $this->accountUsersAnalytics(true);
            $account_overview = $this->account_overview();
            $this->Session->write('filters_data', $this->request->data);
            $frequency_standards['account_user_analytics'] = $account_user_analytics;
            $frequency_standards['account_overview'] = $account_overview;
        }
        $st_date = '';
        $end_date = '';
        if ($this->request->is('post')) {
            if (!empty($this->request->data['startDate']) && $this->request->data['filter_type'] == 'default') {
                $start_date = $this->data['startDate'] . '-01';
                if ($this->data['duration'] == 1) {
                    $qu = 2;
                } elseif ($this->data['duration'] == 2) {
                    $qu = 5;
                } elseif ($this->data['duration'] == 3) {
                    $qu = 8;
                } elseif ($this->data['duration'] == 4) {
                    $qu = 11;
                }
                $start_date = $this->data['startDate'] . '-01 00:00:00';
                $st_date = $start_date;
                $end_date = date('Y-m-d', strtotime($start_date . "+$qu month"));
                $end_date = date_create($end_date);
                $end_date = date_format($end_date, 'Y-m-t') . ' 23:59:59';
            } elseif (!empty($this->request->data['start_date']) && !empty($this->request->data['end_date']) && $this->request->data['filter_type'] == 'custom') {
                $start_date = $this->request->data['start_date'];
                $stDate = date_create($start_date);
                $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

                $end_date = $this->request->data['end_date'];
                $endDate = date_create($end_date);
                $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
            } else {
                if (date('m') >= $acct_duration_start) {
                    $yr = date('Y');
                    $dd = $yr . '-' . $acct_duration_start . '-01';
                } else {
                    $yr = date('Y') - 1;
                    $dd = $yr . '-' . $acct_duration_start . '-01';
                }
                $stDate = date_create($dd);
                $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
                $d = date('Y-m-d', strtotime($start_date . "+11 month"));
                $endDate = date_create($d);
                $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
            }
        } else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

            $d = date('Y-m-d', strtotime($st_date . "+11 month"));
            $endDate = date_create($d);
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        }

        $st_date = explode(' ', $start_date);
        $end_date = explode(' ', $end_date);
        $date = new DateTime($st_date[0]);
        $st_date = $date->format('Y-m-d');
        $endDate = new DateTime($end_date[0]);
        $end_date = $endDate->format('Y-m-d');
        $frequency_standards['start_date'] = $st_date;
        $frequency_standards['end_dates'] = $end_date;
        echo json_encode($frequency_standards);
        die;
    }

    function ajax_single_filter($account_id, $user_id, $start_date, $end_date, $folder_type) {
//        $this->Session->write('account_id', $account_id);
        $coache_view_type = $this->request->data['coachee_view'];
        $huddle_ids = array();
        $account_folder_ids = array();
        $view = new View($this, false);
        $user_type = $this->get_user_type($account_id, $user_id);
        $drop_down = array();
        $view_dropdown = array();
        $user_role_details = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'user_id' => $user_id)));
        $user_ids = array();
        if ($user_type == 'Collaborator' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
            );
            $user_ids = array($user_id);
            $folder_type = 1;
        } elseif ($user_type == 'Coach' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
            );
            $view_dropdown = array(
                '1' => 'Coach Perspective',
                '2' => 'Coachee Perspective'
            );
            $folder_type = 2;
            if ($folder_type == 1) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 2) {
                $user_ids = array($user_id);
            }
        } elseif ($user_type == 'Assessor' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '3' => 'Assessment Huddles',
            );
            $folder_type = 1;
            if ($folder_type == 1) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 3) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'evaluation', $user_id);
                $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                if ($evaluator_ids == false) {
                    $evaluator_ids = array();
                }
                $user_ids = array_merge(array($user_id), $evaluator_ids);
            }
        } elseif ($user_type == 'Coach/Assessor' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
                '3' => 'Assessment Huddles',
            );
            $view_dropdown = array(
                '1' => 'Coach Perspective',
                '2' => 'Coachee Perspective'
            );
            $folder_type = 2;
            if ($folder_type == 1) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 2) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 3) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'evaluation', $user_id);
                $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                if ($evaluator_ids == false) {
                    $evaluator_ids = array();
                }
                $user_ids = array_merge(array($user_ids), $evaluator_ids);
            }
        } elseif ($user_type == 'Participant' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
            );
            $user_ids = array($user_id);
            $folder_type = 1;
        } elseif ($user_type == 'Coachee' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
            );
            $folder_type = 2;
            if ($folder_type == 1) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 2) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                if ($huddle_ids == false) {
                    $huddle_ids = array();
                }
                $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids, $user_id);
                if ($evaluator_ids == false) {
                    $evaluator_ids = array();
                }
                $user_ids = array_merge(array($user_id), $evaluator_ids);
            }
        } elseif ($user_type == 'Coachee/Assessed Participant' && ($user_role_details['UserAccount']['role_id'] == 120 || $user_role_details['UserAccount']['role_id'] == 115)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
                '3' => 'Assessment Huddles',
            );
            $folder_type = 2;
            if ($folder_type == 1) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 2) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                if ($evaluator_ids == false) {
                    $evaluator_ids = array();
                }
                $user_ids = array_merge(array($user_id), $evaluator_ids);
            } elseif ($folder_type == 3) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'evaluation', $user_id);
                $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                if ($evaluator_ids == false) {
                    $evaluator_ids = array();
                }
                $user_ids = array_merge(array($user_ids), $evaluator_ids);
            }
        } elseif ($user_type == 'Coach/Assessor' && ($user_role_details['UserAccount']['role_id'] == 100 || $user_role_details['UserAccount']['role_id'] == 110)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
                '3' => 'Assessment Huddles',
            );
            $view_dropdown = array(
                '1' => 'Coach Perspective',
                '2' => 'Coachee Perspective'
            );
            $folder_type = 2;
            if ($folder_type == 1) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 2) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 3) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'evaluation', $user_id);
                $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                if ($evaluator_ids == false) {
                    $evaluator_ids = array();
                }
                $user_ids = array_merge(array($user_ids), $evaluator_ids);
            }
        } elseif ($user_type == 'Coach' && ($user_role_details['UserAccount']['role_id'] == 100 || $user_role_details['UserAccount']['role_id'] == 110)) {
            $drop_down = array(
                '1' => 'Collaboration Huddles',
                '2' => 'Coaching Huddles',
            );
            $view_dropdown = array(
                '1' => 'Coach Perspective',
                '2' => 'Coachee Perspective'
            );
            $folder_type = 2;
            if ($folder_type == 1) {
                $user_ids = array($user_id);
            } elseif ($folder_type == 2) {
                $user_ids = array($user_id);
            }
        }
//        echo $user_role_details['UserAccount']['role_id'] . "<br/>";
//        echo "$account_id, $user_ids, $start_date, $end_date, $user_type, $folder_type, $huddle_ids";
        $frequency_standards = $this->frequency_of_tagged_standars_2($account_id, $user_ids, $start_date, $end_date, $user_type, $user_role_details['UserAccount']['role_id'], $folder_type, $huddle_ids);
//        die;
        $account_user_analytics = $this->account_overview_by_user($account_id, $user_id, $start_date, $end_date, $folder_type);
        $total_huddles = $this->get_total_huddles_by_user_id($account_id, $user_ids, $folder_type, $start_date, $end_date);

        $frequency_standards['account_user_analytics'] = $account_user_analytics;
        $frequency_standards['total_huddles'] = $total_huddles;
        echo json_encode($frequency_standards);
        die;
    }

    function get_account_folder_ids($account_id, $type = 'collaboration', $user_id) {
        $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, $type, $user_id);
        $account_folder_ids = array();
        if ($huddles) {
            foreach ($huddles as $row) {
                $account_folder_ids[] = $row['AccountFolder']['account_folder_id'];
            }
        }
        return $account_folder_ids;
    }

    function frequency_of_tagged_standars_2($account_id, $user_id, $start_date, $end_date, $user_type, $user_role, $folder_type = 2, $huddle_ids = false) {
        $users_ids = $user_id;
        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric['AccountMetaData']['meta_data_name'], 13)), 'value' => $metric['AccountMetaData']['meta_data_value']);
        }
        $user = $this->Session->read('user_current_account');
        $role_id = $user['users_accounts']['role_id'];
        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();
        $mATCnd_custom_markers = array();
        $account_ids = array();
        if (isset($this->request->data['folder_type']) && $this->request->data['folder_type'] != '') {
            $folder_type = $this->request->data['folder_type'];
        }
        $account_ov_analytics = $this->Account->find('all', array(
            'conditions' => array(
                'Account.is_active' => 1,
                'id' => $account_id
            ),
            'fields' => array('id')
        ));

        $account_overview_analytics = $this->Account->find('all', array(
            'conditions' => array(
                'Account.is_active' => 1,
                'parent_account_id' => $account_id
            ),
            'fields' => array('id')
        ));
        $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
        foreach ($all_accounts as $all_ac) {
            $account_ids[] = $all_ac['Account']['id'];
        }

        $search_by_standard = '';
        $durationGrph = 3;
        $account_id_for_filter = '';
        $filter_data = $this->Session->read('filters_data');
        if (isset($filter_data['subAccount']) && $filter_data['subAccount'] != '') {
            $account_id_for_filter = $filter_data['subAccount'];
        } else {
            $account_id_for_filter = $account_id;
        }
        $frameworks = $this->AccountTag->find("all", array(
            "conditions" => array(
                "tag_type" => '2',
                "account_id" => $account_id_for_filter,
            ),
            'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'a.id = AccountTag.account_id'
                )
            ),
            'fields' => array(
                'AccountTag.*',
                'a.company_name'
            )
        ));

        if (isset($this->request->data['search_by_standards']) && $this->request->data['search_by_standards'] != '') {
            $search_by_standard = $this->request->data['search_by_standards'];
        }

        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
        }
        if (!empty($this->data['framework_id'])) {
            $frm_wrk = array('framework_id' => $this->data['framework_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $this->data['framework_id']);
        } else {
            $frm_wrk = array('framework_id' => isset($frameworks[0]['AccountTag']['account_tag_id']) ? $frameworks[0]['AccountTag']['account_tag_id'] : '');
            $act_frm_wrk = array('AccountTag.framework_id' => isset($frameworks[0]['AccountTag']['account_tag_id']) ? $frameworks[0]['AccountTag']['account_tag_id'] : '');
        }

        $st_date = $start_date . ' 00:00:00';
        $end_date = $end_date . ' 23:59:59';

//Standard off
        $account_folder_ids = array();
        $user_id_c = $user_id;
        $view = new View($this, false);
        $custom_markers_chart = '';
        $serial_charts_conditions = '';
        $pl_conditions = '';
        $mATCnd_custom_markers[] = array('act.created_date >= ' => $st_date);
        $mATCnd_custom_markers[] = array('act.created_date <= ' => $end_date);
        if ($view->Custom->is_enabled_framework_and_standards($account_id)) {
            $mAUCnd[] = array('UserAccount.created_date >= ' => $st_date);
            $mAHCnd[] = array('AccountFolder.created_date >= ' => $st_date);
            $mAVCnd[] = array('d.created_date >= ' => $st_date);
            $mAVWCnd[] = array('UserActivityLog.date_added >= ' => $st_date);
            $mATCnd[] = array('act.created_date >= ' => $st_date);

            if (!empty($search_by_standard)) {
                $mATCnd[] = array('act.account_tag_id IN( ' . $search_by_standard . ')');
            }

            $mAUCnd[] = array('UserAccount.created_date <= ' => $end_date);
            $mAHCnd[] = array('AccountFolder.created_date <= ' => $end_date);
            $mAVCnd[] = array('d.created_date <= ' => $end_date);
            $mAVWCnd[] = array('UserActivityLog.date_added <= ' => $end_date);
            $mATCnd[] = array('act.created_date <= ' => $end_date);



            $start = (new DateTime($st_date))->modify('first day of this month');
            $end = (new DateTime($end_date))->modify('last day of this month');

            $interval = DateInterval::createFromDateString('1 month');
            $period = new DatePeriod($start, $interval, $end);

            $this->AccountTag->virtualFields['tags_date'] = 0;
            $this->AccountTag->virtualFields['total_tags'] = 0;
            $coach_inner_ids = array();
            $accountFolderIds = array();
            $account_tags_analytics = array();
            if ($user_type == 'Collaborator' && ($user_role == 120 || $user_role == 115)) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                $tagAccountFolderCond = '';
                if ($huddle_ids && count($huddle_ids) > 0) {
                    $account_folder_ids = implode(',', $huddle_ids);
                    $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                }
                $joins = array(
                    array(
                        'table' => 'account_comment_tags as act',
                        'type' => 'inner',
                        'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'act.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    ),
                );
                $conditions = array(
                    'AccountTag.tag_type' => 0,
                    'AccountTag.account_id' => $account_ids,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    $mATCnd,
                    $act_frm_wrk,
                    $tagAccountFolderCond,
                );
                $all_accounts = implode(',', $account_ids);
                $custom_conditions = array(
                    'AccountTag.tag_type' => 1,
                    'AccountTag.account_id' => $account_ids,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN(' . $all_accounts . ')) ',
                    $mATCnd_custom_markers,
                    $tagAccountFolderCond
                );
                $fields = array(
                    'AccountTag.tag_title',
                    'AccountTag.tads_code',
                    'AccountTag.account_tag_id',
                    'act.created_date AS AccountTag__tags_date',
                    'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                );
                if (empty($search_by_standard)) {
                    $get_value = array(
                        'joins' => $joins,
                        'conditions' => $conditions,
                        'fields' => $fields,
                        'group' => array('AccountTag.account_tag_id'),
                        'order' => 'AccountTag__total_tags DESC',
                        'limit' => 5
                    );
                } else {
                    $get_value = array(
                        'joins' => $joins,
                        'conditions' => $conditions,
                        'fields' => $fields,
                        'group' => array('AccountTag.account_tag_id'),
                        'order' => 'AccountTag__total_tags DESC',
                        'limit' => 5
                    );
                }
                $pl_conditions = false;
                $serial_charts_conditions = $conditions;
                $response_custom = $this->get_cutom_tags($custom_conditions);
                $data_1['custom_markers_tag'] = json_encode($response_custom);
                $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                $account_tags_analytics = $this->AccountTag->find('all', $get_value);
            } elseif ($user_type == 'Coach' && ($user_role == 120 || $user_role == 115)) {
                $accountFolderCond = '';
                $accountFolderIds = array();
                $coache_view_type = isset($this->request->data['coachee_view']) ? $this->request->data['coachee_view'] : 1;
                $user_ids = array();
                $tagConditions = '';
                if ($folder_type == 2) {
                    if ($coache_view_type == 1) {
                        $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                        if ($huddles) {
                            foreach ($huddles as $row) {
                                if ($row['huddle_users']['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                    $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                }
                            }
                        }

                        $user_ids = $user_id;
                        $joins = array(
                            array(
                                'table' => 'account_comment_tags as act',
                                'type' => 'inner',
                                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                            ),
                            array(
                                'table' => 'account_folder_documents as afd',
                                'type' => 'inner',
                                'conditions' => 'act.ref_id = afd.document_id'
                            ),
                            array(
                                'table' => 'account_folders_meta_data as afmd',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                            )
                        );
                        $conditions = array(
                            'AccountTag.tag_type' => 0,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            $mATCnd,
                            $act_frm_wrk,
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN ( ' . $all_accounts . ')) ',
                            $mATCnd_custom_markers,
                        );
                        $fields = array(
                            'AccountTag.tag_title',
                            'AccountTag.tads_code',
                            'AccountTag.account_tag_id',
                            'act.created_date AS AccountTag__tags_date',
                            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                        );
                        if (empty($search_by_standard)) {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        } else {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        }
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );
                        $serial_charts_conditions = $conditions;
                        if (count($accountFolderIds) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                    } else {
                        $user_ids = array();
                        $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                        $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                        if ($evaluator_ids == false) {
                            $evaluator_ids = array();
                        }
                        $user_ids = array_merge($user_id, $evaluator_ids);

                        $joins = array(
                            array(
                                'table' => 'account_comment_tags as act',
                                'type' => 'inner',
                                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                            ),
                            array(
                                'table' => 'account_folder_documents as afd',
                                'type' => 'inner',
                                'conditions' => 'act.ref_id = afd.document_id'
                            ),
                            array(
                                'table' => 'account_folders_meta_data as afmd',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                            ),
                            array(
                                'table' => 'account_folder_users as afu',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afu.account_folder_id'
                            )
                        );
                        $conditions = array(
                            'AccountTag.tag_type' => 0,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            $mATCnd,
                            $act_frm_wrk,
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN(' . $all_accounts . ')) ',
                            $mATCnd_custom_markers,
                        );
                        $fields = array(
                            'AccountTag.tag_title',
                            'AccountTag.tads_code',
                            'AccountTag.account_tag_id',
                            'act.created_date AS AccountTag__tags_date',
                            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                        );
                        if (empty($search_by_standard)) {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        } else {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        }
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $huddle_ids
                        );

                        $serial_charts_conditions = $conditions;
                        if (count($user_ids) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                        $tagConditions = '';
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);

                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $mATCnd,
                        $act_frm_wrk,
                        $tagAccountFolderCond,
                    );
                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $huddle_ids
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN( ' . $all_accounts . ')) ',
                        $tagAccountFolderCond,
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $pl_conditions = false;
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                }
            } elseif ($user_type == 'Assessor' && ($user_role == 120 || $user_role == 115)) {

                if ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
//                    var_dump($act_frm_wrk);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $accountFolderIds = $huddle_ids;
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $mATCnd,
                        $act_frm_wrk,
                        $tagAccountFolderCond,
                    );
                    $pl_conditions = array();
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN( ' . $all_accounts . '))',
                        $tagAccountFolderCond,
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }

                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                } elseif ($folder_type == 3) {
                    $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'evaluation', $user['User']['id']);
                    if ($huddles) {
                        foreach ($huddles as $row) {
                            if ($row['huddle_users']['role_id'] == 200 && $row[0]['folderType'] == 'evaluation') {
                                $coach_inner_ids[] = $row['huddle_users']['user_id'];
                                $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                $user_ids[] = $row['huddle_users']['user_id'];
                            }
                        }
                    }
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        $mATCnd,
                        $act_frm_wrk,
                    );
                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $accountFolderIds
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN( ' . $all_accounts . '))',
                        $mATCnd_custom_markers
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $serial_charts_conditions = $conditions;
                    if (count($accountFolderIds) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                    } else {

                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                }
            } elseif ($user_type == 'Coach/Assessor' && ($user_role == 120 || $user_role == 115)) {
                $accountFolderCond = '';
                $accountFolderIds = array();
                $coache_view_type = isset($this->request->data['coachee_view']) ? $this->request->data['coachee_view'] : 1;
                $user_ids = array();
                $tagConditions = '';
                if ($folder_type == 2) {
                    if ($coache_view_type == 1) {
                        $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                        if ($huddles) {
                            foreach ($huddles as $row) {
                                if ($row['huddle_users']['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                    $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                }
                            }
                        }

                        $user_ids = $user_id;
                        $joins = array(
                            array(
                                'table' => 'account_comment_tags as act',
                                'type' => 'inner',
                                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                            ),
                            array(
                                'table' => 'account_folder_documents as afd',
                                'type' => 'inner',
                                'conditions' => 'act.ref_id = afd.document_id'
                            ),
                            array(
                                'table' => 'account_folders_meta_data as afmd',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                            )
                        );
                        $conditions = array(
                            'AccountTag.tag_type' => 0,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            $mATCnd,
                            $act_frm_wrk,
                        );
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );

                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN(' . $all_accounts . '))',
                            $mATCnd_custom_markers,
                        );
                        $fields = array(
                            'AccountTag.tag_title',
                            'AccountTag.tads_code',
                            'AccountTag.account_tag_id',
                            'act.created_date AS AccountTag__tags_date',
                            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                        );
                        if (empty($search_by_standard)) {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        } else {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        }
                        $serial_charts_conditions = $conditions;
                        if (count($accountFolderIds) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                            if (count($account_tags_analytics) > 0) {

                            }
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                    } else {
                        $user_ids = array();
                        $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
//                        $accountFolderIds = $huddle_ids;
                        $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                        if ($evaluator_ids == false) {
                            $evaluator_ids = array();
                        }
                        $user_ids = array_merge($user_id, $evaluator_ids);

                        $joins = array(
                            array(
                                'table' => 'account_comment_tags as act',
                                'type' => 'inner',
                                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                            ),
                            array(
                                'table' => 'account_folder_documents as afd',
                                'type' => 'inner',
                                'conditions' => 'act.ref_id = afd.document_id'
                            ),
                            array(
                                'table' => 'account_folders_meta_data as afmd',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                            ),
                            array(
                                'table' => 'account_folder_users as afu',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afu.account_folder_id'
                            )
                        );
                        $conditions = array(
                            'AccountTag.tag_type' => 0,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            // 'afd.account_folder_id' => $accountFolderIds,
                            $mATCnd,
                            $act_frm_wrk,
                        );
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $huddle_ids
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.')) ',
                            $mATCnd_custom_markers
                        );
                        $fields = array(
                            'AccountTag.tag_title',
                            'AccountTag.tads_code',
                            'AccountTag.account_tag_id',
                            'act.created_date AS AccountTag__tags_date',
                            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                        );
                        if (empty($search_by_standard)) {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        } else {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        }
                        $serial_charts_conditions = $conditions;
                        if (count($user_ids) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                        } else {

                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                        $tagConditions = '';
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $accountFolderIds = $huddle_ids;
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $mATCnd,
                        $act_frm_wrk,
                        $tagAccountFolderCond,
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.')) ',
                        $tagAccountFolderCond,
                        $mATCnd_custom_markers
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $pl_conditions = array();
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                } elseif ($folder_type == 3) {
                    $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'evaluation', $user['User']['id']);
                    if ($huddles) {

                        foreach ($huddles as $row) {
                            if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'evaluation') {
                                $coach_inner_ids[] = $row['huddle_users']['user_id'];
                                $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                $user_ids[] = $row['huddle_users']['user_id'];
                            }
                        }
                    }
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        $mATCnd,
                        $act_frm_wrk,
                    );
                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $accountFolderIds
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $mATCnd_custom_markers
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $serial_charts_conditions = $conditions;
                    if (count($accountFolderIds) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                    } else {

                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                }
            } elseif ($user_type == 'Participant' && ( $user_role == 120 || $user_role == 115)) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                $tagAccountFolderCond = '';
                if ($huddle_ids && count($huddle_ids) > 0) {
                    $account_folder_ids = implode(',', $huddle_ids);
                    $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                }
                $joins = array(
                    array(
                        'table' => 'account_comment_tags as act',
                        'type' => 'inner',
                        'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'act.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    ),
                );
                $conditions = array(
                    'AccountTag.tag_type' => 0,
                    'AccountTag.account_id' => $account_ids,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    $mATCnd,
                    $act_frm_wrk,
                    $tagAccountFolderCond,
                );
                $pl_conditions = array();
                $all_accounts = implode(',', $account_ids);
                $custom_conditions = array(
                    'AccountTag.tag_type' => 1,
                    'AccountTag.account_id' => $account_ids,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.')) ',
                    $tagAccountFolderCond,
                    $mATCnd_custom_markers
                );
                $fields = array(
                    'AccountTag.tag_title',
                    'AccountTag.tads_code',
                    'AccountTag.account_tag_id',
                    'act.created_date AS AccountTag__tags_date',
                    'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                );
                if (empty($search_by_standard)) {
                    $get_value = array(
                        'joins' => $joins,
                        'conditions' => $conditions,
                        'fields' => $fields,
                        'group' => array('AccountTag.account_tag_id'),
                        'order' => 'AccountTag__total_tags DESC',
                        'limit' => 5
                    );
                } else {
                    $get_value = array(
                        'joins' => $joins,
                        'conditions' => $conditions,
                        'fields' => $fields,
                        'group' => array('AccountTag.account_tag_id'),
                        'order' => 'AccountTag__total_tags DESC',
                        'limit' => 5
                    );
                }
                $serial_charts_conditions = $conditions;
                $response_custom = $this->get_cutom_tags($custom_conditions);
                $data_1['custom_markers_tag'] = json_encode($response_custom);
                $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                $account_tags_analytics = $this->AccountTag->find('all', $get_value);
            } elseif ($user_type == 'Coachee' && ( $user_role == 120 || $user_role == 115)) {
                $accountFolderCond = '';
                if ($folder_type == 2) {
                    $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                    if ($huddles) {
                        foreach ($huddles as $row) {
                            if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                            }
                        }
                    }
                    $user_ids = $user_id;
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        )
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        $mATCnd,
                        $act_frm_wrk,
                    );
                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $accountFolderIds
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        'afd.account_folder_id' => $accountFolderIds,
                        $mATCnd_custom_markers
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $serial_charts_conditions = $conditions;
                    if (count($accountFolderIds) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                    } else {
                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $pl_conditions = array();
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $mATCnd,
                        $act_frm_wrk,
                        $tagAccountFolderCond,
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $tagAccountFolderCond,
                        $mATCnd_custom_markers
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                }
            } elseif ($user_type == 'Coachee/Assessed Participant' && ( $user_role == 120 || $user_role == 115)) {
                if ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $mATCnd,
                        $act_frm_wrk,
                        $tagAccountFolderCond,
                    );
                    $pl_conditions = array();
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.')) ',
                        'afmd.meta_data_value' => $folder_type,
                        $tagAccountFolderCond,
                        $mATCnd_custom_markers
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                }
                if ($folder_type == 2) {
                    $user_ids = array();
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                    $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                    if ($evaluator_ids == false) {
                        $evaluator_ids = array();
                    }
                    $user_ids = array_merge($user_id, $evaluator_ids);

                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        $mATCnd,
                        $act_frm_wrk,
                    );
                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $huddle_ids
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $mATCnd_custom_markers
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'act.account_comment_tag_id',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $serial_charts_conditions = $conditions;
                    if (count($user_ids) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                    } else {
                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                    $tagConditions = '';
                } elseif ($folder_type == 3) {
                    $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'evaluation', $user['User']['id']);
                    if ($huddles) {

                        foreach ($huddles as $row) {
                            if ($row['huddle_users'] ['role_id'] == 210 && $row[0]['folderType'] == 'evaluation') {
                                $coach_inner_ids[] = $row['huddle_users']['user_id'];
                                $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                            }
                        }
                    }

                    $evaluator_ids = $view->Custom->get_evaluator_ids($accountFolderIds);

                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $evaluator_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        $mATCnd,
                        $act_frm_wrk,
                    );
                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $evaluator_ids,
                        'account_folder_id' => $accountFolderIds
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $evaluator_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        $mATCnd_custom_markers,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $serial_charts_conditions = $conditions;
                    if (count($accountFolderIds) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                    } else {
                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                }
            } elseif ($user_type == 'Coach/Assessor' && ( $user_role == 100 || $user_role == 110)) {
                $accountFolderCond = '';
                $accountFolderIds = array();
                $coache_view_type = isset($this->request->data['coachee_view']) ? $this->request->data['coachee_view'] : 1;
                $user_ids = array();
                $tagConditions = '';
                if ($folder_type == 2) {
                    if ($coache_view_type == 1) {
                        $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                        if ($huddles) {

                            foreach ($huddles as $row) {
                                if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                    $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                }
                            }
                        }

                        $user_ids = $user_id;
                        $joins = array(
                            array(
                                'table' => 'account_comment_tags as act',
                                'type' => 'inner',
                                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                            ),
                            array(
                                'table' => 'account_folder_documents as afd',
                                'type' => 'inner',
                                'conditions' => 'act.ref_id = afd.document_id'
                            ),
                            array(
                                'table' => 'account_folders_meta_data as afmd',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                            )
                        );
                        $conditions = array(
                            'AccountTag.tag_type' => 0,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            $mATCnd,
                            $act_frm_wrk,
                        );
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            $mATCnd_custom_markers,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        );
                        $fields = array(
                            'AccountTag.tag_title',
                            'AccountTag.tads_code',
                            'AccountTag.account_tag_id',
                            'act.created_date AS AccountTag__tags_date',
                            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                        );
                        if (empty($search_by_standard)) {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        } else {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        }
                        $serial_charts_conditions = $conditions;
                        if (count($accountFolderIds) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                    } else {
                        $user_ids = array();
                        $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                        $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                        if ($evaluator_ids == false) {
                            $evaluator_ids = array();
                        }
                        $user_ids = array_merge($user_id, $evaluator_ids);

                        $joins = array(
                            array(
                                'table' => 'account_comment_tags as act',
                                'type' => 'inner',
                                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                            ),
                            array(
                                'table' => 'account_folder_documents as afd',
                                'type' => 'inner',
                                'conditions' => 'act.ref_id = afd.document_id'
                            ),
                            array(
                                'table' => 'account_folders_meta_data as afmd',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                            ),
                        );
                        $conditions = array(
                            'AccountTag.tag_type' => 0,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            // 'afd.account_folder_id' => $accountFolderIds,
                            $mATCnd,
                            $act_frm_wrk,
                        );
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.')) ',
                            $mATCnd_custom_markers
                        );
                        $fields = array(
                            'AccountTag.tag_title',
                            'AccountTag.tads_code',
                            'AccountTag.account_tag_id',
                            'act.created_date AS AccountTag__tags_date',
                            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                        );
                        if (empty($search_by_standard)) {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        } else {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        }
                        $serial_charts_conditions = $conditions;
                        if (count($user_ids) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                        $tagConditions = '';
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
//                    var_dump($act_frm_wrk);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $accountFolderIds = $huddle_ids;
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $mATCnd,
                        $act_frm_wrk,
                        $tagAccountFolderCond,
                    );
                    $pl_conditions = array();
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $tagAccountFolderCond,
                        $mATCnd_custom_markers
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                }
            } elseif ($user_type == 'Coach' && ( $user_role == 100 || $user_role == 110)) {
                $accountFolderCond = '';
                $accountFolderIds = array();
                $coache_view_type = isset($this->request->data['coachee_view']) ? $this->request->data['coachee_view'] : 1;
                $user_ids = array();
                $tagConditions = '';
                if ($folder_type == 2) {
                    if ($coache_view_type == 1) {
                        $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                        if ($huddles) {

                            foreach ($huddles as $row) {
                                if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                    $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                }
                            }
                        }

                        $user_ids = $user_id;
                        $joins = array(
                            array(
                                'table' => 'account_comment_tags as act',
                                'type' => 'inner',
                                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                            ),
                            array(
                                'table' => 'account_folder_documents as afd',
                                'type' => 'inner',
                                'conditions' => 'act.ref_id = afd.document_id'
                            ),
                            array(
                                'table' => 'account_folders_meta_data as afmd',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                            )
                        );
                        $conditions = array(
                            'AccountTag.tag_type' => 0,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            $mATCnd,
                            $act_frm_wrk,
                        );
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );

                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                            $mATCnd_custom_markers
                        );
                        $fields = array(
                            'AccountTag.tag_title',
                            'AccountTag.tads_code',
                            'AccountTag.account_tag_id',
                            'act.created_date AS AccountTag__tags_date',
                            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                        );
                        if (empty($search_by_standard)) {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        } else {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        }
                        $serial_charts_conditions = $conditions;
                        if (count($accountFolderIds) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                    } else {
                        $user_ids = array();
                        $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                        $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                        if ($evaluator_ids == false) {
                            $evaluator_ids = array();
                        }
                        $user_ids = array_merge($user_id, $evaluator_ids);

                        $joins = array(
                            array(
                                'table' => 'account_comment_tags as act',
                                'type' => 'inner',
                                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                            ),
                            array(
                                'table' => 'account_folder_documents as afd',
                                'type' => 'inner',
                                'conditions' => 'act.ref_id = afd.document_id'
                            ),
                            array(
                                'table' => 'account_folders_meta_data as afmd',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                            )
                        );
                        $conditions = array(
                            'AccountTag.tag_type' => 0,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            $mATCnd,
                            $act_frm_wrk,
                        );
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $huddle_ids
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.')) ',
                            $mATCnd_custom_markers
                        );
                        $fields = array(
                            'AccountTag.tag_title',
                            'AccountTag.tads_code',
                            'AccountTag.account_tag_id',
                            'act.created_date AS AccountTag__tags_date',
                            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                        );
                        if (empty($search_by_standard)) {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        } else {
                            $get_value = array(
                                'joins' => $joins,
                                'conditions' => $conditions,
                                'fields' => $fields,
                                'group' => array('AccountTag.account_tag_id'),
                                'order' => 'AccountTag__total_tags DESC',
                                'limit' => 5
                            );
                        }
                        $serial_charts_conditions = $conditions;
                        if (count($user_ids) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                        $tagConditions = '';
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);

                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $conditions = array(
                        'AccountTag.tag_type' => 0,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $mATCnd,
                        $act_frm_wrk,
                        $tagAccountFolderCond,
                    );
                    $pl_conditions = array();
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $tagAccountFolderCond,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $conditions,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => 'AccountTag__total_tags DESC',
                            'limit' => 5
                        );
                    }
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    $account_tags_analytics = $this->AccountTag->find('all', $get_value);
                }
            }

            $account_tags_analytics = Set::extract('/AccountTag/.', $account_tags_analytics);

            $response = '';
            $all_account_ids = array();

            $colors = array('#85c4e3', '#fff568', '#90ec7d', '#f7a35b', '#ee1c25', '#1abc9c', '#2ecc71', '#3498db', '#9b59b6', '#f1c40f', '#e67e22', '#ecf0f1', '#95a5a6', '#5A530D', '#FF0000', '#850CE8', '#0D74FF', '#75810C', '#0D2B5A');
            if ($account_tags_analytics) {
                $count = 0;
                for ($i = 0; $i < count($account_tags_analytics); $i++) {
                    $response[] = array(
                        'country' => $account_tags_analytics[$i]['tag_title'],
                        'tads_code' => $account_tags_analytics[$i]['tads_code'],
                        'litres' => $account_tags_analytics[$i]['total_tags'],
                        'color' => $colors[$count],
                        'account_tag_id' => $account_tags_analytics[$i]['account_tag_id'],
                        'AccountTag__tags_date' => $account_tags_analytics[$i]['tags_date'],
                        'AccountTag__total_tags' => $account_tags_analytics[$i]['total_tags']
                    );
                    $count ++;
                }
                $all_at_ids = array();
                foreach ($account_tags_analytics as $row) {
                    if ($row['tag_title'] != '') {
                        $all_account_ids[$row['account_tag_id']] = array(
                            'account_tag_id' => $row['account_tag_id'],
                            'tads_code' => $row['tads_code'],
                            'country' => $row['tag_title'],
                        );
                        $all_at_ids[] = $row['account_tag_id'];
                    }
                }
            }

            $result = array();
            if ($response != '') {
                $data['standarads_tag'] = json_encode($response);
            } else {
                $data['standarads_tag'] = '[]';
            }

            $data['account_id'] = $this->Session->read('account_id');

            $frequency_of_tagged_standars_chart = $view->element('analytics/frequency_of_tagged_standards', $data, false);
            $result['frequency_of_tagged_standars_chart'] = $frequency_of_tagged_standars_chart;

            if ($search_by_standard == '') {
                $account_empty_tags_standers = $this->AccountTag->find('all', array(
                    'fields' => '',
                    'conditions' => array(
                        'tag_type' => '0',
                        'account_id' => $account_ids,
                        $act_frm_wrk,
                        "tag_html <>  ''"
                    ),
                ));

                if ($account_empty_tags_standers) {
                    foreach ($account_empty_tags_standers as $row) {
                        if (isset($row['AccountTag'] ['account_tag_id']) && in_array($row ['AccountTag']['account_tag_id'], $all_at_ids)) {
                            continue;
                        } else {
                            if ($row['AccountTag']['tag_title'] != '') {
                                $all_account_ids[$row['AccountTag']['account_tag_id']] = array(
                                    'account_tag_id' => $row['AccountTag']['account_tag_id'],
                                    'tads_code' => $row['AccountTag']['tads_code'],
                                    'country' => $row['AccountTag']['tag_title'],
                                );
                            }
                        }
                    }
                }
            }

            $count = 0;
            $acc_tags = array();
            foreach ($all_account_ids as $actag) {
                $acc_tags[] = $actag['account_tag_id'];
                $standard_title = '';
                if (empty($actag['tads_code'])) {
                    $standard_title = $actag['country'];
                } else {
                    $standard_title = $actag['tads_code'];
                }
                $graph_data_all[] = array();
                $graph_data = array();
                $userConditions2 = '';
                if (count($user_id_c) > 1) {
                    $user_id_t = implode(',', $user_id_c);
                    $userConditions2 = array("act.created_by IN($user_id_t)");
                } else {
                    $user_id_t = implode(',', $user_id_c);
                    $userConditions2 = array("afu.user_id IN($user_id_t)");
                }

                $accFolderCond = '';
                if ($accountFolderIds != '') {
                    $accFolderCond[] = array('afd.account_folder_id' => $accountFolderIds);
                }

                foreach ($period as $dt) {

                    $g_data = $dt->format("Y-m-d");
                    $duration = explode('-', $dt->format("Y-m-d"));
                    $dCnd = array();
                    $accountTagCnd = array();
                    $docCnd = array();
                    $graph_data['date'] = $dt->format("M-y");
                    $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                    $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                    $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                    $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                    $serial_charts_conditions['AccountTag.account_tag_id'] = $actag['account_tag_id'];
                    unset($serial_charts_conditions[0]);
                    $serial_conditon = array_merge($serial_charts_conditions, $dCnd);
                    $joins = array(
                        array(
                            'table' => 'account_comment_tags as act',
                            'type' => 'inner',
                            'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                        ),
                        array(
                            'table' => 'account_folder_documents as afd',
                            'type' => 'inner',
                            'conditions' => 'act.ref_id = afd.document_id'
                        ),
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'inner',
                            'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                        ),
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );
                    if (empty($search_by_standard)) {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $serial_conditon,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                        );
                    } else {
                        $get_value = array(
                            'joins' => $joins,
                            'conditions' => $serial_conditon,
                            'fields' => $fields,
                            'group' => array('AccountTag.account_tag_id'),
                            'order' => array('AccountTag__total_tags' => 'desc')
                        );
                    }



                    $account_tags_analytics = $this->AccountTag->find('first', $get_value);


                    if ($folder_type == 1) {
                        $huddle_type = 'collaboration';
                    } elseif ($folder_type == 2) {
                        $huddle_type = 'coaching';
                    } elseif ($folder_type == 3) {
                        $huddle_type = 'evaluation';
                    }

                    $huddle_ids = $this->get_account_folder_ids($account_ids, $huddle_type, $user_id);
                    $pl_conditions[1] = array(
                        "created_at >= ' $g_data'",
                        "created_at <= '$g_data '  + INTERVAL 1 MONTH ",
                        'standard_id' => $actag['account_tag_id']
                    );


                    if ($pl_conditions) {
                        $standard_ratings = $this->DocumentStandardRating->find("all", array("conditions" => array($pl_conditions)));
                        $standard_avg_sum = 0;
                        $standard_count = 0;
                        foreach ($standard_ratings as $row) {
                            $standard_avg_sum = $standard_avg_sum + $row['DocumentStandardRating']['rating_value'];
                            $standard_count++;
                        }
                        if ($standard_avg_sum > 0) {
                            $standard_avg = round($standard_avg_sum / $standard_count);
                        } else {
                            $standard_avg = 0;
                        }

                        $average_rating_name_details = $this->AccountMetaData->find('first', array("conditions" => array(
                                'account_id' => $account_id, 'meta_data_value' => $standard_avg, 'meta_data_name like "metric_value_%"'
                        )));
                        if (!empty($average_rating_name_details)) {
                            $average_rating_name = substr($average_rating_name_details['AccountMetaData']['meta_data_name'], 13);
                        } else {
                            $average_rating_name = 'No Rating';
                        }

                        $graph_data['color'] = $colors[$count];
                        $graph_data['standard_rating_avg'] = $standard_avg;
                        $graph_data['average_rating_name'] = $average_rating_name;
                        $graph_data['color_rating'] = '#000';

                        $graph_data['color'] = $colors[$count];
                        $graph_data['total_tags'] = isset($account_tags_analytics['AccountTag']['total_tags']) ? $account_tags_analytics['AccountTag']['total_tags'] : '0';
                        $graph_data_all [$actag ['account_tag_id'] . '-' . $standard_title . '-' . $colors[$count]][] = $graph_data;
                        unset($graph_data);
                    } else {
                        $graph_data_all = '';
                    }
                }
                $count++;
            }
            $data_provider = array();
            $data_charts = array();
            $title = '';
            $mt_arr = array();
            foreach ($metric_old as $mt_old) {
                $mt_arr[] = substr($mt_old['AccountMetaData']['meta_data_name'], 13);
            }
            $assessments = array(0 => 'No Assessment');
            $arr_all = array_merge($assessments, $mt_arr);

            if (isset($graph_data_all) && $graph_data_all != '') {

                foreach ($graph_data_all as $key => $row) {
                    $total_standards = 0;
                    $total_avg_rattings = 0;
                    $avg_counter = 0;
                    if (count($row) > 0) {
                        $title = explode('-', $key);
                        foreach ($row as $r) {
                            $total_standards = $total_standards + $r['total_tags'];
                            $total_avg_rattings = $total_avg_rattings + $r['standard_rating_avg'];
                            if ($r['standard_rating_avg'] != 0) {
                                $avg_counter ++;
                            }
                        }
                        if (count($all_account_ids) == 0) {
                            $total_avg_rattings = 0;
                        } else {
                            $total_avg_rattings = round($total_avg_rattings / $avg_counter);
                        }

                        if ($total_avg_rattings > 0) {
                            $final_average_rating_name = '';
                            foreach ($metric_new as $metric) {
                                if ($total_avg_rattings == $metric['value']) {
                                    $final_average_rating_name = $metric['name'];
                                }
                            }
                        }
                        if ($total_avg_rattings == 0) {
                            $final_average_rating_name = 'No Rating';
                        }
                        $data_provider = array(
                            'title' => $title[1],
                            'color' => isset($title[2]) ? $title[2] : '',
                            'ratting_title' => 'Average Performance Level',
                            'count' => $count,
                            'data_provider' => json_encode($row),
                            'assessment_array' => $arr_all,
                            'folder_type' => $folder_type,
                            'total_tagged_standards' => $total_standards,
                            'total_avg' => ($role_id == 100) ? $final_average_rating_name : $total_avg_rattings,
                        );

                        $html = $view->element('analytics/frequency_serial_charts', $data_provider);
                        $data_charts[] = $html;
                    }
                    $count++;
                }
            }

            $result['serial_charts'] = $data_charts;
        } else {
//When Standard off
//            $mATCnd_1[] = array('AccountCommentTag.created_date >= ' => $st_date);
//            $mATCnd_1[] = array('AccountCommentTag.created_date <= ' => $end_date);
            $start = (new DateTime($st_date))->modify('first day of this month');
            $end = (new DateTime($end_date))->modify('last day of this month');

            $interval = DateInterval::createFromDateString('1 month');
            $period = new DatePeriod($start, $interval, $end);

            $this->AccountTag->virtualFields['tags_date'] = 0;
            $this->AccountTag->virtualFields['total_tags'] = 0;

            $coach_inner_ids = array();
            $accountFolderIds = array();
            $conditions = array();
            if ($user_type == 'Collaborator' && ( $user_role == 120 || $user_role == 115)) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                $tagAccountFolderCond = '';
                if ($huddle_ids && count($huddle_ids) > 0) {
                    $account_folder_ids = implode(',', $huddle_ids);
                    $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                }

                $conditions = array(
                    'AccountCommentTag.ref_type' => 1,
                    'af.account_id' => $account_ids,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                    //$mATCnd_1,
                    $tagAccountFolderCond,
                );
                $all_accounts = implode(',', $account_ids);
                $custom_conditions = array(
                    'AccountTag.tag_type' => 1,
                    'AccountTag.account_id' => $account_ids,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                    $mATCnd_custom_markers,
                    $tagAccountFolderCond
                );
                $pl_conditions = false;
                $serial_charts_conditions = $conditions;
                $response_custom = $this->get_cutom_tags($custom_conditions);
                $data_1['custom_markers_tag'] = json_encode($response_custom);
                $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
            } elseif ($user_type == 'Coach' && ( $user_role == 120 || $user_role == 115)) {
                $accountFolderCond = '';
                $accountFolderIds = array();
                $coache_view_type = isset($this->request->data['coachee_view']) ? $this->request->data['coachee_view'] : 1;
                $user_ids = array();
                $tagConditions = '';
                if ($folder_type == 2) {
                    if ($coache_view_type == 1) {
                        $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                        if ($huddles) {

                            foreach ($huddles as $row) {
                                if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                    $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                }
                            }
                        }

                        $user_ids = $user_id;
                        $conditions = array(
                            'AccountCommentTag.ref_type' => 1,
                            'af.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'AccountCommentTag.created_by' => $user_ids,
                            'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                            //$mATCnd_1,
                            'afd.account_folder_id' => $accountFolderIds,
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                            $mATCnd_custom_markers,
                        );

                        $pl_conditions = false;
                        $serial_charts_conditions = $conditions;
                        if (count($accountFolderIds) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                    } else {
                        $user_ids = array();
                        $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                        $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                        if ($evaluator_ids == false) {
                            $evaluator_ids = array();
                        }
                        $user_ids = array_merge($user_id, $evaluator_ids);
                        $conditions = array(
                            'AccountCommentTag.ref_type' => 1,
                            'af.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'AccountCommentTag.created_by' => $user_ids,
                            'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
//                            $mATCnd_1
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                            $mATCnd_custom_markers,
                        );
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $huddle_ids
                        );
                        $serial_charts_conditions = $conditions;
                        if (count($user_ids) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                        $tagConditions = '';
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        //$mATCnd_1,
                        $tagAccountFolderCond,
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $tagAccountFolderCond,
                    );

                    $pl_conditions = false;
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                }
            } elseif ($user_type == 'Assessor' && ( $user_role == 120 || $user_role == 115)) {

                if ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $accountFolderIds = $huddle_ids;
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        //$mATCnd_1,
                        $tagAccountFolderCond
                    );
                    $pl_conditions = array();
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $tagAccountFolderCond,
                    );
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                } elseif ($folder_type == 3) {
                    $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'evaluation', $user['User']['id']);
                    if ($huddles) {

                        foreach ($huddles as $row) {
                            if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'evaluation') {
                                $coach_inner_ids[] = $row['huddle_users']['user_id'];
                                $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                $user_ids[] = $row['huddle_users']['user_id'];
                            }
                        }
                    }
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        'afd.account_folder_id' => $accountFolderIds,
                        'AccountCommentTag.created_by' => $user_ids,
//                        $mATCnd_1
                    );

                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $accountFolderIds
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $mATCnd_custom_markers
                    );
                    $fields = array(
                        'AccountTag.tag_title',
                        'AccountTag.tads_code',
                        'AccountTag.account_tag_id',
                        'act.created_date AS AccountTag__tags_date',
                        'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                    );

                    $serial_charts_conditions = $conditions;
                    if (count($accountFolderIds) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    } else {
                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                }
            } elseif ($user_type == 'Coach/Assessor' && ( $user_role == 120 || $user_role == 115)) {
                $accountFolderCond = '';
                $accountFolderIds = array();
                $coache_view_type = isset($this->request->data['coachee_view']) ? $this->request->data['coachee_view'] : 1;
                $user_ids = array();
                $tagConditions = '';
                if ($folder_type == 2) {
                    if ($coache_view_type == 1) {
                        $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                        if ($huddles) {

                            foreach ($huddles as $row) {
                                if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                    $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                }
                            }
                        }

                        $user_ids = $user_id;
                        $conditions = array(
                            'AccountCommentTag.ref_type' => 1,
                            'af.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'AccountCommentTag.created_by' => $user_ids,
                            'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                            //$mATCnd_1,
                            'afd.account_folder_id' => $accountFolderIds,
                        );


                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );

                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                            $mATCnd_custom_markers,
                        );
                        $serial_charts_conditions = $conditions;
                        if (count($accountFolderIds) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                    } else {
                        $user_ids = array();
                        $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                        $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                        if ($evaluator_ids == false) {
                            $evaluator_ids = array();
                        }
                        $user_ids = array_merge($user_id, $evaluator_ids);
                        $conditions = array(
                            'AccountCommentTag.ref_type' => 1,
                            'af.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'AccountCommentTag.created_by' => $user_ids,
                            'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
//                            $mATCnd_1
                        );
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );

                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                            $mATCnd_custom_markers
                        );
                        $serial_charts_conditions = $conditions;
                        if (count($user_ids) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                        $tagConditions = '';
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $accountFolderIds = $huddle_ids;
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        //$mATCnd_1,
                        $tagAccountFolderCond
                    );

                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $tagAccountFolderCond,
                        $mATCnd_custom_markers
                    );
                    $pl_conditions = array();
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                } elseif ($folder_type == 3) {
                    $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'evaluation', $user['User']['id']);
                    if ($huddles) {

                        foreach ($huddles as $row) {
                            if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'evaluation') {
                                $coach_inner_ids[] = $row['huddle_users']['user_id'];
                                $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                $user_ids[] = $row['huddle_users']['user_id'];
                            }
                        }
                    }

                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        'AccountCommentTag.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
//                        $mATCnd_1,
                    );

                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $accountFolderIds
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $mATCnd_custom_markers
                    );
                    $serial_charts_conditions = $conditions;
                    if (count($accountFolderIds) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    } else {
                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                }
            } elseif ($user_type == 'Participant' && ( $user_role == 120 || $user_role == 115)) {
                $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                $tagAccountFolderCond = '';
                if ($huddle_ids && count($huddle_ids) > 0) {
                    $account_folder_ids = implode(',', $huddle_ids);
                    $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                }
                $conditions = array(
                    'AccountCommentTag.ref_type' => 1,
                    'af.account_id' => $account_ids,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                    //$mATCnd_1,
                    $tagAccountFolderCond
                );

                $pl_conditions = array();
                $all_accounts = implode(',', $account_ids);
                $custom_conditions = array(
                    'AccountTag.tag_type' => 1,
                    'AccountTag.account_id' => $account_ids,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                    $tagAccountFolderCond,
                    $mATCnd_custom_markers
                );
                $serial_charts_conditions = $conditions;
                $response_custom = $this->get_cutom_tags($custom_conditions);
                $data_1['custom_markers_tag'] = json_encode($response_custom);
                $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
            } elseif ($user_type == 'Coachee' && ( $user_role == 120 || $user_role == 115)) {
                $accountFolderCond = '';
                if ($folder_type == 2) {
                    $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                    if ($huddles) {

                        foreach ($huddles as $row) {
                            if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                            }
                        }
                    }
                    $user_ids = $user_id;
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        // $mATCnd_1,
                        'AccountCommentTag.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                    );

                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $accountFolderIds
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        'afd.account_folder_id' => $accountFolderIds,
                        $mATCnd_custom_markers
                    );
                    $serial_charts_conditions = $conditions;
                    if (count($accountFolderIds) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    } else {
                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }

                    $pl_conditions = array();
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        //$mATCnd_1,
                        $tagAccountFolderCond
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $tagAccountFolderCond,
                        $mATCnd_custom_markers
                    );
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                }
            } elseif ($user_type == 'Coachee/Assessed Participant' && ( $user_role == 120 || $user_role == 115)) {
                if ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        // $mATCnd_1,
                        $tagAccountFolderCond
                    );

                    $pl_conditions = array();
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        'afmd.meta_data_value' => $folder_type,
                        $tagAccountFolderCond,
                        $mATCnd_custom_markers
                    );
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                }
                if ($folder_type == 2) {
                    $user_ids = array();
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                    $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                    if ($evaluator_ids == false) {
                        $evaluator_ids = array();
                    }
                    $user_ids = array_merge($user_id, $evaluator_ids);

                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        'AccountCommentTag.created_by' => $user_ids,
//                        $mATCnd_1,
                    );

                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $huddle_ids
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $mATCnd_custom_markers
                    );
                    $serial_charts_conditions = $conditions;
                    if (count($user_ids) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    } else {
                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                    $tagConditions = '';
                } elseif ($folder_type == 3) {
                    $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'evaluation', $user['User']['id']);
                    if ($huddles) {

                        foreach ($huddles as $row) {
                            if ($row['huddle_users'] ['role_id'] == 210 && $row[0]['folderType'] == 'evaluation') {
                                $coach_inner_ids[] = $row['huddle_users']['user_id'];
                                $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                            }
                        }
                    }
                    $evaluator_ids = $view->Custom->get_evaluator_ids($accountFolderIds);
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        'AccountCommentTag.created_by' => $evaluator_ids,
                        'afd.account_folder_id' => $accountFolderIds,
//                        $mATCnd_1,
                    );

                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $evaluator_ids,
                        'account_folder_id' => $accountFolderIds
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $evaluator_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        $mATCnd_custom_markers,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                    );
                    $serial_charts_conditions = $conditions;
                    if (count($accountFolderIds) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    } else {
                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                }
            } elseif ($user_type == 'Coach/Assessor' && ( $user_role == 100 || $user_role == 110)) {
                $accountFolderCond = '';
                $accountFolderIds = array();
                $coache_view_type = isset($this->request->data['coachee_view']) ? $this->request->data['coachee_view'] : 1;
                $user_ids = array();
                $tagConditions = '';
                if ($folder_type == 2) {
                    if ($coache_view_type == 1) {
                        $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                        if ($huddles) {

                            foreach ($huddles as $row) {
                                if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                    $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                }
                            }
                        }

                        $user_ids = $user_id;
                        $conditions = array(
                            'AccountCommentTag.ref_type' => 1,
                            'af.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'AccountCommentTag.created_by' => $user_ids,
                            'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
//                          $mATCnd_1,
                            'afd.account_folder_id' => $accountFolderIds,
                        );


                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                            $mATCnd_custom_markers,
                        );
                        $serial_charts_conditions = $conditions;
                        if (count($accountFolderIds) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                    } else {
                        $user_ids = array();
                        $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                        $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                        if ($evaluator_ids == false) {
                            $evaluator_ids = array();
                        }
                        $user_ids = array_merge($user_id, $evaluator_ids);
                        $conditions = array(
                            'AccountCommentTag.ref_type' => 1,
                            'af.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'AccountCommentTag.created_by' => $user_ids,
                            'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
//                            $mATCnd_1
                        );
                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                            $mATCnd_custom_markers
                        );
                        $serial_charts_conditions = $conditions;
                        if (count($user_ids) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                        $tagConditions = '';
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);
                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $accountFolderIds = $huddle_ids;
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
//                      $mATCnd_1,
                        $tagAccountFolderCond
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $tagAccountFolderCond,
                        $mATCnd_custom_markers
                    );
                    $pl_conditions = array();
                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);
                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                } elseif ($folder_type == 3) {
                    $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'evaluation', $user['User']['id']);
                    if ($huddles) {

                        foreach ($huddles as $row) {
                            if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'evaluation') {
                                $coach_inner_ids[] = $row['huddle_users']['user_id'];
                                $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                $user_ids[] = $row['huddle_users']['user_id'];
                            }
                        }
                    }

                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                        'AccountCommentTag.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
//                        $mATCnd_1,
                    );

                    $pl_conditions = array(
                        'account_id' => $account_ids,
                        'user_id' => $user_ids,
                        'account_folder_id' => $accountFolderIds
                    );
                    $all_accounts = implode(',', $account_ids);
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'act.created_by' => $user_ids,
                        'afd.account_folder_id' => $accountFolderIds,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                        $mATCnd_custom_markers
                    );
                    $serial_charts_conditions = $conditions;
                    if (count($accountFolderIds) > 0) {
                        $response_custom = $this->get_cutom_tags($custom_conditions);
                        $data_1['custom_markers_tag'] = json_encode($response_custom);
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                    } else {
                        $data_1['custom_markers_tag'] = json_encode(array());
                        $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        $account_tags_analytics = array();
                    }
                }
            } elseif ($user_type == 'Coach' && ( $user_role == 100 || $user_role == 110)) {
                $accountFolderCond = '';
                $accountFolderIds = array();
                $coache_view_type = isset($this->request->data['coachee_view']) ? $this->request->data['coachee_view'] : 1;
                $user_ids = array();
                $tagConditions = '';
                if ($folder_type == 2) {
                    if ($coache_view_type == 1) {
                        $huddles = $this->AccountFolder->getAllAccountFolderForAnalytics($account_id, 'coaching', $user_id);
                        if ($huddles) {

                            foreach ($huddles as $row) {
                                if ($row['huddle_users'] ['role_id'] == 200 && $row[0]['folderType'] == 'coaching') {
                                    $accountFolderIds[] = $row['AccountFolder']['account_folder_id'];
                                }
                            }
                        }

                        $user_ids = $user_id;
                        $conditions = array(
                            'AccountCommentTag.ref_type' => 1,
                            'af.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                            'AccountCommentTag.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
//                            $mATCnd_1,
                        );

                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $accountFolderIds
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                            $mATCnd_custom_markers
                        );


                        $serial_charts_conditions = $conditions;
                        if (count($accountFolderIds) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                    } else {
                        $user_ids = array();
                        $huddle_ids = $this->get_account_folder_ids($account_id, 'coaching', $user_id);
                        $evaluator_ids = $view->Custom->get_evaluator_ids($huddle_ids);
                        if ($evaluator_ids == false) {
                            $evaluator_ids = array();
                        }
                        $user_ids = array_merge($user_id, $evaluator_ids);
                        $conditions = array(
                            'AccountCommentTag.ref_type' => 1,
                            'af.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
                            'AccountCommentTag.created_by' => $user_ids,
                            'afd.account_folder_id' => $accountFolderIds,
//                            $mATCnd_1,
                        );

                        $pl_conditions = array(
                            'account_id' => $account_ids,
                            'user_id' => $user_ids,
                            'account_folder_id' => $huddle_ids
                        );
                        $all_accounts = implode(',', $account_ids);
                        $custom_conditions = array(
                            'AccountTag.tag_type' => 1,
                            'AccountTag.account_id' => $account_ids,
                            'afmd.meta_data_name' => 'folder_type',
                            'afmd.meta_data_value' => $folder_type,
                            'act.created_by' => $user_ids,
                            'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                            $mATCnd_custom_markers
                        );
                        $serial_charts_conditions = $conditions;
                        if (count($user_ids) > 0) {
                            $response_custom = $this->get_cutom_tags($custom_conditions);
                            $data_1['custom_markers_tag'] = json_encode($response_custom);
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                        } else {
                            $data_1['custom_markers_tag'] = json_encode(array());
                            $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                            $account_tags_analytics = array();
                        }
                        $tagConditions = '';
                    }
                } elseif ($folder_type == 1) {
                    $huddle_ids = $this->get_account_folder_ids($account_id, 'collaboration', $user_id);

                    $tagAccountFolderCond = '';
                    if ($huddle_ids && count($huddle_ids) > 0) {
                        $account_folder_ids = implode(',', $huddle_ids);
                        $tagAccountFolderCond[] = array("afd.account_folder_id IN($account_folder_ids)");
                    }
                    $conditions = array(
                        'AccountCommentTag.ref_type' => 1,
                        'af.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        'LOWER(AccountCommentTag.tag_title) NOT IN  (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.tag_type = 1)',
//                      $mATCnd_1,
                        $tagAccountFolderCond
                    );
                    $all_accounts = implode(',', $account_ids);
                    $pl_conditions = array();
                    $custom_conditions = array(
                        'AccountTag.tag_type' => 1,
                        'AccountTag.account_id' => $account_ids,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => $folder_type,
                        $tagAccountFolderCond,
                        'LOWER(act.tag_title) IN (SELECT LOWER(tag_title) AS t  FROM account_tags WHERE  account_tags.`tag_type` = 1 AND account_tags.`account_id` IN('.$all_accounts.'))',
                    );

                    $serial_charts_conditions = $conditions;
                    $response_custom = $this->get_cutom_tags($custom_conditions);

                    $data_1['custom_markers_tag'] = json_encode($response_custom);
                    $custom_markers_chart = $view->element('analytics/frequency_of_custom_tags', $data_1);
                }
            }
            $searial_chart_cond = $conditions;
            $conditions['AccountCommentTag.created_date >= '] = $st_date;
            $conditions['AccountCommentTag.created_date <='] = $end_date;

            $account_tags_analytics = $this->AccountCommentTag->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'AccountCommentTag.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders as af',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = af.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    ),
                ),
                'conditions' => $conditions,
                'fields' => array(
                    'AccountCommentTag.tag_title',
                    'AccountCommentTag.created_date AS AccountTag__tags_date',
                    'AccountCommentTag.account_comment_tag_id',
                    'AccountCommentTag.account_tag_id',
                    'COUNT(`AccountCommentTag`.`account_comment_tag_id`) AS total_tags '
                ),
                'group' => 'tag_title ORDER BY total_tags DESC',
                'limit' => 5
            ));
            $response = array();
            $colors = array('#85c4e3', '#fff568', '#90ec7d', '#f7a35b', '#ee1c25');
            if ($account_tags_analytics) {
                $count = 0;
                foreach ($account_tags_analytics as $row) {

                    $response[] = array(
                        'account_tag_id' => $row['AccountCommentTag']['account_tag_id'],
                        'country' => $row['AccountCommentTag']['tag_title'],
                        'litres' => !empty($row[0]['total_tags']) ? $row[0]['total_tags'] : '',
                        'color' => $colors[$count],
                        'account_comment_tag_id' => $row['AccountCommentTag']['account_comment_tag_id'],
                        'AccountTag__tags_date' => !empty($row[0]['AccountTag__tags_date']) ? $row[0]['AccountTag__tags_date'] : '',
                        'AccountTag__total_tags' => !empty($row[0]['total_tags']) ? $row[0]['total_tags'] : '',
                    );
                    $count ++;
                }
            }


            $data['standarads_tag'] = json_encode($response);
            $data['account_id'] = $account_id;
            $view = new View($this, false);
            $frequency_of_tagged_standars_chart = $view->element('analytics/frequency_of_tagged_standards', $data, false);
            $result['frequency_of_tagged_standars_chart'] = $frequency_of_tagged_standars_chart;

//Load Serial Charts
            $count = 0;

            foreach ($response as $actag) {
                $acc_tag_title = $actag['country'];
                $account_comment_tag_id = $actag['account_comment_tag_id'];
//                echo $account_comment_tag_id;
                $standard_title = '';
                if (empty($actag['tads_code'])) {
                    $standard_title = $actag['country'];
                } else {
                    $standard_title = $actag['tads_code'];
                }

                $graph_data_all[] = array();
                $graph_data = array();

                foreach ($period as $dt) {
                    $g_data = $dt->format("Y-m-d");
                    $duration = explode('-', $dt->format("Y-m-d"));
                    $accountTagCnd = array();
                    $docCnd = array();
                    $graph_data['date'] = $dt->format("M-y");
                    $searial_chart_cond[1] = array(
                        "AccountCommentTag.created_date >= ' $g_data'",
                        "AccountCommentTag.created_date <= '$g_data'  + INTERVAL 1 MONTH "
                    );

                    $searial_chart_cond['AccountCommentTag.account_comment_tag_id'] = $account_comment_tag_id;

                    $account_tags_analytics = $this->AccountCommentTag->find('first', array(
                        'joins' => array(
                            array(
                                'table' => 'account_folder_documents as afd',
                                'type' => 'inner',
                                'conditions' => 'AccountCommentTag.ref_id = afd.document_id'
                            ),
                            array(
                                'table' => 'account_folders as af',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = af.account_folder_id'
                            ),
                            array(
                                'table' => 'account_folders_meta_data as afmd',
                                'type' => 'inner',
                                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                            ),
                        ),
                        'conditions' => $searial_chart_cond,
                        'fields' => array(
                            'AccountCommentTag.tag_title',
                            'AccountCommentTag.created_date AS AccountTag__tags_date',
                            'AccountCommentTag.account_comment_tag_id',
                            'AccountCommentTag.account_tag_id',
                            'COUNT(`AccountCommentTag`.`account_comment_tag_id`) AS total_tags'
                        ),
                        'group' => 'tag_title ORDER BY total_tags DESC',
                    ));

                    $graph_data['color'] = $colors[$count];
                    $graph_data['total_tags'] = isset($account_tags_analytics[0]['total_tags']) ? $account_tags_analytics[0]['total_tags'] : '0';
                    $graph_data_all[$actag ['account_comment_tag_id'] . '-' . $standard_title . '-' . $colors[$count]][] = $graph_data;
                    unset($graph_data);
                }
                $count++;
            }
            $data_provider = array();
            $data_charts = array();
            $title = '';
            if (isset($graph_data_all) && $graph_data_all != '') {

                foreach ($graph_data_all as $key => $row) {
                    $total_standards = 0;
                    $total_avg_rattings = 0;
                    $avg_counter = 0;
                    if (count($row) > 0) {
                        $title = explode('-', $key);
                        foreach ($row as $r) {
                            if (!empty($r['total_tags'])) {
                                $total_standards = $total_standards + $r['total_tags'];
                            }

                            if (!empty($r['standard_rating_avg'])) {
                                $total_avg_rattings = $total_avg_rattings + $r['standard_rating_avg'];
                            }

                            if (!empty($r['standard_rating_avg']) && $r['standard_rating_avg'] != 0) {
                                $avg_counter ++;
                            }
                        }
                        if ($avg_counter != 0) {
                            $total_avg_rattings = round($total_avg_rattings / $avg_counter);
                        }


                        if ($total_avg_rattings > 0) {
                            $final_average_rating_name = '';
                            foreach ($metric_new as $metric) {
                                if ($total_avg_rattings == $metric['value']) {
                                    $final_average_rating_name = $metric['name'];
                                }
                            }
                        }

                        if ($total_avg_rattings == 0) {
                            $final_average_rating_name = 'No Rating';
                        }


                        $data_provider = array(
                            'title' => $title[1],
                            'color' => isset($title[2]) ? $title[2] : '',
                            'ratting_title' => 'Average Performance Level',
                            'count' => $count,
                            'data_provider' => json_encode($row),
                            'total_tagged_standards' => $total_standards,
                            'total_avg' => ($role_id == 100) ? $final_average_rating_name : $total_avg_rattings,
                        );
                        $html = $view->element('analytics/frequency_serial_charts', $data_provider);
                        $data_charts[] = $html;
                    }
                    $count++;
                }
            }

            $result['serial_charts'] = $data_charts;
        }
        $account_ids = '';
        if (empty($acc_ids)) {
            $account_ids = $account_id;
        } else {
            $account_ids = $account_id;
        }

        $total_huddles = $this->get_total_huddles_by_user_id($account_ids, $users_ids, $folder_type, $start_date, $end_date);
        $st_date = date_create($st_date);
        $end_date = date_create($end_date);
        $result['filter_date'] = date_format($st_date, 'F d, Y') . ' - ' . date_format($end_date, 'F d, Y');
        $result['total_huddles'] = $total_huddles;
        $result['custom_markers_chart'] = $custom_markers_chart;
        if ($this->request->is('post')) {
            echo json_encode($result);
            die;
        } else {

            return $result;
        }
    }

    function get_cutom_tags($conditions_custom) {


        $joins_custom = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'act.ref_id = afd.document_id'
            ),
            array(
                'table' => 'account_folders_meta_data as afmd',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
            ),
        );

        $fields_custom = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );

        $get_value_custom = array(
            'joins' => $joins_custom,
            'conditions' => $conditions_custom,
            'fields' => $fields_custom,
            'group' => array('AccountTag.account_tag_id'),
            'limit' => 4
        );

        $account_custom_tags_analytics1 = $this->AccountTag->find('all', $get_value_custom);
        $response_custom = array();
        $colors_custom = array('#7fc44f', '#326291', '#ff5900', '#1ebdb0');
        if ($account_custom_tags_analytics1) {
            $count = 0;
            foreach ($account_custom_tags_analytics1 as $row) {
                $response_custom[] = array(
                    'country' => $row['AccountTag']['tag_title'],
                    'tads_code' => $row['AccountTag']['tads_code'],
                    'litres' => $row['AccountTag']['total_tags'],
                    'color' => $colors_custom[$count],
                    'account_tag_id' => $row['AccountTag']['account_tag_id'],
                    'AccountTag__tags_date' => $row['AccountTag']['total_tags'],
                    'AccountTag__total_tags' => $row['AccountTag']['total_tags'],
                );
                $count ++;
            }
        }


        return $response_custom;
    }

    function getAccountFramework() {
        $account_id = $this->request->data['account_id'];
        $frameworks = '';
        if ($account_id != '') {
            $frameworks = $this->AccountTag->find("all", array(
                "conditions" => array(
                    "tag_type" => 2,
                    "account_id" => $account_id
                ),
                'joins' => array(
                    array(
                        'table' => 'accounts as a',
                        'type' => 'inner',
                        'conditions' => 'a.id = AccountTag.account_id'
                    )
                ),
                'fields' => array(
                    'AccountTag.*',
                    'a.company_name'
                )
            ));
            $dropdown = '';
            if ($frameworks) {
                foreach ($frameworks as $row) {
                    $dropdown .= '<option value="' . $row['AccountTag']['account_id'] . '-' . $row['AccountTag']['account_tag_id'] . '" >' . $row['AccountTag']['tag_title'] . '</option>';
                }
            }
            echo $dropdown;
            die;
        } else {
            $user = $this->Session->read('user_current_account');
            $account_id = $user['accounts']['account_id'];
            $frameworks = $this->AccountTag->find("all", array(
                "conditions" => array(
                    "tag_type" => 2,
                    "(a.parent_account_id = $account_id OR a.id = $account_id )"
                ),
                'joins' => array(
                    array(
                        'table' => 'accounts as a',
                        'type' => 'inner',
                        'conditions' => 'a.id = AccountTag.account_id'
                    )
                ),
                'fields' => array(
                    'AccountTag.*',
                    'a.company_name'
                )
            ));
            $dropdown = '';
            if ($frameworks) {
                foreach ($frameworks as $row) {
                    $dropdown .= '<option value="' . $row['AccountTag']['account_id'] . '-' . $row['AccountTag']['account_tag_id'] . '" >' . $row['a']['company_name'] . ' - ' . $row['AccountTag']['tag_title'] . '</option>';
                }
            }
            echo $dropdown;
            die;
        }
    }
    function flush_filter(){
        $this->Session->delete('filters_data');
       echo json_encode(array('success'=>'true'));
       die;

    }

}
