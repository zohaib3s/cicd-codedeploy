<?php

App::uses('AppController', 'Controller');
App::uses('Helper', 'Custom');

use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Aws\Common\Enum\DateFormat;
use Aws\S3\Model\MultipartUpload\UploadId;
use Aws\S3\S3Client;
require_once dirname(__FILE__).'/../aws3/autoload.php';
use Aws3\TranscribeService\TranscribeServiceClient;
//This is copy of ZencoderNotificationController
class ZencoderCronController extends AppController {

    public $uses = array('Document', 'DocumentFiles', 'AccountFolder', 'AccountFolderDocument', 'User', 'OnboardingEmailLog', 'UserActivityLog', 'UserAccount', 'EmailUnsubscribers', 'Account', 'AuditEmail', 'AccountFolderMetaData', 'JobQueue', 'Sites');

    function received_notification($check_amazon_job = 0) {

        // $current_user = $this->Session->read('user_current_account');
        //  $account_id = $current_user['accounts']['account_id'];

        require('src/services.php');

        $this->layout = null;

        if ((isset($_REQUEST['check_amazon_job']) && $_REQUEST['check_amazon_job'] == '1') || $check_amazon_job) {

            $temp_dir = realpath(dirname(__FILE__) . '/../..') . "/app/webroot/files/tempupload/lockdir/";
            if (!is_dir($temp_dir)) {
                mkdir($temp_dir);
            }

            require('src/cronHelper.php');


            if (($pid = cronHelper::lock()) !== FALSE) {

                //start of sites

                // $sites = $this->Sites->find('all'); // It is creating problems on Cli
                $sites = $this->Document->query('select * from sites');

                for ($sites_counter = 0; $sites_counter < count($sites); $sites_counter++) {

                    $site_id = $sites[$sites_counter]['sites']['id'];

                    $_SESSION['site_id'] = $site_id;
                    $this->site_id = $site_id;

                    try {

                        $client = ElasticTranscoderClient::factory(array(
                                    'key' => Configure::read('access_key_id'),
                                    'region' => 'us-east-1',
                                    'secret' => Configure::read('secret_access_key')
                        ));


                        $amazon_videos = $this->Document->find('all', array('conditions' => array('encoder_provider' => 2, 'published' => 0, 'encoder_status' => '', 'aws_transcoder_type' => 1)));


                        for ($counter = 0; $counter < count($amazon_videos); $counter++) {

                            $amazon_video = $amazon_videos[$counter];

                            if (empty($amazon_video['Document']['zencoder_output_id']))
                                continue;
                            if ((int) $amazon_video['Document']['zencoder_output_id'] < -2)
                                continue;

                            $amazon_zencoder_output_id_array = explode($amazon_video['Document']['zencoder_output_id'], '-');



                            //its jobQueue task
                            if (count($amazon_zencoder_output_id_array) == 0)
                                continue;

                            echo $amazon_video['Document']['id'] . "-" . $amazon_video['Document']['zencoder_output_id'];

                            try {

                                if (preg_match("/^\d{13}-\w{6}$/", $amazon_video['Document']['zencoder_output_id'])) {

                                    sleep(1);
                                    $result = $client->readJob(array(
                                        'Id' => $amazon_video['Document']['zencoder_output_id']
                                    ));
                                }
                            } catch (Exception $e) {

                                echo $e->getMessage();
                                continue;
                            }


                            if (isset($result) && isset($result['Job']) && !empty($result['Job']['Status'])) {

                                if ($result['Job']['Status'] == "Complete") {

                                    $is_video_published_now = false;

                                    $job_id = $amazon_video['Document']['zencoder_output_id'];

                                    $video_old = $this->Document->find('first', array('conditions' => array(
                                            'zencoder_output_id' => $amazon_video['Document']['zencoder_output_id'],
                                            'published' => 0)
                                    ));

                                    if (isset($video_old) && !empty($video_old)) {

                                        if ($video_old['Document']['published'] != 1) {
                                            $is_video_published_now = true;
                                        }

                                        $this->Document->create();
                                        $data = array(
                                            'published' => 1,
                                            'encoder_status' => '"' . $result['Job']['Status'] . '"'
                                        );
                                       /*Call AWS Transcribe function*/ 
//                                       $this->custom->TranscribeVideo($amazon_video['Document']["id"],$amazon_video['Document']["url"]); //transcribe funtion
                                       
                                       $this->Document->updateAll($data, array('zencoder_output_id' => $amazon_video['Document']['zencoder_output_id']), $validation = TRUE);
                                       $data = array(
                                            'duration' => $result['Job']['Output']['Duration']
                                        );
                                       
                                       $this->DocumentFiles->updateAll($data, array('document_id' => $amazon_video['Document']['id']), $validation = TRUE);

                                        $video = $this->Document->find('first', array('conditions' => array('zencoder_output_id' => $job_id)));
                                        $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $video['Document']['id'])));
                                        $account_folder = $this->AccountFolder->find('first', array('conditions' => array(
                                                'account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'])
                                        ));
                                        $creator = $this->User->find('first', array('conditions' => array('id' => $video['Document']['created_by'])));
                                        $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_document['AccountFolderDocument']['account_folder_id']);
                                        $account_id = $account_folder['AccountFolder']['account_id'];
                                        $document_files = $this->DocumentFiles->find('first', array('conditions' => array('document_id' => $video['Document']['id'])));


                                        if ($account_folder['AccountFolder']['folder_type'] != 1) {
                                            // Fire Websocket Here
                                            $this->executeWebsocket(true, $account_id, $account_folder_document['AccountFolderDocument']['account_folder_id'], $creator['User']['id'], $video['Document']['id'], $creator, $account_folder_document['AccountFolderDocument']['title'], $account_folder['AccountFolder']['desc'], $account_folder_document['AccountFolderDocument']['document_id'], $document_files['DocumentFiles']['duration']);
                                            if ($this->check_subscription($creator['User']['id'], '7', $account_id)) {

                                                $video_name = $account_folder_document['AccountFolderDocument']['title'];

                                                if (empty($video_name))
                                                    $video_name = $account_folder['AccountFolder']['name'];

                                                var_dump($video_name);
                                                var_dump($account_folder['AccountFolder']['account_folder_id']);
                                                var_dump($video['Document']['id']);
                                                var_dump($creator);
                                                var_dump($account_folder['AccountFolder']['account_id']);
                                                var_dump($account_folder['AccountFolder']['folder_type']);

                                                $r_val = $this->sendVideoPublishedNotificationEx(
                                                        $video_name
                                                        , $account_folder['AccountFolder']['account_folder_id']
                                                        , $video['Document']['id']
                                                        , $creator
                                                        , $account_folder['AccountFolder']['account_id']
                                                        , $creator, $creator['User']['email']
                                                        , $creator['User']['id']
                                                        , $account_folder['AccountFolder']['folder_type']
                                                        , '7'
                                                );
                                            }
                                        } else {

                                            if (count($huddleUsers) > 0) {
                                                if ($is_video_published_now) {
                                                    // Fire Websocket Here
                                                    $this->executeWebsocket(false, $account_id, $account_folder_document['AccountFolderDocument']['account_folder_id'], $creator['User']['id'], $video['Document']['id'], $creator, $account_folder_document['AccountFolderDocument']['title'], $account_folder['AccountFolder']['desc'], $account_folder_document['AccountFolderDocument']['document_id'], $document_files['DocumentFiles']['duration']);
                                                }
                                                for ($i = 0; $i < count($huddleUsers); $i++) {

                                                    $huddleUser = $huddleUsers[$i];

                                                    if ($is_video_published_now) {
                                                        if ($this->check_subscription($huddleUser['User']['id'], '8', $account_id)) {
                                                            $r_val = $this->sendVideoPublishedNotification(
                                                                    $account_folder['AccountFolder']['name']
                                                                    , $account_folder['AccountFolder']['account_folder_id']
                                                                    , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $huddleUsers, $huddleUser['User']['email'], $huddleUser['User']['id'], $account_folder['AccountFolder']['folder_type'], '8');
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } elseif ($result['Job']['Status'] == "Error") {

                                    $job_id = $amazon_video['Document']['zencoder_output_id'];

                                    if (empty($amazon_video['Document']['encoder_status'])) {

                                        //send error email

                                        $this->Document->create();
                                        $data = array(
                                            'encoder_status' => '"' . $result['Job']['Status'] . '"'
                                        );
                                        $this->Document->updateAll($data, array('zencoder_output_id' => $amazon_video['Document']['zencoder_output_id']), $validation = TRUE);

                                        $video = $this->Document->find('first', array('conditions' => array('zencoder_output_id' => $job_id)));
                                        $creator = $this->User->find('first', array('conditions' => array('id' => $video['Document']['created_by'])));

                                        $account_folder_type_string = "Workspace";
                                        $video_title = "";

                                        $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $video['Document']['id'])));

                                        if (isset($account_folder_document)) {

                                            $current_account_folder = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'])));
                                            $video_title = $account_folder_document['AccountFolderDocument']['title'];

                                            if ($current_account_folder['AccountFolder']['folder_type'] == 1) {
                                                $account_folder_type_string = $current_account_folder['AccountFolder']['name'];
                                                if (empty($video_title)) {
                                                    $video_title = $account_folder_type_string;
                                                }
                                            }
                                        }

                                        // Fire Websocket Here with error
                                        $r_val = $this->sendVideoUploadingFailed($creator['User']['email'], $account_folder_type_string, $video_title);
                                    }
                                }
                            }
                        }

                        //get videos to be stitched.

                        $amazon_videos = $this->Document->find('all', array('conditions' => array('encoder_provider' => 2, 'published' => 0, 'aws_transcoder_type' => 1)));

                        for ($j = 0; $j < count($amazon_videos); $j++) {

                            $amazon_video = $amazon_videos[$j];
                            $account_id = $amazon_video['Document']['account_id'];
                            $user_id = $amazon_video['Document']['created_by'];
                            $account_folder_id = abs($amazon_video['Document']['zencoder_output_id']);
                            $video_id = $amazon_video['Document']['id'];

                            try {

                                if (isset($amazon_video['Document']['encoder_status']) && $amazon_video['Document']['encoder_status'] == 'Error')
                                    continue;

                                if ((int) $amazon_video['Document']['zencoder_output_id'] < -2) {


                                    $videos_meta_data = $this->AccountFolderMetaData->find('first', array('conditions' => array(
                                            'account_folder_id' => $account_folder_id,
                                            'meta_data_name' => 'file_upload_' . $video_id . '_totalVideoCount'
                                        )
                                            )
                                    );

                                    if (isset($videos_meta_data))
                                        $totalVideoCount = $videos_meta_data['AccountFolderMetaData']['meta_data_value'];
                                    else
                                        $totalVideoCount = -1;


                                    $videos_meta_data = $this->AccountFolderMetaData->find('first', array('conditions' => array(
                                            'account_folder_id' => $account_folder_id,
                                            'meta_data_name' => 'file_upload_' . $video_id . '_sibmeFileName'
                                        )
                                            )
                                    );

                                    if (isset($videos_meta_data))
                                        $sibmeFileName = $videos_meta_data['AccountFolderMetaData']['meta_data_value'];

                                    $videos_meta_data = $this->AccountFolderMetaData->find('first', array('conditions' => array(
                                            'account_folder_id' => $account_folder_id,
                                            'meta_data_name' => 'file_upload_' . $video_id . '_chunkCount'
                                        )
                                            )
                                    );

                                    if (isset($videos_meta_data))
                                        $chunkCount = $videos_meta_data['AccountFolderMetaData']['meta_data_value'];

                                    $videos_meta_data = $this->AccountFolderMetaData->find('first', array('conditions' => array(
                                            'account_folder_id' => $account_folder_id,
                                            'meta_data_name' => 'file_upload_' . $video_id . '_file_name'
                                        )
                                            )
                                    );

                                    if (isset($videos_meta_data))
                                        $file_name = $videos_meta_data['AccountFolderMetaData']['meta_data_value'];

                                    $videos_meta_data = $this->AccountFolderMetaData->find('first', array('conditions' => array(
                                            'account_folder_id' => $account_folder_id,
                                            'meta_data_name' => 'file_upload_' . $video_id . '_title'
                                        )
                                            )
                                    );

                                    if (isset($videos_meta_data))
                                        $title = $videos_meta_data['AccountFolderMetaData']['meta_data_value'];

                                    $videos_meta_data = $this->AccountFolderMetaData->find('first', array('conditions' => array(
                                            'account_folder_id' => $account_folder_id,
                                            'meta_data_name' => 'file_upload_' . $video_id . '_chunk_filename_pattern'
                                        )
                                            )
                                    );

                                    if (isset($videos_meta_data))
                                        $chunk_filename_pattern = $videos_meta_data['AccountFolderMetaData']['meta_data_value'];

                                    $huddle_id = $account_folder_id;

                                    $videos_meta_data = $this->AccountFolderMetaData->find('first', array('conditions' => array(
                                            'account_folder_id' => $account_folder_id,
                                            'meta_data_name' => 'file_upload_' . $video_id . '_device_token'
                                        )
                                            )
                                    );

                                    if (isset($videos_meta_data))
                                        $device_token = $videos_meta_data['AccountFolderMetaData']['meta_data_value'];

                                    $videos_meta_data = $this->AccountFolderMetaData->find('first', array('conditions' => array(
                                            'account_folder_id' => $account_folder_id,
                                            'meta_data_name' => 'file_upload_' . $video_id . '_direct_publish'
                                        )
                                            )
                                    );

                                    if (isset($videos_meta_data))
                                        $direct_publish = $videos_meta_data['AccountFolderMetaData']['meta_data_value'];

                                    $s3_zencoder_id = 0;

                                    if ($totalVideoCount == -1) {


                                        if (isset($videos_meta_data)) {


                                            $path_parts = pathinfo($sibmeFileName);
                                            $sibmeFileNameOnly = $path_parts['filename'];

                                            $video_file_name = $this->cleanFileName($path_parts['filename']) . "." . $path_parts['extension'];
                                            $video_file_name_only = $this->cleanFileName($path_parts['filename']);
                                            $parent_folder_url = "temp/$account_id/$user_id/" . $sibmeFileNameOnly;

                                            //echo $uploaded_video_file; die;
                                            $s3_service = new S3Services(
                                                    Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                                            );

                                            $client = S3Client::factory(array(
                                                        'key' => Configure::read('access_key_id'),
                                                        'secret' => Configure::read('secret_access_key')
                                            ));

                                            if ($chunkCount == 1) {

                                                // Copy a file.
                                                if ($path_parts['extension'] == "mp4") {
                                                    $s3_service->copy_from_s3_wcontent($parent_folder_url . "/" . $chunk_filename_pattern . "chunk0.bin", $parent_folder_url . "/" . $file_name, 'video/mp4');
                                                } else {
                                                    $s3_service->copy_from_s3($parent_folder_url . "/" . $chunk_filename_pattern . "chunk0.bin", $parent_folder_url . "/" . $file_name);
                                                }
                                            } else {

                                                if ($path_parts['extension'] == "mp4") {

                                                    $model = $client->createMultipartUpload(array(
                                                        'Bucket' => Configure::read('bucket_name'),
                                                        'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName,
                                                        'ContentType' => 'video/mp4'
                                                    ));
                                                } else {

                                                    $model = $client->createMultipartUpload(array(
                                                        'Bucket' => Configure::read('bucket_name'),
                                                        'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName
                                                    ));
                                                }

                                                $uploadId = $model->get('UploadId');

                                                for ($i = 0; $i < $chunkCount; $i ++) {

                                                    $result = $client->uploadPartCopy(array(
                                                        'Bucket' => Configure::read('bucket_name'),
                                                        'CopySource' => Configure::read('bucket_name') . "/temp/$account_id/$user_id/$sibmeFileNameOnly/$chunk_filename_pattern" . "chunk$i.bin",
                                                        'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName,
                                                        'PartNumber' => ($i + 1),
                                                        'UploadId' => $uploadId
                                                    ));
                                                }

                                                $partsModel = $client->listParts(array(
                                                    'Bucket' => Configure::read('bucket_name'),
                                                    'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName,
                                                    'UploadId' => $uploadId
                                                ));

                                                $model = $client->completeMultipartUpload(array(
                                                    'Bucket' => Configure::read('bucket_name'),
                                                    'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName,
                                                    'UploadId' => $uploadId,
                                                    'Parts' => $partsModel['Parts'],
                                                ));
                                            }


                                            //uploaded video
                                            $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                                            $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                                            $s3_src_folder_path = $parent_folder_url . "/" . $file_name;

                                            $s3_zencoder_id = $this->CreateJobQueueTranscodingJob($video_id, $s3_src_folder_path);


                                            $this->Document->create();
                                            $this->Document->updateAll(
                                                    array(
                                                'zencoder_output_id' => $s3_zencoder_id,
                                                'url' => "'$s3_src_folder_path'",
                                                'encoder_provider' => Configure::read('encoder_provider'),
                                                'file_size' => (!isset($this->request->data['video_file_size']) ? 0 : $this->request->data['video_file_size'])
                                                    ), array('id' => $video_id)
                                            );
                                        }//end if old Apps
                                    } //old App Uploads
                                    else {

                                        //new App uploads.

                                        if (isset($videos_meta_data)) {


                                            $path_parts = pathinfo($sibmeFileName);
                                            $sibmeFileNameOnly = $path_parts['filename'];
                                            $chunkCountArray = explode(',', $chunkCount);


                                            $video_file_name = $this->cleanFileName($path_parts['filename']) . "." . $path_parts['extension'];
                                            $video_file_name_only = $this->cleanFileName($path_parts['filename']);
                                            $parent_folder_url = "temp/$account_id/$user_id/" . $sibmeFileNameOnly;

                                            //echo $uploaded_video_file; die;
                                            $s3_service = new S3Services(
                                                    Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                                            );

                                            $client = S3Client::factory(array(
                                                        'key' => Configure::read('access_key_id'),
                                                        'secret' => Configure::read('secret_access_key')
                                            ));

                                            if ($totalVideoCount == 1) { // this means new App but with 1 clip.
                                                $chunkCount = (int) $chunkCountArray[0];

                                                if ($chunkCount == 1) {

                                                    // Copy a file.
                                                    if ($path_parts['extension'] == "mp4") {
                                                        $s3_service->copy_from_s3_wcontent($parent_folder_url . "/" . $chunk_filename_pattern . "chunk0_$totalVideoCount.bin", $parent_folder_url . "/" . $file_name, 'video/mp4');
                                                    } else {
                                                        $s3_service->copy_from_s3($parent_folder_url . "/" . $chunk_filename_pattern . "chunk0_$totalVideoCount.bin", $parent_folder_url . "/" . $file_name);
                                                    }
                                                } else {

                                                    if ($path_parts['extension'] == "mp4") {

                                                        $model = $client->createMultipartUpload(array(
                                                            'Bucket' => Configure::read('bucket_name'),
                                                            'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName,
                                                            'ContentType' => 'video/mp4'
                                                        ));
                                                    } else {

                                                        $model = $client->createMultipartUpload(array(
                                                            'Bucket' => Configure::read('bucket_name'),
                                                            'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName
                                                        ));
                                                    }

                                                    $uploadId = $model->get('UploadId');

                                                    for ($i = 0; $i < $chunkCount; $i ++) {


                                                        $result = $client->uploadPartCopy(array(
                                                            'Bucket' => Configure::read('bucket_name'),
                                                            'CopySource' => Configure::read('bucket_name') . "/temp/$account_id/$user_id/$sibmeFileNameOnly/$chunk_filename_pattern" . "chunk" . $i . "_" . $totalVideoCount . ".bin",
                                                            'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName,
                                                            'PartNumber' => ($i + 1),
                                                            'UploadId' => $uploadId
                                                        ));
                                                    }

                                                    $partsModel = $client->listParts(array(
                                                        'Bucket' => Configure::read('bucket_name'),
                                                        'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName,
                                                        'UploadId' => $uploadId
                                                    ));

                                                    $model = $client->completeMultipartUpload(array(
                                                        'Bucket' => Configure::read('bucket_name'),
                                                        'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileName,
                                                        'UploadId' => $uploadId,
                                                        'Parts' => $partsModel['Parts'],
                                                    ));
                                                }


                                                //uploaded video
                                                $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                                                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                                                $s3_src_folder_path = $parent_folder_url . "/" . $file_name;

                                                $s3_zencoder_id = $this->CreateJobQueueTranscodingJob($video_id, $s3_src_folder_path);


                                                $this->Document->create();
                                                $this->Document->updateAll(
                                                        array(
                                                    'zencoder_output_id' => $s3_zencoder_id,
                                                    'url' => "'$s3_src_folder_path'",
                                                    'encoder_provider' => Configure::read('encoder_provider'),
                                                    'file_size' => (!isset($this->request->data['video_file_size']) ? 0 : $this->request->data['video_file_size'])
                                                        ), array('id' => $video_id)
                                                );
                                            } //end new App but with 1 Clip.
                                            else {

                                                //Start new App with multiple clips


                                                for ($clipCounter = 0; $clipCounter < $totalVideoCount; $clipCounter++) {

                                                    $current_clip_counter = $clipCounter + 1;
                                                    $sibmeFileNameClip = $sibmeFileNameOnly . "_" . $current_clip_counter . "." . $path_parts['extension'];

                                                    $chunkCount = 0;

                                                    $chunk_objects = $client->getIterator('ListObjects', array(
                                                        "Bucket" => Configure::read('bucket_name'),
                                                        "Prefix" => "temp/$account_id/$user_id/$sibmeFileNameOnly/"
                                                    ));

                                                    foreach ($chunk_objects as $chunk_object) {

                                                        if (strpos($chunk_object['Key'], "_" . $current_clip_counter . ".bin") !== false) {
                                                            $chunkCount +=1;
                                                        }
                                                    }

                                                    if ($chunkCount == 0)
                                                        $chunkCount = 1;

                                                    if ($chunkCount == 1) {

                                                        // Copy a file.
                                                        if ($path_parts['extension'] == "mp4") {
                                                            $s3_service->copy_from_s3_wcontent($parent_folder_url . "/" . $chunk_filename_pattern . "chunk0_$current_clip_counter.bin", $parent_folder_url . "/" . $sibmeFileNameClip, 'video/mp4');
                                                        } else {
                                                            $s3_service->copy_from_s3($parent_folder_url . "/" . $chunk_filename_pattern . "chunk0_$current_clip_counter.bin", $parent_folder_url . "/" . $sibmeFileNameClip);
                                                        }
                                                    } else {

                                                        if ($path_parts['extension'] == "mp4") {

                                                            $model = $client->createMultipartUpload(array(
                                                                'Bucket' => Configure::read('bucket_name'),
                                                                'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileNameClip,
                                                                'ContentType' => 'video/mp4'
                                                            ));
                                                        } else {

                                                            $model = $client->createMultipartUpload(array(
                                                                'Bucket' => Configure::read('bucket_name'),
                                                                'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileNameClip
                                                            ));
                                                        }

                                                        $uploadId = $model->get('UploadId');

                                                        for ($i = 0; $i < $chunkCount; $i ++) {


                                                            $result = $client->uploadPartCopy(array(
                                                                'Bucket' => Configure::read('bucket_name'),
                                                                'CopySource' => Configure::read('bucket_name') . "/temp/$account_id/$user_id/$sibmeFileNameOnly/$chunk_filename_pattern" . "chunk" . $i . "_" . $current_clip_counter . ".bin",
                                                                'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileNameClip,
                                                                'PartNumber' => ($i + 1),
                                                                'UploadId' => $uploadId
                                                            ));
                                                        }

                                                        $partsModel = $client->listParts(array(
                                                            'Bucket' => Configure::read('bucket_name'),
                                                            'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileNameClip,
                                                            'UploadId' => $uploadId
                                                        ));

                                                        $model = $client->completeMultipartUpload(array(
                                                            'Bucket' => Configure::read('bucket_name'),
                                                            'Key' => "temp/$account_id/$user_id/" . $sibmeFileNameOnly . '/' . $sibmeFileNameClip,
                                                            'UploadId' => $uploadId,
                                                            'Parts' => $partsModel['Parts'],
                                                        ));
                                                    }
                                                }

                                                //uploaded video
                                                $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                                                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                                                $s3_src_folder_path = $parent_folder_url . "/" . $file_name;

                                                $s3_zencoder_id = $this->CreateJobQueueTranscodingJob($video_id, $s3_src_folder_path);


                                                $this->Document->create();
                                                $this->Document->updateAll(
                                                        array(
                                                    'zencoder_output_id' => $s3_zencoder_id,
                                                    'url' => "'$s3_src_folder_path'",
                                                    'encoder_provider' => Configure::read('encoder_provider'),
                                                    'file_size' => (!isset($this->request->data['video_file_size']) ? 0 : $this->request->data['video_file_size'])
                                                        ), array('id' => $video_id)
                                                );
                                            } //end new App with Multiple clips.
                                        }//end if
                                    } // New App uploads.
                                }
                            } catch (Exception $e) {

                                $this->Document->create();
                                $data = array(
                                    'encoder_status' => '"Error"'
                                );
                                $this->Document->updateAll($data, array('id' => $video_id), $validation = TRUE);

                                $video = $this->Document->find('first', array('conditions' => array('id' => $video_id)));
                                $creator = $this->User->find('first', array('conditions' => array('id' => $video['Document']['created_by'])));

                                $account_folder_type_string = "Workspace";
                                $video_title = "";

                                $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $video['Document']['id'])));

                                if (isset($account_folder_document)) {

                                    $current_account_folder = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'])));
                                    $video_title = $account_folder_document['AccountFolderDocument']['title'];

                                    if ($current_account_folder['AccountFolder']['folder_type'] == 1) {
                                        $account_folder_type_string = $current_account_folder['AccountFolder']['name'];
                                        if (empty($video_title)) {
                                            $video_title = $account_folder_type_string;
                                        }
                                    }
                                }

                                $this->sendVideoUploadingFailed($creator['User']['email'], $account_folder_type_string, $video_title);
                            }
                            //end catch
                        }//end of FOR Loop

                        cronHelper::unlock();
                    } catch (Exception $e) {

                        cronHelper::unlock();
                    }
                }
                //end of sites
                //check if there is no response on live observations

                $results = $this->UserActivityLog->find('all', array(
                    'conditions' => array(
                        'type' => 20,
                        "ref_id NOT IN ( select ref_id from user_activity_logs where type = 21)",
                        "TIMESTAMPDIFF(SECOND, date_added, now()) > 400"
                    )
                ));

                if (isset($results) && count($results) > 0) {

                    foreach ($results as $result) {

                        $document_details = $this->Document->find('first', array(
                            'conditions' => array(
                                'id' => $result['UserActivityLog']['ref_id'],
                            )
                        ));

                        if (isset($document_details['Document'])) {

                            $dateFromDatabase = strtotime($document_details['Document']['last_edit_date']);
                            $dateFiveMinutesAgo = strtotime("-5 minutes");

                            $start_date = new DateTime($document_details['Document']['last_edit_date']);

                            $start_date_end = new DateTime();
                            $start_date_end->setTimestamp(strtotime('now'));

                            $since_start = $start_date->diff($start_date_end);

                            if ($since_start->i >= 5) {
                                $user_activity_logs = array(
                                    'ref_id' => $result['UserActivityLog']['ref_id'],
                                    'account_id' => $result['UserActivityLog']['account_id'],
                                    'user_id' => $result['UserActivityLog']['user_id'],
                                    'desc' => 'Observation_Stopped',
                                    'url' => $result['UserActivityLog']['url'],
                                    'account_folder_id' => $result['UserActivityLog']['account_folder_id'],
                                    'type' => '21',
                                    'environment_type' => $result['UserActivityLog']['environment_type']
                                );
                                $this->user_activity_logs($user_activity_logs, $result['UserActivityLog']['account_id'], $result['UserActivityLog']['user_id']);

                                $update_array = array(
                                    'is_processed' => 4,
                                    'upload_status' => "'" . 'cancelled' . "'"
                                );

                                $this->Document->updateAll($update_array, array('id' => $result['UserActivityLog']['ref_id']), $validation = TRUE);
                            }
                        }
                    }
                }



                $results = $this->UserActivityLog->find('all', array(
                    'conditions' => array(
                        'type' => 24,
                        "ref_id NOT IN ( select ref_id from user_activity_logs where type = 25)",
                        "TIMESTAMPDIFF(HOUR, date_added, now()) > 23"
                    )
                ));

                if (isset($results) && count($results) > 0) {

                    foreach ($results as $result) {

                        $document_details = $this->Document->find('first', array(
                            'conditions' => array(
                                'id' => $result['UserActivityLog']['ref_id'],
                            )
                        ));

                        $dateFromDatabase = strtotime($document_details['Document']['last_edit_date']);
                        $dateFiveMinutesAgo = strtotime("-1 minutes");

                        $start_date = new DateTime($document_details['Document']['last_edit_date']);

                        $start_date_end = new DateTime();
                        $start_date_end->setTimestamp(strtotime('now'));

                        $since_start = $start_date->diff($start_date_end);

                        if ($since_start->i >= 1) {
                            $user_activity_logs = array(
                                'ref_id' => $result['UserActivityLog']['ref_id'],
                                'account_id' => $result['UserActivityLog']['account_id'],
                                'user_id' => $result['UserActivityLog']['user_id'],
                                'desc' => 'Live_Recording_Stopped',
                                'url' => $result['UserActivityLog']['url'],
                                'account_folder_id' => $result['UserActivityLog']['account_folder_id'],
                                'type' => '25',
                                'environment_type' => $result['UserActivityLog']['environment_type']
                            );
                            $this->user_activity_logs($user_activity_logs, $result['UserActivityLog']['account_id'], $result['UserActivityLog']['user_id']);

                            $update_array = array(
                                'is_processed' => 4,
                                'upload_status' => "'" . 'cancelled' . "'"
                            );

                            $this->Document->updateAll($update_array, array('id' => $result['UserActivityLog']['ref_id']), $validation = TRUE);
                        }
                    }
                }

                $results = $this->Document->find('all', array(
                    'conditions' => array(
                        'is_processed' => 5,
                        "id IN (select ref_id from user_activity_logs where type = 21)",
                        'upload_progress' => 0,
                        "upload_status is NULL"
                    )
                ));

                foreach ($results as $result) {

                    var_dump($result);

                    $dateFromDatabase = strtotime($result['Document']['last_edit_date']);
                    $dateFiveMinutesAgo = strtotime("-5 minutes");

                    $start_date = new DateTime($result['Document']['last_edit_date']);

                    $start_date_end = new DateTime();
                    $start_date_end->setTimestamp(strtotime('now'));

                    $since_start = $start_date->diff($start_date_end);

                    if ($since_start->i >= 5) {

                        $update_array = array(
                            'is_processed' => 4,
                            'upload_status' => "'" . 'cancelled' . "'"
                        );

                        $this->Document->updateAll($update_array, array('id' => $result['Document']['id']), $validation = TRUE);
                    }
                }

                $results = $this->Document->find('all', array(
                    'conditions' => array(
                        'is_processed' => 5,
                        "id IN (select ref_id from user_activity_logs where type = 21)",
                        'upload_progress' => 0,
                        "upload_status" => 'cancelled'
                    )
                ));

                foreach ($results as $result) {

                    var_dump($result);

                    $dateFromDatabase = strtotime($result['Document']['last_edit_date']);
                    $dateFiveMinutesAgo = strtotime("-5 minutes");

                    $start_date = new DateTime($result['Document']['last_edit_date']);

                    $start_date_end = new DateTime();
                    $start_date_end->setTimestamp(strtotime('now'));

                    $since_start = $start_date->diff($start_date_end);

                    if ($since_start->i >= 5) {

                        $update_array = array(
                            'is_processed' => 4
                        );

                        $this->Document->updateAll($update_array, array('id' => $result['Document']['id']), $validation = TRUE);
                    }
                }


                //for day7 if account has activity
                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) as name FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id INNER JOIN user_activity_logs ual ON a.id = ual.account_id and ual.user_id = ua.user_id WHERE a.is_active = 1 AND a.in_trial = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND u.is_active = 1 AND u.id NOT IN (2422,2423) AND u.site_id=" . $this->site_id . " AND ual.type IN (1,2,3,4,5) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 7
                        )
                    ));
                    if ($onboardingLogCount == 0) {
                        if ($all_users[$i]['ua']['role_id'] == 120) {
                            $data['template_id'] = Configure::read('day7_active_user');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 7;
                            $data['subject'] = "Use Sibme , Anytime, Anywhere!";
                            //    $this->send_email_template($data);
                        }
                        if ($all_users[$i]['ua']['role_id'] == 110 || $all_users[$i]['ua']['role_id'] == 100) {
                            $data['template_id'] = Configure::read('day7_active_superuser');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 7;
                            $data['subject'] = "Invite Your Team to Sibme";
                            $this->send_email_template($data);
                        }
                        if ($all_users[$i]['ua']['role_id'] == 100) {
                            $data['template_id'] = Configure::read('day7_active_accountowner');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 7;
                            $data['subject'] = "Track your Team's Performance";
                            //     $this->send_email_template($data);
                        }
                    }
                }

                //for day7 if account has no activity
                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) AS name FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id WHERE a.is_active = 1 AND a.in_trial = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND u.is_active = 1 AND u.site_id=" . $this->site_id . " AND u.id NOT IN (2422,2423) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 7
                        )
                    ));
                    $activityLogCount = $this->UserActivityLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'type' => 'IN (1,2,3,4,5)'
                        )
                    ));
                    if ($onboardingLogCount == 0 && $activityLogCount == 0) {
                        if ($all_users[$i]['ua']['role_id'] == 120) {
                            $data['template_id'] = Configure::read('day7_inactive_user');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 7;
                            $data['subject'] = "You haven't been back";
                            //     $this->send_email_template($data);
                        }
                        if ($all_users[$i]['ua']['role_id'] == 110) {
                            $data['template_id'] = Configure::read('day7_inactive_superuser');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 7;
                            $data['subject'] = "Set up your coaching Huddles";
                            //    $this->send_email_template($data);
                        }
                        if ($all_users[$i]['ua']['role_id'] == 100) {
                            $data['template_id'] = Configure::read('day7_inactive_accountowner');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 7;
                            $data['subject'] = "We haven't seen you online in a while";
                            $this->send_email_template($data);
                        }
                    }
                }

                //for day14 if account has no activity
                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) AS name FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id WHERE a.is_active = 1 AND a.in_trial = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 13 DAY) AND u.is_active = 1 AND u.site_id=" . $this->site_id . " AND u.id NOT IN (2422,2423) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 14
                        )
                    ));
                    $activityLogCount = $this->UserActivityLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'type' => 'IN (1,2,3,4,5)'
                        )
                    ));
                    if ($onboardingLogCount == 0 && $activityLogCount == 0) {
                        if ($all_users[$i]['ua']['role_id'] == 120) {
                            $data['template_id'] = Configure::read('day14_inactive_user');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 14;
                            $data['subject'] = "Let's upload your first video!";
                            //      $this->send_email_template($data);
                        }
                        if ($all_users[$i]['ua']['role_id'] == 110) {
                            $data['template_id'] = Configure::read('day14_inactive_superuser');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 14;
                            $data['subject'] = "Email us; we would like to hear from you!";
                            //    $this->send_email_template($data);
                        }
                        if ($all_users[$i]['ua']['role_id'] == 100) {
                            $data['template_id'] = Configure::read('day14_inactive_accountowner');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 14;
                            $data['subject'] = "Sibme is Flexible! See how our adopters use Sibme";
                            $this->send_email_template($data);
                        }
                    }
                }

                //for day18 if account has activity
                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) as name FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id INNER JOIN user_activity_logs ual ON a.id = ual.account_id and ual.user_id = ua.user_id WHERE a.is_active = 1 AND a.in_trial = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 17 DAY) AND u.is_active = 1 AND u.id NOT IN (2422,2423) AND ual.site_id=" . $this->site_id . " AND ual.type IN (1,2,3,4,5) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 18
                        )
                    ));
                    if ($onboardingLogCount == 0) {
                        if ($all_users[$i]['ua']['role_id'] == 120) {
                            $data['template_id'] = Configure::read('day18_active_user');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 18;
                            $data['subject'] = "Let us know how you are doing?";
                            $this->send_email_template($data);
                        }
                        if ($all_users[$i]['ua']['role_id'] == 110) {
                            $data['template_id'] = Configure::read('day18_active_superuser');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 18;
                            $data['subject'] = "Let us know how you are doing?";
                            $this->send_email_template($data);
                        }
                        if ($all_users[$i]['ua']['role_id'] == 100) {
                            $data['template_id'] = Configure::read('day18_active_accountowner');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 18;
                            $data['subject'] = "Let us know how you are doing?";
                            $this->send_email_template($data);
                        }
                    }
                }
                //for day25
                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) AS NAME FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id WHERE a.is_active = 1 AND a.in_trial = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 24 DAY) AND u.site_id=" . $this->site_id . " AND u.is_active = 1 AND u.id NOT IN (2422,2423) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 25
                        )
                    ));
                    if ($onboardingLogCount == 0) {

                        if ($all_users[$i]['ua']['role_id'] == 100) {

                            $data['template_id'] = Configure::read('day25_all_users');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 25;
                            $data['subject'] = "You have 5 more days for your Sibme Trial";
                            $this->send_email_template($data);
                        }
                    }
                }

                //for day30
                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) AS NAME FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id WHERE a.is_active = 1 AND a.in_trial = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 29 DAY) AND u.site_id=" . $this->site_id . " AND u.is_active = 1 AND u.id NOT IN (2422,2423) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 30
                        )
                    ));
                    if ($onboardingLogCount == 0) {

                        if ($all_users[$i]['ua']['role_id'] == 100) {

                            $data['template_id'] = Configure::read('day30_all_users');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 30;
                            $data['subject'] = "It has been 30 days and the trial is over";
                            $this->send_email_template($data);
                        }
                    }
                }

                //for day31

                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) AS NAME FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id WHERE a.is_active = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND a.in_trial = 0 AND u.site_id =" . $this->site_id . " AND u.is_active = 1 AND u.id NOT IN (2422,2423) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 31
                        )
                    ));
                    if ($onboardingLogCount == 0) {

                        if ($all_users[$i]['ua']['role_id'] == 100) {

                            $data['template_id'] = Configure::read('day31_all_users');
                            $data['email'] = $all_users[$i]['u']['email'];
                            $data['name'] = $all_users[$i][0]['name'];
                            $data['user_id'] = $all_users[$i]['ua']['user_id'];
                            $data['account_id'] = $all_users[$i]['ua']['account_id'];
                            $data['day'] = 31;
                            $data['subject'] = "Your Sibme Upgrade is Successful";
                            $this->send_email_template($data);
                        }
                    }
                }

                //for day40

                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) AS NAME FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id WHERE a.is_active = 1 AND a.in_trial = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 39 DAY) AND u.site_id=" . $this->site_id . " AND u.is_active = 1 AND u.id NOT IN (2422,2423) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 40
                        )
                    ));
                    if ($onboardingLogCount == 0) {

                        $data['template_id'] = Configure::read('day40_all_users');
                        $data['email'] = $all_users[$i]['u']['email'];
                        $data['name'] = $all_users[$i][0]['name'];
                        $data['user_id'] = $all_users[$i]['ua']['user_id'];
                        $data['account_id'] = $all_users[$i]['ua']['account_id'];
                        $data['day'] = 40;
                        $data['subject'] = "You have 5 more days";
                        //           $this->send_email_template($data);
                    }
                }

                //for day45

                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) AS NAME FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id WHERE a.is_active = 1 AND a.in_trial = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 44 DAY) AND u.site_id=" . $this->site_id . " AND u.is_active = 1 AND u.id NOT IN (2422,2423) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 45
                        )
                    ));
                    if ($onboardingLogCount == 0) {

                        $data['template_id'] = Configure::read('day45_all_users');
                        $data['email'] = $all_users[$i]['u']['email'];
                        $data['name'] = $all_users[$i][0]['name'];
                        $data['user_id'] = $all_users[$i]['ua']['user_id'];
                        $data['account_id'] = $all_users[$i]['ua']['account_id'];
                        $data['day'] = 45;
                        $data['subject'] = "Its time to UPGRADE your Sibme account now";
                        //      $this->send_email_template($data);
                    }
                }

                //for day46

                $all_users = $this->UserAccount->query("SELECT ua.account_id,ua.user_id,ua.role_id,u.email,CONCAT(first_name,' ',last_name) AS NAME FROM accounts a INNER JOIN users_accounts ua ON a.id = ua.account_id INNER JOIN users u ON ua.user_id = u.id WHERE a.is_active = 1 AND DATE(a.created_at) = DATE_SUB(CURDATE(), INTERVAL 45 DAY) AND a.in_trial = 0 AND u.site_id=" . $this->site_id . " AND u.is_active = 1 AND u.id NOT IN (2422,2423) GROUP BY u.id");
                for ($i = 0; $i < count($all_users); $i++) {
                    $onboardingLogCount = $this->OnboardingEmailLog->find('count', array(
                        'conditions' => array(
                            'account_id' => $all_users[$i]['ua']['account_id'],
                            'user_id' => $all_users[$i]['ua']['user_id'],
                            'day' => 46
                        )
                    ));
                    if ($onboardingLogCount == 0) {

                        $data['template_id'] = Configure::read('day46_all_users');
                        $data['email'] = $all_users[$i]['u']['email'];
                        $data['name'] = $all_users[$i][0]['name'];
                        $data['user_id'] = $all_users[$i]['ua']['user_id'];
                        $data['account_id'] = $all_users[$i]['ua']['account_id'];
                        $data['day'] = 46;
                        $data['subject'] = "Congratulations and welcome to Sibme!";
                        //         $this->send_email_template($data);
                    }
                }
            } //end CronHelper


            echo "check_amazon_job_done";
            $this->set('ajaxdata', "check_amazon_job_done");
            die;
        } else {

            require_once('src/Services/Zencoder.php');
            $zencoder = new Services_Zencoder('39bdc265c56dc8e09b62b8fd38f475f9');
            // Catch notification
            $notification = $zencoder->notifications->parseIncoming();

            if (isset($notification)) {

                $job_id = $notification->job->id;

                //if ($notification->job->state == 'finished') {

                $is_video_published_now = false;

                $video_old = $this->Document->find('first', array('conditions' => array('zencoder_output_id' => $job_id, 'published' => 0)));

                if (isset($video_old) && !empty($video_old)) {

                    if ($video['Document']['published'] != 1) {
                        $is_video_published_now = true;
                    }

                    $this->Document->create();
                    $data = array(
                        'published' => 1
                    );
                    $this->Document->updateAll($data, array('zencoder_output_id' => $job_id), $validation = TRUE);

                    $video = $this->Document->find('first', array('conditions' => array('zencoder_output_id' => $job_id)));
                    $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('zencoder_output_id' => $job_id)));
                    $account_folder = $this->AccountFolder->find('first', array('conditions' => array('account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'])));
                    $creator = $this->User->find('first', array('conditions' => array('id' => $video['Document']['created_by'])));
                    $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_document['AccountFolderDocument']['account_folder_id']);


                    if ($account_folder['AccountFolder']['folder_type'] != 1) {
                        if ($this->check_subscription($creator['User']['id'], '7', $account_id)) {

                            $video_name = $account_folder_document['AccountFolderDocument']['title'];

                            if (empty($video_name))
                                $video_name = $account_folder['AccountFolder']['name'];

                            $this->sendVideoPublishedNotification(
                                    $video_name
                                    , $account_folder['AccountFolder']['account_folder_id']
                                    , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $creator, $creator['User']['email'], $creator['User']['id'], $account_folder['AccountFolder']['folder_type'], '7');
                        }
                    } else {

                        if (count($huddleUsers) > 0) {
                            for ($i = 0; $i < count($huddleUsers); $i++) {

                                $huddleUser = $huddleUsers[$i];

                                if ($is_video_published_now) {
                                    if ($this->check_subscription($huddleUser['User']['id'], '8', $account_id)) {
                                        $this->sendVideoPublishedNotification(
                                                $account_folder['AccountFolder']['name']
                                                , $account_folder['AccountFolder']['account_folder_id']
                                                , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $huddleUsers, $huddleUser['User']['email'], $huddleUser['User']['id'], $account_folder['AccountFolder']['folder_type'], '8');
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $this->set('ajaxdata', "$job_id.$state_job");
        }

        $this->render('/Elements/ajax/ajaxreturn');
    }

    function send_email_template($data) {

        $to = $data['email'];
        $subject = $data['subject'];
        $use_template = $data['template_id'];
        $name = $data['name'];
        $user_id = $data['user_id'];
        $account_id = $data['account_id'];
        $day = $data['day'];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sendgrid.com/v3/templates/$use_template",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic ZGF2ZXdAc2libWUuY29tOmM5ODhZWXdzIUA=",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $json = json_decode($response);
        }
        $html = str_replace('<%body%>', '', $json->versions[0]->html_content);
        $user_details = $this->User->find('first', array('conditions' => array('email' => $data['email'])));
        $html = str_replace('[Insert the User Name Here]', $user_details['User']['first_name'] . ' ' . $user_details['User']['last_name'], $html);
        $html = str_replace('{account_id}', $account_id, $html);
        $this->OnboardingEmailLog->create();
        $onboardingEmail = array(
            'account_id' => $account_id,
            'user_id' => $user_id,
            'day' => $day
        );

        $this->OnboardingEmailLog->save($onboardingEmail, $validation = TRUE);

        $this->add_job_queue(1, $to, $subject, $html, true);
        return TRUE;
    }

    function video_success_message_zencoder($document_id) {


        $current_user = $this->Session->read('user_current_account');
        $account_id = $current_user['accounts']['account_id'];

        $is_video_published_now = false;

        $video_old = $this->Document->find('first', array('conditions' => array(
                'id' => $document_id)
        ));



        if (isset($video_old) && !empty($video_old)) {


            $is_video_published_now = true;


            $video = $this->Document->find('first', array('conditions' => array('id' => $document_id)));
            $account_folder_document = $this->AccountFolderDocument->find('first', array('conditions' => array('document_id' => $document_id)));
            $account_folder = $this->AccountFolder->find('first', array('conditions' => array(
                    'account_folder_id' => $account_folder_document['AccountFolderDocument']['account_folder_id'])
            ));
            $creator = $this->User->find('first', array('conditions' => array('id' => $video['Document']['created_by'])));
            $huddleUsers = $this->AccountFolder->getHuddleUsers($account_folder_document['AccountFolderDocument']['account_folder_id']);



            if ($account_folder['AccountFolder']['folder_type'] != 1) {
                if ($this->check_subscription($creator['User']['id'], '7', $account_id)) {

                    $video_name = $account_folder_document['AccountFolderDocument']['title'];

                    if (empty($video_name))
                        $video_name = $account_folder['AccountFolder']['name'];

                    //var_dump($creator);
                    $this->sendVideoPublishedNotification(
                            $video_name
                            , $account_folder['AccountFolder']['account_folder_id']
                            , $video['Document']['id']
                            , $creator
                            , $account_folder['AccountFolder']['account_id']
                            , $creator, $creator['User']['email']
                            , $creator['User']['id']
                            , $account_folder['AccountFolder']['folder_type']
                            , '7'
                    );
                }
            } else {

                if (count($huddleUsers) > 0) {
                    for ($i = 0; $i < count($huddleUsers); $i++) {

                        $huddleUser = $huddleUsers[$i];

                        if ($is_video_published_now) {
                            if ($this->check_subscription($huddleUser['User']['id'], '8', $account_id)) {
                                $this->sendVideoPublishedNotification(
                                        $account_folder['AccountFolder']['name']
                                        , $account_folder['AccountFolder']['account_folder_id']
                                        , $video['Document']['id'], $creator, $account_folder['AccountFolder']['account_id'], $huddleUsers, $huddleUser['User']['email'], $huddleUser['User']['id'], $account_folder['AccountFolder']['folder_type'], '8');
                            }
                        }
                    }
                }
            }
        }
    }

    function test() {
        return true;
    }

    function sendVideoPublishedNotificationEx($huddle_name, $huddle_id, $video_id, $creator, $account_id, $huddleUsers, $recipient_email, $huddle_user_id, $huddle_type, $email_type = '') {
        if (!Validation::email($recipient_email))
            return FALSE;

        $users = $creator;
        $account_info = $this->Account->find('first', array('conditions' => array('id' => $account_id)));

        $sibme_base_url = Configure::read('sibme_base_url');
        $auth_token = md5($creator['User']['id']);
        $this->Email->delivery = 'smtp';
        $from = $users['User']['first_name'] . " " . $users['User']['last_name'] . " " . $users['accounts']['company_name'] . " <" . $this->custom->get_site_settings('static_emails')['noreply'] . "> ";
        $to = $recipient_email;

        $subject = "";

        if ($huddle_type == '2') {
            $subject = $account_info['Account']['company_name'] . " Video Library.";
        } else {
            $subject = "Re: " . $huddle_name . " You've got a new video to view.";
        }

        $this->Email->template = 'default';
        $this->Email->sendAs = 'html';
        $h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $htype = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $view = new View($this, true);
        if ($huddle_type == '1') {
            $huddle_name = 'Huddle: ' . $huddle_name;
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id;
        } elseif ($huddle_type == '2') {
            $huddle_name = $huddle_name;
            $url = $sibme_base_url . "VideoLibrary/view/" . $huddle_id;
        } elseif ($huddle_type == '3') {
            $huddle_name = '' . $huddle_name;
            $url = $sibme_base_url . "MyFiles/view/1/" . $huddle_id;
        } else {
            $huddle_type = '1';
            $huddle_name = 'Huddle: ' . $huddle_name;
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id;
        }

        $params = array(
            'huddle_name' => $huddle_name,
            'account_name' => $account_info['Account']['company_name'],
            'huddle_id' => $huddle_id,
            'video_id' => $video_id,
            'creator' => $creator,
            'huddle_type' => $huddle_type,
            'participated_user' => $huddleUsers,
            'authentication_token' => $auth_token,
            'url' => $url,
            'htype' => $htype,
            'huddle_user_id' => $huddle_user_id,
            'email_type' => $email_type
        );
        $html = $view->element('emails/video_publish', $params);

        $this->AuditEmail->create();
        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $this->Email->from,
            'email_to' => $creator['User']['email'],
            'email_subject' => $this->Email->subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $this->AuditEmail->save($auditEmail, $validation = TRUE);
        $use_job_queue = Configure::read('use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $creator['User']['email'], $subject, $html, true);
            return TRUE;
        } else {
            if ($this->Email->send($html)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function executeWebsocket($workspace, $account_id, $huddle_id, $user_id, $doc_id, $users, $title, $desc, $account_folder_doc_id, $timeDiffSecs){
        $view = new View($this, false);
        $doc_object = self::modify_new_document_websocket($doc_id, $users, $title, $desc, $account_folder_doc_id, $timeDiffSecs, $huddle_id);
        $channel = 'huddle-details-' . $huddle_id;
        if ($workspace) {
            $channel = "workspace-" . $account_id . "-" . $user_id;
        }
        $channel_data = array(
            'item_id' => $doc_object->id,
            'is_video_crop' => 1,
            'data' => $doc_object,
            'huddle_id' => $huddle_id,
            'user_id' => $user_id,
            'channel' => $channel,
            'event' => "resource_renamed"
        );
        $view->Custom->BroadcastEvent($channel_data);
    }

}

?>