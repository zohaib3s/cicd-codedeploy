<?php

App::uses('Subscription', 'Braintree');

class ExternalController extends AppController {


    public $name = 'External';
    var $helpers = array('Custom', 'Browser');
    public $uses = array(
        'User', 'AccountFolder', 'Group', 'UserGroup', 'AccountFolderMetaData', 'AccountFolderUser', 'AccountFolderGroup',
        'UserAccount', 'Document', 'Comment', 'AccountFolderDocument', 'AccountFolderDocumentAttachment', 'CommentUser',
        'CommentAttachment', 'Observations', 'Frameworks', 'AccountTag', 'AccountCommentTag','DocumentFilesShare','Account','AccountMetaData'
    );


    public function anyvideos($account_id='') {
//        $this->layout = false;
        $this->layout = 'external';
        $account_id=  base64_decode($account_id);
        $huddle_info=$this->Account->find('first', array('conditions' => array('id' => (int) $account_id)));
        if($huddle_info){
            $this->set('user_current_account',$huddle_info);
            $this->set('account_id', $account_id);
            $embeded_enable = $this->AccountMetaData->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "meta_data_name" => "enable_embeded_link"
            )));
            $this->set('is_embeded_enable', empty($embeded_enable['AccountMetaData']['meta_data_value']) ? '0' : $embeded_enable['AccountMetaData']['meta_data_value']);
            $get_embed_link = $this->AccountMetaData->find("first", array("conditions" => array(
                        "account_id" => $account_id,
                        "meta_data_name" => "external_embed_link"
                )));
                $this->set('external_embeded_link', empty($get_embed_link['AccountMetaData']['meta_data_value']) ? '<iframe src="https://player.vimeo.com/video/119964796?color=5daf46" width="302" height="167" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : $get_embed_link['AccountMetaData']['meta_data_value']);
            $user_id=$this->UserAccount->find('first',array(
                                                            "conditions"=>array("account_id"=>$account_id,"role_id"=>"100"),
                                                            "fields"=>array("user_id")
                                                        ));
            $this->set('user_id', $user_id['UserAccount']['user_id']);
        }
	else{
          //$this->set('account_id', $account_id);
            echo "Not valid";
            exit;
        }
    }



}