<?php

/**
 * Dashboard controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
App::uses('Subscription', 'Braintree');
use Guzzle\Http\Client;


/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class DashboardController extends AppController {

    /**
     * Controller name
     *
     * @var string
     */
    public $name = 'Dashboard';

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('UserActivityLog', 'UserAccount', 'AccountFolder', 'AccountFolderMetaData', 'AccountTag', 'vwAnalyticsAccounts', 'AccountMetaData', 'AccountFolderUser', 'Documents', 'Comment', 'DocumentMetaData', 'AccountCommentTag', 'DocumentStandardRating', 'AccountBraintreeSubscription', 'AccountFrameworkSettingPerformanceLevel', 'AccountFrameworkSetting','AccountStudentPayments');

    /**
     * Displays a view
     *
     * @param mixed What page to display
     * @return void
     */
    public function home() {
        
        return $this->redirect('/home/profile-page', 301);
        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $user_details = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        $account_details = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('Dashboard/home');
        $this->set('language_based_content',$language_based_content);
        $comment_tags_count = $this->get_counts_of_comment_tags($account_id,$user_id);
        //$this->churnzero_attribute_list($comment_tags_count,$user,$account_id);
        $this->set('comment_tags_count',$comment_tags_count);


        if (empty($loggedInUser)) {
            $this->redirect('/users/login');
        }

        $accountFolderUsers = $this->AccountFolderUser->get($user_id);
        $accountFolderGroups = $this->UserGroup->get_user_group($user_id);
        $userPermissions = $this->AccountFolderUser->getUserAccount($account_id, $user_id);
        $accountFoldereIds = '';
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['AccountFolderUser']['account_folder_id'];
            }
            $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_groups']['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        $huddles = $this->AccountFolder->getAllHuddlesDashboard($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);

        $account_sub_users = $this->UserAccount->find('all', array(
            'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'UserAccount.account_id = a.id'
                )
            ),
            'conditions' => array(
                'a.parent_account_id' => $account_id
            ),
            'fields' => array(
                'a.id',
                'a.company_name'
            ),
            'group' => array('a.id')
        ));
        $html = '';
        $usersHtml = '';
        $subHtml = '';

        /* $html = $this->masterAccountAnalytics();
          $usersHtml = $this->accountUsersAnalytics();
          if(is_array($account_sub_users)&& count($account_sub_users)>0){
          $subHtml = $this->subAccountAnalytics();
          }else{
          $subHtml = '';
          } */
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'afu',
                'type' => 'left',
                'conditions' => array('AccountFolder.account_folder_id = afu.account_folder_id')
            )
        );

        $account_coach_huddles = $this->AccountFolder->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'left',
                    'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                ),
                array(
                    'table' => 'account_folder_users',
                    'alias' => 'huddle_users',
                    'type' => 'left',
                    'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                ),
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=huddle_users.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 2,
                'huddle_users.role_id' => 200,
                'huddle_users.user_id' => $user_id
            ),
            'fields' => array(
                'huddle_users.account_folder_id'
            ),
            'group' => array('huddle_users.account_folder_id'),
        ));

        $matric_enable = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'enable_matric')));
        $enable_matric = isset($matric_enable['AccountMetaData']['meta_data_value']) ? $matric_enable['AccountMetaData']['meta_data_value'] : "0";

        $dashboard_settings = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        //print_r($dashboard_settings);die();
        isset($tracker_enable['AccountMetaData']['meta_data_value']) ? $tracker_enable['AccountMetaData']['meta_data_value'] : "0";
//        $recent_activity = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'recent_activity')));
//        $setting_assistance = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'setting_assistance')));
//        $application_insight = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'application_insight')));
//        $mobile_go = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'mobile_go')));

        $tracker_enable = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'enable_tracker')));
        $enable_tracker = isset($tracker_enable['AccountMetaData']['meta_data_value']) ? $tracker_enable['AccountMetaData']['meta_data_value'] : "0";

        $data = $this->UserActivityLog->get($loggedInUser['accounts']['account_id'], $user_id);
        $this->set('getting_started', isset($dashboard_settings['User']['hide_welcome']) ? $dashboard_settings['User']['hide_welcome'] : "1");
        $this->set('recent_activity', isset($dashboard_settings['User']['recent_activity']) ? $dashboard_settings['User']['recent_activity'] : "1");
        $this->set('setting_assistance', isset($dashboard_settings['User']['settings_assistance']) ? $dashboard_settings['User']['settings_assistance'] : "1");
        $this->set('application_insight', isset($dashboard_settings['User']['application_insight']) ? $dashboard_settings['User']['application_insight'] : "1");
        $this->set('mobile_go', isset($dashboard_settings['User']['go_mobile']) ? $dashboard_settings['User']['go_mobile'] : "1");

        $asessor_or_not = $this->AccountFolderUser->query("SELECT
          *
        FROM
          `account_folder_users` AS afu
          JOIN `account_folders` AS af
            ON afu.`account_folder_id` = af.`account_folder_id`
          JOIN `account_folders_meta_data` AS afmd ON afmd.`account_folder_id` = af.`account_folder_id`

          WHERE afmd.`meta_data_value` = '3'AND afmd.`meta_data_name` = 'folder_type' AND afu.`user_id` = " . $user_id . " AND afu.`role_id` = '200' AND afu.`is_coach` = '1' AND af.`account_id` = " . $account_id . " AND af.site_id =" . $this->site_id);
        if (count($asessor_or_not) > 0) {
            $assessor_or_not_check = 1;
        } else {
            $assessor_or_not_check = 0;
        }


        $today_date = date('Y-m-d');

        $subscription_data_table = $this->AccountBraintreeSubscription->find("first", array(
            "conditions" => array(
                "account_id" => $account_id,
            //    "Subscription_start_date <=" => $today_date,
            //    "Subscription_end_date >=" => $today_date
            ),
            "order" => 'id DESC'
        ));

        if (!empty($subscription_data_table) && !empty($subscription_data_table['AccountBraintreeSubscription']['braintree_subscription_id']) && ($account_details['Account']['braintree_subscription_id'] == $subscription_data_table['AccountBraintreeSubscription']['braintree_subscription_id'] )) {
            $subscription = new Subscription();

            $transaction_detail = $subscription->subscriptions_detail($subscription_data_table['AccountBraintreeSubscription']['braintree_subscription_id']);
            //$transaction_detail = (array) $transaction_detail;


            $billingPeriodStartDate = $transaction_detail->billingPeriodStartDate;
            $billingPeriodEndDate = $transaction_detail->billingPeriodEndDate;


            if (!empty($billingPeriodStartDate)) {
                $billingPeriodStartDate = (array) $billingPeriodStartDate;
            }

            if (!empty($billingPeriodEndDate)) {
                $billingPeriodEndDate = (array) $billingPeriodEndDate;
            }

            $start_date = date("Y-m-d", strtotime($billingPeriodStartDate['date']));
            $end_date = date('Y-m-d', strtotime($billingPeriodEndDate['date'] . ' +1 day'));


            $subscription_data_update = array(
                'Subscription_start_date' => "'" . $start_date . "'",
                'Subscription_end_date' => "'" . $end_date . "'",
            );


            $this->AccountBraintreeSubscription->updateAll($subscription_data_update, array('braintree_subscription_id' => $subscription_data_table['AccountBraintreeSubscription']['braintree_subscription_id']));
        }


        $this->set('assessor_or_not_check', $assessor_or_not_check);
        $this->set('enable_matric', $enable_matric);
        $this->set('enable_tracker', $enable_tracker);
        $this->set('huddles_permissions', $this->get_huddle_permissions());
        $this->set('activityLogs', $data);
        $this->set('myHuddles', $huddles);
        $this->set('current_user', $loggedInUser);
        $this->set('account_sub_users', $account_sub_users);
        $this->set('masterAccountAnalytics', $html);
        $this->set('subAccountAnalytics', $subHtml);
        $this->set('accUsersAnalytics', $usersHtml);
        $this->set('verification_code', $user_details['User']['verification_code']);
        $this->set('bool', count($account_coach_huddles));

        $this->render("home");
    }

    public function custom_analytics() {

    }

    public function analytics() {

        $loggedInUser = $this->Session->read('user_current_account');
        $view = new View($this, false);
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $account_id = $user['accounts']['account_id'];

        if (empty($loggedInUser)) {
            $this->redirect('/users/login');
        }

        $account_sub_users = $this->UserAccount->find('all', array(
            'joins' => array(
                array(
                    'table' => 'accounts as a',
                    'type' => 'inner',
                    'conditions' => 'UserAccount.account_id = a.id'
                )
            ),
            'conditions' => array(
                'a.parent_account_id' => $account_id
            ),
            'fields' => array(
                'a.id',
                'a.company_name'
            ),
            'group' => array('a.id')
        ));

        $html = '';
        $usersHtml = '';
        $subHtml = '';
        $graphHtmlForEvaluationHuddle = '';
        $graphHtml = '';
        $view = new View($this, false);
        $masterHtml = $this->masterAccountAnalytics();
        $html = $view->element('dashboard/masterAccountAnalytics', $masterHtml);
        $graphHtml = $this->masterAccountAnalyticsGraph(2);
        $graphHtmlForEvaluationHuddle = $this->masterAccountAnalyticsGraph(3);
        $usersHtml = $this->accountUsersAnalytics();
        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));

        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '8';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];

        $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                "tag_type" => '2', "account_id" => $account_id
        )));
        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
            $frm_wrk = array('framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
        }
        $frameworks_added = $this->AccountTag->find("all", array("conditions" => array(
                "tag_type" => array(0, 2), "account_id" => $account_id
        )));
        if (!empty($frameworks_added) && count($frameworks_added) > 0) {
            $this->set('frameworks_added', $frameworks_added);
        }

        $standards = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 0,
                $frm_wrk
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code", "tag_html")
        ));

        $standards_array = array();
        if ($standards) {
            foreach ($standards as $st) {
                if (!empty($st['AccountTag']['tag_html'])) {
                    if (empty($st['AccountTag']['tads_code']))
                        $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                    else
                        $ac_tag_title = $st['AccountTag']['tads_code'];
                    $standard_array[] = array(
                        "value" => $st['AccountTag']['account_tag_id'],
                        "label" => $ac_tag_title
                    );
                }
            }
        }



        $this->set('standards', json_encode($standard_array));
        $this->set('account_sub_users', $account_sub_users);
        $this->set('masterAccountAnalytics', $html);
        $this->set('masterAccAnalyticsGraph', $graphHtml);
        $this->set('masterAccAnalyticsGraphForEvaluation', $graphHtmlForEvaluationHuddle);
        $this->set('accUsersAnalytics', $usersHtml);
        $this->set('accAnalyticsDuration', $acct_duration_start);
        $this->set('graph_for_rattings', $ratting_graph);


        $this->render("analytics");
    }

    function getFramework() {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];

        $standards = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 0,
                "framework_id" => $this->data['framework_id']
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code")
        ));

        $standards_array = array();
        if ($standards) {
            foreach ($standards as $st) {
                if (empty($st['AccountTag']['tads_code']))
                    $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                else
                    $ac_tag_title = $st['AccountTag']['tads_code'];
                $standard_array[] = array(
                    "value" => $st['AccountTag']['account_tag_id'],
                    "label" => $ac_tag_title
                );
            }
        }

        echo json_encode($standard_array);
        exit;
    }

    function masterAccountAnalytics($bool = 0) {

        if ($bool == 2 || $bool == 1) {
            $startDate = date("Y-m-d", strtotime($this->request->data['startDate']));
            $endDate = date("Y-m-d", strtotime($this->request->data['endDate']));

            $date_from = date("Y-m-d H:i:s", strtotime($this->request->data['startDate'] . " 00:00:00"));
            $date_to = date("Y-m-d H:i:s", strtotime($this->request->data['endDate'] . " 23:59:59"));
        }

        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        if ($bool == 1) {
            $account_id = $this->request->data['subAccount'];
        } elseif ($bool == 2 || $bool == 0) {
            $account_id = $user['accounts']['account_id'];
        }
        $user_id = $user['User']['id'];
        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();

        $account_ov_analytics = $this->Account->find('all', array(
            'conditions' => array(
                'Account.is_active' => 1,
                'id' => $account_id
            ),
            'fields' => array('id')
        ));

        $account_overview_analytics = $this->Account->find('all', array(
            'conditions' => array(
                'Account.is_active' => 1,
                'parent_account_id' => $account_id
            ),
            'fields' => array('id')
        ));
        $all_account_ids = array();
        if ($bool == 1) {
            $all_account_ids = array($account_id);
        } else {
            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $all_account_ids[] = $all_ac['Account']['id'];
            }
        }
        //$all_acc = implode(',',$all_account_ids);

        $account_user_analytics_results = $this->UserAccount->query("
                select count(*) as total_users from (
                    select distinct u.id as total_users
          from users u join users_accounts ua on u.id = ua.user_id
          join accounts a on a.id = ua.account_id
          WHERE u.is_active=1 AND u.id NOT IN (2423,2422,2427) AND TYPE='Active' and a.id=$account_id and u.site_id =" . $this->site_id . "
          ) as a

          UNION ALL

          select count(*) as total_users from (
           select  distinct u.id as total_users

          from users u join users_accounts ua on u.id = ua.user_id
          join accounts a on a.id = ua.account_id
          WHERE u.is_active=1 AND u.site_id = " . $this->site_id . " AND u.id NOT IN (2423,2422,2427) AND TYPE='Active' and a.parent_account_id=$account_id) as a");

        if (count($account_user_analytics_results) > 0) {

            $account_user_analytics = 0;

            foreach ($account_user_analytics_results as $account_user_analytics_result) {

                $account_user_analytics += $account_user_analytics_result[0]['total_users'];
            }
        }


        if ($bool == 1 || $bool == 2) {

            $account_user_analytics_results = $this->UserAccount->query("
                    select count(*) as total_users from (
                        select distinct u.id as total_users
              from users u join users_accounts ua on u.id = ua.user_id
              join accounts a on a.id = ua.account_id
              WHERE u.is_active=1 AND u.id NOT IN (2423,2422,2427) AND TYPE='Active' and a.id=$account_id and a.site_id =" . $this->site_id . "
              ) as a

              UNION ALL

              select count(*) as total_users from (
               select  distinct u.id as total_users

              from users u join users_accounts ua on u.id = ua.user_id
              join accounts a on a.id = ua.account_id
              WHERE u.is_active=1 AND u.id NOT IN (2423,2422,2427) AND TYPE='Active' and u.site_id =" . $this->site_id . " and a.parent_account_id=$account_id) as a");

            if (count($account_user_analytics_results) > 0) {

                $account_user_analytics = 0;
                foreach ($account_user_analytics_results as $account_user_analytics_result) {

                    $account_user_analytics += $account_user_analytics_result[0]['total_users'];
                }
            }
        }





        $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs`  WHERE `type` = 1 AND account_id IN (" . $account_id . ") AND site_id =" . $this->site_id);

        $huddle_created_count = 0;

        foreach ($total_huddles_results as $result) {

            $huddle_created_count += $result[0]['huddle_created_count'];
        }

        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );

        foreach ($child_accounts as $child_account) {

            $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs`  WHERE `type` = 1 AND site_id =" . $this->site_id . " AND account_id IN (" . $child_account['Account']['id'] . ")");

            foreach ($total_huddles_results as $result) {

                $huddle_created_count += $result[0]['huddle_created_count'];
            }
        }

        $account_huddle_analytics = $huddle_created_count;

        if ($bool == 1 || $bool == 2) {

            $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs`  WHERE `type` = 1 AND account_id IN (" . $account_id . ")  and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' AND site_id =" . $this->site_id);

            $huddle_created_count = 0;

            foreach ($total_huddles_results as $result) {

                $huddle_created_count += $result[0]['huddle_created_count'];
            }

            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                )
                    )
            );

            foreach ($child_accounts as $child_account) {

                $total_huddles_results = $this->UserAccount->query("SELECT COUNT(*) AS `huddle_created_count` FROM `user_activity_logs`  WHERE `type` = 1 AND account_id IN (" . $child_account['Account']['id'] . ")  and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' AND site_id=" . $this->site_id);

                foreach ($total_huddles_results as $result) {

                    $huddle_created_count += $result[0]['huddle_created_count'];
                }
            }

            $account_huddle_analytics = $huddle_created_count;
        }


        $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
    users u join `users_accounts` ua on u.id = ua.user_id
    join accounts a on a.id = ua.account_id
    left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1,3) AND ua.account_id IN (" . $account_id . ") GROUP BY ua.user_id) d on d.user_id = u.id where ua.account_id=" . $account_id . " and ua.site_id=" . $this->site_id);

        $huddle_videos_count = 0;

        foreach ($total_videos_results as $result) {

            $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
        }
        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );

        foreach ($child_accounts as $child_account) {


            $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle  from
            users u join `users_accounts` ua on u.id = ua.user_id
            join accounts a on a.id = ua.account_id
            left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
            account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1,3) AND ua.account_id IN (" . $child_account['Account']['id'] . ") GROUP BY ua.user_id) d on d.user_id = u.id where ua.account_id=" . $child_account['Account']['id'] . " and ua.site_id =" . $this->site_id);

            foreach ($total_videos_results as $result) {

                $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
            }
        }

        $account_video_analytics = $huddle_videos_count;

        if ($bool == 1 || $bool == 2) {


            $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
        account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1,3) AND ua.account_id IN (" . $account_id . ")  and date_added >= '" . $date_from . "' and date_added <= '" . $date_to . "' GROUP BY ua.user_id) d on d.user_id = u.id where ua.account_id=" . $account_id . " and ua.site_id=" . $this->site_id);

            $huddle_videos_count = 0;

            foreach ($total_videos_results as $result) {

                $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
            }




            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                )
                    )
            );

            foreach ($child_accounts as $child_account) {


                $total_videos_results = $this->UserAccount->query("select sum(IFNULL(d.Videos_Uploaded_Huddle, 0)) Videos_Uploaded_Huddle  from
                users u join `users_accounts` ua on u.id = ua.user_id
                join accounts a on a.id = ua.account_id
                left join (SELECT user_id, COUNT(*) AS Videos_Uploaded_Huddle FROM `user_activity_logs` ua LEFT JOIN
                account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1,3) AND ua.account_id IN (" . $child_account['Account']['id'] . ")  and date_added >= '" . $date_from . "' and date_added <= '" . $date_to . "' GROUP BY ua.user_id) d on d.user_id = u.id where ua.account_id=" . $child_account['Account']['id'] . " and ua.site_id=" . $this->site_id);

                foreach ($total_videos_results as $result) {

                    $huddle_videos_count += $result[0]['Videos_Uploaded_Huddle'];
                }
            }

            $account_video_analytics = $huddle_videos_count;
        }



        $total_videos_viewed_results = $this->UserAccount->query("select distinct count(ual.id) as 'Resources_Viewed' from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id = " . $account_id . "  and ual.type=11
        where ua.account_id = " . $account_id . " and ua.site_id=" . $this->site_id);

        $huddle_videos_count = 0;

        foreach ($total_videos_viewed_results as $result) {

            $huddle_videos_count += $result[0]['Resources_Viewed'];
        }




        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );

        foreach ($child_accounts as $child_account) {



            $total_videos_viewed_results = $this->UserAccount->query("select distinct count(ual.id) as 'Resources_Viewed' from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id = " . $child_account['Account']['id'] . "  and ual.type=11
        where ua.account_id = " . $child_account['Account']['id'] . " and ua.site_id=" . $this->site_id);

            foreach ($total_videos_viewed_results as $result) {

                $huddle_videos_count += $result[0]['Resources_Viewed'];
            }
        }

        $account_video_viewed_analytics = $huddle_videos_count;

        if ($bool == 1 || $bool == 2) {

            $total_videos_viewed_results = $this->UserAccount->query("select distinct count(ual.id) as 'Resources_Viewed' from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id = " . $account_id . "  and ual.type=11 and ual.date_added  >= '" . $date_from . "' and ual.date_added <= '" . $date_to . "'
        where ua.account_id = " . $account_id . " and ua.site_id =" . $this->site_id);

            $huddle_videos_count = 0;

            foreach ($total_videos_viewed_results as $result) {

                $huddle_videos_count += $result[0]['Resources_Viewed'];
            }




            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                )
                    )
            );

            foreach ($child_accounts as $child_account) {



                $total_videos_viewed_results = $this->UserAccount->query("select distinct count(ual.id) as 'Resources_Viewed' from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join user_activity_logs ual on ual.account_id = a.id and ual.user_id = u.id and ual.account_id = " . $child_account['Account']['id'] . "  and ual.type=11 and ual.date_added  >= '" . $date_from . "' and ual.date_added <= '" . $date_to . "'
        where ua.account_id = " . $child_account['Account']['id'] . " and ua.site_id =" . $this->site_id);

                foreach ($total_videos_viewed_results as $result) {

                    $huddle_videos_count += $result[0]['Resources_Viewed'];
                }
            }

            $account_video_viewed_analytics = $huddle_videos_count;
        }


        $total_comments_results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join (

        SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE TYPE=5 AND user_activity_logs.account_id IN (" . $account_id . ") GROUP BY user_activity_logs.user_id

        ) d on d.user_id = u.id where ua.account_id=" . $account_id . " and ua.site_id=" . $this->site_id);

        $huddle_videos_count = 0;

        foreach ($total_comments_results as $result) {

            $huddle_videos_count += $result[0]['Total_Video_Comments'];
        }




        $child_accounts = $this->Account->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );

        foreach ($child_accounts as $child_account) {


            $total_comments_results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join (

        SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE TYPE=5 AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ")  GROUP BY user_activity_logs.user_id

        ) d on d.user_id = u.id where ua.account_id=" . $child_account['Account']['id'] . " and ua.site_id =" . $this->site_id);

            foreach ($total_comments_results as $result) {

                $huddle_videos_count += $result[0]['Total_Video_Comments'];
            }
        }

        $account_comments_analytics = $huddle_videos_count;

        if ($bool == 1 || $bool == 2) {


            $total_comments_results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join (

        SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE TYPE=5 AND user_activity_logs.account_id IN (" . $account_id . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' and user_activity_logs.site_id = " . $this->site_id . "  GROUP BY user_activity_logs.user_id

        ) d on d.user_id = u.id where ua.account_id=" . $account_id);

            $huddle_videos_count = 0;

            foreach ($total_comments_results as $result) {

                $huddle_videos_count += $result[0]['Total_Video_Comments'];
            }




            $child_accounts = $this->Account->find('all', array(
                'conditions' => array(
                    'parent_account_id' => $account_id
                )
                    )
            );

            foreach ($child_accounts as $child_account) {


                $total_comments_results = $this->UserAccount->query("select sum(ifnull(d.comments_initiated_count,0)) as Total_Video_Comments from
        users u join `users_accounts` ua on u.id = ua.user_id
        join accounts a on a.id = ua.account_id
        left join (

        SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE TYPE=5 AND user_activity_logs.account_id IN (" . $child_account['Account']['id'] . ") and date_added >='" . $date_from . "' AND date_added <= '" . $date_to . "' AND user_activity_logs.site_id=" . $this->site_id . "  GROUP BY user_activity_logs.user_id

        ) d on d.user_id = u.id where ua.account_id=" . $child_account['Account']['id']);

                foreach ($total_comments_results as $result) {

                    $huddle_videos_count += $result[0]['Total_Video_Comments'];
                }
            }

            $account_comments_analytics = $huddle_videos_count;
        }

        $view = new View($this, false);
        $analytics_result = array(
            'total_account_users' => $account_user_analytics,
            'total_account_huddles' => $account_huddle_analytics,
            'total_account_videos' => $account_video_analytics,
            'total_viewed_videos' => $account_video_viewed_analytics,
            'total_comments_added' => $account_comments_analytics,
        );


        if ($bool == 1 || $bool == 2) {
            $view = new View($this, false);
            $html = $view->element('dashboard/masterAccountAnalytics', $analytics_result);
            echo $html;

            die;
        } else {
            return $analytics_result;
        }
    }

    function subAccountAnalytics() {
        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();

        if (!empty($this->data['subAccount'])) {
            $account_id = $this->data['subAccount'];
        }

        if (!empty($this->data['startDate'])) {
            $stDate = date_create($this->data['startDate']);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
            $mAUCnd[] = array('UserAccount.created_date >= ' => $st_date);
            $mAHCnd[] = array('AccountFolder.created_date >= ' => $st_date);
            $mAVCnd[] = array('d.created_date >= ' => $st_date);
            $mAVWCnd[] = array('UserActivityLog.date_added >= ' => $st_date);
            $mATCnd[] = array('act.created_date >= ' => $st_date);
        }
        if (!empty($this->data['endDate'])) {
            $endDate = date_create($this->data['endDate']);
            $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
            $mAUCnd[] = array('UserAccount.created_date <= ' => $end_date);
            $mAHCnd[] = array('AccountFolder.created_date <= ' => $end_date);
            $mAVCnd[] = array('d.created_date <= ' => $end_date);
            $mAVWCnd[] = array('UserActivityLog.date_added <= ' => $end_date);
            $mATCnd[] = array('act.created_date <= ' => $end_date);
        }

        $account_user_analytics = $this->UserAccount->find('count', array(
            'joins' => array(
                array(
                    'table' => 'users as u',
                    'type' => 'inner',
                    'conditions' => 'UserAccount.user_id = u.id'
                )
            ),
            'conditions' => array(
                'u.is_active' => 1,
                'u.type' => 'Active',
                'UserAccount.account_id' => $account_id
            )
        ));

        $account_huddle_analytics = $this->AccountFolder->find('count', array(
            'conditions' => array(
                'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                'AccountFolder.folder_type' => 1,
                'AccountFolder.account_id' => $account_id,
                $mAHCnd
            )
        ));

        $account_video_analytics = $this->AccountFolder->find('count', array(
            'joins' => array(
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'inner',
                    'conditions' => 'AccountFolder.account_folder_id = afd.account_folder_id'
                ),
                array(
                    'table' => 'documents as d',
                    'type' => 'inner',
                    'conditions' => 'afd.document_id = d.id'
                )
            ),
            'conditions' => array(
                'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                'AccountFolder.folder_type' => 1,
                'd.doc_type' => 1,
                'AccountFolder.account_id' => $account_id,
                $mAVCnd
            )
        ));
        $account_video_viewed_analytics = $this->UserActivityLog->find('count', array(
            'conditions' => array(
                'UserActivityLog.type' => 11,
                'UserActivityLog.account_id' => $account_id,
                $mAVWCnd
            )
        ));
        $account_comments_analytics = $this->UserActivityLog->find('count', array(
            'conditions' => array(
                'UserActivityLog.type' => 5,
                'UserActivityLog.account_id' => $account_id,
                $mAVWCnd
            )
        ));

        $this->AccountTag->virtualFields['total_tags'] = 0;
        $account_colab_tags_analytics = $this->AccountTag->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_comment_tags as act',
                    'type' => 'inner',
                    'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                ),
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'inner',
                    'conditions' => 'act.ref_id = afd.document_id'
                ),
                array(
                    'table' => 'account_folders as af',
                    'type' => 'inner',
                    'conditions' => 'afd.account_folder_id = af.account_folder_id'
                )
            ),
            'conditions' => array(
                'AccountTag.tag_type' => 0,
                'AccountTag.account_id' => $account_id,
                'af.folder_type' => 1,
                $mATCnd
            ),
            'fields' => array(
                'AccountTag.tag_title',
                'act.account_comment_tag_id',
                'AccountTag.account_tag_id',
                'afd.account_folder_id'
            ),
            'group' => 'act.account_comment_tag_id'
        ));
        //echo '<pre>';
        //print_r($account_colab_tags_analytics);
        $new_array = array();
        for ($i = 0; $i < count($account_colab_tags_analytics); $i++) {
            $account_hudd = 0;
            $account_hudd = $this->AccountFolderMetaData->find('count', array(
                'conditions' => array(
                    'AccountFolderMetaData.meta_data_name' => 'folder_type',
                    'AccountFolderMetaData.account_folder_id' => $account_colab_tags_analytics[$i]['afd']['account_folder_id']
            )));
            if ($account_hudd > 0) {
                $new_array[] = $i;
            }
        }

        $account_colab_tags_analytic = array_diff_key($account_colab_tags_analytics, array_flip($new_array));
        $account_colab_tags_analytic = array_values($account_colab_tags_analytic);
        $acc_colb = array();
        $collab = array();
        for ($i = 0; $i < count($account_colab_tags_analytic); $i++) {
            if (in_array($account_colab_tags_analytic[$i]['AccountTag']['account_tag_id'], $acc_colb)) {

                foreach ($collab as $k => $v) {
                    $key = $account_colab_tags_analytic[$i]['AccountTag']['tag_title'] . '||' . $account_colab_tags_analytic[$i]['AccountTag']['account_tag_id'];
                    if ($key == $k)
                        $collab[$k] = $v + 1;
                }
            }else {
                $acc_colb[] = $account_colab_tags_analytic[$i]['AccountTag']['account_tag_id'];
                $key = $account_colab_tags_analytic[$i]['AccountTag']['tag_title'] . '||' . $account_colab_tags_analytic[$i]['AccountTag']['account_tag_id'];
                $collab[$key] = 1;
            }
        }

        $collab_tags = array();
        $i = 0;
        foreach ($collab as $k => $v) {
            $key = explode('||', $k);
            $collab_tags[$i]['tag_title'] = $key[0];
            $collab_tags[$i]['total_tags'] = $v;
            $i++;
        }

        $account_coach_tags_analytics = $this->AccountTag->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_comment_tags as act',
                    'type' => 'inner',
                    'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                ),
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'inner',
                    'conditions' => 'act.ref_id = afd.document_id'
                ),
                array(
                    'table' => 'account_folders as af',
                    'type' => 'inner',
                    'conditions' => 'afd.account_folder_id = af.account_folder_id'
                ),
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'inner',
                    'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                )
            ),
            'conditions' => array(
                'AccountTag.tag_type' => 0,
                'AccountTag.account_id' => $account_id,
                'af.folder_type' => 1,
                'afmd.meta_data_name' => 'folder_type',
                $mATCnd
            ),
            'fields' => array(
                'AccountTag.tag_title',
                'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
            ),
            'group' => array('AccountTag.account_tag_id')
        ));

        $account_coach_tags_analytics = Set::extract('/AccountTag/.', $account_coach_tags_analytics);

        $view = new View($this, false);
        $analytics_result = array(
            'total_account_users' => $account_user_analytics,
            'total_account_huddles' => $account_huddle_analytics,
            'total_account_videos' => $account_video_analytics,
            'total_viewed_videos' => $account_video_viewed_analytics,
            'total_comments_added' => $account_comments_analytics,
            'total_colab_tags' => json_encode($collab_tags),
            'total_coaching_tags' => json_encode($account_coach_tags_analytics)
        );
        $html = $view->element('dashboard/subAccountAnalytics', $analytics_result);

        if (!empty($this->data['startDate']) || !empty($this->data['endDate']))
            echo $html;
        else
            return $html;

        exit;
    }

    function masterAccountAnalyticsGraph($folder_type = '2') {
        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];

        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];
        $user_id = $user['User']['id'];
        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();

        $account_ids = array();
        if (!empty($this->data['subAccount'])) {
            $account_ids[] = $this->data['subAccount'];
        } else {
            $account_ov_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'id' => $account_id
                ),
                'fields' => array('id')
            ));

            $account_overview_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'parent_account_id' => $account_id
                ),
                'fields' => array('id')
            ));
            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $account_ids[] = $all_ac['Account']['id'];
            }
        }

        $acc_ids = implode(',', $account_ids);
        $search_by_standard = '';
        if (!empty($this->data['duration'])) {
            $durationGrph = $this->data['duration'];
            $search_by_standard = $this->data['search_by_standards'];
        } else {
            $durationGrph = 3;
        }

        if (!empty($this->data['framework_id'])) {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id, 'account_tag_id' => $this->data['framework_id']
            )));
        } else {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id
            )));
        }

        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
        }
        if (!empty($this->data['framework_id'])) {
            $frm_wrk = array('framework_id' => $this->data['framework_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $this->data['framework_id']);
        } else {
            $frm_wrk = array('framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
        }

        if (!empty($this->data['startDate'])) {
            if ($durationGrph == 1)
                $d = $this->data['startDate'] . '-' . $acct_duration_start . '-01';
            if ($durationGrph == 2) {
                $q = explode('-', $this->data['startDate']);
                $qu = 0;
                $st_quarter = $q[1];
                $st_yr = $q[0];
                for ($i = 0; $i <= $q[1]; $i++) {
                    if ($i == $q[1])
                        $qu = ($i * 3 - 3);
                }
                $dd = $q[0] . '-' . $acct_duration_start . '-01';
                $d = date('Y-m-d', strtotime($dd . "+$qu month"));
            }
            if ($durationGrph == 3)
                $d = $this->data['startDate'] . '-' . '01';
            $stDate = date_create($d);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }

        $mAUCnd[] = array('UserAccount.created_date >= ' => $st_date);
        $mAHCnd[] = array('AccountFolder.created_date >= ' => $st_date);
        $mAVCnd[] = array('d.created_date >= ' => $st_date);
        $mAVWCnd[] = array('UserActivityLog.date_added >= ' => $st_date);
        $mATCnd[] = array('act.created_date >= ' => $st_date);
        if (!empty($search_by_standard)) {
            $mATCnd[] = array('act.account_tag_id IN( ' . $search_by_standard . ')');
        }

        if (!empty($this->data['endDate'])) {
            if ($durationGrph == 1)
                $d = $this->data['endDate'] . '-' . $acct_duration_start . '-01';
            if ($this->data['startDate'] == $this->data['endDate'])
                $endDate = date_create($d);
            else
                $endDate = date_create($d);
            if ($durationGrph == 2) {
                $q = explode('-', $this->data['endDate']);
                $qu = 0;
                for ($i = 0; $i <= $q[1]; $i++) {
                    if ($i == $q[1])
                        $qu = ($i * 3 - 3);
                }
                $dd = $q[0] . '-' . $acct_duration_start . '-01';
                $d = date('Y-m-d', strtotime($dd . "+$qu month"));
                $endDate = date_create($d);
            }
            if ($durationGrph == 3) {
                $d = $this->data['endDate'] . '-' . '01';
                $endDate = date_create($d);
            }
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        } else {
            $endDate = date_create(date('Y-m-d'));
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        }

        $mAUCnd[] = array('UserAccount.created_date <= ' => $end_date);
        $mAHCnd[] = array('AccountFolder.created_date <= ' => $end_date);
        $mAVCnd[] = array('d.created_date <= ' => $end_date);
        $mAVWCnd[] = array('UserActivityLog.date_added <= ' => $end_date);
        $mATCnd[] = array('act.created_date <= ' => $end_date);

        $start = (new DateTime($st_date))->modify('first day of this month');
        $end = (new DateTime($end_date))->modify('last day of this month');

        if ($durationGrph == 1)
            $interval = DateInterval::createFromDateString('1 year');
        if ($durationGrph == 2)
            $interval = DateInterval::createFromDateString('3 month');
        if ($durationGrph == 3)
            $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        $this->AccountTag->virtualFields['tags_date'] = 0;
        $this->AccountTag->virtualFields['total_tags'] = 0;

        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'act.ref_id = afd.document_id'
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = af.account_folder_id'
            ),
            array(
                'table' => 'account_folders_meta_data as afmd',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'AccountTag.account_id' => $account_ids,
            'af.folder_type' => 1,
            'afmd.meta_data_name' => 'folder_type',
            'afmd.meta_data_value' => $folder_type,
            $mATCnd,
            $act_frm_wrk
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        if (empty($search_by_standard)) {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
                'limit' => 5
            );
        } else {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc')
            );
        }
        $account_tags_analytics = $this->AccountTag->find('all', $get_value);

        $account_tags_analytics = Set::extract('/AccountTag/.', $account_tags_analytics);
        $acc_tags = array();
        $acc_tag_title = array();
        foreach ($account_tags_analytics as $actag) {
            $acc_tags[] = $actag['account_tag_id'];
            if (empty($actag['tads_code']))
                $acc_tag_title[] = $actag['tag_title'];
            else
                $acc_tag_title[] = $actag['tads_code'];
        }

        $graph_data_all = array();
        $graph_data = array();
        if ($durationGrph == 2)
            $m = $st_quarter;
        foreach ($period as $dt) {
            $g_data = $dt->format("Y-m-d");
            $duration = explode('-', $dt->format("Y-m-d"));
            $dCnd = array();
            $docCnd = array();
            if ($durationGrph == 1) {
                $graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('YEAR(act.created_date)' => $duration[0]);
                $docCnd[] = array('YEAR(d.created_date)' => $duration[0]);
            }
            if ($durationGrph == 2) {
                $v = $m % 4;
                if ($v == 0) {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                    $st_yr = $st_yr + 1;
                    $m = 0;
                } else {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                }
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $m++;
            }
            if ($durationGrph == 3) {
                $graph_data['date'] = $dt->format("M-y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
            }
            $account_coach_tags_analytics = $this->AccountTag->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'account_comment_tags as act',
                        'type' => 'inner',
                        'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'act.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders as af',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = af.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountTag.tag_type' => 0,
                    'AccountTag.account_id' => $account_ids,
                    'AccountTag.account_tag_id' => $acc_tags,
                    'af.folder_type' => 1,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    $dCnd,
                    $act_frm_wrk
                ),
                'fields' => array(
                    'AccountTag.tag_title',
                    'AccountTag.account_tag_id',
                    'act.created_date AS AccountTag__tags_date',
                    'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                ),
                'group' => array('AccountTag.account_tag_id')
            ));

            $account_coach_tags_analytics = Set::extract('/AccountTag/.', $account_coach_tags_analytics);
            if (is_array($account_coach_tags_analytics) && count($account_coach_tags_analytics) > 0) {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $flag = false;
                    foreach ($account_coach_tags_analytics as $acta) {
                        if ($acc_tags[$i] == $acta['account_tag_id']) {
                            $graph_data['tag' . $i] = $acta['total_tags'];
                            $graph_data['tag_title_' . $i] = $acta['total_tags'];
                            $flag = true;
                        }
                    }
                    if ($flag == false)
                        $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }else {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }

            $account_video_analytics = $this->AccountFolder->find('count', array(
                'joins' => array(
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'AccountFolder.account_folder_id = afd.account_folder_id'
                    ),
                    array(
                        'table' => 'documents as d',
                        'type' => 'inner',
                        'conditions' => 'afd.document_id = d.id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                    'AccountFolder.folder_type' => 1,
                    'd.doc_type' => 1,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'AccountFolder.account_id' => $account_ids,
                    $docCnd
                )
            ));
            $graph_data['total_sessions'] = $account_video_analytics;
            $graph_data_all[] = $graph_data;
            unset($graph_data);
        }

        $all_ratting = $this->analytics_for_account();

        $view = new View($this, false);
        $analytics_result = array(
            'total_coaching_tags' => json_encode($graph_data_all),
            'assessment_graph' => $all_ratting['assessment_graph'],
            'assessment_array' => $all_ratting['assessment_array'],
            'video_tags' => $all_ratting['video_tags']
        );
        if ($folder_type == 2) {
            $html = $view->element('dashboard/AnalyticsGraph', $analytics_result);
        } else {
            $html = $view->element('dashboard/AnalyticsGraph_2', $analytics_result);
        }

        if (!empty($this->data['startDate']) || !empty($this->data['endDate']))
            echo $html;
        else
            return $html;

        exit;
    }

    function masterAccountAnalyticsGraph_export($folder_type = '2') {
        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];

        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];
        $user_id = $user['User']['id'];
        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();

        $account_ids = array();
        if (!empty($this->data['subAccount'])) {
            $account_ids[] = $this->data['subAccount'];
        } else {
            $account_ov_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'id' => $account_id
                ),
                'fields' => array('id')
            ));

            $account_overview_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'parent_account_id' => $account_id
                ),
                'fields' => array('id')
            ));
            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $account_ids[] = $all_ac['Account']['id'];
            }
        }

        $acc_ids = implode(',', $account_ids);
        $search_by_standard = '';
        if (!empty($this->data['duration'])) {
            $durationGrph = $this->data['duration'];
            $search_by_standard = $this->data['search_by_standards'];
        } else {
            $durationGrph = 3;
        }

        if (!empty($this->data['framework_id'])) {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id, 'account_tag_id' => $this->data['framework_id']
            )));
        } else {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id
            )));
        }

        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
        }
        if (!empty($this->data['framework_id'])) {
            $frm_wrk = array('framework_id' => $this->data['framework_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $this->data['framework_id']);
        } else {
            $frm_wrk = array('framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
        }

        if (!empty($this->data['startDate'])) {
            if ($durationGrph == 1)
                $d = $this->data['startDate'] . '-' . $acct_duration_start . '-01';
            if ($durationGrph == 2) {
                $q = explode('-', $this->data['startDate']);
                $qu = 0;
                $st_quarter = $q[1];
                $st_yr = $q[0];
                for ($i = 0; $i <= $q[1]; $i++) {
                    if ($i == $q[1])
                        $qu = ($i * 3 - 3);
                }
                $dd = $q[0] . '-' . $acct_duration_start . '-01';
                $d = date('Y-m-d', strtotime($dd . "+$qu month"));
            }
            if ($durationGrph == 3)
                $d = $this->data['startDate'] . '-' . '01';
            $stDate = date_create($d);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }

        $mAUCnd[] = array('UserAccount.created_date >= ' => $st_date);
        $mAHCnd[] = array('AccountFolder.created_date >= ' => $st_date);
        $mAVCnd[] = array('d.created_date >= ' => $st_date);
        $mAVWCnd[] = array('UserActivityLog.date_added >= ' => $st_date);
        $mATCnd[] = array('act.created_date >= ' => $st_date);
        if (!empty($search_by_standard)) {
            $mATCnd[] = array('act.account_tag_id IN( ' . $search_by_standard . ')');
        }

        if (!empty($this->data['endDate'])) {
            if ($durationGrph == 1)
                $d = $this->data['endDate'] . '-' . $acct_duration_start . '-01';
            if ($this->data['startDate'] == $this->data['endDate'])
                $endDate = date_create($d);
            else
                $endDate = date_create($d);
            if ($durationGrph == 2) {
                $q = explode('-', $this->data['endDate']);
                $qu = 0;
                for ($i = 0; $i <= $q[1]; $i++) {
                    if ($i == $q[1])
                        $qu = ($i * 3 - 3);
                }
                $dd = $q[0] . '-' . $acct_duration_start . '-01';
                $d = date('Y-m-d', strtotime($dd . "+$qu month"));
                $endDate = date_create($d);
            }
            if ($durationGrph == 3) {
                $d = $this->data['endDate'] . '-' . '01';
                $endDate = date_create($d);
            }
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        } else {
            $endDate = date_create(date('Y-m-d'));
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        }

        $mAUCnd[] = array('UserAccount.created_date <= ' => $end_date);
        $mAHCnd[] = array('AccountFolder.created_date <= ' => $end_date);
        $mAVCnd[] = array('d.created_date <= ' => $end_date);
        $mAVWCnd[] = array('UserActivityLog.date_added <= ' => $end_date);
        $mATCnd[] = array('act.created_date <= ' => $end_date);

        $start = (new DateTime($st_date))->modify('first day of this month');
        $end = (new DateTime($end_date))->modify('last day of this month');

        if ($durationGrph == 1)
            $interval = DateInterval::createFromDateString('1 year');
        if ($durationGrph == 2)
            $interval = DateInterval::createFromDateString('3 month');
        if ($durationGrph == 3)
            $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);
        $this->AccountTag->virtualFields['tags_date'] = 0;
        $this->AccountTag->virtualFields['total_tags'] = 0;

        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'act.ref_id = afd.document_id'
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = af.account_folder_id'
            ),
            array(
                'table' => 'account_folders_meta_data as afmd',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'AccountTag.account_id' => $account_ids,
            'af.folder_type' => 1,
            'afmd.meta_data_name' => 'folder_type',
            'afmd.meta_data_value' => $folder_type,
            $mATCnd,
            $act_frm_wrk
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        if (empty($search_by_standard)) {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
                'limit' => 5
            );
        } else {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc')
            );
        }
        $account_tags_analytics = $this->AccountTag->find('all', $get_value);

        $account_tags_analytics = Set::extract('/AccountTag/.', $account_tags_analytics);
        $acc_tags = array();
        $acc_tag_title = array();
        foreach ($account_tags_analytics as $actag) {
            $acc_tags[] = $actag['account_tag_id'];
            if (empty($actag['tads_code']))
                $acc_tag_title[] = $actag['tag_title'];
            else
                $acc_tag_title[] = $actag['tads_code'];
        }

        $graph_data_all = array();
        $graph_data = array();
        if ($durationGrph == 2)
            $m = $st_quarter;
        foreach ($period as $dt) {
            $g_data = $dt->format("Y-m-d");
            $duration = explode('-', $dt->format("Y-m-d"));
            $dCnd = array();
            $docCnd = array();
            if ($durationGrph == 1) {
                $graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('YEAR(act.created_date)' => $duration[0]);
                $docCnd[] = array('YEAR(d.created_date)' => $duration[0]);
            }
            if ($durationGrph == 2) {
                $v = $m % 4;
                if ($v == 0) {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                    $st_yr = $st_yr + 1;
                    $m = 0;
                } else {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                }
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $m++;
            }
            if ($durationGrph == 3) {
                $graph_data['date'] = $dt->format("M-y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
            }
            $account_coach_tags_analytics = $this->AccountTag->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'account_comment_tags as act',
                        'type' => 'inner',
                        'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'act.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders as af',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = af.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountTag.tag_type' => 0,
                    'AccountTag.account_id' => $account_ids,
                    'AccountTag.account_tag_id' => $acc_tags,
                    'af.folder_type' => 1,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    $dCnd,
                    $act_frm_wrk
                ),
                'fields' => array(
                    'AccountTag.tag_title',
                    'AccountTag.account_tag_id',
                    'act.created_date AS AccountTag__tags_date',
                    'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                ),
                'group' => array('AccountTag.account_tag_id')
            ));

            $account_coach_tags_analytics = Set::extract('/AccountTag/.', $account_coach_tags_analytics);
            if (is_array($account_coach_tags_analytics) && count($account_coach_tags_analytics) > 0) {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $flag = false;
                    foreach ($account_coach_tags_analytics as $acta) {
                        if ($acc_tags[$i] == $acta['account_tag_id']) {
                            $graph_data['tag' . $i] = $acta['total_tags'];
                            $graph_data['tag_title_' . $i] = $acta['total_tags'];
                            $flag = true;
                        }
                    }
                    if ($flag == false)
                        $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }else {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }

            $account_video_analytics = $this->AccountFolder->find('count', array(
                'joins' => array(
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'AccountFolder.account_folder_id = afd.account_folder_id'
                    ),
                    array(
                        'table' => 'documents as d',
                        'type' => 'inner',
                        'conditions' => 'afd.document_id = d.id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                    'AccountFolder.folder_type' => 1,
                    'd.doc_type' => 1,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'AccountFolder.account_id' => $account_ids,
                    $docCnd
                )
            ));
            $graph_data['total_sessions'] = $account_video_analytics;
            $graph_data_all[] = $graph_data;
            unset($graph_data);
        }

        $all_ratting = $this->analytics_for_account();

        $fileName = 'GRAPH EXPORT_' . date('m-d-Y') . '.xlsx';
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                ->setLastModifiedBy("Maarten Balliauw")
                ->setTitle("PHPExcel Test Document")
                ->setSubject("PHPExcel Test Document")
                ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                ->setKeywords("office PHPExcel php")
                ->setCategory("Test result file");


        if ($this->data['coaching_eval'] == '1') {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B1', 'Frequency of Tagged Standards for Coaching Huddle video sessions.')

            ;
        } else {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B1', 'Frequency of Tagged Standards and ratings for Assessment Huddles.')

            ;
        }



        $count = 0;
        $letter = 'B';
        $line = 3;








        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue($letter . $line, 'DATE')

        ;


        $letter++;

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue($letter . $line, 'Total Sessions')

        ;


        $letter++;
        $count = 0;


        while ($count < count($graph_data_all[0])) {
            if (isset($graph_data_all[0]['tag_title_' . $count])) {
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($letter . $line, $graph_data_all[0]['tag_title_' . $count])

                ;
                $letter++;
            }
            $count++;
        }


        $count = 0;
        $line++;
        $letter = 'B';



        while ($count < count($graph_data_all)) {
            $letter = 'B';

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($letter . $line, $graph_data_all[$count]['date'])

            ;

            $letter++;

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($letter . $line, $graph_data_all[$count]['total_sessions'])

            ;

            $letter++;


            $count1 = 0;

            while ($count1 < count($graph_data_all[$count])) {
                if (isset($graph_data_all[$count]['tag' . $count1])) {
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($letter . $line, $graph_data_all[$count]['tag' . $count1])

                    ;

                    $letter++;
                }
                $count1++;
            }





            $line = $line + 2;
            $count++;
        }







        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=$fileName");
        $objWriter->save('php://output');




        exit;
    }

    function accountUsersAnalytics() {
        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $mAUCndS = '';
        $mAUCndE = '';
        $account_ids = array();
        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];
        if (!empty($this->data['subAccount'])) {
            $account_ids[] = $this->data['subAccount'];
        } else {
            $account_ov_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'id' => $account_id
                ),
                'fields' => array('id')
            ));

            $account_overview_analytics = $this->Account->find('all', array(
                'conditions' => array(
                    'Account.is_active' => 1,
                    'parent_account_id' => $account_id
                ),
                'fields' => array('id')
            ));
            $all_accounts = array_merge($account_ov_analytics, $account_overview_analytics);
            foreach ($all_accounts as $all_ac) {
                $account_ids[] = $all_ac['Account']['id'];
            }
        }

        $acc_ids = implode(',', $account_ids);

        $curr_comp = $this->Account->find('first', array(
            'conditions' => array(
                'id' => $account_id
            ),
            'fields' => array('company_name')
        ));

        if (!empty($this->data['startDate'])) {
            $stDate = date_create($this->data['startDate']);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        } else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }
        $mAUCndS = ' AND au.created_date >="' . $st_date . '"';
        $mAUCndSF = ' AND created_date >="' . $st_date . '"';
        $mAUCndSD = ' AND d.created_date >="' . $st_date . '"';
        $mAUCndSLog = ' AND date_added >="' . $st_date . '"';

        if (!empty($this->data['endDate'])) {
            $endDate = date_create($this->data['endDate']);
            $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
        } else {
            $endDate = date_create(date('Y-m-d'));
            $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
        }

        $mAUCndE = ' AND au.created_date <="' . $end_date . '"';
        $mAUCndEF = ' AND created_date <="' . $end_date . '"';
        $mAUCndED = ' AND d.created_date <="' . $end_date . '"';
        $mAUCndELog = ' AND date_added <="' . $end_date . '"';

        $all_users = $this->UserAccount->query("SELECT usr.user_id,usr.account_id,usr.username as username, usr.email,usr.company_name,CONCAT(usr.first_name,' ',usr.last_name) AS account_name,
                                                    web_login_counts.login_counts AS web_login_counts,
                                                    video_upload_counts.video_upload_counts AS video_upload_counts,
                                                    workspace_upload_counts.workspace_upload_counts AS workspace_upload_counts,
                                                    library_upload_counts.library_upload_counts AS library_upload_counts,
                                                    shared_upload_counts.shared_upload_counts AS shared_upload_counts,
                                                    library_shared_upload_counts.library_shared_upload_counts AS library_shared_upload_counts,
                                                    huddle_created_count.huddle_created_count AS huddle_created_count,
                                                    comments_initiated_count.comments_initiated_count AS comments_initiated_count,
                                                    workspace_comments_initiated_count.workspace_comments_initiated_count AS workspace_comments_initiated_count,
                                                    replies_initiated_count.replies_initiated_count AS replies_initiated_count,
                                                    videos_viewed_count.videos_viewed_count AS videos_viewed_count,
                                                    workspace_videos_viewed_count.workspace_videos_viewed_count AS workspace_videos_viewed_count,
                                                    library_videos_viewed_count.library_videos_viewed_count AS library_videos_viewed_count,
                                                    documents_uploaded_count.documents_uploaded_count AS documents_uploaded_count,
                                                    documents_viewed_count.documents_viewed_count AS documents_viewed_count,
                                                    scripted_observations.scripted_observations AS scripted_observations,
                                                    scripted_video_observations.scripted_video_observations AS scripted_video_observations

                                                    FROM (SELECT u.id AS user_id,u.username, u.email,u.first_name,u.last_name,a.company_name as company_name,a.id as account_id FROM users u LEFT JOIN users_accounts ua ON u.id = ua.user_id LEFT JOIN accounts a ON ua.account_id = a.id  WHERE u.is_active=1 AND u.id NOT IN  (2423,2422,2427)  AND u.type='Active' AND ua.account_id IN (" . $acc_ids . ") GROUP BY u.id) AS usr

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS login_counts FROM `user_activity_logs` WHERE TYPE=9 AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " AND user_activity_logs.site_id=" . $this->site_id . " GROUP BY user_id) AS web_login_counts

                                                    ON usr.user_id = web_login_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS video_upload_counts FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) ADN af.site_id=" . $this->site_id . " AND af.`folder_type` IN(1) AND ua.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY ua.user_id) AS video_upload_counts
                                                    ON usr.user_id = video_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS workspace_upload_counts FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.site_id=" . $this->site_id . " AND af.`folder_type` IN(3) AND ua.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY ua.user_id) AS workspace_upload_counts
                                                    ON usr.user_id = workspace_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS library_upload_counts FROM `user_activity_logs` ua LEFT JOIN
    account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE (ua.TYPE=2 OR ua.TYPE=4) AND af.site_id=" . $this->site_id . " AND  af.`folder_type` IN(2) AND ua.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY ua.user_id) AS library_upload_counts
                                                    ON usr.user_id = library_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT d.created_by AS user_id, COUNT(*) AS shared_upload_counts
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 1 AND ua.`type` = 22 AND af.site_id =" . $this->site_id . " AND d.doc_type = 1  GROUP BY d.created_by) AS shared_upload_counts
                                                    ON usr.user_id = shared_upload_counts.user_id

                                                    LEFT JOIN

                                                      (SELECT d.created_by AS user_id, COUNT(*) AS library_shared_upload_counts
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 2 AND ua.`type` = 22 AND af.site_id =" . $this->site_id . " AND d.doc_type = 1  GROUP BY d.created_by) AS library_shared_upload_counts
                                                    ON usr.user_id = library_shared_upload_counts.user_id

                                                    LEFT JOIN

                                                    (SELECT user_id,COUNT(*) AS `huddle_created_count` FROM `user_activity_logs`  WHERE `type` = 1 AND user_activity_logs.site_id = " . $this->site_id . " AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS huddle_created_count
                                                    ON usr.user_id = huddle_created_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id join account_folders on user_activity_logs.account_folder_id = account_folders.account_folder_id   WHERE TYPE=5 AND account_folders.folder_type = 1 AND user_activity_logs.site_id ='" . $this->site_id . "'  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_activity_logs.user_id) AS comments_initiated_count

                                                    ON usr.user_id = comments_initiated_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS workspace_comments_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id join account_folders on user_activity_logs.account_folder_id = account_folders.account_folder_id   WHERE TYPE=5 AND account_folders.folder_type = 3 AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_activity_logs.user_id) AS workspace_comments_initiated_count

                                                    ON usr.user_id = workspace_comments_initiated_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_activity_logs.user_id, COUNT(*) AS replies_initiated_count FROM `user_activity_logs` join comments on user_activity_logs.ref_id = comments.id WHERE TYPE=8 AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_activity_logs.user_id) AS replies_initiated_count

                                                    ON usr.user_id = replies_initiated_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS documents_uploaded_count FROM `user_activity_logs` WHERE TYPE=3 AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS documents_uploaded_count

                                                    ON usr.user_id = documents_uploaded_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS documents_viewed_count FROM `user_activity_logs` WHERE TYPE=13 AND account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS documents_viewed_count

                                                    ON usr.user_id = documents_viewed_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS videos_viewed_count
                                                        FROM `user_activity_logs`
                                                        join account_folder_documents on user_activity_logs.ref_id = account_folder_documents.document_id
                                                        join account_folders on account_folders.account_folder_id = account_folder_documents.account_folder_id

                                                        WHERE TYPE=11 AND account_folders.folder_type = 1  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS videos_viewed_count

                                                    ON usr.user_id = videos_viewed_count.`user_id`


                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS workspace_videos_viewed_count
                                                        FROM `user_activity_logs`
                                                        join account_folder_documents on user_activity_logs.ref_id = account_folder_documents.document_id
                                                        join account_folders on account_folders.account_folder_id = account_folder_documents.account_folder_id

                                                        WHERE TYPE=11 AND account_folders.folder_type = 3  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS workspace_videos_viewed_count

                                                    ON usr.user_id = workspace_videos_viewed_count.`user_id`

                                                    LEFT JOIN

                                                    (SELECT user_id, COUNT(*) AS library_videos_viewed_count
                                                        FROM `user_activity_logs`
                                                        join account_folder_documents on user_activity_logs.ref_id = account_folder_documents.document_id
                                                        join account_folders on account_folders.account_folder_id = account_folder_documents.account_folder_id

                                                        WHERE TYPE=11 AND account_folders.folder_type = 2  AND user_activity_logs.account_id IN (" . $acc_ids . ")" . $mAUCndSLog . $mAUCndELog . " GROUP BY user_id) AS library_videos_viewed_count

                                                    ON usr.user_id = library_videos_viewed_count.`user_id`


                                                      LEFT JOIN

                                                    (SELECT d.created_by AS user_id, COUNT(*) AS scripted_observations
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 1 AND ua.`type` = 23 AND d.is_associated = 1  AND d.current_duration is NULL AND d.published = 1 AND d.doc_type = 3  GROUP BY d.created_by) AS scripted_observations
                                                    ON usr.user_id = scripted_observations.user_id


                                                    LEFT JOIN

                                                    (SELECT d.created_by AS user_id, COUNT(*) AS scripted_video_observations
                                                        FROM documents d
                                                        LEFT JOIN account_folder_documents afd
                                                        ON d.`id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN user_activity_logs ua
                                                        ON (d.id = ua.ref_id)
                                                        WHERE d.account_id IN (" . $acc_ids . ")" . $mAUCndSD . $mAUCndED . " AND af.`folder_type` = 1 AND ua.`type` = 20 AND d.is_associated > 0  AND d.current_duration is NOT NULL AND d.published = 1 AND d.doc_type = 3  GROUP BY d.created_by) AS scripted_video_observations
                                                    ON usr.user_id = scripted_video_observations.user_id


                                                    group by usr.`user_id`





                                                ");




        $view = new View($this, false);

        $user_data = array();
        $cnt = 0;
        foreach ($all_users as $user):
            $user_data[$cnt]['account_id'] = $user['usr']['account_id'];
            $user_data[$cnt]['company_name'] = $user['usr']['company_name'];
            $user_data[$cnt]['user_id'] = $user['usr']['user_id'];
            $user_data[$cnt]['account_name'] = utf8_encode($user[0]['account_name']);
            $user_data[$cnt]['email'] = $user['usr']['email'];
            $user_data[$cnt]['video_upload_counts'] = !empty($user['video_upload_counts']['video_upload_counts']) ? $user['video_upload_counts']['video_upload_counts'] : 0;
            $user_data[$cnt]['workspace_upload_counts'] = !empty($user['workspace_upload_counts']['workspace_upload_counts']) ? $user['workspace_upload_counts']['workspace_upload_counts'] : 0;
            $user_data[$cnt]['library_upload_counts'] = !empty($user['library_upload_counts']['library_upload_counts']) ? $user['library_upload_counts']['library_upload_counts'] : 0;
            $user_data[$cnt]['shared_upload_counts'] = !empty($user['shared_upload_counts']['shared_upload_counts']) ? $user['shared_upload_counts']['shared_upload_counts'] : 0;
            $user_data[$cnt]['library_shared_upload_counts'] = !empty($user['library_shared_upload_counts']['library_shared_upload_counts']) ? $user['library_shared_upload_counts']['library_shared_upload_counts'] : 0;
            $user_data[$cnt]['videos_viewed_count'] = !empty($user['videos_viewed_count']['videos_viewed_count']) ? $user['videos_viewed_count']['videos_viewed_count'] : 0;
            $user_data[$cnt]['workspace_videos_viewed_count'] = !empty($user['workspace_videos_viewed_count']['workspace_videos_viewed_count']) ? $user['workspace_videos_viewed_count']['workspace_videos_viewed_count'] : 0;
            $user_data[$cnt]['library_videos_viewed_count'] = !empty($user['library_videos_viewed_count']['library_videos_viewed_count']) ? $user['library_videos_viewed_count']['library_videos_viewed_count'] : 0;
            $user_data[$cnt]['huddle_created_count'] = !empty($user['huddle_created_count']['huddle_created_count']) ? $user['huddle_created_count']['huddle_created_count'] : 0;
            $user_data[$cnt]['comments_initiated_count'] = !empty($user['comments_initiated_count']['comments_initiated_count']) ? $user['comments_initiated_count']['comments_initiated_count'] : 0;
            $user_data[$cnt]['workspace_comments_initiated_count'] = !empty($user['workspace_comments_initiated_count']['workspace_comments_initiated_count']) ? $user['workspace_comments_initiated_count']['workspace_comments_initiated_count'] : 0;
            $user_data[$cnt]['replies_initiated_count'] = !empty($user['replies_initiated_count']['replies_initiated_count']) ? $user['replies_initiated_count']['replies_initiated_count'] : 0;
            $user_data[$cnt]['documents_uploaded_count'] = !empty($user['documents_uploaded_count']['documents_uploaded_count']) ? $user['documents_uploaded_count']['documents_uploaded_count'] : 0;
            $user_data[$cnt]['documents_viewed_count'] = !empty($user['documents_viewed_count']['documents_viewed_count']) ? $user['documents_viewed_count']['documents_viewed_count'] : 0;
            $user_data[$cnt]['web_login_counts'] = !empty($user['web_login_counts']['web_login_counts']) ? $user['web_login_counts']['web_login_counts'] : 0;
            $user_data[$cnt]['scripted_observations'] = !empty($user['scripted_observations']['scripted_observations']) ? $user['scripted_observations']['scripted_observations'] : 0;
            $user_data[$cnt]['scripted_video_observations'] = !empty($user['scripted_video_observations']['scripted_video_observations']) ? $user['scripted_video_observations']['scripted_video_observations'] : 0;
            $user_data[$cnt]['detail'] = 'detail';
            $cnt++;
        endforeach;

        $user_data_frameworks = array();
        $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                "tag_type" => array(0, 2), "account_id" => $account_id
        )));
        if (!empty($frameworks) && count($frameworks) > 0) {
            $user_data_frameworks['frameworks'] = $frameworks;
        }
        $user_data_frameworks['account_id'] = $account_id;

        $analytics_result = array(
            'user_data' => json_encode($user_data),
            'user_data_frameworks' => $user_data_frameworks,
        );

        $html = $view->element('dashboard/accountUsersAnalytics', $analytics_result);

        if (!empty($this->data['startDate']) || !empty($this->data['endDate']))
            echo $html;
        else
            return $html;

        die;
    }

    function _userGraphForEvaluationHuddle($user_id, $account_id, $start_date = '', $end_date = '', $durationGrph = 3, $folder_type = 2) {
        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $owner_account_id = $user['accounts']['account_id'];
        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $owner_account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];
        //echo $acct_duration_start;
        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();
        if (isset($this->data['durationGrph']) && !empty($this->data['durationGrph'])) {
            $durationGrph = $this->data['durationGrph'];
            $search_by_standard = $this->data['search_by_standards'];
        }
        $curr_user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $user_id
            ),
            'fields' => array('id', 'first_name', 'last_name')
        ));

        if (!empty($this->data['framework_id'])) {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id, 'account_tag_id' => $this->data['framework_id']
            )));
        } else {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id
            )));
        }

        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
        }

        if (!empty($this->data['framework_id'])) {
            $frm_wrk = array('framework_id' => $this->data['framework_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $this->data['framework_id']);
        } else {
            $frm_wrk = array('framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
        }

        if (!empty($start_date)) {
            $sdate = explode('-', $start_date);
            $stDate = $sdate[2] . '-' . $sdate[0] . '-' . $sdate[1];
            if ($sdate[0] >= $acct_duration_start) {
                $st_d = $sdate[0] - $acct_duration_start;
                if ($st_d >= 0 && $st_d < 3) {
                    $st_quarter = 1;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 3 && $st_d < 6) {
                    $st_quarter = 2;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 6 && $st_d < 9) {
                    $st_quarter = 3;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 9 && $st_d < 12) {
                    $st_quarter = 4;
                    $st_yr = $sdate[2];
                }
            } else {
                $st_d = $acct_duration_start - $sdate[0];
                if ($st_d >= 0 && $st_d < 3) {
                    $st_quarter = 4;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 3 && $st_d < 6) {
                    $st_quarter = 3;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 6 && $st_d < 9) {
                    $st_quarter = 2;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 9 && $st_d < 12) {
                    $st_quarter = 1;
                    $st_yr = $sdate[2] - 1;
                }
            }
            $st_date = date('Y-m-d', strtotime($stDate)) . ' 00:00:00';
        } else {
            $stDate = date_create(date('Y-m-d', strtotime('-3 month')));
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }

        $mAUCnd[] = array('UserAccount.created_date >= ' => $st_date);
        $mAHCnd[] = array('AccountFolder.created_date >= ' => $st_date);
        $mAVCnd[] = array('d.created_date >= ' => $st_date);
        $mAVWCnd[] = array('UserActivityLog.date_added >= ' => $st_date);
        $mATCnd[] = array('act.created_date >= ' => $st_date);
        if (!empty($end_date)) {
            $edate = explode('-', $end_date);
            $enDate = $edate[2] . '-' . $edate[0] . '-' . $edate[1];
            $en_date = date('Y-m-d', strtotime($enDate)) . ' 23:59:59';
        } else {
            $enDate = date_create(date('Y-m-d'));
            $en_date = date_format($enDate, 'Y-m-d') . ' 23:59:59';
        }

        $mAUCnd[] = array('UserAccount.created_date <= ' => $en_date);
        $mAHCnd[] = array('AccountFolder.created_date <= ' => $en_date);
        $mAVCnd[] = array('d.created_date <= ' => $en_date);
        $mAVWCnd[] = array('UserActivityLog.date_added <= ' => $en_date);
        $mATCnd[] = array('act.created_date <= ' => $en_date);
        if (!empty($search_by_standard)) {
            $mATCnd[] = array('act.account_tag_id IN( ' . $search_by_standard . ')');
        }

        if (isset($this->data['userType']) && $this->data['userType'] == "coach") {
            $mATCnd[] = array('afu.is_coach' => '1');
            $mATCnd[] = array('afu.user_id' => $user_id);
            $this->set('uType', $this->data['userType']);
        } elseif (isset($this->data['userType']) && $this->data['userType'] == "coachee") {
            $mATCnd[] = array('afu.is_mentee' => '1');
            $mATCnd[] = array('afu.user_id' => $user_id);
            $this->set('uType', $this->data['userType']);
        } else {
            $this->set('uType', 'all');
        }
        $start = (new DateTime($st_date))->modify('first day of this month');
        $end = (new DateTime($en_date))->modify('last day of this month');
        if ($durationGrph == 1)
            $interval = DateInterval::createFromDateString('1 year');
        if ($durationGrph == 2)
            $interval = DateInterval::createFromDateString('3 month');
        if ($durationGrph == 3)
            $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        $this->AccountTag->virtualFields['tags_date'] = 0;
        $this->AccountTag->virtualFields['total_tags'] = 0;
        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'act.ref_id = afd.document_id'
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = af.account_folder_id'
            ),
            array(
                'table' => 'account_folders_meta_data as afmd',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
            )
            ,
            array(
                'table' => 'account_folder_users as afu',
                'type' => 'inner',
                'conditions' => 'afu.account_folder_id = afmd.account_folder_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'AccountTag.account_id' => $account_id,
            'af.folder_type' => 1,
            'act.created_by' => $curr_user['User']['id'],
            'afmd.meta_data_name' => 'folder_type',
            'afmd.meta_data_value' => 3,
            $mATCnd,
            $act_frm_wrk
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        if (empty($search_by_standard)) {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
                'limit' => 5
            );
        } else {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
            );
        }
        $account_tags_analytics = $this->AccountTag->find('all', $get_value);
//        echo $this->AccountTag->getLastQuery();
//        exit;
        $account_tags_analytics = Set::extract('/AccountTag/.', $account_tags_analytics);
        $acc_tags = array();
        $acc_tag_title = array();
        foreach ($account_tags_analytics as $actag) {
            $acc_tags[] = $actag['account_tag_id'];
            if (empty($actag['tads_code']))
                $acc_tag_title[] = $actag['tag_title'];
            else
                $acc_tag_title[] = $actag['tads_code'];
        }

        $graph_data_all = array();
        $graph_data = array();
        if ($durationGrph == 2)
            $m = $st_quarter;
        foreach ($period as $dt) {

            $g_data = $dt->format("Y-m-d");

            //$graph_data['date'] = $dt->format("Y-m-d");
            $duration = explode('-', $dt->format("Y-m-d"));
            $dCnd = array();
            $docCnd = array();
            if ($durationGrph == 1) {
                $graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('YEAR(act.created_date)' => $duration[0]);
                $docCnd[] = array('YEAR(d.created_date)' => $duration[0]);
            }
            if ($durationGrph == 2) {
                $v = $m % 4;
                if ($v == 0) {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                    $st_yr = $st_yr + 1;
                    $m = 0;
                } else {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                }
                //$graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $m++;
            }
            if ($durationGrph == 3) {
                $graph_data['date'] = $dt->format("M-y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
            }

            $account_coach_tags_analytics = $this->AccountTag->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'account_comment_tags as act',
                        'type' => 'inner',
                        'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'act.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders as af',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = af.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountTag.tag_type' => 0,
                    'AccountTag.account_id' => $account_id,
                    'AccountTag.account_tag_id' => $acc_tags,
                    'af.folder_type' => 1,
                    'act.created_by' => $curr_user['User']['id'],
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => 3,
                    $dCnd,
                    $act_frm_wrk
                ),
                'fields' => array(
                    'AccountTag.tag_title',
                    'AccountTag.account_tag_id',
                    'act.created_date AS AccountTag__tags_date',
                    'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                ),
                'group' => array('AccountTag.account_tag_id')
            ));

            $account_coach_tags_analytics = Set::extract('/AccountTag/.', $account_coach_tags_analytics);
            if (is_array($account_coach_tags_analytics) && count($account_coach_tags_analytics) > 0) {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $flag = false;
                    foreach ($account_coach_tags_analytics as $acta) {
                        if ($acc_tags[$i] == $acta['account_tag_id']) {
                            $graph_data['tag' . $i] = $acta['total_tags'];
                            $graph_data['tag_title_' . $i] = $acta['total_tags'];
                            $flag = true;
                        }
                    }
                    if ($flag == false)
                        $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }else {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }

            $account_video_analytics = $this->AccountFolder->find('count', array(
                'joins' => array(
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'AccountFolder.account_folder_id = afd.account_folder_id'
                    ),
                    array(
                        'table' => 'documents as d',
                        'type' => 'inner',
                        'conditions' => 'afd.document_id = d.id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folder_users as afu',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afu.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                    'AccountFolder.folder_type' => 1,
                    'd.doc_type' => 1,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => 3,
                    'afu.user_id' => $curr_user['User']['id'],
                    'AccountFolder.account_id' => $account_id,
                    $docCnd
                )
            ));
            $graph_data['total_sessions'] = $account_video_analytics;
            $graph_data_all[] = $graph_data;
            unset($graph_data);
        }
        $standards = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 0,
                $frm_wrk
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code")
        ));

        $standards_array = array();
        if ($standards) {
            foreach ($standards as $st) {
                if (empty($st['AccountTag']['tads_code']))
                    $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                else
                    $ac_tag_title = $st['AccountTag']['tads_code'];
                $standard_array[] = array(
                    "value" => $st['AccountTag']['account_tag_id'],
                    "label" => $ac_tag_title
                );
            }
        }
        $total_evaluation_tags = json_encode($graph_data_all);

        return $total_evaluation_tags;
    }

    function userGraphForEvaluationHuddle_export($user_id, $account_id, $start_date = '', $end_date = '', $durationGrph = 3, $folder_type = 2) {
        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $owner_account_id = $user['accounts']['account_id'];
        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $owner_account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];
        //echo $acct_duration_start;
        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();
        if (isset($this->data['durationGrph']) && !empty($this->data['durationGrph'])) {
            $durationGrph = $this->data['durationGrph'];
            $search_by_standard = $this->data['search_by_standards'];
        }
        $curr_user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $user_id
            ),
            'fields' => array('id', 'first_name', 'last_name')
        ));

        if (!empty($this->data['framework_id'])) {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id, 'account_tag_id' => $this->data['framework_id']
            )));
        } else {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id
            )));
        }

        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
        }

        if (!empty($this->data['framework_id'])) {
            $frm_wrk = array('framework_id' => $this->data['framework_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $this->data['framework_id']);
        } else {
            $frm_wrk = array('framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
        }

        if (!empty($start_date)) {
            $sdate = explode('-', $start_date);
            $stDate = $sdate[2] . '-' . $sdate[0] . '-' . $sdate[1];
            if ($sdate[0] >= $acct_duration_start) {
                $st_d = $sdate[0] - $acct_duration_start;
                if ($st_d >= 0 && $st_d < 3) {
                    $st_quarter = 1;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 3 && $st_d < 6) {
                    $st_quarter = 2;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 6 && $st_d < 9) {
                    $st_quarter = 3;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 9 && $st_d < 12) {
                    $st_quarter = 4;
                    $st_yr = $sdate[2];
                }
            } else {
                $st_d = $acct_duration_start - $sdate[0];
                if ($st_d >= 0 && $st_d < 3) {
                    $st_quarter = 4;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 3 && $st_d < 6) {
                    $st_quarter = 3;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 6 && $st_d < 9) {
                    $st_quarter = 2;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 9 && $st_d < 12) {
                    $st_quarter = 1;
                    $st_yr = $sdate[2] - 1;
                }
            }
            $st_date = date('Y-m-d', strtotime($stDate)) . ' 00:00:00';
        } else {
            $stDate = date_create(date('Y-m-d', strtotime('-3 month')));
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }

        $mAUCnd[] = array('UserAccount.created_date >= ' => $st_date);
        $mAHCnd[] = array('AccountFolder.created_date >= ' => $st_date);
        $mAVCnd[] = array('d.created_date >= ' => $st_date);
        $mAVWCnd[] = array('UserActivityLog.date_added >= ' => $st_date);
        $mATCnd[] = array('act.created_date >= ' => $st_date);
        if (!empty($end_date)) {
            $edate = explode('-', $end_date);
            $enDate = $edate[2] . '-' . $edate[0] . '-' . $edate[1];
            $en_date = date('Y-m-d', strtotime($enDate)) . ' 23:59:59';
        } else {
            $enDate = date_create(date('Y-m-d'));
            $en_date = date_format($enDate, 'Y-m-d') . ' 23:59:59';
        }

        $mAUCnd[] = array('UserAccount.created_date <= ' => $en_date);
        $mAHCnd[] = array('AccountFolder.created_date <= ' => $en_date);
        $mAVCnd[] = array('d.created_date <= ' => $en_date);
        $mAVWCnd[] = array('UserActivityLog.date_added <= ' => $en_date);
        $mATCnd[] = array('act.created_date <= ' => $en_date);
        if (!empty($search_by_standard)) {
            $mATCnd[] = array('act.account_tag_id IN( ' . $search_by_standard . ')');
        }

        if (isset($this->data['userType']) && $this->data['userType'] == "coach") {
            $mATCnd[] = array('afu.is_coach' => '1');
            $mATCnd[] = array('afu.user_id' => $user_id);
            $this->set('uType', $this->data['userType']);
        } elseif (isset($this->data['userType']) && $this->data['userType'] == "coachee") {
            $mATCnd[] = array('afu.is_mentee' => '1');
            $mATCnd[] = array('afu.user_id' => $user_id);
            $this->set('uType', $this->data['userType']);
        } else {
            $this->set('uType', 'all');
        }
        $start = (new DateTime($st_date))->modify('first day of this month');
        $end = (new DateTime($en_date))->modify('last day of this month');
        if ($durationGrph == 1)
            $interval = DateInterval::createFromDateString('1 year');
        if ($durationGrph == 2)
            $interval = DateInterval::createFromDateString('3 month');
        if ($durationGrph == 3)
            $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        $this->AccountTag->virtualFields['tags_date'] = 0;
        $this->AccountTag->virtualFields['total_tags'] = 0;
        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'act.ref_id = afd.document_id'
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = af.account_folder_id'
            ),
            array(
                'table' => 'account_folders_meta_data as afmd',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
            )
            ,
            array(
                'table' => 'account_folder_users as afu',
                'type' => 'inner',
                'conditions' => 'afu.account_folder_id = afmd.account_folder_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'AccountTag.account_id' => $account_id,
            'af.folder_type' => 1,
            'act.created_by' => $curr_user['User']['id'],
            'afmd.meta_data_name' => 'folder_type',
            'afmd.meta_data_value' => 3,
            $mATCnd,
            $act_frm_wrk
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        if (empty($search_by_standard)) {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
                'limit' => 5
            );
        } else {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
            );
        }
        $account_tags_analytics = $this->AccountTag->find('all', $get_value);
//        echo $this->AccountTag->getLastQuery();
//        exit;
        $account_tags_analytics = Set::extract('/AccountTag/.', $account_tags_analytics);
        $acc_tags = array();
        $acc_tag_title = array();
        foreach ($account_tags_analytics as $actag) {
            $acc_tags[] = $actag['account_tag_id'];
            if (empty($actag['tads_code']))
                $acc_tag_title[] = $actag['tag_title'];
            else
                $acc_tag_title[] = $actag['tads_code'];
        }

        $graph_data_all = array();
        $graph_data = array();
        if ($durationGrph == 2)
            $m = $st_quarter;
        foreach ($period as $dt) {

            $g_data = $dt->format("Y-m-d");

            //$graph_data['date'] = $dt->format("Y-m-d");
            $duration = explode('-', $dt->format("Y-m-d"));
            $dCnd = array();
            $docCnd = array();
            if ($durationGrph == 1) {
                $graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('YEAR(act.created_date)' => $duration[0]);
                $docCnd[] = array('YEAR(d.created_date)' => $duration[0]);
            }
            if ($durationGrph == 2) {
                $v = $m % 4;
                if ($v == 0) {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                    $st_yr = $st_yr + 1;
                    $m = 0;
                } else {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                }
                //$graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $m++;
            }
            if ($durationGrph == 3) {
                $graph_data['date'] = $dt->format("M-y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
            }

            $account_coach_tags_analytics = $this->AccountTag->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'account_comment_tags as act',
                        'type' => 'inner',
                        'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'act.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders as af',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = af.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountTag.tag_type' => 0,
                    'AccountTag.account_id' => $account_id,
                    'AccountTag.account_tag_id' => $acc_tags,
                    'af.folder_type' => 1,
                    'act.created_by' => $curr_user['User']['id'],
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => 3,
                    $dCnd,
                    $act_frm_wrk
                ),
                'fields' => array(
                    'AccountTag.tag_title',
                    'AccountTag.account_tag_id',
                    'act.created_date AS AccountTag__tags_date',
                    'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                ),
                'group' => array('AccountTag.account_tag_id')
            ));

            $account_coach_tags_analytics = Set::extract('/AccountTag/.', $account_coach_tags_analytics);
            if (is_array($account_coach_tags_analytics) && count($account_coach_tags_analytics) > 0) {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $flag = false;
                    foreach ($account_coach_tags_analytics as $acta) {
                        if ($acc_tags[$i] == $acta['account_tag_id']) {
                            $graph_data['tag' . $i] = $acta['total_tags'];
                            $graph_data['tag_title_' . $i] = $acta['total_tags'];
                            $flag = true;
                        }
                    }
                    if ($flag == false)
                        $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }else {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }

            $account_video_analytics = $this->AccountFolder->find('count', array(
                'joins' => array(
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'AccountFolder.account_folder_id = afd.account_folder_id'
                    ),
                    array(
                        'table' => 'documents as d',
                        'type' => 'inner',
                        'conditions' => 'afd.document_id = d.id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folder_users as afu',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afu.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                    'AccountFolder.folder_type' => 1,
                    'd.doc_type' => 1,
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => 3,
                    'afu.user_id' => $curr_user['User']['id'],
                    'AccountFolder.account_id' => $account_id,
                    $docCnd
                )
            ));
            $graph_data['total_sessions'] = $account_video_analytics;
            $graph_data_all[] = $graph_data;
            unset($graph_data);
        }
        $standards = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 0,
                $frm_wrk
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code")
        ));

        $standards_array = array();
        if ($standards) {
            foreach ($standards as $st) {
                if (empty($st['AccountTag']['tads_code']))
                    $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                else
                    $ac_tag_title = $st['AccountTag']['tads_code'];
                $standard_array[] = array(
                    "value" => $st['AccountTag']['account_tag_id'],
                    "label" => $ac_tag_title
                );
            }
        }


        $fileName = 'GRAPH EXPORT_' . date('m-d-Y') . '.xlsx';
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                ->setLastModifiedBy("Maarten Balliauw")
                ->setTitle("PHPExcel Test Document")
                ->setSubject("PHPExcel Test Document")
                ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                ->setKeywords("office PHPExcel php")
                ->setCategory("Test result file");


        if ($this->data['coaching_eval'] == '1') {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B1', 'Frequency of Tagged Standards for Coaching Huddle video sessions.')

            ;
        } else {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B1', "Evidence of standards in " . $this->data['user_name'] . "'s Assessment Huddle")

            ;
        }



        $count = 0;
        $letter = 'B';
        $line = 3;








        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue($letter . $line, 'DATE')

        ;


        $letter++;

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue($letter . $line, 'Total Sessions')

        ;


        $letter++;
        $count = 0;


        while ($count < count($graph_data_all[0])) {
            if (isset($graph_data_all[0]['tag_title_' . $count])) {
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($letter . $line, $graph_data_all[0]['tag_title_' . $count])

                ;
                $letter++;
            }
            $count++;
        }


        $count = 0;
        $line++;
        $letter = 'B';



        while ($count < count($graph_data_all)) {
            $letter = 'B';

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($letter . $line, $graph_data_all[$count]['date'])

            ;

            $letter++;

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($letter . $line, $graph_data_all[$count]['total_sessions'])

            ;

            $letter++;


            $count1 = 0;

            while ($count1 < count($graph_data_all[$count])) {
                if (isset($graph_data_all[$count]['tag' . $count1])) {
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($letter . $line, $graph_data_all[$count]['tag' . $count1])

                    ;

                    $letter++;
                }
                $count1++;
            }





            $line = $line + 2;
            $count++;
        }







        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=$fileName");
        $objWriter->save('php://output');




        exit;
    }

    function userGraph($user_id, $account_id, $start_date = '', $end_date = '', $durationGrph = 3, $folder_type = 2) {
        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $owner_account_id = $user['accounts']['account_id'];
        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $owner_account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];
        //echo $acct_duration_start;
        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();
        if (isset($this->data['durationGrph']) && !empty($this->data['durationGrph'])) {
            $durationGrph = $this->data['durationGrph'];
            $search_by_standard = $this->data['search_by_standards'];
        }
        $curr_user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $user_id
            ),
            'fields' => array('id', 'first_name', 'last_name')
        ));

        if (!empty($this->data['framework_id'])) {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id, 'account_tag_id' => $this->data['framework_id']
            )));
        } else {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id
            )));
        }

        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
        }

        if (!empty($this->data['framework_id'])) {
            $frm_wrk = array('framework_id' => $this->data['framework_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $this->data['framework_id']);
        } else {
            $frm_wrk = array('framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
        }

        if (!empty($start_date)) {
            $sdate = explode('-', $start_date);
            $stDate = $sdate[2] . '-' . $sdate[0] . '-' . $sdate[1];
            if ($sdate[0] >= $acct_duration_start) {
                $st_d = $sdate[0] - $acct_duration_start;
                if ($st_d >= 0 && $st_d < 3) {
                    $st_quarter = 1;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 3 && $st_d < 6) {
                    $st_quarter = 2;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 6 && $st_d < 9) {
                    $st_quarter = 3;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 9 && $st_d < 12) {
                    $st_quarter = 4;
                    $st_yr = $sdate[2];
                }
            } else {
                $st_d = $acct_duration_start - $sdate[0];
                if ($st_d >= 0 && $st_d < 3) {
                    $st_quarter = 4;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 3 && $st_d < 6) {
                    $st_quarter = 3;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 6 && $st_d < 9) {
                    $st_quarter = 2;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 9 && $st_d < 12) {
                    $st_quarter = 1;
                    $st_yr = $sdate[2] - 1;
                }
            }
            $st_date = date('Y-m-d', strtotime($stDate)) . ' 00:00:00';
        } else {
            $stDate = date_create(date('Y-m-d', strtotime('-3 month')));
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }

        $mAUCnd[] = array('UserAccount.created_date >= ' => $st_date);
        $mAHCnd[] = array('AccountFolder.created_date >= ' => $st_date);
        $mAVCnd[] = array('d.created_date >= ' => $st_date);
        $mAVWCnd[] = array('UserActivityLog.date_added >= ' => $st_date);
        $mATCnd[] = array('act.created_date >= ' => $st_date);
        if (!empty($end_date)) {
            $edate = explode('-', $end_date);
            $enDate = $edate[2] . '-' . $edate[0] . '-' . $edate[1];
            $en_date = date('Y-m-d', strtotime($enDate)) . ' 23:59:59';
        } else {
            $enDate = date_create(date('Y-m-d'));
            $en_date = date_format($enDate, 'Y-m-d') . ' 23:59:59';
        }

        $mAUCnd[] = array('UserAccount.created_date <= ' => $en_date);
        $mAHCnd[] = array('AccountFolder.created_date <= ' => $en_date);
        $mAVCnd[] = array('d.created_date <= ' => $en_date);
        $mAVWCnd[] = array('UserActivityLog.date_added <= ' => $en_date);
        $mATCnd[] = array('act.created_date <= ' => $en_date);
        if (!empty($search_by_standard)) {
            $mATCnd[] = array('act.account_tag_id IN( ' . $search_by_standard . ')');
        }

        if (isset($this->data['userType']) && $this->data['userType'] == "coach") {
            $mATCnd[] = array('afu.is_coach' => '1');
            $mATCnd[] = array('afu.user_id' => $user_id);
            $this->set('uType', $this->data['userType']);
        } elseif (isset($this->data['userType']) && $this->data['userType'] == "coachee") {
            $mATCnd[] = array('afu.is_mentee' => '1');
            $mATCnd[] = array('afu.user_id' => $user_id);
            $this->set('uType', $this->data['userType']);
        } else {
            $this->set('uType', 'all');
        }
        $start = (new DateTime($st_date))->modify('first day of this month');
        $end = (new DateTime($en_date))->modify('last day of this month');
        if ($durationGrph == 1)
            $interval = DateInterval::createFromDateString('1 year');
        if ($durationGrph == 2)
            $interval = DateInterval::createFromDateString('3 month');
        if ($durationGrph == 3)
            $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        $this->AccountTag->virtualFields['tags_date'] = 0;
        $this->AccountTag->virtualFields['total_tags'] = 0;
        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'act.ref_id = afd.document_id'
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = af.account_folder_id'
            ),
            array(
                'table' => 'account_folders_meta_data as afmd',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
            )
            ,
            array(
                'table' => 'account_folder_users as afu',
                'type' => 'inner',
                'conditions' => 'afu.account_folder_id = afmd.account_folder_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'AccountTag.account_id' => $account_id,
            'af.folder_type' => 1,
            'act.created_by' => $curr_user['User']['id'],
            'afmd.meta_data_name' => 'folder_type',
            'afmd.meta_data_value' => $folder_type,
            $mATCnd,
            $act_frm_wrk
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        if (empty($search_by_standard)) {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
                'limit' => 5
            );
        } else {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
            );
        }
        $account_tags_analytics = $this->AccountTag->find('all', $get_value);
//        echo $this->AccountTag->getLastQuery();
//        exit;
        $account_tags_analytics = Set::extract('/AccountTag/.', $account_tags_analytics);
        $acc_tags = array();
        $acc_tag_title = array();
        foreach ($account_tags_analytics as $actag) {
            $acc_tags[] = $actag['account_tag_id'];
            if (empty($actag['tads_code']))
                $acc_tag_title[] = $actag['tag_title'];
            else
                $acc_tag_title[] = $actag['tads_code'];
        }

        $graph_data_all = array();
        $graph_data = array();
        if ($durationGrph == 2)
            $m = $st_quarter;
        foreach ($period as $dt) {

            $g_data = $dt->format("Y-m-d");

            //$graph_data['date'] = $dt->format("Y-m-d");
            $duration = explode('-', $dt->format("Y-m-d"));
            $dCnd = array();
            $docCnd = array();
            if ($durationGrph == 1) {
                $graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('YEAR(act.created_date)' => $duration[0]);
                $docCnd[] = array('YEAR(d.created_date)' => $duration[0]);
            }
            if ($durationGrph == 2) {
                $v = $m % 4;
                if ($v == 0) {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                    $st_yr = $st_yr + 1;
                    $m = 0;
                } else {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                }
                //$graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $m++;
            }
            if ($durationGrph == 3) {
                $graph_data['date'] = $dt->format("M-y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
            }

            $account_coach_tags_analytics = $this->AccountTag->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'account_comment_tags as act',
                        'type' => 'inner',
                        'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'act.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders as af',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = af.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountTag.tag_type' => 0,
                    'AccountTag.account_id' => $account_id,
                    'AccountTag.account_tag_id' => $acc_tags,
                    'af.folder_type' => 1,
                    'act.created_by' => $curr_user['User']['id'],
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    $dCnd,
                    $act_frm_wrk
                ),
                'fields' => array(
                    'AccountTag.tag_title',
                    'AccountTag.account_tag_id',
                    'act.created_date AS AccountTag__tags_date',
                    'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                ),
                'group' => array('AccountTag.account_tag_id')
            ));

            $account_coach_tags_analytics = Set::extract('/AccountTag/.', $account_coach_tags_analytics);
            if (is_array($account_coach_tags_analytics) && count($account_coach_tags_analytics) > 0) {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $flag = false;
                    foreach ($account_coach_tags_analytics as $acta) {
                        if ($acc_tags[$i] == $acta['account_tag_id']) {
                            $graph_data['tag' . $i] = $acta['total_tags'];
                            $graph_data['tag_title_' . $i] = $acta['total_tags'];
                            $flag = true;
                        }
                    }
                    if ($flag == false)
                        $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }else {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }

            $account_video_analytics = $this->AccountFolder->find('count', array(
                'joins' => array(
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'AccountFolder.account_folder_id = afd.account_folder_id'
                    ),
                    array(
                        'table' => 'documents as d',
                        'type' => 'inner',
                        'conditions' => 'afd.document_id = d.id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folder_users as afu',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afu.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                    'AccountFolder.folder_type' => 1,
                    'd.doc_type' => array(1, 3),
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'afu.user_id' => $curr_user['User']['id'],
                    'AccountFolder.account_id' => $account_id,
                    $docCnd
                )
            ));
            $graph_data['total_sessions'] = $account_video_analytics;
            $graph_data_all[] = $graph_data;
            unset($graph_data);
        }
        $standards = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 0,
                $frm_wrk
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code", "tag_html")
        ));


        $standards_1 = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 0,
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code", "tag_html")
        ));

        $standards_array = array();
        if ($standards) {
            foreach ($standards as $st) {
                if (!empty($st['AccountTag']['tag_html'])) {
                    if (empty($st['AccountTag']['tads_code']))
                        $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                    else
                        $ac_tag_title = $st['AccountTag']['tads_code'];
                    $standard_array[] = array(
                        "value" => $st['AccountTag']['account_tag_id'],
                        "label" => $ac_tag_title
                    );
                }
            }
        }

        $standards_array_1 = array();
        if ($standards_1) {
            foreach ($standards_1 as $st) {
                if (!empty($st['AccountTag']['tag_html'])) {
                    if (empty($st['AccountTag']['tads_code']))
                        $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                    else
                        $ac_tag_title = $st['AccountTag']['tads_code'];
                    $standard_array_1[] = array(
                        "value" => $st['AccountTag']['account_tag_id'],
                        "label" => $ac_tag_title
                    );
                }
            }
        }


        $saved_standard_values = array();

        if (!empty($search_by_standard)) {
            $exploded_standard_values = explode(',', $search_by_standard);

            $count = 0;
            foreach ($exploded_standard_values as $exploded_standard_value) {
                foreach ($standard_array_1 as $row) {
                    if ($row['value'] == $exploded_standard_value) {
                        $saved_standard_values[$count]['value'] = $row['value'];
                        $saved_standard_values[$count]['label'] = $row['label'];
                        $count++;
                    }
                }
            }
        }



        if (isset($this->request->data['filter_check']) && $this->request->data['filter_check'] == 1) {
            $this->set('saved_standard_values', json_encode($saved_standard_values));
        } elseif (isset($this->request->data['filter_check']) && $this->request->data['filter_check'] == 2) {
            $this->set('saved_standard_values_1', json_encode($saved_standard_values));
        }

        $this->set('standards', json_encode($standard_array));
        $this->set('user_name', $curr_user['User']['first_name'] . ' ' . $curr_user['User']['last_name']);
        $this->set('account_id', $account_id);
        $this->set('user_id', $user_id);
        $this->set('duration', $durationGrph);
        $this->set('st_date', $start_date);
        $this->set('en_date', $end_date);
        $this->set('total_coaching_tags', json_encode($graph_data_all));
        $this->set('total_coaching_tags_eval', $this->_userGraphForEvaluationHuddle($user_id, $account_id, $start_date, $end_date, $durationGrph = 3, $folder_type = 2));
    }

    function userGraph_export($user_id, $account_id, $start_date = '', $end_date = '', $durationGrph = 3, $folder_type = 2) {
        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');
        $owner_account_id = $user['accounts']['account_id'];
        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $owner_account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];
        //echo $acct_duration_start;
        $mAUCnd = array();
        $mAHCnd = array();
        $mAVCnd = array();
        $mAVWCnd = array();
        $mATCnd = array();
        if (isset($this->data['durationGrph']) && !empty($this->data['durationGrph'])) {
            $durationGrph = $this->data['durationGrph'];
            $search_by_standard = $this->data['search_by_standards'];
        }
        $curr_user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $user_id
            ),
            'fields' => array('id', 'first_name', 'last_name')
        ));

        if (!empty($this->data['framework_id'])) {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id, 'account_tag_id' => $this->data['framework_id']
            )));
        } else {
            $frameworks = $this->AccountTag->find("all", array("conditions" => array(
                    "tag_type" => '2', "account_id" => $account_id
            )));
        }

        $frm_wrk = array();
        if (!empty($frameworks) && count($frameworks) > 0) {
            $this->set('frameworks', $frameworks);
        }

        if (!empty($this->data['framework_id'])) {
            $frm_wrk = array('framework_id' => $this->data['framework_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $this->data['framework_id']);
        } else {
            $frm_wrk = array('framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
            $act_frm_wrk = array('AccountTag.framework_id' => $frameworks[0]['AccountTag']['account_tag_id']);
        }

        if (!empty($start_date)) {
            $sdate = explode('-', $start_date);
            $stDate = $sdate[2] . '-' . $sdate[0] . '-' . $sdate[1];
            if ($sdate[0] >= $acct_duration_start) {
                $st_d = $sdate[0] - $acct_duration_start;
                if ($st_d >= 0 && $st_d < 3) {
                    $st_quarter = 1;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 3 && $st_d < 6) {
                    $st_quarter = 2;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 6 && $st_d < 9) {
                    $st_quarter = 3;
                    $st_yr = $sdate[2];
                }
                if ($st_d >= 9 && $st_d < 12) {
                    $st_quarter = 4;
                    $st_yr = $sdate[2];
                }
            } else {
                $st_d = $acct_duration_start - $sdate[0];
                if ($st_d >= 0 && $st_d < 3) {
                    $st_quarter = 4;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 3 && $st_d < 6) {
                    $st_quarter = 3;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 6 && $st_d < 9) {
                    $st_quarter = 2;
                    $st_yr = $sdate[2] - 1;
                }
                if ($st_d >= 9 && $st_d < 12) {
                    $st_quarter = 1;
                    $st_yr = $sdate[2] - 1;
                }
            }
            $st_date = date('Y-m-d', strtotime($stDate)) . ' 00:00:00';
        } else {
            $stDate = date_create(date('Y-m-d', strtotime('-3 month')));
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }

        $mAUCnd[] = array('UserAccount.created_date >= ' => $st_date);
        $mAHCnd[] = array('AccountFolder.created_date >= ' => $st_date);
        $mAVCnd[] = array('d.created_date >= ' => $st_date);
        $mAVWCnd[] = array('UserActivityLog.date_added >= ' => $st_date);
        $mATCnd[] = array('act.created_date >= ' => $st_date);
        if (!empty($end_date)) {
            $edate = explode('-', $end_date);
            $enDate = $edate[2] . '-' . $edate[0] . '-' . $edate[1];
            $en_date = date('Y-m-d', strtotime($enDate)) . ' 23:59:59';
        } else {
            $enDate = date_create(date('Y-m-d'));
            $en_date = date_format($enDate, 'Y-m-d') . ' 23:59:59';
        }

        $mAUCnd[] = array('UserAccount.created_date <= ' => $en_date);
        $mAHCnd[] = array('AccountFolder.created_date <= ' => $en_date);
        $mAVCnd[] = array('d.created_date <= ' => $en_date);
        $mAVWCnd[] = array('UserActivityLog.date_added <= ' => $en_date);
        $mATCnd[] = array('act.created_date <= ' => $en_date);
        if (!empty($search_by_standard)) {
            $mATCnd[] = array('act.account_tag_id IN( ' . $search_by_standard . ')');
        }

        if (isset($this->data['userType']) && $this->data['userType'] == "coach") {
            $mATCnd[] = array('afu.is_coach' => '1');
            $mATCnd[] = array('afu.user_id' => $user_id);
            $this->set('uType', $this->data['userType']);
        } elseif (isset($this->data['userType']) && $this->data['userType'] == "coachee") {
            $mATCnd[] = array('afu.is_mentee' => '1');
            $mATCnd[] = array('afu.user_id' => $user_id);
            $this->set('uType', $this->data['userType']);
        } else {
            $this->set('uType', 'all');
        }
        $start = (new DateTime($st_date))->modify('first day of this month');
        $end = (new DateTime($en_date))->modify('last day of this month');
        if ($durationGrph == 1)
            $interval = DateInterval::createFromDateString('1 year');
        if ($durationGrph == 2)
            $interval = DateInterval::createFromDateString('3 month');
        if ($durationGrph == 3)
            $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        $this->AccountTag->virtualFields['tags_date'] = 0;
        $this->AccountTag->virtualFields['total_tags'] = 0;
        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => 'act.ref_id = afd.document_id'
            ),
            array(
                'table' => 'account_folders as af',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = af.account_folder_id'
            ),
            array(
                'table' => 'account_folders_meta_data as afmd',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
            )
            ,
            array(
                'table' => 'account_folder_users as afu',
                'type' => 'inner',
                'conditions' => 'afu.account_folder_id = afmd.account_folder_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'AccountTag.account_id' => $account_id,
            'af.folder_type' => 1,
            'act.created_by' => $curr_user['User']['id'],
            'afmd.meta_data_name' => 'folder_type',
            'afmd.meta_data_value' => $folder_type,
            $mATCnd,
            $act_frm_wrk
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        if (empty($search_by_standard)) {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
                'limit' => 5
            );
        } else {
            $get_value = array(
                'joins' => $joins,
                'conditions' => $conditions,
                'fields' => $fields,
                'group' => array('AccountTag.account_tag_id'),
                'order' => array('AccountTag__total_tags' => 'desc'),
            );
        }
        $account_tags_analytics = $this->AccountTag->find('all', $get_value);
//        echo $this->AccountTag->getLastQuery();
//        exit;
        $account_tags_analytics = Set::extract('/AccountTag/.', $account_tags_analytics);
        $acc_tags = array();
        $acc_tag_title = array();
        foreach ($account_tags_analytics as $actag) {
            $acc_tags[] = $actag['account_tag_id'];
            if (empty($actag['tads_code']))
                $acc_tag_title[] = $actag['tag_title'];
            else
                $acc_tag_title[] = $actag['tads_code'];
        }

        $graph_data_all = array();
        $graph_data = array();
        if ($durationGrph == 2)
            $m = $st_quarter;
        foreach ($period as $dt) {

            $g_data = $dt->format("Y-m-d");

            //$graph_data['date'] = $dt->format("Y-m-d");
            $duration = explode('-', $dt->format("Y-m-d"));
            $dCnd = array();
            $docCnd = array();
            if ($durationGrph == 1) {
                $graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('YEAR(act.created_date)' => $duration[0]);
                $docCnd[] = array('YEAR(d.created_date)' => $duration[0]);
            }
            if ($durationGrph == 2) {
                $v = $m % 4;
                if ($v == 0) {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                    $st_yr = $st_yr + 1;
                    $m = 0;
                } else {
                    $graph_data['date'] = 'Q' . $m . '-' . $st_yr;
                }
                //$graph_data['date'] = $dt->format("Y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 3 MONTH');
                $m++;
            }
            if ($durationGrph == 3) {
                $graph_data['date'] = $dt->format("M-y");
                $dCnd[] = array('act.created_date >= "' . $g_data . '"');
                $dCnd[] = array('act.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
                $docCnd[] = array('d.created_date >= "' . $g_data . '"');
                $docCnd[] = array('d.created_date <= "' . $g_data . '" + INTERVAL 1 MONTH');
            }

            $account_coach_tags_analytics = $this->AccountTag->find('all', array(
                'joins' => array(
                    array(
                        'table' => 'account_comment_tags as act',
                        'type' => 'inner',
                        'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
                    ),
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'act.ref_id = afd.document_id'
                    ),
                    array(
                        'table' => 'account_folders as af',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = af.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountTag.tag_type' => 0,
                    'AccountTag.account_id' => $account_id,
                    'AccountTag.account_tag_id' => $acc_tags,
                    'af.folder_type' => 1,
                    'act.created_by' => $curr_user['User']['id'],
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    $dCnd,
                    $act_frm_wrk
                ),
                'fields' => array(
                    'AccountTag.tag_title',
                    'AccountTag.account_tag_id',
                    'act.created_date AS AccountTag__tags_date',
                    'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
                ),
                'group' => array('AccountTag.account_tag_id')
            ));

            $account_coach_tags_analytics = Set::extract('/AccountTag/.', $account_coach_tags_analytics);
            if (is_array($account_coach_tags_analytics) && count($account_coach_tags_analytics) > 0) {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $flag = false;
                    foreach ($account_coach_tags_analytics as $acta) {
                        if ($acc_tags[$i] == $acta['account_tag_id']) {
                            $graph_data['tag' . $i] = $acta['total_tags'];
                            $graph_data['tag_title_' . $i] = $acta['total_tags'];
                            $flag = true;
                        }
                    }
                    if ($flag == false)
                        $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }else {
                for ($i = 0; $i < count($acc_tags); $i++) {
                    $graph_data['tag' . $i] = 0;
                    $graph_data['tag_title_' . $i] = $acc_tag_title[$i];
                }
            }

            $account_video_analytics = $this->AccountFolder->find('count', array(
                'joins' => array(
                    array(
                        'table' => 'account_folder_documents as afd',
                        'type' => 'inner',
                        'conditions' => 'AccountFolder.account_folder_id = afd.account_folder_id'
                    ),
                    array(
                        'table' => 'documents as d',
                        'type' => 'inner',
                        'conditions' => 'afd.document_id = d.id'
                    ),
                    array(
                        'table' => 'account_folders_meta_data as afmd',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afmd.account_folder_id'
                    ),
                    array(
                        'table' => 'account_folder_users as afu',
                        'type' => 'inner',
                        'conditions' => 'afd.account_folder_id = afu.account_folder_id'
                    )
                ),
                'conditions' => array(
                    'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                    'AccountFolder.folder_type' => 1,
                    'd.doc_type' => array(1, 3),
                    'afmd.meta_data_name' => 'folder_type',
                    'afmd.meta_data_value' => $folder_type,
                    'afu.user_id' => $curr_user['User']['id'],
                    'AccountFolder.account_id' => $account_id,
                    $docCnd
                )
            ));
            $graph_data['total_sessions'] = $account_video_analytics;
            $graph_data_all[] = $graph_data;
            unset($graph_data);
        }
        $standards = $this->AccountTag->find("all", array(
            "conditions" => array(
                "account_id" => $account_id,
                "tag_type" => 0,
                $frm_wrk
            ),
            "fields" => array("account_tag_id", "tag_title", "tag_code", "tads_code")
        ));

        $standards_array = array();
        if ($standards) {
            foreach ($standards as $st) {
                if (empty($st['AccountTag']['tads_code']))
                    $ac_tag_title = $st['AccountTag']['tag_code'] . '-' . $st['AccountTag']['tag_title'];
                else
                    $ac_tag_title = $st['AccountTag']['tads_code'];
                $standard_array[] = array(
                    "value" => $st['AccountTag']['account_tag_id'],
                    "label" => $ac_tag_title
                );
            }
        }






        $fileName = 'GRAPH EXPORT_' . date('m-d-Y') . '.xlsx';
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                ->setLastModifiedBy("Maarten Balliauw")
                ->setTitle("PHPExcel Test Document")
                ->setSubject("PHPExcel Test Document")
                ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                ->setKeywords("office PHPExcel php")
                ->setCategory("Test result file");


        if ($this->data['coaching_eval'] == '1') {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B1', "Evidence of standards in " . $this->data['user_name'] . "'s Coaching Huddle video sessions")

            ;
        } else {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B1', 'Frequency of Tagged Standards and ratings for Assessment Huddles.')

            ;
        }



        $count = 0;
        $letter = 'B';
        $line = 3;








        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue($letter . $line, 'DATE')

        ;


        $letter++;

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue($letter . $line, 'Total Sessions')

        ;


        $letter++;
        $count = 0;


        while ($count < count($graph_data_all[0])) {
            if (isset($graph_data_all[0]['tag_title_' . $count])) {
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($letter . $line, $graph_data_all[0]['tag_title_' . $count])

                ;
                $letter++;
            }
            $count++;
        }


        $count = 0;
        $line++;
        $letter = 'B';



        while ($count < count($graph_data_all)) {
            $letter = 'B';

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($letter . $line, $graph_data_all[$count]['date'])

            ;

            $letter++;

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($letter . $line, $graph_data_all[$count]['total_sessions'])

            ;

            $letter++;


            $count1 = 0;

            while ($count1 < count($graph_data_all[$count])) {
                if (isset($graph_data_all[$count]['tag' . $count1])) {
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue($letter . $line, $graph_data_all[$count]['tag' . $count1])

                    ;

                    $letter++;
                }
                $count1++;
            }





            $line = $line + 2;
            $count++;
        }







        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=$fileName");
        $objWriter->save('php://output');




        exit;
    }

    function userDetail($user_id, $account_id, $start_date = '', $end_date = '', $type = '', $workspace_huddle_library = 0) {
        return $this->redirect('/analytics_angular/details/' . $user_id . '/' . $account_id . '/' . $start_date . '/' . $end_date . '/' . $type . '/' . $workspace_huddle_library, 301);

        $loggedInUser = $this->Session->read('user_current_account');
        $user = $this->Session->read('user_current_account');

        if (isset($this->data['startDate']) && !empty($this->data['startDate'])) {
            $start_date = $this->data['startDate'];
        }
        if (isset($this->data['endDate']) && !empty($this->data['endDate'])) {
            $end_date = $this->data['endDate'];
        }
        $mAUCndSLog = array();
        $mAUCndELog = array();

        $curr_user = $this->User->find('first', array(
            'conditions' => array(
                'id' => $user_id
            ),
            'fields' => array('id', 'username', 'first_name', 'last_name')
        ));

        if (!empty($start_date)) {
            $start_date = str_replace('-', '/', $start_date);
            $stDate = date_create($start_date);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        } else {
            $stDate = date_create(date('Y-m-d', strtotime('-3 month')));
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }
        $mAUCndSLog = ' AND date_added >="' . $st_date . '"';
        $mAVWCnd[] = array('date_added >= ' => $st_date);

        if (!empty($end_date)) {
            $end_date = str_replace('-', '/', $end_date);
            $endDate = date_create($end_date);
            $en_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
        } else {
            $enDate = date_create(date('Y-m-d'));
            $en_date = date_format($enDate, 'Y-m-d') . ' 23:59:59';
        }
        $mAUCndELog = ' AND date_added <="' . $en_date . '"';
        $mAVWCnd[] = array('date_added <= ' => $en_date);

        if (!empty($type)) {
            if ($type == 4 || $type == 2) {
                if ($workspace_huddle_library == 1) {
                    $this->set('huddles_video_uploaded', true);
                } elseif ($workspace_huddle_library == 2) {
                    $this->set('library_video_uploaded', true);
                } elseif ($workspace_huddle_library == 3) {
                    $this->set('workspace_video_uploaded', true);
                }

                $types = '(ual.type = 2 OR ual.type = 4)';
            } else {
                if ($type == 1)
                    $this->set('huddle_created', true);

                if ($type == 11) {
                    if ($workspace_huddle_library == 1) {
                        $this->set('huddles_video_viewed', true);
                    } elseif ($workspace_huddle_library == 2) {
                        $this->set('library_video_viewed', true);
                    } elseif ($workspace_huddle_library == 3) {
                        $this->set('workspace_video_viewed', true);
                    }
                }
                if ($type == 5) {
                    if ($workspace_huddle_library == 1) {
                        $this->set('huddles_comment_uploaded', true);
                    } elseif ($workspace_huddle_library == 3) {
                        $this->set('workspace_comment_uploaded', true);
                    }
                }
                if ($type == 22) {
                    if ($workspace_huddle_library == 1) {
                        $this->set('huddles_video_shared', true);
                    } elseif ($workspace_huddle_library == 2) {
                        $this->set('library_video_shared', true);
                    }
                }
                if ($type == 3)
                    $this->set('document_uploaded', true);
                if ($type == 8)
                    $this->set('reply_comment', true);
                if ($type == 13)
                    $this->set('document_viewed', true);
                if ($type == 9)
                    $this->set('logged_in', true);

                if ($type == 23)
                    $this->set('scripted_observations', true);

                if ($type == 20)
                    $this->set('scripted_video_observations', true);

                if ($type == 24)
                    $this->set('total_video_hours_uploaded', true);
                if ($type == 25)
                    $this->set('total_video_hours_viewed', true);

                $types = 'ual.type = ' . $type;
            }

            if ($workspace_huddle_library == 1 && $type == 4) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` FROM user_activity_logs ual
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = ual.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($workspace_huddle_library == 2 && ($type == 4 || $type == 2)) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` FROM user_activity_logs ual
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = ual.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 2 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($workspace_huddle_library == 3 && $type == 4) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` FROM user_activity_logs ual
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = ual.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 3 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($workspace_huddle_library == 1 && ($type == 11 || $type == 22 )) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
    ON afd.document_id = ual.ref_id
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($workspace_huddle_library == 2 && ($type == 11 || $type == 22 )) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
    ON afd.document_id = ual.ref_id
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 2 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($workspace_huddle_library == 3 && $type == 11) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
    ON afd.document_id = ual.ref_id
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 3 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($workspace_huddle_library == 1 && $type == 5) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`, comments.`comment` FROM user_activity_logs ual join comments on ual.ref_id = comments.id join account_folders on ual.account_folder_id = account_folders.account_folder_id WHERE ual.account_id = " . $account_id . " AND account_folders.`folder_type` = 1 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($workspace_huddle_library == 3 && $type == 5) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` , comments.`comment` FROM user_activity_logs ual join comments on ual.ref_id = comments.id join account_folders on ual.account_folder_id = account_folders.account_folder_id WHERE ual.account_id = " . $account_id . " AND account_folders.`folder_type` = 3 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($type == 8) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` , comments.`comment` FROM user_activity_logs ual join comments on ual.ref_id = comments.id  WHERE ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($type == 23) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`  FROM user_activity_logs ual LEFT JOIN account_folder_documents afd
                                                        ON ual.`ref_id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN documents d
                                                        ON (d.id = ual.ref_id)  WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND d.is_associated = 1  AND d.current_duration is NULL AND d.published = 1 AND d.doc_type = 3  AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($type == 20) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`  FROM user_activity_logs ual LEFT JOIN account_folder_documents afd
                                                        ON ual.`ref_id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN documents d
                                                        ON (d.id = ual.ref_id)  WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND d.is_associated > 0  AND d.current_duration is NOT NULL AND d.published = 1 AND d.doc_type = 3  AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            } elseif ($type == 24) {
                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` , df.`duration`  FROM user_activity_logs ual INNER JOIN documents d
                                                        ON ual.`ref_id`= d.`id`
                                                        INNER JOIN document_files df
                                                        ON d.`id` = df.`document_id`
                                                        WHERE d.account_id = " . $account_id . " AND d.created_by = " . $user_id . " AND ual.type IN (2,4) AND d.recorded_date >= '$st_date' and d.recorded_date <= '$en_date' ");
            } elseif ($type == 25) {
                $all_users = $this->UserAccount->query("SELECT dvh.document_id , dvh.minutes_watched  FROM users us LEFT JOIN document_viewer_histories dvh
                                                        ON dvh.`user_id`= us.`id`
                                                        INNER JOIN account_folder_documents afd
                                                        ON afd.`document_id` = dvh.`document_id`
                                                        WHERE dvh.account_id = " . $account_id . " AND dvh.user_id = " . $user_id . "  AND dvh.created_date >= '$st_date' and dvh.created_date <= '$en_date' ");
            } else {

                $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` FROM user_activity_logs ual WHERE ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
            }
        } else {
            $filters = '';
            $types = array();
            if (isset($this->data['huddle_created'])) {
                $types[] = $this->data['huddle_created'];
                $this->set('huddle_created', true);
            }
            if (isset($this->data['huddles_video_uploaded'])) {
                //      $types[] = $this->data['huddles_video_uploaded'];
                $types[] = 201;
                $this->set('huddles_video_uploaded', true);
            }

            if (isset($this->data['library_video_uploaded'])) {
                //     $types[] = $this->data['library_video_uploaded'];
                $types[] = 202;
                $this->set('library_video_uploaded', true);
            }

            if (isset($this->data['workspace_video_uploaded'])) {
                //       $types[] = $this->data['workspace_video_uploaded'];
                $types[] = 203;
                $this->set('workspace_video_uploaded', true);
            }

            if (isset($this->data['document_uploaded'])) {
                $types[] = $this->data['document_uploaded'];
                $this->set('document_uploaded', true);
            }
            if (isset($this->data['huddles_comment_uploaded'])) {
                //  $types[] = $this->data['huddles_comment_uploaded'];
                $types[] = 501;
                $this->set('huddles_comment_uploaded', true);
            }

            if (isset($this->data['workspace_comment_uploaded'])) {
                //$types[] = $this->data['workspace_comment_uploaded'];
                $types[] = 503;
                $this->set('workspace_comment_uploaded', true);
            }

            if (isset($this->data['reply_comment'])) {
                $types[] = $this->data['reply_comment'];
                $this->set('reply_comment', true);
            }
            if (isset($this->data['logged_in'])) {
                $types[] = $this->data['logged_in'];
                $this->set('logged_in', true);
            }
            if (isset($this->data['huddles_video_viewed'])) {
                //   $types[] = $this->data['huddles_video_viewed'];
                $types[] = 1101;
                $this->set('huddles_video_viewed', true);
            }

            if (isset($this->data['library_video_viewed'])) {
                //   $types[] = $this->data['library_video_viewed'];
                $types[] = 1102;
                $this->set('library_video_viewed', true);
            }

            if (isset($this->data['workspace_video_viewed'])) {
//                $types[] = $this->data['workspace_video_viewed'];
                $types[] = 1103;
                $this->set('workspace_video_viewed', true);
            }

            if (isset($this->data['access_huddle'])) {
                $types[] = $this->data['access_huddle'];
                $this->set('access_huddle', true);
            }
            if (isset($this->data['document_viewed'])) {
                $types[] = $this->data['document_viewed'];
                $this->set('document_viewed', true);
            }
            if (isset($this->data['standard_added'])) {
                $types[] = $this->data['standard_added'];
                $this->set('standard_added', true);
            }
            if (isset($this->data['tag_added'])) {
                $types[] = $this->data['tag_added'];
                $this->set('tag_added', true);
            }

            if (isset($this->data['scripted_observations'])) {
                $types[] = $this->data['scripted_observations'];
                $this->set('scripted_observations', true);
            }

            if (isset($this->data['scripted_video_observations'])) {
                $types[] = $this->data['scripted_video_observations'];
                $this->set('scripted_video_observations', true);
            }

            if (isset($this->data['total_video_hours_uploaded'])) {
                $types[] = $this->data['total_video_hours_uploaded'];
                $this->set('total_video_hours_uploaded', true);
            }

            if (isset($this->data['total_video_hours_viewed'])) {
                $types[] = $this->data['total_video_hours_viewed'];
                $this->set('total_video_hours_viewed', true);
            }



            if (isset($this->data['device_info'])) {
                $types[] = $this->data['device_info'];
                $this->set('device_info', true);
            }

            if (isset($this->data['huddles_video_shared'])) {
                //    $types[] = $this->data['huddles_video_shared'];
                $types[] = 2201;
                $this->set('huddles_video_shared', true);
            }

            if (isset($this->data['library_video_shared'])) {
                //    $types[] = $this->data['library_video_shared'];
                $types[] = 2202;
                $this->set('library_video_shared', true);
            }

            $type_string = '';
            $type_array = array();
            foreach ($types as $key => $type_in) {
                if ($type_in != 201 && $type_in != 202 && $type_in != 203 && $type_in != 1101 && $type_in != 1102 && $type_in != 1103 && $type_in != 2201 && $type_in != 2202 && $type_in != 501 && $type_in != 503 && $type_in != 4 && $type_in != 11 && $type_in != 22 && $type_in != 5 && $type_in != 2 && $type_in != 8 && $type_in != 23 && $type_in != 20) {
                    $type_array[] = $type_in;
                }
            }

            $type_string = implode(',', $type_array);


            // echo $type_string;die;
//            $all_users = $this->UserActivityLog->find('all', array(
//                'conditions' => array(
//                    'user_id' => $user_id,
//                    'account_id' => $account_id,
//                    'type' => $types,
//                    $mAVWCnd
//                ),
//                'fields' => array('desc', 'url', 'date_added', 'type', 'account_folder_id', 'ref_id')
//            ));
            $all_users = array();
            foreach ($types as $type_in) {
                if ($type_in == 201) {
                    $types = '(ual.type = 2 OR ual.type = 4)';
                    $all_users_1 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` FROM user_activity_logs ual
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = ual.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $all_users = array_merge($all_users_1, $all_users);
                } elseif ($type_in == 202) {
                    $types = '(ual.type = 2 OR ual.type = 4)';
                    $all_users_2 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type`  FROM user_activity_logs ual
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = ual.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 2 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");

                    $all_users = array_merge($all_users_2, $all_users);
                } elseif ($type_in == 203) {
                    $types = '(ual.type = 2 OR ual.type = 4)';
                    $all_users_3 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type`  FROM user_activity_logs ual
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = ual.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 3 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $all_users = array_merge($all_users_3, $all_users);
                } elseif ($type_in == 501) {
                    $types = '(ual.type = 5)';
                    $all_users_4 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` , comments.`comment`  FROM user_activity_logs ual join comments on ual.ref_id = comments.id join account_folders on ual.account_folder_id = account_folders.account_folder_id WHERE ual.account_id = " . $account_id . " AND account_folders.`folder_type` = 1 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $all_users = array_merge($all_users_4, $all_users);
                } elseif ($type_in == 503) {
                    $types = '(ual.type = 5)';
                    $all_users_5 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` , comments.`comment`  FROM user_activity_logs ual join comments on ual.ref_id = comments.id join account_folders on ual.account_folder_id = account_folders.account_folder_id WHERE ual.account_id = " . $account_id . " AND account_folders.`folder_type` = 3 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $all_users = array_merge($all_users_5, $all_users);
                } elseif ($type_in == 1101) {
                    $types = '(ual.type = 11)';
                    $all_users_6 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
    ON afd.document_id = ual.ref_id
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $all_users = array_merge($all_users_6, $all_users);
                } elseif ($type_in == 1102) {
                    $types = '(ual.type = 11)';
                    $all_users_7 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
    ON afd.document_id = ual.ref_id
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 2 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $all_users = array_merge($all_users_7, $all_users);
                } elseif ($type_in == 1103) {
                    $types = '(ual.type = 11)';
                    $all_users_8 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
    ON afd.document_id = ual.ref_id
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 3 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $all_users = array_merge($all_users_8, $all_users);
                } elseif ($type_in == 2201) {
                    $types = '(ual.type = 22)';
                    $all_users_9 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
    ON afd.document_id = ual.ref_id
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $all_users = array_merge($all_users_9, $all_users);
                } elseif ($type_in == 2202) {
                    $types = '(ual.type = 22)';
                    $all_users_10 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
    ON afd.document_id = ual.ref_id
  LEFT JOIN account_folders AS af
    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 2 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $all_users = array_merge($all_users_10, $all_users);
                } elseif ($type_in == 8) {
                    $types = '(ual.type = 8)';
                    $all_users_11 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` , comments.`comment` FROM user_activity_logs ual join comments on ual.ref_id = comments.id  WHERE ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");

                    $all_users = array_merge($all_users_11, $all_users);
                } elseif ($type_in == 23) {
                    $types = '(ual.type = 23)';
                    $all_users_13 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual LEFT JOIN account_folder_documents afd
                                                        ON ual.`ref_id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN documents d
                                                        ON (d.id = ual.ref_id)  WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND d.is_associated = 1  AND d.current_duration is NULL AND d.published = 1 AND d.doc_type = 3  AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");

                    $all_users = array_merge($all_users_13, $all_users);
                } elseif ($type_in == 20) {
                    $types = '(ual.type = 20)';
                    $all_users_14 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual LEFT JOIN account_folder_documents afd
                                                        ON ual.`ref_id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN documents d
                                                        ON (d.id = ual.ref_id)  WHERE ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND d.is_associated > 0  AND d.current_duration is NOT NULL AND d.published = 1 AND d.doc_type = 3  AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");

                    $all_users = array_merge($all_users_14, $all_users);
                } elseif ($type_in == 24) {
                    $all_users_15 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` , df.`duration`  FROM user_activity_logs ual INNER JOIN documents d
                                                        ON ual.`ref_id`= d.`id`
                                                        INNER JOIN document_files df
                                                        ON d.`id` = df.`document_id`
                                                      WHERE d.account_id = " . $account_id . " AND d.created_by = " . $user_id . " AND ual.type IN (2,4) AND d.recorded_date >= '$st_date' and d.recorded_date <= '$en_date' ");
                    $all_user_15_new = array();
                    $array_count = 0;
                    foreach ($all_users_15 as $user) {
                        $all_user_15_new[] = $user;
                        $all_user_15_new[$array_count]['ual']['type'] = 24;
                        $array_count++;
                    }

                    $all_users = array_merge($all_user_15_new, $all_users);
                    //    print_r($all_users);die;
                } elseif ($type_in == 25) {
                    $all_users_16 = $this->UserAccount->query("SELECT dvh.document_id , dvh.minutes_watched  FROM users us LEFT JOIN document_viewer_histories dvh
                                                        ON dvh.`user_id`= us.`id`
                                                        INNER JOIN account_folder_documents afd
                                                        ON afd.`document_id` = dvh.`document_id`
                                                        WHERE dvh.account_id = " . $account_id . " AND dvh.user_id = " . $user_id . "  AND dvh.created_date >= '$st_date' and dvh.created_date <= '$en_date' ");
                    $all_user_16_new = array();
                    $array_count = 0;
                    foreach ($all_users_16 as $user) {
                        $all_user_16_new[] = $user;
                        $all_user_16_new[$array_count]['ual']['type'] = 25;
                        $array_count++;
                    }

                    $all_users = array_merge($all_user_16_new, $all_users);
                    // print_r($all_users);die;
                }






                //   $all_users = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` FROM user_activity_logs ual WHERE ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . " AND ual.type IN (".$type_string.") " . $mAUCndSLog . $mAUCndELog . "");
            }

//            print_r($all_users);die;

            if (!empty($type_string)) {
                $all_users_12 = $this->UserAccount->query("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual WHERE ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . " AND ual.type IN (" . $type_string . ") " . $mAUCndSLog . $mAUCndELog . "");
                $all_users = array_merge($all_users_12, $all_users);
            }


            //  print_r($all_users);die;
//            else
//            {
//                $all_users = $this->UserActivityLog->find('all', array(
//                'conditions' => array(
//                    'user_id' => $user_id,
//                    'account_id' => $account_id,
//                    'type' => $types,
//                    $mAVWCnd
//                ),
//                'fields' => array('desc', 'url', 'date_added', 'type', 'account_folder_id', 'ref_id')
//            ));
//
//            }
        }

        $all_user_detail = array();
        if (!empty($type)) {
            foreach ($all_users as $allusr) {
                if ($type == 1) {
                    $allusr['ual']['action_type'] = 'Huddle Created';
                    $doc_info = $this->UserAccount->query("SELECT af.* FROM account_folders af WHERE af.account_folder_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    } else {
                        $allusr['ual']['url'] = $allusr['ual']['desc'];
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($type == 2) {
                    $allusr['ual']['action_type'] = 'Video Uploaded';
                    //   $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    //  $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['ual']['coach'] = $coach;
//                            $allusr['ual']['coachee'] = $coachee;
//                            $allusr['ual']['url'] = '';
//                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
//                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
//                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));

                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($type == 3) {
                    $allusr['ual']['action_type'] = 'Document Uploaded';
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title FROM account_folder_documents afd LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];

                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($type == 4) {
                    $allusr['ual']['action_type'] = 'Video Uploaded';
                    // $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    //  $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ual']['ref_id']);
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['ual']['coach'] = $coach;
//                            $allusr['ual']['coachee'] = $coachee;
//                            $allusr['ual']['url'] = '';
//                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
//                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
//                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));

                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($type == 5) {
                    $allusr['ual']['action_type'] = 'Comment Added';
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM comments c LEFT JOIN account_folder_documents afd ON c.ref_id = afd.document_id LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE af.account_folder_id =" . $allusr['ual']['account_folder_id']);
                    /* $doc_info = $this->UserAccount->query("SELECT af.* FROM account_folders af WHERE af.account_folder_id = ".$allusr['ual']['account_folder_id']); */

                    $found_comment = $this->Comment->find('all', array('conditions' => array('id' => $allusr['ual']['ref_id'])));
                    if (sizeof($found_comment) == 0)
                        continue;

                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($type == 8) {
                    $allusr['ual']['action_type'] = 'Reply to Comment';
                    $comment_details = $this->Comment->find("first", array("conditions" => array(
                            "id" => $allusr['ual']['ref_id']
                    )));
                    $account_folder_id_data = $this->AccountFolderDocument->find("first", array("conditions" => array(
                            "document_id" => $comment_details['Comment']['ref_id']
                    )));
                    if (!empty($account_folder_id_data['AccountFolderDocument']['account_folder_id'])) {
                        $doc_info = $this->UserAccount->query("SELECT af.* FROM account_folders af WHERE af.account_folder_id = " . $account_folder_id_data['AccountFolderDocument']['account_folder_id']);
                    } else {
                        $doc_info = '';
                    }
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    } else {
                        $allusr['ual']['url'] = $allusr['comments']['comment'];
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($type == 9) {
                    $allusr['ual']['action_type'] = 'Logged In';
                    $allusr['ual']['url'] = '';
                    $allusr['ual']['desc'] = '';
                    $allusr['ual']['video'] = '';
                    $allusr['ual']['coach'] = '';
                    $allusr['ual']['coachee'] = '';
                    $allusr['ual']['session_date'] = '';
                }
                if ($type == 11) {
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    $allusr['ual']['action_type'] = 'Video Viewed';
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = $allusr['ual']['desc'];
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($type == 12) {
                    $allusr['ual']['action_type'] = 'Huddle Accessed';
                    $doc_info = $this->UserAccount->query("SELECT af.* FROM account_folders af WHERE af.account_folder_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    } else {
                        $allusr['ual']['url'] = $allusr['ual']['desc'];
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($type == 13) {
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title FROM account_folder_documents afd LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    $allusr['ual']['action_type'] = 'Document Viewed';
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    } else {
                        $doc_info = $this->UserAccount->query("SELECT af.*,d.original_file_name FROM comment_attachments ca LEFT JOIN comments c ON ca.comment_id = c.id LEFT JOIN documents d ON ca.document_id = d.id LEFT JOIN account_folders af ON c.ref_id = af.account_folder_id WHERE ca.document_id = " . $allusr['ual']['ref_id']);
                        if (isset($doc_info[0])) {
                            if ($doc_info[0]['af']['folder_type'] == 1) {
                                $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                                $coach_arr = array();
                                $coachee_arr = array();
                                $coach = '';
                                $coachee = '';
                                if (isset($huddle_info[0])) {
                                    $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                    foreach ($usr_info as $invitedUsers) {
                                        if ($invitedUsers['afu']['role_id'] == 200) {
                                            $coach_arr[] = $invitedUsers[0]['name'];
                                        }
                                        if ($invitedUsers['afu']['role_id'] == 210) {
                                            $coachee_arr[] = $invitedUsers[0]['name'];
                                        }
                                    }
                                    $coach = implode(',', $coach_arr);
                                    $coachee = implode(',', $coachee_arr);
                                }
                                $allusr['ual']['coach'] = $coach;
                                $allusr['ual']['coachee'] = $coachee;
                                $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                                $allusr['ual']['url'] = '';
                                $allusr['ual']['video'] = $doc_info[0]['d']['original_file_name'];
                                $allusr['ual']['session_date'] = '';
                            }
                        }
                    }
                }
                if ($type == 14) {
                    $allusr['ual']['action_type'] = 'Tag Added';
                    $allusr['ual']['url'] = $allusr['ual']['desc'];
                    $allusr['ual']['desc'] = '';
                    $allusr['ual']['video'] = '';
                    $allusr['ual']['coach'] = '';
                    $allusr['ual']['coachee'] = '';
                    $allusr['ual']['session_date'] = '';
                }
                if ($type == 15) {
                    $allusr['ual']['action_type'] = 'Standard Added';
                    $allusr['ual']['url'] = $allusr['ual']['desc'];
                    $allusr['ual']['desc'] = '';
                    $allusr['ual']['video'] = '';
                    $allusr['ual']['coach'] = '';
                    $allusr['ual']['coachee'] = '';
                    $allusr['ual']['session_date'] = '';
                }
                if ($type == 16) {
                    $allusr['ual']['action_type'] = 'Device Information';
                    $allusr['ual']['url'] = $allusr['ual']['desc'];
                    $allusr['ual']['desc'] = '';
                    $allusr['ual']['video'] = '';
                    $allusr['ual']['coach'] = '';
                    $allusr['ual']['coachee'] = '';
                    $allusr['ual']['session_date'] = '';
                }
                if ($type == 22) {
                    $allusr['ual']['action_type'] = 'Video Shared';
                    $allusr['ual']['url'] = $allusr['ual']['desc'];
                    $allusr['ual']['desc'] = '';
                    $allusr['ual']['video'] = '';
                    $allusr['ual']['coach'] = '';
                    $allusr['ual']['coachee'] = '';
                    $allusr['ual']['session_date'] = '';
                }

                if ($type == 23) {


                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);


                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }



                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['action_type'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];


                            $allusr['ual']['url'] = $allusr['ual']['url'];
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    }
                }


                if ($type == 20) {


                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);


                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }



                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['action_type'] = 'Scripted Video Notes';
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];


                            $allusr['ual']['url'] = $allusr['ual']['url'];
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    }
                }

                if ($type == 24) {
                    $allusr['ual']['action_type'] = 'Video Hours Uploaded';
                    // $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    //  $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ual']['ref_id']);
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['ual']['coach'] = $coach;
//                            $allusr['ual']['coachee'] = $coachee;
//                            $allusr['ual']['url'] = '';
//                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
//                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
//                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));

                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['df']['duration'] / 60, 2);
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['df']['duration'] / 60, 2);
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['df']['duration'] / 60, 2);
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }

                if ($type == 25) {
                    $allusr['ual']['action_type'] = 'Video Hours Viewed';
                    // $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    //  $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ual']['ref_id']);
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['dvh']['document_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['ual']['coach'] = $coach;
//                            $allusr['ual']['coachee'] = $coachee;
//                            $allusr['ual']['url'] = '';
//                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
//                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
//                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['date_added'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['dvh']['minutes_watched'] / 60, 2);
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['date_added'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['dvh']['minutes_watched'] / 60, 2);
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['date_added'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['dvh']['minutes_watched'] / 60, 2);
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }



                $allusr['ual']['username'] = $curr_user['User']['first_name'] . ' ' . $curr_user['User']['last_name'];
                $allusr['ual']['date_added'] = @date('Y-m-d h:i A', strtotime($allusr['ual']['date_added']));
                $all_user_detail[] = $allusr['ual'];
            }
        } else {
            foreach ($all_users as $allusr) {
                if ($allusr['ual']['type'] == 1) {
                    $allusr['ual']['action_type'] = 'Huddle Created';
                    $doc_info = $this->UserAccount->query("SELECT af.* FROM account_folders af WHERE af.account_folder_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($allusr['ual']['type'] == 2) {
                    $allusr['ual']['action_type'] = 'Video Uploaded';
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['video'] = $allusr['ual']['desc'];
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($allusr['ual']['type'] == 3) {
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title FROM account_folder_documents afd LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    $allusr['ual']['action_type'] = 'Document Uploaded';
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($allusr['ual']['type'] == 4) {
                    $allusr['ual']['action_type'] = 'Video Uploaded';
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($allusr['ual']['type'] == 5) {
                    $allusr['ual']['action_type'] = 'Comment Added';
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM comments c LEFT JOIN account_folder_documents afd ON c.ref_id = afd.document_id LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE af.account_folder_id =" . $allusr['ual']['account_folder_id']);
                    /* $doc_info = $this->UserAccount->query("SELECT af.* FROM account_folders af WHERE af.account_folder_id = ".$allusr['ual']['account_folder_id']); */
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($allusr['ual']['type'] == 8) {
                    $allusr['ual']['action_type'] = 'Reply to Comment';
                    /* $doc_info = $this->UserAccount->query("SELECT af.* FROM account_folders af WHERE af.account_folder_id = ".$allusr['ual']['account_folder_id']); */
                    //$doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM comments c LEFT JOIN account_folder_documents afd ON c.ref_id = afd.document_id LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE af.account_folder_id =" . $allusr['ual']['account_folder_id']);
                    $comment_details = $this->Comment->find("first", array("conditions" => array(
                            "id" => $allusr['ual']['ref_id']
                    )));
                    $account_folder_id_data = $this->AccountFolderDocument->find("first", array("conditions" => array(
                            "document_id" => $comment_details['Comment']['ref_id']
                    )));

                    if (!empty($account_folder_id_data['AccountFolderDocument']['account_folder_id'])) {
                        $doc_info = $this->UserAccount->query("SELECT af.* FROM account_folders af WHERE af.account_folder_id = " . $account_folder_id_data['AccountFolderDocument']['account_folder_id']);
                    } else {
                        $doc_info = '';
                    }
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = $allusr['comments']['comment'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = $allusr['comments']['comment'];
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($allusr['ual']['type'] == 9) {
                    $allusr['ual']['action_type'] = 'Logged In';
                    $allusr['ual']['url'] = '';
                    $allusr['ual']['desc'] = '';
                    $allusr['ual']['video'] = '';
                    $allusr['ual']['coach'] = '';
                    $allusr['ual']['coachee'] = '';
                    $allusr['ual']['session_date'] = '';
                }
                if ($allusr['ual']['type'] == 11) {
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    $allusr['ual']['action_type'] = 'Video Viewed';
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($allusr['ual']['type'] == 12) {
                    $allusr['ual']['action_type'] = 'Huddle Accessed';
//                    $doc_info = $this->UserAccount->query("SELECT af.* FROM account_folders af WHERE af.account_folder_id = " . $allusr['ual']['ref_id']);
//                    if (isset($doc_info[0])) {
//                        if ($doc_info[0]['af']['folder_type'] == 1) {
//                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
//                            $coach_arr = array();
//                            $coachee_arr = array();
//                            $coach = '';
//                            $coachee = '';
//                            if (isset($huddle_info[0])) {
//                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
//
//                                foreach ($usr_info as $invitedUsers) {
//                                    if ($invitedUsers['afu']['role_id'] == 200) {
//                                        $coach_arr[] = $invitedUsers[0]['name'];
//                                    }
//                                    if ($invitedUsers['afu']['role_id'] == 210) {
//                                        $coachee_arr[] = $invitedUsers[0]['name'];
//                                    }
//                                }
//                                $coach = implode(',', $coach_arr);
//                                $coachee = implode(',', $coachee_arr);
//                            }
//                            $allusr['ual']['coach'] = $coach;
//                            $allusr['ual']['coachee'] = $coachee;
//                            $allusr['ual']['url'] = '';
//                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
//                            $allusr['ual']['video'] = '';
//                            $allusr['ual']['session_date'] = '';
//                        }
//                        if ($doc_info[0]['af']['folder_type'] == 2) {
//                            $allusr['ual']['url'] = '';
//                            $allusr['ual']['desc'] = 'Video Library';
//                            $allusr['ual']['video'] = '';
//                            $allusr['ual']['coach'] = '';
//                            $allusr['ual']['coachee'] = '';
//                            $allusr['ual']['session_date'] = '';
//                        }
//                        if ($doc_info[0]['af']['folder_type'] == 3) {
//                            $allusr['ual']['url'] = '';
//                            $allusr['ual']['desc'] = '';
//                            $allusr['ual']['workspace'] = 'My Workspace';
//                            $allusr['ual']['video'] = '';
//                            $allusr['ual']['coach'] = '';
//                            $allusr['ual']['coachee'] = '';
//                            $allusr['ual']['session_date'] = '';
//                        }
//                    } else {
//                        $allusr['ual']['url'] = '';
//                        $allusr['ual']['desc'] = '';
//                        $allusr['ual']['video'] = '';
//                        $allusr['ual']['coach'] = '';
//                        $allusr['ual']['coachee'] = '';
//                        $allusr['ual']['session_date'] = '';
//                    }
                }
                if ($allusr['ual']['type'] == 13) {
                    $allusr['ual']['action_type'] = 'Document Viewed';
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title FROM account_folder_documents afd LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);

                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    } else {
                        $doc_info = $this->UserAccount->query("SELECT af.*,d.original_file_name FROM comment_attachments ca LEFT JOIN comments c ON ca.comment_id = c.id LEFT JOIN documents d ON ca.document_id = d.id LEFT JOIN account_folders af ON c.ref_id = af.account_folder_id WHERE ca.document_id = " . $allusr['ual']['ref_id']);
                        if (isset($doc_info[0])) {
                            if ($doc_info[0]['af']['folder_type'] == 1) {
                                $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                                $coach_arr = array();
                                $coachee_arr = array();
                                $coach = '';
                                $coachee = '';
                                if (isset($huddle_info[0])) {
                                    $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                    foreach ($usr_info as $invitedUsers) {
                                        if ($invitedUsers['afu']['role_id'] == 200) {
                                            $coach_arr[] = $invitedUsers[0]['name'];
                                        }
                                        if ($invitedUsers['afu']['role_id'] == 210) {
                                            $coachee_arr[] = $invitedUsers[0]['name'];
                                        }
                                    }
                                    $coach = implode(',', $coach_arr);
                                    $coachee = implode(',', $coachee_arr);
                                }
                                $allusr['ual']['coach'] = $coach;
                                $allusr['ual']['coachee'] = $coachee;
                                $allusr['ual']['video'] = $doc_info[0]['d']['original_file_name'];
                                $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                                $allusr['ual']['url'] = '';
                                $allusr['ual']['session_date'] = '';
                            }
                        }
                    }
                }

                if ($allusr['ual']['type'] == 14) {
                    $allusr['ual']['action_type'] = 'Tag Added';
                    //$doc_info = $this->UserAccount->query("SELECT af.* FROM account_comment_tags act LEFT JOIN account_folder_documents afd ON act.ref_id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE act.comment_id =".$allusr['ual']['ref_id']);
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM comments c LEFT JOIN account_folder_documents afd ON c.ref_id = afd.document_id LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE c.id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = $allusr['ual']['desc'];
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = $allusr['ual']['desc'];
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($allusr['ual']['type'] == 15) {
                    $allusr['ual']['action_type'] = 'Standard Added';
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM account_comment_tags act LEFT JOIN account_folder_documents afd ON act.ref_id = afd.document_id LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE act.comment_id =" . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = $allusr['ual']['desc'];
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                        }
                    } else {
                        $allusr['ual']['url'] = $allusr['ual']['desc'];
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }
                if ($allusr['ual']['type'] == 16) {
                    $allusr['ual']['action_type'] = 'Device Information';
                    $allusr['ual']['url'] = $allusr['ual']['desc'];
                    $allusr['ual']['desc'] = '';
                    $allusr['ual']['video'] = '';
                    $allusr['ual']['coach'] = '';
                    $allusr['ual']['coachee'] = '';
                    $allusr['ual']['session_date'] = '';
                }

                if ($allusr['ual']['type'] == 22) {
                    $allusr['ual']['action_type'] = 'Video Shared';
                    $allusr['ual']['url'] = $allusr['ual']['desc'];
                    $allusr['ual']['desc'] = '';
                    $allusr['ual']['video'] = '';
                    $allusr['ual']['coach'] = '';
                    $allusr['ual']['coachee'] = '';
                    $allusr['ual']['session_date'] = '';
                }

                if ($allusr['ual']['type'] == 23) {


                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);


                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }



                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['action_type'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];


                            $allusr['ual']['url'] = $allusr['ual']['url'];
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    }
                }


                if ($allusr['ual']['type'] == 20) {


                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);


                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }



                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['action_type'] = 'Scripted Video Notes';
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];


                            $allusr['ual']['url'] = $allusr['ual']['url'];
                            $allusr['ual']['video'] = '';
                            $allusr['ual']['session_date'] = '';
                        }
                    }
                }

                if ($allusr['ual']['type'] == 24) {
                    $allusr['ual']['action_type'] = 'Video Hours Uploaded';
                    // $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    //  $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ual']['ref_id']);
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['ual']['coach'] = $coach;
//                            $allusr['ual']['coachee'] = $coachee;
//                            $allusr['ual']['url'] = '';
//                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
//                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
//                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));

                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['df']['duration'] / 60, 2);
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['df']['duration'] / 60, 2);
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['df']['duration'] / 60, 2);
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }

                if ($allusr['ual']['type'] == 25) {
                    $allusr['ual']['action_type'] = 'Video Hours Viewed';
                    // $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ual']['ref_id']);
                    //  $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ual']['ref_id']);
                    $doc_info = $this->UserAccount->query("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['dvh']['document_id']);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['af']['folder_type'] == 1) {
                            $huddle_info = $this->UserAccount->query("SELECT * FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['af']['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = $this->UserAccount->query("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.account_folder_id = " . $doc_info[0]['af']['account_folder_id']);

                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['afu']['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers[0]['name'];
                                    }
                                    if ($invitedUsers['afu']['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers[0]['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['ual']['coach'] = $coach;
//                            $allusr['ual']['coachee'] = $coachee;
//                            $allusr['ual']['url'] = '';
//                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
//                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
//                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['date_added'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['coach'] = $coach;
                            $allusr['ual']['coachee'] = $coachee;
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['video'] = $allusr['ual']['desc'];
                            $allusr['ual']['desc'] = $doc_info[0]['af']['name'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['dvh']['minutes_watched'] / 60, 2);
                        }
                        if ($doc_info[0]['af']['folder_type'] == 2) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['library'] = 'Video Library';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['date_added'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['dvh']['minutes_watched'] / 60, 2);
                        }
                        if ($doc_info[0]['af']['folder_type'] == 3) {
                            $allusr['ual']['url'] = '';
                            $allusr['ual']['desc'] = '';
                            $allusr['ual']['workspace'] = 'My Workspace';
                            $allusr['ual']['coach'] = '';
                            $allusr['ual']['coachee'] = '';
                            $allusr['ual']['video'] = $doc_info[0]['afd']['title'];
                            $allusr['ual']['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['date_added'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['d']['session_date']));
                            $allusr['ual']['minutes'] = round($allusr['dvh']['minutes_watched'] / 60, 2);
                        }
                    } else {
                        $allusr['ual']['url'] = '';
                        $allusr['ual']['desc'] = '';
                        $allusr['ual']['video'] = '';
                        $allusr['ual']['coach'] = '';
                        $allusr['ual']['coachee'] = '';
                        $allusr['ual']['session_date'] = '';
                    }
                }



                $allusr['ual']['username'] = $curr_user['User']['first_name'] . ' ' . $curr_user['User']['last_name'];
                $allusr['ual']['date_added'] = @date('Y-m-d h:i A', strtotime($allusr['ual']['date_added']));
                $all_user_detail[] = $allusr['ual'];
            }
        }
        //print_r($all_user_detail);
        //die;
        $this->set('user_name', $curr_user['User']['first_name'] . ' ' . $curr_user['User']['last_name']);
        $this->set('user_id', $curr_user['User']['id']);
        $this->set('account_id', $account_id);
        $this->set('st_date', $start_date);
        $this->set('en_date', $end_date);
        $this->set('user_detail', json_encode($all_user_detail));
    }

    function excelExport($row_count = 18, $bool = 0) {
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        if (!empty($this->data['acctName'])) {
            $fileName = $this->data['acctName'] . '-' . $account_id . '-' . date('m-d-Y') . '.xlsx';
        } else {
            $fileName = $account_id . '-' . date('m-d-Y') . '.xlsx';
        }
        //header('Content-type: application/vnd.ms-excel');
        ////header("Content-Disposition: attachment; filename=$fileName");
        //  header("Pragma: no-cache");

        $buffer = $this->data['csvBuffer'];

        // echo $buffer; die;
        $buffer = explode('|', $buffer);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                ->setLastModifiedBy("Maarten Balliauw")
                ->setTitle("PHPExcel Test Document")
                ->setSubject("PHPExcel Test Document")
                ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                ->setKeywords("office PHPExcel php")
                ->setCategory("Test result file");
        $count = 0;
        $letter = 'A';
        $line = 1;

        $new_array = $buffer;

        foreach ($new_array as $key => $buff) {
            $buffer[$key] = preg_replace('/[^a-zA-Z0-9-@-_\/.] /', '', $buff);
        }


        while ($count < count($buffer)) {

            if ($count % $row_count == 0 && $count != 0) {
                $line++;
                $letter = 'A';
            }
            if ($line == 1 || $letter < 'F' || $bool) {
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue($letter . $line, $buffer[$count])
                ;
            } else {
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit($letter . $line, $buffer[$count], PHPExcel_Cell_DataType::TYPE_NUMERIC)
                ;
            }



            $letter++;
            $count++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=$fileName");
        $objWriter->save('php://output');

        die;
    }

    function get_huddle_permissions() {
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $result = $this->AccountFolderUser->get_all_account_folder_user($user_id);
        if ($result) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public function beforeFilter() {

        $users = $this->Session->read('user_current_account');
        $this->is_account_activated($users['users_accounts']['account_id'], $users['User']['id']);
       // $this->check_is_account_expired();

        $this->set('AccountFolder', $this->AccountFolder);
        $this->set('AccountFolderUser', $this->AccountFolderUser);
        $this->set('AccountFolderGroup', $this->AccountFolderGroup);
        $this->set('AccountFolderDocument', $this->AccountFolderDocument);
        $this->set('Comment', $this->Comment);
        $this->set('User', $this->User);
        $this->set('logged_in_user', $users);
        $users = $this->Session->read('user_current_account');
        $totalAccounts = $this->Session->read('totalAccounts');
        if ($users['accounts']['is_suspended'] == 1 && $totalAccounts <= 1) {
            $this->redirect_on_account_settings();
        }
        parent::beforeFilter();
    }

    public function coach_tracker() {

        $loggedInUser = $this->Session->read('user_current_account');
        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('tracker_module');
        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $user_role_id = $user['roles']['role_id'];
        $acc_circule_users = $view->Custom->get_users_of_admin_circle($account_id, $user_id);
        $adminUsersIds = array();
        if ($user_role_id == 115) {
            $adminUsers = $this->User->find('all', array(
                'conditions' => array(
                    "User.id IN(select
                        user_id
                      from
                        account_folder_users
                      where account_folder_id in
                        (select
                          afu.account_folder_id
                        from
                          account_folders as af
                          inner join `account_folder_users` as afu
                            on af.`account_folder_id` = afu.`account_folder_id`
                        where af.`account_id` = $account_id
                          and afu.`user_id` = $user_id
                          and afu.`role_id` = 200))"
                )
            ));
            if ($adminUsers) {
                foreach ($adminUsers as $row) {
                    if (is_array($acc_circule_users) && in_array($row['User']['id'], $acc_circule_users)) {
                        $adminUsersIds[] = $row['User']['id'];
                    }
                }
            }
        }

        if (empty($loggedInUser)) {
            $this->redirect('/users/login');
        }

        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count($get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];

        if (!empty($this->data['duration'])) {
            $this->set('sel_duration', $this->data['duration']);
            $dd = $this->data['duration'] - 1;
            $acDt = $acct_duration_start;
            $d = $dd . '-' . $acDt . '-01';
            $stDate = date_create($d);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        } else {
            $this->set('sel_duration', '');
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }

        if (!empty($this->data['duration'])) {
            $acDt = $acct_duration_start - 1;
            $d = $this->data['duration'] . '-' . $acDt . '-01';
            $endDate = date_create($d);

            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        } else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y') + 1;
                $acDt = $acct_duration_start - 1;
                $dd = $yr . '-' . $acDt . '-01';
            } else {
                $yr = date('Y');
                $acDt = $acct_duration_start - 1;
                $dd = $yr . '-' . $acDt . '-01';
            }
            $endDate = date_create($dd);
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        }

        $stDt = date_format($stDate, 'M Y');
        $enDt = date_format($endDate, 'M Y');
        $duration = array();
        $view = new View($this, false);

        if (date('m') >= $acct_duration_start)
            $dy = date('Y');
        else
            $dy = date('Y') - 1;

        for ($i = $dy; $i > 2011; $i--) {
            if($_SESSION['LANG'] == 'es')
            {
             $stmonthName =   $view->Custom->SpanishDate(mktime(null, null, null, $acct_duration_start),'trackers_filter');
            }
            else {
                $stmonthName = date("M", mktime(null, null, null, $acct_duration_start));
            }
            $stDt = $stmonthName . ' ' . $i;
            $d = $i + 1;
            $m = $acct_duration_start - 1;
            if($_SESSION['LANG'] == 'es')
            {
             $enmonthName =  $view->Custom->SpanishDate(mktime(null, null, null, $m),'trackers_filter');
            }
            else
            {
            $enmonthName = date("M", mktime(null, null, null, $m));
            }
            $endDt = $enmonthName . ' ' . $d;
            $duration[$i]['dt'] = $stDt . ' to ' . $endDt;
            $duration[$i]['vl'] = $d;
        }

        $is_user = array();
        if ($user['users_accounts']['role_id'] == 120) {
            $is_user[] = array('huddle_users.user_id' => $user_id);
        }
        $is_admin = array();
        $is_admin_cond = array();
        if ($user['users_accounts']['role_id'] == 115) {
            $is_admin[] = array('huddle_users.user_id' => $user_id);
        }
        if ($user['users_accounts']['role_id'] == 115) {
            $is_admin_cond[] = array(
                'AccountFolderUser.user_id' => $adminUsersIds,
            );
        }
        $mAVCnd = array();
        $mAVCnd[] = array('Documents.recorded_date >= ' => $st_date);
        $mAVCnd[] = array('Documents.recorded_date <= ' => $end_date);

        $fields = array(
            'User.*'
        );
        $account_coaches = $this->AccountFolder->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'left',
                    'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                ),
                array(
                    'table' => 'account_folder_users',
                    'alias' => 'huddle_users',
                    'type' => 'left',
                    'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                ),
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=huddle_users.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                'AccountFolder.is_sample <>1',
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 2,
                'huddle_users.role_id' => 200,
                $is_user,
                $is_admin
            ),
            'fields' => $fields,
            'group' => array('huddle_users.user_id'),
        ));
        if (is_array($account_coaches) && count($account_coaches) > 0) {
            for ($i = 0; $i < count($account_coaches); $i++) {
                $account_coaches[$i]['Coachees'] = array();
                $account_coach_huddles = $this->AccountFolder->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'account_folders_meta_data as afmd',
                            'type' => 'left',
                            'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                        ),
                        array(
                            'table' => 'account_folder_users',
                            'alias' => 'huddle_users',
                            'type' => 'left',
                            'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                        ),
                        array(
                            'table' => 'users as User',
                            'type' => 'left',
                            'conditions' => array('User.id=huddle_users.user_id')
                        )
                    ),
                    'conditions' => array(
                        'AccountFolder.account_id' => $account_id,
                        'AccountFolder.folder_type IN(1)',
                        'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => 2,
                        'huddle_users.role_id' => 200,
                        'AccountFolder.is_sample <>1',
                        'huddle_users.user_id' => $account_coaches[$i]['User']['id']
                    ),
                    'fields' => array(
                        'huddle_users.account_folder_id',
                        'huddle_users.user_id'
                    ),
                    'group' => array('huddle_users.account_folder_id'),
                ));
                $coach_hud = array();
                $account_folder_users = '';
                $is_admin_inner = array();
                foreach ($account_coach_huddles as $coach_huddles) {
                    $coach_hud[] = $coach_huddles['huddle_users']['account_folder_id'];
                    $account_folder_users[] = $coach_huddles['huddle_users']['user_id'];
                }
                $role_id_cond = array();
                if ($user['users_accounts']['role_id'] == 115) {
                    $is_admin_inner[] = array('AccountFolderUser.user_id' => $account_folder_users);
                    $role_id_cond[] = array('AccountFolderUser.role_id' => array(210, 200));
                } else {
                    $role_id_cond[] = array('AccountFolderUser.role_id' => 210);
                }
                $account_coachees = $this->AccountFolderUser->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'users as User',
                            'type' => 'left',
                            'conditions' => array('User.id=AccountFolderUser.user_id')
                        ),
                        array(
                            'table' => 'account_folders',
                            'alias' => 'af',
                            'type' => 'left',
                            'conditions' => array('af.account_folder_id = AccountFolderUser.account_folder_id')
                        ),
                    ),
                    'conditions' => array(
                        'AccountFolderUser.account_folder_id' => $coach_hud,
                        'User.is_active' => 1,
                        'af.is_sample <> 1',
                        $role_id_cond,
                        $is_admin_cond
                    ),
                    'fields' => array(
                        'User.*'
                    ),
                    'group' => array('AccountFolderUser.user_id'),
                    'order' => 'User.first_name ASC',
                ));
                $account_coaches[$i]['Coachees'] = $account_coachees;
                if (is_array($account_coaches [$i]['Coachees']) && count($account_coaches [$i]['Coachees']) > 0) {
                    for ($j = 0; $j < count($account_coaches[$i]['Coachees']); $j++) {
                        $account_coaches[$i]['Coachees'][$j]['Videos'] = array();
                        $account_coachees_videos = $this->Documents->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'account_folder_documents as afd',
                                    'type' => 'left',
                                    'conditions' => array('Documents.id=afd.document_id')
                                ),
                                array(
                                    'table' => 'account_folder_users as afu',
                                    'type' => 'left',
                                    'conditions' => array('afd.account_folder_id=afu.account_folder_id')
                                )
                            ),
                            'conditions' => array(
                                'afd.account_folder_id' => $coach_hud,
                                'Documents.doc_type' => array(1, 3),
                                'afu.user_id' => $account_coaches[$i]['Coachees'][$j]['User']['id'],
                                'afu.role_id' => 210,
                                $mAVCnd
                            ),
                            'fields' => array(
                                'Documents.*',
                                '(SELECT "1" FROM document_meta_data WHERE document_id = `afd`.`document_id` AND meta_data_name = "remove_coaching_tracking" AND meta_data_value = 1) AS RemoveTracking'
                            ),
                            'order' => array('Documents.recorded_date' => 'ASC'),
                            'group' => array('Documents.id'),
                        ));

                        $output = array();
                        for ($l = 0; $l < count($account_coachees_videos); $l++) {
                            if ($account_coachees_videos [$l][0]['RemoveTracking'] != '1') {
                                if ($account_coachees_videos[$l]['Documents']['doc_type'] == '1' || ( $account_coachees_videos[$l]['Documents']['doc_type'] == '3' && empty($account_coachees_videos[$l]['Documents']['is_associated'])) || ($account_coachees_videos[$l]['Documents']['doc_type'] == '3' && !empty($account_coachees_videos[$l]['Documents']['scripted_current_duration']))) {
                                    $output[] = $account_coachees_videos[$l];
                                }
                            }
                        }
                        $account_coachees_videos = $output;

                        $account_coaches[$i]['Coachees'][$j]['Videos'] = $account_coachees_videos;

                        if (is_array($account_coaches[$i] ['Coachees'][$j]['Videos']) && count($account_coaches[$i] ['Coachees'][$j]['Videos']) > 0) {

                            for ($k = 0; $k < count($account_coaches[$i]['Coachees'][$j]['Videos']); $k++) {
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Comments'] = array();
                                $coach_comments = $this->Comment->find('all', array(
                                    'conditions' => array(
                                        'ref_id' => $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Documents']['id']
                                        , 'ref_type' => 2
                                        , 'user_id' => $account_coaches[$i]['User']['id']
                                    ),
                                    'order' => array('created_date' => 'ASC')
                                        )
                                );
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Comments'] = $coach_comments;
                            }
                        }
                        if (is_array($account_coaches[$i] ['Coachees'][$j]['Videos']) && count($account_coaches[$i] ['Coachees'][$j]['Videos']) > 0) {

                            for ($k = 0; $k < count($account_coaches[$i]['Coachees'][$j]['Videos']); $k++) {
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Feedback'] = array();
                                $coach_feedback = $this->Comment->find('all', array(
                                    'conditions' => array(
                                        'ref_id' => $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Documents']['id']
                                        , 'ref_type IN (2,3)'
                                        , 'user_id' => $account_coaches[$i]['Coachees'][$j]['User']['id']
                                    ),
                                    'order' => array('created_date' => 'ASC')
                                        )
                                );
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Feedback'] = $coach_feedback;
                            }
                        }
                    }
                }
            }
        }

        $tracking = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'tracking_duration')));
        $tracking_duration = isset($tracking['AccountMetaData']['meta_data_value']) ? $tracking['AccountMetaData']['meta_data_value'] : "48";
        $this->set('account_coaches', $account_coaches);
        $this->set('duration', $duration);
        $this->set('tracking_duration', $tracking_duration);
        $this->set('language_based_content',$language_based_content);
        $this->render("coach_tracker");
    }

    public function video_detail($document_id, $coach_id, $coachee_id, $from_video = 0, $class = 0) {

        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_role_id = $user['roles']['role_id'];
        $user_id = $user['User']['id'];
        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric['AccountMetaData']['meta_data_name'], 13)), 'value' => $metric['AccountMetaData']['meta_data_value']);
        }
        $this->AccountTag->virtualFields['tags_date'] = 0;
        $this->AccountTag->virtualFields['total_tags'] = 0;
        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'act.ref_id' => $document_id
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        $get_value = array(
            'joins' => $joins,
            'conditions' => $conditions,
            'fields' => $fields,
            'group' => array('AccountTag.account_tag_id'),
            'order' => array('AccountTag__total_tags' => 'desc'),
            'limit' => 5
        );
        $video_tags = $this->AccountTag->find('all', $get_value);

        $account_coach_tags_analytics = Set::extract('/AccountTag/.', $video_tags);

        $standards_settings = false;
        $performace_level_switch = false;

        $mt_arr = array();
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = substr($mt_old['AccountMetaData']['meta_data_name'], 13);
        }

        $assessments = array(0 => 'No Assessment');
        $arr_all = array_merge($assessments, $mt_arr);



        foreach ($account_coach_tags_analytics as $key => $row) {


            $framework_details = $this->AccountTag->find('first', array('conditions' => array('account_tag_id' => $row['account_tag_id'], 'account_id' => $account_id)));

            if (!empty($framework_details['AccountTag']['framework_id'])) {
                $framework_id = $framework_details['AccountTag']['framework_id'];
                $standards_settings = $this->AccountFrameworkSetting->find('first', array('conditions' => array(
                        "account_id" => $account_id,
                        "account_tag_id" => $framework_id
                )));
                if (isset($standards_settings['AccountFrameworkSetting']['enable_performance_level'])) {
                    $performace_level_switch = $standards_settings['AccountFrameworkSetting']['enable_performance_level'];
                } else {
                    $performace_level_switch = false;
                }
            }


            if ($performace_level_switch) {
                $mt_arr = array();
                $account_framework_settings_performance_levels = $this->AccountFrameworkSettingPerformanceLevel->find('all', array('conditions' => array(
                        'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $standards_settings['AccountFrameworkSetting']['id']
                    ), 'order' => 'performance_level_rating'));

                foreach ($account_framework_settings_performance_levels as $pl) {
                    $mt_arr[] = $pl['AccountFrameworkSettingPerformanceLevel']['performance_level'];
                }

                $assessments = array(0 => 'No Assessment');
                $arr_all = array_merge($assessments, $mt_arr);
            }



            $ratings_array = $this->DocumentStandardRating->find('all', array('conditions' => array('document_id' => $document_id, 'standard_id' => $row['account_tag_id'], 'account_id' => $account_id)));
            $count = 0;
            $avg_sum = 0;
            $avg = 0;
            foreach ($ratings_array as $values) {



                $avg = $avg + (int) $values['DocumentStandardRating']['rating_value'];
                $count++;
            }
            if ($count > 0) {

                $final_average = round($avg / $count);
            } else {
                $final_average = 0;
            }
            if ($final_average == 0) {
                $account_coach_tags_analytics[$key]['label'] = 'No Rating';
            }

            if ($final_average > 0) {
                $final_average_name = '';

                if ($performace_level_switch) {
                    foreach ($account_framework_settings_performance_levels as $pl) {
                        if ($final_average == $pl['AccountFrameworkSettingPerformanceLevel']['performance_level_rating']) {
                            $final_average_name = $pl['AccountFrameworkSettingPerformanceLevel']['performance_level'];
                        }
                    }
                } else {

                    foreach ($metric_new as $metric) {
                        if ($final_average == $metric['value']) {
                            $final_average_name = $metric['name'];
                        }
                    }
                }

                $account_coach_tags_analytics[$key]['label'] = $final_average_name;
                $account_coach_tags_analytics[$key]['average_rating'] = $final_average;
                $account_coach_tags_analytics[$key]['color_rating'] = '#000';
            }
        } $this->set('video_tags', json_encode($account_coach_tags_analytics));




        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folderdocument_attachments as afda',
                'type' => 'inner',
                'conditions' => array(
                    'afda.account_folder_document_id= afd.id and afda.attach_id=' . $document_id
                )
            )
        );

        $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');
        $doc_count = $this->Document->find('count', array(
            'joins' => $joins,
            'conditions' => array('doc_type' => '2'),
            'fields' => $fields
        ));
        $this->set('resources', $doc_count);

        $addtional_join = '';
        $additionalCon = '';
        if ($user_role_id == 115) {
            $addtional_join = array(
                'table' => 'account_folder_users as afu',
                'type' => 'inner',
                'conditions' => 'afd.account_folder_id = afd.account_folder_id'
            );
            $additionalCon = array(
                'afu.role_id' => 200,
                'afu.user_id' => $user_id
            );
        }
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            $addtional_join
        );
        $video_detail = $this->Document->find('first', array(
            'joins' => $joins,
            'conditions' => array(
                'Document.id' => $document_id,
                'doc_type' => array(1, 3),
                $additionalCon
            ),
            'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.account_folder_id')
        ));

        $joins = array(
            array(
                'table' => 'account_folder_users as afu',
                'type' => 'left',
                'conditions' => array('afu.account_folder_id = AccountFolderDocument.account_folder_id')
            )
        );

        $fields = array('afu.user_id');
        $h_id = $this->AccountFolderDocument->find('all', array(
            'joins' => $joins,
            'conditions' => array('AccountFolderDocument.document_id' => $document_id, 'afu.role_id' => 200),
            'fields' => $fields
        ));

        $coach_users = array();
        foreach ($h_id as $coach_huddles) {
            $coach_users[] = $coach_huddles['afu']['user_id'];
        }

        $coach_feedback = $this->Comment->find('first', array(
            'conditions' => array(
                'ref_id' => $document_id,
                'ref_type' => 4,
                'user_id' => $coach_users
            ),
            'order' => array('created_date' => 'DESC'))
        );
        $this->set('coach_feedback', $coach_feedback);
        $this->set('video_detail', $video_detail);

        $coachee_name = $this->User->find('first', array('conditions' => array('id' => $coachee_id)));
        $this->set('coachee_name', $coachee_name);
        $this->set('assessment_array', $arr_all);


        $coach_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $document_id, 'user_id' => $coach_users, 'ref_type IN (2,3)')));
        $this->set('coach_comments', $coach_comments);
        $coachee_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $document_id, 'user_id' => $coachee_id, 'ref_type IN (2,3)')));
        $this->set('coachee_comments', $coachee_comments);

        $document = $this->Document->find('first', array('conditions' => array('id' => $document_id)));

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('tracker_module');
        $this->set('language_based_content',$language_based_content);

        $this->set('document', $document);
        $this->set('load_back', $from_video);
        $this->set('url', $this->base . '/dashboard/coach_tracker');
        $this->set('class', $class);
    }

    public function assessment_tracker() {

        $loggedInUser = $this->Session->read('user_current_account');
        $view = new View($this, false);

        $user = $this->Session->read('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_role_id = $user['roles']['role_id'];
        $user_id = $user['User']['id'];
        if ($view->Custom->check_if_eval_huddle_active($account_id) == false) {
            $this->Session->setFlash($this->language_based_messages['you_dont_have_access_to_access_assessment_tracker'], 'default', array('class' => 'message error'));
            return $this->redirect('/Dashboard');
        }
        $adminUsersIds = array();
        if ($user_role_id == 115) {
            $adminUsers = $this->User->find('all', array(
                'conditions' => array(
                    "User.id IN(select
                        user_id
                      from
                        account_folder_users
                      where account_folder_id in
                        (select
                          afu.account_folder_id
                        from
                          account_folders as af
                          inner join `account_folder_users` as afu
                            on af.`account_folder_id` = afu.`account_folder_id`
                        where af.`account_id` = $account_id
                          and afu.`user_id` = $user_id
                          and afu.`role_id` = 200))"
                )
            ));
            if ($adminUsers) {
                foreach ($adminUsers as $row) {
                    $adminUsersIds[] = $row['User']['id'];
                }
            }
        }

        if (empty($loggedInUser)) {
            $this->redirect('/users/login');
        }

        $get_analytics_duration = $this->AccountMetaData->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "analytics_duration"
        )));
        if (is_array($get_analytics_duration) && count(
                        $get_analytics_duration) == 0)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration['AccountMetaData']['meta_data_value'];

        if (!empty($this->data['duration'])) {
            $this->set('sel_duration', $this->data['duration']);
            $dd = $this->data['duration'] - 1;
            $acDt = $acct_duration_start - 1;
            $d = $dd . '-' . $acDt . '-01';
            $stDate = date_create($d);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        } else {
            $this->set('sel_duration', '');
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y');
                $dd = $yr . '-' . $acct_duration_start . '-01';
            } else {
                $yr = date('Y') - 1;
                $dd = $yr . '-' . $acct_duration_start . '-01';
            }
            $stDate = date_create($dd);
            $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }

        if (!empty($this->data['duration'])) {
            $acDt = $acct_duration_start - 1;
            $d = $this->data ['duration'] . '-' . $acDt . '-01';
            $endDate = date_create($d);

            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        } else {
            if (date('m') >= $acct_duration_start) {
                $yr = date('Y') + 1;
                $acDt = $acct_duration_start - 1;
                $dd = $yr . '-' . $acDt . '-01';
            } else {
                $yr = date('Y');
                $acDt = $acct_duration_start - 1;
                $dd = $yr . '-' . $acDt . '-01';
            }
            $endDate = date_create($dd);
            $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
        }

        $stDt = date_format($stDate, 'M Y');
        $enDt = date_format($endDate, 'M Y');
        $duration = array();
        $view = new View($this, false);

        if (date('m') >=
                $acct_duration_start)
            $dy = date('Y'
            );
        else
            $dy = date('Y') - 1;

        for ($i = $dy; $i > 2011; $i--) {
            if($_SESSION['LANG'] == 'es')
            {
                $to = 'a';
                $stmonthName = $view->Custom->SpanishDate(mktime(null, null, null, $acct_duration_start),'trackers_filter');
            }
            else
            {
                $to = 'to';
                $stmonthName = date("M", mktime(null, null, null, $acct_duration_start));
            }
            $stDt = $stmonthName . ' ' . $i;
            $d = $i + 1;
            $m = $acct_duration_start - 1;
            if($_SESSION['LANG'] == 'es')
            {
                $to = 'a';
                $enmonthName = $view->Custom->SpanishDate(mktime(null, null, null, $m),'trackers_filter');   
            }
            else
            {
                $to = 'to';
                $enmonthName = date("M", mktime(null, null, null, $m));
            }
            $endDt = $enmonthName . ' ' . $d;
            $duration [$i] ['dt'] = $stDt . ' ' . $to .' ' . $endDt;
            $duration[$i]['vl'] = $d;
        }

        $is_user = array();
        if ($user['users_accounts'] [
                'role_id'] == 120)
            $is_user[] = array('huddle_users.user_id' => $user_id);

        $is_admin = array();
        if ($user['users_accounts']['role_id'] == 115) {
            $is_admin[] = array('User.id' => $adminUsersIds);
        }
        $mAVCnd = array();
        $mAVCnd[] = array('Documents.recorded_date >= ' => $st_date);
        $mAVCnd[] = array('Documents.recorded_date <= ' => $end_date);

        $fields = array(
            'User.*',
            'huddle_users.account_folder_id',
            'AccountFolder.name'
        );
        $account_coaches = $this->AccountFolder->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'left',
                    'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                ),
                array(
                    'table' => 'account_folder_users',
                    'alias' => 'huddle_users',
                    'type' => 'left',
                    'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                ),
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=huddle_users.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 3,
                'User.is_active' => 1,
                'huddle_users.role_id' => 200,
                $is_admin
            ),
            'fields' => $fields,
            'group' => array('huddle_users.user_id'),
        ));

        if (is_array($account_coaches) && count($account_coaches) > 0) {
            for ($i = 0; $i < count($account_coaches); $i++) {
                $account_coaches[$i]['Coachees'] = array();
                $coach_hud = array();
                foreach ($account_coaches as $coach_huddles) {
                    $coach_hud = $this->evaluator_huddle_ids($account_id, $user_id);
//                    $coach_hud[] = $coach_huddles['huddle_users']['account_folder_id'];
                }

                $account_coachees = $this->AccountFolderUser->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'users as User',
                            'type' => 'left',
                            'conditions' => array('User.id=AccountFolderUser.user_id')
                        )
                    ),
                    'conditions' => array(
                        'AccountFolderUser.account_folder_id' => $coach_hud,
                        'User.is_active' => 1,
                        'AccountFolderUser.role_id' => 210
                    ),
                    'fields' => array(
                        'User.*'
                    ),
                    'group' => array('AccountFolderUser.user_id'),
                ));
                $account_coaches[$i]['Coachees'] = $account_coachees;

                if (is_array($account_coaches [$i]['Coachees']) && count($account_coaches [$i]['Coachees']) > 0) {

                    for ($j = 0; $j < count($account_coaches[$i]['Coachees']); $j++) {
                        $cochee_id = $account_coaches[$i]['Coachees'][$j]['User']['id'];
                        $account_coaches[$i]['Coachees'][$j]['Videos'] = array();
                        $account_coachees_videos = $this->Documents->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'account_folder_documents as afd',
                                    'type' => 'left',
                                    'conditions' => array('Documents.id=afd.document_id')
                                ),
                                array(
                                    'table' => 'account_folder_users as afu',
                                    'type' => 'left',
                                    'conditions' => array('afd.account_folder_id=afu.account_folder_id')
                                ),
                                array(
                                    'table' => 'account_folder_groups as afg',
                                    'type' => 'left',
                                    'conditions' => array('afd.account_folder_id=afg.account_folder_id')
                                ),
                                array(
                                    'table' => 'user_groups as ug',
                                    'type' => 'left',
                                    'conditions' => array('afg.group_id=ug.group_id')
                                ),
                            ),
                            'conditions' => array(
                                'afd.account_folder_id' => $coach_hud,
                                'Documents.doc_type' => 1,
                                'OR' => array(
                                    'afu.user_id' => $account_coaches[$i]['Coachees'][$j]['User']['id'],
                                    'ug.user_id' => $account_coaches[$i]['Coachees'][$j]['User']['id'],
                                ),
                                'OR' => array(
                                    'afu.role_id' => 210,
                                    'afg.role_id' => 210
                                ),
                                'Documents.created_by' => $account_coaches[$i]['Coachees'][$j]['User']['id'],
                                $mAVCnd
                            ),
                            'fields' => array(
                                'Documents.*',
                                'afd.account_folder_id',
                                '(SELECT "1" FROM document_meta_data WHERE document_id = `afd`.`document_id` AND meta_data_name = "remove_assessment_tracking" AND meta_data_value IN(2,1)) AS RemoveTracking'
                            ),
                            'order' => array('Documents.recorded_date' => 'ASC'),
                            'group' => array('Documents.id'),
                        ));
                        $output = array();

                        for ($l = 0; $l < count($account_coachees_videos); $l++) {
                            if ($account_coachees_videos [$l][0]['RemoveTracking'] != '1') {
                                $output[] = $account_coachees_videos[$l];
                            }
                        }
                        $account_coachees_videos = $output;

                        $account_coaches[$i]['Coachees'][$j]['Videos'] = $account_coachees_videos;

                        if (is_array($account_coaches[$i] ['Coachees'][$j]['Videos']) && count($account_coaches[$i] ['Coachees'][$j]['Videos']) > 0) {

                            for ($k = 0; $k < count($account_coaches[$i]['Coachees'][$j]['Videos']); $k++) {
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Feedback'] = array();
                                $coach_feedback = $this->AccountCommentTag->find('all', array(
                                    'conditions' => array(
                                        'ref_id' => $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Documents']['id']
                                        , 'ref_type' => 2
                                    ),
                                    'order' => array('created_date' => 'ASC')
                                        )
                                );
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Feedback'] = $coach_feedback;
                            }
                        }
                    }
                }
            }
        }

        $tracking = $this->AccountMetaData->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'tracking_duration')));
        $tracking_duration = isset($tracking['AccountMetaData']['meta_data_value']) ? $tracking['AccountMetaData']['meta_data_value'] : "48";
//        echo "<pre>";
//        print_r($account_coaches);
//       echo "</pre>";
//       die;
        $this->set('tracking_duration', $tracking_duration);
        $this->set('account_coaches', $account_coaches);
        $this->set('duration', $duration);

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('tracker_module');
        $this->set('language_based_content',$language_based_content);

        $this->render("assessment_tracker");
    }

    function evaluator_huddle_ids($account_id, $evaluator_id) {
        $account_folder = $this->AccountFolder->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'left',
                    'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                ),
                array(
                    'table' => 'account_folder_users',
                    'alias' => 'huddle_users',
                    'type' => 'left',
                    'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                )
            ),
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 3,
                'huddle_users.role_id' => 200,
                'huddle_users.user_id' => $evaluator_id,
            ),
            'fields' => array(
                'huddle_users.account_folder_id'
            ),
        ));
        $account_folder_ids = array();
        $view = new View($this, false);
        if ($account_folder) {
            foreach ($account_folder as $row) {
                if ($view->Custom->check_if_evalutor($row['huddle_users'] ['account_folder_id'], $evaluator_id)) {
                    $account_folder_ids[] = $row['huddle_users'] ['account_folder_id'];
                }
            }
        }
        return

                $account_folder_ids;
    }

    public function assessment_detail($document_id, $coach_id, $coachee_id, $from_video = 0, $class = 0) {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $this->set('metrics_name', $metric_old);

        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric['AccountMetaData']['meta_data_name'], 13)), 'value' => $metric['AccountMetaData']['meta_data_value']);
        }

        $joins = array(
            array(
                'table' => 'account_folder_users as afu',
                'type' => 'left',
                'conditions' => array('afu.account_folder_id = AccountFolderDocument.account_folder_id')
            )
        );

        $fields = array('afu.user_id');
        $h_id = $this->AccountFolderDocument->find('all', array(
            'joins' => $joins,
            'conditions' => array('AccountFolderDocument.document_id' => $document_id, 'afu.role_id' => 200),
            'fields' => $fields
        ));

        $coach_users = array();
        foreach ($h_id as $coach_huddles) {
            $coach_users[] = $coach_huddles['afu']['user_id'];
        }

        $account_coach_huddles = $this->AccountFolder->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'left',
                    'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                ),
                array(
                    'table' => 'account_folder_users',
                    'alias' => 'huddle_users',
                    'type' => 'left',
                    'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                ),
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=huddle_users.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 3,
                'huddle_users.role_id' => 200,
                'huddle_users.user_id' => $coach_users
            ),
            'fields' => array(
                'huddle_users.account_folder_id'
            ),
            'group' => array('huddle_users.account_folder_id'),
        ));
        $coach_hud = array();
        foreach ($account_coach_huddles as $coach_huddles) {
            $coach_hud[] = $coach_huddles['huddle_users']['account_folder_id'];
        }
        $this->Documents->virtualFields['Assessment'] = '';
        $account_coachees_videos = $this->Documents->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'left',
                    'conditions' => array('Documents.id=afd.document_id')
                ),
                array(
                    'table' => 'account_folder_users as afu',
                    'type' => 'left',
                    'conditions' => array('afd.account_folder_id=afu.account_folder_id')
                )
            ),
            'conditions' => array(
                'afd.account_folder_id' => $coach_hud,
                'Documents.doc_type' => 1,
                'afu.user_id' => $coachee_id,
                'Documents.id' => $document_id
//$mAVCnd
            ),
            'fields' => array(
                'Documents.id,Documents.created_date',
                '(SELECT "1" FROM document_meta_data WHERE document_id = `afd`.`document_id` AND meta_data_name = "remove_assessment_tracking" AND meta_data_value = 1) AS RemoveTracking',
                //'(SELECT comment FROM comments WHERE ref_id = `afd`.`document_id` AND ref_type = 5 AND active = 1) AS Documents__Assessment'
//'(SELECT LOWER(tag_title) FROM account_comment_tags WHERE ref_id = `afd`.`document_id` AND ref_type = 2 GROUP BY tag_title ORDER BY count(*) DESC limit 1) AS Documents__Assessment'
                '( SELECT ROUND(AVG(meta_data_value)) FROM account_meta_data , account_comment_tags WHERE LOWER(account_comment_tags.`tag_title`)= LOWER(SUBSTRING(account_meta_data.`meta_data_name` , 14)) AND
                        account_comment_tags.`ref_id` = `afd`.`document_id` AND account_comment_tags.`ref_type` = 2 AND account_id = ' . $account_id . ' ) AS Documents__Assessment'
            ),
            'group' => array('Documents.id'),
            'order' => array('Documents.recorded_date' => 'asc'),
        ));


        $new_array = array();
        foreach ($account_coachees_videos as $videos) {

            foreach ($metric_new as $metric) {
                if ($videos ['Documents']['Assessment'] == $metric['value']) {
                    $videos['Documents']['Assessment'] = $metric['name'];
                }
            }
            $new_array[] = $videos;
        }
        $account_coachees_videos = $new_array;
        $output = array();

        for ($l = 0; $l < count($account_coachees_videos); $l++) {
            if ($account_coachees_videos [$l][0]['RemoveTracking'] != '1') {
                if (empty($account_coachees_videos[$l]['Documents'][
                                'Assessment']))
                    $account_coachees_videos[$l]['Documents']['Assessment'] = 'No Assessment';
                $output[] = $account_coachees_videos[$l];
            }
        }
        $account_coachees_videos = $output;

        $mt_arr = array();
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = substr($mt_old['AccountMetaData']['meta_data_name'], 13);
        }

        $assessments = array(0 => 'No Assessment');
        $arr_all = array_merge($assessments, $mt_arr);
        $video_assessments = array();
        foreach ($account_coachees_videos as $acta) {
            foreach ($arr_all as $key => $value) {
                $acta['Documents']['assessment_point'] = array();
                if (strtolower($acta['Documents'] ['Assessment']) == strtolower($value)) {
                    $acta['Documents']['assessment_point'] = $key;
//                    $date = explode(' ', $acta['Documents']['created_date']);
//                    $acta['Documents']['created_date'] = $date[0];
                }
                $video_assessments[$key] = $acta;
            }
        }
        $results = $this->AccountFolder->query("SELECT
                account_meta_data.`meta_data_value` AS assessment_point,
                account_comment_tags.`tag_title` AS Assessment,
                comments.`id` AS id,
                comments.`time` AS created_date
                FROM
                  account_meta_data,
                  account_comment_tags ,
                  account_folder_documents,
                  comments
                WHERE LOWER(
                    account_comment_tags.`tag_title`
                  ) = LOWER(
                    SUBSTRING(
                      account_meta_data.`meta_data_name`,
                      14
                    )
                  )
                  AND account_comment_tags.`ref_id` = account_folder_documents.`document_id`
                  AND comments.`active` = 1
                  AND account_comment_tags.`ref_type` = 2
                  AND account_id = " . $account_id . "
                  AND account_meta_data.site_id =" . $this->site_id . "
                  AND account_folder_documents.`document_id` =  " . $document_id . "
                  AND account_comment_tags.`comment_id` = comments.`id`");



        $account_coach_tags_analytics = array();
        foreach ($results as $key => $result) {
            $account_coach_tags_analytics[$key]['assessment_point'] = $result['account_meta_data']['assessment_point'];
            $comment_standard = $this->AccountCommentTag->find("first", array("conditions" => array(
                    "comment_id" => $result['comments']['id'],
                    "ref_type" => '0'
            )));
            if (!empty($comment_standard)) {
                $account_coach_tags_analytics[$key]['Assessment'] = $result['account_comment_tags'] ['Assessment'] . '<br>Standard: ' . $comment_standard['AccountCommentTag']['tag_title'];
            } else {
                $account_coach_tags_analytics[$key]['Assessment'] = $result['account_comment_tags']['Assessment'];
            }
            $account_coach_tags_analytics[$key]['id'] = $result['comments']['id'];
            if ($result['comments'] ['created_date'] == 0) {
                $account_coach_tags_analytics[$key]['created_date'] = 'All';
            } else {
                $account_coach_tags_analytics[$key]['created_date'] = gmdate("H:i:s", $result['comments']['created_date']);
            }
        }

// $account_coach_tags_analytics = Set::extract('/Documents/.', $video_assessments);
//        print_r($account_coach_tags_analytics);
//        die;
        $this->set('assessment_graph', json_encode($account_coach_tags_analytics));
        $this->set('assessment_array', $arr_all);

        $this->AccountTag->virtualFields['tags_date'] = 0;
        $this->AccountTag->virtualFields['total_tags'] = 0;
        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'act.ref_id' => $document_id
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        $get_value = array(
            'joins' => $joins,
            'conditions' => $conditions,
            'fields' => $fields,
            'group' => array('AccountTag.account_tag_id'),
            'order' => array('AccountTag__total_tags' => 'desc'),
                //   'limit' => 5
        );
        $video_tags = $this->AccountTag->find('all', $get_value);

        $account_coach_tags_analytics = Set::extract('/AccountTag/.', $video_tags);
        $count_all = 0;
        $total_standards = 0;
        $total_avg_rattings = 0;
        $count_t_avg = 0;
        foreach ($account_coach_tags_analytics as $key => $row) {
            $ratings_array = $this->DocumentStandardRating->find('all', array('conditions' => array('document_id' => $document_id, 'standard_id' => $row['account_tag_id'], 'account_id' => $account_id)));
            $count = 0;
            $avg_sum = 0;
            $avg = 0;
//            echo "<pre>";
//            print_r($row);
//            echo "</pre>";
            $total_standards = $total_standards + $row['total_tags'];
            foreach ($ratings_array as $values) {
                $avg = $avg + (int) $values['DocumentStandardRating']['rating_value'];
                $count++;
            }
            if ($count > 0) {
                $final_average = round($avg / $count);
            } else {
                $final_average = 0;
            }
            if ($final_average == 0) {
                $account_coach_tags_analytics[$key]['label'] = 'No Rating';
            }
            $total_avg_rattings = $total_avg_rattings + $final_average;
            if ($final_average != 0) {
                $count_t_avg ++;
            }
            if ($final_average > 0) {
                $final_average_name = '';
                foreach ($metric_new as $metric) {
                    if ($final_average == $metric['value']) {
                        $final_average_name = $metric['name'];
                    }
                }

                $account_coach_tags_analytics[$key]['label'] = $final_average_name;
                $account_coach_tags_analytics[$key]['average_rating'] = $final_average;
                $account_coach_tags_analytics[$key]['color_rating'] = '#000';
            }
            $total_ratting_avg = round($total_avg_rattings / $count_t_avg);
            $view = new View($this, false);
            $total_ratting_avg = $total_ratting_avg . ' - ' . $view->Custom->get_rating_name($total_ratting_avg, $account_id);
            $this->set('ratting_title', 'Average Performance Level');
            $this->set('standard_title', 'Total tagged standards');
            $this->set('total_ratting', $total_ratting_avg);
            $this->set('total_standards', $total_standards);
            $this->set('standard_color', '#85c4e3');
            $this->set('ratting_color', '#000');

            $count_all ++;
        }

//    print_r($account_coach_tags_analytics);die;
//        $completed_graph = array_merge($account_coach_tags_analytics,$account_coach_tags_analytics);

        $this->set('video_tags', json_encode($account_coach_tags_analytics));

        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folderdocument_attachments as afda',
                'type' => 'inner',
                'conditions' => array(
                    'afda.account_folder_document_id= afd.id and afda.attach_id=' . $document_id
                )
            )
        );

        $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');
        $doc_count = $this->Document->find('count', array(
            'joins' => $joins,
            'conditions' => array('doc_type' => '2'),
            'fields' => $fields
        ));
        $this->set('resources', $doc_count);

        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('Document.created_by=User.id')
            ),
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            )
        );
        $video_detail = $this->Document->find('first', array(
            'joins' => $joins,
            'conditions' => array('Document.id' => $document_id, 'doc_type' => '1'),
            'fields' => array('Document.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.account_folder_id')
        ));
        $this->set('video_detail', $video_detail);

        $doc_assessment = $this->Comment->find('first', array('conditions' => array('ref_id' => $document_id, 'ref_type' => 5)));
        if (!empty($doc_assessment) && count(
                        $doc_assessment) > 0)
            $user_assessment = $doc_assessment['Comment']['comment'
            ];
        else
            $user_assessment = '';
        $this->set('doc_assessment', $user_assessment);

        $coachee_name = $this->User->find('first', array('conditions' => array('id' => $coachee_id)));
        $this->set('coachee_name', $coachee_name);

        $assessment_feedback = $this->Comment->find('first', array(
            'conditions' => array(
                'ref_id' => $document_id
                , 'ref_type' => 5
                , 'user_id' => $coach_users
            ),
            'order' => array('created_date' => 'DESC')
                )
        );

        $this->set('assessment_feedback', $assessment_feedback);

        $coach_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $document_id, 'user_id' => $coach_users, 'ref_type IN (2,3)')));
        $this->set('coach_comments', $coach_comments);
        $coachee_comments = $this->Comment->find('count', array('conditions' => array('ref_id' => $document_id, 'user_id' => $coachee_id, 'ref_type IN (2,3)')));
        $this->set('coachee_comments', $coachee_comments);

        $view = new View($this, false);
        $language_based_content = $view->Custom->get_page_lang_based_content('tracker_module');
        $this->set('language_based_content',$language_based_content);


        $document = $this->Document->find('first', array('conditions' => array('id' => $document_id)));
        $this->set('document', $document);
        $this->set('load_back', $from_video);
        $this->set('url', $this->base . '/dashboard/assessment_tracker');
        $this->set('class', $class);
    }

    public function coaching_tracker_note() {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        if (empty($this->data['coach_feedback_id'])) {
            $this->Comment->create();
            $data = array(
                'title' => '',
                'comment' => $this->data['coach_response'],
                'ref_id' => $this->data['document_id'],
                'ref_type' => '4',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );

            if ($this->Comment->save($data)) {
                $this->Session->setFlash($this->language_based_messages['coach_comment_note_has_been_added_successfully'], 'default', array('class' => 'message success'));
                if ($this->data['load_back'] == 0)
                    $this->redirect('/Dashboard/coach_tracker');
                else
                    $this->redirect('/Huddles/view/' . $this->data ['huddle_id'] . '/1/' . $this->data['document_id']);
            }
        } else {
            $data = array(
                'comment' => "'" . addslashes($this->data['coach_response']) . "'",
            );
            $this->Comment->updateAll($data, array('id' => $this->data['coach_feedback_id']), $validation = TRUE);
            $this->Session->setFlash($this->language_based_messages['coach_comment_note_has_been_updated_successfully'], 'default', array('class' => 'message success'));
            if ($this->data[
                    'load_back'] == 0)
                $this->redirect('/Dashboard/coach_tracker');
            else
                $this->redirect('/Huddles/view/' . $this->data ['huddle_id'] . '/1/' . $this->data[
                        'document_id']);
        }
    }

    public function assessment_note() {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        if (empty($this->data['coach_feedback_id'])) {
            $this->Comment->create();
            $data = array(
                'title' => '',
                'comment' => $this->data['assessment_note'],
                'ref_id' => $this->data['document_id'],
                'ref_type' => '5',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );

            if ($this->Comment->save($data)) {
                $this->Session->setFlash($this->language_based_messages['assessment_has_been_added_successfully'], 'default', array('class' => 'message success'));
                if ($this->data['load_back'] == 0)
                    $this->redirect('/Dashboard/assessment_tracker');
                else
                    $this->redirect('/Huddles/view/' . $this->data ['huddle_id'] . '/1/' . $this->data['document_id']);
            }
        } else {
            $this->Comment->create();
            $data = array(
                'comment' => "'" . Sanitize::escape(htmlspecialchars($this->data['assessment_note'])) . "'",
            );
            $this->Comment->updateAll($data, array('id' => $this->data['coach_feedback_id']), $validation = TRUE);
            $this->Session->setFlash($this->language_based_messages['assessment_has_been_updated_successfully'], 'default', array('class' => 'message success'));
            if ($this->data[
                    'load_back'] == 0)
                $this->redirect('/Dashboard/assessment_tracker');
            else
                $this->redirect('/Huddles/view/' . $this->data ['huddle_id'] . '/1/' . $this->data[
                        'document_id']);
        }
    }

    public function remove_tracking() {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $this->DocumentMetaData->create();
        $data = array(
            'meta_data_name' => $this->data['remove_from_tracking'],
            'meta_data_value' => 1,
            'document_id' => $this->data['document_id'],
            'created_date' => date('y-m-d H:i:s', time()),
            'created_by' => $user_id,
            'last_edit_date' => date('y-m-d H:i:s', time()),
            'last_edit_by' => $user_id
        );

        if ($this->DocumentMetaData->save($data)) {
            $this->Session->setFlash($this->language_based_messages['video_has_been_removed_from_tracking'], 'default', array('class' => 'message success'));
            if ($this->data['load_back'] == 1) {
                $this->redirect('/Huddles/view/' . $this->data ['huddle_id'] . '/1/' . $this->data['document_id']);
            } else {
                if ($this->data ['remove_from_tracking'] ==
                        'remove_assessment_tracking')
                    $this->redirect('/Dashboard/assessment_tracker');
                if ($this->data ['remove_from_tracking'] ==
                        'remove_coaching_tracking')
                    $this->redirect('/Dashboard/coach_tracker');
            }
        }
    }

    function dashboard_settings() {
        $attr = $this->request->data['attr'];
        $account_id = $this->request->data['account_id'];
        $user_id = $this->request->data['user_id'];
        if ($attr == 'getting_started') {
            if ($this->request->data['check'] == 'true') {
                $check = 1;
            } else {
                $check = 0;
            }
            $data = array(
                'hide_welcome' => $check,
            );
            $this->User->updateAll($data, array('id' => $user_id));
        } elseif ($attr == 'recent_activity') {

            if ($this->request->data['check'] == 'true') {
                $check = 1;
            } else {
                $check = 0;
            }
            $data = array(
                'recent_activity' => $check,
            );
            $this->User->updateAll($data, array('id' => $user_id));
        } elseif ($attr == 'setting_assistance') {

            if ($this->request->data['check'] == 'true') {
                $check = 1;
            } else {
                $check = 0;
            }
            $data = array(
                'settings_assistance' => $check,
            );
            $this->User->updateAll($data, array('id' => $user_id));
        } elseif ($attr == 'application_insight') {
            if ($this->request->data['check'] == 'true') {
                $check = 1;
            } else {
                $check = 0;
            }
            $data = array(
                'application_insight' => $check,
            );
            $this->User->updateAll($data, array('id' => $user_id));
        } elseif ($attr == 'mobile_go') {

            if ($this->request->data['check'] == 'true') {
                $check = 1;
            } else {
                $check = 0;
            }
            $data = array(
                'go_mobile' => $check,
            );
            $this->User->updateAll($data, array('id' => $user_id));
        } elseif ($attr == 'setting_assistance_edTPA') {
            if ($this->request->data['check'] == 'true') {
                $check = 1;
            } else {
                $check = 0;
            }
            $data = array(
                'setting_assistance_edTPA' => $check,
            );
            $this->User->updateAll($data, array('id' => $user_id));
        } else {
            echo "failed";
            die;
        }
        die();
    }

    public function analytics_for_account() {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];

        $metric_old = $this->AccountMetaData->find('all', array('conditions' => array('account_id' => $account_id, 'meta_data_name like "metric_value_%"'), 'order' => array('meta_data_value' => 'asc'),));
        $this->set('metrics_name', $metric_old);

        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric['AccountMetaData']['meta_data_name'], 13)), 'value' => $metric['AccountMetaData']['meta_data_value']);
        }


        $joins = array(
            array(
                'table' => 'account_folder_users as afu',
                'type' => 'left',
                'conditions' => array('afu.account_folder_id = AccountFolderDocument.account_folder_id')
            )
        );

        $fields = array('afu.user_id');
        $h_id = $this->AccountFolderDocument->find('all', array(
            'joins' => $joins,
            'conditions' => array('afu.role_id' => 200),
            'fields' => $fields
        ));

        $coach_users = array();
        foreach ($h_id as $coach_huddles) {
            $coach_users[] = $coach_huddles['afu']['user_id'];
        }

        $account_coach_huddles = $this->AccountFolder->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'left',
                    'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                ),
                array(
                    'table' => 'account_folder_users',
                    'alias' => 'huddle_users',
                    'type' => 'left',
                    'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                ),
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=huddle_users.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)',
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 3,
                'huddle_users.role_id' => 200,
                'huddle_users.user_id' => $coach_users
            ),
            'fields' => array(
                'huddle_users.account_folder_id'
            ),
            'group' => array('huddle_users.account_folder_id'),
        ));
        $coach_hud = array();

        foreach ($account_coach_huddles as $coach_huddles) {
            $coach_hud[] = $coach_huddles['huddle_users']['account_folder_id'];
        }

        $this->Documents->virtualFields['Assessment'] = '';
        $account_coachees_videos = $this->Documents->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'left',
                    'conditions' => array('Documents.id=afd.document_id')
                ),
                array(
                    'table' => 'account_folder_users as afu',
                    'type' => 'left',
                    'conditions' => array('afd.account_folder_id=afu.account_folder_id')
                )
            ),
            'conditions' => array(
                'afd.account_folder_id' => $coach_hud,
                'Documents.doc_type' => 1,
            // 'afu.user_id' => $coachee_id,
// 'Documents.id' => $document_id
//$mAVCnd
            ),
            'fields' => array(
                'Documents.id,Documents.created_date',
                '(SELECT "1" FROM document_meta_data WHERE document_id = `afd`.`document_id` AND meta_data_name = "remove_assessment_tracking" AND meta_data_value = 1) AS RemoveTracking',
                //'(SELECT comment FROM comments WHERE ref_id = `afd`.`document_id` AND ref_type = 5 AND active = 1) AS Documents__Assessment'
//'(SELECT LOWER(tag_title) FROM account_comment_tags WHERE ref_id = `afd`.`document_id` AND ref_type = 2 GROUP BY tag_title ORDER BY count(*) DESC limit 1) AS Documents__Assessment'
                '( SELECT ROUND(AVG(meta_data_value)) FROM account_meta_data , account_comment_tags WHERE LOWER(account_comment_tags.`tag_title`)= LOWER(SUBSTRING(account_meta_data.`meta_data_name` , 14)) AND
                        account_comment_tags.`ref_id` = `afd`.`document_id` AND account_comment_tags.`ref_type` = 2 AND account_id = ' . $account_id . ' ) AS Documents__Assessment'
            ),
            'group' => array('Documents.id'),
            'order' => array('Documents.recorded_date' => 'asc'),
        ));


        $new_array = array();
        foreach ($account_coachees_videos as $videos) {

            foreach ($metric_new as $metric) {
                if ($videos ['Documents']['Assessment'] == $metric['value']) {
                    $videos['Documents']['Assessment'] = $metric['name'];
                }
            }
            $new_array[] = $videos;
        }
        $account_coachees_videos = $new_array;
        $output = array();

        for ($l = 0; $l < count($account_coachees_videos); $l++) {
            if ($account_coachees_videos [$l][0]['RemoveTracking'] != '1') {
                if (empty($account_coachees_videos[$l]['Documents'][
                                'Assessment']))
                    $account_coachees_videos[$l]['Documents']['Assessment'] = 'No Assessment';
                $output[] = $account_coachees_videos[$l];
            }
        }
        $account_coachees_videos = $output;

        $mt_arr = array();
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = substr($mt_old['AccountMetaData']['meta_data_name'], 13);
        }

        $assessments = array(0 => 'No Assessment');
        $arr_all = array_merge($assessments, $mt_arr);
        $video_assessments = array();
        $document_ids = array();
        foreach ($account_coachees_videos as $acta) {
            foreach ($arr_all as $key => $value) {
                $acta['Documents']['assessment_point'] = array();
                if (strtolower($acta['Documents'] ['Assessment']) == strtolower($value)) {
                    $acta['Documents']['assessment_point'] = $key;
                    $date = explode(' ', $acta['Documents']['created_date']);
                    $acta['Documents']['created_date'] = $date[0];
                }
                $video_assessments[$key] = $acta;
            }
            $document_ids[] = $acta['Documents']['id'];
        }

        $account_coach_tags_analytics = Set::extract('/Documents/.', $video_assessments);
        $this->AccountTag->virtualFields['tags_date'] = 0;
        $this->AccountTag->virtualFields['total_tags'] = 0;
        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'act.ref_id' => $document_ids
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        $get_value = array(
            'joins' => $joins,
            'conditions' => $conditions,
            'fields' => $fields,
            'group' => array('AccountTag.account_tag_id'),
            'order' => array('AccountTag__total_tags' => 'desc'),
            'limit' => 5
        );
        $video_tags = $this->AccountTag->find('all', $get_value);


        $account_coach_tags_analytics = Set::extract('/AccountTag/.', $video_tags);
        $this->set('video_tags', json_encode($account_coach_tags_analytics));
        $response_data = array(
            'assessment_graph' => json_encode($account_coach_tags_analytics),
            'assessment_array' => $arr_all,
            'video_tags' => json_encode($account_coach_tags_analytics)
        );

        return $response_data;
        die;
    }
    
    function get_counts_of_comment_tags($account_id,$user_id) {
          
        
        $type_of_account = 'Individual';
        $account_detail = $this->Account->find("first", array("conditions" => array(
                "id" => $account_id
        )));
        
        $number_of_child_account = $this->Account->find("count", array("conditions" => array(
                "parent_account_id" => $account_id
        )));
        
        if(!empty($account_detail['Account']['parent_account_id']) && $account_detail['Account']['parent_account_id'] != '0' )
        {
            $type_of_account = 'Child Account';
        }
        else
        {
            $type_of_account = 'Individual';  
        }
        
        
        if($number_of_child_account > 0)
        {
            $type_of_account = 'Parent Account';
        }
        else {
            $type_of_account = 'Individual';
        }
        
        
        
        
        
            
            
            
            
        
         // Can't use find() here because it adds site_id in where clause which we don't need here. The labels are same for both HMH and WL.
 
//         $avg_rating = $db->query("SELECT ROUND(AVG(rating_value),2) AS avg_rating FROM `document_standard_ratings` WHERE account_id = $account_id AND user_id = $user_id");
//         
//            
//         $custom_markers = $db->query("SELECT 
//                        COUNT(*) AS custom_markers 
//                      FROM
//                        `account_comment_tags` act 
//                        JOIN documents d 
//                          ON act.`ref_id` = d.`id` 
//                      WHERE act.`created_by` = $user_id 
//                        AND act.`ref_type` IN (2)
//                        AND d.`account_id` = $account_id");
// 
//         
//         
//         $tagged_standards = $db->query("SELECT 
//                        COUNT(*) AS tagged_standards 
//                      FROM
//                        `account_comment_tags` act 
//                        JOIN documents d 
//                          ON act.`ref_id` = d.`id` 
//                      WHERE act.`created_by` = $user_id 
//                        AND act.`ref_type` IN (0)
//                        AND d.`account_id` = $account_id");
         
         
//         $total_super_admins = $db->query("SELECT COUNT(*) AS total_super_admins FROM `users_accounts` WHERE role_id = 110 AND account_id = $account_id");
//         
//         $total_admins = $db->query("SELECT COUNT(*) AS total_admins FROM `users_accounts` WHERE role_id = 115 AND account_id = $account_id");
//         
//         $total_users = $db->query("SELECT COUNT(*) AS total_users FROM `users_accounts` WHERE role_id = 120 AND account_id = $account_id");
//         
//         $total_viewers = $db->query("SELECT COUNT(*) AS total_viewers FROM `users_accounts` WHERE role_id = 125 AND account_id = $account_id");
//         
        // $number_of_frameworks = $db->query("SELECT COUNT(*) AS number_of_frameworks FROM `account_tags` WHERE account_id = $account_id AND tag_type = 2 ");
         
         $coaching_perfomance_level = $this->User->query("SELECT meta_data_value AS coaching_perfomance_level FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'coaching_perfomance_level'");
         
         $assessment_perfomance_level = $this->User->query("SELECT meta_data_value AS assessment_perfomance_level FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'assessment_perfomance_level'");
         
         $account_framework_setting = $this->User->query("SELECT meta_data_value AS account_framework_setting FROM `account_folders_meta_data` WHERE account_folder_id = $account_id AND meta_data_name = 'enable_framework_standard'");
         
         $account_custom_marker_setting = $this->User->query("SELECT meta_data_value AS account_custom_marker_setting FROM `account_folders_meta_data` WHERE account_folder_id = $account_id AND meta_data_name = 'enable_tags'");
         
         $assessment_tracker = $this->User->query("SELECT meta_data_value AS assessment_tracker FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_matric'");
         
         $coaching_tracker = $this->User->query("SELECT meta_data_value AS coaching_tracker FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_tracker'");
         
         $student_payment = $this->User->query("SELECT amount AS payment_amount , `type` AS payment_type FROM `account_student_payments` WHERE account_id = $account_id");
         
         $view = new View($this, false);
         $user_limit = $view->Custom->get_allowed_users($account_id);
         
         $enable_video_library = $this->User->query("SELECT meta_data_value AS enable_video_library FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_video_library'");
         $number_of_child_accounts = $this->User->query("SELECT COUNT(*) AS number_of_child_accounts FROM accounts WHERE parent_account_id = $account_id");
         $tracking_duration = $this->User->query("SELECT meta_data_value AS tracking_duration FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'tracking_duration'");
         
        
         $account_payments = $this->AccountStudentPayments->find("all", array("conditions" => array(
                "account_id" => $account_id,
            )));
         
         $payment_type = "";
         $payment_amount = "";
         
         foreach($account_payments as $key => $pay)
         {
            if(count($account_payments)- 1 > $key)
            {
            $payment_type .=  $pay['AccountStudentPayments']['type'].'|';
            $payment_amount .=  $pay['AccountStudentPayments']['amount'].'|';
            }
            else {
            $payment_type .=  $pay['AccountStudentPayments']['type'];
            $payment_amount .=  $pay['AccountStudentPayments']['amount'];
            }
         }
         
         $data = array(
         //    'custom_markers' => $custom_markers[0][0]['custom_markers'],
        //     'tagged_standards' => $tagged_standards[0][0]['tagged_standards'],
        //     'avg_rating' => $avg_rating[0][0]['avg_rating'],
            // 'total_super_admins' => $total_super_admins[0][0]['total_super_admins'],
            // 'total_admins' => $total_admins[0][0]['total_admins'],
            // 'total_users' =>  $total_users[0][0]['total_users'],
            // 'total_viewers' =>  $total_viewers[0][0]['total_viewers'],
            // 'number_of_frameworks' => $number_of_frameworks[0][0]['number_of_frameworks'],
             'assessment_perfomance_level' => (isset($assessment_perfomance_level[0]['account_meta_data']['assessment_perfomance_level']) ? $assessment_perfomance_level[0]['account_meta_data']['assessment_perfomance_level'] : "0" ) ,
             'coaching_perfomance_level' => (isset($coaching_perfomance_level[0]['account_meta_data']['coaching_perfomance_level']) ? $coaching_perfomance_level[0]['account_meta_data']['coaching_perfomance_level'] : "0"),
             'account_framework_setting' => (isset($account_framework_setting[0]['account_folders_meta_data']['account_framework_setting']) ? $account_framework_setting[0]['account_folders_meta_data']['account_framework_setting'] : "0"),
             'account_custom_marker_setting' => (isset($account_custom_marker_setting[0]['account_folders_meta_data']['account_custom_marker_setting']) ? $account_custom_marker_setting[0]['account_folders_meta_data']['account_custom_marker_setting'] : "0"),
             'account_detail' => $account_detail,
             'assessment_tracker' => (isset($assessment_tracker[0]['account_meta_data']['assessment_tracker']) ? $assessment_tracker[0]['account_meta_data']['assessment_tracker'] : "0") ,
             'coaching_tracker' => (isset($coaching_tracker[0]['account_meta_data']['coaching_tracker']) ? $coaching_tracker[0]['account_meta_data']['coaching_tracker'] : "0"),
             'enable_video_library' => (isset($enable_video_library[0]['account_meta_data']['enable_video_library']) ? $enable_video_library[0]['account_meta_data']['enable_video_library'] : "0"),
             'number_of_child_accounts' => (isset($number_of_child_accounts[0][0]['number_of_child_accounts']) ? $number_of_child_accounts[0][0]['number_of_child_accounts'] : "0"),
             'tracking_duration' => (isset($tracking_duration[0]['account_meta_data']['tracking_duration']) ? $tracking_duration[0]['account_meta_data']['tracking_duration'] : "0"),
             'payment_amount' => $payment_amount,
             'payment_type' => $payment_type,
             'type_of_account' => $type_of_account,
             'user_limit' => $user_limit
             
             
         );
 
 
         return $data;
     }
     
    function churnzero_attribute_list()
    {
        
        $user_current_account = $this->request->data['user_current_account'];
        $comment_tags_count = $this->request->data['comment_tags_count'];
        $account_id = $this->request->data['account_id'];
        
        $view = new View($this, false);
        $user_role_id = $user_current_account['roles']['role_id'];
        $user_id = $user_current_account['User']['id'];
        $user_role_name = $view->Custom->get_user_role_name($user_role_id);
        $user_email_info = $this->User->find('first', array('conditions' => array('id' => $user_id)));
        $user_email = $user_email_info['User']['email'];
        //Account Attributes
        
        $this->create_churnzero_attribute('Account+Number',$account_id,$user_email,'account',$user_current_account['accounts']['account_id']);
        $this->create_churnzero_attribute('Performance+Levels+Enabled+in+Assessment+Huddles',$account_id,$user_email,'account',$comment_tags_count['assessment_perfomance_level']);
        $this->create_churnzero_attribute('Performance+Levels+Enabled+in+Coaching+Huddles',$account_id,$user_email,'account',$comment_tags_count['coaching_perfomance_level']);
        $this->create_churnzero_attribute('Frameworks+Enabled',$account_id,$user_email,'account',$comment_tags_count['account_framework_setting']);
        $this->create_churnzero_attribute('Custom+Marker+Tags+Enabled',$account_id,$user_email,'account',$comment_tags_count['account_custom_marker_setting']);
        $this->create_churnzero_attribute('Account+Created+Date',$account_id,$user_email,'account',$comment_tags_count['account_detail']['Account']['created_at']);
        $this->create_churnzero_attribute('Assessment+Tracker+Enabled',$account_id,$user_email,'account',$comment_tags_count['assessment_tracker']);
        $this->create_churnzero_attribute('Coaching+Tracker+Enabled',$account_id,$user_email,'account',$comment_tags_count['coaching_tracker']);
        $this->create_churnzero_attribute('Video+Library+Enabled',$account_id,$user_email,'account',$comment_tags_count['enable_video_library']);
        $this->create_churnzero_attribute('edTPA+Enabled',$account_id,$user_email,'account',$comment_tags_count['account_detail']['Account']['enable_edtpa']);
        $this->create_churnzero_attribute('Credit+Card+Customer',$account_id,$user_email,'account',$comment_tags_count['account_detail']['Account']['braintree_customer_id']);
        $this->create_churnzero_attribute('Number+of+Child+Accounts',$account_id,$user_email,'account',$comment_tags_count['number_of_child_accounts']);
        $this->create_churnzero_attribute('Tracker+feedback+duration',$account_id,$user_email,'account',$comment_tags_count['tracking_duration']);
        $this->create_churnzero_attribute('Student+Fee+Type',$account_id,$user_email,'account',$comment_tags_count['payment_type']);
        $this->create_churnzero_attribute('Student+Fee+Amount',$account_id,$user_email,'account',$comment_tags_count['payment_amount']);
        $this->create_churnzero_attribute('Type+of+Account',$account_id,$user_email,'account',$comment_tags_count['type_of_account']);
        $this->create_churnzero_attribute('User+Limit',$account_id,$user_email,'account',$comment_tags_count['user_limit']);
        if($comment_tags_count['account_detail']['Account']['parent_account_id'] != '0' ) {
            $this->create_churnzero_attribute('ParentAccountExternalId',$account_id,$user_email,'account',$comment_tags_count['account_detail']['Account']['parent_account_id']);
        }
        //Contact Attributes
        
        $this->create_churnzero_attribute('User+Role',$account_id,$user_email,'contact',$user_role_name);
        $this->create_churnzero_attribute('User+ID',$account_id,$user_email,'contact',$user_id);
        $this->create_churnzero_attribute('Access+Video+Library',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_access_video_library']);
        $this->create_churnzero_attribute('Upload+Videos+to+Video+Library',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_video_library_upload']);
        $this->create_churnzero_attribute('Manage+Folders',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_maintain_folders']);
        $this->create_churnzero_attribute('Manage+Collaboration+Huddles',$account_id,$user_email,'contact',$user_current_account['users_accounts']['manage_collab_huddles']);
        $this->create_churnzero_attribute('Manage+Coaching+Huddles',$account_id,$user_email,'contact',$user_current_account['users_accounts']['manage_coach_huddles']);
        $this->create_churnzero_attribute('Manage+Users',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_administrator_user_new_role']);
        $this->create_churnzero_attribute('Access+Account-wide+analytics',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_view_analytics']);
        $this->create_churnzero_attribute('Access+personal+analytics',$account_id,$user_email,'contact','1');
        $this->create_churnzero_attribute('User+Created+Date',$account_id,$user_email,'contact',$user_current_account['User']['created_date']);
        
        
        
        die;
        
    }
     
    function create_churnzero_attribute($attribute_name,$account_id,$user_email,$entity,$value)
    {
        
        
//        $params = [
//           'attribute_name' => $attribute_name,
//           'account_id' => $account_id,
//           'user_email' => $user_email,
//           'entity' => $entity,
//           'value' => $value
//       ];
//
//       $query_string = http_build_query($params);
//
//       $output = exec('wget -O - "http://local.sibme.local/async_create_churnzero_attribute?'.$query_string.'" > /dev/null 2>&1 &');
        
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://analytics.churnzero.net/i?appKey=invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg&accountExternalId='.$account_id.'&contactExternalId='.$user_email.'&action=setAttribute&entity='.$entity.'&name='.$attribute_name.'&value='.$value );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            return ['success'=> true];
        
        
        
    }
    
    

     

}
