<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::import('Controller', 'HuddlesController');

/**
 * Description of ObserveController
 *
 * @author 3S
 */
class ObserveController extends AppController {

    public $name = 'Observe';
    //put your code here
    var $helpers = array('Custom', 'Browser');
    public $uses = array("AccountFolder", "Observations", "AccountFolderObservationUsers",
        'User', 'Group', 'UserGroup', 'AccountFolderMetaData', 'AccountFolderUser', 'AccountFolderGroup',
        'UserAccount', 'Document', 'Comment', 'AccountFolderDocument', 'AccountFolderDocumentAttachment', 'CommentUser',
        'CommentAttachment', 'ObservationNoticeLog');
    public $components = array('Email');
    private $video_per_page = 12;

    public function __construct($request = null, $response = null) {
        parent::__construct($request, $response);
    }

    public function index($sort = '', $filter = '') {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        $this->set("sort", $sort);
        $this->set("filter", $filter);

        $totalObservations = $this->Observations->getObservationCount($user_id, $account_id, $sort, '', '', $start = 0, $filter, '');
        $params = array(
            'sort' => $sort,
            'new_huddle_button' => 'New Observation',
            'page_title' => 'Observations ',
            'total_observ' => $totalObservations
        );

        $this->set('user_id', $user_id);
        $this->set('total_observ', $totalObservations);
        $this->set('video_per_page', $this->video_per_page);
        $view = new View($this, false);
        $this->set('head', $view->element('observations', $params));
        $this->render('index');
    }

    public function videos() {
        
    }

    public function add() {
        $user_permissions = $this->Session->read('user_permissions');
        $users = $this->Session->read('user_current_account');
        if ($users['roles']['role_id'] != '120' || $user_permissions['UserAccount']['permission_administrator_observation_new_role'] == '1') {
            $account_id = $users['accounts']['account_id'];
            $this->set('account_id', $account_id);
            $this->set('action', $this->base . '/Observe/submitobservation');
            $this->set('Page', 'Create');
        } else {
            $this->redirect('/NoPermissions/no_permission');
        }
    }

    public function edit($account_folder_obsrvation_id) {
        $user_permissions = $this->Session->read('user_permissions');
        $users = $this->Session->read('user_current_account');
        if ($users['roles']['role_id'] != '120' || $user_permissions['UserAccount']['permission_administrator_observation_new_role'] == '1') {
            $observation_info = $this->Observations->getObservation($account_folder_obsrvation_id);
            $notifications = $this->AccountFolderObservationUsers->getnotification($account_folder_obsrvation_id);
//            $observation_info['Observations']['huddle_account_folder_id'];
            $huddles_user = $this->AccountFolder->getHuddleUsers($observation_info['Observations']['huddle_account_folder_id']);
            $observation_info["notify_at"] = $notifications[0]['AccountFolderObservationUsers']['notify_at'];
            $observation_info["notify_at_unit"] = $notifications[0]['AccountFolderObservationUsers']['notify_at_unit'];
            $observation_info["Users"] = $huddles_user;
            $account_id = $users['accounts']['account_id'];
            $user_id = $users['User']['id'];
            $view = new View($this, false);
            $this->set('account_id', $account_id);
            $this->set('user_id', $user_id);
            $this->set('observation', $observation_info);
            $this->set('Page', 'Edit');
            $this->set('action', $this->base . '/Observe/observationupdate');
            $this->render('add');
        } else {
            $this->redirect('/NoPermissions/no_permission');
        }
    }

    public function submitobservation() {
        $users = $this->Session->read('user_current_account');
        $observation_date = $this->request->data['observation_date_time'];
        $observation_time = $this->request->data['observation-time-picker'];
        $location = $this->request->data['txtlocation'];
        $huddle_name = $this->request->data['txthuddles'];
        $observee = $this->request->data['observee'];
        $notify_duration = $this->request->data['notification_duration'];
        $notification_type = $this->request->data['notification_type'];
        $huddle_account_folder_id = $this->request->data['accflid'];
        $is_private = isset($this->request->data['thing']) ? '1' : '0';
        $observers = isset($this->request->data['group_ids']) ? $this->request->data['group_ids'] : '';
        $observers = isset($this->request->data['group_ids']) ? $this->request->data['group_ids'] : '';
        $whereFrom = isset($this->request->data['whereFrom']) ? $this->request->data['whereFrom'] : '';
        $date = new DateTime($observation_time . ' ' . $observation_date);
        $observation_date_time = $date->format("Y-m-d H:i:s");
        $error_message = '';
        $error_status = 1;
        if (empty($observation_date)) {
            $error_message.='Observation date ,';
            $error_status = 0;
        }
        if (empty($observation_time)) {
            $error_message.='Observation time ,';
            $error_status = 0;
        }
        if (empty($huddle_name) || empty($huddle_account_folder_id)) {
            $error_message.='Observation huddle ,';
            $error_status = 0;
        }

        if (empty($observee)) {
            $error_message.='Observee';
            $error_status = 0;
        }
        if ($error_status == 1) {
            $account_folder_data = array(
                'name' => 'Observation-' . $huddle_name,
                'desc' => '',
                'created_date' => date("Y-m-d H:i:s"),
                'last_edit_date' => date("Y-m-d H:i:s"),
                'created_by' => $users['User']['id'],
                'last_edit_by' => $users['User']['id'],
                'folder_type' => 4,
                'active' => 1,
                'account_id' => $users['accounts']['account_id']
            );
            if ($this->AccountFolder->save($account_folder_data)) {
                $account_folder_id = $this->AccountFolder->id;
                $account_folder_obsercation_data = array(
                    'account_folder_id' => $account_folder_id,
                    'location_name' => $location,
                    'observation_date_time' => $observation_date_time,
                    'is_private' => $is_private,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id'],
                    'huddle_account_folder_id' => $huddle_account_folder_id
                );
                $this->Observations->save($account_folder_obsercation_data);
                $observation_id = $this->Observations->getInsertID();

                $observee_data = array(
                    "account_folder_observation_id" => $observation_id,
                    "user_id" => $observee,
                    "role_id" => 300,
                    "notify_at" => $notify_duration,
                    "notify_at_unit" => $notification_type,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $users['User']['id'],
                    'last_edit_by' => $users['User']['id'],
                );
                $this->AccountFolderObservationUsers->save($observee_data);

                foreach ($observers as $observer) {
                    if (is_numeric($observer)) {
                        $this->AccountFolderObservationUsers->create();
                        $observer_data = array(
                            "account_folder_observation_id" => $observation_id,
                            "user_id" => $observer,
                            "role_id" => 310,
                            "notify_at" => $notify_duration,
                            "notify_at_unit" => $notification_type,
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                        );
                        $this->AccountFolderObservationUsers->save($observer_data);
                    } else {
                        $new_observer = explode('::', $observer);
                        $new_observer_id = $this->_addUser($new_observer[1], $new_observer[2], $huddle_account_folder_id);
                        $superUserData = array(
                            'account_folder_id' => $huddle_account_folder_id,
                            'user_id' => $new_observer_id,
                            'role_id' => '220',
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id']
                        );
//print_r($superUserData);die();
//                        $this->save_huddle_log($account_folder_id, $supUsers);

                        $this->AccountFolderUser->create();
                        $this->AccountFolderUser->save($superUserData);
                        $this->AccountFolderObservationUsers->create();
                        $observer_data = array(
                            "account_folder_observation_id" => $observation_id,
                            "user_id" => $new_observer_id,
                            "role_id" => 310,
                            "notify_at" => $notify_duration,
                            "notify_at_unit" => $notification_type,
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                        );
                        $this->AccountFolderObservationUsers->save($observer_data);
                    }
                }
                $this->Session->setflash("Observation has been saved successfully.", 'default', array('class' => 'message success'));
                if ($whereFrom != '') {
                    $this->redirect('/Huddles/view/' . $huddle_account_folder_id . '/5');
                } else {
                    $this->redirect('/Observe');
                }
            } else {
                $this->Session->setflash("Observation not saved.", 'default', array('class' => 'message error'));
                if ($whereFrom != '') {
                    $this->redirect('/Huddles/view/' . $huddle_account_folder_id . '/5');
                } else {
                    $this->redirect('/Observe');
                }
            }
        } else {
            $error_message.=' not selected';
            $this->Session->setflash($error_message, 'default', array('class' => 'message error'));
            if ($whereFrom != '') {
                $this->redirect('/Huddles/view/' . $huddle_account_folder_id . '/5/add');
            } else {
                $this->redirect('/Observe/add');
            }
        }
        exit;
    }

    public function observationupdate() {
        $users = $this->Session->read('user_current_account');
        $observation_date = $this->request->data['observation_date_time'];
        $observation_time = $this->request->data['observation-time-picker'];
        $location = $this->request->data['txtlocation'];
        $huddle_name = $this->request->data['txthuddles'];
        $observee = $this->request->data['observee'];
        $notify_duration = $this->request->data['notification_duration'];
        $notification_type = $this->request->data['notification_type'];
        $huddle_account_folder_id = $this->request->data['accflid'];
        $is_private = isset($this->request->data['thing']) ? '1' : '0';
        $present_observer = $this->request->data['group_ids'];
        $date = new DateTime($observation_time . ' ' . $observation_date);
        $observation_date_time = $date->format("Y-m-d H:i:s");
        $account_folder_id = $this->request->data['account_folder_id'];
        $account_folder_observation_id = $this->request->data['account_folder_observation_id'];
        $user_id = $users['User']['id'];

        $error_message = '';
        $error_status = 1;
        if (empty($observation_date)) {
            $error_message.='Observation date ,';
            $error_status = 0;
        }
        if (empty($observation_time)) {
            $error_message.='Observation time ,';
            $error_status = 0;
        }
        if (empty($huddle_name) || empty($huddle_account_folder_id)) {
            $error_message.='Observation huddle ,';
            $error_status = 0;
        }

        if (empty($observee)) {
            $error_message.='Observee';
            $error_status = 0;
        }
        if ($error_status == 1) {
            $account_folder_data = array(
                'name' => '"Observation-' . $huddle_name . '"',
                'last_edit_date' => '"' . date("Y-m-d H:i:s") . '"',
                'last_edit_by' => $users['User']['id'],
                'folder_type' => 4,
                'active' => 1,
                'account_id' => $users['accounts']['account_id']
            );
            $this->AccountFolder->updateAll($account_folder_data, array("account_folder_id" => $account_folder_id));

            $account_folder_obsercation_data = array(
                'account_folder_id' => $account_folder_id,
                'location_name' => "'" . $location . "'",
                'observation_date_time' => "'" . $observation_date_time . "'",
                'is_private' => $is_private,
                'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                'last_edit_by' => $users['User']['id'],
                'huddle_account_folder_id' => $huddle_account_folder_id
            );

            $this->Observations->updateAll($account_folder_obsercation_data, array("account_folder_observation_id" => $account_folder_observation_id));

            $observee_data = array(
                "user_id" => $observee,
                "notify_at" => $notify_duration,
                "notify_at_unit" => "'" . $notification_type . "'",
                'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                'last_edit_by' => $users['User']['id'],
            );

            $this->AccountFolderObservationUsers->updateAll($observee_data, array("role_id" => 300, "account_folder_observation_id" => $account_folder_observation_id));

            $old_obser2 = $old_observer = $old_observer = $this->AccountFolderObservationUsers->getobserveruser($account_folder_observation_id, 310);

            $addArray = array();
            $editArray = array();
            $deleteArray = array();

            foreach ($present_observer as $pres) {
                if (is_numeric($pres)) {
                    if (in_array(trim($pres), $old_observer)) {
                        $editArray[] = trim($pres);
                        $observer_data = array(
                            "notify_at" => $notify_duration,
                            "notify_at_unit" => "'" . $notification_type . "'",
                            'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                            'last_edit_by' => $users['User']['id'],
                        );
                        $this->AccountFolderObservationUsers->updateAll($observer_data, array('account_folder_observation_id' => $account_folder_observation_id, "user_id" => trim($pres), "role_id" => 310));
                    } else {
                        $addArray[] = trim($pres);
                        $observer_data = array(
                            "account_folder_observation_id" => $account_folder_observation_id,
                            "user_id" => trim($pres),
                            "role_id" => 310,
                            "notify_at" => $notify_duration,
                            "notify_at_unit" => "'" . $notification_type . "'",
                            'created_date' => "'" . date("Y-m-d H:i:s") . "'",
                            'last_edit_date' => "'" . date("Y-m-d H:i:s") . "'",
                            'created_by' => $users['User']['id'],
                            'last_edit_by' => $users['User']['id'],
                        );
                        $this->AccountFolderObservationUsers->save($observer_data);
                    }
                } else {
                    $addArray[] = trim($pres);
                    $new_observer = explode('::', $pres);
                    $new_observer_id = $this->_addUser($new_observer[1], $new_observer[2], $huddle_account_folder_id);
                    $superUserData = array(
                        'account_folder_id' => $huddle_account_folder_id,
                        'user_id' => $new_observer_id,
                        'role_id' => '220',
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id']
                    );
//print_r($superUserData);die();
//                        $this->save_huddle_log($account_folder_id, $supUsers);

                    $this->AccountFolderUser->create();
                    $this->AccountFolderUser->save($superUserData);
                    $this->AccountFolderObservationUsers->create();
                    $observer_data = array(
                        "account_folder_observation_id" => $account_folder_observation_id,
                        "user_id" => $new_observer_id,
                        "role_id" => 310,
                        "notify_at" => $notify_duration,
                        "notify_at_unit" => $notification_type,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $users['User']['id'],
                        'last_edit_by' => $users['User']['id'],
                    );
                    $this->AccountFolderObservationUsers->save($observer_data);
                }
            }
            foreach ($old_observer as $oldObs) {
                if (!in_array(trim($oldObs), $editArray)) {
                    $deleteArray[] = trim($oldObs);
                    try {
                        $this->AccountFolderObservationUsers->deleteAll(array('account_folder_observation_id' => $account_folder_observation_id, "user_id" => trim($oldObs), "role_id" => 310), false);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                }
            }

            $this->Session->setflash("Observation has been Updated.", 'default', array('class' => 'message success'));
            $this->redirect('/Observe');
        } else {
            $error_message.=' not selected';
            $this->Session->setflash($error_message, 'default', array('class' => 'message error'));
            $this->redirect('/Observe/edit/' . $account_folder_observation_id);
        }
        exit;
    }

    public function detail() {
        
    }

    public function details() {
        
    }

    public function gethuddlesbyname() {
        if ($this->request->is('post')) {
            $name = $this->request->data['name'];
            $account_id = $this->request->data['account_id'];
            $account_folder_id = '';
            $huddles_info = $this->AccountFolder->gethuddlessbyname($account_id, $name);
            echo json_encode($huddles_info);
            exit();
        }
    }

    public function gethuddleuser() {
        if ($this->request->is('post')) {
            $account_floder_id = $this->request->data['ac_fl_id'];
            $results = $this->AccountFolder->getHuddleUsers($account_floder_id);
            if ($results[0]["User"]["id"] != '') {
                echo json_encode($results);
            } else {
                echo json_encode(array('false'));
            }
            exit();
        }
    }

    public function getAjaxObservations() {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $view = new View($this, true);

        $page = isset($this->request->data['page']) ? $this->request->data['page'] : '1';
        $page_huddle = isset($this->request->data['page_huddle']) ? $this->request->data['page_huddle'] : '1';
        $keywords = isset($this->request->data['title']) ? $this->request->data['title'] : '';
        $sort = isset($this->request->data['sort']) ? $this->request->data['sort'] : 'all';
        $filter = isset($this->request->data['filter']) ? $this->request->data['filter'] : 'all';
        $type = isset($this->request->data['type']) ? $this->request->data['type'] : '';
        $huddle_id = isset($this->request->data['huddle_id']) ? $this->request->data['huddle_id'] : '';
        $cur_page = $page;
        $page -= 1;
        $page_huddle -= 1;
        $per_page = $this->video_per_page;
        $previous_btn = true;
        $next_btn = true;
        $first_btn = true;
        $last_btn = true;
        $start = $page * $per_page;
        $msg = '';
        $count = $this->Observations->getObservationCount($user_id, $account_id, $sort, $keywords, $this->video_per_page, $start, $filter, $huddle_id);
        $observations = array(
            'observs' => $this->Observations->searchObservation($user_id, $account_id, $sort, $keywords, $this->video_per_page, $start, $filter, $huddle_id),
            'sort' => $sort,
            'new_huddle_button' => 'New Observation',
            'page_title' => 'Observations ',
            'total_observ' => ($count == 0 ? "0" : $count)
        );

        if ($type != '' && $type == 'get_huddel_observations') {
            $observations_huddles =         $this->Observations->searchObservation($user_id, $account_id, $sort, $keywords, $this->video_per_page, $page_huddle, $filter, $huddle_id);
            $observations_count = $this->Observations->getObservationCount($user_id, $account_id, $sort, $keywords, $this->video_per_page, $page_huddle, $filter, $huddle_id);
            $obser_data = array(
                'observations' => $observations_huddles,
                'totalVideos' => $observations_count,
                'video_per_page' => $this->video_per_page,
                'current_page' => $page_huddle,
                'huddle_id' => $huddle_id,
                'user_id' => $user_id,
                'tab' => '5',
                'huddle_des' => $this->AccountFolder->get($huddle_id),
                'show_ff_message' => false,
                'documentController' => $this->Document,
                'hideLoadMore' => false
            );
            if ($this->video_per_page * $page_huddle >= $observations_count-$this->video_per_page) {
                $obser_data['hideLoadMore'] = true;
            }
            if ($page_huddle > 0) {
                $msg = $view->element('huddles/ajax/ajax_observations_in_huddle_for_load_more', $obser_data);
            } else {
                $msg = $view->element('huddles/ajax/ajax_observations_in_huddle', $obser_data);
            }
            echo $msg;
            exit;
        } else {
            $msg = $view->element('huddles/ajax/ajax_observation_search', $observations);
        }
        /* --------------------------------------------- */

        if ($count > $per_page) {
            $no_of_paginations = ceil($count / $per_page);

            /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
            if ($cur_page >= 7) {
                $start_loop = $cur_page - 3;
                if ($no_of_paginations > $cur_page + 3)
                    $end_loop = $cur_page + 3;
                else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                    $start_loop = $no_of_paginations - 6;
                    $end_loop = $no_of_paginations;
                } else {
                    $end_loop = $no_of_paginations;
                }
            } else {
                $start_loop = 1;
                if ($no_of_paginations > 7)
                    $end_loop = 7;
                else
                    $end_loop = $no_of_paginations;
            }
            /* ----------------------------------------------------------------------------------------------------------- */
            $msg .= "<div class='pagination'><ul>";

            // FOR ENABLING THE FIRST BUTTON
            if ($first_btn && $cur_page > 1) {
                $msg .= "<li p='1' class='active'>&lt;&lt;</li>";
            } else if ($first_btn) {
                $msg .= "<li p='1' class='inactive'>&lt;&lt;</li>";
            }

            // FOR ENABLING THE PREVIOUS BUTTON
            if ($previous_btn && $cur_page > 1) {
                $pre = $cur_page - 1;
                $msg .= "<li p='$pre' class='active'>&lt;</li>";
            } else if ($previous_btn) {
                $msg .= "<li class='inactive'>&lt;</li>";
            }

            for ($i = $start_loop; $i <= $end_loop; $i++) {
                if ($cur_page == $i)
                    $msg .= "<li p='$i' style='color:#fff;background:url(\"/img/active-pagination-bg.png\") repeat;' class='active'>{$i}</li>";
                else
                    $msg .= "<li p='$i' class='active'>{$i}</li>";
            }

            // TO ENABLE THE NEXT BUTTON
            if ($next_btn && $cur_page < $no_of_paginations) {
                $nex = $cur_page + 1;
                $msg .= "<li p='$nex' class='active'>&gt;</li>";
            } else if ($next_btn) {
                $msg .= "<li class='inactive'>&gt;</li>";
            }

            // TO ENABLE THE END BUTTON
            if ($last_btn && $cur_page < $no_of_paginations) {
                $msg .= "<li p='$no_of_paginations' class='active'>&gt;&gt;</li>";
            } else if ($last_btn) {
                $msg .= "<li p='$no_of_paginations' class='inactive'>&gt;&gt;</li>";
            }

            //$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
            //$msg = $msg . "</ul>" . $total_string . "</div>";  // Content for pagination
            //$msg = $msg . "<hr class='full--slim dashed'>";  // Content for pagination
        }
        echo $msg;
        exit;
    }

    public function delete($observation_id) {

        $account_folder_id = $this->Observations->get_account_folders_id($observation_id);
        if ($account_folder_id != 'NULL') {
            //DELET ALL USERES OF THE ONSERVAION
            $this->AccountFolderObservationUsers->deleteAll(array('account_folder_observation_id' => $observation_id), false);

            //DELET OBSERVATION
            $this->Observations->deleteAll(array('account_folder_observation_id' => $observation_id), false);

            //DELET ALL USERES OF THE Account Folder
            $this->AccountFolder->deleteAll(array('account_folder_id' => $account_folder_id), false);


            $this->Session->setFlash('Observatation has been deleted successfully', 'default', array('class' => 'message success'));
            $this->redirect('/Observe');
        } else {
            $this->Session->setFlash('Observatation not Deleted', 'default', array('class' => 'message error'));
            $this->redirect('/Observe');
        }
    }

    public function _addUser($full_name, $email, $account_folder_id = '') {

        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $this->User->create();
        $arr = '';
        $data = '';
        if (isset($full_name)) {
            $name = explode(' ', $full_name);
            $arr['first_name'] = isset($name[0]) ? $name[0] : '';
            $arr['last_name'] = isset($name[1]) ? $name[1] : '';
        }
        if (isset($email)) {

            $arr['created_by'] = $users['User']['id'];
            $arr['created_date'] = date('Y-m-d H:i:s');
            $arr['last_edit_by'] = $users['User']['id'];
            $arr['last_edit_date'] = date('Y-m-d H:i:s');
            $arr['account_id'] = $account_id;
            $arr['email'] = $email;
            $arr['authentication_token'] = $this->digitalKey(20);
            $arr['is_active'] = false;
            $arr['type'] = 'Invite_Sent';
            if ($arr['first_name'] != '' && $arr['email'] != '') {
                $data[] = $arr;
            }
        }

        $new_user_id = -1;
        $roleData = array();
        foreach ($data as $user) {
            if (isset($user['first_name']) && isset($user['email']) && $user['email']) {
                $exUser = $this->User->find('first', array('conditions' => array('email' => $user['email'])));
                $userAccountCount = 0;
                $this->User->create();
                if (count($exUser) == 0 && $this->User->save($user, $validation = TRUE)) {
                    $user_id = $this->User->id;
                    $this->User->updateAll(array(
                        'authentication_token' => "'" . md5($user_id) . "'"), array('id' => $user_id)
                    );
                    $user['authentication_token'] = md5($user_id);
                } else {
                    $user_id = $exUser['User']['id'];
                }

                if (!empty($user_id)) {

                    $userAccountCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id,
                            'account_id' => $account_id
                        )
                    ));
                    if ($userAccountCount == 0) {

                        $this->UserAccount->create();
                        //Checking default account is exist or not
                        $CurrentUserAccountsCount = $this->UserAccount->find('count', array('conditions' => array(
                            'user_id' => $user_id
                        )));
                        $userAccount = array(
                            'account_id' => $account_id,
                            'user_id' => $user_id,
                            'role_id' => '120',
                            'is_default' => ($CurrentUserAccountsCount > 1 ? 0 : 1),
                            'created_by' => $users['User']['id'],
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'last_edit_by' => $users['User']['id']
                        );

                        $this->UserAccount->save($userAccount, $validation = TRUE);

                        if (!empty($user_id)) {
                            $user['user_id'] = $user_id;                            
                            if (count($exUser) == 0) {
                                $this->sendInvitation($user, $account_folder_id);
                                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                                $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_id . ')')));
                                $huddle[0]['User']['email'] = $huddleUserInfo[0]['User']['email'];
                                $this->newHuddleEmail($huddle[0], $user_id);
                               
                            } else {                                                              
                                $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);                        
                                $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $user_id . ')')));                                                                
                                $user['authentication_token']=$huddleUserInfo[0]['User']['authentication_token'];
                                $huddle[0]['User']['email'] = $huddleUserInfo[0]['User']['email'];
                                 $this->sendConfirmation($user);                                
                                $this->newHuddleEmail($huddle[0], $user_id);                                
                            }

                            $user_activity_logs = array(
                                'ref_id' => $account_folder_id,
                                'desc' => $user['first_name'] . " " . isset($user['last_name']) ? $user['last_name'] : '',
                                'url' => $this->base . 'Huddles',
                                'account_folder_id' => $account_folder_id,
                                'date_added' => date("Y-m-d H:i:s")
                            );
                            $this->user_activity_logs($user_activity_logs);

                            $new_user_id = $user_id;
                        }
                    } else {                        
                        $new_user_id = $user_id;
                        $huddle = $this->AccountFolder->getHuddle($account_folder_id, $account_id);
                        $huddleUserInfo = $this->User->find('all', array('conditions' => array('id IN(' . $new_user_id . ')')));
                        $huddle[0]['User']['email'] = $huddleUserInfo[0]['User']['email'];
                        $this->newHuddleEmail($huddle[0], $user_id);
                    }
                }
            }
        }
        
        return $new_user_id;
    }

    public function getObservationNoteTime() {
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $observation_id = isset($this->request->data['observation_id']) ? $this->request->data['observation_id'] : '';
        if ($observation_id != '') {
            $fields = array('notify_at', 'notify_at_unit');
            $conditions = array('account_folder_observation_id' => $observation_id, 'user_id' => $user_id);
            $result = $this->AccountFolderObservationUsers->find('first', array('fields' => $fields, 'conditions' => $conditions));
            $resultArray = array();
            if (count($result) > 0) {
                $resultArray['notify_at'] = $result['AccountFolderObservationUsers']['notify_at'];
                $resultArray['notify_at_unit'] = $result['AccountFolderObservationUsers']['notify_at_unit'];
            }
            if ($resultArray['notify_at'] != '' && $resultArray['notify_at_unit'] != '') {
                echo json_encode($resultArray);
                exit;
            }
        }
        die(json_encode(array('error' => 'true')));
    }

    public function updateNotificationTime() {
        $users = $this->Session->read('user_current_account');
        $user_id = $users['User']['id'];
        $observation_id = isset($this->request->data['observation_id']) ? $this->request->data['observation_id'] : '';
        $notify_at = isset($this->request->data['notify_at']) ? $this->request->data['notify_at'] : '';
        $notification_type = isset($this->request->data['notification_type']) ? $this->request->data['notification_type'] : '';
        $this->AccountFolderObservationUsers->create();
        $data = array(
            'notify_at' => '"' . addslashes($notify_at) . '"',
            'notify_at_unit' => '"' . addslashes($notification_type) . '"'
        );
        $conditions = array('account_folder_observation_id' => addslashes($observation_id));
        $return = $this->AccountFolderObservationUsers->updateAll($data, $conditions);
        echo json_encode($return);
        exit;
    }

    public function checkuserinhuddle() {
        $users = $this->Session->read('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $acount_folder_id = $this->request->data['account_folder_id'];
        $this->layout = null;
        $errorMessages = '';
        $user_data = $_POST['user_data'];
        for ($i = 0; $i < count($user_data); $i++) {
            $posted_user = $user_data[$i];
            $user_info = $this->User->find("all", array(
                "conditions" => array("email" => $posted_user[2]),
                "fields" => "User.id"
            ));
            if (count($user_info) > 0) {
                if ($this->AccountFolderUser->checkuserexistinhuddle($acount_folder_id, $user_info[0]['User']['id']) > 0) {
                    if (!empty($errorMessages))
                        $errorMessages .= "<br/>";
                    $errorMessages .= $posted_user[2] . " email already exists in huddle.";
                }
            }
        }
        $this->set('ajaxdata', "$errorMessages");
        $this->render('/Elements/ajax/ajaxreturn');
    }

}