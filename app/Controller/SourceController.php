Appletree is our people! Our entire team understands precisely how their role brings a critical element to your business success. Ours is not a team that just attends – they perform! Sure, there are pages of accountants listed in the phone book, so why choose Appletree?

&nbsp;

Each member of our staff is specially trained in small business accounting needs. We bring more than two decades of experience in helping small business owners just like you to increase your profitability, decrease your taxes, deal with IRS problems, and to plan your business financial confidence strategy.

&nbsp;

We work with our clients year-round; giving them confidence that they can trust in their process, providing them with altitude so they can see the big picture, keeping them in full compliance with the tax laws, talking with them on a regular basis on issues such as whether someone should be an employee  versus a subcontractor, or whether they should be leasing or buying that next vehicle. We make sure that business planning and tax planning is done as well to make sure taxes are anticipated and that they are prepared, so that clients can focus on their business <em>worry free!</em>

&nbsp;
<div class="clearfloat"></div>
<div class="col_info"><img alt="" src="http://jjtestsite.us/appletree/wp-content/uploads/2013/09/staff_07.png" /></div>
<div class="col_info_txt">
    <h1>Want More Information?</h1>
    <h2>We're happy to offer you a complimentary consultation</h2>
    Give us a call at 603-434-2775 (APPLE) or <a href="mailto:info@AppletreeBusiness.com" target="_blank"><em>send us an email</em></a>, and find out what Appletree Business Services can do for your business!

</div>
&nbsp;
<div class="col_info_txt">
    <div class="staff_div">
        <h1>Meet Steve Feinberg CPA - Owner</h1><div class="clearline"></div>
        <div class="clearfloat"></div>
        <h2>About</h2><div style="float:left;padding: 4px;border: 1px solid #006832;margin-right: 10px;"><img  alt="" src="http://jjtestsite.us/appletree/wp-content/uploads/2013/09/staff_11.png" /></div>
        <span style="color: #0000ff;"><a href="mailto:steve@AppletreeBusiness.com" target="_blank"><span style="color: #006633;">Steve</span></a></span> (Ext 70) has been in the Accounting industry for over 30 years. He founded Appletree Business Services in 1988.

        &nbsp;
        <h2>Experience</h2>
        After graduating from Bryant University majoring in both Accounting and Systems Management, Steve worked for Price Waterhouse for 8 years as an auditor and computer consultant before branching off on his own. He thrives on keeping the firm technologically on target and at least one step ahead of his clients.
        <h2>Hobbies</h2>
        Many will recognize Steve from his performances at the Majestic Theater in Manchester. If you ask, he'll even show you a copy of his review in the Nashua Telegraph! You can find him posting on blog.appletreebusiness.com and on Twitter as CPASteve.

        &nbsp;
        <div class="col_info_txt">
            <div class="staff_div">
                <h1>Meet Paul Smythe CPA - Accounting Manager</h1>
                <div class="clearline"></div>
                <div class="clearfloat"></div>
                <h2>About</h2>
                <div style="float:left;padding: 4px;border: 1px solid #006832;margin-right: 10px;"><a href="http://www.appletreebusiness.com/wp-content/uploads/2013/11/Paula_02.png"><img class="alignnone size-full wp-image-332" alt="Paul" src="http://www.appletreebusiness.com/wp-content/uploads/2013/11/Paul_03.png" width="180" height="240" /></a></div>
                <span style="color: #006633;"><a href="mailto:paul@AppletreeBusiness.com" target="_blank"><span style="color: #006633;">Paul</span></a></span> (Ext 71) joined Appletree Business Services in November 2007.

                &nbsp;
                <h2>Experience</h2>
                Paul earned degrees in Physics, Computer Science, and Accounting from Princeton University, State University of New York at Stony Brook, and Franklin Pierce College. His employment experience includes technical work in the software industry, business management in the masonry manufacturing industry, and tax work in the public accounting industry.
                <h2>Hobbies</h2>
                Paul enjoys sports, outdoor activities, and travel with his wife and kids.

                &nbsp;
                <div class="col_info_txt">
                    <div class="staff_div">
                        <h1>Trish - Business Support Specialist</h1>
                        <div class="clearline"></div>
                        <div class="clearfloat"></div>
                        <h2>About</h2>
                        <div style="float:left;padding: 4px;border: 1px solid #006832;margin-right: 10px;">
                            <img  alt="" src="http://www.appletreebusiness.com/wp-content/uploads/2013/11/Trish.jpg" />
                        </div>
                        <span style="color: #006633;"><a href="mailto:trish@AppletreeBusiness.com" target="_blank"><span style="color: #006633;">Trish</span></a></span>  (Ext 72) has worked at Appletree Business Services since 1989.

                        &nbsp;
                        <h2>Experience</h2>
                        Trish studied accounting at NH College. Trish has expertise in Accounts Receivable, Payables and Collections along with Bookkeeping.
                        <h2>Hobbies</h2>
                        Trish enjoys spending time with family and friends. Summers you will find Trish on the lake boating or relaxing on the dock. She enjoys water sports, skiing and snowmobiling. Trish also loves shopping, reading and going to the movies.

                        &nbsp;
                        <div class="col_info_txt">
                            <div class="staff_div">
                                <h1>Pat - Business Support Specialist</h1>
                                <div class="clearline"></div>
                                <div class="clearfloat"></div>
                                <h2>About</h2>
                                <div style="float:left;padding: 4px;border: 1px solid #006832;margin-right: 10px;">
                                    <img  alt="" src="http://www.appletreebusiness.com/wp-content/uploads/2013/11/Pat-e1384860713452.jpg" />
                                </div>

                                <span style="color: #006633;"><a href="mailto:pat@AppletreeBusiness.com" target="_blank"><span style="color: #006633;">Pat</span></a></span> (Ext 73) has been employed by Appletree since August 1989.  She has enjoyed a business relationship with 15 of her 34 clients for at least ten years.

                                &nbsp;
                                <h2>Experience</h2>
                                Prior to working with Steve, Pat was busy earning her MBA from the Whittemore School of Business at UNH and an undergraduate BS in Math and a minor in physics from Rensselaer Polytechnic Institute.
                                <h2>Hobbies</h2>
                                In addition to spending time with family and friends, and gardening, Pat enjoys traveling, mainly to warmer climates in the winter months.  She is also an avid golfer and volunteers with the state women's golfing association. She has the best tan of all the staff!
                                <div class="col_info_txt">
                                    <div class="staff_div">
                                        <h1>Paula - Business Support Specialist</h1>
                                        <div class="clearline"></div>
                                        <div class="clearfloat"></div>
                                        <h2>About</h2>
                                        <div style="float:left;padding: 4px;border: 1px solid #006832;margin-right: 10px;">
                                            <img  alt="" src="http://www.appletreebusiness.com/wp-content/uploads/2013/11/Paula_02.png" />
                                        </div>

                                        <span style="color: #006633;"><a href="mailto:paula@AppletreeBusiness.com" target="_blank"><span style="color: #006633;">Paula</span></a></span> (Ext 74) has been with Appletree Business Services since 2005.

                                        &nbsp;
                                        <h2>Experience</h2>
                                        Paula graduated from University of Lowell (now UMass @ Lowell) with a BS in Business Administration.
                                        <h2>Hobbies</h2>
                                        She enjoys traveling with her husband and two sons.

                                        &nbsp;

                                        <div class="col_info_txt">
                                            <div class="staff_div">
                                                <h1>Nancy - Office Support Specialist</h1>
                                                <div class="clearline"></div>
                                                <div class="clearfloat"></div>
                                                <h2>About</h2>
                                                <div style="float:left;padding: 4px;border: 1px solid #006832;margin-right: 10px;">
                                                    <img  alt="" src="http://www.appletreebusiness.com/wp-content/uploads/2013/11/Nancy.jpg" />
                                                </div>

                                                <span style="color: #006633;"><a href="mailto:nancy@AppletreeBusiness.com" target="_blank"><span style="color: #006633;">Nancy</span></a></span> (Ext 76) started with Appletree in 2005. Along with Tina, Nancy is the other friendly voice you hear when calling Appletree Business Services.
                                                <h2>Experience</h2>
                                                Nancy has over twenty years of experience in bookkeeping, payroll and office administration.
                                                <h2>Hobbies</h2>
                                                In her spare time, Nancy enjoys being outdoors, camping and biking with her family. Like most New Englanders, she is an avid Red Sox fan.

                                            </div>
                                        </div>
                                        &nbsp;
                                        <div class="col_info_txt">
                                            <div class="staff_div">
                                                <h1>Karen - Marketing Manager</h1>

                                                <div class="clearline"></div>
                                                <div class="clearfloat"></div>
                                                <h2>About</h2><div style="float:left;padding: 4px;border: 1px solid #006832;margin-right: 10px;">
                                                    <img  alt="" src="http://www.appletreebusiness.com/wp-content/uploads/2013/11/Trish-e1384860583887.jpg" /></div>
                                                <span style="color: #006633;"><a href="mailto:karen@AppletreeBusiness.com" target="_blank"><span style="color: #006633;">Karen</span></a></span> (Ext 77) joined Appletree in winter 2008. You will find her out there schmoozing and behind the scenes working on our marketing efforts, both online and offline.
                                                <h2>Experience</h2>
                                                An Army veteran, Karen has over 17 years experience in Sales and Marketing, as well as years of administration experience while on Active Duty in the Army and as a civilian at the Portsmouth Shipyard. She majored in Psychology and Business Administration.
                                                <h2>Hobbies</h2>
                                                Karen enjoys live theater, loves fresh sushi, and has a passion for cooking unusual foods.

                                            </div>
                                        </div>
                                        &nbsp;
                                        <div class="col_info_txt">
                                            <div class="staff_div">
                                                <h1>Tina - Payroll Support Specialist</h1>
                                                <div class="clearline"></div>
                                                <div class="clearfloat"></div>
                                                &nbsp;
                                                <h2>About</h2>
                                                When you call Appletree Business Services, you won't get an automated mailbox, more than likely <span style="color: #006633;"><a href="mailto:tina@AppletreeBusiness.com" target="_blank"><span style="color: #006633;">Tina's</span></a></span> (Ext 75) will be one of the friendly voices on the line! Tina has been with Appletree since January, 2000.
                                                <h2>Experience</h2>
                                                Tina attended Salem State College majoring in Theatre.
                                                <h2>Hobbies</h2>
                                                Tina enjoys the theater, both in the  audience and as a performer on stage. She enjoys gourmet food and frequent trips to Fenway Park &amp; Disney World.

                                            </div>
                                        </div>
                                        &nbsp;
                                        <div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>