<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountFolderdocumentAttachment;
use App\Models\DocumentFiles;
use App\Models\JobQueue;
use App\Models\UserReadComments;
use Com\Wowza\Application;
use Com\Wowza\Server;
use Com\Wowza\Statistics;
use Com\Wowza\Entities\Application\Helpers\Settings;
use Illuminate\Http\Request;
use App\Models\AccountFolderUser;
use App\Models\UserGroup;
use App\Models\AccountFolder;
use App\Models\AccountFolderGroup;
use App\Models\AccountFolderMetaData;
use App\Models\Document;
use App\Models\User;
use App\Models\UserActivityLog;
use App\Models\AccountCommentTag;
use App\Models\UserAccount;
use App\Models\AccountFolderDocument;
use App\Models\Comment;
use App\Models\EmailUnsubscribers;
use App\Models\Sites;
use App\Models\AuditDeletionLog;
use App\Api\Activities\GetActivities;
use Illuminate\Support\Facades\DB;
use Aws\S3\S3Client;
use App\Services\SendGridEmailManager;
use App\Services\HelperFunctions;
use App\Services\S3Services;
use Aws\CloudFront\CloudFrontClient;
use FFMpeg\FFMpeg;
use Datetime;
use FFMpeg\FFProbe\DataMapping\StreamCollection;
use FFMpeg\FFProbe\DataMapping\Stream;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Filters\Video\ResizeFilter;
use App\Services\TranslationsManager;
use Illuminate\Routing\Route;

class HuddleController extends Controller {

    protected $httpreq;
    protected $site_id;
    protected $activities;
    protected $video_per_page;
    protected $current_lang;
    protected $base;
    protected $wowzaUrl;
    protected $route;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request, GetActivities $activities) {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->activities = $activities;
        $this->video_per_page = 12;
        $this->base = config('s3.sibme_base_url');
        $this->current_lang = $this->httpreq->header('current-lang');
        //
        $this->module_url = 'Api/huddle_list';
        $this->wowzaUrl = 'http://18.204.39.27';

    }

    public function get_huddles(Request $request) {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $role_id = $input['role_id'];
        $page = $input['page'];
        $user_current_account = $input['user_current_account'];
        $folder_id = (isset($input['folder_id']) && !empty($input['folder_id'])) ? $input['folder_id'] : false;
        $title = (isset($input['title']) && !empty($input['title'])) ? $input['title'] : "";
        $huddle_sort = (isset($input['huddle_sort']) && !empty($input['huddle_sort'])) ? $input['huddle_sort'] : '0';
        $huddle_type = (isset($input['huddle_type']) && !empty($input['huddle_type'])) ? $input['huddle_type'] : '0';


        if ($folder_id) {

            $folder_data = AccountFolder::where(array('account_folder_id' => $folder_id, 'site_id' => $this->site_id))->first();
            if ($folder_data) {
                $folder_data = $folder_data->toArray();
                if (!(AccountFolder::isUserParticipatingInHuddle($folder_id, $account_id, $user_id, $this->site_id) || $folder_data['created_by'] == $user_id)) {

                    $final_data = array(
                        'message' => TranslationsManager::get_translation('Folder_is_not_accessible', 'Api/huddle_list', $this->site_id),
                        'success' => false
                    );

                    return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                }
            } else {

                $final_data = array(
                    'message' => TranslationsManager::get_translation('Folder_is_not_accessible', 'Api/huddle_list', $this->site_id),
                    'success' => false
                );

                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
            }
        }


        $limit = 10;
        //$limit = 300;

        $final_data = [];

         $accountFolderUsers = AccountFolderUser::join('account_folders', 'AccountFolderUser.account_folder_id', '=', 'account_folders.account_folder_id')
                                                ->selectRaw("AccountFolderUser.*,folder_type,is_published")
                                                ->selectRaw("(SELECT meta_data_value FROM account_folders_meta_data as afmd WHERE afmd.account_folder_id=AccountFolderUser.account_folder_id AND meta_data_name = 'folder_type' LIMIT 1) as htype")
                                                ->where('user_id', $user_id)->where('AccountFolderUser.site_id', $this->site_id)->get();
        //$accountFolderUsers = AccountFolderUser::where('user_id', $user_id)->where('site_id', $this->site_id)->get();
        $accountFolderGroups = UserGroup::get_user_group($user_id, $this->site_id);
        // $userPermissions = AccountFolderUser::where('user_id', $user_id)->where('account_folder_id', $account_id)->get();
         $accountFoldereIds = array();
        //dd($accountFolderUsers);
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
               //$htype =  $this->get_huddle_type($row->account_folder_id);
               //dd($htype);
                // Excluded unpublished assessment huddles for assessees
                if($row->htype == '3' && $row->folder_type == '1' && $row->role_id != '200' &&  $row->is_published == '0') {
                    continue;
                }
                $accountFoldereIds[] = $row->account_folder_id;
            }
            // $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        // $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_id'];
            }
            // $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        if (!empty($title)) {
         $title = str_replace('"', '""', $title);
            $search_bool = 1;
        } else {
            $search_bool = 0;
        }

        $folders = AccountFolder::getAllAccountFolders($this->site_id, $account_id, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, '/root/', $search_bool, $title, $huddle_sort);
        // foreach($folders as $folder){
        //     dd($folder->meta_data_value);
        // }
        // dd($folders);
        $huddles = AccountFolder::getAllAccountHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, $search_bool, $limit, $page, false, $title, $huddle_sort, $huddle_type, $role_id);
        $huddles_count = AccountFolder::getAllAccountHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, $search_bool, $limit, $page, true, $title, $huddle_sort, $huddle_type, $role_id);

        foreach ($huddles as $huddle) {
            // dd($huddle);
            // echo($huddle->name . " <br> ");
        }
        // dd($huddles);

        if ($folders && $page == '1' && $huddle_type == '0') {
            $accessible_folders = array();
            foreach ($folders as $folder) {
                if (AccountFolder::isUserParticipatingInHuddle($folder->account_folder_id, $account_id, $user_id, $this->site_id) || $folder->created_by == $user_id) {
                    $stats = [];
                    // Get Subfolders Count in this Folder
                    $stats ['folders'] = AccountFolder::count_folders_in_folder($folder->account_folder_id, $user_id, $account_id, $this->site_id);
                    // Get Collaboration Huddles Count in this Folder
                    $stats ['collaboration'] = AccountFolder::count_coaching_colab_huddles_in_folder($folder->account_folder_id, $user_id, 1, $this->site_id);
                    // Get Coaching Huddles Count in this Folder
                    $stats ['coaching'] = AccountFolder::count_coaching_colab_huddles_in_folder($folder->account_folder_id, $user_id, 2, $this->site_id);
                    // Get Assessment Huddles Count in this Folder
                    $stats ['assessment'] = AccountFolder::count_coaching_colab_huddles_in_folder($folder->account_folder_id, $user_id, 3, $this->site_id);

                    $folder->stats = $stats;
                    $folder->folder_id = $folder->account_folder_id;
                    $folder->title = $folder->name;
                    $creator = $this->is_creator($folder->created_by, $user_id);

                    $user_permissions = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->first();
                    if ($user_permissions) {
                        $user_permissions = $user_permissions->toArray();
                        if ($creator && ($user_permissions['folders_check'] == '1')) {
                            $folder_permissions = true;
                        } else {
                            $folder_permissions = false;
                        }
                    } else {
                        $folder_permissions = false;
                    }

                    $folder->created_date = self::getCurrentLangDate(strtotime($folder->created_date), "archive_module"); //date('M d, Y', strtotime($folder->created_date));
                    $folder->last_edit_date = self::getCurrentLangDate(strtotime($folder->last_edit_date), "archive_module_folders"); //date('M d, Y', strtotime($folder->last_edit_date));

                    $folder->folder_permissions = $folder_permissions;
                    //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);
                    $accessible_folders[] = $folder;
                    
                }
            }

            $folders = $accessible_folders;
            if ($role_id != 125) {
                $final_data['folders'] = $folders;
                // $total_huddles = $huddles_count;
            }

            
        } else {

            $folders = array();
        }

        if ($huddles) {
            $huddles_temp = array();
            foreach ($huddles as $key => $huddle) {
                // dd($huddle->v_total);
                // echo $huddle->folderType . "<br>";
                $item = array();
                if ($huddle->folderType == 'assessment') {
                    // get total video count
                    if (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id) && $role_id == 120) {
                        $participants_ids = AccountFolderUser::get_participants_ids($huddle->account_folder_id, 210, $this->site_id);
                        $huddles[$key]['total_videos'] = Document::countVideosEvaluator($this->site_id, $huddle->account_folder_id, $user_id, 1, $participants_ids);
                    } elseif (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id) && in_array($role_id, [110, 100, 115])) {
                        $huddles[$key]['total_videos'] = $huddle->v_total;
                    } else {
                        $evaluators_ids = AccountFolderUser::get_participants_ids($huddle->account_folder_id, 200, $this->site_id);
                        $huddles[$key]['total_videos'] = Document::countVideosEvaluator($this->site_id, $huddle->account_folder_id, $user_id, 1, $evaluators_ids);
                    }

                    // get total resources count
                    $total_resources = 0;
                    if (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id)) {
                        //  $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        $huddles[$key]['total_docs'] = $huddle->doc_total;
                        // echo("Docs:". $huddle->doc_total . "<br>");
                    } else {

                        $participants_ids = AccountFolderUser::get_participants_ids($huddle->account_folder_id, 210, $this->site_id);
                        $evaluators_ids = AccountFolderUser::get_participants_ids($huddle->account_folder_id, 200, $this->site_id);
                        // $total_resources = Document::countVideosEvaluator($this->site_id, $huddle->account_folder_id, $user_id, 2, $evaluators_ids);
                        $total_resources = Document::countVideosEvaluator($this->site_id, $huddle->account_folder_id, $user_id, 2);
                        $huddles[$key]['total_docs'] = $total_resources;
                        /*
                          if ($total_participant_videos) {
                          foreach ($total_participant_videos as $row) {

                          if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {

                          $total_count_r = count($this->Document->getVideoDocumentsByVideo($row['Document']['id'], $huddles[$i]['AccountFolder']['account_folder_id']));


                          $total_resources = $total_resources + $total_count_r;
                          }
                          }
                          }
                         */

// echo("Resources:".__LINE__. " - " . $total_resources)."<br>";
/*
                        if (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id)) {
                            $huddles[$key]['total_docs'] = $total_resources;
                        } else {
                            // $huddles[$key]['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                            $huddles[$key]['total_docs'] = Document::countAsseseeDocuments($huddle->account_folder_id, $this->site_id);
                        }
*/
                    }
                } else {
                    $huddles[$key]['total_videos'] = $huddle->v_total;



                    //  $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    if (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id)) {
                        $huddles[$key]['total_docs'] = $huddle->doc_total;
                    } else {
                        // $huddles[$key]['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        $huddles[$key]['total_docs'] = Document::countAsseseeDocuments($huddle->account_folder_id, $this->site_id);
                    }
                }

                $huddle_users = AccountFolder::getHuddleUsers($huddle->account_folder_id, $this->site_id);              
               
                $huddle_group_users = AccountFolder::getHuddleGroupsUsers($huddle->account_folder_id, $this->site_id);

                $participants_arranged = array();

                if (!empty($huddle_groups)) {

                    foreach ($huddle_groups as $huddle_group) {

                        $participants_arranged[] = array(
                            'user_id' => $huddle_group['id'],
                            'role_id' => $huddle_group['role_id'],
                            'user_name' => $huddle_group['first_name'] . ' ' . $huddle_group['last_name'],
                            'image' => $huddle_group['image'],
                            'user_email' => $huddle_group['email']
                        );
                    }
                }

                if (!empty($huddle_users)) {

                    foreach ($huddle_users as $huddle_user) {
                        $participants_arranged[] = array(
                            'user_id' => $huddle_user['user_id'],
                            'role_id' => $huddle_user['role_id'],
                            'user_name' => $huddle_user['first_name'] . ' ' . $huddle_user['last_name'],
                            'image' => $huddle_user['image'],
                            'user_email' => $huddle_user['email']
                        );
                    }

                    $participants = array();
                    $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', $this->module_url, $this->site_id, $user_id);
                    $huddle_name = 'collaboration';
                    if ($huddle->folderType == 'assessment') {
                        $role_200 = 'assessor';
                        $role_210 = 'assessed_participants';
                        $role_220 = '';
                        $huddle_name_type = TranslationsManager::get_translation('huddle_list_assessment', $this->module_url, $this->site_id, $user_id);
                        $huddle_name = 'assessment';
                    } elseif ($huddle->folderType == 'coaching') {

                        $role_200 = 'coach';
                        $role_210 = 'coachee';
                        $role_220 = '';
                        $huddle_name_type = TranslationsManager::get_translation('coaching_list_coaching', $this->module_url, $this->site_id, $user_id);
                        $huddle_name = 'coaching';
                    } else {

                        $role_200 = 'collaboration_participants';
                        $role_210 = 'collaboration_participants';
                        $role_220 = 'collaboration_participants';
                        $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', $this->module_url, $this->site_id, $user_id);
                        $huddle_name = 'collaboration';
                    }

                    foreach ($participants_arranged as $row) {
                        if ($row['role_id'] == '200') {
                            $row['role_name'] = 'Admin';
                            $participants[$role_200][] = $row;
                        } elseif ($row['role_id'] == '210') {
                            $row['role_name'] = 'Member';
                            $participants[$role_210][] = $row;
                        } else {
                            $row['role_name'] = 'Viewer';
                            $participants[$role_220][] = $row;
                        }
                    }
                }

                // $huddle_participants = AccountFolder::getHuddleUsersIncludingGroups($huddle->account_folder_id, $this->site_id, $user_id, $role_id, true);

                $stats = array();

                $user_huddle_role = $this->has_admin_access($huddle_users, $huddle_group_users, $user_id);
                $creator = $this->is_creator($huddle->created_by, $user_id);

                if ($user_huddle_role == 200 || $creator) {
                    $huddle_permissions = true;
                } else {
                    $huddle_permissions = false;
                }

                $stats['videos'] = $huddles[$key]['total_videos'];
                $stats['attachments'] = $huddles[$key]['total_docs'];


                $huddle->participants = $participants; //$huddle_participants['participants'];
                $huddle->stats = $stats;
                $huddle->type = $huddle_name; //$huddle_participants['huddle_name'];
                $huddle->huddle_type = $huddle_name_type; //$huddle_participants['huddle_name_type'];
                $huddle->title = $huddles[$key]['name'];
                $huddle->created_on = self::getCurrentLangDate(strtotime($huddles[$key]['created_date']), "archive_module"); //date('M d, Y', strtotime($huddles[$key]['created_date']));
                $huddle->last_edit_date = self::getCurrentLangDate(strtotime($huddles[$key]['last_edit_date']), "archive_module"); //date('M d, Y', strtotime($huddles[$key]['last_edit_date']));
                $huddle->created_by = $this->get_user_name_from_id($huddles[$key]['created_by']);
                $huddle->huddle_id = $huddles[$key]['account_folder_id'];
                $huddle->huddle_permissions = $huddle_permissions;
                $huddle->role_id = $this->get_huddle_role($huddle->account_folder_id, $user_id);

                $huddles_temp[] = $huddle;
            }

            $huddles = $huddles_temp;

            $folder_create_permission = $this->folder_create_permission($user_current_account);
            $huddle_create_permission = $this->huddle_create_permission($user_current_account);

            $final_data['huddles'] = $huddles;
            $final_data['total_huddles'] = $huddles_count;
            $final_data['folder_create_permission'] = $folder_create_permission;
            $final_data['huddle_create_permission'] = $huddle_create_permission;
            //$final_data['folders_count_for_move'] = $this->treeview_detail_function($account_id, $user_id);
            $final_data['folders_count_for_move'] =  2;
            $final_data['success'] = true;



            // dd("....Ending....",$huddles);
            /*
              $all_huddles = array();
              if ($huddles && $role_id == 125) {
              foreach ($huddles as $row) {
              if ($row[0]['folderType'] == 'collaboration' || $row[0]['folderType'] == '') {
              $all_huddles[] = $row;
              }
              }
              } else {
              $all_huddles = $huddles;
              }

             */
            /*
              if ($role_id == 125) {
              $total_huddles = $all_huddles;
              }
             */
        }

        return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
    }

    function get_user_name_from_id($user_id) {
        $result = User::where(array(
                    'id' => $user_id, 'site_id' => $this->site_id))
                ->first();

        if ($result) {
            return $result->first_name . ' ' . $result->last_name;
        } else {
            $empty_name = '';
            return $empty_name;
        }
    }

    public function treeview_detail(Request $request) {

        $account_id = $request->account_id;
        $user_id = $request->user_id;
        $id = isset($request->id) && !empty($request->id) ? $request->id : "";
//        $account_id = '2936';
//        $user_id = '18737';
//        $id = '';
        $folders = array();
        $folders = AccountFolder::where(array(
                            'account_id' => $account_id, 'active' => '1', 'folder_type' => '5', 'site_id' => $this->site_id))
                        ->get()->toArray();

        $afolder = array();
        if ($folders) {
            $ids_arr = array();
            foreach ($folders as $folder) {
                if (AccountFolder::isUserParticipatingInHuddle($folder['account_folder_id'], $account_id, $user_id, $this->site_id) || $folder['created_by'] == $user_id) {
                    array_push($ids_arr, $folder['account_folder_id']);
                }
            }
            $accessible_folders = array();
            foreach ($folders as $folder) {

                if (AccountFolder::isUserParticipatingInHuddle($folder['account_folder_id'], $account_id, $user_id, $this->site_id) || $folder['created_by'] == $user_id) {
                    if (in_array($folder['parent_folder_id'], $ids_arr)) {
                        $afolder['id'] = $folder['account_folder_id'];
                        $afolder['text'] = $folder['name'];
                        $afolder['parent'] = $folder['parent_folder_id'];
                        $accessible_folders[] = $afolder;
                    } else {
                        $afolder['id'] = $folder['account_folder_id'];
                        $afolder['text'] = $folder['name'];
                        $afolder['parent'] = '#';
                        $accessible_folders[] = $afolder;
                    }
                }
            }

            $folders = $accessible_folders;
        }

        $data = $folders;

        foreach ($data as $key => &$item) {

            if ($item['parent'] == null || empty($item['parent']))
                $item['parent'] = '#';
        }
        if ($id != '') {

            $folder_childs_ids = array();
            $new_data = array();

            foreach ($data as $dat) {
                if ($dat['id'] != $id && !in_array($dat['id'], $folder_childs_ids)) {
                    $new_data[] = $dat;
                }
            }

            $dat = array(
                'id' => '-1',
                'text' => TranslationsManager::get_translation('huddle_delete_btn_list', 'Api/huddle_list', $this->site_id),
                'parent' => '#');

            array_unshift($new_data, $dat);


            return response()->json($new_data, 200, [], JSON_PRETTY_PRINT);
        } else {

            $dat = array(
                'id' => '-1',
                'text' => TranslationsManager::get_translation('huddle_delete_btn_list', 'Api/huddle_list', $this->site_id),
                'parent' => '#');

            array_unshift($data, $dat);

            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        }
    }

    public function move_huddle_folder(Request $request) {

        $huddle_id = $request->huddle_id;
        $folder_id = $request->folder_id;

        $get_current_directory = AccountFolder::where(array('account_folder_id' => $huddle_id, 'site_id' => $this->site_id))->first()->toArray();

        if ($folder_id != '-1') {

            $get_destination_folder = AccountFolder::where(array('account_folder_id' => $folder_id, 'site_id' => $this->site_id))->first()->toArray();

            if ($get_destination_folder['parent_folder_id'] == $huddle_id) {

                $data = array(
                    'success' => false,
                    'message' => TranslationsManager::get_translation('Destination_folder_is_inside_the_folder', 'Api/huddle_list', $this->site_id)
                );

                return response()->json($data, 200, [], JSON_PRETTY_PRINT);
            }
        }

        if (empty($get_current_directory['parent_folder_id']) && $folder_id == '-1') {
            $data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Huddle_Folder_is_already_in_this_Folder', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        }

        if ($get_current_directory['parent_folder_id'] == $folder_id) {

            $data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Huddle_Folder_is_already_in_this_Folder', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        }

        $return = $this->save_huddle_to_new_folder($huddle_id, $folder_id);

        if ($return) {
            $data = array(
                'success' => true,
                'message' => TranslationsManager::get_translation('huddle_moved_successfully', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        } else {
            $data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Huddle_not_moved', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        }
    }

    public function save_huddle_to_new_folder($huddle_id, $new_folder_id) {
        if ($huddle_id) {

            if ($new_folder_id == '-1') {
                $new_folder_id = NULL;
            }

            DB::table('account_folders')->where(['account_folder_id' => $huddle_id, 'site_id' => $this->site_id])->update(['parent_folder_id' => $new_folder_id]);

            return TRUE;
        } else {
            return FALSE;
        }
    }

    function create_folder(Request $request) {

        $input = $request->all();
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];


        if (!empty($input['folder_id'])) {

            $result = AccountFolder::where(array('folder_type' => '5', 'site_id' => $this->site_id, 'parent_folder_id' => $input['folder_id'], 'active' => '1', 'name' => $input['folder_name'], 'account_id' => $input['account_id']))
                    ->first();

            if ($result) {
                $result = $result->toArray();
            }


            if (!empty($result)) {
                $json_data = array(
                    'success' => false,
                    'message' => TranslationsManager::get_translation('Folder_with_this_name_already_exists', 'Api/huddle_list', $this->site_id)
                );
                return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
            }
        } else {

            $result = AccountFolder::where(array('folder_type' => '5', 'site_id' => $this->site_id, 'parent_folder_id' => NULL, 'active' => '1', 'name' => $input['folder_name'], 'account_id' => $input['account_id']))
                    ->first();

            if ($result) {
                $result = $result->toArray();
            }



            if (!empty($result)) {
                $json_data = array(
                    'success' => false,
                    'message' => TranslationsManager::get_translation('Folder_with_this_name_already_exists', 'Api/huddle_list', $this->site_id)
                );
                return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
            }
        }

        if (!empty($input['folder_id'])) {
            $data = array(
                'name' => trim($input['folder_name']),
                'desc' => '',
                'created_date' => date("Y-m-d H:i:s"),
                'last_edit_date' => date("Y-m-d H:i:s"),
                'created_by' => $input['user_id'],
                'last_edit_by' => $input['user_id'],
                'folder_type' => 5,
                'parent_folder_id' => $input['folder_id'],
                'active' => 1,
                'account_id' => $input['account_id'],
                'site_id' => $this->site_id
            );
        } else {
            $data = array(
                'name' => trim($input['folder_name']),
                'desc' => '',
                'created_date' => date("Y-m-d H:i:s"),
                'last_edit_date' => date("Y-m-d H:i:s"),
                'created_by' => $input['user_id'],
                'last_edit_by' => $input['user_id'],
                'folder_type' => 5,
                'active' => 1,
                'account_id' => $input['account_id'],
                'site_id' => $this->site_id
            );
        }


        $account_folder_id = DB::table('account_folders')->insertGetId($data);

        if (!empty($account_folder_id)) {

            $user_activity_logs = array(
                'ref_id' => $account_folder_id,
                'desc' => $input['folder_name'],
                'url' => '/Folder/' . $account_folder_id,
                'account_folder_id' => $account_folder_id,
                'date_added' => date("Y-m-d H:i:s"),
                'type' => '1',
                'site_id' => $this->site_id
            );
            DB::table('user_activity_logs')->insertGetId($user_activity_logs);

            $created_folder_object = AccountFolder::where(array('account_folder_id' => $account_folder_id, 'site_id' => $this->site_id))
                    ->first();

            $arranged_folder_object = $created_folder_object;

            $stats = [];
            // Get Subfolders Count in this Folder
            $stats ['folders'] = AccountFolder::count_folders_in_folder($created_folder_object->account_folder_id, $user_id, $account_id, $this->site_id);
            // Get Collaboration Huddles Count in this Folder
            $stats ['collaboration'] = AccountFolder::count_coaching_colab_huddles_in_folder($created_folder_object->account_folder_id, $user_id, 1, $this->site_id);
            // Get Coaching Huddles Count in this Folder
            $stats ['coaching'] = AccountFolder::count_coaching_colab_huddles_in_folder($created_folder_object->account_folder_id, $user_id, 2, $this->site_id);
            // Get Assessment Huddles Count in this Folder
            $stats ['assessment'] = AccountFolder::count_coaching_colab_huddles_in_folder($created_folder_object->account_folder_id, $user_id, 3, $this->site_id);

            $arranged_folder_object->stats = $stats;
            $arranged_folder_object->folder_id = $created_folder_object->account_folder_id;
            $arranged_folder_object->title = $created_folder_object->name;
            $arranged_folder_object->created_by_name = $this->get_user_name_from_id($created_folder_object->created_by);

            $json_data = array(
                'success' => true,
                'message' => TranslationsManager::get_translation('huddle_folder_created_successfully', 'Api/huddle_list', $this->site_id),
                'folder_object' => $arranged_folder_object
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        } else {

            $json_data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Folder_not_created', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }

        die;
    }

    function edit_folder(Request $request) {




        $input = $request->all();
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];
        $folder_id = $input['folder_id'];


        if (!empty($input['parent_folder_id'])) {

            $result = AccountFolder::where(array('folder_type' => '5', 'site_id' => $this->site_id, 'parent_folder_id' => $input['parent_folder_id'], 'active' => '1', 'name' => $input['folder_name'], 'account_id' => $input['account_id']))
                    ->first();

            if ($result) {
                $result = $result->toArray();
            }


            if (!empty($result)) {
                $json_data = array(
                    'success' => false,
                    'message' => TranslationsManager::get_translation('Folder_with_this_name_already_exists', 'Api/huddle_list', $this->site_id)
                );
                return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
            }
        } else {


            $result = AccountFolder::where(array('folder_type' => '5', 'site_id' => $this->site_id, 'parent_folder_id' => NULL, 'active' => '1', 'name' => $input['folder_name'], 'account_id' => $input['account_id']))
                    ->first();

            if ($result) {
                $result = $result->toArray();
            }

            if (!empty($result)) {
                $json_data = array(
                    'success' => false,
                    'message' => TranslationsManager::get_translation('Folder_with_this_name_already_exists', 'Api/huddle_list', $this->site_id)
                );
                return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
            }
        }

        $rows = DB::table('account_folders')->where(['account_folder_id' => $folder_id, 'site_id' => $this->site_id])->update(['name' => trim($input['folder_name']), 'last_edit_date' => date("Y-m-d H:i:s")]);


        if ($rows > 0) {
            $edited_result = AccountFolder::where(array('account_folder_id' => $folder_id, 'site_id' => $this->site_id))
                    ->first();

            if ($edited_result) {
                $edited_result = $edited_result->toArray();
                $last_edit_date = self::getCurrentLangDate(strtotime($edited_result['last_edit_date']), "archive_module_folders_date");
            }
            else {
                $last_edit_date = ''; 
            }
            $json_data = array(
                'success' => true,
                'last_edit_date' => $last_edit_date,
                'message' => TranslationsManager::get_translation('Folder_updated_successfully', 'Api/huddle_list', $this->site_id)
            );

            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        } else {
            $json_data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Folder_not_updated', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }
    }

    function delete_folder(Request $request) {
        $input = $request->all();
        $folder_id = $input['folder_id'];


        $result = AccountFolder::where(array('parent_folder_id' => $folder_id, 'active' => '1', 'site_id' => $this->site_id))
                ->whereIn('folder_type', array('1', '5'))
                ->get()
                ->toArray();

        if (!empty($result)) {
            $json_data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Folder_cannot_be_deleted_because_its_not_empty', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }


        $data = array(
            'active' => 0
        );

        $rows = DB::table('account_folders')->where(['account_folder_id' => $folder_id])->update($data);


        if ($rows > 0) {
            $json_data = array(
                'success' => true,
                'message' => TranslationsManager::get_translation('Folder_deleted_successfully', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        } else {
            $json_data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Folder_not_deleted', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }
    }

    function delete_huddle(Request $request) {
        $input = $request->all();
        $huddle_id = $input['huddle_id'];
        $user_id = !empty($input['user_id'])?$input['user_id']:'';
        $data = array(
            'active' => 0
        );
        $result = AccountFolder::where(array('account_folder_id' =>$huddle_id))->first();

        if ($result) {
            $result = $result->toArray();
        }
       
        $rows = DB::table('account_folders')->where(['account_folder_id' => $huddle_id, 'site_id' => $this->site_id])->update($data);

        if ($rows > 0) {
            $json_data = array(
                'success' => true,
                'message' => TranslationsManager::get_translation('Huddle_deleted_successfully', 'Api/huddle_list', $this->site_id)
            );
            $event = [
                'channel' => "publish_channel",
                'event' => "huddle_deleted",
                'data' => $huddle_id,
                'document_id' => $huddle_id,
                'huddle_id' => $huddle_id,
            ];
            HelperFunctions::broadcastEvent($event);
            $result = array(
                'entity_id'=>'',
                'deleted_entity_id'=>$huddle_id,
                'entity_name'=>$result['name'],
                'entity_data'=>json_encode($result),
                'created_by'=>$user_id,
                'created_at'=> date("Y-m-d H:i:s"),
                'site_id'=>$this->site_id,
                'ip_address'=>$_SERVER['REMOTE_ADDR'],
                'posted_data'=>json_encode($input),
                'action'=> $_SERVER['REQUEST_URI'],
                'user_agenet'=>$_SERVER['HTTP_USER_AGENT'],
                'request_url'=>$_SERVER['REQUEST_URI'],
                'platform'=>'',
                'created_date' => date("Y-m-d H:i:s"),
            );
            DB::table('audit_deletion_log')->insert($result);    
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);

        } else {
            $json_data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Huddle_not_deleted', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }
       

    }

    function get_bread_crumb(Request $request) {

        $input = $request->all();
        $folder_id = (isset($input['folder_id'])) ? $input['folder_id'] : false;
        $user_id = (isset($input['user_id'])) ? $input['user_id'] : false;
        $account_id = (isset($input['account_id'])) ? $input['account_id'] : false;
        $folder_data = AccountFolder::where(array('account_folder_id' => $folder_id, 'site_id' => $this->site_id))->first();
        if(!$folder_data)
        {
            $final_data = array(
                'message' => 'Folder not found',
                'success' => '-1'
            );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }
        else
        {
            $folder_data = $folder_data->toArray();
        }
        if ($folder_data['folder_type'] != 5) {
            if (!empty(AccountFolderUser::user_present_huddle($user_id, $folder_id))) {
                $value = app('App\Http\Controllers\VideoController')->bread_crumb_output($folder_id);

                $folders = AccountFolder::where(array('account_folder_id' => $folder_id, 'site_id' => $this->site_id))
                                ->first()->toArray();

                $new_array = array(
                    'folder_id' => $folders['account_folder_id'],
                    'folder_name' => $folders['name'],
                    'location' => 'Huddle'
                );

                if (!empty($value)) {
                    array_push($value, $new_array);

                    return $value;
                } else {
                    $result = array();
                    $result[] = $new_array;
                    return $result;
                }
            } else {
                $final_data = array(
                    'message' => 'Folder is not accessible',
                    'success' => '-1'
                );

                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
            }
        } else {
            if ($folder_id && $user_id && $account_id) {

                $folder_data = AccountFolder::where(array('account_folder_id' => $folder_id, 'site_id' => $this->site_id))->first();

                if ($folder_data) {

                    $folder_data = AccountFolder::where(array('account_folder_id' => $folder_id, 'site_id' => $this->site_id))->first()->toArray();

                    if (!(AccountFolder::isUserParticipatingInHuddle($folder_id, $account_id, $user_id, $this->site_id) || $folder_data['created_by'] == $user_id)) {

                        $final_data = array(
                            'message' => 'Folder is not accessible',
                            'success' => '-1'
                        );

                        return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                    }
                } else {
                    $final_data = array(
                        'message' => TranslationsManager::get_translation('Folder_is_not_accessible', 'Api/huddle_list', $this->site_id),
                        'success' => '-1'
                    );

                    return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                }

                if ($folder_id) {

                    $value = app('App\Http\Controllers\VideoController')->bread_crumb_output($folder_id);

                    $folders = AccountFolder::where(array('account_folder_id' => $folder_id, 'site_id' => $this->site_id))
                                    ->first()->toArray();

                    $new_array = array(
                        'folder_id' => $folders['account_folder_id'],
                        'folder_name' => $folders['name']
                    );

                    if (!empty($value)) {
                        array_push($value, $new_array);

                        return $value;
                    } else {
                        $result = array();
                        $result[] = $new_array;
                        return $result;
                    }
                } else {
                    $result = array();
                    return $result;
                }
            }
        }
    }

    public static function has_admin_access($huddleUsers, $account_folder_groups, $user_id) {
        $huddle_role = FALSE;
        if ($huddleUsers && count($huddleUsers) > 0 || $account_folder_groups && count($account_folder_groups) > 0 && $user_id != '') {
            foreach ($huddleUsers as $huddle_user) {
                if (isset($huddle_user) && $huddle_user['user_id'] == $user_id) {
                    $huddle_role = $huddle_user['role_id'];
                    return $huddle_role;
                } else {
                    $huddle_role = FALSE;
                }
            }
            if ($huddle_role != '') {
                return $huddle_role;
            } else {
                if ($account_folder_groups && count($account_folder_groups) > 0) {
                    foreach ($account_folder_groups as $groups) {

                        if (isset($groups['id']) && $groups['id'] == $user_id) {
                            $huddle_role = $groups['role_id'];
                            return $huddle_role;
                        } else {
                            $huddle_role = FALSE;
                        }
                    }
                } else {
                    return FALSE;
                }
            }
        } else {

            return FALSE;
        }
    }

    function is_creator($created_by_id, $user_id) {
        if ($created_by_id == $user_id) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function folder_create_permission($user_current_account) {
        if (( ($user_current_account['roles']['role_id'] == '120' && $user_current_account['users_accounts']['folders_check'] == '1' ) || ($user_current_account['roles']['role_id'] == '110') || ($user_current_account['roles']['role_id'] == '100' ) || ($user_current_account['roles']['role_id'] == '115' && $user_current_account['users_accounts']['folders_check'] == '1'))) {
            return true;
        } else {
            return false;
        }
    }

    function huddle_create_permission($user_current_account) {
        if (($user_current_account['roles']['role_id'] != '120' && $user_current_account['roles']['role_id'] != '125') || $user_current_account['users_accounts']['manage_collab_huddles'] == '1' || $user_current_account['users_accounts']['manage_coach_huddles'] == '1' || $user_current_account['users_accounts']['manage_evaluation_huddles'] == '1') {
            return true;
        } else {
            return false;
        }
    }

    function get_huddle_role($huddle_id, $user_id) {
        $huddle_role = AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'user_id' => $user_id,
                    'site_id' => $this->site_id
                ))
                ->first();

        if ($huddle_role) {
            $huddle_role = $huddle_role->toArray();
            return $huddle_role['role_id'];
        } else {
            return 0;
        }
    }

    public function treeview_detail_function($account_id, $user_id, $id = '') {

        $folders = array();
        $folders = AccountFolder::where(array(
                            'account_id' => $account_id, 'active' => '1', 'folder_type' => '5', 'site_id' => $this->site_id))
                        ->get()->toArray();

        $afolder = array();
        if ($folders) {
            $ids_arr = array();
            foreach ($folders as $folder) {
                if (AccountFolder::isUserParticipatingInHuddle($folder['account_folder_id'], $account_id, $user_id, $this->site_id) || $folder['created_by'] == $user_id) {
                    array_push($ids_arr, $folder['account_folder_id']);
                }
            }
            $accessible_folders = array();
            foreach ($folders as $folder) {

                if (AccountFolder::isUserParticipatingInHuddle($folder['account_folder_id'], $account_id, $user_id, $this->site_id) || $folder['created_by'] == $user_id) {
                    if (in_array($folder['parent_folder_id'], $ids_arr)) {
                        $afolder['id'] = $folder['account_folder_id'];
                        $afolder['text'] = $folder['name'];
                        $afolder['parent'] = $folder['parent_folder_id'];
                        $accessible_folders[] = $afolder;
                    } else {
                        $afolder['id'] = $folder['account_folder_id'];
                        $afolder['text'] = $folder['name'];
                        $afolder['parent'] = '#';
                        $accessible_folders[] = $afolder;
                    }
                }
            }

            $folders = $accessible_folders;
        }

        $data = $folders;

        foreach ($data as $key => &$item) {

            if ($item['parent'] == null || empty($item['parent']))
                $item['parent'] = '#';
        }
        if ($id != '') {

            $folder_childs_ids = array();
            $new_data = array();

            foreach ($data as $dat) {
                if ($dat['id'] != $id && !in_array($dat['id'], $folder_childs_ids)) {
                    $new_data[] = $dat;
                }
            }

            $dat = array(
                'id' => '-1',
                'text' => TranslationsManager::get_translation('huddle_delete_btn_list', 'Api/huddle_list', $this->site_id),
                'parent' => '#');

            array_unshift($new_data, $dat);


            return count($new_data);
        } else {

            $dat = array(
                'id' => '-1',
                'text' => TranslationsManager::get_translation('huddle_delete_btn_list', 'Api/huddle_list', $this->site_id),
                'parent' => '#');

            array_unshift($data, $dat);

            return count($data);
        }
    }

    function get_huddles_actvities(Request $request) {
        $user_id = $request->get('user_id');
        $account_id = $request->get('account_id');
        $folder_id = $request->get('folder_id');
        $role_id = $request->get('role_id');
        $data = UserActivityLog::get_huddle_activity($this->site_id, $account_id, $folder_id);

        $data = $this->convert_array($data);

        $final_data = $this->activities->get_ctivities($data, $user_id, $this->site_id, $account_id, $role_id);
        return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
    }

    /*
      public static function get_evaluator_ids($huddle_id) {
      $site_id = app("Illuminate\Http\Request")->header("site_id");

      $result = AccountFolderUser::where(array(
      'role_id' => 200,
      'site_id' => $site_id,
      'account_folder_id' => $huddle_id
      ))->get()->toArray();

      $evaluator = array();
      if ($result) {
      foreach ($result as $row) {
      $evaluator[] = $row['user_id'];
      }
      return $evaluator;
      } else {
      return FALSE;
      }
      }
     */

    function get_huddle_type($huddle_id) {
        $htype = AccountFolderMetaData::where('account_folder_id', $huddle_id)
                ->where('meta_data_name', 'folder_type')
                ->where('site_id', $this->site_id)
                ->first();
        if ($htype) {
            $htype->toArray();
            return $htype['meta_data_value'];
        } else {
            return 1;
        }
    }

    function get_huddle_info($huddle_id) {
        $htypeinfo = AccountFolder::where('account_folder_id', $huddle_id)
                ->where('active', '1')
                ->where('site_id', $this->site_id)
                ->get()
                ->toArray();
        return $htypeinfo;
    }

    function get_artifects(Request $request) {
        $input = $request->all();
        $user_id = @$input['user_id'];
        $account_id = @$input['account_id'];
        $huddle_id = @$input['huddle_id'];
        $role_id = @$input['role_id'];
        $title = @$input['title'];
        $page = @$input['page'];
        $sort = @$input['sort'];
        $doc_type = @$input['doc_type'];
        $get_all_comments = @$input['get_all_comments'];
        $only_user_artifacts = $request->has("only_user_artifacts") ? $request->get("only_user_artifacts") : 0;
        $send_remaining_artifact = $request->has("send_remaining_artifact") ? $request->get("send_remaining_artifact") : 0;
        $support_audio_annotation = isset($input['support_audio_annotation']) ? $input['support_audio_annotation'] : 1;
        // $is_user_participant = AccountFolderUser::has_participant($huddle_id, $user_id, $this->site_id);
        $is_user_participant = AccountFolderUser::user_present_huddle($user_id, $huddle_id);
        $is_mobile_request = false;
        $default_photo = isset($input['use_default_photo']) ? $input['use_default_photo'] : 1;
        /*
        if (!empty($input['app_name']) && ($input['app_name'] == 'com.sibme.mobileapp' || $input['app_name'] == 'com.sibme.sibme')) {
            $is_mobile_request = true;
        } else {
            $is_mobile_request = false;
        }
        */
        $is_mobile_request = isset($input['app_name']) ? HelperFunctions::is_mobile_request($input['app_name']) : false;
        $htypeinfo = $this->get_huddle_info($huddle_id);
        if (empty($htypeinfo) || empty($is_user_participant)) {
            return response()->json(["success" => false, "message" => "You are not participating in this huddle"], 200, [], JSON_PRETTY_PRINT);
        }
        if($htypeinfo[0]["account_id"] != $account_id)
        {
            //return response()->json(["success" => false, "message" => "Switch Account", "switch_account" => $htypeinfo[0]["account_id"]], 200, [], JSON_PRETTY_PRINT);
        }
        $item = array();
        $htype = $this->get_huddle_type($huddle_id);
        $htypes = ($htype == 3) ? [1, 2] : [1, 2, 3, 4];
        if (!empty($doc_type) && $doc_type != 0) {
            $htypes = [$doc_type];
            if($doc_type == 1)
            {
                $htypes[] = 4;//when video is selected live stream should show
            }
        }
        $document_ids = array();
        $evaluator = AccountFolderUser::check_if_evalutor($this->site_id, $huddle_id, $user_id);
        //$documents = $this->get_documents($huddle_id, $user_id, $htype, $title);
        if (!$evaluator && $htype == '2') {
            $document_ids = Document::getCoacheeDocuments($this->site_id, $huddle_id, $title);
        }
        
        if($request->has("turn_on_pagination"))
        {
           $is_mobile_request = false; 
        }

        if (!$evaluator && $htype == 3) {
            $limit = $this->video_per_page;
            if ($only_user_artifacts) {
                $limit = 0;
                $evaluator_ids = $user_id; //only user videos
                if($request->has("logged_in_user_id")){
                    $user_id = $request->get("logged_in_user_id");
                }
            } else {
                // $evaluator_ids = $this->get_evaluator_ids($huddle_id);
                $evaluator_ids = HelperFunctions::get_evaluator_ids($huddle_id, 200);
                array_push($evaluator_ids, $user_id);
            }
            $all_videos = Document::getArtifects($this->site_id, $huddle_id, $title, $sort, $limit, $page, $evaluator_ids, $htypes, '', $htype, $is_mobile_request);
            $total_records = $all_videos["total_records"];
            $all_videos = $all_videos["data"];
        } else {
            $all_videos = Document::getArtifects($this->site_id, $huddle_id, $title, $sort, $this->video_per_page, $page, '', $htypes, $document_ids, $htype, $is_mobile_request);
            $total_records = $all_videos["total_records"];
            $all_videos = $all_videos["data"];
        }


        if ($all_videos) {
            foreach ($all_videos as $row) {
                $created_by = $row['created_by'];
                if ($this->_check_evaluator_permissions($huddle_id, $created_by, $user_id, $role_id) == false) {
                    continue;
                } else {
                    $item[] = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($row, 1);
                }
            }
        }

        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['total_comments'] = HelperFunctions::get_video_comment_numbers($item[$j]['doc_id'], $huddle_id, $user_id);
                //$item[$j]['total_comments'] = Comment::comments_count($item[$j]['doc_id'], $this->site_id);
                //$item[$j]['total_attachment'] = Document::getDocumentsCount($item[$j]['doc_id'], $this->site_id);
                $if_evaluator = HelperFunctions::check_if_evalutor($huddle_id, $user_id);
                $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
                $is_coach_enable = HelperFunctions::is_enabled_coach_feedback($huddle_id);
                $item[$j]['total_attachment'] = count(Document::getVideoDocumentsByVideo($item[$j]['doc_id'], $huddle_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $this->site_id));
                if($h_type==3){
                    $assessor_ids = AccountFolderUser::get_participants_ids($huddle_id, 200, $this->site_id);
                    $item[$j]['assessed'] = $this->check_video_assessed($item[$j]['doc_id'], $assessor_ids);
                }
                if (!AccountFolderUser::check_if_evalutor($this->site_id, $huddle_id, $user_id) && (($htype == '2' && $this->is_enabled_coach_feedback($huddle_id, $this->site_id)) || $htype == '3')) {
                    $comments_res = $this->getVideoComments($item[$j]['doc_id'], '', '', '', '', $support_audio_annotation, 1, false, '', true);
                } else {
                    $comments_res = $this->getVideoComments($item[$j]['doc_id'], '', '', '', '', $support_audio_annotation);
                }

                $comment_result = array();

                foreach ($comments_res as $row) {
                    if ($row['ref_type'] == 6) {
                        if (config('use_cloudfront') == true) {
                            $url = $row['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];
                            if ($videoFilePath['dirname'] == ".") {

                                $video_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . ".m4a");
                                $relative_video_path = $videoFileName . ".m4a";
                            } else {

                                $video_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".m4a");
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".m4a";
                            }
                        } else {
                            $url = $row['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];

                            if ($videoFilePath['dirname'] == ".") {

                                $video_path = $this->getSecureAmazonUrl($videoFileName . ".m4a");
                                $relative_video_path = $videoFileName . ".m4a";
                            } else {

                                $video_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName . ".m4a");
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".m4a";
                            }
                        }
                        $row['comment'] = $video_path;
                    }
                    $created_date = new DateTime($row['created_date']);
                    $last_edit_date = new DateTime($row['last_edit_date']);

                    $row['last_edit_date'] = $last_edit_date->format(DateTime::W3C);
                    $row['created_date'] = $created_date->format(DateTime::W3C);

                    if (isset($row['image']) && $row['image'] != '') {
                        $use_folder_imgs = config('use_local_file_store');
                        if ($use_folder_imgs == false) {
                            $file_name = "static/users/" . $row['id'] . "/" . $row['image'];
                            $row['image'] = config('amazon_base_url') . config('bucket_name_cdn') . "/" . $file_name;
                        }

                        //$item['User']['image'] = $sibme_base_url . '/img/users/' . $item['User']['image'];
                        //else
                        //$item['User']['image'] = $view->Custom->getSecureSibmecdnUrl("static/users/" . $item['User']['id'] . "/" . $item['User']['image']);
                    } else {
                        if ($default_photo == 1) {
                            $row['image'] = config('sibme_base_url') . '/img/home/photo-default.png';
                        }
                    }

                    $commentDate = $row['created_date'];
                    $currentDate = date('Y-m-d H:i:s');

                    $start_date = new DateTime($commentDate);
                    $since_start = $start_date->diff(new DateTime($currentDate));

                    $commentsDate = '';
                    if ($since_start->y > 0) {
                        $commentsDate = "About " . $since_start->y . ($since_start->y > 1 ? ' Years Ago' : ' Year Ago');
                    } elseif ($since_start->m > 0) {
                        $commentsDate = "About " . $since_start->m . ($since_start->m > 1 ? ' Months Ago' : ' Month Ago');
                    } elseif ($since_start->d > 0) {
                        $commentsDate = $since_start->d . ($since_start->d > 1 ? ' Days Ago' : ' Day Ago');
                    } elseif ($since_start->h > 0) {
                        $commentsDate = $since_start->h . ($since_start->h > 1 ? ' Hours Ago' : ' Hour Ago');
                    } elseif ($since_start->i > 0) {
                        $commentsDate = $since_start->i . ($since_start->i > 1 ? ' Minutes Ago' : ' Minute Ago');
                    } elseif ($since_start->s > 0) {
                        $commentsDate = 'Less Than A Minute Ago';
                    } else {
                        $commentsDate = 'Just Now';
                    }

                    $row['Comment']['created_date_string'] = $commentsDate;

                    $response = $this->get_replies_from_comment($row, $get_all_comments);
                    $row['attachment_count'] = $this->match_timestamp_count_comment($row['id'], $item[$j]['doc_id']);
                    $row['attachment_names'] = $this->get_comment_attachment_file_names($row['id'], $item[$j]['doc_id']);
                    $row['Comment'] = $response;
                    $get_standard = $this->gettagsbycommentid($row['id'], array('0')); //get standards
                    $get_tags = $this->gettagsbycommentid($row['id'], array('1', '2')); //get tags
                    $row['standard'] = $get_standard;
                    $row['default_tags'] = $get_tags;

                    $comment_result[] = $row;
                }
                //echo "<pre>";

                $item[$j]['comments'] = $comment_result;
            }
        }

        $artifects['success'] = true;
        $artifects['permissions'] = [
            'resources_tab' => ($htype == 3) ? false : true
        ];
        $artifects['role_id'] = HelperFunctions::get_huddle_roles($huddle_id, $user_id);
        if ($htype == 3 && $send_remaining_artifact) {
            $submission_allowed = AccountFolderMetaData::where(array(
                        'account_folder_id' => $huddle_id,
                        'site_id' => $this->site_id,
                        'meta_data_name' => 'submission_allowed'
                    ))->get()->toArray();

            $submission_allowed = isset($submission_allowed[0]['meta_data_value']) ? $submission_allowed[0]['meta_data_value'] : 1;

            $resource_submission_allowed = app("App\Http\Controllers\ApiController")->count_resource_submission_allowed($huddle_id);
            $artifects['submission_allowed'] = $submission_allowed;

            $artifects['resource_submission_allowed'] = $resource_submission_allowed;
            $videos_added = [];
            $resource_added = [];
            $is_assignment_submitted = app("App\Http\Controllers\VideoController")->check_if_assignment_submitted($huddle_id, $user_id);
            foreach ($item as &$artifact) {
                if ($only_user_artifacts && !$is_assignment_submitted && $role_id != 210 && strtotime($artifact["created_date"]) < strtotime("2019-05-25 00:00:00") ) {//please remove last && condition
                    $artifact["is_dummy"] = 1;
                }
                if ($artifact['doc_type'] == 1) {
                    $videos_added[] = $artifact;
                } else if ($artifact['doc_type'] == 2) {
                    $resource_added[] = $artifact;
                }
            }
            $required_videos = [];
            $is_added = false;
            for ($i = 0; $i < $submission_allowed; $i++) {
                $is_added = false;
                foreach ($videos_added as $key => $video) {
                    if ($video["slot_index"] == $i || strtotime($video["created_date"]) < strtotime("2019-05-25 00:00:00")) {//please remove condition in OR(||)
                        $required_videos[] = $video;
                        unset($videos_added[$key]);
                        $is_added = true;
                        break;
                    }
                }
                if (!$is_added) {
                    $required_videos[] = ["doc_type" => 1, "is_dummy" => 1, "slot_index" => $i];
                }
            }
            $required_resources = [];
            $is_added = false;
            for ($i = 0; $i < $resource_submission_allowed; $i++) {
                $is_added = false;
                foreach ($resource_added as $key => $resource) {
                    if ($resource["slot_index"] == $i || strtotime($resource["created_date"]) < strtotime("2019-05-25 00:00:00")) {//please remove condition in OR(||)
                        $required_resources[] = $resource;
                        unset($resource_added[$key]);
                        $is_added = true;
                        break;
                    }
                }
                if (!$is_added) {
                    $required_resources[] = ["doc_type" => 2, "is_dummy" => 1, "slot_index" => $i];
                }
            }
            $item = array_merge($required_videos, $required_resources);
        }

        $artifects['huddle_info'] = $htypeinfo;
        $artifects['huddle_type'] = $htype;
        $artifects['users_accounts'] = HelperFunctions::get_special_permission($user_id, $account_id);
        $artifects['user_info'] = User::where("id", $user_id)->where("site_id", $this->site_id)->first();
        if ($only_user_artifacts && $request->has("logged_in_user_id")) {
            $artifects['user_info'] = User::where("id", $evaluator_ids)->where("site_id", $this->site_id)->first();  
        }
        $artifects['is_participant'] = HelperFunctions::check_if_evaluated_participant($huddle_id, $user_id);
        $artifects['is_evaluator'] = HelperFunctions::check_if_evalutor($huddle_id, $user_id);
        $artifects['evaluators_ids'] = HelperFunctions::get_evaluator_ids($huddle_id, 200);
        $artifects['participants_ids'] = HelperFunctions::get_evaluator_ids($huddle_id, 210);
        $artifects['dis_mem_del_video'] = HelperFunctions::dis_mem_del_video($account_id, $artifects['role_id'], $htype);
        if ($htype == 3) {
            $artifects['submission_date_passed'] = HelperFunctions::check_if_submission_date_passed($huddle_id, $user_id);
        } else {
            $artifects['submission_date_passed'] = false;
        }
        $artifects['coachee_permissions'] = HelperFunctions::coachee_permissions($huddle_id);
        $artifects['artifects']['all'] = $item;
        $artifects['artifects']['total_records'] = $total_records;
        $artifects['huddles_data'] = $this->video_huddle($input);

        $artifects['live_video_permission'] = false;
        $accounts_permissions = Account::where(array('id' => $account_id, 'site_id' => $this->site_id))->first();
        $user_accounts_permissions = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->first();
        if ($accounts_permissions && $accounts_permissions->enable_live_rec == 1 && $user_accounts_permissions && $user_accounts_permissions->live_recording == 1) {
            $artifects['live_video_permission'] = true;
        }

        return response()->json($artifects, 200, [], JSON_PRETTY_PRINT);
    }

    public function gettagsbycommentid($comment_id, $tag_type) {

        $tag_type = implode(',', $tag_type);

        // $data = DB::select('call gettagcommentids(?,?)',array($comment_id, $tag_type));
        // $data = $this->convert_array($data);
        // return $data;
        $get_tags = AccountCommentTag::leftJoin('account_tags', 'AccountCommentTag.account_tag_id', '=', 'account_tags.account_tag_id')
                ->selectRaw("account_tags.*, AccountCommentTag.*")
                ->where(array("comment_id" => $comment_id, 'AccountCommentTag.site_id' => $this->site_id))
                ->whereRaw('ref_type IN(' . $tag_type . ')')
                ->get()
                ->toArray();

        return $get_tags;
    }

    function match_timestamp_count_comment($comment_id, $video_id) {
        $result = Comment::get_all($comment_id, $this->site_id);
        if(!empty($result))
        {
            $commentDate = $result['created_date'];
            $currentDate = date('Y-m-d H:i:s');
            $start_date = new DateTime($commentDate);
            $since_start = $start_date->diff(new DateTime($currentDate));
            if ($start_date < new DateTime('2018-08-10 00:00:00')) {
                if (!empty($result['time'])) {
                    $results = Document::getVideoAttachmentCount($video_id, $result['time'], $this->site_id);
                }
                if (!empty($results)) {
                    return $results;
                } else {
                    return 0;
                }
            } else {
                $results = Document::getVideoAttachmentTotalCount($comment_id, $this->site_id);
                if (!empty($results)) {
                    return $results;
                } else {
                    return 0;
                }
            }
        }
        else
        {
            return 0;
        }
    }

    function get_comment_attachment_file_names($comment_id, $video_id) {
        $result = Comment::get_all($comment_id, $this->site_id);
        if(empty($result)){
            return '';
        }
        $commentDate = $result['created_date'];
        $currentDate = date('Y-m-d H:i:s');

        $start_date = new DateTime($commentDate);
        $since_start = $start_date->diff(new DateTime($currentDate));
        $results = false;
        if ($start_date < new DateTime('2018-08-10 00:00:00')) {
            if (!empty($result['time'])) {
                $results = Document::getVideoAttachmentNames($video_id, $result['time'], $this->site_id);
            }
        } else {
            $results = Document::getVideoAttachmentTitles($comment_id, $this->site_id);
        }
        $file_names = array();
        $final_string = '';
        if ($results) {
            foreach ($results as $result) {
                $file_names[] = $result['title'];
            }
        }


        if (!empty($file_names)) {
            $final_string = implode(',', $file_names);
        } else {
            $final_string = '';
        }

        return $final_string;
    }

    function getVideoComments($video_id, $changType = '', $huddel_id = '', $searchCmt = '', $tags = '', $support_audio_annotation = 0, $get_all_comments = 0, $include_replys = false, $account_tag_id = '', $active = false) {
        $video_id = (int) $video_id;
        if ($huddel_id != '') {
            $huddel_id = (int) $huddel_id;
        }


        $query = Comment::leftJoin('users as User', 'User.id', '=', 'Comment.user_id')
                ->leftJoin('account_comment_tags AS act', 'Comment.id', '=', 'act.comment_id');

        $fields = "Comment.*, User.id as user_id, User.first_name, User.last_name, User.image";

        if ($changType == '1') {
            $order_by = 'Comment.id ASC';
            $query->selectRaw($fields);
        } elseif ($changType == '2') {
            $order_by = 'time_modified ASC';
            $query->selectRaw($fields)->selectRaw('CASE WHEN time IS NULL THEN (SELECT MAX(id) + 1 FROM comments) ELSE time  END time_modified');
        } elseif ($changType == '3') {
            $order_by = 'User.first_name ASC,Comment.time ASC';
            $query->selectRaw($fields);
        } elseif ($changType == '4') {
            $order_by = 'Comment.id DESC';
            $query->selectRaw($fields);
        } elseif ($changType == '5') {
            $order_by = 'Comment.id DESC';
            $query->selectRaw($fields);
        } elseif ($changType == '6') {
            $order_by = 'Comment.time ASC';
            $query->selectRaw($fields);
        } else {
            $order_by = 'Comment.time ASC';
            $query->selectRaw($fields);
        }
        if ($support_audio_annotation == 1) {
            if ($get_all_comments == 1)
                $cmt_type = array('2', '6', '7');
            else
                $cmt_type = array('2', '6');
        }else {
            $cmt_type = array('2');
        }



        if ($include_replys) {
            array_push($cmt_type, '3');
            $parent_condition = '';
        } else {
            $parent_condition = 'Comment.parent_comment_id IS NULL';
            $query->whereRaw($parent_condition);
        }
        $cmt_type = implode(',', $cmt_type);
        if (!empty($searchCmt)) {
            if (!empty($tags)) {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"), "act.account_tag_id" => $tags);
            } else {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"));
            }
        } else {
            if (!empty($tags)) {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, "act.account_tag_id" => $tags);

                $query->where(array(
                    'Comment.ref_id' => $video_id
                ))->whereRaw("Comment.ref_type IN (" . $cmt_type . ")")->whereRaw("act.account_tag_id IN (" . $tags . ")");
            } else {
                $conditions = array($parent_condition, 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id);

                if (!empty($account_tag_id)) {
                    $query->where(array(
                        'Comment.ref_id' => $video_id,
                        "act.account_tag_id" => $account_tag_id
                    ))->whereRaw("Comment.ref_type IN (" . $cmt_type . ")");
                } else {

                    $query->where(array(
                        'Comment.ref_id' => $video_id
                    ))->whereRaw("Comment.ref_type IN (" . $cmt_type . ")");
                }
            }
        }

        if ($active) {
            $query->where('Comment.active', 1);
        }

        $query->where('Comment.site_id', $this->site_id);

        $result = $query->groupBy('Comment.id')->orderByRaw($order_by)->get()->toArray();
        return $result;
    }

    function get_replies_from_comment($comment, $get_all_comments) {

        $result = $this->commentReplys($comment['id'], $get_all_comments);
        $responses = array();
        foreach ($result as $row) {
            $row = $this->get_replies_from_comment($row, $get_all_comments);
            if ($row['ref_type'] == 6) {
                $row['comment'] = $this->get_cloudfront_url($row['comment']);
            }
            $row['created_date_string'] = $this->times_ago($row['created_date']);
            $responses[] = $row;
        }
        $comment['responses'] = $responses;
        return $comment;
    }

    public function times_ago($commentDate) {
        $translations = TranslationsManager::get_page_lang_based_content("Api/video_details", $this->site_id, '', $this->current_lang);
        //$commentDate = $row['created_date'];
        $currentDate = date('Y-m-d H:i:s');

        $start_date = new DateTime($commentDate);
        $since_start = $start_date->diff(new DateTime($currentDate));

        $commentsDate = '';
        if ($since_start->y > 0) {
            $commentsDate = ($since_start->y > 1 ? str_replace("xx", $since_start->y, $translations["vd_years_ago"]) : $translations["vd_year_ago"]);
        } elseif ($since_start->m > 0) {
            $commentsDate = ($since_start->m > 1 ? str_replace("xx", $since_start->m, $translations["vd_months_ago"]) : $translations["vd_month_ago"]);
        } elseif ($since_start->d > 0) {
            $commentsDate = ($since_start->d > 1 ? str_replace("xx", $since_start->d, $translations["vd_days_ago"]) : $translations["vd_day_ago"]);
        } elseif ($since_start->h > 0) {
            $commentsDate = ($since_start->h > 1 ? str_replace("xx", $since_start->h, $translations["vd_hours_ago"]) : $translations["vd_hour_ago"]);
        } elseif ($since_start->i > 0) {
            $commentsDate = ($since_start->i > 1 ? str_replace("xx", $since_start->i, $translations["vd_minutes_ago"]) : $translations["vd_minute_ago"]);
        } elseif ($since_start->s > 0) {
            $commentsDate = $translations["vd_less_than_a_minute_ago"];
        } else {
            $commentsDate = $translations["vd_just_now"];
        }
        return $commentsDate;
    }

    function commentReplys($comment_id, $get_all_comments = 0) {
        $comment_id = (int) $comment_id;
        $active = "0";
        if ($get_all_comments == 1) {
            $active = "'0','1'";
        } else {
            $active = "'1'";
        }

        // $data = DB::select('call getusercommentreplies(?,?)',array($comment_id, $active));
        // $data = $this->convert_array($data);
        // return $data;
        $result = Comment::leftJoin('users as User', 'User.id', '=', 'Comment.user_id')
                        ->leftJoin('comment_attachments as CommentAttachment', 'Comment.id', '=', 'CommentAttachment.comment_id')
                        ->leftJoin('documents as Document', 'CommentAttachment.document_id', '=', 'Document.id')
                        ->select('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image', 'Document.url', 'Document.original_file_name', 'Document.id as document_id')
                        ->where(array('Comment.parent_comment_id' => $comment_id))
                        ->where(array('Comment.site_id' => $this->site_id))
                        ->whereRaw('Comment.active IN (' . $active . ')')
                        ->whereRaw('user_id!=""')
                        ->orderBy('Comment.created_date')
                        ->groupBy('Comment.id')
                        ->get()->toArray();

        return $result;
    }

    public static function getSecureAmazonCloudFrontUrl($resource, $name = '', $timeout = 600, $useHTTP = false) {


        //This comes from key pair you generated for cloudfront
        $keyPairId = 'APKAJ64SUURDPDPPLA5Q';

        //Read Cloudfront Private Key Pair
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => realpath(dirname(__FILE__) . '/../..') . "/Config/cloudfront_key.pem",
                    'key_pair_id' => $keyPairId,
        ));

        $streamHostUrl = 'https://d1ebp8oyqi4ffr.cloudfront.net';

        if ($useHTTP == true) {
            $streamHostUrl = str_replace("https://", "http://", $streamHostUrl);
        }
        $resourceKey = $resource;
        $expires = time() + 21600;

        //Construct the URL
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));

        return $signedUrlCannedPolicy;
    }

    function is_enabled_coach_feedback($account_folder_id, $site_id) {

        $check_data = AccountFolderMetaData
                ::where(array(
                    "account_folder_id" => $account_folder_id,
                    "meta_data_name" => "coach_hud_feedback",
                    "site_id" => $this->site_id
                ))->get()->toArray();

        if (isset($check_data[0]['meta_data_value'])) {
            if ($check_data[0]['meta_data_value'] == '1') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function video_huddle($input) {
        // $input = $request->all();
        $video_huddle_id = $input['huddle_id'];
        $account_id = $input['account_id'];
        //echo "<pre>";

        $output = array();
        $default_photo = isset($input['use_default_photo']) ? $input['use_default_photo'] : 1;
        $support_audio_annotation = isset($input['support_audio_annotation']) ? $input['support_audio_annotation'] : 0;
        $huddle = AccountFolder::getHuddle($this->site_id, $video_huddle_id, $account_id);

        $lang = !empty($input['lang']) ? $input['lang'] : 'en';

        if(!isset($huddle) || empty($huddle)) {
            return response()->json(['success' => false, 'message' => 'Invalid Huddle ID'], 200);
        }
        $huddle = $huddle[0];
        //$h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $video_huddle_id, 'meta_data_name' => 'folder_type')));
        $huddle_type = $this->get_huddle_type($video_huddle_id);
        $huddle['folder_type'] = !empty($huddle_type) ? $huddle_type : 1;
        //isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";

        $amazon_base_url = config('s3.amazon_base_url');
        $sibme_base_url = config('sibme_base_url');

        $huddleUsers = AccountFolder::getHuddleUsers($video_huddle_id, $this->site_id);
        $huddleGroups = AccountFolderGroup::getHuddleGroups($video_huddle_id, $this->site_id);
        //$huddleVideos = $this->Document->getVideos($video_huddle_id);

        $user_id = $input['user_id'];
        $permissions = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->first();
        //$permissions = $this->UserAccount->get($account_id, $user_id);
        $user_huddle_role = '220';
        $p_huddle_group_users = AccountFolder::getHuddleGroupsUsers($video_huddle_id, $this->site_id);

        if (isset($account_id) && isset($user_id)) {
            $environment_type = 1;
            if (isset($input['environment_type'])) {
                $environment_type = $input['environment_type'];
            }

            $user_activity_logs = array(
                'ref_id' => $huddle['account_folder_id'],
                'desc' => $huddle['name'],
                'url' => '/Huddles/view/' . $huddle['account_folder_id'],
                'type' => '12',
                'account_id' => $account_id,
                'account_folder_id' => $huddle['account_folder_id'],
                'environment_type' => $environment_type,
                'site_id'=>$this->site_id
            );
            DB::table('user_activity_logs')->insertGetId($user_activity_logs);
            //$this->user_activity_logs($user_activity_logs, $account_id, $user_id);
        }

        if ($p_huddle_group_users && count($p_huddle_group_users) > 0) {
            for ($i = 0; $i < count($p_huddle_group_users); $i++) {
                $p_huddle_group_user = $p_huddle_group_users[$i];
                if (isset($p_huddle_group_user['id']) && $p_huddle_group_user['id'] == $user_id) {
                    $user_huddle_role = $p_huddle_group_user['role_id'];
                }
            }
        }

        if ($huddleUsers && count($huddleUsers) > 0) {

            for ($i = 0; $i < count($huddleUsers); $i++) {

                $p_huddle_user = $huddleUsers[$i];
                if ($p_huddle_user['user_id'] == $user_id) {
                    $user_huddle_role = $p_huddle_user['role_id'];
                }
            }
        }


        //$this->get_huddle_level_permissions($video_huddle_id);
        //$view = new View($this, false);

        for ($i = 0; $i < count($huddleUsers); $i++) {

            $huddleUser = $huddleUsers[$i];

            if (isset($huddleUser['image']) && $huddleUser['image'] != '') {
                $use_folder_imgs = config('use_local_file_store');
                if ($use_folder_imgs == false) {
                    $file_name = "static/users/" . $huddleUser['id'] . "/" . $huddleUser['image'];
                    //$huddleUser['User']['image'] = $view->Custom->getSecureSibmecdnImageUrl("static/users/" . $huddleUser['User']['id'] . "/" . $huddleUser['User']['image']);
                    $huddleUser['image'] = config('amazon_base_url') . config('bucket_name_cdn') . "/" . $file_name;
                }

                // $huddleUser['User']['image'] = $sibme_base_url . '/img/users/' . $huddleUser['User']['image'];
                // else
                // $huddleUser['User']['image'] = $view->Custom->getSecureSibmecdnUrl("static/users/" . $huddleUser['User']['id'] . "/" . $huddleUser['User']['image']);
            } else {
                if ($default_photo == 1) {
                    $huddleUser['image'] = $sibme_base_url . '/img/home/photo-default.png';
                }
            }

            $huddleUsers[$i] = $huddleUser;
        }

        $check_data = AccountFolderMetaData::where('account_folder_id', $video_huddle_id)->where('meta_data_name', 'chk_frameworks')->first();
        $huddle_framework_permission = 0;
        if (isset($check_data['meta_data_value']) && $check_data['meta_data_value'] == '1') {
            $huddle_framework_permission = 1;
        } else {
            $huddle_framework_permission = 0;
        }
        $check_data = AccountFolderMetaData::where('account_folder_id', $video_huddle_id)->where('meta_data_name', 'chk_tags')->first();
        $chk_tags = 0;
        if (isset($check_data['meta_data_value']) && $check_data['meta_data_value'] == '1') {
            $chk_tags = 1;
        } else {
            $chk_tags = 0;
        }

        $check_data = AccountFolderMetaData::where('account_folder_id', $video_huddle_id)->where('meta_data_name', 'framework_id')->first();

        $framework_id = 0;
        if (isset($check_data['meta_data_value'])) {
            $framework_id = $check_data['meta_data_value'];
        } else {
            $framework_id = 0;
        }

        $check_data = AccountFolderMetaData::where('account_folder_id', $video_huddle_id)->where('meta_data_name', 'submission_deadline_time')->first();

        $submission_deadline_time = 0;
        if (isset($check_data['meta_data_value'])) {
            $submission_deadline_time = $check_data['meta_data_value'];
        } else {
            $submission_deadline_time = 0;
        }

        $check_data = AccountFolderMetaData::where('account_folder_id', $video_huddle_id)->where('meta_data_name', 'submission_deadline_date')->first();

        $submission_deadline_date = 0;
        if (isset($check_data['meta_data_value'])) {
            $submission_deadline_date = $check_data['meta_data_value'];
        } else {
            $submission_deadline_date = 0;
        }
        $is_eval_huddle = AccountFolderMetaData::check_if_eval_huddle($video_huddle_id, $this->site_id);
        $coachee_can_download = AccountFolderMetaData::where(array('site_id' => $this->site_id, 'account_folder_id' => $video_huddle_id, "meta_data_name" => "coachee_can_download"
                ))->first();
        $coachee_can_view_summary = AccountFolderMetaData::where(array('site_id' => $this->site_id, 'account_folder_id' => $video_huddle_id, "meta_data_name" => "coachee_can_view_summary"
                ))->first();
        $enable_publish_button = AccountFolderMetaData::where(array('site_id' => $this->site_id, 'account_folder_id' => $video_huddle_id, "meta_data_name" => "enable_publish_button"
                ))->first();
        $output['coachee_can_download'] = !empty($coachee_can_download['meta_data_value']) ? $coachee_can_download['meta_data_value'] : 0;
        $output['coachee_can_view_summary'] = !empty($coachee_can_view_summary['meta_data_value']) ? $coachee_can_view_summary['meta_data_value'] : 0;
        $output['enable_publish_button'] = !empty($enable_publish_button['meta_data_value']) ? $enable_publish_button['meta_data_value'] : 0;

        if (!$huddle) {
            $output['success'] = false;
            $output['error_msg'] = 'Error loading videoHuddles';
        } else {
            $output['success'] = true;
            $output['Huddle'] = $huddle;
            $output['huddle_role'] = $user_huddle_role;
            $output['user_account_permission'] = $permissions;
            $output['HuddleUsers'] = $huddleUsers;
            $output['HuddleGroups'] = $huddleGroups;
            //$output['HuddleVideos'] = $huddleVideos;
            //$output['HuddleDocs'] = $documents;
            $output['huddle_framework_permission'] = $huddle_framework_permission;
            $output['framework_id'] = $framework_id;
            $output['chk_frameworks'] = $huddle_framework_permission;
            $output['chk_tags'] = $chk_tags;
            $h_type = AccountFolderMetaData::where('account_folder_id', $account_id)->where('meta_data_name', 'enable_framework_standard')->first();
            //$h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_id, 'meta_data_name' => 'enable_framework_standard')));
            $output['chk_frameworks_account_level'] = isset($h_type['meta_data_value']) ? $h_type['meta_data_value'] : "0";

            //$h_type = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $account_id, 'meta_data_name' => 'enable_tags')));
            $h_type = AccountFolderMetaData::where('account_folder_id', $account_id)->where('meta_data_name', 'enable_tags')->first();
            $output['chk_tags_account_level'] = isset($h_type['meta_data_value']) ? $h_type['meta_data_value'] : "0";



            if ($is_eval_huddle) {
                $output['submission_deadline_date'] = $submission_deadline_date;
                $output['submission_deadline_time'] = $submission_deadline_time;
            }

            if (AccountFolderMetaData::check_if_eval_huddle($video_huddle_id, $this->site_id)) {
                $submission_allowed = AccountFolderMetaData::where('account_folder_id', $video_huddle_id)->where('meta_data_name', 'submission_allowed')->first();
                //$submission_allowed = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $video_huddle_id, 'meta_data_name' => 'submission_allowed')));
                $vide_max_limit = 1;
                if (!empty($submission_allowed) && $submission_allowed['meta_data_value'] != '') {
                    $vide_max_limit = $submission_allowed['meta_data_value'];
                }
                $output['max_allowed_videos'] = $vide_max_limit;
                $output['logged_in_participant_uploaded_videos'] = (string) HelperFunctions::get_eval_participant_videos($video_huddle_id, $user_id, $this->site_id);
                $output['stats'] = AccountFolderUser::getAssesseeStats($this->site_id, $video_huddle_id, $user_id, [] , false);
            }
        }
        return $output;
    }

    public static function getSecureAmazonUrl($file, $name = null) {
        $aws_access_key_id = 'AKIAJ4ZWDR5X5JKB7CZQ';
        $aws_secret_key = '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';
        $aws_bucket = 'sibme-production';
        $expires = '+240 minutes';
        // Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));
        $url = $client->getObjectUrl($aws_bucket, $file, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file)),
        ));
        return $url;
    }

    public static function _check_evaluator_permissions($huddle_id, $created_by, $user_id, $role_id) {
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        if (!AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
            return true;
        }
        $is_avaluator = AccountFolderUser::check_if_evalutor($site_id, $huddle_id, $user_id);
        // $evaluators_ids = self::get_evaluator_ids($huddle_id);
        $evaluators_ids = HelperFunctions::get_evaluator_ids($huddle_id, 200);
        array_push($evaluators_ids, $created_by);
        $participants_ids = AccountFolderUser::get_participants_ids($huddle_id, 210, $site_id);
        $isEditable = '';
       // if ($is_avaluator && $role_id == 120) {
         //   $isEditable = ($user_id == $created_by || (is_array($participants_ids) && in_array($created_by, $participants_ids)));
       // } 
        if ($is_avaluator && ($role_id == 110 || $role_id == 100 || $role_id == 115 || $role_id == 120 )) {
            $isEditable = ($is_avaluator || ($user_id == $created_by || (is_array($evaluators_ids) && in_array($created_by, $evaluators_ids))));
        } else {
            $isEditable = (($user_id == $created_by || (is_array($evaluators_ids) && in_array($created_by, $evaluators_ids))));
        }
        return $isEditable;
    }

    function get_documents($huddle_id, $user_id, $htype, $title) {
        $documents = array();

        if (!AccountFolderUser::check_if_evalutor($this->site_id, $huddle_id, $user_id) && $htype == '2') {

            $coachee_documents = Document::getCoacheeDocuments($this->site_id, $huddle_id, $title);

            foreach ($coachee_documents as $document) {
                $temp_array = array();
                $temp_array['title'] = $document['title'];
                $temp_array['desc'] = $document['desc'];
                $temp_array['id'] = $document['afdid'];
                $temp_array['first_name'] = $document['first_name'];
                $temp_array['last_name'] = $document['last_name'];
                $temp_array['email'] = $document['email'];

                $documents[] = $temp_array;
            }
        } else {
            $documents = Document::getDocuments($this->site_id, $huddle_id, $title);
        }

        return $documents;
    }

    public function get_participants(Request $request) {
        $huddle_id = $request->get('huddle_id');
        //$folder_type = $request->get('folder_type');
        $user_role_id = $request->get('user_role_id');
        $user_id = $request->get('user_id');
        $result_array = array();       
        $participants = AccountFolder::getHuddleUsersIncludingGroups($this->site_id, $huddle_id, $user_id, $user_role_id,true)['participants'];
        foreach($participants as $row){
            $created_by = $row['user_id'];
            $row['total_submissions'] =  AccountFolderDocument::get_total_assessments($created_by,$huddle_id,$this->site_id); 
            $result_array[] = $row;
        }
        /*
          $huddle_users = AccountFolder::getHuddleUsers($huddle_id, $this->site_id);
          $huddle_groups = AccountFolder::getHuddleGroups($huddle_id);
          if (!empty($huddle_users)) {
          foreach ($huddle_users as $huddle_user) {
          $participants_arranged[] = array(
          'user_id' => $huddle_user['user_id'],
          'role_id' => $huddle_user['role_id'],
          'user_name' => $huddle_user['first_name'] . ' ' . $huddle_user['last_name'],
          'image' => $huddle_user['image'],
          'user_email' => $huddle_user['email']
          );
          }
          }
          if ($huddle_groups) {
          foreach ($huddle_groups as $row) {
          $participants_arranged[] = array(
          'user_id' => $row['user_id'],
          'role_id' => $row['role_id'],
          'user_name' => $row['name'],
          'image' => 'groups',
          'user_email' => 'N/A'
          );
          }
          }
          $folder_type = AccountFolder::getAccountFolderType($huddle_id);
          if ($folder_type == 1) {
          $folder_type = "collaboration";
          } else if ($folder_type == 2) {
          $folder_type = "coaching";
          } else {//3
          $folder_type = "assessment";
          }

          $participants = array();
          $huddle_name = 'collaboration';
          if ($folder_type == 'assessment') {
          $role_200 = 'Assessor';
          $role_210 = 'Assessed Participants';
          $role_220 = '';
          $huddle_name = 'Assessment';
          } elseif ($folder_type == 'coaching') {

          $role_200 = 'Coach';
          $role_210 = 'Coachee';
          $role_220 = '';
          $huddle_name = 'Coaching';
          } else {

          $role_200 = 'Collaboration Participants';
          $role_210 = 'Collaboration Participants';
          $role_220 = 'Collaboration Participants';
          $huddle_name = 'Collaboration';
          }

          foreach ($participants_arranged as $row) {
          if ($row['role_id'] == '200') {
          $row['role_name'] = 'Admin';
          if ($folder_type == 'assessment') {
          $row['role_name'] = "Assessor";
          }
          if ($folder_type == 'coaching') {
          $row['role_name'] = "Coach";
          }
          $row['role'] = $role_200;
          } elseif ($row['role_id'] == '210') {
          $row['role_name'] = 'Member';
          if ($folder_type == 'assessment') {
          $row['role_name'] = "Assessee";
          }
          if ($folder_type == 'coaching') {
          $row['role_name'] = "Coachee";
          }
          $row['role'] = $role_210;
          } else {
          $row['role_name'] = 'Viewer';
          if ($folder_type == 'coaching') {
          $row['role_name'] = "Coachee";
          }
          $row['role'] = $role_220;
          }
          if (!($folder_type == "assessment" && $user_role_id == "210" && $row['role_id'] == "210" && $row["user_id"] != $user_id)) {
          $participants[] = $row;
          }
          }
         */
        return response()->json($participants, 200, [], JSON_PRETTY_PRINT);
    }

    public function edit_title(Request $request) {
        $input = $request->all();
        $title = $input['title'];
        $document_id = $input['document_id'];
        $doc_id = @$input['doc_id'];
        $huddle_id = $input['huddle_id'];
        $user_id = $input['user_id'];
        $workspace = isset($input['workspace']) && !empty($input['workspace']) ? 1 : 0;
        $folder_type = 1;
        if ($workspace) {
            $folder_type = 3;
        }
        $response = AccountFolderDocument::update_video_title($title, $document_id, $this->site_id,$user_id);
        if (!empty($response)) {
            $res = Document::get_single_video_data($this->site_id, $doc_id, $folder_type, 1, $huddle_id, $user_id);
            if($res)
            {
                $res['updated_by'] = $user_id;
                $channel_name = 'huddle-details-' . $huddle_id;
                if ($workspace) {
                    $account_id = $input["account_id"];
                    $channel_name = "workspace-" . $account_id . "-" . $user_id;
                }
                $event = [
                    'channel' => $channel_name,
                    'event' => "resource_renamed",
                    'data' => $res,
                    'video_file_name' => $res["title"],
                ];
                HelperFunctions::broadcastEvent($event);
            }
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        }
        return response()->json(['success'=>false, 'message'=>'Title cannot be changed.'], 200, [], JSON_PRETTY_PRINT);
    }

    public function get_all_discussions(Request $request) {
        $input = $request->all();
        $huddle_id = $input['huddle_id'];
        $user_id = $input['user_id'];

        $page = (isset($input['page']) ? $input['page'] : 1);
        $limit = (isset($input['limit']) ? $input['limit'] : 10);
        $title = (isset($input['search']) && !empty($input['search']) ? $input['search'] : '');
        $sort = (isset($input['sort']) ? $input['sort'] : '');
        /*
        $is_mobile_request = false;        
        if (!empty($input['app_name']) && ($input['app_name'] == 'com.sibme.mobileapp' || $input['app_name'] == 'com.sibme.sibme')) {
            $is_mobile_request = true;
        } else {
            $is_mobile_request = false;
        }
        */
        
        
        if(isset($input['app_name']) && isset($input['turn_on_pagination']) ) 
        {
            $is_mobile_request = false;
        }
        else if(isset($input['app_name']) && !isset($input['turn_on_pagination']))
        {
            $is_mobile_request = true;
        }
        else
        {
           $is_mobile_request = false;
        }
        $response = Comment::getCommentsSearch($huddle_id, $this->site_id, $title, $sort, $page, $limit, true, $user_id,$is_mobile_request);
        $dataresp = $response;
        
        $is_mobile_request = isset($input['app_name']) ? HelperFunctions::is_mobile_request($input['app_name']) : false;

        foreach ($dataresp as $key => $value) {
            $reply_counts = Comment::getRepliesCount($value['id'], $this->site_id);
            $response[$key]['reply_count'] = $reply_counts;
            $response[$key]['unread_comments'] = Comment::get_unread_counts($value['id'], $this->site_id, $user_id, $reply_counts);
            $temp_attachments = Comment::getattachments($value['id']);
            $attachments = array();
            $base = config('s3.sibme_base_url');
            
            foreach ($temp_attachments as $index => $attachment)
                {
                        $attachments[$index] = $attachment;
                        
                        
                        if(!empty($attachment['stack_url']))
                        {
                          if($is_mobile_request)
                          {
                            $attachments[$index]['stack_url'] = $attachment['stack_url'];
                          }
                          else
                          {
                            $attachments[$index]['stack_url'] = $base . 'app/view_document/'.$attachment['stack_url']; 
                          }
                        }
                        else {
                           $attachments[$index]['stack_url'] = ''; 
                        }
                        
                        
                        $ext = pathinfo($attachment['original_file_name'], PATHINFO_EXTENSION);
                        if ($ext == 'xls' || $ext == 'xlsx') {
                            $attachments[$index]['thubnail_url'] = $base . 'img/excel_gry_icon.svg';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == 'pdf') {
                            $attachments[$index]['thubnail_url'] = $base . 'img/pdf_gry_icon.svg';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == "pptx" || $ext == "ppt") {
                            $attachments[$index]['thubnail_url'] = $base . 'img/power_point.png';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == "docx" || $ext == "doc") {
                            $attachments[$index]['thubnail_url'] = $base . 'img/word_icon.png';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                            $attachments[$index]['thubnail_url'] = $base . 'img/image_icon.png';
                            $attachments[$index]['file_type'] = $ext;
                        } else {
                            $attachments[$index]['thubnail_url'] = $base . 'img/file_gry_icon.svg';
                            $attachments[$index]['file_type'] = $ext;
                        }
                }
            $response[$key]['attachments'] = $attachments;
            $response[$key]['total_comments'] = $reply_counts;
        }

        if ($sort == 'unread') {
            HelperFunctions::sort_array_of_array($response, 'unread_comments');
        }

        $res['discussions'] = $response;
        if ($page == 1) {
            $res['total_discussions'] = Comment::countDiscussion($huddle_id, $this->site_id, $title,true);
        } else {
            $res['total_discussions'] = false;
        }

        if (!empty($res)) {
            return response()->json($res, 200, [], JSON_PRETTY_PRINT);
        }
        return response()->json($res, 200, [], JSON_PRETTY_PRINT);
    }

    public function add_discussion(Request $request) {
        $input = $request->all();
        
        if(empty($input))
        {
            return response()->json(['success' => false, 'message' => 'Empty Response'], 200, [], JSON_PRETTY_PRINT);
        }
        $huddle_id = $input['huddle_id'];
        $account_id = $input['account_id'];
        $fake_id = $input['fake_id'];
        $user_id = $input['user_id'];
        $title = $input['title'];
        // $comment = HelperFunctions::addHttpWithLinks($input['comment']);
        $comment = $this->addHttpWithLinks($input['comment']);
        $comment = str_replace('<em','<i',$comment);
        $comment = str_replace('</em>','</i>',$comment);
        

        if (isset($input['ref_type'])) {
            $ref_type = '8';
        } else {

            $ref_type = '1';
        }

        $commentable_id = $input['commentable_id'];
        $discussion_id = @$input['discussion_id'];
        $notification_user_ids = $input['notification_user_ids'];
        $notification_user_ids = empty($notification_user_ids) ? '' : $notification_user_ids;
        $user_current_account = json_decode($input['user_current_account']);
        $send_email = $input['send_email'];
        $tab = 0;
        $notificationIds = array();

        $users_ids = explode(',', $notification_user_ids);
        $counter = 0;
        foreach ($users_ids as $key => $val) {
            if (isset($users_ids[$counter]) && $users_ids[$counter] != 0) {
                $notificationIds[] = $users_ids[$counter];
            }
            $counter++;
        }

        $comment_data = array(
            'title' => $title,
            'comment' => (html_entity_decode($comment)),
            'ref_id' => $huddle_id,
            'parent_comment_id' => empty($commentable_id) ? null : $commentable_id,
            'ref_type' => $ref_type,
            'created_date' => date('y-m-d H:i:s', time()),
            'created_by' => $user_id,
            'last_edit_date' => date('y-m-d H:i:s', time()),
            'last_edit_by' => $user_id,
            'user_id' => $user_id,
            'site_id' => $this->site_id,
            'fake_id'=>$fake_id
        );
        $comment_id = DB::table('comments')->insertGetId($comment_data);
        if (!empty($comment_id)) {
            $comment_data["id"] = $comment_id;
            if ($commentable_id == '' || empty($commentable_id) || $commentable_id == 'null' ) {
                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $comment,
                    'url' => $this->base . 'video_huddles/huddle/details/' . $huddle_id . "/discussions/details/" . $comment_id,
                    'account_folder_id' => $huddle_id,
                    'type' => '6',
                    'site_id' => $this->site_id,
                    'user_id' => $user_id,
                    'account_id' => $account_id,
                    'date_added' => date("Y-m-d H:i:s")
                );
            } else {
                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $comment,
                    'url' => $this->base . 'video_huddles/huddle/details/' . $huddle_id . "/discussions/details/" . $commentable_id,
                    'account_folder_id' => $huddle_id,
                    'type' => '8',
                    'site_id' => $this->site_id,
                    'user_id' => $user_id,
                    'account_id' => $account_id,
                    'date_added' => date("Y-m-d H:i:s")
                    
                );
            }
            DB::table('user_activity_logs')->insertGetId($user_activity_logs);

            if (isset($_FILES['attachment']) && !empty($_FILES['attachment'])) {
                $attachments = $_FILES['attachment'];
                if (isset($attachments['error'])) {
                    $attachments = array($attachments);
                }
                $attachments = self::GetRealAttachments($attachments);
                foreach ($attachments as $att) {
                    if ($att['error'] != 0)
                        continue;
                    $document_id = $this->uploadAttachementToS3($att, $account_id, $huddle_id, $user_current_account);
                    if ($document_id != false) {
                        $data = array(
                            'comment_id' => $comment_id,
                            'document_id' => $document_id,
                            'site_id' => $this->site_id
                        );
                        $ca = DB::table('comment_attachments')->insertGetId($data);
                        $response[] = $document_id;
                    }
                }
            }


        //    if ($notificationIds) {
                $huddle = AccountFolder::getHuddle($this->site_id, $huddle_id, $account_id);
                $huddleUsers = AccountFolder::getHuddleUsers($huddle_id, $this->site_id);
                $userGroups = AccountFolderGroup::getHuddleGroups($huddle_id, $this->site_id);
                $huddle_user_ids = array();
                if ($userGroups && count($userGroups) > 0) {
                    foreach ($userGroups as $row) {
                        $huddle_user_ids[] = $row['user_id'];
                    }
                }
                if ($huddleUsers && count($huddleUsers) > 0) {
                    foreach ($huddleUsers as $row) {
                        $huddle_user_ids[] = $row['user_id'];
                    }
                }

                $user_ids = implode(',', $huddle_user_ids);
                $huddleUserInfo = '';
                if ($user_ids != '') {
                    // This is not used in Email Template, so disabling it for now.
                    // $huddleUserInfo = User::whereIn("id",$huddle_user_ids)->get();
                }

                $sibme_base_url = $this->base;
              
            for ($i = 0; $i < count($huddle_user_ids); $i++) {
                    $notifier_id = $huddle_user_ids[$i];
                    
                    if($notifier_id == $user_id)
                    {
                        continue;
                    }

                    $notifier = User::join('users_accounts as ua','ua.user_id','=','users.id')->where('users.id', $notifier_id)->where('ua.account_id',$account_id)->whereIn('role_id',array(100,110,115,120))->first();
                    
                    if ($notifier && $notifier->is_active == 1) {
                        
                        $data = array(
                            'comment_id' => $comment_id,
                            'user_id' => $notifier_id,
                            'comment_user_type' => 1,
                            'site_id' => $this->site_id
                        );

                        DB::table('comment_users')->insertGetId($data);
                        if ($commentable_id != '' && $commentable_id != 'null') {
                            $orignal_comment = Comment::get_parent_discussion($commentable_id, $this->site_id);
                            $huddle_topic_name = $orignal_comment['title'];
                            $discussion_link = $sibme_base_url . '/video_huddles/huddle/details/' . $huddle_id . '/discussions/details/' . $commentable_id;
                        } else {
                            $huddle_topic_name = $input['title'];
                            $discussion_link = $sibme_base_url . '/video_huddles/huddle/details/' . $huddle_id . '/discussions/details/' . $comment_id;
                        }
                        $current_user = User::find($user_id);
                        if(!isset($huddle[0]))
                        {
                            $response = ['success' => false, 'message' => TranslationsManager::get_translation("discussion_comments_not_saved")];
                            return response()->json($response, 200);
                        }
                        $discussions = array(
                            'huddle_name' => $huddle[0]['name'],
                            'discussion_topic' => $huddle_topic_name,
                            'created_by' => $current_user->first_name . " " . $current_user->last_name,
                            'message' => $comment,
                            'discussion_link' => $discussion_link,
                            'email' => $notifier['email'],
//                            'participting_users' => $huddleUserInfo
                        );
                        
                        if(EmailUnsubscribers::check_subscription($notifier_id, '2', $account_id, $this->site_id)){
                           if($commentable_id == '' || $commentable_id == 'null' || $commentable_id == null){
                            $this->SendDiscussionEmail($account_id, $notifier, '', $comment, $discussions, $commentable_id, $user_current_account);
                           }elseif($commentable_id != '' && !empty($notificationIds)){                             
                            $this->SendDiscussionEmail($account_id, $notifier, '', $comment, $discussions, $commentable_id, $user_current_account);
                           }                           
                        }
                       
                        // if (EmailUnsubscribers::check_subscription($notifier_id, '2', $account_id, $this->site_id) && ($commentable_id == '' || $commentable_id == 'null' || $commentable_id == null)) {
                         //     $this->SendDiscussionEmail($account_id, $notifier, '', $comment, $discussions, $commentable_id, $user_current_account);
                        // } elseif (EmailUnsubscribers::check_subscription($notifier_id, '2', $account_id, $this->site_id) && $commentable_id != '' && !empty($notificationIds)) {
                        //     $this->SendDiscussionEmail($account_id, $notifier, '', $comment, $discussions, $commentable_id, $user_current_account);
                        // }
                    }
                }

       //     }
            $user_current_account = (array) $user_current_account;
            $user_current_account['users_accounts'] = (array) $user_current_account['users_accounts'];
            $user_current_account['User'] = (array) $user_current_account['User'];
            $user_role = HelperFunctions::get_user_role_name($user_current_account['users_accounts']['role_id']);
            $in_trial_intercom = HelperFunctions::check_if_account_in_trial_intercom($account_id);
            //    if (IS_QA):

            $meta_data = array(
                'discussion-comment-added' => 'true',
                'comment' => $comment,
                'user_role' => $user_role,
                'is_in_trial' => $in_trial_intercom,
                'Platform' => 'Web',
                'fake_id'=>$fake_id
            );

            HelperFunctions::create_intercom_event('discussion-added', $meta_data, $user_current_account['User']['email']);


            if (isset($send_email) && ($send_email == '1' || $send_email == '2' )) {
                //  if (IS_QA):

                $meta_data = array(
                    'included_in_discussion' => 'true',
                    'comment' => $comment,
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web',
                    'fake_id'=>$fake_id
                );

                HelperFunctions::create_intercom_event('included-in-discussion', $meta_data, $user_current_account['User']['email']);
                // endif;
            }

            $discussion = Comment::getsingleDiscussion($huddle_id, $this->site_id, $comment_id, $user_id,$fake_id);
            $response = ['success' => true, 'message' => TranslationsManager::get_translation("discussion_discussion_started_successfully"), "data" => $discussion];
            $channel = 'discussions-list-' . $huddle_id;
            $discussion['fake_id'] = $fake_id;
            if (!empty($commentable_id) && $commentable_id != "null") {
                $comment_read = new UserReadComments();
                $comment_read->discussion_id = $commentable_id;
                $comment_read->user_id = $user_id;
                $comment_read->comment_id = $comment_id;
                $comment_read->save();
                $comment_count_event = array(
                    'discussion_id' => $commentable_id,
                    'reference_id' => $discussion_id,
                    'data' => $discussion,
                    'huddle_id' => $huddle_id,
                    'channel' => "discussion-comment-changes-" . $huddle_id,
                    'event' => 'comment_added',
                    'fake_id'=>$fake_id
                );
                HelperFunctions::broadcastEvent($comment_count_event);

                $channel = "discussions-details-" . $discussion_id;
            }

            $res = array(
                'document_id' => $comment_id,
                'reference_id' => $discussion_id,
                'data' => $discussion,
                'huddle_id' => $huddle_id,
                'channel' => $channel,
                'event' => 'discussion_added',
                'fake_id'=>$fake_id
            );
            HelperFunctions::broadcastEvent($res);
            return response()->json($response, 200);
        } else {
            $response = ['success' => false, 'message' => TranslationsManager::get_translation("discussion_comments_not_saved")];
            return response()->json($response, 200);
        }

        if (!empty($response)) {
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        }
        return response()->json(['success' => false, 'message' => 'Empty Response'], 200, [], JSON_PRETTY_PRINT);
    }

    function edit_discussion(Request $request) {
        $input = $request->all();
        if(empty($input))
        {
          return response()->json(['success' => false, 'message' => 'Session Does not Exists.' ], 200);  
        }
        $huddle_id = $input['huddle_id'];
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $title = $input['title'];
        // $comment = HelperFunctions::addHttpWithLinks($input['comment']);
        $comment = $this->addHttpWithLinks($input['comment']);
        $comment = str_replace('<em','<i',$comment);
        $comment = str_replace('</em>','</i>',$comment);
        $comment_id = $input['comment_id'];
        $discussion_id = $input['discussion_id'];
        $user_current_account = $input['user_current_account'];
        $send_email = $input['send_email'];
        $removeAttachment = json_decode($input['remove_attachments']);

        $tab = 0;

        $objComment = Comment::where("id", $comment_id)->first();
        if (!$objComment) {
            return response()->json(['success' => false, 'message' => TranslationsManager::get_translation('gen_comment_not_found', 'Api/video_details',  $this->site_id)], 200);
        }

        $data = [
            'title' => $title,
            'comment' => $comment,
            'last_edit_date' => date('y-m-d H:i:s', time())
        ];

        $objComment->update($data);

        if (isset($_FILES['attachment']) && !empty($_FILES['attachment'])) {
            $attachments = $_FILES['attachment'];
            if (isset($attachments['error'])) {
                $attachments = array($attachments);
            }
            $attachments = self::GetRealAttachments($attachments);
            foreach ($attachments as $att) {
                if ($att['error'] != 0)
                    continue;
                $document_id = $this->uploadAttachementToS3($att, $account_id, $huddle_id, $user_current_account);
                if ($document_id != false) {
                    $data = array(
                        'comment_id' => $comment_id,
                        'document_id' => $document_id,
                        'site_id'=>$this->site_id
                    );
                    $ca = DB::table('comment_attachments')->insertGetId($data);
                    $response[] = $document_id;
                }
            }
        }

        if (!empty($removeAttachment)) {
            $removeAttachmentIds = explode(',', $removeAttachment);
            DB::table('comment_attachments')->whereIn('document_id', $removeAttachmentIds)->delete();
            // Document::whereIn('id', $removeAttachmentIds)->update(['published' => 0]);
        }
        $discussion = Comment::getsingleDiscussion($huddle_id, $this->site_id, $comment_id, $user_id);
        $channel = 'discussions-list-' . $huddle_id;
        if (!empty($discussion[0]["parent_comment_id"]) && $discussion[0]["parent_comment_id"] != "null") {
            $channel = 'discussions-details-' . $discussion_id;
        }
        $res = array(
            'document_id' => $comment_id,
            'reference_id' => $discussion_id,
            'data' => $discussion,
            'huddle_id' => $huddle_id,
            'channel' => $channel,
            'event' => 'discussion_edited'
        );

        HelperFunctions::broadcastEvent($res);
        return response()->json(['success' => true, 'message' => TranslationsManager::get_translation('gen_discussion_updated_successfully', 'Api/video_details',  $this->site_id), "data" => $discussion], 200);
    }
    
    public function getSingleDiscussion(Request $request)
    {
        $discussion = Comment::getsingleDiscussion($request->get("huddle_id"), $this->site_id, $request->get("comment_id"), $request->get("user_id"));
        return $discussion;
        
    }

    function uploadAttachementToS3($data, $account_id, $account_folder_id, $user_current_account) {

        if (!empty($data['tmp_name'])) {

            if (!is_uploaded_file($data['tmp_name'])) {
                return FALSE;
            }

            $users = self::ConvertToArray($user_current_account);
            $user_id = $users['User']['id'];

            $s3_folder_path = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
            //require_once('src/services.php');
//uploaded video

            $uploaded_video_file = realpath(dirname(__FILE__) . '/../../../../..') . "/app/webroot/files/tempupload/" . $data['name'];

            if (file_exists($uploaded_video_file))
                unlink($uploaded_video_file);

            if (move_uploaded_file($data['tmp_name'], $uploaded_video_file)) {

                /* $path_parts = pathinfo($uploaded_video_file);

                  $new_uploaded_video_file = str_replace(basename($uploaded_video_file), HelperFunctions::cleanFileName($path_parts['filename']) . "." . $path_parts['extension'], $uploaded_video_file);
                  rename($uploaded_video_file, $new_uploaded_video_file);

                  $uploaded_video_file = $new_uploaded_video_file; */

                $path_parts = HelperFunctions::customPathInfo($uploaded_video_file);
                $video_file_name = $path_parts['filename'] . "." . $path_parts['extension'];

                // $s3_service = new S3Services(
                //         Configure::read('amazon_base_url'), Configure::read('bucket_name'), Configure::read('access_key_id'), Configure::read('secret_access_key')
                // );


                $client = S3Client::factory(array(
                            'key' => config('s3.access_key_id'),
                            'secret' => config('s3.secret_access_key')
                ));

                $s_url = $client->putObject([
                    'Bucket' => config('s3.bucket_name'),
                    'Key' => "uploads/" . $s3_folder_path . "/" . $video_file_name,
                    'SourceFile' => $uploaded_video_file
                ]);


                // $s3_service = new \App\Services\S3Services(
                //         $conf['amazon_base_url'], $conf['bucket_name'], $conf['access_key_id'], $conf['secret_access_key']
                // );
                //$s_url = $s3_service->publish_to_s3($uploaded_video_file, "uploads/" . $s3_folder_path . "/", false);

                $data = array(
                    'account_id' => $account_id,
                    'doc_type' => 2,
                    'url' => $s3_folder_path . "/" . $video_file_name,
                    'original_file_name' => $video_file_name,
                    'active' => 1,
                    'created_date' => date("Y-m-d h:i:s"),
                    'created_by' => $user_id,
                    'last_edit_date' => date("Y-m-d h:i:s"),
                    'last_edit_by' => $user_id,
                    'published' => '1',
                    'post_rubric_per_video' => '1',
                    'site_id' => $this->site_id
                );

                $last_id = DB::table('documents')->insertGetId($data);

                return $last_id;
            } else {
                return false;
            }
        } else {
            return FALSE;
        }
    }

    public function delete_discussion(Request $request) {
        $input = $request->all();
        $discussion_id = $input['discussion_id'];
        $original_discussion_id = $input['original_discussion_id'];
        $huddle_id = $input['huddle_id'];
        $discussion = Comment::getsingleDiscussion($huddle_id, $this->site_id, $discussion_id);
        $response = Comment::deleteDiscussion($discussion_id, $this->site_id);
        if ($response['success']) {
            $channel = "discussions-list-" . $huddle_id;
            if ( !empty($discussion) && isset($discussion[0]) && (!empty($discussion[0]["parent_comment_id"]) || $discussion[0]["parent_comment_id"] != "null") ) {
                $comment_count_event = array(
                    'discussion_id' => $discussion[0]["parent_comment_id"],
                    'reference_id' => $original_discussion_id,
                    'data' => $discussion,
                    'huddle_id' => $huddle_id,
                    'channel' => "discussion-comment-changes-" . $huddle_id,
                    'event' => 'comment_deleted'
                );
                HelperFunctions::broadcastEvent($comment_count_event);

                $channel = "discussions-details-" . $original_discussion_id;
            }
            $res = array(
                'document_id' => $discussion_id,
                'reference_id' => $original_discussion_id,
                'data' => $discussion,
                'huddle_id' => $huddle_id,
                'channel' => $channel,
                'event' => 'discussion_deleted'
            );

            HelperFunctions::broadcastEvent($res);
            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        }
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function convert_array($data) {
        $data = collect($data)->map(function($x) {
                    return (array) $x;
                })->toArray();
        return $data;
    }

    public function uploadVideos(Request $request) {

        $account_id = $request->get("account_id");
        $site_id = $request->header('site_id');
        $user_id = $request->get("user_id");
        $suppress_render = $request->has("suppress_render") ? $request->get("suppress_render") : false;
        $suppress_success_email = $request->has("suppress_success_email") ? $request->get("suppress_success_email") : false;
        $workspace = $request->has("workspace") ? $request->get("workspace") : false;
        $activity_log_id = $request->has("activity_log_id") ? $request->get("activity_log_id") : '';
        $video_file_name_org = $request->get('video_file_name');
        $video_url = $request->get('video_url');
        $video_file_size = $request->get('video_file_size');
        $direct_publish = $request->get("direct_publish");
        $af_id = 0;
        if (empty($request->get("account_folder_id"))) {
            $data = array(
                'account_id' => $account_id,
                'folder_type' => 3,
                'name' => $video_file_name_org,
                'desc' => '',
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
                'site_id' => $this->site_id
            );
            $af_id = DB::table('account_folders')->insertGetId($data);
            
            $data_account_folder_meta_data = array(
                'account_folder_id' => $af_id,
                'meta_data_name' => 'folder_type',
                'meta_data_value' => '5',
                'created_by' => $user_id,
                'created_date' => date('Y-m-d H:i:s'),
                'site_id' => $this->site_id
            );

            DB::table('account_folders_meta_data')->insertGetId($data_account_folder_meta_data);
            
            
        }
        $account_folder_id = $request->get("account_folder_id") ? $request->get("account_folder_id") : $af_id;
        if ($request->isMethod("POST")) {

            if ($workspace) {
                //$suppress_render = true;
                $loggedInUser = $request->get('user_current_account');
                $user_role = HelperFunctions::get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = HelperFunctions::check_if_account_in_trial_intercom($account_id);
//      if (IS_QA):

                $meta_data = array(
                    'workspace_video' => 'true',
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );


                HelperFunctions::create_intercom_event('workspace-video-uploaded', $meta_data, $loggedInUser['User']['email']);
                $this->create_churnzero_event('Workspace+Video+Uploaded', $account_id, $loggedInUser['User']['email']);

//    endif;
            } else {
                $h_type = AccountFolderMetaData::where('account_folder_id', $account_folder_id)->where('meta_data_name', 'folder_type')->first();
                $htype = isset($h_type->meta_data_value) ? $h_type->meta_data_value : "1";
                if ($htype == 1) {
                    $huddle_type = 'Collaboration Huddle';
                } elseif ($htype == 2) {
                    $huddle_type = 'Coaching Huddle';
                } else {
                    $huddle_type = 'Assessment Huddle';
                }
                $loggedInUser = $request->get('user_current_account');
                $user_role = HelperFunctions::get_user_role_name($loggedInUser['users_accounts']['role_id']);
                $in_trial_intercom = HelperFunctions::check_if_account_in_trial_intercom($account_id);
// if (IS_QA):

                $meta_data = array(
                    'huddle_video' => 'true',
                    'huddle_type' => $huddle_type,
                    'user_role' => $user_role,
                    'is_in_trial' => $in_trial_intercom,
                    'Platform' => 'Web'
                );

                if ($huddle_type == 'Assessment Huddle' && !HelperFunctions::check_if_evalutor($account_folder_id, $loggedInUser['User']['id'])) {

                    HelperFunctions::create_intercom_event('huddle-video-submitted-for-assessment', $meta_data, $loggedInUser['User']['email']);
                    $this->create_churnzero_event('Huddle+Video+Uploaded', $account_id, $loggedInUser['User']['email']);
                } else {

                    HelperFunctions::create_intercom_event('huddle-video-uploaded', $meta_data, $loggedInUser['User']['email']);
                    $this->create_churnzero_event('Huddle+Video+Uploaded', $account_id, $loggedInUser['User']['email']);
                }

//  endif;
            }

            $previous_storage_used = Account::getPreviousStorageUsed($account_id, $site_id);

            $current_user = User::find($user_id);
            $current_account_folder = AccountFolder::where('account_folder_id', $account_folder_id)->first();

            $account_folder_type_string = "Workspace";
            $video_title = $request->get('video_title');

            if ($current_account_folder->folder_type == 1) {
                $account_folder_type_string = $current_account_folder->name;
                if (empty($video_title)) {
                    $video_title = $account_folder_type_string;
                }
            }

            $path_parts = pathinfo($video_url);
            $video_file_name = HelperFunctions::cleanFileName($path_parts['filename']) . "." . $path_parts['extension'];
            $video_file_name_only = HelperFunctions::cleanFileName($path_parts['filename']);

            $request_dump = addslashes(print_r($_POST, true));

            $document = new Document();
            $document->account_id = $account_id;
            $document->doc_type = 1;
            $document->url = $video_url;
            $document->original_file_name = $video_file_name_org;
            $document->active = 1;
            $document->created_date = date("Y-m-d H:i:s");
            $document->created_by = $user_id;
            $document->last_edit_date = date("Y-m-d H:i:s");
            $document->recorded_date = date("Y-m-d H:i:s");
            $document->file_size = (int) $video_file_size;
            $document->last_edit_by = $user_id;
            $document->published = '0';
            $document->post_rubric_per_video = '1';
            $document->site_id = $site_id;
            $document->save();

            if ($document) {
                $video_id = $document->id;

                if (!empty($activity_log_id)) {
                    $activity_log = UserActivityLog::find($activity_log_id);
                    $activity_log->ref_id = $video_id;
                    $activity_log->save();
                }

                if ($current_account_folder->folder_type == 1) {

                    $user_activity_logs = array(
                        'ref_id' => $video_id,
                        'desc' => $video_file_name_org,
                        'url' => $this->base . '/Huddles/view/' . $account_folder_id . "/1/" . $video_id,
                        'account_folder_id' => $account_folder_id,
                        'type' => '2',
                        'environment_type' => 2,
                        'site_id' => $this->site_id
                    );
                    UserActivityLog::user_activity_logs($site_id, $user_activity_logs, $account_id, $user_id);
                } elseif ($current_account_folder->folder_type == 3) {
                    $user_activity_logs = array(
                        'ref_id' => $video_id,
                        'desc' => $video_file_name_org,
                        'url' => $this->base . '/MyFiles/view/1/' . $account_folder_id,
                        'account_folder_id' => $account_folder_id,
                        'type' => '2',
                        'environment_type' => 2,
                        'site_id' => $this->site_id
                    );
                    UserActivityLog::user_activity_logs($site_id, $user_activity_logs, $account_id, $user_id);
                }
                
                
                $Videoduration = self::getVideoDuration($video_id)["data"];
                $this->create_churnzero_event('Video+Hours+Uploaded', $account_id, $loggedInUser['User']['email'],$Videoduration); 

//require('src/services.php');
                $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                $s3_src_folder_path = $video_url;

                $s3_service = new S3Services(
                        config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
                );

                $s3_service->set_acl_permission($s3_src_folder_path);
// $video_file_size_in_mbs = isset($video_file_size) && !empty($video_file_size) ? number_format($video_file_size / 1048576, 2) : 0;
                $video_file_size_in_int = isset($video_file_size) ? (int) $video_file_size : 0;
                $video_file_size_in_mbs = !empty($video_file_size_in_int) ? $video_file_size_in_int / 1048576 : 0;

                if ($video_file_size_in_mbs > 800 || $video_file_size_in_mbs === 0) {
                    $direct_publish = 0;
                }
                
                $extensions = ['3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov' , 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv','aiff'];
                
                if(in_array(strtolower($path_parts['extension']), $extensions))
                {
                   $direct_publish = 0; 
                }

                if ($direct_publish == 0) {

                    if ($suppress_success_email == false) {
                        $s3_zencoder_id = HelperFunctions::CreateJobQueueTranscodingJob($video_id, $s3_src_folder_path);
                    } else {
                        $s3_zencoder_id = HelperFunctions::CreateJobQueueTranscodingJob($video_id, $s3_src_folder_path, $suppress_success_email);
                    }
                }

                if ($direct_publish == 1) {

                    try {
                        if (strtolower($path_parts['extension']) == "mp4") {
                            $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name_only" . "_enc.mp4";
                            $published_for_audio = 1;
                        } else {
                            $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name_only" . "_enc.mp3";
                            $published_for_audio = 0;
                        }

                        $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);
                        $s3_zencoder_id = $s3_service->encode_videos($s_url, "uploads/" . $s3_dest_folder_path_only, "uploads/" . $s3_dest_folder_path, config('core.encoder_provider'));

                        if(isset($h_type) && !empty($h_type)){
                            $this->sendVideoUploadEmailToHuddleParticipants($current_account_folder->name, $account_folder_id, $video_id, $loggedInUser, $account_id, $user_id, $current_account_folder->folder_type);
                        } else {
                            //send video added email
                            S3Services::sendVideoPublishedNotification($current_account_folder->name, $account_folder_id, $video_id, $loggedInUser, $account_id, $loggedInUser["User"]["email"], $user_id, $current_account_folder->folder_type, '8');
                        }
                    } catch (\Exception $e) {
                        $user_activity_logs = array(
                            'ref_id' => $user_id,
                            'desc' => 'Error @ Line:' . __LINE__ . ' ' . $e->getMessage(),
                            'url' => 'uploadVideos',
                            'type' => '313',
                            'account_id' => $account_id,
                            'account_folder_id' => $account_folder_id,
                            'environment_type' => '2',
                            'site_id' => $this->site_id
                        );
                        UserActivityLog::user_activity_logs($site_id, $user_activity_logs, $account_id, $user_id);

                        return response()->json(["status" => false, "message" => $e->getMessage(), "status_code" => http_response_code()]);
                        // var_dump("Line: " . __LINE__, http_response_code($e->getMessage()));
                        // echo "<hr>";
                        // var_dump($e->getMessage());
                        // exit;
                    }
//revert back to actual destination.

                    $s3_dest_folder_path = "uploads/" . $s3_dest_folder_path;

                    $document = Document::find($video_id);
                    $document->zencoder_output_id = $s3_zencoder_id;
                    $document->url = $s3_dest_folder_path;
                    $document->encoder_provider = config('core.encoder_provider');
                    $document->file_size = (!isset($video_file_size) ? 0 : $video_file_size);
                    $document->published = $published_for_audio;
                    $document->save();

                    $document_files = new DocumentFiles();
                    $document_files->document_id = $video_id;
                    $document_files->url = $s3_dest_folder_path;
                    $document_files->duration = 0;
                    $document_files->file_size = 0;
                    $document_files->transcoding_job_id = $s3_zencoder_id;
                    $document_files->transcoding_status = 2;
                    $document_files->default_web = true;
                    $document_files->site_id = $this->site_id;
                    $document_files->save();
                } else {
                    $document = Document::find($video_id);

                    $document->zencoder_output_id = $s3_zencoder_id;
                    $document->encoder_provider = config('core.encoder_provider');
                    $document->file_size = (int) $video_file_size;
                    $document->request_dump = "'$request_dump'";
                    $document->site_id = $this->site_id;
                    $document->save();
                }
                if (!empty($request->get('video_comments')) && $request->get('video_comments') != '') {
                    // echo $this->data['video_comments']."<br/>";
                    $comments = json_decode($request->get('video_comments'));
                    for ($i = 0; $i < count($comments); $i++) {
                        $time = '';
                        $accessLevel = '';
                        if ($comments[$i]->for == 'synchro_time' && $comments[$i]->synchro_time != '') {
                            $time = $comments[$i]->synchro_time;
                        } else {
                            $accessLevel = 'All';
                        }
                        if (isset($comments[$i]->ref_type) && $comments[$i]->ref_type == '6') {
                            $ref_type = $comments[$i]->ref_type;
                        } else {
                            $ref_type = '2';
                        }

                        if (isset($comments[$i]->audio_duration) && $comments[$i]->audio_duration > 0) {
                            $audio_duration = $comments[$i]->audio_duration;
                        } else {
                            $audio_duration = '0';
                        }
                        $data = array(
                            'comment' => $comments[$i]->comment,
                            'time' => $time,
                            'access_level' => $accessLevel,
                            'ref_id' => $video_id,
                            'ref_type' => $ref_type,
                            'created_date' => $comments[$i]->createdDate,
                            'created_by' => $user_id,
                            'last_edit_date' => $comments[$i]->editedDate,
                            'last_edit_by' => $user_id,
                            'user_id' => $user_id,
                            'audio_duration' => $audio_duration,
                            'site_id' => $this->site_id
                        );
                        //$this->Comment->create();
                        if (DB::table('comments')->insert($data)) {
                            $comment_id = DB::getPdo()->lastInsertId();
                            $standards = isset($comments[$i]->standards) ? $comments[$i]->standards : '';
                            if (!empty($standards)) {

                                //$this->save_standard_tag($standards, $this->Comment->id, $video_id, $account_id, $user_id, $account_folder_id);
                                $this->save_standard_tag($standards, $comment_id, $video_id, $account_id, $user_id, $account_folder_id);
                            }
                            $tags = isset($comments[$i]->default_tags) ? $comments[$i]->default_tags : '';
                            if (!empty($tags)) {
                                //$this->save_tag($tags, $this->Comment->id, $video_id, $account_id, $user_id);
                                $this->save_tag($tags, $comment_id, $video_id, $account_id, $user_id);
                            }

                            if (isset($account_id) && isset($user_id)) {
                                if ($current_account_folder->folder_type == 1) {
                                    $user_activity_logs = array(
                                        'ref_id' => $this->Comment->id,
                                        'desc' => $comments[$i]->comment,
                                        'url' => $this->base . '/huddles/view/' . $account_folder_id . "/1/" . $video_id,
                                        'account_folder_id' => $account_folder_id,
                                        'type' => '5',
                                        'account_id' => $account_id,
                                        'environment_type' => 1,
                                        'site_id' => $this->site_id
                                    );
                                } else {
                                    $user_activity_logs = array(
                                        'ref_id' => $this->Comment->id,
                                        'desc' => $comments[$i]->comment,
                                        'url' => $this->base . '/MyFiles/view/1/' . $account_folder_id,
                                        'account_folder_id' => $account_folder_id,
                                        'type' => '5',
                                        'account_id' => $account_id,
                                        'environment_type' => 1,
                                        'site_id' => $this->site_id
                                    );
                                }
                                UserActivityLog::user_activity_logs($site_id, $user_activity_logs, $account_id, $user_id);
                                //$this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                            }
                        }
                    }
                }

                if ($previous_storage_used && $previous_storage_used->storage_used != '') {
                    $new_storage_used = $video_file_size + $previous_storage_used->storage_used;
                } else {
                    $new_storage_used = $video_file_size;
                }

                $account = Account::find($account_id);
                $account->storage_used = $new_storage_used;
                $account->save();

                JobQueue::job_queue_storage_function($site_id, $account_id, '4');
                $account_folder_doc = new AccountFolderDocument();
                $account_folder_doc->account_folder_id = $account_folder_id;
                $account_folder_doc->document_id = $video_id;
                $account_folder_doc->title = $video_title;
                $account_folder_doc->zencoder_output_id = "$s3_zencoder_id";
                $account_folder_doc->site_id = $this->site_id;
                //$account_folder_doc->encoder_provider = config('core.encoder_provider');
                $account_folder_doc->desc = $request->get('video_desc');
                $account_folder_doc->save();
            } else {
                $video_id = 0;
            }

            if ($suppress_render == true) {
                return response()->json(["status" => false, "message" => "Video ID = " . $video_id]);
            } else {
                $folder_type = 1;
                if ($workspace) {
                    $folder_type = 3;
                }
                $res = Document::get_single_video_data($this->site_id, $video_id, $folder_type);
                $data_ws = null;
                if (count($res)) {
                    $duration = self::getVideoDuration($video_id)["data"];
                    $res[0]['duration'] = $duration;
                    $res[0]['video_duration'] = $duration;
                    $res[0]['total_comments'] = 0;
                    $res[0]['total_attachment'] = 0;
                    $data_ws = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($res[0],0,0,true);
                }
                $res = array(
                    'video_id' => $video_id,
                    'data' => $data_ws,
                    'url' => ''
                );
                $allowed_participants = [];
                if ($loggedInUser && !$workspace) {
                    $allowed_participants = HelperFunctions::get_security_permissions($account_folder_id);
                    if ($htype == 3 && $loggedInUser['users_accounts']['role_id'] == "210") {
                        foreach ($allowed_participants as $key => $allowed_participant) {
                            if ($allowed_participant["user_id"] != $user_id && $allowed_participant["role_id"] == "210") {
                                unset($allowed_participants[$key]);
                            }
                        }
                    }
                }

                $channel_name = 'huddle-details-' . $account_folder_id;
                if ($workspace) {
                    $channel_name = "workspace-" . $account_id . "-" . $user_id;
                }
                $channel_data = array(
                    'document_id' => $video_id,
                    'reference_id' => $video_id,
                    'data' => $data_ws,
                    'new_video_uploaded' => true,
                    'allowed_participants' => $allowed_participants,
                    'huddle_id' => $account_folder_id,
                    'channel' => $channel_name,
                    'event' => "resource_added"
                );

                HelperFunctions::broadcastEvent($channel_data);

//$this->set('ajaxdata', json_encode($res));
//$this->render('/Elements/ajax/ajaxreturn');
                return response()->json(["status" => true, "message" => $res, "data" => $data_ws]);
            }
        } else {
            return response()->json(["status" => false, "message" => "Not Posted"]);
        }
    }

    public function save_standard_tag($standard_tag, $comment_id, $ref_id, $account_id, $user_id, $huddle_id, $standards_acc_tags) {
        $framework_id = AccountTag::getFrameworkId($account_id, $huddle_id, $this->site_id);


        $video_data = Document::where(array(
                    'id' => $ref_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        }

        if ($video_data['post_rubric_per_video'] == '1') {

            $account_folder_details = AccountFolderDocument::where(array(
                        'document_id' => $ref_id,
                        'site_id' => $this->site_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            if (!empty($account_folder_details)) {
                if (!empty($account_folder_details['video_framework_id'])) {
                    $framework_id = $account_folder_details['video_framework_id'];
                }
            }
        }
        // $standards = str_replace('# ', '', $standard_tag);
        //$standardtags = explode(',', $standards);
        $standardtags_acc_tags = explode(',', $standards_acc_tags);
        $save_cmt_tag = [];
        foreach ($standardtags_acc_tags as $standardtag) {
            $tagcode = explode('-', $standardtag);
            $code_standard = AccountTag::getStandardByCodeWeb($this->site_id, $standardtag, $account_id, $framework_id);
            $save_cmt_tag [] = [
                "comment_id" => $comment_id,
                "ref_type" => "0",
                "ref_id" => $ref_id,
                "tag_title" => isset($code_standard->tag_title) ? $code_standard->tag_title : '',
                "account_tag_id" => isset($code_standard->account_tag_id) ? $code_standard->account_tag_id : '',
                "created_by" => $user_id,
                "site_id" => $this->site_id,
                "created_date" => date('Y-m-d H:i:s')
            ];

            $user_activity_logs = [
                'ref_id' => $comment_id,
                'desc' => isset($code_standard->tag_title) ? $code_standard->tag_title : '',
                'url' => '/' . $comment_id,
                'type' => '15',
                'account_id' => $account_id,
                'account_folder_id' => isset($code_standard->account_tag_id) ? $code_standard->account_tag_id : '',
                'environment_type' => 2,
                'site_id' => $this->site_id
            ];

            $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
        }

        return empty($save_cmt_tag) ? false : DB::table('account_comment_tags')->insert($save_cmt_tag);
    }

    function associateVideoDocuments(Request $request) {
        $document_id = $request->get("document_id");
        $account_folder_id = $request->get("account_folder_id");
        $video_ids = $request->get("associated_videos");
        $video_ids_array = explode(',', $video_ids);

        $account_folder_documents = AccountFolderDocument::where('account_folder_id', $account_folder_id)->where('document_id', $document_id)->first();

        if ($account_folder_documents) {
            AccountFolderDocumentAttachment::where('account_folder_document_id', $account_folder_documents->id)->delete();

            for ($i = 0; $i < count($video_ids_array); $i++) {
                $video_id = $video_ids_array[$i];
                if (!empty($video_id)) {
                    $AFDA = new AccountFolderDocumentAttachment();
                    $AFDA->account_folder_document_id = $account_folder_documents->id;
                    $AFDA->attach_id = $video_id;
                    $AFDA->save();
                }
            }
        }
        return array("status" => true, "message" => "Successfully Attached Resources");
    }

    public function getHuddleVideos(Request $request) {
        $account_id = $request->get("account_id");
        $account_folder_id = $request->get("account_folder_id");
        $doc_type = $request->has("doc_type") ? $request->get("doc_type") : 1;
        $documents = Document::join("account_folder_documents", "documents.id", "account_folder_documents.document_id")
                ->select("documents.*", "account_folder_documents.title")
                ->where("account_folder_documents.account_folder_id", $account_folder_id)
                ->where("documents.account_id", $account_id)
                ->where("documents.doc_type", $doc_type)
                ->get();
        return response()->json(["status" => true, "message" => "Success", "data" => $documents]);
    }

    public static function getSecureAmazonCloudFrontHttpUrl($resource, $name = '', $timeout = 600) {


//This comes from key pair you generated for cloudfront
        $keyPairId = config('core.cloudfront_keypair');

//Read Cloudfront Private Key Pair
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => realpath(dirname(__FILE__) . '/../../..') . "/config/cloudfront_key.pem",
                    'key_pair_id' => $keyPairId,
        ));

        $streamHostUrl = config('core.cloudfront_host_http_url');
        $resourceKey = $resource;
        $expires = time() + 21600;

//Construct the URL
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));

        return $signedUrlCannedPolicy;
    }
    
    public function get_video_duration(Request $request) {
        $document_id = $request->get("document_id");
        return self::getVideoDuration($document_id);
    }

    public static function getVideoDuration($document_id) {
        // return ['status'=>''];
        try {
            $videoDetail = Document::find($document_id);
            if (isset($videoDetail)) {
                $videoID = $videoDetail->id;
                $videoFilePath = pathinfo($videoDetail->url);
                $videoFileName = $videoFilePath['filename'];

                $use_ffmpeg_path = config('core.use_ffmpeg_path');
                $ffmpeg_binaries = config('core.ffmpeg_binaries');
                $ffprobe_binaries = config('core.ffprobe_binaries');

                if ($use_ffmpeg_path == 0) {

                    $ffmpeg = FFMpeg::create();
                } else {

                    $ffmpeg = FFMpeg::create(array(
                                'ffmpeg.binaries' => $ffmpeg_binaries,
                                'ffprobe.binaries' => $ffprobe_binaries,
                    ));
                }
//echo "<pre>"; print_r($ffmpeg);
//die;
                $lvideo_path = '';

//$video_path = $this->getSecureAmazonCloudFrontHttpUrl("uploads/" . $videoFilePath['dirname'] . "/" . $videoFileName . "_enc.mp4");
                $duration = 0;
                $document_files_array = VideoController::get_document_url($videoDetail);

                $lvideo_path = self::getSecureAmazonCloudFrontHttpUrl(urlencode($videoDetail->url));

                $ffmpeg_streams = $ffmpeg->getFFProbe()->streams($lvideo_path);
                // $ffmpeg_streams ='';
                if (isset($ffmpeg_streams)) {

                    $ffmpeg_streams_videos = $ffmpeg_streams->videos();
                    $ffmpeg_streams_audios = $ffmpeg_streams->audios();
                    if (isset($ffmpeg_streams_videos)) {
                        $ffmpeg_streams_videos_first = $ffmpeg_streams_videos->first();

                        if (isset($ffmpeg_streams_videos_first) && !empty($ffmpeg_streams_videos_first)) {
                            // $duration = $ffmpeg_streams_videos->first()->get('duration');
                            $duration = $ffmpeg_streams_videos_first->get('duration');
                        }
                    }
                }

                return ["status" => true, "message" => "duration found.", "data" => $duration];
            } else {

                return ["status" => false, "message" => "Unable to find duration.", "data" => -1];
            }
        } catch (\Exception $ex) {

            return ["status" => false, "message" => "Unable to find duration.", "data" => -1];
        }
    }

    public function SendDiscussionEmail($account_id, $User, $attachment_path, $comment, $data = '', $reply = '', $user_current_account) {

        $users = (array) $user_current_account;
        $User = $User->toArray();
        $users['User'] = (array) $users['User'];
        $users['accounts'] = (array) $users['accounts'];
        $email_from = $users['User']['first_name'] . " " . $users['User']['last_name'] . " <" . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . ">";
        $email_to = $User['email'];
        $company_name = isset($users['accounts']['company_name']) ? $users['accounts']['company_name'] : $users['company'];
        $site_email_subject = Sites::get_site_settings('email_subject', $this->site_id);

        if ($attachment_path != '') {
            // To be implemented WAQAS
            // $this->Email->attachments = array($attachment_path);
        }
        $site_id = $this->site_id;
        $html = '';
        if ($reply != '' && $reply != 'null') {
            $reciver_detail = User::where(array('id' => $users['User']['id'], 'site_id' => $this->site_id))->first();
            $lang = $reciver_detail['lang'];
            if ($lang == 'en') {
                $email_subject = $site_email_subject . " - New Discussion Message Posted";
            } else {
                $email_subject = $site_email_subject . " - PublicaciÃ³n de discusiÃ³n";
            }
            $key = "huddle_discussions_reply_" . $this->site_id . "_" . $lang;
            $sibme_base_url = config('s3.sibme_base_url');
            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $data['created_by'], $html);
            $html = str_replace('{discussion_topic}', $data['discussion_topic'], $html);
            $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
            $html = str_replace('{redirect}', $data['discussion_link'], $html);
            $html = str_replace('{unsubscribe}', '#', $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
        } else {

            $lang = $User['lang'];
            if ($lang == 'en') {
                $email_subject = $site_email_subject . " - New Discussion Added";
            } else {
                $email_subject = $site_email_subject . " - Nueva discusiÃ³n";
            }

            $key = "huddle_discussion_" . $this->site_id . "_" . $lang;

            $result = SendGridEmailManager::get_send_grid_contents($key);
            if (!isset($result->versions[0])) {
                return false;
            }
            
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $data['created_by'], $html);
            $html = str_replace('{discussion_topic}', $data['discussion_topic'], $html);
            $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
            $html = str_replace('{redirect}', $data['discussion_link'], $html);
            $html = str_replace('{unsubscribe}', '#', $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
        }

        $acid = isset($users['accounts']['account_id']) ? $users['accounts']['account_id'] : $users['account_id'];
        $auditEmail = array(
            'account_id' => $acid,
            'site_id' => $this->site_id,
            'email_from' => $email_from,
            'email_to' => $data['email'],
            'email_subject' => $email_subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {

            DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = 1;

            if ($use_job_queue) {
                JobQueue::add_job_queue($this->site_id, 1, $email_to, $email_subject, $html, true);
                return TRUE;
            }
        }

        return FALSE;
    }

    function check_videos_count(Request $request) {
        $input = $request->all();
        $huddle_id = $input['huddle_id'];
        $user_id = $input['user_id'];
        $huddle_type = $this->get_huddle_type($huddle_id);
        if ($huddle_type == 1 || $huddle_type == 2) {
            return response()->json(['message' => '', 'can_upload' => 'yes'], 200);
        }
        $check_if_evaluated_participant = HelperFunctions::check_if_evaluated_participant($huddle_id, $user_id);
        $get_eval_participant_videos = HelperFunctions::get_eval_participant_videos($huddle_id, $user_id, $this->site_id);
        $submission_allowed = AccountFolderMetaData::where('account_folder_id', $huddle_id)->where('meta_data_name', 'submission_allowed')->first();
        $vide_max_limit = 1;

        if ($submission_allowed) {
            $submission_allowed = $submission_allowed->toArray();

            if ($submission_allowed['meta_data_value'] != '') {
                $vide_max_limit = $submission_allowed['meta_data_value'];
            }
        }



        if ($check_if_evaluated_participant) {
            $submission_date = HelperFunctions::get_submission_date($huddle_id, false, $this->site_id);
            if ($submission_date != '') {
                $submission_string = strtotime($submission_date);
                $current_time = strtotime(date('Y-m-d H:i:s a', time()));
                if ($submission_string < $current_time) {

                    return response()->json(['message' => 'You cannot upload a video because your submission date has expired.', 'can_upload' => 'no'], 200);
                }
            }
        }


        if ($check_if_evaluated_participant && $get_eval_participant_videos >= $vide_max_limit) {
            return response()->json(['message' => 'You have already submitted a video to be evaluated.  You must delete your submission before resubmitting.', 'can_upload' => 'no'], 200);
        } else {
            return response()->json(['message' => '', 'can_upload' => 'yes'], 200);
        }
    }

    public function addHttpWithLinks($comment) {
        $tidy_config = array(
            //'clean' => true,
            'output-xhtml' => true,
            'show-body-only' => true,
            'wrap' => 0,
            //'fix-style-tags'=>false
        );
        
        $tidy = tidy_parse_string( $comment, $tidy_config, 'UTF8');
        $tidy->cleanRepair();
        $dom = new \DOMdocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . (string) $tidy);
        libxml_use_internal_errors(false);

        foreach ($dom->getElementsByTagName('a') as $item) {
            $href = $item->getAttribute('href');
            if (!$this->isURLContainsHttp($href)) {
                $href = "https://" . $href;
                $item->setAttribute('href', $href);
                $dom->saveHTML($item);
            }
        }
        // echo "Ddd";
        // var_dump($dom->saveHTML());
        // die;
        return $dom->saveHTML();
    }

    public function isURLContainsHttp($url) {
        $url = parse_url($url);
        return isset($url['scheme']) && in_array($url['scheme'], ['http', 'https']);
    }
    
    
    function document_viewed_mobile(Request $request)
    {
        $input = $request->all();
        
        $document_id = $input['document_id'];
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        
        
            $user_activity_logs = array(
                'ref_id' => $document_id,
                'site_id' => $this->site_id,
                'desc' => 'Download Resource',
                'url' => '/download_document',
                'type' => '13',
                'account_folder_id' => '',
                'environment_type' => '2',
            );
            UserActivityLog::user_activity_logs($this->site_id, $user_activity_logs, $account_id, $user_id);
            $user_detail = User::where(array('id' => $user_id))->first();

            if ($user_detail) {
                $user_detail = $user_detail->toArray();
                $this->create_churnzero_event('Resource+Viewed', $account_id, $user_detail['email']);
            }
            
            return response()->json(array('success' => true ), 200, [], JSON_PRETTY_PRINT);
        
    }
    
    function create_churnzero_event($event_name,$account_id,$user_email,$value = 1)
    {
         
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://analytics.churnzero.net/i?appKey=invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg&accountExternalId='.$account_id.'&contactExternalId='.$user_email.'&action=trackEvent&eventName='.$event_name.'&description='.$event_name.'&quantity='.$value );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
        
    }
    
    
    function get_comments_for_huddle_video($huddle_id,$video_id,$current_user_id,$huddle_type,$support_audio_annotation,$get_all_comments)
    {
        $default_photo = 1;
        if (!AccountFolderUser::check_if_evalutor($this->site_id, $huddle_id, $current_user_id) && (($huddle_type == '2' && $this->is_enabled_coach_feedback($huddle_id, $this->site_id)) || $huddle_type == '3')) {
            $comments_res = $this->getVideoComments($video_id, '', '', '', '', $support_audio_annotation, 1, false, '', true);
        } else {
            $comments_res = $this->getVideoComments($video_id, '', '', '', '', $support_audio_annotation);
        }

                $comment_result = array();
                
                foreach ($comments_res as $row) {
                    if ($row['ref_type'] == 6) {
                        if (config('use_cloudfront') == true) {
                            $url = $row['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];
                            if ($videoFilePath['dirname'] == ".") {

                                $video_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . ".m4a");
                                $relative_video_path = $videoFileName . ".m4a";
                            } else {

                                $video_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".m4a");
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".m4a";
                            }
                        } else {
                            $url = $row['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];

                            if ($videoFilePath['dirname'] == ".") {

                                $video_path = $this->getSecureAmazonUrl($videoFileName . ".m4a");
                                $relative_video_path = $videoFileName . ".m4a";
                            } else {

                                $video_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName . ".m4a");
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".m4a";
                            }
                        }
                        $row['comment'] = $video_path;
                    }
                    $created_date = new DateTime($row['created_date']);
                    $last_edit_date = new DateTime($row['last_edit_date']);

                    $row['last_edit_date'] = $last_edit_date->format(DateTime::W3C);
                    $row['created_date'] = $created_date->format(DateTime::W3C);

                    if (isset($row['image']) && $row['image'] != '') {
                        $use_folder_imgs = config('use_local_file_store');
                        if ($use_folder_imgs == false) {
                            $file_name = "static/users/" . $row['id'] . "/" . $row['image'];
                            $row['image'] = config('amazon_base_url') . config('bucket_name_cdn') . "/" . $file_name;
                        }

                        //$item['User']['image'] = $sibme_base_url . '/img/users/' . $item['User']['image'];
                        //else
                        //$item['User']['image'] = $view->Custom->getSecureSibmecdnUrl("static/users/" . $item['User']['id'] . "/" . $item['User']['image']);
                    } else {
                        if ($default_photo == 1) {
                            $row['image'] = config('sibme_base_url') . '/img/home/photo-default.png';
                        }
                    }

                    $commentDate = $row['created_date'];
                    $currentDate = date('Y-m-d H:i:s');

                    $start_date = new DateTime($commentDate);
                    $since_start = $start_date->diff(new DateTime($currentDate));

                    $commentsDate = '';
                    if ($since_start->y > 0) {
                        $commentsDate = "About " . $since_start->y . ($since_start->y > 1 ? ' Years Ago' : ' Year Ago');
                    } elseif ($since_start->m > 0) {
                        $commentsDate = "About " . $since_start->m . ($since_start->m > 1 ? ' Months Ago' : ' Month Ago');
                    } elseif ($since_start->d > 0) {
                        $commentsDate = $since_start->d . ($since_start->d > 1 ? ' Days Ago' : ' Day Ago');
                    } elseif ($since_start->h > 0) {
                        $commentsDate = $since_start->h . ($since_start->h > 1 ? ' Hours Ago' : ' Hour Ago');
                    } elseif ($since_start->i > 0) {
                        $commentsDate = $since_start->i . ($since_start->i > 1 ? ' Minutes Ago' : ' Minute Ago');
                    } elseif ($since_start->s > 0) {
                        $commentsDate = 'Less Than A Minute Ago';
                    } else {
                        $commentsDate = 'Just Now';
                    }

                    $row['Comment']['created_date_string'] = $commentsDate;

                    $response = $this->get_replies_from_comment($row, $get_all_comments);
                    $row['attachment_count'] = $this->match_timestamp_count_comment($row['id'], $video_id);
                    $row['attachment_names'] = $this->get_comment_attachment_file_names($row['id'], $video_id);
                    $row['Comment'] = $response;
                    $get_standard = $this->gettagsbycommentid($row['id'], array('0')); //get standards
                    $get_tags = $this->gettagsbycommentid($row['id'], array('1', '2')); //get tags
                    $row['standard'] = $get_standard;
                    $row['default_tags'] = $get_tags;

                    $comment_result[] = $row;
                }
                
                return $comment_result;
                
    }

    public static function removeRoleAnomalies()
    {
        //set role to 200 where is_coach is 1
        $update_counts = \DB::update("UPDATE account_folder_users AS afu
                    INNER JOIN account_folders_meta_data AS afmd
                       ON afmd.`account_folder_id` = afu.`account_folder_id`
                    SET afu.role_id = 200
                    WHERE role_id = 220
                     AND is_coach = 1
                     AND afmd.`meta_data_name` = 'folder_type'
                     AND afmd.`meta_data_value` IN (2,3)");
        echo "Total ".$update_counts." rows invalid roles updated to 200 where is_coach is 1 <br><br>";

        $update_counts = \DB::update("UPDATE account_folder_users AS afu
                    INNER JOIN account_folders_meta_data AS afmd
                       ON afmd.`account_folder_id` = afu.`account_folder_id`
                    SET afu.role_id = 210
                    WHERE role_id = 220
                     AND is_mentee = 1
                     AND afmd.`meta_data_name` = 'folder_type'
                     AND afmd.`meta_data_value` IN (2,3)");
        echo "Total ".$update_counts." rows invalid roles updated to 210 where is_mentee is 1 <br><br>";


        $invalid_role_users = \DB::select("SELECT * FROM account_folder_users WHERE account_folder_id IN 
              
                      (SELECT account_folder_id FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND meta_data_value IN (2,3))
                       
                       AND role_id = 220 and (is_coach = 0 OR is_coach IS NULL OR is_coach = '') and (is_mentee = 0 OR is_mentee IS NULL OR is_mentee = '')");
        $set_user_role_to_coach = [];
        foreach ($invalid_role_users as $invalid_role_user)
        {
            if(!in_array($invalid_role_user->account_folder_id, $set_user_role_to_coach))
            {
                $users_count_in_huddle = AccountFolderUser::where("account_folder_id",$invalid_role_user->account_folder_id)->count();
                if($users_count_in_huddle == 1)
                {
                    $set_user_role_to_coach[] = $invalid_role_user->account_folder_id;
                }
            }
        }
        if(!empty($set_user_role_to_coach))
        {
            //set invalid roles to 200 where only one user in huddle
            $update_counts = \DB::table("account_folder_users")->where("role_id",220)->whereIn("account_folder_id",$set_user_role_to_coach)->update(["role_id"=>200,"is_coach"=>1]);
            echo "Total ".$update_counts." rows invalid roles updated to 200 where only one user in huddle <br><br>";
        }
        else
        {
            echo "Total 0 rows invalid roles updated to 200 where only one user in huddle <br><br>";
        }



        $coaching_huddle_invalid_users = \DB::select("SELECT * FROM account_folder_users WHERE account_folder_id IN 
              
                      (SELECT account_folder_id FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND meta_data_value = 2)
                       
                       AND role_id = 220 and (is_coach = 0 OR is_coach IS NULL OR is_coach = '') and (is_mentee = 0 OR is_mentee IS NULL OR is_mentee = '')");

        $set_user_role_to_coach_2 = [];
        foreach ($coaching_huddle_invalid_users as $invalid_role)
        {
            $huddle_coachee_count = AccountFolderUser::where("account_folder_id",$invalid_role->account_folder_id)->where("role_id",210)->count();
            if($huddle_coachee_count == 1)
            {
                if(!in_array($invalid_role->account_folder_id,$set_user_role_to_coach_2))
                {
                    $set_user_role_to_coach_2[] = $invalid_role->account_folder_id;
                }
            }
        }
        if(!empty($set_user_role_to_coach_2))
        {
            //set invalid roles to 200 where atleast one coachee in coaching huddle
            $update_counts = \DB::table("account_folder_users")->where("role_id",220)->whereIn("account_folder_id",$set_user_role_to_coach_2)->update(["role_id"=>200,"is_coach"=>1]);
            echo "Total ".$update_counts." rows invalid roles updated to 200 where atleast one coachee in coaching huddle <br><br>";
        }
        else
        {
            echo "Total 0 rows invalid roles updated to 200 where atleast one coachee in coaching huddle <br><br>";
        }



        //fix all other remaining invalid roles
        $update_counts = \DB::update("UPDATE account_folder_users AS afu
                    INNER JOIN account_folders_meta_data AS afmd
                       ON afmd.`account_folder_id` = afu.`account_folder_id`
                    SET afu.role_id = 210
                    WHERE role_id = 220
                     AND afmd.`meta_data_name` = 'folder_type'
                     AND afmd.`meta_data_value` IN (2,3)");
        echo "Total ".$update_counts." rows invalid roles updated to 210 for all other coaching and assessment huddles <br><br>";

        $multiple_coachee = \DB::select("SELECT account_folder_id,COUNT(role_id) AS num_of_coachee FROM account_folder_users WHERE role_id = 210 AND account_folder_id IN (SELECT account_folder_id FROM account_folders_meta_data WHERE meta_data_name = 'folder_type' AND meta_data_value = 2) GROUP BY account_folder_id ");
        $huddles_with_multiple_coachee = [];
        if(!empty($multiple_coachee))
        {
            echo "Attention: Following coaching huddles have more then one coachee, please resolve manualy <br><br>";
            foreach ($multiple_coachee as $item)
            {
                if($item->num_of_coachee > 1)
                {
                    $huddles_with_multiple_coachee[] = (array)$item;
                }
            }
            print_r($huddles_with_multiple_coachee);
        }

        return response()->json(["status"=>true,"message"=>"Roles correction done"]);

    }

    public function sendVideoUploadEmailToHuddleParticipants($current_account_folder_name, $account_folder_id, $video_id, $loggedInUser, $account_id, $user_id, $current_account_folder_folder_type){
        $participants = AccountFolder::getHuddleUsersIncludingGroups($this->site_id, $account_folder_id, $user_id, $loggedInUser['users_accounts']['role_id'], true, [200,210,220]);
        if(!isset($participants['participants']) && empty($participants['participants'])){
            return false;
        }
        $participants = $participants['participants'];
        foreach ($participants as $participant) {
            if($user_id==$participant['user_id']){
                continue; // Skip current user. Because he is uploader of this video.
            }
            S3Services::sendVideoPublishedNotification($current_account_folder_name, $account_folder_id, $video_id, $loggedInUser, $account_id, $participant['user_email'], $participant['user_id'], $current_account_folder_folder_type, '8');
        }
    }

    public function check_video_assessed($video_id, $assessor_ids){
        if(empty($assessor_ids)){
            return false;
        }
        $user_ids = [];
        foreach($assessor_ids as $assessor_id){
            $user_ids [] = $assessor_id['user_id'];
        }
        $comments = Comment::where('ref_id',$video_id)->whereIn('created_by',$user_ids)->where('active',1)->count();
        return $comments>0;
    }

    public function send_push_notification(Request $request)
    {
        HelperFunctions::sendPushNotification($request->all());
        return response()->json(["status"=>true,"message"=>"success"]);
    }
    
    public function get_assessee_data(Request $request) {
        $huddle_id = $request->get('huddle_id');
        $user_role_id = $request->get('user_role_id');
        $user_id = $request->get('user_id');
        $result_array = array();       
        $participants = AccountFolder::getHuddleUsersIncludingGroups($this->site_id, $huddle_id, $user_id, $user_role_id,true)['participants'];
        foreach($participants as $row){
            if( $user_id ==  $row['user_id']){
                $row['total_submissions'] =  AccountFolderDocument::get_total_assessments($user_id,$huddle_id,$this->site_id); 
                $result_array[] = $row;
            }            
        }       
        return response()->json($result_array, 200, [], JSON_PRETTY_PRINT);
    }

    public function getWowzaStreamDetails(Request $request)
    {
        define("WOWZA_HOST",$this->wowzaUrl.":8087/v2");
        define("WOWZA_SERVER_INSTANCE", "_defaultServer_");
        define("WOWZA_VHOST_INSTANCE", "_defaultVHost_");
        define("WOWZA_USERNAME", "wowza");
        define("WOWZA_PASSWORD", "i-013e859309eb51dc3");

// It is simple to create a setup object for transporting our settings
        $setup = new Settings();
        $setup->setHost(WOWZA_HOST);
        $setup->setUsername(WOWZA_USERNAME);
        $setup->setPassword(WOWZA_PASSWORD);

// Connect to the server or deal with statistics NOTICE THE CAPS IN COM AND WOWZA


        $server = new Server($setup);
        $application = new Application($setup);
        $sf = new Statistics($setup);
//$response = $sf->getServerStatistics($server);
        $stream_name = $request->get("stream_name");
        $response = $sf->getIncomingApplicationStatistics($application,$stream_name);
        $response->bytesInRate = (int)$response->bytesInRate;
        return response()->json(["data"=>$response],200, [], JSON_PRETTY_PRINT);
    }

    public function getStreamViewerCount(Request $request)
    {
        $stream_name = $request->get("stream_name");
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $res = $client->get($this->wowzaUrl.":8086/connectioncounts");
        $myXMLData = $res->getBody();
        $xml= simplexml_load_string($myXMLData);
        $streams = $xml->VHost->Application->ApplicationInstance->Stream;
        $viewers = 0;
        if(!empty($xml->VHost->Application->ApplicationInstance->Stream))
        {
            foreach ($xml->VHost->Application->ApplicationInstance->Stream as $stream)
            {
                if($stream_name.'_all' == $stream->Name)
                {
                    $viewers = (int)(((array)$stream->SessionsTotal)[0]);
                    break;
                }
            }
        }
        return response()->json(["data"=>$viewers],200, [], JSON_PRETTY_PRINT);
    }

}
